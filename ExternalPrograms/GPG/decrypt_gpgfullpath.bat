cd %2
"C:\Program Files (x86)\GnuPG\bin\gpg.exe" --verbose --batch --passphrase-file %5 --import %4
"C:\Program Files (x86)\GnuPG\bin\gpg.exe" --verbose --batch --pinentry-mode loopback --use-embedded-filename --passphrase-file %5 %1