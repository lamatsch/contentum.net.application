IF "%2%"=="""" (
    "C:\Program Files (x86)\GnuPG\bin\gpg.exe" --verbose --always-trust --batch --encrypt --recipient-file %1 --output %4 %3
) ELSE (
	"C:\Program Files (x86)\GnuPG\bin\gpg.exe" --verbose --always-trust --batch --encrypt --recipient-file %1 --recipient-file %2 --output %4 %3
)