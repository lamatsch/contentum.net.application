﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contentum.HKP_KRService
{
    public class ServiceFactory
    {
        string _BusinessServiceType = "SOAP";
        string _BusinessServiceUrl = "";
        string _BusinessServiceAuthentication = "Windows";
        string _BusinessServiceUserDomain = "";
        string _BusinessServiceUserName = "";
        string _BusinessServicePassword = "";

        public string BusinessServiceType
        {
            get { return _BusinessServiceType; }
            set { _BusinessServiceType = value; }
        }

        public string BusinessServiceUrl
        {
            get { return _BusinessServiceUrl; }
            set { _BusinessServiceUrl = value; }
        }

        public string BusinessServiceAuthentication
        {
            get { return _BusinessServiceAuthentication; }
            set { _BusinessServiceAuthentication = value; }
        }

        public string BusinessServiceUserDomain
        {
            get { return _BusinessServiceUserDomain; }
            set { _BusinessServiceUserDomain = value; }
        }

        public string BusinessServiceUserName
        {
            get { return _BusinessServiceUserName; }
            set { _BusinessServiceUserName = value; }
        }

        public string BusinessServicePassword
        {
            get { return _BusinessServicePassword; }
            set { _BusinessServicePassword = value; }
        }

        private static void SetDefaults(System.Web.Services.Protocols.SoapHttpClientProtocol service)
        {
            service.PreAuthenticate = true;
            // BUG_11753
            service.AllowAutoRedirect = true;
        }

        private static System.Net.CredentialCache GetCacheSetting(string Url, string _BusinessServiceAuthentication, string _BusinessServiceUserDomain, string _BusinessServiceUserName, string _BusinessServicePassword)
        {
            System.Net.CredentialCache cache = new System.Net.CredentialCache();

            if (_BusinessServiceAuthentication == "Windows")
            {
                cache.Add(new Uri(Url), // Web service URL
                          "Negotiate",         // Kerberos or NTLM
                           System.Net.CredentialCache.DefaultNetworkCredentials);
                return cache;

            }
            if (_BusinessServiceAuthentication == "Basic")
            {
                cache.Add(new Uri(Url), // Web service URL
                          "Basic",         // Kerberos or NTLM
                           new System.Net.NetworkCredential(_BusinessServiceUserName, _BusinessServicePassword, _BusinessServiceUserDomain));
                return cache;

            }
            if (_BusinessServiceAuthentication == "NTLM")
            {
                cache.Add(new Uri(Url), // Web service URL
                          "Negotiate",         // Kerberos or NTLM
                           new System.Net.NetworkCredential(_BusinessServiceUserName, _BusinessServicePassword, _BusinessServiceUserDomain));
                return cache;

            }

            return null;
        }

        public KRService GetKRService()
        {
            if (_BusinessServiceType == "SOAP")
            {
                KRService _Service = new KRService(_BusinessServiceUrl + "KRService.asmx");
                _Service.Credentials = GetCacheSetting(_Service.Url, _BusinessServiceAuthentication, _BusinessServiceUserDomain, _BusinessServiceUserName, _BusinessServicePassword);
                SetDefaults(_Service);
                return _Service;
            }

            return null;
        }

        public static ServiceFactory GetServiceFactory()
        {
            ServiceFactory sc = new ServiceFactory();
            sc.BusinessServiceType = GetAppSetting("KRServiceBusinessServiceType", "SOAP");
            sc.BusinessServiceUrl = GetAppSetting("KRServiceBusinessServiceUrl");
            sc.BusinessServiceAuthentication = GetAppSetting("KRServiceBusinessServiceAuthentication", "Windows");
            sc.BusinessServiceUserDomain = GetAppSetting("KRServiceBusinessServiceUserDomain");
            sc.BusinessServiceUserName = GetAppSetting("KRServiceBusinessServiceUserName");
            sc.BusinessServicePassword = GetAppSetting("KRServiceBusinessServicePassword");
            return sc;
        }

        private static string GetAppSetting(string Name)
        {
            return GetAppSetting(Name, "");
        }

        private static string GetAppSetting(string Name, string defaultValue)
        {
            string _ret;
            _ret = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(Name);
            if (_ret == null)
                _ret = defaultValue;
            return _ret;
        }
    }
}
