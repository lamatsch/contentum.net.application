﻿using System;
using System.Collections.Generic;
using System.Web;

namespace KRService.Core
{
    public class BaseEnvelopeTags
    {
        public class hkpHibaUzenet
        {
            private int _szam;
            private string _tartalom;

            public int Szam
            {
                get { return _szam; }
                set { _szam = value; }
            }
            public string Tartalom
            {
                get { return _tartalom; }
                set { _tartalom = value; }
            }

            public hkpHibaUzenet()
            {
                this._szam = -1;
                this._tartalom = string.Empty;
            }
        }

        public class hkpValaszFejlec
        {
            private string _felhasznalo;
            private string _uzenetIdopont;

            public string Felhasznalo
            {
                get { return _felhasznalo; }
                set { _felhasznalo = value; }
            }
            public string UzenetIdopont
            {
                get { return _uzenetIdopont; }
                set { _uzenetIdopont = value; }
            }
            public DateTime UzenetIdopontDateTime
            {
                get { return Utility.KRDateTimeHandler.getDateTime_FromKR(_uzenetIdopont); }
                set { _uzenetIdopont = Utility.KRDateTimeHandler.getKRDateTime(value); }
            }

            public hkpValaszFejlec()
            {
                this._felhasznalo = String.Empty;
                this._uzenetIdopont = Utility.KRDateTimeHandler.getKRDateTime(DateTime.Today);
            }
        }
    }
}