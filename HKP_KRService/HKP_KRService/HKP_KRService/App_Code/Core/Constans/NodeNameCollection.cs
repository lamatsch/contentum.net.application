﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KRService.Core.Constans
{
    public class NodeNameCollection
    {
        public class BaseNodes
        {
            public const string hibaUzenet = "iop:HibaUzenet";
            public const string valaszFejlec = "iop:ValaszFejlec";
            public const string formDef = "iop:Form";
            public const string valasz = "iop:Valasz";
        }

        public class PostaFiokLekerdezesValasz
        {
            public const string PostafiokTipus = "ns12:PostafiokLekerdezesValasz";
            public const string AtmenetiKezbvaroUzenetek = "ns12:AtmenetiKezbvaroUzenetek";
			public const string AtmenetiKezbesitettUzenetek = "ns12:AtmenetiKezbesitettUzenetek";
            public const string AtmenetiKezbesitetlenUzenetek = "ns12:AtmenetiKezbesitetlenUzenetek";
            public const string AtmenetiElkuldottUzenetek = "ns12:AtmenetiElkuldottUzenetek";
            public const string SzervezettolErkezettKezbvaroUzenetek = "ns12:SzervezettolErkezettKezbvaroUzenetek";
            public const string SzervezetnekKezbesitettUzenetek = "ns12:SzervezetnekKezbesitettUzenetek";
            public const string SzervezetnekKezbesitetlenUzenetek = "ns12:SzervezetnekKezbesitetlenUzenetek";
            public const string SzervezetnekElkuldottUzenetek = "ns12:SzervezetnekElkuldottUzenetek";
            public const string TertivevenyKezbvaroUzenetek = "ns12:TertivevenyKezbvaroUzenetek";
            public const string TertivevenyKezbesitettUzenetek = "ns12:TertivevenyKezbesitettUzenetek";
            public const string TertivevenyKezbVelelemUzenetek = "ns12:TertivevenyKezbVelelemUzenetek";
            public const string TertivevenyElkuldottUzenetek = "ns12:TertivevenyElkuldottUzenetek";
        }

        public class DokumentumLetoltesValasz
        {
            public const string ErkeztetesiSzam = "ns12:ErkeztetesiSzam";
            public const string DokTipusHivatal = "ns12:DokTipusHivatal";
            public const string DokTipusAzonosito = "ns12:DokTipusAzonosito";
            public const string DokTipusLeiras = "ns12:DokTipusLeiras";
            public const string Megjegyzes = "ns12:Megjegyzes";
            public const string FileNev = "ns12:FileNev";
            public const string ErvenyessegiDatum = "ns12:ErvenyessegiDatum";
            public const string ErkeztetesiDatum = "ns12:ErkeztetesiDatum";
            public const string Kezbesitettseg = "ns12:Kezbesitettseg";
            public const string Idopecset = "ns12:Idopecset";
            public const string ValaszTitkositas = "ns12:ValaszTitkositas";
            public const string ValaszUtvonal = "ns12:ValaszUtvonal";
            public const string Rendszeruzenet = "ns12:Rendszeruzenet";
            public const string Tarterulet = "ns12:Tarterulet";
            public const string Felado = "ns12:Felado";
            public const string HivatkozasiSzam = "ns12:HivatkozasiSzam";

            public class FeladoNS
            {        
                public const string AllampolgarFelado = "ns12:AllampolgarFelado";
                public const string HivatalFelado = "ns12:HivatalFelado";
                public const string Rendszer = "ns12:Rendszer";

                public class HivatalFeladoNS
                { 
                    public const string RovidNev = "ns12:RovidNev";
                    public const string Nev = "ns12:Nev";
                    public const string MAKKod = "ns12:MAKKod";
                    public const string KRID = "n12:KRID";
                }

                public class AllampolgarFeladoNS
                {
                    public const string KapcsolatiKod = "ns12:KapcsolatiKod";
                    public const string Nev = "ns12:Nev";
                    public const string Email = "ns12:Email";
                }
            }
        }

        public class DokumentumVisszaigazolasValasz
        {
            public class DokumentumVisszaigazolasSikeres
            {
                public const string Szam = "ns12:Szam";
                public const string Szoveg = "ns12:Szoveg";
            }
            public class DokumentumVisszaigazolasHiba
            {
                public const string HibaKod = "ns12:HibaKod";
                public const string HibaSzoveg = "ns12:HibaSzoveg";
            }
            public const string Uzenet = "ns12:Uzenet";
            public const string Hiba = "ns12:Hiba";
        }

        public class HivatalListaElem
        {
            public const string RovidNev = "ns12:RovidNev";
            public const string Nev = "ns12:Nev";
            public const string MAKKKod = "ns12:MAKKod";
            public const string KRID = "ns12:KRID";
        }

        public class CsoportosDokumentumFeltoltesValasz
        { 
            public const string HibaTag = "ns12:Hiba";
            public const string UjDokumentum = "ns12:UjDokumentumAdatok";
            public const string TarolasiValasz = "ns12:TarolasiValasz";

            public class Hiba
            {
                public const string HibaLeiras = "ns12:HibaLeiras";
                public const string HibaKod = "ns12:HibaKod";
                public const string HibaSzoveg = "ns12:HibaSzoveg";
            }

            public class UjDokumentumAdatok
            {
                public const string ErkeztetesiSzam = "ns12:ErkeztetesiSzam";
                public const string ErkeztetesiDatum = "ns12:ErkeztetesiDatum";
                public const string Idopecset = "ns12:Idopecset";
            }
        }

        public class Azonositas
        {
            public const string AzonositottAdatokTag = "ns12:AzonositottAdatok";

            public class AzonositottAdatok
            {
                public const string KapcsolatiKod = "ns12:KapcsolatiKod";
                public const string Nev = "ns12:Nev";
                public const string Email = "ns12:Email";
            }
        }
    }
}
