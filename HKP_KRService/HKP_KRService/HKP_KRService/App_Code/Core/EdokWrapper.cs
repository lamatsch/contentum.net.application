﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Configuration;

namespace KRService.Core
{
    public static class EdokWrapper
    {
        static string eDok_FelhasznaloId;
        static EdokWrapper()
        {
            eDok_FelhasznaloId = ConfigurationManager.AppSettings["eDok_FelhasznaloId"];
        }

        public static EREC_eBeadvanyokService GetService()
        {
            EREC_eBeadvanyokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
            service.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
            return service;
        }

        public static ExecParam GetExecParam()
        {
            ExecParam param = new ExecParam();
            param.Felhasznalo_Id = eDok_FelhasznaloId;
            return param;
        }

        // CR3114 : HAIR adatok letöltése
        public static class Rendszer
        {
            public const string HAIR = "HAIR";
            public const string WEB = "WEB";

        }

        public static bool CheckHivatkozasiSzam(HKP2.hkpDokumentumAdatok dokumentumItem, out bool insert, out string rendszer)
        {
            LogMaster logger = new LogMaster();
            logger.Info("CheckHivatkozasiSzam start");
            logger.Info(String.Format("HivatkozasiSzam: {0}", dokumentumItem.HivatkozasiSzam));

            bool hiba = false;
            insert = true;
            rendszer = EdokWrapper.Rendszer.WEB;


            if (!String.IsNullOrEmpty(dokumentumItem.HivatkozasiSzam))
            {
                insert = false;
                try
                {
                    string doktipusAzonosito = dokumentumItem.DokTipusAzonosito;
                    logger.Info(String.Format("doktipusAzonosito={0}", doktipusAzonosito));

                    bool isLetoltesiIgazolas = "LetoltesiIgazolas".Equals(doktipusAzonosito, StringComparison.CurrentCultureIgnoreCase);
                    bool isMeghiusulasiIgazolas = "MeghiusulasiIgazolas".Equals(doktipusAzonosito, StringComparison.CurrentCultureIgnoreCase);
                    bool isFeladasiIgazolas = "FeladasiIgazolas".Equals(doktipusAzonosito, StringComparison.CurrentCultureIgnoreCase);
                    bool isAtveteliErtesito = "AtveteliErtesito".Equals(doktipusAzonosito, StringComparison.CurrentCultureIgnoreCase);

                    EREC_eBeadvanyokService service = GetService();
                    ExecParam param = GetExecParam();
                    EREC_eBeadvanyokSearch search = new EREC_eBeadvanyokSearch();
                    search.KR_ErkeztetesiSzam.Value = dokumentumItem.HivatkozasiSzam;
                    search.KR_ErkeztetesiSzam.Operator = "=";
                    Result eDokUzenetekSearchResult = service.GetAll(param, search);

                    if (eDokUzenetekSearchResult.IsError)
                    {
                        logger.Error(String.Format("EREC_eBeadvanyokService.GetAll hiba: {0}", eDokUzenetekSearchResult.ErrorMessage));
                        return true;
                    }

                    if (eDokUzenetekSearchResult.Ds.Tables[0].Rows.Count == 0)
                    {
                        logger.Info("EDOK-ban nincs benne!");
                        insert = true;

                        if (isLetoltesiIgazolas || isMeghiusulasiIgazolas || isFeladasiIgazolas || isAtveteliErtesito)
                        {
                            rendszer = EdokWrapper.Rendszer.HAIR;
                        }
                        else
                        {
                            rendszer = EdokWrapper.Rendszer.WEB;
                        }
                        logger.Info(String.Format("rendszer={0}", rendszer));
                        return false;
                    }
                    else
                    {
                        logger.Info("EDOK-ban benne van!");

                        if (!dokumentumItem.RendszerUzenet)
                        {
                            logger.Info("Nem rendszerüzenet!");
                            insert = true;
                        }
                        else
                        {
                            logger.Info("Rendszerüzenet!");
                            if (!isLetoltesiIgazolas && !isMeghiusulasiIgazolas)
                            {
                                logger.Info("Nem kezelt rendszerüzenet");
                                insert = true;
                            }
                            else
                            {
                                EREC_eBeadvanyok updateDokAdatok = new EREC_eBeadvanyok();
                                updateDokAdatok.Base.Updated.SetValueAll(false);
                                updateDokAdatok.Updated.SetValueAll(false);
                                string Id = eDokUzenetekSearchResult.Ds.Tables[0].Rows[0]["Id"].ToString();
                                string ver = eDokUzenetekSearchResult.Ds.Tables[0].Rows[0]["Ver"].ToString();
                                param.Record_Id = Id;

                                updateDokAdatok.Base.Ver = ver;
                                updateDokAdatok.Base.Updated.Ver = true;

                                //üzenet érkeztetési dátumának átadás, ez kerül beállításra az iratpéldány átvételi idejének
                                updateDokAdatok.KR_ErkeztetesiDatum = dokumentumItem.ErkeztetesiDatum;

                                if (isMeghiusulasiIgazolas)
                                {
                                    logger.Info("MeghiusulasiIgazolas");
                                    updateDokAdatok.Updated.Allapot = true;
                                    updateDokAdatok.Allapot = "13"; //Címzett nem vette át
                                }
                                else if (isLetoltesiIgazolas)
                                {
                                    logger.Info("LetoltesiIgazolas");
                                    updateDokAdatok.Updated.Allapot = true;
                                    updateDokAdatok.Allapot = "12"; //Címzett átvette
                                }

                                Result updateResult = service.Update(param, updateDokAdatok);

                                if (updateResult.IsError)
                                {
                                    logger.Error(String.Format("EREC_eBeadvanyokService.Update hiba: {0}", updateResult.ErrorMessage));
                                    return true;
                                }
                                else
                                {
                                    insert = true;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("CheckHivatkozasiSzam hiba", ex);
                    hiba = true;
                }
            }

            logger.Info("CheckHivatkozasiSzam vege");
            return hiba;
        }
        // CR3114 : HAIR adatok letöltése
        //HAIR jelenesetben csak az, aminél a a DokTipusHivatal = 'NAV' és DokTipusAzonosito utolsó 4 karaktere = 'HIPA'
        public static string RendszerSelect(HKP2.hkpDokumentumAdatok dokumentumItem)
        {
            //NAV és HIPA vagy HIPAEK
            if ((dokumentumItem.DokTipusHivatal.ToUpper() == "NAV") && (dokumentumItem.DokTipusAzonosito.ToUpper().EndsWith("HIPA")
                || dokumentumItem.DokTipusAzonosito.ToUpper().EndsWith("HIPAEK")))
                return Rendszer.HAIR;
            else if ((dokumentumItem.DokTipusHivatal.ToUpper() == "ORFK") && (dokumentumItem.DokTipusAzonosito.ToUpper().EndsWith("IVH")))
                return Rendszer.HAIR;
            else if (dokumentumItem.FileNev.ToUpper().StartsWith("NAV_ONKOR_"))
                return Rendszer.HAIR;
            else
                return Rendszer.WEB;

        }
    }
}