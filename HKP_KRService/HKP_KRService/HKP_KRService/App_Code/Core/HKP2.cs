﻿using System;
using System.Collections.Generic;
using System.Xml;

using KRService.Core.Constans;
using Contentum.eBusinessDocuments;

namespace KRService.Core
{
    public class HKP2
    {
        public class PostaFiokAdatokLekerdezesValasz : BaseEnvelopeTags
        {
            private hkpHibaUzenet _hibaUzenet;
            private hkpValaszFejlec _valaszFejlec;
            private int _atmenetiKezbvaroUzenetek;
            private int _atmenetiKezbesitettUzenetek;
            private int _atmenetiKezbesitetlenUzenetek;
            private int _atmenetiElkuldottUzenetek;
            private int _szervezettolErkezettUzenetek;
            private int _szervezetnekKezbesitettUzenetek;
            private int _szervezetnekKezbesitettlenUzenetek;
            private int _szervezetnekElkuldottUzenetek;
            private int _tertivevenyKezbvaroUzenetek;
            private int _tertivevenyKezbesitettUzenetek;
            private int _tertivevenyKezbVelelemUzenetek;
            private int _tertivevenyElkuldottUzenetek;

            public hkpHibaUzenet HibaUzenet
            {
                get { return _hibaUzenet; }
                set { _hibaUzenet = value; }
            }
            public hkpValaszFejlec ValaszFejlec
            {
                get { return _valaszFejlec; }
                set { _valaszFejlec = value; }
            }
            public int AtmenetiKezbvaroUzenetek
            {
                get { return _atmenetiKezbvaroUzenetek; }
                set { _atmenetiKezbvaroUzenetek = value; }
            }
            public int AtmenetiKezbesitettUzenetek
            {
                get { return _atmenetiKezbesitettUzenetek; }
                set { _atmenetiKezbesitettUzenetek = value; }
            }
            public int AtmenetiKezbesitetlenUzenetek
            {
                get { return _atmenetiKezbesitetlenUzenetek; }
                set { _atmenetiKezbesitetlenUzenetek = value; }
            }
            public int AtmenetiElkuldottUzenetek
            {
                get { return _atmenetiElkuldottUzenetek; }
                set { _atmenetiElkuldottUzenetek = value; }
            }
            public int SzervezettolErkezettUzenetek
            {
                get { return _szervezettolErkezettUzenetek; }
                set { _szervezettolErkezettUzenetek = value; }
            }
            public int SzervezetnekKezbesitettUzenetek
            {
                get { return _szervezetnekKezbesitettUzenetek; }
                set { _szervezetnekKezbesitettUzenetek = value; }
            }
            public int SzervezetnekKezbesitettlenUzenetek
            {
                get { return _szervezetnekKezbesitettlenUzenetek; }
                set { _szervezetnekKezbesitettlenUzenetek = value; }
            }
            public int SzervezetnekElkuldottUzenetek
            {
                get { return _szervezetnekElkuldottUzenetek; }
                set { _szervezetnekElkuldottUzenetek = value; }
            }
            public int TertivevenyKezbvaroUzenetek
            {
                get { return _tertivevenyKezbvaroUzenetek; }
                set { _tertivevenyKezbvaroUzenetek = value; }
            }
            public int TertivevenyKezbesitettUzenetek
            {
                get { return _tertivevenyKezbesitettUzenetek; }
                set { _tertivevenyKezbesitettUzenetek = value; }
            }
            public int TertivevenyKezbVelelemUzenetek
            {
                get { return _tertivevenyKezbVelelemUzenetek; }
                set { _tertivevenyKezbVelelemUzenetek = value; }
            }
            public int TertivevenyElkuldottUzenetek
            {
                get { return _tertivevenyElkuldottUzenetek; }
                set { _tertivevenyElkuldottUzenetek = value; }
            }

            public PostaFiokAdatokLekerdezesValasz()
            {
                this._valaszFejlec = new hkpValaszFejlec();
                this._hibaUzenet = new hkpHibaUzenet();
                this._atmenetiKezbvaroUzenetek = -1;
                this._atmenetiKezbesitettUzenetek = -1;
                this._atmenetiKezbesitetlenUzenetek = -1;
                this._atmenetiElkuldottUzenetek = -1;
                this._szervezettolErkezettUzenetek = -1;
                this._szervezetnekKezbesitettUzenetek = -1;
                this._szervezetnekKezbesitettlenUzenetek = -1;
                this._szervezetnekElkuldottUzenetek = -1;
                this._tertivevenyKezbvaroUzenetek = -1;
                this._tertivevenyKezbesitettUzenetek = -1;
                this._tertivevenyKezbVelelemUzenetek = -1;
                this._tertivevenyElkuldottUzenetek = -1;
            }

            public void getData(XmlDocument xDoc)
            {
                XmlNodeList ValaszNode = xDoc.GetElementsByTagName(NodeNameCollection.BaseNodes.valasz);
                if (ValaszNode.Count == 0)
                    return; //TODO nulla visszatérési érték kezelése
                XmlNode ValaszFejlecNode = ValaszNode.Item(0).FirstChild;
                _valaszFejlec.Felhasznalo = ValaszFejlecNode.FirstChild.InnerText;
                _valaszFejlec.UzenetIdopont = ValaszFejlecNode.LastChild.InnerText;
                foreach (XmlNode valaszItem in ValaszNode.Item(0).ChildNodes)
                {
                    if (valaszItem.Name.Equals(NodeNameCollection.BaseNodes.formDef))
                    {
                        foreach (XmlNode postafiokValasz in valaszItem.FirstChild.ChildNodes)
                        {
                            switch (postafiokValasz.Name)
                            {
                                case NodeNameCollection.PostaFiokLekerdezesValasz.AtmenetiElkuldottUzenetek:
                                    this._atmenetiElkuldottUzenetek = int.Parse(postafiokValasz.InnerText);
                                    break;
                                case NodeNameCollection.PostaFiokLekerdezesValasz.AtmenetiKezbesitetlenUzenetek:
                                    this._atmenetiKezbesitetlenUzenetek = int.Parse(postafiokValasz.InnerText);
                                    break;
                                case NodeNameCollection.PostaFiokLekerdezesValasz.AtmenetiKezbesitettUzenetek:
                                    this._atmenetiKezbesitettUzenetek = int.Parse(postafiokValasz.InnerText);
                                    break;
                                case NodeNameCollection.PostaFiokLekerdezesValasz.AtmenetiKezbvaroUzenetek:
                                    this._atmenetiKezbvaroUzenetek = int.Parse(postafiokValasz.InnerText);
                                    break;
                                case NodeNameCollection.PostaFiokLekerdezesValasz.SzervezetnekElkuldottUzenetek:
                                    this._szervezetnekElkuldottUzenetek = int.Parse(postafiokValasz.InnerText);
                                    break;
                                case NodeNameCollection.PostaFiokLekerdezesValasz.SzervezetnekKezbesitetlenUzenetek:
                                    this._szervezetnekKezbesitettlenUzenetek = int.Parse(postafiokValasz.InnerText);
                                    break;
                                case NodeNameCollection.PostaFiokLekerdezesValasz.SzervezetnekKezbesitettUzenetek:
                                    this._szervezetnekKezbesitettUzenetek = int.Parse(postafiokValasz.InnerText);
                                    break;
                                case NodeNameCollection.PostaFiokLekerdezesValasz.SzervezettolErkezettKezbvaroUzenetek:
                                    this._szervezettolErkezettUzenetek = int.Parse(postafiokValasz.InnerText);
                                    break;
                                case NodeNameCollection.PostaFiokLekerdezesValasz.TertivevenyElkuldottUzenetek:
                                    this._tertivevenyElkuldottUzenetek = int.Parse(postafiokValasz.InnerText);
                                    break;
                                case NodeNameCollection.PostaFiokLekerdezesValasz.TertivevenyKezbesitettUzenetek:
                                    this._tertivevenyKezbesitettUzenetek = int.Parse(postafiokValasz.InnerText);
                                    break;
                                case NodeNameCollection.PostaFiokLekerdezesValasz.TertivevenyKezbvaroUzenetek:
                                    this._tertivevenyKezbvaroUzenetek = int.Parse(postafiokValasz.InnerText);
                                    break;
                                case NodeNameCollection.PostaFiokLekerdezesValasz.TertivevenyKezbVelelemUzenetek:
                                    this._tertivevenyKezbVelelemUzenetek = int.Parse(postafiokValasz.InnerText);
                                    break;
                            }
                        }
                    }
                    if (valaszItem.Name.Equals(NodeNameCollection.BaseNodes.hibaUzenet))
                    {
                        _hibaUzenet.Szam = int.Parse(valaszItem.FirstChild.InnerText);
                        _hibaUzenet.Tartalom = valaszItem.LastChild.InnerText;
                    }
                }
            }

            public static PostaFiokAdatokLekerdezesValasz getError(Exception ex)
            {
                PostaFiokAdatokLekerdezesValasz _return = new PostaFiokAdatokLekerdezesValasz();
                _return.HibaUzenet.Szam = 5000;
                if (ex.InnerException != null)
                {
                    _return.HibaUzenet.Tartalom = ex.InnerException.Message;
                }
                else
                {
                    _return.HibaUzenet.Tartalom = ex.Message;
                }
                return _return;
            }
        }

        public class CsoportosDokumentumLetoltesValasz : BaseEnvelopeTags
        {
            private hkpHibaUzenet _hibaUzenet;
            private hkpValaszFejlec _valaszFejlec;
            private List<hkpDokumentumAdatok> _dokumentumList;
            private List<Soap.SoapAttachment> _attachmentList;

            public hkpHibaUzenet HibaUzenet
            {
                get { return _hibaUzenet; }
                set { _hibaUzenet = value; }
            }
            public hkpValaszFejlec ValaszFejlec
            {
                get { return _valaszFejlec; }
                set { _valaszFejlec = value; }
            }
            public List<hkpDokumentumAdatok> DokumentumList
            {
                get { return _dokumentumList; }
                set { _dokumentumList = value; }
            }
            public List<Soap.SoapAttachment> AttachmentList
            {
                get { return _attachmentList; }
                set { _attachmentList = value; }
            }

            public CsoportosDokumentumLetoltesValasz()
            {
                _hibaUzenet = new hkpHibaUzenet();
                _valaszFejlec = new hkpValaszFejlec();
                _dokumentumList = new List<hkpDokumentumAdatok>();
                _attachmentList = new List<Soap.SoapAttachment>();
            }

            public void getData(XmlDocument xDoc)
            {
                XmlNodeList ValaszNode = xDoc.GetElementsByTagName(NodeNameCollection.BaseNodes.valasz);
                if (ValaszNode.Count == 0)
                    return; //TODO nulla visszatérési érték kezelése
                XmlNode ValaszFejlecNode = ValaszNode.Item(0).FirstChild;
                _valaszFejlec.Felhasznalo = ValaszFejlecNode.FirstChild.InnerText;
                _valaszFejlec.UzenetIdopont = ValaszFejlecNode.LastChild.InnerText;
                foreach (XmlNode valaszItem in ValaszNode.Item(0).ChildNodes)
                {
                    if (valaszItem.Name.Equals(NodeNameCollection.BaseNodes.formDef))
                    {
                        foreach (XmlNode csopDokItem in valaszItem.FirstChild.ChildNodes)
                        {
                            hkpDokumentumAdatok dokAdat = new hkpDokumentumAdatok();
                            dokAdat.getData(csopDokItem);
                            this._dokumentumList.Add(dokAdat);
                        }
                    }
                    if (valaszItem.Name.Equals(NodeNameCollection.BaseNodes.hibaUzenet))
                    {
                        _hibaUzenet.Szam = int.Parse(valaszItem.FirstChild.InnerText);
                        _hibaUzenet.Tartalom = valaszItem.LastChild.InnerText;
                    }
                }
            }

            public static CsoportosDokumentumLetoltesValasz getError(Exception ex)
            {
                CsoportosDokumentumLetoltesValasz _return = new CsoportosDokumentumLetoltesValasz();
                _return.HibaUzenet.Szam = 5000;
                if (ex.InnerException != null)
                {
                    _return.HibaUzenet.Tartalom = ex.InnerException.Message;
                }
                else
                {
                    _return.HibaUzenet.Tartalom = ex.Message;
                }
                return _return;
            }

            public Csatolmany getCsatolmanyByFilename(string filename)
            {
                Csatolmany _return = new Csatolmany();
                Soap.SoapAttachment selectedAttachment = new Soap.SoapAttachment();
                foreach (Soap.SoapAttachment attachment in this._attachmentList)
                {
                    if (attachment.Filename == filename)
                    {
                        selectedAttachment = attachment;
                        break;
                    }
                }
                if (selectedAttachment.ContentStream != null)
                {
                    _return.Tartalom = selectedAttachment.ContentStream;
                    _return.Nev = selectedAttachment.Filename;
                }
                return _return;
            }

            public Csatolmany getCsatolmanyByContentID(string contentID)
            {
                string _contentID = String.Format("<{0}>", contentID);
                Csatolmany _return = new Csatolmany();
                Soap.SoapAttachment selectedAttachment = new Soap.SoapAttachment();
                foreach (Soap.SoapAttachment attachment in this._attachmentList)
                {
                    if (attachment.Header.ContentID == _contentID)
                    {
                        selectedAttachment = attachment;
                        break;
                    }
                }
                if (selectedAttachment.ContentStream != null)
                {
                    _return.Tartalom = selectedAttachment.ContentStream;
                    _return.Nev = selectedAttachment.Filename;
                }
                return _return;
            }

        }

        public class hkpDokumentumAdatok
        {
            private hkpFelado _felado;
            private string _href;
            private string _erkezetesiSzam;
            private string _hivatkozasiSzam;
            private string _dokTipusHivatal;
            private string _dokTipusAzonosito;
            private string _dokTipusLeiras;
            private string _megjegyzes;
            private string _fileNev;
            private string _ervenyessegiDatum;
            private string _erkeztetesiDatum;
            private int _kezbesitettseg;
            private string _idoPecset;
            private bool _valaszTitkositas;
            private string _valaszUtvonal;
            private bool _rendszerUzenet;
            private int _tarterulet;

            public hkpFelado Felado
            {
                get { return _felado; }
                set { _felado = value; }
            }
            public string Href
            {
                get { return _href; }
                set { _href = value; }
            }
            public string ErkezetesiSzam
            {
                get { return _erkezetesiSzam; }
                set { _erkezetesiSzam = value; }
            }
            public string HivatkozasiSzam
            {
                get { return _hivatkozasiSzam; }
                set { _hivatkozasiSzam = value; }
            }
            public string DokTipusHivatal
            {
                get { return _dokTipusHivatal; }
                set { _dokTipusHivatal = value; }
            }
            public string DokTipusAzonosito
            {
                get { return _dokTipusAzonosito; }
                set { _dokTipusAzonosito = value; }
            }
            public string DokTipusLeiras
            {
                get { return _dokTipusLeiras; }
                set { _dokTipusLeiras = value; }
            }
            public string Megjegyzes
            {
                get { return _megjegyzes; }
                set { _megjegyzes = value; }
            }
            public string FileNev
            {
                get { return _fileNev; }
                set { _fileNev = value; }
            }
            public string ErvenyessegiDatum
            {
                get { return _ervenyessegiDatum; }
                set { _ervenyessegiDatum = value; }
            }
            public string ErkeztetesiDatum
            {
                get { return _erkeztetesiDatum; }
                set { _erkeztetesiDatum = value; }
            }
            public int Kezbesitettseg
            {
                get { return _kezbesitettseg; }
                set { _kezbesitettseg = value; }
            }
            public string IdoPecset
            {
                get { return _idoPecset; }
                set { _idoPecset = value; }
            }
            public bool ValaszTitkositas
            {
                get { return _valaszTitkositas; }
                set { _valaszTitkositas = value; }
            }
            public string ValaszUtvonal
            {
                get { return _valaszUtvonal; }
                set { _valaszUtvonal = value; }
            }
            public bool RendszerUzenet
            {
                get { return _rendszerUzenet; }
                set { _rendszerUzenet = value; }
            }
            public int Tarterulet
            {
                get { return _tarterulet; }
                set { _tarterulet = value; }
            }

            public hkpDokumentumAdatok()
            {
                this._felado = new hkpFelado();
                this._href = string.Empty;
                this._erkezetesiSzam = string.Empty;
                this._hivatkozasiSzam = string.Empty;
                this._dokTipusHivatal = string.Empty;
                this._dokTipusAzonosito = string.Empty;
                this._dokTipusLeiras = string.Empty;
                this._fileNev = string.Empty;
                this._ervenyessegiDatum = string.Empty;
                this._erkeztetesiDatum = string.Empty;
                this._kezbesitettseg = -1;
                this._idoPecset = string.Empty;
                this._valaszTitkositas = false;
                this._valaszUtvonal = string.Empty;
                this._rendszerUzenet = false;
                this._tarterulet = -1;
            }

            public void getData(XmlNode xNode)
            {
                if (xNode.Attributes["href"] != null)
                {
                    this._href = xNode.Attributes["href"].Value;
                }
                foreach (XmlNode innerNode in xNode)
                {
                    switch (innerNode.Name)
                    { 
                        case NodeNameCollection.DokumentumLetoltesValasz.Felado:
                            this._felado.getData(innerNode);
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.ErkeztetesiSzam:
                            this._erkezetesiSzam = innerNode.InnerText;
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.HivatkozasiSzam:
                            this._hivatkozasiSzam = innerNode.InnerText;
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.DokTipusHivatal:
                            this._dokTipusHivatal = innerNode.InnerText;
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.DokTipusAzonosito:
                            this._dokTipusAzonosito = innerNode.InnerText;
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.DokTipusLeiras:
                            this._dokTipusLeiras = innerNode.InnerText;
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.Megjegyzes:
                            this._megjegyzes = innerNode.InnerText;
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.FileNev:
                            this._fileNev = innerNode.InnerText;
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.ErvenyessegiDatum:
                            this._ervenyessegiDatum = innerNode.InnerText;
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.ErkeztetesiDatum:
                            this._erkeztetesiDatum = innerNode.InnerText;
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.Kezbesitettseg:
                            int kezbIn = 0;
                            int.TryParse(innerNode.InnerText, out kezbIn);
                            this._kezbesitettseg = kezbIn;
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.Idopecset:
                            this._idoPecset = innerNode.InnerText;
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.ValaszTitkositas:
                            bool valaszTitkBool = false;
                            bool.TryParse(innerNode.InnerText, out valaszTitkBool);
                            this._valaszTitkositas = valaszTitkBool;
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.ValaszUtvonal:
                            this._valaszUtvonal = innerNode.InnerText;
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.Rendszeruzenet:
                            bool rendszBool = false;
                            bool.TryParse(innerNode.InnerText, out rendszBool);
                            this._rendszerUzenet = rendszBool;
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.Tarterulet:
                            int tarTint = 0;
                            int.TryParse(innerNode.InnerText, out tarTint);
                            this._tarterulet = tarTint;
                            break;
                    }
                }
            }

            public EREC_eBeadvanyok get_eBusinessObject()
            {
                EREC_eBeadvanyok dokAdatok = new EREC_eBeadvanyok();
                dokAdatok.Base.Updated.SetValueAll(false);
                dokAdatok.Updated.ErvKezd = false;
                dokAdatok.Updated.ErvVege = false;

                dokAdatok.Irany = "0"; //Bejövő
                if (Felado.Rendszer)
                {
                    dokAdatok.FeladoTipusa = "2"; //Rendszer
                }
                else
                if (!String.IsNullOrEmpty(Felado.AllampolgarFelado.Nev))
                {
                    dokAdatok.FeladoTipusa = "0"; //Állampolgár
                    dokAdatok.PartnerEmail = this.Felado.AllampolgarFelado.Email;
                    dokAdatok.PartnerKapcsolatiKod = this.Felado.AllampolgarFelado.KapcsolatiKod;
                    dokAdatok.PartnerNev = this.Felado.AllampolgarFelado.Nev;
                }
                else
                {
                    dokAdatok.FeladoTipusa = "1"; //Hivatal
                    dokAdatok.PartnerNev = this.Felado.HivatalFelado.Nev;
                    dokAdatok.PartnerRovidNev = this.Felado.HivatalFelado.RovidNev;
                    dokAdatok.PartnerMAKKod = this.Felado.HivatalFelado.MAKKod;
                    dokAdatok.PartnerKRID = this.Felado.HivatalFelado.KRID;
                }
                dokAdatok.KR_DokTipusAzonosito = this.DokTipusAzonosito;
                dokAdatok.KR_DokTipusHivatal = this.DokTipusHivatal;
                dokAdatok.KR_DokTipusLeiras = this.DokTipusLeiras;
                dokAdatok.Id = Guid.NewGuid().ToString();
                dokAdatok.KR_ErkeztetesiDatum = Utility.KRDateTimeHandler.getDateTime_FromKR(this.ErkeztetesiDatum).ToString();
                dokAdatok.KR_ErkeztetesiSzam = this.ErkezetesiSzam;
                dokAdatok.KR_ErvenyessegiDatum = Utility.KRDateTimeHandler.getDateTime_FromKR(this.ErvenyessegiDatum).ToString();
                //dokAdatok.ErvKezd = DateTime.Now.ToString();
                //dokAdatok.ErvVege = new DateTime(4700, 12, 31).ToString();
                dokAdatok.KR_FileNev = this.FileNev.Replace(this._dokTipusLeiras + "-", "");
                dokAdatok.KR_HivatkozasiSzam = this.HivatkozasiSzam;
                dokAdatok.KR_Idopecset = this.IdoPecset;
                dokAdatok.KR_Kezbesitettseg = this.Kezbesitettseg.ToString();
                dokAdatok.KR_Megjegyzes = this.Megjegyzes;
                dokAdatok.PartnerNev = this.Felado.HivatalFelado.Nev;
                dokAdatok.KR_Rendszeruzenet = Utility.GetEDOKBoolean(this.RendszerUzenet);
                dokAdatok.KR_Tarterulet = this.Tarterulet.ToString();
                dokAdatok.KR_Valasztitkositas = Utility.GetEDOKBoolean(this.ValaszTitkositas);
                dokAdatok.KR_Valaszutvonal = this.ValaszUtvonal;

                // CR3114 : HAIR adatok letöltése
                dokAdatok.Cel = EdokWrapper.RendszerSelect(this);

                return dokAdatok;
            }
        }

        public class hkpFelado
        {
            private hkpAllampolgarFelado _allampolgarFelado;
            private hkpHivatalFelado _hivatalFelado;
            private bool _rendszer;

            public hkpAllampolgarFelado AllampolgarFelado
            {
                get { return _allampolgarFelado; }
                set { _allampolgarFelado = value; }
            }
            public hkpHivatalFelado HivatalFelado
            {
                get { return _hivatalFelado; }
                set { _hivatalFelado = value; }
            }
            public bool Rendszer
            {
                get { return _rendszer; }
                set { _rendszer = value; }
            }

            public hkpFelado()
            {
                this._allampolgarFelado = new hkpAllampolgarFelado();
                this._hivatalFelado = new hkpHivatalFelado();
                this._rendszer = false;
            }

            public void getData(XmlNode node)
            {
                foreach (XmlNode item in node.ChildNodes)
                {
                    switch (item.Name)
                    {
                        case NodeNameCollection.DokumentumLetoltesValasz.FeladoNS.AllampolgarFelado:
                            foreach (XmlNode innerItem in item.ChildNodes)
                            {
                                switch (innerItem.Name)
                                {
                                    case NodeNameCollection.DokumentumLetoltesValasz.FeladoNS.AllampolgarFeladoNS.KapcsolatiKod:
                                        this._allampolgarFelado.KapcsolatiKod = innerItem.InnerText;
                                        break;
                                    case NodeNameCollection.DokumentumLetoltesValasz.FeladoNS.AllampolgarFeladoNS.Nev:
                                        this._allampolgarFelado.Nev = innerItem.InnerText;
                                        break;
                                    case NodeNameCollection.DokumentumLetoltesValasz.FeladoNS.AllampolgarFeladoNS.Email:
                                        this._allampolgarFelado.Email = innerItem.InnerText;
                                        break;
                                }
                            }
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.FeladoNS.HivatalFelado:
                            foreach (XmlNode innerHivatalItems in item.ChildNodes)
                            {
                                switch (innerHivatalItems.Name)
                                {
                                    case NodeNameCollection.DokumentumLetoltesValasz.FeladoNS.HivatalFeladoNS.RovidNev:
                                        this._hivatalFelado.RovidNev = innerHivatalItems.InnerText;
                                        break;
                                    case NodeNameCollection.DokumentumLetoltesValasz.FeladoNS.HivatalFeladoNS.Nev:
                                        this._hivatalFelado.Nev = innerHivatalItems.InnerText;
                                        break;
                                    case NodeNameCollection.DokumentumLetoltesValasz.FeladoNS.HivatalFeladoNS.MAKKod:
                                        this._hivatalFelado.MAKKod = innerHivatalItems.InnerText;
                                        break;
                                    case NodeNameCollection.DokumentumLetoltesValasz.FeladoNS.HivatalFeladoNS.KRID:
                                        this._hivatalFelado.KRID = innerHivatalItems.InnerText;
                                        break;
                                }
                            }
                            break;
                        case NodeNameCollection.DokumentumLetoltesValasz.FeladoNS.Rendszer:
                            this._rendszer = bool.Parse(item.InnerText);
                            break;
                    }
                }
            }
        }

        public class hkpHivatalFelado
        {
             private string _rovidNev;
            private string _nev;
            private string _MAKKod;
            private string _KRID;

            public string RovidNev
            {
                get { return _rovidNev; }
                set { _rovidNev = value; }
            }
            public string Nev
            {
                get { return _nev; }
                set { _nev = value; }
            }
            public string MAKKod
            {
                get { return _MAKKod; }
                set { _MAKKod = value; }
            }
            public string KRID
            {
                get { return _KRID; }
                set { _KRID = value; }
            }

            public hkpHivatalFelado()
            {
                this._rovidNev = String.Empty;
                this._nev = String.Empty;
                this._MAKKod = String.Empty;
                this._KRID = String.Empty;
            }
        }

        public class hkpAllampolgarFelado
        {
            private string _kapcsolatiKod;
            private string _nev;
            private string _email;

            public string KapcsolatiKod
            {
                get { return _kapcsolatiKod; }
                set { _kapcsolatiKod = value; }
            }
            public string Nev
            {
                get { return _nev; }
                set { _nev = value; }
            }
            public string Email
            {
                get { return _email; }
                set { _email = value; }
            }

            public hkpAllampolgarFelado()
            {
                this._kapcsolatiKod = String.Empty;
                this._nev = String.Empty;
                this._email = String.Empty;
            }
        }

        public class HivatalokListajaValasz : BaseEnvelopeTags
        {
            private hkpHibaUzenet _hibaUzenet;
            private hkpValaszFejlec _valaszFejlec;
            private List<HivatalListaElem> _hivatalokLista;

            public hkpHibaUzenet HibaUzenet
            {
                get { return _hibaUzenet; }
                set { _hibaUzenet = value; }
            }
            public hkpValaszFejlec ValaszFejlec
            {
                get { return _valaszFejlec; }
                set { _valaszFejlec = value; }
            }
            public List<HivatalListaElem> HivatalokLista
            {
                get { return _hivatalokLista; }
                set { _hivatalokLista = value; }
            }

            public HivatalokListajaValasz()
            {
                this._hibaUzenet = new hkpHibaUzenet();
                this._valaszFejlec = new hkpValaszFejlec();
                this._hivatalokLista = new List<HivatalListaElem>();
            }

            public void getData(XmlDocument xDoc)
            {
                XmlNodeList ValaszNode = xDoc.GetElementsByTagName(NodeNameCollection.BaseNodes.valasz);
                if (ValaszNode.Count == 0)
                    return; //TODO nulla visszatérési érték kezelése
                XmlNode ValaszFejlecNode = ValaszNode.Item(0).FirstChild;
                _valaszFejlec.Felhasznalo = ValaszFejlecNode.FirstChild.InnerText;
                _valaszFejlec.UzenetIdopont = ValaszFejlecNode.LastChild.InnerText;
                foreach (XmlNode valaszItem in ValaszNode.Item(0).ChildNodes)
                {
                    if (valaszItem.Name.Equals(NodeNameCollection.BaseNodes.formDef))
                    {
                        foreach (XmlNode hivatalValaszItem in valaszItem.FirstChild.ChildNodes)
                        {
                            HivatalListaElem hivatalElem = new HivatalListaElem();
                            hivatalElem.getData(hivatalValaszItem);
                            this._hivatalokLista.Add(hivatalElem);
                        }
                    }
                    if (valaszItem.Name.Equals(NodeNameCollection.BaseNodes.hibaUzenet))
                    {
                        _hibaUzenet.Szam = int.Parse(valaszItem.FirstChild.InnerText);
                        _hibaUzenet.Tartalom = valaszItem.LastChild.InnerText;
                    }
                }
            }

            public static HivatalokListajaValasz getError(Exception ex)
            {
                HivatalokListajaValasz _return = new HivatalokListajaValasz();
                _return.HibaUzenet.Szam = 5000;
                if (ex.InnerException != null)
                {
                    _return.HibaUzenet.Tartalom = ex.InnerException.Message;
                }
                else
                {
                    _return.HibaUzenet.Tartalom = ex.Message;
                }
                return _return;
            }
        }

        public class HivatalListaElem
        {
            private string _rovidNev;
            private string _nev;
            private string _MAKKod;
            private string _KRID;

            public string RovidNev
            {
                get { return _rovidNev; }
                set { _rovidNev = value; }
            }
            public string Nev
            {
                get { return _nev; }
                set { _nev = value; }
            }
            public string MAKKod
            {
                get { return _MAKKod; }
                set { _MAKKod = value; }
            }
            public string KRID
            {
                get { return _KRID; }
                set { _KRID = value; }
            }

            public HivatalListaElem()
            {
                this._rovidNev = String.Empty;
                this._nev = String.Empty;
                this._MAKKod = String.Empty;
                this._KRID = String.Empty;
            }

            public void getData(XmlNode xNode)
            {
                foreach (XmlNode xNodeItem in xNode.ChildNodes)
                {
                    switch (xNodeItem.Name)
                    { 
                        case NodeNameCollection.HivatalListaElem.RovidNev:
                            this._rovidNev = xNodeItem.InnerText;
                            break;
                        case NodeNameCollection.HivatalListaElem.Nev:
                            this._nev = xNodeItem.InnerText;
                            break;
                        case NodeNameCollection.HivatalListaElem.MAKKKod:
                            this._MAKKod = xNodeItem.InnerText;
                            break;
                        case NodeNameCollection.HivatalListaElem.KRID:
                            this._KRID = xNodeItem.InnerText;
                            break;
                    }
                }
            }
        }

        public class CsoportosDokumentumLetoltesVisszaigazolasValasz : BaseEnvelopeTags
        {
            private hkpHibaUzenet _hibaUzenet;
            private hkpValaszFejlec _valaszFejlec;
            private List<CsopDokLetoltVisszaigValaszHiba> _hibaLista;
            private List<CsopDokLetoltVisszaigValaszSiker> _sikeresLista;

            public hkpHibaUzenet HibaUzenet
            {
                get { return _hibaUzenet; }
                set { _hibaUzenet = value; }
            }
            public hkpValaszFejlec ValaszFejlec
            {
                get { return _valaszFejlec; }
                set { _valaszFejlec = value; }
            }
            public List<CsopDokLetoltVisszaigValaszHiba> HibaLista
            {
                get { return _hibaLista; }
                set { _hibaLista = value; }
            }
            public List<CsopDokLetoltVisszaigValaszSiker> SikeresLista
            {
                get { return _sikeresLista; }
                set { _sikeresLista = value; }
            }

            public CsoportosDokumentumLetoltesVisszaigazolasValasz()
            {
                this._hibaUzenet = new hkpHibaUzenet();
                this._valaszFejlec = new hkpValaszFejlec();
                this._hibaLista = new List<CsopDokLetoltVisszaigValaszHiba>();
                this._sikeresLista = new List<CsopDokLetoltVisszaigValaszSiker>();
            }

            public void getData(XmlDocument xDoc)
            {
                XmlNodeList ValaszNode = xDoc.GetElementsByTagName(NodeNameCollection.BaseNodes.valasz);
                if (ValaszNode.Count == 0)
                    return; //TODO nulla visszatérési érték kezelése
                XmlNode ValaszFejlecNode = ValaszNode.Item(0).FirstChild;
                _valaszFejlec.Felhasznalo = ValaszFejlecNode.FirstChild.InnerText;
                _valaszFejlec.UzenetIdopont = ValaszFejlecNode.LastChild.InnerText;
                foreach (XmlNode valaszItem in ValaszNode.Item(0).ChildNodes)
                {
                    if (valaszItem.Name.Equals(NodeNameCollection.BaseNodes.formDef))
                    {
                        foreach (XmlNode visszaIgazolasItem in valaszItem.FirstChild.ChildNodes)
                        {
                            switch (visszaIgazolasItem.FirstChild.Name)
                            { 
                                case NodeNameCollection.DokumentumVisszaigazolasValasz.Hiba:
                                    CsopDokLetoltVisszaigValaszHiba hiba = new CsopDokLetoltVisszaigValaszHiba();
                                    hiba.getData(visszaIgazolasItem);
                                    _hibaLista.Add(hiba);
                                    break;
                                case NodeNameCollection.DokumentumVisszaigazolasValasz.Uzenet:
                                    CsopDokLetoltVisszaigValaszSiker siker = new CsopDokLetoltVisszaigValaszSiker();
                                    siker.getData(visszaIgazolasItem);
                                    _sikeresLista.Add(siker);
                                    break;
                            }
                        }
                    }
                    if (valaszItem.Name.Equals(NodeNameCollection.BaseNodes.hibaUzenet))
                    {
                        _hibaUzenet.Szam = int.Parse(valaszItem.FirstChild.InnerText);
                        _hibaUzenet.Tartalom = valaszItem.LastChild.InnerText;
                    }
                }
            }

            public static CsoportosDokumentumLetoltesVisszaigazolasValasz getError(Exception ex)
            {
                CsoportosDokumentumLetoltesVisszaigazolasValasz _return = new CsoportosDokumentumLetoltesVisszaigazolasValasz();
                _return.HibaUzenet.Szam = 5000;
                if (ex.InnerException != null)
                {
                    _return.HibaUzenet.Tartalom = ex.InnerException.Message;
                }
                else
                {
                    _return.HibaUzenet.Tartalom = ex.Message;
                }
                return _return;
            }
        }

        public class CsopDokLetoltVisszaigValaszHiba
        {
            private int _hibaKod;
            private string _hibaSzoveg;

            public int HibaKod
            {
                get { return _hibaKod; }
                set { _hibaKod = value; }
            }
            public string HibaSzoveg
            {
                get { return _hibaSzoveg; }
                set { _hibaSzoveg = value; }
            }

            public CsopDokLetoltVisszaigValaszHiba()
            {
                this._hibaKod = -1;
                this._hibaSzoveg = string.Empty;
            }

            public void getData(XmlNode xNode)
            {
                foreach (XmlNode innerNode in xNode.FirstChild.ChildNodes)
                {
                    switch (innerNode.Name)
                    { 
                        case NodeNameCollection.DokumentumVisszaigazolasValasz.DokumentumVisszaigazolasHiba.HibaKod:
                            this._hibaKod = int.Parse(innerNode.InnerText);
                            break;
                        case NodeNameCollection.DokumentumVisszaigazolasValasz.DokumentumVisszaigazolasHiba.HibaSzoveg:
                            this._hibaSzoveg = innerNode.InnerText;
                            break;
                    }
                }
            }
        }

        public class CsopDokLetoltVisszaigValaszSiker
        {
            private int _szam;
            private string _szoveg;

            public int Szam
            {
                get { return _szam; }
                set { _szam = value; }
            }
            public string Szoveg
            {
                get { return _szoveg; }
                set { _szoveg = value; }
            }

            public CsopDokLetoltVisszaigValaszSiker()
            {
                this._szam = -1;
                this._szoveg = string.Empty;
            }

            public void getData(XmlNode xNode)
            {
                foreach (XmlNode innerNode in xNode.FirstChild.ChildNodes)
                {
                    switch (innerNode.Name)
                    {
                        case NodeNameCollection.DokumentumVisszaigazolasValasz.DokumentumVisszaigazolasSikeres.Szam:
                            this._szam = int.Parse(innerNode.InnerText);
                            break;
                        case NodeNameCollection.DokumentumVisszaigazolasValasz.DokumentumVisszaigazolasSikeres.Szoveg:
                            this._szoveg = innerNode.InnerText;
                            break;
                    }
                }
            }
        }

        public class VisszaigazolasAdatok
        {
            private string _erkeztetesiSzam;
            private bool _sikeres;

            public string ErkeztetesiSzam
            {
                get { return _erkeztetesiSzam; }
                set { _erkeztetesiSzam = value; }
            }
            public bool Sikeres
            {
                get { return _sikeres; }
                set { _sikeres = value; }
            }

            public VisszaigazolasAdatok()
            {
                this._erkeztetesiSzam = string.Empty;
                this._sikeres = false;
            }
        }

        public class CsoportosDokumentumFeltoltesValasz : BaseEnvelopeTags
        {
            private hkpValaszFejlec _valaszFejlec;
            private hkpHibaUzenet _hibaUzenet;
            private List<UjDokumentumAdatokValasz> _ujDokumentumAdatokList;
            private List<hkpHibaLeiras> _hibaLista;

            public hkpValaszFejlec ValaszFejlec
            {
                get { return _valaszFejlec; }
                set { _valaszFejlec = value; }
            }
            public hkpHibaUzenet HibaUzenet
            {
                get { return _hibaUzenet; }
                set { _hibaUzenet = value; }
            }
            public List<UjDokumentumAdatokValasz> UjDokumentumAdatokList
            {
                get { return _ujDokumentumAdatokList; }
                set { _ujDokumentumAdatokList = value; }
            }
            public List<hkpHibaLeiras> HibaLista
            {
                get { return _hibaLista; }
                set { _hibaLista = value; }
            }

            public CsoportosDokumentumFeltoltesValasz()
            {
                this._valaszFejlec = new hkpValaszFejlec();
                this._hibaUzenet = new hkpHibaUzenet();
                this._ujDokumentumAdatokList = new List<UjDokumentumAdatokValasz>();
                this._hibaLista = new List<hkpHibaLeiras>();
            }

            public void getData(XmlDocument xDoc)
            {
                XmlNodeList ValaszNode = xDoc.GetElementsByTagName(NodeNameCollection.BaseNodes.valasz);
                if (ValaszNode.Count == 0)
                    return; //TODO nulla visszatérési érték kezelése
                XmlNode ValaszFejlecNode = ValaszNode.Item(0).FirstChild;
                _valaszFejlec.Felhasznalo = ValaszFejlecNode.FirstChild.InnerText;
                _valaszFejlec.UzenetIdopont = ValaszFejlecNode.LastChild.InnerText;
                foreach (XmlNode valaszItem in ValaszNode.Item(0).ChildNodes)
                {
                    if (valaszItem.Name.Equals(NodeNameCollection.BaseNodes.formDef))
                    {
                        foreach (XmlNode adatValasz in valaszItem.FirstChild.ChildNodes)
                        {
                            foreach (XmlNode TarolasValasz in adatValasz.ChildNodes)
                            {
                                switch (TarolasValasz.Name)
                                {
                                    case NodeNameCollection.CsoportosDokumentumFeltoltesValasz.UjDokumentum:
                                        UjDokumentumAdatokValasz valasz = new UjDokumentumAdatokValasz();
                                        valasz.getData(TarolasValasz);
                                        UjDokumentumAdatokList.Add(valasz);
                                        break;
                                    case NodeNameCollection.CsoportosDokumentumFeltoltesValasz.HibaTag:
                                        hkpHibaLeiras hiba = new hkpHibaLeiras();
                                        hiba.getData(TarolasValasz);
                                        _hibaLista.Add(hiba);
                                        break;
                                }
                            }
                        }
                    }
                    if (valaszItem.Name.Equals(NodeNameCollection.BaseNodes.hibaUzenet))
                    {
                        _hibaUzenet.Szam = int.Parse(valaszItem.FirstChild.InnerText);
                        _hibaUzenet.Tartalom = valaszItem.LastChild.InnerText;
                    }
                }
            }

            public static CsoportosDokumentumFeltoltesValasz getError(Exception ex)
            {
                CsoportosDokumentumFeltoltesValasz _return = new CsoportosDokumentumFeltoltesValasz();
                _return.HibaUzenet.Szam = 5000;
                if (ex.InnerException != null)
                {
                    _return.HibaUzenet.Tartalom = ex.InnerException.Message;
                }
                else
                {
                    _return.HibaUzenet.Tartalom = ex.Message;
                }
                return _return;
            }

        }

        public class UjDokumentumAdatokValasz
        {
            private string _erkeztetesiSzam;
            private string _erkeztetesiDatum;
            private string _idopecset;

            public string ErkeztetesiSzam
            {
                get { return _erkeztetesiSzam; }
                set { _erkeztetesiSzam = value; }
            }
            public string ErkeztetesiDatum
            {
                get { return _erkeztetesiDatum; }
                set { _erkeztetesiDatum = value; }
            }
            public string Idopecset
            {
                get { return _idopecset; }
                set { _idopecset = value; }
            }

            public UjDokumentumAdatokValasz()
            {
                _erkeztetesiSzam = String.Empty;
                _erkeztetesiDatum = String.Empty;
                _idopecset = String.Empty;
            }

            public void getData(XmlNode input)
            {
                foreach (XmlNode inputItem in input.ChildNodes)
                {
                    switch (inputItem.Name)
                    {
                        case NodeNameCollection.CsoportosDokumentumFeltoltesValasz.UjDokumentumAdatok.ErkeztetesiSzam:
                            this._erkeztetesiSzam = inputItem.InnerText;
                            break;
                        case NodeNameCollection.CsoportosDokumentumFeltoltesValasz.UjDokumentumAdatok.ErkeztetesiDatum:
                            this._erkeztetesiDatum = inputItem.InnerText;
                            break;
                        case NodeNameCollection.CsoportosDokumentumFeltoltesValasz.UjDokumentumAdatok.Idopecset:
                            this._idopecset = inputItem.InnerText;
                            break;
                    }
                }
            }
        }

        public class hkpHibaLeiras
        {
            private string _hibaKod;
            private string _hibaSzoveg;

            public string HibaKod
            {
                get { return _hibaKod; }
                set { _hibaKod = value; }
            }
            public string HibaSzoveg
            {
                get { return _hibaSzoveg; }
                set { _hibaSzoveg = value; }
            }

            public hkpHibaLeiras()
            {
                HibaKod = String.Empty;
                HibaSzoveg = String.Empty;
            }

            public void getData(XmlNode input)
            {
                foreach (XmlNode inputItem in input.FirstChild.ChildNodes)
                {
                    switch (inputItem.Name)
                    { 
                        case NodeNameCollection.CsoportosDokumentumFeltoltesValasz.Hiba.HibaKod:
                            this._hibaKod = inputItem.InnerText;
                            break;
                        case NodeNameCollection.CsoportosDokumentumFeltoltesValasz.Hiba.HibaSzoveg:
                            this._hibaSzoveg = inputItem.InnerText;
                            break;
                    }
                }
            }
        }

        public class AzonositasKerdes : BaseEnvelopeTags
        {
            private hkpValaszFejlec _valaszFejlec;
            private hkpHibaUzenet _hibaUzenet;
            private hkpTermeszetesSzemelyAzonosito _termeszetesSzemelyAzonosito;

            public hkpValaszFejlec ValaszFejlec
            {
                get { return _valaszFejlec; }
                set { _valaszFejlec = value; }
            }
            public hkpHibaUzenet HibaUzenet
            {
                get { return _hibaUzenet; }
                set { _hibaUzenet = value; }
            }
            public hkpTermeszetesSzemelyAzonosito TermeszetesSzemelyAzonosito
            {
                get { return _termeszetesSzemelyAzonosito; }
                set { _termeszetesSzemelyAzonosito = value; }
            }

            public AzonositasKerdes()
            {
                this._valaszFejlec = new hkpValaszFejlec();
                this._hibaUzenet = new hkpHibaUzenet();
                this._termeszetesSzemelyAzonosito = new hkpTermeszetesSzemelyAzonosito();
            }
        }

        public class hkpTermeszetesSzemelyAzonosito
        {
            private NevAdat _viseltNeve;
            private NevAdat _szuletesiNeve;
            private NevAdat _anyjaNeve;
            private hkpSzuletesiHely _szuletesiHely;
            private DateTime _szuletesiIdo;

            public NevAdat ViseltNeve
            {
                get { return _viseltNeve; }
                set { _viseltNeve = value; }
            }
            public NevAdat SzuletesiNeve
            {
                get { return _szuletesiNeve; }
                set { _szuletesiNeve = value; }
            }
            public NevAdat AnyjaNeve
            {
                get { return _anyjaNeve; }
                set { _anyjaNeve = value; }
            }
            public hkpSzuletesiHely SzuletesiHely
            {
                get { return _szuletesiHely; }
                set { _szuletesiHely = value; }
            }
            public DateTime SzuletesiIdo
            {
                get { return _szuletesiIdo; }
                set { _szuletesiIdo = value; }
            }

            public hkpTermeszetesSzemelyAzonosito()
            {
                this._viseltNeve = new NevAdat();
                this._szuletesiNeve = new NevAdat();
                this._anyjaNeve = new NevAdat();
                this._szuletesiHely = new hkpSzuletesiHely();
                this._szuletesiIdo = new DateTime();
            }
        }

        public class NevAdat
        {
            private string _csaladiNev;
            private string _utoNev1;
            private string _utoNev2;
            private string _doktor;

            public string CsaladiNev
            {
                get { return _csaladiNev; }
                set { _csaladiNev = value; }
            }
            public string UtoNev1
            {
                get { return _utoNev1; }
                set { _utoNev1 = value; }
            }
            public string UtoNev2
            {
                get { return _utoNev2; }
                set { _utoNev2 = value; }
            }
            public string Doktor
            {
                get { return _doktor; }
                set { _doktor = value; }
            }

            public NevAdat()
            {
                this._csaladiNev = String.Empty;
                this._utoNev1 = String.Empty;
                this._utoNev2 = String.Empty;
                this._doktor = String.Empty;
            }
        }

        public class hkpSzuletesiHely
        {
            private string _telepules;
            private string _orszag;

            public string Telepules
            {
                get { return _telepules; }
                set { _telepules = value; }
            }
            public string Orszag
            {
                get { return _orszag; }
                set { _orszag = value; }
            }

            public hkpSzuletesiHely()
            {
                this._telepules = String.Empty;
                this._orszag = String.Empty;
            }
        }

        public class AzonositasValasz : BaseEnvelopeTags
        {
            private hkpValaszFejlec _valaszFejlec;
            private hkpHibaUzenet _hibaUzenet;
            private List<Azonositott> _azonositottList;

            public hkpValaszFejlec ValaszFejlec
            {
                get { return _valaszFejlec; }
                set { _valaszFejlec = value; }
            }
            public hkpHibaUzenet HibaUzenet
            {
                get { return _hibaUzenet; }
                set { _hibaUzenet = value; }
            }
            public List<Azonositott> AzonositottList
            {
                get { return _azonositottList; }
                set { _azonositottList = value; }
            }

            public AzonositasValasz()
            {
                this._valaszFejlec = new hkpValaszFejlec();
                this._hibaUzenet = new hkpHibaUzenet();
                this._azonositottList = new List<Azonositott>();
            }

            public void getData(XmlDocument xDoc)
            {
                XmlNodeList ValaszNode = xDoc.GetElementsByTagName(NodeNameCollection.BaseNodes.valasz);
                if (ValaszNode.Count == 0)
                    return; //TODO nulla visszatérési érték kezelése
                XmlNode ValaszFejlecNode = ValaszNode.Item(0).FirstChild;
                _valaszFejlec.Felhasznalo = ValaszFejlecNode.FirstChild.InnerText;
                _valaszFejlec.UzenetIdopont = ValaszFejlecNode.LastChild.InnerText;
                foreach (XmlNode valaszItem in ValaszNode.Item(0).ChildNodes)
                {
                    if (valaszItem.Name.Equals(NodeNameCollection.BaseNodes.formDef))
                    {
                        foreach (XmlNode azonositottItem in valaszItem.FirstChild.ChildNodes)
                        {
                            if (azonositottItem.ChildNodes.Count != 0)
                            {
                                Azonositott azonItem = new Azonositott();
                                azonItem.getData(azonositottItem.FirstChild);
                                this._azonositottList.Add(azonItem);
                            }
                        }
                    }
                    if (valaszItem.Name.Equals(NodeNameCollection.BaseNodes.hibaUzenet))
                    {
                        _hibaUzenet.Szam = int.Parse(valaszItem.FirstChild.InnerText);
                        _hibaUzenet.Tartalom = valaszItem.LastChild.InnerText;
                    }
                }
            }

            public static AzonositasValasz getError(Exception ex)
            {
                AzonositasValasz _return = new AzonositasValasz();
                _return.HibaUzenet.Szam = 5000;
                if (ex.InnerException != null)
                {
                    _return.HibaUzenet.Tartalom = ex.InnerException.Message;
                }
                else
                {
                    _return.HibaUzenet.Tartalom = ex.Message;
                }
                return _return;
            }
        }

        public class Azonositott
        {
            private string _kapcsolatiKod;
            private string _nev;
            private string _email;

            public string KapcsolatiKod
            {
                get { return _kapcsolatiKod; }
                set { _kapcsolatiKod = value; }
            }
            public string Nev
            {
                get { return _nev; }
                set { _nev = value; }
            }
            public string Email
            {
                get { return _email; }
                set { _email = value; }
            }

            public Azonositott()
            {
                this._kapcsolatiKod = String.Empty;
                this._nev = String.Empty;
                this._email = String.Empty;
            }

            public void getData(XmlNode input)
            {
                foreach (XmlNode inputElement in input.ChildNodes)
                {
                    switch (inputElement.Name)
                    { 
                        case NodeNameCollection.Azonositas.AzonositottAdatok.Nev:
                            this._nev = inputElement.InnerText;
                            break;
                        case NodeNameCollection.Azonositas.AzonositottAdatok.KapcsolatiKod:
                            this._kapcsolatiKod = inputElement.InnerText;
                            break;
                        case NodeNameCollection.Azonositas.AzonositottAdatok.Email:
                            this._email = inputElement.InnerText;
                            break;
                    }
                }
            }
        }
    }
}