﻿using System;
using System.Collections.Generic;
using System.Configuration;
using KRService.Core.Soap;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;

namespace KRService.Core
{
    public class KRCaller
    {
        private string iopServiceUrl;
        private string KRID; 
        private string Felhasznalo;
        private string wsseSec_Username;
        private string wsseSec_Password;
        private LogMaster logger;
        private string eDok_FelhasznaloId;
        private bool isServiceActive;
        
        public KRCaller()
        {
            this.iopServiceUrl = ConfigurationManager.AppSettings["iopServiceUrl"];
            this.KRID = ConfigurationManager.AppSettings["KRID"];
            this.Felhasznalo = ConfigurationManager.AppSettings["Felhasznalo"];
            this.wsseSec_Username = ConfigurationManager.AppSettings["wsseSec_Username"];
            this.wsseSec_Password = ConfigurationManager.AppSettings["wsseSec_Password"];
            this.logger = new LogMaster();
            this.eDok_FelhasznaloId = ConfigurationManager.AppSettings["eDok_FelhasznaloId"];
            this.isServiceActive = bool.Parse(ConfigurationManager.AppSettings["isServiceActive"]);
        }

        public Result PostaFiokFeldolgozas()
        {
            Result _return = new Result();
            ExecParam param = new ExecParam();
            param.Felhasznalo_Id = eDok_FelhasznaloId;

            logger.Info("postaFiokAdatokLekerdezese kezdete");
            HKP2.PostaFiokAdatokLekerdezesValasz pfLekerdezes = postaFiokAdatokLekerdezese();
            logger.Info("postaFiokAdatokLekerdezese vege");
            if (pfLekerdezes.HibaUzenet.Szam != -1)
            {
                _return.ErrorCode = pfLekerdezes.HibaUzenet.Szam.ToString();
                _return.ErrorMessage = pfLekerdezes.HibaUzenet.Tartalom;
                logger.Error(String.Format("postaFiokAdatokLekerdezese hiba, Hibakód: {0}, Hibaüzenet: {1}", _return.ErrorCode, _return.ErrorMessage));
                return _return;
            }
            logger.Info(String.Format("AtmenetiKezbvaroUzenetek: {0}", pfLekerdezes.AtmenetiKezbvaroUzenetek));

            int uzenetekSzama = pfLekerdezes.AtmenetiKezbvaroUzenetek;

            while(uzenetekSzama > 0)
            {
                int letoltottUzenetekSzama;
                _return = CsoportosDokumentumLetoltesEsFeldolgozas(out letoltottUzenetekSzama);
                uzenetekSzama = uzenetekSzama - letoltottUzenetekSzama;

                if (!String.IsNullOrEmpty(_return.ErrorCode))
                {
                    return _return;
                }
            }
            return _return;
        }

        public Result HivatalokListajaFeldolgozas()
        {
            return HivatalokListajaFeldolgozas(String.Empty, String.Empty);
        }

        public Result HivatalokListajaFeldolgozas(string hivatalRovidNevSzuro, string hivatalNevSzuro)
        {
            Result _return = new Result();
            HKP2.HivatalokListajaValasz hlLekerdezes = hivatalokListajaLekerdezes(hivatalRovidNevSzuro, hivatalNevSzuro);
            if (hlLekerdezes.HibaUzenet.Szam != -1)
            {
                _return.ErrorCode = hlLekerdezes.HibaUzenet.Szam.ToString();
                _return.ErrorMessage = hlLekerdezes.HibaUzenet.Tartalom;
            }
            _return.Record = hlLekerdezes;
            return _return;
        }

        public Result CsoportosDokumentumLetoltes()
        {
            Result _return = new Result();
            ExecParam param = new ExecParam();
            param.Felhasznalo_Id = eDok_FelhasznaloId;
            List<HKP2.VisszaigazolasAdatok> visszaIgList = new List<HKP2.VisszaigazolasAdatok>();

            HKP2.CsoportosDokumentumLetoltesValasz csopDokLetoltesLekerdezes = csopDokumentumLetoltesLekerdezes();
            if (csopDokLetoltesLekerdezes.HibaUzenet.Szam != -1)
            {
                _return.ErrorCode = csopDokLetoltesLekerdezes.HibaUzenet.Szam.ToString();
                _return.ErrorMessage = csopDokLetoltesLekerdezes.HibaUzenet.Tartalom;
                return _return;
            }
            foreach (HKP2.hkpDokumentumAdatok dokumentumItem in csopDokLetoltesLekerdezes.DokumentumList)
            {
                EREC_eBeadvanyok dokAdatok = new EREC_eBeadvanyok();
                try
                {
                    dokAdatok = dokumentumItem.get_eBusinessObject();
                }
                catch 
                {
                    //TODO ?
                }
                Csatolmany csatolmany = csopDokLetoltesLekerdezes.getCsatolmanyByFilename(dokumentumItem.FileNev);
                HKP2.VisszaigazolasAdatok visszaIgItem = new HKP2.VisszaigazolasAdatok();
                visszaIgItem.ErkeztetesiSzam = dokumentumItem.ErkezetesiSzam;

                #region eDok kommunikáció és ellenőrzése, hogy engedélyezve van-e a configban
                bool isServiceActive = true;

                //TODO - Majd ha bekerül a rendszerbe...
                /*if (ConfigMaster.getAppSetting("eDOKServices_Active") == "true")
                {
                    isServiceActive = true;
                }*/
                if (isServiceActive)
                {
                    EREC_eBeadvanyokService service = EdokWrapper.GetService();
                    Result serviceResult = service.InsertAndAttachDocument(param, dokAdatok, new Csatolmany[] { csatolmany });
                    if (String.IsNullOrEmpty(serviceResult.ErrorCode))
                    {
                        visszaIgItem.Sikeres = true;
                    }
                    else
                    {
                        logger.Warn(String.Format("KRCaller#CsoportosDokumentumLetoltes#EREC_eBeadvanyokService - lekérdezés hiba! Hibakód: {0}; HibaÜzenet: {1}"
                        , serviceResult.ErrorCode, serviceResult.ErrorMessage));
                        visszaIgItem.Sikeres = false;
                    }
                }
                else
                {
                    logger.Warn("KRCaller#CsoportosDokumentumLetoltes - A konfigurációban nincs engedélyezve az eDok rendszer használata, így nem történik feldolgozás. Nem sikeres DokumentumVisszaigazolás indítása.");
                }
                #endregion

                visszaIgList.Add(visszaIgItem);

            }
            HKP2.CsoportosDokumentumLetoltesVisszaigazolasValasz csopDokLetoltesVisszaigazolasLekerdezes = csopDokumentumLetoltesVisszaigazolas(visszaIgList);
            if (csopDokLetoltesVisszaigazolasLekerdezes.HibaUzenet.Szam != -1)
            {
                logger.Error(String.Format("KRCaller#CsoportosDokumentumLetoltes#CsoportosDokumentumVisszaigazolas - A visszaigazolás nem volt sikeres. Hibakód: {0}; Hibaüzenet {1}",
                    csopDokLetoltesVisszaigazolasLekerdezes.HibaUzenet.Szam.ToString(), csopDokLetoltesVisszaigazolasLekerdezes.HibaUzenet.Tartalom));
                _return.ErrorCode = csopDokLetoltesVisszaigazolasLekerdezes.HibaUzenet.Szam.ToString();
                _return.ErrorMessage = csopDokLetoltesVisszaigazolasLekerdezes.HibaUzenet.Tartalom;
            }
            if (csopDokLetoltesVisszaigazolasLekerdezes.HibaLista.Count != 0)
            {
                foreach (HKP2.CsopDokLetoltVisszaigValaszHiba hibaItem in csopDokLetoltesVisszaigazolasLekerdezes.HibaLista)
                {
                    logger.Warn(String.Format("KRCaller#CsoportosDokumentumLetoltes#CsoportosDokumentumVisszaigazolas - A visszaigazolás nem volt sikeres. Hibakód: {0}; Hibaüzenet {1}",
                        hibaItem.HibaKod, hibaItem.HibaSzoveg));
                }
            }
            _return.Record = csopDokLetoltesVisszaigazolasLekerdezes;
            return _return;
        }

        public Result CsoportosDokumentumLetoltesVisszaigazolas(List<HKP2.VisszaigazolasAdatok> visszaIgazolasAdatokList)
        {
            Result _return = new Result();
            HKP2.CsoportosDokumentumLetoltesVisszaigazolasValasz csopDokVisszaigValasz = csopDokumentumLetoltesVisszaigazolas(visszaIgazolasAdatokList);
            if (csopDokVisszaigValasz.HibaUzenet.Szam != -1)
            {
                _return.ErrorCode = csopDokVisszaigValasz.HibaUzenet.Szam.ToString();
                _return.ErrorMessage = csopDokVisszaigValasz.HibaUzenet.Tartalom;
            }
            _return.Record = csopDokVisszaigValasz;
            return _return;
        }

        public Result CsoportosDokumentumFeltoltes(List<Csatolmany> csatolmanyList, List<EREC_eBeadvanyok> dokAdatokList)
        {
            Result _return = new Result();
            //teszt
            //foreach (Csatolmany cs in csatolmanyList)
            //{
            //    System.IO.File.WriteAllBytes("C:\\Temp\\" + cs.Nev, cs.Tartalom);
            //}
            HKP2.CsoportosDokumentumFeltoltesValasz csopdokFelValasz = csoportosDokumentumFeltoltese(csatolmanyList, dokAdatokList);
            if (csopdokFelValasz.HibaUzenet.Szam != -1)
            {
                _return.ErrorCode = csopdokFelValasz.HibaUzenet.Szam.ToString();
                _return.ErrorMessage = csopdokFelValasz.HibaUzenet.Tartalom;
            }
            _return.Record = csopdokFelValasz;
            return _return;
        }

        public Result Azonositas(HKP2.AzonositasKerdes azonKerdes)
        {
            Result _return = new Result();
            HKP2.AzonositasValasz azonValasz = AzonositasLekerdezes(azonKerdes);
            if (azonValasz.HibaUzenet.Szam != -1)
            {
                _return.ErrorCode = azonValasz.HibaUzenet.Szam.ToString();
                _return.ErrorMessage = azonValasz.HibaUzenet.Tartalom;
            }
            _return.Record = azonValasz;
            return _return;
        }

        #region private methods (messageBuilding and other methods)

        private HKP2.PostaFiokAdatokLekerdezesValasz postaFiokAdatokLekerdezese()
        {
            try
            {
                SoapCaller caller = new SoapCaller(iopServiceUrl);
                caller.setRequestEnvelope(getMessage_PostaFiokAdatokLekerdezes());
                caller.call();
                HKP2.PostaFiokAdatokLekerdezesValasz valasz = new HKP2.PostaFiokAdatokLekerdezesValasz();
                valasz.getData(caller.SoapResult.Envelope);
                return valasz;
            }
            catch (Exception ex)
            {
                return HKP2.PostaFiokAdatokLekerdezesValasz.getError(ex);
            }
        }

        private HKP2.CsoportosDokumentumLetoltesValasz csopDokumentumLetoltesLekerdezes()
        {
            try
            {
                SoapCaller caller = new SoapCaller(iopServiceUrl);
                caller.setRequestEnvelope(getMessage_CsoportosDokumentumLetoltes());
                caller.call(true);
                HKP2.CsoportosDokumentumLetoltesValasz valasz = new HKP2.CsoportosDokumentumLetoltesValasz();
                valasz.getData(caller.SoapResult.Envelope);
                valasz.AttachmentList = caller.SoapResult.Attachments;
                return valasz;
            }
            catch (Exception ex)
            {
                return HKP2.CsoportosDokumentumLetoltesValasz.getError(ex);
            }
        }

        private HKP2.CsoportosDokumentumLetoltesVisszaigazolasValasz csopDokumentumLetoltesVisszaigazolas(List<HKP2.VisszaigazolasAdatok> visszaIgList)
        {
            try
            {
                SoapCaller caller = new SoapCaller(iopServiceUrl);
                caller.setRequestEnvelope(getMessage_CsoportosDokumentumLetoltesVisszaigazolas(visszaIgList));
                caller.call();
                HKP2.CsoportosDokumentumLetoltesVisszaigazolasValasz valasz = new HKP2.CsoportosDokumentumLetoltesVisszaigazolasValasz();
                valasz.getData(caller.SoapResult.Envelope);
                return valasz;
            }
            catch (Exception ex)
            {
                return HKP2.CsoportosDokumentumLetoltesVisszaigazolasValasz.getError(ex);
            }
        }

        private HKP2.HivatalokListajaValasz hivatalokListajaLekerdezes()
        {
            return hivatalokListajaLekerdezes(String.Empty, String.Empty);
        }

        private HKP2.HivatalokListajaValasz hivatalokListajaLekerdezes(string hivatalRovidNevSzuro, string hivatalNevSzuro)
        {
            try
            {
                SoapCaller caller = new SoapCaller(iopServiceUrl);
                caller.setRequestEnvelope(getMessage_HivatalokLekerdezese(hivatalRovidNevSzuro, hivatalNevSzuro));
                caller.call();
                HKP2.HivatalokListajaValasz valasz = new HKP2.HivatalokListajaValasz();
                valasz.getData(caller.SoapResult.Envelope);
                return valasz;
            }
            catch (Exception ex)
            {
                return HKP2.HivatalokListajaValasz.getError(ex);
            }
        }

        private HKP2.CsoportosDokumentumFeltoltesValasz csoportosDokumentumFeltoltese(List<Csatolmany> csatolmanyList, List<EREC_eBeadvanyok> dokAdatokList)
        {
            try
            {
                SoapCaller caller = new SoapCaller(iopServiceUrl);
                SoapBuilder builder = new SoapBuilder("--");
                SoapMessage message = getSoapMessage_CsoportosDokumentumFeltoltes(csatolmanyList,dokAdatokList);
                caller.callCustomRequest(builder.buildMessage(message, caller.destinationUrl));
                HKP2.CsoportosDokumentumFeltoltesValasz valasz = new HKP2.CsoportosDokumentumFeltoltesValasz();
                valasz.getData(caller.SoapResult.Envelope);
                return valasz;
            }
            catch (Exception ex)
            {
                return HKP2.CsoportosDokumentumFeltoltesValasz.getError(ex);
            }
        }

        private HKP2.AzonositasValasz AzonositasLekerdezes(HKP2.AzonositasKerdes azonKerdes)
        {
            try
            {
                SoapCaller caller = new SoapCaller(iopServiceUrl);
                caller.setRequestEnvelope(getMessage_AzonositasLekerdezese(azonKerdes));
                caller.call();
                HKP2.AzonositasValasz azonValasz = new HKP2.AzonositasValasz();
                azonValasz.getData(caller.SoapResult.Envelope);
                return azonValasz;
            }
            catch (Exception ex)
            {
                return HKP2.AzonositasValasz.getError(ex);
            }
        }

        private string getMessage_PostaFiokAdatokLekerdezes()
        {
            string _return = @"<?xml version=""1.0"" encoding=""UTF-8""?>
                            <soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://schemas.xmlsoap.org/soap/envelope/
                            http://schemas.xmlsoap.org/soap/envelope/"">
	                        <soap:Header>
		                    <wsse:Security xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"">
			                <wsse:UsernameToken>
				            <wsse:Username>" + wsseSec_Username + @"</wsse:Username>
				            <wsse:Password>" + wsseSec_Password + @"</wsse:Password>
			                </wsse:UsernameToken>
		                    </wsse:Security>
	                        </soap:Header>
	                        <soap:Body>
		                    <iop:Kerdes xmlns:iop=""http://www.iop.hu/2004"">
			                <iop:KerdesFejlec>
				            <iop:Felhasznalo>" + Felhasznalo + @"</iop:Felhasznalo>
				            <iop:UzenetIdopont>" + Utility.KRDateTimeHandler.getKRDateTime(DateTime.Now) + @"</iop:UzenetIdopont>
			                </iop:KerdesFejlec>
			                <iop:Parancs>
				            <iop:Rendszer>HKP</iop:Rendszer>
				            <iop:Szolgaltatas Module=""HivataliModule"">PostaFiokAdatokLekerdezese</iop:Szolgaltatas>
			                </iop:Parancs>
			                <iop:Form xmlns:hkp=""http://iop.gov.hu/2008/10/hkp"">
				            <hkp:PostafiokLekerdezesKerdes>
					        <hkp:PostafiokFelhasznalo>
						    <hkp:KRID>" + KRID + @"</hkp:KRID>
					        </hkp:PostafiokFelhasznalo>
				            </hkp:PostafiokLekerdezesKerdes>
			                </iop:Form>
		                    </iop:Kerdes>
	                        </soap:Body>
                            </soap:Envelope>";
            return Utility.xmlPrettify(_return);
        }

        private string getMessage_CsoportosDokumentumLetoltes()
        {
            //<hkp:FeladoTipusa>1</hkp:FeladoTipusa>
            string _return = @"<?xml version=""1.0"" encoding=""UTF-8""?>
                            <soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://schemas.xmlsoap.org/soap/envelope/
                            http://schemas.xmlsoap.org/soap/envelope/"">
	                        <soap:Header>
		                    <wsse:Security xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"">
			                <wsse:UsernameToken>
				            <wsse:Username>" + wsseSec_Username + @"</wsse:Username>
				            <wsse:Password>" + wsseSec_Password + @"</wsse:Password>
			                </wsse:UsernameToken>
		                    </wsse:Security>
	                        </soap:Header>
	                        <soap:Body>
		                    <iop:Kerdes xmlns:iop=""http://www.iop.hu/2004"">
			                <iop:KerdesFejlec>
				            <iop:Felhasznalo>" + Felhasznalo + @"</iop:Felhasznalo>
				            <iop:UzenetIdopont>" + Utility.KRDateTimeHandler.getKRDateTime(DateTime.Now) + @"</iop:UzenetIdopont>
			                </iop:KerdesFejlec>
			                <iop:Parancs>
				            <iop:Rendszer>HKP</iop:Rendszer>
				            <iop:Szolgaltatas Module=""HivataliModule"">DokumentumokLekerdezeseGepiInterfeszAltal</iop:Szolgaltatas>
			                </iop:Parancs>
			                <iop:Form xmlns:hkp=""http://iop.gov.hu/2008/10/hkp"">
				            <hkp:DokumentumAdatokLetoltesKerdes>
					        <hkp:PostafiokAlapjan>
						    <hkp:EredmenyekSzama>64</hkp:EredmenyekSzama>
					        </hkp:PostafiokAlapjan>
				            </hkp:DokumentumAdatokLetoltesKerdes>
			                </iop:Form>
		                    </iop:Kerdes>
	                        </soap:Body>
                            </soap:Envelope>";

            return Utility.xmlPrettify(_return);
        }

        private string getMessage_CsoportosDokumentumLetoltesVisszaigazolas(List<HKP2.VisszaigazolasAdatok> visszaIgList)
        {
            string _return = @"<?xml version=""1.0"" encoding=""UTF-8""?>
                            <soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://schemas.xmlsoap.org/soap/envelope/
                            http://schemas.xmlsoap.org/soap/envelope/"">
	                        <soap:Header>
		                    <wsse:Security xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"">
			                <wsse:UsernameToken>
				            <wsse:Username>" + wsseSec_Username + @"</wsse:Username>
				            <wsse:Password>" + wsseSec_Password + @"</wsse:Password>
			                </wsse:UsernameToken>
		                    </wsse:Security>
	                        </soap:Header>
	                        <soap:Body>
		                    <iop:Kerdes xmlns:iop=""http://www.iop.hu/2004"">
			                <iop:KerdesFejlec>
				            <iop:Felhasznalo>" + Felhasznalo + @"</iop:Felhasznalo>
				            <iop:UzenetIdopont>" + Utility.KRDateTimeHandler.getKRDateTime(DateTime.Now) + @"</iop:UzenetIdopont>
			                </iop:KerdesFejlec>
			                <iop:Parancs>
				            <iop:Rendszer>HKP</iop:Rendszer>
				            <iop:Szolgaltatas Module=""HivataliModule"">SikeresDokumentumokOlvasas</iop:Szolgaltatas>
			                </iop:Parancs>
			                <iop:Form>
				            <hkp:OlvasasVisszaigazolas xmlns:hkp=""http://iop.gov.hu/2008/10/hkp"">";
            foreach (HKP2.VisszaigazolasAdatok visszaIgItem in visszaIgList)
            {
                _return += @"<hkp:VisszaigazolasAdatok>
						<hkp:ErkeztetesiSzam>" + visszaIgItem.ErkeztetesiSzam.ToString() + @"</hkp:ErkeztetesiSzam>
						<hkp:Sikeres>" + visszaIgItem.Sikeres.ToString() + @"</hkp:Sikeres>
					    </hkp:VisszaigazolasAdatok>";
            }
            _return += @"</hkp:OlvasasVisszaigazolas>
			            </iop:Form>
		                </iop:Kerdes>
	                    </soap:Body>
                        </soap:Envelope>";
            return Utility.xmlPrettify(_return);
        }

        private string getMessage_HivatalokLekerdezese(string hivatalRovidNevSzuro, string hivatalNevSzuro)
        { 
            string _return = @"<?xml version=""1.0"" encoding=""UTF-8""?>
                            <soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://schemas.xmlsoap.org/soap/envelope/
                            http://schemas.xmlsoap.org/soap/envelope/"">
	                        <soap:Header>
		                    <wsse:Security xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"">
			                <wsse:UsernameToken>
				            <wsse:Username>" + wsseSec_Username + @"</wsse:Username>
				            <wsse:Password>" + wsseSec_Password + @"</wsse:Password>
			                </wsse:UsernameToken>
		                    </wsse:Security>
	                        </soap:Header>
	                        <soap:Body>
		                    <iop:Kerdes xmlns:iop=""http://www.iop.hu/2004"">
			                <iop:KerdesFejlec>
				            <iop:Felhasznalo>" + Felhasznalo + @"</iop:Felhasznalo>
				            <iop:UzenetIdopont>" + Utility.KRDateTimeHandler.getKRDateTime(DateTime.Now) + @"</iop:UzenetIdopont>
			                </iop:KerdesFejlec>
			                <iop:Session>
                            </iop:Session>
			                <iop:Parancs>
				            <iop:Rendszer>HKP</iop:Rendszer>
				            <iop:Szolgaltatas Module=""Workflow"">HivatalokLekerdezese</iop:Szolgaltatas>
				            <iop:Cimke/>
			                </iop:Parancs>
			                <iop:Form>
				            <hkp:GIHivatalokLekerdezeseKerdes xmlns:hkp=""http://iop.gov.hu/2008/10/hkp"">
					        <hkp:RovidNevSzures>" + hivatalRovidNevSzuro + @"</hkp:RovidNevSzures>
					        <hkp:NevSzures>"+hivatalNevSzuro+@"</hkp:NevSzures>
				            </hkp:GIHivatalokLekerdezeseKerdes>
			                </iop:Form>
		                    </iop:Kerdes>
	                        </soap:Body>
                            </soap:Envelope>";

            return Utility.xmlPrettify(_return);
        }

        private string getMessage_AzonositasLekerdezese(HKP2.AzonositasKerdes azonKerdes)
        {
            string _return =  @"<?xml version=""1.0"" encoding=""UTF-8""?>
                        <soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://schemas.xmlsoap.org/soap/envelope/
                        http://schemas.xmlsoap.org/soap/envelope/"">
		                <soap:Header>
		                <wsse:Security xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"">
			            <wsse:UsernameToken>
				        <wsse:Username>" + wsseSec_Username + @"</wsse:Username>
				        <wsse:Password>" + wsseSec_Password + @"</wsse:Password>
			            </wsse:UsernameToken>
		                </wsse:Security>
	                    </soap:Header>
	                    <soap:Body>
		                <iop:Kerdes xmlns:iop=""http://www.iop.hu/2004"">
			            <iop:KerdesFejlec>
				        <iop:Felhasznalo>" + Felhasznalo + @"</iop:Felhasznalo>
				        <iop:UzenetIdopont>" + Utility.KRDateTimeHandler.getKRDateTime(DateTime.Now) + @"</iop:UzenetIdopont>
			            </iop:KerdesFejlec>
			            <iop:Session />
			            <iop:Parancs>
				        <iop:Rendszer>KRI</iop:Rendszer>
				        <iop:Szolgaltatas Module=""Workflow"">Azonositas</iop:Szolgaltatas>
			            </iop:Parancs>
			            <iop:Form>
				        <hkp:AzonositasKerdes xmlns:hkp=""http://iop.gov.hu/2008/10/hkp"">
					    <hkp:TermeszetesSzemelyAzonosito>
						<hkp:ViseltNeve>";

            if (!String.IsNullOrEmpty(azonKerdes.TermeszetesSzemelyAzonosito.ViseltNeve.Doktor))
                    _return += "<hkp:Doktor>"+azonKerdes.TermeszetesSzemelyAzonosito.ViseltNeve.Doktor +"</hkp:Doktor>";

            _return +=@"<hkp:CsaladiNev>" + azonKerdes.TermeszetesSzemelyAzonosito.ViseltNeve.CsaladiNev + @"</hkp:CsaladiNev>
						<hkp:UtoNev1>" + azonKerdes.TermeszetesSzemelyAzonosito.ViseltNeve.UtoNev1 + @"</hkp:UtoNev1>
						<hkp:UtoNev2>" + azonKerdes.TermeszetesSzemelyAzonosito.ViseltNeve.UtoNev2 + @"</hkp:UtoNev2>
						</hkp:ViseltNeve>
						<hkp:SzuletesiNeve>";

            if (!String.IsNullOrEmpty(azonKerdes.TermeszetesSzemelyAzonosito.SzuletesiNeve.Doktor))
                _return += "<hkp:Doktor>" + azonKerdes.TermeszetesSzemelyAzonosito.SzuletesiNeve.Doktor + "</hkp:Doktor>";

            _return +=@"<hkp:CsaladiNev>" + azonKerdes.TermeszetesSzemelyAzonosito.SzuletesiNeve.CsaladiNev + @"</hkp:CsaladiNev>
						<hkp:UtoNev1>" + azonKerdes.TermeszetesSzemelyAzonosito.SzuletesiNeve.UtoNev1 + @"</hkp:UtoNev1>
						<hkp:UtoNev2>" + azonKerdes.TermeszetesSzemelyAzonosito.SzuletesiNeve.UtoNev2 + @"</hkp:UtoNev2>
						</hkp:SzuletesiNeve>
						<hkp:AnyjaNeve>";

            if (!String.IsNullOrEmpty(azonKerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.Doktor))
                _return += "<hkp:Doktor>" + azonKerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.Doktor + "</hkp:Doktor>";

            _return +=@"<hkp:CsaladiNev>" + azonKerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.CsaladiNev + @"</hkp:CsaladiNev>
						<hkp:UtoNev1>" + azonKerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.UtoNev1 + @"</hkp:UtoNev1>
						<hkp:UtoNev2>" + azonKerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.UtoNev2 + @"</hkp:UtoNev2>
						</hkp:AnyjaNeve>
						<hkp:SzuletesiHely>
			            <hkp:Telepules>" + azonKerdes.TermeszetesSzemelyAzonosito.SzuletesiHely.Telepules + @"</hkp:Telepules>";

            if (!String.IsNullOrEmpty(azonKerdes.TermeszetesSzemelyAzonosito.SzuletesiHely.Orszag))
                _return += "<hkp:Orszag>" + azonKerdes.TermeszetesSzemelyAzonosito.SzuletesiHely.Orszag + "</hkp:Orszag>";

            _return += @"</hkp:SzuletesiHely>
						<hkp:SzuletesiIdo>" + azonKerdes.TermeszetesSzemelyAzonosito.SzuletesiIdo.ToString("yyyy-MM-dd") + @"</hkp:SzuletesiIdo>
					    </hkp:TermeszetesSzemelyAzonosito>
				        </hkp:AzonositasKerdes>
			            </iop:Form>
		                </iop:Kerdes>
	                    </soap:Body>
                        </soap:Envelope>";

            return Utility.xmlPrettify(_return);
        }

        private SoapMessage getSoapMessage_CsoportosDokumentumFeltoltes(List<Csatolmany> csatolmanyList, List<EREC_eBeadvanyok> dokAdatokList)
        {
            SoapMessage _return = new SoapMessage();
            string envelopeString = @"<?xml version=""1.0"" encoding=""UTF-8""?>
                        <soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://schemas.xmlsoap.org/soap/envelope/
                        http://schemas.xmlsoap.org/soap/envelope/"">
	                    <soap:Header>
		                <wsse:Security xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"">
			            <wsse:UsernameToken>
				        <wsse:Username>"+wsseSec_Username+@"</wsse:Username>
				        <wsse:Password>"+wsseSec_Password+@"</wsse:Password>
			            </wsse:UsernameToken>
		                </wsse:Security>
	                    </soap:Header>
	                    <soap:Body>
		                <iop:Kerdes xmlns:iop=""http://www.iop.hu/2004"">
			            <iop:KerdesFejlec>
				        <iop:Felhasznalo>" + Felhasznalo + @"</iop:Felhasznalo>
				        <iop:UzenetIdopont>" + Utility.KRDateTimeHandler.getKRDateTime(DateTime.Now) + @"</iop:UzenetIdopont>
			            </iop:KerdesFejlec>
			            <iop:Session/>
			            <iop:Parancs>
				        <iop:Rendszer>HKP</iop:Rendszer>
				        <iop:Szolgaltatas Module=""HivataliModule"">UjDokumentumokFeltolteseGepiInterfeszAltal</iop:Szolgaltatas>
				        <iop:Cimke/>
			            </iop:Parancs>
			            <iop:Form xmlns:hkp=""http://iop.gov.hu/2008/10/hkp"">
				        <hkp:UjDokumentumokFeltoltes>";

            ContentIdMapper mapper = new ContentIdMapper();

            foreach (EREC_eBeadvanyok dokadatItem in dokAdatokList)
            {
                envelopeString += @"<hkp:DokumentumAdatok href=""" + mapper.GetContentId(dokadatItem.KR_FileNev) + @""">";

                if (!String.IsNullOrEmpty(dokadatItem.KR_HivatkozasiSzam))
                    envelopeString += "<hkp:HivatkozasiSzam>" + dokadatItem.KR_HivatkozasiSzam + "</hkp:HivatkozasiSzam>";
                if (!String.IsNullOrEmpty(dokadatItem.PartnerKapcsolatiKod))
                    envelopeString += "<hkp:Cimzett><hkp:KapcsolatiKod>" + dokadatItem.PartnerKapcsolatiKod + "</hkp:KapcsolatiKod></hkp:Cimzett>";
                else
                    envelopeString += "<hkp:Cimzett><hkp:KRID>" + dokadatItem.PartnerKRID + "</hkp:KRID></hkp:Cimzett>";

                envelopeString += @"<hkp:DokTipusHivatal>" + dokadatItem.KR_DokTipusHivatal + @"</hkp:DokTipusHivatal>
						<hkp:DokTipusAzonosito>" + dokadatItem.KR_DokTipusAzonosito + @"</hkp:DokTipusAzonosito>
						<hkp:DokTipusLeiras>" + dokadatItem.KR_DokTipusLeiras + @"</hkp:DokTipusLeiras>
						<hkp:Megjegyzes>" + dokadatItem.KR_Megjegyzes + @"</hkp:Megjegyzes>
						<hkp:FileNev>" + dokadatItem.KR_FileNev + @"</hkp:FileNev>
						<hkp:ValaszUtvonal>" + dokadatItem.KR_Valaszutvonal + @"</hkp:ValaszUtvonal>
						<hkp:ValaszTitkositas>" + Utility.GetKRBoolean(dokadatItem.KR_Valasztitkositas) + @"</hkp:ValaszTitkositas>
						<hkp:RendszerUzenet>" + Utility.GetKRBoolean(dokadatItem.KR_Rendszeruzenet) + @"</hkp:RendszerUzenet>
						<hkp:ETertiveveny>" + Utility.GetKRBoolean(dokadatItem.KR_ETertiveveny) + @"</hkp:ETertiveveny> 
					    </hkp:DokumentumAdatok>";
                //TODO - ETertiveveny módosítása -> HKPDokumentumAdatok módosítása
            }
			envelopeString += @"</hkp:UjDokumentumokFeltoltes>
			            </iop:Form>
		                </iop:Kerdes>
	                    </soap:Body>
                        </soap:Envelope>";
            _return.Envelope.LoadXml(envelopeString);
            _return.EnvelopeHeader.Charset = "utf-8";
            _return.EnvelopeHeader.ContentType = "text/xml";
            _return.EnvelopeHeader.ContentTransfer_Encoding = "8bit";
            _return.Header.ContentType.Boundary = "MIMEBoundary_placeholders";
            _return.Header.ContentType.Charset = "ISO-8859-2";
            _return.Header.ContentType.ContentType = @"""text/xml""";
            _return.Header.ContentType.MessageType = "multipart/related";

            foreach (Csatolmany csatolmanyItem in csatolmanyList)
            {
                SoapAttachment attachment = new SoapAttachment();
                attachment.ContentStream = csatolmanyItem.Tartalom;
                attachment.Filename = csatolmanyItem.Nev;
                attachment.Header.ContentTransfer_Encoding = Soapifier.getContentTransferEncoding_ByFilename(csatolmanyItem.Nev);
                attachment.Header.ContentType = Soapifier.getContentType_ByFilename(csatolmanyItem.Nev);
                attachment.Header.Filename = csatolmanyItem.Nev;
                attachment.Header.Name = csatolmanyItem.Nev;
                attachment.Header.ContentDisposition = "attachment";
                attachment.Header.ContentID = "<" + mapper.GetContentId(csatolmanyItem.Nev) + ">";
                _return.Attachments.Add(attachment);
            }

            return _return;
        }

        private class ContentIdMapper
        {
            const string csatString = "csat-{0}";
            private Dictionary<string, string> _ContentIds = new Dictionary<string, string>();

            public string GetContentId(string fileName)
            {
                if (!_ContentIds.ContainsKey(fileName))
                {
                    int i = _ContentIds.Count;
                    _ContentIds.Add(fileName, String.Format(csatString, i));
                }

                return _ContentIds[fileName];
            }
        }

        private Result CsoportosDokumentumLetoltesEsFeldolgozas(out int letoltottUzenetekSzama)
        {
            Result _return = new Result();
            ExecParam param = new ExecParam();
            param.Felhasznalo_Id = eDok_FelhasznaloId;

            logger.Info("csopDokumentumLetoltesLekerdezes kezdete");
            HKP2.CsoportosDokumentumLetoltesValasz csopDokLetoltesLekerdezes = csopDokumentumLetoltesLekerdezes();
            logger.Info("csopDokumentumLetoltesLekerdezes vege");

            if (csopDokLetoltesLekerdezes.HibaUzenet.Szam != -1)
            {
                letoltottUzenetekSzama = 0;
                _return.ErrorCode = csopDokLetoltesLekerdezes.HibaUzenet.Szam.ToString();
                _return.ErrorMessage = csopDokLetoltesLekerdezes.HibaUzenet.Tartalom;
                return _return;
            }

            letoltottUzenetekSzama = csopDokLetoltesLekerdezes.DokumentumList.Count;
            logger.Info(String.Format("letoltottUzenetekSzama: {0}", letoltottUzenetekSzama));

            List<HKP2.VisszaigazolasAdatok> visszaIgList = new List<HKP2.VisszaigazolasAdatok>();
            foreach (HKP2.hkpDokumentumAdatok dokumentumItem in csopDokLetoltesLekerdezes.DokumentumList)
            {
                EREC_eBeadvanyok dokAdatok = new EREC_eBeadvanyok();
                try
                {
                    dokAdatok = dokumentumItem.get_eBusinessObject();
                }
                catch
                {
                    //TODO?
                }
                Csatolmany csatolmany = csopDokLetoltesLekerdezes.getCsatolmanyByContentID(dokumentumItem.Href);
                csatolmany.Nev = dokumentumItem.FileNev;
                HKP2.VisszaigazolasAdatok visszaIgItem = new HKP2.VisszaigazolasAdatok();
                visszaIgItem.ErkeztetesiSzam = dokumentumItem.ErkezetesiSzam;
                logger.Debug(String.Format("ErkezetesiSzam={0}", dokumentumItem.ErkezetesiSzam));

                //teszt
                //System.IO.File.WriteAllBytes("C:\\Temp\\" + dokumentumItem.FileNev, csatolmany.Tartalom);

                #region eDok kommunikáció
                if (isServiceActive)
                {
                    try
                    {
                        bool insert;
                        string rendszer;

                        // CR3114 : HAIR adatok letöltése
                        if (EdokWrapper.RendszerSelect(dokumentumItem) == EdokWrapper.Rendszer.HAIR)
                        {
                            insert = true;
                            dokAdatok.Cel = EdokWrapper.Rendszer.HAIR;

                        }
                        else
                        {
                            EdokWrapper.CheckHivatkozasiSzam(dokumentumItem, out insert, out rendszer);
                            dokAdatok.Cel = rendszer;
                        }

                        logger.Debug(String.Format("insert: {0}", insert));
                        logger.Debug(String.Format("cel: {0}", dokAdatok.Cel));
                        if (insert)
                        {
                            logger.Info(String.Format("insert: {0} kezdete", dokumentumItem.ErkezetesiSzam));

                            #region eDok - Keresés érkeztetési szám alapján
                            EREC_eBeadvanyokService service = EdokWrapper.GetService();
                            EREC_eBeadvanyokSearch search = new EREC_eBeadvanyokSearch();
                            search.KR_ErkeztetesiSzam.Value = dokumentumItem.ErkezetesiSzam;
                            search.KR_ErkeztetesiSzam.Operator = "=";
                            search.Irany.Value = "0";
                            search.Irany.Operator = "=";
                            // CR3181: duplikalt sorok betöltődése
                            // Érvényességi időt ne nézze
                            search.ErvKezd.Clear();
                            search.ErvVege.Clear();

                            logger.Info(String.Format("EREC_eBeadvanyokService.GetAll kezdete: {0}", dokumentumItem.ErkezetesiSzam));
                            Result eDokUzenetekSearchResult = service.GetAll(param, search);
                            logger.Info(String.Format("EREC_eBeadvanyokService.GetAll vege: {0}", dokumentumItem.ErkezetesiSzam));
                            if (!String.IsNullOrEmpty(eDokUzenetekSearchResult.ErrorCode))
                            {
                                visszaIgItem.Sikeres = false;
                                logger.Error(String.Format("EREC_eBeadvanyokService.GetAll hiba! Hibakód: {0}; HibaÜzenet: {1}"
                                       , eDokUzenetekSearchResult.ErrorCode, eDokUzenetekSearchResult.ErrorMessage));
                                _return = eDokUzenetekSearchResult;
                            }
                            else
                            {
                                if (eDokUzenetekSearchResult.Ds.Tables[0].Rows.Count == 0)
                                {
                                    logger.Info("EREC_eBeadvanyokService.InsertAndAttachDocument kezdete");
                                    Result serviceResult = service.InsertAndAttachDocument(param, dokAdatok, new Csatolmany[] { csatolmany });
                                    logger.Info("EREC_eBeadvanyokService.InsertAndAttachDocument vege");
                                    if (String.IsNullOrEmpty(serviceResult.ErrorCode))
                                    {
                                        visszaIgItem.Sikeres = true;
                                    }
                                    else
                                    {
                                        logger.Error(String.Format("KP_DokumentumAdatokService.InsertAndAttachDocument hiba! Hibakód: {0}; HibaÜzenet: {1}"
                                        , serviceResult.ErrorCode, serviceResult.ErrorMessage));
                                        visszaIgItem.Sikeres = false;
                                        _return = serviceResult;
                                    }
                                }
                                else
                                {
                                    logger.Info("EDOK-ban már benne van!");
                                    // CR3181: A korábban már betöltött üzeneteket ne töltse le mindig újra
                                    // Ez a HKP felé sikeres-nek igazolható vissza 
                                    //visszaIgItem.Sikeres = false;   
                                    visszaIgItem.Sikeres = true;
                                }
                            }
                            #endregion

                            logger.Info(String.Format("insert: {0} vege", dokumentumItem.ErkezetesiSzam));
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("KRCaller#PostaFiokLekerdezes - Hiba az edok hívás során!", ex);
                        visszaIgItem.Sikeres = false;
                        _return.ErrorCode = "6666";
                        _return.ErrorMessage = ex.Message;
                    }
                }
                else
                {
                    logger.Warn("KRCaller#PostaFiokLekerdezes - A konfigurációban nincs engedélyezve az eDok rendszer használata, így nem történik feldolgozás. Nem sikeres DokumentumVisszaigazolás indítása.");
                }
                #endregion
                visszaIgList.Add(visszaIgItem);
            }
            logger.Info("csopDokumentumLetoltesVisszaigazolas kezdete");
            HKP2.CsoportosDokumentumLetoltesVisszaigazolasValasz csopDokLetoltesVisszaigazolasLekerdezes = csopDokumentumLetoltesVisszaigazolas(visszaIgList);
            logger.Info("csopDokumentumLetoltesVisszaigazolas vege");
            return _return;
        }

        #endregion
    }
}