﻿using System;
using System.Collections.Generic;
using System.Text;
using log4net;

namespace KRService.Core
{
    public class LogMaster
    {
        internal ILog log;
        
        public LogMaster()
        {
           log = LogManager.GetLogger(typeof(HKP_KRService.KRService));
           GlobalContext.Properties["component"] = "HKP_KRService";
           log4net.Config.XmlConfigurator.Configure();
        }

        public bool IsDebugEnabled
        {
            get { return log.IsDebugEnabled; }
        }
        public bool IsErrorEnabled
        {
            get { return log.IsErrorEnabled; }
        }
        public bool IsFatalEnabled
        {
            get { return log.IsFatalEnabled; }
        }
        public bool IsInfoEnabled
        {
            get { return log.IsInfoEnabled; }
        }
        public bool IsWarnEnabled
        {
            get { return log.IsWarnEnabled; }
        }

        public void Error(object message)
        {
            log.Error(message);
        }
        public void Error(object message, Exception ex)
        {
            log.Error(message, ex);
        }
        public void Info(object message)
        {
            log.Info(message);
        }
        public void Info(object message, Exception ex)
        {
            log.Info(message, ex);
        }
        public void Fatal(object message)
        {
            log.Fatal(message);
        }
        public void Fatal(object message, Exception ex)
        {
            log.Fatal(message, ex);
        }
        public void Debug(object message)
        {
            log.Debug(message);
        }
        public void Debug(object message, Exception ex)
        {
            log.Debug(message, ex);
        }
        public void Warn(object message)
        {
            log.Warn(message);
        }
        public void Warn(object message, Exception ex)
        {
            log.Warn(message, ex);
        }
    }
}
