﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Reflection;
using System.Security.Cryptography;

namespace KRService.Core
{
    public class Utility
    {
        public class KRDateTimeHandler
        {
            public static DateTime getDateTime_FromKR(string inputDate)
            {
                DateTime outDateTime = new DateTime();
                //inputDate = inputDate.Replace("T", " ").Replace("Z", "");
                if (DateTime.TryParse(inputDate, out outDateTime))
                {
                    return outDateTime;
                }
                else
                    return DateTime.Today;
            }

            public static string getKRDateTime(DateTime inputDate)
            {
                return inputDate.ToString("yyyy-MM-ddTHH:mm:ssZ");
            }
        }

        public static string xmlPrettify(string input)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(input);
                return doc.OuterXml;
            }
            catch (Exception)
            {
                return input;
            }
        }

        public static string GetKRBoolean(string edokInput)
        {
            if ("1".Equals(edokInput))
                return "true";
            return "false";
        }

        public static string GetEDOKBoolean(bool krInput)
        {
            if (krInput)
                return "1";
            return "0";
        }

        public static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input

            MD5 md5 = System.Security.Cryptography.MD5.Create();

            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);


            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {

                sb.Append(hash[i].ToString("X2"));

            }

            return sb.ToString();

        }

    }
}
