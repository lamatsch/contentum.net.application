﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Xml.Serialization;

using KRService.Core;
using Contentum.eBusinessDocuments;

namespace HKP_KRService
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "Contentum.HKP_KRService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class KRService : System.Web.Services.WebService
    {
        [WebMethod]
        public void PostaFiokFeldolgozas()
        {
            LogMaster logger = new LogMaster();
            try
            {
                logger.Info("KRService#PostaFiokFeldolgozas - Lekérdezés kezdet: " + DateTime.Now.ToString());
                KRCaller caller = new KRCaller();
                Result callerResult = caller.PostaFiokFeldolgozas();
                if (!String.IsNullOrEmpty(callerResult.ErrorCode))
                {
                    logger.Error(String.Format("HKPService#Postafioklekerdezes - lekérdezés hiba! Hibakód: {0}; HibaÜzenet: {1}"
                        , callerResult.ErrorCode, callerResult.ErrorMessage));
                }
                logger.Info("KRService#PostaFiokFeldolgozas  - Lekérdezés vége: " + DateTime.Now.ToString());
            }
            catch (Exception ex)
            {
                logger.Error("PostaFiokFeldolgozas hiba!", ex);
            }
        }

        [WebMethod]
        [XmlInclude(typeof(HKP2.HivatalokListajaValasz))]
        public Result HivatalokListajaFeldolgozas()
        {
            return HivatalokListajaFeldolgozas_Szurt(String.Empty, String.Empty);
        }

        [WebMethod]
        [XmlInclude(typeof(HKP2.HivatalokListajaValasz))]
        public Result HivatalokListajaFeldolgozas_Szurt(string hivatalRovidNevSzuro, string hivatalNevSzuro)
        {
            LogMaster logger = new LogMaster();
            try
            {
                logger.Info("KRService#HivatalokListajaFeldolgozas - Lekérdezés kezdet: " + DateTime.Now.ToString());
                KRCaller caller = new KRCaller();
                Result callerResult = caller.HivatalokListajaFeldolgozas(hivatalRovidNevSzuro, hivatalNevSzuro);
                if (!String.IsNullOrEmpty(callerResult.ErrorCode))
                {
                    logger.Error(String.Format("HKPService#HivatalokListajaFeldolgozas - lekérdezés hiba! Hibakód: {0}; HibaÜzenet: {1}"
                        , callerResult.ErrorCode, callerResult.ErrorMessage));
                }
                logger.Info("KRService#HivatalokListajaFeldolgozas  - Lekérdezés vége: " + DateTime.Now.ToString());
                return callerResult;
            }
            catch (Exception ex)
            {
                logger.Error("HivatalokListajaFeldolgozas hiba!", ex);
            }
            return new Result();
        }

        [WebMethod]
        [XmlInclude(typeof(HKP2.CsoportosDokumentumFeltoltesValasz))]
        public Result CsoportosDokumentumFeltoltes(List<Csatolmany>csatolmanyList, List<EREC_eBeadvanyok> dokAdatokList)
        {
            LogMaster logger = new LogMaster();
            try
            {
                logger.Info("KRService#CsoportosDokumentumFeltoltes - Feltöltés kezdete: " + DateTime.Now.ToString());
                KRCaller caller = new KRCaller();
                /*//TESZT
                List<Csatolmany> csatolmanyList = new List<Csatolmany>();
                List<EREC_eBeadvanyok> dokAdatokList = new List<EREC_eBeadvanyok>();
                Csatolmany csatolmany = new Csatolmany();
                csatolmany.Nev = "teszt.pdf";
                csatolmany.Tartalom = System.IO.File.ReadAllBytes("C:\\Users\\molnarp_axis\\Documents\\teszt.pdf");
                csatolmanyList.Add(csatolmany);

                EREC_eBeadvanyok dokAdat = new EREC_eBeadvanyok();
                dokAdat.KR_FileNev = "teszt.pdf";
                dokAdat.KR_DokTipusHivatal = "BPFPH";
                dokAdat.Cimzett = "101098399";
                dokAdat.KR_DokTipusAzonosito = "0058";
                dokAdat.KR_Valasztitkositas = "false";
                dokAdat.KR_Valaszutvonal = "1";
                dokAdat.KR_Rendszeruzenet = "false";
                dokAdat.KR_Megjegyzes = "This is a test";
                dokAdat.KR_DokTipusLeiras = "teszt";
                dokAdatokList.Add(dokAdat);
                //TESZT*/
                Result callerResult = caller.CsoportosDokumentumFeltoltes(csatolmanyList, dokAdatokList);
                if (!String.IsNullOrEmpty(callerResult.ErrorCode))
                {
                    logger.Error(String.Format("HKPService#CsoportosDokumentumFeltoltes - feltöltési hiba! Hibakód: {0}; HibaÜzenet: {1}"
                        , callerResult.ErrorCode, callerResult.ErrorMessage));
                }
                logger.Info("KRService#CsoportosDokumentumFeltoltes  - Feltöltés vége: " + DateTime.Now.ToString());
                return callerResult;
            }
            catch (Exception ex)
            {
                logger.Error("CsoportosDokumentumFeltoltes hiba!", ex);
            }
            return new Result();
        }

        [WebMethod]
        [XmlInclude(typeof(HKP2.CsoportosDokumentumLetoltesVisszaigazolasValasz))]
        public Result CsoportosDokumentumLetoltes_NotControlled()
        {
            LogMaster logger = new LogMaster();
            try
            {
                logger.Info("KRService#CsoportosDokumentumLetoltes_NotControlled - Lekérdezés kezdet: " + DateTime.Now.ToString());
                KRCaller caller = new KRCaller();
                Result callerResult = caller.CsoportosDokumentumLetoltes();
                if (!String.IsNullOrEmpty(callerResult.ErrorCode))
                {
                    logger.Error(String.Format("HKPService#CsoportosDokumentumLetoltes_NotControlled - lekérdezés hiba! Hibakód: {0}; HibaÜzenet: {1}"
                        , callerResult.ErrorCode, callerResult.ErrorMessage));
                }
                logger.Info("KRService#CsoportosDokumentumLetoltes_NotControlled  - Lekérdezés vége: " + DateTime.Now.ToString());
                return callerResult;
            }
            catch (Exception ex)
            {
                logger.Error("CsoportosDokumentumLetoltes_NotControlled hiba!", ex);
            }
            return new Result();
        }

        [WebMethod]
        [XmlInclude(typeof(HKP2.CsoportosDokumentumLetoltesVisszaigazolasValasz))]
        public Result CsoportosDokumentumLetoltesVisszaigazolas(List<HKP2.VisszaigazolasAdatok> visszaIgAdatok)
        {
            LogMaster logger = new LogMaster();
            try
            {
                logger.Info("KRService#CsoportosDokumentumLetoltesVisszaigazolas - Lekérdezés kezdet: " + DateTime.Now.ToString());
                KRCaller caller = new KRCaller();
                Result callerResult = caller.CsoportosDokumentumLetoltesVisszaigazolas(visszaIgAdatok);
                if (!String.IsNullOrEmpty(callerResult.ErrorCode))
                {
                    logger.Error(String.Format("HKPService#CsoportosDokumentumLetoltesVisszaigazolas - lekérdezés hiba! Hibakód: {0}; HibaÜzenet: {1}"
                        , callerResult.ErrorCode, callerResult.ErrorMessage));
                }
                logger.Info("KRService#CsoportosDokumentumLetoltesVisszaigazolas  - Lekérdezés vége: " + DateTime.Now.ToString());
                return callerResult;
            }
            catch (Exception ex)
            {
                logger.Error("CsoportosDokumentumLetoltesVisszaigazolas hiba!", ex);
            }
            return new Result();
        }

        [WebMethod]
        [XmlInclude(typeof(HKP2.AzonositasValasz))]
        public Result Azonositas(HKP2.AzonositasKerdes azonKerdes)
        {
            LogMaster logger = new LogMaster();
            try
            {
                logger.Info("KRService#Azonositas - Lekérdezés kezdet: " + DateTime.Now.ToString());
                KRCaller caller = new KRCaller();
                Result callerResult = caller.Azonositas(azonKerdes);
                if (!String.IsNullOrEmpty(callerResult.ErrorCode))
                {
                    logger.Error(String.Format("HKPService#Azonositas - lekérdezés hiba! Hibakód: {0}; HibaÜzenet: {1}"
                        , callerResult.ErrorCode, callerResult.ErrorMessage));
                }
                logger.Info("KRService#Azonositas  - Lekérdezés vége: " + DateTime.Now.ToString());
                return callerResult;
            }
            catch (Exception ex)
            {
                logger.Error("Azonositas hiba!", ex);
            }
            return new Result();
        }
    }
}