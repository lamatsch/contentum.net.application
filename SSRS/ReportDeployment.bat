@echo off 
IF "%1"=="" GOTO :HELP 
IF "%2"=="" GOTO :HELP
IF "%3"=="" GOTO :HELP

::Script Variables 
SET LOGFILE=".\ReportDeployment_%2%_log.txt" 
SET REPORTSERVER=%1
SET PARENT_FOLDER=%2
SET FILEPATH=%3
SET RS="RS.EXE" 
SET TIMEOUT=60 
::Clear Log file 
IF EXIST %logfile% DEL %logfile% 
::Run Scripts
echo. ReportDeployment script paramterek: filepath: %FILEPATH%" szerver: %REPORTSERVER% parentFolder: %PARENT_FOLDER% 
%rs% -i "./ReportDeployment.rss" -s "%REPORTSERVER%" -v parentFolder="%PARENT_FOLDER%" -v filePath="%FILEPATH%" -e Mgmt2010
ECHO Finished Load at TE% %TIME% >> %LOGFILE% 
ECHO. >>%LOGFILE%
pause
GOTO :EOF

:HELP
echo. Hasznalat:
echo.  1. parameter: SSRS szerver nev
echo.
echo.  2. parameter: SSRS almappa ahova a riportokat telep�teni kell 
echo.
echo.  3. parameter: RDL f�jlok el�r�si �tvonala
echo.
echo. Pelda:
echo. ReportDeployment.bat http://axhuophypsql01:81/ReportServer_AX_VFPHTST02 NMHH_R2T c:\SSRS

:EOF
PAUSE 