@echo off 
IF "%1"=="" GOTO :HELP 
IF "%2"=="" GOTO :HELP
IF "%3"=="" GOTO :HELP

::Script Variables 
SET LOGFILE=".\ReportDeployment_log.txt" 
SET RS="RS.EXE" 
SET TIMEOUT=60 
::Run Scripts
echo. Adminisztracio
%rs% -i "./ReportDeployment.rss" -s "%1" -v parentFolder="%2/Adminisztracio" -v filePath="%3\Adminisztracio" -e Mgmt2010
echo. BizottsagiEloterjesztesek
%rs% -i "./ReportDeployment.rss" -s "%1" -v parentFolder="%2/BizottsagiEloterjesztesek" -v filePath="%3\BizottsagiEloterjesztesek" -e Mgmt2010
echo. Erkeztetes
%rs% -i "./ReportDeployment.rss" -s "%1" -v parentFolder="%2/Erkeztetes" -v filePath="%3\Erkeztetes" -e Mgmt2010
echo. Iktatas
%rs% -i "./ReportDeployment.rss" -s "%1" -v parentFolder="%2/Iktatas" -v filePath="%3\Iktatas" -e Mgmt2010
echo. IktatokonyvLista
%rs% -i "./ReportDeployment.rss" -s "%1" -v parentFolder="%2/IktatokonyvLista" -v filePath="%3\IktatokonyvLista" -e Mgmt2010
echo. Iratmozgas
%rs% -i "./ReportDeployment.rss" -s "%1" -v parentFolder="%2/Iratmozgas" -v filePath="%3\Iratmozgas" -e Mgmt2010
echo. Irattarozas
%rs% -i "./ReportDeployment.rss" -s "%1" -v parentFolder="%2/Irattarozas" -v filePath="%3\Irattarozas" -e Mgmt2010
echo. KozgyulesiEloterjesztesek
%rs% -i "./ReportDeployment.rss" -s "%1" -v parentFolder="%2/KozgyulesiEloterjesztesek" -v filePath="%3\KozgyulesiEloterjesztesek" -e Mgmt2010
echo. Lekerdezes
%rs% -i "./ReportDeployment.rss" -s "%1" -v parentFolder="%2/Lekerdezes" -v filePath="%3\Lekerdezes" -e Mgmt2010
echo. Migracio_Foszam
%rs% -i "./ReportDeployment.rss" -s "%1" -v parentFolder="%2/Migracio_Foszam" -v filePath="%3\Migracio_Foszam" -e Mgmt2010
echo. Sablonok
%rs% -i "./ReportDeployment.rss" -s "%1" -v parentFolder="%2/Sablonok" -v filePath="%3\Sablonok" -e Mgmt2010
echo. Szamlak
%rs% -i "./ReportDeployment.rss" -s "%1" -v parentFolder="%2/Szamlak" -v filePath="%3\Szamlak" -e Mgmt2010
echo. TUK
%rs% -i "./ReportDeployment.rss" -s "%1" -v parentFolder="%2/TUK" -v filePath="%3\TUK" -e Mgmt2010
ECHO Finished Load at TE% %TIME% >> %LOGFILE% 
ECHO. >>%LOGFILE%
GOTO :EOF

:HELP
echo. Hasznalat:
echo.  1. parameter: SSRS szerver nev
echo.
echo.  2. parameter: SSRS almappa ahova a riportokat telep�teni kell 
echo.
echo.  3. parameter: RDL f�jlokat tartalmaz� k�nyvt�rak el�r�si �tvonala
echo.
echo. Pelda:
echo. ReportDeployment.bat http://axhuophypsql01:81/ReportServer_AX_VFPHTST02 NMHH_R2T c:\SSRS

:EOF

PAUSE 