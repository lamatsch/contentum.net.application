@echo off

if "%1"=="" goto :help 

set "url=http://axhuophypsql01:81/ReportServer_AX_VFPHTST02"

if "%1"=="NMHH" ( 
 SET "dir=NMHH"
 goto :install
)
if "%1"=="NMHHTUK" (
 SET "dir=TUK"
 goto :install
)
if "%1"=="FPH" (
 SET "dir=FPH_V3"
 goto :install
)

if "%1"=="CSEPEL" (
 SET "dir=CSEPEL"
 goto :install
)

if "%1"=="BOPMH" (
 SET "dir=BOPMH"
 goto :install
)


goto :help

:install


call SSRS_patch_install.bat %dir% %url% > AXIS_%1_SSRS_patch_install.log

goto :EOF

:help
echo. Hasznalat:
echo.  1. parameter: szerver nev
echo.  Egy az alabbiak kozul:
echo.  NMHH
echo.  NMHHTUK
echo.  FPH
echo.  CSEPEL
echo.  BOPMH
echo.
echo. Pelda:
echo. SSRS_patch_install_AXIS.bat NMHH

:EOF
PAUSE