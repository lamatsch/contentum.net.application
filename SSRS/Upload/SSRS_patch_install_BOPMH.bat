@echo off

if "%1"=="" goto :help 

set "url=http://ikt-bopmh/ReportServer"

if "%1"=="TESZT" ( 
 SET "dir=CONTENTUM_TEST"
 goto :install
)
if "%1"=="ELES" (
 SET "dir=CONTENTUM_PROD"
 goto :install
)


goto :help

:install


call SSRS_patch_install.bat %dir% %url% > BOPMH_%1_SSRS_patch_install.log

goto :EOF

:help
echo. Hasznalat:
echo.  1. parameter: szerver nev
echo.  Egy az alabbiak kozul:
echo.  TESZT
echo.  ELES
echo.
echo. Pelda:
echo. SSRS_patch_install_BOPMH.bat ELES

:EOF
PAUSE