﻿##################################
# Beállítandó paraméterek
# Előfeltételek:
#  -- Contentum, Contentum_migr és Felhasznalo_Szervezet ds-ek létezzenek a $environment-nek megfelelő mappában
#  -- A script legyen bemásolva az AKTUÁLIS mappába és ONNAN INDÍTSUK!!!!!
# 
# A meglévő mappák és riportok felülíródnak

param (
    [string]$environment = "NMHH_DEV", 
    [string]$webServiceUrl = "http://axhuophypsql01:81/ReportServer_AX_VFPHTST02",
	[string]$userName = "",
	[string]$password = ""
)



########################################################################x
# Származtatott paraméterek -- általában nincs velük teendő

function FormatPath([string]$path)
{
    return $path.Replace("//","/");
}

$reportFolder = $environment
$originalreportFolder  = $environment
$SourceDirectory = "."
$DataSourceName ="Contentum"
$DatasourceName2 = "Contentum_migr"
$DataSourcePath = FormatPath("/"+$environment + "/"+$DatasourceName)
$DataSourcePath2 = FormatPath("/"+$environment + "/" + $DatasourceName2)
$DataSetName = "Felhasznalo_szervezet"
$DataSetPath =  FormatPath("/"+$environment + "/"+ $DataSetName)
$DataSetName2 = "Felhasznalo_Szervezet"
$DataSetPath2 = FormatPath("/"+$environment + "/"+ $DataSetName2)


if($userName -ne '')
{
	$securePassword = ConvertTo-SecureString -String $password -AsPlainText -Force
    $credentials = New-Object System.Management.Automation.PSCredential($userName, $securePassword)
	$ssrsProxy = New-WebServiceProxy -Uri $webServiceUrl'/ReportService2010.asmx?WSDL' -Credential $credentials
}
else
{
	$ssrsProxy = New-WebServiceProxy -Uri $webServiceUrl'/ReportService2010.asmx?WSDL' -UseDefaultCredential
}

#######


function SSRSFolder ([string]$reportFolder,[string]$reportPath)
{
##########################################    
#Create Folder     
        try
        {
            if($reportFolder -ne '')
            {
                $ssrsProxy.CreateFolder($reportFolder, $reportPath, $null) | out-null
                if($reportPath -eq '/')
                {
                Write-Host "Folder `"$reportpath$reportFolder`" Created"
                }
                else
                {
                Write-Host "Folder `"$reportpath/$reportFolder`" Created"
            }
            }
        }
        catch [System.Web.Services.Protocols.SoapException]
        {
            if ($_.Exception.Detail.InnerText -match "rsItemAlreadyExists400")
            {
                Write-Host "Folder: $reportFolder already exists."
            }
            else
            {
                $msg = "Error creating folder: $reportFolder. Msg: '{0}'" -f $_.Exception.Detail.InnerText
                Write-Error $msg
            }
        }
}


Function SSRSItem ([string]$ItemType,$item,[string]$folder)
 {
 Write-host ""
 #ReportName
 if ($ItemType -ne "Resource")
 {
 $ItemName = [System.IO.Path]::GetFileNameWithoutExtension($item);
 }
 else
 {
 $ItemName = $item.Name
 }
 write-host $ItemName -ForegroundColor Green 

 #Upload File
     try
    {
        #Get Report content in bytes
        Write-Host "Getting file content of : $item"
        $byteArray = Get-Content $item.FullName -encoding byte
        $msg = "Size: {0} KB" -f "{0:N0}" -f ($byteArray.Length/1KB) 
        Write-Host $msg 
        
        Write-Host "Uploading to: $folder"
 
        #Sets property for images(only png)
        $type = $ssrsProxy.GetType().Namespace
        $datatype = ($type + '.Property')              
        $property =New-Object ($datatype);
        if ($ItemType -eq "Resource" -and $item -like "*.png")
        {
        $property.Name = "MimeType"
        $property.Value = “image/png”
        }
        else
        {
        $property = $null
        }

        #Call Proxy to upload report  
        $warnings =@();   
        $overwrite = $true 
        $ssrsProxy.CreateCatalogItem($ItemType,$itemName,$folder,$overwrite,$byteArray,$property,[ref]$warnings) | out-null
        if($warnings.Length -le 1) { Write-Host "Upload Success." -ForegroundColor Green }
        else 
        {        
            foreach ($message in $warnings)
                {
                if ($message.Code -ne "rsDataSourceReferenceNotPublished")
                    {
                    write-host "$($message.Severity) $($message.Code) $($message.Message)" -ForegroundColor Yellow
                    }
                }
        }
    }
    catch [System.IO.IOException]
    {
        $msg = "Error while reading rdl file : '{0}', Message: '{1}'" -f $rdlFile, $_.Exception.Message
        Write-Error msg
    }
    catch [System.Web.Services.Protocols.SoapException]
	{
    $caught = $false
            if ($_.Exception.Detail.InnerText -match 'rsItemAlreadyExists400')
            {
                $caught = $true
                Write-Host "Report: $itemName already exists." -ForegroundColor Red
            }
            if ($_.Exception.Detail.InnerText -match 'CoretechSSRS')
            {
                $caught = $true
                Write-Host "Cant't find Reporting Extention File." -ForegroundColor Red
            }
            elseif ($caught -eq $false)
            {
                $msg = "Error uploading report: $reportName. Msg: '{0}'" -f $_.Exception.Detail.InnerText
                Write-Error $msg
            }
		
	}
}

Function SSRSDatasource ([string]$ReportPath,$DataSourceName,$DataSourcePath)
{
$report = $ssrsProxy.GetItemDataSources($ReportPath)
ForEach ($Source in $report)
    {
    $proxyNamespace = $Source.GetType().Namespace
        $constDatasource = New-Object ("$proxyNamespace.DataSource")
        $constDatasource.Name = $DataSourceName
        $constDatasource.Item = New-Object ("$proxyNamespace.DataSourceReference")
        $constDatasource.Item.Reference = $DataSourcePath

    $Source.item = $constDatasource.Item
    $ssrsProxy.SetItemDataSources($ReportPath, $Source)
    Write-Host "Changing datasource `"$($Source.Name)`" to $($Source.ItemReference)"
    }
    
}

Function SSRSDataset ([string]$ReportPath, $DataSetName, $DataSetPath)
{
   Write-Host $ReportPath
   $report = $ssrsProxy.GetItemReferences($ReportPath,"DataSet")
   ForEach ($Set in $report)
   {
    $proxyNamespace = $Set.GetType().Namespace
    $dataSetReference = New-Object ("$proxyNamespace.ItemReference")
    if ($Set.Name -cmatch '_szervezet')
    {
    $dataSetReference.Reference = $DataSetPath
    $dataSetReference.Name = $DataSetName      
    }

    else

    {
      $dataSetReference.Reference = $DataSetPath2
      $dataSetReference.Name = $DataSetName2      
    }
    
    $Set = $dataSetReference
    Write-Host "Changing dataset `"$($Set.Name)`" to $($Set.Reference)"
    
    $ssrsProxy.SetItemReferences($ReportPath, @($Set))
   
   }
 }
 



##Create Folder Structure
#Create Base Folder

Write-Host "Creating Folders Structure:" -ForegroundColor DarkMagenta
SSRSFolder $reportFolder "/"

ForEach($Folder in Get-ChildItem $SourceDirectory -Directory)
    {
    #Add each folder in the sourcefolder to the reporting service 
    SSRSFolder $Folder.Name /$reportFolder
    }

#Upload Reports in root folder
ForEach ($rdlfile in Get-ChildItem $SourceDirectory -Filter *.rdl )
{
SSRSItem Report $rdlfile /$reportFolder

#Change Report Datasource
$ReportPath = FormatPath("/" + $reportFolder + "/" +  $rdlfile.BaseName)
 if ($ReportPath -match 'migr')
        {
           SSRSDatasource $ReportPath $DataSourceName2 $DataSourcePath2
        }

        else

        {
           SSRSDatasource $ReportPath $DataSourceName $DataSourcePath
        }
SSRSDataset $ReportPath $DataSetName $DataSetPath
}

##For Each folder
 
ForEach($Folder in Get-ChildItem $SourceDirectory -Directory)
    {
    #Add each folder in the sourcefolder to the reporting service 
    #SSRSFolder $Folder.Name /$Basefolder 
    
    #Add reports in the folder
    ForEach ($rdlfile in Get-ChildItem $Folder -Filter *.rdl )
        {

        $ReportPath = FormatPath /$reportFolder/$($folder.Name)
        SSRSItem Report $rdlfile $ReportPath 

        #Change Report Datasource
        $ReportPath = FormatPath("/" + $reportFolder + "/" + $folder.Name + "/" + $rdlfile.BaseName)

        if ($ReportPath -match 'migr')
        {
           SSRSDatasource $ReportPath $DataSourceName2 $DataSourcePath2
        }

        else

        {
           SSRSDatasource $ReportPath $DataSourceName $DataSourcePath
        }
        
        SSRSDataset $ReportPath $DataSetName $DataSetPath
        }
    }

