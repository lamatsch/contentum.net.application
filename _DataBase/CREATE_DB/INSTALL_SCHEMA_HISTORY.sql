﻿IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_TranzakciokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TranzakciokHistory] DROP CONSTRAINT [DF_KRT_TranzakciokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Tranz_ObjHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tranz_ObjHistory] DROP CONSTRAINT [DF_KRT_Tranz_ObjHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_TemplateManagerHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TemplateManagerHistory] DROP CONSTRAINT [DF_KRT_TemplateManagerHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_TelepulesekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TelepulesekHistory] DROP CONSTRAINT [DF_KRT_TelepulesekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_TartomanyokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TartomanyokHistory] DROP CONSTRAINT [DF_KRT_TartomanyokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Tartomanyok_SzervezetekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tartomanyok_SzervezetekHistory] DROP CONSTRAINT [DF_KRT_Tartomanyok_SzervezetekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Ragsz__Histo__44952D46]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagszamSavokHistory] DROP CONSTRAINT [DF__KRT_Ragsz__Histo__44952D46]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_RagSz__Histo__41B8C09B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagSzamokHistory] DROP CONSTRAINT [DF__KRT_RagSz__Histo__41B8C09B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_PartnerMinositesekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesekHistory] DROP CONSTRAINT [DF_KRT_PartnerMinositesekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_OrszagokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_OrszagokHistory] DROP CONSTRAINT [DF_KRT_OrszagokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_OrgokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_OrgokHistory] DROP CONSTRAINT [DF_KRT_OrgokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_NezetekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_NezetekHistory] DROP CONSTRAINT [DF_KRT_NezetekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_MuveletekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MuveletekHistory] DROP CONSTRAINT [DF_KRT_MuveletekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Modul_FunkcioHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Modul_FunkcioHistory] DROP CONSTRAINT [DF_KRT_Modul_FunkcioHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_MappaUtakHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaUtakHistory] DROP CONSTRAINT [DF_KRT_MappaUtakHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_MappaTartalmakHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaTartalmakHistory] DROP CONSTRAINT [DF_KRT_MappaTartalmakHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_MappakHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappakHistory] DROP CONSTRAINT [DF_KRT_MappakHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_KozteruletTipusokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_KozteruletTipusokHistory] DROP CONSTRAINT [DF_KRT_KozteruletTipusokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_KozteruletekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_KozteruletekHistory] DROP CONSTRAINT [DF_KRT_KozteruletekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_KodCsoportokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_KodCsoportokHistory] DROP CONSTRAINT [DF_KRT_KodCsoportokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Halozat__Ver__489AC854]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Halozati_NyomtatokHistory] DROP CONSTRAINT [DF__KRT_Halozat__Ver__489AC854]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Halozati_NyomtatokHistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Halozati_NyomtatokHistory] DROP CONSTRAINT [DF_KRT_Halozati_NyomtatokHistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_FunkcioListaHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioListaHistory] DROP CONSTRAINT [DF_KRT_FunkcioListaHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_FelhasznaloProfilokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloProfilokHistory] DROP CONSTRAINT [DF_KRT_FelhasznaloProfilokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felhasz__Ver__3B40CD36]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory] DROP CONSTRAINT [DF__KRT_Felhasz__Ver__3B40CD36]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Felhasznalok_Halozati_NyomtatoiHistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory] DROP CONSTRAINT [DF_KRT_Felhasznalok_Halozati_NyomtatoiHistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Extra_NapokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Extra_NapokHistory] DROP CONSTRAINT [DF_KRT_Extra_NapokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Erintett_TablakHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Erintett_TablakHistory] DROP CONSTRAINT [DF_KRT_Erintett_TablakHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_DokumentumKapcsolatokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatokHistory] DROP CONSTRAINT [DF_KRT_DokumentumKapcsolatokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_DokuAlairasKapcsolatokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokuAlairasKapcsolatokHistory] DROP CONSTRAINT [DF_KRT_DokuAlairasKapcsolatokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_BarkodSavokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_BarkodSavokHistory] DROP CONSTRAINT [DF_KRT_BarkodSavokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_BankszamlaszamokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_BankszamlaszamokHistory] DROP CONSTRAINT [DF_KRT_BankszamlaszamokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_AlkalmazasokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_AlkalmazasokHistory] DROP CONSTRAINT [DF_KRT_AlkalmazasokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_AlairtDokumentumokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_AlairtDokumentumokHistory] DROP CONSTRAINT [DF_KRT_AlairtDokumentumokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_UgyKezFeljegyzesekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyKezFeljegyzesekHistory] DROP CONSTRAINT [DF_EREC_UgyKezFeljegyzesekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_UgyiratKapcsolatokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatokHistory] DROP CONSTRAINT [DF_EREC_UgyiratKapcsolatokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_SzamlakHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_SzamlakHistory] DROP CONSTRAINT [DF_EREC_SzamlakHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_PldKapjakMegHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_PldKapjakMegHistory] DROP CONSTRAINT [DF_EREC_PldKapjakMegHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_Obj_MetaXmlHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Obj_MetaXmlHistory] DROP CONSTRAINT [DF_EREC_Obj_MetaXmlHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldTertivevenyekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldTertivevenyekHistory] DROP CONSTRAINT [DF_EREC_KuldTertivevenyekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldMellekletekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldMellekletekHistory] DROP CONSTRAINT [DF_EREC_KuldMellekletekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldKezFeljegyzesekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKezFeljegyzesekHistory] DROP CONSTRAINT [DF_EREC_KuldKezFeljegyzesekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldKapcsolatokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKapcsolatokHistory] DROP CONSTRAINT [DF_EREC_KuldKapcsolatokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldDokumentumokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldDokumentumokHistory] DROP CONSTRAINT [DF_EREC_KuldDokumentumokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldBekuldokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldBekuldokHistory] DROP CONSTRAINT [DF_EREC_KuldBekuldokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IrattariKikeroHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariKikeroHistory] DROP CONSTRAINT [DF_EREC_IrattariKikeroHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__Histo__5535A963]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariHelyekHistory] DROP CONSTRAINT [DF__EREC_Irat__Histo__5535A963]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IratMellekletekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratMellekletekHistory] DROP CONSTRAINT [DF_EREC_IratMellekletekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IratKapcsolatokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratKapcsolatokHistory] DROP CONSTRAINT [DF_EREC_IratKapcsolatokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IratelemKapcsolatokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratelemKapcsolatokHistory] DROP CONSTRAINT [DF_EREC_IratelemKapcsolatokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraKezFeljegyzesekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraKezFeljegyzesekHistory] DROP CONSTRAINT [DF_EREC_IraKezFeljegyzesekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraJegyzekTetelekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekTetelekHistory] DROP CONSTRAINT [DF_EREC_IraJegyzekTetelekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraJegyzekekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekekHistory] DROP CONSTRAINT [DF_EREC_IraJegyzekekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraIratokDokumentumokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraIratokDokumentumokHistory] DROP CONSTRAINT [DF_EREC_IraIratokDokumentumokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraElosztoivTetelekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivTetelekHistory] DROP CONSTRAINT [DF_EREC_IraElosztoivTetelekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraElosztoivekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivekHistory] DROP CONSTRAINT [DF_EREC_IraElosztoivekHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_Hatarid_ObjektumokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Hatarid_ObjektumokHistory] DROP CONSTRAINT [DF_EREC_Hatarid_ObjektumokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_FeladatDefinicioHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_FeladatDefinicioHistory] DROP CONSTRAINT [DF_EREC_FeladatDefinicioHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_eMailFiokokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_eMailFiokokHistory] DROP CONSTRAINT [DF_EREC_eMailFiokokHistory_HistoryId]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_VallalkozasokHistory]') AND name = N'IX_KRT_VallalkozasokHistory_ID_VER')
DROP INDEX [IX_KRT_VallalkozasokHistory_ID_VER] ON [dbo].[KRT_VallalkozasokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TranzakciokHistory]') AND name = N'IX_KRT_TranzakciokHistory_ID_VER')
DROP INDEX [IX_KRT_TranzakciokHistory_ID_VER] ON [dbo].[KRT_TranzakciokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tranz_ObjHistory]') AND name = N'IX_KRT_Tranz_ObjHistory_ID_VER')
DROP INDEX [IX_KRT_Tranz_ObjHistory_ID_VER] ON [dbo].[KRT_Tranz_ObjHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManagerHistory]') AND name = N'IX_KRT_TemplateManagerHistory_ID_VER')
DROP INDEX [IX_KRT_TemplateManagerHistory_ID_VER] ON [dbo].[KRT_TemplateManagerHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TelepulesekHistory]') AND name = N'IX_KRT_TelepulesekHistory_ID_VER')
DROP INDEX [IX_KRT_TelepulesekHistory_ID_VER] ON [dbo].[KRT_TelepulesekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TartomanyokHistory]') AND name = N'IX_KRT_TartomanyokHistory_ID_VER')
DROP INDEX [IX_KRT_TartomanyokHistory_ID_VER] ON [dbo].[KRT_TartomanyokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tartomanyok_SzervezetekHistory]') AND name = N'IX_KRT_Tartomanyok_SzervezetekHistory_ID_VER')
DROP INDEX [IX_KRT_Tartomanyok_SzervezetekHistory_ID_VER] ON [dbo].[KRT_Tartomanyok_SzervezetekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_SzerepkorokHistory]') AND name = N'IX_KRT_SzerepkorokHistory_ID_VER')
DROP INDEX [IX_KRT_SzerepkorokHistory_ID_VER] ON [dbo].[KRT_SzerepkorokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_FunkcioHistory]') AND name = N'IX_KRT_Szerepkor_FunkcioHistory_ID_VER')
DROP INDEX [IX_KRT_Szerepkor_FunkcioHistory_ID_VER] ON [dbo].[KRT_Szerepkor_FunkcioHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_SzemelyekHistory]') AND name = N'IX_KRT_SzemelyekHistory_ID_VER')
DROP INDEX [IX_KRT_SzemelyekHistory_ID_VER] ON [dbo].[KRT_SzemelyekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavokHistory]') AND name = N'IX_KRT_RagszamSavokHistory_ID_VER')
DROP INDEX [IX_KRT_RagszamSavokHistory_ID_VER] ON [dbo].[KRT_RagszamSavokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagSzamokHistory]') AND name = N'IX_KRT_RagSzamokHistory_ID_VER')
DROP INDEX [IX_KRT_RagSzamokHistory_ID_VER] ON [dbo].[KRT_RagSzamokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesekHistory]') AND name = N'IX_KRT_PartnerMinositesekHistory_ID_VER')
DROP INDEX [IX_KRT_PartnerMinositesekHistory_ID_VER] ON [dbo].[KRT_PartnerMinositesekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatokHistory]') AND name = N'IX_KRT_PartnerKapcsolatokHistory_ID_VER')
DROP INDEX [IX_KRT_PartnerKapcsolatokHistory_ID_VER] ON [dbo].[KRT_PartnerKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerekHistory]') AND name = N'IX_KRT_PartnerekHistory_ID_VER')
DROP INDEX [IX_KRT_PartnerekHistory_ID_VER] ON [dbo].[KRT_PartnerekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerCimekHistory]') AND name = N'IX_KRT_PartnerCimekHistory_ID_VER')
DROP INDEX [IX_KRT_PartnerCimekHistory_ID_VER] ON [dbo].[KRT_PartnerCimekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ParameterekHistory]') AND name = N'IX_KRT_ParameterekHistory_ID_VER')
DROP INDEX [IX_KRT_ParameterekHistory_ID_VER] ON [dbo].[KRT_ParameterekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_OrszagokHistory]') AND name = N'IX_KRT_OrszagokHistory_ID_VER')
DROP INDEX [IX_KRT_OrszagokHistory_ID_VER] ON [dbo].[KRT_OrszagokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_OrgokHistory]') AND name = N'IX_KRT_OrgokHistory_ID_VER')
DROP INDEX [IX_KRT_OrgokHistory_ID_VER] ON [dbo].[KRT_OrgokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusokHistory]') AND name = N'IX_KRT_ObjTipusokHistory_ID_VER')
DROP INDEX [IX_KRT_ObjTipusokHistory_ID_VER] ON [dbo].[KRT_ObjTipusokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_NezetekHistory]') AND name = N'IX_KRT_NezetekHistory_ID_VER')
DROP INDEX [IX_KRT_NezetekHistory_ID_VER] ON [dbo].[KRT_NezetekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MuveletekHistory]') AND name = N'IX_KRT_MuveletekHistory_ID_VER')
DROP INDEX [IX_KRT_MuveletekHistory_ID_VER] ON [dbo].[KRT_MuveletekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ModulokHistory]') AND name = N'IX_KRT_ModulokHistory_ID_VER')
DROP INDEX [IX_KRT_ModulokHistory_ID_VER] ON [dbo].[KRT_ModulokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Modul_FunkcioHistory]') AND name = N'IX_KRT_Modul_FunkcioHistory_ID_VER')
DROP INDEX [IX_KRT_Modul_FunkcioHistory_ID_VER] ON [dbo].[KRT_Modul_FunkcioHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MenukHistory]') AND name = N'IX_KRT_MenukHistory_ID_VER')
DROP INDEX [IX_KRT_MenukHistory_ID_VER] ON [dbo].[KRT_MenukHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaUtakHistory]') AND name = N'IX_KRT_MappaUtakHistory_ID_VER')
DROP INDEX [IX_KRT_MappaUtakHistory_ID_VER] ON [dbo].[KRT_MappaUtakHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaTartalmakHistory]') AND name = N'IX_KRT_MappaTartalmakHistory_ID_VER')
DROP INDEX [IX_KRT_MappaTartalmakHistory_ID_VER] ON [dbo].[KRT_MappaTartalmakHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappakHistory]') AND name = N'IX_KRT_MappakHistory_ID_VER')
DROP INDEX [IX_KRT_MappakHistory_ID_VER] ON [dbo].[KRT_MappakHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KozteruletTipusokHistory]') AND name = N'IX_KRT_KozteruletTipusokHistory_ID_VER')
DROP INDEX [IX_KRT_KozteruletTipusokHistory_ID_VER] ON [dbo].[KRT_KozteruletTipusokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KozteruletekHistory]') AND name = N'IX_KRT_KozteruletekHistory_ID_VER')
DROP INDEX [IX_KRT_KozteruletekHistory_ID_VER] ON [dbo].[KRT_KozteruletekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodTarakHistory]') AND name = N'IX_KRT_KodTarakHistory_ID_VER')
DROP INDEX [IX_KRT_KodTarakHistory_ID_VER] ON [dbo].[KRT_KodTarakHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodCsoportokHistory]') AND name = N'IX_KRT_KodCsoportokHistory_ID_VER')
DROP INDEX [IX_KRT_KodCsoportokHistory_ID_VER] ON [dbo].[KRT_KodCsoportokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_HelyettesitesekHistory]') AND name = N'IX_KRT_HelyettesitesekHistory_ID_VER')
DROP INDEX [IX_KRT_HelyettesitesekHistory_ID_VER] ON [dbo].[KRT_HelyettesitesekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioListaHistory]') AND name = N'IX_KRT_FunkcioListaHistory_ID_VER')
DROP INDEX [IX_KRT_FunkcioListaHistory_ID_VER] ON [dbo].[KRT_FunkcioListaHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FunkciokHistory]') AND name = N'IX_KRT_FunkciokHistory_ID_VER')
DROP INDEX [IX_KRT_FunkciokHistory_ID_VER] ON [dbo].[KRT_FunkciokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloProfilokHistory]') AND name = N'IX_KRT_FelhasznaloProfilokHistory_ID_VER')
DROP INDEX [IX_KRT_FelhasznaloProfilokHistory_ID_VER] ON [dbo].[KRT_FelhasznaloProfilokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznalokHistory]') AND name = N'IX_KRT_FelhasznalokHistory_ID_VER')
DROP INDEX [IX_KRT_FelhasznalokHistory_ID_VER] ON [dbo].[KRT_FelhasznalokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_SzerepkorHistory]') AND name = N'IX_KRT_Felhasznalo_SzerepkorHistory_ID_VER')
DROP INDEX [IX_KRT_Felhasznalo_SzerepkorHistory_ID_VER] ON [dbo].[KRT_Felhasznalo_SzerepkorHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Extra_NapokHistory]') AND name = N'IX_KRT_Extra_NapokHistory_ID_VER')
DROP INDEX [IX_KRT_Extra_NapokHistory_ID_VER] ON [dbo].[KRT_Extra_NapokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Erintett_TablakHistory]') AND name = N'IX_KRT_Erintett_TablakHistory_ID_VER')
DROP INDEX [IX_KRT_Erintett_TablakHistory_ID_VER] ON [dbo].[KRT_Erintett_TablakHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumokHistory]') AND name = N'IX_KRT_DokumentumokHistory_ID_VER')
DROP INDEX [IX_KRT_DokumentumokHistory_ID_VER] ON [dbo].[KRT_DokumentumokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumKapcsolatokHistory]') AND name = N'IX_KRT_DokumentumKapcsolatokHistory_ID_VER')
DROP INDEX [IX_KRT_DokumentumKapcsolatokHistory_ID_VER] ON [dbo].[KRT_DokumentumKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumAlairasokHistory]') AND name = N'IX_KRT_DokumentumAlairasokHistory_ID_VER')
DROP INDEX [IX_KRT_DokumentumAlairasokHistory_ID_VER] ON [dbo].[KRT_DokumentumAlairasokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokuAlairasKapcsolatokHistory]') AND name = N'IX_KRT_DokuAlairasKapcsolatokHistory_ID_VER')
DROP INDEX [IX_KRT_DokuAlairasKapcsolatokHistory_ID_VER] ON [dbo].[KRT_DokuAlairasKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagokHistory]') AND name = N'IX_KRT_CsoportTagokHistory_ID_VER')
DROP INDEX [IX_KRT_CsoportTagokHistory_ID_VER] ON [dbo].[KRT_CsoportTagokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CsoportokHistory]') AND name = N'IX_KRT_CsoportokHistory_ID_VER')
DROP INDEX [IX_KRT_CsoportokHistory_ID_VER] ON [dbo].[KRT_CsoportokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CimekHistory]') AND name = N'IX_KRT_CimekHistory_ID_VER')
DROP INDEX [IX_KRT_CimekHistory_ID_VER] ON [dbo].[KRT_CimekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodSavokHistory]') AND name = N'IX_KRT_BarkodSavokHistory_ID_VER')
DROP INDEX [IX_KRT_BarkodSavokHistory_ID_VER] ON [dbo].[KRT_BarkodSavokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodokHistory]') AND name = N'IX_KRT_BarkodokHistory_ID_VER')
DROP INDEX [IX_KRT_BarkodokHistory_ID_VER] ON [dbo].[KRT_BarkodokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BankszamlaszamokHistory]') AND name = N'IX_KRT_BankszamlaszamokHistory_ID_VER')
DROP INDEX [IX_KRT_BankszamlaszamokHistory_ID_VER] ON [dbo].[KRT_BankszamlaszamokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_AlkalmazasokHistory]') AND name = N'IX_KRT_AlkalmazasokHistory_ID_VER')
DROP INDEX [IX_KRT_AlkalmazasokHistory_ID_VER] ON [dbo].[KRT_AlkalmazasokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_AlairtDokumentumokHistory]') AND name = N'IX_KRT_AlairtDokumentumokHistory_ID_VER')
DROP INDEX [IX_KRT_AlairtDokumentumokHistory_ID_VER] ON [dbo].[KRT_AlairtDokumentumokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratokHistory]') AND name = N'IX_EREC_UgyUgyiratokHistory_ID_VER')
DROP INDEX [IX_EREC_UgyUgyiratokHistory_ID_VER] ON [dbo].[EREC_UgyUgyiratokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabokHistory]') AND name = N'IX_EREC_UgyUgyiratdarabokHistory_ID_VER')
DROP INDEX [IX_EREC_UgyUgyiratdarabokHistory_ID_VER] ON [dbo].[EREC_UgyUgyiratdarabokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyKezFeljegyzesekHistory]') AND name = N'IX_EREC_UgyKezFeljegyzesekHistory_ID_VER')
DROP INDEX [IX_EREC_UgyKezFeljegyzesekHistory_ID_VER] ON [dbo].[EREC_UgyKezFeljegyzesekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratObjKapcsolatokHistory]') AND name = N'IX_EREC_UgyiratObjKapcsolatokHistory_ID_VER')
DROP INDEX [IX_EREC_UgyiratObjKapcsolatokHistory_ID_VER] ON [dbo].[EREC_UgyiratObjKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatokHistory]') AND name = N'IX_EREC_UgyiratKapcsolatokHistory_ID_VER')
DROP INDEX [IX_EREC_UgyiratKapcsolatokHistory_ID_VER] ON [dbo].[EREC_UgyiratKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TargySzavakHistory]') AND name = N'IX_EREC_TargySzavakHistory_ID_VER')
DROP INDEX [IX_EREC_TargySzavakHistory_ID_VER] ON [dbo].[EREC_TargySzavakHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekekHistory]') AND name = N'IX_EREC_SzignalasiJegyzekekHistory_ID_VER')
DROP INDEX [IX_EREC_SzignalasiJegyzekekHistory_ID_VER] ON [dbo].[EREC_SzignalasiJegyzekekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_SzamlakHistory]') AND name = N'IX_EREC_SzamlakHistory_ID_VER')
DROP INDEX [IX_EREC_SzamlakHistory_ID_VER] ON [dbo].[EREC_SzamlakHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldKapjakMegHistory]') AND name = N'IX_EREC_PldKapjakMegHistory_ID_VER')
DROP INDEX [IX_EREC_PldKapjakMegHistory_ID_VER] ON [dbo].[EREC_PldKapjakMegHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyokHistory]') AND name = N'IX_EREC_PldIratPeldanyokHistory_ID_VER')
DROP INDEX [IX_EREC_PldIratPeldanyokHistory_ID_VER] ON [dbo].[EREC_PldIratPeldanyokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_ObjektumTargyszavaiHistory]') AND name = N'IX_EREC_ObjektumTargyszavaiHistory_ID_VER')
DROP INDEX [IX_EREC_ObjektumTargyszavaiHistory_ID_VER] ON [dbo].[EREC_ObjektumTargyszavaiHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaXmlHistory]') AND name = N'IX_EREC_Obj_MetaXmlHistory_ID_VER')
DROP INDEX [IX_EREC_Obj_MetaXmlHistory_ID_VER] ON [dbo].[EREC_Obj_MetaXmlHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicioHistory]') AND name = N'IX_EREC_Obj_MetaDefinicioHistory_ID_VER')
DROP INDEX [IX_EREC_Obj_MetaDefinicioHistory_ID_VER] ON [dbo].[EREC_Obj_MetaDefinicioHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdataiHistory]') AND name = N'IX_EREC_Obj_MetaAdataiHistory_ID_VER')
DROP INDEX [IX_EREC_Obj_MetaAdataiHistory_ID_VER] ON [dbo].[EREC_Obj_MetaAdataiHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_MellekletekHistory]') AND name = N'IX_EREC_MellekletekHistory_ID_VER')
DROP INDEX [IX_EREC_MellekletekHistory_ID_VER] ON [dbo].[EREC_MellekletekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldTertivevenyekHistory]') AND name = N'IX_EREC_KuldTertivevenyekHistory_ID_VER')
DROP INDEX [IX_EREC_KuldTertivevenyekHistory_ID_VER] ON [dbo].[EREC_KuldTertivevenyekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldMellekletekHistory]') AND name = N'IX_EREC_KuldMellekletekHistory_ID_VER')
DROP INDEX [IX_EREC_KuldMellekletekHistory_ID_VER] ON [dbo].[EREC_KuldMellekletekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyekHistory]') AND name = N'IX_EREC_KuldKuldemenyekHistory_ID_VER')
DROP INDEX [IX_EREC_KuldKuldemenyekHistory_ID_VER] ON [dbo].[EREC_KuldKuldemenyekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKezFeljegyzesekHistory]') AND name = N'IX_EREC_KuldKezFeljegyzesekHistory_ID_VER')
DROP INDEX [IX_EREC_KuldKezFeljegyzesekHistory_ID_VER] ON [dbo].[EREC_KuldKezFeljegyzesekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatokHistory]') AND name = N'IX_EREC_KuldKapcsolatokHistory_ID_VER')
DROP INDEX [IX_EREC_KuldKapcsolatokHistory_ID_VER] ON [dbo].[EREC_KuldKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyaiHistory]') AND name = N'IX_EREC_Kuldemeny_IratPeldanyaiHistory_ID_VER')
DROP INDEX [IX_EREC_Kuldemeny_IratPeldanyaiHistory_ID_VER] ON [dbo].[EREC_Kuldemeny_IratPeldanyaiHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldDokumentumokHistory]') AND name = N'IX_EREC_KuldDokumentumokHistory_ID_VER')
DROP INDEX [IX_EREC_KuldDokumentumokHistory_ID_VER] ON [dbo].[EREC_KuldDokumentumokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldokHistory]') AND name = N'IX_EREC_KuldBekuldokHistory_ID_VER')
DROP INDEX [IX_EREC_KuldBekuldokHistory_ID_VER] ON [dbo].[EREC_KuldBekuldokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_IktatokonyvHistory]') AND name = N'IX_EREC_IrattariTetel_IktatokonyvHistory_ID_VER')
DROP INDEX [IX_EREC_IrattariTetel_IktatokonyvHistory_ID_VER] ON [dbo].[EREC_IrattariTetel_IktatokonyvHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariKikeroHistory]') AND name = N'IX_EREC_IrattariKikeroHistory_ID_VER')
DROP INDEX [IX_EREC_IrattariKikeroHistory_ID_VER] ON [dbo].[EREC_IrattariKikeroHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariHelyekHistory]') AND name = N'IX_EREC_IrattariHelyekHistory_ID_VER')
DROP INDEX [IX_EREC_IrattariHelyekHistory_ID_VER] ON [dbo].[EREC_IrattariHelyekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMetaDefinicioHistory]') AND name = N'IX_EREC_IratMetaDefinicioHistory_ID_VER')
DROP INDEX [IX_EREC_IratMetaDefinicioHistory_ID_VER] ON [dbo].[EREC_IratMetaDefinicioHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMellekletekHistory]') AND name = N'IX_EREC_IratMellekletekHistory_ID_VER')
DROP INDEX [IX_EREC_IratMellekletekHistory_ID_VER] ON [dbo].[EREC_IratMellekletekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatokHistory]') AND name = N'IX_EREC_IratKapcsolatokHistory_ID_VER')
DROP INDEX [IX_EREC_IratKapcsolatokHistory_ID_VER] ON [dbo].[EREC_IratKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratelemKapcsolatokHistory]') AND name = N'IX_EREC_IratelemKapcsolatokHistory_ID_VER')
DROP INDEX [IX_EREC_IratelemKapcsolatokHistory_ID_VER] ON [dbo].[EREC_IratelemKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratAlairokHistory]') AND name = N'IX_EREC_IratAlairokHistory_ID_VER')
DROP INDEX [IX_EREC_IratAlairokHistory_ID_VER] ON [dbo].[EREC_IratAlairokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Irat_IktatokonyveiHistory]') AND name = N'IX_EREC_Irat_IktatokonyveiHistory_ID_VER')
DROP INDEX [IX_EREC_Irat_IktatokonyveiHistory_ID_VER] ON [dbo].[EREC_Irat_IktatokonyveiHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraOnkormAdatokHistory]') AND name = N'IX_EREC_IraOnkormAdatokHistory_ID_VER')
DROP INDEX [IX_EREC_IraOnkormAdatokHistory_ID_VER] ON [dbo].[EREC_IraOnkormAdatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezFeljegyzesekHistory]') AND name = N'IX_EREC_IraKezFeljegyzesekHistory_ID_VER')
DROP INDEX [IX_EREC_IraKezFeljegyzesekHistory_ID_VER] ON [dbo].[EREC_IraKezFeljegyzesekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelekHistory]') AND name = N'IX_EREC_IraKezbesitesiTetelekHistory_ID_VER')
DROP INDEX [IX_EREC_IraKezbesitesiTetelekHistory_ID_VER] ON [dbo].[EREC_IraKezbesitesiTetelekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiFejekHistory]') AND name = N'IX_EREC_IraKezbesitesiFejekHistory_ID_VER')
DROP INDEX [IX_EREC_IraKezbesitesiFejekHistory_ID_VER] ON [dbo].[EREC_IraKezbesitesiFejekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekTetelekHistory]') AND name = N'IX_EREC_IraJegyzekTetelekHistory_ID_VER')
DROP INDEX [IX_EREC_IraJegyzekTetelekHistory_ID_VER] ON [dbo].[EREC_IraJegyzekTetelekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekekHistory]') AND name = N'IX_EREC_IraJegyzekekHistory_ID_VER')
DROP INDEX [IX_EREC_IraJegyzekekHistory_ID_VER] ON [dbo].[EREC_IraJegyzekekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIrattariTetelekHistory]') AND name = N'IX_EREC_IraIrattariTetelekHistory_ID_VER')
DROP INDEX [IX_EREC_IraIrattariTetelekHistory_ID_VER] ON [dbo].[EREC_IraIrattariTetelekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokHistory]') AND name = N'IX_EREC_IraIratokHistory_ID_VER')
DROP INDEX [IX_EREC_IraIratokHistory_ID_VER] ON [dbo].[EREC_IraIratokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokDokumentumokHistory]') AND name = N'IX_EREC_IraIratokDokumentumokHistory_ID_VER')
DROP INDEX [IX_EREC_IraIratokDokumentumokHistory_ID_VER] ON [dbo].[EREC_IraIratokDokumentumokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIktatoKonyvekHistory]') AND name = N'IX_EREC_IraIktatoKonyvekHistory_ID_VER')
DROP INDEX [IX_EREC_IraIktatoKonyvekHistory_ID_VER] ON [dbo].[EREC_IraIktatoKonyvekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelekHistory]') AND name = N'IX_EREC_IraElosztoivTetelekHistory_ID_VER')
DROP INDEX [IX_EREC_IraElosztoivTetelekHistory_ID_VER] ON [dbo].[EREC_IraElosztoivTetelekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivekHistory]') AND name = N'IX_EREC_IraElosztoivekHistory_ID_VER')
DROP INDEX [IX_EREC_IraElosztoivekHistory_ID_VER] ON [dbo].[EREC_IraElosztoivekHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatokHistory]') AND name = N'IX_EREC_HataridosFeladatokHistory_ID_VER')
DROP INDEX [IX_EREC_HataridosFeladatokHistory_ID_VER] ON [dbo].[EREC_HataridosFeladatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Hatarid_ObjektumokHistory]') AND name = N'IX_EREC_Hatarid_ObjektumokHistory_ID_VER')
DROP INDEX [IX_EREC_Hatarid_ObjektumokHistory_ID_VER] ON [dbo].[EREC_Hatarid_ObjektumokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_FeladatDefinicioHistory]') AND name = N'IX_EREC_FeladatDefinicioHistory_ID_VER')
DROP INDEX [IX_EREC_FeladatDefinicioHistory_ID_VER] ON [dbo].[EREC_FeladatDefinicioHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailFiokokHistory]') AND name = N'IX_EREC_eMailFiokokHistory_ID_VER')
DROP INDEX [IX_EREC_eMailFiokokHistory_ID_VER] ON [dbo].[EREC_eMailFiokokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekokHistory]') AND name = N'IX_EREC_eMailBoritekokHistory_ID_VER')
DROP INDEX [IX_EREC_eMailBoritekokHistory_ID_VER] ON [dbo].[EREC_eMailBoritekokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCsatolmanyokHistory]') AND name = N'IX_EREC_eMailBoritekCsatolmanyokHistory_ID_VER')
DROP INDEX [IX_EREC_eMailBoritekCsatolmanyokHistory_ID_VER] ON [dbo].[EREC_eMailBoritekCsatolmanyokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimeiHistory]') AND name = N'IX_EREC_eMailBoritekCimeiHistory_ID_VER')
DROP INDEX [IX_EREC_eMailBoritekCimeiHistory_ID_VER] ON [dbo].[EREC_eMailBoritekCimeiHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_CsatolmanyokHistory]') AND name = N'IX_EREC_CsatolmanyokHistory_ID_VER')
DROP INDEX [IX_EREC_CsatolmanyokHistory_ID_VER] ON [dbo].[EREC_CsatolmanyokHistory]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_AgazatiJelekHistory]') AND name = N'IX_EREC_AgazatiJelekHistory_ID_VER')
DROP INDEX [IX_EREC_AgazatiJelekHistory_ID_VER] ON [dbo].[EREC_AgazatiJelekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_VallalkozasokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_VallalkozasokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TranzakciokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_TranzakciokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tranz_ObjHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Tranz_ObjHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManagerHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_TemplateManagerHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TelepulesekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_TelepulesekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TartomanyokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_TartomanyokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tartomanyok_SzervezetekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Tartomanyok_SzervezetekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_SzerepkorokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_SzerepkorokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_FunkcioHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Szerepkor_FunkcioHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_SzemelyekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_SzemelyekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_RagszamSavokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagSzamokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_RagSzamokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_PartnerMinositesekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_PartnerKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_PartnerekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerCimekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_PartnerCimekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ParameterekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_ParameterekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_OrszagokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_OrszagokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_OrgokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_OrgokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_ObjTipusokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_NezetekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_NezetekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MuveletekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_MuveletekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ModulokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_ModulokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Modul_FunkcioHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Modul_FunkcioHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MenukHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_MenukHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaUtakHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_MappaUtakHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaTartalmakHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_MappaTartalmakHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappakHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_MappakHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KozteruletTipusokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_KozteruletTipusokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KozteruletekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_KozteruletekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodTarakHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_KodTarakHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodCsoportokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_KodCsoportokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_HelyettesitesekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_HelyettesitesekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Halozati_NyomtatokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Halozati_NyomtatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioListaHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_FunkcioListaHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FunkciokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_FunkciokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloProfilokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_FelhasznaloProfilokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznalokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_FelhasznalokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_SzerepkorHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Felhasznalo_SzerepkorHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Extra_NapokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Extra_NapokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Erintett_TablakHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Erintett_TablakHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_DokumentumokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumKapcsolatokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_DokumentumKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumAlairasokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_DokumentumAlairasokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokuAlairasKapcsolatokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_DokuAlairasKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_CsoportTagokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CsoportokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_CsoportokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CimekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_CimekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodSavokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_BarkodSavokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_BarkodokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BankszamlaszamokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_BankszamlaszamokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_AlkalmazasokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_AlkalmazasokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_AlairtDokumentumokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_AlairtDokumentumokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_UgyUgyiratokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_UgyUgyiratdarabokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyKezFeljegyzesekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_UgyKezFeljegyzesekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratObjKapcsolatokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_UgyiratObjKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_UgyiratKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TargySzavakHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_TargySzavakHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_SzignalasiJegyzekekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_SzamlakHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_SzamlakHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldKapjakMegHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_PldKapjakMegHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_PldIratPeldanyokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_ObjektumTargyszavaiHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_ObjektumTargyszavaiHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaXmlHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_Obj_MetaXmlHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicioHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_Obj_MetaDefinicioHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdataiHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_Obj_MetaAdataiHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_MellekletekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_MellekletekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldTertivevenyekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_KuldTertivevenyekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldMellekletekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_KuldMellekletekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_KuldKuldemenyekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKezFeljegyzesekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_KuldKezFeljegyzesekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_KuldKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyaiHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_Kuldemeny_IratPeldanyaiHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldDokumentumokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_KuldDokumentumokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_KuldBekuldokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_IktatokonyvHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IrattariTetel_IktatokonyvHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariKikeroHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IrattariKikeroHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariHelyekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IrattariHelyekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMetaDefinicioHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IratMetaDefinicioHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMellekletekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IratMellekletekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IratKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratelemKapcsolatokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IratelemKapcsolatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratAlairokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IratAlairokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Irat_IktatokonyveiHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_Irat_IktatokonyveiHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraOnkormAdatokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraOnkormAdatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezFeljegyzesekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraKezFeljegyzesekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraKezbesitesiTetelekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiFejekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraKezbesitesiFejekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekTetelekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraJegyzekTetelekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraJegyzekekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIrattariTetelekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraIrattariTetelekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraIratokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokDokumentumokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraIratokDokumentumokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIktatoKonyvekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraIktatoKonyvekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraElosztoivTetelekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraElosztoivekHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_HataridosFeladatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Hatarid_ObjektumokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_Hatarid_ObjektumokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_FeladatDefinicioHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_FeladatDefinicioHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailFiokokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_eMailFiokokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_eMailBoritekokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCsatolmanyokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_eMailBoritekCsatolmanyokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimeiHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_eMailBoritekCimeiHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_CsatolmanyokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_CsatolmanyokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_AgazatiJelekHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_AgazatiJelekHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_AgazatiJelekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_AgazatiJelekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_AgazatiJelekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Kod] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[AgazatiJel_Id] [uniqueidentifier] NULL,
	[AgazatiJel_Id_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_AgazatiJelekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_CsatolmanyokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_CsatolmanyokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_CsatolmanyokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[IraIrat_Id] [uniqueidentifier] NULL,
	[Dokumentum_Id] [uniqueidentifier] NULL,
	[Leiras] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Lapszam] [int] NULL,
	[KapcsolatJelleg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[DokumentumSzerep] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[IratAlairoJel] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_CsatolmanyokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimeiHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_eMailBoritekCimeiHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_eMailBoritekCimeiHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[eMailBoritek_Id] [uniqueidentifier] NULL,
	[eMailBoritek_Id_Ver] [int] NULL,
	[Sorszam] [int] NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[MailCim] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Partner_Id] [uniqueidentifier] NULL,
	[Partner_Id_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_eMailBoritekCimeiHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCsatolmanyokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_eMailBoritekCsatolmanyokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_eMailBoritekCsatolmanyokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[eMailBoritek_Id] [uniqueidentifier] NULL,
	[eMailBoritek_Id_Ver] [int] NULL,
	[Dokumentum_Id] [uniqueidentifier] NULL,
	[Dokumentum_Id_Ver] [int] NULL,
	[Nev] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Tomoritve] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_eMailBoritekCsatolmanyokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_eMailBoritekokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_eMailBoritekokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Felado] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Cimzett] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Targy] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[FeladasDatuma] [datetime] NULL,
	[ErkezesDatuma] [datetime] NULL,
	[Fontossag] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[IraIrat_Id] [uniqueidentifier] NULL,
	[DigitalisAlairas] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Uzenet] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[EmailForras] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[EmailGuid] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[FeldolgozasIdo] [datetime] NULL,
	[ForrasTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[CC] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_eMailBoritekokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailFiokokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_eMailFiokokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UserNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Csoportok_Id] [uniqueidentifier] NULL,
	[Jelszo] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[NapiMax] [int] NULL,
	[NapiTeny] [int] NULL,
	[EmailCim] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Modul_Id] [uniqueidentifier] NULL,
	[MappaNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_eMailFiokokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_FeladatDefinicioHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_FeladatDefinicioHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Funkcio_Id_Kivalto] [uniqueidentifier] NULL,
	[Obj_Metadefinicio_Id] [uniqueidentifier] NULL,
	[ObjStateValue_Id] [uniqueidentifier] NULL,
	[FeladatDefinicioTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FeladatSorszam] [int] NULL,
	[MuveletDefinicio] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[SpecMuveletDefinicio] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Funkcio_Id_Inditando] [uniqueidentifier] NULL,
	[Funkcio_Id_Futtatando] [uniqueidentifier] NULL,
	[BazisObjLeiro] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[FelelosTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Obj_Id_Felelos] [uniqueidentifier] NULL,
	[FelelosLeiro] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Idobazis] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[ObjTip_Id_DateCol] [uniqueidentifier] NULL,
	[AtfutasiIdo] [int] NULL,
	[Idoegyseg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FeladatLeiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Prioritas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[LezarasPrioritas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Obj_SzukitoFeltetel] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_FeladatDefinicioHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Hatarid_ObjektumokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_Hatarid_ObjektumokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[HataridosFeladat_Id] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_Hatarid_ObjektumokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_HataridosFeladatokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_HataridosFeladatokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Csoport_id_felelos] [uniqueidentifier] NULL,
	[Felhasznalo_Id_kiado] [uniqueidentifier] NULL,
	[HataridosFeladat_Id] [uniqueidentifier] NULL,
	[Forras] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Prioritas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[KezdesiIdo] [datetime] NULL,
	[IntezkHatarido] [datetime] NULL,
	[LezarasDatuma] [datetime] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[Funkcio_Id_Inditando] [uniqueidentifier] NULL,
	[Esemeny_Id_Kivalto] [uniqueidentifier] NULL,
	[Esemeny_Id_Lezaro] [uniqueidentifier] NULL,
	[Felhasznalo_Id_Felelos] [uniqueidentifier] NULL,
	[HataridosFeladat_Id_Fo] [uniqueidentifier] NULL,
	[ReszfeladatSorszam] [int] NULL,
	[FeladatDefinicio_Id] [uniqueidentifier] NULL,
	[FeladatDefinicio_Id_Lezaro] [uniqueidentifier] NULL,
	[LezarasPrioritas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Altipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Megoldas] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id_Kiado] [uniqueidentifier] NULL,
	[Memo] [char](1) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_HataridosFeladatokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraElosztoivekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Ev] [int] NULL,
	[Fajta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Kod] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[NEV] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Hasznalat] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NULL,
	[Csoport_Id_Tulaj_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IraElosztoivekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraElosztoivTetelekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[ElosztoIv_Id] [uniqueidentifier] NULL,
	[ElosztoIv_Id_Ver] [int] NULL,
	[Sorszam] [int] NULL,
	[Partner_Id] [uniqueidentifier] NULL,
	[Partner_Id_Ver] [int] NULL,
	[Cim_Id] [uniqueidentifier] NULL,
	[CimSTR] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[NevSTR] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Kuldesmod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Visszavarolag] [char](1) COLLATE Hungarian_CI_AS NULL,
	[VisszavarasiIdo] [datetime] NULL,
	[Vissza_Nap_Mulva] [int] NULL,
	[AlairoSzerep] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IraElosztoivTetelekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIktatoKonyvekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraIktatoKonyvekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_IraIktatoKonyvekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Ev] [int] NULL,
	[Azonosito] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[DefaultIrattariTetelszam] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[MegkulJelzes] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Iktatohely] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KozpontiIktatasJelzo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[UtolsoFoszam] [int] NULL,
	[Csoport_Id_Olvaso] [uniqueidentifier] NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NULL,
	[IktSzamOsztas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FormatumKod] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Titkos] [char](1) COLLATE Hungarian_CI_AS NULL,
	[IktatoErkezteto] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[LezarasDatuma] [datetime] NULL,
	[PostakonyvVevokod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[PostakonyvMegallapodasAzon] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UtolsoSorszam] [int] NULL,
	[EFeladojegyzekUgyfelAdatok] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Statusz] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Terjedelem] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[SelejtezesDatuma] [datetime] NULL,
	[KezelesTipusa] [char](1) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_IraIktatoKonyvekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokDokumentumokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraIratokDokumentumokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Dokumentum_Id] [uniqueidentifier] NULL,
	[Dokumentum_Id_Ver] [int] NULL,
	[IraIrat_Id] [uniqueidentifier] NULL,
	[IraIrat_Id_Ver] [int] NULL,
	[Leiras] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Lapszam] [int] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Forras] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Formatum] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Vonalkodozas] [char](1) COLLATE Hungarian_CI_AS NULL,
	[DokumentumSzerep] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IraIratokDokumentumokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraIratokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_IraIratokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[PostazasIranya] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Alszam] [int] NULL,
	[UtolsoSorszam] [int] NULL,
	[UgyUgyIratDarab_Id] [uniqueidentifier] NULL,
	[UgyUgyIratDarab_Id_Ver] [int] NULL,
	[Kategoria] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[HivatkozasiSzam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[IktatasDatuma] [datetime] NULL,
	[ExpedialasDatuma] [datetime] NULL,
	[ExpedialasModja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FelhasznaloCsoport_Id_Expedial] [uniqueidentifier] NULL,
	[Targy] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Jelleg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[FelhasznaloCsoport_Id_Iktato] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Kiadmany] [uniqueidentifier] NULL,
	[UgyintezesAlapja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[Csoport_Id_Ugyfelelos] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Orzo] [uniqueidentifier] NULL,
	[KiadmanyozniKell] [char](1) COLLATE Hungarian_CI_AS NULL,
	[FelhasznaloCsoport_Id_Ugyintez] [uniqueidentifier] NULL,
	[MegorzesiIdo] [datetime] NULL,
	[IratFajta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Irattipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[KuldKuldemenyek_Id] [uniqueidentifier] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[GeneraltTargy] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[IratMetaDef_Id] [uniqueidentifier] NULL,
	[IrattarbaKuldDatuma] [datetime] NULL,
	[IrattarbaVetelDat] [datetime] NULL,
	[Ugyirat_Id] [uniqueidentifier] NULL,
	[Minosites] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[Hatarido] [datetime] NULL,
	[AdathordozoTipusa] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Sorszam] [int] NULL,
	[Munkaallomas] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UgyintezesModja] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[IntezesIdopontja] [datetime] NULL,
	[AKTIV] [int] NULL,
 CONSTRAINT [PK_EREC_IraIratokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIrattariTetelekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraIrattariTetelekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_IraIrattariTetelekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[IrattariTetelszam] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[IrattariJel] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[MegorzesiIdo] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Idoegyseg] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Folyo_CM] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[AgazatiJel_Id] [uniqueidentifier] NULL,
	[AgazatiJel_Id_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[EvenTuliIktatas] [char](1) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_IraIrattariTetelekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraJegyzekekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Tipus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Minosites] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id_felelos] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Vegrehaj] [uniqueidentifier] NULL,
	[Partner_Id_LeveltariAtvevo] [uniqueidentifier] NULL,
	[LezarasDatuma] [datetime] NULL,
	[SztornirozasDat] [datetime] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[VegrehajtasDatuma] [datetime] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IraJegyzekekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekTetelekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraJegyzekTetelekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Jegyzek_Id] [uniqueidentifier] NULL,
	[Jegyzek_Id_Ver] [int] NULL,
	[Sorszam] [int] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SztornozasDat] [datetime] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IraJegyzekTetelekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiFejekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraKezbesitesiFejekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_IraKezbesitesiFejekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[CsomagAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[UtolsoNyomtatasIdo] [datetime] NULL,
	[UtolsoNyomtatas_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IraKezbesitesiFejekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraKezbesitesiTetelekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_IraKezbesitesiTetelekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[KezbesitesFej_Id] [uniqueidentifier] NULL,
	[AtveteliFej_Id] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AtadasDat] [datetime] NULL,
	[AtvetelDat] [datetime] NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[UtolsoNyomtatas_Id] [uniqueidentifier] NULL,
	[UtolsoNyomtatasIdo] [datetime] NULL,
	[Felhasznalo_Id_Atado_USER] [uniqueidentifier] NULL,
	[Felhasznalo_Id_Atado_LOGIN] [uniqueidentifier] NULL,
	[Csoport_Id_Cel] [uniqueidentifier] NULL,
	[Felhasznalo_Id_AtvevoUser] [uniqueidentifier] NULL,
	[Felhasznalo_Id_AtvevoLogin] [uniqueidentifier] NULL,
	[Allapot] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Azonosito_szoveges] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UgyintezesModja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IraKezbesitesiTetelekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezFeljegyzesekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraKezFeljegyzesekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[IraIrat_Id] [uniqueidentifier] NULL,
	[IraIrat_Id_Ver] [int] NULL,
	[KezelesTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IraKezFeljegyzesekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraOnkormAdatokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraOnkormAdatokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_IraOnkormAdatokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[IraIratok_Id] [uniqueidentifier] NULL,
	[UgyFajtaja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[DontestHozta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[DontesFormaja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[UgyintezesHataridore] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[JogorvoslatiEljarasTipusa] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[JogorvoslatiDontesTipusa] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[JogorvoslatiDontestHozta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[JogorvoslatiDontesTartalma] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[HataridoTullepes] [int] NULL,
	[JogorvoslatiDontes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[HatosagiEllenorzes] [char](1) COLLATE Hungarian_CI_AS NULL,
	[MunkaorakSzama] [float] NULL,
	[EljarasiKoltseg] [int] NULL,
	[KozigazgatasiBirsagMerteke] [int] NULL,
	[SommasEljDontes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[NyolcNapBelulNemSommas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoHatalyuHatarozat] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoHatHatalybaLepes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoHatalyuVegzes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoVegzesHatalyba] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[HatAltalVisszafizOsszeg] [int] NULL,
	[HatTerheloEljKtsg] [int] NULL,
	[FelfuggHatarozat] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_IraOnkormAdatokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Irat_IktatokonyveiHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_Irat_IktatokonyveiHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_Irat_IktatokonyveiHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[IraIktatokonyv_Id] [uniqueidentifier] NULL,
	[IraIktatokonyv_Id_Ver] [int] NULL,
	[Csoport_Id_Iktathat] [uniqueidentifier] NULL,
	[Csoport_Id_Iktathat_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_Irat_IktatokonyveiHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratAlairokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IratAlairokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_IratAlairokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[PldIratPeldany_Id] [uniqueidentifier] NULL,
	[Objtip_Id] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[AlairasDatuma] [datetime] NULL,
	[Leiras] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[AlairoSzerep] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[AlairasSorrend] [int] NULL,
	[FelhasznaloCsoport_Id_Alairo] [uniqueidentifier] NULL,
	[FelhaszCsoport_Id_Helyettesito] [uniqueidentifier] NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AlairasMod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[AlairasSzabaly_Id] [uniqueidentifier] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IratAlairokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratelemKapcsolatokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IratelemKapcsolatokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Melleklet_Id] [uniqueidentifier] NULL,
	[Csatolmany_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IratelemKapcsolatokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IratKapcsolatokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[KapcsolatTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Kezi] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Irat_Irat_Beepul] [uniqueidentifier] NULL,
	[Irat_Irat_Beepul_Ver] [int] NULL,
	[Irat_Irat_Felepul] [uniqueidentifier] NULL,
	[Irat_Irat_Felepul_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IratKapcsolatokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMellekletekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IratMellekletekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[IraIrat_Id] [uniqueidentifier] NULL,
	[IraIrat_Id_Ver] [int] NULL,
	[AdathordozoTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[MennyisegiEgyseg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Mennyiseg] [int] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[KuldMellekletek_Id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IratMellekletekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMetaDefinicioHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IratMetaDefinicioHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_IratMetaDefinicioHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Ugykor_Id] [uniqueidentifier] NULL,
	[UgykorKod] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Ugytipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[UgytipusNev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[EljarasiSzakasz] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Irattipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[GeneraltTargy] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Rovidnev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UgyFajta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[UgyiratIntezesiIdo] [int] NULL,
	[Idoegyseg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[UgyiratIntezesiIdoKotott] [char](1) COLLATE Hungarian_CI_AS NULL,
	[UgyiratHataridoKitolas] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IratMetaDefinicioHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariHelyekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IrattariHelyekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Ertek] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Vonalkod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuloId] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[Felelos_Csoport_Id] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariKikeroHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IrattariKikeroHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Keres_note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[FelhasznalasiCel] [char](1) COLLATE Hungarian_CI_AS NULL,
	[DokumentumTipus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Indoklas_note] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[FelhasznaloCsoport_Id_Kikero] [uniqueidentifier] NULL,
	[KeresDatuma] [datetime] NULL,
	[KulsoAzonositok] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UgyUgyirat_Id] [uniqueidentifier] NULL,
	[UgyUgyirat_Id_Ver] [int] NULL,
	[Tertiveveny_Id] [uniqueidentifier] NULL,
	[Tertiveveny_Id_Ver] [int] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[KikerKezd] [datetime] NULL,
	[KikerVege] [datetime] NULL,
	[FelhasznaloCsoport_Id_Jovahagy] [uniqueidentifier] NULL,
	[JovagyasDatuma] [datetime] NULL,
	[FelhasznaloCsoport_Id_Kiado] [uniqueidentifier] NULL,
	[KiadasDatuma] [datetime] NULL,
	[FelhasznaloCsoport_Id_Visszaad] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Visszave] [uniqueidentifier] NULL,
	[VisszaadasDatuma] [datetime] NULL,
	[SztornirozasDat] [datetime] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Irattar_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IrattariKikeroHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_IktatokonyvHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IrattariTetel_IktatokonyvHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_IrattariTetel_IktatokonyvHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[IrattariTetel_Id] [uniqueidentifier] NULL,
	[IrattariTetel_Id_Ver] [int] NULL,
	[Iktatokonyv_Id] [uniqueidentifier] NULL,
	[Iktatokonyv_Id_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IrattariTetel_IktatokonyvHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_KuldBekuldokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id_Ver] [int] NULL,
	[Partner_Id_Bekuldo] [uniqueidentifier] NULL,
	[Partner_Id_Bekuldo_Ver] [int] NULL,
	[PartnerCim_Id_Bekuldo] [uniqueidentifier] NULL,
	[PartnerCim_Id_Bekuldo_Ver] [int] NULL,
	[NevSTR] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[CimSTR] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_KuldBekuldokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldDokumentumokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_KuldDokumentumokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Dokumentum_Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id_Ver] [int] NULL,
	[Leiras] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Lapszam] [int] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Forras] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Formatum] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Vonalkodozas] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[DokumentumSzerep] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_KuldDokumentumokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyaiHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_Kuldemeny_IratPeldanyaiHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_Kuldemeny_IratPeldanyaiHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id_Ver] [int] NULL,
	[Peldany_Id] [uniqueidentifier] NULL,
	[Peldany_Id_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_Kuldemeny_IratPeldanyaiHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_KuldKapcsolatokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[KapcsolatTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Kezi] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Kuld_Kuld_Beepul] [uniqueidentifier] NULL,
	[Kuld_Kuld_Beepul_Ver] [int] NULL,
	[Kuld_Kuld_Felepul] [uniqueidentifier] NULL,
	[Kuld_Kuld_Felepul_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_KuldKapcsolatokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKezFeljegyzesekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_KuldKezFeljegyzesekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id_Ver] [int] NULL,
	[KezelesTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_KuldKezFeljegyzesekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_KuldKuldemenyekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_KuldKuldemenyekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[BeerkezesIdeje] [datetime] NULL,
	[FelbontasDatuma] [datetime] NULL,
	[IraIktatokonyv_Id] [uniqueidentifier] NULL,
	[IraIktatokonyv_Id_Ver] [int] NULL,
	[KuldesMod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Erkezteto_Szam] [int] NULL,
	[HivatkozasiSzam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Targy] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Tartalom] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[RagSzam] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Surgosseg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[BelyegzoDatuma] [datetime] NULL,
	[UgyintezesModja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[PostazasIranya] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Tovabbito] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[PeldanySzam] [int] NULL,
	[IktatniKell] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Iktathato] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[KuldKuldemeny_Id_Szulo] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id_Szulo_Ver] [int] NULL,
	[Erkeztetes_Ev] [int] NULL,
	[Csoport_Id_Cimzett] [uniqueidentifier] NULL,
	[Csoport_Id_Cimzett_Ver] [int] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos_Ver] [int] NULL,
	[FelhasznaloCsoport_Id_Expedial] [uniqueidentifier] NULL,
	[ExpedialasIdeje] [datetime] NULL,
	[FelhasznaloCsoport_Id_Orzo] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Orzo_Ver] [int] NULL,
	[FelhasznaloCsoport_Id_Atvevo] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Atvevo_Ver] [int] NULL,
	[Partner_Id_Bekuldo] [uniqueidentifier] NULL,
	[Partner_Id_Bekuldo_Ver] [int] NULL,
	[Cim_Id] [uniqueidentifier] NULL,
	[Cim_Id_Ver] [int] NULL,
	[CimSTR_Bekuldo] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[NevSTR_Bekuldo] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[AdathordozoTipusa] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[ElsodlegesAdathordozoTipusa] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FelhasznaloCsoport_Id_Alairo] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Alairo_Ver] [int] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[FelhasznaloCsoport_Id_Bonto] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Bonto_Ver] [int] NULL,
	[CsoportFelelosEloszto_Id] [uniqueidentifier] NULL,
	[CsoportFelelosEloszto_Id_Ver] [int] NULL,
	[Csoport_Id_Felelos_Elozo] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos_Elozo_Ver] [int] NULL,
	[Kovetkezo_Felelos_Id] [uniqueidentifier] NULL,
	[Elektronikus_Kezbesitesi_Allap] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Kovetkezo_Orzo_Id] [uniqueidentifier] NULL,
	[Fizikai_Kezbesitesi_Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[IraIratok_Id] [uniqueidentifier] NULL,
	[BontasiMegjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[MegtagadasIndoka] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Megtagado_Id] [uniqueidentifier] NULL,
	[MegtagadasDat] [datetime] NULL,
	[KimenoKuldemenyFajta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Elsobbsegi] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ajanlott] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Tertiveveny] [char](1) COLLATE Hungarian_CI_AS NULL,
	[SajatKezbe] [char](1) COLLATE Hungarian_CI_AS NULL,
	[E_ertesites] [char](1) COLLATE Hungarian_CI_AS NULL,
	[E_elorejelzes] [char](1) COLLATE Hungarian_CI_AS NULL,
	[PostaiLezaroSzolgalat] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ar] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KimenoKuld_Sorszam] [int] NULL,
	[Ver] [int] NULL,
	[TovabbitasAlattAllapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[BoritoTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[MegorzesJelzo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Minosites] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[IktatastNemIgenyel] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KezbesitesModja] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Munkaallomas] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SerultKuldemeny] [char](1) COLLATE Hungarian_CI_AS NULL,
	[TevesCimzes] [char](1) COLLATE Hungarian_CI_AS NULL,
	[TevesErkeztetes] [char](1) COLLATE Hungarian_CI_AS NULL,
	[CimzesTipusa] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_KuldKuldemenyekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldMellekletekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_KuldMellekletekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id_Ver] [int] NULL,
	[Mennyiseg] [int] NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[AdathordozoTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[MennyisegiEgyseg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_KuldMellekletekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldTertivevenyekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_KuldTertivevenyekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Kuldemeny_Id] [uniqueidentifier] NULL,
	[Kuldemeny_Id_Ver] [int] NULL,
	[Ragszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[TertivisszaKod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[TertivisszaDat] [datetime] NULL,
	[AtvevoSzemely] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[AtvetelDat] [datetime] NULL,
	[KezbVelelemBeallta] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KezbVelelemDatuma] [datetime] NULL,
 CONSTRAINT [PK_EREC_KuldTertivevenyekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_MellekletekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_MellekletekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_MellekletekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[IraIrat_Id] [uniqueidentifier] NULL,
	[AdathordozoTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[MennyisegiEgyseg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Mennyiseg] [int] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[MellekletAdathordozoTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_MellekletekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdataiHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_Obj_MetaAdataiHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_Obj_MetaAdataiHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Obj_MetaDefinicio_Id] [uniqueidentifier] NULL,
	[Targyszavak_Id] [uniqueidentifier] NULL,
	[AlapertelmezettErtek] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Sorszam] [int] NULL,
	[Opcionalis] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ismetlodo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Funkcio] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SPSSzinkronizalt] [char](1) COLLATE Hungarian_CI_AS NULL,
	[SPS_Field_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_Id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_Id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_Id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_Id] [uniqueidentifier] NULL,
	[UIAccessLog_Id] [uniqueidentifier] NULL,
	[ControlTypeSource] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[ControlTypeDataSource] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_Obj_MetaAdataiHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicioHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_Obj_MetaDefinicioHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_Obj_MetaDefinicioHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Objtip_Id] [uniqueidentifier] NULL,
	[Objtip_Id_Column] [uniqueidentifier] NULL,
	[ColumnValue] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[DefinicioTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Felettes_Obj_Meta] [uniqueidentifier] NULL,
	[MetaXSD] [xml] NULL,
	[ImportXML] [xml] NULL,
	[EgyebXML] [xml] NULL,
	[ContentType] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SPSSzinkronizalt] [char](1) COLLATE Hungarian_CI_AS NULL,
	[SPS_CTT_Id] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[WorkFlowVezerles] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_Obj_MetaDefinicioHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaXmlHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_Obj_MetaXmlHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Kodertek] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[IratMetadefinicio_Id] [uniqueidentifier] NULL,
	[Meta_XML] [xml] NULL,
	[Meta_XML_Ervenyes] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_Id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_Id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_Id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_Id] [uniqueidentifier] NULL,
	[UIAccessLog_Id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_Obj_MetaXmlHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_ObjektumTargyszavaiHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_ObjektumTargyszavaiHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_ObjektumTargyszavaiHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Targyszo_Id] [uniqueidentifier] NULL,
	[Obj_Metaadatai_Id] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Targyszo] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Forras] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[SPSSzinkronizalt] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ertek] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Sorszam] [int] NULL,
	[Targyszo_XML] [xml] NULL,
	[Targyszo_XML_Ervenyes] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_ObjektumTargyszavaiHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_PldIratPeldanyokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_PldIratPeldanyokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[IraIrat_Id] [uniqueidentifier] NULL,
	[IraIrat_Id_Ver] [int] NULL,
	[Sorszam] [int] NULL,
	[SztornirozasDat] [datetime] NULL,
	[AtvetelDatuma] [datetime] NULL,
	[Eredet] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[KuldesMod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ragszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UgyintezesModja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[VisszaerkezesiHatarido] [datetime] NULL,
	[Visszavarolag] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[VisszaerkezesDatuma] [datetime] NULL,
	[Cim_id_Cimzett] [uniqueidentifier] NULL,
	[Partner_Id_Cimzett] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Orzo] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos_Elozo] [uniqueidentifier] NULL,
	[CimSTR_Cimzett] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[NevSTR_Cimzett] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Tovabbito] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[IraIrat_Id_Kapcsolt] [uniqueidentifier] NULL,
	[IrattariHely] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Gener_Id] [uniqueidentifier] NULL,
	[PostazasDatuma] [datetime] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Kovetkezo_Felelos_Id] [uniqueidentifier] NULL,
	[Elektronikus_Kezbesitesi_Allap] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Kovetkezo_Orzo_Id] [uniqueidentifier] NULL,
	[Fizikai_Kezbesitesi_Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[TovabbitasAlattAllapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[PostazasAllapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[ValaszElektronikus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[UgyUgyirat_Id_Kulso] [uniqueidentifier] NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AKTIV] [int] NULL,
 CONSTRAINT [PK_EREC_PldIratPeldanyokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldKapjakMegHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_PldKapjakMegHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[PldIratPeldany_Id] [uniqueidentifier] NULL,
	[Csoport_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_PldKapjakMegHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_SzamlakHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_SzamlakHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[Dokumentum_Id] [uniqueidentifier] NULL,
	[Partner_Id_Szallito] [uniqueidentifier] NULL,
	[Cim_Id_Szallito] [uniqueidentifier] NULL,
	[Bankszamlaszam_Id] [uniqueidentifier] NULL,
	[SzamlaSorszam] [varchar](16) COLLATE Hungarian_CI_AS NULL,
	[FizetesiMod] [varchar](4) COLLATE Hungarian_CI_AS NULL,
	[TeljesitesDatuma] [smalldatetime] NULL,
	[BizonylatDatuma] [smalldatetime] NULL,
	[FizetesiHatarido] [smalldatetime] NULL,
	[DevizaKod] [varchar](3) COLLATE Hungarian_CI_AS NULL,
	[VisszakuldesDatuma] [smalldatetime] NULL,
	[EllenorzoOsszeg] [float] NULL,
	[PIRAllapot] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[VevoBesorolas] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_SzamlakHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_SzignalasiJegyzekekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_SzignalasiJegyzekekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[UgykorTargykor_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[SzignalasTipusa] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[SzignalasiKotelezettseg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_SzignalasiJegyzekekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TargySzavakHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_TargySzavakHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_TargySzavakHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Tipus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[TargySzavak] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AlapertelmezettErtek] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[BelsoAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SPSSzinkronizalt] [char](1) COLLATE Hungarian_CI_AS NULL,
	[SPS_Field_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NULL,
	[RegExp] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ToolTip] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[XSD] [xml] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[ControlTypeSource] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[ControlTypeDataSource] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_TargySzavakHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_UgyiratKapcsolatokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[KapcsolatTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Kezi] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ugyirat_Ugyirat_Beepul] [uniqueidentifier] NULL,
	[Ugyirat_Ugyirat_Beepul_Ver] [int] NULL,
	[Ugyirat_Ugyirat_Felepul] [uniqueidentifier] NULL,
	[Ugyirat_Ugyirat_Felepul_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_UgyiratKapcsolatokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratObjKapcsolatokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_UgyiratObjKapcsolatokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_UgyiratObjKapcsolatokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[KapcsolatTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Kezi] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Obj_Id_Elozmeny] [uniqueidentifier] NULL,
	[Obj_Tip_Id_Elozmeny] [uniqueidentifier] NULL,
	[Obj_Type_Elozmeny] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Obj_Id_Kapcsolt] [uniqueidentifier] NULL,
	[Obj_Tip_Id_Kapcsolt] [uniqueidentifier] NULL,
	[Obj_Type_Kapcsolt] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_UgyiratObjKapcsolatokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyKezFeljegyzesekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_UgyKezFeljegyzesekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[UgyUgyirat_Id] [uniqueidentifier] NULL,
	[UgyUgyirat_Id_Ver] [int] NULL,
	[KezelesTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_UgyKezFeljegyzesekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_UgyUgyiratdarabokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_UgyUgyiratdarabokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[UgyUgyirat_Id] [uniqueidentifier] NULL,
	[UgyUgyirat_Id_Ver] [int] NULL,
	[EljarasiSzakasz] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Sorszam] [int] NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Hatarido] [datetime] NULL,
	[FelhasznaloCsoport_Id_Ugyintez] [uniqueidentifier] NULL,
	[ElintezesDat] [datetime] NULL,
	[LezarasDat] [datetime] NULL,
	[ElintezesMod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[LezarasOka] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[UgyUgyirat_Id_Elozo] [uniqueidentifier] NULL,
	[IraIktatokonyv_Id] [uniqueidentifier] NULL,
	[IraIktatokonyv_Id_Ver] [int] NULL,
	[Foszam] [int] NULL,
	[UtolsoAlszam] [int] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos_Elozo] [uniqueidentifier] NULL,
	[IratMetadefinicio_Id] [uniqueidentifier] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_UgyUgyiratdarabokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_UgyUgyiratokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_UgyUgyiratokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Foszam] [int] NULL,
	[Ugyazonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Alkalmazas_Id] [uniqueidentifier] NULL,
	[UgyintezesModja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Hatarido] [datetime] NULL,
	[SkontrobaDat] [datetime] NULL,
	[LezarasDat] [datetime] NULL,
	[IrattarbaKuldDatuma] [datetime] NULL,
	[IrattarbaVetelDat] [datetime] NULL,
	[FelhCsoport_Id_IrattariAtvevo] [uniqueidentifier] NULL,
	[SelejtezesDat] [datetime] NULL,
	[FelhCsoport_Id_Selejtezo] [uniqueidentifier] NULL,
	[LeveltariAtvevoNeve] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[FelhCsoport_Id_Felulvizsgalo] [uniqueidentifier] NULL,
	[FelulvizsgalatDat] [datetime] NULL,
	[IktatoszamKieg] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[Targy] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[UgyUgyirat_Id_Szulo] [uniqueidentifier] NULL,
	[UgyUgyirat_Id_Szulo_Ver] [int] NULL,
	[UgyTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[IrattariHely] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos_Ver] [int] NULL,
	[FelhasznaloCsoport_Id_Orzo] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Orzo_Ver] [int] NULL,
	[Csoport_Id_Cimzett] [uniqueidentifier] NULL,
	[Jelleg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[IraIrattariTetel_Id] [uniqueidentifier] NULL,
	[IraIrattariTetel_Id_Ver] [int] NULL,
	[IraIktatokonyv_Id] [uniqueidentifier] NULL,
	[IraIktatokonyv_Id_Ver] [int] NULL,
	[SkontroOka] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SkontroVege] [datetime] NULL,
	[Surgosseg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[SkontrobanOsszesen] [int] NULL,
	[MegorzesiIdoVege] [datetime] NULL,
	[Partner_Id_Ugyindito] [uniqueidentifier] NULL,
	[Partner_Id_Ugyindito_Ver] [int] NULL,
	[NevSTR_Ugyindito] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[ElintezesDat] [datetime] NULL,
	[FelhasznaloCsoport_Id_Ugyintez] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Ugyintez_Ver] [int] NULL,
	[Csoport_Id_Felelos_Elozo] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos_Elozo_Ver] [int] NULL,
	[KolcsonKikerDat] [datetime] NULL,
	[KolcsonKiadDat] [datetime] NULL,
	[Kolcsonhatarido] [datetime] NULL,
	[BARCODE] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[IratMetadefinicio_Id] [uniqueidentifier] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[TovabbitasAlattAllapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Fizikai_Kezbesitesi_Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Kovetkezo_Orzo_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Ugyfelelos] [uniqueidentifier] NULL,
	[Elektronikus_Kezbesitesi_Allap] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Kovetkezo_Felelos_Id] [uniqueidentifier] NULL,
	[UtolsoAlszam] [int] NULL,
	[ElintezesMod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[RegirendszerIktatoszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[GeneraltTargy] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Cim_Id_Ugyindito] [uniqueidentifier] NULL,
	[CimSTR_Ugyindito] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[UgyUgyirat_Id_Kulso] [uniqueidentifier] NULL,
	[IratSzam] [int] NULL,
	[UtolsoSorszam] [int] NULL,
	[Sorszam] [int] NULL,
	[UjOrzesiIdo] [int] NULL,
	[IrattarId] [uniqueidentifier] NULL,
	[LezarasOka] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[AKTIV] [int] NULL,
	[SkontroOka_Kod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_UgyUgyiratokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_AlairtDokumentumokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_AlairtDokumentumokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[AlairasMod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[AlairtFajlnev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[AlairtTartalomHash] [varbinary](4000) NULL,
	[IratAlairasSzabaly_Id] [uniqueidentifier] NULL,
	[AlairasRendben] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KivarasiIdoVege] [datetime] NULL,
	[AlairasVeglegRendben] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Idopecset] [datetime] NULL,
	[Csoport_Id_Alairo] [uniqueidentifier] NULL,
	[AlairasTulajdonos] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Tanusitvany_Id] [uniqueidentifier] NULL,
	[External_Link] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[External_Id] [uniqueidentifier] NULL,
	[External_Source] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[External_Info] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_AlairtDokumentumokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_AlkalmazasokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_AlkalmazasokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[KRT_Id] [uniqueidentifier] NULL,
	[KRT_Id_Ver] [int] NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Kulso] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Org] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_AlkalmazasokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BankszamlaszamokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_BankszamlaszamokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Partner_Id] [uniqueidentifier] NULL,
	[Partner_Id_Ver] [int] NULL,
	[Bankszamlaszam] [varchar](34) COLLATE Hungarian_CI_AS NULL,
	[Partner_Id_Bank] [uniqueidentifier] NULL,
	[Partner_Id_Bank_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_BankszamlaszamokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_BarkodokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_BarkodokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KodType] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_BarkodokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodSavokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_BarkodSavokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[SavKezd] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SavVege] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SavType] [char](1) COLLATE Hungarian_CI_AS NULL,
	[SavAllapot] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_BarkodSavokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CimekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_CimekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_CimekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Kategoria] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KulsoAzonositok] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Forras] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Orszag_Id] [uniqueidentifier] NULL,
	[Orszag_Id_Ver] [int] NULL,
	[OrszagNev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Telepules_Id] [uniqueidentifier] NULL,
	[Telepules_Id_Ver] [int] NULL,
	[TelepulesNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[IRSZ] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[CimTobbi] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Kozterulet_Id] [uniqueidentifier] NULL,
	[Kozterulet_Id_Ver] [int] NULL,
	[KozteruletNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KozteruletTipus_Id] [uniqueidentifier] NULL,
	[KozteruletTipus_Id_Ver] [int] NULL,
	[KozteruletTipusNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Hazszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Hazszamig] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[HazszamBetujel] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[MindketOldal] [char](1) COLLATE Hungarian_CI_AS NULL,
	[HRSZ] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Lepcsohaz] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Szint] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Ajto] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[AjtoBetujel] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Tobbi] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_CimekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CsoportokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_CsoportokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_CsoportokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Jogalany] [char](1) COLLATE Hungarian_CI_AS NULL,
	[ErtesitesEmail] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[System] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Adatforras] [char](1) COLLATE Hungarian_CI_AS NULL,
	[ObjTipus_Id_Szulo] [uniqueidentifier] NULL,
	[ObjTipus_Id_Szulo_Ver] [int] NULL,
	[Kiszolgalhato] [char](1) COLLATE Hungarian_CI_AS NULL,
	[JogosultsagOroklesMod] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_CsoportokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_CsoportTagokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_CsoportTagokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Ver] [int] NULL,
	[Csoport_Id_Jogalany] [uniqueidentifier] NULL,
	[Csoport_Id_Jogalany_Ver] [int] NULL,
	[ObjTip_Id_Jogalany] [uniqueidentifier] NULL,
	[ErtesitesMailCim] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[ErtesitesKell] [char](1) COLLATE Hungarian_CI_AS NULL,
	[System] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Orokolheto] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[ObjektumTulajdonos] [char](1) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_KRT_CsoportTagokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokuAlairasKapcsolatokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_DokuAlairasKapcsolatokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Dokumentum_id] [uniqueidentifier] NULL,
	[DokumentumAlairas_Id] [uniqueidentifier] NULL,
	[DokuKapcsolatSorrend] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_DokuAlairasKapcsolatokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumAlairasokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_DokumentumAlairasokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_DokumentumAlairasokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Dokumentum_Id_Alairt] [uniqueidentifier] NULL,
	[AlairasMod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[AlairasSzabaly_Id] [uniqueidentifier] NULL,
	[AlairasRendben] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KivarasiIdoVege] [datetime] NULL,
	[AlairasVeglegRendben] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Idopecset] [datetime] NULL,
	[Csoport_Id_Alairo] [uniqueidentifier] NULL,
	[AlairasTulajdonos] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Tanusitvany_Id] [uniqueidentifier] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[AlairoSzemely] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_KRT_DokumentumAlairasokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumKapcsolatokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_DokumentumKapcsolatokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Dokumentum_Id_Fo] [uniqueidentifier] NULL,
	[Dokumentum_Id_Al] [uniqueidentifier] NULL,
	[DokumentumKapcsolatJelleg] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[DokuKapcsolatSorrend] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_DokumentumKapcsolatokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_DokumentumokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_DokumentumokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[FajlNev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[VerzioJel] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Formatum] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Meret] [int] NULL,
	[TartalomHash] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AlairtTartalomHash] [varbinary](4000) NULL,
	[KivonatHash] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Dokumentum_Id] [uniqueidentifier] NULL,
	[Dokumentum_Id_Kovetkezo] [uniqueidentifier] NULL,
	[Dokumentum_Id_Kovetkezo_Ver] [int] NULL,
	[Alkalmazas_Id] [uniqueidentifier] NULL,
	[Alkalmazas_Id_Ver] [int] NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NULL,
	[KulsoAzonositok] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[External_Source] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[External_Link] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[External_Id] [uniqueidentifier] NULL,
	[External_Info] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[CheckedOut] [uniqueidentifier] NULL,
	[CheckedOutTime] [datetime] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[OCRPrioritas] [int] NULL,
	[OCRAllapot] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Megnyithato] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Olvashato] [char](1) COLLATE Hungarian_CI_AS NULL,
	[ElektronikusAlairas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[AlairasFelulvizsgalat] [datetime] NULL,
	[Titkositas] [char](1) COLLATE Hungarian_CI_AS NULL,
	[SablonAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_Id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_Id] [uniqueidentifier] NULL,
	[UIAccessLog_Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[WorkFlowAllapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_KRT_DokumentumokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Erintett_TablakHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Erintett_TablakHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Tranzakci_id] [uniqueidentifier] NULL,
	[Tranzakci_id_Ver] [int] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_Erintett_TablakHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Extra_NapokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Extra_NapokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Datum] [datetime] NULL,
	[Jelzo] [int] NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_Extra_NapokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_SzerepkorHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Felhasznalo_SzerepkorHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_Felhasznalo_SzerepkorHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[CsoportTag_Id] [uniqueidentifier] NULL,
	[Csoport_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Ver] [int] NULL,
	[Felhasznalo_Id] [uniqueidentifier] NULL,
	[Felhasznalo_Id_Ver] [int] NULL,
	[Szerepkor_Id] [uniqueidentifier] NULL,
	[Szerepkor_Id_Ver] [int] NULL,
	[Helyettesites_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_Felhasznalo_SzerepkorHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[Felhasznalo_Id] [uniqueidentifier] NOT NULL,
	[Halozati_Nyomtato_Id] [uniqueidentifier] NOT NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_Felhasznalok_Halozati_NyomtatoiHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznalokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_FelhasznalokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_FelhasznalokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Partner_id] [uniqueidentifier] NULL,
	[Partner_id_Ver] [int] NULL,
	[Tipus] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[UserNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Jelszo] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[JelszoLejaratIdo] [datetime] NULL,
	[System] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Kiszolgalo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[DefaultPrivatKulcs] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Partner_Id_Munkahely] [uniqueidentifier] NULL,
	[Partner_Id_Munkahely_Ver] [int] NULL,
	[MaxMinosites] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[EMail] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Engedelyezett] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Telefonszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Beosztas] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_FelhasznalokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloProfilokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_FelhasznaloProfilokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Tipus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Felhasznalo_id] [uniqueidentifier] NULL,
	[Felhasznalo_id_Ver] [int] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[Obj_Id_Ver] [int] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[XML] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_FelhasznaloProfilokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FunkciokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_FunkciokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_FunkciokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[ObjTipus_Id_AdatElem] [uniqueidentifier] NULL,
	[ObjTipus_Id_AdatElem_Ver] [int] NULL,
	[ObjStat_Id_Kezd] [uniqueidentifier] NULL,
	[ObjStat_Id_Kezd_Ver] [int] NULL,
	[Alkalmazas_Id] [uniqueidentifier] NULL,
	[Alkalmazas_Id_Ver] [int] NULL,
	[Muvelet_Id] [uniqueidentifier] NULL,
	[Muvelet_Id_Ver] [int] NULL,
	[ObjStat_Id_Veg] [uniqueidentifier] NULL,
	[ObjStat_Id_Veg_Ver] [int] NULL,
	[Leiras] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Funkcio_Id_Szulo] [uniqueidentifier] NULL,
	[Funkcio_Id_Szulo_Ver] [int] NULL,
	[Csoportosito] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Modosithato] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Org] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[MunkanaploJelzo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[FeladatJelzo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KeziFeladatJelzo] [char](1) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_KRT_FunkciokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioListaHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_FunkcioListaHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Funkcio_Id] [uniqueidentifier] NULL,
	[Funkcio_Id_Ver] [int] NULL,
	[Funkcio_Id_Hivott] [uniqueidentifier] NULL,
	[Funkcio_Id_Hivott_Ver] [int] NULL,
	[FutasiSorrend] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_FunkcioListaHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Halozati_NyomtatokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Halozati_NyomtatokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Cim] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_Halozati_NyomtatokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_HelyettesitesekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_HelyettesitesekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_HelyettesitesekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Felhasznalo_ID_helyettesitett] [uniqueidentifier] NULL,
	[Felhasznalo_ID_helyettesitett_Ver] [int] NULL,
	[Felhasznalo_ID_helyettesito] [uniqueidentifier] NULL,
	[Felhasznalo_ID_helyettesito_Ver] [int] NULL,
	[HelyettesitesKezd] [datetime] NULL,
	[HelyettesitesVege] [datetime] NULL,
	[Megjegyzes] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[HelyettesitesMod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[CsoportTag_ID_helyettesitett] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_HelyettesitesekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodCsoportokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_KodCsoportokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[KodTarak_Id_KodcsoportTipus] [uniqueidentifier] NULL,
	[KodTarak_Id_KodcsoportTipus_Ver] [int] NULL,
	[Kod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Modosithato] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Hossz] [int] NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NULL,
	[Leiras] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[BesorolasiSema] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KiegAdat] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KiegMezo] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[KiegAdattipus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KiegSzotar] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_KodCsoportokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodTarakHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_KodTarakHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_KodTarakHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[KodCsoport_Id] [uniqueidentifier] NULL,
	[KodCsoport_Id_Ver] [int] NULL,
	[ObjTip_Id_AdatElem] [uniqueidentifier] NULL,
	[ObjTip_Id_AdatElem_Ver] [int] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[Kod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[RovidNev] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Egyeb] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Modosithato] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Sorrend] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_KodTarakHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KozteruletekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_KozteruletekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_KozteruletekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KozteruletTipusokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_KozteruletTipusokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[KozterTipNev_Id_Szinonima] [uniqueidentifier] NULL,
	[KozterTipNev_Id_Szinonima_Ver] [int] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_KozteruletTipusokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappakHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_MappakHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[SzuloMappa_Id] [uniqueidentifier] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_MappakHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaTartalmakHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_MappaTartalmakHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Mappa_Id] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_MappaTartalmakHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaUtakHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_MappaUtakHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Mappa_Id] [uniqueidentifier] NULL,
	[PathFromORG] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_MappaUtakHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MenukHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_MenukHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_MenukHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Menu_Id_Szulo] [uniqueidentifier] NULL,
	[Menu_Id_Szulo_Ver] [int] NULL,
	[Kod] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Funkcio_Id] [uniqueidentifier] NULL,
	[Funkcio_Id_Ver] [int] NULL,
	[Modul_Id] [uniqueidentifier] NULL,
	[Modul_Id_Ver] [int] NULL,
	[Parameter] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ImageURL] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Sorrend] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_MenukHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Modul_FunkcioHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Modul_FunkcioHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Funkcio_Id] [uniqueidentifier] NULL,
	[Funkcio_Id_Ver] [int] NULL,
	[Modul_Id] [uniqueidentifier] NULL,
	[Modul_Id_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_Modul_FunkcioHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ModulokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_ModulokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_ModulokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Tipus] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[SelectString] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NULL,
	[Parameterek] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ObjTip_Id_Adatelem] [uniqueidentifier] NULL,
	[ObjTip_Adatelem] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_ModulokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MuveletekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_MuveletekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Kod] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_MuveletekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_NezetekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_NezetekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Form_Id] [uniqueidentifier] NULL,
	[Form_Id_Ver] [int] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id] [uniqueidentifier] NULL,
	[Muvelet_Id] [uniqueidentifier] NULL,
	[Muvelet_Id_Ver] [int] NULL,
	[DisableControls] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ReadOnlyControls] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Prioritas] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[InvisibleControls] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_KRT_NezetekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_ObjTipusokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_ObjTipusokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[ObjTipus_Id_Tipus] [uniqueidentifier] NULL,
	[ObjTipus_Id_Tipus_Ver] [int] NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Obj_Id_Szulo] [uniqueidentifier] NULL,
	[Obj_Id_Szulo_Ver] [int] NULL,
	[KodCsoport_Id] [uniqueidentifier] NULL,
	[KodCsoport_Id_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_ObjTipusokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_OrgokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_OrgokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Sorszam] [int] NULL,
	[Partner_id_szulo] [uniqueidentifier] NULL,
	[Partner_id_szulo_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_OrgokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_OrszagokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_OrszagokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Kod] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Viszonylatkod] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_OrszagokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ParameterekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_ParameterekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_ParameterekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Felhasznalo_id] [uniqueidentifier] NULL,
	[Felhasznalo_id_Ver] [int] NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ertek] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Karbantarthato] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_ParameterekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerCimekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_PartnerCimekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_PartnerCimekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Partner_id] [uniqueidentifier] NULL,
	[Partner_id_Ver] [int] NULL,
	[Cim_Id] [uniqueidentifier] NULL,
	[Cim_Id_Ver] [int] NULL,
	[UtolsoHasznalatSiker] [char](1) COLLATE Hungarian_CI_AS NULL,
	[UtolsoHasznalatiIdo] [datetime] NULL,
	[eMailKuldes] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Fajta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_PartnerCimekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_PartnerekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_PartnerekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Id_Ver] [int] NULL,
	[Org] [uniqueidentifier] NULL,
	[Orszag_Id] [uniqueidentifier] NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Hierarchia] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[KulsoAzonositok] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[PublikusKulcs] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Kiszolgalo] [uniqueidentifier] NULL,
	[LetrehozoSzervezet] [uniqueidentifier] NULL,
	[MaxMinosites] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[MinositesKezdDat] [datetime] NULL,
	[MinositesVegDat] [datetime] NULL,
	[MaxMinositesSzervezet] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[Belso] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Forras] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_PartnerekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_PartnerKapcsolatokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_PartnerKapcsolatokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Partner_id] [uniqueidentifier] NULL,
	[Partner_id_Ver] [int] NULL,
	[Partner_id_kapcsolt] [uniqueidentifier] NULL,
	[Partner_id_kapcsolt_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_PartnerKapcsolatokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_PartnerMinositesekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[ALLAPOT] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Partner_id] [uniqueidentifier] NULL,
	[Partner_id_Ver] [int] NULL,
	[Felhasznalo_id_kero] [uniqueidentifier] NULL,
	[Felhasznalo_id_kero_Ver] [int] NULL,
	[KertMinosites] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[KertKezdDat] [datetime] NULL,
	[KertVegeDat] [datetime] NULL,
	[KerelemAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KerelemBeadIdo] [datetime] NULL,
	[KerelemDontesIdo] [datetime] NULL,
	[Felhasznalo_id_donto] [uniqueidentifier] NULL,
	[Felhasznalo_id_donto_Ver] [int] NULL,
	[DontesAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Minosites] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[MinositesKezdDat] [datetime] NULL,
	[MinositesVegDat] [datetime] NULL,
	[SztornirozasDat] [datetime] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_PartnerMinositesekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagSzamokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_RagSzamokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KodNum] [float] NULL,
	[Postakonyv_Id] [uniqueidentifier] NULL,
	[RagszamSav_Id] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KodType] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_RagszamSavokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[Postakonyv_Id] [uniqueidentifier] NULL,
	[SavKezd] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SavKezdNum] [float] NULL,
	[SavVege] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SavVegeNum] [float] NULL,
	[SavType] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SavAllapot] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_SzemelyekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_SzemelyekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_SzemelyekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Partner_Id] [uniqueidentifier] NULL,
	[Partner_Id_Ver] [int] NULL,
	[AnyjaNeve] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[ApjaNeve] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiOrszag] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiOrszagId] [uniqueidentifier] NULL,
	[UjTitulis] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[UjCsaladiNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UjUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiHely] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiHely_id] [uniqueidentifier] NULL,
	[SzuletesiIdo] [datetime] NULL,
	[TAJSzam] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[SZIGSzam] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Neme] [char](1) COLLATE Hungarian_CI_AS NULL,
	[SzemelyiAzonosito] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Adoazonosito] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Adoszam] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[AnyjaNeveCsaladiNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AnyjaNeveElsoUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AnyjaNeveTovabbiUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiCsaladiNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiElsoUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiTovabbiUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UjTovabbiUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Allampolgarsag] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KulfoldiAdoszamJelolo] [char](1) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_KRT_SzemelyekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_FunkcioHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Szerepkor_FunkcioHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_Szerepkor_FunkcioHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Funkcio_Id] [uniqueidentifier] NULL,
	[Funkcio_Id_Ver] [int] NULL,
	[Szerepkor_Id] [uniqueidentifier] NULL,
	[Szerepkor_Id_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_Szerepkor_FunkcioHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_SzerepkorokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_SzerepkorokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_SzerepkorokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KulsoAzonositok] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_SzerepkorokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tartomanyok_SzervezetekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Tartomanyok_SzervezetekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Tartomany_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[TartType] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_Tartomanyok_SzervezetekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TartomanyokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_TartomanyokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[TartomanyKezd] [numeric](12, 0) NULL,
	[TartomanyHossz] [numeric](12, 0) NULL,
	[FoglaltDarab] [numeric](12, 0) NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_TartomanyokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TelepulesekHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_TelepulesekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[IRSZ] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Telepules_Id_Fo] [uniqueidentifier] NULL,
	[Telepules_Id_Fo_Ver] [int] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Orszag_Id] [uniqueidentifier] NULL,
	[Orszag_Id_Ver] [int] NULL,
	[Megye] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Regio] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_TelepulesekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManagerHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_TemplateManagerHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[KRT_Modul_Id] [uniqueidentifier] NULL,
	[KRT_Modul_Id_Ver] [int] NULL,
	[KRT_Dokumentum_Id] [uniqueidentifier] NULL,
	[KRT_Dokumentum_Id_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_TemplateManagerHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tranz_ObjHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Tranz_ObjHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[ObjTip_Id_AdatElem] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_Tranz_ObjHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TranzakciokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_TranzakciokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Alkalmazas_Id] [uniqueidentifier] NULL,
	[Alkalmazas_Id_Ver] [int] NULL,
	[Felhasznalo_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_TranzakciokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_VallalkozasokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_VallalkozasokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_VallalkozasokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Partner_Id] [uniqueidentifier] NULL,
	[Partner_Id_Ver] [int] NULL,
	[Adoszam] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[TB_Torzsszam] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Cegjegyzekszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[KulfoldiAdoszamJelolo] [char](1) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_KRT_VallalkozasokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_AgazatiJelekHistory]') AND name = N'IX_EREC_AgazatiJelekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_AgazatiJelekHistory_ID_VER] ON [dbo].[EREC_AgazatiJelekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_CsatolmanyokHistory]') AND name = N'IX_EREC_CsatolmanyokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_CsatolmanyokHistory_ID_VER] ON [dbo].[EREC_CsatolmanyokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimeiHistory]') AND name = N'IX_EREC_eMailBoritekCimeiHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_eMailBoritekCimeiHistory_ID_VER] ON [dbo].[EREC_eMailBoritekCimeiHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCsatolmanyokHistory]') AND name = N'IX_EREC_eMailBoritekCsatolmanyokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_eMailBoritekCsatolmanyokHistory_ID_VER] ON [dbo].[EREC_eMailBoritekCsatolmanyokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekokHistory]') AND name = N'IX_EREC_eMailBoritekokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_eMailBoritekokHistory_ID_VER] ON [dbo].[EREC_eMailBoritekokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailFiokokHistory]') AND name = N'IX_EREC_eMailFiokokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_eMailFiokokHistory_ID_VER] ON [dbo].[EREC_eMailFiokokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_FeladatDefinicioHistory]') AND name = N'IX_EREC_FeladatDefinicioHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_FeladatDefinicioHistory_ID_VER] ON [dbo].[EREC_FeladatDefinicioHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Hatarid_ObjektumokHistory]') AND name = N'IX_EREC_Hatarid_ObjektumokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_Hatarid_ObjektumokHistory_ID_VER] ON [dbo].[EREC_Hatarid_ObjektumokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatokHistory]') AND name = N'IX_EREC_HataridosFeladatokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_HataridosFeladatokHistory_ID_VER] ON [dbo].[EREC_HataridosFeladatokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivekHistory]') AND name = N'IX_EREC_IraElosztoivekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IraElosztoivekHistory_ID_VER] ON [dbo].[EREC_IraElosztoivekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelekHistory]') AND name = N'IX_EREC_IraElosztoivTetelekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IraElosztoivTetelekHistory_ID_VER] ON [dbo].[EREC_IraElosztoivTetelekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIktatoKonyvekHistory]') AND name = N'IX_EREC_IraIktatoKonyvekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IraIktatoKonyvekHistory_ID_VER] ON [dbo].[EREC_IraIktatoKonyvekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokDokumentumokHistory]') AND name = N'IX_EREC_IraIratokDokumentumokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IraIratokDokumentumokHistory_ID_VER] ON [dbo].[EREC_IraIratokDokumentumokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokHistory]') AND name = N'IX_EREC_IraIratokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IraIratokHistory_ID_VER] ON [dbo].[EREC_IraIratokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIrattariTetelekHistory]') AND name = N'IX_EREC_IraIrattariTetelekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IraIrattariTetelekHistory_ID_VER] ON [dbo].[EREC_IraIrattariTetelekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekekHistory]') AND name = N'IX_EREC_IraJegyzekekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IraJegyzekekHistory_ID_VER] ON [dbo].[EREC_IraJegyzekekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekTetelekHistory]') AND name = N'IX_EREC_IraJegyzekTetelekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IraJegyzekTetelekHistory_ID_VER] ON [dbo].[EREC_IraJegyzekTetelekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiFejekHistory]') AND name = N'IX_EREC_IraKezbesitesiFejekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IraKezbesitesiFejekHistory_ID_VER] ON [dbo].[EREC_IraKezbesitesiFejekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelekHistory]') AND name = N'IX_EREC_IraKezbesitesiTetelekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IraKezbesitesiTetelekHistory_ID_VER] ON [dbo].[EREC_IraKezbesitesiTetelekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezFeljegyzesekHistory]') AND name = N'IX_EREC_IraKezFeljegyzesekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IraKezFeljegyzesekHistory_ID_VER] ON [dbo].[EREC_IraKezFeljegyzesekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraOnkormAdatokHistory]') AND name = N'IX_EREC_IraOnkormAdatokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IraOnkormAdatokHistory_ID_VER] ON [dbo].[EREC_IraOnkormAdatokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Irat_IktatokonyveiHistory]') AND name = N'IX_EREC_Irat_IktatokonyveiHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_Irat_IktatokonyveiHistory_ID_VER] ON [dbo].[EREC_Irat_IktatokonyveiHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratAlairokHistory]') AND name = N'IX_EREC_IratAlairokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IratAlairokHistory_ID_VER] ON [dbo].[EREC_IratAlairokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratelemKapcsolatokHistory]') AND name = N'IX_EREC_IratelemKapcsolatokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IratelemKapcsolatokHistory_ID_VER] ON [dbo].[EREC_IratelemKapcsolatokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatokHistory]') AND name = N'IX_EREC_IratKapcsolatokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IratKapcsolatokHistory_ID_VER] ON [dbo].[EREC_IratKapcsolatokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMellekletekHistory]') AND name = N'IX_EREC_IratMellekletekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IratMellekletekHistory_ID_VER] ON [dbo].[EREC_IratMellekletekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMetaDefinicioHistory]') AND name = N'IX_EREC_IratMetaDefinicioHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IratMetaDefinicioHistory_ID_VER] ON [dbo].[EREC_IratMetaDefinicioHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariHelyekHistory]') AND name = N'IX_EREC_IrattariHelyekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IrattariHelyekHistory_ID_VER] ON [dbo].[EREC_IrattariHelyekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariKikeroHistory]') AND name = N'IX_EREC_IrattariKikeroHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IrattariKikeroHistory_ID_VER] ON [dbo].[EREC_IrattariKikeroHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_IktatokonyvHistory]') AND name = N'IX_EREC_IrattariTetel_IktatokonyvHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_IrattariTetel_IktatokonyvHistory_ID_VER] ON [dbo].[EREC_IrattariTetel_IktatokonyvHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldokHistory]') AND name = N'IX_EREC_KuldBekuldokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_KuldBekuldokHistory_ID_VER] ON [dbo].[EREC_KuldBekuldokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldDokumentumokHistory]') AND name = N'IX_EREC_KuldDokumentumokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_KuldDokumentumokHistory_ID_VER] ON [dbo].[EREC_KuldDokumentumokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyaiHistory]') AND name = N'IX_EREC_Kuldemeny_IratPeldanyaiHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_Kuldemeny_IratPeldanyaiHistory_ID_VER] ON [dbo].[EREC_Kuldemeny_IratPeldanyaiHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatokHistory]') AND name = N'IX_EREC_KuldKapcsolatokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_KuldKapcsolatokHistory_ID_VER] ON [dbo].[EREC_KuldKapcsolatokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKezFeljegyzesekHistory]') AND name = N'IX_EREC_KuldKezFeljegyzesekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_KuldKezFeljegyzesekHistory_ID_VER] ON [dbo].[EREC_KuldKezFeljegyzesekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyekHistory]') AND name = N'IX_EREC_KuldKuldemenyekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_KuldKuldemenyekHistory_ID_VER] ON [dbo].[EREC_KuldKuldemenyekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldMellekletekHistory]') AND name = N'IX_EREC_KuldMellekletekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_KuldMellekletekHistory_ID_VER] ON [dbo].[EREC_KuldMellekletekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldTertivevenyekHistory]') AND name = N'IX_EREC_KuldTertivevenyekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_KuldTertivevenyekHistory_ID_VER] ON [dbo].[EREC_KuldTertivevenyekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_MellekletekHistory]') AND name = N'IX_EREC_MellekletekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_MellekletekHistory_ID_VER] ON [dbo].[EREC_MellekletekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdataiHistory]') AND name = N'IX_EREC_Obj_MetaAdataiHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_Obj_MetaAdataiHistory_ID_VER] ON [dbo].[EREC_Obj_MetaAdataiHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicioHistory]') AND name = N'IX_EREC_Obj_MetaDefinicioHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_Obj_MetaDefinicioHistory_ID_VER] ON [dbo].[EREC_Obj_MetaDefinicioHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaXmlHistory]') AND name = N'IX_EREC_Obj_MetaXmlHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_Obj_MetaXmlHistory_ID_VER] ON [dbo].[EREC_Obj_MetaXmlHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_ObjektumTargyszavaiHistory]') AND name = N'IX_EREC_ObjektumTargyszavaiHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_ObjektumTargyszavaiHistory_ID_VER] ON [dbo].[EREC_ObjektumTargyszavaiHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyokHistory]') AND name = N'IX_EREC_PldIratPeldanyokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_PldIratPeldanyokHistory_ID_VER] ON [dbo].[EREC_PldIratPeldanyokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldKapjakMegHistory]') AND name = N'IX_EREC_PldKapjakMegHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_PldKapjakMegHistory_ID_VER] ON [dbo].[EREC_PldKapjakMegHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_SzamlakHistory]') AND name = N'IX_EREC_SzamlakHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_SzamlakHistory_ID_VER] ON [dbo].[EREC_SzamlakHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekekHistory]') AND name = N'IX_EREC_SzignalasiJegyzekekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_SzignalasiJegyzekekHistory_ID_VER] ON [dbo].[EREC_SzignalasiJegyzekekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TargySzavakHistory]') AND name = N'IX_EREC_TargySzavakHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_TargySzavakHistory_ID_VER] ON [dbo].[EREC_TargySzavakHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatokHistory]') AND name = N'IX_EREC_UgyiratKapcsolatokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_UgyiratKapcsolatokHistory_ID_VER] ON [dbo].[EREC_UgyiratKapcsolatokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratObjKapcsolatokHistory]') AND name = N'IX_EREC_UgyiratObjKapcsolatokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_UgyiratObjKapcsolatokHistory_ID_VER] ON [dbo].[EREC_UgyiratObjKapcsolatokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyKezFeljegyzesekHistory]') AND name = N'IX_EREC_UgyKezFeljegyzesekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_UgyKezFeljegyzesekHistory_ID_VER] ON [dbo].[EREC_UgyKezFeljegyzesekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabokHistory]') AND name = N'IX_EREC_UgyUgyiratdarabokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_UgyUgyiratdarabokHistory_ID_VER] ON [dbo].[EREC_UgyUgyiratdarabokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratokHistory]') AND name = N'IX_EREC_UgyUgyiratokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_UgyUgyiratokHistory_ID_VER] ON [dbo].[EREC_UgyUgyiratokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_AlairtDokumentumokHistory]') AND name = N'IX_KRT_AlairtDokumentumokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_AlairtDokumentumokHistory_ID_VER] ON [dbo].[KRT_AlairtDokumentumokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_AlkalmazasokHistory]') AND name = N'IX_KRT_AlkalmazasokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_AlkalmazasokHistory_ID_VER] ON [dbo].[KRT_AlkalmazasokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BankszamlaszamokHistory]') AND name = N'IX_KRT_BankszamlaszamokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_BankszamlaszamokHistory_ID_VER] ON [dbo].[KRT_BankszamlaszamokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodokHistory]') AND name = N'IX_KRT_BarkodokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_BarkodokHistory_ID_VER] ON [dbo].[KRT_BarkodokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodSavokHistory]') AND name = N'IX_KRT_BarkodSavokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_BarkodSavokHistory_ID_VER] ON [dbo].[KRT_BarkodSavokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CimekHistory]') AND name = N'IX_KRT_CimekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_CimekHistory_ID_VER] ON [dbo].[KRT_CimekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CsoportokHistory]') AND name = N'IX_KRT_CsoportokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_CsoportokHistory_ID_VER] ON [dbo].[KRT_CsoportokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagokHistory]') AND name = N'IX_KRT_CsoportTagokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_CsoportTagokHistory_ID_VER] ON [dbo].[KRT_CsoportTagokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokuAlairasKapcsolatokHistory]') AND name = N'IX_KRT_DokuAlairasKapcsolatokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_DokuAlairasKapcsolatokHistory_ID_VER] ON [dbo].[KRT_DokuAlairasKapcsolatokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumAlairasokHistory]') AND name = N'IX_KRT_DokumentumAlairasokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_DokumentumAlairasokHistory_ID_VER] ON [dbo].[KRT_DokumentumAlairasokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumKapcsolatokHistory]') AND name = N'IX_KRT_DokumentumKapcsolatokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_DokumentumKapcsolatokHistory_ID_VER] ON [dbo].[KRT_DokumentumKapcsolatokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumokHistory]') AND name = N'IX_KRT_DokumentumokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_DokumentumokHistory_ID_VER] ON [dbo].[KRT_DokumentumokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Erintett_TablakHistory]') AND name = N'IX_KRT_Erintett_TablakHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_Erintett_TablakHistory_ID_VER] ON [dbo].[KRT_Erintett_TablakHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Extra_NapokHistory]') AND name = N'IX_KRT_Extra_NapokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_Extra_NapokHistory_ID_VER] ON [dbo].[KRT_Extra_NapokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_SzerepkorHistory]') AND name = N'IX_KRT_Felhasznalo_SzerepkorHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_Felhasznalo_SzerepkorHistory_ID_VER] ON [dbo].[KRT_Felhasznalo_SzerepkorHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznalokHistory]') AND name = N'IX_KRT_FelhasznalokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_FelhasznalokHistory_ID_VER] ON [dbo].[KRT_FelhasznalokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloProfilokHistory]') AND name = N'IX_KRT_FelhasznaloProfilokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_FelhasznaloProfilokHistory_ID_VER] ON [dbo].[KRT_FelhasznaloProfilokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FunkciokHistory]') AND name = N'IX_KRT_FunkciokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_FunkciokHistory_ID_VER] ON [dbo].[KRT_FunkciokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioListaHistory]') AND name = N'IX_KRT_FunkcioListaHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_FunkcioListaHistory_ID_VER] ON [dbo].[KRT_FunkcioListaHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_HelyettesitesekHistory]') AND name = N'IX_KRT_HelyettesitesekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_HelyettesitesekHistory_ID_VER] ON [dbo].[KRT_HelyettesitesekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodCsoportokHistory]') AND name = N'IX_KRT_KodCsoportokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_KodCsoportokHistory_ID_VER] ON [dbo].[KRT_KodCsoportokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodTarakHistory]') AND name = N'IX_KRT_KodTarakHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_KodTarakHistory_ID_VER] ON [dbo].[KRT_KodTarakHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KozteruletekHistory]') AND name = N'IX_KRT_KozteruletekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_KozteruletekHistory_ID_VER] ON [dbo].[KRT_KozteruletekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KozteruletTipusokHistory]') AND name = N'IX_KRT_KozteruletTipusokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_KozteruletTipusokHistory_ID_VER] ON [dbo].[KRT_KozteruletTipusokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappakHistory]') AND name = N'IX_KRT_MappakHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_MappakHistory_ID_VER] ON [dbo].[KRT_MappakHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaTartalmakHistory]') AND name = N'IX_KRT_MappaTartalmakHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_MappaTartalmakHistory_ID_VER] ON [dbo].[KRT_MappaTartalmakHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaUtakHistory]') AND name = N'IX_KRT_MappaUtakHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_MappaUtakHistory_ID_VER] ON [dbo].[KRT_MappaUtakHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MenukHistory]') AND name = N'IX_KRT_MenukHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_MenukHistory_ID_VER] ON [dbo].[KRT_MenukHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Modul_FunkcioHistory]') AND name = N'IX_KRT_Modul_FunkcioHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_Modul_FunkcioHistory_ID_VER] ON [dbo].[KRT_Modul_FunkcioHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ModulokHistory]') AND name = N'IX_KRT_ModulokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_ModulokHistory_ID_VER] ON [dbo].[KRT_ModulokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MuveletekHistory]') AND name = N'IX_KRT_MuveletekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_MuveletekHistory_ID_VER] ON [dbo].[KRT_MuveletekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_NezetekHistory]') AND name = N'IX_KRT_NezetekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_NezetekHistory_ID_VER] ON [dbo].[KRT_NezetekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusokHistory]') AND name = N'IX_KRT_ObjTipusokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_ObjTipusokHistory_ID_VER] ON [dbo].[KRT_ObjTipusokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_OrgokHistory]') AND name = N'IX_KRT_OrgokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_OrgokHistory_ID_VER] ON [dbo].[KRT_OrgokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_OrszagokHistory]') AND name = N'IX_KRT_OrszagokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_OrszagokHistory_ID_VER] ON [dbo].[KRT_OrszagokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ParameterekHistory]') AND name = N'IX_KRT_ParameterekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_ParameterekHistory_ID_VER] ON [dbo].[KRT_ParameterekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerCimekHistory]') AND name = N'IX_KRT_PartnerCimekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_PartnerCimekHistory_ID_VER] ON [dbo].[KRT_PartnerCimekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerekHistory]') AND name = N'IX_KRT_PartnerekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_PartnerekHistory_ID_VER] ON [dbo].[KRT_PartnerekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatokHistory]') AND name = N'IX_KRT_PartnerKapcsolatokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_PartnerKapcsolatokHistory_ID_VER] ON [dbo].[KRT_PartnerKapcsolatokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesekHistory]') AND name = N'IX_KRT_PartnerMinositesekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_PartnerMinositesekHistory_ID_VER] ON [dbo].[KRT_PartnerMinositesekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagSzamokHistory]') AND name = N'IX_KRT_RagSzamokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_RagSzamokHistory_ID_VER] ON [dbo].[KRT_RagSzamokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavokHistory]') AND name = N'IX_KRT_RagszamSavokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_RagszamSavokHistory_ID_VER] ON [dbo].[KRT_RagszamSavokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_SzemelyekHistory]') AND name = N'IX_KRT_SzemelyekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_SzemelyekHistory_ID_VER] ON [dbo].[KRT_SzemelyekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_FunkcioHistory]') AND name = N'IX_KRT_Szerepkor_FunkcioHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_Szerepkor_FunkcioHistory_ID_VER] ON [dbo].[KRT_Szerepkor_FunkcioHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_SzerepkorokHistory]') AND name = N'IX_KRT_SzerepkorokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_SzerepkorokHistory_ID_VER] ON [dbo].[KRT_SzerepkorokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tartomanyok_SzervezetekHistory]') AND name = N'IX_KRT_Tartomanyok_SzervezetekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_Tartomanyok_SzervezetekHistory_ID_VER] ON [dbo].[KRT_Tartomanyok_SzervezetekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TartomanyokHistory]') AND name = N'IX_KRT_TartomanyokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_TartomanyokHistory_ID_VER] ON [dbo].[KRT_TartomanyokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TelepulesekHistory]') AND name = N'IX_KRT_TelepulesekHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_TelepulesekHistory_ID_VER] ON [dbo].[KRT_TelepulesekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManagerHistory]') AND name = N'IX_KRT_TemplateManagerHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_TemplateManagerHistory_ID_VER] ON [dbo].[KRT_TemplateManagerHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tranz_ObjHistory]') AND name = N'IX_KRT_Tranz_ObjHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_Tranz_ObjHistory_ID_VER] ON [dbo].[KRT_Tranz_ObjHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TranzakciokHistory]') AND name = N'IX_KRT_TranzakciokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_TranzakciokHistory_ID_VER] ON [dbo].[KRT_TranzakciokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_VallalkozasokHistory]') AND name = N'IX_KRT_VallalkozasokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_VallalkozasokHistory_ID_VER] ON [dbo].[KRT_VallalkozasokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_eMailFiokokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_eMailFiokokHistory] ADD  CONSTRAINT [DF_EREC_eMailFiokokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_FeladatDefinicioHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_FeladatDefinicioHistory] ADD  CONSTRAINT [DF_EREC_FeladatDefinicioHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_Hatarid_ObjektumokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Hatarid_ObjektumokHistory] ADD  CONSTRAINT [DF_EREC_Hatarid_ObjektumokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraElosztoivekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivekHistory] ADD  CONSTRAINT [DF_EREC_IraElosztoivekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraElosztoivTetelekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivTetelekHistory] ADD  CONSTRAINT [DF_EREC_IraElosztoivTetelekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraIratokDokumentumokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraIratokDokumentumokHistory] ADD  CONSTRAINT [DF_EREC_IraIratokDokumentumokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraJegyzekekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekekHistory] ADD  CONSTRAINT [DF_EREC_IraJegyzekekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraJegyzekTetelekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekTetelekHistory] ADD  CONSTRAINT [DF_EREC_IraJegyzekTetelekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraKezFeljegyzesekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraKezFeljegyzesekHistory] ADD  CONSTRAINT [DF_EREC_IraKezFeljegyzesekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IratelemKapcsolatokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratelemKapcsolatokHistory] ADD  CONSTRAINT [DF_EREC_IratelemKapcsolatokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IratKapcsolatokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratKapcsolatokHistory] ADD  CONSTRAINT [DF_EREC_IratKapcsolatokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IratMellekletekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratMellekletekHistory] ADD  CONSTRAINT [DF_EREC_IratMellekletekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__Histo__5535A963]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariHelyekHistory] ADD  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IrattariKikeroHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariKikeroHistory] ADD  CONSTRAINT [DF_EREC_IrattariKikeroHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldBekuldokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldBekuldokHistory] ADD  CONSTRAINT [DF_EREC_KuldBekuldokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldDokumentumokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldDokumentumokHistory] ADD  CONSTRAINT [DF_EREC_KuldDokumentumokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldKapcsolatokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKapcsolatokHistory] ADD  CONSTRAINT [DF_EREC_KuldKapcsolatokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldKezFeljegyzesekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKezFeljegyzesekHistory] ADD  CONSTRAINT [DF_EREC_KuldKezFeljegyzesekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldMellekletekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldMellekletekHistory] ADD  CONSTRAINT [DF_EREC_KuldMellekletekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldTertivevenyekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldTertivevenyekHistory] ADD  CONSTRAINT [DF_EREC_KuldTertivevenyekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_Obj_MetaXmlHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Obj_MetaXmlHistory] ADD  CONSTRAINT [DF_EREC_Obj_MetaXmlHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_PldKapjakMegHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_PldKapjakMegHistory] ADD  CONSTRAINT [DF_EREC_PldKapjakMegHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_SzamlakHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_SzamlakHistory] ADD  CONSTRAINT [DF_EREC_SzamlakHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_UgyiratKapcsolatokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatokHistory] ADD  CONSTRAINT [DF_EREC_UgyiratKapcsolatokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_UgyKezFeljegyzesekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyKezFeljegyzesekHistory] ADD  CONSTRAINT [DF_EREC_UgyKezFeljegyzesekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_AlairtDokumentumokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_AlairtDokumentumokHistory] ADD  CONSTRAINT [DF_KRT_AlairtDokumentumokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_AlkalmazasokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_AlkalmazasokHistory] ADD  CONSTRAINT [DF_KRT_AlkalmazasokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_BankszamlaszamokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_BankszamlaszamokHistory] ADD  CONSTRAINT [DF_KRT_BankszamlaszamokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_BarkodSavokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_BarkodSavokHistory] ADD  CONSTRAINT [DF_KRT_BarkodSavokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_DokuAlairasKapcsolatokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokuAlairasKapcsolatokHistory] ADD  CONSTRAINT [DF_KRT_DokuAlairasKapcsolatokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_DokumentumKapcsolatokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatokHistory] ADD  CONSTRAINT [DF_KRT_DokumentumKapcsolatokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Erintett_TablakHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Erintett_TablakHistory] ADD  CONSTRAINT [DF_KRT_Erintett_TablakHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Extra_NapokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Extra_NapokHistory] ADD  CONSTRAINT [DF_KRT_Extra_NapokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Felhasznalok_Halozati_NyomtatoiHistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory] ADD  CONSTRAINT [DF_KRT_Felhasznalok_Halozati_NyomtatoiHistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felhasz__Ver__3B40CD36]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory] ADD  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_FelhasznaloProfilokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloProfilokHistory] ADD  CONSTRAINT [DF_KRT_FelhasznaloProfilokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_FunkcioListaHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioListaHistory] ADD  CONSTRAINT [DF_KRT_FunkcioListaHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Halozati_NyomtatokHistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Halozati_NyomtatokHistory] ADD  CONSTRAINT [DF_KRT_Halozati_NyomtatokHistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Halozat__Ver__489AC854]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Halozati_NyomtatokHistory] ADD  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_KodCsoportokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_KodCsoportokHistory] ADD  CONSTRAINT [DF_KRT_KodCsoportokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_KozteruletekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_KozteruletekHistory] ADD  CONSTRAINT [DF_KRT_KozteruletekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_KozteruletTipusokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_KozteruletTipusokHistory] ADD  CONSTRAINT [DF_KRT_KozteruletTipusokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_MappakHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappakHistory] ADD  CONSTRAINT [DF_KRT_MappakHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_MappaTartalmakHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaTartalmakHistory] ADD  CONSTRAINT [DF_KRT_MappaTartalmakHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_MappaUtakHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaUtakHistory] ADD  CONSTRAINT [DF_KRT_MappaUtakHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Modul_FunkcioHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Modul_FunkcioHistory] ADD  CONSTRAINT [DF_KRT_Modul_FunkcioHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_MuveletekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MuveletekHistory] ADD  CONSTRAINT [DF_KRT_MuveletekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_NezetekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_NezetekHistory] ADD  CONSTRAINT [DF_KRT_NezetekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_OrgokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_OrgokHistory] ADD  CONSTRAINT [DF_KRT_OrgokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_OrszagokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_OrszagokHistory] ADD  CONSTRAINT [DF_KRT_OrszagokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_PartnerMinositesekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesekHistory] ADD  CONSTRAINT [DF_KRT_PartnerMinositesekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_RagSz__Histo__41B8C09B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagSzamokHistory] ADD  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Ragsz__Histo__44952D46]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagszamSavokHistory] ADD  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Tartomanyok_SzervezetekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tartomanyok_SzervezetekHistory] ADD  CONSTRAINT [DF_KRT_Tartomanyok_SzervezetekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_TartomanyokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TartomanyokHistory] ADD  CONSTRAINT [DF_KRT_TartomanyokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_TelepulesekHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TelepulesekHistory] ADD  CONSTRAINT [DF_KRT_TelepulesekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_TemplateManagerHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TemplateManagerHistory] ADD  CONSTRAINT [DF_KRT_TemplateManagerHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Tranz_ObjHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tranz_ObjHistory] ADD  CONSTRAINT [DF_KRT_Tranz_ObjHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_TranzakciokHistory_HistoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TranzakciokHistory] ADD  CONSTRAINT [DF_KRT_TranzakciokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO

---ISOLATION
declare @sql varchar(100)
set @sql='ALTER DATABASE '+quotename(db_name())+' SET ALLOW_SNAPSHOT_ISOLATION ON';

exec(@sql)

GO
