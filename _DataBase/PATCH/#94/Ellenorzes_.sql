
use [CONTENTUM_PROD_MIGR_UPD]
GO

select mf.EdokSav, mf.UI_NUM, mf.UI_YEAR, mf.IRJ2000, mf.UGYHOL, mf.Selejtezes_Datuma, mf.Ugyirat_tipus, mf.* 
  from [dbo].MIG_FOSZAM mf
 inner join TELJES tt on tt.ITSZ_MIG_ID = mf.ID
 order by mf.EdokSav, mf.UI_NUM, mf.UI_YEAR;

select TOP 20 tt.IKTSZ, tt.ITSZ_JO, tt.SDATUM, tt.ITSZ_Modositas, tt.ITSZ_MIG_ID, tt.IT_JO, tt.* 
  from [dbo].TELJES tt
 order by tt.SeqId /* tt.IKTSZ */;

select mfh.EdokSav, mfh.UI_NUM, mfh.UI_YEAR, mfh.IRJ2000, mfh.UGYHOL, mfh.Selejtezes_Datuma, mfh.Ugyirat_tipus, mfh.* 
  from [dbo].MIG_FOSZAMHISTORY mfh
 inner join TELJES tt on tt.ITSZ_MIG_ID = mfh.ID
 order by mfh.EdokSav, mfh.UI_NUM, mfh.UI_YEAR;

/*

-- print '.. Iktatószámonkénti darabszám ..'
select '' as 'IKTATOSZAMonkenti_darabszam', mf.IRJ2000, count(1) as uj_ITSZ_DB 
  from MIG_Foszam mf
 where exists ( select 1 from TELJES tt where tt.ITSZ_MIG_ID = mf.ID )
 group by mf.IRJ2000
 order by 1,2

-- print '.. ''S''elejtezettek darabszáma ..'
select count(1) as Selejtezettek_darabszama 
  from MIG_Foszam mf
 where mf.UGYHOL = 'S'
   and exists ( select 1 from TELJES tt where tt.ITSZ_MIG_ID = mf.ID )

-- print '.. Sikertelen update ..'
select '' as SIKERTELEN_Update,tt.ITSZ_Modositas, tt.* 
  from TELJES tt
 where tt.ITSZ_Modositas is NOT NULL and tt.ITSZ_Modositas <> ''
   and tt.ITSZ_MIG_ID is Null

*/

-- tábla beolvasása
/*
select COUNT(1) from TELJES_b;
select * from TELJES_b where IKTSZ = 'N/2/1989';
select COUNT(1) from TELJES_m;
select * from TELJES_m where IKTSZ = 'II/3-1/2003';
select COUNT(1) from TELJES_e;
select * from TELJES_e where IKTSZ = 'XIV/1671/2011';
*/  