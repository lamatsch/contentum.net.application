
A kapott mapping t�bla bet�lt�se
........................................
- a TELJES.csv -t az SSSMS seg�ts�g�vel t�lt�ttem be az ax-vfphtst02/CONTENTUM_PROD_MIGR_UPD adatb�zisba
- a nev�t nem v�ltoztattam meg: TELJES nev� t�bl�t hozott l�tre a bet�lt� 
- b�r Peti mutatta, de nekem NEM engedte elmenteni a bet�lt� scriptet

A bet�lt�s folyamata:
- a C:\BOPMH_export\TELJES.csv ker�lt bet�lt�sre
- adatb�zis n�ven jobb Click\ Tasks \ Import Data men�b�l indul el a var�zsl�
Next
- Data source = Flat File Source
- File Name = ki kell v�lasztani TELJES.csv -t ( ! az xls nem j�, mert az Excel m�r a csv beolvas�sakor eleve elrontja a tartalmat pl. ITSZ_JO �rt�k�t )
- Text qualifier = " ( egyetlen darab id�z�jel )
- 'Column names in the first data row' - checked
- oldals� men�n Advanced lap: minden mez�n�l m�dos�tani kell az OutputColumnWidth �rt�ket
.. IT, IT_JO, AGAZAT, AGAZAT_JO, TARGY, SORSZAM, SIKSZ, SMEGJ mez�k eset�ben = 500
.. SDATUM = marad 50
.. minden egy�b mez� eset�ben = 100
.. minden egy�b param�ter marad a megaj�nlott ( ! a DataType is )
- ! Suggest Types -t v�letlen�l se ind�tsd el
NEXT
- Destination = SQL Server Native Client xx.0
Next
- Edit Mappings
.. Create destination table
.. minden adatmez� Type = nvarchar
.. ahol Size = 500, azt �t kell �ll�tani = max -ra
.. Edit SQL / Auto generate / OK
.. OK
Next
- Review Data Type Mapping ablakon az OnError �s az OnTruncation oszlopokat 
.. Use Global ( als� soron mindkett� legyen = Fail ) �rt�kre kell �ll�tani a: IKTSZ, ITSZ_JO, IT_JO, SDATUM mez�kn�l
.. (sajnos egyenk�nt) Ignore �rt�kre kell �ll�tani minden egy�b adatmez�n�l
Next
Finish - �s p�r perc alatt benyalta a TELJES t�bl�t ( sicc! )

Update script
..................
- Update_MIG_Foszam_ITSZ.sql
.. a TELJES t�bl�t haszn�lja, amelynek minden adatmez�je nvarchar t�pus�
.. a script fej�ben benne van az adatb�zis hivatkoz�s: az �les fut�sn�l ki kell t�r�lni/vagy �t kell �rni
.. a cursor defin�ci�ban TOP 20 t�telre van korl�tozva a fut�s ( erre v�geztem el a tesztet ) - term�szetesen ezt lehet n�velni ill. az �lesen ki kell venni
.. a script ak�rh�nyszor futtathat�, mind�g a TELJES t�bla ITSZ_Modositas, ITSZ_MIG_ID adatnez�ibe �r a script
.. a script v�g�n lefut n�h�ny ellen�rz� select, de az Ellenorzes_.sql script-ben tov�bbi ellen�rz�sek futathat�k


