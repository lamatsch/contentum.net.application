
USE [CONTENTUM_PROD_MIGR_UPD]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

print '---'
print '/* --- MIG_Foszam t�bla iktat�sz�m update TELJES t�bla alapj�n --- */'
print '---'

print '- TELJES t�bla -'
if not exists 
	(select 1 from  sysobjects
	  where name = 'TELJES' 
	    and xtype = 'U'
		and id = object_id('TELJES'))
  begin
    print '- A TELJES t�bla hi�nyzik - az iktat�sz�m update NEM futtathat� ! -'
	--- GOTO EndOfScript
  end
GO

print '.. t�bla m�dos�t�sa ..'

if exists 
	(select 1 from  sysobjects
      where name = 'TELJES_PK' 
	    and xtype = 'PK'
		and parent_obj = object_id('TELJES'))
  ALTER TABLE [dbo].TELJES DROP constraint TELJES_PK

if not exists 
	(select 1 from  syscolumns 
		where name = 'ITSZ_Modositas' 
		and id = object_id('TELJES'))
  ALTER TABLE [dbo].TELJES ADD ITSZ_Modositas nvarchar(MAX)

if not exists 
	(select 1 from  syscolumns 
		where name = 'ITSZ_MIG_ID' 
		and id = object_id('TELJES'))
  ALTER TABLE [dbo].TELJES ADD ITSZ_MIG_ID uniqueidentifier
GO

print '.. SeqId kit�lt�se a t�bl�ban ..'
if exists 
	(select 1 from  syscolumns 
      where name = 'SeqID' 
		and id = object_id('TELJES'))
  ALTER TABLE [dbo].TELJES DROP COLUMN SeqID
GO

ALTER TABLE [dbo].TELJES ADD SeqID bigint Identity(101,1)
GO

ALTER TABLE [dbo].TELJES alter column SeqID bigint NOT NULL
GO

ALTER TABLE [dbo].[TELJES]
  ADD CONSTRAINT [TELJES_PK] PRIMARY KEY CLUSTERED 
    (    [SeqID] ASC
    )
GO

print '... minden fut�sn�l �res lappal indulunk ...'
update TELJES
   SET ITSZ_Modositas = '', ITSZ_MIG_ID = convert(uniqueidentifier,Null)                       

print ''
print '- MIG_FOSZAM t�bla update -'

DECLARE cur_teljes CURSOR FOR
         SELECT TOP 20 IKTSZ, ITSZ, ITSZ_JO, IT_JO, SDATUM, SeqId /*, ITSZ_Modositas, ITSZ_MIG_ID */ from [dbo].TELJES order by SeqId  /* IKTSZ */
		    FOR UPDATE OF ITSZ_Modositas, ITSZ_MIG_ID
DECLARE @IKTSZ   nvarchar(100),
        @ITSZ    nvarchar(100),
        @ITSZ_JO nvarchar(100),
		@IT_JO   nvarchar(MAX),
		@SDATUM  nvarchar(50),
		@SeqId   bigint,
		@ITSZ_Modositas nvarchar(MAX)
DECLARE @Maxseqid  bigint,
		@IktKonyv  nvarchar(20),
		@IktFoszam nvarchar(20),                      --- kell a hossz, mert haszn�lom IKTSZ felbont�sakor �s akkor lehet 10 karaktern�l hosszabb is
		@IktEv     nvarchar(10),
		@nsper     int,
		@nskot     int
DECLARE @ITSZ_MIG_ID uniqueidentifier,
        @IRJ2000 nvarchar(20),
        @UGYHOL nvarchar(1),
		@Selejtezes_datuma datetime,
        @p xml,
		@AxisUser uniqueidentifier,
		@ExecTime datetime

--- BEGIN TRY        --- egyszer�s�tett 'hibakezel�s' lett be�p�tve -> nem halunk meg, hanem tov�bbl�p�nk
BEGIN

  SET @Maxseqid = ( select max(SeqID) from TELJES )
  if @Maxseqid = 0
    begin
      print '- A TELJES t�bl�nak nincs egyetlen sora sem - az iktat�sz�m update NEM futtathat� ! -'
	  RETURN
	  --- GOTO EndOfScript
	end

  SET @p=convert(xml,N'<root><IRJ2000/><UGYHOL/><Selejtezes_Datuma/><Ver/></root>')
  SET @AxisUser = convert(uniqueidentifier,'54E861A5-36ED-44CA-BAA7-C287D125B309')                     --- Itt igaz�b�l ki kellene venni - de mib�l ??? - 'bopmh\axis' felhaszn�l� azonos�t�j�t

  OPEN cur_teljes;
  FETCH NEXT FROM cur_teljes 
             INTO @IKTSZ, @ITSZ, @ITSZ_JO, @IT_JO, @SDATUM, @SeqId/*, @ITSZ_Modositas, @MIG_ID */

  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    -- print convert( char(10), @SeqId ) + ' / ' + convert( char(10), @Maxseqid )
	SET @ITSZ_Modositas = ''

	-- egy�ltal�n van �j iktat�sz�munk ?
	if @ITSZ_JO is Null or @ITSZ_JO = ''
	  SET @ITSZ_Modositas += 'A TELJES t�bl�ban NINCS megadva az �j iktat�sz�m (ITSZ_JO is Null); '
	else
	  begin

    	--- �llitsuk el� a keres�si kulcsokat
	    SET @nsper = CHARINDEX('/',@IKTSZ)                                        -- egy darab '/' mind�g van benne
	    SET @IktKonyv = rtrim(substring(@IKTSZ, 1, @nsper-1))
	    SET @nskot = CHARINDEX('-', @IktKonyv)
	    if @nskot > 0                                                             -- <iktatokonyv>-<foszam>/<�v> t�pus� megad�s
	      begin
	        SET @IktFoszam = substring(@IktKonyv, @nskot+1, len(@IktKonyv))        -- UI_NUM
		    SET @IktKonyv = rtrim(substring(@IktKonyv, 1, @nskot-1))                -- EdokSav
		    SET @IktEv = substring(@IKTSZ, @nsper+1, len(@IKTSZ))                  -- UI_YEAR
          end
	    else	-- ekkor @IktKonyv rendben van                                    -- EdokSav 
	      begin
	        SET @IktFoszam = substring(@IKTSZ, @nsper+1, len(@IKTSZ))
            SET @nsper = CHARINDEX('/', @IktFoszam)
		    SET @IktEv = substring(@IktFoszam, @nsper+1, len(@IktFoszam))          -- UI_YEAR

		    SET @nskot = CHARINDEX('-', @IktFoszam)
            if @nskot > 0                                                         -- <iktatokonyv>/<foszam>-<alszam>/<�v> t�pus� megad�s
              SET @IktFoszam = substring(@IktFoszam, 1, @nskot-1 )                 -- UI_NUM
            else                                                                  -- <iktatokonyv>/<foszam>/<�v> t�pus� megad�s     
              SET @IktFoszam = substring(@IktFoszam, 1, @nsper-1 )                 -- UI_NUM
	      end
	    
		--- akkor keress�k is meg
	    SET @ITSZ_MIG_ID = convert(uniqueidentifier,Null);
	    SET @ITSZ_MIG_ID = ( select ID from MIG_FOSZAM mf 
	                     where mf.EdokSav = @IktKonyv
	                       and mf.UI_NUM = convert( int, @IktFoszam)
	                       and mf.UI_YEAR = convert( int, @IktEv) )
        if @ITSZ_MIG_ID is Null
	      SET @ITSZ_Modositas += 'A MIG_FOSZAM t�bl�ban NEM tal�lhat� meg a sor (IKTSZ); '
	    else
		  begin
            select @IRJ2000 = mf.IRJ2000
	          from MIG_FOSZAM mf 
		     where ID = @ITSZ_MIG_ID

	        if @SDATUM is Null or @SDATUM = ''
		      begin
		        select @UGYHOL = mf.UGYHOL, @Selejtezes_datuma = mf.Selejtezes_datuma                  --- isnull(mf.Selejtezes_datuma, convert(datetime,Null))
			      from MIG_FOSZAM mf 
			     where ID = @ITSZ_MIG_ID
		        /*
		        SET @UGYHOL = ( select UGYHOL from MIG_FOSZAM mf 
	                             where ID = @ITSZ_MIG_ID )
		        SET @Selejtezes_datuma = ( select Selejtezes_datuma from MIG_FOSZAM mf 
	                                        where ID = @ITSZ_MIG_ID )
                */
		      end
            else
		      begin
		        SET @UGYHOL = 'S'
		        SET @Selejtezes_datuma = CONVERT(datetime, @SDATUM, 120)
		      end

            SET @ExecTime = getdate()                              --- mert egy�bk�nt nem tudom �tadni param�terk�nt !?
	        --- akkor update-elj�k meg MIG_FOSZAM-ot
            exec [sp_MIG_FoszamUpdate] 
			     @IRJ2000 = @ITSZ_JO,
				 @UGYHOL  = @UGYHOL,
				 @Selejtezes_Datuma = @Selejtezes_datuma,
				 @Ver = 0,                                               /* nem kell vmi verzi�sz�m ??? */
				 @UpdatedColumns = @p,
				 @ExecutorUserId = @AxisUser,
				 @ExecutionTime = @ExecTime,
				 @id = @ITSZ_MIG_ID

	        if @@ERROR != 0                                                --- ha ennyi hibakezel�s nem el�g, akkor TRY / CATCH blokkok kellenek
	          SET @ITSZ_Modositas += 'MIG_FOSZAM sor m�dos�t�sa NEM siker�lt; '
            else
			  begin
	            SET @ITSZ_Modositas += 'MIG_FOSZAM sor m�dos�t�sa megt�rt�nt (r�gi IRJ200=' + @IRJ2000 + ' -> IRJ2000=' + @ITSZ_JO + '); '
                --- r�gi ITSZ egyez�s�g�nek ellen�rz�se
				/* 
	      		if ( @ITSZ <> @IRJ2000 ) and ( @ITSZ_JO = @IRJ2000 )
				  SET @ITSZ_Modositas += '(m�r egyszer jav�tott sor)'
                */ 
	      		if ( @ITSZ <> @IRJ2000 ) and ( @ITSZ_JO <> @IRJ2000 )
				  SET @ITSZ_Modositas += '(eredeti ITSZ �rt�kek elt�rtek ITSZ=' + @ITSZ + ', IRJ2000=' + @IRJ2000 + ')'
			  end
          end                       -- @ITSZ_MIG_ID is NOT Null
	  end                           -- @ITSZ_JO tartalmaz �rt�ket   
	print convert( char(10), @SeqId ) + ' / ' + convert( char(10), @Maxseqid ) + ' - ' + @ITSZ_Modositas

    --- ??? Lehet, hogy ide m�gegyszer kellene az 'if @@ERROR != 0 ... ' - a saj�t hib�k miatti kiakad�sok kezel�s�re ???
		
	--- �rjuk vissza TELJES-be, hogy mik�nt siker�lt a m�dos�t�s
	-- SET @ITSZ_Modositas = convert(char(10), getdate(),102) + ': ' + @ITSZ_Modositas
	SET @ITSZ_Modositas = convert(char(25), getdate(),120) + ': ' + @ITSZ_Modositas
    UPDATE TELJES SET ITSZ_Modositas = @ITSZ_Modositas, ITSZ_MIG_ID = @ITSZ_MIG_ID WHERE CURRENT OF cur_teljes
  	
    FETCH NEXT FROM cur_teljes 
               INTO @IKTSZ, @ITSZ, @ITSZ_JO, @IT_JO, @SDATUM, @SeqId/*, @ITSZ_Modositas, @ITSZ_MIG_ID */
  END                                   -- cursor ciklus v�ge
  CLOSE cur_teljes
  DEALLOCATE cur_teljes

END
/*
END TRY

BEGIN CATCH  
    DECLARE @ErrorMessage NVARCHAR(4000);  
    DECLARE @ErrorSeverity INT;  
    DECLARE @ErrorState INT;  

    SELECT   
        @ErrorMessage = ERROR_MESSAGE(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorState = ERROR_STATE();

    print 'A MIG_FOSZAM iktat�sz�m update - HIB�val megszakadt: ' + @ErrorMessage
    RAISERROR (@ErrorMessage, -- Message text.  
               @ErrorSeverity, -- Severity.  
               @ErrorState -- State.  
               );  
END CATCH
*/
GO

print '- Ellen�rz� lek�rdez�sek -'
-- print '.. Iktat�sz�monk�nti darabsz�m ..'
select '' as 'IKTATOSZAMonkenti_darabszam', mf.IRJ2000, count(1) as uj_ITSZ_DB 
  from MIG_Foszam mf
 where exists ( select 1 from TELJES tt where tt.ITSZ_MIG_ID = mf.ID )
 group by mf.IRJ2000
 order by 1,2

-- print '.. ''S''elejtezettek darabsz�ma ..'
select count(1) as Selejtezettek_darabszama 
  from MIG_Foszam mf
 where mf.UGYHOL = 'S'
   and exists ( select 1 from TELJES tt where tt.ITSZ_MIG_ID = mf.ID )

-- print '.. Sikertelen update ..'
select '' as SIKERTELEN_Update,tt.ITSZ_Modositas, tt.* 
  from TELJES tt
 where tt.ITSZ_Modositas is NOT NULL and tt.ITSZ_Modositas <> ''
   and tt.ITSZ_MIG_ID is Null
/*
print '.. T�bbsz�r m�dos�tott sorok ..'
select tt.IKTSZ, tt.ITSZ_MIG_ID, count(1) as TELJES_sor_DB
  from TELJES tt
 group by tt.IKTSZ, tt.ITSZ_MIG_ID
 order by tt.IKTSZ, tt.ITSZ_MIG_ID
*/

/******
 
select mf.EdokSav, mf.UI_NUM, mf.UI_YEAR, mf.IRJ2000, mf.UGYHOL, mf.Selejtezes_Datuma, mf.Ugyirat_tipus, mf.* 
  from [dbo].MIG_FOSZAM mf
 inner join TELJES tt on tt.ITSZ_MIG_ID = mf.ID
 order by mf.EdokSav, mf.UI_NUM, mf.UI_YEAR;

select TOP 20 tt.IKTSZ, tt.ITSZ_JO, tt.SDATUM, tt.ITSZ_Modositas, tt.ITSZ_MIG_ID, tt.IT_JO, tt.* 
  from [dbo].TELJES tt
 order by tt.IKTSZ;

******/


--- EndOfScript:
print ''
print '/* --- V�ge --- */'