
USE [CONTENTUM_PROD_MIGR_UPD]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

print '---'
print '/* --- MIG_Foszam t�bla UI_IRJ update IRJ2000 �rt�k�vel, ha UI_YEAR < 2000 --- */'
print '---'

print ''
print '- Ellen�rz� lek�rdez�sek -'
select count(1) as Eltero_IRJ_szama from [dbo].MIG_FOSZAM f
 where 1 = 1
   and f.UI_YEAR < 2000 
   and f.UI_IRJ <> f.IRJ2000
GO

print ''
print '- MIG_FOSZAM t�bla update -'

DECLARE cur_foszam CURSOR FOR
         SELECT /*TOP 5*/ ID, UI_IRJ, IRJ2000 from [dbo].MIG_FOSZAM f
		  where 1 = 1 and f.UI_YEAR < 2000 and f.UI_IRJ <> f.IRJ2000
          order by f.UI_YEAR, f.UI_NUM/*, f.ID*/

DECLARE @ID uniqueidentifier,
        @IRJ2000 nvarchar(20),
		@UI_IRJ nvarchar(20),
        @p xml,
		@AxisUser uniqueidentifier,
		@ExecTime datetime

--- BEGIN TRY        --- egyszer�s�tett 'hibakezel�s' lett be�p�tve -> nem halunk meg, hanem tov�bbl�p�nk
BEGIN

  SET @p=convert(xml,N'<root><UI_IRJ/><Ver/></root>')
  -- SET @AxisUser = convert(uniqueidentifier,'54E861A5-36ED-44CA-BAA7-C287D125B309')                     --- 'admin' felhaszn�l� azonos�t�
  SET @AxisUser = convert(uniqueidentifier,'838DB5BB-B8C6-E611-90F7-0050569A6FBA')                     --- Itt igaz�b�l ki kellene venni - de mib�l ??? - 'bopmh\axis' felhaszn�l� azonos�t�j�t

  OPEN cur_foszam;
  FETCH NEXT FROM cur_foszam INTO @ID, @UI_IRJ, @IRJ2000;

  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    print '-'

	if @IRJ2000 is NOT Null and @IRJ2000 <> ''
	  begin
            SET @ExecTime = getdate();                              --- mert egy�bk�nt nem tudom �tadni param�terk�nt !?
	        --- akkor update-elj�k meg MIG_FOSZAM-ot
            exec [sp_MIG_FoszamUpdate] 
			     @UI_IRJ = @IRJ2000,
				 @Ver = 0,                                               /* nem kell vmi verzi�sz�m ??? */
				 @UpdatedColumns = @p,
				 @ExecutorUserId = @AxisUser,
				 @ExecutionTime = @ExecTime,
				 @id = @ID;

            print convert( nvarchar(40), @ID ) + ' - UI_IRJ: ' + @UI_IRJ + ' -> ' + @IRJ2000;
	  end;
 	
    FETCH NEXT FROM cur_foszam INTO @ID, @UI_IRJ, @IRJ2000;
  END                                   -- cursor ciklus v�ge
  CLOSE cur_foszam
  DEALLOCATE cur_foszam

END
/*
END TRY

BEGIN CATCH  
    DECLARE @ErrorMessage NVARCHAR(4000);  
    DECLARE @ErrorSeverity INT;  
    DECLARE @ErrorState INT;  

    SELECT   
        @ErrorMessage = ERROR_MESSAGE(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorState = ERROR_STATE();

    print 'A MIG_FOSZAM->UI_IRJ mez� update - HIB�val megszakadt: ' + @ErrorMessage
    RAISERROR (@ErrorMessage, -- Message text.  
               @ErrorSeverity, -- Severity.  
               @ErrorState -- State.  
               );  
END CATCH
*/
GO

print ''
print '- Ellen�rz� lek�rdez�sek -'
select count(1) as Eltero_IRJ_szama from [dbo].MIG_FOSZAM f
 where 1 = 1
   and f.UI_YEAR < 2000 
   and f.UI_IRJ <> f.IRJ2000

/*
select * from [dbo].MIG_FOSZAM f
 where 1 = 1
   and f.UI_YEAR < 2000 
   and f.UI_IRJ <> f.IRJ2000

-----

select * from [dbo].MIG_FoszamHistory f
 where HistoryVegrehajtasIdo > getdate()-1
 order by f.HistoryVegrehajtasIdo desc
*/
GO

print ''
print '/* --- V�ge --- */'
