if exists (select 1
            from  sysobjects
           where  id = object_id('KRT_KodtarFuggoseg')
            and   type = 'U')
   drop table KRT_KodtarFuggoseg
go

/*==============================================================*/
/* Table: KRT_KodtarFuggoseg                                    */
/*==============================================================*/
create table KRT_KodtarFuggoseg (
   Id                   uniqueidentifier     not null,
   Org                  uniqueidentifier     null,
   Vezerlo_KodCsoport_Id uniqueidentifier     not null,
   Fuggo_KodCsoport_Id  uniqueidentifier     not null,
   Adat                 Nvarchar(Max)        null,
   Aktiv                char(1)              null default '1',
   Ver                  int                  null default 1,
   Note                 Nvarchar(4000)       null,
   Stat_id              uniqueidentifier     null,
   ErvKezd              datetime             null default Getdate(),
   ErvVege              datetime             null,
   Letrehozo_id         uniqueidentifier     null,
   LetrehozasIdo        datetime             not null default Getdate(),
   Modosito_id          uniqueidentifier     null,
   ModositasIdo         datetime             null,
   Zarolo_id            uniqueidentifier     null,
   ZarolasIdo           datetime             null,
   Tranz_id             uniqueidentifier     null,
   UIAccessLog_id       uniqueidentifier     null,
   constraint PK_KRT_KODTARFUGGOSEG primary key (Id)
)
go

alter table KRT_KodtarFuggoseg 
  ADD CONSTRAINT
	DF_KRT_KodtarFuggoseg_ErvVege DEFAULT CONVERT(datetime, '4700-12-31', 102) FOR ErvVege;
go


alter table KRT_KodtarFuggoseg
   add constraint FK_KRT_KTFU_KCSVEZ foreign key (Fuggo_KodCsoport_Id)
      references KRT_KodCsoportok (Id)
go

alter table KRT_KodtarFuggoseg
   add constraint FK_KRT_KTFU_KCSGUGG foreign key (Vezerlo_KodCsoport_Id)
      references KRT_KodCsoportok (Id)
go
