-- =========================================
-- Create History table for KRT_KodtarFuggoseg
-- =========================================
 
IF OBJECT_ID('KRT_KodtarFuggosegHistory', 'U') IS NOT NULL
  DROP TABLE KRT_KodtarFuggosegHistory
GO
 
CREATE TABLE KRT_KodtarFuggosegHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 Org uniqueidentifier,  
  Vezerlo_KodCsoport_Id uniqueidentifier,
  
  Vezerlo_KodCsoport_Id_Ver int,  
  Fuggo_KodCsoport_Id uniqueidentifier,
  
  Fuggo_KodCsoport_Id_Ver int,  
 Adat Nvarchar(Max),  
 Aktiv char(1),  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_KRT_KodtarFuggosegHistory_ID_VER ON KRT_KodtarFuggosegHistory
(
	Id,Ver ASC
)
GO