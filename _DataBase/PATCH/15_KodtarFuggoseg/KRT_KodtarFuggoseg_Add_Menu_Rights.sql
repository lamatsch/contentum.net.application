BEGIN TRANSACTION AddMenuItem;  
GO  
		--TIPUSOK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier=	null
		DECLARE @ObjTipus_Id_Tipus uniqueidentifier=	null
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Obj_Id_Szulo uniqueidentifier	=	null
		DECLARE @KodCsoport_Id uniqueidentifier=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id			='3D83B087-DE83-490D-A44C-ADDDDDE48D20'
		set @Kod		='KRT_KodtarFuggoseg'
		set @Nev		='KRT_KodtarFuggoseg t�bla'
		EXECUTE @RC = [dbo].[sp_KRT_ObjTipusokInsert] 
		   @Id
		  ,@ObjTipus_Id_Tipus
		  ,@Kod
		  ,@Nev
		  ,@Obj_Id_Szulo
		  ,@KodCsoport_Id
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--TIPUSOK END
		-----------------------------------------------------------------------
		--FUNKCI�K
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= 'CFCBD8A8-CC4B-48DA-9F52-DDDCC3355A01'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '5B9AC7B3-879C-48A7-BEBA-EF73A49AF893'
		set @ObjTipus_Id_AdatElem	= '3D83B087-DE83-490D-A44C-ADDDDDE48D20'
		set @Kod					= 'KRT_KodtarFuggosegKezeles'
		set @Nev					= 'K�dt�r f�gg�s�g'

		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCI�K END

		--FUNKCI�K
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= 'CFCBD8A8-CC4B-48DA-9F52-DDDCC3355A02'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '5B9AC7B3-879C-48A7-BEBA-EF73A49AF893'
		set @ObjTipus_Id_AdatElem	= '3D83B087-DE83-490D-A44C-ADDDDDE48D20'
		set @Kod					= 'KRT_KodtarFuggosegModify'
		set @Nev					= 'K�dt�r f�gg�s�g m�dos�t�sa'

		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCI�K END

		--FUNKCI�K
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= 'CFCBD8A8-CC4B-48DA-9F52-DDDCC3355A03'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '939A0831-2914-4348-93FA-9DED0225747E'
		set @ObjTipus_Id_AdatElem	= '3D83B087-DE83-490D-A44C-ADDDDDE48D20'
		set @Kod					= 'KRT_KodtarFuggosegNew'
		set @Nev					= 'K�dt�r f�gg�s�g l�trehoz�sa'

		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCI�K END

		--FUNKCI�K
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= 'CFCBD8A8-CC4B-48DA-9F52-DDDCC3355A04'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '9508908E-AEFA-41C6-B6D7-807E3AE3A3AB'
		set @ObjTipus_Id_AdatElem	= '3D83B087-DE83-490D-A44C-ADDDDDE48D20'
		set @Kod					= 'KRT_KodtarFuggosegList'
		set @Nev					= 'K�dt�r f�gg�s�gi lista'

		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCI�K END
	
		--FUNKCI�K
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= 'CFCBD8A8-CC4B-48DA-9F52-DDDCC3355A05'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '78F0D5A2-E048-4A1F-9F11-5D57AB78B985'
		set @ObjTipus_Id_AdatElem	= '3D83B087-DE83-490D-A44C-ADDDDDE48D20'
		set @Kod					= 'KRT_KodtarFuggosegView'
		set @Nev					= 'K�dt�r f�gg�s�g megtekint�se'

		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCI�K END

		--FUNKCI�K
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= 'CFCBD8A8-CC4B-48DA-9F52-DDDCC3355A06'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '805C7634-F205-4AE7-8580-DB3C73D8F43B'
		set @ObjTipus_Id_AdatElem	= '3D83B087-DE83-490D-A44C-ADDDDDE48D20'
		set @Kod					= 'KRT_KodtarFuggosegInvalidate'
		set @Nev					= 'K�dt�r f�gg�s�g �rv�nytelen�t�se'

		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCI�K END
		
		--FUNKCI�K
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= 'CFCBD8A8-CC4B-48DA-9F52-DDDCC3355A09'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= 'FBDDC6B8-6671-4BA7-9BDD-885FFC031986'
		set @ObjTipus_Id_AdatElem	= '3D83B087-DE83-490D-A44C-ADDDDDE48D20'
		set @Kod					= 'KRT_KodtarFuggosegLock'
		set @Nev					= 'K�dt�r f�gg�s�g z�rol�sa'

		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCI�K END

		-----------------------------------
		--MODULOK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Org uniqueidentifier=	null
		DECLARE @Tipus nvarchar(10)=	null
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @SelectString nvarchar(4000)=	null
		DECLARE @Csoport_Id_Tulaj uniqueidentifier=	null
		DECLARE @Parameterek nvarchar(4000)=	null
		DECLARE @ObjTip_Id_Adatelem uniqueidentifier=	null
		DECLARE @ObjTip_Adatelem nvarchar(100)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		SET @Id			='CFCBD8A8-CC4B-48DA-9F52-DDDCC3355A07'
		SET @Kod		='KodtarFuggosegList.aspx'
		SET @Nev		='K�dt�r f�gg�s�g kezel�se'
		SET @Tipus		='F'

		EXECUTE @RC = [dbo].[sp_KRT_ModulokInsert] 
		   @Id
		  ,@Org
		  ,@Tipus
		  ,@Kod
		  ,@Nev
		  ,@Leiras
		  ,@SelectString
		  ,@Csoport_Id_Tulaj
		  ,@Parameterek
		  ,@ObjTip_Id_Adatelem
		  ,@ObjTip_Adatelem
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--MODULOK END
		-----------------------------------
		--MENUK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Org uniqueidentifier=	null
		DECLARE @Menu_Id_Szulo uniqueidentifier=	null
		DECLARE @Kod nvarchar(20)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Funkcio_Id uniqueidentifier=	null
		DECLARE @Modul_Id uniqueidentifier=	null
		DECLARE @Parameter nvarchar(400)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ImageURL nvarchar(400)=	null
		DECLARE @Sorrend nvarchar(100)=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @id				= 'CFCBD8A8-CC4B-48DA-9F52-DDDCC3355A08'
		set @Menu_Id_Szulo	= 'AA1421B9-0498-4CA3-B075-60F699CB1B7C'
		set @Kod			= 'eAdmin'
		set @Nev			= 'K�dt�r f�gg�s�g kezel�se'
		set @Funkcio_Id		= 'CFCBD8A8-CC4B-48DA-9F52-DDDCC3355A06'
		set @Modul_Id		= 'CFCBD8A8-CC4B-48DA-9F52-DDDCC3355A07'
		set @Sorrend		= 81

		EXECUTE @RC = [dbo].[sp_KRT_MenukInsert] 
		   @Id
		  ,@Org
		  ,@Menu_Id_Szulo
		  ,@Kod
		  ,@Nev
		  ,@Funkcio_Id
		  ,@Modul_Id
		  ,@Parameter
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ImageURL
		  ,@Sorrend
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--MENUK END

GO  
COMMIT TRANSACTION AddMenuItem;  
GO 