
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_LogKRT_KodtarFuggosegHistory')
            and   type = 'P')
   drop procedure sp_LogKRT_KodtarFuggosegHistory
go

create procedure sp_LogKRT_KodtarFuggosegHistory
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from KRT_KodtarFuggoseg where Id = @Row_Id;
          declare @Vezerlo_KodCsoport_Id_Ver int;
       -- select @Vezerlo_KodCsoport_Id_Ver = max(Ver) from KRT_KodCsoportok where Id in (select Vezerlo_KodCsoport_Id from #tempTable);
              declare @Fuggo_KodCsoport_Id_Ver int;
       -- select @Fuggo_KodCsoport_Id_Ver = max(Ver) from KRT_ObjTipusok where Id in (select Fuggo_KodCsoport_Id from #tempTable);
          
   insert into KRT_KodtarFuggosegHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Org     
 ,Vezerlo_KodCsoport_Id       
 ,Vezerlo_KodCsoport_Id_Ver     
 ,Fuggo_KodCsoport_Id       
 ,Fuggo_KodCsoport_Id_Ver     
 ,Adat     
 ,Aktiv     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Org          
 ,Vezerlo_KodCsoport_Id            
 ,@Vezerlo_KodCsoport_Id_Ver     
 ,Fuggo_KodCsoport_Id            
 ,@Fuggo_KodCsoport_Id_Ver     
 ,Adat          
 ,Aktiv          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go