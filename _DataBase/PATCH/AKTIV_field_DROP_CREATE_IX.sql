SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

GO

print ''
print '---'
print ''

/*==============================================================*/
/* Table: EREC_UgyUgyiratok                                     */
/*==============================================================*/
if EXISTS  
   ( select 1 
       from  sysindexes 
	  where name = 'IX_EREC_UgyUgyiratok_Aktiv' 
		and id = object_id('EREC_UgyUgyiratok') )
  begin
    DROP INDEX [IX_EREC_UgyUgyiratok_Aktiv] ON [dbo].[EREC_UgyUgyiratok]
	print 'IX_EREC_UgyUgyiratok_Aktiv INDEX DROPped';
  end;
  GO


if EXISTS  
   ( select 1 
       from  syscolumns 
	  where name = 'AKTIV' 
		and id = object_id('EREC_UgyUgyiratok') )
  begin
    ALTER TABLE [dbo].EREC_UgyUgyiratok
     DROP column AKTIV;
	print 'EREC_UgyUgyiratok.AKTIV mez� DROPped';
  end;
  GO

if NOT EXISTS 
   ( select 1 
       from  syscolumns 
	  where name = 'AKTIV' 
		and id = object_id('EREC_UgyUgyiratok'))
  begin
    ALTER table [dbo].EREC_UgyUgyiratok
      ADD AKTIV AS (cast(case when [ALLAPOT]='90' then (0) else (1) end as bit));

    CREATE index IX_EREC_UgyUgyiratok_Aktiv ON [dbo].[EREC_UgyUgyiratok] (AKTIV,ID);

	print 'EREC_UgyUgyiratok.AKTIV mez� ADDed, INDEX created';
  end;
  GO


/*==============================================================*/
/* Table: EREC_IraIratok                                        */
/*==============================================================*/
if EXISTS  
   ( select 1 
       from  sysindexes 
	  where name = 'IX_EREC_IraIratok_Aktiv' 
		and id = object_id('EREC_IraIratok') )
  begin
    DROP INDEX [IX_EREC_IraIratok_Aktiv] ON [dbo].[EREC_IraIratok]
	print 'IX_EREC_IraIratok_Aktiv INDEX DROPped';
  end;
  GO


if EXISTS  
   ( select 1 
       from  syscolumns 
	  where name = 'AKTIV' 
		and id = object_id('EREC_IraIratok') )
  begin
    ALTER TABLE [dbo].EREC_IraIratok
     DROP column AKTIV;
	print 'EREC_IraIratok.AKTIV mez� DROPped';
  end;
  GO

if NOT EXISTS 
   ( select 1 
       from  syscolumns 
	  where name = 'AKTIV' 
		and id = object_id('EREC_IraIratok'))
  begin
    ALTER table [dbo].EREC_IraIratok
      ADD AKTIV AS (cast(case when [ALLAPOT]='90' then (0) else (1) end as bit));

    CREATE index IX_EREC_IraIratok_Aktiv ON [dbo].[EREC_IraIratok] (AKTIV,ID);

	print 'EREC_IraIratok.AKTIV mez� ADDed, INDEX created';
  end;
  GO

/*==============================================================*/
/* Table: EREC_PldIratPeldanyok                                 */
/*==============================================================*/
if EXISTS  
   (select 1 
       from  sysindexes 
	  where name = 'IX_EREC_PldIratPeldanyok_Aktiv' 
		and id = object_id('EREC_PldIratPeldanyok') )
  begin
    DROP INDEX [IX_EREC_PldIratPeldanyok_Aktiv] ON [dbo].[EREC_PldIratPeldanyok]
	print 'IX_EREC_PldIratPeldanyok_Aktiv INDEX DROPped';
  end;
  GO

if EXISTS  
   ( select 1 
       from  syscolumns 
	  where name = 'AKTIV' 
		and id = object_id('EREC_PldIratPeldanyok') )
  begin
    ALTER TABLE [dbo].EREC_PldIratPeldanyok
     DROP column AKTIV;
	print 'EREC_PldIratPeldanyok.AKTIV mez� DROPped';
  end;
  GO

if NOT EXISTS 
   ( select 1 
       from  syscolumns 
	  where name = 'AKTIV' 
		and id = object_id('EREC_PldIratPeldanyok'))
  begin
    ALTER table [dbo].EREC_PldIratPeldanyok
      ADD AKTIV AS (cast(case when [ALLAPOT]='90' then (0) else (1) end as bit));

    CREATE index IX_EREC_PldIratPeldanyok_Aktiv ON [dbo].[EREC_PldIratPeldanyok] (AKTIV,ID);

	print 'EREC_PldIratPeldanyok.AKTIV mez� ADDed, INDEX created';
  end;
  GO

print ''
print '--- v�ge ---'
print ''


