declare @id uniqueidentifier
set @id = 'D15AFC14-8D91-E711-80C6-00155D020BD9'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	print 'insert'

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'BOPMH',
		'eRecord',
		'eRecordComponent/KuldKuldemenyFormTab.ascx',
		'labelBoritoTipus',
		'Vonalk�d helye:'
	)
END