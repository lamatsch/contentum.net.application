-- Forditasok.aspx modul

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Modulok
			where Id='85C14331-239E-4F76-9454-E004C5615C44') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Modulok(
			 Tranz_id
			,Stat_id
			,Id
			,Tipus
			,Kod
			,Nev
			,Leiras
			) values (
			 'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'85C14331-239E-4F76-9454-E004C5615C44'
			,'F'
			,'ForditasokList.aspx'
			,'Ford�t�sok list�ja'
			,null
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Modulok
		 SET 
			 Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Id='85C14331-239E-4F76-9454-E004C5615C44'
			,Tipus='F'
			,Kod='ForditasokList.aspx'
			,Nev='Ford�t�sok list�ja'
			,Leiras=null
		 WHERE Id=@record_id 
	 END
	 go

-- ot.tbl.KRT_Forditasok objtipus

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_ObjTipusok
			where Id='178E7044-B0DD-476A-8A20-B6AD05A62B21') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_ObjTipusok(
			 Tranz_id
			,Stat_id
			,Id
			,ObjTipus_Id_Tipus
			,Kod
			,Nev
			) values (
			 'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,'d1bb5269-3fe2-434f-b0df-21d741ce716a'
			,'KRT_Forditasok'
			,'KRT_Forditasok t�bla'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_ObjTipusok
		 SET 
			 Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Id='178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,ObjTipus_Id_Tipus='d1bb5269-3fe2-434f-b0df-21d741ce716a'
			,Kod='KRT_Forditasok'
			,Nev='KRT_Forditasok t�bla'
		 WHERE Id=@record_id 
	 END
	 go

-- funcki�k

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='3D34F3CF-CADF-4043-A50A-A67EC02872BA') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Funkciok(
			 Tranz_id
			,Stat_id
			,Alkalmazas_Id
			,Modosithato
			,Id
			,ObjTipus_Id_AdatElem
			,Muvelet_Id
			,Kod
			,Nev
			) values (
			 'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,'0'
			,'3D34F3CF-CADF-4043-A50A-A67EC02872BA'
			,'178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,'9508908e-aefa-41c6-b6d7-807e3ae3a3ab'
			,'ForditasokList'
			,'Ford�t�sok list�z�s'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Funkciok
		 SET 
			 Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,Modosithato='0'
			,Id='3D34F3CF-CADF-4043-A50A-A67EC02872BA'
			,ObjTipus_Id_AdatElem='178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,Muvelet_Id='9508908e-aefa-41c6-b6d7-807e3ae3a3ab'
			,Kod='ForditasokList'
			,Nev='Ford�t�sok list�z�s'
		 WHERE Id=@record_id 
	 END
	 go

	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='D3041B81-0441-4250-B083-AFF17C9184E7') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Funkciok(
			 Tranz_id
			,Stat_id
			,Alkalmazas_Id
			,Modosithato
			,Id
			,ObjTipus_Id_AdatElem
			,Muvelet_Id
			,Kod
			,Nev
			) values (
			 'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,'0'
			,'D3041B81-0441-4250-B083-AFF17C9184E7'
			,'178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,'78f0d5a2-e048-4a1f-9f11-5d57ab78b985'
			,'ForditasNew'
			,'Ford�t�s felv�tel'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Funkciok
		 SET 
			 Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,Modosithato='0'
			,Id='D3041B81-0441-4250-B083-AFF17C9184E7'
			,ObjTipus_Id_AdatElem='178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,Muvelet_Id='78f0d5a2-e048-4a1f-9f11-5d57ab78b985'
			,Kod='ForditasNew'
			,Nev='Ford�t�s felv�tel'
		 WHERE Id=@record_id 
	 END
	 go

	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='6A4B573A-FE4D-465E-9DDB-D04CABF5ED16') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Funkciok(
			 Tranz_id
			,Stat_id
			,Alkalmazas_Id
			,Modosithato
			,Id
			,ObjTipus_Id_AdatElem
			,Muvelet_Id
			,Kod
			,Nev
			) values (
			 'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,'0'
			,'6A4B573A-FE4D-465E-9DDB-D04CABF5ED16'
			,'178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,'78f0d5a2-e048-4a1f-9f11-5d57ab78b985'
			,'ForditasView'
			,'Ford�t�s megtekint�s'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Funkciok
		 SET 
			 Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,Modosithato='0'
			,Id='6A4B573A-FE4D-465E-9DDB-D04CABF5ED16'
			,ObjTipus_Id_AdatElem='178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,Muvelet_Id='78f0d5a2-e048-4a1f-9f11-5d57ab78b985'
			,Kod='ForditasView'
			,Nev='Ford�t�s megtekint�s'
		 WHERE Id=@record_id 
	 END
	 go

	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='BC958348-C913-4923-84F1-712F9A1011EB') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Funkciok(
			 Tranz_id
			,Stat_id
			,Alkalmazas_Id
			,Modosithato
			,Id
			,ObjTipus_Id_AdatElem
			,Muvelet_Id
			,Kod
			,Nev
			) values (
			 'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,'0'
			,'BC958348-C913-4923-84F1-712F9A1011EB'
			,'178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,'78f0d5a2-e048-4a1f-9f11-5d57ab78b985'
			,'ForditasModify'
			,'Ford�t�s m�dos�t�s'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Funkciok
		 SET 
			 Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,Modosithato='0'
			,Id='BC958348-C913-4923-84F1-712F9A1011EB'
			,ObjTipus_Id_AdatElem='178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,Muvelet_Id='78f0d5a2-e048-4a1f-9f11-5d57ab78b985'
			,Kod='ForditasModify'
			,Nev='Ford�t�s m�dos�t�s'
		 WHERE Id=@record_id 
	 END
	 go

	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='85F16BAD-E084-4E8A-8331-9211C2D3AD6F') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Funkciok(
			 Tranz_id
			,Stat_id
			,Alkalmazas_Id
			,Modosithato
			,Id
			,ObjTipus_Id_AdatElem
			,Muvelet_Id
			,Kod
			,Nev
			) values (
			 'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,'0'
			,'85F16BAD-E084-4E8A-8331-9211C2D3AD6F'
			,'178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,'89806484-BF5A-4396-87B2-0066513A008D'
			,'ForditasViewHistory'
			,'Ford�t�s el�zm�nyeinek megtekint�se'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Funkciok
		 SET 
			 Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,Modosithato='0'
			,Id='85F16BAD-E084-4E8A-8331-9211C2D3AD6F'
			,ObjTipus_Id_AdatElem='178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,Muvelet_Id='89806484-BF5A-4396-87B2-0066513A008D'
			,Kod='ForditasViewHistory'
			,Nev='Ford�t�s el�zm�nyeinek megtekint�se'
		 WHERE Id=@record_id 
	 END
	 go

	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='E90486E0-9BED-4A30-AB24-1F50CDEEFD54') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Funkciok(
			 Tranz_id
			,Stat_id
			,Alkalmazas_Id
			,Modosithato
			,Id
			,ObjTipus_Id_AdatElem
			,Muvelet_Id
			,Kod
			,Nev
			) values (
			 'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,'0'
			,'E90486E0-9BED-4A30-AB24-1F50CDEEFD54'
			,'178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,'805c7634-f205-4ae7-8580-db3c73d8f43b'
			,'ForditasInvalidate'
			,'Ford�t�s �rv�nytelen�t�s'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Funkciok
		 SET 
			 Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,Modosithato='0'
			,Id='E90486E0-9BED-4A30-AB24-1F50CDEEFD54'
			,ObjTipus_Id_AdatElem='178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,Muvelet_Id='805c7634-f205-4ae7-8580-db3c73d8f43b'
			,Kod='ForditasInvalidate'
			,Nev='Ford�t�s �rv�nytelen�t�s'
		 WHERE Id=@record_id 
	 END
	 go

	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='BFC5F366-A8AA-4EE2-BB71-6784C229611E') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Funkciok(
			 Tranz_id
			,Stat_id
			,Alkalmazas_Id
			,Modosithato
			,Id
			,ObjTipus_Id_AdatElem
			,Muvelet_Id
			,Kod
			,Nev
			) values (
			 'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,'0'
			,'BFC5F366-A8AA-4EE2-BB71-6784C229611E'
			,'178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,'fbddc6b8-6671-4ba7-9bdd-885ffc031986'
			,'ForditasLock'
			,'Ford�t�s z�rol�s'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Funkciok
		 SET 
			 Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,Modosithato='0'
			,Id='BFC5F366-A8AA-4EE2-BB71-6784C229611E'
			,ObjTipus_Id_AdatElem='178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,Muvelet_Id='fbddc6b8-6671-4ba7-9bdd-885ffc031986'
			,Kod='ForditasLock'
			,Nev='Ford�t�s z�rol�s'
		 WHERE Id=@record_id 
	 END
	 go

	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='6A54389F-4ABD-48A4-B2E0-E381FE18852D') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Funkciok(
			 Tranz_id
			,Stat_id
			,Alkalmazas_Id
			,Modosithato
			,Id
			,ObjTipus_Id_AdatElem
			,Muvelet_Id
			,Kod
			,Nev
			) values (
			 'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,'0'
			,'6A54389F-4ABD-48A4-B2E0-E381FE18852D'
			,'178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,'fbddc6b8-6671-4ba7-9bdd-885ffc031986'
			,'ForditasForceLock'
			,'Ford�t�s z�rol�s (Force)'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Funkciok
		 SET 
			 Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,Modosithato='0'
			,Id='6A54389F-4ABD-48A4-B2E0-E381FE18852D'
			,ObjTipus_Id_AdatElem='178E7044-B0DD-476A-8A20-B6AD05A62B21'
			,Muvelet_Id='fbddc6b8-6671-4ba7-9bdd-885ffc031986'
			,Kod='ForditasForceLock'
			,Nev='Ford�t�s z�rol�s (Force)'
		 WHERE Id=@record_id 
	 END
	 go
	 
-- men�

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Menuk
			where Id='DB5AABA5-AA93-49B9-BE41-BFB028C4C0F1') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Menuk(
			 Menu_Id_Szulo
			,Tranz_id
			,Stat_id
			,Id
			,Kod
			,Nev
			,Funkcio_Id
			,Modul_Id
			,Sorrend
			) values (
			 'AA1421B9-0498-4CA3-B075-60F699CB1B7C'
			,'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'DB5AABA5-AA93-49B9-BE41-BFB028C4C0F1'
			,'eAdmin'
			,'Ford�t�sok'
			,'3D34F3CF-CADF-4043-A50A-A67EC02872BA'
			,'85C14331-239E-4F76-9454-E004C5615C44'
			,'35'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Menuk
		 SET 
			 Menu_Id_Szulo='AA1421B9-0498-4CA3-B075-60F699CB1B7C'
			,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Id='DB5AABA5-AA93-49B9-BE41-BFB028C4C0F1'
			,Kod='eAdmin'
			,Nev='Ford�t�sok'
			,Funkcio_Id='3D34F3CF-CADF-4043-A50A-A67EC02872BA'
			,Modul_Id='85C14331-239E-4F76-9454-E004C5615C44'
			,Sorrend='35'
		 WHERE Id=@record_id 
	 END
	 go

-- szerepkor-funkci�k

-- FELJELSZTO
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='DF7AEE42-ED77-47D4-BE01-D64CAC4CA4A7') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Tranz_id
			,Stat_id
			,Id
			,Funkcio_Id
			) values (
			 'e855f681-36ed-41b6-8413-576fcb5d1542'
			,'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'DF7AEE42-ED77-47D4-BE01-D64CAC4CA4A7'
			,'3D34F3CF-CADF-4043-A50A-A67EC02872BA'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Szerepkor_Funkcio
		 SET 
			 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
			,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Id='DF7AEE42-ED77-47D4-BE01-D64CAC4CA4A7'
			,Funkcio_Id='3D34F3CF-CADF-4043-A50A-A67EC02872BA'
		 WHERE Id=@record_id 
	 END
	 go

	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='8EAB8D16-2C4E-4907-81B8-21933DCE86D3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Tranz_id
			,Stat_id
			,Id
			,Funkcio_Id
			) values (
			 'e855f681-36ed-41b6-8413-576fcb5d1542'
			,'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'8EAB8D16-2C4E-4907-81B8-21933DCE86D3'
			,'6A4B573A-FE4D-465E-9DDB-D04CABF5ED16'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Szerepkor_Funkcio
		 SET 
			 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
			,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Id='8EAB8D16-2C4E-4907-81B8-21933DCE86D3'
			,Funkcio_Id='6A4B573A-FE4D-465E-9DDB-D04CABF5ED16'
		 WHERE Id=@record_id 
	 END
	 go

	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='B6793D6C-778B-424C-AC77-3D5144FFF78A') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Tranz_id
			,Stat_id
			,Id
			,Funkcio_Id
			) values (
			 'e855f681-36ed-41b6-8413-576fcb5d1542'
			,'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'B6793D6C-778B-424C-AC77-3D5144FFF78A'
			,'85F16BAD-E084-4E8A-8331-9211C2D3AD6F'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Szerepkor_Funkcio
		 SET 
			 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
			,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Id='B6793D6C-778B-424C-AC77-3D5144FFF78A'
			,Funkcio_Id='85F16BAD-E084-4E8A-8331-9211C2D3AD6F'
		 WHERE Id=@record_id 
	 END
	 go

	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='656D3700-C5A4-41FB-BEEC-332BF52B50E6') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Tranz_id
			,Stat_id
			,Id
			,Funkcio_Id
			) values (
			 'e855f681-36ed-41b6-8413-576fcb5d1542'
			,'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'656D3700-C5A4-41FB-BEEC-332BF52B50E6'
			,'E90486E0-9BED-4A30-AB24-1F50CDEEFD54'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Szerepkor_Funkcio
		 SET 
			 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
			,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Id='656D3700-C5A4-41FB-BEEC-332BF52B50E6'
			,Funkcio_Id='E90486E0-9BED-4A30-AB24-1F50CDEEFD54'
		 WHERE Id=@record_id 
	 END
	 go

	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='9D1D13F6-D7A3-46EC-9A6D-1B3040C59783') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Tranz_id
			,Stat_id
			,Id
			,Funkcio_Id
			) values (
			 'e855f681-36ed-41b6-8413-576fcb5d1542'
			,'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'9D1D13F6-D7A3-46EC-9A6D-1B3040C59783'
			,'BFC5F366-A8AA-4EE2-BB71-6784C229611E'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Szerepkor_Funkcio
		 SET 
			 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
			,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Id='9D1D13F6-D7A3-46EC-9A6D-1B3040C59783'
			,Funkcio_Id='BFC5F366-A8AA-4EE2-BB71-6784C229611E'
		 WHERE Id=@record_id 
	 END
	 go

	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='4C47C8BB-8F21-4AB5-B070-F9233CEE9671') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Tranz_id
			,Stat_id
			,Id
			,Funkcio_Id
			) values (
			 'e855f681-36ed-41b6-8413-576fcb5d1542'
			,'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'4C47C8BB-8F21-4AB5-B070-F9233CEE9671'
			,'6A54389F-4ABD-48A4-B2E0-E381FE18852D'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Szerepkor_Funkcio
		 SET 
			 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
			,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Id='4C47C8BB-8F21-4AB5-B070-F9233CEE9671'
			,Funkcio_Id='6A54389F-4ABD-48A4-B2E0-E381FE18852D'
		 WHERE Id=@record_id 
	 END
	 go

	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='407738C3-B14E-4E80-831A-908295268A43') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Tranz_id
			,Stat_id
			,Id
			,Funkcio_Id
			) values (
			 'e855f681-36ed-41b6-8413-576fcb5d1542'
			,'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'407738C3-B14E-4E80-831A-908295268A43'
			,'BC958348-C913-4923-84F1-712F9A1011EB'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Szerepkor_Funkcio
		 SET 
			 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
			,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Id='407738C3-B14E-4E80-831A-908295268A43'
			,Funkcio_Id='BC958348-C913-4923-84F1-712F9A1011EB'
		 WHERE Id=@record_id 
	 END
	 go

	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='E05C3158-DDCF-41FC-9810-D0B7F0365B1D') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Tranz_id
			,Stat_id
			,Id
			,Funkcio_Id
			) values (
			 'e855f681-36ed-41b6-8413-576fcb5d1542'
			,'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'E05C3158-DDCF-41FC-9810-D0B7F0365B1D'
			,'D3041B81-0441-4250-B083-AFF17C9184E7'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Szerepkor_Funkcio
		 SET 
			 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
			,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Id='E05C3158-DDCF-41FC-9810-D0B7F0365B1D'
			,Funkcio_Id='D3041B81-0441-4250-B083-AFF17C9184E7'
		 WHERE Id=@record_id 
	 END
	 go