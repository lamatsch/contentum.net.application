
USE [??? KORNYEZET ???]                                           /* Meg kell adni az adatb�zisnevet */
GO

  SET NOEXEC OFF
  print 'El�sz�r ellen�rizd a KORNYEZET-i param�terek helyess�g�t !!!'         /* Comment-be kell tenni ezt a blokkot */
  SET NOEXEC ON

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF NOT EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_HTTPRequest]') AND type in (N'P', N'PC') )
  BEGIN
    print 'sp_HTTPRequest t�rolt elj�r�s hi�nyzik az adatb�zisb�l - az update NEM futtathat�'                               /* Telep�teni kell */
    SET NOEXEC ON
  END
GO


print '---'
print '/* --- Irat g�pi storno --- */'
--- elvben megoldott:  print '/* Felt�telezz�k, hogy minden IRAT-hoz egy �s csak egy Iratp�ld�ny tartozik, de az tartozik hozz� */'
print '---'

print '- @IratAzonosito TMP fel�p�t�se -'

DECLARE @IratAzonosito table ( [Azonosito] [nvarchar](100) NULL );
/* ezt TUTI �t kell �rni minden KORNYEZET-ben */

/* ez a sor legyen comment - a kontroll sor */
insert into @IratAzonosito values ('??? KORNYEZET ???');                          

/* ezeket a sorokat viszont vegy�k ki comment-b�l */
/*																				
--- az els� 91 sor
insert into @IratAzonosito values ('XVIII /8789 - 4 /2017');
insert into @IratAzonosito values ('XVIII /8789 - 3 /2017');
insert into @IratAzonosito values ('XVIII /22669 - 1 /2017');
insert into @IratAzonosito values ('XVIII /8790 - 3 /2017');
insert into @IratAzonosito values ('XVIII /20212 - 2 /2017');
insert into @IratAzonosito values ('XVIII /22677 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22627 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22665 - 1 /2017');
insert into @IratAzonosito values ('XVIII /9592 - 3 /2017');
insert into @IratAzonosito values ('XVIII /9592 - 2 /2017');
insert into @IratAzonosito values ('XVIII /20213 - 2 /2017');
insert into @IratAzonosito values ('XVIII /22672 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22666 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22620 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22661 - 1 /2017');
insert into @IratAzonosito values ('XVIII /10864 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10892 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10865 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10866 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10894 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10893 - 2 /2017');
insert into @IratAzonosito values ('XVIII /22670 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22659 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22631 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22632 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22633 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22634 - 1 /2017');
insert into @IratAzonosito values ('XVIII /10867 - 3 /2017');
insert into @IratAzonosito values ('XVIII /10867 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10863 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10862 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10868 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10869 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10871 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10870 - 2 /2017');
insert into @IratAzonosito values ('XVIII /22636 - 1 /2017');
insert into @IratAzonosito values ('XVIII /10872 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10873 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10879 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10874 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10875 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10878 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10876 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10877 - 2 /2017');
insert into @IratAzonosito values ('XVIII /10880 - 2 /2017');
insert into @IratAzonosito values ('XVIII /22637 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22638 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22639 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22640 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22641 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22642 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22643 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22645 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22644 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22625 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22646 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22647 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22648 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22649 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22650 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22651 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22652 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22654 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22653 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22655 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22656 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22657 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22635 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22658 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22674 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22626 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22664 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22668 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22624 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22676 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22622 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22630 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22667 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22678 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22673 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22623 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22663 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22675 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22671 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22680 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22621 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22679 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22662 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22628 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22629 - 1 /2017');
insert into @IratAzonosito values ('XVIII /22660 - 1 /2017');
*/

/* ezt TUTI �t kell �rni minden KORNYEZET-ben */
/* ezeket a sorokat viszont vegy�k ki comment-b�l */
/*
--- a k�vetkez� 384 sor
insert  into @IratAzonosito values ( 'XVIII /47 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /1807 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /1955 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /2379 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /2669 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /3114 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /4922 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23275 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23274 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23273 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23271 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23270 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23269 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23268 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23267 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23265 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23264 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23263 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23262 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23261 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23260 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23259 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23258 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23257 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23256 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23255 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23254 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23253 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23251 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23250 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23249 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23248 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23247 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23246 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23245 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23244 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23243 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23242 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23241 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23240 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23239 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23238 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23237 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23236 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23235 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23234 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23233 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23232 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23231 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23230 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23229 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23228 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /5255 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23226 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23225 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23224 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23223 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23222 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23221 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23220 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23219 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23218 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23217 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23216 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23215 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23214 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23213 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23212 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23211 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23210 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23209 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23208 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23207 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23206 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23203 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23202 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23201 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23200 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23199 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23198 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23197 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23196 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23194 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23193 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /23192 - 1 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /22480 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /22349 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /22325 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /22255 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /22235 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /22200 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /22172 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /22123 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /22048 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /22034 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21959 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21958 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21926 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21868 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21721 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21710 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21663 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21646 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21512 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21501 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21370 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21344 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21238 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21216 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21134 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21129 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21048 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /21019 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20921 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20899 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20883 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20873 - 7 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20855 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20852 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20828 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20758 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20649 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20637 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20623 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20622 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20601 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20542 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20514 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20478 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20340 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20333 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20278 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20231 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20144 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20013 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /20012 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19996 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19989 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19988 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19887 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19769 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19765 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19656 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19583 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19555 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19537 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19269 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19210 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19171 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19167 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19115 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19109 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /19107 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18892 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18873 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18849 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18839 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18801 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18719 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18715 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18704 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18650 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18632 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18373 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18328 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18327 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18314 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18302 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18274 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18270 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18204 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18200 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18165 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18144 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18143 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18094 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18093 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18081 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18061 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /18031 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17932 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17921 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17919 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17897 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17885 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17879 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17860 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17848 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17823 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17781 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17750 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17749 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17748 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17736 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17721 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17585 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17583 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17571 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17492 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17468 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17466 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17460 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17445 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17444 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17433 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17403 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17385 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17369 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17366 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17359 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17357 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17344 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17208 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17156 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17155 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17144 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17036 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /17012 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16975 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16974 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16967 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16965 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16959 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16941 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16937 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16896 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16883 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16880 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16876 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16866 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16848 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16837 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16833 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16790 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16754 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16700 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16656 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16643 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16637 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16587 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16577 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16563 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16531 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16524 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16471 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16463 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16438 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16359 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16326 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16306 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16276 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16275 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16272 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16207 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16203 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16163 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16145 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16129 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16127 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16107 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16092 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16082 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16038 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /16020 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15973 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15962 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15927 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15896 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15893 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15891 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15884 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15857 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15803 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15795 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15782 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15688 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15658 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15623 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15604 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15573 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15571 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15542 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15478 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15451 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15427 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15408 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15407 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15406 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15376 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15337 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15292 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15281 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15267 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15237 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15235 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15233 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15219 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15175 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15146 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15117 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /15027 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14868 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14841 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14803 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14782 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14727 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14724 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14698 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14659 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14642 - 6 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14628 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14589 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14576 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14552 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14544 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14537 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14511 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14383 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14378 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14346 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14309 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14293 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14226 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14183 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14085 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14066 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14060 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14055 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14050 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /14040 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /13987 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /13978 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /13971 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /13859 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /13841 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /13468 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /13448 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /6719 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /13387 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /13381 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /13352 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /13349 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /13280 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /13272 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /12756 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /12719 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /12639 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /12623 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /12576 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /12548 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /12537 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /12469 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /12378 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /12373 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /12341 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /12224 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /12132 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /12120 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /11136 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /11117 - 6 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /11109 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /11097 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /11077 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /11012 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /10812 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /10477 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /7998 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /10397 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /10396 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /10369 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /10245 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /9818 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /9714 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /9559 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /9530 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /9233 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /9077 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /8976 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /8760 - 3 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /8500 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /8373 - 4 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /8353 - 5 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /8301 - 2 /2017'); 
insert  into @IratAzonosito values ( 'XVIII /8162 - 3 /2017'); 
*/

print ''
print '- Irat �llapot query fut�s el�tt -'
select substring(i.Azonosito,1,35) as ELOTTE
       ,convert(nvarchar(10), u.Foszam) as Foszam, substring(u.Allapot,1,2) as UAllapot, substring(ktu.Nev,1,30) as UgyiratAllapot
	   ,substring(u.TovabbitasAlattAllapot,1,2) as TAllapot, substring(kttu.Nev,1,30) as TovabbitasAlattAllapot
       ,convert(nvarchar(10), i.Alszam) as Alszam, substring(i.Allapot,1,2) as IAllapot, substring(kti.Nev,1,30) as IratAllapot
	   ,substring(p.Allapot,1,2) as PAllapot, substring(ktp.Nev,1,30) as PeldanyAllapot
	   ,substring(k.Allapot,1,2) as KAllapot, substring(ktk.Nev,1,30) as KuldemenyAllapot
	   ,i.Azonosito, i.*
  from [dbo].EREC_IraIratok i
 left join [dbo].KRT_KodCsoportok kcsi on kcsi.Kod = 'IRAT_ALLAPOT'
 left join [dbo].KRT_KodTarak kti on kti.KodCsoport_Id = kcsi.ID and kti.kod = i.Allapot
 inner join [dbo].EREC_UgyUgyiratok u on u.Id = i.Ugyirat_Id
 left join [dbo].KRT_KodCsoportok kcsu on kcsu.Kod = 'UGYIRAT_ALLAPOT'
 left join [dbo].KRT_KodTarak ktu on ktu.KodCsoport_Id = kcsu.ID and ktu.kod = u.Allapot
 left join [dbo].KRT_KodCsoportok ktcsu on ktcsu.Kod = 'UGYIRAT_ALLAPOT'
 left join [dbo].KRT_KodTarak kttu on kttu.KodCsoport_Id = ktcsu.ID and kttu.kod = u.TovabbitasAlattAllapot
 inner join [dbo].EREC_PldIratPeldanyok p on p.IraIrat_Id = i.Id
 left join [dbo].KRT_KodCsoportok kcsp on kcsp.Kod = 'IRATPELDANY_ALLAPOT'
 left join [dbo].KRT_KodTarak ktp on ktp.KodCsoport_Id = kcsp.ID and ktp.kod = p.Allapot 
 left join [dbo].EREC_Kuldemeny_IratPeldanyai kp on kp.Peldany_ID = p.ID
 left join [dbo].EREC_KuldKuldemenyek k on k.ID = kp.KuldKuldemeny_ID 
 left join [dbo].KRT_KodCsoportok kcsk on kcsk.Kod = 'KULDEMENY_ALLAPOT'
 left join [dbo].KRT_KodTarak ktk on ktk.KodCsoport_Id = kcsk.ID and ktk.kod = k.Allapot 
 where i.Azonosito in (select ia.Azonosito from @IratAzonosito ia)
 order by u.Allapot /*i.Azonosito*/
;

print ''
print '- IratAzonosito ciklus -'

DECLARE cur_Azonosito CURSOR FOR
        SELECT /*TOP 100*/ AZONOSITO from @IratAzonosito order by AZONOSITO;

DECLARE @Irat_Azon nvarchar(100)

DECLARE @Ugyirat_ID uniqueidentifier,
        @Irat_ID uniqueidentifier,
        @IratPld_ID uniqueidentifier,
		@Kuldemeny_ID uniqueidentifier
DECLARE @Ugyirat_Orzo uniqueidentifier,
        @IratPld_Orzo uniqueidentifier,
		@Kuldemeny_Orzo uniqueidentifier
DECLARE @Orzo_Csoport_ID uniqueidentifier
DECLARE @Ugyirat_Allapot nvarchar(64),
        @Irat_Allapot nvarchar(64),
		@IratPld_Allapot nvarchar(64),
		@Kuldemeny_Allapot nvarchar(64)

declare @n_db int,
        @n_irat_nemtalalt int,
        @n_irat_stornozott int,
        @n_pld_storno int,
        @n_ugy_storno int,
		@n_elo_irat int,
		@b_tovabb bit

Declare @xmlOut varchar(8000)
Declare @xml xml
Declare @RequestText as varchar(8000)

--- BEGIN TRY        --- egyszer�s�tett 'hibakezel�s' lett be�p�tve -> nem halunk meg, hanem tov�bbl�p�nk
BEGIN

  SET @n_db = 0;
  SET @n_irat_nemtalalt = 0;
  SET @n_irat_stornozott = 0;
  SET @n_pld_storno = 0;
  SET @n_ugy_storno = 0;
  
  OPEN cur_Azonosito;
  FETCH NEXT FROM cur_Azonosito INTO @Irat_Azon;

  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    print ''
    print '-- ' + @Irat_Azon + ' --'

    SET @n_db += 1;

	--- Ezzel a kurzorral t�bb sor lesz egy Iratr�l, amennyiben t�bb IratPeldany kapcsol�dik hozz�; de egy IratPeldany csak egy Kuldemeny-ben lehet benne 
    DECLARE cur_IratPeldany CURSOR FOR
        SELECT u.ID, u.FelhasznaloCsoport_ID_Orzo, u.Allapot, i.ID, i.Allapot, p.ID, p.FelhasznaloCsoport_ID_Orzo, p.Allapot, k.ID, k.FelhasznaloCsoport_ID_Orzo, k.Allapot
		  from [dbo].EREC_IraIratok i
         inner join [dbo].EREC_UgyUgyiratok u on u.Id = i.Ugyirat_Id
         inner join [dbo].EREC_PldIratPeldanyok p on p.IraIrat_Id = i.Id
          left join [dbo].EREC_Kuldemeny_IratPeldanyai kp on kp.Peldany_ID = p.ID
          left join [dbo].EREC_KuldKuldemenyek k on k.ID = kp.KuldKuldemeny_ID and k.Allapot <> '90'
         where i.Azonosito = @Irat_Azon;    

	OPEN cur_IratPeldany;
	FETCH NEXT FROM cur_IratPeldany 
	      INTO @Ugyirat_ID, @Ugyirat_Orzo, @Ugyirat_Allapot, @Irat_ID, @Irat_Allapot, @IratPld_ID, @IratPld_Orzo, @IratPld_Allapot, @Kuldemeny_ID, @Kuldemeny_Orzo, @Kuldemeny_Allapot
    if @Irat_ID is Null
	  SET @n_irat_nemtalalt += 1;
    --- a t�bbi mehetne ELSE �gban, de a WHILE @@FETCH_STATUS �gyis kezeli

    WHILE @@FETCH_STATUS = 0  
    BEGIN  
	    begin
	      if ISNULL(@Irat_Allapot,'00') = '90'
	        SET @n_irat_stornozott +=1;
	      else
	        begin
			  SET @b_tovabb = 1;
			  --- a Kuldemeny -t mindenk�ppen storn�zzuk, ha m�s IratPeldanya is van benne, akkor is; eleve storn�zott NEM lehet, azt a cursor kisz�ri
			  if @Kuldemeny_ID is NOT Null 
			    begin
				  SET @Orzo_Csoport_ID = ( select TOP 1 cs.ID from KRT_CsoportTagok cst
				                            inner join KRT_Csoportok cs on cs.ID = cst.Csoport_ID and getdate() between cs.ErvKezd and cs.ErvVege
											where cst.Csoport_Id_Jogalany = @Kuldemeny_Orzo );
			      --- WebService h�v�s                  											 
                  set @RequestText=
                    '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
	                 <soap:Body>
	                  <KimenoKuldemenySztorno xmlns="Contentum.eRecord.WebService">
	                   <execParam>
		                <Typed>
		                 <Felhasznalo_Id>'+Convert( nvarchar(36), @Kuldemeny_Orzo )+'</Felhasznalo_Id>
		                 <FelhasznaloSzervezet_Id>'+Convert( nvarchar(36), @Orzo_Csoport_ID )+'</FelhasznaloSzervezet_Id>
						 <Record_Id>'+Convert( nvarchar(36), @Kuldemeny_ID )+'</Record_Id>
		                </Typed>                                           
	                   </execParam>
                      </KimenoKuldemenySztorno>
	                 </soap:Body>
	                 </soap:Envelope>
	                '
                  print 'K�ldem�ny: ' + @RequestText

	              exec sp_HTTPRequest 
				    'http://??? KORNYEZET ???/eRecordWebService/EREC_KuldKuldemenyekService.asmx',                /* Meg kell adni a WebService el�r�st */
	                'POST', 
	                @RequestText,
	                'Contentum.eRecord.WebService/KimenoKuldemenySztorno',
	                '', 
					'', 
					@xmlOut out
                  /*  
	              select @xmlOut
			      */
	              SET @xml = convert(xml, @xmlOut)
			      select @xml
			  			  
			      --- ellen�rz�s
	              SET @Kuldemeny_Allapot = ( select k.Allapot from EREC_KuldKuldemenyek k where k.ID = @Kuldemeny_ID );
				  if @Kuldemeny_Allapot <> '90'
                    SET @b_tovabb = 0;  
				end;                                                       --- Kuldemeny storn�
			  if @b_tovabb = 1
			    begin			
			      --- Iratp�ld�ny storn�, !!! ez egy�ttal az Iratot is sztorn�zza
  				  SET @Orzo_Csoport_ID = ( select TOP 1 cs.ID from KRT_CsoportTagok cst
				                            inner join KRT_Csoportok cs on cs.ID = cst.Csoport_ID and getdate() between cs.ErvKezd and cs.ErvVege
											where cst.Csoport_Id_Jogalany = @IratPld_Orzo );
				  --- WebService h�v�s
	              set @RequestText=
                    '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
	                 <soap:Body>
	                  <Sztornozas xmlns="Contentum.eRecord.WebService">
	                   <execParam>
		                <Typed>
		                 <Felhasznalo_Id>'+Convert( nvarchar(36), @IratPld_Orzo )+'</Felhasznalo_Id>
		                 <FelhasznaloSzervezet_Id>'+Convert( nvarchar(36), @Orzo_Csoport_ID )+'</FelhasznaloSzervezet_Id>
		                </Typed>                                           
	                   </execParam>
                       <erec_PldIratPeldanyok_Id>' + Convert( nvarchar(36), @IratPld_ID ) + '</erec_PldIratPeldanyok_Id>
                       <SztornozasIndoka>Iratp�ld�ny g�pi storn� (AXIS)</SztornozasIndoka>
                      </Sztornozas>
	                 </soap:Body>
	                 </soap:Envelope>
	                '
                  print 'Iratp�ld�ny: ' + @RequestText

	              exec sp_HTTPRequest 
				    'http://??? KORNYEZET ???/eRecordWebService/EREC_PldIratPeldanyokService.asmx',                /* Meg kell adni a WebService el�r�st */
	                'POST', 
	                @RequestText,
	                'Contentum.eRecord.WebService/Sztornozas',
	                '', 
					'', 
					@xmlOut out
                  /*  
	              select @xmlOut
			      */
	              SET @xml = convert(xml, @xmlOut)
			      select @xml
			  			  
			      --- ellen�rz�s: IratP�ld�ny �llapot NEM �rdekel csak Irat �llapotot vizsg�lok
	              SET @Irat_Allapot = ( select i.Allapot from EREC_IraIratok i where i.ID = @Irat_ID );
			      if ISNULL(@Irat_Allapot,'00') = '90'
			        begin
			          SET @n_pld_storno +=1;

				      SET @n_elo_irat = 0;
				      SET @n_elo_irat = ( select count(1) from EREC_IraIratok i where i.Ugyirat_ID = @Ugyirat_ID and i.Allapot <> '90' );
				      if @n_elo_irat = 0                    --- ekkor storn�zzuk az ugyiratot is
				        begin
			              --- �gyirat storn� 
  				          SET @Orzo_Csoport_ID = ( select TOP 1 cs.ID from KRT_CsoportTagok cst
				                                    inner join KRT_Csoportok cs on cs.ID = cst.Csoport_ID and getdate() between cs.ErvKezd and cs.ErvVege
							            			where cst.Csoport_Id_Jogalany = @Ugyirat_Orzo );					  

			              --- WebService h�v�s
	                      set @RequestText=
                            '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
	                         <soap:Body>
	                          <Sztornozas xmlns="Contentum.eRecord.WebService">
	                           <execParam>
		                        <Typed>
		                         <Felhasznalo_Id>'+Convert( nvarchar(36), @Ugyirat_Orzo )+'</Felhasznalo_Id>
		                         <FelhasznaloSzervezet_Id>'+Convert( nvarchar(36), @Orzo_Csoport_ID )+'</FelhasznaloSzervezet_Id>
		                        </Typed>                                           
	                           </execParam>
                               <erec_UgyUgyiratok_Id>' + Convert( nvarchar(36), @Ugyirat_ID ) + '</erec_UgyUgyiratok_Id>
                               <SztornozasIndoka>�gyirat g�pi storno (AXIS)</SztornozasIndoka>
                              </Sztornozas>
	                         </soap:Body>
	                         </soap:Envelope>
	                        '
                          print 'Ugyirat: ' + @RequestText

	                      exec sp_HTTPRequest 
	                        'http://??? KORNYEZET ???/eRecordWebService/EREC_UgyUgyiratokService.asmx',                 /* Meg kell adni a WebService el�r�st */
	                        'POST', 
	                        @RequestText,
	                        'Contentum.eRecord.WebService/Sztornozas',
	                        '', 
							'', 
							@xmlOut out
                          /*  
	                      select @xmlOut
					      */
	                      SET @xml = convert(xml, @xmlOut)
					      select @xml;
			  
			              --- ellen�rz�s
	                      SET @Ugyirat_Allapot = ( select u.Allapot from EREC_UgyUgyiratok u where u.ID = @Ugyirat_ID );
			              if ISNULL(@Ugyirat_Allapot,'00') = '90'
                            SET @n_ugy_storno +=1;
					    end;                                   --- ugyirat storno
				    end;                                       --- sikeres irat storno
			    end;		                                   --- Kuldemeny storno ut�n b_tovabb az IratPeldany storn�ra
            end;                                             --- irat m�g nem stornozott
        end;                                                 --- iratot megtal�ltuk
		
      FETCH NEXT FROM cur_IratPeldany 
	        INTO @Ugyirat_ID, @Ugyirat_Orzo, @Ugyirat_Allapot, @Irat_ID, @Irat_Allapot, @IratPld_ID, @IratPld_Orzo, @IratPld_Allapot, @Kuldemeny_ID, @Kuldemeny_Orzo, @Kuldemeny_Allapot	
    END;
    CLOSE cur_IratPeldany
    DEALLOCATE cur_IratPeldany
	
    FETCH NEXT FROM cur_Azonosito INTO @Irat_Azon;
  END;                                   -- cursor ciklus v�ge
  CLOSE cur_Azonosito
  DEALLOCATE cur_Azonosito

  print ''
  print '----------'
  print '�sszesen: ' + convert( nvarchar(10), @n_db );
  print 'Nem tal�lt: ' + convert( nvarchar(10), @n_irat_nemtalalt );
  print 'M�r storn�zott volt: ' + convert( nvarchar(10), @n_irat_stornozott );
  print 'Iratp�ld�ny storn�: ' + convert( nvarchar(10), @n_pld_storno );
  print '    �gyirat storn�: ' + convert( nvarchar(10), @n_ugy_storno );
  print '----------'

END
/*
END TRY

BEGIN CATCH  
    DECLARE @ErrorMessage NVARCHAR(4000);  
    DECLARE @ErrorSeverity INT;  
    DECLARE @ErrorState INT;  

    SELECT   
        @ErrorMessage = ERROR_MESSAGE(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorState = ERROR_STATE();

    print 'A MIG_FOSZAM iktat�sz�m update - HIB�val megszakadt: ' + @ErrorMessage
    RAISERROR (@ErrorMessage, -- Message text.  
               @ErrorSeverity, -- Severity.  
               @ErrorState -- State.  
               );  
END CATCH
*/

print ''
print '- Ellen�rz� lek�rdez�sek -'

select substring(i.Azonosito,1,35) as UTANA
       ,convert(nvarchar(10), u.Foszam) as Foszam, substring(u.Allapot,1,2) as UAllapot, substring(ktu.Nev,1,30) as UgyiratAllapot
	   ,substring(u.TovabbitasAlattAllapot,1,2) as TAllapot, substring(kttu.Nev,1,30) as TovabbitasAlattAllapot
       ,convert(nvarchar(10), i.Alszam) as Alszam, substring(i.Allapot,1,2) as IAllapot, substring(kti.Nev,1,30) as IratAllapot
	   ,substring(p.Allapot,1,2) as PAllapot, substring(ktp.Nev,1,30) as PeldanyAllapot
	   ,substring(k.Allapot,1,2) as KAllapot, substring(ktk.Nev,1,30) as KuldemenyAllapot
	   ,i.Azonosito, i.*
  from [dbo].EREC_IraIratok i
 left join [dbo].KRT_KodCsoportok kcsi on kcsi.Kod = 'IRAT_ALLAPOT'
 left join [dbo].KRT_KodTarak kti on kti.KodCsoport_Id = kcsi.ID and kti.kod = i.Allapot
 inner join [dbo].EREC_UgyUgyiratok u on u.Id = i.Ugyirat_Id
 left join [dbo].KRT_KodCsoportok kcsu on kcsu.Kod = 'UGYIRAT_ALLAPOT'
 left join [dbo].KRT_KodTarak ktu on ktu.KodCsoport_Id = kcsu.ID and ktu.kod = u.Allapot
 left join [dbo].KRT_KodCsoportok ktcsu on ktcsu.Kod = 'UGYIRAT_ALLAPOT'
 left join [dbo].KRT_KodTarak kttu on kttu.KodCsoport_Id = ktcsu.ID and kttu.kod = u.TovabbitasAlattAllapot
 inner join [dbo].EREC_PldIratPeldanyok p on p.IraIrat_Id = i.Id
 left join [dbo].KRT_KodCsoportok kcsp on kcsp.Kod = 'IRATPELDANY_ALLAPOT'
 left join [dbo].KRT_KodTarak ktp on ktp.KodCsoport_Id = kcsp.ID and ktp.kod = p.Allapot 
 left join [dbo].EREC_Kuldemeny_IratPeldanyai kp on kp.Peldany_ID = p.ID
 left join [dbo].EREC_KuldKuldemenyek k on k.ID = kp.KuldKuldemeny_ID 
 left join [dbo].KRT_KodCsoportok kcsk on kcsk.Kod = 'KULDEMENY_ALLAPOT'
 left join [dbo].KRT_KodTarak ktk on ktk.KodCsoport_Id = kcsk.ID and ktk.kod = k.Allapot 
 where i.Azonosito in (select ia.Azonosito from @IratAzonosito ia)
 order by u.Allapot /*i.Azonosito*/
;

/*
print ''
print '- NEM storn�zott sorok -'

select substring(i.Azonosito,1,35) as NEM_STORNOZOTT
       ,convert(nvarchar(10), u.Foszam) as Foszam, substring(u.Allapot,1,2) as UAllapot, substring(ktu.Nev,1,30) as UgyiratAllapot
	   ,substring(u.TovabbitasAlattAllapot,1,2) as TAllapot, substring(kttu.Nev,1,30) as TovabbitasAlattAllapot
       ,convert(nvarchar(10), i.Alszam) as Alszam, substring(i.Allapot,1,2) as IAllapot, substring(kti.Nev,1,30) as IratAllapot
	   ,substring(p.Allapot,1,2) as PAllapot, substring(ktp.Nev,1,30) as PeldanyAllapot
	   ,substring(k.Allapot,1,2) as KAllapot, substring(ktk.Nev,1,30) as KuldemenyAllapot
  from [dbo].EREC_IraIratok i
 left join [dbo].KRT_KodCsoportok kcsi on kcsi.Kod = 'IRAT_ALLAPOT'
 left join [dbo].KRT_KodTarak kti on kti.KodCsoport_Id = kcsi.ID and kti.kod = i.Allapot
 inner join [dbo].EREC_UgyUgyiratok u on u.Id = i.Ugyirat_Id
 left join [dbo].KRT_KodCsoportok kcsu on kcsu.Kod = 'UGYIRAT_ALLAPOT'
 left join [dbo].KRT_KodTarak ktu on ktu.KodCsoport_Id = kcsu.ID and ktu.kod = u.Allapot
 left join [dbo].KRT_KodCsoportok ktcsu on ktcsu.Kod = 'UGYIRAT_ALLAPOT'
 left join [dbo].KRT_KodTarak kttu on kttu.KodCsoport_Id = ktcsu.ID and kttu.kod = u.TovabbitasAlattAllapot
 inner join [dbo].EREC_PldIratPeldanyok p on p.IraIrat_Id = i.Id
 left join [dbo].KRT_KodCsoportok kcsp on kcsp.Kod = 'IRATPELDANY_ALLAPOT'
 left join [dbo].KRT_KodTarak ktp on ktp.KodCsoport_Id = kcsp.ID and ktp.kod = p.Allapot 
 left join [dbo].EREC_Kuldemeny_IratPeldanyai kp on kp.Peldany_ID = p.ID
 left join [dbo].EREC_KuldKuldemenyek k on k.ID = kp.KuldKuldemeny_ID 
 left join [dbo].KRT_KodCsoportok kcsk on kcsk.Kod = 'KULDEMENY_ALLAPOT'
 left join [dbo].KRT_KodTarak ktk on ktk.KodCsoport_Id = kcsk.ID and ktk.kod = k.Allapot 
 where i.Azonosito in (select ia.Azonosito from @IratAzonosito ia)
   and ( p.Allapot <> '90' OR i.Allapot <> '90' )
 order by u.Allapot
;
*/


SET NOEXEC OFF

print ''
print '/* --- V�ge --- */'
GO

