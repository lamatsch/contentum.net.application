-- BLG_1014 Ugyintezo cimke fordítások

declare @id uniqueidentifier

print 'NMHH'
print 'EgyszerusitettIktatasForm'
print 'labelUgyiratUgyintezo'
set @id = '0E7EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'EgyszerusitettIktatasForm.aspx',
		'labelUgyiratUgyintezo',
		'Felelős ügyintéző:'
	)
END
print '--'
print 'labelIratUgyintezo'

set @id = '0F7EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'EgyszerusitettIktatasForm.aspx',
		'labelIratUgyintezo',
		'Intézkedő ügyintéző:'
	)
END
print '----'
print '--'
print 'eRecordComponent/IraIratFormTab'
print 'labelUgyUgyirat_Ugyintezo'
set @id = '107EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/IraIratFormTab.ascx',
		'labelUgyUgyirat_Ugyintezo',
		'Felelős ügyintéző:'
	)
END

print '--'
print 'labelIratUgyintezo'

set @id = '117EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	
	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/IraIratFormTab.ascx',
		'labelIratUgyintezo',
		'Intézkedő ügyintéző:'
	)
END

print '--'
print 'resultPanel_UgyiratUgyintezo'

set @id = '127EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	
	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/IraIratFormTab.ascx',
		'resultPanel_UgyiratUgyintezo',
		'Felelős ügyintéző:'
	)
END

print '----'
print '--'
print 'eRecordComponent/UgyUgyiratFormTab'
print 'Ugyintezo_felirat'

set @id = '137EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	
	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/UgyUgyiratFormTab.ascx',
		'Ugyintezo_felirat',
		'Felelős ügyintéző:'
	)
END

print '----'
print '--'
print 'eRecordComponent/UgyiratokList'
print 'BoundField_UgyiratUgyintezo'
set @id = '147EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/UgyiratokList.ascx',
		'BoundField_UgyiratUgyintezo',
		'Felelős ügyintéző'
	)
END

print '----'
print '--'
print 'IraIratokList'
print 'BoundField_UgyiratUgyintezo'
set @id = '157EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'IraIratokList.aspx',
		'BoundField_UgyiratUgyintezo',
		'Felelős ügyintéző'
	)
END

print '--'
print 'BoundField_IratUgyintezo'
set @id = '167EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'IraIratokList.aspx',
		'BoundField_IratUgyintezo',
		'Intézkedő ügyintéző'
	)
END

print '----'
print '--'
print 'IraIratokSearch'
print 'labelIratUgyintezo'

set @id = '177EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	
	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'IraIratokSearch.aspx',
		'labelIratUgyintezo',
		'Intézkedő ügyintéző:'
	)
END


print '----'
print '--'
print 'UgyUgyiratokSearch'
print 'labelUgyUgyirat_Ugyintezo'

set @id = '187EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	
	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'UgyUgyiratokSearch.aspx',
		'labelUgyUgyirat_Ugyintezo',
		'Felelős ügyintéző:'
	)
END

print '----'
print '--'
print 'eRecordComponent/UgyiratTerkepTab'
print 'StrUgyiratUgyintezo'

set @id = 'A396C9F8-BAC3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	
	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/UgyiratTerkepTab.ascx',
		'StrUgyiratUgyintezo',
		'Felelős ügyintéző:'
	)
END

print '--'
print 'StrIratUgyintezo'

set @id = '707F2D61-BDC3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	
	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/UgyiratTerkepTab.ascx',
		'StrIratUgyintezo',
		'Intézkedő ügyintéző:'
	)
END