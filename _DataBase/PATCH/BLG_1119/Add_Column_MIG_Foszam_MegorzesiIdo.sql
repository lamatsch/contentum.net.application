IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS 
WHERE  TABLE_NAME = 'MIG_Foszam' AND COLUMN_NAME = 'MegorzesiIdo')
BEGIN
	print 'Add Column MIG_Foszam.MegorzesiIdo'
	alter table MIG_Foszam
	add MegorzesiIdo datetime null
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS 
WHERE  TABLE_NAME = 'MIG_FoszamHistory' AND COLUMN_NAME = 'MegorzesiIdo')
BEGIN
	print 'Add Column MIG_FoszamHistory.MegorzesiIdo'
	alter table MIG_FoszamHistory
	add MegorzesiIdo datetime null
END