INSERT INTO [dbo].[KRT_Tanusitvanyok]
           ([Csoport_Id]
           ,[TanusitvanyTipus_Id]
           ,[Tulajdonos]           
           ,[Tarolas]           
           ,[Ver]
           ,[ErvKezd]
           )
                select cs.id, tt.id, cs.Nev, 'BTAR',1,getdate() from krt_tanusitvanytipusok tt, (select * from krt_csoportok where tipus = '0' and ervkezd <= getdate() and ervvege >= getdate() and org = '450B510A-7CAA-46B0-83E3-18445C0C53A9' and id in
  (select distinct cst.csoport_id from krt_csoporttagok cst inner join krt_csoportok cs on cst.csoport_id_jogalany = cs.id where cs.tipus = '1' and cs.ervkezd <= getdate() and cs.ervvege >= getdate() and cst.ervkezd <= getdate() and cst.ervvege >= getdate())) cs
  where tt.TanusitvanyTipus = 'MSZERV'      
GO

insert into 
KRT_EASZTanusitvanyok (EASZ_Id, Tanusitvany_Id,KonfigTetel, Ver, ErvKezd,LetrehozasIdo) 
select et.id, t.id, 9,1,GETDATE(),GETDATE() 
from 
KRT_Tanusitvanyok t, 
(select distinct et.id from KRT_AlairasSzabalyok asz, KRT_EASZTipus et, KRT_AlairasTipusok at
where asz.AlairasTipus_Id = et.AlairasTipus_Id 
and at.id = et.AlairasTIpus_Id) et
where concat(t.id, et.id) not in (select concat(tanusitvany_id, easz_id) from KRT_EASZTanusitvanyok);
GO