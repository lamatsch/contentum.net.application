begin tran;

declare @tranzId uniqueidentifier
set @tranzId = NEWID();

declare @admin uniqueidentifier
set @admin = '54E861A5-36ED-44CA-BAA7-C287D125B309';

with hair as
(
	select Id from HKP_DokumentumAdatok
	where
	(
	DokTipusAzonosito in ('MeghiusulasiIgazolas','LetoltesiIgazolas','FeladasiIgazolas','AtveteliErtesito')
	or (DokTipusHivatal = 'NAV' and right(DokTipusAzonosito, 4) = 'HIPA')
	or (DokTipusHivatal = 'NAV' and right(DokTipusAzonosito, 6) = 'HIPAEK')
	or (DokTipusHivatal = 'ORFK' and right(DokTipusAzonosito, 3) = 'IVH')
	)
	and Rendszer = 'HAIR'
)

update HKP_DokumentumAdatok
	   set Rendszer = 'WEB',
	   ErvVege = '4700-12-31',
	   Ver = Ver + 1,
	   ModositasIdo = GETDATE(),
	   Modosito_id = @admin,
	   Tranz_id = @tranzId
where Rendszer = 'HAIR'
and id  not in (select id from hair)


commit;

