-- PASSWORD_POLICY_* rendszerparameterek
print '''PASSWORD_POLICY'' rendszerparameter hozzaadasa'

/*
password_policy 
password_policy_pass_length
password_policy_regexp
password_policy_history_number
password_policy_expire_days
password_policy_wrong_pass_try_number
*/

declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'  
DECLARE @org_kod nvarchar(100) 
SET @org_kod = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
/*select kod from KRT_Orgok where id=@org)*/

print 'Org = ' + @org_kod

declare @tranz_id uniqueidentifier = newid()

declare @record_id uniqueidentifier = '353c3582-e8ce-49a8-a8fa-74503a4a7434'

declare @Ertek nvarchar(400)

set @Ertek = 'STRICT'

print '@record_id = ' + cast(@record_id as nvarchar(36))

print '@Ertek = ' + @Ertek

declare @Nev nvarchar(400) = 'PASSWORD_POLICY'

declare @Note nvarchar(4000) = 'Az alkalmazott jelsz� h�zirend.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		, LetrehozasIdo
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note
		,getdate()
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
	     Tranz_id=@tranz_id
		,Org=@org
		,Id= @record_id
		,Nev= @Nev
		,Ertek= @Ertek
		,Karbantarthato='1'
		,Note = @Note
	 WHERE Id=@record_id 
 END

print '''PASSWORD_POLICY_REGEXP'' rendszerparameter hozzaadasa'
print 'Org = ' + @org_kod

set @tranz_id  = newid()
set @record_id  = 'ef0e39f5-ae8c-48c7-864a-2a43ff095fa2'
set @Ertek = '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.+[!@#$%^&*-]).{PASSLENGTH,}'
print '@record_id = ' + cast(@record_id as nvarchar(36))
print '@Ertek = ' + @Ertek

set @Nev = 'PASSWORD_POLICY_REGEXP'
set @Note = 'A jelsz� er�ss�g�re vonatkoz� regul�ris kifejez�s �rt�ke.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		, LetrehozasIdo
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note		
		,getdate()
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
	     Tranz_id=@tranz_id
		,Org=@org
		,Id= @record_id
		,Nev= @Nev
		,Ertek= @Ertek
		,Karbantarthato='1'
		,Note = @Note
	 WHERE Id=@record_id 
 END

 print '''PASSWORD_POLICY_EXPIRE_DAYS'' rendszerparameter hozzaadasa'
print 'Org = ' + @org_kod

set @tranz_id  = newid()
set @record_id  = 'ef262764-27d3-40dd-9f42-ea6852d95318'
set @Ertek = '90'
print '@record_id = ' + cast(@record_id as nvarchar(36))
print '@Ertek = ' + @Ertek

set @Nev = 'PASSWORD_POLICY_EXPIRE_DAYS'
set @Note = 'Az er�s jelsz� h�zirend alkalmaz�sa eset�n, h�ny nap mulva j�rjon le a jelsz�.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		, LetrehozasIdo
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note
		,getdate()
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
	     Tranz_id=@tranz_id
		,Org=@org
		,Id= @record_id
		,Nev= @Nev
		,Ertek= @Ertek
		,Karbantarthato='1'
		,Note = @Note
	 WHERE Id=@record_id 
 END

 print '''PASSWORD_POLICY_PASS_LENGTH'' rendszerparameter hozzaadasa'
print 'Org = ' + @org_kod

set @tranz_id  = newid()
set @record_id  = '86ecf355-eeab-4712-b6b5-62dba6787a35'
set @Ertek = '8'
print '@record_id = ' + cast(@record_id as nvarchar(36))
print '@Ertek = ' + @Ertek

set @Nev = 'PASSWORD_POLICY_PASS_LENGTH'
set @Note = 'Legal�bb h�ny karakter hossz� kell, hogy legyen a jelsz�.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		, LetrehozasIdo
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note
		,getdate()
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
	     Tranz_id=@tranz_id
		,Org=@org
		,Id= @record_id
		,Nev= @Nev
		,Ertek= @Ertek
		,Karbantarthato='1'
		,Note = @Note
	 WHERE Id=@record_id 
 END


print '''PASSWORD_POLICY_HISTORY_NUMBER'' rendszerparameter hozzaadasa'
print 'Org = ' + @org_kod

set @tranz_id  = newid()
set @record_id  = 'c754a343-5377-4698-a0de-115dcb0772ba'
set @Ertek = '3'
print '@record_id = ' + cast(@record_id as nvarchar(36))
print '@Ertek = ' + @Ertek

set @Nev = 'PASSWORD_POLICY_HISTORY_NUMBER'
set @Note = 'A strong jelsz� h�zirendhez tartozik, h�ny visszamen�leges jelsz� nem lehet �jra jelsz�.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		, LetrehozasIdo
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note
		,getdate()
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
	     Tranz_id=@tranz_id
		,Org=@org
		,Id= @record_id
		,Nev= @Nev
		,Ertek= @Ertek
		,Karbantarthato='1'
		,Note = @Note
	 WHERE Id=@record_id 
 END
 
 print '''PASSWORD_POLICY_WRONG_PASS_TRY_NUMBER'' rendszerparameter hozzaadasa'
print 'Org = ' + @org_kod

set @tranz_id  = newid()
set @record_id  = 'f9189884-1712-4675-8ddd-42b9a7a48766'
set @Ertek = '3'
print '@record_id = ' + cast(@record_id as nvarchar(36))
print '@Ertek = ' + @Ertek

set @Nev = 'PASSWORD_POLICY_WRONG_PASS_TRY_NUMBER'
set @Note = 'A strong jelsz� h�zirendhez tartozik, h�nyszor lehet  �jra elrontani a jelsz�t.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT' 
	  insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		, LetrehozasIdo
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note
		,getdate()
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
	     Tranz_id=@tranz_id
		,Org=@org
		,Id= @record_id
		,Nev= @Nev
		,Ertek= @Ertek
		,Karbantarthato='1'
		,Note = @Note
	 WHERE Id=@record_id 
 END
 
 
 