SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT 1 FROM sys.objects where name = 'sp_KRT_FelhasznalokGet' and type = 'P')
drop procedure [sp_KRT_FelhasznalokGet]
GO

CREATE procedure [dbo].[sp_KRT_FelhasznalokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Felhasznalok.Id,
	   KRT_Felhasznalok.Org,
	   KRT_Felhasznalok.Partner_id,
	   KRT_Felhasznalok.Tipus,
	   KRT_Felhasznalok.UserNev,
	   KRT_Felhasznalok.Nev,
	   KRT_Felhasznalok.Jelszo,
	   KRT_Felhasznalok.JelszoLejaratIdo,
	   KRT_Felhasznalok.System,
	   KRT_Felhasznalok.Kiszolgalo,
	   KRT_Felhasznalok.DefaultPrivatKulcs,
	   KRT_Felhasznalok.Partner_Id_Munkahely,
	   KRT_Felhasznalok.MaxMinosites,
	   KRT_Felhasznalok.EMail,
	   KRT_Felhasznalok.WrongPassCnt,
	   KRT_Felhasznalok.Engedelyezett,
	   KRT_Felhasznalok.Telefonszam,
	   KRT_Felhasznalok.Beosztas,
	   KRT_Felhasznalok.Ver,
	   KRT_Felhasznalok.Note,
	   KRT_Felhasznalok.Stat_id,
	   KRT_Felhasznalok.ErvKezd,
	   KRT_Felhasznalok.ErvVege,
	   KRT_Felhasznalok.Letrehozo_id,
	   KRT_Felhasznalok.LetrehozasIdo,
	   KRT_Felhasznalok.Modosito_id,
	   KRT_Felhasznalok.ModositasIdo,
	   KRT_Felhasznalok.ZarolasIdo,
	   KRT_Felhasznalok.Zarolo_id,
	   KRT_Felhasznalok.Tranz_id,
	   KRT_Felhasznalok.UIAccessLog_id
	   from 
		 KRT_Felhasznalok as KRT_Felhasznalok 
	   where
		 KRT_Felhasznalok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


