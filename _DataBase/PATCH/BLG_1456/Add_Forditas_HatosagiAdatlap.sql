declare @OrgKod nvarchar(100)
set @OrgKod = (select Kod from KRT_Orgok where Id = '450B510A-7CAA-46B0-83E3-18445C0C53A9')

print '@Org = ' + @OrgKod

declare @id uniqueidentifier
set @id = '45691089-533D-4ED1-B7B2-2D2D4F67B064'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	print 'insert'

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		@OrgKod,
		'eRecord',
		'eRecordComponent/OnkormanyzatTab.ascx',
		'HatosagAltalVisszafizetettOsszeg_Felirat',
		'22.) a Ket 71/A. � (2) a) alapj�n a hat�s�g �ltal visszafizetett �sszeg (Ft-ban):'
	)
END

set @id = 'F2BE91EB-DA23-4CA1-B80C-F11255D23C40'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	print 'insert'

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		@OrgKod,
		'eRecord',
		'eRecordComponent/OnkormanyzatTab.ascx',
		'HatosagotTerheloEljKtsg_Felirat',
		'23.) a Ket 71/A. � (2) b) alapj�n a hat�s�got terhel� elj�r�si k�lts�g �sszege (Ft-ban):'
	)
END