declare @admin uniqueidentifier
set @admin = '54E861A5-36ED-44CA-BAA7-C287D125B309'

declare @now datetime
set @now = GETDATE()

declare @tranz_id uniqueidentifier
set @tranz_id = NEWID()

begin tran

-- DONTES_FORMAJA_ONK
update KRT_KodTarak
Set Nev = 'Az �KR 47. �-a alapj�n t�rt�n� megsz�ntet�s',
	RovidNev = '5',
	Sorrend = '05',
	Ver = Ver+1,
	Modosito_Id = @admin,
	ModositasIdo = @now,
	Tranz_id = @tranz_id
where Id = '4FD402DD-5245-4A8E-B095-9CBAB99DC54C'

exec sp_LogRecordToHistory 'KRT_KodTarak','4FD402DD-5245-4A8E-B095-9CBAB99DC54C'
					,'KRT_KodTarakHistory',1,@admin,@now


update KRT_KodTarak
Set Nev = 'Az �KR 46 �-a alapj�n t�rt�n� visszautas�t�s',
	RovidNev = '4',
	Sorrend = '04',
	Ver = Ver+1,
	Modosito_Id = @admin,
	ModositasIdo = @now,
	Tranz_id = @tranz_id
where Id = '039534D1-EE4F-44D9-A006-C06469F6ACA2'

exec sp_LogRecordToHistory 'KRT_KodTarak','039534D1-EE4F-44D9-A006-C06469F6ACA2'
					,'KRT_KodTarakHistory',1,@admin,@now

--Hat�s�gi szerz�d�s
update KRT_KodTarak
Set RovidNev = '3',
	Sorrend = '03',
	Ver = Ver+1,
	Modosito_Id = @admin,
	ModositasIdo = @now,
	Tranz_id = @tranz_id
where Id = '07FB02BE-A6DD-4064-A584-25109DEC6537'

exec sp_LogRecordToHistory 'KRT_KodTarak','07FB02BE-A6DD-4064-A584-25109DEC6537'
					,'KRT_KodTarakHistory',1,@admin,@now

--DONTES_FORMAJA_ALLAMIG

update KRT_KodTarak
Set nev = 'Az �KR 46 �-a alapj�n t�rt�n� visszautas�t�s', 
	Ver = Ver+1,
	Modosito_Id = @admin,
	ModositasIdo = @now,
	Tranz_id = @tranz_id
where Id = 'AD13974F-828C-446C-B78E-AE91BE519FBF'

exec sp_LogRecordToHistory 'KRT_KodTarak','AD13974F-828C-446C-B78E-AE91BE519FBF'
					,'KRT_KodTarakHistory',1,@admin,@now

update KRT_KodTarak
Set nev = 'Az �KR 47. � (1) bekezd�s a) - f) pontjai alapj�n t�rt�n� megsz�ntet�s', 
	Ver = Ver+1,
	Modosito_Id = @admin,
	ModositasIdo = @now,
	Tranz_id = @tranz_id
where Id = '9DD9D4C9-CB78-44C0-835A-D8659AC5842E'


exec sp_LogRecordToHistory 'KRT_KodTarak','9DD9D4C9-CB78-44C0-835A-D8659AC5842E'
					,'KRT_KodTarakHistory',1,@admin,@now

update KRT_KodTarak
Set nev = 'Az �KR 47. � (1) bekezd�s g) pontja alapj�n t�rt�n� megsz�ntet�s', 
	Ver = Ver+1,
	Modosito_Id = @admin,
	ModositasIdo = @now,
	Tranz_id = @tranz_id
where Id = 'F0C62B16-D6DD-406D-980D-4C98E92F1646'


exec sp_LogRecordToHistory 'KRT_KodTarak','F0C62B16-D6DD-406D-980D-4C98E92F1646'
					,'KRT_KodTarakHistory',1,@admin,@now

/* select * from KRT_KodTarak
where KodCsoport_Id = (select id from KRT_KodCsoportok where Kod = 'DONTES_FORMAJA_ONK')
and getdate() between Ervkezd and ErvVege
order by Sorrend */

/* select * from KRT_KodTarak
where KodCsoport_Id = (select id from KRT_KodCsoportok where Kod = 'DONTES_FORMAJA_ALLAMIG')
and getdate() between Ervkezd and ErvVege
order by Sorrend */


commit