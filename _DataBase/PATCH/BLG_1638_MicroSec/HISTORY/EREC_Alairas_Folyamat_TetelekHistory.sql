-- =========================================
-- Create History table for EREC_Alairas_Folyamat_Tetelek
-- =========================================
 
IF OBJECT_ID('EREC_Alairas_Folyamat_TetelekHistory', 'U') IS NOT NULL
  DROP TABLE EREC_Alairas_Folyamat_TetelekHistory
GO
 
CREATE TABLE EREC_Alairas_Folyamat_TetelekHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 AlairasFolyamat_Id uniqueidentifier,  
 IraIrat_Id uniqueidentifier,  
 Csatolmany_Id uniqueidentifier,  
 AlairasStatus Nvarchar(64),  
 Hiba Nvarchar(400),  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_EREC_Alairas_Folyamat_TetelekHistory_ID_VER ON EREC_Alairas_Folyamat_TetelekHistory
(
	Id,Ver ASC
)
GO