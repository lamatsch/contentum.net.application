/****** Object:  View [dbo].[EREC_Alairas_Folyamat_TetelekHistory]    Script Date: 3/23/2018 9:54:28 AM ******/
DROP VIEW [dbo].[EREC_Alairas_Folyamat_TetelekHistory]
GO

/****** Object:  View [dbo].[EREC_Alairas_Folyamat_TetelekHistory]    Script Date: 3/23/2018 9:54:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @DB nvarchar(100)
DECLARE @SQL nvarchar(max)
SELECT @DB = DB_NAME()

SET @SQL = 'CREATE view [dbo].[EREC_Alairas_Folyamat_TetelekHistory] as select * from ' + @DB + '_History.dbo.EREC_Alairas_Folyamat_TetelekHistory;'

exec sp_executesql @SQL

GO


