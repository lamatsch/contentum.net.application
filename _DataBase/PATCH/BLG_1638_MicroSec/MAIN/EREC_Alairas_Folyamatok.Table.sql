
/****** Object:  Table [dbo].[EREC_Alairas_Folyamatok]    Script Date: 2017.10.09. 11:25:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EREC_Alairas_Folyamatok](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid()),
	[AlairasStarted] [datetime] NULL DEFAULT (getdate()),
	[AlairasStopped] [datetime] NULL,
	[AlairasStatus] [nvarchar](64) NOT NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_ALAIRAS_FOLYAMATOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamatok] ADD  DEFAULT ((1)) FOR [Ver]
GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamatok] ADD  DEFAULT (getdate()) FOR [ErvKezd]
GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamatok] ADD  DEFAULT (getdate()) FOR [LetrehozasIdo]
GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamatok]  WITH NOCHECK ADD  CONSTRAINT [CK_EREC_Alairas_Folyamatok] CHECK  (((0)=[dbo].[fn_EREC_Alairas_FolyamatokCheckUK]([Id],[ErvKezd],[ErvVege])))
GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamatok] CHECK CONSTRAINT [CK_EREC_Alairas_Folyamatok]
GO


