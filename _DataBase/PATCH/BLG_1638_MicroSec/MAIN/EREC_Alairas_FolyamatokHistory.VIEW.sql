/****** Object:  View [dbo].[EREC_Alairas_FolyamatokHistory]    Script Date: 3/23/2018 9:54:40 AM ******/
DROP VIEW [dbo].[EREC_Alairas_FolyamatokHistory]
GO

/****** Object:  View [dbo].[EREC_Alairas_FolyamatokHistory]    Script Date: 3/23/2018 9:54:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DECLARE @DB nvarchar(100)
DECLARE @SQL nvarchar(max)
SELECT @DB = DB_NAME()

SET @SQL = 'CREATE view [dbo].[EREC_Alairas_FolyamatokHistory] as select * from ' + @DB + '_History.dbo.EREC_Alairas_FolyamatokHistory';

exec sp_executesql @SQL

GO


