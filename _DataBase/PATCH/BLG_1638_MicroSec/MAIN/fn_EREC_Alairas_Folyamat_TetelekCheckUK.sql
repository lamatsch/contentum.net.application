
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--FF, 20070926
IF  EXISTS (SELECT * FROM sys.Objects WHERE object_id = OBJECT_ID(N'dbo.CK_EREC_Alairas_Folyamat_Tetelek') AND type = N'C')
  ALTER TABLE EREC_Alairas_Folyamat_Tetelek DROP CONSTRAINT CK_EREC_Alairas_Folyamat_Tetelek  
GO
--FF

IF  EXISTS (SELECT * FROM sys.Objects WHERE object_id = OBJECT_ID(N'dbo.fn_EREC_Alairas_Folyamat_TetelekCheckUK') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION dbo.fn_EREC_Alairas_Folyamat_TetelekCheckUK
GO

CREATE FUNCTION dbo.fn_EREC_Alairas_Folyamat_TetelekCheckUK (
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

-- nincsenek megadva oszlopok a Unique Key ellenorzeshez
SET @ret = 0      
      
Return @ret

END

GO

ALTER TABLE EREC_Alairas_Folyamat_Tetelek  
WITH NOCHECK ADD CONSTRAINT 
CK_EREC_Alairas_Folyamat_Tetelek
CHECK  (((0)=dbo.fn_EREC_Alairas_Folyamat_TetelekCheckUK(Id,ErvKezd,ErvVege)
))

GO
