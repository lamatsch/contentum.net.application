

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_Alairas_Folyamat_TetelekGet')
            and   type = 'P')
   drop procedure sp_EREC_Alairas_Folyamat_TetelekGet
go

create procedure sp_EREC_Alairas_Folyamat_TetelekGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_Alairas_Folyamat_Tetelek.Id,
	   EREC_Alairas_Folyamat_Tetelek.AlairasFolyamat_Id,
	   EREC_Alairas_Folyamat_Tetelek.IraIrat_Id,
	   EREC_Alairas_Folyamat_Tetelek.Csatolmany_Id,
	   EREC_Alairas_Folyamat_Tetelek.AlairasStatus,
	   EREC_Alairas_Folyamat_Tetelek.Hiba,
	   EREC_Alairas_Folyamat_Tetelek.Ver,
	   EREC_Alairas_Folyamat_Tetelek.Note,
	   EREC_Alairas_Folyamat_Tetelek.Stat_id,
	   EREC_Alairas_Folyamat_Tetelek.ErvKezd,
	   EREC_Alairas_Folyamat_Tetelek.ErvVege,
	   EREC_Alairas_Folyamat_Tetelek.Letrehozo_id,
	   EREC_Alairas_Folyamat_Tetelek.LetrehozasIdo,
	   EREC_Alairas_Folyamat_Tetelek.Modosito_id,
	   EREC_Alairas_Folyamat_Tetelek.ModositasIdo,
	   EREC_Alairas_Folyamat_Tetelek.Zarolo_id,
	   EREC_Alairas_Folyamat_Tetelek.ZarolasIdo,
	   EREC_Alairas_Folyamat_Tetelek.Tranz_id,
	   EREC_Alairas_Folyamat_Tetelek.UIAccessLog_id
	   from 
		 EREC_Alairas_Folyamat_Tetelek as EREC_Alairas_Folyamat_Tetelek 
	   where
		 EREC_Alairas_Folyamat_Tetelek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
