set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_Alairas_Folyamat_TetelekHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_EREC_Alairas_Folyamat_TetelekHistoryGetAllRecord
go

create procedure sp_EREC_Alairas_Folyamat_TetelekHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'L�trehoz�s' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_Alairas_Folyamat_TetelekHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Alairas_Folyamat_TetelekHistory Old
         inner join EREC_Alairas_Folyamat_TetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'AlairasFolyamat_Id' as ColumnName,               cast(Old.AlairasFolyamat_Id as nvarchar(99)) as OldValue,
               cast(New.AlairasFolyamat_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Alairas_Folyamat_TetelekHistory Old
         inner join EREC_Alairas_Folyamat_TetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasFolyamat_Id != New.AlairasFolyamat_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'IraIrat_Id' as ColumnName,               cast(Old.IraIrat_Id as nvarchar(99)) as OldValue,
               cast(New.IraIrat_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Alairas_Folyamat_TetelekHistory Old
         inner join EREC_Alairas_Folyamat_TetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IraIrat_Id != New.IraIrat_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Csatolmany_Id' as ColumnName,               cast(Old.Csatolmany_Id as nvarchar(99)) as OldValue,
               cast(New.Csatolmany_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Alairas_Folyamat_TetelekHistory Old
         inner join EREC_Alairas_Folyamat_TetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csatolmany_Id != New.Csatolmany_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'AlairasStatus' as ColumnName,               cast(Old.AlairasStatus as nvarchar(99)) as OldValue,
               cast(New.AlairasStatus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Alairas_Folyamat_TetelekHistory Old
         inner join EREC_Alairas_Folyamat_TetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasStatus != New.AlairasStatus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Hiba' as ColumnName,               cast(Old.Hiba as nvarchar(99)) as OldValue,
               cast(New.Hiba as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Alairas_Folyamat_TetelekHistory Old
         inner join EREC_Alairas_Folyamat_TetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Hiba != New.Hiba 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go