
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_Alairas_Folyamat_TetelekInvalidate')
            and   type = 'P')
   drop procedure sp_EREC_Alairas_Folyamat_TetelekInvalidate
go

create procedure sp_EREC_Alairas_Folyamat_TetelekInvalidate
        @Id							uniqueidentifier,
		@ExecutorUserId				uniqueidentifier,
		@ExecutionTime					datetime			

as

BEGIN TRY
--BEGIN TRANSACTION InvalidateTransaction

  
	set nocount on
   
   SET @ExecutionTime = getdate()

	-- Zarolas ellenorzese:
	DECLARE @IsLockedByOtherUser BIT
	exec sp_GetLockingInfo 'EREC_Alairas_Folyamat_Tetelek',@Id,@ExecutorUserId,@IsLockedByOtherUser OUTPUT,null

	if (@IsLockedByOtherUser = 0)
	BEGIN
   
      DECLARE @Act_ErvKezd datetime
      DECLARE @Act_ErvVege datetime
      DECLARE @Act_Ver int
      
      select
         @Act_ErvKezd = ErvKezd,
         @Act_ErvVege = ErvVege,
         @Act_Ver = Ver
      from EREC_Alairas_Folyamat_Tetelek
      where Id = @Id
      
      if @@rowcount = 0
	   begin
	    	RAISERROR('[50602]',16,1)
	   end
      
      if @Act_ErvVege <= @ExecutionTime
      begin
         -- már érvénytelenítve van
         RAISERROR('[50603]',16,1)
      end
      
      IF @Act_Ver is null
	   	SET @Act_Ver = 2	
      ELSE
	   	SET @Act_Ver = @Act_Ver+1
      
      if @Act_ErvKezd > @ExecutionTime
      begin
      
         -- ha a jövőben kezdődött volna el, az ErvKezd-et is beállítjuk
      
		   UPDATE EREC_Alairas_Folyamat_Tetelek
		   SET		 					
		   		 ErvVege = @ExecutionTime,
                ErvKezd = @ExecutionTime,
                Ver     = @Act_Ver
		   WHERE
		         Id = @Id
        
      end
      else
      begin
   
		   UPDATE EREC_Alairas_Folyamat_Tetelek
		   SET		 					
		   		 ErvVege = @ExecutionTime,
                Ver     = @Act_Ver
		   WHERE
		         Id = @Id
        
      end

		if @@rowcount != 1
		begin
			RAISERROR('[50601]',16,1)
		end        else
        begin
           
           /* History Log */
           exec sp_LogRecordToHistory 'EREC_Alairas_Folyamat_Tetelek',@Id
                 ,'EREC_Alairas_Folyamat_TetelekHistory',2,@ExecutorUserId,@ExecutionTime
           
           
        end	END
	ELSE BEGIN
		RAISERROR('[50699]',16,1)
	END


--COMMIT TRANSACTION InvalidateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InvalidateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

go


/* használt errorcode-ok:
	50601: UPDATE sikertelen (update-elt sorok száma 0)
   50602: nincs ilyen rekord
   50603: Már érvénytelenítve van a rekord
	50699: rekord zárolva
*/