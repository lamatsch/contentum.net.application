
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_Alairas_FolyamatokGetAll')
            and   type = 'P')
   drop procedure sp_EREC_Alairas_FolyamatokGetAll
go

create procedure sp_EREC_Alairas_FolyamatokGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_Alairas_Folyamatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_Alairas_Folyamatok.Id,
	   EREC_Alairas_Folyamatok.AlairasStarted,
	   EREC_Alairas_Folyamatok.AlairasStopped,
	   EREC_Alairas_Folyamatok.AlairasStatus,
	   EREC_Alairas_Folyamatok.Ver,
	   EREC_Alairas_Folyamatok.Note,
	   EREC_Alairas_Folyamatok.Stat_id,
	   EREC_Alairas_Folyamatok.ErvKezd,
	   EREC_Alairas_Folyamatok.ErvVege,
	   EREC_Alairas_Folyamatok.Letrehozo_id,
	   EREC_Alairas_Folyamatok.LetrehozasIdo,
	   EREC_Alairas_Folyamatok.Modosito_id,
	   EREC_Alairas_Folyamatok.ModositasIdo,
	   EREC_Alairas_Folyamatok.Zarolo_id,
	   EREC_Alairas_Folyamatok.ZarolasIdo,
	   EREC_Alairas_Folyamatok.Tranz_id,
	   EREC_Alairas_Folyamatok.UIAccessLog_id  
   from 
     EREC_Alairas_Folyamatok as EREC_Alairas_Folyamatok      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go