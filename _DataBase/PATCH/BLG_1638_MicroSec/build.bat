@echo off

set sqlcmd="sqlcmd.exe"

if "%1"=="" goto :help
if "%2"=="" goto :help
if "%3"=="" goto :help

echo. !!!!! Adatb�zis m�dos�t� szkriptek futtat�sa az al�bbi be�ll�t�sokkal !!!!!
echo Szerver: %1 Adatb�zis: %2 HISTORY_Adatb�zis: %3

@echo on
cd HISTORY
for %%I in (*.sql) do %sqlcmd% -S %1 -d %3 -i %%I
cd ..
@echo off


@echo on
cd MAIN
for %%I in (*.sql) do %sqlcmd% -S %1 -d %2 -i %%I
@echo off


goto :EOF

:help
echo. Hasznalat:
echo.  1. parameter: szerver nev
echo.  2. parameter: adatbazis neve
echo.  3. parameter: history adatbazis neve
echo.

:EOF