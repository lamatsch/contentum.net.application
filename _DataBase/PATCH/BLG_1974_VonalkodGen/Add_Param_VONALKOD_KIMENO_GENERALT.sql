 -- BLG_1974 VONALKOD_KIMENO_GENERALT be�ll�t�s
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
-- VONALKOD_KIMENO_GENERALT
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='E1213508-AB0A-4BE3-A2E5-A0A57D1E0BE3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

print 'VONALKOD_KIMENO_GENERALT be�ll�t�s'
if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'E1213508-AB0A-4BE3-A2E5-A0A57D1E0BE3'
		,'VONALKOD_KIMENO_GENERALT'
		,'0'
		,'1'
		,'A vonalk�d gener�l�sa belso keletkez�su irat iktat�sa eset�n'
		); 
 END 
 
 if @org_kod = 'NMHH' 
BEGIN 
	Print @org_kod +': NMHH' 
	UPDATE KRT_Parameterek
	   SET Ertek='1'
	   WHERE Id='E1213508-AB0A-4BE3-A2E5-A0A57D1E0BE3'
END
GO