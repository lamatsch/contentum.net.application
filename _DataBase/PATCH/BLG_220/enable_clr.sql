--clre engedelyezese
print 'Configure SQL to allow running CLR code...'
GO

sp_configure 'clr enabled',1

PRINT 'RECONFIGURE...'
RECONFIGURE
go

-- Mark the database as TRUSTWORTHY, to allow you to run EXTERNAL_ACCESS CLR code.

--alter database edok set trustworthy ON

PRINT 'alter database $(DatabaseName) set trustworthy ON...'
alter database $(DatabaseName) set trustworthy ON
