IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokHistory]'))
	DROP VIEW [dbo].[EREC_IraIratokHistory]
GO

GO

/****** Object:  View [dbo].[EREC_IraIratokHistory]    Script Date: 2017. 09. 26. 14:11:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[EREC_IraIratokHistory] as select * from $(history).dbo.EREC_IraIratokHistory;

GO


