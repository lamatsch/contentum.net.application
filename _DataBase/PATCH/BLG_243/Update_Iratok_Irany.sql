declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'  
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id=@org)

print 'Org = ' + @org_kod

IF @org_kod IN ('BOPMH','NMHH')  
BEGIN
    print 'update'

	declare @tranz_id uniqueidentifier = newid();
	declare @felh_id uniqueidentifier = (select Id from KRT_Felhasznalok where nev like 'Axis felhasználó')
	declare @date datetime = getdate()

	begin tran

		update EREC_IraIratok
		set PostazasIranya = '0', -- Belső
			Tranz_id = @tranz_id,
			Ver = Ver +1,
			ModositasIdo = @date,
			Modosito_id = @felh_id
		where
			PostazasIranya = '2' --Kimenő
	
		--select PostazasIranya, * from EREC_IraIratok
		--where Tranz_id = @tranz_id

		/* History Log */

		declare @row_ids varchar(MAX)
		set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from EREC_IraIratok where Tranz_id = @tranz_id FOR XML PATH('')),1, 2, '')

		if @row_ids is not null
		  set @row_ids = @row_ids + '''';

		exec sp_LogRecordsToHistory_Tomeges
		 @TableName = 'EREC_IraIratok'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @felh_id
		,@VegrehajtasIdo = @date
	
		--select * from EREC_IraIratokHistory
		--where Tranz_id = @tranz_id

		commit

END