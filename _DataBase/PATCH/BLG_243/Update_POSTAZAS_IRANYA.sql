declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'  
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id=@org)

print 'Org = ' + @org_kod

IF @org_kod IN ('BOPMH','NMHH') 
BEGIN
	print 'Update'

	declare @tranz_id uniqueidentifier = newid();
	declare @felh_id uniqueidentifier = (select Id from KRT_Felhasznalok where nev like 'Axis felhaszn�l�')
	declare @date datetime = getdate()
	declare @Id uniqueidentifier

	BEGIN TRAN

	set @Id = '155152F1-D8E9-42EB-9C51-0A6E99B8B906'

	-- Bels� -> Kimen�
	update KRT_KodTarak
	set Nev = 'Kimen�',
		Tranz_id = @tranz_id,
		Ver = Ver +1,
	    ModositasIdo = @date,
	    Modosito_id = @felh_id
	where Id = @Id

	exec sp_LogRecordToHistory 'KRT_KodTarak',@Id,'KRT_KodTarakHistory',1, @felh_id ,@date   

	-- Kimen� �rv�nytelen�t�se
	set @Id = '910412E8-ECCE-400A-9F6B-01CC888A2B25'

	UPDATE KRT_KodTarak
	SET		 					
		ErvVege = @date,
        Ver     = Ver + 1
	 WHERE
		Id = @Id

	exec sp_LogRecordToHistory 'KRT_KodTarak',@Id,'KRT_KodTarakHistory',2,@felh_id ,@date

	COMMIT
END