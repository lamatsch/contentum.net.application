﻿-- CREATE_LEVELTARI_ADATCSOMAG rendszerparameter	 

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='78C2CD19-EBEA-41FE-891F-3A32AD535E14') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'78C2CD19-EBEA-41FE-891F-3A32AD535E14'
		,'CREATE_LEVELTARI_ADATCSOMAG'
		,'1'
		,'1'
		,'Levéltári átadáskor létejöjjön-e átadási atadcsomag.'
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		,Id='78C2CD19-EBEA-41FE-891F-3A32AD535E14'
		,Nev='CREATE_LEVELTARI_ADATCSOMAG'
		,Ertek='1'
		,Karbantarthato='1'
		,Note='Levéltári átadáskor létejöjjön-e átadási atadcsomag.'
	 WHERE Id=@record_id 
 END
