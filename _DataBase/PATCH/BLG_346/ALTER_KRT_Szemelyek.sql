IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'Beosztas'
          AND Object_ID = Object_ID(N'dbo.KRT_Szemelyek'))
begin
	ALTER TABLE [dbo].[KRT_Szemelyek] ADD [Beosztas] nvarchar(100) null  ;
end

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'MinositesiSzint'
          AND Object_ID = Object_ID(N'dbo.KRT_Szemelyek'))
begin
	ALTER TABLE [dbo].[KRT_Szemelyek] ADD [MinositesiSzint] nvarchar(64) null  ;
end

