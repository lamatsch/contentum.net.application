set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('KRT_SzemelyekHistory')
            and   type = 'V')
   drop view KRT_SzemelyekHistory
go
/****** Object:  View [dbo].[KRT_SzemelyekHistory]    ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view KRT_SzemelyekHistory as select * from $(HistoryDatabase).dbo.KRT_SzemelyekHistory;


GO


