-- BLG_346: PARTNERKAPCSOLAT_TIPUSA kódcsoportba új kódtár érték: MINOSITO	 

Print 'PARTNERKAPCSOLAT_TIPUSA kódcsoport'


 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_KodCsoportok
			where Id='A17AB343-1EB3-4035-868C-7E48F2AC3019') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'ERROR: Nincs PARTNERKAPCSOLAT_TIPUSA kódcsoport vagy nem jó az ID-ja' 

END 
ELSE 
BEGIN 
	DECLARE @org_id uniqueidentifier SET @org_id = (select Org from KRT_KodCsoportok
				where Id='A17AB343-1EB3-4035-868C-7E48F2AC3019') 
	Print '@org_id='+convert(NVARCHAR(100),@record_id) 
	IF @org_id IS NULL
	BEGIN
		print 'PARTNERKAPCSOLAT_TIPUSA kódcsoportnál nincs org megadva'
		Print 'UPDATE'
		UPDATE KRT_KodCsoportok
		SET 
		  Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		WHERE Id='A17AB343-1EB3-4035-868C-7E48F2AC3019' 
		if @@ERROR<>0 
			begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
	END		
	Print 'MINOSITO kódtárérték' 
	SET @record_id= null
	SET @record_id = (select Id from KRT_KodTarak
		where Id='B5DC24E8-C2A9-E711-80C6-00155D020BD9') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
		
		Print 'INSERT' 
		insert into KRT_KodTarak(
			KodCsoport_Id
		,Tranz_id
		,Stat_id
		,Org
		,Id
		,Kod
		,Nev
		,RovidNev
		,Modosithato
		,Sorrend
		) values (
		'A17AB343-1EB3-4035-868C-7E48F2AC3019'
		,null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'B5DC24E8-C2A9-E711-80C6-00155D020BD9'
		,'M'
		,'Minősítő'
		,'Minősítő'
		,'0'
		,'M'
		); 
		if @@ERROR<>0 
			begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
	END 
	ELSE 
	BEGIN 
		Print 'UPDATE'
		UPDATE KRT_KodTarak
		SET 
			KodCsoport_Id='A17AB343-1EB3-4035-868C-7E48F2AC3019'
		,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		,Kod='M'
		,Nev='Minősítő'
		,RovidNev='Minősítő'
		,Modosithato='0'
		,Sorrend='M'
		WHERE Id='B5DC24E8-C2A9-E711-80C6-00155D020BD9'
		if @@ERROR<>0 
			begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
	END
END

