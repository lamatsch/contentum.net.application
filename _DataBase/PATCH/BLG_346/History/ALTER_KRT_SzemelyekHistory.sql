if not exists (select 1
            from  sysobjects
           where  id = object_id('KRT_SzemelyekHistory')
            and   type = 'U')
  begin
/****** Object:  Table [dbo].[KRT_SzemelyekHistory]    Script Date: 2017.07.04. 11:36:02 ******/

CREATE TABLE [dbo].[KRT_SzemelyekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_SzemelyekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Partner_Id] [uniqueidentifier] NULL,
	[Partner_Id_Ver] [int] NULL,
	[AnyjaNeve] [nvarchar](100) NULL,
	[ApjaNeve] [nvarchar](100) NULL,
	[SzuletesiNev] [nvarchar](100) NULL,
	[SzuletesiOrszag] [nvarchar](100) NULL,
	[SzuletesiOrszagId] [uniqueidentifier] NULL,
	[UjTitulis] [nvarchar](10) NULL,
	[UjCsaladiNev] [nvarchar](100) NULL,
	[UjUtonev] [nvarchar](100) NULL,
	[SzuletesiHely] [nvarchar](400) NULL,
	[SzuletesiHely_id] [uniqueidentifier] NULL,
	[SzuletesiIdo] [datetime] NULL,
	[TAJSzam] [nvarchar](20) NULL,
	[SZIGSzam] [nvarchar](20) NULL,
	[Neme] [char](1) NULL,
	[SzemelyiAzonosito] [nvarchar](20) NULL,
	[Adoazonosito] [nvarchar](20) NULL,
	[Adoszam] [nvarchar](20) NULL,
	[Beosztas] [Nvarchar](100) null,
	[MinositesiSzint] [nvarchar](64) null,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[AnyjaNeveCsaladiNev] [nvarchar](100) NULL,
	[AnyjaNeveElsoUtonev] [nvarchar](100) NULL,
	[AnyjaNeveTovabbiUtonev] [nvarchar](100) NULL,
	[SzuletesiCsaladiNev] [nvarchar](100) NULL,
	[SzuletesiElsoUtonev] [nvarchar](100) NULL,
	[SzuletesiTovabbiUtonev] [nvarchar](100) NULL,
	[UjTovabbiUtonev] [nvarchar](100) NULL,
	[Allampolgarsag] [nvarchar](100) NULL,
	[KulfoldiAdoszamJelolo] [char](1) NULL,
 CONSTRAINT [PK_KRT_SzemelyekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

END


IF not EXISTS(SELECT *
          FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'KRT_SzemelyekHistory'
                 AND COLUMN_NAME = 'Beosztas') 
  begin
	ALTER TABLE [dbo].[KRT_SzemelyekHistory] ADD [Beosztas] nvarchar(100) null  ;
end

IF not EXISTS(SELECT *
          FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'KRT_SzemelyekHistory'
                 AND COLUMN_NAME = 'MinositesiSzint') 
  begin
	ALTER TABLE [dbo].[KRT_SzemelyekHistory] ADD [MinositesiSzint] nvarchar(64) null  ;
end