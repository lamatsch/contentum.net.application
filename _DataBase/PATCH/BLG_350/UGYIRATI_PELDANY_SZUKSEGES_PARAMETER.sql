﻿-- UGYIRATI_PELDANY_SZUKSEGES rendszerparameter	 
-- FPH = BOPMH = 0, NMHH = 1

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='5BC55766-D4AC-E711-80C6-00155D020BD9') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'5BC55766-D4AC-E711-80C6-00155D020BD9'
		,'UGYIRATI_PELDANY_SZUKSEGES'
		,'0'
		,'1'
		,'1-e érték esetén a Belső iktatásnál az Ügyirati példány checkbox alapértelmezetten jelölt lesz, 0-nál nem'
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		,Id='5BC55766-D4AC-E711-80C6-00155D020BD9'
		,Nev='UGYIRATI_PELDANY_SZUKSEGES'
		,Ertek='0'
		,Karbantarthato='1'
		,Note='1-e érték esetén a Belső iktatásnál az Ügyirati példány checkbox alapértelmezetten jelölt lesz, 0-nál nem'
	 WHERE Id=@record_id 
 END

 if @org_kod = 'NMHH' 
BEGIN 
	Print @org_kod +': UGYIRATI_PELDANY_SZUKSEGES = 1'
	UPDATE KRT_Parameterek
	   SET Ertek='1'
	   WHERE Id='5BC55766-D4AC-E711-80C6-00155D020BD9'
END