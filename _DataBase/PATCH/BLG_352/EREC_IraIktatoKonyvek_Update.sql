-- BLG_352 Iktatókönyvek kezelési módját alapértelmezetten 'E'-re állítja
Begin tran

UPDATE [dbo].[EREC_IraIktatoKonyvek]
SET [KezelesTipusa] = 'E'
WHERE [KezelesTipusa] is null

Commit tran