﻿-- HATOSAGI_ADATOK_BEJOVOHOZ rendszerparameter	 
-- FPH = Igen, BOPMH = NMHH = Nem

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='AAF9CFA4-3EA7-E711-80C6-00155D020BD9') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'AAF9CFA4-3EA7-E711-80C6-00155D020BD9'
		,'HATOSAGI_ADATOK_BEJOVOHOZ'
		,'Nem'
		,'1'
		,'Igen = Bejövő iratnál lehet hatósági adatot kitölteni, Nem = Csak kimenő iratnál lehet'
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		,Id='AAF9CFA4-3EA7-E711-80C6-00155D020BD9'
		,Nev='HATOSAGI_ADATOK_BEJOVOHOZ'
		,Ertek='Nem'
		,Karbantarthato='1'
		,Note='Igen = Bejövő iratnál lehet hatósági adatot kitölteni, Nem = Csak kimenő iratnál lehet'
	 WHERE Id=@record_id 
 END

 if @org_kod = 'FPH' 
BEGIN 
	Print @org_kod +': HATOSAGI_ADATOK_BEJOVOHOZ = Igen'
	UPDATE KRT_Parameterek
	   SET Ertek='Igen'
	   WHERE Id='AAF9CFA4-3EA7-E711-80C6-00155D020BD9'
END