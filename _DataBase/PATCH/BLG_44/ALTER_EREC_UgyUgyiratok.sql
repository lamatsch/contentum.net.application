IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'Ugy_Fajtaja'
          AND Object_ID = Object_ID(N'dbo.EREC_UGYUGYIRATOK'))
BEGIN
     print 'ADD COLUMN Ugy_Fajtaja'
	ALTER TABLE [dbo].[EREC_UGYUGYIRATOK]
	 ADD [Ugy_Fajtaja] nvarchar(64) NULL;

END
