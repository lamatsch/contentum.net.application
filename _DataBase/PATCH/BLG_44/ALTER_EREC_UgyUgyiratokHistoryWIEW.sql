set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EREC_UgyUgyiratokHistory')
            and   type = 'V')
   drop view EREC_UgyUgyiratokHistory
go
/****** Object:  View [dbo].[EREC_UgyUgyiratokHistory]    Script Date: 2017.04.05. 18:42:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view EREC_UgyUgyiratokHistory as select * from $(HistoryDatabase).dbo.EREC_UgyUgyiratokHistory;


GO


