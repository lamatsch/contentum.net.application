if not exists (select 1
            from  sysobjects
           where  id = object_id('EREC_UgyUgyiratokHistory')
            and   type = 'U')
  begin
/****** Object:  Table [dbo].[EREC_UgyUgyiratokHistory]    Script Date: 2017.07.04. 11:36:02 ******/

CREATE TABLE [dbo].[EREC_UgyUgyiratok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_UgyUgyi__Id__61BB7BD9]  DEFAULT (newsequentialid()),
	[Foszam] [int] NULL,
	[Sorszam] [int] NULL,
	[Ugyazonosito] [nvarchar](100) NULL,
	[Alkalmazas_Id] [uniqueidentifier] NULL,
	[UgyintezesModja] [nvarchar](64) NULL,
	[Hatarido] [datetime] NULL,
	[SkontrobaDat] [datetime] NULL,
	[LezarasDat] [datetime] NULL,
	[IrattarbaKuldDatuma] [datetime] NULL,
	[IrattarbaVetelDat] [datetime] NULL,
	[FelhCsoport_Id_IrattariAtvevo] [uniqueidentifier] NULL,
	[SelejtezesDat] [datetime] NULL,
	[FelhCsoport_Id_Selejtezo] [uniqueidentifier] NULL,
	[LeveltariAtvevoNeve] [nvarchar](100) NULL,
	[FelhCsoport_Id_Felulvizsgalo] [uniqueidentifier] NULL,
	[FelulvizsgalatDat] [datetime] NULL,
	[IktatoszamKieg] [nvarchar](2) NULL,
	[Targy] [nvarchar](4000) NULL,
	[UgyUgyirat_Id_Szulo] [uniqueidentifier] NULL,
	[UgyUgyirat_Id_Kulso] [uniqueidentifier] NULL,
	[UgyTipus] [nvarchar](64) NULL,
	[IrattariHely] [nvarchar](100) NULL,
	[SztornirozasDat] [datetime] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Orzo] [uniqueidentifier] NULL,
	[Csoport_Id_Cimzett] [uniqueidentifier] NULL,
	[Jelleg] [nvarchar](64) NULL,
	[IraIrattariTetel_Id] [uniqueidentifier] NULL,
	[IraIktatokonyv_Id] [uniqueidentifier] NULL,
	[SkontroOka] [nvarchar](400) NULL,
	[SkontroVege] [datetime] NULL,
	[Surgosseg] [nvarchar](64) NULL,
	[SkontrobanOsszesen] [int] NULL,
	[MegorzesiIdoVege] [datetime] NULL CONSTRAINT [DF_EREC_UgyUgyiratok_MegorzesiIdoVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Partner_Id_Ugyindito] [uniqueidentifier] NULL,
	[NevSTR_Ugyindito] [nvarchar](400) NULL,
	[ElintezesDat] [datetime] NULL,
	[FelhasznaloCsoport_Id_Ugyintez] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos_Elozo] [uniqueidentifier] NULL,
	[KolcsonKikerDat] [datetime] NULL CONSTRAINT [DF__EREC_UgyU__Kolcs__63A3C44B]  DEFAULT (NULL),
	[KolcsonKiadDat] [datetime] NULL CONSTRAINT [DF__EREC_UgyU__Kolcs__6497E884]  DEFAULT (NULL),
	[Kolcsonhatarido] [datetime] NULL CONSTRAINT [DF__EREC_UgyU__Kolcs__658C0CBD]  DEFAULT (NULL),
	[BARCODE] [nvarchar](100) NULL,
	[IratMetadefinicio_Id] [uniqueidentifier] NULL,
	[Allapot] [nvarchar](64) NULL,
	[TovabbitasAlattAllapot] [nvarchar](64) NULL,
	[Megjegyzes] [nvarchar](400) NULL,
	[Azonosito] [nvarchar](100) NULL,
	[Fizikai_Kezbesitesi_Allapot] [nvarchar](64) NULL,
	[Kovetkezo_Orzo_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Ugyfelelos] [uniqueidentifier] NULL,
	[Elektronikus_Kezbesitesi_Allap] [nvarchar](64) NULL,
	[Kovetkezo_Felelos_Id] [uniqueidentifier] NULL,
	[UtolsoAlszam] [int] NULL,
	[UtolsoSorszam] [int] NULL,
	[IratSzam] [int] NULL,
	[ElintezesMod] [nvarchar](64) NULL,
	[RegirendszerIktatoszam] [nvarchar](100) NULL,
	[GeneraltTargy] [nvarchar](400) NULL,
	[Cim_Id_Ugyindito] [uniqueidentifier] NULL,
	[CimSTR_Ugyindito] [nvarchar](400) NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_UgyUgy__Ver__668030F6]  DEFAULT ((1)),
	[Note] [nvarchar](4000) NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_UgyU__ErvKe__6774552F]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_UgyUgyiratok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_UgyU__Letre__695C9DA1]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[UjOrzesiIdo] [int] NULL,
	[IrattarId] [uniqueidentifier] NULL,
	[LezarasOka] [nvarchar](64) NULL,
	[SkontroOka_Kod] [nvarchar](64) NULL,
	[AKTIV]  AS (case when [ALLAPOT]='90' then (0) else (1) end) PERSISTED NOT NULL,
	[FelfuggesztesOka] [nvarchar](400) NULL,
	[UgyintezesKezdete] [datetime] NULL,
	[FelfuggesztettNapokSzama] [int] NULL,
	[IntezesiIdo] [int] NULL,
	[IntezesiIdoegyseg] [nvarchar](64) NULL,
 CONSTRAINT [EIV_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]



SET ANSI_PADDING OFF


ALTER TABLE [dbo].[EREC_UgyUgyiratok]  WITH NOCHECK ADD  CONSTRAINT [EIV_EIV_FK] FOREIGN KEY([UgyUgyirat_Id_Szulo])
REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id])


ALTER TABLE [dbo].[EREC_UgyUgyiratok] NOCHECK CONSTRAINT [EIV_EIV_FK]


ALTER TABLE [dbo].[EREC_UgyUgyiratok]  WITH NOCHECK ADD  CONSTRAINT [EREC_Ugyirat_UgyiratKulso_FK] FOREIGN KEY([UgyUgyirat_Id_Kulso])
REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id])


ALTER TABLE [dbo].[EREC_UgyUgyiratok] NOCHECK CONSTRAINT [EREC_Ugyirat_UgyiratKulso_FK]


ALTER TABLE [dbo].[EREC_UgyUgyiratok]  WITH NOCHECK ADD  CONSTRAINT [Ugy_Csoport_Felelos_FK] FOREIGN KEY([Csoport_Id_Felelos])
REFERENCES [dbo].[KRT_Csoportok] ([Id])


ALTER TABLE [dbo].[EREC_UgyUgyiratok] NOCHECK CONSTRAINT [Ugy_Csoport_Felelos_FK]


ALTER TABLE [dbo].[EREC_UgyUgyiratok]  WITH NOCHECK ADD  CONSTRAINT [UgyUgyirat_IraIktatokonyv_FK] FOREIGN KEY([IraIktatokonyv_Id])
REFERENCES [dbo].[EREC_IraIktatoKonyvek] ([Id])


ALTER TABLE [dbo].[EREC_UgyUgyiratok] NOCHECK CONSTRAINT [UgyUgyirat_IraIktatokonyv_FK]


end
IF not EXISTS(SELECT *
          FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'EREC_UgyUgyiratokHistory'
                 AND COLUMN_NAME = 'Ugy_fajtaja') 
  begin
	ALTER TABLE [dbo].[EREC_UgyUgyiratokHistory] ADD [Ugy_fajtaja] nvarchar(64) null  ;
end