﻿-- IKTATAS_UGYTIPUS_IRATMETADEFBOL rendszerparameter	 
-- FPH = BOPMH = 1, NMHH = 0

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='7B8BBA83-8997-E711-80C6-00155D020BD9') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'7B8BBA83-8997-E711-80C6-00155D020BD9'
		,'IKTATAS_UGYTIPUS_IRATMETADEFBOL'
		,'1'
		,'0'
		,'Ügytipus választás iratmetadefinició alapján = 1, Nincs iratmetadefiniciós összerendelés = 0'
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		,Id='7B8BBA83-8997-E711-80C6-00155D020BD9'
		,Nev='IKTATAS_UGYTIPUS_IRATMETADEFBOL'
		,Ertek='1'
		,Karbantarthato='0'
		,Note='Ügytipus választás iratmetadefinició alapján = 1, Nincs iratmetadefiniciós összerendelés = 0'
	 WHERE Id=@record_id 
 END

 if @org_kod = 'NMHH' 
BEGIN 
	Print @org_kod +': IKTATAS_UGYTIPUS_IRATMETADEFBOL = 0'
	UPDATE KRT_Parameterek
	   SET Ertek='0'
	   WHERE Id='7B8BBA83-8997-E711-80C6-00155D020BD9'
END