set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_UgyUgyiratokHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_EREC_UgyUgyiratokHistoryGetAllRecord
go

create procedure sp_EREC_UgyUgyiratokHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_UgyUgyiratokHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Foszam' as ColumnName,               cast(Old.Foszam as nvarchar(99)) as OldValue,
               cast(New.Foszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Foszam != New.Foszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Sorszam' as ColumnName,               cast(Old.Sorszam as nvarchar(99)) as OldValue,
               cast(New.Sorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Sorszam != New.Sorszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ugyazonosito ' as ColumnName,               cast(Old.Ugyazonosito  as nvarchar(99)) as OldValue,
               cast(New.Ugyazonosito  as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Ugyazonosito  != New.Ugyazonosito  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Alkalmazas_Id' as ColumnName,               cast(Old.Alkalmazas_Id as nvarchar(99)) as OldValue,
               cast(New.Alkalmazas_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Alkalmazas_Id != New.Alkalmazas_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesModja' as ColumnName,               cast(Old.UgyintezesModja as nvarchar(99)) as OldValue,
               cast(New.UgyintezesModja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyintezesModja != New.UgyintezesModja 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Hatarido' as ColumnName,               cast(Old.Hatarido as nvarchar(99)) as OldValue,
               cast(New.Hatarido as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Hatarido != New.Hatarido 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SkontrobaDat' as ColumnName,               cast(Old.SkontrobaDat as nvarchar(99)) as OldValue,
               cast(New.SkontrobaDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SkontrobaDat != New.SkontrobaDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'LezarasDat' as ColumnName,               cast(Old.LezarasDat as nvarchar(99)) as OldValue,
               cast(New.LezarasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.LezarasDat != New.LezarasDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarbaKuldDatuma ' as ColumnName,               cast(Old.IrattarbaKuldDatuma  as nvarchar(99)) as OldValue,
               cast(New.IrattarbaKuldDatuma  as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IrattarbaKuldDatuma  != New.IrattarbaKuldDatuma  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarbaVetelDat' as ColumnName,               cast(Old.IrattarbaVetelDat as nvarchar(99)) as OldValue,
               cast(New.IrattarbaVetelDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IrattarbaVetelDat != New.IrattarbaVetelDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhCsoport_Id_IrattariAtvevo' as ColumnName,               cast(Old.FelhCsoport_Id_IrattariAtvevo as nvarchar(99)) as OldValue,
               cast(New.FelhCsoport_Id_IrattariAtvevo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhCsoport_Id_IrattariAtvevo != New.FelhCsoport_Id_IrattariAtvevo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SelejtezesDat' as ColumnName,               cast(Old.SelejtezesDat as nvarchar(99)) as OldValue,
               cast(New.SelejtezesDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SelejtezesDat != New.SelejtezesDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhCsoport_Id_Selejtezo' as ColumnName,               cast(Old.FelhCsoport_Id_Selejtezo as nvarchar(99)) as OldValue,
               cast(New.FelhCsoport_Id_Selejtezo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhCsoport_Id_Selejtezo != New.FelhCsoport_Id_Selejtezo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'LeveltariAtvevoNeve' as ColumnName,               cast(Old.LeveltariAtvevoNeve as nvarchar(99)) as OldValue,
               cast(New.LeveltariAtvevoNeve as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.LeveltariAtvevoNeve != New.LeveltariAtvevoNeve 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhCsoport_Id_Felulvizsgalo' as ColumnName,               cast(Old.FelhCsoport_Id_Felulvizsgalo as nvarchar(99)) as OldValue,
               cast(New.FelhCsoport_Id_Felulvizsgalo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhCsoport_Id_Felulvizsgalo != New.FelhCsoport_Id_Felulvizsgalo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelulvizsgalatDat' as ColumnName,               cast(Old.FelulvizsgalatDat as nvarchar(99)) as OldValue,
               cast(New.FelulvizsgalatDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelulvizsgalatDat != New.FelulvizsgalatDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IktatoszamKieg' as ColumnName,               cast(Old.IktatoszamKieg as nvarchar(99)) as OldValue,
               cast(New.IktatoszamKieg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IktatoszamKieg != New.IktatoszamKieg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Targy' as ColumnName,               cast(Old.Targy as nvarchar(99)) as OldValue,
               cast(New.Targy as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Targy != New.Targy 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyUgyirat_Id_Szulo' as ColumnName,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratokAzonosito(Old.UgyUgyirat_Id_Szulo) as OldValue,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratokAzonosito(New.UgyUgyirat_Id_Szulo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyUgyirat_Id_Szulo != New.UgyUgyirat_Id_Szulo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join EREC_UgyUgyiratok FTOld on FTOld.Id = Old.UgyUgyirat_Id_Szulo --and FTOld.Ver = Old.Ver
         left join EREC_UgyUgyiratok FTNew on FTNew.Id = New.UgyUgyirat_Id_Szulo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyUgyirat_Id_Kulso' as ColumnName,               cast(Old.UgyUgyirat_Id_Kulso as nvarchar(99)) as OldValue,
               cast(New.UgyUgyirat_Id_Kulso as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyUgyirat_Id_Kulso != New.UgyUgyirat_Id_Kulso 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyTipus' as ColumnName,               cast(Old.UgyTipus as nvarchar(99)) as OldValue,
               cast(New.UgyTipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyTipus != New.UgyTipus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattariHely' as ColumnName,               cast(Old.IrattariHely as nvarchar(99)) as OldValue,
               cast(New.IrattariHely as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IrattariHely != New.IrattariHely 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SztornirozasDat' as ColumnName,               cast(Old.SztornirozasDat as nvarchar(99)) as OldValue,
               cast(New.SztornirozasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SztornirozasDat != New.SztornirozasDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Felelos' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Felelos) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Felelos) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Felelos != New.Csoport_Id_Felelos 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.Csoport_Id_Felelos --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Csoport_Id_Felelos --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Orzo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Orzo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Orzo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Orzo != New.FelhasznaloCsoport_Id_Orzo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Orzo --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Orzo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Cimzett' as ColumnName,               cast(Old.Csoport_Id_Cimzett as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Cimzett as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Cimzett != New.Csoport_Id_Cimzett 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Jelleg' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Jelleg != New.Jelleg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGY_JELLEG'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Jelleg and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Jelleg and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIrattariTetel_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_IraIrattariTetelekAzonosito(Old.IraIrattariTetel_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_IraIrattariTetelekAzonosito(New.IraIrattariTetel_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IraIrattariTetel_Id != New.IraIrattariTetel_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join EREC_IraIrattariTetelek FTOld on FTOld.Id = Old.IraIrattariTetel_Id --and FTOld.Ver = Old.Ver
         left join EREC_IraIrattariTetelek FTNew on FTNew.Id = New.IraIrattariTetel_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIktatokonyv_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_IraIktatoKonyvekAzonosito(Old.IraIktatokonyv_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_IraIktatoKonyvekAzonosito(New.IraIktatokonyv_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IraIktatokonyv_Id != New.IraIktatokonyv_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join EREC_IraIktatoKonyvek FTOld on FTOld.Id = Old.IraIktatokonyv_Id --and FTOld.Ver = Old.Ver
         left join EREC_IraIktatoKonyvek FTNew on FTNew.Id = New.IraIktatokonyv_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SkontroOka' as ColumnName,               cast(Old.SkontroOka as nvarchar(99)) as OldValue,
               cast(New.SkontroOka as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SkontroOka != New.SkontroOka 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SkontroVege' as ColumnName,               cast(Old.SkontroVege as nvarchar(99)) as OldValue,
               cast(New.SkontroVege as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SkontroVege != New.SkontroVege 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Surgosseg' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Surgosseg != New.Surgosseg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'SURGOSSEG'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Surgosseg and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Surgosseg and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SkontrobanOsszesen' as ColumnName,               cast(Old.SkontrobanOsszesen as nvarchar(99)) as OldValue,
               cast(New.SkontrobanOsszesen as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SkontrobanOsszesen != New.SkontrobanOsszesen 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MegorzesiIdoVege' as ColumnName,               cast(Old.MegorzesiIdoVege as nvarchar(99)) as OldValue,
               cast(New.MegorzesiIdoVege as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MegorzesiIdoVege != New.MegorzesiIdoVege 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id_Ugyindito' as ColumnName,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(Old.Partner_Id_Ugyindito) as OldValue,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(New.Partner_Id_Ugyindito) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Partner_Id_Ugyindito != New.Partner_Id_Ugyindito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Partnerek FTOld on FTOld.Id = Old.Partner_Id_Ugyindito --and FTOld.Ver = Old.Ver
         left join KRT_Partnerek FTNew on FTNew.Id = New.Partner_Id_Ugyindito --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'NevSTR_Ugyindito' as ColumnName,               cast(Old.NevSTR_Ugyindito as nvarchar(99)) as OldValue,
               cast(New.NevSTR_Ugyindito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.NevSTR_Ugyindito != New.NevSTR_Ugyindito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ElintezesDat' as ColumnName,               cast(Old.ElintezesDat as nvarchar(99)) as OldValue,
               cast(New.ElintezesDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ElintezesDat != New.ElintezesDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Ugyintez' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Ugyintez) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Ugyintez) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Ugyintez != New.FelhasznaloCsoport_Id_Ugyintez 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Ugyintez --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Ugyintez --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Felelos_Elozo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Felelos_Elozo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Felelos_Elozo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Felelos_Elozo != New.Csoport_Id_Felelos_Elozo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.Csoport_Id_Felelos_Elozo --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Csoport_Id_Felelos_Elozo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KolcsonKikerDat' as ColumnName,               cast(Old.KolcsonKikerDat as nvarchar(99)) as OldValue,
               cast(New.KolcsonKikerDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KolcsonKikerDat != New.KolcsonKikerDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KolcsonKiadDat' as ColumnName,               cast(Old.KolcsonKiadDat as nvarchar(99)) as OldValue,
               cast(New.KolcsonKiadDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KolcsonKiadDat != New.KolcsonKiadDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kolcsonhatarido' as ColumnName,               cast(Old.Kolcsonhatarido as nvarchar(99)) as OldValue,
               cast(New.Kolcsonhatarido as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Kolcsonhatarido != New.Kolcsonhatarido 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BARCODE' as ColumnName,               cast(Old.BARCODE as nvarchar(99)) as OldValue,
               cast(New.BARCODE as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BARCODE != New.BARCODE 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IratMetadefinicio_Id' as ColumnName,               cast(Old.IratMetadefinicio_Id as nvarchar(99)) as OldValue,
               cast(New.IratMetadefinicio_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IratMetadefinicio_Id != New.IratMetadefinicio_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Allapot != New.Allapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYIRAT_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Allapot and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Allapot and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TovabbitasAlattAllapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.TovabbitasAlattAllapot != New.TovabbitasAlattAllapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYIRAT_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.TovabbitasAlattAllapot and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.TovabbitasAlattAllapot and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Megjegyzes' as ColumnName,               cast(Old.Megjegyzes as nvarchar(99)) as OldValue,
               cast(New.Megjegyzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Megjegyzes != New.Megjegyzes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Azonosito' as ColumnName,               cast(Old.Azonosito as nvarchar(99)) as OldValue,
               cast(New.Azonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Azonosito != New.Azonosito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Fizikai_Kezbesitesi_Allapot' as ColumnName,               cast(Old.Fizikai_Kezbesitesi_Allapot as nvarchar(99)) as OldValue,
               cast(New.Fizikai_Kezbesitesi_Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Fizikai_Kezbesitesi_Allapot != New.Fizikai_Kezbesitesi_Allapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kovetkezo_Orzo_Id' as ColumnName,               cast(Old.Kovetkezo_Orzo_Id as nvarchar(99)) as OldValue,
               cast(New.Kovetkezo_Orzo_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Kovetkezo_Orzo_Id != New.Kovetkezo_Orzo_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Ugyfelelos' as ColumnName,               cast(Old.Csoport_Id_Ugyfelelos as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Ugyfelelos as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Ugyfelelos != New.Csoport_Id_Ugyfelelos 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Elektronikus_Kezbesitesi_Allap' as ColumnName,               cast(Old.Elektronikus_Kezbesitesi_Allap as nvarchar(99)) as OldValue,
               cast(New.Elektronikus_Kezbesitesi_Allap as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Elektronikus_Kezbesitesi_Allap != New.Elektronikus_Kezbesitesi_Allap 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kovetkezo_Felelos_Id' as ColumnName,               cast(Old.Kovetkezo_Felelos_Id as nvarchar(99)) as OldValue,
               cast(New.Kovetkezo_Felelos_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Kovetkezo_Felelos_Id != New.Kovetkezo_Felelos_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UtolsoAlszam' as ColumnName,               cast(Old.UtolsoAlszam as nvarchar(99)) as OldValue,
               cast(New.UtolsoAlszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UtolsoAlszam != New.UtolsoAlszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UtolsoSorszam' as ColumnName,               cast(Old.UtolsoSorszam as nvarchar(99)) as OldValue,
               cast(New.UtolsoSorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UtolsoSorszam != New.UtolsoSorszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IratSzam' as ColumnName,               cast(Old.IratSzam as nvarchar(99)) as OldValue,
               cast(New.IratSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IratSzam != New.IratSzam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ElintezesMod' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ElintezesMod != New.ElintezesMod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'ELINTEZES_MOD'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.ElintezesMod and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.ElintezesMod and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'RegirendszerIktatoszam' as ColumnName,               cast(Old.RegirendszerIktatoszam as nvarchar(99)) as OldValue,
               cast(New.RegirendszerIktatoszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.RegirendszerIktatoszam != New.RegirendszerIktatoszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'GeneraltTargy' as ColumnName,               cast(Old.GeneraltTargy as nvarchar(99)) as OldValue,
               cast(New.GeneraltTargy as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.GeneraltTargy != New.GeneraltTargy 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Cim_Id_Ugyindito ' as ColumnName,               cast(Old.Cim_Id_Ugyindito  as nvarchar(99)) as OldValue,
               cast(New.Cim_Id_Ugyindito  as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Cim_Id_Ugyindito  != New.Cim_Id_Ugyindito  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CimSTR_Ugyindito ' as ColumnName,               cast(Old.CimSTR_Ugyindito  as nvarchar(99)) as OldValue,
               cast(New.CimSTR_Ugyindito  as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.CimSTR_Ugyindito  != New.CimSTR_Ugyindito  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'LezarasOka' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.LezarasOka != New.LezarasOka 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'LEZARAS_OKA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.LezarasOka and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.LezarasOka and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelfuggesztesOka' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelfuggesztesOka != New.FelfuggesztesOka 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYIRAT_FELFUGGESZTES_OKA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.FelfuggesztesOka and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.FelfuggesztesOka and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarId' as ColumnName,               cast(Old.IrattarId as nvarchar(99)) as OldValue,
               cast(New.IrattarId as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IrattarId != New.IrattarId 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SkontroOka_Kod' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SkontroOka_Kod != New.SkontroOka_Kod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = ''
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.SkontroOka_Kod and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.SkontroOka_Kod and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UjOrzesiIdo' as ColumnName,               cast(Old.UjOrzesiIdo as nvarchar(99)) as OldValue,
               cast(New.UjOrzesiIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UjOrzesiIdo != New.UjOrzesiIdo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesKezdete' as ColumnName,               cast(Old.UgyintezesKezdete as nvarchar(99)) as OldValue,
               cast(New.UgyintezesKezdete as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyintezesKezdete != New.UgyintezesKezdete 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelfuggesztettNapokSzama' as ColumnName,               cast(Old.FelfuggesztettNapokSzama as nvarchar(99)) as OldValue,
               cast(New.FelfuggesztettNapokSzama as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelfuggesztettNapokSzama != New.FelfuggesztettNapokSzama 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IntezesiIdo' as ColumnName,               cast(Old.IntezesiIdo as nvarchar(99)) as OldValue,
               cast(New.IntezesiIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IntezesiIdo != New.IntezesiIdo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IntezesiIdoegyseg' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IntezesiIdoegyseg != New.IntezesiIdoegyseg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IDOEGYSEG'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.IntezesiIdoegyseg and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.IntezesiIdoegyseg and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ugy_Fajtaja' as ColumnName,               cast(Old.Ugy_Fajtaja as nvarchar(99)) as OldValue,
               cast(New.Ugy_Fajtaja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Ugy_Fajtaja != New.Ugy_Fajtaja 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go