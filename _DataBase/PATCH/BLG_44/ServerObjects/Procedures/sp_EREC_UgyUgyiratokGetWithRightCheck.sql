
/****** Object:  StoredProcedure [dbo].[sp_EREC_UgyUgyiratokGetWithRightCheck]    Script Date: 2017.07.27. 11:11:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_UgyUgyiratokGetWithRightCheck')
            and   type = 'P')
   drop procedure sp_EREC_UgyUgyiratokGetWithRightCheck
go

CREATE procedure [dbo].[sp_EREC_UgyUgyiratokGetWithRightCheck]
         @Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FelhasznaloSzervezet_Id   uniqueidentifier,
      @Jogszint   char(1),
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

   set nocount on

   DECLARE @Org uniqueidentifier
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
   if (@Org is null)
   begin
      RAISERROR('[50202]',16,1)
   end

   declare @IraIktatokonyv_Id uniqueidentifier;
   select @IraIktatokonyv_Id = IraIktatokonyv_Id from erec_ugyugyiratok where Id = @Id;

   --if dbo.fn_IsAdmin(@ExecutorUserId) = 0 and not exists
   IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
   BEGIN
      IF not exists
      (
         SELECT 1 FROM erec_ugyugyiratok
            INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
            where erec_ugyugyiratok.Id = @Id
      ) AND NOT EXISTS
      (
         SELECT 1 FROM erec_ugyugyiratok
            INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
            where erec_ugyugyiratok.Id = @Id --AND @Jogszint != 'I'
      ) AND NOT EXISTS
      (
         SELECT 1 FROM erec_ugyugyiratok
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
               ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
            where erec_ugyugyiratok.Id = @Id
      ) AND NOT EXISTS
      (
         SELECT 1 FROM erec_ugyugyiratok
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
               ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
            where erec_ugyugyiratok.Id = @Id
      ) AND NOT EXISTS
      (
         SELECT 1 FROM erec_ugyugyiratok
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
               ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
            where erec_ugyugyiratok.Id = @Id
      ) AND NOT EXISTS
      (
         SELECT 1 FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_UgyUgyiratok') as ids
            WHERE ids.Id = @Id
      ) AND NOT EXISTS
      (
         SELECT 1 FROM erec_ugyugyiratok
            INNER JOIN KRT_MappaTartalmak on KRT_MappaTartalmak.Obj_Id = erec_ugyugyiratok.Id
            INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
            INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
            where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
            and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
            and erec_ugyugyiratok.Id = @Id
      )
      begin
         RAISERROR('[50102]',16,1)
      end

      -- Ha (átmeneti v. központi) irattárban van az ügyirat és a felhasználó nem vezető és nem irattáros, valamint a
      -- szervezet dolgozói egymás tételeit nem láthatják, és feladata sincs,
      -- akkor csak a saját ügyintézésű ügyiratokat láthatja
      -- Irattározott állapotok: Irattárba küldött, Irattárban őrzött, Irattárból elkért, Átmeneti irattárból kikölcsönzött, Engedélyezett kikérőn lévő
      if dbo.fn_IsLeaderInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
      begin
         if dbo.fn_HasFunctionRight(@ExecutorUserId,@FelhasznaloSzervezet_Id, 'IrattarKolcsonzesKiadasa') = 0
            and dbo.fn_HasFunctionRight(@ExecutorUserId,@FelhasznaloSzervezet_Id, 'AtmenetiIrattarAtadasKozpontiIrattarba') = 0
         begin
         -- Irattár szűrés, EXISTS és nem NOT EXISTS!
            IF EXISTS(select 1 from EREC_UgyUgyiratok
               where Id=@Id
               and (Allapot in ('11', '10', '55', '54', '56') or TovabbitasAlattAllapot in ('11', '10', '55', '54', '56'))
               and Csoport_Id_Felelos in (select Id from KRT_Csoportok where IsNull(JogosultsagOroklesMod, '0') = '0')
               and EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez != @ExecutorUserId
               and EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId
               and not exists
               (
                  SELECT 1 FROM erec_ugyugyiratok
                     INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
                     where erec_ugyugyiratok.Id = @Id
               ) AND NOT EXISTS
               (
                  SELECT 1 FROM erec_ugyugyiratok
                     INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
                     where erec_ugyugyiratok.Id = @Id --AND @Jogszint != 'I'
               ) AND NOT EXISTS
               (
                  SELECT 1 FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_UgyUgyiratok') as ids
                     WHERE ids.Id = @Id
               )
            )
            RAISERROR('[50102]',16,1)
         end
      end

   END



   -- Org szűrés
   IF not exists(select 1 from EREC_IraIktatoKonyvek
      where EREC_IraIktatoKonyvek.Org=@Org
      and EREC_IraIktatoKonyvek.Id=
      (select EREC_UgyUgyiratok.IraIktatoKonyv_Id from EREC_UgyUgyiratok
      where EREC_UgyUgyiratok.Id=@Id
      ))
   BEGIN
      RAISERROR('[50101]',16,1)
   END

   DECLARE @sqlcmd nvarchar(4000)

/*************************Begin simple get**************************/  
  
   SET @sqlcmd = 
     'select
       EREC_UgyUgyiratok.Id,
	   EREC_UgyUgyiratok.Foszam,
	   EREC_UgyUgyiratok.Sorszam,
	   EREC_UgyUgyiratok.Ugyazonosito ,
	   EREC_UgyUgyiratok.Alkalmazas_Id,
	   EREC_UgyUgyiratok.UgyintezesModja,
	   EREC_UgyUgyiratok.Hatarido,
	   EREC_UgyUgyiratok.SkontrobaDat,
	   EREC_UgyUgyiratok.LezarasDat,
	   EREC_UgyUgyiratok.IrattarbaKuldDatuma ,
	   EREC_UgyUgyiratok.IrattarbaVetelDat,
	   EREC_UgyUgyiratok.FelhCsoport_Id_IrattariAtvevo,
	   EREC_UgyUgyiratok.SelejtezesDat,
	   EREC_UgyUgyiratok.FelhCsoport_Id_Selejtezo,
	   EREC_UgyUgyiratok.LeveltariAtvevoNeve,
	   EREC_UgyUgyiratok.FelhCsoport_Id_Felulvizsgalo,
	   EREC_UgyUgyiratok.FelulvizsgalatDat,
	   EREC_UgyUgyiratok.IktatoszamKieg,
	   EREC_UgyUgyiratok.Targy,
	   EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo,
	   EREC_UgyUgyiratok.UgyUgyirat_Id_Kulso,
	   EREC_UgyUgyiratok.UgyTipus,
	   EREC_UgyUgyiratok.IrattariHely,
	   EREC_UgyUgyiratok.SztornirozasDat,
	   EREC_UgyUgyiratok.Csoport_Id_Felelos,
	   EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo,
	   EREC_UgyUgyiratok.Csoport_Id_Cimzett,
	   EREC_UgyUgyiratok.Jelleg,
	   EREC_UgyUgyiratok.IraIrattariTetel_Id,
	   EREC_UgyUgyiratok.IraIktatokonyv_Id,
	   EREC_UgyUgyiratok.SkontroOka,
	   EREC_UgyUgyiratok.SkontroVege,
	   EREC_UgyUgyiratok.Surgosseg,
	   EREC_UgyUgyiratok.SkontrobanOsszesen,
	   EREC_UgyUgyiratok.MegorzesiIdoVege,
	   EREC_UgyUgyiratok.Partner_Id_Ugyindito,
	   EREC_UgyUgyiratok.NevSTR_Ugyindito,
	   EREC_UgyUgyiratok.ElintezesDat,
	   EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez,
	   EREC_UgyUgyiratok.Csoport_Id_Felelos_Elozo,
	   EREC_UgyUgyiratok.KolcsonKikerDat,
	   EREC_UgyUgyiratok.KolcsonKiadDat,
	   EREC_UgyUgyiratok.Kolcsonhatarido,
	   EREC_UgyUgyiratok.BARCODE,
	   EREC_UgyUgyiratok.IratMetadefinicio_Id,
	   EREC_UgyUgyiratok.Allapot,
	   EREC_UgyUgyiratok.TovabbitasAlattAllapot,
	   EREC_UgyUgyiratok.Megjegyzes,
	   EREC_UgyUgyiratok.Azonosito,
	   EREC_UgyUgyiratok.Fizikai_Kezbesitesi_Allapot,
	   EREC_UgyUgyiratok.Kovetkezo_Orzo_Id,
	   EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos,
	   EREC_UgyUgyiratok.Elektronikus_Kezbesitesi_Allap,
	   EREC_UgyUgyiratok.Kovetkezo_Felelos_Id,
	   EREC_UgyUgyiratok.UtolsoAlszam,
	   EREC_UgyUgyiratok.UtolsoSorszam,
	   EREC_UgyUgyiratok.IratSzam,
	   EREC_UgyUgyiratok.ElintezesMod,
	   EREC_UgyUgyiratok.RegirendszerIktatoszam,
	   EREC_UgyUgyiratok.GeneraltTargy,
	   EREC_UgyUgyiratok.Cim_Id_Ugyindito ,
	   EREC_UgyUgyiratok.CimSTR_Ugyindito ,
	   EREC_UgyUgyiratok.LezarasOka,
	   EREC_UgyUgyiratok.FelfuggesztesOka,
	   EREC_UgyUgyiratok.IrattarId,
	   EREC_UgyUgyiratok.SkontroOka_Kod,
	   EREC_UgyUgyiratok.UjOrzesiIdo,
	   EREC_UgyUgyiratok.UgyintezesKezdete,
	   EREC_UgyUgyiratok.FelfuggesztettNapokSzama,
	   EREC_UgyUgyiratok.IntezesiIdo,
	   EREC_UgyUgyiratok.IntezesiIdoegyseg,
	   EREC_UgyUgyiratok.Ugy_Fajtaja,
	   EREC_UgyUgyiratok.Ver,
	   EREC_UgyUgyiratok.Note,
	   EREC_UgyUgyiratok.Stat_id,
	   EREC_UgyUgyiratok.ErvKezd,
	   EREC_UgyUgyiratok.ErvVege,
	   EREC_UgyUgyiratok.Letrehozo_id,
	   EREC_UgyUgyiratok.LetrehozasIdo,
	   EREC_UgyUgyiratok.Modosito_id,
	   EREC_UgyUgyiratok.ModositasIdo,
	   EREC_UgyUgyiratok.Zarolo_id,
	   EREC_UgyUgyiratok.ZarolasIdo,
	   EREC_UgyUgyiratok.Tranz_id,
	   EREC_UgyUgyiratok.UIAccessLog_id
      from 
       EREC_UgyUgyiratok as EREC_UgyUgyiratok 
      where
       EREC_UgyUgyiratok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

/*************************End simple get**************************/  

   exec sp_executesql @sqlcmd;
    
   if @@rowcount = 0
     begin
      RAISERROR('[50101]',16,1)
     end

END TRY
BEGIN CATCH
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
