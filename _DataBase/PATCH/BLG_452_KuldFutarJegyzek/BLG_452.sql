if  not exists(select 1 
			   from sys.all_columns 
			   where object_id = OBJECT_ID('EREC_KuldKuldemenyek') 
			   and name = 'FutarJegyzekListaSzama')
 begin
ALTER TABLE EREC_KuldKuldemenyek ADD FutarJegyzekListaSzama Nvarchar(100)
end;
go
------------------
if  not exists(select 1 
			   from sys.all_columns 
			   where object_id = OBJECT_ID('EREC_KuldKuldemenyek') 
			   and name = 'Minosito')
 begin
ALTER TABLE EREC_KuldKuldemenyek ADD Minosito uniqueidentifier
end;
go
---------------------
if  not exists(select 1 
			   from sys.all_columns 
			   where object_id = OBJECT_ID('EREC_KuldKuldemenyek') 
			   and name = 'MinositesErvenyessegiIdeje')
 begin
ALTER TABLE EREC_KuldKuldemenyek ADD MinositesErvenyessegiIdeje datetime
end;
go
---------------------
if  not exists(select 1 
			   from sys.all_columns 
			   where object_id = OBJECT_ID('EREC_KuldKuldemenyek') 
			   and name = 'TerjedelemMennyiseg')
 begin
ALTER TABLE EREC_KuldKuldemenyek ADD TerjedelemMennyiseg int
end;
go
---------------------
 if  not exists(select 1 
			   from sys.all_columns 
			   where object_id = OBJECT_ID('EREC_KuldKuldemenyek') 
			   and name = 'TerjedelemMennyisegiEgyseg')
 begin
ALTER TABLE EREC_KuldKuldemenyek ADD TerjedelemMennyisegiEgyseg nvarchar(64)
end;
go
---------------------
 if  not exists(select 1 
			   from sys.all_columns 
			   where object_id = OBJECT_ID('EREC_KuldKuldemenyek') 
			   and name = 'TerjedelemMegjegyzes')
 begin
ALTER TABLE EREC_KuldKuldemenyek ADD TerjedelemMegjegyzes Nvarchar(400)
end;
go
---------------------

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_KuldKuldemenyekDelete')
            and   type = 'P')
   drop procedure sp_EREC_KuldKuldemenyekDelete
go

create procedure sp_EREC_KuldKuldemenyekDelete 
  @Id uniqueidentifier,
  @ExecutorUserId uniqueidentifier,
  @ExecutionTime datetime

as
begin

BEGIN TRY
--BEGIN TRANSACTION DeleteTransaction
  
	set nocount on

-- Zarolas ellenorzese:
	DECLARE @IsLockedByOtherUser BIT
	exec sp_GetLockingInfo 'EREC_KuldKuldemenyek',@Id,@ExecutorUserId,@IsLockedByOtherUser OUTPUT,null

	if (@IsLockedByOtherUser = 0)
	BEGIN	
         /* History Log */
      exec sp_LogDeleteToHistory 'EREC_KuldKuldemenyek',@Id
                 ,'EREC_KuldKuldemenyekHistory',@ExecutorUserId,@ExecutionTime   
		delete from EREC_KuldKuldemenyek 
		where Id = @Id

		if @@rowcount != 1
		begin
			RAISERROR('[50501]',16,1)
			return @@error
		end
	END
	ELSE BEGIN
		RAISERROR('[50599]',16,1)			
	END

--COMMIT TRANSACTION DeleteTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION DeleteTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()	
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go
---------------------


set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_KuldKuldemenyekGet')
            and   type = 'P')
   drop procedure sp_EREC_KuldKuldemenyekGet
go

create procedure sp_EREC_KuldKuldemenyekGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_KuldKuldemenyek.Id,
	   EREC_KuldKuldemenyek.Allapot,
	   EREC_KuldKuldemenyek.BeerkezesIdeje,
	   EREC_KuldKuldemenyek.FelbontasDatuma,
	   EREC_KuldKuldemenyek.IraIktatokonyv_Id,
	   EREC_KuldKuldemenyek.KuldesMod,
	   EREC_KuldKuldemenyek.Erkezteto_Szam,
	   EREC_KuldKuldemenyek.HivatkozasiSzam,
	   EREC_KuldKuldemenyek.Targy,
	   EREC_KuldKuldemenyek.Tartalom,
	   EREC_KuldKuldemenyek.RagSzam,
	   EREC_KuldKuldemenyek.Surgosseg,
	   EREC_KuldKuldemenyek.BelyegzoDatuma,
	   EREC_KuldKuldemenyek.UgyintezesModja,
	   EREC_KuldKuldemenyek.PostazasIranya,
	   EREC_KuldKuldemenyek.Tovabbito,
	   EREC_KuldKuldemenyek.PeldanySzam,
	   EREC_KuldKuldemenyek.IktatniKell,
	   EREC_KuldKuldemenyek.Iktathato,
	   EREC_KuldKuldemenyek.SztornirozasDat,
	   EREC_KuldKuldemenyek.KuldKuldemeny_Id_Szulo,
	   EREC_KuldKuldemenyek.Erkeztetes_Ev,
	   EREC_KuldKuldemenyek.Csoport_Id_Cimzett,
	   EREC_KuldKuldemenyek.Csoport_Id_Felelos,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Expedial,
	   EREC_KuldKuldemenyek.ExpedialasIdeje,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Atvevo,
	   EREC_KuldKuldemenyek.Partner_Id_Bekuldo,
	   EREC_KuldKuldemenyek.Cim_Id,
	   EREC_KuldKuldemenyek.CimSTR_Bekuldo,
	   EREC_KuldKuldemenyek.NevSTR_Bekuldo,
	   EREC_KuldKuldemenyek.AdathordozoTipusa,
	   EREC_KuldKuldemenyek.ElsodlegesAdathordozoTipusa,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Alairo,
	   EREC_KuldKuldemenyek.BarCode,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto,
	   EREC_KuldKuldemenyek.CsoportFelelosEloszto_Id,
	   EREC_KuldKuldemenyek.Csoport_Id_Felelos_Elozo,
	   EREC_KuldKuldemenyek.Kovetkezo_Felelos_Id,
	   EREC_KuldKuldemenyek.Elektronikus_Kezbesitesi_Allap,
	   EREC_KuldKuldemenyek.Kovetkezo_Orzo_Id,
	   EREC_KuldKuldemenyek.Fizikai_Kezbesitesi_Allapot,
	   EREC_KuldKuldemenyek.IraIratok_Id,
	   EREC_KuldKuldemenyek.BontasiMegjegyzes,
	   EREC_KuldKuldemenyek.Tipus,
	   EREC_KuldKuldemenyek.Minosites,
	   EREC_KuldKuldemenyek.MegtagadasIndoka,
	   EREC_KuldKuldemenyek.Megtagado_Id,
	   EREC_KuldKuldemenyek.MegtagadasDat,
	   EREC_KuldKuldemenyek.KimenoKuldemenyFajta,
	   EREC_KuldKuldemenyek.Elsobbsegi,
	   EREC_KuldKuldemenyek.Ajanlott,
	   EREC_KuldKuldemenyek.Tertiveveny,
	   EREC_KuldKuldemenyek.SajatKezbe,
	   EREC_KuldKuldemenyek.E_ertesites,
	   EREC_KuldKuldemenyek.E_elorejelzes,
	   EREC_KuldKuldemenyek.PostaiLezaroSzolgalat,
	   EREC_KuldKuldemenyek.Ar,
	   EREC_KuldKuldemenyek.KimenoKuld_Sorszam ,
	   EREC_KuldKuldemenyek.Ver,
	   EREC_KuldKuldemenyek.TovabbitasAlattAllapot,
	   EREC_KuldKuldemenyek.Azonosito,
	   EREC_KuldKuldemenyek.BoritoTipus,
	   EREC_KuldKuldemenyek.MegorzesJelzo,
	   EREC_KuldKuldemenyek.Note,
	   EREC_KuldKuldemenyek.Stat_id,
	   EREC_KuldKuldemenyek.ErvKezd,
	   EREC_KuldKuldemenyek.ErvVege,
	   EREC_KuldKuldemenyek.Letrehozo_id,
	   EREC_KuldKuldemenyek.LetrehozasIdo,
	   EREC_KuldKuldemenyek.Modosito_id,
	   EREC_KuldKuldemenyek.ModositasIdo,
	   EREC_KuldKuldemenyek.Zarolo_id,
	   EREC_KuldKuldemenyek.ZarolasIdo,
	   EREC_KuldKuldemenyek.Tranz_id,
	   EREC_KuldKuldemenyek.UIAccessLog_id,
	   EREC_KuldKuldemenyek.IktatastNemIgenyel,
	   EREC_KuldKuldemenyek.KezbesitesModja,
	   EREC_KuldKuldemenyek.Munkaallomas,
	   EREC_KuldKuldemenyek.SerultKuldemeny,
	   EREC_KuldKuldemenyek.TevesCimzes,
	   EREC_KuldKuldemenyek.TevesErkeztetes,
	   EREC_KuldKuldemenyek.CimzesTipusa,
	   EREC_KuldKuldemenyek.FutarJegyzekListaSzama,
	   EREC_KuldKuldemenyek.Minosito,
	   EREC_KuldKuldemenyek.MinositesErvenyessegiIdeje,
	   EREC_KuldKuldemenyek.TerjedelemMennyiseg,
	   EREC_KuldKuldemenyek.TerjedelemMennyisegiEgyseg,
	   EREC_KuldKuldemenyek.TerjedelemMegjegyzes
	   from 
		 EREC_KuldKuldemenyek as EREC_KuldKuldemenyek 
	   where
		 EREC_KuldKuldemenyek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
---------------------

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_KuldKuldemenyekGetAll')
            and   type = 'P')
   drop procedure sp_EREC_KuldKuldemenyekGetAll
go

create procedure sp_EREC_KuldKuldemenyekGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_KuldKuldemenyek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_KuldKuldemenyek.Id,
	   EREC_KuldKuldemenyek.Allapot,
	   EREC_KuldKuldemenyek.BeerkezesIdeje,
	   EREC_KuldKuldemenyek.FelbontasDatuma,
	   EREC_KuldKuldemenyek.IraIktatokonyv_Id,
	   EREC_KuldKuldemenyek.KuldesMod,
	   EREC_KuldKuldemenyek.Erkezteto_Szam,
	   EREC_KuldKuldemenyek.HivatkozasiSzam,
	   EREC_KuldKuldemenyek.Targy,
	   EREC_KuldKuldemenyek.Tartalom,
	   EREC_KuldKuldemenyek.RagSzam,
	   EREC_KuldKuldemenyek.Surgosseg,
	   EREC_KuldKuldemenyek.BelyegzoDatuma,
	   EREC_KuldKuldemenyek.UgyintezesModja,
	   EREC_KuldKuldemenyek.PostazasIranya,
	   EREC_KuldKuldemenyek.Tovabbito,
	   EREC_KuldKuldemenyek.PeldanySzam,
	   EREC_KuldKuldemenyek.IktatniKell,
	   EREC_KuldKuldemenyek.Iktathato,
	   EREC_KuldKuldemenyek.SztornirozasDat,
	   EREC_KuldKuldemenyek.KuldKuldemeny_Id_Szulo,
	   EREC_KuldKuldemenyek.Erkeztetes_Ev,
	   EREC_KuldKuldemenyek.Csoport_Id_Cimzett,
	   EREC_KuldKuldemenyek.Csoport_Id_Felelos,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Expedial,
	   EREC_KuldKuldemenyek.ExpedialasIdeje,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Atvevo,
	   EREC_KuldKuldemenyek.Partner_Id_Bekuldo,
	   EREC_KuldKuldemenyek.Cim_Id,
	   EREC_KuldKuldemenyek.CimSTR_Bekuldo,
	   EREC_KuldKuldemenyek.NevSTR_Bekuldo,
	   EREC_KuldKuldemenyek.AdathordozoTipusa,
	   EREC_KuldKuldemenyek.ElsodlegesAdathordozoTipusa,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Alairo,
	   EREC_KuldKuldemenyek.BarCode,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto,
	   EREC_KuldKuldemenyek.CsoportFelelosEloszto_Id,
	   EREC_KuldKuldemenyek.Csoport_Id_Felelos_Elozo,
	   EREC_KuldKuldemenyek.Kovetkezo_Felelos_Id,
	   EREC_KuldKuldemenyek.Elektronikus_Kezbesitesi_Allap,
	   EREC_KuldKuldemenyek.Kovetkezo_Orzo_Id,
	   EREC_KuldKuldemenyek.Fizikai_Kezbesitesi_Allapot,
	   EREC_KuldKuldemenyek.IraIratok_Id,
	   EREC_KuldKuldemenyek.BontasiMegjegyzes,
	   EREC_KuldKuldemenyek.Tipus,
	   EREC_KuldKuldemenyek.Minosites,
	   EREC_KuldKuldemenyek.MegtagadasIndoka,
	   EREC_KuldKuldemenyek.Megtagado_Id,
	   EREC_KuldKuldemenyek.MegtagadasDat,
	   EREC_KuldKuldemenyek.KimenoKuldemenyFajta,
	   EREC_KuldKuldemenyek.Elsobbsegi,
	   EREC_KuldKuldemenyek.Ajanlott,
	   EREC_KuldKuldemenyek.Tertiveveny,
	   EREC_KuldKuldemenyek.SajatKezbe,
	   EREC_KuldKuldemenyek.E_ertesites,
	   EREC_KuldKuldemenyek.E_elorejelzes,
	   EREC_KuldKuldemenyek.PostaiLezaroSzolgalat,
	   EREC_KuldKuldemenyek.Ar,
	   EREC_KuldKuldemenyek.KimenoKuld_Sorszam ,
	   EREC_KuldKuldemenyek.Ver,
	   EREC_KuldKuldemenyek.TovabbitasAlattAllapot,
	   EREC_KuldKuldemenyek.Azonosito,
	   EREC_KuldKuldemenyek.BoritoTipus,
	   EREC_KuldKuldemenyek.MegorzesJelzo,
	   EREC_KuldKuldemenyek.Note,
	   EREC_KuldKuldemenyek.Stat_id,
	   EREC_KuldKuldemenyek.ErvKezd,
	   EREC_KuldKuldemenyek.ErvVege,
	   EREC_KuldKuldemenyek.Letrehozo_id,
	   EREC_KuldKuldemenyek.LetrehozasIdo,
	   EREC_KuldKuldemenyek.Modosito_id,
	   EREC_KuldKuldemenyek.ModositasIdo,
	   EREC_KuldKuldemenyek.Zarolo_id,
	   EREC_KuldKuldemenyek.ZarolasIdo,
	   EREC_KuldKuldemenyek.Tranz_id,
	   EREC_KuldKuldemenyek.UIAccessLog_id,
	   EREC_KuldKuldemenyek.IktatastNemIgenyel,
	   EREC_KuldKuldemenyek.KezbesitesModja,
	   EREC_KuldKuldemenyek.Munkaallomas,
	   EREC_KuldKuldemenyek.SerultKuldemeny,
	   EREC_KuldKuldemenyek.TevesCimzes,
	   EREC_KuldKuldemenyek.TevesErkeztetes,
	   EREC_KuldKuldemenyek.CimzesTipusa,
	   EREC_KuldKuldemenyek.FutarJegyzekListaSzama,
	   EREC_KuldKuldemenyek.Minosito,
	   EREC_KuldKuldemenyek.MinositesErvenyessegiIdeje,
	   EREC_KuldKuldemenyek.TerjedelemMennyiseg,
	   EREC_KuldKuldemenyek.TerjedelemMennyisegiEgyseg,
	   EREC_KuldKuldemenyek.TerjedelemMegjegyzes  
   from 
     EREC_KuldKuldemenyek as EREC_KuldKuldemenyek      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go
---------------------
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_KuldKuldemenyekHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_EREC_KuldKuldemenyekHistoryGetAllRecord
go

create procedure sp_EREC_KuldKuldemenyekHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'L�trehoz�s' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_KuldKuldemenyekHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Allapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Allapot != New.Allapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'KULDEMENY_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Allapot and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Allapot and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'BeerkezesIdeje' as ColumnName,               cast(Old.BeerkezesIdeje as nvarchar(99)) as OldValue,
               cast(New.BeerkezesIdeje as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BeerkezesIdeje != New.BeerkezesIdeje 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'FelbontasDatuma' as ColumnName,               cast(Old.FelbontasDatuma as nvarchar(99)) as OldValue,
               cast(New.FelbontasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelbontasDatuma != New.FelbontasDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'IraIktatokonyv_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_IraIktatoKonyvekAzonosito(Old.IraIktatokonyv_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_IraIktatoKonyvekAzonosito(New.IraIktatokonyv_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IraIktatokonyv_Id != New.IraIktatokonyv_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join EREC_IraIktatoKonyvek FTOld on FTOld.Id = Old.IraIktatokonyv_Id --and FTOld.Ver = Old.Ver
         left join EREC_IraIktatoKonyvek FTNew on FTNew.Id = New.IraIktatokonyv_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'KuldesMod' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KuldesMod != New.KuldesMod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'KULDEMENY_KULDES_MODJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.KuldesMod and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.KuldesMod and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Erkezteto_Szam' as ColumnName,               cast(Old.Erkezteto_Szam as nvarchar(99)) as OldValue,
               cast(New.Erkezteto_Szam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Erkezteto_Szam != New.Erkezteto_Szam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'HivatkozasiSzam' as ColumnName,               cast(Old.HivatkozasiSzam as nvarchar(99)) as OldValue,
               cast(New.HivatkozasiSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HivatkozasiSzam != New.HivatkozasiSzam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Targy' as ColumnName,               cast(Old.Targy as nvarchar(99)) as OldValue,
               cast(New.Targy as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Targy != New.Targy 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Tartalom' as ColumnName,               cast(Old.Tartalom as nvarchar(99)) as OldValue,
               cast(New.Tartalom as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tartalom != New.Tartalom 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'RagSzam' as ColumnName,               cast(Old.RagSzam as nvarchar(99)) as OldValue,
               cast(New.RagSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.RagSzam != New.RagSzam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Surgosseg' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Surgosseg != New.Surgosseg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'SURGOSSEG'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Surgosseg and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Surgosseg and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'BelyegzoDatuma' as ColumnName,               cast(Old.BelyegzoDatuma as nvarchar(99)) as OldValue,
               cast(New.BelyegzoDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BelyegzoDatuma != New.BelyegzoDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'UgyintezesModja' as ColumnName,               cast(Old.UgyintezesModja as nvarchar(99)) as OldValue,
               cast(New.UgyintezesModja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyintezesModja != New.UgyintezesModja 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'PostazasIranya' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PostazasIranya != New.PostazasIranya 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'POSTAZAS_IRANYA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.PostazasIranya and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.PostazasIranya and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Tovabbito' as ColumnName,               cast(Old.Tovabbito as nvarchar(99)) as OldValue,
               cast(New.Tovabbito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tovabbito != New.Tovabbito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'PeldanySzam' as ColumnName,               cast(Old.PeldanySzam as nvarchar(99)) as OldValue,
               cast(New.PeldanySzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PeldanySzam != New.PeldanySzam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'IktatniKell' as ColumnName,               cast(Old.IktatniKell as nvarchar(99)) as OldValue,
               cast(New.IktatniKell as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IktatniKell != New.IktatniKell 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Iktathato' as ColumnName,               cast(Old.Iktathato as nvarchar(99)) as OldValue,
               cast(New.Iktathato as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Iktathato != New.Iktathato 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'SztornirozasDat' as ColumnName,               cast(Old.SztornirozasDat as nvarchar(99)) as OldValue,
               cast(New.SztornirozasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SztornirozasDat != New.SztornirozasDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'KuldKuldemeny_Id_Szulo' as ColumnName,
/*FK*/           dbo.fn_GetEREC_KuldKuldemenyekAzonosito(Old.KuldKuldemeny_Id_Szulo) as OldValue,
/*FK*/           dbo.fn_GetEREC_KuldKuldemenyekAzonosito(New.KuldKuldemeny_Id_Szulo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KuldKuldemeny_Id_Szulo != New.KuldKuldemeny_Id_Szulo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join EREC_KuldKuldemenyek FTOld on FTOld.Id = Old.KuldKuldemeny_Id_Szulo --and FTOld.Ver = Old.Ver
         left join EREC_KuldKuldemenyek FTNew on FTNew.Id = New.KuldKuldemeny_Id_Szulo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Erkeztetes_Ev' as ColumnName,               cast(Old.Erkeztetes_Ev as nvarchar(99)) as OldValue,
               cast(New.Erkeztetes_Ev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Erkeztetes_Ev != New.Erkeztetes_Ev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Csoport_Id_Cimzett' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Cimzett) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Cimzett) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Cimzett != New.Csoport_Id_Cimzett 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.Csoport_Id_Cimzett --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Csoport_Id_Cimzett --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Csoport_Id_Felelos' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Felelos) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Felelos) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Felelos != New.Csoport_Id_Felelos 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.Csoport_Id_Felelos --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Csoport_Id_Felelos --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'FelhasznaloCsoport_Id_Expedial' as ColumnName,               cast(Old.FelhasznaloCsoport_Id_Expedial as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Expedial as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Expedial != New.FelhasznaloCsoport_Id_Expedial 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'ExpedialasIdeje' as ColumnName,               cast(Old.ExpedialasIdeje as nvarchar(99)) as OldValue,
               cast(New.ExpedialasIdeje as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ExpedialasIdeje != New.ExpedialasIdeje 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'FelhasznaloCsoport_Id_Orzo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Orzo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Orzo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Orzo != New.FelhasznaloCsoport_Id_Orzo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Orzo --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Orzo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'FelhasznaloCsoport_Id_Atvevo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Atvevo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Atvevo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Atvevo != New.FelhasznaloCsoport_Id_Atvevo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Atvevo --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Atvevo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Partner_Id_Bekuldo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(Old.Partner_Id_Bekuldo) as OldValue,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(New.Partner_Id_Bekuldo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Partner_Id_Bekuldo != New.Partner_Id_Bekuldo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Partnerek FTOld on FTOld.Id = Old.Partner_Id_Bekuldo --and FTOld.Ver = Old.Ver
         left join KRT_Partnerek FTNew on FTNew.Id = New.Partner_Id_Bekuldo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Cim_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CimekAzonosito(Old.Cim_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_CimekAzonosito(New.Cim_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Cim_Id != New.Cim_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Cimek FTOld on FTOld.Id = Old.Cim_Id --and FTOld.Ver = Old.Ver
         left join KRT_Cimek FTNew on FTNew.Id = New.Cim_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'CimSTR_Bekuldo' as ColumnName,               cast(Old.CimSTR_Bekuldo as nvarchar(99)) as OldValue,
               cast(New.CimSTR_Bekuldo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.CimSTR_Bekuldo != New.CimSTR_Bekuldo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'NevSTR_Bekuldo' as ColumnName,               cast(Old.NevSTR_Bekuldo as nvarchar(99)) as OldValue,
               cast(New.NevSTR_Bekuldo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.NevSTR_Bekuldo != New.NevSTR_Bekuldo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'AdathordozoTipusa' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AdathordozoTipusa != New.AdathordozoTipusa 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'ADATHORDOZO_TIPUSA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.AdathordozoTipusa and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.AdathordozoTipusa and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'ElsodlegesAdathordozoTipusa' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ElsodlegesAdathordozoTipusa != New.ElsodlegesAdathordozoTipusa 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'ELSODLEGES_ADATHORDOZO'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.ElsodlegesAdathordozoTipusa and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.ElsodlegesAdathordozoTipusa and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'FelhasznaloCsoport_Id_Alairo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Alairo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Alairo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Alairo != New.FelhasznaloCsoport_Id_Alairo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Alairo --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Alairo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'BarCode' as ColumnName,               cast(Old.BarCode as nvarchar(99)) as OldValue,
               cast(New.BarCode as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BarCode != New.BarCode 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'FelhasznaloCsoport_Id_Bonto' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Bonto) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Bonto) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Bonto != New.FelhasznaloCsoport_Id_Bonto 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Bonto --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Bonto --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'CsoportFelelosEloszto_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.CsoportFelelosEloszto_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.CsoportFelelosEloszto_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.CsoportFelelosEloszto_Id != New.CsoportFelelosEloszto_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.CsoportFelelosEloszto_Id --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.CsoportFelelosEloszto_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Csoport_Id_Felelos_Elozo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Felelos_Elozo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Felelos_Elozo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Felelos_Elozo != New.Csoport_Id_Felelos_Elozo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.Csoport_Id_Felelos_Elozo --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Csoport_Id_Felelos_Elozo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Kovetkezo_Felelos_Id' as ColumnName,               cast(Old.Kovetkezo_Felelos_Id as nvarchar(99)) as OldValue,
               cast(New.Kovetkezo_Felelos_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Kovetkezo_Felelos_Id != New.Kovetkezo_Felelos_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Elektronikus_Kezbesitesi_Allap' as ColumnName,               cast(Old.Elektronikus_Kezbesitesi_Allap as nvarchar(99)) as OldValue,
               cast(New.Elektronikus_Kezbesitesi_Allap as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Elektronikus_Kezbesitesi_Allap != New.Elektronikus_Kezbesitesi_Allap 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Kovetkezo_Orzo_Id' as ColumnName,               cast(Old.Kovetkezo_Orzo_Id as nvarchar(99)) as OldValue,
               cast(New.Kovetkezo_Orzo_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Kovetkezo_Orzo_Id != New.Kovetkezo_Orzo_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Fizikai_Kezbesitesi_Allapot' as ColumnName,               cast(Old.Fizikai_Kezbesitesi_Allapot as nvarchar(99)) as OldValue,
               cast(New.Fizikai_Kezbesitesi_Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Fizikai_Kezbesitesi_Allapot != New.Fizikai_Kezbesitesi_Allapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'IraIratok_Id' as ColumnName,               cast(Old.IraIratok_Id as nvarchar(99)) as OldValue,
               cast(New.IraIratok_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IraIratok_Id != New.IraIratok_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'BontasiMegjegyzes' as ColumnName,               cast(Old.BontasiMegjegyzes as nvarchar(99)) as OldValue,
               cast(New.BontasiMegjegyzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BontasiMegjegyzes != New.BontasiMegjegyzes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Tipus' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tipus != New.Tipus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'KULDEMENY_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Tipus and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Tipus and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Minosites' as ColumnName,               cast(Old.Minosites as nvarchar(99)) as OldValue,
               cast(New.Minosites as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Minosites != New.Minosites 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'MegtagadasIndoka' as ColumnName,               cast(Old.MegtagadasIndoka as nvarchar(99)) as OldValue,
               cast(New.MegtagadasIndoka as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MegtagadasIndoka != New.MegtagadasIndoka 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Megtagado_Id' as ColumnName,               cast(Old.Megtagado_Id as nvarchar(99)) as OldValue,
               cast(New.Megtagado_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Megtagado_Id != New.Megtagado_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'MegtagadasDat' as ColumnName,               cast(Old.MegtagadasDat as nvarchar(99)) as OldValue,
               cast(New.MegtagadasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MegtagadasDat != New.MegtagadasDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'KimenoKuldemenyFajta' as ColumnName,               cast(Old.KimenoKuldemenyFajta as nvarchar(99)) as OldValue,
               cast(New.KimenoKuldemenyFajta as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KimenoKuldemenyFajta != New.KimenoKuldemenyFajta 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Elsobbsegi' as ColumnName,               cast(Old.Elsobbsegi as nvarchar(99)) as OldValue,
               cast(New.Elsobbsegi as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Elsobbsegi != New.Elsobbsegi 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Ajanlott' as ColumnName,               cast(Old.Ajanlott as nvarchar(99)) as OldValue,
               cast(New.Ajanlott as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Ajanlott != New.Ajanlott 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Tertiveveny' as ColumnName,               cast(Old.Tertiveveny as nvarchar(99)) as OldValue,
               cast(New.Tertiveveny as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tertiveveny != New.Tertiveveny 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'SajatKezbe' as ColumnName,               cast(Old.SajatKezbe as nvarchar(99)) as OldValue,
               cast(New.SajatKezbe as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SajatKezbe != New.SajatKezbe 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'E_ertesites' as ColumnName,               cast(Old.E_ertesites as nvarchar(99)) as OldValue,
               cast(New.E_ertesites as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.E_ertesites != New.E_ertesites 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'E_elorejelzes' as ColumnName,               cast(Old.E_elorejelzes as nvarchar(99)) as OldValue,
               cast(New.E_elorejelzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.E_elorejelzes != New.E_elorejelzes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'PostaiLezaroSzolgalat' as ColumnName,               cast(Old.PostaiLezaroSzolgalat as nvarchar(99)) as OldValue,
               cast(New.PostaiLezaroSzolgalat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PostaiLezaroSzolgalat != New.PostaiLezaroSzolgalat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Ar' as ColumnName,               cast(Old.Ar as nvarchar(99)) as OldValue,
               cast(New.Ar as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Ar != New.Ar 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'KimenoKuld_Sorszam ' as ColumnName,               cast(Old.KimenoKuld_Sorszam  as nvarchar(99)) as OldValue,
               cast(New.KimenoKuld_Sorszam  as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KimenoKuld_Sorszam  != New.KimenoKuld_Sorszam  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go
---------------------
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_KuldKuldemenyekHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_EREC_KuldKuldemenyekHistoryGetRecord
go

create procedure sp_EREC_KuldKuldemenyekHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_KuldKuldemenyekHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'L�trehoz�s' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_KuldKuldemenyekHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Allapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Allapot != New.Allapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'KULDEMENY_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Allapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Allapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'BeerkezesIdeje' as ColumnName,               
               cast(Old.BeerkezesIdeje as nvarchar(99)) as OldValue,
               cast(New.BeerkezesIdeje as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BeerkezesIdeje != New.BeerkezesIdeje 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'FelbontasDatuma' as ColumnName,               
               cast(Old.FelbontasDatuma as nvarchar(99)) as OldValue,
               cast(New.FelbontasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelbontasDatuma != New.FelbontasDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'IraIktatokonyv_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_IraIktatoKonyvekAzonosito(Old.IraIktatokonyv_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_IraIktatoKonyvekAzonosito(New.IraIktatokonyv_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IraIktatokonyv_Id != New.IraIktatokonyv_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_IraIktatoKonyvek FTOld on FTOld.Id = Old.IraIktatokonyv_Id and FTOld.Ver = Old.Ver
         left join EREC_IraIktatoKonyvek FTNew on FTNew.Id = New.IraIktatokonyv_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'KuldesMod' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KuldesMod != New.KuldesMod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'KULDEMENY_KULDES_MODJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.KuldesMod and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.KuldesMod and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Erkezteto_Szam' as ColumnName,               
               cast(Old.Erkezteto_Szam as nvarchar(99)) as OldValue,
               cast(New.Erkezteto_Szam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Erkezteto_Szam != New.Erkezteto_Szam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'HivatkozasiSzam' as ColumnName,               
               cast(Old.HivatkozasiSzam as nvarchar(99)) as OldValue,
               cast(New.HivatkozasiSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HivatkozasiSzam != New.HivatkozasiSzam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Targy' as ColumnName,               
               cast(Old.Targy as nvarchar(99)) as OldValue,
               cast(New.Targy as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Targy != New.Targy 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Tartalom' as ColumnName,               
               cast(Old.Tartalom as nvarchar(99)) as OldValue,
               cast(New.Tartalom as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tartalom != New.Tartalom 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'RagSzam' as ColumnName,               
               cast(Old.RagSzam as nvarchar(99)) as OldValue,
               cast(New.RagSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.RagSzam != New.RagSzam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Surgosseg' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Surgosseg != New.Surgosseg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'SURGOSSEG'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Surgosseg and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Surgosseg and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'BelyegzoDatuma' as ColumnName,               
               cast(Old.BelyegzoDatuma as nvarchar(99)) as OldValue,
               cast(New.BelyegzoDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BelyegzoDatuma != New.BelyegzoDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'UgyintezesModja' as ColumnName,               
               cast(Old.UgyintezesModja as nvarchar(99)) as OldValue,
               cast(New.UgyintezesModja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyintezesModja != New.UgyintezesModja 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'PostazasIranya' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PostazasIranya != New.PostazasIranya 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'POSTAZAS_IRANYA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.PostazasIranya and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.PostazasIranya and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Tovabbito' as ColumnName,               
               cast(Old.Tovabbito as nvarchar(99)) as OldValue,
               cast(New.Tovabbito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tovabbito != New.Tovabbito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'PeldanySzam' as ColumnName,               
               cast(Old.PeldanySzam as nvarchar(99)) as OldValue,
               cast(New.PeldanySzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PeldanySzam != New.PeldanySzam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'IktatniKell' as ColumnName,               
               cast(Old.IktatniKell as nvarchar(99)) as OldValue,
               cast(New.IktatniKell as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IktatniKell != New.IktatniKell 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Iktathato' as ColumnName,               
               cast(Old.Iktathato as nvarchar(99)) as OldValue,
               cast(New.Iktathato as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Iktathato != New.Iktathato 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'SztornirozasDat' as ColumnName,               
               cast(Old.SztornirozasDat as nvarchar(99)) as OldValue,
               cast(New.SztornirozasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SztornirozasDat != New.SztornirozasDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'KuldKuldemeny_Id_Szulo' as ColumnName,
/*FK*/           dbo.fn_GetEREC_KuldKuldemenyekAzonosito(Old.KuldKuldemeny_Id_Szulo) as OldValue,
/*FK*/           dbo.fn_GetEREC_KuldKuldemenyekAzonosito(New.KuldKuldemeny_Id_Szulo) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KuldKuldemeny_Id_Szulo != New.KuldKuldemeny_Id_Szulo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_KuldKuldemenyek FTOld on FTOld.Id = Old.KuldKuldemeny_Id_Szulo and FTOld.Ver = Old.Ver
         left join EREC_KuldKuldemenyek FTNew on FTNew.Id = New.KuldKuldemeny_Id_Szulo and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Erkeztetes_Ev' as ColumnName,               
               cast(Old.Erkeztetes_Ev as nvarchar(99)) as OldValue,
               cast(New.Erkeztetes_Ev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Erkeztetes_Ev != New.Erkeztetes_Ev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Csoport_Id_Cimzett' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Cimzett) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Cimzett) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Cimzett != New.Csoport_Id_Cimzett 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Csoportok FTOld on FTOld.Id = Old.Csoport_Id_Cimzett and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Csoport_Id_Cimzett and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Csoport_Id_Felelos' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Felelos) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Felelos) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Felelos != New.Csoport_Id_Felelos 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Csoportok FTOld on FTOld.Id = Old.Csoport_Id_Felelos and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Csoport_Id_Felelos and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'FelhasznaloCsoport_Id_Expedial' as ColumnName,               
               cast(Old.FelhasznaloCsoport_Id_Expedial as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Expedial as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Expedial != New.FelhasznaloCsoport_Id_Expedial 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'ExpedialasIdeje' as ColumnName,               
               cast(Old.ExpedialasIdeje as nvarchar(99)) as OldValue,
               cast(New.ExpedialasIdeje as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ExpedialasIdeje != New.ExpedialasIdeje 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'FelhasznaloCsoport_Id_Orzo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Orzo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Orzo) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Orzo != New.FelhasznaloCsoport_Id_Orzo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Orzo and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Orzo and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'FelhasznaloCsoport_Id_Atvevo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Atvevo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Atvevo) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Atvevo != New.FelhasznaloCsoport_Id_Atvevo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Atvevo and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Atvevo and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Partner_Id_Bekuldo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(Old.Partner_Id_Bekuldo) as OldValue,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(New.Partner_Id_Bekuldo) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Partner_Id_Bekuldo != New.Partner_Id_Bekuldo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Partnerek FTOld on FTOld.Id = Old.Partner_Id_Bekuldo and FTOld.Ver = Old.Ver
         left join KRT_Partnerek FTNew on FTNew.Id = New.Partner_Id_Bekuldo and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Cim_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CimekAzonosito(Old.Cim_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_CimekAzonosito(New.Cim_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Cim_Id != New.Cim_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Cimek FTOld on FTOld.Id = Old.Cim_Id and FTOld.Ver = Old.Ver
         left join KRT_Cimek FTNew on FTNew.Id = New.Cim_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'CimSTR_Bekuldo' as ColumnName,               
               cast(Old.CimSTR_Bekuldo as nvarchar(99)) as OldValue,
               cast(New.CimSTR_Bekuldo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.CimSTR_Bekuldo != New.CimSTR_Bekuldo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'NevSTR_Bekuldo' as ColumnName,               
               cast(Old.NevSTR_Bekuldo as nvarchar(99)) as OldValue,
               cast(New.NevSTR_Bekuldo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.NevSTR_Bekuldo != New.NevSTR_Bekuldo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'AdathordozoTipusa' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AdathordozoTipusa != New.AdathordozoTipusa 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'ADATHORDOZO_TIPUSA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.AdathordozoTipusa and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.AdathordozoTipusa and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'ElsodlegesAdathordozoTipusa' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ElsodlegesAdathordozoTipusa != New.ElsodlegesAdathordozoTipusa 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'ELSODLEGES_ADATHORDOZO'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.ElsodlegesAdathordozoTipusa and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.ElsodlegesAdathordozoTipusa and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'FelhasznaloCsoport_Id_Alairo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Alairo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Alairo) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Alairo != New.FelhasznaloCsoport_Id_Alairo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Alairo and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Alairo and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'BarCode' as ColumnName,               
               cast(Old.BarCode as nvarchar(99)) as OldValue,
               cast(New.BarCode as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BarCode != New.BarCode 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'FelhasznaloCsoport_Id_Bonto' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Bonto) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Bonto) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Bonto != New.FelhasznaloCsoport_Id_Bonto 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Bonto and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Bonto and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'CsoportFelelosEloszto_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.CsoportFelelosEloszto_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.CsoportFelelosEloszto_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.CsoportFelelosEloszto_Id != New.CsoportFelelosEloszto_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Csoportok FTOld on FTOld.Id = Old.CsoportFelelosEloszto_Id and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.CsoportFelelosEloszto_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Csoport_Id_Felelos_Elozo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Felelos_Elozo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Felelos_Elozo) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Felelos_Elozo != New.Csoport_Id_Felelos_Elozo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Csoportok FTOld on FTOld.Id = Old.Csoport_Id_Felelos_Elozo and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Csoport_Id_Felelos_Elozo and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Kovetkezo_Felelos_Id' as ColumnName,               
               cast(Old.Kovetkezo_Felelos_Id as nvarchar(99)) as OldValue,
               cast(New.Kovetkezo_Felelos_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Kovetkezo_Felelos_Id != New.Kovetkezo_Felelos_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Elektronikus_Kezbesitesi_Allap' as ColumnName,               
               cast(Old.Elektronikus_Kezbesitesi_Allap as nvarchar(99)) as OldValue,
               cast(New.Elektronikus_Kezbesitesi_Allap as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Elektronikus_Kezbesitesi_Allap != New.Elektronikus_Kezbesitesi_Allap 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Kovetkezo_Orzo_Id' as ColumnName,               
               cast(Old.Kovetkezo_Orzo_Id as nvarchar(99)) as OldValue,
               cast(New.Kovetkezo_Orzo_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Kovetkezo_Orzo_Id != New.Kovetkezo_Orzo_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Fizikai_Kezbesitesi_Allapot' as ColumnName,               
               cast(Old.Fizikai_Kezbesitesi_Allapot as nvarchar(99)) as OldValue,
               cast(New.Fizikai_Kezbesitesi_Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Fizikai_Kezbesitesi_Allapot != New.Fizikai_Kezbesitesi_Allapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'IraIratok_Id' as ColumnName,               
               cast(Old.IraIratok_Id as nvarchar(99)) as OldValue,
               cast(New.IraIratok_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IraIratok_Id != New.IraIratok_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'BontasiMegjegyzes' as ColumnName,               
               cast(Old.BontasiMegjegyzes as nvarchar(99)) as OldValue,
               cast(New.BontasiMegjegyzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BontasiMegjegyzes != New.BontasiMegjegyzes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Tipus' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tipus != New.Tipus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'KULDEMENY_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Tipus and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Tipus and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Minosites' as ColumnName,               
               cast(Old.Minosites as nvarchar(99)) as OldValue,
               cast(New.Minosites as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Minosites != New.Minosites 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'MegtagadasIndoka' as ColumnName,               
               cast(Old.MegtagadasIndoka as nvarchar(99)) as OldValue,
               cast(New.MegtagadasIndoka as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MegtagadasIndoka != New.MegtagadasIndoka 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Megtagado_Id' as ColumnName,               
               cast(Old.Megtagado_Id as nvarchar(99)) as OldValue,
               cast(New.Megtagado_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Megtagado_Id != New.Megtagado_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'MegtagadasDat' as ColumnName,               
               cast(Old.MegtagadasDat as nvarchar(99)) as OldValue,
               cast(New.MegtagadasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MegtagadasDat != New.MegtagadasDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'KimenoKuldemenyFajta' as ColumnName,               
               cast(Old.KimenoKuldemenyFajta as nvarchar(99)) as OldValue,
               cast(New.KimenoKuldemenyFajta as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KimenoKuldemenyFajta != New.KimenoKuldemenyFajta 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Elsobbsegi' as ColumnName,               
               cast(Old.Elsobbsegi as nvarchar(99)) as OldValue,
               cast(New.Elsobbsegi as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Elsobbsegi != New.Elsobbsegi 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Ajanlott' as ColumnName,               
               cast(Old.Ajanlott as nvarchar(99)) as OldValue,
               cast(New.Ajanlott as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Ajanlott != New.Ajanlott 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Tertiveveny' as ColumnName,               
               cast(Old.Tertiveveny as nvarchar(99)) as OldValue,
               cast(New.Tertiveveny as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tertiveveny != New.Tertiveveny 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'SajatKezbe' as ColumnName,               
               cast(Old.SajatKezbe as nvarchar(99)) as OldValue,
               cast(New.SajatKezbe as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SajatKezbe != New.SajatKezbe 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'E_ertesites' as ColumnName,               
               cast(Old.E_ertesites as nvarchar(99)) as OldValue,
               cast(New.E_ertesites as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.E_ertesites != New.E_ertesites 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'E_elorejelzes' as ColumnName,               
               cast(Old.E_elorejelzes as nvarchar(99)) as OldValue,
               cast(New.E_elorejelzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.E_elorejelzes != New.E_elorejelzes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'PostaiLezaroSzolgalat' as ColumnName,               
               cast(Old.PostaiLezaroSzolgalat as nvarchar(99)) as OldValue,
               cast(New.PostaiLezaroSzolgalat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PostaiLezaroSzolgalat != New.PostaiLezaroSzolgalat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Ar' as ColumnName,               
               cast(Old.Ar as nvarchar(99)) as OldValue,
               cast(New.Ar as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Ar != New.Ar 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'KimenoKuld_Sorszam ' as ColumnName,               
               cast(Old.KimenoKuld_Sorszam  as nvarchar(99)) as OldValue,
               cast(New.KimenoKuld_Sorszam  as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KimenoKuld_Sorszam  != New.KimenoKuld_Sorszam  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go
---------------------

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_KuldKuldemenyekInsert')
            and   type = 'P')
   drop procedure sp_EREC_KuldKuldemenyekInsert
go

create procedure sp_EREC_KuldKuldemenyekInsert    
                @Id      uniqueidentifier = null,    
                               @Allapot     nvarchar(64)  = null,
	            @BeerkezesIdeje     datetime,
                @FelbontasDatuma     datetime  = null,
                @IraIktatokonyv_Id     uniqueidentifier  = null,
	            @KuldesMod     nvarchar(64),
                @Erkezteto_Szam     int  = null,
                @HivatkozasiSzam     Nvarchar(100)  = null,
                @Targy     Nvarchar(4000)  = null,
                @Tartalom     Nvarchar(4000)  = null,
                @RagSzam     Nvarchar(400)  = null,
	            @Surgosseg     nvarchar(64),
                @BelyegzoDatuma     datetime  = null,
                @UgyintezesModja     nvarchar(64)  = null,
	            @PostazasIranya     nvarchar(64),
                @Tovabbito     nvarchar(64)  = null,
	            @PeldanySzam     int,
                @IktatniKell     nvarchar(64)  = null,
                @Iktathato     nvarchar(64)  = null,
                @SztornirozasDat     datetime  = null,
                @KuldKuldemeny_Id_Szulo     uniqueidentifier  = null,
                @Erkeztetes_Ev     int  = null,
                @Csoport_Id_Cimzett     uniqueidentifier  = null,
                @Csoport_Id_Felelos     uniqueidentifier  = null,
                @FelhasznaloCsoport_Id_Expedial     uniqueidentifier  = null,
                @ExpedialasIdeje     datetime  = null,
                @FelhasznaloCsoport_Id_Orzo     uniqueidentifier  = null,
                @FelhasznaloCsoport_Id_Atvevo     uniqueidentifier  = null,
                @Partner_Id_Bekuldo     uniqueidentifier  = null,
                @Cim_Id     uniqueidentifier  = null,
                @CimSTR_Bekuldo     Nvarchar(400)  = null,
                @NevSTR_Bekuldo     Nvarchar(400)  = null,
                @AdathordozoTipusa     nvarchar(64)  = null,
                @ElsodlegesAdathordozoTipusa     nvarchar(64)  = null,
                @FelhasznaloCsoport_Id_Alairo     uniqueidentifier  = null,
                @BarCode     Nvarchar(20)  = null,
                @FelhasznaloCsoport_Id_Bonto     uniqueidentifier  = null,
                @CsoportFelelosEloszto_Id     uniqueidentifier  = null,
                @Csoport_Id_Felelos_Elozo     uniqueidentifier  = null,
                @Kovetkezo_Felelos_Id     uniqueidentifier  = null,
                @Elektronikus_Kezbesitesi_Allap     nvarchar(64)  = null,
                @Kovetkezo_Orzo_Id     uniqueidentifier  = null,
                @Fizikai_Kezbesitesi_Allapot     nvarchar(64)  = null,
                @IraIratok_Id     uniqueidentifier  = null,
                @BontasiMegjegyzes     Nvarchar(400)  = null,
                @Tipus     nvarchar(64)  = null,
                @Minosites     nvarchar(64)  = null,
                @MegtagadasIndoka     Nvarchar(4000)  = null,
                @Megtagado_Id     uniqueidentifier  = null,
                @MegtagadasDat     datetime  = null,
                @KimenoKuldemenyFajta     nvarchar(64)  = null,
                @Elsobbsegi     char(1)  = null,
                @Ajanlott     char(1)  = null,
                @Tertiveveny     char(1)  = null,
                @SajatKezbe     char(1)  = null,
                @E_ertesites     char(1)  = null,
                @E_elorejelzes     char(1)  = null,
                @PostaiLezaroSzolgalat     char(1)  = null,
                @Ar     Nvarchar(100)  = null,
                @KimenoKuld_Sorszam      int  = null,
                @Ver     int  = null,
                @TovabbitasAlattAllapot     nvarchar(64)  = null,
                @Azonosito     Nvarchar(100)  = null,
                @BoritoTipus     nvarchar(64)  = null,
                @MegorzesJelzo     char(1)  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,
                @IktatastNemIgenyel     char(1)  = null,
                @KezbesitesModja     Nvarchar(100)  = null,
                @Munkaallomas     Nvarchar(100)  = null,
                @SerultKuldemeny     char(1)  = null,
                @TevesCimzes     char(1)  = null,
                @TevesErkeztetes     char(1)  = null,
                @CimzesTipusa     nvarchar(64)  = null,
                @FutarJegyzekListaSzama     Nvarchar(100)  = null,
                @Minosito     uniqueidentifier  = null,
                @MinositesErvenyessegiIdeje     datetime  = null,
                @TerjedelemMennyiseg     int  = null,
                @TerjedelemMennyisegiEgyseg     nvarchar(64)  = null,
                @TerjedelemMegjegyzes     Nvarchar(400)  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @BeerkezesIdeje is not null
         begin
            SET @insertColumns = @insertColumns + ',BeerkezesIdeje'
            SET @insertValues = @insertValues + ',@BeerkezesIdeje'
         end 
       
         if @FelbontasDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',FelbontasDatuma'
            SET @insertValues = @insertValues + ',@FelbontasDatuma'
         end 
       
         if @IraIktatokonyv_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IraIktatokonyv_Id'
            SET @insertValues = @insertValues + ',@IraIktatokonyv_Id'
         end 
       
         if @KuldesMod is not null
         begin
            SET @insertColumns = @insertColumns + ',KuldesMod'
            SET @insertValues = @insertValues + ',@KuldesMod'
         end 
       
         if @Erkezteto_Szam is not null
         begin
            SET @insertColumns = @insertColumns + ',Erkezteto_Szam'
            SET @insertValues = @insertValues + ',@Erkezteto_Szam'
         end 
       
         if @HivatkozasiSzam is not null
         begin
            SET @insertColumns = @insertColumns + ',HivatkozasiSzam'
            SET @insertValues = @insertValues + ',@HivatkozasiSzam'
         end 
       
         if @Targy is not null
         begin
            SET @insertColumns = @insertColumns + ',Targy'
            SET @insertValues = @insertValues + ',@Targy'
         end 
       
         if @Tartalom is not null
         begin
            SET @insertColumns = @insertColumns + ',Tartalom'
            SET @insertValues = @insertValues + ',@Tartalom'
         end 
       
         if @RagSzam is not null
         begin
            SET @insertColumns = @insertColumns + ',RagSzam'
            SET @insertValues = @insertValues + ',@RagSzam'
         end 
       
         if @Surgosseg is not null
         begin
            SET @insertColumns = @insertColumns + ',Surgosseg'
            SET @insertValues = @insertValues + ',@Surgosseg'
         end 
       
         if @BelyegzoDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',BelyegzoDatuma'
            SET @insertValues = @insertValues + ',@BelyegzoDatuma'
         end 
       
         if @UgyintezesModja is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyintezesModja'
            SET @insertValues = @insertValues + ',@UgyintezesModja'
         end 
       
         if @PostazasIranya is not null
         begin
            SET @insertColumns = @insertColumns + ',PostazasIranya'
            SET @insertValues = @insertValues + ',@PostazasIranya'
         end 
       
         if @Tovabbito is not null
         begin
            SET @insertColumns = @insertColumns + ',Tovabbito'
            SET @insertValues = @insertValues + ',@Tovabbito'
         end 
       
         if @PeldanySzam is not null
         begin
            SET @insertColumns = @insertColumns + ',PeldanySzam'
            SET @insertValues = @insertValues + ',@PeldanySzam'
         end 
       
         if @IktatniKell is not null
         begin
            SET @insertColumns = @insertColumns + ',IktatniKell'
            SET @insertValues = @insertValues + ',@IktatniKell'
         end 
       
         if @Iktathato is not null
         begin
            SET @insertColumns = @insertColumns + ',Iktathato'
            SET @insertValues = @insertValues + ',@Iktathato'
         end 
       
         if @SztornirozasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SztornirozasDat'
            SET @insertValues = @insertValues + ',@SztornirozasDat'
         end 
       
         if @KuldKuldemeny_Id_Szulo is not null
         begin
            SET @insertColumns = @insertColumns + ',KuldKuldemeny_Id_Szulo'
            SET @insertValues = @insertValues + ',@KuldKuldemeny_Id_Szulo'
         end 
       
         if @Erkeztetes_Ev is not null
         begin
            SET @insertColumns = @insertColumns + ',Erkeztetes_Ev'
            SET @insertValues = @insertValues + ',@Erkeztetes_Ev'
         end 
       
         if @Csoport_Id_Cimzett is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Cimzett'
            SET @insertValues = @insertValues + ',@Csoport_Id_Cimzett'
         end 
       
         if @Csoport_Id_Felelos is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Felelos'
            SET @insertValues = @insertValues + ',@Csoport_Id_Felelos'
         end 
       
         if @FelhasznaloCsoport_Id_Expedial is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Expedial'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Expedial'
         end 
       
         if @ExpedialasIdeje is not null
         begin
            SET @insertColumns = @insertColumns + ',ExpedialasIdeje'
            SET @insertValues = @insertValues + ',@ExpedialasIdeje'
         end 
       
         if @FelhasznaloCsoport_Id_Orzo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Orzo'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Orzo'
         end 
       
         if @FelhasznaloCsoport_Id_Atvevo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Atvevo'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Atvevo'
         end 
       
         if @Partner_Id_Bekuldo is not null
         begin
            SET @insertColumns = @insertColumns + ',Partner_Id_Bekuldo'
            SET @insertValues = @insertValues + ',@Partner_Id_Bekuldo'
         end 
       
         if @Cim_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Cim_Id'
            SET @insertValues = @insertValues + ',@Cim_Id'
         end 
       
         if @CimSTR_Bekuldo is not null
         begin
            SET @insertColumns = @insertColumns + ',CimSTR_Bekuldo'
            SET @insertValues = @insertValues + ',@CimSTR_Bekuldo'
         end 
       
         if @NevSTR_Bekuldo is not null
         begin
            SET @insertColumns = @insertColumns + ',NevSTR_Bekuldo'
            SET @insertValues = @insertValues + ',@NevSTR_Bekuldo'
         end 
       
         if @AdathordozoTipusa is not null
         begin
            SET @insertColumns = @insertColumns + ',AdathordozoTipusa'
            SET @insertValues = @insertValues + ',@AdathordozoTipusa'
         end 
       
         if @ElsodlegesAdathordozoTipusa is not null
         begin
            SET @insertColumns = @insertColumns + ',ElsodlegesAdathordozoTipusa'
            SET @insertValues = @insertValues + ',@ElsodlegesAdathordozoTipusa'
         end 
       
         if @FelhasznaloCsoport_Id_Alairo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Alairo'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Alairo'
         end 
       
         if @BarCode is not null
         begin
            SET @insertColumns = @insertColumns + ',BarCode'
            SET @insertValues = @insertValues + ',@BarCode'
         end 
       
         if @FelhasznaloCsoport_Id_Bonto is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Bonto'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Bonto'
         end 
       
         if @CsoportFelelosEloszto_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',CsoportFelelosEloszto_Id'
            SET @insertValues = @insertValues + ',@CsoportFelelosEloszto_Id'
         end 
       
         if @Csoport_Id_Felelos_Elozo is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Felelos_Elozo'
            SET @insertValues = @insertValues + ',@Csoport_Id_Felelos_Elozo'
         end 
       
         if @Kovetkezo_Felelos_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Kovetkezo_Felelos_Id'
            SET @insertValues = @insertValues + ',@Kovetkezo_Felelos_Id'
         end 
       
         if @Elektronikus_Kezbesitesi_Allap is not null
         begin
            SET @insertColumns = @insertColumns + ',Elektronikus_Kezbesitesi_Allap'
            SET @insertValues = @insertValues + ',@Elektronikus_Kezbesitesi_Allap'
         end 
       
         if @Kovetkezo_Orzo_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Kovetkezo_Orzo_Id'
            SET @insertValues = @insertValues + ',@Kovetkezo_Orzo_Id'
         end 
       
         if @Fizikai_Kezbesitesi_Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Fizikai_Kezbesitesi_Allapot'
            SET @insertValues = @insertValues + ',@Fizikai_Kezbesitesi_Allapot'
         end 
       
         if @IraIratok_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IraIratok_Id'
            SET @insertValues = @insertValues + ',@IraIratok_Id'
         end 
       
         if @BontasiMegjegyzes is not null
         begin
            SET @insertColumns = @insertColumns + ',BontasiMegjegyzes'
            SET @insertValues = @insertValues + ',@BontasiMegjegyzes'
         end 
       
         if @Tipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Tipus'
            SET @insertValues = @insertValues + ',@Tipus'
         end 
       
         if @Minosites is not null
         begin
            SET @insertColumns = @insertColumns + ',Minosites'
            SET @insertValues = @insertValues + ',@Minosites'
         end 
       
         if @MegtagadasIndoka is not null
         begin
            SET @insertColumns = @insertColumns + ',MegtagadasIndoka'
            SET @insertValues = @insertValues + ',@MegtagadasIndoka'
         end 
       
         if @Megtagado_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Megtagado_Id'
            SET @insertValues = @insertValues + ',@Megtagado_Id'
         end 
       
         if @MegtagadasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',MegtagadasDat'
            SET @insertValues = @insertValues + ',@MegtagadasDat'
         end 
       
         if @KimenoKuldemenyFajta is not null
         begin
            SET @insertColumns = @insertColumns + ',KimenoKuldemenyFajta'
            SET @insertValues = @insertValues + ',@KimenoKuldemenyFajta'
         end 
       
         if @Elsobbsegi is not null
         begin
            SET @insertColumns = @insertColumns + ',Elsobbsegi'
            SET @insertValues = @insertValues + ',@Elsobbsegi'
         end 
       
         if @Ajanlott is not null
         begin
            SET @insertColumns = @insertColumns + ',Ajanlott'
            SET @insertValues = @insertValues + ',@Ajanlott'
         end 
       
         if @Tertiveveny is not null
         begin
            SET @insertColumns = @insertColumns + ',Tertiveveny'
            SET @insertValues = @insertValues + ',@Tertiveveny'
         end 
       
         if @SajatKezbe is not null
         begin
            SET @insertColumns = @insertColumns + ',SajatKezbe'
            SET @insertValues = @insertValues + ',@SajatKezbe'
         end 
       
         if @E_ertesites is not null
         begin
            SET @insertColumns = @insertColumns + ',E_ertesites'
            SET @insertValues = @insertValues + ',@E_ertesites'
         end 
       
         if @E_elorejelzes is not null
         begin
            SET @insertColumns = @insertColumns + ',E_elorejelzes'
            SET @insertValues = @insertValues + ',@E_elorejelzes'
         end 
       
         if @PostaiLezaroSzolgalat is not null
         begin
            SET @insertColumns = @insertColumns + ',PostaiLezaroSzolgalat'
            SET @insertValues = @insertValues + ',@PostaiLezaroSzolgalat'
         end 
       
         if @Ar is not null
         begin
            SET @insertColumns = @insertColumns + ',Ar'
            SET @insertValues = @insertValues + ',@Ar'
         end 
       
         if @KimenoKuld_Sorszam  is not null
         begin
            SET @insertColumns = @insertColumns + ',KimenoKuld_Sorszam '
            SET @insertValues = @insertValues + ',@KimenoKuld_Sorszam '
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @TovabbitasAlattAllapot is not null
         begin
            SET @insertColumns = @insertColumns + ',TovabbitasAlattAllapot'
            SET @insertValues = @insertValues + ',@TovabbitasAlattAllapot'
         end 
       
         if @Azonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Azonosito'
            SET @insertValues = @insertValues + ',@Azonosito'
         end 
       
         if @BoritoTipus is not null
         begin
            SET @insertColumns = @insertColumns + ',BoritoTipus'
            SET @insertValues = @insertValues + ',@BoritoTipus'
         end 
       
         if @MegorzesJelzo is not null
         begin
            SET @insertColumns = @insertColumns + ',MegorzesJelzo'
            SET @insertValues = @insertValues + ',@MegorzesJelzo'
         end 
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end 
       
         if @IktatastNemIgenyel is not null
         begin
            SET @insertColumns = @insertColumns + ',IktatastNemIgenyel'
            SET @insertValues = @insertValues + ',@IktatastNemIgenyel'
         end 
       
         if @KezbesitesModja is not null
         begin
            SET @insertColumns = @insertColumns + ',KezbesitesModja'
            SET @insertValues = @insertValues + ',@KezbesitesModja'
         end 
       
         if @Munkaallomas is not null
         begin
            SET @insertColumns = @insertColumns + ',Munkaallomas'
            SET @insertValues = @insertValues + ',@Munkaallomas'
         end 
       
         if @SerultKuldemeny is not null
         begin
            SET @insertColumns = @insertColumns + ',SerultKuldemeny'
            SET @insertValues = @insertValues + ',@SerultKuldemeny'
         end 
       
         if @TevesCimzes is not null
         begin
            SET @insertColumns = @insertColumns + ',TevesCimzes'
            SET @insertValues = @insertValues + ',@TevesCimzes'
         end 
       
         if @TevesErkeztetes is not null
         begin
            SET @insertColumns = @insertColumns + ',TevesErkeztetes'
            SET @insertValues = @insertValues + ',@TevesErkeztetes'
         end 
       
         if @CimzesTipusa is not null
         begin
            SET @insertColumns = @insertColumns + ',CimzesTipusa'
            SET @insertValues = @insertValues + ',@CimzesTipusa'
         end 
       
         if @FutarJegyzekListaSzama is not null
         begin
            SET @insertColumns = @insertColumns + ',FutarJegyzekListaSzama'
            SET @insertValues = @insertValues + ',@FutarJegyzekListaSzama'
         end 
       
         if @Minosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Minosito'
            SET @insertValues = @insertValues + ',@Minosito'
         end 
       
         if @MinositesErvenyessegiIdeje is not null
         begin
            SET @insertColumns = @insertColumns + ',MinositesErvenyessegiIdeje'
            SET @insertValues = @insertValues + ',@MinositesErvenyessegiIdeje'
         end 
       
         if @TerjedelemMennyiseg is not null
         begin
            SET @insertColumns = @insertColumns + ',TerjedelemMennyiseg'
            SET @insertValues = @insertValues + ',@TerjedelemMennyiseg'
         end 
       
         if @TerjedelemMennyisegiEgyseg is not null
         begin
            SET @insertColumns = @insertColumns + ',TerjedelemMennyisegiEgyseg'
            SET @insertValues = @insertValues + ',@TerjedelemMennyisegiEgyseg'
         end 
       
         if @TerjedelemMegjegyzes is not null
         begin
            SET @insertColumns = @insertColumns + ',TerjedelemMegjegyzes'
            SET @insertValues = @insertValues + ',@TerjedelemMegjegyzes'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_KuldKuldemenyek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Allapot nvarchar(64),@BeerkezesIdeje datetime,@FelbontasDatuma datetime,@IraIktatokonyv_Id uniqueidentifier,@KuldesMod nvarchar(64),@Erkezteto_Szam int,@HivatkozasiSzam Nvarchar(100),@Targy Nvarchar(4000),@Tartalom Nvarchar(4000),@RagSzam Nvarchar(400),@Surgosseg nvarchar(64),@BelyegzoDatuma datetime,@UgyintezesModja nvarchar(64),@PostazasIranya nvarchar(64),@Tovabbito nvarchar(64),@PeldanySzam int,@IktatniKell nvarchar(64),@Iktathato nvarchar(64),@SztornirozasDat datetime,@KuldKuldemeny_Id_Szulo uniqueidentifier,@Erkeztetes_Ev int,@Csoport_Id_Cimzett uniqueidentifier,@Csoport_Id_Felelos uniqueidentifier,@FelhasznaloCsoport_Id_Expedial uniqueidentifier,@ExpedialasIdeje datetime,@FelhasznaloCsoport_Id_Orzo uniqueidentifier,@FelhasznaloCsoport_Id_Atvevo uniqueidentifier,@Partner_Id_Bekuldo uniqueidentifier,@Cim_Id uniqueidentifier,@CimSTR_Bekuldo Nvarchar(400),@NevSTR_Bekuldo Nvarchar(400),@AdathordozoTipusa nvarchar(64),@ElsodlegesAdathordozoTipusa nvarchar(64),@FelhasznaloCsoport_Id_Alairo uniqueidentifier,@BarCode Nvarchar(20),@FelhasznaloCsoport_Id_Bonto uniqueidentifier,@CsoportFelelosEloszto_Id uniqueidentifier,@Csoport_Id_Felelos_Elozo uniqueidentifier,@Kovetkezo_Felelos_Id uniqueidentifier,@Elektronikus_Kezbesitesi_Allap nvarchar(64),@Kovetkezo_Orzo_Id uniqueidentifier,@Fizikai_Kezbesitesi_Allapot nvarchar(64),@IraIratok_Id uniqueidentifier,@BontasiMegjegyzes Nvarchar(400),@Tipus nvarchar(64),@Minosites nvarchar(64),@MegtagadasIndoka Nvarchar(4000),@Megtagado_Id uniqueidentifier,@MegtagadasDat datetime,@KimenoKuldemenyFajta nvarchar(64),@Elsobbsegi char(1),@Ajanlott char(1),@Tertiveveny char(1),@SajatKezbe char(1),@E_ertesites char(1),@E_elorejelzes char(1),@PostaiLezaroSzolgalat char(1),@Ar Nvarchar(100),@KimenoKuld_Sorszam  int,@Ver int,@TovabbitasAlattAllapot nvarchar(64),@Azonosito Nvarchar(100),@BoritoTipus nvarchar(64),@MegorzesJelzo char(1),@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@IktatastNemIgenyel char(1),@KezbesitesModja Nvarchar(100),@Munkaallomas Nvarchar(100),@SerultKuldemeny char(1),@TevesCimzes char(1),@TevesErkeztetes char(1),@CimzesTipusa nvarchar(64),@FutarJegyzekListaSzama Nvarchar(100),@Minosito uniqueidentifier,@MinositesErvenyessegiIdeje datetime,@TerjedelemMennyiseg int,@TerjedelemMennyisegiEgyseg nvarchar(64),@TerjedelemMegjegyzes Nvarchar(400),@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Allapot = @Allapot,@BeerkezesIdeje = @BeerkezesIdeje,@FelbontasDatuma = @FelbontasDatuma,@IraIktatokonyv_Id = @IraIktatokonyv_Id,@KuldesMod = @KuldesMod,@Erkezteto_Szam = @Erkezteto_Szam,@HivatkozasiSzam = @HivatkozasiSzam,@Targy = @Targy,@Tartalom = @Tartalom,@RagSzam = @RagSzam,@Surgosseg = @Surgosseg,@BelyegzoDatuma = @BelyegzoDatuma,@UgyintezesModja = @UgyintezesModja,@PostazasIranya = @PostazasIranya,@Tovabbito = @Tovabbito,@PeldanySzam = @PeldanySzam,@IktatniKell = @IktatniKell,@Iktathato = @Iktathato,@SztornirozasDat = @SztornirozasDat,@KuldKuldemeny_Id_Szulo = @KuldKuldemeny_Id_Szulo,@Erkeztetes_Ev = @Erkeztetes_Ev,@Csoport_Id_Cimzett = @Csoport_Id_Cimzett,@Csoport_Id_Felelos = @Csoport_Id_Felelos,@FelhasznaloCsoport_Id_Expedial = @FelhasznaloCsoport_Id_Expedial,@ExpedialasIdeje = @ExpedialasIdeje,@FelhasznaloCsoport_Id_Orzo = @FelhasznaloCsoport_Id_Orzo,@FelhasznaloCsoport_Id_Atvevo = @FelhasznaloCsoport_Id_Atvevo,@Partner_Id_Bekuldo = @Partner_Id_Bekuldo,@Cim_Id = @Cim_Id,@CimSTR_Bekuldo = @CimSTR_Bekuldo,@NevSTR_Bekuldo = @NevSTR_Bekuldo,@AdathordozoTipusa = @AdathordozoTipusa,@ElsodlegesAdathordozoTipusa = @ElsodlegesAdathordozoTipusa,@FelhasznaloCsoport_Id_Alairo = @FelhasznaloCsoport_Id_Alairo,@BarCode = @BarCode,@FelhasznaloCsoport_Id_Bonto = @FelhasznaloCsoport_Id_Bonto,@CsoportFelelosEloszto_Id = @CsoportFelelosEloszto_Id,@Csoport_Id_Felelos_Elozo = @Csoport_Id_Felelos_Elozo,@Kovetkezo_Felelos_Id = @Kovetkezo_Felelos_Id,@Elektronikus_Kezbesitesi_Allap = @Elektronikus_Kezbesitesi_Allap,@Kovetkezo_Orzo_Id = @Kovetkezo_Orzo_Id,@Fizikai_Kezbesitesi_Allapot = @Fizikai_Kezbesitesi_Allapot,@IraIratok_Id = @IraIratok_Id,@BontasiMegjegyzes = @BontasiMegjegyzes,@Tipus = @Tipus,@Minosites = @Minosites,@MegtagadasIndoka = @MegtagadasIndoka,@Megtagado_Id = @Megtagado_Id,@MegtagadasDat = @MegtagadasDat,@KimenoKuldemenyFajta = @KimenoKuldemenyFajta,@Elsobbsegi = @Elsobbsegi,@Ajanlott = @Ajanlott,@Tertiveveny = @Tertiveveny,@SajatKezbe = @SajatKezbe,@E_ertesites = @E_ertesites,@E_elorejelzes = @E_elorejelzes,@PostaiLezaroSzolgalat = @PostaiLezaroSzolgalat,@Ar = @Ar,@KimenoKuld_Sorszam  = @KimenoKuld_Sorszam ,@Ver = @Ver,@TovabbitasAlattAllapot = @TovabbitasAlattAllapot,@Azonosito = @Azonosito,@BoritoTipus = @BoritoTipus,@MegorzesJelzo = @MegorzesJelzo,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id,@IktatastNemIgenyel = @IktatastNemIgenyel,@KezbesitesModja = @KezbesitesModja,@Munkaallomas = @Munkaallomas,@SerultKuldemeny = @SerultKuldemeny,@TevesCimzes = @TevesCimzes,@TevesErkeztetes = @TevesErkeztetes,@CimzesTipusa = @CimzesTipusa,@FutarJegyzekListaSzama = @FutarJegyzekListaSzama,@Minosito = @Minosito,@MinositesErvenyessegiIdeje = @MinositesErvenyessegiIdeje,@TerjedelemMennyiseg = @TerjedelemMennyiseg,@TerjedelemMennyisegiEgyseg = @TerjedelemMennyisegiEgyseg,@TerjedelemMegjegyzes = @TerjedelemMegjegyzes ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_KuldKuldemenyek',@ResultUid
					,'EREC_KuldKuldemenyekHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
go

/* haszn�lt errorcode-ok:
	50301:
	50302: 'Org' oszlop �rt�ke nem lehet NULL
*/

---------------------

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_KuldKuldemenyekInvalidate')
            and   type = 'P')
   drop procedure sp_EREC_KuldKuldemenyekInvalidate
go

create procedure sp_EREC_KuldKuldemenyekInvalidate
        @Id							uniqueidentifier,
		@ExecutorUserId				uniqueidentifier,
		@ExecutionTime					datetime			

as

BEGIN TRY
--BEGIN TRANSACTION InvalidateTransaction

  
	set nocount on
   
   SET @ExecutionTime = getdate()

	-- Zarolas ellenorzese:
	DECLARE @IsLockedByOtherUser BIT
	exec sp_GetLockingInfo 'EREC_KuldKuldemenyek',@Id,@ExecutorUserId,@IsLockedByOtherUser OUTPUT,null

	if (@IsLockedByOtherUser = 0)
	BEGIN
   
      DECLARE @Act_ErvKezd datetime
      DECLARE @Act_ErvVege datetime
      DECLARE @Act_Ver int
      
      select
         @Act_ErvKezd = ErvKezd,
         @Act_ErvVege = ErvVege,
         @Act_Ver = Ver
      from EREC_KuldKuldemenyek
      where Id = @Id
      
      if @@rowcount = 0
	   begin
	    	RAISERROR('[50602]',16,1)
	   end
      
      if @Act_ErvVege <= @ExecutionTime
      begin
         -- m�r �rv�nytelen�tve van
         RAISERROR('[50603]',16,1)
      end
      
      IF @Act_Ver is null
	   	SET @Act_Ver = 2	
      ELSE
	   	SET @Act_Ver = @Act_Ver+1
      
      if @Act_ErvKezd > @ExecutionTime
      begin
      
         -- ha a j�v�ben kezd�d�tt volna el, az ErvKezd-et is be�ll�tjuk
      
		   UPDATE EREC_KuldKuldemenyek
		   SET		 					
		   		 ErvVege = @ExecutionTime,
                ErvKezd = @ExecutionTime,
                Ver     = @Act_Ver
		   WHERE
		         Id = @Id
        
      end
      else
      begin
   
		   UPDATE EREC_KuldKuldemenyek
		   SET		 					
		   		 ErvVege = @ExecutionTime,
                Ver     = @Act_Ver
		   WHERE
		         Id = @Id
        
      end

		if @@rowcount != 1
		begin
			RAISERROR('[50601]',16,1)
		end        else
        begin
           
           /* History Log */
           exec sp_LogRecordToHistory 'EREC_KuldKuldemenyek',@Id
                 ,'EREC_KuldKuldemenyekHistory',2,@ExecutorUserId,@ExecutionTime
           
           
        end	END
	ELSE BEGIN
		RAISERROR('[50699]',16,1)
	END


--COMMIT TRANSACTION InvalidateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InvalidateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

go


/* haszn�lt errorcode-ok:
	50601: UPDATE sikertelen (update-elt sorok sz�ma 0)
   50602: nincs ilyen rekord
   50603: M�r �rv�nytelen�tve van a rekord
	50699: rekord z�rolva
*/
---------------------


set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_KuldKuldemenyekUpdate')
            and   type = 'P')
   drop procedure sp_EREC_KuldKuldemenyekUpdate
go

create procedure sp_EREC_KuldKuldemenyekUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Allapot     nvarchar(64)  = null,         
             @BeerkezesIdeje     datetime  = null,         
             @FelbontasDatuma     datetime  = null,         
             @IraIktatokonyv_Id     uniqueidentifier  = null,         
             @KuldesMod     nvarchar(64)  = null,         
             @Erkezteto_Szam     int  = null,         
             @HivatkozasiSzam     Nvarchar(100)  = null,         
             @Targy     Nvarchar(4000)  = null,         
             @Tartalom     Nvarchar(4000)  = null,         
             @RagSzam     Nvarchar(400)  = null,         
             @Surgosseg     nvarchar(64)  = null,         
             @BelyegzoDatuma     datetime  = null,         
             @UgyintezesModja     nvarchar(64)  = null,         
             @PostazasIranya     nvarchar(64)  = null,         
             @Tovabbito     nvarchar(64)  = null,         
             @PeldanySzam     int  = null,         
             @IktatniKell     nvarchar(64)  = null,         
             @Iktathato     nvarchar(64)  = null,         
             @SztornirozasDat     datetime  = null,         
             @KuldKuldemeny_Id_Szulo     uniqueidentifier  = null,         
             @Erkeztetes_Ev     int  = null,         
             @Csoport_Id_Cimzett     uniqueidentifier  = null,         
             @Csoport_Id_Felelos     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Expedial     uniqueidentifier  = null,         
             @ExpedialasIdeje     datetime  = null,         
             @FelhasznaloCsoport_Id_Orzo     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Atvevo     uniqueidentifier  = null,         
             @Partner_Id_Bekuldo     uniqueidentifier  = null,         
             @Cim_Id     uniqueidentifier  = null,         
             @CimSTR_Bekuldo     Nvarchar(400)  = null,         
             @NevSTR_Bekuldo     Nvarchar(400)  = null,         
             @AdathordozoTipusa     nvarchar(64)  = null,         
             @ElsodlegesAdathordozoTipusa     nvarchar(64)  = null,         
             @FelhasznaloCsoport_Id_Alairo     uniqueidentifier  = null,         
             @BarCode     Nvarchar(20)  = null,         
             @FelhasznaloCsoport_Id_Bonto     uniqueidentifier  = null,         
             @CsoportFelelosEloszto_Id     uniqueidentifier  = null,         
             @Csoport_Id_Felelos_Elozo     uniqueidentifier  = null,         
             @Kovetkezo_Felelos_Id     uniqueidentifier  = null,         
             @Elektronikus_Kezbesitesi_Allap     nvarchar(64)  = null,         
             @Kovetkezo_Orzo_Id     uniqueidentifier  = null,         
             @Fizikai_Kezbesitesi_Allapot     nvarchar(64)  = null,         
             @IraIratok_Id     uniqueidentifier  = null,         
             @BontasiMegjegyzes     Nvarchar(400)  = null,         
             @Tipus     nvarchar(64)  = null,         
             @Minosites     nvarchar(64)  = null,         
             @MegtagadasIndoka     Nvarchar(4000)  = null,         
             @Megtagado_Id     uniqueidentifier  = null,         
             @MegtagadasDat     datetime  = null,         
             @KimenoKuldemenyFajta     nvarchar(64)  = null,         
             @Elsobbsegi     char(1)  = null,         
             @Ajanlott     char(1)  = null,         
             @Tertiveveny     char(1)  = null,         
             @SajatKezbe     char(1)  = null,         
             @E_ertesites     char(1)  = null,         
             @E_elorejelzes     char(1)  = null,         
             @PostaiLezaroSzolgalat     char(1)  = null,         
             @Ar     Nvarchar(100)  = null,         
             @KimenoKuld_Sorszam      int  = null,         
             @Ver     int  = null,         
             @TovabbitasAlattAllapot     nvarchar(64)  = null,         
             @Azonosito     Nvarchar(100)  = null,         
             @BoritoTipus     nvarchar(64)  = null,         
             @MegorzesJelzo     char(1)  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
             @IktatastNemIgenyel     char(1)  = null,         
             @KezbesitesModja     Nvarchar(100)  = null,         
             @Munkaallomas     Nvarchar(100)  = null,         
             @SerultKuldemeny     char(1)  = null,         
             @TevesCimzes     char(1)  = null,         
             @TevesErkeztetes     char(1)  = null,         
             @CimzesTipusa     nvarchar(64)  = null,         
             @FutarJegyzekListaSzama     Nvarchar(100)  = null,         
             @Minosito     uniqueidentifier  = null,         
             @MinositesErvenyessegiIdeje     datetime  = null,         
             @TerjedelemMennyiseg     int  = null,         
             @TerjedelemMennyisegiEgyseg     nvarchar(64)  = null,         
             @TerjedelemMegjegyzes     Nvarchar(400)  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_BeerkezesIdeje     datetime         
     DECLARE @Act_FelbontasDatuma     datetime         
     DECLARE @Act_IraIktatokonyv_Id     uniqueidentifier         
     DECLARE @Act_KuldesMod     nvarchar(64)         
     DECLARE @Act_Erkezteto_Szam     int         
     DECLARE @Act_HivatkozasiSzam     Nvarchar(100)         
     DECLARE @Act_Targy     Nvarchar(4000)         
     DECLARE @Act_Tartalom     Nvarchar(4000)         
     DECLARE @Act_RagSzam     Nvarchar(400)         
     DECLARE @Act_Surgosseg     nvarchar(64)         
     DECLARE @Act_BelyegzoDatuma     datetime         
     DECLARE @Act_UgyintezesModja     nvarchar(64)         
     DECLARE @Act_PostazasIranya     nvarchar(64)         
     DECLARE @Act_Tovabbito     nvarchar(64)         
     DECLARE @Act_PeldanySzam     int         
     DECLARE @Act_IktatniKell     nvarchar(64)         
     DECLARE @Act_Iktathato     nvarchar(64)         
     DECLARE @Act_SztornirozasDat     datetime         
     DECLARE @Act_KuldKuldemeny_Id_Szulo     uniqueidentifier         
     DECLARE @Act_Erkeztetes_Ev     int         
     DECLARE @Act_Csoport_Id_Cimzett     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Felelos     uniqueidentifier         
     DECLARE @Act_FelhasznaloCsoport_Id_Expedial     uniqueidentifier         
     DECLARE @Act_ExpedialasIdeje     datetime         
     DECLARE @Act_FelhasznaloCsoport_Id_Orzo     uniqueidentifier         
     DECLARE @Act_FelhasznaloCsoport_Id_Atvevo     uniqueidentifier         
     DECLARE @Act_Partner_Id_Bekuldo     uniqueidentifier         
     DECLARE @Act_Cim_Id     uniqueidentifier         
     DECLARE @Act_CimSTR_Bekuldo     Nvarchar(400)         
     DECLARE @Act_NevSTR_Bekuldo     Nvarchar(400)         
     DECLARE @Act_AdathordozoTipusa     nvarchar(64)         
     DECLARE @Act_ElsodlegesAdathordozoTipusa     nvarchar(64)         
     DECLARE @Act_FelhasznaloCsoport_Id_Alairo     uniqueidentifier         
     DECLARE @Act_BarCode     Nvarchar(20)         
     DECLARE @Act_FelhasznaloCsoport_Id_Bonto     uniqueidentifier         
     DECLARE @Act_CsoportFelelosEloszto_Id     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Felelos_Elozo     uniqueidentifier         
     DECLARE @Act_Kovetkezo_Felelos_Id     uniqueidentifier         
     DECLARE @Act_Elektronikus_Kezbesitesi_Allap     nvarchar(64)         
     DECLARE @Act_Kovetkezo_Orzo_Id     uniqueidentifier         
     DECLARE @Act_Fizikai_Kezbesitesi_Allapot     nvarchar(64)         
     DECLARE @Act_IraIratok_Id     uniqueidentifier         
     DECLARE @Act_BontasiMegjegyzes     Nvarchar(400)         
     DECLARE @Act_Tipus     nvarchar(64)         
     DECLARE @Act_Minosites     nvarchar(64)         
     DECLARE @Act_MegtagadasIndoka     Nvarchar(4000)         
     DECLARE @Act_Megtagado_Id     uniqueidentifier         
     DECLARE @Act_MegtagadasDat     datetime         
     DECLARE @Act_KimenoKuldemenyFajta     nvarchar(64)         
     DECLARE @Act_Elsobbsegi     char(1)         
     DECLARE @Act_Ajanlott     char(1)         
     DECLARE @Act_Tertiveveny     char(1)         
     DECLARE @Act_SajatKezbe     char(1)         
     DECLARE @Act_E_ertesites     char(1)         
     DECLARE @Act_E_elorejelzes     char(1)         
     DECLARE @Act_PostaiLezaroSzolgalat     char(1)         
     DECLARE @Act_Ar     Nvarchar(100)         
     DECLARE @Act_KimenoKuld_Sorszam      int         
     DECLARE @Act_Ver     int         
     DECLARE @Act_TovabbitasAlattAllapot     nvarchar(64)         
     DECLARE @Act_Azonosito     Nvarchar(100)         
     DECLARE @Act_BoritoTipus     nvarchar(64)         
     DECLARE @Act_MegorzesJelzo     char(1)         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier         
     DECLARE @Act_IktatastNemIgenyel     char(1)         
     DECLARE @Act_KezbesitesModja     Nvarchar(100)         
     DECLARE @Act_Munkaallomas     Nvarchar(100)         
     DECLARE @Act_SerultKuldemeny     char(1)         
     DECLARE @Act_TevesCimzes     char(1)         
     DECLARE @Act_TevesErkeztetes     char(1)         
     DECLARE @Act_CimzesTipusa     nvarchar(64)         
     DECLARE @Act_FutarJegyzekListaSzama     Nvarchar(100)         
     DECLARE @Act_Minosito     uniqueidentifier         
     DECLARE @Act_MinositesErvenyessegiIdeje     datetime         
     DECLARE @Act_TerjedelemMennyiseg     int         
     DECLARE @Act_TerjedelemMennyisegiEgyseg     nvarchar(64)         
     DECLARE @Act_TerjedelemMegjegyzes     Nvarchar(400)           
  
set nocount on

select 
     @Act_Allapot = Allapot,
     @Act_BeerkezesIdeje = BeerkezesIdeje,
     @Act_FelbontasDatuma = FelbontasDatuma,
     @Act_IraIktatokonyv_Id = IraIktatokonyv_Id,
     @Act_KuldesMod = KuldesMod,
     @Act_Erkezteto_Szam = Erkezteto_Szam,
     @Act_HivatkozasiSzam = HivatkozasiSzam,
     @Act_Targy = Targy,
     @Act_Tartalom = Tartalom,
     @Act_RagSzam = RagSzam,
     @Act_Surgosseg = Surgosseg,
     @Act_BelyegzoDatuma = BelyegzoDatuma,
     @Act_UgyintezesModja = UgyintezesModja,
     @Act_PostazasIranya = PostazasIranya,
     @Act_Tovabbito = Tovabbito,
     @Act_PeldanySzam = PeldanySzam,
     @Act_IktatniKell = IktatniKell,
     @Act_Iktathato = Iktathato,
     @Act_SztornirozasDat = SztornirozasDat,
     @Act_KuldKuldemeny_Id_Szulo = KuldKuldemeny_Id_Szulo,
     @Act_Erkeztetes_Ev = Erkeztetes_Ev,
     @Act_Csoport_Id_Cimzett = Csoport_Id_Cimzett,
     @Act_Csoport_Id_Felelos = Csoport_Id_Felelos,
     @Act_FelhasznaloCsoport_Id_Expedial = FelhasznaloCsoport_Id_Expedial,
     @Act_ExpedialasIdeje = ExpedialasIdeje,
     @Act_FelhasznaloCsoport_Id_Orzo = FelhasznaloCsoport_Id_Orzo,
     @Act_FelhasznaloCsoport_Id_Atvevo = FelhasznaloCsoport_Id_Atvevo,
     @Act_Partner_Id_Bekuldo = Partner_Id_Bekuldo,
     @Act_Cim_Id = Cim_Id,
     @Act_CimSTR_Bekuldo = CimSTR_Bekuldo,
     @Act_NevSTR_Bekuldo = NevSTR_Bekuldo,
     @Act_AdathordozoTipusa = AdathordozoTipusa,
     @Act_ElsodlegesAdathordozoTipusa = ElsodlegesAdathordozoTipusa,
     @Act_FelhasznaloCsoport_Id_Alairo = FelhasznaloCsoport_Id_Alairo,
     @Act_BarCode = BarCode,
     @Act_FelhasznaloCsoport_Id_Bonto = FelhasznaloCsoport_Id_Bonto,
     @Act_CsoportFelelosEloszto_Id = CsoportFelelosEloszto_Id,
     @Act_Csoport_Id_Felelos_Elozo = Csoport_Id_Felelos_Elozo,
     @Act_Kovetkezo_Felelos_Id = Kovetkezo_Felelos_Id,
     @Act_Elektronikus_Kezbesitesi_Allap = Elektronikus_Kezbesitesi_Allap,
     @Act_Kovetkezo_Orzo_Id = Kovetkezo_Orzo_Id,
     @Act_Fizikai_Kezbesitesi_Allapot = Fizikai_Kezbesitesi_Allapot,
     @Act_IraIratok_Id = IraIratok_Id,
     @Act_BontasiMegjegyzes = BontasiMegjegyzes,
     @Act_Tipus = Tipus,
     @Act_Minosites = Minosites,
     @Act_MegtagadasIndoka = MegtagadasIndoka,
     @Act_Megtagado_Id = Megtagado_Id,
     @Act_MegtagadasDat = MegtagadasDat,
     @Act_KimenoKuldemenyFajta = KimenoKuldemenyFajta,
     @Act_Elsobbsegi = Elsobbsegi,
     @Act_Ajanlott = Ajanlott,
     @Act_Tertiveveny = Tertiveveny,
     @Act_SajatKezbe = SajatKezbe,
     @Act_E_ertesites = E_ertesites,
     @Act_E_elorejelzes = E_elorejelzes,
     @Act_PostaiLezaroSzolgalat = PostaiLezaroSzolgalat,
     @Act_Ar = Ar,
     @Act_KimenoKuld_Sorszam  = KimenoKuld_Sorszam ,
     @Act_Ver = Ver,
     @Act_TovabbitasAlattAllapot = TovabbitasAlattAllapot,
     @Act_Azonosito = Azonosito,
     @Act_BoritoTipus = BoritoTipus,
     @Act_MegorzesJelzo = MegorzesJelzo,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id,
     @Act_IktatastNemIgenyel = IktatastNemIgenyel,
     @Act_KezbesitesModja = KezbesitesModja,
     @Act_Munkaallomas = Munkaallomas,
     @Act_SerultKuldemeny = SerultKuldemeny,
     @Act_TevesCimzes = TevesCimzes,
     @Act_TevesErkeztetes = TevesErkeztetes,
     @Act_CimzesTipusa = CimzesTipusa,
     @Act_FutarJegyzekListaSzama = FutarJegyzekListaSzama,
     @Act_Minosito = Minosito,
     @Act_MinositesErvenyessegiIdeje = MinositesErvenyessegiIdeje,
     @Act_TerjedelemMennyiseg = TerjedelemMennyiseg,
     @Act_TerjedelemMennyisegiEgyseg = TerjedelemMennyisegiEgyseg,
     @Act_TerjedelemMegjegyzes = TerjedelemMegjegyzes
from EREC_KuldKuldemenyek
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- z�rol�s ellen�rz�s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/BeerkezesIdeje')=1
         SET @Act_BeerkezesIdeje = @BeerkezesIdeje
   IF @UpdatedColumns.exist('/root/FelbontasDatuma')=1
         SET @Act_FelbontasDatuma = @FelbontasDatuma
   IF @UpdatedColumns.exist('/root/IraIktatokonyv_Id')=1
         SET @Act_IraIktatokonyv_Id = @IraIktatokonyv_Id
   IF @UpdatedColumns.exist('/root/KuldesMod')=1
         SET @Act_KuldesMod = @KuldesMod
   IF @UpdatedColumns.exist('/root/Erkezteto_Szam')=1
         SET @Act_Erkezteto_Szam = @Erkezteto_Szam
   IF @UpdatedColumns.exist('/root/HivatkozasiSzam')=1
         SET @Act_HivatkozasiSzam = @HivatkozasiSzam
   IF @UpdatedColumns.exist('/root/Targy')=1
         SET @Act_Targy = @Targy
   IF @UpdatedColumns.exist('/root/Tartalom')=1
         SET @Act_Tartalom = @Tartalom
   IF @UpdatedColumns.exist('/root/RagSzam')=1
         SET @Act_RagSzam = @RagSzam
   IF @UpdatedColumns.exist('/root/Surgosseg')=1
         SET @Act_Surgosseg = @Surgosseg
   IF @UpdatedColumns.exist('/root/BelyegzoDatuma')=1
         SET @Act_BelyegzoDatuma = @BelyegzoDatuma
   IF @UpdatedColumns.exist('/root/UgyintezesModja')=1
         SET @Act_UgyintezesModja = @UgyintezesModja
   IF @UpdatedColumns.exist('/root/PostazasIranya')=1
         SET @Act_PostazasIranya = @PostazasIranya
   IF @UpdatedColumns.exist('/root/Tovabbito')=1
         SET @Act_Tovabbito = @Tovabbito
   IF @UpdatedColumns.exist('/root/PeldanySzam')=1
         SET @Act_PeldanySzam = @PeldanySzam
   IF @UpdatedColumns.exist('/root/IktatniKell')=1
         SET @Act_IktatniKell = @IktatniKell
   IF @UpdatedColumns.exist('/root/Iktathato')=1
         SET @Act_Iktathato = @Iktathato
   IF @UpdatedColumns.exist('/root/SztornirozasDat')=1
         SET @Act_SztornirozasDat = @SztornirozasDat
   IF @UpdatedColumns.exist('/root/KuldKuldemeny_Id_Szulo')=1
         SET @Act_KuldKuldemeny_Id_Szulo = @KuldKuldemeny_Id_Szulo
   IF @UpdatedColumns.exist('/root/Erkeztetes_Ev')=1
         SET @Act_Erkeztetes_Ev = @Erkeztetes_Ev
   IF @UpdatedColumns.exist('/root/Csoport_Id_Cimzett')=1
         SET @Act_Csoport_Id_Cimzett = @Csoport_Id_Cimzett
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1
         SET @Act_Csoport_Id_Felelos = @Csoport_Id_Felelos
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Expedial')=1
         SET @Act_FelhasznaloCsoport_Id_Expedial = @FelhasznaloCsoport_Id_Expedial
   IF @UpdatedColumns.exist('/root/ExpedialasIdeje')=1
         SET @Act_ExpedialasIdeje = @ExpedialasIdeje
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Orzo')=1
         SET @Act_FelhasznaloCsoport_Id_Orzo = @FelhasznaloCsoport_Id_Orzo
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Atvevo')=1
         SET @Act_FelhasznaloCsoport_Id_Atvevo = @FelhasznaloCsoport_Id_Atvevo
   IF @UpdatedColumns.exist('/root/Partner_Id_Bekuldo')=1
         SET @Act_Partner_Id_Bekuldo = @Partner_Id_Bekuldo
   IF @UpdatedColumns.exist('/root/Cim_Id')=1
         SET @Act_Cim_Id = @Cim_Id
   IF @UpdatedColumns.exist('/root/CimSTR_Bekuldo')=1
         SET @Act_CimSTR_Bekuldo = @CimSTR_Bekuldo
   IF @UpdatedColumns.exist('/root/NevSTR_Bekuldo')=1
         SET @Act_NevSTR_Bekuldo = @NevSTR_Bekuldo
   IF @UpdatedColumns.exist('/root/AdathordozoTipusa')=1
         SET @Act_AdathordozoTipusa = @AdathordozoTipusa
   IF @UpdatedColumns.exist('/root/ElsodlegesAdathordozoTipusa')=1
         SET @Act_ElsodlegesAdathordozoTipusa = @ElsodlegesAdathordozoTipusa
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Alairo')=1
         SET @Act_FelhasznaloCsoport_Id_Alairo = @FelhasznaloCsoport_Id_Alairo
   IF @UpdatedColumns.exist('/root/BarCode')=1
         SET @Act_BarCode = @BarCode
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Bonto')=1
         SET @Act_FelhasznaloCsoport_Id_Bonto = @FelhasznaloCsoport_Id_Bonto
   IF @UpdatedColumns.exist('/root/CsoportFelelosEloszto_Id')=1
         SET @Act_CsoportFelelosEloszto_Id = @CsoportFelelosEloszto_Id
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos_Elozo')=1
         SET @Act_Csoport_Id_Felelos_Elozo = @Csoport_Id_Felelos_Elozo
   IF @UpdatedColumns.exist('/root/Kovetkezo_Felelos_Id')=1
         SET @Act_Kovetkezo_Felelos_Id = @Kovetkezo_Felelos_Id
   IF @UpdatedColumns.exist('/root/Elektronikus_Kezbesitesi_Allap')=1
         SET @Act_Elektronikus_Kezbesitesi_Allap = @Elektronikus_Kezbesitesi_Allap
   IF @UpdatedColumns.exist('/root/Kovetkezo_Orzo_Id')=1
         SET @Act_Kovetkezo_Orzo_Id = @Kovetkezo_Orzo_Id
   IF @UpdatedColumns.exist('/root/Fizikai_Kezbesitesi_Allapot')=1
         SET @Act_Fizikai_Kezbesitesi_Allapot = @Fizikai_Kezbesitesi_Allapot
   IF @UpdatedColumns.exist('/root/IraIratok_Id')=1
         SET @Act_IraIratok_Id = @IraIratok_Id
   IF @UpdatedColumns.exist('/root/BontasiMegjegyzes')=1
         SET @Act_BontasiMegjegyzes = @BontasiMegjegyzes
   IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @Act_Tipus = @Tipus
   IF @UpdatedColumns.exist('/root/Minosites')=1
         SET @Act_Minosites = @Minosites
   IF @UpdatedColumns.exist('/root/MegtagadasIndoka')=1
         SET @Act_MegtagadasIndoka = @MegtagadasIndoka
   IF @UpdatedColumns.exist('/root/Megtagado_Id')=1
         SET @Act_Megtagado_Id = @Megtagado_Id
   IF @UpdatedColumns.exist('/root/MegtagadasDat')=1
         SET @Act_MegtagadasDat = @MegtagadasDat
   IF @UpdatedColumns.exist('/root/KimenoKuldemenyFajta')=1
         SET @Act_KimenoKuldemenyFajta = @KimenoKuldemenyFajta
   IF @UpdatedColumns.exist('/root/Elsobbsegi')=1
         SET @Act_Elsobbsegi = @Elsobbsegi
   IF @UpdatedColumns.exist('/root/Ajanlott')=1
         SET @Act_Ajanlott = @Ajanlott
   IF @UpdatedColumns.exist('/root/Tertiveveny')=1
         SET @Act_Tertiveveny = @Tertiveveny
   IF @UpdatedColumns.exist('/root/SajatKezbe')=1
         SET @Act_SajatKezbe = @SajatKezbe
   IF @UpdatedColumns.exist('/root/E_ertesites')=1
         SET @Act_E_ertesites = @E_ertesites
   IF @UpdatedColumns.exist('/root/E_elorejelzes')=1
         SET @Act_E_elorejelzes = @E_elorejelzes
   IF @UpdatedColumns.exist('/root/PostaiLezaroSzolgalat')=1
         SET @Act_PostaiLezaroSzolgalat = @PostaiLezaroSzolgalat
   IF @UpdatedColumns.exist('/root/Ar')=1
         SET @Act_Ar = @Ar
   IF @UpdatedColumns.exist('/root/KimenoKuld_Sorszam ')=1
         SET @Act_KimenoKuld_Sorszam  = @KimenoKuld_Sorszam 
   IF @UpdatedColumns.exist('/root/TovabbitasAlattAllapot')=1
         SET @Act_TovabbitasAlattAllapot = @TovabbitasAlattAllapot
   IF @UpdatedColumns.exist('/root/Azonosito')=1
         SET @Act_Azonosito = @Azonosito
   IF @UpdatedColumns.exist('/root/BoritoTipus')=1
         SET @Act_BoritoTipus = @BoritoTipus
   IF @UpdatedColumns.exist('/root/MegorzesJelzo')=1
         SET @Act_MegorzesJelzo = @MegorzesJelzo
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id
   IF @UpdatedColumns.exist('/root/IktatastNemIgenyel')=1
         SET @Act_IktatastNemIgenyel = @IktatastNemIgenyel
   IF @UpdatedColumns.exist('/root/KezbesitesModja')=1
         SET @Act_KezbesitesModja = @KezbesitesModja
   IF @UpdatedColumns.exist('/root/Munkaallomas')=1
         SET @Act_Munkaallomas = @Munkaallomas
   IF @UpdatedColumns.exist('/root/SerultKuldemeny')=1
         SET @Act_SerultKuldemeny = @SerultKuldemeny
   IF @UpdatedColumns.exist('/root/TevesCimzes')=1
         SET @Act_TevesCimzes = @TevesCimzes
   IF @UpdatedColumns.exist('/root/TevesErkeztetes')=1
         SET @Act_TevesErkeztetes = @TevesErkeztetes
   IF @UpdatedColumns.exist('/root/CimzesTipusa')=1
         SET @Act_CimzesTipusa = @CimzesTipusa
   IF @UpdatedColumns.exist('/root/FutarJegyzekListaSzama')=1
         SET @Act_FutarJegyzekListaSzama = @FutarJegyzekListaSzama
   IF @UpdatedColumns.exist('/root/Minosito')=1
         SET @Act_Minosito = @Minosito
   IF @UpdatedColumns.exist('/root/MinositesErvenyessegiIdeje')=1
         SET @Act_MinositesErvenyessegiIdeje = @MinositesErvenyessegiIdeje
   IF @UpdatedColumns.exist('/root/TerjedelemMennyiseg')=1
         SET @Act_TerjedelemMennyiseg = @TerjedelemMennyiseg
   IF @UpdatedColumns.exist('/root/TerjedelemMennyisegiEgyseg')=1
         SET @Act_TerjedelemMennyisegiEgyseg = @TerjedelemMennyisegiEgyseg
   IF @UpdatedColumns.exist('/root/TerjedelemMegjegyzes')=1
         SET @Act_TerjedelemMegjegyzes = @TerjedelemMegjegyzes   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_KuldKuldemenyek
SET
     Allapot = @Act_Allapot,
     BeerkezesIdeje = @Act_BeerkezesIdeje,
     FelbontasDatuma = @Act_FelbontasDatuma,
     IraIktatokonyv_Id = @Act_IraIktatokonyv_Id,
     KuldesMod = @Act_KuldesMod,
     Erkezteto_Szam = @Act_Erkezteto_Szam,
     HivatkozasiSzam = @Act_HivatkozasiSzam,
     Targy = @Act_Targy,
     Tartalom = @Act_Tartalom,
     RagSzam = @Act_RagSzam,
     Surgosseg = @Act_Surgosseg,
     BelyegzoDatuma = @Act_BelyegzoDatuma,
     UgyintezesModja = @Act_UgyintezesModja,
     PostazasIranya = @Act_PostazasIranya,
     Tovabbito = @Act_Tovabbito,
     PeldanySzam = @Act_PeldanySzam,
     IktatniKell = @Act_IktatniKell,
     Iktathato = @Act_Iktathato,
     SztornirozasDat = @Act_SztornirozasDat,
     KuldKuldemeny_Id_Szulo = @Act_KuldKuldemeny_Id_Szulo,
     Erkeztetes_Ev = @Act_Erkeztetes_Ev,
     Csoport_Id_Cimzett = @Act_Csoport_Id_Cimzett,
     Csoport_Id_Felelos = @Act_Csoport_Id_Felelos,
     FelhasznaloCsoport_Id_Expedial = @Act_FelhasznaloCsoport_Id_Expedial,
     ExpedialasIdeje = @Act_ExpedialasIdeje,
     FelhasznaloCsoport_Id_Orzo = @Act_FelhasznaloCsoport_Id_Orzo,
     FelhasznaloCsoport_Id_Atvevo = @Act_FelhasznaloCsoport_Id_Atvevo,
     Partner_Id_Bekuldo = @Act_Partner_Id_Bekuldo,
     Cim_Id = @Act_Cim_Id,
     CimSTR_Bekuldo = @Act_CimSTR_Bekuldo,
     NevSTR_Bekuldo = @Act_NevSTR_Bekuldo,
     AdathordozoTipusa = @Act_AdathordozoTipusa,
     ElsodlegesAdathordozoTipusa = @Act_ElsodlegesAdathordozoTipusa,
     FelhasznaloCsoport_Id_Alairo = @Act_FelhasznaloCsoport_Id_Alairo,
     BarCode = @Act_BarCode,
     FelhasznaloCsoport_Id_Bonto = @Act_FelhasznaloCsoport_Id_Bonto,
     CsoportFelelosEloszto_Id = @Act_CsoportFelelosEloszto_Id,
     Csoport_Id_Felelos_Elozo = @Act_Csoport_Id_Felelos_Elozo,
     Kovetkezo_Felelos_Id = @Act_Kovetkezo_Felelos_Id,
     Elektronikus_Kezbesitesi_Allap = @Act_Elektronikus_Kezbesitesi_Allap,
     Kovetkezo_Orzo_Id = @Act_Kovetkezo_Orzo_Id,
     Fizikai_Kezbesitesi_Allapot = @Act_Fizikai_Kezbesitesi_Allapot,
     IraIratok_Id = @Act_IraIratok_Id,
     BontasiMegjegyzes = @Act_BontasiMegjegyzes,
     Tipus = @Act_Tipus,
     Minosites = @Act_Minosites,
     MegtagadasIndoka = @Act_MegtagadasIndoka,
     Megtagado_Id = @Act_Megtagado_Id,
     MegtagadasDat = @Act_MegtagadasDat,
     KimenoKuldemenyFajta = @Act_KimenoKuldemenyFajta,
     Elsobbsegi = @Act_Elsobbsegi,
     Ajanlott = @Act_Ajanlott,
     Tertiveveny = @Act_Tertiveveny,
     SajatKezbe = @Act_SajatKezbe,
     E_ertesites = @Act_E_ertesites,
     E_elorejelzes = @Act_E_elorejelzes,
     PostaiLezaroSzolgalat = @Act_PostaiLezaroSzolgalat,
     Ar = @Act_Ar,
     KimenoKuld_Sorszam  = @Act_KimenoKuld_Sorszam ,
     Ver = @Act_Ver,
     TovabbitasAlattAllapot = @Act_TovabbitasAlattAllapot,
     Azonosito = @Act_Azonosito,
     BoritoTipus = @Act_BoritoTipus,
     MegorzesJelzo = @Act_MegorzesJelzo,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id,
     IktatastNemIgenyel = @Act_IktatastNemIgenyel,
     KezbesitesModja = @Act_KezbesitesModja,
     Munkaallomas = @Act_Munkaallomas,
     SerultKuldemeny = @Act_SerultKuldemeny,
     TevesCimzes = @Act_TevesCimzes,
     TevesErkeztetes = @Act_TevesErkeztetes,
     CimzesTipusa = @Act_CimzesTipusa,
     FutarJegyzekListaSzama = @Act_FutarJegyzekListaSzama,
     Minosito = @Act_Minosito,
     MinositesErvenyessegiIdeje = @Act_MinositesErvenyessegiIdeje,
     TerjedelemMennyiseg = @Act_TerjedelemMennyiseg,
     TerjedelemMennyisegiEgyseg = @Act_TerjedelemMennyisegiEgyseg,
     TerjedelemMegjegyzes = @Act_TerjedelemMegjegyzes
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_KuldKuldemenyek',@Id
					,'EREC_KuldKuldemenyekHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

go

/* haszn�lt errorcode-ok:
	50401:
	50402: 'Ver' oszlop �rt�ke nem egyezik a megl�v� rekord�val
   50403: �rv�nyess�g m�r lej�rt a rekordra
	50499: rekord z�rolva
*/

---------------------

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_LogEREC_KuldKuldemenyekHistory')
            and   type = 'P')
   drop procedure sp_LogEREC_KuldKuldemenyekHistory
go

create procedure sp_LogEREC_KuldKuldemenyekHistory
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_KuldKuldemenyek where Id = @Row_Id;
          declare @IraIktatokonyv_Id_Ver int;
       -- select @IraIktatokonyv_Id_Ver = max(Ver) from EREC_IraIktatoKonyvek where Id in (select IraIktatokonyv_Id from #tempTable);
              declare @KuldKuldemeny_Id_Szulo_Ver int;
       -- select @KuldKuldemeny_Id_Szulo_Ver = max(Ver) from EREC_KuldKuldemenyek where Id in (select KuldKuldemeny_Id_Szulo from #tempTable);
              declare @Csoport_Id_Cimzett_Ver int;
       -- select @Csoport_Id_Cimzett_Ver = max(Ver) from KRT_Csoportok where Id in (select Csoport_Id_Cimzett from #tempTable);
              declare @Csoport_Id_Felelos_Ver int;
       -- select @Csoport_Id_Felelos_Ver = max(Ver) from KRT_Csoportok where Id in (select Csoport_Id_Felelos from #tempTable);
              declare @FelhasznaloCsoport_Id_Orzo_Ver int;
       -- select @FelhasznaloCsoport_Id_Orzo_Ver = max(Ver) from KRT_Csoportok where Id in (select FelhasznaloCsoport_Id_Orzo from #tempTable);
              declare @FelhasznaloCsoport_Id_Atvevo_Ver int;
       -- select @FelhasznaloCsoport_Id_Atvevo_Ver = max(Ver) from KRT_Csoportok where Id in (select FelhasznaloCsoport_Id_Atvevo from #tempTable);
              declare @Partner_Id_Bekuldo_Ver int;
       -- select @Partner_Id_Bekuldo_Ver = max(Ver) from KRT_Partnerek where Id in (select Partner_Id_Bekuldo from #tempTable);
              declare @Cim_Id_Ver int;
       -- select @Cim_Id_Ver = max(Ver) from KRT_Cimek where Id in (select Cim_Id from #tempTable);
              declare @FelhasznaloCsoport_Id_Alairo_Ver int;
       -- select @FelhasznaloCsoport_Id_Alairo_Ver = max(Ver) from KRT_Csoportok where Id in (select FelhasznaloCsoport_Id_Alairo from #tempTable);
              declare @FelhasznaloCsoport_Id_Bonto_Ver int;
       -- select @FelhasznaloCsoport_Id_Bonto_Ver = max(Ver) from KRT_Csoportok where Id in (select FelhasznaloCsoport_Id_Bonto from #tempTable);
              declare @CsoportFelelosEloszto_Id_Ver int;
       -- select @CsoportFelelosEloszto_Id_Ver = max(Ver) from KRT_Csoportok where Id in (select CsoportFelelosEloszto_Id from #tempTable);
              declare @Csoport_Id_Felelos_Elozo_Ver int;
       -- select @Csoport_Id_Felelos_Elozo_Ver = max(Ver) from KRT_Csoportok where Id in (select Csoport_Id_Felelos_Elozo from #tempTable);
          
   insert into EREC_KuldKuldemenyekHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Allapot     
 ,BeerkezesIdeje     
 ,FelbontasDatuma     
 ,IraIktatokonyv_Id       
 ,IraIktatokonyv_Id_Ver     
 ,KuldesMod     
 ,Erkezteto_Szam     
 ,HivatkozasiSzam     
 ,Targy     
 ,Tartalom     
 ,RagSzam     
 ,Surgosseg     
 ,BelyegzoDatuma     
 ,UgyintezesModja     
 ,PostazasIranya     
 ,Tovabbito     
 ,PeldanySzam     
 ,IktatniKell     
 ,Iktathato     
 ,SztornirozasDat     
 ,KuldKuldemeny_Id_Szulo       
 ,KuldKuldemeny_Id_Szulo_Ver     
 ,Erkeztetes_Ev     
 ,Csoport_Id_Cimzett       
 ,Csoport_Id_Cimzett_Ver     
 ,Csoport_Id_Felelos       
 ,Csoport_Id_Felelos_Ver     
 ,FelhasznaloCsoport_Id_Expedial     
 ,ExpedialasIdeje     
 ,FelhasznaloCsoport_Id_Orzo       
 ,FelhasznaloCsoport_Id_Orzo_Ver     
 ,FelhasznaloCsoport_Id_Atvevo       
 ,FelhasznaloCsoport_Id_Atvevo_Ver     
 ,Partner_Id_Bekuldo       
 ,Partner_Id_Bekuldo_Ver     
 ,Cim_Id       
 ,Cim_Id_Ver     
 ,CimSTR_Bekuldo     
 ,NevSTR_Bekuldo     
 ,AdathordozoTipusa     
 ,ElsodlegesAdathordozoTipusa     
 ,FelhasznaloCsoport_Id_Alairo       
 ,FelhasznaloCsoport_Id_Alairo_Ver     
 ,BarCode     
 ,FelhasznaloCsoport_Id_Bonto       
 ,FelhasznaloCsoport_Id_Bonto_Ver     
 ,CsoportFelelosEloszto_Id       
 ,CsoportFelelosEloszto_Id_Ver     
 ,Csoport_Id_Felelos_Elozo       
 ,Csoport_Id_Felelos_Elozo_Ver     
 ,Kovetkezo_Felelos_Id     
 ,Elektronikus_Kezbesitesi_Allap     
 ,Kovetkezo_Orzo_Id     
 ,Fizikai_Kezbesitesi_Allapot     
 ,IraIratok_Id     
 ,BontasiMegjegyzes     
 ,Tipus     
 ,Minosites     
 ,MegtagadasIndoka     
 ,Megtagado_Id     
 ,MegtagadasDat     
 ,KimenoKuldemenyFajta     
 ,Elsobbsegi     
 ,Ajanlott     
 ,Tertiveveny     
 ,SajatKezbe     
 ,E_ertesites     
 ,E_elorejelzes     
 ,PostaiLezaroSzolgalat     
 ,Ar     
 ,KimenoKuld_Sorszam      
 ,Ver     
 ,TovabbitasAlattAllapot     
 ,Azonosito     
 ,BoritoTipus     
 ,MegorzesJelzo     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id     
 ,IktatastNemIgenyel     
 ,KezbesitesModja     
 ,Munkaallomas     
 ,SerultKuldemeny     
 ,TevesCimzes     
 ,TevesErkeztetes     
 ,CimzesTipusa     
 ,FutarJegyzekListaSzama     
 ,Minosito     
 ,MinositesErvenyessegiIdeje     
 ,TerjedelemMennyiseg     
 ,TerjedelemMennyisegiEgyseg     
 ,TerjedelemMegjegyzes   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Allapot          
 ,BeerkezesIdeje          
 ,FelbontasDatuma          
 ,IraIktatokonyv_Id            
 ,@IraIktatokonyv_Id_Ver     
 ,KuldesMod          
 ,Erkezteto_Szam          
 ,HivatkozasiSzam          
 ,Targy          
 ,Tartalom          
 ,RagSzam          
 ,Surgosseg          
 ,BelyegzoDatuma          
 ,UgyintezesModja          
 ,PostazasIranya          
 ,Tovabbito          
 ,PeldanySzam          
 ,IktatniKell          
 ,Iktathato          
 ,SztornirozasDat          
 ,KuldKuldemeny_Id_Szulo            
 ,@KuldKuldemeny_Id_Szulo_Ver     
 ,Erkeztetes_Ev          
 ,Csoport_Id_Cimzett            
 ,@Csoport_Id_Cimzett_Ver     
 ,Csoport_Id_Felelos            
 ,@Csoport_Id_Felelos_Ver     
 ,FelhasznaloCsoport_Id_Expedial          
 ,ExpedialasIdeje          
 ,FelhasznaloCsoport_Id_Orzo            
 ,@FelhasznaloCsoport_Id_Orzo_Ver     
 ,FelhasznaloCsoport_Id_Atvevo            
 ,@FelhasznaloCsoport_Id_Atvevo_Ver     
 ,Partner_Id_Bekuldo            
 ,@Partner_Id_Bekuldo_Ver     
 ,Cim_Id            
 ,@Cim_Id_Ver     
 ,CimSTR_Bekuldo          
 ,NevSTR_Bekuldo          
 ,AdathordozoTipusa          
 ,ElsodlegesAdathordozoTipusa          
 ,FelhasznaloCsoport_Id_Alairo            
 ,@FelhasznaloCsoport_Id_Alairo_Ver     
 ,BarCode          
 ,FelhasznaloCsoport_Id_Bonto            
 ,@FelhasznaloCsoport_Id_Bonto_Ver     
 ,CsoportFelelosEloszto_Id            
 ,@CsoportFelelosEloszto_Id_Ver     
 ,Csoport_Id_Felelos_Elozo            
 ,@Csoport_Id_Felelos_Elozo_Ver     
 ,Kovetkezo_Felelos_Id          
 ,Elektronikus_Kezbesitesi_Allap          
 ,Kovetkezo_Orzo_Id          
 ,Fizikai_Kezbesitesi_Allapot          
 ,IraIratok_Id          
 ,BontasiMegjegyzes          
 ,Tipus          
 ,Minosites          
 ,MegtagadasIndoka          
 ,Megtagado_Id          
 ,MegtagadasDat          
 ,KimenoKuldemenyFajta          
 ,Elsobbsegi          
 ,Ajanlott          
 ,Tertiveveny          
 ,SajatKezbe          
 ,E_ertesites          
 ,E_elorejelzes          
 ,PostaiLezaroSzolgalat          
 ,Ar          
 ,KimenoKuld_Sorszam           
 ,Ver          
 ,TovabbitasAlattAllapot          
 ,Azonosito          
 ,BoritoTipus          
 ,MegorzesJelzo          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id          
 ,IktatastNemIgenyel          
 ,KezbesitesModja          
 ,Munkaallomas          
 ,SerultKuldemeny          
 ,TevesCimzes          
 ,TevesErkeztetes          
 ,CimzesTipusa          
 ,FutarJegyzekListaSzama          
 ,Minosito          
 ,MinositesErvenyessegiIdeje          
 ,TerjedelemMennyiseg          
 ,TerjedelemMennyisegiEgyseg          
 ,TerjedelemMegjegyzes        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go
---------------------
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--FF, 20070926
IF  EXISTS (SELECT * FROM sys.Objects WHERE object_id = OBJECT_ID(N'dbo.CK_EREC_KuldKuldemenyek') AND type = N'C')
  ALTER TABLE EREC_KuldKuldemenyek DROP CONSTRAINT CK_EREC_KuldKuldemenyek  
GO
--FF

IF  EXISTS (SELECT * FROM sys.Objects WHERE object_id = OBJECT_ID(N'dbo.fn_EREC_KuldKuldemenyekCheckUK') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION dbo.fn_EREC_KuldKuldemenyekCheckUK
GO

CREATE FUNCTION dbo.fn_EREC_KuldKuldemenyekCheckUK (
             @IraIktatokonyv_Id uniqueidentifier ,
             @Erkezteto_Szam int ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_KuldKuldemenyek where
         (IraIktatokonyv_Id = @IraIktatokonyv_Id or (IraIktatokonyv_Id is null and @IraIktatokonyv_Id is null)) and            
         (Erkezteto_Szam = @Erkezteto_Szam or (Erkezteto_Szam is null and @Erkezteto_Szam is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END

GO