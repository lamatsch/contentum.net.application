PRINT 'TUK - KULDEMENY_KULDES_MODJA'
IF EXISTS (SELECT 1 FROM KRT_PARAMETEREK WHERE NEV= 'TUK' AND Ertek = '1')
BEGIN
	BEGIN TRAN
		PRINT 'TUK - KULDEMENY_KULDES_MODJA - START'

		DECLARE @Tanz_Id uniqueidentifier	= 'f60ad910-541a-470d-b888-da5a6a0616c8'
		DECLARE @Stat_id uniqueidentifier	= 'a0848405-e664-4e79-8fab-cfeca4a290cf'
		DECLARE @Org uniqueidentifier		= '450b510a-7caa-46b0-83e3-18445c0c53a9'
		DECLARE @KodCsoport_Id uniqueidentifier = '6982F668-C8CD-47A3-AC62-9F16E439473C'

		DECLARE @ID1 uniqueidentifier 
		DECLARE @ID2 uniqueidentifier 
		DECLARE @ID3 uniqueidentifier 
		SET @ID1 = 'BBEBFA16-4C37-4E75-87A7-598E84B26E01'
		SET @ID2 = 'BBEBFA16-4C37-4E75-87A7-598E84B26E02'
		SET @ID3 = 'BBEBFA16-4C37-4E75-87A7-598E84B26E03'

		IF NOT EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID= @ID1 )
			DELETE FROM KRT_Kodtarak WHERE Kodcsoport_id = @KodCsoport_Id
		
		IF NOT EXISTS (SELECT 1 FROM KRT_KODTARAK WHERE ID = @ID1)
			INSERT INTO KRT_KodTarak(
					KodCsoport_Id,Tranz_id,Stat_id,Org,Id,Kod,Nev,Sorrend,Modosithato,Egyeb) 
					VALUES (
					@KodCsoport_Id
					,@Tanz_Id
					,@Stat_id
					,@Org
					,@ID1
					,'01'
					,'Postai úton'
					,'01'
					,'0'
					,'TUK - FIX'
					); 

	
		IF NOT EXISTS (SELECT 1 FROM KRT_KODTARAK WHERE ID = @ID2)
			INSERT INTO KRT_KodTarak(
					KodCsoport_Id,Tranz_id,Stat_id,Org,Id,Kod,Nev,Sorrend,Modosithato,Egyeb) 
					VALUES (
					@KodCsoport_Id
					,@Tanz_Id
					,@Stat_id
					,@Org
					,@ID2
					,'98'
					,'Személyes kézbesítés'
					,'02'
					,'0'
					,'TUK - FIX'
					); 

		IF NOT EXISTS (SELECT 1 FROM KRT_KODTARAK WHERE ID = @ID3)
			INSERT INTO KRT_KodTarak(
					KodCsoport_Id,Tranz_id,Stat_id,Org,Id,Kod,Nev,Sorrend,Modosithato,Egyeb) 
					VALUES (
					@KodCsoport_Id
					,@Tanz_Id
					,@Stat_id
					,@Org
					,@ID3
					,'99'
					,'Futárszolgálat'
					,'03'
					,'0'
					,'TUK - FIX'
					); 
	COMMIT TRAN
END
GO