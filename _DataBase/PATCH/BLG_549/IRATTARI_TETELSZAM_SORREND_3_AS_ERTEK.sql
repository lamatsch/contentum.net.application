﻿-- IRATTARI_TETELSZAM_SORREND rendszerparameterhez új (3-as) érték beállítása
-- FPH = BOPMH = nem változik, NMHH = 3

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='EEE526EF-5F6C-4BBF-A706-E49DC116217E') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'ERROR - Nincs IRATTARI_TETELSZAM_SORREND paraméter vagy nem ez az id-ja' 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
		Note='A paraméter értékek lehetnek: 0 - ágazati jel / tételszám / megőrzési idő; 1 - tételszám / ágazati jel / megőrzési idő; 2 - tételszám / megőrzési idő (I.); 3 -  tételszám / megőrzési idő (II.) '
	 WHERE Id=@record_id 
 END

 if @org_kod = 'NMHH' 
BEGIN 
	Print @org_kod +': IRATTARI_TETELSZAM_SORREND = 3'
	UPDATE KRT_Parameterek
	   SET Ertek='3'
	   WHERE Id='EEE526EF-5F6C-4BBF-A706-E49DC116217E'
END