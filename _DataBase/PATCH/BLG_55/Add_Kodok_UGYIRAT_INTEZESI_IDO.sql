﻿print 'Add_Kodok_UGYIRAT_INTEZESI_IDO.sql'

DECLARE @Tanz_Id uniqueidentifier = 'f60ad910-541a-470d-b888-da5a6a061668'
declare @Stat_id uniqueidentifier = 'a0848405-e664-4e79-8fab-cfeca4a290af'
declare @Org uniqueidentifier='450b510a-7caa-46b0-83e3-18445c0c53a9'

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_KodCsoportok
			where Id='992520A2-5892-413A-A374-8515E4781307') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
		insert into KRT_KodCsoportok(
		Tranz_id
		,Stat_id
		,KodTarak_Id_KodcsoportTipus
		,BesorolasiSema
		,Id
		,Kod
		,Nev
		,Modosithato
		) values (
		@Tanz_Id
		,@Stat_id
		,'a54af681-ada2-420f-860f-826257e95932'
		,'0'
		,'992520A2-5892-413A-A374-8515E4781307'
		,'UGYIRAT_INTEZESI_IDO'
		,'Ügyirat lehetséges intézési idejei (napok vagy munkanapok száma)'
		,'1'
		); 
		 
			 
	END 
	ELSE 
	BEGIN 
		Print 'UPDATE'
		UPDATE KRT_KodCsoportok
		SET 
		Tranz_id=@Tanz_Id
		,Stat_id=@Stat_id
		,KodTarak_Id_KodcsoportTipus='a54af681-ada2-420f-860f-826257e95932'
		,BesorolasiSema='0'
		,Id=@record_id
		,Kod='UGYIRAT_INTEZESI_IDO'
		,Nev='Ügyirat lehetséges intézési idejei (napok vagy munkanapok száma)'
		,Modosithato='1'
		WHERE Id=@record_id 
		 
			 
	END

-- új kódok
declare @KodCsoport_Id uniqueidentifier = '992520A2-5892-413A-A374-8515E4781307'

-- 5
SET @record_id = (select Id from KRT_KodTarak where Id='ce773480-b750-4c83-9f44-e347efa59cef') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'ce773480-b750-4c83-9f44-e347efa59cef'
    ,'5'
    ,'5'
    ,'05'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='5'
    ,Nev='5'
    ,Sorrend='05'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- 10
SET @record_id = (select Id from KRT_KodTarak where Id='57d26675-9a4e-4c31-ba9d-21ee84781fb8') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'57d26675-9a4e-4c31-ba9d-21ee84781fb8'
    ,'10'
    ,'10'
    ,'10'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='10'
    ,Nev='10'
    ,Sorrend='10'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- 22
SET @record_id = (select Id from KRT_KodTarak where Id='1bbee23a-a10e-4d84-87da-6972757fe005') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'1bbee23a-a10e-4d84-87da-6972757fe005'
    ,'22'
    ,'22'
    ,'22'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='22'
    ,Nev='22'
    ,Sorrend='22'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END











