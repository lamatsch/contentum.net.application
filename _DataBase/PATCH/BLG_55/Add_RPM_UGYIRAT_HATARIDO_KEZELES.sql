﻿-- UGYIRAT_HATARIDO_KEZELES rendszerparameter
print '''UGYIRAT_HATARIDO_KEZELES'' rendszerparameter hozzaadasa'

declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9' 
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id=@org)

print 'Org = ' + @org_kod

declare @tranz_id uniqueidentifier = newid()

declare @record_id uniqueidentifier = 'C662BF4F-DB33-4094-938D-509B90F0A24A'

declare @Ertek nvarchar(400)

set @Ertek = (case @org_kod when 'NMHH' then '1' else '0' end)

print '@record_id = ' + cast(@record_id as nvarchar(36))

print '@Ertek = ' + @Ertek

declare @Nev nvarchar(400) = 'UGYIRAT_HATARIDO_KEZELES'

declare @Note nvarchar(4000) = 'Ügyirat határidő számítási módja. Értékek: 0 - mostani, 1 - NMHH.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT: ' + @Nev 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE: ' + @Nev
	 UPDATE KRT_Parameterek
	 SET 
	     Tranz_id=@tranz_id
		,Org=@org
		,Id= @record_id
		,Nev = @Nev
		,Ertek= @Ertek
		,Karbantarthato='1'
		,Note = @Note
	 WHERE Id=@record_id 
 END