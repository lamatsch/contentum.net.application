print 'AddColumns_EREC_UgyUgyiratokHistory.sql'

IF NOT EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'EREC_UgyUgyiratokHistory' AND COLUMN_NAME = 'UgyintezesKezdete') 
begin
	print 'Add EREC_UgyUgyiratokHistory.UgyintezesKezdete'
	alter table EREC_UgyUgyiratokHistory 
	add UgyintezesKezdete datetime null 
end

IF NOT EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'EREC_UgyUgyiratokHistory' AND COLUMN_NAME = 'FelfuggesztettNapokSzama') 
begin
	print 'Add EREC_UgyUgyiratokHistory.FelfuggesztettNapokSzama'
	alter table EREC_UgyUgyiratokHistory 
	add FelfuggesztettNapokSzama int null
end

IF NOT EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'EREC_UgyUgyiratokHistory' AND COLUMN_NAME = 'IntezesiIdo') 
begin
	print 'Add EREC_UgyUgyiratokHistory.IntezesiIdo'
	alter table EREC_UgyUgyiratokHistory 
	add IntezesiIdo int null
end

IF NOT EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'EREC_UgyUgyiratokHistory' AND COLUMN_NAME = 'IntezesiIdoegyseg') 
begin
	print 'Add EREC_UgyUgyiratokHistory.IntezesiIdoegyseg'
	alter table EREC_UgyUgyiratokHistory 
	add IntezesiIdoegyseg nvarchar(64) collate Hungarian_CS_AS null
end