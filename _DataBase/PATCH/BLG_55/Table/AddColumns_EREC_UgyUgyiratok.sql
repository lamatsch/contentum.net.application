print 'AddColumns_EREC_UgyUgyiratok.sql'

IF NOT EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'EREC_UgyUgyiratok' AND COLUMN_NAME = 'UgyintezesKezdete') 
begin
	print 'Add EREC_UgyUgyiratok.UgyintezesKezdete'
	alter table EREC_UgyUgyiratok 
	add UgyintezesKezdete datetime null 
end

IF NOT EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'EREC_UgyUgyiratok' AND COLUMN_NAME = 'FelfuggesztettNapokSzama') 
begin
	print 'Add EREC_UgyUgyiratok.FelfuggesztettNapokSzama'
	alter table EREC_UgyUgyiratok 
	add FelfuggesztettNapokSzama int null
end

IF NOT EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'EREC_UgyUgyiratok' AND COLUMN_NAME = 'IntezesiIdo') 
begin
	print 'Add EREC_UgyUgyiratok.IntezesiIdo'
	alter table EREC_UgyUgyiratok 
	add IntezesiIdo int null
end

IF NOT EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'EREC_UgyUgyiratok' AND COLUMN_NAME = 'IntezesiIdoegyseg') 
begin
	print 'Add EREC_UgyUgyiratok.IntezesiIdoegyseg'
	alter table EREC_UgyUgyiratok 
	add IntezesiIdoegyseg nvarchar(64) collate Hungarian_CS_AS null
end