BEGIN TRANSACTION AddMenuItem;  
GO  
		-----------------------------------
		--MODULOK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Org uniqueidentifier=	null
		DECLARE @Tipus nvarchar(10)=	null
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @SelectString nvarchar(4000)=	null
		DECLARE @Csoport_Id_Tulaj uniqueidentifier=	null
		DECLARE @Parameterek nvarchar(4000)=	null
		DECLARE @ObjTip_Id_Adatelem uniqueidentifier=	null
		DECLARE @ObjTip_Adatelem nvarchar(100)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		SET @Id			='F434DF15-36CD-4D44-AAC4-32A2D5CE9E41'
		SET @Kod		='PldIratPeldanyokKimenoList.aspx'
		SET @Nev		='Kimen� iratp�ld�nyok list�ja'
		SET @Tipus		='F'

		EXECUTE @RC = [dbo].[sp_KRT_ModulokInsert] 
		   @Id
		  ,@Org
		  ,@Tipus
		  ,@Kod
		  ,@Nev
		  ,@Leiras
		  ,@SelectString
		  ,@Csoport_Id_Tulaj
		  ,@Parameterek
		  ,@ObjTip_Id_Adatelem
		  ,@ObjTip_Adatelem
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--MODULOK END
		-----------------------------------
		--MENUK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Org uniqueidentifier=	null
		DECLARE @Menu_Id_Szulo uniqueidentifier=	null
		DECLARE @Kod nvarchar(20)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Funkcio_Id uniqueidentifier=	null
		DECLARE @Modul_Id uniqueidentifier=	null
		DECLARE @Parameter nvarchar(400)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ImageURL nvarchar(400)=	null
		DECLARE @Sorrend nvarchar(100)=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @id				= 'FA3BCD75-82AF-428E-AF57-E95D082A02A5'
		set @Menu_Id_Szulo	= 'CCDA184D-7FB8-4BDE-88DB-CD0BCCE6CED2'
		set @Kod			= 'eRecord'
		set @Nev			= 'Kimen� iratp�ld�nyok list�ja'
		set @Funkcio_Id		= '05228912-CC61-4F2B-8E1C-C66448BF4C99' 
		set @Modul_Id		= 'F434DF15-36CD-4D44-AAC4-32A2D5CE9E41'
		set @Sorrend		= 12

		EXECUTE @RC = [dbo].[sp_KRT_MenukInsert] 
		   @Id
		  ,@Org
		  ,@Menu_Id_Szulo
		  ,@Kod
		  ,@Nev
		  ,@Funkcio_Id
		  ,@Modul_Id
		  ,@Parameter
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ImageURL
		  ,@Sorrend
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--MENUK END

GO  
COMMIT TRANSACTION AddMenuItem;  
GO 

