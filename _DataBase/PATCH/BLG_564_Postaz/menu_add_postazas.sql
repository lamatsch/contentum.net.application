BEGIN TRANSACTION AddMenuItem;  
GO  

		-----------------------------------
		--MENUK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Org uniqueidentifier=	null
		DECLARE @Menu_Id_Szulo uniqueidentifier=	null
		DECLARE @Kod nvarchar(20)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Funkcio_Id uniqueidentifier=	null
		DECLARE @Modul_Id uniqueidentifier=	null
		DECLARE @Parameter nvarchar(400)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ImageURL nvarchar(400)=	null
		DECLARE @Sorrend nvarchar(100)=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @id				= 'CCDA184D-7FB8-4BDE-88DB-CD0BCCE6CED2'
		set @Menu_Id_Szulo	=  NULL
		set @Kod			= 'eRecord'
		set @Nev			= 'Postázás'
		set @Funkcio_Id		=  NULL
		set @Modul_Id		=  NULL
		set @Sorrend		=  31
		SET @ImageURL       = 'images/hu/menuelemek/postazasikon.jpg'

		EXECUTE @RC = [dbo].[sp_KRT_MenukInsert] 
		   @Id
		  ,@Org
		  ,@Menu_Id_Szulo
		  ,@Kod
		  ,@Nev
		  ,@Funkcio_Id
		  ,@Modul_Id
		  ,@Parameter
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ImageURL
		  ,@Sorrend
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--MENUK END

		update krt_menuk
		set Menu_Id_Szulo = 'CCDA184D-7FB8-4BDE-88DB-CD0BCCE6CED2'
		where id = '71BEB165-7F61-461D-9DA9-194E6776586E'

		update krt_menuk
		set Menu_Id_Szulo = 'CCDA184D-7FB8-4BDE-88DB-CD0BCCE6CED2'
		where id = '72C12527-FD93-4086-B567-9DFF00CB1327'
		
		update krt_menuk
		set Menu_Id_Szulo = 'CCDA184D-7FB8-4BDE-88DB-CD0BCCE6CED2'
		where id = 'C0D13F4D-276F-41C5-8B3D-3F44D71E528E'

		update krt_menuk
		set Menu_Id_Szulo = 'CCDA184D-7FB8-4BDE-88DB-CD0BCCE6CED2'
		where id = 'BEAEB104-4444-5555-BB9B-534345734454'

		update krt_menuk
		set Menu_Id_Szulo = 'CCDA184D-7FB8-4BDE-88DB-CD0BCCE6CED2'
		where id = '2BCAA245-4BA8-450D-B323-C87FB91ED9E7'
		
GO  
COMMIT TRANSACTION AddMenuItem;  
GO 

