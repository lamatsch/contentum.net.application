declare @id uniqueidentifier
set @id = '4C12A702-7AF9-4150-A46B-724326EB26D9'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	print 'insert'

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/KuldKuldemenyFormTab.ascx',
		'labelBeerkezesModja',
		'Be�rkez�s m�dja:'
	)
END

GO

declare @id uniqueidentifier
set @id = 'B833F8B3-7738-43B5-80C6-8CF0D20CD514'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	print 'insert'

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'EgyszerusitettIktatasForm.aspx',
		'labelBeerkezesModja',
		'Be�rkez�s m�dja:'
	)
END

GO
GO