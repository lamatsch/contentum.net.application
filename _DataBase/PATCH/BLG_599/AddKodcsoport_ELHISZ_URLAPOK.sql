Print 'ELHISZ_URLAPOK kódcsoport'

declare @Org uniqueidentifier
set @Org = '450b510a-7caa-46b0-83e3-18445c0c53a9'

declare @Tanz_Id uniqueidentifier
set @Tanz_Id = 'B93532A5-05FF-4D3E-91B1-4CF9284E6A75'

declare @Stat_Id uniqueidentifier
set @Stat_Id = 'a0848405-e664-4e79-8fab-cfeca4a290af'

DECLARE @record_id uniqueidentifier 
DECLARE @KodCsoport_Id uniqueidentifier
set @KodCsoport_Id = 'DEC5EEBE-C22D-4400-BBC1-DE72F4ED208E'

SET @record_id = (select Id from KRT_KodCsoportok where Id=@KodCsoport_Id) 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
	insert into KRT_KodCsoportok(
	 BesorolasiSema
	,Id
	,Kod
	,Nev
	,Modosithato
	,Note
	,Tranz_id
	, Stat_id
	) values (
	'0'
	,@KodCsoport_Id
	,'ELHISZ_URLAPOK '
	,'ELHISZ űrlapok'
	,'1'
	,'A letöltésre kerülő űtlapok kódjai.'
	, @Tanz_Id
	, @Stat_Id
	); 

END 
ELSE 
BEGIN 
	Print 'UPDATE'
	UPDATE KRT_KodCsoportok
	SET 
	BesorolasiSema='0'
	,Kod='ELHISZ_URLAPOK '
	,Nev='ELHISZ űrlapok'
	,Modosithato='1'	
	,Note='A letöltésre kerülő űtlapok kódjai.'
	,Tranz_id = @Tanz_Id
	,Stat_id = @Stat_Id
	WHERE Id=@KodCsoport_Id
	
END



-- esf00101
SET @record_id = (select Id from KRT_KodTarak where Id='2b9d76ae-420d-48da-b5e5-2da6f9b3dcd1') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'2b9d76ae-420d-48da-b5e5-2da6f9b3dcd1'
    ,'esf00101'
    ,'Kéretlen hirdetés bejelentő űrlap'
    ,'001'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00101'
    ,Nev='Kéretlen hirdetés bejelentő űrlap'
    ,Sorrend='001'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00102
SET @record_id = (select Id from KRT_KodTarak where Id='d197ea93-41e9-4a22-b455-32dcbb0d0ab4') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'d197ea93-41e9-4a22-b455-32dcbb0d0ab4'
    ,'esf00102'
    ,'Rádiófrekvenciás zavar bejelentése'
    ,'002'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00102'
    ,Nev='Rádiófrekvenciás zavar bejelentése'
    ,Sorrend='002'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00103
SET @record_id = (select Id from KRT_KodTarak where Id='b201c7a6-e532-4690-98e5-2ef1a5bdded8') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'b201c7a6-e532-4690-98e5-2ef1a5bdded8'
    ,'esf00103'
    ,'Piacfelügyeleti eljárás kezdeményezése'
    ,'003'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00103'
    ,Nev='Piacfelügyeleti eljárás kezdeményezése'
    ,Sorrend='003'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00104
SET @record_id = (select Id from KRT_KodTarak where Id='7c216fdf-73f2-4e30-ada3-deacca77c98a') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'7c216fdf-73f2-4e30-ada3-deacca77c98a'
    ,'esf00104'
    ,'Alkalmi rendezvényekhez kapcsolódó kérelem'
    ,'004'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00104'
    ,Nev='Alkalmi rendezvényekhez kapcsolódó kérelem'
    ,Sorrend='004'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00105
SET @record_id = (select Id from KRT_KodTarak where Id='0f59e0b3-a915-470a-ab9c-f03dff3db39e') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'0f59e0b3-a915-470a-ab9c-f03dff3db39e'
    ,'esf00105'
    ,'Alkalmi SNG rádióengedély-kérelem'
    ,'005'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00105'
    ,Nev='Alkalmi SNG rádióengedély-kérelem'
    ,Sorrend='005'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00106
SET @record_id = (select Id from KRT_KodTarak where Id='c3aab80a-afb5-4c76-9696-efe430346d44') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'c3aab80a-afb5-4c76-9696-efe430346d44'
    ,'esf00106'
    ,'Engedély-meghosszabbítási kérelem'
    ,'006'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00106'
    ,Nev='Engedély-meghosszabbítási kérelem'
    ,Sorrend='006'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00107
SET @record_id = (select Id from KRT_KodTarak where Id='f3594800-8b24-4343-bde4-263b7cc0ecc8') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'f3594800-8b24-4343-bde4-263b7cc0ecc8'
    ,'esf00107'
    ,'Panasz a Média- és Hírközlési Biztoshoz'
    ,'007'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00107'
    ,Nev='Panasz a Média- és Hírközlési Biztoshoz'
    ,Sorrend='007'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00108
SET @record_id = (select Id from KRT_KodTarak where Id='1702eb1e-8f04-4cfb-bf2a-22ff376dd237') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'1702eb1e-8f04-4cfb-bf2a-22ff376dd237'
    ,'esf00108'
    ,'Rádióamatőr-engedély kérelem'
    ,'008'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00108'
    ,Nev='Rádióamatőr-engedély kérelem'
    ,Sorrend='008'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00109
SET @record_id = (select Id from KRT_KodTarak where Id='28064091-68f2-45ef-a1b9-42ecbe4ae177') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'28064091-68f2-45ef-a1b9-42ecbe4ae177'
    ,'esf00109'
    ,'Jelentkezés rádióamatőr kezelői vizsgára'
    ,'009'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00109'
    ,Nev='Jelentkezés rádióamatőr kezelői vizsgára'
    ,Sorrend='009'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00110
SET @record_id = (select Id from KRT_KodTarak where Id='abc1585e-64a9-444e-9fbb-99a7ee46d7d7') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'abc1585e-64a9-444e-9fbb-99a7ee46d7d7'
    ,'esf00110'
    ,'Rádióberendezés bejelentése'
    ,'010'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00110'
    ,Nev='Rádióberendezés bejelentése'
    ,Sorrend='010'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00111xx
SET @record_id = (select Id from KRT_KodTarak where Id='d502fc66-afbd-42a3-a1d9-2413688b9908') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'d502fc66-afbd-42a3-a1d9-2413688b9908'
    ,'esf00111xx'
    ,'esf00111xx'
    ,'011'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00111xx'
    ,Nev='esf00111xx'
    ,Sorrend='011'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00112
SET @record_id = (select Id from KRT_KodTarak where Id='49900a8f-3488-4253-9c24-46adf4e35e60') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'49900a8f-3488-4253-9c24-46adf4e35e60'
    ,'esf00112'
    ,'Piacfelügyeleti eljárás kezdeményezése'
    ,'012'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00112'
    ,Nev='Piacfelügyeleti eljárás kezdeményezése'
    ,Sorrend='012'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00114
SET @record_id = (select Id from KRT_KodTarak where Id='be814be2-efbd-43bf-a3d2-5178fd1b050d') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'be814be2-efbd-43bf-a3d2-5178fd1b050d'
    ,'esf00114'
    ,'Tájékoztatás kérése a szolgáltatóval kapcsolatos ügyintézéshez panasz esetén'
    ,'013'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00114'
    ,Nev='Tájékoztatás kérése a szolgáltatóval kapcsolatos ügyintézéshez panasz esetén'
    ,Sorrend='013'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00117
SET @record_id = (select Id from KRT_KodTarak where Id='c33b52b2-98c9-41f3-8709-e1b8550dc3db') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'c33b52b2-98c9-41f3-8709-e1b8550dc3db'
    ,'esf00117'
    ,'Sajtótermék nyilvántartásba vétele'
    ,'014'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00117'
    ,Nev='Sajtótermék nyilvántartásba vétele'
    ,Sorrend='014'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00118
SET @record_id = (select Id from KRT_KodTarak where Id='687ef5a5-b27a-4c7f-8ed7-b0dc4355b520') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'687ef5a5-b27a-4c7f-8ed7-b0dc4355b520'
    ,'esf00118'
    ,'Sajtótermék nyilvántartásba vétele'
    ,'015'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00118'
    ,Nev='Sajtótermék nyilvántartásba vétele'
    ,Sorrend='015'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00119
SET @record_id = (select Id from KRT_KodTarak where Id='741cab91-5d09-4b01-8d85-4d10aaeb8912') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'741cab91-5d09-4b01-8d85-4d10aaeb8912'
    ,'esf00119'
    ,'Kiegyensúlyozottsági kérelem'
    ,'016'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00119'
    ,Nev='Kiegyensúlyozottsági kérelem'
    ,Sorrend='016'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00120
SET @record_id = (select Id from KRT_KodTarak where Id='6797ef26-6e63-4a39-9c1a-662d3069dae3') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'6797ef26-6e63-4a39-9c1a-662d3069dae3'
    ,'esf00120'
    ,'Hotline bejelentés kezdeményezése'
    ,'017'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00120'
    ,Nev='Hotline bejelentés kezdeményezése'
    ,Sorrend='017'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00122
SET @record_id = (select Id from KRT_KodTarak where Id='53925ede-1781-4b87-a822-f32ec947427f') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'53925ede-1781-4b87-a822-f32ec947427f'
    ,'esf00122'
    ,'Közös eszközhasználat - elektronikus űrlap'
    ,'018'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00122'
    ,Nev='Közös eszközhasználat - elektronikus űrlap'
    ,Sorrend='018'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00125
SET @record_id = (select Id from KRT_KodTarak where Id='9dbc66f6-62ea-4df8-9d13-0242c0552a02') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'9dbc66f6-62ea-4df8-9d13-0242c0552a02'
    ,'esf00125'
    ,'Postai panaszbejelentő űrlap'
    ,'019'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00125'
    ,Nev='Postai panaszbejelentő űrlap'
    ,Sorrend='019'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00998
SET @record_id = (select Id from KRT_KodTarak where Id='6b8393b2-286c-428a-b2c1-8730d1007d20') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'6b8393b2-286c-428a-b2c1-8730d1007d20'
    ,'esf00998'
    ,'Hivatali kapun keresztül érkezett küldemény'
    ,'020'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00998'
    ,Nev='Hivatali kapun keresztül érkezett küldemény'
    ,Sorrend='020'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf00999
SET @record_id = (select Id from KRT_KodTarak where Id='12472164-3370-4301-8f8e-6b13dd51ca1a') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'12472164-3370-4301-8f8e-6b13dd51ca1a'
    ,'esf00999'
    ,'Hivatali kapun keresztül érkezett küldemény'
    ,'021'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf00999'
    ,Nev='Hivatali kapun keresztül érkezett küldemény'
    ,Sorrend='021'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- esf10120
SET @record_id = (select Id from KRT_KodTarak where Id='a67ab60e-17cf-4ecb-8c40-fe1986ccd29f') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'a67ab60e-17cf-4ecb-8c40-fe1986ccd29f'
    ,'esf10120'
    ,'Hotline bejelentés kezdeményezése'
    ,'022'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='esf10120'
    ,Nev='Hotline bejelentés kezdeményezése'
    ,Sorrend='022'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf02856
SET @record_id = (select Id from KRT_KodTarak where Id='aadd825a-e3e7-43bd-a916-d2e39709a137') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'aadd825a-e3e7-43bd-a916-d2e39709a137'
    ,'naf02856'
    ,'Helyhez kötött telefon adatszolgáltatás - 2015'
    ,'023'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf02856'
    ,Nev='Helyhez kötött telefon adatszolgáltatás - 2015'
    ,Sorrend='023'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf02938x
SET @record_id = (select Id from KRT_KodTarak where Id='212b81ff-1378-4c98-9e5a-4c61bff6f1e8') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'212b81ff-1378-4c98-9e5a-4c61bff6f1e8'
    ,'naf02938x'
    ,'Új szolgáltató regisztrálása a KRA-ban'
    ,'024'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf02938x'
    ,Nev='Új szolgáltató regisztrálása a KRA-ban'
    ,Sorrend='024'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf02940x
SET @record_id = (select Id from KRT_KodTarak where Id='7ad3195c-85d7-4291-90ba-39d710e513f4') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'7ad3195c-85d7-4291-90ba-39d710e513f4'
    ,'naf02940x'
    ,'KRA szolgáltatói adatok módosítása'
    ,'025'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf02940x'
    ,Nev='KRA szolgáltatói adatok módosítása'
    ,Sorrend='025'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf02942x
SET @record_id = (select Id from KRT_KodTarak where Id='4fa8738e-b16a-481f-9ea4-0e92ea2caa37') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'4fa8738e-b16a-481f-9ea4-0e92ea2caa37'
    ,'naf02942x'
    ,'Új felhasználó regisztrálása a KRA-ban'
    ,'026'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf02942x'
    ,Nev='Új felhasználó regisztrálása a KRA-ban'
    ,Sorrend='026'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf02945x
SET @record_id = (select Id from KRT_KodTarak where Id='789ef20b-8d5a-439e-8e97-9ca8a0fc6b10') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'789ef20b-8d5a-439e-8e97-9ca8a0fc6b10'
    ,'naf02945x'
    ,'KRA Felhasználó felfüggesztése'
    ,'027'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf02945x'
    ,Nev='KRA Felhasználó felfüggesztése'
    ,Sorrend='027'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf02947x
SET @record_id = (select Id from KRT_KodTarak where Id='1c142645-a039-49f9-862e-fecb3adc932b') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'1c142645-a039-49f9-862e-fecb3adc932b'
    ,'naf02947x'
    ,'KRA Felhasználói adatok módosítása'
    ,'028'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf02947x'
    ,Nev='KRA Felhasználói adatok módosítása'
    ,Sorrend='028'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf02950x
SET @record_id = (select Id from KRT_KodTarak where Id='5c1826f8-cc5f-4807-82db-57beb51a4d01') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'5c1826f8-cc5f-4807-82db-57beb51a4d01'
    ,'naf02950x'
    ,'AZONOSÍTÓ LEKÖTÉSI/KIJELÖLÉSI KÉRELEM'
    ,'029'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf02950x'
    ,Nev='AZONOSÍTÓ LEKÖTÉSI/KIJELÖLÉSI KÉRELEM'
    ,Sorrend='029'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03109
SET @record_id = (select Id from KRT_KodTarak where Id='191ff750-8a51-4a91-972e-6437077431e1') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'191ff750-8a51-4a91-972e-6437077431e1'
    ,'naf03109'
    ,'Helyhez kötött telefon adatszolgáltatás - 2015'
    ,'030'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03109'
    ,Nev='Helyhez kötött telefon adatszolgáltatás - 2015'
    ,Sorrend='030'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03453
SET @record_id = (select Id from KRT_KodTarak where Id='981d6c0e-7fd8-4b50-b50d-1ab191846d6b') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'981d6c0e-7fd8-4b50-b50d-1ab191846d6b'
    ,'naf03453'
    ,'Béreltvonali szolgáltatás (2011)'
    ,'031'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03453'
    ,Nev='Béreltvonali szolgáltatás (2011)'
    ,Sorrend='031'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03557
SET @record_id = (select Id from KRT_KodTarak where Id='ff985da4-dfe0-46aa-8616-17fdb8c8d289') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'ff985da4-dfe0-46aa-8616-17fdb8c8d289'
    ,'naf03557'
    ,'ÁSZF beküldés'
    ,'032'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03557'
    ,Nev='ÁSZF beküldés'
    ,Sorrend='032'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03579
SET @record_id = (select Id from KRT_KodTarak where Id='5a98a969-aaf8-43ad-8599-9c220f36f0f9') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'5a98a969-aaf8-43ad-8599-9c220f36f0f9'
    ,'naf03579'
    ,'Helyhez kötött telefon adatszolgáltatás - 2015'
    ,'033'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03579'
    ,Nev='Helyhez kötött telefon adatszolgáltatás - 2015'
    ,Sorrend='033'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03657
SET @record_id = (select Id from KRT_KodTarak where Id='0cabc508-9a91-4ea7-88c7-57c9d7c6b5d1') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'0cabc508-9a91-4ea7-88c7-57c9d7c6b5d1'
    ,'naf03657'
    ,'Adatszolgáltatás a helyhez kötött piacelemzési eljárások lefolytatása érdekében: nem tejlesítettek 2011'
    ,'034'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03657'
    ,Nev='Adatszolgáltatás a helyhez kötött piacelemzési eljárások lefolytatása érdekében: nem tejlesítettek 2011'
    ,Sorrend='034'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03660
SET @record_id = (select Id from KRT_KodTarak where Id='5a6bf66c-8dc7-4910-a029-fe27fd8a38d7') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'5a6bf66c-8dc7-4910-a029-fe27fd8a38d7'
    ,'naf03660'
    ,'13/2011 NMHH rendelet szerinti adatszolgáltatás (229/2008)'
    ,'035'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03660'
    ,Nev='13/2011 NMHH rendelet szerinti adatszolgáltatás (229/2008)'
    ,Sorrend='035'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03741
SET @record_id = (select Id from KRT_KodTarak where Id='c4a9ce2d-96ef-4e82-ae3f-5e34a512624e') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'c4a9ce2d-96ef-4e82-ae3f-5e34a512624e'
    ,'naf03741'
    ,'Adatszolgáltatás a helyhez kötött piacelemzési eljárások lefolytatása érdekében: nem tejlesítettek 2011 - 2012-ES ELJÁRÁS'
    ,'036'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03741'
    ,Nev='Adatszolgáltatás a helyhez kötött piacelemzési eljárások lefolytatása érdekében: nem tejlesítettek 2011 - 2012-ES ELJÁRÁS'
    ,Sorrend='036'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03761
SET @record_id = (select Id from KRT_KodTarak where Id='63404949-7a18-4553-a35f-04d6029367e8') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'63404949-7a18-4553-a35f-04d6029367e8'
    ,'naf03761'
    ,'Adatszolgáltatás a helyhez kötött piacelemzési eljárások lefolytatása érdekében: nem tejlesítettek 2011 - 2012-ES ELJÁRÁS hiánypótlás'
    ,'037'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03761'
    ,Nev='Adatszolgáltatás a helyhez kötött piacelemzési eljárások lefolytatása érdekében: nem tejlesítettek 2011 - 2012-ES ELJÁRÁS hiánypótlás'
    ,Sorrend='037'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03820
SET @record_id = (select Id from KRT_KodTarak where Id='fb05e308-a79e-4694-adc3-9eb08353065b') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'fb05e308-a79e-4694-adc3-9eb08353065b'
    ,'naf03820'
    ,'Hírközlési építmények közös eszközhasználatának bejelentése'
    ,'038'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03820'
    ,Nev='Hírközlési építmények közös eszközhasználatának bejelentése'
    ,Sorrend='038'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03860
SET @record_id = (select Id from KRT_KodTarak where Id='e09be776-0e58-4c15-a095-db36a4da9a7b') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'e09be776-0e58-4c15-a095-db36a4da9a7b'
    ,'naf03860'
    ,'Ellátott területek felülvizsgálata -adatkapus adatbeküldés'
    ,'039'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03860'
    ,Nev='Ellátott területek felülvizsgálata -adatkapus adatbeküldés'
    ,Sorrend='039'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03921
SET @record_id = (select Id from KRT_KodTarak where Id='fe754bdc-f96f-4cd6-be9a-16fc0f2bafb9') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'fe754bdc-f96f-4cd6-be9a-16fc0f2bafb9'
    ,'naf03921'
    ,'Műsorterjesztési szolgáltatás adatbeküldése - elektronikus űrlap 1'
    ,'040'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03921'
    ,Nev='Műsorterjesztési szolgáltatás adatbeküldése - elektronikus űrlap 1'
    ,Sorrend='040'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03923
SET @record_id = (select Id from KRT_KodTarak where Id='d0723b33-ffd2-4927-8cc7-45efb36f067b') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'d0723b33-ffd2-4927-8cc7-45efb36f067b'
    ,'naf03923'
    ,'Műsorterjesztési szolgáltatás adatbeküldése - elektronikus űrlap 2'
    ,'041'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03923'
    ,Nev='Műsorterjesztési szolgáltatás adatbeküldése - elektronikus űrlap 2'
    ,Sorrend='041'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03925
SET @record_id = (select Id from KRT_KodTarak where Id='c9639a0e-74f8-4e6c-98e7-05c5601e7d5c') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'c9639a0e-74f8-4e6c-98e7-05c5601e7d5c'
    ,'naf03925'
    ,'Műsorterjesztési szolgáltatás adatbeküldése - elektronikus űrlap 3'
    ,'042'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03925'
    ,Nev='Műsorterjesztési szolgáltatás adatbeküldése - elektronikus űrlap 3'
    ,Sorrend='042'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03927
SET @record_id = (select Id from KRT_KodTarak where Id='254d4c68-04f5-4516-af8b-4ba26215ff18') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'254d4c68-04f5-4516-af8b-4ba26215ff18'
    ,'naf03927'
    ,'Műholdas adatszolgáltatás beküldése - elektronikus űrlap'
    ,'043'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03927'
    ,Nev='Műholdas adatszolgáltatás beküldése - elektronikus űrlap'
    ,Sorrend='043'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03935
SET @record_id = (select Id from KRT_KodTarak where Id='4f231209-9a8d-4214-a1fa-74530c0265c4') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'4f231209-9a8d-4214-a1fa-74530c0265c4'
    ,'naf03935'
    ,'Helyhez kötött 25016/2012. sz. eljárás -adatkapus beküldései'
    ,'044'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03935'
    ,Nev='Helyhez kötött 25016/2012. sz. eljárás -adatkapus beküldései'
    ,Sorrend='044'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf03980
SET @record_id = (select Id from KRT_KodTarak where Id='85962257-41fa-48b8-9527-40dd79c21612') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'85962257-41fa-48b8-9527-40dd79c21612'
    ,'naf03980'
    ,'Szolgáltatásminőségi adatszolgáltatás (13/2011)'
    ,'045'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf03980'
    ,Nev='Szolgáltatásminőségi adatszolgáltatás (13/2011)'
    ,Sorrend='045'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04011
SET @record_id = (select Id from KRT_KodTarak where Id='9df6fc6d-3f6b-4d0e-be9e-0afd4b5dcb65') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'9df6fc6d-3f6b-4d0e-be9e-0afd4b5dcb65'
    ,'naf04011'
    ,'Tanúsított elektronikus aláírási termékek - adatkapus adatszolgáltatás - nyilvántartásbavétel'
    ,'046'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04011'
    ,Nev='Tanúsított elektronikus aláírási termékek - adatkapus adatszolgáltatás - nyilvántartásbavétel'
    ,Sorrend='046'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04013
SET @record_id = (select Id from KRT_KodTarak where Id='60e25909-769b-4af8-b649-354ecc82c224') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'60e25909-769b-4af8-b649-354ecc82c224'
    ,'naf04013'
    ,'Tanúsított elektronikus aláírási termékek - adatkapus adatszolgáltatás - visszavonásbejelentés'
    ,'047'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04013'
    ,Nev='Tanúsított elektronikus aláírási termékek - adatkapus adatszolgáltatás - visszavonásbejelentés'
    ,Sorrend='047'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04018
SET @record_id = (select Id from KRT_KodTarak where Id='acd3b47d-7b7a-443a-91ea-2f512a29220f') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'acd3b47d-7b7a-443a-91ea-2f512a29220f'
    ,'naf04018'
    ,'Elektronikus aláírással kapcsolatos szolgáltatási statisztikák - adatkapus adatszolgáltatás'
    ,'048'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04018'
    ,Nev='Elektronikus aláírással kapcsolatos szolgáltatási statisztikák - adatkapus adatszolgáltatás'
    ,Sorrend='048'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04064
SET @record_id = (select Id from KRT_KodTarak where Id='8a958a9b-adcf-407c-9932-8c1134d5d3e5') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'8a958a9b-adcf-407c-9932-8c1134d5d3e5'
    ,'naf04064'
    ,'12/2004.(IV.22.) IHM adatszolgáltatás - Bejelentett postai szolgáltató által szolgáltatandó adatok 2012'
    ,'049'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04064'
    ,Nev='12/2004.(IV.22.) IHM adatszolgáltatás - Bejelentett postai szolgáltató által szolgáltatandó adatok 2012'
    ,Sorrend='049'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04071
SET @record_id = (select Id from KRT_KodTarak where Id='06c3209b-9485-4568-bc6a-501a1e577d03') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'06c3209b-9485-4568-bc6a-501a1e577d03'
    ,'naf04071'
    ,'12/2004.(IV.22.) IHM adatszolgáltatás - Egyetemes postai szolgáltató által szolgáltatandó adatok 2012'
    ,'050'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04071'
    ,Nev='12/2004.(IV.22.) IHM adatszolgáltatás - Egyetemes postai szolgáltató által szolgáltatandó adatok 2012'
    ,Sorrend='050'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04075
SET @record_id = (select Id from KRT_KodTarak where Id='a63af241-1f00-4d6d-96d2-0eaa1e8ded48') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'a63af241-1f00-4d6d-96d2-0eaa1e8ded48'
    ,'naf04075'
    ,'Műsorterjesztési szolgáltatás adatbeküldése - elektronikus űrlap 4'
    ,'051'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04075'
    ,Nev='Műsorterjesztési szolgáltatás adatbeküldése - elektronikus űrlap 4'
    ,Sorrend='051'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04261
SET @record_id = (select Id from KRT_KodTarak where Id='cc0449c3-c82a-4fa6-9d25-f61b7e4607ec') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'cc0449c3-c82a-4fa6-9d25-f61b7e4607ec'
    ,'naf04261'
    ,'ADATSZOLGÁLTATÁS A HELYHEZ KÖTÖTT PIACELEMZÉSI ELJÁRÁSOK LEFOLYTATÁSA ÉRDEKÉBEN 2013 (1 -  sorszám)'
    ,'052'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04261'
    ,Nev='ADATSZOLGÁLTATÁS A HELYHEZ KÖTÖTT PIACELEMZÉSI ELJÁRÁSOK LEFOLYTATÁSA ÉRDEKÉBEN 2013 (1 -  sorszám)'
    ,Sorrend='052'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04263
SET @record_id = (select Id from KRT_KodTarak where Id='47919a01-38d5-4977-a9b1-dfef462f2a85') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'47919a01-38d5-4977-a9b1-dfef462f2a85'
    ,'naf04263'
    ,'ADATSZOLGÁLTATÁS A HELYHEZ KÖTÖTT PIACELEMZÉSI ELJÁRÁSOK LEFOLYTATÁSA ÉRDEKÉBEN 2013 (2 -  sorszám)'
    ,'053'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04263'
    ,Nev='ADATSZOLGÁLTATÁS A HELYHEZ KÖTÖTT PIACELEMZÉSI ELJÁRÁSOK LEFOLYTATÁSA ÉRDEKÉBEN 2013 (2 -  sorszám)'
    ,Sorrend='053'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04267
SET @record_id = (select Id from KRT_KodTarak where Id='51c0a45a-9d6e-4619-aefb-f2da02c2912c') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'51c0a45a-9d6e-4619-aefb-f2da02c2912c'
    ,'naf04267'
    ,'ADATSZOLGÁLTATÁS A HELYHEZ KÖTÖTT PIACELEMZÉSI ELJÁRÁSOK LEFOLYTATÁSA ÉRDEKÉBEN 2013 (3 -  sorszám)'
    ,'054'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04267'
    ,Nev='ADATSZOLGÁLTATÁS A HELYHEZ KÖTÖTT PIACELEMZÉSI ELJÁRÁSOK LEFOLYTATÁSA ÉRDEKÉBEN 2013 (3 -  sorszám)'
    ,Sorrend='054'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04282
SET @record_id = (select Id from KRT_KodTarak where Id='5e7ceea9-d25d-4af5-85ff-62b74c474b2d') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'5e7ceea9-d25d-4af5-85ff-62b74c474b2d'
    ,'naf04282'
    ,'Szolgáltató nyilatkozattételi űrlap'
    ,'055'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04282'
    ,Nev='Szolgáltató nyilatkozattételi űrlap'
    ,Sorrend='055'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04401
SET @record_id = (select Id from KRT_KodTarak where Id='a1abc333-7379-44ab-8303-633e244db854') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'a1abc333-7379-44ab-8303-633e244db854'
    ,'naf04401'
    ,'ADATSZOLGÁLTATÁS A MŰSORTERJESZTÉSI PIACELEMZÉSI ELJÁRÁSOK LEFOLYTATÁSA ÉRDEKÉBEN 2013 (1 -  sorszám)'
    ,'056'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04401'
    ,Nev='ADATSZOLGÁLTATÁS A MŰSORTERJESZTÉSI PIACELEMZÉSI ELJÁRÁSOK LEFOLYTATÁSA ÉRDEKÉBEN 2013 (1 -  sorszám)'
    ,Sorrend='056'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04417
SET @record_id = (select Id from KRT_KodTarak where Id='44dbeeef-1161-42e1-87e4-6e348c84ef1f') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'44dbeeef-1161-42e1-87e4-6e348c84ef1f'
    ,'naf04417'
    ,'ADATSZOLGÁLTATÁS A MŰSORTERJESZTÉSI PIACELEMZÉSI ELJÁRÁSOK LEFOLYTATÁSA ÉRDEKÉBEN 2013 (2 -  sorszám)'
    ,'057'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04417'
    ,Nev='ADATSZOLGÁLTATÁS A MŰSORTERJESZTÉSI PIACELEMZÉSI ELJÁRÁSOK LEFOLYTATÁSA ÉRDEKÉBEN 2013 (2 -  sorszám)'
    ,Sorrend='057'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04441
SET @record_id = (select Id from KRT_KodTarak where Id='f58192a9-ccc9-4805-b278-8b77fef8c4f5') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'f58192a9-ccc9-4805-b278-8b77fef8c4f5'
    ,'naf04441'
    ,'ADATSZOLGÁLTATÁS A MŰSORTERJESZTÉSI PIACELEMZÉSI ELJÁRÁSOK LEFOLYTATÁSA ÉRDEKÉBEN 2013 (3 -  sorszám)'
    ,'058'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04441'
    ,Nev='ADATSZOLGÁLTATÁS A MŰSORTERJESZTÉSI PIACELEMZÉSI ELJÁRÁSOK LEFOLYTATÁSA ÉRDEKÉBEN 2013 (3 -  sorszám)'
    ,Sorrend='058'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04624
SET @record_id = (select Id from KRT_KodTarak where Id='484cde94-79a0-4bc4-a3e8-82b66d44a71c') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'484cde94-79a0-4bc4-a3e8-82b66d44a71c'
    ,'naf04624'
    ,'Online interaktív engedélykérés FMS (polgári és nem polgári)'
    ,'059'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04624'
    ,Nev='Online interaktív engedélykérés FMS (polgári és nem polgári)'
    ,Sorrend='059'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04682
SET @record_id = (select Id from KRT_KodTarak where Id='5420e292-408a-4544-bff4-2ee61276485b') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'5420e292-408a-4544-bff4-2ee61276485b'
    ,'naf04682'
    ,'Személyes adatok megsértésének bejelentője (EU-s űrlap)'
    ,'060'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04682'
    ,Nev='Személyes adatok megsértésének bejelentője (EU-s űrlap)'
    ,Sorrend='060'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04706
SET @record_id = (select Id from KRT_KodTarak where Id='e6cfd106-e189-44b1-b054-ff5391a5db09') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'e6cfd106-e189-44b1-b054-ff5391a5db09'
    ,'naf04706'
    ,'E-szolgáltatásbejelentés ÁSZF beküldés'
    ,'061'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04706'
    ,Nev='E-szolgáltatásbejelentés ÁSZF beküldés'
    ,Sorrend='061'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04708
SET @record_id = (select Id from KRT_KodTarak where Id='d02340d6-d6e2-49f7-8485-c4887e5a09aa') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'d02340d6-d6e2-49f7-8485-c4887e5a09aa'
    ,'naf04708'
    ,'E-szolgáltatásbejelentés Szolgáltatói adatváltozás bejelentése'
    ,'062'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04708'
    ,Nev='E-szolgáltatásbejelentés Szolgáltatói adatváltozás bejelentése'
    ,Sorrend='062'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04710
SET @record_id = (select Id from KRT_KodTarak where Id='deb9a530-8936-44af-88f6-0734232e1ca1') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'deb9a530-8936-44af-88f6-0734232e1ca1'
    ,'naf04710'
    ,'E-szolgáltatásbejelentés Szolgáltatás adatbejelentése'
    ,'063'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04710'
    ,Nev='E-szolgáltatásbejelentés Szolgáltatás adatbejelentése'
    ,Sorrend='063'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04712
SET @record_id = (select Id from KRT_KodTarak where Id='393790fa-cbba-44f1-b88f-ee1e960f85f5') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'393790fa-cbba-44f1-b88f-ee1e960f85f5'
    ,'naf04712'
    ,'E-Szolgáltatásbejelentés E-Ügyintézés szolgáltatói  nyilatkozattételi űrlap'
    ,'064'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04712'
    ,Nev='E-Szolgáltatásbejelentés E-Ügyintézés szolgáltatói  nyilatkozattételi űrlap'
    ,Sorrend='064'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04714
SET @record_id = (select Id from KRT_KodTarak where Id='f4a6286b-6c34-4893-8fb9-fc221aec4e0d') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'f4a6286b-6c34-4893-8fb9-fc221aec4e0d'
    ,'naf04714'
    ,'E-Szolgáltatásbejelentés Interfész bejelentés'
    ,'065'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04714'
    ,Nev='E-Szolgáltatásbejelentés Interfész bejelentés'
    ,Sorrend='065'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04716
SET @record_id = (select Id from KRT_KodTarak where Id='a522ff5c-b053-405a-8b45-517213be4d64') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'a522ff5c-b053-405a-8b45-517213be4d64'
    ,'naf04716'
    ,'E-Szolgáltatásbejelentés Kiegészítő médiaszolgáltatás bejelentése'
    ,'066'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04716'
    ,Nev='E-Szolgáltatásbejelentés Kiegészítő médiaszolgáltatás bejelentése'
    ,Sorrend='066'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04718
SET @record_id = (select Id from KRT_KodTarak where Id='6559b3d1-7a55-43de-9b2e-e3d3c8a75e39') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'6559b3d1-7a55-43de-9b2e-e3d3c8a75e39'
    ,'naf04718'
    ,'E-Solgáltatásbejelentés Szolgáltatás nyújtására vonatkozó jogosultság igazolása'
    ,'067'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04718'
    ,Nev='E-Solgáltatásbejelentés Szolgáltatás nyújtására vonatkozó jogosultság igazolása'
    ,Sorrend='067'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04720
SET @record_id = (select Id from KRT_KodTarak where Id='5aae9903-b1a8-4fe3-b68a-4fb529174d7a') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'5aae9903-b1a8-4fe3-b68a-4fb529174d7a'
    ,'naf04720'
    ,'E-Szolgáltatásbejelentés Új szolgáltató adatbejelentése -dosszié'
    ,'068'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04720'
    ,Nev='E-Szolgáltatásbejelentés Új szolgáltató adatbejelentése -dosszié'
    ,Sorrend='068'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04735
SET @record_id = (select Id from KRT_KodTarak where Id='0656ae90-9ef3-46b4-b5a2-69a17d5d6a36') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'0656ae90-9ef3-46b4-b5a2-69a17d5d6a36'
    ,'naf04735'
    ,'E-szolgáltatásbejelentés Regisztráció kérése'
    ,'069'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04735'
    ,Nev='E-szolgáltatásbejelentés Regisztráció kérése'
    ,Sorrend='069'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04802
SET @record_id = (select Id from KRT_KodTarak where Id='664bc3a8-7d78-4bf1-8216-44ec535b94f5') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'664bc3a8-7d78-4bf1-8216-44ec535b94f5'
    ,'naf04802'
    ,'KEHTA kapcsolattartók karbantartása'
    ,'070'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04802'
    ,Nev='KEHTA kapcsolattartók karbantartása'
    ,Sorrend='070'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf04962
SET @record_id = (select Id from KRT_KodTarak where Id='5224b9f3-4ce0-48e1-a3c7-97846620010a') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'5224b9f3-4ce0-48e1-a3c7-97846620010a'
    ,'naf04962'
    ,'ADATSZOLGÁLTATÁS HELYHEZ KÖTÖTT INTERNET ÉS NAGYKERESKEDELMI HOZZÁFÉRÉSI SZOLGÁLTATÁS 2013'
    ,'071'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf04962'
    ,Nev='ADATSZOLGÁLTATÁS HELYHEZ KÖTÖTT INTERNET ÉS NAGYKERESKEDELMI HOZZÁFÉRÉSI SZOLGÁLTATÁS 2013'
    ,Sorrend='071'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf05150
SET @record_id = (select Id from KRT_KodTarak where Id='fa49e62e-4694-4fbb-9603-2a296e11e4d5') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'fa49e62e-4694-4fbb-9603-2a296e11e4d5'
    ,'naf05150'
    ,'Műsorterjesztési adatszolgáltatás weben keresztüli kitöltéssel- 2014'
    ,'072'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf05150'
    ,Nev='Műsorterjesztési adatszolgáltatás weben keresztüli kitöltéssel- 2014'
    ,Sorrend='072'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf05152
SET @record_id = (select Id from KRT_KodTarak where Id='a63a7ae1-0ed9-499c-bb7f-a9725601aba2') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'a63a7ae1-0ed9-499c-bb7f-a9725601aba2'
    ,'naf05152'
    ,'Műsorterjesztési adatszolgáltatás XML állomány feltöltése -2014'
    ,'073'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf05152'
    ,Nev='Műsorterjesztési adatszolgáltatás XML állomány feltöltése -2014'
    ,Sorrend='073'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf05158
SET @record_id = (select Id from KRT_KodTarak where Id='4b6e4f6a-858e-4023-b94e-e3f143220958') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'4b6e4f6a-858e-4023-b94e-e3f143220958'
    ,'naf05158'
    ,'Műsorterjesztési adatszolgáltatás  - terjesztett médiaszolgáltatási adatok beküldése -2014'
    ,'074'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf05158'
    ,Nev='Műsorterjesztési adatszolgáltatás  - terjesztett médiaszolgáltatási adatok beküldése -2014'
    ,Sorrend='074'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf06195
SET @record_id = (select Id from KRT_KodTarak where Id='dd37152d-309b-4c03-a942-c9e0c8738c76') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'dd37152d-309b-4c03-a942-c9e0c8738c76'
    ,'naf06195'
    ,'ADATSZOLGÁLTATÁS A MOBIL PIACOKON ÉRINTETT SZOLGÁLTATÓK KÖRÉBEN 2014'
    ,'075'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf06195'
    ,Nev='ADATSZOLGÁLTATÁS A MOBIL PIACOKON ÉRINTETT SZOLGÁLTATÓK KÖRÉBEN 2014'
    ,Sorrend='075'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf06510
SET @record_id = (select Id from KRT_KodTarak where Id='5d16f945-0e9d-4a97-a763-ef583d7ca26e') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'5d16f945-0e9d-4a97-a763-ef583d7ca26e'
    ,'naf06510'
    ,'Műsorterjesztési adatszolgáltatás weben keresztüli kitöltéssel- 2015'
    ,'076'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf06510'
    ,Nev='Műsorterjesztési adatszolgáltatás weben keresztüli kitöltéssel- 2015'
    ,Sorrend='076'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf06514
SET @record_id = (select Id from KRT_KodTarak where Id='1437820b-a597-4023-bdae-dd77ec9c89bb') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'1437820b-a597-4023-bdae-dd77ec9c89bb'
    ,'naf06514'
    ,'Műsorterjesztési adatszolgáltatás XML állomány feltöltése -2015'
    ,'077'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf06514'
    ,Nev='Műsorterjesztési adatszolgáltatás XML állomány feltöltése -2015'
    ,Sorrend='077'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf06570
SET @record_id = (select Id from KRT_KodTarak where Id='d4342412-3310-41d4-b112-07553ad7df5b') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'d4342412-3310-41d4-b112-07553ad7df5b'
    ,'naf06570'
    ,'Szolgáltatásminőségi adatszolgáltatás (13/2011)'
    ,'078'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf06570'
    ,Nev='Szolgáltatásminőségi adatszolgáltatás (13/2011)'
    ,Sorrend='078'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf06611
SET @record_id = (select Id from KRT_KodTarak where Id='859e015d-14c5-4bb6-a230-be6ea0aa4aaf') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'859e015d-14c5-4bb6-a230-be6ea0aa4aaf'
    ,'naf06611'
    ,'Egyetemes postai szolgáltatás -2015'
    ,'079'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf06611'
    ,Nev='Egyetemes postai szolgáltatás -2015'
    ,Sorrend='079'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf06613
SET @record_id = (select Id from KRT_KodTarak where Id='7caeb47c-f8b5-428d-9fd1-b3ac2e7daf63') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'7caeb47c-f8b5-428d-9fd1-b3ac2e7daf63'
    ,'naf06613'
    ,'Egyetemes postai szolgáltatást helyettesítő postai szolgáltatás -2015'
    ,'080'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf06613'
    ,Nev='Egyetemes postai szolgáltatást helyettesítő postai szolgáltatás -2015'
    ,Sorrend='080'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf06615
SET @record_id = (select Id from KRT_KodTarak where Id='62e86c36-e11f-482a-ab8b-3d43bb351e7a') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'62e86c36-e11f-482a-ab8b-3d43bb351e7a'
    ,'naf06615'
    ,'Egyetemes postai szolgáltatást nem helyettesítő postai szolgáltatás -2015'
    ,'081'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf06615'
    ,Nev='Egyetemes postai szolgáltatást nem helyettesítő postai szolgáltatás -2015'
    ,Sorrend='081'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf06692
SET @record_id = (select Id from KRT_KodTarak where Id='093f4ef6-d020-49b1-aa3e-ceb1df932707') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'093f4ef6-d020-49b1-aa3e-ceb1df932707'
    ,'naf06692'
    ,'Postai kérelem, bejelentés'
    ,'082'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf06692'
    ,Nev='Postai kérelem, bejelentés'
    ,Sorrend='082'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf06750
SET @record_id = (select Id from KRT_KodTarak where Id='7d30bc35-f1e8-415a-96cb-1266ddc37f64') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'7d30bc35-f1e8-415a-96cb-1266ddc37f64'
    ,'naf06750'
    ,'Adatszolgáltatás a 4/2014-es "Helyhez kötött jó minőségű hozzáférés nagykereskedelmi biztosítása" piacokhoz 2015'
    ,'083'
    ,'1'
    ); 
     
         END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf06750'
    ,Nev='Adatszolgáltatás a 4/2014-es "Helyhez kötött jó minőségű hozzáférés nagykereskedelmi biztosítása" piacokhoz 2015'
    ,Sorrend='083'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf07205
SET @record_id = (select Id from KRT_KodTarak where Id='8c1600e2-a4fe-4278-a92a-02974b989c9c') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'8c1600e2-a4fe-4278-a92a-02974b989c9c'
    ,'naf07205'
    ,'ADATSZOLGÁLTATÁS HELYHEZ KÖTÖTT INTERNET ÉS NAGYKERESKEDELMI HOZZÁFÉRÉSI SZOLGÁLTATÁS 2015'
    ,'084'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf07205'
    ,Nev='ADATSZOLGÁLTATÁS HELYHEZ KÖTÖTT INTERNET ÉS NAGYKERESKEDELMI HOZZÁFÉRÉSI SZOLGÁLTATÁS 2015'
    ,Sorrend='084'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf07673
SET @record_id = (select Id from KRT_KodTarak where Id='7de787ca-83c8-4287-ab38-3b182c417a06') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'7de787ca-83c8-4287-ab38-3b182c417a06'
    ,'naf07673'
    ,'FMS Műsorszóró sávú frekvencia kérelem'
    ,'085'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf07673'
    ,Nev='FMS Műsorszóró sávú frekvencia kérelem'
    ,Sorrend='085'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf07675
SET @record_id = (select Id from KRT_KodTarak where Id='5abfa231-99d3-4468-95d8-e0e1dbcb022a') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'5abfa231-99d3-4468-95d8-e0e1dbcb022a'
    ,'naf07675'
    ,'FMS Általános célú engedély kérelem'
    ,'086'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf07675'
    ,Nev='FMS Általános célú engedély kérelem'
    ,Sorrend='086'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf07683
SET @record_id = (select Id from KRT_KodTarak where Id='6c1dcc5d-3308-4c7a-a038-e63385fd1301') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'6c1dcc5d-3308-4c7a-a038-e63385fd1301'
    ,'naf07683'
    ,'Online engedélykérés XML csatolással FMS'
    ,'087'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf07683'
    ,Nev='Online engedélykérés XML csatolással FMS'
    ,Sorrend='087'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf07685
SET @record_id = (select Id from KRT_KodTarak where Id='2c57eb4f-41e5-4e27-9be8-90a1592f1a29') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'2c57eb4f-41e5-4e27-9be8-90a1592f1a29'
    ,'naf07685'
    ,'Nem polgári online engedélykérés XML csatolással FMS'
    ,'088'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf07685'
    ,Nev='Nem polgári online engedélykérés XML csatolással FMS'
    ,Sorrend='088'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf07688
SET @record_id = (select Id from KRT_KodTarak where Id='3c7b326e-5311-4474-85f5-3d698a842cee') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'3c7b326e-5311-4474-85f5-3d698a842cee'
    ,'naf07688'
    ,'Egyéb frekvencia kérelem'
    ,'089'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf07688'
    ,Nev='Egyéb frekvencia kérelem'
    ,Sorrend='089'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf07755
SET @record_id = (select Id from KRT_KodTarak where Id='e59c0426-e792-47b3-a942-f0d9fc5144fe') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'e59c0426-e792-47b3-a942-f0d9fc5144fe'
    ,'naf07755'
    ,'Adatszolgáltatás a mobil piacokon érintett szolgáltatók körében - 2015'
    ,'090'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf07755'
    ,Nev='Adatszolgáltatás a mobil piacokon érintett szolgáltatók körében - 2015'
    ,Sorrend='090'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf07990
SET @record_id = (select Id from KRT_KodTarak where Id='b3ece5c1-ccc4-493c-a297-c2f16e5e4cdc') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'b3ece5c1-ccc4-493c-a297-c2f16e5e4cdc'
    ,'naf07990'
    ,'Adatszolgáltatás a műsorterjesztési piacon érintett szolgáltatók körében 2015'
    ,'091'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf07990'
    ,Nev='Adatszolgáltatás a műsorterjesztési piacon érintett szolgáltatók körében 2015'
    ,Sorrend='091'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf08090
SET @record_id = (select Id from KRT_KodTarak where Id='d4165053-8d0a-4a3b-97be-52d64d0fc07b') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'d4165053-8d0a-4a3b-97be-52d64d0fc07b'
    ,'naf08090'
    ,'Szolgáltatásminőségi adatszolgáltatás új űrlap -2016'
    ,'092'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf08090'
    ,Nev='Szolgáltatásminőségi adatszolgáltatás új űrlap -2016'
    ,Sorrend='092'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf08092
SET @record_id = (select Id from KRT_KodTarak where Id='9ab67a0d-81d3-4ef0-a186-8e82ce3ea1fc') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'9ab67a0d-81d3-4ef0-a186-8e82ce3ea1fc'
    ,'naf08092'
    ,'Műsorterjesztési adatszolgáltatás web-es űrlap -2016'
    ,'093'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf08092'
    ,Nev='Műsorterjesztési adatszolgáltatás web-es űrlap -2016'
    ,Sorrend='093'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf08100
SET @record_id = (select Id from KRT_KodTarak where Id='e8bc18b6-5438-435b-9c02-f04b9700a621') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'e8bc18b6-5438-435b-9c02-f04b9700a621'
    ,'naf08100'
    ,'Műsorterjesztés XML állomány feltöltése 2016'
    ,'094'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf08100'
    ,Nev='Műsorterjesztés XML állomány feltöltése 2016'
    ,Sorrend='094'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf08315
SET @record_id = (select Id from KRT_KodTarak where Id='118defd4-8d13-4168-abc5-ee0829ba9714') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'118defd4-8d13-4168-abc5-ee0829ba9714'
    ,'naf08315'
    ,'Egyetemes postai szolgáltatás -2016'
    ,'095'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf08315'
    ,Nev='Egyetemes postai szolgáltatás -2016'
    ,Sorrend='095'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf08317
SET @record_id = (select Id from KRT_KodTarak where Id='7ff1616c-ff17-4343-afdc-29a6fe813aeb') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'7ff1616c-ff17-4343-afdc-29a6fe813aeb'
    ,'naf08317'
    ,'Egyetemes postai szolgáltatást helyettesítő postai szolgáltatás -2016'
    ,'096'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf08317'
    ,Nev='Egyetemes postai szolgáltatást helyettesítő postai szolgáltatás -2016'
    ,Sorrend='096'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf08319
SET @record_id = (select Id from KRT_KodTarak where Id='d7969375-126b-42b5-acf5-52be81fb505d') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'d7969375-126b-42b5-acf5-52be81fb505d'
    ,'naf08319'
    ,'Egyetemes postai szolgáltatást nem helyettesítő postai szolgáltatás -2016'
    ,'097'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf08319'
    ,Nev='Egyetemes postai szolgáltatást nem helyettesítő postai szolgáltatás -2016'
    ,Sorrend='097'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf08611
SET @record_id = (select Id from KRT_KodTarak where Id='db92e82c-c0c0-4cbb-8614-d3f4c80cee84') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'db92e82c-c0c0-4cbb-8614-d3f4c80cee84'
    ,'naf08611'
    ,'Bérelt vonali adatszolgáltatás – 2016'
    ,'098'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf08611'
    ,Nev='Bérelt vonali adatszolgáltatás – 2016'
    ,Sorrend='098'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf08751
SET @record_id = (select Id from KRT_KodTarak where Id='779b6087-3128-43a9-b155-180a115ed231') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'779b6087-3128-43a9-b155-180a115ed231'
    ,'naf08751'
    ,'ADATSZOLGÁLTATÁS HELYHEZ KÖTÖTT INTERNET ÉS NAGYKERESKEDELMI HOZZÁFÉRÉSI SZOLGÁLTATÁS 2016'
    ,'099'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf08751'
    ,Nev='ADATSZOLGÁLTATÁS HELYHEZ KÖTÖTT INTERNET ÉS NAGYKERESKEDELMI HOZZÁFÉRÉSI SZOLGÁLTATÁS 2016'
    ,Sorrend='099'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf08797x
SET @record_id = (select Id from KRT_KodTarak where Id='630bab0c-9d8d-4585-bf83-d2f322dc57b8') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'630bab0c-9d8d-4585-bf83-d2f322dc57b8'
    ,'naf08797x'
    ,'Adatszolgáltatás a 2016. évi hozzáférési BULRIC költségmodellhez'
    ,'100'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf08797x'
    ,Nev='Adatszolgáltatás a 2016. évi hozzáférési BULRIC költségmodellhez'
    ,Sorrend='100'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf08831
SET @record_id = (select Id from KRT_KodTarak where Id='3d2e0867-123c-414d-96e6-f341fc932964') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'3d2e0867-123c-414d-96e6-f341fc932964'
    ,'naf08831'
    ,'ADATSZOLGÁLTATÁS  hívásvégződtetés űrlap 2016'
    ,'101'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf08831'
    ,Nev='ADATSZOLGÁLTATÁS  hívásvégződtetés űrlap 2016'
    ,Sorrend='101'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09051
SET @record_id = (select Id from KRT_KodTarak where Id='0abf6173-ce55-4219-9735-9a40bb16ea2d') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'0abf6173-ce55-4219-9735-9a40bb16ea2d'
    ,'naf09051'
    ,'Adatszolgáltatás a helyhez  kötött telefon piacelemzési eljárás lefolytatása érdekében 2016'
    ,'102'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09051'
    ,Nev='Adatszolgáltatás a helyhez  kötött telefon piacelemzési eljárás lefolytatása érdekében 2016'
    ,Sorrend='102'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09338
SET @record_id = (select Id from KRT_KodTarak where Id='e66a7a69-97f2-453d-8ff9-a9df07c9d7c8') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'e66a7a69-97f2-453d-8ff9-a9df07c9d7c8'
    ,'naf09338'
    ,'Adatszolgáltatás a helyhez kötött telefon szolgáltatás piacokon érintett szolgáltatók körében  BI/17473-1/2014.'
    ,'103'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09338'
    ,Nev='Adatszolgáltatás a helyhez kötött telefon szolgáltatás piacokon érintett szolgáltatók körében  BI/17473-1/2014.'
    ,Sorrend='103'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09340
SET @record_id = (select Id from KRT_KodTarak where Id='b59f72d6-4bf6-496b-8cc7-5981ab253eb4') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'b59f72d6-4bf6-496b-8cc7-5981ab253eb4'
    ,'naf09340'
    ,'Adatszolgáltatás a helyhez kötött telefon szolgáltatás piacokon érintett szolgáltatók körében  BI/17473-3/2014.'
    ,'104'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09340'
    ,Nev='Adatszolgáltatás a helyhez kötött telefon szolgáltatás piacokon érintett szolgáltatók körében  BI/17473-3/2014.'
    ,Sorrend='104'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09342
SET @record_id = (select Id from KRT_KodTarak where Id='18ee8e80-98fb-476d-83c8-0c3d196bdebc') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'18ee8e80-98fb-476d-83c8-0c3d196bdebc'
    ,'naf09342'
    ,'Adatszolgáltatás a helyhez kötött telefon szolgáltatás piacokon érintett szolgáltatók körében  BI/17473-2/2014.'
    ,'105'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09342'
    ,Nev='Adatszolgáltatás a helyhez kötött telefon szolgáltatás piacokon érintett szolgáltatók körében  BI/17473-2/2014.'
    ,Sorrend='105'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09347
SET @record_id = (select Id from KRT_KodTarak where Id='a1277f4d-7c76-4964-abdc-cef1dcb3e5da') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'a1277f4d-7c76-4964-abdc-cef1dcb3e5da'
    ,'naf09347'
    ,'ADATSZOLGÁLTATÁS HELYHEZ KÖTÖTT INTERNET ÉS NAGYKERESKEDELMI HOZZÁFÉRÉSI SZOLGÁLTATÁS 2014'
    ,'106'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09347'
    ,Nev='ADATSZOLGÁLTATÁS HELYHEZ KÖTÖTT INTERNET ÉS NAGYKERESKEDELMI HOZZÁFÉRÉSI SZOLGÁLTATÁS 2014'
    ,Sorrend='106'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09357
SET @record_id = (select Id from KRT_KodTarak where Id='d37c5e3f-b742-4524-9128-b02db422f9f2') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'d37c5e3f-b742-4524-9128-b02db422f9f2'
    ,'naf09357'
    ,'ADATSZOLGÁLTATÁS A MOBIL PIACOKON ÉRINTETT SZOLGÁLTATÓK KÖRÉBEN 2013'
    ,'107'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09357'
    ,Nev='ADATSZOLGÁLTATÁS A MOBIL PIACOKON ÉRINTETT SZOLGÁLTATÓK KÖRÉBEN 2013'
    ,Sorrend='107'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09533
SET @record_id = (select Id from KRT_KodTarak where Id='893bec6b-865d-40ce-82ff-ec5dffacba3a') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'893bec6b-865d-40ce-82ff-ec5dffacba3a'
    ,'naf09533'
    ,'Adatszolgáltatás a mobil piacokon érintett szolgáltatók körében - 2016'
    ,'108'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09533'
    ,Nev='Adatszolgáltatás a mobil piacokon érintett szolgáltatók körében - 2016'
    ,Sorrend='108'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09602
SET @record_id = (select Id from KRT_KodTarak where Id='1cf09726-eb3d-4005-a3b5-449cfbd8ed1e') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'1cf09726-eb3d-4005-a3b5-449cfbd8ed1e'
    ,'naf09602'
    ,'Elektronikus hírközlési építmények Előzetes  bejelentés'
    ,'109'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09602'
    ,Nev='Elektronikus hírközlési építmények Előzetes  bejelentés'
    ,Sorrend='109'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09604
SET @record_id = (select Id from KRT_KodTarak where Id='0fe781bd-930b-4fe2-8779-e3c0be6a2f13') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'0fe781bd-930b-4fe2-8779-e3c0be6a2f13'
    ,'naf09604'
    ,'Elektronikus hírközlési építmények utólagos  bejelentés'
    ,'110'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id


    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09604'
    ,Nev='Elektronikus hírközlési építmények utólagos  bejelentés'
    ,Sorrend='110'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09618
SET @record_id = (select Id from KRT_KodTarak where Id='dab171f8-a2bd-4cb5-8871-195698502978') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'dab171f8-a2bd-4cb5-8871-195698502978'
    ,'naf09618'
    ,'Adatszolgáltatás a műsorterjesztési piacon érintett szolgáltatók körében 2016'
    ,'111'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09618'
    ,Nev='Adatszolgáltatás a műsorterjesztési piacon érintett szolgáltatók körében 2016'
    ,Sorrend='111'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09818
SET @record_id = (select Id from KRT_KodTarak where Id='cfad1252-5709-4e53-966d-f00224f2e7b7') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'cfad1252-5709-4e53-966d-f00224f2e7b7'
    ,'naf09818'
    ,'Adatszolgáltatás a 2016. évi alaphálózati BULRIC költségmodellhez'
    ,'112'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09818'
    ,Nev='Adatszolgáltatás a 2016. évi alaphálózati BULRIC költségmodellhez'
    ,Sorrend='112'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09823
SET @record_id = (select Id from KRT_KodTarak where Id='d9dbc040-8c70-4c26-bca5-12f095f73426') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'d9dbc040-8c70-4c26-bca5-12f095f73426'
    ,'naf09823'
    ,'Adatszolgáltatás a 2016. évi alaphálózati BULRIC költségmodellhez'
    ,'113'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09823'
    ,Nev='Adatszolgáltatás a 2016. évi alaphálózati BULRIC költségmodellhez'
    ,Sorrend='113'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09825
SET @record_id = (select Id from KRT_KodTarak where Id='4ad42b32-1b90-454c-af58-d035b750c50f') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'4ad42b32-1b90-454c-af58-d035b750c50f'
    ,'naf09825'
    ,'Adatszolgáltatás a 2016. évi alaphálózati BULRIC költségmodellhez'
    ,'114'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09825'
    ,Nev='Adatszolgáltatás a 2016. évi alaphálózati BULRIC költségmodellhez'
    ,Sorrend='114'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09877
SET @record_id = (select Id from KRT_KodTarak where Id='49deb9a6-4956-4e5d-9d0c-89b7fab29331') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'49deb9a6-4956-4e5d-9d0c-89b7fab29331'
    ,'naf09877'
    ,'Kísérleti rádióengedély'
    ,'115'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09877'
    ,Nev='Kísérleti rádióengedély'
    ,Sorrend='115'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09879
SET @record_id = (select Id from KRT_KodTarak where Id='d5d7c47c-aebb-45cf-8da2-c5cfbaac1f7a') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'d5d7c47c-aebb-45cf-8da2-c5cfbaac1f7a'
    ,'naf09879'
    ,'Frekvenciakijelölés'
    ,'116'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09879'
    ,Nev='Frekvenciakijelölés'
    ,Sorrend='116'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09881
SET @record_id = (select Id from KRT_KodTarak where Id='04292230-09d0-4572-b598-09025bba5195') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'04292230-09d0-4572-b598-09025bba5195'
    ,'naf09881'
    ,'Rádióengedély rendezvény biztosítására'
    ,'117'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09881'
    ,Nev='Rádióengedély rendezvény biztosítására'
    ,Sorrend='117'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09883
SET @record_id = (select Id from KRT_KodTarak where Id='1d6af325-a9c4-4a02-be50-4f5d26194826') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'1d6af325-a9c4-4a02-be50-4f5d26194826'
    ,'naf09883'
    ,'Rádióberendezés nyilvántartásba vétel'
    ,'118'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09883'
    ,Nev='Rádióberendezés nyilvántartásba vétel'
    ,Sorrend='118'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09885
SET @record_id = (select Id from KRT_KodTarak where Id='7c576adf-8c40-4010-84c2-33ed61a7ed6d') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'7c576adf-8c40-4010-84c2-33ed61a7ed6d'
    ,'naf09885'
    ,'Általános rádióengedély'
    ,'119'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09885'
    ,Nev='Általános rádióengedély'
    ,Sorrend='119'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09897
SET @record_id = (select Id from KRT_KodTarak where Id='8a135b0b-c3a7-4576-bc32-3d6a7823dd44') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'8a135b0b-c3a7-4576-bc32-3d6a7823dd44'
    ,'naf09897'
    ,'árbevételi adatszolgáltatás (FDKR)'
    ,'120'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09897'
    ,Nev='árbevételi adatszolgáltatás (FDKR)'
    ,Sorrend='120'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf09977
SET @record_id = (select Id from KRT_KodTarak where Id='165b5680-0758-4702-8c1b-812b5087566e') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'165b5680-0758-4702-8c1b-812b5087566e'
    ,'naf09977'
    ,'Szolgáltatásminőségi adatszolgáltatás új űrlap -2017'
    ,'121'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf09977'
    ,Nev='Szolgáltatásminőségi adatszolgáltatás új űrlap -2017'
    ,Sorrend='121'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf10049
SET @record_id = (select Id from KRT_KodTarak where Id='a96d9023-8c91-4400-8b2f-1ee952106f9a') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'a96d9023-8c91-4400-8b2f-1ee952106f9a'
    ,'naf10049'
    ,'Egyetemes postai szolgáltatás -2017'
    ,'122'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf10049'
    ,Nev='Egyetemes postai szolgáltatás -2017'
    ,Sorrend='122'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf10051
SET @record_id = (select Id from KRT_KodTarak where Id='4db1ab87-8060-481a-8029-45153995cb55') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'4db1ab87-8060-481a-8029-45153995cb55'
    ,'naf10051'
    ,'Egyetemes postai szolgáltatást nem helyettesítő postai szolgáltatás -2017'
    ,'123'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf10051'
    ,Nev='Egyetemes postai szolgáltatást nem helyettesítő postai szolgáltatás -2017'
    ,Sorrend='123'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf10053
SET @record_id = (select Id from KRT_KodTarak where Id='3b2af6ba-d991-48eb-b313-c928a225c745') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'3b2af6ba-d991-48eb-b313-c928a225c745'
    ,'naf10053'
    ,'Egyetemes postai szolgáltatást helyettesítő postai szolgáltatás -2017'
    ,'124'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf10053'
    ,Nev='Egyetemes postai szolgáltatást helyettesítő postai szolgáltatás -2017'
    ,Sorrend='124'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf10058
SET @record_id = (select Id from KRT_KodTarak where Id='a7a5ee39-11a9-40bc-8cb5-3896b1a34341') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'a7a5ee39-11a9-40bc-8cb5-3896b1a34341'
    ,'naf10058'
    ,'Műsorterjesztési adatszolgáltatás web-es űrlap -2017'
    ,'125'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf10058'
    ,Nev='Műsorterjesztési adatszolgáltatás web-es űrlap -2017'
    ,Sorrend='125'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf10064
SET @record_id = (select Id from KRT_KodTarak where Id='50bc5141-be3f-4e75-b2d2-b99e96754f62') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'50bc5141-be3f-4e75-b2d2-b99e96754f62'
    ,'naf10064'
    ,'Műsorterjesztés XML állomány feltöltése 2017'
    ,'126'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf10064'
    ,Nev='Műsorterjesztés XML állomány feltöltése 2017'
    ,Sorrend='126'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf10738
SET @record_id = (select Id from KRT_KodTarak where Id='426e26b7-0a67-4d0c-b1ec-44e0bd9cee80') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'426e26b7-0a67-4d0c-b1ec-44e0bd9cee80'
    ,'naf10738'
    ,'Adatszolgáltatás a helyhez  kötött telefon piacelemzési eljárás lefolytatása érdekében 2017'
    ,'127'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf10738'
    ,Nev='Adatszolgáltatás a helyhez  kötött telefon piacelemzési eljárás lefolytatása érdekében 2017'
    ,Sorrend='127'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf10922
SET @record_id = (select Id from KRT_KodTarak where Id='7bc1f81d-bafd-40b5-8e77-08e5896bacaf') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'7bc1f81d-bafd-40b5-8e77-08e5896bacaf'
    ,'naf10922'
    ,'Bérelt vonali adatszolgáltatás – 2017'
    ,'128'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf10922'
    ,Nev='Bérelt vonali adatszolgáltatás – 2017'
    ,Sorrend='128'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf11038
SET @record_id = (select Id from KRT_KodTarak where Id='84fe3c57-1c5f-4c43-919b-5d6e1b5abb46') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'84fe3c57-1c5f-4c43-919b-5d6e1b5abb46'
    ,'naf11038'
    ,'naf11038'
    ,'129'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf11038'
    ,Nev='naf11038'
    ,Sorrend='129'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END


-- naf11298
SET @record_id = (select Id from KRT_KodTarak where Id='f6e24665-822a-4ead-affe-86c3c308b209') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'f6e24665-822a-4ead-affe-86c3c308b209'
    ,'naf11298'
    ,'naf11298'
    ,'130'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='naf11298'
    ,Nev='naf11298'
    ,Sorrend='130'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END



go

