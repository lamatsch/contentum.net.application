declare @modulId uniqueidentifier
set @modulId = 'AA6D9944-40C0-40CD-8B75-9116B2E46C3D'

IF NOT EXISTS (Select 1 FROM INT_Modulok where Id = @modulId)
BEGIN
	INSERT INTO INT_Modulok
	(Id, Nev)
	VALUES
	(@modulId,'eUzenet')
END


declare @recordId uniqueidentifier
set @recordId = '7B452F3A-C485-473B-B905-629D4333E7EB'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @recordId)
BEGIN
	INSERT INTO INT_Parameterek
	(Id, Modul_id, Nev, Ertek, Karbantarthato)
	VALUES
	(@recordId, @modulId, 'eUzenetForras', 'ELHISZ','1')
END