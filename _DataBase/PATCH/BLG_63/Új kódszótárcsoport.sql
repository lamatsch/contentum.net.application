--begin tran

DECLARE @RC int
DECLARE @Id uniqueidentifier
DECLARE @Org uniqueidentifier
DECLARE @KodCsoport_Id uniqueidentifier
DECLARE @ObjTip_Id_AdatElem uniqueidentifier
DECLARE @Obj_Id uniqueidentifier
DECLARE @Kod nvarchar(64)
DECLARE @Nev nvarchar(400)
DECLARE @RovidNev nvarchar(10)
DECLARE @Egyeb nvarchar(4000)
DECLARE @Modosithato char(1)
DECLARE @Sorrend nvarchar(100)
DECLARE @Ver int
DECLARE @Note nvarchar(4000)
DECLARE @Stat_id uniqueidentifier
DECLARE @ErvKezd datetime
DECLARE @ErvVege datetime
DECLARE @Letrehozo_id uniqueidentifier
DECLARE @LetrehozasIdo datetime
DECLARE @Modosito_id uniqueidentifier
DECLARE @ModositasIdo datetime
DECLARE @Zarolo_id uniqueidentifier
DECLARE @ZarolasIdo datetime
DECLARE @Tranz_id uniqueidentifier
DECLARE @UIAccessLog_id uniqueidentifier
DECLARE @UpdatedColumns xml
DECLARE @ResultUid uniqueidentifier
declare @KodTarak_Id_KodcsoportTipus uniqueidentifier,
               @Hossz     int  ,
                @Csoport_Id_Tulaj     uniqueidentifier,
                @Leiras     Nvarchar(4000) ,
	            @BesorolasiSema     char(1),
                @KiegAdat     char(1)  ,
                @KiegMezo     Nvarchar(400) ,
                @KiegAdattipus     char(1)  ,
                @KiegSzotar     Nvarchar(100)  

-----------------------------------------------------------
SET @Modosithato = 0
SELECT @Letrehozo_id = id, @Org = Org FROM krt_felhasznalok WHERE UserNev = 'Admin'
-----------------------------------------------------------

SELECT @KodCsoport_Id = id from KRT_KodCsoportok where Id = 'B04B5BCF-BDA6-4DDE-8AEA-2B89391F6802'		-- select newId()

SET @Kod = 'TOMEGES_ALAIRAS_FOLYAMAT_STATUS'
SET @Nev = 'T�meges al��r�si folyamat st�tuszok'
SET @BesorolasiSema = '0'

IF	@KodCsoport_Id is null
begin

	exec [dbo].[sp_KRT_KodCsoportokInsert]
	 @KodCsoport_Id
	,@Org
	,@KodTarak_Id_KodcsoportTipus
	,@Kod
	,@Nev
	,@Modosithato
	,@Hossz
	,@Csoport_Id_Tulaj
	,@Leiras
	,@BesorolasiSema
	,@KiegAdat
	,@KiegMezo
	,@KiegAdattipus
	,@KiegSzotar
	,@Ver
	,@Note
	,@Stat_id
	,@ErvKezd
	,@ErvVege
	,@Letrehozo_id
	,@LetrehozasIdo
	,@Modosito_id
	,@ModositasIdo
	,@Zarolo_id
	,@ZarolasIdo
	,@Tranz_id
	,@UIAccessLog_id
	,@UpdatedColumns
	,@ResultUid OUTPUT

	SET @KodCsoport_Id = @ResultUid
END
ELSE
BEGIN
	exec [dbo].[sp_KRT_KodCsoportokUpdate]
	 @KodCsoport_Id
	,@Org
	,@KodTarak_Id_KodcsoportTipus
	,@Kod
	,@Nev
	,@Modosithato
	,@Hossz
	,@Csoport_Id_Tulaj
	,@Leiras
	,@BesorolasiSema
	,@KiegAdat
	,@KiegMezo
	,@KiegAdattipus
	,@KiegSzotar
	,@Ver
	,@Note
	,@Stat_id
	,@ErvKezd
	,@ErvVege
	,@Letrehozo_id
	,@LetrehozasIdo
	,@Modosito_id
	,@ModositasIdo
	,@Zarolo_id
	,@ZarolasIdo
	,@Tranz_id
	,@UIAccessLog_id
	,@UpdatedColumns
END

SELECT @Id  = Id FROM KRT_Kodtarak where Id = '16222A1E-F5F5-4170-BB1B-86F18DD832DF' --select newId()

SET @Kod = 'ALAIRAS_FOLYAMAT_SIKERES'
SET @Nev = 'Sikeres al��r�s'
SET @RovidNev = 'Az al��r�si folyamat sikeres'

IF @Id IS NULL
BEGIN

	EXECUTE  [dbo].[sp_KRT_KodTarakInsert] 
	   @Id
	  ,@Org
	  ,@KodCsoport_Id
	  ,@ObjTip_Id_AdatElem
	  ,@Obj_Id
	  ,@Kod
	  ,@Nev
	  ,@RovidNev
	  ,@Egyeb
	  ,@Modosithato
	  ,@Sorrend
	  ,@Ver
	  ,@Note
	  ,@Stat_id
	  ,@ErvKezd
	  ,@ErvVege
	  ,@Letrehozo_id
	  ,@LetrehozasIdo
	  ,@Modosito_id
	  ,@ModositasIdo
	  ,@Zarolo_id
	  ,@ZarolasIdo
	  ,@Tranz_id
	  ,@UIAccessLog_id
	  ,@UpdatedColumns
	  ,@ResultUid OUTPUT
END
ELSE
BEGIN
	EXECUTE  [dbo].[sp_KRT_KodTarakUpdate] 
	   @Id
	  ,@Org
	  ,@KodCsoport_Id
	  ,@ObjTip_Id_AdatElem
	  ,@Obj_Id
	  ,@Kod
	  ,@Nev
	  ,@RovidNev
	  ,@Egyeb
	  ,@Modosithato
	  ,@Sorrend
	  ,@Ver
	  ,@Note
	  ,@Stat_id
	  ,@ErvKezd
	  ,@ErvVege
	  ,@Letrehozo_id
	  ,@LetrehozasIdo
	  ,@Modosito_id
	  ,@ModositasIdo
	  ,@Zarolo_id
	  ,@ZarolasIdo
	  ,@Tranz_id
	  ,@UIAccessLog_id
	  ,@UpdatedColumns
END

SELECT @Id  = Id FROM KRT_Kodtarak where Id = 'F5C145DB-89CD-48F5-AB9C-4088C1F40ED3' --select newId()

SET @Kod = 'ALAIRAS_FOLYAMAT_FOLYAMATBAN_VAN'
SET @Nev = 'Az al��r�s folyamatban van'
SET @RovidNev = 'Az al��r�si folyamat folyamatban van'

IF @Id IS NULL
BEGIN

	EXECUTE  [dbo].[sp_KRT_KodTarakInsert] 
	   @Id
	  ,@Org
	  ,@KodCsoport_Id
	  ,@ObjTip_Id_AdatElem
	  ,@Obj_Id
	  ,@Kod
	  ,@Nev
	  ,@RovidNev
	  ,@Egyeb
	  ,@Modosithato
	  ,@Sorrend
	  ,@Ver
	  ,@Note
	  ,@Stat_id
	  ,@ErvKezd
	  ,@ErvVege
	  ,@Letrehozo_id
	  ,@LetrehozasIdo
	  ,@Modosito_id
	  ,@ModositasIdo
	  ,@Zarolo_id
	  ,@ZarolasIdo
	  ,@Tranz_id
	  ,@UIAccessLog_id
	  ,@UpdatedColumns
	  ,@ResultUid OUTPUT
END
ELSE
BEGIN
	EXECUTE  [dbo].[sp_KRT_KodTarakUpdate] 
	   @Id
	  ,@Org
	  ,@KodCsoport_Id
	  ,@ObjTip_Id_AdatElem
	  ,@Obj_Id
	  ,@Kod
	  ,@Nev
	  ,@RovidNev
	  ,@Egyeb
	  ,@Modosithato
	  ,@Sorrend
	  ,@Ver
	  ,@Note
	  ,@Stat_id
	  ,@ErvKezd
	  ,@ErvVege
	  ,@Letrehozo_id
	  ,@LetrehozasIdo
	  ,@Modosito_id
	  ,@ModositasIdo
	  ,@Zarolo_id
	  ,@ZarolasIdo
	  ,@Tranz_id
	  ,@UIAccessLog_id
	  ,@UpdatedColumns
END

SELECT @Id  = Id FROM KRT_Kodtarak where Id = '6BA92BBA-6A46-48D1-BAA9-CD0B72DB1904' --select newId()

SET @Kod = 'ALAIRAS_FOLYAMAT_SIKERTELEN'
SET @Nev = 'Az al��r�s sikertelen volt'
SET @RovidNev = 'Az al��r�si folyamat sikertelen volt'

IF @Id IS NULL
BEGIN

	EXECUTE  [dbo].[sp_KRT_KodTarakInsert] 
	   @Id
	  ,@Org
	  ,@KodCsoport_Id
	  ,@ObjTip_Id_AdatElem
	  ,@Obj_Id
	  ,@Kod
	  ,@Nev
	  ,@RovidNev
	  ,@Egyeb
	  ,@Modosithato
	  ,@Sorrend
	  ,@Ver
	  ,@Note
	  ,@Stat_id
	  ,@ErvKezd
	  ,@ErvVege
	  ,@Letrehozo_id
	  ,@LetrehozasIdo
	  ,@Modosito_id
	  ,@ModositasIdo
	  ,@Zarolo_id
	  ,@ZarolasIdo
	  ,@Tranz_id
	  ,@UIAccessLog_id
	  ,@UpdatedColumns
	  ,@ResultUid OUTPUT
END
ELSE
BEGIN
	EXECUTE  [dbo].[sp_KRT_KodTarakUpdate] 
	   @Id
	  ,@Org
	  ,@KodCsoport_Id
	  ,@ObjTip_Id_AdatElem
	  ,@Obj_Id
	  ,@Kod
	  ,@Nev
	  ,@RovidNev
	  ,@Egyeb
	  ,@Modosithato
	  ,@Sorrend
	  ,@Ver
	  ,@Note
	  ,@Stat_id
	  ,@ErvKezd
	  ,@ErvVege
	  ,@Letrehozo_id
	  ,@LetrehozasIdo
	  ,@Modosito_id
	  ,@ModositasIdo
	  ,@Zarolo_id
	  ,@ZarolasIdo
	  ,@Tranz_id
	  ,@UIAccessLog_id
	  ,@UpdatedColumns
END

select	cs.kod,cs.Nev,kt.Kod,kt.Nev 
from	KRT_KodTarak		kt
join	KRT_Kodcsoportok	cs on cs.id = kt.KodCsoport_Id
where	cs.id = @KodCsoport_Id


--rollback