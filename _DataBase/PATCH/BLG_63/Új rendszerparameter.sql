﻿-- pki_filetypes rendszerparameter	 : azon file típusok, amelyekre működik az elektronikus aláírás
DECLARE @record_id uniqueidentifier 
SET @record_id = (select Id from KRT_Parameterek
			      where Id='1CE6E4BC-3CB2-4B2B-AFD6-2B2B5EEEC7D4') -- select newId()

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 

	insert into KRT_Parameterek(
	Tranz_id
	,Stat_id
	,Org
	,Id
	,Nev
	,Ertek
	,Karbantarthato
	) 
	values 
	(
	 null
	,null
	,'450b510a-7caa-46b0-83e3-18445c0c53a9'
	,'1CE6E4BC-3CB2-4B2B-AFD6-2B2B5EEEC7D4'
	,'PKI_FILETYPES'
	,'PDF'
	,'1'
	); 
END 
ELSE 
BEGIN 
	Print 'UPDATE'

	UPDATE KRT_Parameterek
	SET 
	 Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	,Nev='PKI_FILETYPES'
	,Ertek='PDF'
	,Karbantarthato='1'
	WHERE Id=@record_id 
END

SELECT * FROM KRT_Parameterek WHERE ID = @record_id