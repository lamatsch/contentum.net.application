declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @OrgKod nvarchar(100)
set @OrgKod = (select Kod from KRT_Orgok where Id = @Org)

IF (@OrgKod = 'NMHH')
BEGIN
	print 'Add_Statisztika_Metaadatok kezdete'

	declare @UgyObjTip_Id uniqueidentifier
	set @UgyObjTip_Id = 'A04417A6-54AC-4AF1-9B63-5BABF0203D42'

	declare @IratObjTip_Id uniqueidentifier
	set @IratObjTip_Id = 'AA5E7BBA-96A0-4C17-8709-06A6D297E107'

	declare @DefinicioTipus nvarchar(64)
	set @DefinicioTipus = 'B3'

	declare @Letrehozo_Id uniqueidentifier
	set @Letrehozo_Id = '54E861A5-36ED-44CA-BAA7-C287D125B309'

	declare @now datetime
	set @now = GETDATE()

	declare @Tranz_id uniqueidentifier
	set @Tranz_id = '09940A31-4611-4038-BF17-C327E72D0111'

	declare @ugyStat_Id uniqueidentifier
	set @ugyStat_Id = '74861630-2A05-4F57-9288-37BAE6225D0F'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaDefinicio WHERE Id = @ugyStat_Id)
	BEGIN
		print 'insert - �gyiratt�pus statisztika'

		INSERT INTO EREC_Obj_MetaDefinicio
		(
			[Id]
		   ,[Org]
		   ,[Objtip_Id]
		   ,[DefinicioTipus]
		   ,[ContentType]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@ugyStat_Id
		   ,@Org
		   ,@UgyObjTip_Id
		   ,@DefinicioTipus
		   ,'�gyiratt�pus statisztika'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	--ELSE
	--BEGIN
	--	print 'update - �gyiratt�pus statisztika'

	--	UPDATE EREC_Obj_MetaDefinicio
	--		set DefinicioTipus = @DefinicioTipus
	--	WHERE Id = @ugyStat_Id
	--END

	declare @iratStat_Id uniqueidentifier
	set @iratStat_Id = '786E32A4-AC18-44DB-894B-F89C70E3772F'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaDefinicio WHERE Id = @iratStat_Id)
	BEGIN
		print 'insert - Iratt�pus statisztika'

		INSERT INTO EREC_Obj_MetaDefinicio
		(
			[Id]
		   ,[Org]
		   ,[Objtip_Id]
		   ,[DefinicioTipus]
		   ,[ContentType]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@iratStat_Id
		   ,@Org
		   ,@IratObjTip_Id
		   ,@DefinicioTipus
		   ,'Iratt�pus statisztika'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	--ELSE
	--BEGIN
	--	print 'update - Iratt�pus statisztika'

	--	UPDATE EREC_Obj_MetaDefinicio
	--		set DefinicioTipus = @DefinicioTipus
	--	WHERE Id = @iratStat_Id
	--END

	--T�rgyszavak

	-- �gyiratt�pus statisztik
	declare @Tipus char(1)
	set @Tipus = '1'

	declare @ControlTypeSource nvarchar(64)
	set @ControlTypeSource = '~/Component/FuggoKodtarakDropDownList.ascx'

	declare @targyszoId uniqueidentifier
	set @targyszoId = 'A92E4382-6D67-4A58-98B2-FAC82A138037'

	declare @targyszoIdFt1 uniqueidentifier
	set @targyszoIdFt1 = @targyszoId

	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId)
	BEGIN
		print 'insert - FT1'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId
		   ,@Org
		   ,@Tipus
		   ,'FT1'
		   ,'FT1'
		   ,@ControlTypeSource
		   ,'UGYSTATISZTIKA_T1'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	--ELSE
	--BEGIN
	--	print 'update - FT1'

	--	UPDATE EREC_TargySzavak
	--		set ControlTypeSource = @ControlTypeSource
	--	WHERE Id = @targyszoId
	--END

	-- EREC_Obj_MetaAdatai
	declare @recordId uniqueidentifier
	set @recordId = 'D785667E-56A1-4C0E-8971-BC733D8F5178'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - �gyiratt�pus statisztika - FT1'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@ugyStat_Id
		   ,@targyszoId
		   ,1
		   ,'0'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	set @targyszoId = 'C148F027-8BDD-4B0C-971B-C94CD4322002'

	declare @targyszoIdFt2 uniqueidentifier
	set @targyszoIdFt2 = @targyszoId

	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId)
	BEGIN
		print 'insert - FT2'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Targyszo_Id_Parent]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId
		   ,@Org
		   ,@Tipus
		   ,'FT2'
		   ,'FT2'
		   ,@ControlTypeSource
		   ,'UGYSTATISZTIKA_T2'
		   ,@targyszoIdFt1
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	--ELSE
	--BEGIN
	--	print 'update - FT2'

	--	UPDATE EREC_TargySzavak
	--		set ControlTypeSource = @ControlTypeSource,
	--			Targyszo_Id_Parent = @targyszoIdFt1
	--	WHERE Id = @targyszoId
	--END

	-- EREC_Obj_MetaAdatai
	set @recordId = '4C4C14DB-4B27-494A-928D-A65765603C61'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - �gyiratt�pus statisztika - FT2'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@ugyStat_Id
		   ,@targyszoId
		   ,2
		   ,'0'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	set @targyszoId = '4CAE4517-782F-47E1-92F8-8BB45A5406D7'

	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId)
	BEGIN
		print 'insert - FT3'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Targyszo_Id_Parent]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId
		   ,@Org
		   ,@Tipus
		   ,'FT3'
		   ,'FT3'
		   ,@ControlTypeSource
		   ,'UGYSTATISZTIKA_T3'
		   ,@targyszoIdFt2
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	--ELSE
	--BEGIN
	--	print 'update - FT3'

	--	UPDATE EREC_TargySzavak
	--		set ControlTypeSource = @ControlTypeSource,
	--			Targyszo_Id_Parent = @targyszoIdFt2
	--	WHERE Id = @targyszoId
	--END

	-- EREC_Obj_MetaAdatai
	set @recordId = 'F62013A6-CA9B-496C-9A7F-C5C5AD93093D'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - �gyiratt�pus statisztika - FT3'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@ugyStat_Id
		   ,@targyszoId
		   ,3
		   ,'0'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	--Iratt�pus statisztika
	set @targyszoId = '2C8777A8-8C6F-4F1F-AF16-2F91407FA727'
	declare @targyszoIdIt1 uniqueidentifier
	set @targyszoIdIt1 = @targyszoId

	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId)
	BEGIN
		print 'insert - IT1'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId
		   ,@Org
		   ,@Tipus
		   ,'IT1'
		   ,'IT1'
		   ,@ControlTypeSource
		   ,'IRATSTATISZTIKA_T1'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	--ELSE
	--BEGIN
	--	print 'update - IT1'

	--	UPDATE EREC_TargySzavak
	--		set ControlTypeSource = @ControlTypeSource
	--	WHERE Id = @targyszoId
	--END

	-- EREC_Obj_MetaAdatai
	set @recordId = '8C9C82DB-5E63-4BE7-B4E2-C73BE79904A0'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Iratt�pus statisztika - IT1'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@iratStat_Id
		   ,@targyszoId
		   ,1
		   ,'0'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	set @targyszoId = '60B88389-08C5-4E03-B72C-8DD0B0F76ACF'
	declare @targyszoIdIt2 uniqueidentifier
	set @targyszoIdIt2 = @targyszoId

	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId)
	BEGIN
		print 'insert - IT2'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Targyszo_Id_Parent]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId
		   ,@Org
		   ,@Tipus
		   ,'IT2'
		   ,'IT2'
		   ,@ControlTypeSource
		   ,'IRATSTATISZTIKA_T2'
		   , @targyszoIdIt1
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	--ELSE
	--BEGIN
	--	print 'update - IT2'

	--	UPDATE EREC_TargySzavak
	--		set ControlTypeSource = @ControlTypeSource,
	--			Targyszo_Id_Parent = @targyszoIdIt1
	--	WHERE Id = @targyszoId
	--END

	-- EREC_Obj_MetaAdatai
	set @recordId = '1B8D343B-D556-4220-AED5-F44FA01443F3'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Iratt�pus statisztika - IT2'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@iratStat_Id
		   ,@targyszoId
		   ,2
		   ,'0'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	set @targyszoId = 'DB174024-C5F4-433D-9D87-29EB30582E69'

	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId)
	BEGIN
		print 'insert - IT3'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Targyszo_Id_Parent]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId
		   ,@Org
		   ,@Tipus
		   ,'IT3'
		   ,'IT3'
		   ,@ControlTypeSource
		   ,'IRATSTATISZTIKA_T3'
		   ,@targyszoIdIt2
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	--ELSE
	--BEGIN
	--	print 'update - IT3'

	--	UPDATE EREC_TargySzavak
	--		set ControlTypeSource = @ControlTypeSource,
	--			Targyszo_Id_Parent = @targyszoIdIt2
	--	WHERE Id = @targyszoId
	--END


	-- EREC_Obj_MetaAdatai
	set @recordId = 'B2E48666-973A-4E53-A5F1-EBCD95747393'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Iratt�pus statisztika - IT3'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@iratStat_Id
		   ,@targyszoId
		   ,3
		   ,'0'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- OBJMETADEFINICIO_TIPUS
	DECLARE @KodCsoport_Id uniqueidentifier
	set @KodCsoport_Id = (select Id from KRT_Kodcsoportok where Kod = 'OBJMETADEFINICIO_TIPUS')

	SET @recordId = '0AFFBE4E-5DFB-4294-B936-F8C5091C40AC'


	IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE Id = @recordId)
	BEGIN 
    
		Print 'insert - OBJMETADEFINICIO_TIPUS.B3' 
		insert into KRT_KodTarak(
		KodCsoport_Id
		,Tranz_id
		,Org
		,Id
		,Kod
		,Nev
		,Sorrend
		,Modosithato
		) values (
		@KodCsoport_Id
		,@Tranz_id
		,@Org
		, @recordId
		,'B3'
		,'Objektum statisztika'
		,'31'
		,'1'
		); 
     
       
	  END 
	 -- ELSE 
	 -- BEGIN 
		--Print 'update - OBJMETADEFINICIO_TIPUS.B3'
		--UPDATE KRT_KodTarak
		--SET 
		--KodCsoport_Id=@KodCsoport_Id
		--,Tranz_id=@Tranz_id
		--,Org=@Org
		--,Id=@recordId
		--,Kod='B3'
		--,Nev='Objektum statisztika'
		--,Sorrend='31'
		--,Modosithato='1'
		--WHERE Id=@recordId 
       
	 -- END

	-- �GYTIPUS_STATISZTIKA
	SET @recordId = '1C519435-7757-4B88-9104-A02558E63CC7'

	declare @Ertek nvarchar(400)

	IF @OrgKod = 'NMHH'
		set @Ertek = '1'
	ELSE
		set @Ertek = '0'

	IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE Id = @recordId)
	BEGIN 
		Print 'insert - UGYTIPUS_STATISZTIKA - '  + @Ertek
		 insert into KRT_Parameterek(
			 Tranz_id
			,Org
			,Id
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_Id
			,@Org
			,@recordId
			,'UGYTIPUS_STATISZTIKA'
			,@Ertek
			,'1'
			,'�gyirat- �s iratt�pus statisztika megjelenik-e.'
			); 
	 END 
	 --ELSE 
	 --BEGIN 
		-- Print 'update - UGYTIPUS_STATISZTIKA - '  + @Ertek
		-- UPDATE KRT_Parameterek
		-- SET 
		--	Org=@Org
		--	,Id=@recordId
		--	,Nev='UGYTIPUS_STATISZTIKA'
		--	,Ertek=@Ertek
		--	,Karbantarthato='1'
		--	,Note='�gyirat- �s iratt�pus statisztika megjelenik-e.'
		-- WHERE Id=@recordId 
	 --END

	--CONTROLTYPE_SOURCE

	set @KodCsoport_Id = (select Id from KRT_Kodcsoportok where Kod = 'CONTROLTYPE_SOURCE')
	SET @recordId = 'F94B0E86-6614-444C-BB22-3EE3AFFE3819'


	IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE Id = @recordId)
	BEGIN 
    
		Print 'insert - CONTROLTYPE_SOURCE.FuggoKodtarakDropDownList' 
		insert into KRT_KodTarak(
		KodCsoport_Id
		,Tranz_id
		,Org
		,Id
		,Kod
		,Nev
		,Sorrend
		,Modosithato
		) values (
		@KodCsoport_Id
		,@Tranz_id
		,@Org
		, @recordId
		,'~/Component/FuggoKodtarakDropDownList.ascx'
		,'F�gg� K�dv�laszt� leg�rd�l� lista (FuggoKodtarakDropDownList)'
		,'14'
		,'1'
		); 
     
      
	  END 
	 -- ELSE 
	 -- BEGIN 
		--Print 'update - CONTROLTYPE_SOURCE.FuggoKodtarakDropDownList'
		--UPDATE KRT_KodTarak
		--SET 
		--KodCsoport_Id=@KodCsoport_Id
		--,Tranz_id=@Tranz_id
		--,Org=@Org
		--,Id=@recordId
		--,Kod='~/Component/FuggoKodtarakDropDownList.ascx'
		--,Nev='F�gg� K�dv�laszt� leg�rd�l� lista (FuggoKodtarakDropDownList)'
		--,Sorrend='14'
		--,Modosithato='1'
		--WHERE Id=@recordId 
       
	 -- END

	  print 'Add_Statisztika_Metaadatok vege'
END

