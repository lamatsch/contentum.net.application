/*************
* POSTAZAS_IRANYA k�dcsoportban a k�dt�relemek �tnevez�se (csak NMHH-ban, FPH-ban nem)
* - 0: 'Bels�' --> 'Kimen�'
* - 2: 'Kimen�' --> 'Bels�'
**************/

begin tran

	DECLARE @org_kod nvarchar(100),
			@org_Id uniqueidentifier
		
	set @org_Id = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
		 
	SET @org_kod = (select kod from KRT_Orgok
				where id=@org_Id) 

	if (@org_kod = 'NMHH')
	begin

	/*
		select kt.*
		from KRT_KodCsoportok kcs
		 join KRT_KodTarak kt on kt.KodCsoport_Id = kcs.Id
		where kcs.Kod = 'POSTAZAS_IRANYA'
	*/

		update KRT_KodTarak
		SET Nev = 'Kimen�'
		from KRT_KodCsoportok kcs
		 join KRT_KodTarak kt on kt.KodCsoport_Id = kcs.Id
		where kcs.Kod = 'POSTAZAS_IRANYA' and kt.Kod = '0'

		update KRT_KodTarak
		SET Nev = 'Bels�'
		from KRT_KodCsoportok kcs
		 join KRT_KodTarak kt on kt.KodCsoport_Id = kcs.Id
		where kcs.Kod = 'POSTAZAS_IRANYA' and kt.Kod = '2'

	end
	else
	begin
		print 'POSTAZAS_IRANYA k�dt�relemek: nincs m�dos�t�s (csak NMHH-ban kell)'
	end

commit tran