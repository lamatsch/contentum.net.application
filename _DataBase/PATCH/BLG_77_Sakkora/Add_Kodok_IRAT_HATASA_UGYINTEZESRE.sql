﻿print 'Add_Kodok_IRAT_HATASA_UGYINTEZESRE.sql'

DECLARE @Tanz_Id uniqueidentifier = 'f60ad910-541a-470d-b888-da5a6a061668'
declare @Stat_id uniqueidentifier = 'a0848405-e664-4e79-8fab-cfeca4a290af'
declare @Org uniqueidentifier='450b510a-7caa-46b0-83e3-18445c0c53a9'

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_KodCsoportok
			where Id='8DECBC3D-6466-4918-8FB1-ED0E402E6CD4') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
		insert into KRT_KodCsoportok(
		Tranz_id
		,Stat_id
		,KodTarak_Id_KodcsoportTipus
		,BesorolasiSema
		,Id
		,Kod
		,Nev
		,Modosithato
		) values (
		@Tanz_Id
		,@Stat_id
		,'a54af681-ada2-420f-860f-826257e95932'
		,'0'
		,'8DECBC3D-6466-4918-8FB1-ED0E402E6CD4'
		,'IRAT_HATASA_UGYINTEZESRE'
		,'Irat hatása az ügyintézésre (sakkóra)'
		,'1'
		); 
		 
			 
	END 
	ELSE 
	BEGIN 
		Print 'UPDATE'
		UPDATE KRT_KodCsoportok
		SET 
		Tranz_id=@Tanz_Id
		,Stat_id=@Stat_id
		,KodTarak_Id_KodcsoportTipus='a54af681-ada2-420f-860f-826257e95932'
		,BesorolasiSema='0'
		,Id=@record_id
		,Kod='IRAT_HATASA_UGYINTEZESRE'
		,Nev='Irat hatása az ügyintézésre (sakkóra)'
		,Modosithato='1'	
		WHERE Id=@record_id 
		 
			 
	END



-- új kódok
declare @KodCsoport_Id uniqueidentifier = '8DECBC3D-6466-4918-8FB1-ED0E402E6CD4'

-- A hatósági ügyintézési időre nincs hatással
SET @record_id = (select Id from KRT_KodTarak where Id='a970e4fb-106f-45e9-809e-3a921ca18812') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
	,Egyeb
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'a970e4fb-106f-45e9-809e-3a921ca18812'
    ,'0'
    ,'A hatósági ügyintézési időre nincs hatással'
    ,'0'
    ,'1'
	,'ON_PROGRESS,START,STOP,PAUSE|ON_PROGRESS'
    ); 
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='0'
    ,Nev='A hatósági ügyintézési időre nincs hatással'
    ,Sorrend='0'
    ,Modosithato='1'
	,Egyeb = 'ON_PROGRESS,START,STOP,PAUSE|ON_PROGRESS'
    WHERE Id=@record_id 
  END


-- A hatósági ügyintézési idő indul/folytatódik
SET @record_id = (select Id from KRT_KodTarak where Id='b5e6226c-24da-4d23-8bd5-ab2bf543cd31') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
	,Egyeb
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'b5e6226c-24da-4d23-8bd5-ab2bf543cd31'
    ,'1'
    ,'A hatósági ügyintézési idő indul/folytatódik'
    ,'1'
    ,'1'
	,'ON_PROGRESS|ON_PROGRESS'
    ); 
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='1'
    ,Nev='A hatósági ügyintézési idő indul/folytatódik'
    ,Sorrend='1'
    ,Modosithato='1'
	,Egyeb = 'ON_PROGRESS|ON_PROGRESS'
    WHERE Id=@record_id 
       
  END


-- A hatósági ügyintézési idő megáll
SET @record_id = (select Id from KRT_KodTarak where Id='86a5dc9a-c604-4178-afc7-d70e73868cc1') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
	,Egyeb
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'86a5dc9a-c604-4178-afc7-d70e73868cc1'
    ,'2'
    ,'A hatósági ügyintézési idő megáll'
    ,'2'
    ,'1'
	,'ON_PROGRESS|PAUSE'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='2'
    ,Nev='A hatósági ügyintézési idő megáll'
    ,Sorrend='2'
    ,Modosithato='1'
	,Egyeb = 'ON_PROGRESS|PAUSE'
    WHERE Id=@record_id 
       
  END


-- Hatósági ügyintézési idő vége
SET @record_id = (select Id from KRT_KodTarak where Id='8ad1623b-041c-475f-88ae-049a735a5dfa') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
	,Egyeb
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'8ad1623b-041c-475f-88ae-049a735a5dfa'
    ,'3'
    ,'Hatósági ügyintézési idő vége'
    ,'3'
    ,'1'
	,'ON_PROGRESS,PAUSE|STOP'
    ); 
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='3'
    ,Nev='Hatósági ügyintézési idő vége'
    ,Sorrend='3'
    ,Modosithato='1'
	,Egyeb = 'ON_PROGRESS,PAUSE|STOP'
    WHERE Id=@record_id 
       
  END


-- Az elsőfok fellebbezési kérelem alapján módosít/visszavon
SET @record_id = (select Id from KRT_KodTarak where Id='449a11cd-c83a-4f58-8fce-b168a2b38635') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
	,Egyeb
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'449a11cd-c83a-4f58-8fce-b168a2b38635'
    ,'4'
    ,'Az elsőfok fellebbezési kérelem alapján módosít/visszavon'
    ,'4'
    ,'1'
	,null
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='4'
    ,Nev='Az elsőfok fellebbezési kérelem alapján módosít/visszavon'
    ,Sorrend='4'
    ,Modosithato='1'
	,Egyeb = 'ON_PROGRESS|ON_PROGRESS'
    WHERE Id=@record_id 
       
  END


-- Másodfokú hatósági ügyintézési idő indul/folytatódik
SET @record_id = (select Id from KRT_KodTarak where Id='5df1584a-05c5-4b6c-ad53-db05444c83e1') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
	,Egyeb
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'5df1584a-05c5-4b6c-ad53-db05444c83e1'
    ,'5'
    ,'Másodfokú hatósági ügyintézési idő indul/folytatódik'
    ,'5'
    ,'1'
	,'ON_PROGRESS|ON_PROGRESS'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='5'
    ,Nev='Másodfokú hatósági ügyintézési idő indul/folytatódik'
    ,Sorrend='5'
    ,Modosithato='1'
	,Egyeb = 'ON_PROGRESS|ON_PROGRESS'
    WHERE Id=@record_id 
       
  END


-- Másodfokú hatósági ügyintézési idő megáll
SET @record_id = (select Id from KRT_KodTarak where Id='c6793f21-62d8-489b-a3ae-235680639037') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
	,Egyeb
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'c6793f21-62d8-489b-a3ae-235680639037'
    ,'6'
    ,'Másodfokú hatósági ügyintézési idő megáll'
    ,'6'
    ,'1'
	,'ON_PROGRESS|PAUSE'
    ); 
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='6'
    ,Nev='Másodfokú hatósági ügyintézési idő megáll'
    ,Sorrend='6'
    ,Modosithato='1'
	,Egyeb = 'ON_PROGRESS|PAUSE'
    WHERE Id=@record_id 
       
  END


-- Másodfokú hatósági ügyintézési idő vége
SET @record_id = (select Id from KRT_KodTarak where Id='a7ca3503-d543-4879-915c-c38aa55e017b') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
	,Egyeb
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'a7ca3503-d543-4879-915c-c38aa55e017b'
    ,'7'
    ,'Másodfokú hatósági ügyintézési idő vége'
    ,'7'
    ,'1'
	,'ON_PROGRESS,PAUSE|STOP'
    );   
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='7'
    ,Nev='Másodfokú hatósági ügyintézési idő vége'
    ,Sorrend='7'
    ,Modosithato='1'
	,Egyeb = 'ON_PROGRESS,PAUSE|STOP'
    WHERE Id=@record_id 
       
  END


-- Hatósági ügyintézési idő indul/újraindul és vége
SET @record_id = (select Id from KRT_KodTarak where Id='2a72aad6-6b55-4dbc-8328-9bc57d2f423d') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
	,Egyeb
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'2a72aad6-6b55-4dbc-8328-9bc57d2f423d'
    ,'8'
    ,'Hatósági ügyintézési idő indul/újraindul és vége'
    ,'8'
    ,'1'
	,'ON_PROGRESS,PAUSE|STOP'
    );    
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='8'
    ,Nev='Hatósági ügyintézési idő indul/újraindul és vége'
    ,Sorrend='8'
    ,Modosithato='1'
	,Egyeb = 'ON_PROGRESS,PAUSE|STOP'
    WHERE Id=@record_id 
       
  END


-- A hatósági ügyintézési idő indul/újraindul és megáll
SET @record_id = (select Id from KRT_KodTarak where Id='5e807261-4f21-4630-81e9-e3cf57f7d80c') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
	,Egyeb
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'5e807261-4f21-4630-81e9-e3cf57f7d80c'
    ,'9'
    ,'A hatósági ügyintézési idő indul/újraindul és megáll'
    ,'9'
    ,'1'
	,'ON_PROGRESS|PAUSE'
    );   
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='9'
    ,Nev='A hatósági ügyintézési idő indul/újraindul és megáll'
    ,Sorrend='9'
    ,Modosithato='1'
	,Egyeb = 'ON_PROGRESS|PAUSE'
    WHERE Id=@record_id 
       
  END


-- Másodfokú hatósági ügyintézési idő indul/újraindul és vége
SET @record_id = (select Id from KRT_KodTarak where Id='fc351f3e-bbdf-4f8d-9a85-103c2e507ca0') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
	,Egyeb
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'fc351f3e-bbdf-4f8d-9a85-103c2e507ca0'
    ,'10'
    ,'Másodfokú hatósági ügyintézési idő indul/újraindul és vége'
    ,'10'
    ,'1'
	,'ON_PROGRESS,PAUSE|STOP'
    );  
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='10'
    ,Nev='Másodfokú hatósági ügyintézési idő indul/újraindul és vége'
    ,Sorrend='10'
    ,Modosithato='1'
	,Egyeb = 'ON_PROGRESS,PAUSE|STOP'
    WHERE Id=@record_id 
       
  END


-- Másodfokú hatósági ügyintézési idő indul/újraindul és megáll
SET @record_id = (select Id from KRT_KodTarak where Id='5926df13-fe0f-4178-9ee0-3b0c51e041ba') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
	,Egyeb
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'5926df13-fe0f-4178-9ee0-3b0c51e041ba'
    ,'11'
    ,'Másodfokú hatósági ügyintézési idő indul/újraindul és megáll'
    ,'11'
    ,'1'
	,'ON_PROGRESS|PAUSE'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='11'
    ,Nev='Másodfokú hatósági ügyintézési idő indul/újraindul és megáll'
    ,Sorrend='11'
    ,Modosithato='1'
	,Egyeb= 'ON_PROGRESS|PAUSE'
    WHERE Id=@record_id 
       
  END






