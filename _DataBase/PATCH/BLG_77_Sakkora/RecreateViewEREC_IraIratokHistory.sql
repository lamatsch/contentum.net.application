if exists (select 1
            from  sysobjects
           where  id = object_id('EREC_IraIratokHistory')
            and   type = 'V')
   drop view EREC_IraIratokHistory
go

create view EREC_IraIratokHistory as select * from $(HistoryDatabase).dbo.EREC_IraIratokHistory;

GO


