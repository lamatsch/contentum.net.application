DECLARE @RC int
DECLARE @Id uniqueidentifier
DECLARE @Org uniqueidentifier
DECLARE @Felhasznalo_id uniqueidentifier
DECLARE @Nev nvarchar(400)
DECLARE @Ertek nvarchar(400)
DECLARE @Karbantarthato char(1)
DECLARE @Ver int
DECLARE @Note nvarchar(4000)
DECLARE @Stat_id uniqueidentifier
DECLARE @ErvKezd datetime
DECLARE @ErvVege datetime
DECLARE @Letrehozo_id uniqueidentifier
DECLARE @LetrehozasIdo datetime
DECLARE @Modosito_id uniqueidentifier
DECLARE @ModositasIdo datetime
DECLARE @Zarolo_id uniqueidentifier
DECLARE @ZarolasIdo datetime
DECLARE @Tranz_id uniqueidentifier
DECLARE @UIAccessLog_id uniqueidentifier
DECLARE @UpdatedColumns xml
DECLARE @ResultUid uniqueidentifier

SET @Id = 'BC67EEBF-D437-4BE8-92EC-7732F5C02AD8'
SET @Nev = 'UGYINTEZESI_IDO_FELFUGGESZTES'
SET @Ertek = 'FANCY'
SET @Note = 'FANCY,SIMPLE'
SET @Karbantarthato = '1'
SET @Letrehozo_id = '54E861A5-36ED-44CA-BAA7-C287D125B309'

IF NOT EXISTS (select 1 from KRT_Parameterek where Id = @Id)
BEGIN
	EXECUTE @RC = [dbo].[sp_KRT_ParameterekInsert] 
	   @Id
	  ,@Org
	  ,@Felhasznalo_id
	  ,@Nev
	  ,@Ertek
	  ,@Karbantarthato
	  ,@Ver
	  ,@Note
	  ,@Stat_id
	  ,@ErvKezd
	  ,@ErvVege
	  ,@Letrehozo_id
	  ,@LetrehozasIdo
	  ,@Modosito_id
	  ,@ModositasIdo
	  ,@Zarolo_id
	  ,@ZarolasIdo
	  ,@Tranz_id
	  ,@UIAccessLog_id
	  ,@UpdatedColumns
	  ,@ResultUid OUTPUT
END

GO