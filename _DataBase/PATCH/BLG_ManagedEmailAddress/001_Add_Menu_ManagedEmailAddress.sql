BEGIN TRANSACTION AddMenuItem;  
GO  
		--TIPUSOK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier=	null
		DECLARE @ObjTipus_Id_Tipus uniqueidentifier=	null
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Obj_Id_Szulo uniqueidentifier	=	null
		DECLARE @KodCsoport_Id uniqueidentifier=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id			='240029D6-4444-4444-884D-9624E70A5BF4'
		set @Kod		='INT_ManagedEmailAddress'
		set @Nev		='INT_ManagedEmailAddress t�bla'
		EXECUTE @RC = [dbo].[sp_KRT_ObjTipusokInsert] 
		   @Id
		  ,@ObjTipus_Id_Tipus
		  ,@Kod
		  ,@Nev
		  ,@Obj_Id_Szulo
		  ,@KodCsoport_Id
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--TIPUSOK END
		-----------------------------------------------------------------------
		--FUNKCI�K
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= '2533E523-4444-4444-9BE0-D477854E1306'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '5B9AC7B3-879C-48A7-BEBA-EF73A49AF893'
		set @ObjTipus_Id_AdatElem	= '240029D6-4444-4444-884D-9624E70A5BF4'
		set @Kod					= 'ManagedEmailAddressKezeles'
		set @Nev					= 'Automatikus email �rkeztet�s/iktat�s kezel�se'

		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCI�K END

		--FUNKCI�K
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= '2533E523-4444-4444-9BE0-D477854E1307'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '5B9AC7B3-879C-48A7-BEBA-EF73A49AF893'
		set @ObjTipus_Id_AdatElem	= '240029D6-4444-4444-884D-9624E70A5BF4'
		set @Kod					= 'ManagedEmailAddressModify'
		set @Nev					= 'Automatikus email �rkeztet�s/iktat�s m�dos�t�sa'

		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCI�K END

		--FUNKCI�K
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= '2533E523-4444-4444-9BE0-D477854E1308'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '939A0831-2914-4348-93FA-9DED0225747E'
		set @ObjTipus_Id_AdatElem	= '240029D6-4444-4444-884D-9624E70A5BF4'
		set @Kod					= 'ManagedEmailAddressNew'
		set @Nev					= 'Automatikus email �rkeztet�s/iktat�s l�trehoz�s'

		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCI�K END

		--FUNKCI�K
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= '2533E523-4444-4444-9BE0-D477854E1309'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '9508908E-AEFA-41C6-B6D7-807E3AE3A3AB'
		set @ObjTipus_Id_AdatElem	= '240029D6-4444-4444-884D-9624E70A5BF4'
		set @Kod					= 'ManagedEmailAddressList'
		set @Nev					= 'Automatikus email �rkeztet�s/iktat�s lista'

		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCI�K END
	
		--FUNKCI�K
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= '2533E523-4444-4444-9BE0-D477854E1310'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '78F0D5A2-E048-4A1F-9F11-5D57AB78B985'
		set @ObjTipus_Id_AdatElem	= '240029D6-4444-4444-884D-9624E70A5BF4'
		set @Kod					= 'ManagedEmailAddressView'
		set @Nev					= 'Automatikus email �rkeztet�s/iktat�s megtekint�se'

		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCI�K END

		--FUNKCI�K
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= '2533E523-4444-4444-9BE0-D477854E1311'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '805C7634-F205-4AE7-8580-DB3C73D8F43B'
		set @ObjTipus_Id_AdatElem	= '240029D6-4444-4444-884D-9624E70A5BF4'
		set @Kod					= 'ManagedEmailAddressInvalidate'
		set @Nev					= 'Automatikus email �rkeztet�s/iktat�s �rv�nytelen�t�s'

		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCI�K END
		
		-----------------------------------
		--MODULOK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Org uniqueidentifier=	null
		DECLARE @Tipus nvarchar(10)=	null
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @SelectString nvarchar(4000)=	null
		DECLARE @Csoport_Id_Tulaj uniqueidentifier=	null
		DECLARE @Parameterek nvarchar(4000)=	null
		DECLARE @ObjTip_Id_Adatelem uniqueidentifier=	null
		DECLARE @ObjTip_Adatelem nvarchar(100)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		SET @Id			='64DA85C8-4444-4444-97CE-30E1A69FEB8E'
		SET @Kod		='ManagedEmailAddressList.aspx'
		SET @Nev		='Automatikus email �rkeztet�s/iktat�s kezel�se'
		SET @Tipus		='F'

		EXECUTE @RC = [dbo].[sp_KRT_ModulokInsert] 
		   @Id
		  ,@Org
		  ,@Tipus
		  ,@Kod
		  ,@Nev
		  ,@Leiras
		  ,@SelectString
		  ,@Csoport_Id_Tulaj
		  ,@Parameterek
		  ,@ObjTip_Id_Adatelem
		  ,@ObjTip_Adatelem
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--MODULOK END
		-----------------------------------
		--MENUK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Org uniqueidentifier=	null
		DECLARE @Menu_Id_Szulo uniqueidentifier=	null
		DECLARE @Kod nvarchar(20)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Funkcio_Id uniqueidentifier=	null
		DECLARE @Modul_Id uniqueidentifier=	null
		DECLARE @Parameter nvarchar(400)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ImageURL nvarchar(400)=	null
		DECLARE @Sorrend nvarchar(100)=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @id				= 'BEAEB104-4444-4444-BB9B-534345734454'
		set @Menu_Id_Szulo	= '19B72B61-0FF3-4239-8D0F-6FB20792D79C'
		set @Kod			= 'eRecord'
		set @Nev			= 'Automatikus email �rkeztet�s/iktat�s kezel�se'
		set @Funkcio_Id		= '2533E523-4444-4444-9BE0-D477854E1306'
		set @Modul_Id		= '64DA85C8-4444-4444-97CE-30E1A69FEB8E'
		set @Sorrend		= 81

		EXECUTE @RC = [dbo].[sp_KRT_MenukInsert] 
		   @Id
		  ,@Org
		  ,@Menu_Id_Szulo
		  ,@Kod
		  ,@Nev
		  ,@Funkcio_Id
		  ,@Modul_Id
		  ,@Parameter
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ImageURL
		  ,@Sorrend
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--MENUK END

GO  
COMMIT TRANSACTION AddMenuItem;  
GO 

