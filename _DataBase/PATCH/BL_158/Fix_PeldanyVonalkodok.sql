﻿--vonalkód formátum ellenőrzése
declare @Ertek nvarchar(400)
set @Ertek = (select Ertek from KRT_Parameterek where Nev = 'VONALKOD_KEZELES')

IF @Ertek = 'AZONOSITO' 
BEGIN
	declare @hibas table(Id uniqueidentifier, Azonosito nvarchar(100), BarCode nvarchar(100), GeneraltBarcode nvarchar(100))

	declare @tranz_id uniqueidentifier = newid();
	declare @felh_id uniqueidentifier = (select Id from KRT_Felhasznalok where nev like 'Axis felhasználó')
	declare @date datetime = getdate()

	;with peldanyok
	as
	(
		select pld.Id,
		GeneraltBarcode = (iktatokonyv.Iktatohely + '/' + 
		right('000000' + cast(ugyiratok.Foszam as nvarchar), 6) + '-' + 
		right('000000' + cast(iratok.Alszam as nvarchar),6) + '/' + 
		cast(iktatokonyv.Ev as nvarchar))
		from EREC_PldIratPeldanyok pld
		join EREC_IraIratok iratok on pld.IraIrat_Id = iratok.Id
		join EREC_UgyUgyiratok ugyiratok on iratok.Ugyirat_Id = ugyiratok.Id
		join EREC_IraIktatoKonyvek iktatokonyv on ugyiratok.IraIktatokonyv_Id = iktatokonyv.Id
	)


	insert into @hibas
	select EREC_PldIratPeldanyok.Id, 
		   EREC_PldIratPeldanyok.Azonosito, 
		   EREC_PldIratPeldanyok.BarCode, 
		   pld.GeneraltBarcode
	from peldanyok pld join EREC_PldIratPeldanyok on pld.Id = EREC_PldIratPeldanyok.Id
	join EREC_IraIratok iratok on EREC_PldIratPeldanyok.IraIrat_Id = iratok.Id
	left join EREC_UgyiratObjKapcsolatok kapcsolatok on iratok.Id = kapcsolatok.Obj_Id_Kapcsolt and kapcsolatok.KapcsolatTipus = '06'
	where
	pld.GeneraltBarcode != EREC_PldIratPeldanyok.BarCode
	and kapcsolatok.Id is null -- nem átiktatott
	and EREC_PldIratPeldanyok.BarCode like 'xviii%' -- winsoft

	select * from @hibas

	begin tran

	-- példányok javítása
	update EREC_PldIratPeldanyok
		set BarCode = hibaspeldanyok.GeneraltBarcode,
			Tranz_id = @tranz_id,
			Ver = Ver +1,
			ModositasIdo = @date,
			Modosito_id = @felh_id
	from EREC_PldIratPeldanyok pld
	join @hibas hibaspeldanyok on pld.Id = hibaspeldanyok.Id

	select Id, Azonosito, Barcode
	from EREC_PldIratPeldanyok 
	where Tranz_id = @tranz_id

	-- vonalkód tábla javítása
	update KRT_Barkodok
		set Kod = hibaspeldanyok.GeneraltBarcode,
			Tranz_id = @tranz_id,
			Ver = Ver +1,
			ModositasIdo = @date,
			Modosito_id = @felh_id
	from KRT_Barkodok barkodok
	join @hibas hibaspeldanyok on barkodok.Obj_Id = hibaspeldanyok.Id

	select Obj_id, Kod
	from KRT_Barkodok 
	where Tranz_id = @tranz_id

	/* History Log */

	declare @row_ids varchar(MAX)
	set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from EREC_PldIratPeldanyok where Tranz_id = @tranz_id FOR XML PATH('')),1, 2, '')

	if @row_ids is not null
	  set @row_ids = @row_ids + '''';

	exec sp_LogRecordsToHistory_Tomeges
	@TableName = 'EREC_PldIratPeldanyok'
	,@Row_Ids = @row_ids
	,@Muvelet = 1
	,@Vegrehajto_Id = @felh_id
	,@VegrehajtasIdo = @date

	--select * from EREC_PldIratPeldanyokHistory
	--where Tranz_id = @tranz_id

	commit
END
ELSE
BEGIN
	print 'nem kell vonalkód javítás'
END




