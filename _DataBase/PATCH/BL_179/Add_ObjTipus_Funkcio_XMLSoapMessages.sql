/*
KRT_SoapMessageLog t�bla objetum t�pus l�trehoz�sa

SoapMessageLog... funkci�jogok felv�tele �s hozz�kapcsol�sa a FEJLESZTO szerepk�rh�z
New, List, View, Modify, Invalidate, ViewHistory, Lock, ForceLock
*/

Print '$Header: Add_ObjTipus_Funkcio_XMLSoapMessages.sql, 1, 2017. 10. 30. Nagy Csaba$ kezdete'

BEGIN TRAN
BEGIN TRY

	Print '--------------------------------------------------------------------'
	Print 'XMLSoapMessages --> START'
	Print '--------------------------------------------------------------------'

	DECLARE @record_id uniqueidentifier

	/*-------------------------------------------------------------------------*/
	PRINT 'KRT_ObjTipusok - KRT_SoapMessageLog tabla'
	/*-------------------------------------------------------------------------*/

	SET @record_id = (select Id from KRT_ObjTipusok
				where Id='093b9194-1515-4691-b0e5-6b1c9006f3cd') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_ObjTipusok(
				 Tranz_id
				,Stat_id
				,Id
				,ObjTipus_Id_Tipus
				,Kod
				,Nev
				) values (
				 'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,'d1bb5269-3fe2-434f-b0df-21d741ce716a'
				,'KRT_SoapMessageLog'
				,'KRT_SoapMessageLog t�bla'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_ObjTipusok
			 SET 
				 Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,ObjTipus_Id_Tipus='d1bb5269-3fe2-434f-b0df-21d741ce716a'
				,Kod='KRT_SoapMessageLog'
				,Nev='KRT_SoapMessageLog t�bla'
			 WHERE Id=@record_id
		 END

	/*-------------------------------------------------------------------------*/
	PRINT 'KRT_Funkciok - SoapMessageMaint, New, List, View, Modify,'
	PRINT 'Invalidate, ViewHistory, Lock, ForceLock'
	/*-------------------------------------------------------------------------*/

	SET @record_id = (select Id from KRT_Funkciok
				where Id='11b4905f-bd56-4742-938b-e768b94f8f42') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Funkciok(
				 Tranz_id
				,Stat_id
				,Alkalmazas_Id
				,Modosithato
				,Id
				,ObjTipus_Id_AdatElem
				,Muvelet_Id
				,Kod
				,Nev
				) values (
				 'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,'0'
				,'11b4905f-bd56-4742-938b-e768b94f8f42'
				,'093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,'db1010e4-2d27-4858-9ba4-8b3abd5df931'
				,'SoapMessageMaint'
				,'SoapMessages karbantart�sa'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Funkciok
			 SET 
				 Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,Modosithato='0'
				,Id='11b4905f-bd56-4742-938b-e768b94f8f42'
				,ObjTipus_Id_AdatElem='093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,Muvelet_Id='db1010e4-2d27-4858-9ba4-8b3abd5df931'
				,Kod='SoapMessageMaint'
				,Nev='SoapMessages karbantart�sa'
			 WHERE Id=@record_id
		 END


	SET @record_id = (select Id from KRT_Funkciok
				where Id='56f8a00d-52c0-4aec-ab79-5b8e7c662ea4') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Funkciok(
				 Alkalmazas_Id
				,Modosithato
				,Funkcio_Id_Szulo
				,Tranz_id
				,Stat_id
				,Id
				,ObjTipus_Id_AdatElem
				,Muvelet_Id
				,Kod
				,Nev
				) values (
				 '20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,'0'
				,'11b4905f-bd56-4742-938b-e768b94f8f42'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'56f8a00d-52c0-4aec-ab79-5b8e7c662ea4'
				,'093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,'939a0831-2914-4348-93fa-9ded0225747e'
				,'SoapMessageNew'
				,'Soapmessage felvitel'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Funkciok
			 SET 
				 Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,Modosithato='0'
				,Funkcio_Id_Szulo='11b4905f-bd56-4742-938b-e768b94f8f42'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='56f8a00d-52c0-4aec-ab79-5b8e7c662ea4'
				,ObjTipus_Id_AdatElem='093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,Muvelet_Id='939a0831-2914-4348-93fa-9ded0225747e'
				,Kod='SoapMessageNew'
				,Nev='Soapmessage felvitel'
			 WHERE Id=@record_id
		 END


	SET @record_id = (select Id from KRT_Funkciok
				where Id='a3c5a8db-709b-46c3-9116-badbd4e4e91c') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Funkciok(
				 Alkalmazas_Id
				,Modosithato
				,Funkcio_Id_Szulo
				,Tranz_id
				,Stat_id
				,Id
				,ObjTipus_Id_AdatElem
				,Muvelet_Id
				,Kod
				,Nev
				) values (
				 '20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,'0'
				,'11b4905f-bd56-4742-938b-e768b94f8f42'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'a3c5a8db-709b-46c3-9116-badbd4e4e91c'
				,'093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,'9508908e-aefa-41c6-b6d7-807e3ae3a3ab'
				,'SoapMessageList'
				,'SoapMessage list�z�s'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Funkciok
			 SET 
				 Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,Modosithato='0'
				,Funkcio_Id_Szulo='11b4905f-bd56-4742-938b-e768b94f8f42'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='a3c5a8db-709b-46c3-9116-badbd4e4e91c'
				,ObjTipus_Id_AdatElem='093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,Muvelet_Id='9508908e-aefa-41c6-b6d7-807e3ae3a3ab'
				,Kod='SoapMessageList'
				,Nev='SoapMessage list�z�s'
			 WHERE Id=@record_id
		 END


	SET @record_id = (select Id from KRT_Funkciok
				where Id='c75ba721-bac2-4de4-9bf5-6535c17bccc7') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Funkciok(
				 Alkalmazas_Id
				,Modosithato
				,Funkcio_Id_Szulo
				,Tranz_id
				,Stat_id
				,Id
				,ObjTipus_Id_AdatElem
				,Muvelet_Id
				,Kod
				,Nev
				) values (
				 '20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,'0'
				,'11b4905f-bd56-4742-938b-e768b94f8f42'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'c75ba721-bac2-4de4-9bf5-6535c17bccc7'
				,'093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,'78f0d5a2-e048-4a1f-9f11-5d57ab78b985'
				,'SoapMessageView'
				,'SoapMessage megn�z�s'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Funkciok
			 SET 
				 Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,Modosithato='0'
				,Funkcio_Id_Szulo='11b4905f-bd56-4742-938b-e768b94f8f42'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='c75ba721-bac2-4de4-9bf5-6535c17bccc7'
				,ObjTipus_Id_AdatElem='093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,Muvelet_Id='78f0d5a2-e048-4a1f-9f11-5d57ab78b985'
				,Kod='SoapMessageView'
				,Nev='SoapMessage megn�z�s'
			 WHERE Id=@record_id
		 END


	SET @record_id = (select Id from KRT_Funkciok
				where Id='4f973d08-4262-40ef-b039-9c2b0c80e05d') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Funkciok(
				 Alkalmazas_Id
				,Modosithato
				,Funkcio_Id_Szulo
				,Tranz_id
				,Stat_id
				,Id
				,ObjTipus_Id_AdatElem
				,Muvelet_Id
				,Kod
				,Nev
				) values (
				 '20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,'0'
				,'11b4905f-bd56-4742-938b-e768b94f8f42'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'4f973d08-4262-40ef-b039-9c2b0c80e05d'
				,'093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,'5b9ac7b3-879c-48a7-beba-ef73a49af893'
				,'SoapMessageModify'
				,'SoapMessage m�dos�t�s'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Funkciok
			 SET 
				 Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,Modosithato='0'
				,Funkcio_Id_Szulo='11b4905f-bd56-4742-938b-e768b94f8f42'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='4f973d08-4262-40ef-b039-9c2b0c80e05d'
				,ObjTipus_Id_AdatElem='093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,Muvelet_Id='5b9ac7b3-879c-48a7-beba-ef73a49af893'
				,Kod='SoapMessageModify'
				,Nev='SoapMessage m�dos�t�s'
			 WHERE Id=@record_id
		 END


	SET @record_id = (select Id from KRT_Funkciok
				where Id='2f15ee15-a585-4af5-88f4-c64e03de4c33') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Funkciok(
				 Alkalmazas_Id
				,Modosithato
				,Funkcio_Id_Szulo
				,Tranz_id
				,Stat_id
				,Id
				,ObjTipus_Id_AdatElem
				,Muvelet_Id
				,Kod
				,Nev
				) values (
				 '20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,'0'
				,'11b4905f-bd56-4742-938b-e768b94f8f42'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'2f15ee15-a585-4af5-88f4-c64e03de4c33'
				,'093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,'22fe4d06-86cf-4d20-865c-37124d9ea879'
				,'SoapMessageInvalidate'
				,'SoapMessage t�rl�s'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Funkciok
			 SET 
				 Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,Modosithato='0'
				,Funkcio_Id_Szulo='11b4905f-bd56-4742-938b-e768b94f8f42'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='2f15ee15-a585-4af5-88f4-c64e03de4c33'
				,ObjTipus_Id_AdatElem='093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,Muvelet_Id='22fe4d06-86cf-4d20-865c-37124d9ea879'
				,Kod='SoapMessageInvalidate'
				,Nev='SoapMessage t�rl�s'
			 WHERE Id=@record_id
		 END


	SET @record_id = (select Id from KRT_Funkciok
				where Id='37bd69f3-3d7b-4d53-87c4-c98853daf83c') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Funkciok(
				 Alkalmazas_Id
				,Modosithato
				,Funkcio_Id_Szulo
				,Tranz_id
				,Stat_id
				,Id
				,ObjTipus_Id_AdatElem
				,Muvelet_Id
				,Kod
				,Nev
				) values (
				 '20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,'0'
				,'11b4905f-bd56-4742-938b-e768b94f8f42'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'37bd69f3-3d7b-4d53-87c4-c98853daf83c'
				,'093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,'89806484-BF5A-4396-87B2-0066513A008D'
				,'SoapMessageViewHistory'
				,'SoapMessage el�zm�nyek megtekint�se'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Funkciok
			 SET 
				 Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,Modosithato='0'
				,Funkcio_Id_Szulo='11b4905f-bd56-4742-938b-e768b94f8f42'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='37bd69f3-3d7b-4d53-87c4-c98853daf83c'
				,ObjTipus_Id_AdatElem='093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,Muvelet_Id='89806484-BF5A-4396-87B2-0066513A008D'
				,Kod='SoapMessageViewHistory'
				,Nev='SoapMessage el�zm�nyek megtekint�se'
			 WHERE Id=@record_id
		 END


	SET @record_id = (select Id from KRT_Funkciok
				where Id='81330f5a-2d8a-414a-b85e-4a99ed678099') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Funkciok(
				 Alkalmazas_Id
				,Modosithato
				,Funkcio_Id_Szulo
				,Tranz_id
				,Stat_id
				,Id
				,ObjTipus_Id_AdatElem
				,Muvelet_Id
				,Kod
				,Nev
				) values (
				 '20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,'0'
				,'11b4905f-bd56-4742-938b-e768b94f8f42'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'81330f5a-2d8a-414a-b85e-4a99ed678099'
				,'093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,'fbddc6b8-6671-4ba7-9bdd-885ffc031986'
				,'SoapMessageLock'
				,'SoapMessage z�rol�s'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Funkciok
			 SET 
				 Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,Modosithato='0'
				,Funkcio_Id_Szulo='11b4905f-bd56-4742-938b-e768b94f8f42'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='81330f5a-2d8a-414a-b85e-4a99ed678099'
				,ObjTipus_Id_AdatElem='093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,Muvelet_Id='fbddc6b8-6671-4ba7-9bdd-885ffc031986'
				,Kod='SoapMessageLock'
				,Nev='SoapMessage z�rol�s'
			 WHERE Id=@record_id
		 END


	SET @record_id = (select Id from KRT_Funkciok
				where Id='f9328346-c621-43ce-ade7-106e13cb89c2') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Funkciok(
				 Alkalmazas_Id
				,Modosithato
				,Funkcio_Id_Szulo
				,Tranz_id
				,Stat_id
				,Id
				,ObjTipus_Id_AdatElem
				,Muvelet_Id
				,Kod
				,Nev
				) values (
				 '20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,'0'
				,'11b4905f-bd56-4742-938b-e768b94f8f42'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'f9328346-c621-43ce-ade7-106e13cb89c2'
				,'093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,'fbddc6b8-6671-4ba7-9bdd-885ffc031986'
				,'SoapMessageForceLock'
				,'SoapMessage z�rol�s (Force)'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Funkciok
			 SET 
				 Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,Modosithato='0'
				,Funkcio_Id_Szulo='11b4905f-bd56-4742-938b-e768b94f8f42'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='f9328346-c621-43ce-ade7-106e13cb89c2'
				,ObjTipus_Id_AdatElem='093b9194-1515-4691-b0e5-6b1c9006f3cd'
				,Muvelet_Id='fbddc6b8-6671-4ba7-9bdd-885ffc031986'
				,Kod='SoapMessageForceLock'
				,Nev='SoapMessage z�rol�s (Force)'
			 WHERE Id=@record_id
		 END

	/*-------------------------------------------------------------------------*/
	PRINT 'KRT_Szerepkor_Funkcio - SoapMessage funkciok FEJLESZTO szerepkorhoz kotese' 
	/*-------------------------------------------------------------------------*/

	SET @record_id = (select Id from KRT_Szerepkor_Funkcio
				where Id='49c38fb9-7a03-4240-b996-ca43dd3a6608') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Tranz_id
				,Stat_id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'49c38fb9-7a03-4240-b996-ca43dd3a6608'
				,'a3c5a8db-709b-46c3-9116-badbd4e4e91c'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Szerepkor_Funkcio
			 SET 
				 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='49c38fb9-7a03-4240-b996-ca43dd3a6608'
				,Funkcio_Id='a3c5a8db-709b-46c3-9116-badbd4e4e91c'
			 WHERE Id=@record_id
		 END


	SET @record_id = (select Id from KRT_Szerepkor_Funkcio
				where Id='20c1aa85-f8e1-4581-992f-67ce61d67b2e') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Tranz_id
				,Stat_id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'20c1aa85-f8e1-4581-992f-67ce61d67b2e'
				,'56f8a00d-52c0-4aec-ab79-5b8e7c662ea4'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Szerepkor_Funkcio
			 SET 
				 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='20c1aa85-f8e1-4581-992f-67ce61d67b2e'
				,Funkcio_Id='56f8a00d-52c0-4aec-ab79-5b8e7c662ea4'
			 WHERE Id=@record_id
		 END


	SET @record_id = (select Id from KRT_Szerepkor_Funkcio
				where Id='d1159f1c-fa1a-4648-8529-6c9b7510e25c') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Tranz_id
				,Stat_id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'d1159f1c-fa1a-4648-8529-6c9b7510e25c'
				,'c75ba721-bac2-4de4-9bf5-6535c17bccc7'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Szerepkor_Funkcio
			 SET 
				 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='d1159f1c-fa1a-4648-8529-6c9b7510e25c'
				,Funkcio_Id='c75ba721-bac2-4de4-9bf5-6535c17bccc7'
			 WHERE Id=@record_id
		 END


	SET @record_id = (select Id from KRT_Szerepkor_Funkcio
				where Id='41443dd4-88ac-44ad-904f-4fda77e025d7') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Tranz_id
				,Stat_id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'41443dd4-88ac-44ad-904f-4fda77e025d7'
				,'4f973d08-4262-40ef-b039-9c2b0c80e05d'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Szerepkor_Funkcio
			 SET 
				 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='41443dd4-88ac-44ad-904f-4fda77e025d7'
				,Funkcio_Id='4f973d08-4262-40ef-b039-9c2b0c80e05d'
			 WHERE Id=@record_id
		 END


	SET @record_id = (select Id from KRT_Szerepkor_Funkcio
				where Id='04d78891-1dd1-4d92-8ef9-9b5acb3598aa') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Tranz_id
				,Stat_id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'04d78891-1dd1-4d92-8ef9-9b5acb3598aa'
				,'2f15ee15-a585-4af5-88f4-c64e03de4c33'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Szerepkor_Funkcio
			 SET 
				 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='04d78891-1dd1-4d92-8ef9-9b5acb3598aa'
				,Funkcio_Id='2f15ee15-a585-4af5-88f4-c64e03de4c33'
			 WHERE Id=@record_id
		 END


	SET @record_id = (select Id from KRT_Szerepkor_Funkcio
				where Id='f0fcc3ed-777e-4cf4-b71d-d452d8c0720a') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Tranz_id
				,Stat_id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'f0fcc3ed-777e-4cf4-b71d-d452d8c0720a'
				,'37bd69f3-3d7b-4d53-87c4-c98853daf83c'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Szerepkor_Funkcio
			 SET 
				 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='f0fcc3ed-777e-4cf4-b71d-d452d8c0720a'
				,Funkcio_Id='37bd69f3-3d7b-4d53-87c4-c98853daf83c'
			 WHERE Id=@record_id
		 END


	SET @record_id = (select Id from KRT_Szerepkor_Funkcio
				where Id='e4f6d9d6-820c-4eb2-83a3-6dec0402b222') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Tranz_id
				,Stat_id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'e4f6d9d6-820c-4eb2-83a3-6dec0402b222'
				,'81330f5a-2d8a-414a-b85e-4a99ed678099'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Szerepkor_Funkcio
			 SET 
				 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='e4f6d9d6-820c-4eb2-83a3-6dec0402b222'
				,Funkcio_Id='81330f5a-2d8a-414a-b85e-4a99ed678099'
			 WHERE Id=@record_id
		 END


	SET @record_id = (select Id from KRT_Szerepkor_Funkcio
				where Id='54479d1e-795c-4fb3-a9ac-3a355bc7f868') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
			
	Print 'INSERT' 
			 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Tranz_id
				,Stat_id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'54479d1e-795c-4fb3-a9ac-3a355bc7f868'
				,'f9328346-c621-43ce-ade7-106e13cb89c2'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Szerepkor_Funkcio
			 SET 
				 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
				,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
				,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
				,Id='54479d1e-795c-4fb3-a9ac-3a355bc7f868'
				,Funkcio_Id='f9328346-c621-43ce-ade7-106e13cb89c2'
			 WHERE Id=@record_id
		 END

	COMMIT

	Print '--------------------------------------------------------------------'
	Print 'SoapMessage betoltes <-- END'
	Print '--------------------------------------------------------------------'

END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
	ROLLBACK

	DECLARE @errorSeverity INT,
		@errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()

	IF ERROR_NUMBER() < 50000 
		SET @errorCode = '[' + CONVERT(NVARCHAR(10), ERROR_NUMBER())
			+ '] ' + ERROR_MESSAGE()
	ELSE 
		SET @errorCode = ERROR_MESSAGE()

	IF @errorState = 0 
		SET @errorState = 1

	RAISERROR ( @errorCode, @errorSeverity, @errorState )

	PRINT '*** TRANSACTION ROLLED BACK. ***'
	PRINT ''

END CATCH

Print '$Header: Add_ObjTipus_Funkcio_XMLSoapMessages.sql, 1, 2017. 10. 30. Nagy Csaba$ v�ge'