
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--FF, 20070926
IF  EXISTS (SELECT * FROM sys.Objects WHERE object_id = OBJECT_ID(N'dbo.CK_KRT_SoapMessageLog') AND type = N'C')
  ALTER TABLE KRT_SoapMessageLog DROP CONSTRAINT CK_KRT_SoapMessageLog  
GO
--FF

IF  EXISTS (SELECT * FROM sys.Objects WHERE object_id = OBJECT_ID(N'dbo.fn_KRT_SoapMessageLogCheckUK') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION dbo.fn_KRT_SoapMessageLogCheckUK
GO

CREATE FUNCTION dbo.fn_KRT_SoapMessageLogCheckUK (
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

-- nincsenek megadva oszlopok a Unique Key ellenorzeshez
SET @ret = 0      
      
Return @ret

END

GO

ALTER TABLE KRT_SoapMessageLog  
WITH NOCHECK ADD CONSTRAINT 
CK_KRT_SoapMessageLog
CHECK  (((0)=dbo.fn_KRT_SoapMessageLogCheckUK(Id,ErvKezd,ErvVege)
))

GO
