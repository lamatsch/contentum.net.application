-- =========================================
-- Create History table for KRT_SoapMessageLog
-- =========================================
 
IF OBJECT_ID('KRT_SoapMessageLogHistory', 'U') IS NOT NULL
  DROP TABLE KRT_SoapMessageLogHistory
GO
 
CREATE TABLE KRT_SoapMessageLogHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 ForrasRendszer nvarchar(200),  
 Url nvarchar(1500),  
 CelRendszer nvarchar(200),  
 MetodusNeve nvarchar(1000),  
 KeresFogadas datetime,  
 KeresKuldes datetime,  
 RequestData nvarchar(max),  
 ResponseData nvarchar(max),  
 FuttatoFelhasznalo nvarchar(500),  
 Local char(1),  
 Hiba nvarchar(max),  
 Sikeres char(1),  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_KRT_SoapMessageLogHistory_ID_VER ON KRT_SoapMessageLogHistory
(
	Id,Ver ASC
)
GO