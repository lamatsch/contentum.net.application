/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2005                    */
/* Created on:     2017. 10. 19. 15:47:52                       */
/*==============================================================*/


if exists (select 1
            from  sysobjects
           where  id = object_id('KRT_SoapMessageLog')
            and   type = 'U')
   drop table KRT_SoapMessageLog
go


/*==============================================================*/
/* Table: KRT_SoapMessageLog                                    */
/*==============================================================*/
create table KRT_SoapMessageLog (
   Id                   uniqueidentifier     not null default newsequentialid(),
   Ver                  int                  null default 1,
   Note                 Nvarchar(4000)       null,
   Stat_id              uniqueidentifier     null,
   ErvKezd              datetime             null default Getdate(),
   ErvVege              datetime             null,
   Letrehozo_id         uniqueidentifier     null,
   LetrehozasIdo        datetime             not null default Getdate(),
   Modosito_id          uniqueidentifier     null,
   ModositasIdo         datetime             null,
   Zarolo_id            uniqueidentifier     null,
   ZarolasIdo           datetime             null,
   Tranz_id             uniqueidentifier     null,
   UIAccessLog_id       uniqueidentifier     null,
   ForrasRendszer       nvarchar(200)        null,
   Url                  nvarchar(1500)       null,
   CelRendszer          nvarchar(200)        null,
   MetodusNeve          nvarchar(1000)       null,
   KeresFogadas         datetime             null,
   KeresKuldes          datetime             null,
   RequestData          nvarchar(max)        null,
   ResponseData         nvarchar(max)        null,
   FuttatoFelhasznalo   nvarchar(500)        null,
   Local                char(1)              null,
   Hiba                 nvarchar(max)        null,
   Sikeres              char(1)              null,
   constraint PK_KRT_SOAPMESSAGELOG primary key (Id)
)
go

alter table KRT_SoapMessageLog 
  ADD CONSTRAINT
	DF_KRT_SoapMessageLog_ErvVege DEFAULT CONVERT(datetime, '4700-12-31', 102) FOR ErvVege;
go


