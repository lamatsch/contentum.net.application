
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_SoapMessageLogInsert')
            and   type = 'P')
   drop procedure sp_KRT_SoapMessageLogInsert
go

create procedure sp_KRT_SoapMessageLogInsert    
                @Id      uniqueidentifier = null,    
                               @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,
                @ForrasRendszer     nvarchar(200)  = null,
                @Url     nvarchar(1500)  = null,
                @CelRendszer     nvarchar(200)  = null,
                @MetodusNeve     nvarchar(1000)  = null,
                @KeresFogadas     datetime  = null,
                @KeresKuldes     datetime  = null,
                @RequestData     nvarchar(max)  = null,
                @ResponseData     nvarchar(max)  = null,
                @FuttatoFelhasznalo     nvarchar(500)  = null,
                @Local     char(1)  = null,
                @Hiba     nvarchar(max)  = null,
                @Sikeres     char(1)  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end 
       
         if @ForrasRendszer is not null
         begin
            SET @insertColumns = @insertColumns + ',ForrasRendszer'
            SET @insertValues = @insertValues + ',@ForrasRendszer'
         end 
       
         if @Url is not null
         begin
            SET @insertColumns = @insertColumns + ',Url'
            SET @insertValues = @insertValues + ',@Url'
         end 
       
         if @CelRendszer is not null
         begin
            SET @insertColumns = @insertColumns + ',CelRendszer'
            SET @insertValues = @insertValues + ',@CelRendszer'
         end 
       
         if @MetodusNeve is not null
         begin
            SET @insertColumns = @insertColumns + ',MetodusNeve'
            SET @insertValues = @insertValues + ',@MetodusNeve'
         end 
       
         if @KeresFogadas is not null
         begin
            SET @insertColumns = @insertColumns + ',KeresFogadas'
            SET @insertValues = @insertValues + ',@KeresFogadas'
         end 
       
         if @KeresKuldes is not null
         begin
            SET @insertColumns = @insertColumns + ',KeresKuldes'
            SET @insertValues = @insertValues + ',@KeresKuldes'
         end 
       
         if @RequestData is not null
         begin
            SET @insertColumns = @insertColumns + ',RequestData'
            SET @insertValues = @insertValues + ',@RequestData'
         end 
       
         if @ResponseData is not null
         begin
            SET @insertColumns = @insertColumns + ',ResponseData'
            SET @insertValues = @insertValues + ',@ResponseData'
         end 
       
         if @FuttatoFelhasznalo is not null
         begin
            SET @insertColumns = @insertColumns + ',FuttatoFelhasznalo'
            SET @insertValues = @insertValues + ',@FuttatoFelhasznalo'
         end 
       
         if @Local is not null
         begin
            SET @insertColumns = @insertColumns + ',Local'
            SET @insertValues = @insertValues + ',@Local'
         end 
       
         if @Hiba is not null
         begin
            SET @insertColumns = @insertColumns + ',Hiba'
            SET @insertValues = @insertValues + ',@Hiba'
         end 
       
         if @Sikeres is not null
         begin
            SET @insertColumns = @insertColumns + ',Sikeres'
            SET @insertValues = @insertValues + ',@Sikeres'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into KRT_SoapMessageLog ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ForrasRendszer nvarchar(200),@Url nvarchar(1500),@CelRendszer nvarchar(200),@MetodusNeve nvarchar(1000),@KeresFogadas datetime,@KeresKuldes datetime,@RequestData nvarchar(max),@ResponseData nvarchar(max),@FuttatoFelhasznalo nvarchar(500),@Local char(1),@Hiba nvarchar(max),@Sikeres char(1),@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id,@ForrasRendszer = @ForrasRendszer,@Url = @Url,@CelRendszer = @CelRendszer,@MetodusNeve = @MetodusNeve,@KeresFogadas = @KeresFogadas,@KeresKuldes = @KeresKuldes,@RequestData = @RequestData,@ResponseData = @ResponseData,@FuttatoFelhasznalo = @FuttatoFelhasznalo,@Local = @Local,@Hiba = @Hiba,@Sikeres = @Sikeres ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'KRT_SoapMessageLog',@ResultUid
					,'KRT_SoapMessageLogHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
go

/* használt errorcode-ok:
	50301:
	50302: 'Org' oszlop értéke nem lehet NULL
*/
