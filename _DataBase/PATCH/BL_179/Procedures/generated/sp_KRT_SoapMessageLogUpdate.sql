

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_SoapMessageLogUpdate')
            and   type = 'P')
   drop procedure sp_KRT_SoapMessageLogUpdate
go

create procedure sp_KRT_SoapMessageLogUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
             @ForrasRendszer     nvarchar(200)  = null,         
             @Url     nvarchar(1500)  = null,         
             @CelRendszer     nvarchar(200)  = null,         
             @MetodusNeve     nvarchar(1000)  = null,         
             @KeresFogadas     datetime  = null,         
             @KeresKuldes     datetime  = null,         
             @RequestData     nvarchar(max)  = null,         
             @ResponseData     nvarchar(max)  = null,         
             @FuttatoFelhasznalo     nvarchar(500)  = null,         
             @Local     char(1)  = null,         
             @Hiba     nvarchar(max)  = null,         
             @Sikeres     char(1)  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier         
     DECLARE @Act_ForrasRendszer     nvarchar(200)         
     DECLARE @Act_Url     nvarchar(1500)         
     DECLARE @Act_CelRendszer     nvarchar(200)         
     DECLARE @Act_MetodusNeve     nvarchar(1000)         
     DECLARE @Act_KeresFogadas     datetime         
     DECLARE @Act_KeresKuldes     datetime         
     DECLARE @Act_RequestData     nvarchar(max)         
     DECLARE @Act_ResponseData     nvarchar(max)         
     DECLARE @Act_FuttatoFelhasznalo     nvarchar(500)         
     DECLARE @Act_Local     char(1)         
     DECLARE @Act_Hiba     nvarchar(max)         
     DECLARE @Act_Sikeres     char(1)           
  
set nocount on

select 
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id,
     @Act_ForrasRendszer = ForrasRendszer,
     @Act_Url = Url,
     @Act_CelRendszer = CelRendszer,
     @Act_MetodusNeve = MetodusNeve,
     @Act_KeresFogadas = KeresFogadas,
     @Act_KeresKuldes = KeresKuldes,
     @Act_RequestData = RequestData,
     @Act_ResponseData = ResponseData,
     @Act_FuttatoFelhasznalo = FuttatoFelhasznalo,
     @Act_Local = Local,
     @Act_Hiba = Hiba,
     @Act_Sikeres = Sikeres
from KRT_SoapMessageLog
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zárolás ellenőrzés:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id
   IF @UpdatedColumns.exist('/root/ForrasRendszer')=1
         SET @Act_ForrasRendszer = @ForrasRendszer
   IF @UpdatedColumns.exist('/root/Url')=1
         SET @Act_Url = @Url
   IF @UpdatedColumns.exist('/root/CelRendszer')=1
         SET @Act_CelRendszer = @CelRendszer
   IF @UpdatedColumns.exist('/root/MetodusNeve')=1
         SET @Act_MetodusNeve = @MetodusNeve
   IF @UpdatedColumns.exist('/root/KeresFogadas')=1
         SET @Act_KeresFogadas = @KeresFogadas
   IF @UpdatedColumns.exist('/root/KeresKuldes')=1
         SET @Act_KeresKuldes = @KeresKuldes
   IF @UpdatedColumns.exist('/root/RequestData')=1
         SET @Act_RequestData = @RequestData
   IF @UpdatedColumns.exist('/root/ResponseData')=1
         SET @Act_ResponseData = @ResponseData
   IF @UpdatedColumns.exist('/root/FuttatoFelhasznalo')=1
         SET @Act_FuttatoFelhasznalo = @FuttatoFelhasznalo
   IF @UpdatedColumns.exist('/root/Local')=1
         SET @Act_Local = @Local
   IF @UpdatedColumns.exist('/root/Hiba')=1
         SET @Act_Hiba = @Hiba
   IF @UpdatedColumns.exist('/root/Sikeres')=1
         SET @Act_Sikeres = @Sikeres   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE KRT_SoapMessageLog
SET
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id,
     ForrasRendszer = @Act_ForrasRendszer,
     Url = @Act_Url,
     CelRendszer = @Act_CelRendszer,
     MetodusNeve = @Act_MetodusNeve,
     KeresFogadas = @Act_KeresFogadas,
     KeresKuldes = @Act_KeresKuldes,
     RequestData = @Act_RequestData,
     ResponseData = @Act_ResponseData,
     FuttatoFelhasznalo = @Act_FuttatoFelhasznalo,
     Local = @Act_Local,
     Hiba = @Act_Hiba,
     Sikeres = @Act_Sikeres
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'KRT_SoapMessageLog',@Id
					,'KRT_SoapMessageLogHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

go

/* használt errorcode-ok:
	50401:
	50402: 'Ver' oszlop értéke nem egyezik a meglévő rekordéval
   50403: érvényesség már lejárt a rekordra
	50499: rekord zárolva
*/
