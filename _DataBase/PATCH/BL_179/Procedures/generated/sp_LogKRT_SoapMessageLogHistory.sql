
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_LogKRT_SoapMessageLogHistory')
            and   type = 'P')
   drop procedure sp_LogKRT_SoapMessageLogHistory
go

create procedure sp_LogKRT_SoapMessageLogHistory
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from KRT_SoapMessageLog where Id = @Row_Id;
      
   insert into KRT_SoapMessageLogHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id     
 ,ForrasRendszer     
 ,Url     
 ,CelRendszer     
 ,MetodusNeve     
 ,KeresFogadas     
 ,KeresKuldes     
 ,RequestData     
 ,ResponseData     
 ,FuttatoFelhasznalo     
 ,Local     
 ,Hiba     
 ,Sikeres   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id          
 ,ForrasRendszer          
 ,Url          
 ,CelRendszer          
 ,MetodusNeve          
 ,KeresFogadas          
 ,KeresKuldes          
 ,RequestData          
 ,ResponseData          
 ,FuttatoFelhasznalo          
 ,Local          
 ,Hiba          
 ,Sikeres        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go