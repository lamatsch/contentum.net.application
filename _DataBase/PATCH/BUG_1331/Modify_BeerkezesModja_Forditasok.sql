-- declare @id uniqueidentifier
-- set @id = '4C12A702-7AF9-4150-A46B-724326EB26D9'

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Forditasok where Id = '4C12A702-7AF9-4150-A46B-724326EB26D9') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
		print 'insert'

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)

	VALUES
	(
		@record_id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/KuldKuldemenyFormTab.ascx',
		'labelBeerkezesModja',
		'Be�rkez�s m�dja:'
	);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Forditasok
	 SET 
		Nyelvkod ='hu-HU'
		,OrgKod ='NMHH'
		,Modul ='eRecord'
		,Komponens ='eRecordComponent/KuldKuldemenyFormTab.ascx'
		,ObjAzonosito = 'labelBeerkezesModja'
		,Forditas = 'Be�rkez�s m�dja:'
	 WHERE Id=@record_id 
 END

 
SET @record_id = (select Id from KRT_Forditasok where Id = 'B833F8B3-7738-43B5-80C6-8CF0D20CD514') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
		print 'insert'

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)

	VALUES
	(
		@record_id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'EgyszerusitettIktatasForm.aspx',
		'labelBeerkezesModja',
		'Be�rkez�s m�dja:'
	);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Forditasok
	 SET 
		Nyelvkod ='hu-HU'
		,OrgKod ='NMHH'
		,Modul ='eRecord'
		,Komponens ='EgyszerusitettIktatasForm.aspx'
		,ObjAzonosito = 'labelBeerkezesModja'
		,Forditas = 'Be�rkez�s m�dja:'
	 WHERE Id=@record_id 
 END

GO