﻿-- EXPEDIALAS_MODJA_DEFAULT rendszerparameter
print '''EXPEDIALAS_MODJA_DEFAULT'' rendszerparameter hozzaadasa'

declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'  
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id=@org)

print 'Org = ' + @org_kod

declare @tranz_id uniqueidentifier = newid()

declare @record_id uniqueidentifier = '06EBA217-7AEE-495F-860C-3D5E30CD5DAE'

declare @Ertek nvarchar(400)

-- BOPMH: Posta - hivatalos, FPH: Helyben
set @Ertek = (case @org_kod when 'BOPMH' then '01' else '13' end)

print '@record_id = ' + cast(@record_id as nvarchar(36))

print '@Ertek = ' + @Ertek

declare @Nev nvarchar(400) = 'EXPEDIALAS_MODJA_DEFAULT'

declare @Note nvarchar(4000) = 'Word-ból iktatás esetén az alapértelmezetten beállított expediálás mód kódja.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
	     Tranz_id=@tranz_id
		,Org=@org
		,Id= @record_id
		,Nev= @Nev
		,Ertek= @Ertek
		,Karbantarthato='1'
		,Note = @Note
	 WHERE Id=@record_id 
 END