declare @tranz_id uniqueidentifier = newid();
declare @felh_id uniqueidentifier = (select Id from KRT_Felhasznalok where nev like 'Axis felhasználó')
declare @date datetime = getdate()

BEGIN TRAN

	--A Ket. 31. § (1) l) alapján történő megszüntetés: 5a -> 9
	update KRT_KodTarak
	set Kod = '9',
		Tranz_id = @tranz_id,
		Ver = Ver +1,
		ModositasIdo = @date,
		Modosito_id = @felh_id
	where Id IN ('C23A22BA-4D51-4557-83D1-1A482662C853','F0C62B16-D6DD-406D-980D-4C98E92F1646')
	AND Kod != '9'

	UPDATE EREC_IraOnkormAdatok
	SET DontesFormaja = '9',
		Tranz_id = @tranz_id,
		Ver = Ver +1,
		ModositasIdo = @date,
		Modosito_id = @felh_id
	WHERE DontesFormaja = '5a'

COMMIT
