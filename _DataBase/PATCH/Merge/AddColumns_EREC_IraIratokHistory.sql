print 'AddColumns_EREC_IraIratokHistory.sql'

IF NOT EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'EREC_IraIratokHistory' AND COLUMN_NAME = 'Elintezett') 
begin
	print 'Add EREC_IraIratokHistory.Elintezett'
	alter table EREC_IraIratokHistory 
	add Elintezett char(1) null 
end

IF NOT EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS
          WHERE  TABLE_NAME = 'EREC_IraIratokHistory' AND COLUMN_NAME = 'IratHatasaUgyintezesre') 
begin
	print 'Add EREC_IraIratokHistory.IratHatasaUgyintezesre'
	alter table EREC_IraIratokHistory 
	add IratHatasaUgyintezesre nvarchar(64) null
end
