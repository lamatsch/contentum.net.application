-- =========================================
-- Create History table for EREC_Alairas_Folyamatok
-- =========================================
 
IF OBJECT_ID('EREC_Alairas_FolyamatokHistory', 'U') IS NOT NULL
  DROP TABLE EREC_Alairas_FolyamatokHistory
GO
 
CREATE TABLE EREC_Alairas_FolyamatokHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 AlairasStarted datetime,  
 AlairasStopped datetime,  
 AlairasStatus Nvarchar(64),  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_EREC_Alairas_FolyamatokHistory_ID_VER ON EREC_Alairas_FolyamatokHistory
(
	Id,Ver ASC
)
GO