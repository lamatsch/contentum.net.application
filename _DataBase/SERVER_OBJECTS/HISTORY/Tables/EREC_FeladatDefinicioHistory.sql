ALTER TABLE [dbo].[EREC_FeladatDefinicioHistory] DROP CONSTRAINT [DF_EREC_FeladatDefinicioHistory_HistoryId]
GO
/****** Object:  Index [IX_EREC_FeladatDefinicioHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_FeladatDefinicioHistory_ID_VER] ON [dbo].[EREC_FeladatDefinicioHistory]
GO
/****** Object:  Table [dbo].[EREC_FeladatDefinicioHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_FeladatDefinicioHistory]
GO
/****** Object:  Table [dbo].[EREC_FeladatDefinicioHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EREC_FeladatDefinicioHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Funkcio_Id_Kivalto] [uniqueidentifier] NULL,
	[Obj_Metadefinicio_Id] [uniqueidentifier] NULL,
	[ObjStateValue_Id] [uniqueidentifier] NULL,
	[FeladatDefinicioTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FeladatSorszam] [int] NULL,
	[MuveletDefinicio] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[SpecMuveletDefinicio] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Funkcio_Id_Inditando] [uniqueidentifier] NULL,
	[Funkcio_Id_Futtatando] [uniqueidentifier] NULL,
	[BazisObjLeiro] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[FelelosTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Obj_Id_Felelos] [uniqueidentifier] NULL,
	[FelelosLeiro] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Idobazis] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[ObjTip_Id_DateCol] [uniqueidentifier] NULL,
	[AtfutasiIdo] [int] NULL,
	[Idoegyseg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FeladatLeiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Prioritas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[LezarasPrioritas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Obj_SzukitoFeltetel] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_FeladatDefinicioHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [IX_EREC_FeladatDefinicioHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_FeladatDefinicioHistory_ID_VER] ON [dbo].[EREC_FeladatDefinicioHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EREC_FeladatDefinicioHistory] ADD  CONSTRAINT [DF_EREC_FeladatDefinicioHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
GO
