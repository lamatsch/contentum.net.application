/****** Object:  Index [IX_EREC_HataridosFeladatokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_HataridosFeladatokHistory_ID_VER] ON [dbo].[EREC_HataridosFeladatokHistory]
GO
/****** Object:  Table [dbo].[EREC_HataridosFeladatokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_HataridosFeladatokHistory]
GO
/****** Object:  Table [dbo].[EREC_HataridosFeladatokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EREC_HataridosFeladatokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_HataridosFeladatokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Csoport_id_felelos] [uniqueidentifier] NULL,
	[Felhasznalo_Id_kiado] [uniqueidentifier] NULL,
	[HataridosFeladat_Id] [uniqueidentifier] NULL,
	[Forras] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Prioritas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[KezdesiIdo] [datetime] NULL,
	[IntezkHatarido] [datetime] NULL,
	[LezarasDatuma] [datetime] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[Funkcio_Id_Inditando] [uniqueidentifier] NULL,
	[Esemeny_Id_Kivalto] [uniqueidentifier] NULL,
	[Esemeny_Id_Lezaro] [uniqueidentifier] NULL,
	[Felhasznalo_Id_Felelos] [uniqueidentifier] NULL,
	[HataridosFeladat_Id_Fo] [uniqueidentifier] NULL,
	[ReszfeladatSorszam] [int] NULL,
	[FeladatDefinicio_Id] [uniqueidentifier] NULL,
	[FeladatDefinicio_Id_Lezaro] [uniqueidentifier] NULL,
	[LezarasPrioritas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Altipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Megoldas] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id_Kiado] [uniqueidentifier] NULL,
	[Memo] [char](1) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_HataridosFeladatokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_EREC_HataridosFeladatokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_HataridosFeladatokHistory_ID_VER] ON [dbo].[EREC_HataridosFeladatokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
