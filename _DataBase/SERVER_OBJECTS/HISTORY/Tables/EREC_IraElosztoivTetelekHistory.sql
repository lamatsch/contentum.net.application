ALTER TABLE [dbo].[EREC_IraElosztoivTetelekHistory] DROP CONSTRAINT [DF_EREC_IraElosztoivTetelekHistory_HistoryId]
GO
/****** Object:  Index [IX_EREC_IraElosztoivTetelekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_IraElosztoivTetelekHistory_ID_VER] ON [dbo].[EREC_IraElosztoivTetelekHistory]
GO
/****** Object:  Table [dbo].[EREC_IraElosztoivTetelekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_IraElosztoivTetelekHistory]
GO
/****** Object:  Table [dbo].[EREC_IraElosztoivTetelekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EREC_IraElosztoivTetelekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[ElosztoIv_Id] [uniqueidentifier] NULL,
	[ElosztoIv_Id_Ver] [int] NULL,
	[Sorszam] [int] NULL,
	[Partner_Id] [uniqueidentifier] NULL,
	[Partner_Id_Ver] [int] NULL,
	[Cim_Id] [uniqueidentifier] NULL,
	[CimSTR] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[NevSTR] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Kuldesmod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Visszavarolag] [char](1) COLLATE Hungarian_CI_AS NULL,
	[VisszavarasiIdo] [datetime] NULL,
	[Vissza_Nap_Mulva] [int] NULL,
	[AlairoSzerep] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IraElosztoivTetelekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_EREC_IraElosztoivTetelekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_IraElosztoivTetelekHistory_ID_VER] ON [dbo].[EREC_IraElosztoivTetelekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EREC_IraElosztoivTetelekHistory] ADD  CONSTRAINT [DF_EREC_IraElosztoivTetelekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
GO
