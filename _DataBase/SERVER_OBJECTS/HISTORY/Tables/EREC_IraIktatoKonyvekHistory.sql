/****** Object:  Index [IX_EREC_IraIktatoKonyvekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_IraIktatoKonyvekHistory_ID_VER] ON [dbo].[EREC_IraIktatoKonyvekHistory]
GO
/****** Object:  Table [dbo].[EREC_IraIktatoKonyvekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_IraIktatoKonyvekHistory]
GO
/****** Object:  Table [dbo].[EREC_IraIktatoKonyvekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EREC_IraIktatoKonyvekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_IraIktatoKonyvekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Ev] [int] NULL,
	[Azonosito] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[DefaultIrattariTetelszam] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[MegkulJelzes] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Iktatohely] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KozpontiIktatasJelzo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[UtolsoFoszam] [int] NULL,
	[Csoport_Id_Olvaso] [uniqueidentifier] NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NULL,
	[IktSzamOsztas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FormatumKod] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Titkos] [char](1) COLLATE Hungarian_CI_AS NULL,
	[IktatoErkezteto] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[LezarasDatuma] [datetime] NULL,
	[PostakonyvVevokod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[PostakonyvMegallapodasAzon] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UtolsoSorszam] [int] NULL,
	[EFeladojegyzekUgyfelAdatok] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Statusz] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Terjedelem] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[SelejtezesDatuma] [datetime] NULL,
	[KezelesTipusa] [char](1) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_IraIktatoKonyvekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_EREC_IraIktatoKonyvekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_IraIktatoKonyvekHistory_ID_VER] ON [dbo].[EREC_IraIktatoKonyvekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
