/****** Object:  Index [IX_EREC_IraIratokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_IraIratokHistory_ID_VER] ON [dbo].[EREC_IraIratokHistory]
GO
/****** Object:  Table [dbo].[EREC_IraIratokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_IraIratokHistory]
GO
/****** Object:  Table [dbo].[EREC_IraIratokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EREC_IraIratokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_IraIratokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[PostazasIranya] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Alszam] [int] NULL,
	[UtolsoSorszam] [int] NULL,
	[UgyUgyIratDarab_Id] [uniqueidentifier] NULL,
	[UgyUgyIratDarab_Id_Ver] [int] NULL,
	[Kategoria] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[HivatkozasiSzam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[IktatasDatuma] [datetime] NULL,
	[ExpedialasDatuma] [datetime] NULL,
	[ExpedialasModja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FelhasznaloCsoport_Id_Expedial] [uniqueidentifier] NULL,
	[Targy] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Jelleg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[FelhasznaloCsoport_Id_Iktato] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Kiadmany] [uniqueidentifier] NULL,
	[UgyintezesAlapja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[Csoport_Id_Ugyfelelos] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Orzo] [uniqueidentifier] NULL,
	[KiadmanyozniKell] [char](1) COLLATE Hungarian_CI_AS NULL,
	[FelhasznaloCsoport_Id_Ugyintez] [uniqueidentifier] NULL,
	[MegorzesiIdo] [datetime] NULL,
	[IratFajta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Irattipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[KuldKuldemenyek_Id] [uniqueidentifier] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[GeneraltTargy] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[IratMetaDef_Id] [uniqueidentifier] NULL,
	[IrattarbaKuldDatuma] [datetime] NULL,
	[IrattarbaVetelDat] [datetime] NULL,
	[Ugyirat_Id] [uniqueidentifier] NULL,
	[Minosites] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[Hatarido] [datetime] NULL,
	[AdathordozoTipusa] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Sorszam] [int] NULL,
	[Munkaallomas] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UgyintezesModja] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[IntezesIdopontja] [datetime] NULL,
	[AKTIV] [int] NULL,
 CONSTRAINT [PK_EREC_IraIratokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_EREC_IraIratokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_IraIratokHistory_ID_VER] ON [dbo].[EREC_IraIratokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
