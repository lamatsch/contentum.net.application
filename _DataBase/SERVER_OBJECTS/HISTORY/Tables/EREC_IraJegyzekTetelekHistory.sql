ALTER TABLE [dbo].[EREC_IraJegyzekTetelekHistory] DROP CONSTRAINT [DF_EREC_IraJegyzekTetelekHistory_HistoryId]
GO
/****** Object:  Index [IX_EREC_IraJegyzekTetelekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_IraJegyzekTetelekHistory_ID_VER] ON [dbo].[EREC_IraJegyzekTetelekHistory]
GO
/****** Object:  Table [dbo].[EREC_IraJegyzekTetelekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_IraJegyzekTetelekHistory]
GO
/****** Object:  Table [dbo].[EREC_IraJegyzekTetelekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EREC_IraJegyzekTetelekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Jegyzek_Id] [uniqueidentifier] NULL,
	[Jegyzek_Id_Ver] [int] NULL,
	[Sorszam] [int] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SztornozasDat] [datetime] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IraJegyzekTetelekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [IX_EREC_IraJegyzekTetelekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_IraJegyzekTetelekHistory_ID_VER] ON [dbo].[EREC_IraJegyzekTetelekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EREC_IraJegyzekTetelekHistory] ADD  CONSTRAINT [DF_EREC_IraJegyzekTetelekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
GO
