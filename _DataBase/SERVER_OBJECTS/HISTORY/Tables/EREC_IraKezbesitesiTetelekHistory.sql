/****** Object:  Index [IX_EREC_IraKezbesitesiTetelekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_IraKezbesitesiTetelekHistory_ID_VER] ON [dbo].[EREC_IraKezbesitesiTetelekHistory]
GO
/****** Object:  Table [dbo].[EREC_IraKezbesitesiTetelekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_IraKezbesitesiTetelekHistory]
GO
/****** Object:  Table [dbo].[EREC_IraKezbesitesiTetelekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EREC_IraKezbesitesiTetelekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_IraKezbesitesiTetelekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[KezbesitesFej_Id] [uniqueidentifier] NULL,
	[AtveteliFej_Id] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AtadasDat] [datetime] NULL,
	[AtvetelDat] [datetime] NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[UtolsoNyomtatas_Id] [uniqueidentifier] NULL,
	[UtolsoNyomtatasIdo] [datetime] NULL,
	[Felhasznalo_Id_Atado_USER] [uniqueidentifier] NULL,
	[Felhasznalo_Id_Atado_LOGIN] [uniqueidentifier] NULL,
	[Csoport_Id_Cel] [uniqueidentifier] NULL,
	[Felhasznalo_Id_AtvevoUser] [uniqueidentifier] NULL,
	[Felhasznalo_Id_AtvevoLogin] [uniqueidentifier] NULL,
	[Allapot] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Azonosito_szoveges] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UgyintezesModja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IraKezbesitesiTetelekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_EREC_IraKezbesitesiTetelekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_IraKezbesitesiTetelekHistory_ID_VER] ON [dbo].[EREC_IraKezbesitesiTetelekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
