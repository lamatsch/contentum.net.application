/****** Object:  Index [IX_EREC_IraOnkormAdatokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_IraOnkormAdatokHistory_ID_VER] ON [dbo].[EREC_IraOnkormAdatokHistory]
GO
/****** Object:  Table [dbo].[EREC_IraOnkormAdatokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_IraOnkormAdatokHistory]
GO
/****** Object:  Table [dbo].[EREC_IraOnkormAdatokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EREC_IraOnkormAdatokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_IraOnkormAdatokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[IraIratok_Id] [uniqueidentifier] NULL,
	[UgyFajtaja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[DontestHozta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[DontesFormaja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[UgyintezesHataridore] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[JogorvoslatiEljarasTipusa] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[JogorvoslatiDontesTipusa] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[JogorvoslatiDontestHozta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[JogorvoslatiDontesTartalma] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[HataridoTullepes] [int] NULL,
	[JogorvoslatiDontes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[HatosagiEllenorzes] [char](1) COLLATE Hungarian_CI_AS NULL,
	[MunkaorakSzama] [float] NULL,
	[EljarasiKoltseg] [int] NULL,
	[KozigazgatasiBirsagMerteke] [int] NULL,
	[SommasEljDontes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[NyolcNapBelulNemSommas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoHatalyuHatarozat] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoHatHatalybaLepes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoHatalyuVegzes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoVegzesHatalyba] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[HatAltalVisszafizOsszeg] [int] NULL,
	[HatTerheloEljKtsg] [int] NULL,
	[FelfuggHatarozat] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_IraOnkormAdatokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_EREC_IraOnkormAdatokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_IraOnkormAdatokHistory_ID_VER] ON [dbo].[EREC_IraOnkormAdatokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
