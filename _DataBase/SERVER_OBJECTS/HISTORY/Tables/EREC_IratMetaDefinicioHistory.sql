/****** Object:  Index [IX_EREC_IratMetaDefinicioHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_IratMetaDefinicioHistory_ID_VER] ON [dbo].[EREC_IratMetaDefinicioHistory]
GO
/****** Object:  Table [dbo].[EREC_IratMetaDefinicioHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_IratMetaDefinicioHistory]
GO
/****** Object:  Table [dbo].[EREC_IratMetaDefinicioHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EREC_IratMetaDefinicioHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_IratMetaDefinicioHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Ugykor_Id] [uniqueidentifier] NULL,
	[UgykorKod] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Ugytipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[UgytipusNev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[EljarasiSzakasz] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Irattipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[GeneraltTargy] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Rovidnev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UgyFajta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[UgyiratIntezesiIdo] [int] NULL,
	[Idoegyseg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[UgyiratIntezesiIdoKotott] [char](1) COLLATE Hungarian_CI_AS NULL,
	[UgyiratHataridoKitolas] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IratMetaDefinicioHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_EREC_IratMetaDefinicioHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_IratMetaDefinicioHistory_ID_VER] ON [dbo].[EREC_IratMetaDefinicioHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
