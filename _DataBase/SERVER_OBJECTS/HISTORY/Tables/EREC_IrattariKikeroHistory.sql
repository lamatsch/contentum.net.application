ALTER TABLE [dbo].[EREC_IrattariKikeroHistory] DROP CONSTRAINT [DF_EREC_IrattariKikeroHistory_HistoryId]
GO
/****** Object:  Index [IX_EREC_IrattariKikeroHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_IrattariKikeroHistory_ID_VER] ON [dbo].[EREC_IrattariKikeroHistory]
GO
/****** Object:  Table [dbo].[EREC_IrattariKikeroHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_IrattariKikeroHistory]
GO
/****** Object:  Table [dbo].[EREC_IrattariKikeroHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EREC_IrattariKikeroHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Keres_note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[FelhasznalasiCel] [char](1) COLLATE Hungarian_CI_AS NULL,
	[DokumentumTipus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Indoklas_note] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[FelhasznaloCsoport_Id_Kikero] [uniqueidentifier] NULL,
	[KeresDatuma] [datetime] NULL,
	[KulsoAzonositok] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UgyUgyirat_Id] [uniqueidentifier] NULL,
	[UgyUgyirat_Id_Ver] [int] NULL,
	[Tertiveveny_Id] [uniqueidentifier] NULL,
	[Tertiveveny_Id_Ver] [int] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[KikerKezd] [datetime] NULL,
	[KikerVege] [datetime] NULL,
	[FelhasznaloCsoport_Id_Jovahagy] [uniqueidentifier] NULL,
	[JovagyasDatuma] [datetime] NULL,
	[FelhasznaloCsoport_Id_Kiado] [uniqueidentifier] NULL,
	[KiadasDatuma] [datetime] NULL,
	[FelhasznaloCsoport_Id_Visszaad] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Visszave] [uniqueidentifier] NULL,
	[VisszaadasDatuma] [datetime] NULL,
	[SztornirozasDat] [datetime] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Irattar_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IrattariKikeroHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_EREC_IrattariKikeroHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_IrattariKikeroHistory_ID_VER] ON [dbo].[EREC_IrattariKikeroHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EREC_IrattariKikeroHistory] ADD  CONSTRAINT [DF_EREC_IrattariKikeroHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
GO
