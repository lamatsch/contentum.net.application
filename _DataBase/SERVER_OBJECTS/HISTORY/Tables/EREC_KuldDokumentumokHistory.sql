ALTER TABLE [dbo].[EREC_KuldDokumentumokHistory] DROP CONSTRAINT [DF_EREC_KuldDokumentumokHistory_HistoryId]
GO
/****** Object:  Index [IX_EREC_KuldDokumentumokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_KuldDokumentumokHistory_ID_VER] ON [dbo].[EREC_KuldDokumentumokHistory]
GO
/****** Object:  Table [dbo].[EREC_KuldDokumentumokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_KuldDokumentumokHistory]
GO
/****** Object:  Table [dbo].[EREC_KuldDokumentumokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EREC_KuldDokumentumokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Dokumentum_Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id_Ver] [int] NULL,
	[Leiras] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Lapszam] [int] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Forras] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Formatum] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Vonalkodozas] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[DokumentumSzerep] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_KuldDokumentumokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_EREC_KuldDokumentumokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_KuldDokumentumokHistory_ID_VER] ON [dbo].[EREC_KuldDokumentumokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EREC_KuldDokumentumokHistory] ADD  CONSTRAINT [DF_EREC_KuldDokumentumokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
GO
