-- =========================================
-- Create History table for EREC_KuldKuldemenyek
-- =========================================
 
IF OBJECT_ID('EREC_KuldKuldemenyekHistory', 'U') IS NOT NULL
  DROP TABLE EREC_KuldKuldemenyekHistory
GO
 
CREATE TABLE EREC_KuldKuldemenyekHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 Allapot nvarchar(64),  
 BeerkezesIdeje datetime,  
 FelbontasDatuma datetime,  
  IraIktatokonyv_Id uniqueidentifier,
  
  IraIktatokonyv_Id_Ver int,  
 KuldesMod nvarchar(64),  
 Erkezteto_Szam int,  
 HivatkozasiSzam Nvarchar(100),  
 Targy Nvarchar(4000),  
 Tartalom Nvarchar(4000),  
 RagSzam Nvarchar(400),  
 Surgosseg nvarchar(64),  
 BelyegzoDatuma datetime,  
 UgyintezesModja nvarchar(64),  
 PostazasIranya nvarchar(64),  
 Tovabbito nvarchar(64),  
 PeldanySzam int,  
 IktatniKell nvarchar(64),  
 Iktathato nvarchar(64),  
 SztornirozasDat datetime,  
  KuldKuldemeny_Id_Szulo uniqueidentifier,
  
  KuldKuldemeny_Id_Szulo_Ver int,  
 Erkeztetes_Ev int,  
  Csoport_Id_Cimzett uniqueidentifier,
  
  Csoport_Id_Cimzett_Ver int,  
  Csoport_Id_Felelos uniqueidentifier,
  
  Csoport_Id_Felelos_Ver int,  
 FelhasznaloCsoport_Id_Expedial uniqueidentifier,  
 ExpedialasIdeje datetime,  
  FelhasznaloCsoport_Id_Orzo uniqueidentifier,
  
  FelhasznaloCsoport_Id_Orzo_Ver int,  
  FelhasznaloCsoport_Id_Atvevo uniqueidentifier,
  
  FelhasznaloCsoport_Id_Atvevo_Ver int,  
  Partner_Id_Bekuldo uniqueidentifier,
  
  Partner_Id_Bekuldo_Ver int,  
  Cim_Id uniqueidentifier,
  
  Cim_Id_Ver int,  
 CimSTR_Bekuldo Nvarchar(400),  
 NevSTR_Bekuldo Nvarchar(400),  
 AdathordozoTipusa nvarchar(64),  
 ElsodlegesAdathordozoTipusa nvarchar(64),  
  FelhasznaloCsoport_Id_Alairo uniqueidentifier,
  
  FelhasznaloCsoport_Id_Alairo_Ver int,  
 BarCode Nvarchar(20),  
  FelhasznaloCsoport_Id_Bonto uniqueidentifier,
  
  FelhasznaloCsoport_Id_Bonto_Ver int,  
  CsoportFelelosEloszto_Id uniqueidentifier,
  
  CsoportFelelosEloszto_Id_Ver int,  
  Csoport_Id_Felelos_Elozo uniqueidentifier,
  
  Csoport_Id_Felelos_Elozo_Ver int,  
 Kovetkezo_Felelos_Id uniqueidentifier,  
 Elektronikus_Kezbesitesi_Allap nvarchar(64),  
 Kovetkezo_Orzo_Id uniqueidentifier,  
 Fizikai_Kezbesitesi_Allapot nvarchar(64),  
 IraIratok_Id uniqueidentifier,  
 BontasiMegjegyzes Nvarchar(400),  
 Tipus nvarchar(64),  
 Minosites nvarchar(64),  
 MegtagadasIndoka Nvarchar(4000),  
 Megtagado_Id uniqueidentifier,  
 MegtagadasDat datetime,  
 KimenoKuldemenyFajta nvarchar(64),  
 Elsobbsegi char(1),  
 Ajanlott char(1),  
 Tertiveveny char(1),  
 SajatKezbe char(1),  
 E_ertesites char(1),  
 E_elorejelzes char(1),  
 PostaiLezaroSzolgalat char(1),  
 Ar Nvarchar(100),  
 KimenoKuld_Sorszam  int,  
 Ver int,  
 TovabbitasAlattAllapot nvarchar(64),  
 Azonosito Nvarchar(100),  
 BoritoTipus nvarchar(64),  
 MegorzesJelzo char(1),  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 IktatastNemIgenyel char(1),  
 KezbesitesModja Nvarchar(100),  
 Munkaallomas Nvarchar(100),  
 SerultKuldemeny char(1),  
 TevesCimzes char(1),  
 TevesErkeztetes char(1),  
 CimzesTipusa nvarchar(64),  
 FutarJegyzekListaSzama Nvarchar(100),  
 Minosito uniqueidentifier,  
 MinositesErvenyessegiIdeje datetime,  
 TerjedelemMennyiseg int,  
 TerjedelemMennyisegiEgyseg nvarchar(64),  
 TerjedelemMegjegyzes Nvarchar(400),  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_EREC_KuldKuldemenyekHistory_ID_VER ON EREC_KuldKuldemenyekHistory
(
	Id,Ver ASC
)
GO