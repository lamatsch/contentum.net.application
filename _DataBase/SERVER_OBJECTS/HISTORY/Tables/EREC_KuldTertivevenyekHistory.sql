ALTER TABLE [dbo].[EREC_KuldTertivevenyekHistory] DROP CONSTRAINT [DF_EREC_KuldTertivevenyekHistory_HistoryId]
GO
/****** Object:  Index [IX_EREC_KuldTertivevenyekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_KuldTertivevenyekHistory_ID_VER] ON [dbo].[EREC_KuldTertivevenyekHistory]
GO
/****** Object:  Table [dbo].[EREC_KuldTertivevenyekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_KuldTertivevenyekHistory]
GO
/****** Object:  Table [dbo].[EREC_KuldTertivevenyekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EREC_KuldTertivevenyekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Kuldemeny_Id] [uniqueidentifier] NULL,
	[Kuldemeny_Id_Ver] [int] NULL,
	[Ragszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[TertivisszaKod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[TertivisszaDat] [datetime] NULL,
	[AtvevoSzemely] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[AtvetelDat] [datetime] NULL,
	[KezbVelelemBeallta] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KezbVelelemDatuma] [datetime] NULL,
 CONSTRAINT [PK_EREC_KuldTertivevenyekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_EREC_KuldTertivevenyekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_KuldTertivevenyekHistory_ID_VER] ON [dbo].[EREC_KuldTertivevenyekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EREC_KuldTertivevenyekHistory] ADD  CONSTRAINT [DF_EREC_KuldTertivevenyekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
GO
