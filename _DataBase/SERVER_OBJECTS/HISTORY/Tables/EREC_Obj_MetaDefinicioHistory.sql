/****** Object:  Index [IX_EREC_Obj_MetaDefinicioHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_Obj_MetaDefinicioHistory_ID_VER] ON [dbo].[EREC_Obj_MetaDefinicioHistory]
GO
/****** Object:  Table [dbo].[EREC_Obj_MetaDefinicioHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_Obj_MetaDefinicioHistory]
GO
/****** Object:  Table [dbo].[EREC_Obj_MetaDefinicioHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EREC_Obj_MetaDefinicioHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_Obj_MetaDefinicioHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Objtip_Id] [uniqueidentifier] NULL,
	[Objtip_Id_Column] [uniqueidentifier] NULL,
	[ColumnValue] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[DefinicioTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Felettes_Obj_Meta] [uniqueidentifier] NULL,
	[MetaXSD] [xml] NULL,
	[ImportXML] [xml] NULL,
	[EgyebXML] [xml] NULL,
	[ContentType] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SPSSzinkronizalt] [char](1) COLLATE Hungarian_CI_AS NULL,
	[SPS_CTT_Id] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[WorkFlowVezerles] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_Obj_MetaDefinicioHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_EREC_Obj_MetaDefinicioHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_Obj_MetaDefinicioHistory_ID_VER] ON [dbo].[EREC_Obj_MetaDefinicioHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
