/****** Object:  Index [IX_EREC_PldIratPeldanyokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_PldIratPeldanyokHistory_ID_VER] ON [dbo].[EREC_PldIratPeldanyokHistory]
GO
/****** Object:  Table [dbo].[EREC_PldIratPeldanyokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_PldIratPeldanyokHistory]
GO
/****** Object:  Table [dbo].[EREC_PldIratPeldanyokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EREC_PldIratPeldanyokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_PldIratPeldanyokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[IraIrat_Id] [uniqueidentifier] NULL,
	[IraIrat_Id_Ver] [int] NULL,
	[Sorszam] [int] NULL,
	[SztornirozasDat] [datetime] NULL,
	[AtvetelDatuma] [datetime] NULL,
	[Eredet] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[KuldesMod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ragszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UgyintezesModja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[VisszaerkezesiHatarido] [datetime] NULL,
	[Visszavarolag] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[VisszaerkezesDatuma] [datetime] NULL,
	[Cim_id_Cimzett] [uniqueidentifier] NULL,
	[Partner_Id_Cimzett] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Orzo] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos_Elozo] [uniqueidentifier] NULL,
	[CimSTR_Cimzett] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[NevSTR_Cimzett] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Tovabbito] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[IraIrat_Id_Kapcsolt] [uniqueidentifier] NULL,
	[IrattariHely] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Gener_Id] [uniqueidentifier] NULL,
	[PostazasDatuma] [datetime] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Kovetkezo_Felelos_Id] [uniqueidentifier] NULL,
	[Elektronikus_Kezbesitesi_Allap] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Kovetkezo_Orzo_Id] [uniqueidentifier] NULL,
	[Fizikai_Kezbesitesi_Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[TovabbitasAlattAllapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[PostazasAllapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[ValaszElektronikus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[UgyUgyirat_Id_Kulso] [uniqueidentifier] NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AKTIV] [int] NULL,
 CONSTRAINT [PK_EREC_PldIratPeldanyokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_EREC_PldIratPeldanyokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_PldIratPeldanyokHistory_ID_VER] ON [dbo].[EREC_PldIratPeldanyokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
