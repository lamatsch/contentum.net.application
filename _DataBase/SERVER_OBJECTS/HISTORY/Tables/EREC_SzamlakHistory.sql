ALTER TABLE [dbo].[EREC_SzamlakHistory] DROP CONSTRAINT [DF_EREC_SzamlakHistory_HistoryId]
GO
/****** Object:  Index [IX_EREC_SzamlakHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_SzamlakHistory_ID_VER] ON [dbo].[EREC_SzamlakHistory]
GO
/****** Object:  Table [dbo].[EREC_SzamlakHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_SzamlakHistory]
GO
/****** Object:  Table [dbo].[EREC_SzamlakHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EREC_SzamlakHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[Dokumentum_Id] [uniqueidentifier] NULL,
	[Partner_Id_Szallito] [uniqueidentifier] NULL,
	[Cim_Id_Szallito] [uniqueidentifier] NULL,
	[Bankszamlaszam_Id] [uniqueidentifier] NULL,
	[SzamlaSorszam] [varchar](16) COLLATE Hungarian_CI_AS NULL,
	[FizetesiMod] [varchar](4) COLLATE Hungarian_CI_AS NULL,
	[TeljesitesDatuma] [smalldatetime] NULL,
	[BizonylatDatuma] [smalldatetime] NULL,
	[FizetesiHatarido] [smalldatetime] NULL,
	[DevizaKod] [varchar](3) COLLATE Hungarian_CI_AS NULL,
	[VisszakuldesDatuma] [smalldatetime] NULL,
	[EllenorzoOsszeg] [float] NULL,
	[PIRAllapot] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[VevoBesorolas] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_SzamlakHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_EREC_SzamlakHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_SzamlakHistory_ID_VER] ON [dbo].[EREC_SzamlakHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EREC_SzamlakHistory] ADD  CONSTRAINT [DF_EREC_SzamlakHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
GO
