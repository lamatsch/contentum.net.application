/****** Object:  Index [IX_EREC_TargySzavakHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_TargySzavakHistory_ID_VER] ON [dbo].[EREC_TargySzavakHistory]
GO
/****** Object:  Table [dbo].[EREC_TargySzavakHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_TargySzavakHistory]
GO
/****** Object:  Table [dbo].[EREC_TargySzavakHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EREC_TargySzavakHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_TargySzavakHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Tipus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[TargySzavak] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AlapertelmezettErtek] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[BelsoAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SPSSzinkronizalt] [char](1) COLLATE Hungarian_CI_AS NULL,
	[SPS_Field_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NULL,
	[RegExp] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ToolTip] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[XSD] [xml] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[ControlTypeSource] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[ControlTypeDataSource] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_TargySzavakHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_EREC_TargySzavakHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_TargySzavakHistory_ID_VER] ON [dbo].[EREC_TargySzavakHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
