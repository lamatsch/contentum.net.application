/****** Object:  Index [IX_EREC_UgyUgyiratokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_UgyUgyiratokHistory_ID_VER] ON [dbo].[EREC_UgyUgyiratokHistory]
GO
/****** Object:  Table [dbo].[EREC_UgyUgyiratokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_UgyUgyiratokHistory]
GO
/****** Object:  Table [dbo].[EREC_UgyUgyiratokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EREC_UgyUgyiratokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_UgyUgyiratokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Foszam] [int] NULL,
	[Ugyazonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Alkalmazas_Id] [uniqueidentifier] NULL,
	[UgyintezesModja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Hatarido] [datetime] NULL,
	[SkontrobaDat] [datetime] NULL,
	[LezarasDat] [datetime] NULL,
	[IrattarbaKuldDatuma] [datetime] NULL,
	[IrattarbaVetelDat] [datetime] NULL,
	[FelhCsoport_Id_IrattariAtvevo] [uniqueidentifier] NULL,
	[SelejtezesDat] [datetime] NULL,
	[FelhCsoport_Id_Selejtezo] [uniqueidentifier] NULL,
	[LeveltariAtvevoNeve] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[FelhCsoport_Id_Felulvizsgalo] [uniqueidentifier] NULL,
	[FelulvizsgalatDat] [datetime] NULL,
	[IktatoszamKieg] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[Targy] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[UgyUgyirat_Id_Szulo] [uniqueidentifier] NULL,
	[UgyUgyirat_Id_Szulo_Ver] [int] NULL,
	[UgyTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[IrattariHely] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos_Ver] [int] NULL,
	[FelhasznaloCsoport_Id_Orzo] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Orzo_Ver] [int] NULL,
	[Csoport_Id_Cimzett] [uniqueidentifier] NULL,
	[Jelleg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[IraIrattariTetel_Id] [uniqueidentifier] NULL,
	[IraIrattariTetel_Id_Ver] [int] NULL,
	[IraIktatokonyv_Id] [uniqueidentifier] NULL,
	[IraIktatokonyv_Id_Ver] [int] NULL,
	[SkontroOka] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SkontroVege] [datetime] NULL,
	[Surgosseg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[SkontrobanOsszesen] [int] NULL,
	[MegorzesiIdoVege] [datetime] NULL,
	[Partner_Id_Ugyindito] [uniqueidentifier] NULL,
	[Partner_Id_Ugyindito_Ver] [int] NULL,
	[NevSTR_Ugyindito] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[ElintezesDat] [datetime] NULL,
	[FelhasznaloCsoport_Id_Ugyintez] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Ugyintez_Ver] [int] NULL,
	[Csoport_Id_Felelos_Elozo] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos_Elozo_Ver] [int] NULL,
	[KolcsonKikerDat] [datetime] NULL,
	[KolcsonKiadDat] [datetime] NULL,
	[Kolcsonhatarido] [datetime] NULL,
	[BARCODE] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[IratMetadefinicio_Id] [uniqueidentifier] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[TovabbitasAlattAllapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Fizikai_Kezbesitesi_Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Kovetkezo_Orzo_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Ugyfelelos] [uniqueidentifier] NULL,
	[Elektronikus_Kezbesitesi_Allap] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Kovetkezo_Felelos_Id] [uniqueidentifier] NULL,
	[UtolsoAlszam] [int] NULL,
	[ElintezesMod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[RegirendszerIktatoszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[GeneraltTargy] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Cim_Id_Ugyindito] [uniqueidentifier] NULL,
	[CimSTR_Ugyindito] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
        FelfuggesztesOka Nvarchar(400),
        UgyintezesKezdete datetime,
        FelfuggesztettNapokSzama int,
        IntezesiIdo int,  
        IntezesiIdoegyseg nvarchar(64),  
        Ugy_Fajtaja nvarchar(64),
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[UgyUgyirat_Id_Kulso] [uniqueidentifier] NULL,
	[IratSzam] [int] NULL,
	[UtolsoSorszam] [int] NULL,
	[Sorszam] [int] NULL,
	[UjOrzesiIdo] [int] NULL,
	[IrattarId] [uniqueidentifier] NULL,
	[LezarasOka] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[AKTIV] [int] NULL,
	[SkontroOka_Kod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_UgyUgyiratokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [IX_EREC_UgyUgyiratokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_UgyUgyiratokHistory_ID_VER] ON [dbo].[EREC_UgyUgyiratokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
