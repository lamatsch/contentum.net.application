-- =========================================
-- Create History table for EREC_eBeadvanyCsatolmanyok
-- =========================================
 
IF OBJECT_ID('EREC_eBeadvanyCsatolmanyokHistory', 'U') IS NOT NULL
  DROP TABLE EREC_eBeadvanyCsatolmanyokHistory
GO
 
CREATE TABLE EREC_eBeadvanyCsatolmanyokHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 eBeadvany_Id uniqueidentifier,  
 Dokumentum_Id uniqueidentifier,  
 Nev Nvarchar(400),  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_EREC_eBeadvanyCsatolmanyokHistory_ID_VER ON EREC_eBeadvanyCsatolmanyokHistory
(
	Id,Ver ASC
)
GO