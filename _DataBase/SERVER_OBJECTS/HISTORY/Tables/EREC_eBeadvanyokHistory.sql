-- =========================================
-- Create History table for EREC_eBeadvanyok
-- =========================================
 
IF OBJECT_ID('EREC_eBeadvanyokHistory', 'U') IS NOT NULL
  DROP TABLE EREC_eBeadvanyokHistory
GO
 
CREATE TABLE EREC_eBeadvanyokHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 Irany char(1),  
 Allapot nvarchar(64),  
 KuldoRendszer Nvarchar(400),  
 UzenetTipusa nvarchar(64),  
 FeladoTipusa int,  
 PartnerKapcsolatiKod Nvarchar(400),  
 PartnerNev Nvarchar(400),  
 PartnerEmail Nvarchar(400),  
 PartnerRovidNev Nvarchar(400),  
 PartnerMAKKod Nvarchar(400),  
 PartnerKRID Nvarchar(400),  
 Partner_Id uniqueidentifier,  
 Cim_Id uniqueidentifier,  
 KR_HivatkozasiSzam Nvarchar(400),  
 KR_ErkeztetesiSzam Nvarchar(400),  
 Contentum_HivatkozasiSzam uniqueidentifier,  
 PR_HivatkozasiSzam Nvarchar(400),  
 PR_ErkeztetesiSzam Nvarchar(400),  
 KR_DokTipusHivatal Nvarchar(400),  
 KR_DokTipusAzonosito Nvarchar(400),  
 KR_DokTipusLeiras Nvarchar(400),  
 KR_Megjegyzes Nvarchar(4000),  
 KR_ErvenyessegiDatum datetime,  
 KR_ErkeztetesiDatum datetime,  
 KR_FileNev Nvarchar(400),  
 KR_Kezbesitettseg int,  
 KR_Idopecset Nvarchar(4000),  
 KR_Valasztitkositas char(1),  
 KR_Valaszutvonal int,  
 KR_Rendszeruzenet char(1),  
 KR_Tarterulet int,  
 KR_ETertiveveny char(1),  
 KR_Lenyomat Nvarchar(Max),  
 KuldKuldemeny_Id uniqueidentifier,  
 IraIrat_Id uniqueidentifier,  
 IratPeldany_Id uniqueidentifier,  
 Cel Nvarchar(400),  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_EREC_eBeadvanyokHistory_ID_VER ON EREC_eBeadvanyokHistory
(
	Id,Ver ASC
)
GO