/****** Object:  Index [IX_EREC_eMailBoritekokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_eMailBoritekokHistory_ID_VER] ON [dbo].[EREC_eMailBoritekokHistory]
GO
/****** Object:  Table [dbo].[EREC_eMailBoritekokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_eMailBoritekokHistory]
GO
/****** Object:  Table [dbo].[EREC_eMailBoritekokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EREC_eMailBoritekokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_EREC_eMailBoritekokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Felado] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Cimzett] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Targy] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[FeladasDatuma] [datetime] NULL,
	[ErkezesDatuma] [datetime] NULL,
	[Fontossag] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[IraIrat_Id] [uniqueidentifier] NULL,
	[DigitalisAlairas] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Uzenet] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[EmailForras] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[EmailGuid] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[FeldolgozasIdo] [datetime] NULL,
	[ForrasTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[CC] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_eMailBoritekokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Index [IX_EREC_eMailBoritekokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_eMailBoritekokHistory_ID_VER] ON [dbo].[EREC_eMailBoritekokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
