ALTER TABLE [dbo].[EREC_eMailFiokokHistory] DROP CONSTRAINT [DF_EREC_eMailFiokokHistory_HistoryId]
GO
/****** Object:  Index [IX_EREC_eMailFiokokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_EREC_eMailFiokokHistory_ID_VER] ON [dbo].[EREC_eMailFiokokHistory]
GO
/****** Object:  Table [dbo].[EREC_eMailFiokokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[EREC_eMailFiokokHistory]
GO
/****** Object:  Table [dbo].[EREC_eMailFiokokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EREC_eMailFiokokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UserNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Csoportok_Id] [uniqueidentifier] NULL,
	[Jelszo] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[NapiMax] [int] NULL,
	[NapiTeny] [int] NULL,
	[EmailCim] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Modul_Id] [uniqueidentifier] NULL,
	[MappaNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_eMailFiokokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_EREC_eMailFiokokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_EREC_eMailFiokokHistory_ID_VER] ON [dbo].[EREC_eMailFiokokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EREC_eMailFiokokHistory] ADD  CONSTRAINT [DF_EREC_eMailFiokokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
GO
