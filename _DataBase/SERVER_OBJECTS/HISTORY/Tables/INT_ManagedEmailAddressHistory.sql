-- =========================================
-- Create History table for INT_ManagedEmailAddress
-- =========================================
 
IF OBJECT_ID('INT_ManagedEmailAddressHistory', 'U') IS NOT NULL
  DROP TABLE INT_ManagedEmailAddressHistory
GO
 
CREATE TABLE INT_ManagedEmailAddressHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 Org uniqueidentifier,  
 Felhasznalo_id uniqueidentifier,  
 Nev Nvarchar(400),  
 Ertek Nvarchar(max),  
 Karbantarthato char(1),  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_INT_ManagedEmailAddressHistory_ID_VER ON INT_ManagedEmailAddressHistory
(
	Id,Ver ASC
)
GO