ALTER TABLE [dbo].[KRT_BankszamlaszamokHistory] DROP CONSTRAINT [DF_KRT_BankszamlaszamokHistory_HistoryId]
GO
/****** Object:  Index [IX_KRT_BankszamlaszamokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_KRT_BankszamlaszamokHistory_ID_VER] ON [dbo].[KRT_BankszamlaszamokHistory]
GO
/****** Object:  Table [dbo].[KRT_BankszamlaszamokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[KRT_BankszamlaszamokHistory]
GO
/****** Object:  Table [dbo].[KRT_BankszamlaszamokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KRT_BankszamlaszamokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Partner_Id] [uniqueidentifier] NULL,
	[Partner_Id_Ver] [int] NULL,
	[Bankszamlaszam] [varchar](34) COLLATE Hungarian_CI_AS NULL,
	[Partner_Id_Bank] [uniqueidentifier] NULL,
	[Partner_Id_Bank_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_BankszamlaszamokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_KRT_BankszamlaszamokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_KRT_BankszamlaszamokHistory_ID_VER] ON [dbo].[KRT_BankszamlaszamokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[KRT_BankszamlaszamokHistory] ADD  CONSTRAINT [DF_KRT_BankszamlaszamokHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
GO
