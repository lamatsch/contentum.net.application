/****** Object:  Index [IX_KRT_CimekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_KRT_CimekHistory_ID_VER] ON [dbo].[KRT_CimekHistory]
GO
/****** Object:  Table [dbo].[KRT_CimekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[KRT_CimekHistory]
GO
/****** Object:  Table [dbo].[KRT_CimekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KRT_CimekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_CimekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Kategoria] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KulsoAzonositok] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Forras] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Orszag_Id] [uniqueidentifier] NULL,
	[Orszag_Id_Ver] [int] NULL,
	[OrszagNev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Telepules_Id] [uniqueidentifier] NULL,
	[Telepules_Id_Ver] [int] NULL,
	[TelepulesNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[IRSZ] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[CimTobbi] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Kozterulet_Id] [uniqueidentifier] NULL,
	[Kozterulet_Id_Ver] [int] NULL,
	[KozteruletNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KozteruletTipus_Id] [uniqueidentifier] NULL,
	[KozteruletTipus_Id_Ver] [int] NULL,
	[KozteruletTipusNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Hazszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Hazszamig] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[HazszamBetujel] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[MindketOldal] [char](1) COLLATE Hungarian_CI_AS NULL,
	[HRSZ] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Lepcsohaz] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Szint] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Ajto] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[AjtoBetujel] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Tobbi] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_CimekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_KRT_CimekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_KRT_CimekHistory_ID_VER] ON [dbo].[KRT_CimekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
