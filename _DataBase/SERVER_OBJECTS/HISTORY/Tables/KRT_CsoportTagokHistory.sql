/****** Object:  Index [IX_KRT_CsoportTagokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_KRT_CsoportTagokHistory_ID_VER] ON [dbo].[KRT_CsoportTagokHistory]
GO
/****** Object:  Table [dbo].[KRT_CsoportTagokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[KRT_CsoportTagokHistory]
GO
/****** Object:  Table [dbo].[KRT_CsoportTagokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KRT_CsoportTagokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_CsoportTagokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Ver] [int] NULL,
	[Csoport_Id_Jogalany] [uniqueidentifier] NULL,
	[Csoport_Id_Jogalany_Ver] [int] NULL,
	[ObjTip_Id_Jogalany] [uniqueidentifier] NULL,
	[ErtesitesMailCim] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[ErtesitesKell] [char](1) COLLATE Hungarian_CI_AS NULL,
	[System] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Orokolheto] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[ObjektumTulajdonos] [char](1) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_KRT_CsoportTagokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_KRT_CsoportTagokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_KRT_CsoportTagokHistory_ID_VER] ON [dbo].[KRT_CsoportTagokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
