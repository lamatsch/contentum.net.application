/****** Object:  Index [IX_KRT_DokumentumokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_KRT_DokumentumokHistory_ID_VER] ON [dbo].[KRT_DokumentumokHistory]
GO
/****** Object:  Table [dbo].[KRT_DokumentumokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[KRT_DokumentumokHistory]
GO
/****** Object:  Table [dbo].[KRT_DokumentumokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KRT_DokumentumokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_DokumentumokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[FajlNev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[VerzioJel] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Formatum] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Meret] [int] NULL,
	[TartalomHash] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AlairtTartalomHash] [varbinary](4000) NULL,
	[KivonatHash] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Dokumentum_Id] [uniqueidentifier] NULL,
	[Dokumentum_Id_Kovetkezo] [uniqueidentifier] NULL,
	[Dokumentum_Id_Kovetkezo_Ver] [int] NULL,
	[Alkalmazas_Id] [uniqueidentifier] NULL,
	[Alkalmazas_Id_Ver] [int] NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NULL,
	[KulsoAzonositok] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[External_Source] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[External_Link] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[External_Id] [uniqueidentifier] NULL,
	[External_Info] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[CheckedOut] [uniqueidentifier] NULL,
	[CheckedOutTime] [datetime] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[OCRPrioritas] [int] NULL,
	[OCRAllapot] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Megnyithato] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Olvashato] [char](1) COLLATE Hungarian_CI_AS NULL,
	[ElektronikusAlairas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[AlairasFelulvizsgalat] [datetime] NULL,
	[Titkositas] [char](1) COLLATE Hungarian_CI_AS NULL,
	[SablonAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_Id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_Id] [uniqueidentifier] NULL,
	[UIAccessLog_Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[WorkFlowAllapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_KRT_DokumentumokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_KRT_DokumentumokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_KRT_DokumentumokHistory_ID_VER] ON [dbo].[KRT_DokumentumokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
