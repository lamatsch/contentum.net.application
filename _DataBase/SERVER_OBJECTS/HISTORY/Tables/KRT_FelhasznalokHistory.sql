/****** Object:  Index [IX_KRT_FelhasznalokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_KRT_FelhasznalokHistory_ID_VER] ON [dbo].[KRT_FelhasznalokHistory]
GO
/****** Object:  Table [dbo].[KRT_FelhasznalokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[KRT_FelhasznalokHistory]
GO
/****** Object:  Table [dbo].[KRT_FelhasznalokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KRT_FelhasznalokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_FelhasznalokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[Partner_id] [uniqueidentifier] NULL,
	[Partner_id_Ver] [int] NULL,
	[Tipus] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[UserNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Jelszo] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[JelszoLejaratIdo] [datetime] NULL,
	[System] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Kiszolgalo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[DefaultPrivatKulcs] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Partner_Id_Munkahely] [uniqueidentifier] NULL,
	[Partner_Id_Munkahely_Ver] [int] NULL,
	[MaxMinosites] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[EMail] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Engedelyezett] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Telefonszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Beosztas] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_FelhasznalokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_KRT_FelhasznalokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_KRT_FelhasznalokHistory_ID_VER] ON [dbo].[KRT_FelhasznalokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
