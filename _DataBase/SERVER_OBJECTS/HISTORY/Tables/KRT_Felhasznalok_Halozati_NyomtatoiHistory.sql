ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory] DROP CONSTRAINT [DF__KRT_Felhasz__Ver__3B40CD36]
GO
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory] DROP CONSTRAINT [DF_KRT_Felhasznalok_Halozati_NyomtatoiHistoryId]
GO
/****** Object:  Table [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory]
GO
/****** Object:  Table [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[Felhasznalo_Id] [uniqueidentifier] NOT NULL,
	[Halozati_Nyomtato_Id] [uniqueidentifier] NOT NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_Felhasznalok_Halozati_NyomtatoiHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory] ADD  CONSTRAINT [DF_KRT_Felhasznalok_Halozati_NyomtatoiHistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
GO
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory] ADD  DEFAULT ((1)) FOR [Ver]
GO
