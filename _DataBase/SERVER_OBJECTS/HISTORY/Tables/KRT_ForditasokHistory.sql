-- =========================================
-- Create History table for KRT_Forditasok
-- =========================================
 
IF OBJECT_ID('KRT_ForditasokHistory', 'U') IS NOT NULL
  DROP TABLE KRT_ForditasokHistory
GO
 
CREATE TABLE KRT_ForditasokHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 NyelvKod Nvarchar(10),  
 OrgKod Nvarchar(100),  
 Modul Nvarchar(400),  
 Komponens Nvarchar(400),  
 ObjAzonosito Nvarchar(400),  
 Forditas Nvarchar(4000),  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_KRT_ForditasokHistory_ID_VER ON KRT_ForditasokHistory
(
	Id,Ver ASC
)
GO