/****** Object:  Index [IX_KRT_FunkciokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_KRT_FunkciokHistory_ID_VER] ON [dbo].[KRT_FunkciokHistory]
GO
/****** Object:  Table [dbo].[KRT_FunkciokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[KRT_FunkciokHistory]
GO
/****** Object:  Table [dbo].[KRT_FunkciokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KRT_FunkciokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_FunkciokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[ObjTipus_Id_AdatElem] [uniqueidentifier] NULL,
	[ObjTipus_Id_AdatElem_Ver] [int] NULL,
	[ObjStat_Id_Kezd] [uniqueidentifier] NULL,
	[ObjStat_Id_Kezd_Ver] [int] NULL,
	[Alkalmazas_Id] [uniqueidentifier] NULL,
	[Alkalmazas_Id_Ver] [int] NULL,
	[Muvelet_Id] [uniqueidentifier] NULL,
	[Muvelet_Id_Ver] [int] NULL,
	[ObjStat_Id_Veg] [uniqueidentifier] NULL,
	[ObjStat_Id_Veg_Ver] [int] NULL,
	[Leiras] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Funkcio_Id_Szulo] [uniqueidentifier] NULL,
	[Funkcio_Id_Szulo_Ver] [int] NULL,
	[Csoportosito] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Modosithato] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Org] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[MunkanaploJelzo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[FeladatJelzo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KeziFeladatJelzo] [char](1) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_KRT_FunkciokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_KRT_FunkciokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_KRT_FunkciokHistory_ID_VER] ON [dbo].[KRT_FunkciokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
