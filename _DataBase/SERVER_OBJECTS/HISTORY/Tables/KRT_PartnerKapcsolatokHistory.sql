/****** Object:  Index [IX_KRT_PartnerKapcsolatokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_KRT_PartnerKapcsolatokHistory_ID_VER] ON [dbo].[KRT_PartnerKapcsolatokHistory]
GO
/****** Object:  Table [dbo].[KRT_PartnerKapcsolatokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[KRT_PartnerKapcsolatokHistory]
GO
/****** Object:  Table [dbo].[KRT_PartnerKapcsolatokHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KRT_PartnerKapcsolatokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_PartnerKapcsolatokHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Partner_id] [uniqueidentifier] NULL,
	[Partner_id_Ver] [int] NULL,
	[Partner_id_kapcsolt] [uniqueidentifier] NULL,
	[Partner_id_kapcsolt_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_PartnerKapcsolatokHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [IX_KRT_PartnerKapcsolatokHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_KRT_PartnerKapcsolatokHistory_ID_VER] ON [dbo].[KRT_PartnerKapcsolatokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
