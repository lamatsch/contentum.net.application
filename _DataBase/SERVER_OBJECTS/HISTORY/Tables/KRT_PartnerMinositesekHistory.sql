ALTER TABLE [dbo].[KRT_PartnerMinositesekHistory] DROP CONSTRAINT [DF_KRT_PartnerMinositesekHistory_HistoryId]
GO
/****** Object:  Index [IX_KRT_PartnerMinositesekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_KRT_PartnerMinositesekHistory_ID_VER] ON [dbo].[KRT_PartnerMinositesekHistory]
GO
/****** Object:  Table [dbo].[KRT_PartnerMinositesekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[KRT_PartnerMinositesekHistory]
GO
/****** Object:  Table [dbo].[KRT_PartnerMinositesekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KRT_PartnerMinositesekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[ALLAPOT] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Partner_id] [uniqueidentifier] NULL,
	[Partner_id_Ver] [int] NULL,
	[Felhasznalo_id_kero] [uniqueidentifier] NULL,
	[Felhasznalo_id_kero_Ver] [int] NULL,
	[KertMinosites] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[KertKezdDat] [datetime] NULL,
	[KertVegeDat] [datetime] NULL,
	[KerelemAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KerelemBeadIdo] [datetime] NULL,
	[KerelemDontesIdo] [datetime] NULL,
	[Felhasznalo_id_donto] [uniqueidentifier] NULL,
	[Felhasznalo_id_donto_Ver] [int] NULL,
	[DontesAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Minosites] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[MinositesKezdDat] [datetime] NULL,
	[MinositesVegDat] [datetime] NULL,
	[SztornirozasDat] [datetime] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_PartnerMinositesekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_KRT_PartnerMinositesekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_KRT_PartnerMinositesekHistory_ID_VER] ON [dbo].[KRT_PartnerMinositesekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[KRT_PartnerMinositesekHistory] ADD  CONSTRAINT [DF_KRT_PartnerMinositesekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
GO
