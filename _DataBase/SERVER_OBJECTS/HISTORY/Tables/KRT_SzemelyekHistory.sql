/****** Object:  Index [IX_KRT_SzemelyekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_KRT_SzemelyekHistory_ID_VER] ON [dbo].[KRT_SzemelyekHistory]
GO
/****** Object:  Table [dbo].[KRT_SzemelyekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[KRT_SzemelyekHistory]
GO
/****** Object:  Table [dbo].[KRT_SzemelyekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KRT_SzemelyekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_KRT_SzemelyekHistory_HistoryId]  DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Partner_Id] [uniqueidentifier] NULL,
	[Partner_Id_Ver] [int] NULL,
	[AnyjaNeve] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[ApjaNeve] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiOrszag] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiOrszagId] [uniqueidentifier] NULL,
	[UjTitulis] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[UjCsaladiNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UjUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiHely] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiHely_id] [uniqueidentifier] NULL,
	[SzuletesiIdo] [datetime] NULL,
	[TAJSzam] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[SZIGSzam] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Neme] [char](1) COLLATE Hungarian_CI_AS NULL,
	[SzemelyiAzonosito] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Adoazonosito] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Adoszam] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[AnyjaNeveCsaladiNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AnyjaNeveElsoUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AnyjaNeveTovabbiUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiCsaladiNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiElsoUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiTovabbiUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UjTovabbiUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Allampolgarsag] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KulfoldiAdoszamJelolo] [char](1) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_KRT_SzemelyekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_KRT_SzemelyekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_KRT_SzemelyekHistory_ID_VER] ON [dbo].[KRT_SzemelyekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
