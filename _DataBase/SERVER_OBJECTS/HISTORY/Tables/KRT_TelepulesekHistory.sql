ALTER TABLE [dbo].[KRT_TelepulesekHistory] DROP CONSTRAINT [DF_KRT_TelepulesekHistory_HistoryId]
GO
/****** Object:  Index [IX_KRT_TelepulesekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_KRT_TelepulesekHistory_ID_VER] ON [dbo].[KRT_TelepulesekHistory]
GO
/****** Object:  Table [dbo].[KRT_TelepulesekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[KRT_TelepulesekHistory]
GO
/****** Object:  Table [dbo].[KRT_TelepulesekHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KRT_TelepulesekHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Org] [uniqueidentifier] NULL,
	[IRSZ] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Telepules_Id_Fo] [uniqueidentifier] NULL,
	[Telepules_Id_Fo_Ver] [int] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Orszag_Id] [uniqueidentifier] NULL,
	[Orszag_Id_Ver] [int] NULL,
	[Megye] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Regio] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_TelepulesekHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [IX_KRT_TelepulesekHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_KRT_TelepulesekHistory_ID_VER] ON [dbo].[KRT_TelepulesekHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[KRT_TelepulesekHistory] ADD  CONSTRAINT [DF_KRT_TelepulesekHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
GO
