ALTER TABLE [dbo].[KRT_TemplateManagerHistory] DROP CONSTRAINT [DF_KRT_TemplateManagerHistory_HistoryId]
GO
/****** Object:  Index [IX_KRT_TemplateManagerHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
DROP INDEX [IX_KRT_TemplateManagerHistory_ID_VER] ON [dbo].[KRT_TemplateManagerHistory]
GO
/****** Object:  Table [dbo].[KRT_TemplateManagerHistory]    Script Date: 2017.08.07. 9:50:28 ******/
DROP TABLE [dbo].[KRT_TemplateManagerHistory]
GO
/****** Object:  Table [dbo].[KRT_TemplateManagerHistory]    Script Date: 2017.08.07. 9:50:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KRT_TemplateManagerHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[KRT_Modul_Id] [uniqueidentifier] NULL,
	[KRT_Modul_Id_Ver] [int] NULL,
	[KRT_Dokumentum_Id] [uniqueidentifier] NULL,
	[KRT_Dokumentum_Id_Ver] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_TemplateManagerHistory] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [IX_KRT_TemplateManagerHistory_ID_VER]    Script Date: 2017.08.07. 9:50:28 ******/
CREATE NONCLUSTERED INDEX [IX_KRT_TemplateManagerHistory_ID_VER] ON [dbo].[KRT_TemplateManagerHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[KRT_TemplateManagerHistory] ADD  CONSTRAINT [DF_KRT_TemplateManagerHistory_HistoryId]  DEFAULT (newsequentialid()) FOR [HistoryId]
GO
