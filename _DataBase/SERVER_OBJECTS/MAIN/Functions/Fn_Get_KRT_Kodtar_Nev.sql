IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fn_Get_KRT_Kodtar_Nev]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[Fn_Get_KRT_Kodtar_Nev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fn_Get_KRT_Kodtar_Nev]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		AXIS RENDSZERHAZ KFT.
-- Create date: 2017.06
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[Fn_Get_KRT_Kodtar_Nev]
(
	@KodcsoportKod				NVARCHAR(MAX),
	@KodtarKod					NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @RET				NVARCHAR(MAX)
	
	DECLARE @KCSID				UNIQUEIDENTIFIER
	SELECT TOP 1 @KCSID = Id FROM KRT_Kodcsoportok WHERE Kod = @KodcsoportKod

	IF ( @KCSID IS NULL)
		RETURN NULL
	
	SELECT @RET = Nev
	FROM KRT_KodTarak
	WHERE KodCsoport_Id = @KCSID 
			AND
		   Kod = @KodtarKod

	RETURN @RET

END


' 
END

GO
