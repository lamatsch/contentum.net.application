IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fn_RagszamSavFelhasznaltElemSzam]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[Fn_RagszamSavFelhasznaltElemSzam]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fn_RagszamSavFelhasznaltElemSzam]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		AXIS RENDSZERHAZ KFT.
-- Create date: 2017.06
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[Fn_RagszamSavFelhasznaltElemSzam]
(
	@RagszamSavId				UNIQUEIDENTIFIER
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @RET				BIGINT
	
	SELECT @RET = COUNT(*)
	FROM KRT_RagSzamok
	WHERE RagszamSav_Id = @RagszamSavId

	RETURN @RET

END


' 
END

GO
