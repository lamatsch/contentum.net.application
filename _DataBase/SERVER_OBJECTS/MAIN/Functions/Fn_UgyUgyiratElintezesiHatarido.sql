SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select 1
            from  sysobjects
           where  id = object_id('Fn_UgyUgyiratElintezesiHatarido')
            and   type = 'FN')
   drop function Fn_UgyUgyiratElintezesiHatarido
go

-- =============================================
-- Author:		AXIS RENDSZERHAZ KFT.
-- Create date: 2017.05
-- Description:	
-- =============================================
CREATE FUNCTION dbo.Fn_UgyUgyiratElintezesiHatarido
(
	@UgyiratId					UNIQUEIDENTIFIER,
	@IntezesiIdoegyseg			nvarchar(64),
	@IratMetaDefinicioId		UNIQUEIDENTIFIER	=	NULL,
	@StartDate					DATETIME,
	@EndDate					DATETIME
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @RET				NVARCHAR(MAX)
	DECLARE @TMP				INT				=		0
	DECLARE @VALUE_IDOEGYSEG	VARCHAR(100)
	DECLARE @CURRENT_DATE		DATETIME		=		GETDATE()
	
	IF  (@CURRENT_DATE > DATEADD(Day, 1, DATEDIFF(Day, 0, @EndDate))) 
		RETURN 	'Lej�rt'

	IF @IntezesiIdoegyseg is null
	BEGIN
		IF (@IratMetadefinicioId IS NULL)
			BEGIN
				SELECT TOP 1 @VALUE_IDOEGYSEG = Ertek 
				FROM KRT_PARAMETEREK
				WHERE NEV = 'DEFAULT_INTEZESI_IDO_IDOEGYSEG' 
			END
		ELSE
			BEGIN
				  SELECT @VALUE_IDOEGYSEG = Idoegyseg 
				  FROM EREC_IratMetadefinicio
				  WHERE Id = @IratMetaDefinicioId

				  IF (@VALUE_IDOEGYSEG IS NULL)
				  BEGIN
						SELECT TOP 1 @VALUE_IDOEGYSEG = Ertek 
						FROM KRT_PARAMETEREK
						WHERE NEV = 'DEFAULT_INTEZESI_IDO_IDOEGYSEG' 
				  END
			END
	END
	ELSE
	BEGIN
		SET @VALUE_IDOEGYSEG = @IntezesiIdoegyseg 
	END
	
	IF (@VALUE_IDOEGYSEG = '10080')
		BEGIN
			SET @TMP = DATEDIFF(WEEK,@CURRENT_DATE,@EndDate)
			SET @RET = 'M�g ' + CAST(@TMP AS NVARCHAR) + ' h�t'
		END
	ELSE IF (@VALUE_IDOEGYSEG = '1440')
		BEGIN
			SET @TMP = DATEDIFF(DAY,@CURRENT_DATE,@EndDate)
			SET @RET = 'M�g ' + CAST(@TMP AS NVARCHAR) + ' nap'
		END
	ELSE IF (@VALUE_IDOEGYSEG = '60')
		BEGIN
			SET @TMP = DATEDIFF(HOUR,@CURRENT_DATE,@EndDate)
			SET @RET = 'M�g ' + CAST(@TMP AS NVARCHAR) + ' �ra'
		END
	ELSE IF (@VALUE_IDOEGYSEG = '1')
		BEGIN
			SET @TMP = DATEDIFF(MINUTE,@CURRENT_DATE,@EndDate)
			SET @RET = 'M�g ' + CAST(@TMP AS NVARCHAR) + ' perc'
		END
	ELSE IF (@VALUE_IDOEGYSEG = '-1440')
		BEGIN
			SET @TMP = dbo.fn_munkanapok_szama(@CURRENT_DATE,@EndDate);
			SET @RET = 'M�g ' + CAST(@TMP AS NVARCHAR) + ' munkanap'
		END
	ELSE
		BEGIN
			SET @TMP = DATEDIFF(MINUTE,@CURRENT_DATE,@EndDate) / CAST (@VALUE_IDOEGYSEG AS INT)
			SET @RET = CAST(@TMP AS NVARCHAR)
		END

	RETURN @RET

END


