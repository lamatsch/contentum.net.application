IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_CheckKuldemeny_IsConfidential]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_CheckKuldemeny_IsConfidential]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_CheckKuldemeny_IsConfidential]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_CheckKuldemeny_IsConfidential]
(	
	@KuldKuldemeny_Id UNIQUEIDENTIFIER
)
RETURNS INT
AS
BEGIN

DECLARE @ret INT
SET @ret = 0

DECLARE @IratMinositesParamName varchar(30);
set @IratMinositesParamName = ''IRAT_MINOSITES_BIZALMAS''

IF @KuldKuldemeny_Id IS NOT NULL
BEGIN
	-- Org meghatározása
	DECLARE @Org uniqueidentifier
	SET @Org = (select EREC_IraIktatoKonyvek.Org from EREC_IraIktatoKonyvek
				inner join EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.IraIktatokonyv_Id=EREC_IraIktatoKonyvek.Id
				where EREC_KuldKuldemenyek.Id=@KuldKuldemeny_Id)

	IF (@Org is not null)
	BEGIN
		DECLARE @confidential varchar(4000)
		SET @confidential = (select top 1 Ertek from KRT_Parameterek where Nev=@IratMinositesParamName
									and Org=@Org and getdate() between ErvKezd and ErvVege)

		IF (@confidential IS NOT NULL)
		BEGIN
			IF exists(select 1 from EREC_KuldKuldemenyek where EREC_KuldKuldemenyek.Id=@KuldKuldemeny_Id
				and EREC_KuldKuldemenyek.Minosites in (select Value collate Hungarian_CS_AS from fn_Split(@confidential, '','')))
			BEGIN
				SET @ret = 1
			END
		END
	END
END
             

RETURN @ret
END

' 
END

GO
