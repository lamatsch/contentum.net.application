IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_EREC_Irat_IktatokonyveiCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_EREC_Irat_IktatokonyveiCheckUK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_EREC_Irat_IktatokonyveiCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_EREC_Irat_IktatokonyveiCheckUK] (
             @IraIktatokonyv_Id uniqueidentifier ,
             @Csoport_Id_Iktathat uniqueidentifier ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_Irat_Iktatokonyvei where
         (IraIktatokonyv_Id = @IraIktatokonyv_Id or (IraIktatokonyv_Id is null and @IraIktatokonyv_Id is null)) and            
         (Csoport_Id_Iktathat = @Csoport_Id_Iktathat or (Csoport_Id_Iktathat is null and @Csoport_Id_Iktathat is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END

' 
END

GO
