IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_EREC_IrattariKikeroCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_EREC_IrattariKikeroCheckUK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_EREC_IrattariKikeroCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_EREC_IrattariKikeroCheckUK] (
             @FelhasznaloCsoport_Id_Kikero uniqueidentifier ,
             @KeresDatuma datetime ,
             @KulsoAzonositok Nvarchar(100) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_IrattariKikero where
         (FelhasznaloCsoport_Id_Kikero = @FelhasznaloCsoport_Id_Kikero or (FelhasznaloCsoport_Id_Kikero is null and @FelhasznaloCsoport_Id_Kikero is null)) and            
         (KeresDatuma = @KeresDatuma or (KeresDatuma is null and @KeresDatuma is null)) and            
         (KulsoAzonositok = @KulsoAzonositok or (KulsoAzonositok is null and @KulsoAzonositok is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END

' 
END

GO
