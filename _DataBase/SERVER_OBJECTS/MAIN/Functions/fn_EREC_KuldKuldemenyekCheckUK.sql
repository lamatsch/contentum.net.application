
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--FF, 20070926
IF  EXISTS (SELECT * FROM sys.Objects WHERE object_id = OBJECT_ID(N'dbo.CK_EREC_KuldKuldemenyek') AND type = N'C')
  ALTER TABLE EREC_KuldKuldemenyek DROP CONSTRAINT CK_EREC_KuldKuldemenyek  
GO
--FF

IF  EXISTS (SELECT * FROM sys.Objects WHERE object_id = OBJECT_ID(N'dbo.fn_EREC_KuldKuldemenyekCheckUK') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
  DROP FUNCTION dbo.fn_EREC_KuldKuldemenyekCheckUK
GO

CREATE FUNCTION dbo.fn_EREC_KuldKuldemenyekCheckUK (
             @IraIktatokonyv_Id uniqueidentifier ,
             @Erkezteto_Szam int ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_KuldKuldemenyek where
         (IraIktatokonyv_Id = @IraIktatokonyv_Id or (IraIktatokonyv_Id is null and @IraIktatokonyv_Id is null)) and            
         (Erkezteto_Szam = @Erkezteto_Szam or (Erkezteto_Szam is null and @Erkezteto_Szam is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END

GO

ALTER TABLE EREC_KuldKuldemenyek  
WITH NOCHECK ADD CONSTRAINT 
CK_EREC_KuldKuldemenyek
CHECK  (((0)=dbo.fn_EREC_KuldKuldemenyekCheckUK(
         IraIktatokonyv_Id ,
         Erkezteto_Szam ,Id,ErvKezd,ErvVege)
))

GO
