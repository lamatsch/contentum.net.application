IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_EREC_Obj_MetaDefinicioCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_EREC_Obj_MetaDefinicioCheckUK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_EREC_Obj_MetaDefinicioCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_EREC_Obj_MetaDefinicioCheckUK] (
             @Objtip_Id uniqueidentifier ,
             @Objtip_Id_Column uniqueidentifier ,
             @ColumnValue Nvarchar(100) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_Obj_MetaDefinicio where
         (Objtip_Id = @Objtip_Id or (Objtip_Id is null and @Objtip_Id is null)) and            
         (Objtip_Id_Column = @Objtip_Id_Column or (Objtip_Id_Column is null and @Objtip_Id_Column is null)) and            
         (ColumnValue = @ColumnValue or (ColumnValue is null and @ColumnValue is null)) and    
         DefinicioTipus <> ''A0'' and
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END

' 
END

GO
