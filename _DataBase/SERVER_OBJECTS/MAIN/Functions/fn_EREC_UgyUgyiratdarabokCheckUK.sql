IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_EREC_UgyUgyiratdarabokCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_EREC_UgyUgyiratdarabokCheckUK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_EREC_UgyUgyiratdarabokCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_EREC_UgyUgyiratdarabokCheckUK] (
             @UgyUgyirat_Id uniqueidentifier ,
             @LetrehozasIdo datetime ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_UgyUgyiratdarabok where
         (UgyUgyirat_Id = @UgyUgyirat_Id or (UgyUgyirat_Id is null and @UgyUgyirat_Id is null)) and            
         (LetrehozasIdo = @LetrehozasIdo or (LetrehozasIdo is null and @LetrehozasIdo is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END

' 
END

GO
