IF  EXISTS (SELECT * 
			FROM sys.objects 
			WHERE object_id = OBJECT_ID(N'[dbo].[fn_EREC_UgyUgyiratok_Sakkora_GetHataridoKovetkezo]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fn_EREC_UgyUgyiratok_Sakkora_GetHataridoKovetkezo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_EREC_UgyUgyiratok_Sakkora_GetHataridoKovetkezo] 
(
	@Ugyirat_Id uniqueidentifier
)

RETURNS DATETIME
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return DATETIME;
		
		DECLARE @HATARIDO   DATETIME
		DECLARE @MEGVAN_KOV_DATUM BIT 
		DECLARE @LIMIT BIT 
		DECLARE @KOV_DATUM DATETIME

		SELECT @HATARIDO = U.hatarido
	    FROM dbo.EREC_UgyUgyiratok U
		WHERE U.ID = @Ugyirat_Id

	    SET @KOV_DATUM = @HATARIDO
	    SET @MEGVAN_KOV_DATUM = 0
	    SET @LIMIT = 0

		WHILE(@MEGVAN_KOV_DATUM = 0 OR @LIMIT > 365)
		BEGIN

			SET @KOV_DATUM = DATEADD(DAY,1,@KOV_DATUM)

			IF ([dbo].[fn_munkanap] (@KOV_DATUM) = 1)
			BEGIN
				SET @MEGVAN_KOV_DATUM = 1
				BREAK
			END
			SET @LIMIT = @LIMIT + 1
		 
		END

   SET @Return = @KOV_DATUM;
   
   RETURN @Return;

END

GO
