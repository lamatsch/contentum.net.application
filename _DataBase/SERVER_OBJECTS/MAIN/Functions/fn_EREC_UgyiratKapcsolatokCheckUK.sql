IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_EREC_UgyiratKapcsolatokCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_EREC_UgyiratKapcsolatokCheckUK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_EREC_UgyiratKapcsolatokCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_EREC_UgyiratKapcsolatokCheckUK] (
             @Ugyirat_Ugyirat_Beepul uniqueidentifier ,
             @Ugyirat_Ugyirat_Felepul uniqueidentifier ,
             @KapcsolatTipus nvarchar(64) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_UgyiratKapcsolatok where
         (Ugyirat_Ugyirat_Beepul = @Ugyirat_Ugyirat_Beepul or (Ugyirat_Ugyirat_Beepul is null and @Ugyirat_Ugyirat_Beepul is null)) and            
         (Ugyirat_Ugyirat_Felepul = @Ugyirat_Ugyirat_Felepul or (Ugyirat_Ugyirat_Felepul is null and @Ugyirat_Ugyirat_Felepul is null)) and            
         (KapcsolatTipus = @KapcsolatTipus or (KapcsolatTipus is null and @KapcsolatTipus is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END

' 
END

GO
