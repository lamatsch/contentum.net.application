IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_Ervenyes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_Ervenyes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_Ervenyes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_Ervenyes]
		(
		@ErvKezd  datetime,
		@ErvVege  datetime
		)
RETURNS char(1) 
AS
BEGIN
  RETURN dbo.fn_Ervenyesseg(''ERV'',@ErvKezd,@ErvVege,NULL,NULL)
END
-- select dbo.fn_Ervenyes(getdate()-1,NULL) ERV
-- select top 1 * from KRT_Csoportok where dbo.fn_Ervenyes(ErvKezd,ErvVege)= ''I''

' 
END

GO
