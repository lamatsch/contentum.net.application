IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllConfidentialKuldemeny_ExecutorIsOrzoOrHisLeader]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetAllConfidentialKuldemeny_ExecutorIsOrzoOrHisLeader]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllConfidentialKuldemeny_ExecutorIsOrzoOrHisLeader]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetAllConfidentialKuldemeny_ExecutorIsOrzoOrHisLeader]
(	
	@ExecutorUserId UNIQUEIDENTIFIER,
	@FelhasznaloSzervezet_Id UNIQUEIDENTIFIER
)
RETURNS @KuldKuldemeny_Ids table
(
	Id uniqueidentifier
)
AS
BEGIN

	DECLARE @IratMinositesParamName varchar(30);
	set @IratMinositesParamName = ''IRAT_MINOSITES_BIZALMAS''

	IF @ExecutorUserId IS NOT NULL and @FelhasznaloSzervezet_Id IS NOT NULL
	BEGIN
		-- Org meghatározása
		DECLARE @Org uniqueidentifier
		SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)

		IF (@Org is not null)
		BEGIN
			DECLARE @confidential varchar(4000)
			set @confidential = (select top 1 Ertek from KRT_Parameterek where Nev=@IratMinositesParamName
									and Org=@Org and getdate() between ErvKezd and ErvVege)

			IF (@confidential IS NOT NULL)
			BEGIN
				declare @Bizalmas table (val nvarchar(64) collate Hungarian_CS_AS )
				insert into @Bizalmas (val) (select Value from fn_Split(@confidential, '',''))

				DECLARE @csoporttagsag_tipus nvarchar(64)
				SET @csoporttagsag_tipus = (select top 1 KRT_CsoportTagok.Tipus from KRT_CsoportTagok
									where KRT_CsoportTagok.Csoport_Id_Jogalany=@ExecutorUserId
									and KRT_CsoportTagok.Csoport_Id=@FelhasznaloSzervezet_Id
									and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
									)
				IF @csoporttagsag_tipus is not null
				BEGIN
					insert into @KuldKuldemeny_Ids
						select EREC_KuldKuldemenyek.Id
							from EREC_KuldKuldemenyek
							where EREC_KuldKuldemenyek.Minosites in (select val from @Bizalmas)
							and EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo = @ExecutorUserId						
						UNION -- végrehajtó felhasználó az orzo vezetoje-e
						select EREC_KuldKuldemenyek.Id
							from EREC_KuldKuldemenyek
							WHERE EREC_KuldKuldemenyek.Minosites in (select val from @Bizalmas)
							and @csoporttagsag_tipus=''3''
							and EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo IN
							(
								select KRT_Csoportok.Id
									from KRT_Csoportok
										inner join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id = @FelhasznaloSzervezet_Id 
											and KRT_Csoportok.Tipus = ''1'' and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
									where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege 
										and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
							)
						UNION -- van-e objektumon keresztül joga a küldeményhez
						select EREC_KuldKuldemenyek.Id
							from EREC_KuldKuldemenyek
								inner join @Bizalmas as Bizalmas on EREC_KuldKuldemenyek.Minosites = Bizalmas.val
							where EREC_KuldKuldemenyek.Minosites in (select val from @Bizalmas)
							and EXISTS
								(
									select 1
										from KRT_Jogosultak where
											KRT_Jogosultak.Obj_Id=EREC_KuldKuldemenyek.Id
											and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
											and @ExecutorUserId = KRT_Jogosultak.Csoport_Id_Jogalany
								)
				END					
			END
		END
	END

	RETURN;
END

' 
END

GO
