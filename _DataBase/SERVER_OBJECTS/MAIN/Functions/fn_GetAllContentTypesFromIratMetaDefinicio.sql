IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllContentTypesFromIratMetaDefinicio]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetAllContentTypesFromIratMetaDefinicio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllContentTypesFromIratMetaDefinicio]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_GetAllContentTypesFromIratMetaDefinicio] (@Org uniqueidentifier, @ContentType nvarchar(100))
returns @result table ( ContentType nvarchar(100)
                      , Szint int
                      )
/*
feladata, hogy a ContentType alapján megkeresve az objektum metadefiníciót,
majd abból az irat metadefiníciót, meghatározza a szülo iratmetadefiníciókat,
követve az irat metadefiníció hierarchiát, és az azoknak megfelelo C2 típusú
objektum metadefiníciókat (ha vannak),
amiket az eredeti ContentType felettesének tekintünk.
returns: a megfelelo ContentType-okat tartalmazó tábla, a Szint oszlop tartalmazza a relatív
         pozíciót az irat metadefiníció hierarchiában,
         az 1 szinten található az eredeti ContentType közvetlen szüloje stb. 

-- hívás példa:
select * from dbo.fn_GetAllContentTypesFromIratMetaDefinicio(''450B510A-7CAA-46B0-83E3-18445C0C53A9'',''U31-075-01-AuditDokumentum'') --> ''U31-075-MinosegBiztositas'',1
go
select * from dbo.fn_GetAllContentTypesFromIratMetaDefinicio(''450B510A-7CAA-46B0-83E3-18445C0C53A9'',''U31-075-MinosegBiztositas'') --> <üres tábla>
go
*/
AS
BEGIN

    if @ContentType is not null and @Org is not null
    begin
        declare @IratMetadefinicio_Id uniqueidentifier
        select @IratMetadefinicio_Id = dbo.fn_GetIratMetaDefinicioByContentType(@Org, @ContentType)
        if @IratMetaDefinicio_Id is not null
        begin
			-- hierarchia lekérése, összefuzése az EREC_Obj_MetaDefinicio táblával ColumnValue alapján
            insert into @result
                select omd.ContentType as ContentType, IMD_Hierarchia.Szint as Szint
                from dbo.fn_GetIratMetaDefinicioHierarchia(@IratMetadefinicio_Id) as IMD_Hierarchia
                join EREC_Obj_MetaDefinicio omd
                on convert(nvarchar(36), IMD_Hierarchia.Id) = omd.ColumnValue
                and omd.DefinicioTipus = ''C2''
				and omd.Org=@Org
                and getdate() between omd.ErvKezd and omd.ErvVege
                and IMD_Hierarchia.Szint > 0; -- saját magát nem adjuk vissza
        end
    end
    return;

END

' 
END

GO
