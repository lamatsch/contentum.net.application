IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllIktathatoEREC_IraIktatokonyvekByCsoportIdAndIktatoErkezteto]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetAllIktathatoEREC_IraIktatokonyvekByCsoportIdAndIktatoErkezteto]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllIktathatoEREC_IraIktatokonyvekByCsoportIdAndIktatoErkezteto]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetAllIktathatoEREC_IraIktatokonyvekByCsoportIdAndIktatoErkezteto]
	(	@CsoportId		UNIQUEIDENTIFIER,
		@SzervezetId	UNIQUEIDENTIFIER,
		@IktatoErkezteto CHAR(1)
	)
RETURNS TABLE
AS
RETURN
(
	WITH temp AS
	(
		SELECT
				@SzervezetId AS CsoportId,
				CASE 
					WHEN NOT EXISTS
					(
						SELECT 1
							FROM EREC_Irat_Iktatokonyvei
								INNER JOIN EREC_IraIktatoKonyvek ON EREC_IraIktatoKonyvek.Id = EREC_Irat_Iktatokonyvei.IraIktatokonyv_Id
							WHERE EREC_Irat_Iktatokonyvei.Csoport_Id_Iktathat = @SzervezetId
								AND GETDATE() BETWEEN EREC_Irat_Iktatokonyvei.ErvKezd AND EREC_Irat_Iktatokonyvei.ErvVege
								AND GETDATE() < ISNULL(EREC_IraIktatoKonyvek.LezarasDatuma, GETDATE() + 1)
								AND EREC_IraIktatoKonyvek.IktatoErkezteto = @IktatoErkezteto
					) THEN 0
					ELSE 1
				END AS Hit
				
		UNION ALL
				
		SELECT
				KRT_CsoportTagok.Csoport_Id AS CsoportId,
				CASE 
					WHEN NOT EXISTS
					(
						SELECT 1
							FROM EREC_Irat_Iktatokonyvei
								INNER JOIN EREC_IraIktatoKonyvek ON EREC_IraIktatoKonyvek.Id = EREC_Irat_Iktatokonyvei.IraIktatokonyv_Id
							WHERE EREC_Irat_Iktatokonyvei.Csoport_Id_Iktathat = KRT_CsoportTagok.Csoport_Id
								AND GETDATE() BETWEEN EREC_Irat_Iktatokonyvei.ErvKezd AND EREC_Irat_Iktatokonyvei.ErvVege
								AND GETDATE() < ISNULL(EREC_IraIktatoKonyvek.LezarasDatuma, GETDATE() + 1)
								AND EREC_IraIktatoKonyvek.IktatoErkezteto = @IktatoErkezteto								
					) AND temp.Hit = 0 THEN 0
					ELSE 1
				END AS Hit
			FROM KRT_CsoportTagok
				INNER JOIN temp ON temp.CsoportId = KRT_CsoportTagok.Csoport_Id_Jogalany
			WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
				AND KRT_CsoportTagok.Tipus = ''1''
				AND temp.Hit = 0
	)
	select distinct EREC_IraIktatokonyvek.*
		FROM EREC_IraIktatoKonyvek
			INNER JOIN EREC_Irat_Iktatokonyvei ON EREC_Irat_Iktatokonyvei.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id
		WHERE EREC_Irat_Iktatokonyvei.Csoport_Id_Iktathat IN
		(
			SELECT CsoportId
				FROM temp
			UNION ALL
			SELECT @CsoportId
		)
		AND GETDATE() BETWEEN EREC_Irat_Iktatokonyvei.ErvKezd AND EREC_Irat_Iktatokonyvei.ErvVege
		AND EREC_IraIktatoKonyvek.IktatoErkezteto = @IktatoErkezteto
)

' 
END

GO
