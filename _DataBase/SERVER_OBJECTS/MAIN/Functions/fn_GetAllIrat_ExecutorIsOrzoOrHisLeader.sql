IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllIrat_ExecutorIsOrzoOrHisLeader]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetAllIrat_ExecutorIsOrzoOrHisLeader]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllIrat_ExecutorIsOrzoOrHisLeader]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetAllIrat_ExecutorIsOrzoOrHisLeader]
(	
	@ExecutorUserId UNIQUEIDENTIFIER,
	@FelhasznaloSzervezet_Id UNIQUEIDENTIFIER
)
RETURNS @IraIrat_Ids table
(
	Id uniqueidentifier
)
AS
BEGIN

-- az irat orzojét itt a példányainak az orzoi határozzák meg,
-- hiszen nyilván láthatja, akinél van példánya és annak vezetoje is

	IF @ExecutorUserId IS NOT NULL and @FelhasznaloSzervezet_Id IS NOT NULL
	BEGIN
		DECLARE @csoporttagsag_tipus nvarchar(64)
		SET @csoporttagsag_tipus = (select top 1 KRT_CsoportTagok.Tipus from KRT_CsoportTagok
							where KRT_CsoportTagok.Csoport_Id_Jogalany=@ExecutorUserId
							and KRT_CsoportTagok.Csoport_Id=@FelhasznaloSzervezet_Id
							and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
							)
		IF @csoporttagsag_tipus is not null
		BEGIN
			insert into @IraIrat_Ids select EREC_IraIratok.Id from EREC_IraIratok
			where exists(select 1 from EREC_PldIratPeldanyok
						where EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id
						and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo = @ExecutorUserId)
			or -- végrehajtó felhasználó az orzo vezetoje-e
			(@csoporttagsag_tipus=''3'' and exists(select 1 from EREC_PldIratPeldanyok where EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id
			and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo in (select KRT_Csoportok.Id from KRT_Csoportok
				inner join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id = @FelhasznaloSzervezet_Id 
				and KRT_Csoportok.Tipus = ''1'' and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
				where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege 
				and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege)))

		END					
	END
	RETURN;
END

' 
END

GO
