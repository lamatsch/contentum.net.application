IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllKiegeszitoObjMetaDefinicioByContentType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetAllKiegeszitoObjMetaDefinicioByContentType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllKiegeszitoObjMetaDefinicioByContentType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_GetAllKiegeszitoObjMetaDefinicioByContentType] (@Org uniqueidentifier, @ContentType nvarchar(100))
returns @result table ( Id uniqueidentifier
                      , ContentType nvarchar(100)
                      , ObjTip_Kod nvarchar(100)
                      , Szint int
                      ) 

/*
feladata, hogy az objektum metadefiníciótól kiindulva meghatározza az
ügyiratnyilvántartás objektumaihoz (irat, ügyiratdarab, ügyirat) kapcsolódó
B1 típusú objektum metadefiníciókat, melyek nincsenek alacsonyabb szinten, mint az adott ContentType
(tehát ügyiratdarab/eljárási szakasz szintu ContentType esetén az iratra vonatkozók nem jönnek vissza)
returns: a megfelelo objektum metadefiníciók azonosítóit, ContentType-jukat,
         az objektum (tábla) nevét
         és relatív hierarchiaszintjüket tartalmazó tábla,
         a 0 szinten az eredeti objektum metadefiníció található (ha létezik megfeleloje az irat metadefiníciós táblában)

-- hívás példa:
select * from dbo.fn_GetAllKiegeszitoObjMetaDefinicioByContentType(''450B510A-7CAA-46B0-83E3-18445C0C53A9'',''C272_01_00_Tajekoztato'')
       --> ''FA242B51-7819-4003-A279-7CEFC1D5892A	Irat_Kiegeszites	EREC_IraIratok	0
       --> ''99FC16DF-3C23-4445-AD62-6E802D2AAC9A	Ugyirat_Kiegeszites	EREC_UgyUgyiratok	2
go
select * from dbo.fn_GetAllKiegeszitoObjMetaDefinicioByContentType(''450B510A-7CAA-46B0-83E3-18445C0C53A9'',''C272_01_Dokumentum'')
       --> ''99FC16DF-3C23-4445-AD62-6E802D2AAC9A''	Ugyirat_Kiegeszites	EREC_UgyUgyiratok	0
go
select * from dbo.fn_GetAllKiegeszitoObjMetaDefinicioByContentType(''450B510A-7CAA-46B0-83E3-18445C0C53A9'',''CTT_AX_213'')
       --> <üres tábla>
go
*/
AS
BEGIN

    if @ContentType is not null and @Org is not null
    begin
        -- ellenorizzük, hogy létezik-e a ContentType-nak megfelelo irat metadefiníció
        declare @IratMetadefinicio_Id uniqueidentifier
        select @IratMetadefinicio_Id = dbo.fn_GetIratMetaDefinicioByContentType(@Org, @ContentType)

        if @IratMetaDefinicio_Id is not null
        begin
            -- a ContentType-nak megfelelelo objektum metadefinícióból kinyerheto az objektum típusa
            declare @BottomLevelObjtip_Id uniqueidentifier
            declare @BottomLevelObjtip_Kod nvarchar(100)
            
            set @BottomLevelObjtip_Id = (select Objtip_Id
                                         from EREC_Obj_MetaDefinicio
                                         where ContentType = @ContentType
                                         and Org=@Org
                                         and getdate() between ErvKezd and ErvVege)
            if @BottomLevelObjtip_Id is not null
            begin
                set @BottomLevelObjtip_Kod = (select objtip.Kod from KRT_ObjTipusok objtip
                                              where objtip.Id = @BottomLevelObjtip_Id
                                              and getdate() between objtip.Ervkezd and objtip.ErvVege
                                              )

                if @BottomLevelObjtip_Kod is not null
                    and (@BottomLevelObjtip_Kod = ''EREC_UgyUgyiratok''
                    or @BottomLevelObjtip_Kod = ''EREC_UgyUgyiratdarabok''
                    or @BottomLevelObjtip_Kod = ''EREC_IraIratok'')
                begin
                    declare @Objtip_Id_Iratok uniqueidentifier
                    declare @Objtip_Id_Ugyiratdarabok uniqueidentifier
                    declare @Objtip_Id_Ugyiratok uniqueidentifier
                    -- objektum típusok azonosítóinak lekérése
                    set @Objtip_Id_Iratok = (select objtip.Id from KRT_ObjTipusok objtip
                                             where objtip.Kod = ''EREC_IraIratok''
                                             and objtip.ObjTipus_Id_Tipus = (select Id from KRT_ObjTipusok objtip2
                                                                             where objtip2.Kod = ''DbTable''
                                                                             and getdate() between objtip2.ErvKezd and objtip2.ErvVege)
                                             and getdate() between objtip.ErvKezd and objtip.ErvVege)

                    set @Objtip_Id_Ugyiratdarabok = (select objtip.Id from KRT_ObjTipusok objtip
                                             where objtip.Kod = ''EREC_UgyUgyiratdarabok''
                                             and objtip.ObjTipus_Id_Tipus = (select Id from KRT_ObjTipusok objtip2
                                                                             where objtip2.Kod = ''DbTable''
                                                                             and getdate() between objtip2.ErvKezd and objtip2.ErvVege)
                                             and getdate() between objtip.ErvKezd and objtip.ErvVege)

                    set @Objtip_Id_Ugyiratok = (select objtip.Id from KRT_ObjTipusok objtip
                                             where objtip.Kod = ''EREC_UgyUgyiratok''
                                             and objtip.ObjTipus_Id_Tipus = (select Id from KRT_ObjTipusok objtip2
                                                                             where objtip2.Kod = ''DbTable''
                                                                             and getdate() between objtip2.ErvKezd and objtip2.ErvVege)
                                             and getdate() between objtip.ErvKezd and objtip.ErvVege)

                    -- felhasználjuk a már lekért adatokat
                    insert into @result
                        select Id
                             , ContentType
                             , @BottomLevelObjtip_Kod as Objtip_Kod
                             , 0 as Szint
                             from EREC_Obj_MetaDefinicio
                                       where DefinicioTipus = ''B1''
                                       and Objtip_Id = @BottomLevelObjtip_Id
                                       and Org=@Org
                                       and getdate() between ErvKezd and ErvVege
                    -- magasabb szintek felvétele
                    if @BottomLevelObjtip_Kod = ''EREC_UgyUgyiratdarabok'' or @BottomLevelObjtip_Kod = ''EREC_IraIratok''
                    begin
                        declare @Szint int
                        if @BottomLevelObjtip_Kod = ''EREC_UgyUgyiratdarabok''
                           set @Szint = 1
                        else
                           set @Szint = 2

                        insert into @result
                            select Id
                                 , ContentType
                                 , ''EREC_UgyUgyiratok'' as Objtip_Kod
                                 , @Szint as Szint
                            from EREC_Obj_MetaDefinicio
                            where DefinicioTipus = ''B1''
                            and Objtip_Id = @Objtip_Id_Ugyiratok
                            and Org=@Org
                            and getdate() between ErvKezd and ErvVege
                    end
                
                    if @BottomLevelObjtip_Kod = ''EREC_IraIratok''
                    begin
                        insert into @result
                            select Id
                                 , ContentType
                                 , ''EREC_UgyUgyiratdarabok'' as Objtip_Kod
                                 , 1 as Szint
                            from EREC_Obj_MetaDefinicio
                            where DefinicioTipus = ''B1''
                            and Objtip_Id = @Objtip_Id_Ugyiratdarabok
                            and Org=@Org
                            and getdate() between ErvKezd and ErvVege
                    end
                end
            end
        end
    end
    return;

END

' 
END

GO
