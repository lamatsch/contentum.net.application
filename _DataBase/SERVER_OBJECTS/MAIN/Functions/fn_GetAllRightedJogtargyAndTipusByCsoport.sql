IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllRightedJogtargyAndTipusByCsoport]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetAllRightedJogtargyAndTipusByCsoport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllRightedJogtargyAndTipusByCsoport]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetAllRightedJogtargyAndTipusByCsoport]
	(	@CsoportId		uniqueidentifier,
		@JogTargyId		UNIQUEIDENTIFIER = null
	)
RETURNS @Jogtargyak TABLE
(	
	Id	uniqueidentifier,
	Jogszint	char(1),
	Tipus		char(1)
)
AS
BEGIN
	declare @CurCsoport_Id	uniqueidentifier
	declare @CurTipus		nvarchar(64)
	declare CsoportCursor cursor for
		select Csoport_Id,Tipus from KRT_CsoportTagok where Csoport_Id_Jogalany = @CsoportId AND Tipus is not null AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
	
	open CsoportCursor
	fetch next from CsoportCursor into @CurCsoport_Id,@CurTipus
	
	if @CurCsoport_Id is null
	BEGIN
		insert into @Jogtargyak(Id,Jogszint,Tipus)
			select distinct KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos as Id, KRT_OBJEKTUM_ACLEK.OrokolhetoJogszint as Jogszint, KRT_Jogosultak.Tipus
			from KRT_Jogosultak KRT_Jogosultak
				left join KRT_OBJEKTUM_ACLEK KRT_OBJEKTUM_ACLEK on KRT_OBJEKTUM_ACLEK.Obj_Id = KRT_Jogosultak.Obj_Id
			where KRT_Jogosultak.Csoport_Id_Jogalany = @CsoportId
				AND KRT_Jogosultak.Obj_Id = Isnull(@JogTargyId,KRT_Jogosultak.Obj_Id)
	END

	if exists (select 1 from KRT_CsoportTagok 
					inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id and KRT_Csoportok.Tipus = ''R''
					where KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId 
							AND getdate() between KRT_CsoportTagok.ErvKezd 
							AND KRT_CsoportTagok.ErvVege
				)
	BEGIN
		insert into @Jogtargyak(Id,Jogszint,Tipus)
			select Id,''I'',''R'' from KRT_ACL
			WHERE KRT_ACL.Id = Isnull(@JogTargyId,KRT_ACL.Id)
		return;
	END

	WHILE @@FETCH_STATUS = 0
	BEGIN

		if @CurTipus = ''3''
		insert into @Jogtargyak(Id,Jogszint,Tipus)
			select distinct KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos as Id, KRT_OBJEKTUM_ACLEK.OrokolhetoJogszint as Jogszint, KRT_Jogosultak.Tipus
			from KRT_CsoportTagokAll KRT_CsoportTagokAll
				inner join KRT_Jogosultak KRT_Jogosultak on KRT_Jogosultak.Csoport_Id_Jogalany = KRT_CsoportTagokAll.Csoport_Id_Jogalany
				left join KRT_OBJEKTUM_ACLEK KRT_OBJEKTUM_ACLEK on KRT_OBJEKTUM_ACLEK.Obj_Id = KRT_Jogosultak.Obj_Id
			where KRT_CsoportTagokAll.Csoport_Id = @CurCsoport_Id
				--and getdate() between KRT_CsoportTagokAll.ErvKezd and KRT_CsoportTagokAll.ErvVege
				and KRT_OBJEKTUM_ACLEK.Obj_Id not in (select Id from @Jogtargyak)
				AND KRT_Jogosultak.Obj_Id = Isnull(@JogTargyId,KRT_Jogosultak.Obj_Id)
		else --dolgozó
		BEGIN
		if not exists (select 1 from KRT_Csoportok where Id = @CurCsoport_Id and JogosultsagOroklesMod = ''0'')
			insert into @Jogtargyak(Id,Jogszint,Tipus)
				select distinct KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos as Id, KRT_OBJEKTUM_ACLEK.OrokolhetoJogszint as Jogszint, KRT_Jogosultak.Tipus
				from KRT_CsoportTagok KRT_CsoportTagokAll
					inner join KRT_Jogosultak KRT_Jogosultak on (KRT_Jogosultak.Csoport_Id_Jogalany = KRT_CsoportTagokAll.Csoport_Id_Jogalany or KRT_Jogosultak.Csoport_Id_Jogalany = @CurCsoport_Id) and (KRT_Jogosultak.Tipus != ''T'' or KRT_Jogosultak.Csoport_Id_Jogalany = @CsoportId)
					left join KRT_OBJEKTUM_ACLEK KRT_OBJEKTUM_ACLEK on KRT_OBJEKTUM_ACLEK.Obj_Id = KRT_Jogosultak.Obj_Id
				where KRT_CsoportTagokAll.Csoport_Id = @CurCsoport_Id and KRT_CsoportTagokAll.Tipus = ''2'' 
					--and getdate() between KRT_CsoportTagokAll.ErvKezd and KRT_CsoportTagokAll.ErvVege 
					and KRT_OBJEKTUM_ACLEK.Obj_Id not in (select Id from @Jogtargyak)
					AND KRT_Jogosultak.Obj_Id = Isnull(@JogTargyId,KRT_Jogosultak.Obj_Id)
		else 
			insert into @Jogtargyak(Id,Jogszint,Tipus)
				select distinct KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos as Id, KRT_OBJEKTUM_ACLEK.OrokolhetoJogszint as Jogszint, KRT_Jogosultak.Tipus
				from KRT_Jogosultak
					left join KRT_OBJEKTUM_ACLEK KRT_OBJEKTUM_ACLEK on KRT_OBJEKTUM_ACLEK.Obj_Id = KRT_Jogosultak.Obj_Id
				where (KRT_Jogosultak.Csoport_Id_Jogalany = @CurCsoport_Id or KRT_Jogosultak.Csoport_Id_Jogalany = @CsoportId) 
					and KRT_OBJEKTUM_ACLEK.Obj_Id not in (select Id from @Jogtargyak)
					AND KRT_Jogosultak.Obj_Id = Isnull(@JogTargyId,KRT_Jogosultak.Obj_Id)
		END

		fetch next from CsoportCursor into @CurCsoport_Id,@CurTipus
	END
	close CsoportCursor
	deallocate CsoportCursor

	return;
END

' 
END

GO
