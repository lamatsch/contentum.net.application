IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllSubCsoportByCsoportId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetAllSubCsoportByCsoportId]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllSubCsoportByCsoportId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetAllSubCsoportByCsoportId]
	(	@CsoportId		uniqueidentifier
	)
RETURNS TABLE
AS
RETURN
(
	WITH temp AS
	(
		SELECT KRT_CsoportTagok.Csoport_Id AS Id
			FROM KRT_CsoportTagok
			WHERE KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId
				AND Tipus IS NOT NULL
				AND GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
				
		UNION ALL
		
		SELECT KRT_CsoportTagok.Csoport_Id_Jogalany
			FROM KRT_CsoportTagok
				INNER JOIN temp ON temp.Id = KRT_CsoportTagok.Csoport_Id
			WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
				AND KRT_CsoportTagok.Tipus IS NOT NULL
	)
	SELECT DISTINCT KRT_Csoportok.*
		FROM KRT_Csoportok
			INNER JOIN temp ON temp.Id = KRT_Csoportok.Id
)

' 
END

GO
