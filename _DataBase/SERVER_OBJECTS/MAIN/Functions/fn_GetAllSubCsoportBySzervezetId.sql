IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllSubCsoportBySzervezetId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetAllSubCsoportBySzervezetId]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllSubCsoportBySzervezetId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetAllSubCsoportBySzervezetId]
	(	@CsoportId		uniqueidentifier
	)
RETURNS TABLE
AS
RETURN
(
WITH temp AS
(
	SELECT Id from KRT_Csoportok
	where Id = @CsoportId
	and Tipus = ''0''
	and getdate() between ErvKezd and ErvVege
				
	UNION ALL
		
	SELECT KRT_CsoportTagok.Csoport_Id_Jogalany
		FROM KRT_CsoportTagok
			INNER JOIN temp ON temp.Id = KRT_CsoportTagok.Csoport_Id
		WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
			AND KRT_CsoportTagok.Tipus IS NOT NULL
)
select KRT_Csoportok.* from KRT_Csoportok
where KRT_Csoportok.Id in (select Id from temp)
and getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege
)

' 
END

GO
