IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllUgyirat_InDosszie]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetAllUgyirat_InDosszie]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllUgyirat_InDosszie]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetAllUgyirat_InDosszie]
	(	@Mappa_Id		UNIQUEIDENTIFIER = NULL,
		@Ugyirat_Ids	NVARCHAR(MAX)
	)
RETURNS @Ugyiratok TABLE
(	
	Id	UNIQUEIDENTIFIER
)
AS
BEGIN
	declare @ugyiratTable table(id uniqueidentifier);
	declare @it	int;
	declare @curId	nvarchar(36);

	set @it = 0;
	while (@it < ((len(@Ugyirat_Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Ugyirat_Ids,@it*39+2,37);
		insert into @ugyiratTable(id) values(@curId);
		set @it = @it + 1;
	END	
	
	IF (@Mappa_Id IS NOT NULL)
	BEGIN
		INSERT INTO @Ugyiratok
			SELECT KRT_MappaTartalmak.Obj_Id
				FROM KRT_MappaTartalmak
				WHERE KRT_MappaTartalmak.Obj_Id IN (SELECT Id FROM @ugyiratTable)
					AND KRT_MappaTartalmak.Mappa_Id = @Mappa_Id
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
	END
	ELSE
	BEGIN
		INSERT INTO @Ugyiratok
			SELECT KRT_MappaTartalmak.Obj_Id
				FROM KRT_MappaTartalmak
					INNER JOIN KRT_Mappak ON KRT_MappaTartalmak.Mappa_Id = KRT_Mappak.Id
				WHERE KRT_MappaTartalmak.Obj_Id IN (SELECT Id FROM @ugyiratTable)
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
					AND KRT_Mappak.Tipus = ''01''
	END
	RETURN;

END

' 
END

GO
