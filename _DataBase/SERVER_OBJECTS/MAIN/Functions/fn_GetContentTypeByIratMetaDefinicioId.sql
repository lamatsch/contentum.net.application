IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetContentTypeByIratMetaDefinicioId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetContentTypeByIratMetaDefinicioId]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetContentTypeByIratMetaDefinicioId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_GetContentTypeByIratMetaDefinicioId](@IratMetadefinicio_Id uniqueidentifier)
returns nvarchar(100)
/*
feladata, hogy az azonosítóval megadott irat metadefiníció Id-hoz
megkeresse a C2 típusú EREC_Obj_MetaDefinicio-t (ha van ilyen)
és meghatározza annak ContentType-ját
returns null, ha ilyen objektum metadefiníció nem taláható,
        vagy az objektum metadefiníció ContentType

-- hívás példa:
select dbo.fn_GetContentTypeByIratMetaDefinicioId(''5E71977D-E15F-41FF-ACC0-62C7B5B3C075'') --> U31_075_01_AuditDokumentum''
go
select dbo.fn_GetContentTypeByIratMetaDefinicioId(''2119E8E0-A3E6-483C-924C-27576F4730F9'') --> null
go
*/
AS
BEGIN
    declare @result nvarchar(100)
    set @result = null
    if @IratMetadefinicio_Id is not null
    begin
        declare @Record_Id uniqueidentifier -- csak ellenorzéshez
        select top 1 @Record_Id = omd.Id
             , @result = omd.ContentType
        from EREC_Obj_MetaDefinicio omd
        where omd.ColumnValue = convert(nvarchar(36), @IratMetadefinicio_Id)
              and omd.DefinicioTipus = ''C2''
			  and omd.Org=(select Org from EREC_IratMetaDefinicio where Id=@IratMetadefinicio_Id)
              and getdate() between omd.ErvKezd and omd.ErvVege
    end
    return @result

END

' 
END

GO
