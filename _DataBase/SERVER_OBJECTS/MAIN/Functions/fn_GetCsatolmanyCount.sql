IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetCsatolmanyCount]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetCsatolmanyCount]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetCsatolmanyCount]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetCsatolmanyCount]
(	
	@UgyiratId UNIQUEIDENTIFIER
	
)
RETURNS INT
AS
BEGIN

DECLARE @ret INT

-- CR#1796:
-- 1. csak a csatolmányokat számoljuk, a mellékleteket nem
-- 2. az iratba bekerült az ügyirat id-ja, ezért szükségtelenná váltak a join muveletek

--SET @ret =  (
--              (SELECT COUNT(*) FROM EREC_UgyUgyiratdarabok INNER JOIN EREC_IraIratok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id AND EREC_UgyUgyiratdarabok.UgyUgyirat_Id =  @UgyiratId 
--               INNER JOIN EREC_Csatolmanyok ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id  WHERE getdate() BETWEEN EREC_Csatolmanyok.ErvKezd AND EREC_Csatolmanyok.ErvVege) +
--		      (SELECT COUNT(*) FROM EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabokMellekletek INNER JOIN EREC_IraIratok as EREC_IraIratokMellekletek  ON EREC_UgyUgyiratdarabokMellekletek.Id = EREC_IraIratokMellekletek.UgyUgyIratDarab_Id AND EREC_UgyUgyiratdarabokMellekletek.UgyUgyirat_Id =  @UgyiratId 
--		       INNER JOIN EREC_Mellekletek
--				ON EREC_Mellekletek.IraIrat_Id = EREC_IraIratokMellekletek.Id 
--				WHERE getdate() BETWEEN EREC_Mellekletek.ErvKezd AND EREC_Mellekletek.ErvVege)
--	        )
--
SET @ret =  (SELECT COUNT(*) FROM EREC_IraIratok
               INNER JOIN EREC_Csatolmanyok
				ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
				WHERE getdate() BETWEEN EREC_Csatolmanyok.ErvKezd AND EREC_Csatolmanyok.ErvVege
				AND EREC_IraIratok.Ugyirat_Id =  @UgyiratId
	        )  
              

RETURN @ret
END

' 
END

GO
