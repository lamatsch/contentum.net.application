IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetCsoportKod]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetCsoportKod]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetCsoportKod]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_GetCsoportKod] (
	@CsoportId UNIQUEIDENTIFIER
)
/*
	Feladata, hogy visszaadja az id-val megadott csoport kódját.
 */
returns nvarchar(100)

as
begin

DECLARE @kod nvarchar(100)

select @kod = KRT_Csoportok.Kod
	FROM KRT_Csoportok
	WHERE KRT_Csoportok.Id = @CsoportId
      
return @kod

END

--select [dbo].[fn_GetCsoportNev](''d093b485-d55f-4a18-9239-018871887059'')

' 
END

GO
