IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetCsoportNev]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetCsoportNev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetCsoportNev]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_GetCsoportNev] (
	@CsoportId UNIQUEIDENTIFIER
)
/*
	Feladata, hogy visszaadja az id-val megadott csoport nevét.
 */
returns nvarchar(400)

as
begin

DECLARE @nev nvarchar(400)

select @nev = KRT_Csoportok.Nev
	FROM KRT_Csoportok
	WHERE KRT_Csoportok.Id = @CsoportId
      
return @nev

END

--select [dbo].[fn_GetCsoportNev](''d093b485-d55f-4a18-9239-018871887059'')

' 
END

GO
