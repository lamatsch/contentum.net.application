IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetDosszieStatus]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetDosszieStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetDosszieStatus]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetDosszieStatus]
	(	@Id		UNIQUEIDENTIFIER
	)
RETURNS @Status TABLE
(	
	Allapot		INT,
	AtadoUser	UNIQUEIDENTIFIER,
	CimzettUser	UNIQUEIDENTIFIER,
	Tipus		NVARCHAR(64)
)
AS
BEGIN
	/*
	Állapotok:	0 -> Normál
				1 -> Továbbítás alatt
	
	*/

	DECLARE @Allapot INT;
	
	IF EXISTS 
	(
		SELECT 1
			FROM KRT_MappaTartalmak
				INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = KRT_MappaTartalmak.Obj_Id
			WHERE KRT_MappaTartalmak.Mappa_Id = @Id
				AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
				AND EREC_UgyUgyiratok.Allapot = ''50''
		UNION ALL
		SELECT 1
			FROM KRT_MappaTartalmak
				INNER JOIN EREC_KuldKuldemenyek ON EREC_KuldKuldemenyek.Id = KRT_MappaTartalmak.Obj_Id
			WHERE KRT_MappaTartalmak.Mappa_Id = @Id
				AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
				AND EREC_KuldKuldemenyek.Allapot = ''03''
		UNION ALL
		SELECT 1
			FROM KRT_MappaTartalmak
				INNER JOIN EREC_PldIratPeldanyok ON EREC_PldIratPeldanyok.Id = KRT_MappaTartalmak.Obj_Id
			WHERE KRT_MappaTartalmak.Mappa_Id = @Id
				AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
				AND EREC_PldIratPeldanyok.Allapot = ''50''
	)
	BEGIN
		INSERT INTO @Status
			SELECT TOP(1) 1 AS Allapot, AtadoUser, CimzettUser, (SELECT Tipus FROM KRT_Mappak WHERE Id = @Id)
				FROM
				(
					SELECT EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo AS AtadoUser ,EREC_UgyUgyiratok.Csoport_Id_Felelos AS CimzettUser
						FROM KRT_MappaTartalmak
							INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = KRT_MappaTartalmak.Obj_Id
						WHERE KRT_MappaTartalmak.Mappa_Id = @Id
							AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
							AND EREC_UgyUgyiratok.Allapot = ''50''
					UNION ALL
					SELECT EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo AS AtadoUser ,EREC_KuldKuldemenyek.Csoport_Id_Felelos AS CimzettUser
						FROM KRT_MappaTartalmak
							INNER JOIN EREC_KuldKuldemenyek ON EREC_KuldKuldemenyek.Id = KRT_MappaTartalmak.Obj_Id
						WHERE KRT_MappaTartalmak.Mappa_Id = @Id
							AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
							AND EREC_KuldKuldemenyek.Allapot = ''03''
					UNION ALL
					SELECT EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo AS AtadoUser ,EREC_PldIratPeldanyok.Csoport_Id_Felelos AS CimzettUser
						FROM KRT_MappaTartalmak
							INNER JOIN EREC_PldIratPeldanyok ON EREC_PldIratPeldanyok.Id = KRT_MappaTartalmak.Obj_Id
						WHERE KRT_MappaTartalmak.Mappa_Id = @Id
							AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
							AND EREC_PldIratPeldanyok.Allapot = ''50''
				) AS Objs
		RETURN;
	END
	ELSE
	BEGIN
		INSERT INTO @Status
			SELECT 0, Csoport_Id_Tulaj, Csoport_Id_Tulaj, Tipus
				FROM KRT_Mappak
				WHERE Id = @Id
		
		RETURN;
	END
	
	RETURN;
END

' 
END

GO
