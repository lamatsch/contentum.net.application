IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetEREC_CsatolmanyokAzonosito]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetEREC_CsatolmanyokAzonosito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetEREC_CsatolmanyokAzonosito]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetEREC_CsatolmanyokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

 --  select @Return = Leiras from EREC_Csatolmanyok where Id = @Obj_Id;

  select @Return = FajlNev from EREC_Csatolmanyok join Krt_dokumentumok on Erec_csatolmanyok.Dokumentum_Id= Krt_dokumentumok.id  where EREC_Csatolmanyok.Id = @Obj_Id
   
   RETURN @Return;

END

' 
END

GO
