IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetEREC_Hatarid_ObjektumokAzonosito]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetEREC_Hatarid_ObjektumokAzonosito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetEREC_Hatarid_ObjektumokAzonosito]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetEREC_Hatarid_ObjektumokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = EREC_HataridosFeladatok.Leiras from EREC_Hatarid_Objektumok 
	inner join EREC_HataridosFeladatok on EREC_HataridosFeladatok.Id = EREC_Hatarid_Objektumok.HataridosFeladat_Id
	where EREC_Hatarid_Objektumok.Id = @Obj_Id;
   
   RETURN @Return;

END

' 
END

GO
