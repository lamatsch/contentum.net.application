IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetEREC_HataridosFeladatokCountTable]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetEREC_HataridosFeladatokCountTable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetEREC_HataridosFeladatokCountTable]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetEREC_HataridosFeladatokCountTable]
(	
	@ObjektumId UNIQUEIDENTIFIER,
	@ExecutorUserId				uniqueidentifier,
    @FelhasznaloSzervezet_Id	uniqueidentifier
)
RETURNS TABLE
AS
RETURN
(
    SELECT CAST(COUNT(EREC_HataridosFeladatok.Id) AS NVARCHAR) + '',''
    + CAST(ISNULL(SUM(CASE WHEN ISNULL(EREC_HataridosFeladatok.Allapot,1) IN (''0'',''1'',''2'') THEN 1 ELSE 0 END),0) AS NVARCHAR) + '','' --új, nyitott, folyamatban állapot
    + CAST(ISNULL(MAX(CASE WHEN ISNULL(EREC_HataridosFeladatok.Allapot,1) IN (''0'',''1'',''2'') THEN EREC_HataridosFeladatok.Prioritas ELSE 0 END),0) AS nvarchar) --új, nyitott, folyamatban állapot
    AS FeladatCount
    FROM EREC_HataridosFeladatok
	WHERE getdate() BETWEEN EREC_HataridosFeladatok.ErvKezd AND EREC_HataridosFeladatok.ErvVege
	AND EREC_HataridosFeladatok.Obj_Id =  @ObjektumId
	AND EREC_HataridosFeladatok.Id IN 
	(
	    SELECT Id from dbo.fn_GetAllRightedEREC_HataridosFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id)
	)
)

' 
END

GO
