IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetEREC_IratAlairokAzonosito]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetEREC_IratAlairokAzonosito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetEREC_IratAlairokAzonosito]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetEREC_IratAlairokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = dbo.fn_GetEREC_IraIratokAzonosito(EREC_IratAlairok.Obj_Id) + '' - '' + KRT_Csoportok.Nev from EREC_IratAlairok
	LEFT JOIN KRT_Csoportok ON KRT_Csoportok.Id = EREC_IratAlairok.FelhasznaloCsoport_Id_Alairo
	where EREC_IratAlairok.Id = @Obj_Id;
   
   RETURN @Return;

END

' 
END

GO
