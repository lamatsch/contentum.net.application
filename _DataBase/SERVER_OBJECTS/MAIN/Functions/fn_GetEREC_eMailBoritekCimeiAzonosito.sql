IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetEREC_eMailBoritekCimeiAzonosito]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetEREC_eMailBoritekCimeiAzonosito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetEREC_eMailBoritekCimeiAzonosito]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetEREC_eMailBoritekCimeiAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = KRT_Partnerek.Nev from EREC_eMailBoritekCimei 
		inner join KRT_Partnerek on KRT_Partnerek.Id = EREC_eMailBoritekCimei.Partner_Id
		where EREC_eMailBoritekCimei.Id = @Obj_Id;
   
   RETURN @Return;

END

' 
END

GO
