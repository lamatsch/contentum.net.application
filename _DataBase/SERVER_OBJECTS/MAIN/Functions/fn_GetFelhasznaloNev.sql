IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetFelhasznaloNev]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetFelhasznaloNev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetFelhasznaloNev]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_GetFelhasznaloNev] (
	@FelhasznaloId UNIQUEIDENTIFIER
)
/*
	Feladata, hogy visszaadja az id-val megadott felhasználó nevét.
 */
returns nvarchar(400)

as
begin

DECLARE @nev nvarchar(400)

select @nev = KRT_Felhasznalok.Nev
	FROM KRT_Felhasznalok
	WHERE KRT_Felhasznalok.Id = @FelhasznaloId
      
return @nev

END

--select [dbo].fn_GetFelhasznaloNev(''efc09ec1-bb8e-438e-bcda-08e489f085b3'')

' 
END

GO
