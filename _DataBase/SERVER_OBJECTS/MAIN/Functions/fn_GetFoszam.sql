IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetFoszam]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetFoszam]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetFoszam]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetFoszam]
(	
	@UgyiratId uniqueidentifier
	
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400)

 SET @ret = 
              (SELECT EREC_UgyUgyiratok.Azonosito
		      FROM  EREC_UgyUgyiratok as EREC_UgyUgyiratok			  
			  WHERE EREC_UgyUgyiratok.Id = @UgyiratId )

RETURN @ret
END

' 
END

GO
