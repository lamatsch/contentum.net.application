IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetIratIktatoszam]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetIratIktatoszam]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetIratIktatoszam]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetIratIktatoszam]
(	
	@IratId uniqueidentifier
	
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400)

 SET @ret = 
          (SELECT EREC_IraIratok.[Azonosito]
		   FROM EREC_IraIratok as EREC_IraIratok						
		   WHERE EREC_IraIratok.Id = @IratId
		  )

RETURN @ret
END

' 
END

GO
