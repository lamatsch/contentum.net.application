IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetIratMetaDefinicioByContentType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetIratMetaDefinicioByContentType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetIratMetaDefinicioByContentType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_GetIratMetaDefinicioByContentType] (@Org uniqueidentifier, @ContentType nvarchar(100))
returns uniqueidentifier
/*
feladata, hogy a ContentType alapján megkeresse az irat metadefiníciót,
ha van ilyen (C2, esetleg B2 típusú obejktum metadefiníció)
returns null, ha ilyen irat metadefiníció nem taláható, vagy az irat metadefiníció Id

-- hívás példa:
select dbo.fn_GetIratMetaDefinicioByContentType(''450B510A-7CAA-46B0-83E3-18445C0C53A9'',''U31-075-01-AuditDokumentum'') --> ''5E71977D-E15F-41FF-ACC0-62C7B5B3C075''
go
select dbo.fn_GetIratMetaDefinicioByContentType(''450B510A-7CAA-46B0-83E3-18445C0C53A9'',''U31-075-MinosegBiztositas'') --> null
go
*/
AS
BEGIN
    declare @result uniqueidentifier
    set @result = null
    if @ContentType is not null and @Org is not null
    begin
        select @result = imd.Id
        from EREC_IratMetaDefinicio imd
        where convert(nvarchar(36),imd.Id) =
                  (select top 1 ColumnValue
                   from EREC_Obj_MetaDefinicio omd
                   where omd.ContentType = @ContentType
                   and Org=@Org
                   and getdate() between omd.ErvKezd and omd.ErvVege
                   )
              and getdate() between imd.ErvKezd and imd.ErvVege
    end
    return @result

END

' 
END

GO
