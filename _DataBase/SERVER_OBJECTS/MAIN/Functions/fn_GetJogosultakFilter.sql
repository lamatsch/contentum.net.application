IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetJogosultakFilter]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetJogosultakFilter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetJogosultakFilter]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [dbo].[fn_GetJogosultakFilter]
(
	@Org uniqueidentifier,
	@tablePrefix nvarchar(100)
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400) = ''''

DECLARE @Ertek nvarchar(400)

set @Ertek = (select Ertek from KRT_Parameterek where Nev = ''IGNORE_TULAJDONOS_JOGOSULTSAG'' and Org = @org)

IF(@Ertek = ''1'')
	set @ret = replace('' where (krt_jogosultak.Kezi = ''''I'''' or krt_jogosultak.Tipus != ''''T'''')'',''krt_jogosultak'', @tablePrefix)

RETURN @ret
END

' 
END

GO
