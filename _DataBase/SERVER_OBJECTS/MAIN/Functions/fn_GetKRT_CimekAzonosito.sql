IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetKRT_CimekAzonosito]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetKRT_CimekAzonosito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetKRT_CimekAzonosito]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetKRT_CimekAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = CASE Tipus
						WHEN ''03'' THEN CimTobbi
						ELSE ISNULL(Nev,ISNULL(TelepulesNev,'''') + '' '' + ISNULL(KozteruletNev,'''') + '' '' + ISNULL(Hazszam,''''))
					END
					from KRT_Cimek where Id = @Obj_Id;
   
   RETURN @Return;

END

' 
END

GO
