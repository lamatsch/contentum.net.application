IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetKRT_Szerepkor_FunkcioAzonosito]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetKRT_Szerepkor_FunkcioAzonosito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetKRT_Szerepkor_FunkcioAzonosito]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetKRT_Szerepkor_FunkcioAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = dbo.fn_GetKRT_FunkciokAzonosito(Funkcio_Id) + '' / '' + dbo.fn_GetKRT_SzerepkorokAzonosito(Szerepkor_Id) from KRT_Szerepkor_Funkcio where Id = @Obj_Id;
   
   RETURN @Return;

END

' 
END

GO
