IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetKuldemenyErkeztetoszam]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetKuldemenyErkeztetoszam]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetKuldemenyErkeztetoszam]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetKuldemenyErkeztetoszam]
(	
	@KuldemenyId uniqueidentifier
	
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400)

 SET @ret = 
          (SELECT EREC_KuldKuldemenyek.Azonosito
		   FROM EREC_KuldKuldemenyek as EREC_KuldKuldemenyek					    
		   WHERE EREC_KuldKuldemenyek.Id = @KuldemenyId
		  )

RETURN @ret
END

' 
END

GO
