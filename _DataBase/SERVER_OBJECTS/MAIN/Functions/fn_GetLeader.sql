IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetLeader]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetLeader]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetLeader]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION  [dbo].[fn_GetLeader]
  (
	@SzervezetId	uniqueidentifier	-- a felhasználó aktuális szervezetének azonosítója
  )
RETURNS UNIQUEIDENTIFIER
AS
BEGIN
	
	DECLARE @ret UNIQUEIDENTIFIER
	
	select @ret = KRT_CsoportTagok.Csoport_Id_Jogalany
	FROM KRT_CsoportTagok
	where KRT_CsoportTagok.Csoport_Id = @SzervezetId 
	AND KRT_CsoportTagok.Tipus = ''3'' 
	and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
	
	RETURN @ret

END

' 
END

GO
