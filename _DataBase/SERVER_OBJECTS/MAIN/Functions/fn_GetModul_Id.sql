IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetModul_Id]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetModul_Id]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetModul_Id]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_GetModul_Id] (
	@Kod nvarchar(100)
)
/*
	Feladata, hogy visszaadja a kóddal megadott modul id-ját
 */
returns uniqueidentifier

as
begin

DECLARE @Id uniqueidentifier

select top 1 @Id = KRT_Modulok.Id
	FROM KRT_Modulok
	WHERE KRT_Modulok.Kod = @Kod
      
return @Id

END

--select [dbo].[fn_GetObjTipId](''EREC_UgyUgyiratok'')

' 
END

GO
