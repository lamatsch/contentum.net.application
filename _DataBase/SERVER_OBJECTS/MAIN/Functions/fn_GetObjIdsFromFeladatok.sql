IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjIdsFromFeladatok]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetObjIdsFromFeladatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjIdsFromFeladatok]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetObjIdsFromFeladatok]
	(	@FelhasznaloId		UNIQUEIDENTIFIER
		,@SzervezetId		UNIQUEIDENTIFIER
		,@Obj_Type			NVARCHAR(100)
	)
RETURNS TABLE
AS
RETURN
(
	SELECT Obj_Id AS Id
		FROM EREC_HataridosFeladatok
			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,@SzervezetId) AS csoportok ON csoportok.Id = EREC_HataridosFeladatok.Felhasznalo_Id_Felelos
		WHERE GETDATE() BETWEEN EREC_HataridosFeladatok.ErvKezd AND EREC_HataridosFeladatok.ErvVege
			AND EREC_HataridosFeladatok.Allapot IN (''0'',''1'',''2'')
			AND EREC_HataridosFeladatok.Obj_type = @Obj_Type
)

' 
END

GO
