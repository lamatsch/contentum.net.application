
/*
Mint az fn_GetObjIdsFromFeladatok, de CSAK a felhaszn�l�
saj�t jog�n relev�ns Id-kat adja vissza
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjIdsFromFeladatokByFelhasznalo]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT') )
  DROP FUNCTION [dbo].[fn_GetObjIdsFromFeladatokByFelhasznalo]
GO

IF NOT EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjIdsFromFeladatokByFelhasznalo]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT') )
  BEGIN
    execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetObjIdsFromFeladatokByFelhasznalo]
    (	@FelhasznaloId		UNIQUEIDENTIFIER
		,@Obj_Type			NVARCHAR(100)
	)
    RETURNS TABLE
    AS
    RETURN
    ( 
             SELECT EREC_HataridosFeladatok.Obj_Id as ID
               FROM EREC_HataridosFeladatok
              WHERE 1 = 1
			    AND EREC_HataridosFeladatok.Felhasznalo_Id_Felelos = @FelhasznaloId
			    AND GETDATE() BETWEEN EREC_HataridosFeladatok.ErvKezd AND EREC_HataridosFeladatok.ErvVege
                AND EREC_HataridosFeladatok.Allapot IN (''0'',''1'',''2'')
                AND EREC_HataridosFeladatok.Obj_type = @Obj_Type
    )' 
  END

GO
