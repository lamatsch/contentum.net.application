IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjMetaDefinicioFelettesekByContentType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetObjMetaDefinicioFelettesekByContentType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjMetaDefinicioFelettesekByContentType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_GetObjMetaDefinicioFelettesekByContentType] (@Org uniqueidentifier, @ContentType nvarchar(100))
returns @result table ( Id uniqueidentifier
                      , ContentType nvarchar(100)
                      , Szint int
                      )
/*
feladata, hogy az objektum metadefiníciótól kiindulva meghatározza a
felettes objektum metadefiníciókat
returns: a megfelelo objektum metadefiníciók azonosítóit, ContentType-jukat
         és hierarchiaszintjüket tartalmazó tábla,
         a 0 szinten az eredeti objektum metadefiníció található (ha létezik)

-- hívás példa:
select * from dbo.fn_GetObjMetaDefinicioFelettesekByContentType(''450B510A-7CAA-46B0-83E3-18445C0C53A9'',''C_272_01_00_Tajekoztato_HajlektalanokElhelyezese'')
       --> ''6E819C53-88CE-417E-B1D1-79859AD6CD21''	C_272_01_00_Tajekoztato_HajlektalanokElhelyezese	0
       --> ''415DFA9D-ADF5-404E-A279-3C8BBEE1C8B9''	AltalanosMetaadatok_Tajekoztato						1
go

go
select * from dbo.fn_GetObjMetaDefinicioFelettesekByContentType(''450B510A-7CAA-46B0-83E3-18445C0C53A9'',''CTT_AX_213'')
       --> <üres tábla>
go
*/
AS
BEGIN

    if @ContentType is not null and @Org is not null
    begin
       declare @Obj_Metadefinicio_Id uniqueidentifier
       
       select top 1 @Obj_Metadefinicio_Id = omd.Id
           from EREC_Obj_MetaDefinicio omd
           where omd.ContentType = @ContentType
           and Org=@Org
           and getdate() between omd.ErvKezd and omd.ErvVege

        if @Obj_MetaDefinicio_Id is not null
        begin

		    insert into @result
                   select Id, ContentType, Szint
                   from dbo.fn_GetObjMetaDefinicioFelettesekById(@Obj_MetaDefinicio_Id)
                   order by Szint ASC;
        end
    end
    return;

END

' 
END

GO
