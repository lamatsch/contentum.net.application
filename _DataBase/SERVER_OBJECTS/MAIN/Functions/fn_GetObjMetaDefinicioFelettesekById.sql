IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjMetaDefinicioFelettesekById]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetObjMetaDefinicioFelettesekById]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjMetaDefinicioFelettesekById]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_GetObjMetaDefinicioFelettesekById] (@Obj_Metadefinicio_Id uniqueidentifier)
returns @result table ( Id uniqueidentifier
                      , ContentType nvarchar(100)
                      , Szint int
                      )
/*
feladata, hogy az objektum metadefiníciótól kiindulva meghatározza a
felettes objektum metadefiníciókat
returns: a megfelelo objektum metadefiníciók azonosítóit, ContentType-jukat
         és hierarchiaszintjüket tartalmazó tábla,
         a 0 szinten az eredeti objektum metadefiníció található (ha létezik)

-- hívás példa:
select * from dbo.fn_GetObjMetaDefinicioFelettesekById(''6E819C53-88CE-417E-B1D1-79859AD6CD21'')
       --> ''6E819C53-88CE-417E-B1D1-79859AD6CD21''	C_272_01_00_Tajekoztato_HajlektalanokElhelyezese	0
       --> ''415DFA9D-ADF5-404E-A279-3C8BBEE1C8B9''	AltalanosMetaadatok_Tajekoztato						1
go

go
select * from dbo.fn_GetObjMetaDefinicioFelettesekById(''9BA7EE76-279A-4534-82CB-B2FC5E0AE0A1'')
       --> <üres tábla>
go
*/
AS
BEGIN

    if @Obj_Metadefinicio_Id is not null
    begin
        -- hierarchia rekurzióval való lekérése
        with OMD_Hierarchia as
        (
            -- Base case
            select Id, Felettes_Obj_Meta, ContentType, 0 as Szint
            from EREC_Obj_MetaDefinicio
            where Id = @Obj_Metadefinicio_Id
            and getdate() between ErvKezd and ErvVege

            union all

            -- Recursive step
            select omd.Id, omd.Felettes_Obj_Meta, omd.ContentType, omdh.Szint + 1 as Szint
            from EREC_Obj_MetaDefinicio as omd
            join OMD_Hierarchia as omdh
            on omdh.Felettes_Obj_Meta = omd.Id
            and getdate() between omd.ErvKezd and omd.ErvVege
        )

		insert into @result
               select Id, ContentType, Szint
               from OMD_Hierarchia
               order by Szint ASC;
    end
    return;

END

' 
END

GO
