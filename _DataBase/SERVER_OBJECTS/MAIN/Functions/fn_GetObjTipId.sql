IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjTipId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetObjTipId]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjTipId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_GetObjTipId] (
	@Kod nvarchar(100)
)
/*
	Feladata, hogy visszaadja a kóddal megadott objektumtípus id-ját
 */
returns uniqueidentifier

as
begin

DECLARE @Id uniqueidentifier

select top 1 @Id = KRT_ObjTipusok.Id
	FROM KRT_ObjTipusok
	WHERE KRT_ObjTipusok.Kod = @Kod
      
return @Id

END

--select [dbo].[fn_GetObjTipId](''EREC_UgyUgyiratok'')

' 
END

GO
