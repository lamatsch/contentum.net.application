IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjTipusByTablaNev]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetObjTipusByTablaNev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjTipusByTablaNev]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetObjTipusByTablaNev]
	(	@TablaNev		sysname
	)
RETURNS CHAR(1)
AS
BEGIN
	if (@TablaNev = ''KRT_Orgok'') RETURN ''A'';
	if (@TablaNev = ''EREC_IraIktatoKonyvek'') RETURN ''B'';
	if (@TablaNev = ''EREC_UgyUgyiratok'') RETURN ''C'';
	if (@TablaNev = ''EREC_UgyUgyiratdarabok'') RETURN ''D'';
	if (@TablaNev = ''EREC_IraIratok'') RETURN ''E'';
	if (@TablaNev = ''EREC_PldIratPeldanyok'') RETURN ''F'';
	if (@TablaNev = ''EREC_KuldKuldemenyek'') RETURN ''G'';

RETURN null;
	
END

' 
END

GO
