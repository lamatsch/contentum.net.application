IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetObjType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjType]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_GetObjType] (
	@Id uniqueidentifier
)
/*
	Feladata, hogy visszaadja az id-val megadott objektum típus kódját
 */
returns nvarchar(100)

as
begin

DECLARE @Kod nvarchar(100)

select top 1 @Kod = KRT_ObjTipusok.Kod
	FROM KRT_ObjTipusok
	WHERE KRT_ObjTipusok.Id = @Id
      
return @Kod

END

--select [dbo].[fn_GetObjType](''A04417A6-54AC-4AF1-9B63-5BABF0203D42'')

' 
END

GO
