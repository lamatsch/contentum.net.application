IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjTypeNev]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetObjTypeNev]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjTypeNev]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_GetObjTypeNev] (
	@ObjType NVARCHAR(100)
)
/*
	Feladata, hogy visszaadja az id-val megadott objektum típus kódját
 */
returns nvarchar(100)

as
begin

DECLARE @Nev nvarchar(100)

select @Nev =  (CASE @ObjType
			    WHEN ''EREC_UgyUgyiratok'' THEN ''Ügyirat''
			    WHEN ''EREC_UgyUgyiratdarabok'' THEN ''Ügyiratdarab''
				WHEN ''EREC_IraIratok'' THEN ''Irat''
			    WHEN ''EREC_PldIratpeldanyok'' THEN ''Iratpéldány''
			    WHEN ''EREC_KuldKuldemenyek'' THEN ''Küldemény''
				ELSE ''''
				END)
      
return @Nev

END

--select [dbo].[fn_GetObjType](''A04417A6-54AC-4AF1-9B63-5BABF0203D42'')

' 
END

GO
