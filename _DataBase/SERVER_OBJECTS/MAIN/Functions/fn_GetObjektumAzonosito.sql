IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjektumAzonosito]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetObjektumAzonosito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetObjektumAzonosito]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetObjektumAzonosito] (
 @Obj_Type NVARCHAR(100),
 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = 
   CASE @Obj_Type
	   WHEN ''EREC_UgyUgyiratok'' THEN
            (dbo.fn_GetEREC_UgyUgyiratokAzonosito(@Obj_Id))
	   WHEN ''EREC_IraIratok'' THEN
	        (dbo.fn_GetEREC_IraIratokAzonosito(@Obj_Id))
	   WHEN ''EREC_PldIratPeldanyok'' THEN
	        (dbo.fn_GetEREC_PldIratPeldanyokAzonosito(@Obj_Id))
	   WHEN ''EREC_KuldKuldemenyek'' THEN
	         (dbo.fn_GetEREC_KuldKuldemenyekAzonosito(@Obj_Id))
	   ELSE NULL
   END
   
   RETURN @Return;

END

' 
END

GO
