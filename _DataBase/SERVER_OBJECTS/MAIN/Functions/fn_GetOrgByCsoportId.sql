IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetOrgByCsoportId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetOrgByCsoportId]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetOrgByCsoportId]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetOrgByCsoportId]
(
	@Csoport_Id uniqueidentifier
)
RETURNS uniqueidentifier
AS
BEGIN

	DECLARE @Org uniqueidentifier

	select @Org = KRT_Csoportok.Org
	from 
		KRT_Csoportok
	where
		KRT_Csoportok.Id = @Csoport_Id

	RETURN @Org

END

' 
END

GO
