IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetParentContentTypeFromIratMetaDefinicio]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetParentContentTypeFromIratMetaDefinicio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetParentContentTypeFromIratMetaDefinicio]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_GetParentContentTypeFromIratMetaDefinicio] (@Org uniqueidentifier, @ContentType nvarchar(100))
returns nvarchar(100)
/*
feladata, hogy a ContentType alapján megkeresve az objektum metadefiníciót,
majd abból az irat metadefiníciót, meghatározza a szülo iratmetadefiníciót,
és az annak megfelelo C2 típusú objektum metadefiníciót, amit az eredeti ContentType
felettesének tekintünk.
returns null, ha ilyen metadefiníció nem taláható, vagy a szülo ContentType-ja

-- hívás példa:
select dbo.fn_GetParentContentTypeFromIratMetaDefinicio(''450B510A-7CAA-46B0-83E3-18445C0C53A9'',''U31-075-01-AuditDokumentum'') --> ''U31-075-MinosegBiztositas''
go
select dbo.fn_GetParentContentTypeFromIratMetaDefinicio(''450B510A-7CAA-46B0-83E3-18445C0C53A9'',''U31-075-MinosegBiztositas'') --> null
go
*/
AS
BEGIN
    declare @result nvarchar(100)
    set @result = null
    if @ContentType is not null and @Org is not null
    begin
        declare @IratMetadefinicio_Id uniqueidentifier
        
        select @IratMetadefinicio_Id = dbo.fn_GetIratMetaDefinicioByContentType(@Org,@ContentType)
        if @IratMetaDefinicio_Id is not null
        begin
            declare @IratMetadefinicio_Id_Szulo uniqueidentifier
            select @IratMetadefinicio_Id_Szulo = dbo.fn_GetParentIratMetaDefinicioById(@IratMetadefinicio_Id)
            if @IratMetadefinicio_Id_Szulo is not null
            begin
                select @result = dbo.fn_GetContentTypeByIratMetaDefinicioId(@IratMetadefinicio_Id_Szulo)
            end
        end
    end
    return @result

END

' 
END

GO
