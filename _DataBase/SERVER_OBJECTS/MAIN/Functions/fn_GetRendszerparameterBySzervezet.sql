IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetRendszerparameterBySzervezet]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetRendszerparameterBySzervezet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetRendszerparameterBySzervezet]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================

CREATE FUNCTION [dbo].[fn_GetRendszerparameterBySzervezet]
(
	@CsoportId UNIQUEIDENTIFIER,
	@Nev NVARCHAR(400)
)
RETURNS NVARCHAR(400)
/*
	--Error_50202: org nem határozható meg: RAISERROR(''[50202]'',16,1)
*/
AS
BEGIN

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByCsoportId(@CsoportId)
	if (@Org is null)
	begin
		RETURN ''Error_50202''
	END
	
	DECLARE @Ertek NVARCHAR(400)

	select @Ertek = [KRT_Parameterek].[Ertek]
	from 
		[KRT_Parameterek]
	where
		[KRT_Parameterek].[Org] = @Org
		AND [KRT_Parameterek].[Nev] = @Nev

	RETURN @Ertek

END

' 
END

GO
