IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetRightsByJogtargyIdAndCsoportIdAndTipus]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetRightsByJogtargyIdAndCsoportIdAndTipus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetRightsByJogtargyIdAndCsoportIdAndTipus]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetRightsByJogtargyIdAndCsoportIdAndTipus]
  (	@JogtargyId	uniqueidentifier,	-- a jogtárgy azonosítója
	@CsoportId	uniqueidentifier,	-- a csoport azonosítója
	@Jogszint	char(1)	= ''I'',		-- vizsgált jogosultság szintje (I/O)
	@Tipus		char(1) = null		-- vizsgált jogosultság típusa (F,O,T,0,M)
  )
RETURNS int
AS
/******************************************************************************************************** 
* Visszaadja, hogy a paraméterben megadott csoportnak van-e hozzáfárése a megadott jogtárgyhoz.
* Visszatérési érték: 1, ha van jogosultsága, 0 egyébként.
********************************************************************************************************/
BEGIN
	return 1
/*
	if @Tipus is null
	BEGIN
		if @Jogszint = ''I'' and exists (select 1 from dbo.fn_GetAllRightedJogtargyAndTipusByCsoport(@CsoportId) where Id = @JogtargyId and Jogszint = @Jogszint)
			return 1
		else if exists (select 1 from dbo.fn_GetAllRightedJogtargyAndTipusByCsoport(@CsoportId) where Id = @JogtargyId)
			return 1
	END
	else
	BEGIN
		if @Jogszint = ''I'' and exists (select 1 from dbo.fn_GetAllRightedJogtargyAndTipusByCsoport(@CsoportId) where Id = @JogtargyId and Jogszint = @Jogszint and Tipus = @Tipus)
			return 1
		else if exists (select 1 from dbo.fn_GetAllRightedJogtargyAndTipusByCsoport(@CsoportId) where Id = @JogtargyId and Tipus = @Tipus)
			return 1
	END

	return 0
*/
END

' 
END

GO
