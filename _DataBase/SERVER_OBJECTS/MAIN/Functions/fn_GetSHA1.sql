IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetSHA1]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetSHA1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetSHA1]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_GetSHA1]
(	
	@Value VARCHAR(4000)
	
)
RETURNS nvarchar(40) 
AS
BEGIN

DECLARE @ret Nvarchar(40)

DECLARE @binvalue varbinary(160)
SET @binvalue = HashBytes(''SHA1'',@Value)

declare @i int
declare @length int
declare @hexstring char(16)
select @ret = ''''
select @i = 1
select @length = datalength(@binvalue)
select @hexstring = ''0123456789ABCDEF''
while (@i <= @length)
begin
	declare @tempint int
	declare @firstint int
	declare @secondint int
	select @tempint = convert(int, substring(@binvalue,@i,1))
	select @firstint = floor(@tempint/16)
	select @secondint = @tempint - (@firstint*16)
	select @ret = @ret + substring(@hexstring, @firstint+1, 1) + substring(@hexstring, @secondint+1, 1)
	select @i = @i + 1
end

RETURN @ret
END

' 
END

GO
