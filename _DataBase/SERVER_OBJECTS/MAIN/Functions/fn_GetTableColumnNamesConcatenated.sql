IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetTableColumnNamesConcatenated]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_GetTableColumnNamesConcatenated]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetTableColumnNamesConcatenated]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_GetTableColumnNamesConcatenated] (@TableName nvarchar(100))
returns varchar(4000)
/*
a név szerint átadott táblában szereplo oszlopok neveit vesszovel elválasztva összefuzi és stringként adja vissza
használható pl. rekordok másolásánál insert into ... select ... parancsban
ha a tábla nem létezik, vagy nem találhatók oszlopok, null-t ad vissza
-- hívás példa:
select dbo.fn_GetTableColumnNamesConcatenated(''EREC_Irat_Iktatokonyvei'') -->
	''Id, IraIktatokonyv_Id, Csoport_Id_Iktathat, Ver, Note, Stat_id, ErvKezd, ErvVege, Letrehozo_id, LetrehozasIdo, Modosito_id, ModositasIdo, Zarolo_id, ZarolasIdo, Tranz_id, UIAccessLog_id''
go
select dbo.fn_GetTableColumnNamesConcatenated(''EREC_MinosegBiztositas'') --> null
go
select dbo.fn_GetTableColumnNamesConcatenated(null) --> null
go
*/
AS
BEGIN
    declare @result varchar(4000)
    set @result = null
    if @TableName is not null
    begin
        select @result = STUFF((SELECT '', '' +  name FROM (select name from syscolumns
					where id = 
					(select id from sysobjects
					  where name = @TableName)) as tbl FOR XML PATH('''')),1, 2, '''')
    end
    return @result

END

' 
END

GO
