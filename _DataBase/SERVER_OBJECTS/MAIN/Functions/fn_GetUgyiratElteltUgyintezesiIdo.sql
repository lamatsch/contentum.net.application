SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select 1
            from  sysobjects
           where  id = object_id('fn_GetUgyiratElteltUgyintezesiIdo')
            and   type = 'FN')
   drop function fn_GetUgyiratElteltUgyintezesiIdo
go

-- =============================================
-- Author:		AXIS RENDSZERHAZ KFT.
-- Create date: 2017.08
-- Description:	
-- =============================================
CREATE FUNCTION dbo.fn_GetUgyiratElteltUgyintezesiIdo
(
	@UgyintezesKezdete datetime,
	@IntezesiIdoegyseg	nvarchar(64),
	@IratMetadefinicio_Id uniqueidentifier,
	@FelfuggesztettNapokSzama int,
	@Org uniqueidentifier
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	declare @current_date datetime = getdate()
	declare @diff int = 0
	declare @idoegysegnev nvarchar(400)
	declare @ret nvarchar(max)
	
	--ugyiratban megadott
	IF @IntezesiIdoegyseg is null
	BEGIN
	    -- metadefinicióban megadott
		IF @IratMetadefinicio_Id is not null
		BEGIN
			SELECT @IntezesiIdoegyseg = Idoegyseg 
			FROM EREC_IratMetadefinicio
			WHERE Id = @IratMetadefinicio_Id
		END

		-- default rendszerparameter
		IF @IntezesiIdoegyseg is null
		BEGIN
			SELECT @IntezesiIdoegyseg = Ertek 
			FROM KRT_Parameterek
			WHERE Nev = 'DEFAULT_INTEZESI_IDO_IDOEGYSEG'
			and Org = @Org
			and getdate() between ErvKezd and ErvVege
		END

		-- naptari nap
		IF @IntezesiIdoegyseg is null
		BEGIN
			set @IntezesiIdoegyseg = '1440'
		END
	END

	select @idoegysegnev = KRT_KodTarak.Nev from
	KRT_KodTarak join KRT_KodCsoportok
	on KRT_KodTarak.KodCsoport_Id = KRT_KodCsoportok.Id
	where KRT_KodCsoportok.Kod = 'IDOEGYSEG'
	and KRT_KodTarak.Kod = @IntezesiIdoegyseg
	and KRT_KodTarak.Org = @Org


	IF @FelfuggesztettNapokSzama is null
	BEGIN
		set @FelfuggesztettNapokSzama = 0
	END

	-- munkanap nap
	IF (@IntezesiIdoegyseg = '-1440')
	BEGIN
			SET @diff = dbo.fn_munkanapok_szama(@UgyintezesKezdete, @current_date);
			set @diff = @diff - @FelfuggesztettNapokSzama
	END
	ELSE IF (@IntezesiIdoegyseg = '1440')
		BEGIN
			SET @diff = DATEDIFF(DAY, @UgyintezesKezdete, @current_date)
			set @diff = @diff - @FelfuggesztettNapokSzama
		END
	ELSE
		BEGIN
		    set @UgyintezesKezdete = DATEADD(DAY, @FelfuggesztettNapokSzama, @UgyintezesKezdete)
			SET @diff = DATEDIFF(MINUTE,  @current_date, @UgyintezesKezdete) / CAST (@IntezesiIdoegyseg AS INT)
		END


	IF @diff < 0
		SET @diff = 0

    IF @idoegysegnev is not null
		set @ret = cast(@diff as nvarchar) + ' ' +@idoegysegnev
	else
		set @ret = cast(@diff as nvarchar) 

	RETURN @ret

END


