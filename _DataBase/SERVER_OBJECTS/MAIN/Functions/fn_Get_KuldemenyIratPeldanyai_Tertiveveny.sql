IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_Get_KuldemenyIratPeldanyai_Tertiveveny]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_Get_KuldemenyIratPeldanyai_Tertiveveny]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_Get_KuldemenyIratPeldanyai_Tertiveveny]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE function [dbo].[fn_Get_KuldemenyIratPeldanyai_Tertiveveny] (@Kuldemeny_Id uniqueidentifier, @isCsatolmany char(1))
returns uniqueidentifier
/*
Visszaadja az Iratpéldányhoz tartozó küldeményhez rendelt visszaérkezett Tértivevény azonosítóját
@isCsatolmany = 1, akkor a Csatolmanyok között keresi (scannelt) és a DocumentumId-t adja vissza
@isCsatolmany = 0, akkor csak a melléklet Id-t adja vissza
*/
AS
BEGIN

	declare @Tertiveveny_id uniqueidentifier 
	if @isCsatolmany = ''0''
	Begin

		-- Tertivevény (nincs csatolmány)
		Set @Tertiveveny_Id = (select top 1 t.Id from EREC_Mellekletek m
		inner join EREC_KuldTertivevenyek t on t.Kuldemeny_Id=m.KuldKuldemeny_Id 
		left join EREC_KuldKuldemenyek k on k.Id=m.KuldKuldemeny_Id
		where k.PostazasIranya=''2'' -- Kimeno
		and m.AdathordozoTipus=''14'' -- Tertiveveny
		and getdate() between m.ErvKezd and m.ErvVege
		and k.Allapot != ''90'' --Sztornozott
		and k.Id = @Kuldemeny_Id) 
	End
	Else
	Begin
-- Tertivevény-csatolmány
		Set @Tertiveveny_Id =(select top 1 cs.Dokumentum_Id from EREC_Mellekletek m
		left join EREC_KuldKuldemenyek k on k.Id=m.KuldKuldemeny_Id
		left join EREC_Csatolmanyok cs on cs.KuldKuldemeny_Id=m.KuldKuldemeny_Id
		where k.PostazasIranya=''2'' -- Kimeno
		and m.AdathordozoTipus=''14'' -- Tertiveveny
		and getdate() between m.ErvKezd and m.ErvVege
		and getdate() between cs.ErvKezd and cs.ErvVege
		and k.Allapot != ''90'' --Sztornozott
		and k.Id = @Kuldemeny_Id ) 
	End

  return @Tertiveveny_Id

END

' 
END

GO
