IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_HasFunctionRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_HasFunctionRight]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_HasFunctionRight]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_HasFunctionRight]
  (	@FelhasznaloId	uniqueidentifier,	-- a felhasználó azonosítója (azonos a csoport azonosítóval)
	@SzervezetId	uniqueidentifier,	-- a felhasználó aktuális szervezetének azonosítója
	@FunkcioKod	nvarchar(100)	-- A KRT_Funkciok tábla Kod mezojének keresett értéke
  )
RETURNS int
AS
BEGIN
	-- ha a felhasználó rendelkezik a megadott funkciójoggal, és a szervezeti tagsága is érvényes, 1-et
	-- egyébként 0-át ad vissza
	IF EXISTS
		(
			SELECT 1
				FROM KRT_CsoportTagok
					INNER JOIN KRT_Csoportok ON KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
					INNER JOIN KRT_Felhasznalok ON KRT_Felhasznalok.Id = KRT_Csoportok.Id
					INNER JOIN KRT_Felhasznalo_Szerepkor ON KRT_Felhasznalok.Id = KRT_Felhasznalo_Szerepkor.Felhasznalo_Id
					INNER JOIN KRT_Szerepkorok ON KRT_Szerepkorok.Id = KRT_Felhasznalo_Szerepkor.Szerepkor_Id -- csak az érvényesség ellenorzés miatt
					INNER JOIN KRT_Szerepkor_Funkcio ON KRT_Szerepkor_Funkcio.Szerepkor_Id = KRT_Szerepkorok.Id
					INNER JOIN KRT_Funkciok ON KRT_Funkciok.Id = KRT_Szerepkor_Funkcio.Funkcio_Id
				WHERE KRT_CsoportTagok.Csoport_Id_Jogalany = @FelhasznaloId
					AND KRT_CsoportTagok.Csoport_Id = @SzervezetId
					AND GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
					AND GETDATE() BETWEEN KRT_Csoportok.ErvKezd AND KRT_Csoportok.ErvVege
					AND GETDATE() BETWEEN KRT_Felhasznalok.ErvKezd AND KRT_Felhasznalok.ErvVege
					AND GETDATE() BETWEEN KRT_Felhasznalo_Szerepkor.ErvKezd AND KRT_Felhasznalo_Szerepkor.ErvVege
					AND GETDATE() BETWEEN KRT_Szerepkorok.ErvKezd AND KRT_Szerepkorok.ErvVege
					AND GETDATE() BETWEEN KRT_Szerepkor_Funkcio.ErvKezd AND KRT_Szerepkor_Funkcio.ErvVege
					AND GETDATE() BETWEEN KRT_Funkciok.ErvKezd AND KRT_Funkciok.ErvVege
					AND KRT_Funkciok.Kod = @FunkcioKod
					AND IsNull(KRT_Felhasznalo_Szerepkor.Csoport_Id, @SzervezetId) = @SzervezetId
		)
		RETURN 1;
	RETURN 0;
END

' 
END

GO
