IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_HatosagiAdatKodtarErtek]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_HatosagiAdatKodtarErtek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_HatosagiAdatKodtarErtek]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_HatosagiAdatKodtarErtek] (
	@KodCsoport nvarchar(64),
	@Kod nvarchar(64),
	@Org uniqueidentifier,
	@Ervenyesseg datetime
)
/*
	Feladata, hogy lekérdezze egy kódcsoporton belüli érték nevét.
 */
returns nvarchar(64)

as
begin

DECLARE @ret nvarchar(400)

IF @Ervenyesseg < ''2012.01.01''
BEGIN
	set @ret = @Kod
END
ELSE
BEGIN
   SELECT 
   @ret = 
   KRT_KodTarak.RovidNev
   FROM KRT_KodTarak,
   KRT_KodCsoportok
   WHERE 
   KRT_KodTarak.KodCsoport_Id = KRT_KodCsoportok.Id
   AND KRT_KodCsoportok.Kod = @KodCsoport
   AND KRT_KodTarak.Kod = @Kod
   AND @Ervenyesseg BETWEEN KRT_KodTarak.ErvKezd AND KRT_KodTarak.ErvVege
   AND KRT_KodTarak.Org = @Org
END
   
      
return @ret

end

-- select [dbo].[fn_HatosagiAdatKodtarErtek](''UGY_FAJTAJA'', ''1'', ''450B510A-7CAA-46B0-83E3-18445C0C53A9'',''2012.01.02'')

' 
END

GO
