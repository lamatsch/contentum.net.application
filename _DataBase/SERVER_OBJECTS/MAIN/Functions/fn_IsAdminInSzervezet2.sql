IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_IsAdminInSzervezet2]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_IsAdminInSzervezet2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_IsAdminInSzervezet2]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_IsAdminInSzervezet2]
  (	@CsoportId	uniqueidentifier,	-- a csoport (felhasználónak megfelelo saját csoport) azonosítója
	@FelhasznaloSzervezet_Id	uniqueidentifier	-- a szervezet azonosítója, amiben az adott felhasználó jogait figyeljük (belépéskor választott szervezet)
  )
RETURNS int
AS
BEGIN
 
 ------------
 DECLARE @maidatum date  = GETDATE()

 ----------- modified by mg

	IF @FelhasznaloSzervezet_Id IS NOT NULL AND EXISTS
		(
			SELECT 1
				FROM KRT_CsoportTagok
					INNER JOIN KRT_Csoportok ON KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id
				WHERE KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId
					AND KRT_CsoportTagok.Csoport_Id = @FelhasznaloSzervezet_Id
                  ----------------------

				  AND @maidatum BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
					AND @maidatum BETWEEN KRT_Csoportok.ErvKezd AND KRT_Csoportok.ErvVege
				  ---------------------modified by mg


				--	AND GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
				--	AND GETDATE() BETWEEN KRT_Csoportok.ErvKezd AND KRT_Csoportok.ErvVege
					AND KRT_Csoportok.Tipus = ''R''
		)
		RETURN 1;
	RETURN 0;
END

' 
END

GO
