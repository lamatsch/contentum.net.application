IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_IsLeaderInSzervezet]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_IsLeaderInSzervezet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_IsLeaderInSzervezet]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION  [dbo].[fn_IsLeaderInSzervezet]
  (	@FelhasznaloId	uniqueidentifier,	-- a felhasználó azonosítója (azonos a csoport azonosítóval)
	@SzervezetId	uniqueidentifier	-- a felhasználó aktuális szervezetének azonosítója
  )
RETURNS int
AS
BEGIN
	-- ha a felhasználó rendelkezik a vezetoi tagsággal az adott szervezetben, 1-et
	-- egyébként 0-át ad vissza
	IF EXISTS
		(
			SELECT 1
				FROM KRT_CsoportTagok
					INNER JOIN KRT_Csoportok jogalany ON jogalany.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
					INNER JOIN KRT_Csoportok szervezet ON szervezet.Id = KRT_CsoportTagok.Csoport_Id
				WHERE KRT_CsoportTagok.Csoport_Id_Jogalany = @FelhasznaloId
					AND KRT_CsoportTagok.Csoport_Id = @SzervezetId
					AND GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
					AND GETDATE() BETWEEN jogalany.ErvKezd AND jogalany.ErvVege
					AND GETDATE() BETWEEN szervezet.ErvKezd AND szervezet.ErvVege
					AND KRT_CsoportTagok.Tipus = ''3'' -- vezeto
		)
		RETURN 1;
	RETURN 0;
END

' 
END

GO
