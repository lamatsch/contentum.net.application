IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_KRT_CimekCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_KRT_CimekCheckUK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_KRT_CimekCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_KRT_CimekCheckUK] (
             @Org uniqueidentifier ,
             @Tipus nvarchar(64) ,
             @Forras char(1) ,
             @IRSZ Nvarchar(20) ,
             @Nev Nvarchar(100) ,
             @TelepulesNev Nvarchar(100) ,
             @KozteruletNev Nvarchar(100) ,
             @KozteruletTipusNev Nvarchar(100) ,
             @Hazszam Nvarchar(100) ,
             @Hazszamig Nvarchar(100) ,
             @HazszamBetujel Nvarchar(10) ,
             @Lepcsohaz Nvarchar(100) ,
             @Szint Nvarchar(10) ,
             @Ajto Nvarchar(20) ,
             @AjtoBetujel Nvarchar(20) ,
             @Tobbi Nvarchar(100) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_Cimek where
         (Org = @Org or (Org is null and @Org is null)) and            
         (Tipus = @Tipus or (Tipus is null and @Tipus is null)) and            
         (Forras = @Forras or (Forras is null and @Forras is null)) and            
         (IRSZ = @IRSZ or (IRSZ is null and @IRSZ is null)) and            
         (Nev = @Nev or (Nev is null and @Nev is null)) and            
         (TelepulesNev = @TelepulesNev or (TelepulesNev is null and @TelepulesNev is null)) and            
         (KozteruletNev = @KozteruletNev or (KozteruletNev is null and @KozteruletNev is null)) and            
         (KozteruletTipusNev = @KozteruletTipusNev or (KozteruletTipusNev is null and @KozteruletTipusNev is null)) and            
         (Hazszam = @Hazszam or (Hazszam is null and @Hazszam is null)) and            
         (Hazszamig = @Hazszamig or (Hazszamig is null and @Hazszamig is null)) and            
         (HazszamBetujel = @HazszamBetujel or (HazszamBetujel is null and @HazszamBetujel is null)) and            
         (Lepcsohaz = @Lepcsohaz or (Lepcsohaz is null and @Lepcsohaz is null)) and            
         (Szint = @Szint or (Szint is null and @Szint is null)) and            
         (Ajto = @Ajto or (Ajto is null and @Ajto is null)) and            
         (AjtoBetujel = @AjtoBetujel or (AjtoBetujel is null and @AjtoBetujel is null)) and            
         (Tobbi = @Tobbi or (Tobbi is null and @Tobbi is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END

' 
END

GO
