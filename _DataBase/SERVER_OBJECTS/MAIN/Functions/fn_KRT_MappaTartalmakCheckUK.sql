IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_KRT_MappaTartalmakCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_KRT_MappaTartalmakCheckUK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_KRT_MappaTartalmakCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_KRT_MappaTartalmakCheckUK] (
             @Mappa_Id uniqueidentifier ,
             @Obj_Id uniqueidentifier ,
             @Obj_type Nvarchar(100) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_MappaTartalmak where
         (Mappa_Id = @Mappa_Id or (Mappa_Id is null and @Mappa_Id is null)) and            
         (Obj_Id = @Obj_Id or (Obj_Id is null and @Obj_Id is null)) and            
         (Obj_type = @Obj_type or (Obj_type is null and @Obj_type is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END

' 
END

GO
