IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_KRT_ObjTipusokCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_KRT_ObjTipusokCheckUK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_KRT_ObjTipusokCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_KRT_ObjTipusokCheckUK] (
             @Kod Nvarchar(100) , @Obj_Id_Szulo uniqueidentifier ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_ObjTipusok where
         (Kod = @Kod or (Kod is null and @Kod is null)) and                  
         (Obj_Id_Szulo = @Obj_Id_Szulo or (Obj_Id_Szulo is null and @Obj_Id_Szulo is null )) and 
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END

' 
END

GO
