IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_KRT_OrszagokCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_KRT_OrszagokCheckUK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_KRT_OrszagokCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_KRT_OrszagokCheckUK] (
             @Org uniqueidentifier ,
             @Kod Nvarchar(10) ,
             @Nev Nvarchar(100) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_Orszagok where
         (Org = @Org or (Org is null and @Org is null)) and            
         (Kod = @Kod or (Kod is null and @Kod is null)) and            
         (Nev = @Nev or (Nev is null and @Nev is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END

' 
END

GO
