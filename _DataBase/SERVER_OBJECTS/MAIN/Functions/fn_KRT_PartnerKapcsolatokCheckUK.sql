IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_KRT_PartnerKapcsolatokCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_KRT_PartnerKapcsolatokCheckUK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_KRT_PartnerKapcsolatokCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_KRT_PartnerKapcsolatokCheckUK] (
             @Partner_id uniqueidentifier ,
             @Partner_id_kapcsolt uniqueidentifier ,
             @Tipus nvarchar(64) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_PartnerKapcsolatok where
         (Partner_id = @Partner_id or (Partner_id is null and @Partner_id is null)) and            
         (Partner_id_kapcsolt = @Partner_id_kapcsolt or (Partner_id_kapcsolt is null and @Partner_id_kapcsolt is null)) and            
         (Tipus = @Tipus or (Tipus is null and @Tipus is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END

' 
END

GO
