IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_KRT_Szerepkor_FunkcioCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_KRT_Szerepkor_FunkcioCheckUK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_KRT_Szerepkor_FunkcioCheckUK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_KRT_Szerepkor_FunkcioCheckUK] (
             @Szerepkor_Id uniqueidentifier ,
             @Funkcio_Id uniqueidentifier ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_Szerepkor_Funkcio where
         (Szerepkor_Id = @Szerepkor_Id or (Szerepkor_Id is null and @Szerepkor_Id is null)) and            
         (Funkcio_Id = @Funkcio_Id or (Funkcio_Id is null and @Funkcio_Id is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END

' 
END

GO
