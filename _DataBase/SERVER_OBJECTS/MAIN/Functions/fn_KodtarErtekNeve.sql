IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_KodtarErtekNeve]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_KodtarErtekNeve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_KodtarErtekNeve]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_KodtarErtekNeve] (
	@KodCsoport nvarchar(64),
	@Kod nvarchar(64),
	@Org uniqueidentifier
)
/*
	Feladata, hogy lekérdezze egy kódcsoporton belüli érték nevét.
 */
returns nvarchar(100)

as
begin

DECLARE @nev nvarchar(100)

select @nev = dbo.krt_kodtarak.nev
  from dbo.krt_kodcsoportok,
       dbo.krt_kodtarak
 where dbo.krt_kodcsoportok.id = dbo.krt_kodtarak.kodcsoport_id
   and dbo.krt_kodcsoportok.kod = @KodCsoport
   and dbo.krt_kodtarak.kod = @Kod
   and dbo.krt_kodtarak.Org=@Org
      
   return @nev

end

-- select [dbo].[fn_KodtarErtekNeve](''EMAILCIMCIM_TIPUS'', ''1'')

' 
END

GO
