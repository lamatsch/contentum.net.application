IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_Letezik]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_Letezik]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_Letezik]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_Letezik]
		(
		@ErvKezd  datetime,
		@ErvVege  datetime,
		@Datumtol datetime,
		@Datumig  datetime
		)
RETURNS char(1) 
AS
BEGIN
  RETURN dbo.fn_Ervenyesseg(''LET'',@ErvKezd,@ErvVege,@Datumtol,@Datumig)
END
-- select dbo.fn_Letezik(getdate()-1,NULL,getdate()-2,NULL) LET

' 
END

GO
