IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_MergeCim]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_MergeCim]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_MergeCim]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fn_MergeCim]
(	
	-- Add the parameters for the function here
	@Tipus Nvarchar(64),
	@OrszagNev Nvarchar(400),
	@IRSZ Nvarchar(20),
	@TelepulesNev Nvarchar(100),
	@KozteruletNev Nvarchar(100),
	@KozteruletTipusNev Nvarchar(100),
	@Hazszam Nvarchar(100),
	@Hazszamig Nvarchar(100),
	@HazszamBetujel Nvarchar(100),
	@Lepcsohaz Nvarchar(100),
	@Szint Nvarchar(10),
	@Ajto Nvarchar(20),
	@AjtoBetujel Nvarchar(20),
	@CimTobbi Nvarchar(100)
)
RETURNS nvarchar(4000) 
AS
BEGIN
DECLARE @ret Nvarchar(4000)
select @ret =
CASE @Tipus
	WHEN ''01'' THEN 
		CASE WHEN @OrszagNev is NULL OR @OrszagNev = '''' THEN '''' ELSE @OrszagNev + '' '' END +
		CASE WHEN @IRSZ is NULL OR @IRSZ = '''' THEN '''' ELSE @IRSZ + '' '' END +
		CASE WHEN @TelepulesNev is NULL OR @TelepulesNev = '''' THEN '''' ELSE @TelepulesNev + '' '' END +
		CASE WHEN @KozteruletNev is NULL OR @KozteruletNev = '''' THEN '''' ELSE @KozteruletNev + '' '' END +
		CASE WHEN @KozteruletTipusNev is NULL OR @KozteruletTipusNev = '''' THEN '''' ELSE @KozteruletTipusNev + '' '' END +
		CASE WHEN @Hazszam is NULL OR @Hazszam = '''' THEN '''' ELSE @Hazszam + '' '' END +
		CASE WHEN @Hazszamig is NULL OR @Hazszamig = '''' THEN '''' ELSE ''-'' + @Hazszamig + '' '' END +		
--		CASE WHEN @HazszamBetujel is NULL OR @HazszamBetujel = '''' THEN '''' ELSE @HazszamBetujel + '' '' END +
		CASE WHEN @HazszamBetujel is NULL OR @HazszamBetujel = '''' THEN '''' ELSE ''/'' + @HazszamBetujel + '' '' END +
--		CASE WHEN @Lepcsohaz is NULL OR @Lepcsohaz = '''' THEN '''' ELSE @Lepcsohaz + '' '' END +
		CASE WHEN @Lepcsohaz is NULL OR @Lepcsohaz = '''' THEN '''' ELSE @Lepcsohaz + '' lépcsőház '' END +
--		CASE WHEN @Szint is NULL OR @Szint = '''' THEN '''' ELSE @Szint + '' '' END +
		CASE WHEN @Szint is NULL OR @Szint = '''' THEN '''' ELSE @Szint + ''. emelet '' END +
--		CASE WHEN @Ajto is NULL OR @Ajto = '''' THEN '''' ELSE @Ajto + '' '' END +
--		CASE WHEN @AjtoBetujel is NULL OR @AjtoBetujel = '''' THEN '''' ELSE @AjtoBetujel + '' '' END
		CASE WHEN @Ajto is NULL OR @Ajto = '''' THEN '''' ELSE 
			CASE WHEN @AjtoBetujel is NULL OR @AjtoBetujel = '''' THEN '' ajtó '' ELSE ''/'' + @AjtoBetujel + '' ajtó '' END
		END
ELSE
	@CimTobbi
END

	-- Add the SELECT statement with parameter references here
	RETURN @ret
END

' 
END

GO
