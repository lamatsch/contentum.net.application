IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_MergeErkeztetoszam]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_MergeErkeztetoszam]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_MergeErkeztetoszam]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_MergeErkeztetoszam]
(	
	@IktatoKonyv_MegkulJelzes Nvarchar(20) = null,
	@Ev int = null,
	@ErkeztetoSzam int = null
	
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400)
DECLARE @Erkeztetoszam_str NVARCHAR(40)
DECLARE @Ev_str NVARCHAR(40)

if (@IktatoKonyv_MegkulJelzes is null) SET @IktatoKonyv_MegkulJelzes = ''''

if (@ErkeztetoSzam is not null)
	SET @Erkeztetoszam_str = CAST(@ErkeztetoSzam as Nvarchar(40))
else
	SET @Erkeztetoszam_str = '' ''

if (@Ev is not null)
	SET @Ev_str = CAST(@Ev as Nvarchar(40))
else
	SET @Ev_str = '' ''
	
SET @ret = @IktatoKonyv_MegkulJelzes + '' - '' + @Erkeztetoszam_str + '' /'' + @Ev_str + ''''

RETURN @ret
END

' 
END

GO
