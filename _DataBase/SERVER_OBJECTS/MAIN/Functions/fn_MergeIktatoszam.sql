IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_MergeIktatoszam]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_MergeIktatoszam]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_MergeIktatoszam]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_MergeIktatoszam]
(	
	@IktatoKonyv_MegkulJelzes Nvarchar(20) = null,
	@IktatoKonyv_Iktatohely Nvarchar(100) = null,
	@KozpontiIktatasJelzo char(1) = null,
	@Ev int = null,
	@Foszam int = null,
	@Alszam int = null,
	@IratPldSzam int = null
	
	
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400)
DECLARE @Foszam_str NVARCHAR(40)
DECLARE @Alszam_str NVARCHAR(40)
DECLARE @Ev_str NVARCHAR(40)
DECLARE @IratPldSzam_str NVARCHAR(40)

if (@IktatoKonyv_MegkulJelzes is null) SET @IktatoKonyv_MegkulJelzes = ''''
if (@IktatoKonyv_Iktatohely is null) SET @IktatoKonyv_Iktatohely = ''''
if (@KozpontiIktatasJelzo is null) SET @KozpontiIktatasJelzo = ''''

if (@Foszam is not null)
	SET @Foszam_str = CAST(@Foszam as Nvarchar(40))
else
	SET @Foszam_str = ''??''

if (@Alszam is not null)
	SET @Alszam_str = CAST(@Alszam as Nvarchar(40))
else
	SET @Alszam_str = ''??''

if (@Ev is not null)
	SET @Ev_str = CAST(@Ev as Nvarchar(40))
else
	SET @Ev_str = '' ''

if (@IratPldSzam is not null)
	SET @IratPldSzam_str = '' ('' + CAST(@IratPldSzam as Nvarchar(40)) + '')''
	--SET @IratPldSzam_str = '' /'' + CAST(@IratPldSzam as Nvarchar(40))
else
	SET @IratPldSzam_str = ''''
	
--SET @ret = @IktatoKonyv_MegkulJelzes + '' /'' + @Foszam_str + +'' - '' + @Alszam_str + '' /'' + @Ev_str + @IratPldSzam_str

if (@KozpontiIktatasJelzo = ''1'')
	begin
		SET @ret = @Foszam_str + '' - '' + @Alszam_str + '' /'' + @Ev_str
        if (@IktatoKonyv_MegkulJelzes is not null and @IktatoKonyv_MegkulJelzes != '''')
        begin
            SET @ret = @ret + '' /'' + @IktatoKonyv_MegkulJelzes
        end
        SET @ret = @ret + @IratPldSzam_str
	end
else
	begin
		SET @ret = @IktatoKonyv_Iktatohely + '' /'' + @Foszam_str + '' - '' + @Alszam_str + '' /'' + @Ev_str
        if (@IktatoKonyv_MegkulJelzes is not null and @IktatoKonyv_MegkulJelzes != '''')
        begin
            SET @ret = @ret + '' /'' + @IktatoKonyv_MegkulJelzes
        end
        SET @ret = @ret + @IratPldSzam_str
	end

RETURN @ret
END

' 
END

GO
