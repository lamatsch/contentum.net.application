IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_MergeIrattariTetelSzam]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_MergeIrattariTetelSzam]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_MergeIrattariTetelSzam]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_MergeIrattariTetelSzam]
(	
	@AgazatiJelek_Kod Nvarchar(100) = '''',
	@IrattariTetelszam Nvarchar(100) = '''',
	@MegorzesiIdo Nvarchar(100) = '''',
	@IrattariJel nvarchar(2) = '''', -- ha értéke ''L'' = ''Levéltárba adandó'', a megorzési ido helyett 0-t adunk vissza
	@Org uniqueidentifier
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400)

	DECLARE @IRATTARI_TETELSZAM_SORREND nvarchar(400)
	SET @IRATTARI_TETELSZAM_SORREND=
		(SELECT top 1 Ertek from dbo.KRT_Parameterek
		where Nev = ''IRATTARI_TETELSZAM_SORREND''
		and getdate() between ErvKezd and ErvVege
		and Org=@Org)

	declare @separator nvarchar(10)
	set @separator = (select top 1 Ertek from KRT_Parameterek where Nev=''ITSZ_ELVALASZTO_JEL'' and getdate() between ErvKezd and ErvVege and Org=@Org)
	if (@separator is null)
		set @separator = '' / ''

   -- Ha NULL maradna, üres értéket generálna
	if (@MegorzesiIdo is null)
		set @MegorzesiIdo = ''?''
		
	--2008, 2009-es irattári jelekre	
	if (ISNUMERIC(@IrattariTetelszam) = 1 and @IrattariJel = ''L'')
	BEGIN
		if (@MegorzesiIdo = ''4'')
			SET @MegorzesiIdo = ''15''
	    if (@MegorzesiIdo = ''2'')
			SET @MegorzesiIdo = ''5''
	END

	IF @IRATTARI_TETELSZAM_SORREND=0 or @IRATTARI_TETELSZAM_SORREND=1
	BEGIN
		IF @IrattariJel = ''L''
			SET @MegorzesiIdo = ''0''
	END
	ELSE IF @IRATTARI_TETELSZAM_SORREND=2
	BEGIN
		IF @IrattariJel = ''L''
		BEGIN
			SET @MegorzesiIdo = ''NS'' + @separator + @MegorzesiIdo
		END
		ELSE IF @IrattariJel = ''N''
		BEGIN
			SET @MegorzesiIdo = ''NS'' + @separator + @MegorzesiIdo
		END
	END


	IF (@IRATTARI_TETELSZAM_SORREND = ''0'')
	BEGIN
		SET @ret = @AgazatiJelek_Kod + @separator + @IrattariTetelszam + @separator + @MegorzesiIdo
	END	
	ELSE IF (@IRATTARI_TETELSZAM_SORREND = ''1'')
	BEGIN
		SET @ret = @IrattariTetelszam + @separator + @AgazatiJelek_Kod + @separator + @MegorzesiIdo
	END
	ELSE IF (@IRATTARI_TETELSZAM_SORREND = ''2'')
	BEGIN
	    --2008, 2009-es irattári jeleknél az ágazati jel megjelenítése
	    if ISNUMERIC(@IrattariTetelszam) = 1
			SET @IrattariTetelszam = LEFT(@AgazatiJelek_Kod, 1) + @IrattariTetelszam
			
		SET @ret = @IrattariTetelszam + @separator + @MegorzesiIdo
	END
	-- BLG_549
	ELSE IF (@IRATTARI_TETELSZAM_SORREND = ''3'')
	BEGIN	
		SET @ret = @IrattariTetelszam + @separator + @MegorzesiIdo
	END
	ELSE
	BEGIN
		SET @ret = @AgazatiJelek_Kod + @separator + @IrattariTetelszam + @separator + @MegorzesiIdo
	END

RETURN @ret
END

' 
END

GO
