IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_ReplaceSpecialChars]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_ReplaceSpecialChars]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_ReplaceSpecialChars]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_ReplaceSpecialChars]
(	
	@str Nvarchar(400) = null
)
RETURNS nvarchar(400) 
AS
BEGIN

declare @result nvarchar(100)

if @str is not null
begin
	set @result = ''''
	declare @ind int
	set @ind = 1

	while @ind <= len(@str)
	begin
		set @result = (select @result +
			case substring(@str, @ind, 1) collate Hungarian_CS_AS
				when ''o'' then ''o''
				when ''O'' then ''O''
				when ''u'' then ''u''
				when ''U'' then ''U''
				when ''ö'' then ''o''
				when ''Ö'' then ''O''
				when ''ü'' then ''u''
				when ''Ü'' then ''U''
				when ''ó'' then ''o''
				when ''Ó'' then ''O''
				when ''ú'' then ''u''
				when ''Ú'' then ''U''
				when ''í'' then ''i''
				when ''Í'' then ''I''
				when ''á'' then ''a''
				when ''Á'' then ''A''
				when ''é'' then ''e''
				when ''É'' then ''E''
				else substring(@str, @ind, 1) end)
		set @ind = @ind + 1
	end

end

RETURN @result
END

' 
END

GO
