IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_WF_GetElozmenyFeladat]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_WF_GetElozmenyFeladat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_WF_GetElozmenyFeladat]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_WF_GetElozmenyFeladat]
		(
		 @Obj_Id               uniqueidentifier,
         @ElozmenyKod          nvarchar(100),
		 @Obj_MetaDefinicio_Id uniqueidentifier,
		 @ObjStateValue_Id     uniqueidentifier
		)

RETURNS @Result_Feladat  TABLE
        (Elozmeny_Id      uniqueidentifier,
         Elozmeny_Leiras  nvarchar(400) )
AS
/*------------------------------------------------------------------------------
Funkció: Lezáratlan eseményben érintett objektum elozmény eseményéhez generált feladat
         azonosítójának lekérdezése, az objektum és az elozmény típus ismeretében.

Paraméterek:
         @Obj_Id                - az eseményben érintett objektum azonosítója
         @ElozmenyKod           - az elozmény esemény funkciókódja
		 @Obj_MetaDefinicio_Id  - metadefinício azonosító
		 @ObjStateValue_Id      - objektum állapot azonosító

Kimenet: a határidos feladat adatai
Példa: 
  select * from dbo.fn_WF_GetElozmenyFeladat
   (''BDAAFB99-5FB6-DD11-ACF3-005056C00008'',''IraIratAlairoNew'',''7BF96899-EA8A-DD11-8549-005056C00008'',''D42263F2-D5B0-DD11-ACF3-005056C00008'') HataridosFeladat
   (''BA65912D-DFB0-DD11-ACF3-005056C00008'',''IraIratAlairoNew'',NULL,NULL) HataridosFeladat
--------------------------------------------------------------------------------*/ 
BEGIN

DECLARE @Elozmeny_Id     uniqueIdentifier,
        @Elozmeny_Leiras nvarchar(400)
select @Elozmeny_Id     = NULL,
	   @Elozmeny_Leiras = NULL

-- elozmény feladat lekérdezés
select @Elozmeny_Id     = hf.Id,
       @Elozmeny_Leiras = hf.Leiras
  from EREC_HataridosFeladatok hf
	   inner join KRT_Esemenyek es on es.Obj_Id = @Obj_Id
             and es.Id = hf.Esemeny_Id_Kivalto
	   inner join EREC_FeladatDefinicio fd on fd.Id = hf.FeladatDefinicio_Id
		     and isnull(fd.Obj_Metadefinicio_Id,fd.Id) = isnull(@Obj_Metadefinicio_Id,fd.Id)
		     and isnull(fd.ObjStateValue_Id,fd.Id)     = isnull(@ObjStateValue_Id,fd.Id)
	   inner join KRT_Funkciok fn on fn.Id = fd.Funkcio_Id_Kivalto
             and fn.Kod = @ElozmenyKod
where hf.Allapot in (''0'',''1'',''2'') -- új, nyitott, folyamatban

-- kimenet
insert @Result_Feladat
select @Elozmeny_Id, @Elozmeny_Leiras

RETURN

END

' 
END

GO
