IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_WF_GetFelelos]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_WF_GetFelelos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_WF_GetFelelos]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_WF_GetFelelos]
		(
		@Csoport_Id      uniqueidentifier,
        @Ugyirat_Id      uniqueidentifier
		)
RETURNS @Result_Felelos  TABLE
        (Csoport_Id_Felelos      uniqueIdentifier,
         Csoport_Id_FelelosFelh  uniqueIdentifier)
AS
/*------------------------------------------------------------------------------
Funkció: Felelos szervezet és felelos felhasználó meghatározása (opcionálisan adott
         ügyirat szerint) a megadott csoportazonosító alapján.

Paraméterek:
         @Csoport_Id    - csoportazonosító
         @Ugyirat_Id    - az ügyirat azonosítója

Kimenet: a felelos csoport és a felelos azonosítója
Példa:
  select * from dbo.fn_WF_GetFelelos
   (''EE8E3C7F-6866-455C-8622-D2B27933C8B1'',NULL)
  select * from dbo.fn_WF_GetFelelos
   (''1B6763A5-A8EA-4A9F-A44D-1D1F9A60E0D7'',NULL)
  select * from dbo.fn_WF_GetFelelos
   (''1B6763A5-A8EA-4A9F-A44D-1D1F9A60E0D7'',''E4EC511C-5E2D-4B9E-9B1D-0004EF84CE5C'')

select * from KRT_Csoportok where Nev like ''Heté%''
select * from fn_GetCsoportTagokAll(''5B7FEE68-CC7A-42D5-9D58-95794E5129F4'')
select c.* from fn_GetCsoportTagokAll(''54828F90-7446-403D-9444-CED21D5F46BF'') a,
         KRT_Csoportok c where a.Id = c.Id --and Tipus = ''0''
 order by Tipus, Nev
select c.* from fn_GetCsoportTagokAll(''E1422663-5C54-41D5-8C97-D401CF471C5D'') a,
         KRT_Csoportok c where a.Id = c.Id --and Tipus = ''0''
 order by Tipus, Nev
--
select top 1 * from EREC_UgyUgyiratok where LetrehozasIdo > ''2008.12.01''

select * from KRT_Csoportok where Id = ''54828F90-7446-403D-9444-CED21D5F46BF''
select * from KRT_Csoportok where Id = ''E1422663-5C54-41D5-8C97-D401CF471C5D''
select * from KRT_CsoportTagok where Csoport_Id_jogalany = ''54828F90-7446-403D-9444-CED21D5F46BF''
--------------------------------------------------------------------------------*/ 
BEGIN

DECLARE @Result_Csoport_Id      uniqueidentifier,
        @Result_Csoport_Id_Felh uniqueidentifier,
		@CsoportTipus           nvarchar(64)

select @CsoportTipus = Tipus
  from KRT_Csoportok
 where Id = @Csoport_Id

select @Result_Csoport_Id      = case when @CsoportTipus = ''1'' then NULL
									  else @Csoport_Id end,
       @Result_Csoport_Id_Felh = case when @CsoportTipus <> ''1'' then NULL
									  else @Csoport_Id end

-- csoport vezetoje
if @CsoportTipus <> ''1''
	select top 1 @Result_Csoport_Id_Felh = Csoport_Id_Jogalany
	  from KRT_CsoportTagok ct
	 where Csoport_Id = @Csoport_Id
	   and Tipus  in(''3'',''2'')
	   and dbo.fn_Ervenyes(ct.ErvKezd,ct.ErvVege)=''I''
	order by Tipus desc

-- felhasználó csoportja
if @CsoportTipus = ''1''
	select top 1 @Result_Csoport_Id = cs.Id
	  from KRT_CsoportTagok ct,
		   KRT_Csoportok cs
	 where ct.Csoport_Id_Jogalany = @Csoport_Id
	   and dbo.fn_Ervenyes(ct.ErvKezd,ct.ErvVege)=''I''
	   and ct.Tipus in(''2'',''3'')
	   and ct.Csoport_Id = cs.Id
	   and cs.Tipus <> ''1''
	   and dbo.fn_Ervenyes(cs.ErvKezd,cs.ErvVege)=''I''
	 order by cs.Tipus, ct.Tipus, cs.Nev

insert @Result_Felelos
select @Result_Csoport_Id, @Result_Csoport_Id_Felh

RETURN

END

' 
END

GO
