IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_WF_GetFelhasznaloCsoport]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_WF_GetFelhasznaloCsoport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_WF_GetFelhasznaloCsoport]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_WF_GetFelhasznaloCsoport]
		(
		@Felhasznalo_Id      uniqueidentifier
		)
RETURNS TABLE
AS
/*------------------------------------------------------------------------------
Funkció: Felhasználó alapértelmezett szervezetének meghatározása.

Paraméterek:
         @Felhasznalo_Id - felasználó azonosító

Kimenet: a csoport és a felhasználó azonosítója
--------------------------------------------------------------------------------*/ 
RETURN
(
	select ct.Csoport_Id,
		   ct.Csoport_Id_Jogalany as Felhasznalo_Id,
		   count(*) as Db
	  from KRT_CsoportTagok ct,
		   KRT_Csoportok cs
	 where ct.Csoport_Id = cs.Id
	   and dbo.fn_Ervenyes(ct.ErvKezd,ct.ErvVege)=''I''
	   and ct.Tipus in(''2'',''3'')
	   and cs.Tipus <> ''1''
	   and dbo.fn_Ervenyes(cs.ErvKezd,cs.ErvVege)=''I''
	   and (@Felhasznalo_Id is NULL or
			@Felhasznalo_Id is not NULL	and	ct.Csoport_Id_Jogalany = @Felhasznalo_Id)
	group by ct.Csoport_Id, ct.Csoport_Id_Jogalany
	having count(*) = 1
	union all
	select (select Csoport_Id_Felelos from dbo.fn_WF_GetFelelos(mc.Csoport_Id_Jogalany, NULL)),
		   mc.Csoport_Id_Jogalany, mc.Db
	  from (select ct.Csoport_Id_Jogalany, count(*) Db
			  from KRT_CsoportTagok ct,
				   KRT_Csoportok cs
			 where ct.Csoport_Id = cs.Id
			   and dbo.fn_Ervenyes(ct.ErvKezd,ct.ErvVege)=''I''
			   and ct.Tipus in(''2'',''3'')
			   and cs.Tipus <> ''1''
			   and dbo.fn_Ervenyes(cs.ErvKezd,cs.ErvVege)=''I''
			   and (@Felhasznalo_Id is NULL or
					@Felhasznalo_Id is not NULL	and	ct.Csoport_Id_Jogalany = @Felhasznalo_Id)
			group by ct.Csoport_Id, ct.Csoport_Id_Jogalany
			having count(*) > 1) mc
)

' 
END

GO
