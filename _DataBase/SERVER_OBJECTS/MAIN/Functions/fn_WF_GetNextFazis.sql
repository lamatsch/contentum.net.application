IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_WF_GetNextFazis]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_WF_GetNextFazis]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_WF_GetNextFazis]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_WF_GetNextFazis]
		(
		 @WorkFlowFazis_Id   uniqueidentifier,
         @FazisKimenet       nvarchar(64)
		)

RETURNS TABLE
AS
/*------------------------------------------------------------------------------
Funkció: Adott állapotból új állapotba való átmenetnél a lehetséges állapotok
         adatainak lekérdezése.

Paraméterek:
         @WorkFlowFazis_Id  - az aktuális workflow állapot azonosítója
         @FazisKimenet      - az aktuális állapot kimenete (0-Negatív,1-Pozitív)

Kimenet: a lehetséges állapot átmenetek
Példa: 
--
declare @Id uniqueidentifier
select @Id = Id from EREC_WorkFlowFazis where Ertek = ''Szakmai koordináció alatt''
--select @Id Id
select * from dbo.fn_WF_GetNextFazis(@Id,NULL)
 order by FazisKimenet, Kod
--------------------------------------------------------------------------------*/ 
RETURN
(
select isnull(btf.Id,bot.Id)       as Id,
	   isnull(btf.Tipus,bot.Tipus) as Tipus,
	   isnull(btf.Kod,bot.Kod)     as Kod,
	   isnull(btf.Ertek,bot.Ertek) as Ertek,
	   isnull(btf.Nev,bot.Nev)     as Nev,
	   case when btf.Id is not null then bot.Id    else NULL end as Belso_Id,
	   case when btf.Id is not null then bot.Tipus else NULL end as BelsoTipus,
	   case when btf.Id is not null then bot.Kod   else NULL end as BelsoKod,
	   case when btf.Id is not null then bot.Ertek else NULL end as BelsoErtek,
	   case when btf.Id is not null then bot.Nev   else NULL end as BelsoNev,
	   fat.FazisKimenet
  from EREC_WorkFlowFazisAtmenet fat
	   inner join EREC_WorkFlowFazis bot on fat.BealloFazis_Id = bot.Id
	   left outer join EREC_WorkFlowFazis btf on bot.Fazis_Id = btf.Id
 where fat.BealltFazis_Id = @WorkFlowFazis_Id
   and fat.FazisKimenet   = isnull(@FazisKimenet,fat.FazisKimenet)
)

' 
END

GO
