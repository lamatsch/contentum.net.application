IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_barkod_checksum_add]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_barkod_checksum_add]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_barkod_checksum_add]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_barkod_checksum_add] (@barkod nvarchar(100),@cheksumVektor NVARCHAR(12))
returns nvarchar(100)
/*
feladata, hogy a bárkód elso 12-számjegye alapján megképezze a 13.számjegyet,
azaz a cheksum -ot, s ezzel együtt visszaadja a teljes bárkódot
természetesen elobb ellenorzi, hogy az inputként megadott bárkód "alap"
12-hosszúságú, csupa számjegybol álló string-e?

returns   --  teljes bárkód = ha OK
              NULL          = ha hibás az input (bárkód "alap")

-- hívás példa:
select dbo.fn_barkod_checksum_add(NULL)             --> NULL
go
select dbo.fn_barkod_checksum_add('''')               --> NULL
go
select dbo.fn_barkod_checksum_add(''1234'')           --> NULL
go
select dbo.fn_barkod_checksum_add(''123456789A12'')   --> NULL
go
select dbo.fn_barkod_checksum_add(''123456789014'')   --> ''1234567890140''
go
select dbo.fn_barkod_checksum_add(''123456789012'')   --> ''1234567890122''
go

*/
AS
BEGIN

  declare @cs         integer

  -- cheksum kalkulálás  
  select @cs = dbo.fn_barkod_checksum_calc(@barkod,@cheksumVektor)

  if @cs < 0
     return NULL

  return @barkod + convert(nvarchar(1), @cs)

END

' 
END

GO
