IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_barkod_checksum_addn]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_barkod_checksum_addn]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_barkod_checksum_addn]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_barkod_checksum_addn] (@barkod numeric(12), @cheksumVektor NVARCHAR(12) )
returns nvarchar(100)
/*
feladata, hogy a bárkód elso 12-számjegye alapján megképezze a 13.számjegyet,
azaz a cheksum -ot, s ezzel együtt visszaadja a teljes bárkódot,
az inputként megadott bárkód "alap" ez esetben egy numeric(12) típusú szám!!
mj: ez egy belso fv nekem, C#-bol nem hívják ...

returns   --  teljes bárkód = ha OK
              NULL          = ha hibás az input (bárkód "alap") (pl. NULL)

-- hívás példa:
select dbo.fn_barkod_checksum_addn(NULL)            --> NULL
go
select dbo.fn_barkod_checksum_addn(0)               --> ''0000000000000''
go
select dbo.fn_barkod_checksum_addn(123456789014)    --> ''1234567890140''
go
select dbo.fn_barkod_checksum_addn(123456789012)    --> ''1234567890122''
go

*/
AS
BEGIN

  declare @cs         integer
  declare @barkodc    nvarchar(100)
  
  select  @barkodc = right( replicate(''0'',12)+convert(varchar(12),@barkod) , 12)
  -- cheksum kalkulálás  
  select @cs = dbo.fn_barkod_checksum_calc( @barkodc, @cheksumVektor )

  if @cs < 0
     return NULL

  return @barkodc + convert(nvarchar(1), @cs)

END

' 
END

GO
