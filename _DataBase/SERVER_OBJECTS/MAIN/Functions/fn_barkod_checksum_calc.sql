IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_barkod_checksum_calc]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_barkod_checksum_calc]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_barkod_checksum_calc]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_barkod_checksum_calc] (@barkod nvarchar(100), @cheksumVektor NVARCHAR(12))
returns integer
/*
feladata a cheksum kiszámítása azaz, hogy
a bárkód elso 12-számjegye alapján megképezze és visszaadja azt,
természetesen elobb ellenorzi, hogy az inputként megadott bárkód "alap"
12-hosszúságú, csupa számjegybol álló string-e?

returns cheksum (0-9) = ha OK
                   -1 = ha NULL-string
                   -2 = ha nem 12-hosszúságú string
                   -3 = ha nem csak számjegyekbol álló 12-hosszúságú string

FONTOS: a fn_barkod_checksum_calc egy alapfüggvény,
        csak ez tartalmazza a checksum-számításának algoritmusát!!!!!

-- hívás példa:
select dbo.fn_barkod_checksum_calc(NULL)            --> -1
go
select dbo.fn_barkod_checksum_calc('''')              --> -2
go
select dbo.fn_barkod_checksum_calc(''1234'')          --> -2
go
select dbo.fn_barkod_checksum_calc(''123456789A12'')  --> -3
go
select dbo.fn_barkod_checksum_calc(''123456789014'')  --> 0
go
select dbo.fn_barkod_checksum_calc(''123456789012'')  --> 2
go

*/
AS
BEGIN

  declare @vektor     nvarchar(12)   -- vektor a checksum számításához
  SET @vektor = @cheksumVektor
  
  declare @i          integer        -- bárkód pozició (1-13)
  declare @cs         integer        -- az 1-12 poziciók alapján számított cheksum
  
  -- ellenorzések:
  if @barkod is NULL
     return -1
     
  if len(@barkod) <> 12
     return -2
     
  if @barkod like ''%[^0-9]%''
     return -3
  
  select @i = 1, @cs = 0   ------ cheksum meghatározás
  while  @i < 13
  begin
    select @cs = @cs + convert(integer, substring(@barkod,@i,1))*convert(integer, substring(@vektor,@i,1))
    select @i  = @i  + 1
  end
  select @cs = 10 - (@cs % 10) -- LEHETNE egyszeruen @cs = (10-@cs)%10 is
  if @cs = 10                  -- de 2-lépésben jobban látszik a leírt szabály ...
     select @cs = 0 -- !!!!

  return @cs

END

' 
END

GO
