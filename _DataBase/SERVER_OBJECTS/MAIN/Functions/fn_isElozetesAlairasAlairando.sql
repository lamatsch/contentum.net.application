IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_isElozetesAlairasAlairando]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_isElozetesAlairasAlairando]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_isElozetesAlairasAlairando]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fn_isElozetesAlairasAlairando] (@RecordId UNIQUEIDENTIFIER)

RETURNS int
AS
-- ============================================
-- Author:		Nagy Csaba
-- Create date: 2017.02.08
-- Description:	A megadott RecordId-ra az 
--   EREC_IratAlairok táblából vissza adja,
--   hogy van-e előzetes aláírás, ami aláírandó.
-- Returns : 0 - ha van
--			 1 - ha nincs 
-- =============================================
BEGIN	
	
	
	DECLARE @rowcount int,
			@AlairasSorrend int,
			@AlairandoObj_Id UNIQUEIDENTIFIER

	-- Kivenni a sorrendet
	select @AlairasSorrend = AlairasSorrend,
	@AlairandoObj_Id = Obj_Id
	 from EREC_IratAlairok
	where Id  = @RecordId
	--Megnézni hogy van e még előzetes aláírandó aláírás
	select @rowcount = count(*)
		  from EREC_IratAlairok
		 where Obj_Id         = @AlairandoObj_Id
		   and Allapot        = ''1'' -- Aláírandó
		   and AlairasSorrend < @AlairasSorrend
		   and dbo.fn_Ervenyes(ErvKezd,ErvVege)=''I''		
			if @rowcount > 0		
			return 0					
	return 1
END

' 
END

GO
