IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_kesobbi_munkaidopont]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_kesobbi_munkaidopont]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_kesobbi_munkaidopont]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_kesobbi_munkaidopont] 
      (@kezdeti_idopont    datetime     = null, 
       @eltolas_ora_perc   varchar(10)  = null)
returns datetime

/*
Paraméterek:
   @kezdeti_idopont -- 
      egy konkrét idopont (date & time), ahonnan "ketyeg" az ido
      pl. ''2007.12.03 14:33'' vagy 
      pl. getdate() azaz MOST, de
      pl. a NULL is a MOST -nak felelne meg;
      a konkrét idopontnak nem feltétlenül kell munkaidopontnak lennie,
      ilyenkor a következo munkanap kezdo idopontjaként értelmezzük!!
                       
   @eltolas_ora_perc -- 
      egy "eltolási" érték, ahány órával és perccel (tiszta munkaido!) 
      késobbi munkaidopontot szeretnénk meghatározni,
      az eltolást ''[+/-]hhhh:mm'' formában kell megadni,
      pl. ''3:14'' ==> 3 óra 14 perc VAGY ''2'' ==> 2 óra VAGY '':5'' ==> 5 perc ...
      korlátként csak a 0 <= óra rész <= 9999, 0 <= perc rész <= 59 van eloírva,
      pl. ''15:59'' ==> 15 óra 59 perc VAGY ''16:00'' ==> 16 óra
      a negatív elojel pedig az idoben való visszafelé haladást jelenti, azaz egy korábbi munkaidopontot
      pl. ''-16:00'' ==> -16 óra, azaz pont 2 "munkanappal" korábban ...
      a NULL érték pedig default módon 0 óra 0 percet jelent, de
      pl. elfogadjuk az '''' ''+'' ''-'' ''+:'' ''-:'' '':'' értékeket is, s ezek is mind  0 óra 0 percet jelentenek!


Kikötések:
----------
      Minden munkanap EGYFORMA!!!!!  
      
      azaz minden munkanap
      @munkanap_kezd = "hh:mm" -kor kezdodik és folyamatosan haladva egészen      
      @munkanap_vege = "hh:mm" -ig  tart
      ahol  "00:00" <= @munkanap_kezd < @munkanap_vege <= "23:59"
      a napi munkaido pedig (@munkanap_vege - @munkanap_kezd) terjedelmu (óra:perc)
      
      a fenti két "paraméter", 
      a függvényben jelenleg "konstans"-ként szerepel,
      tehát csak újrafordítás után változtatható !!!!!!!!!!!
      
      a "paraméterek" konkrét értékei esetünkben:
      @munkanap_kezd = "08:00"
      @munkanap_vege = "16:00"
      s így a napi munkaido pontosan 8 óra!


-- hívás példa:
select dbo.fn_kesobbi_munkaidopont(NULL,NULL)       -- az akt.idopontot adja vissza, ha MOST épp munkaidoben vagyunk
go                                                  -- a köv.munkanap kezdoidopontját, ha MOST épp munkaidon kívül vagyunk

select dbo.fn_kesobbi_munkaidopont(getdate(),''0:0'') -- az akt.idopontot adja vissza, ha MOST épp munkaidoben vagyunk
go                                                  -- a köv.munkanap kezdoidopontját, ha MOST épp munkaidon kívül vagyunk

-- 252 munkanap (volt 2006-ban!) *8 == 2016 munkaóra => az eredménynek az utolsó munkanap záróidopontját kell adni!! 
select dbo.fn_kesobbi_munkaidopont(''2006.01.01'', ''2016:00'') -- ''2006-12-29 16:00:00.000'' -- OK
-- de 2016 óra 1 perc eltolásnak, már a következo év elso munkanapjának elso munkaperce végére :)
select dbo.fn_kesobbi_munkaidopont(''2006.01.01'', ''2016:01'') -- ''2007-01-02 08:01:00.000'' -- OK
go

-- 255 munkanap (volt 2005-ban!) *8 == 2040 munkaóra => az eredménynek az utolsó munkanap záróidopontját kell adni!! 
select dbo.fn_kesobbi_munkaidopont(''2005.01.01'', ''2040:00'') -- ''2005-12-30 16:00:00.000'' -- OK
-- de 2040 óra 1 perc eltolásnak, már a következo év elso munkanapjának elso munkaperce végére :)
select dbo.fn_kesobbi_munkaidopont(''2005.01.01'', ''2040:01'') -- ''2006-01-02 08:01:00.000'' -- OK
go


-- vagy egy példa a visszafelé irányra ...
select dbo.fn_kesobbi_munkaidopont(''2007.12.26'', ''-4:00'')   -- ''2007-12-22 12:00:00.000'' -- OK
go

      
*/

AS
BEGIN
  
  ---------------------------------------------
  -- KONSTANSOK -------------------------------
  ---------------------------------------------
  -- NAPI munka- kezdés és befejezés idopontjai
  ---------------------------------------------
  declare @munkanap_kezd             varchar(10)
  declare @munkanap_vege             varchar(10)
  --
  select  @munkanap_kezd        =    ''08:00''  -- NAGYON FONTOS!
  select  @munkanap_vege        =    ''16:00''  -- NAGYON FONTOS!
  ---------------------------------------------
  ---------------------------------------------

  ---------------------------------------------
  -- MUNKAVÁLTOZÓK (a konstansok kezeléséhez)
  declare @napi_munkaido_ora         integer
  declare @napi_munkaido_perc        integer
  declare @napi_munkaido_percben     integer

  -- MUNKAVÁLTOZÓ  (az eredmény visszaadásához, RETURN érték)
  declare @kesobbi_idopont           datetime

  -- MUNKAVÁLTOZÓK (az eltolási paraméter kezeléséhez)
  -- a szétbontásához
  declare @elt_EJ_pozicio            integer      -- @eltolas_ora_perc -ben az elojel [+-] poziciója
  declare @elt_KP_pozicio            integer      -- @eltolas_ora_perc -ben az kettospont [:] poziciója
  declare @elt_param_hossz           integer      -- @eltolas_ora_perc string hosszúsága (karakterben)
  -- a szétbontott részek tárolásához
  declare @elt_elojel_char           varchar(1)
  declare @elt_ora_char              varchar(10)
  declare @elt_perc_char             varchar(10)
  -- a szétbontott részek számmá konvertáláshoz
  declare @elt_elojel                integer
  declare @elt_ora                   integer
  declare @elt_perc                  integer
  declare @elt_percben               integer
  -- az eltolási érték tényleges munkanap, munkaóra, munkaperc -re való transzformálásához
  declare @elt_munkanap              integer
  declare @elt_munkaora              integer
  declare @elt_munkaperc             integer
    
  -- MUNKAVÁLTOZÓK (egyéb célra)
  declare @hiba_sorszam              integer
  
  ---------------------------------------------
  ---------------------------------------------
  -- ELLENORZÉSEK
  ---------------------------------------------
  ---------------------------------------------

  ---------------------------------------
  -- napi munkaido konstansok ellenorzése
  ---------------------------------------
  if @munkanap_kezd not like ''[0-9][0-9]:[0-9][0-9]''
     OR substring(@munkanap_kezd,1,2) not between ''00'' and ''24''
     OR substring(@munkanap_kezd,4,2) not between ''00'' and ''59''
  begin
     select @hiba_sorszam = 1 -- hibás konstans: @munkanap_kezd!
     goto hiba_kezeles
  end   
     
  if @munkanap_vege not like ''[0-9][0-9]:[0-9][0-9]''
     OR substring(@munkanap_vege,1,2) not between ''00'' and ''24''
     OR substring(@munkanap_vege,4,2) not between ''00'' and ''59''
  begin
     select @hiba_sorszam = 2 -- hibás konstans: @munkanap_vege!
     goto hiba_kezeles
  end   

  if @munkanap_kezd >= @munkanap_vege
  begin
     select @hiba_sorszam = 3 -- hibás konstans: @munkanap_kezd >= @munkanap_vege!
     goto hiba_kezeles
  end   

  -- a napi munkaido kalkulálása:
  select @napi_munkaido_percben = 
         ( convert(int, substring(@munkanap_vege,1,2))*60 + convert(int, substring(@munkanap_vege,4,2)) ) -
         ( convert(int, substring(@munkanap_kezd,1,2))*60 + convert(int, substring(@munkanap_kezd,4,2)) )
  
  select @napi_munkaido_ora  = @napi_munkaido_percben / 60
  select @napi_munkaido_perc = @napi_munkaido_percben % 60


  ------------------------------------------
  -- input paraméterek ellenorzése, kezelése
  ------------------------------------------
  --
  -- @kezdeti_idopont paraméter napra & óra:percre truncate-olása
  --
  select @kezdeti_idopont = isnull(@kezdeti_idopont,getdate())                        -- NULL-kezelés default módon
  select @kezdeti_idopont = convert(datetime, convert(varchar,@kezdeti_idopont,100))  -- mp és ezred mp csonkolás

  --
  -- @eltolas_ora_perc paraméter vizsgálata
  --   
  select @eltolas_ora_perc = isnull(@eltolas_ora_perc, ''00:00'')  -- NULL-kezelés default módon
  select @eltolas_ora_perc = ltrim(rtrim(@eltolas_ora_perc))

  if patindex(''%[+-]%'',@eltolas_ora_perc) = 0  -- ha nincs benne elojel teszünk az elejére egy "+" -t!
     select @eltolas_ora_perc = ''+'' + @eltolas_ora_perc

  if patindex(''%[:]%'',@eltolas_ora_perc) = 0  -- ha nincs benne kettospont teszünk a végére egy ":" -t!
     select @eltolas_ora_perc = @eltolas_ora_perc + '':'' 

  -- állapítsuk meg az elojel és a kettospont pozicióit! (most már biztos van benne ...)
  select  @elt_EJ_pozicio  = patindex(''%[+-]%'',@eltolas_ora_perc)
  select  @elt_KP_pozicio  = patindex(''%[:]%'', @eltolas_ora_perc)
  select  @elt_param_hossz = len(@eltolas_ora_perc)

  -- akkor jön az esetleges hibák kiszurése ...
  if @elt_EJ_pozicio <> 1
  begin
     select @hiba_sorszam = 4 -- hibás paraméter: @eltolas_ora_perc! (az elojel nem az elso pozición áll)
     goto hiba_kezeles
  end   

  select @elt_elojel_char = substring(@eltolas_ora_perc, @elt_EJ_pozicio, 1)

  -- óra és perc rész leválasztása
  select @elt_ora_char  = substring(@eltolas_ora_perc, @elt_EJ_pozicio + 1, @elt_KP_pozicio - @elt_EJ_pozicio -1 )
  select @elt_perc_char = substring(@eltolas_ora_perc, @elt_KP_pozicio + 1, @elt_param_hossz - @elt_KP_pozicio)

  if isnull(@elt_ora_char,'''') = ''''
     select @elt_ora_char  = ''0''

  if isnull(@elt_perc_char,'''') = ''''
     select @elt_perc_char  = ''0''

  if @elt_ora_char like ''%[^0-9]%''
  begin
     select @hiba_sorszam = 5 -- hibás paraméter: @eltolas_ora_perc! (az óra részben nem csak számjegy áll)
     goto hiba_kezeles
  end   
  
  if @elt_perc_char like ''%[^0-9]%''
  begin
     select @hiba_sorszam = 6 -- hibás paraméter: @eltolas_ora_perc! (a perc részben nem csak számjegy áll)
     goto hiba_kezeles
  end   

  -- óra és perc és elojel értékek numerikussá konvertálása 
  select @elt_ora  = convert(integer, @elt_ora_char)
  select @elt_perc = convert(integer, @elt_perc_char)
  
  select @elt_elojel = 1
  if @elt_elojel_char = ''-''
     select @elt_elojel = -1
  if @elt_ora=0 AND @elt_perc=0
     select @elt_elojel = 0
     
  -- itt még le kéne ellenorízni, hogy @elt_ora és @elt_perc értékei nem túl nagyok-e!
  if @elt_ora not between 0 and 9999
  begin
     select @hiba_sorszam = 7 -- hibás paraméter: @eltolas_ora_perc! (az óra rész túl nagy)
     goto hiba_kezeles
  end   

  if @elt_perc not between 0 and 59
  begin
     select @hiba_sorszam = 8 -- hibás paraméter: @eltolas_ora_perc! (az perc rész túl nagy)
     goto hiba_kezeles
  end   

  -- eltolási érték kiszámítása percben
  select @elt_percben = @elt_ora*60 + @elt_perc 

  ------------------------------------------------
  -- AKKOR, fogjunk bele a tényleges "ELTOLÁS"-ba!
  ------------------------------------------------
  --
  -- 1. elöször is a kezdeti idopontot 
  --    mindenképp helyezzük a "legközelebbi" értelmes munkaidopontra,
  --    ha az eleve nem munkaidopont lenne!!!
  
  -- a @kezdeti_idopont munkanapra ÉS munkaórákra esik?
  if dbo.fn_munkanap(@kezdeti_idopont) = 1 AND
     convert(varchar(5),@kezdeti_idopont,108) between @munkanap_kezd and @munkanap_vege  -- pl: ''13:48'' between ''08:00'' and ''16:00''    
  begin
     select @kesobbi_idopont = @kezdeti_idopont 
  end

  -- a @kezdeti_idopont munkanapra DE munkaórák elottre esik?
  if dbo.fn_munkanap(@kezdeti_idopont) = 1 AND
     convert(varchar(5),@kezdeti_idopont,108) < @munkanap_kezd  -- pl: ''06:30'' < ''08:00''
  begin
     if @elt_elojel >= 0
        -- az adott munkanap kezdoidopontja legyen
        select @kesobbi_idopont = convert(datetime, convert(varchar,@kezdeti_idopont,102)+'' ''+@munkanap_kezd)
     else   
        -- az elozo munkanap végzoidopontja legyen
        select @kesobbi_idopont = convert(datetime, convert(varchar,dbo.fn_kovetkezo_munkanap(@kezdeti_idopont,-1),102)+'' ''+@munkanap_vege)
  end

  -- a @kezdeti_idopont munkanapra DE munkaórák utánra esik?
  if dbo.fn_munkanap(@kezdeti_idopont) = 1 AND
     convert(varchar(5),@kezdeti_idopont,108) > @munkanap_vege  -- pl: ''19:30'' > ''16:00''
  begin
     if @elt_elojel >= 0
        -- a következo munkanap kezdoidopontja legyen
        select @kesobbi_idopont = convert(datetime, convert(varchar,dbo.fn_kovetkezo_munkanap(@kezdeti_idopont,+1),102)+'' ''+@munkanap_kezd)
     else   
        -- az adott munkanap végzoidopontja legyen
        select @kesobbi_idopont = convert(datetime, convert(varchar,@kezdeti_idopont,102)+'' ''+@munkanap_vege)
  end

  -- a @kezdeti_idopont nem munkanapra esik?
  if dbo.fn_munkanap(@kezdeti_idopont) = 0
  begin
     if @elt_elojel >= 0
        -- a következo munkanap kezdoidopontja legyen
        select @kesobbi_idopont = convert(datetime, convert(varchar,dbo.fn_kovetkezo_munkanap(@kezdeti_idopont,+1),102)+'' ''+@munkanap_kezd)
     else   
        -- az elozo munkanap végzoidopontja legyen
        select @kesobbi_idopont = convert(datetime, convert(varchar,dbo.fn_kovetkezo_munkanap(@kezdeti_idopont,-1),102)+'' ''+@munkanap_vege)
  end

  -- OK: most már egy "értelmes" kezdeti idoponton állunk!!!  
  --     a "@kesobbi_idopont" változó tartalmazza ...

  --
  -- 2. az eltolási értéket (amit jelenleg @elt_ora & @elt_perc ill. @elt_percben tartalmaz),
  --    bonstsuk fel tényleges munkanap-munkaóra-munkaperc komponensekre!
  --
  select @elt_munkanap  = @elt_percben / @napi_munkaido_percben
  select @elt_munkaperc = @elt_percben % @napi_munkaido_percben
  select @elt_munkaora  = @elt_munkaperc / 60
  select @elt_munkaperc = @elt_munkaperc % 60

  -- MJ: itt ezért egy dologra VIGYÁZNI kell majd!!!
  --     ui pl.  8 óra 0 perc eltolás az ne 1 nap 0 óra 0 perc legyen,
  --     hanem 0 nap 8 óra 0 perc!
  --     mert a napi munkaido intervallum zárt, 
  --     s így ha pl. reggel ''08:00''-kor adnak ki egy pont 8 órás eltolást,
  --     akkor az ''16:00''-kor kell, hogy érvénybe jusson (és nem másnap ''08:00''-kor)

  if @elt_munkaora = 0 AND @elt_munkaperc = 0 AND @elt_munkanap > 0
     select @elt_munkanap  = @elt_munkanap -1,
            @elt_munkaora  = @napi_munkaido_ora,
            @elt_munkaperc = @napi_munkaido_perc
  
  --
  -- 3. jöhet a munkanap, munkaóra, munkaperc -cel való eltolás
  --

  -- munkanap hozzáadása, ha kell! (az óra:perc részt megorzése mellett!!!)
  if @elt_munkanap <> 0
     select @kesobbi_idopont = convert
                                ( 
                                 datetime, 
                                 convert(varchar,dbo.fn_kovetkezo_munkanap(@kesobbi_idopont,@elt_elojel*@elt_munkanap),102)
                                 +'' ''
                                 +convert(varchar(5),@kesobbi_idopont,108) 
                                )

  -- NOS, most ott tartunk, hogy a "@kesobbi_idopont" változó valamely munkanap, valamely munkaidopontját tartalmazza,
  -- amelyet @elt_munkaora & @elt_munkaperc -cel még korrigálni kell, 
  -- de ez az korrekciós érték már legfeljebb egy munkanapnyi lehet!!!

  -- egy pillanatra helyezzük át a bázisunkat mondjuk ''2000.01.02''-re,
  -- hogy egyszerubben tudjunk dateadd()-dal és datediff()-fel számolni ...
  -- nem lenne muszáj, csak félek az esetleges óra átállításoktól, s az január eleje flé nem szokott lenni :)

  -- ezeket a változókat azért deklarálom itt, 
  -- mert csak most fogom munkavátozóként használni oket!!!
  declare @aznapi_idopont     datetime 
  declare @aznapi_kezd        datetime 
  declare @aznapi_vege        datetime 
  --
  declare @kovnapi_idopont    datetime 
  declare @kovnapi_kezd       datetime 
  --
  declare @elonapi_idopont    datetime 
  declare @elonapi_vege       datetime 
  --
  declare @delta              integer   -- idopontok különbsége (percben)
  -- 

  select @aznapi_idopont = convert( datetime, ''2000.01.02''+'' ''+convert(varchar(5),@kesobbi_idopont,108) )  
  select @aznapi_kezd    = convert( datetime, ''2000.01.02''+'' ''+@munkanap_kezd )  
  select @aznapi_vege    = convert( datetime, ''2000.01.02''+'' ''+@munkanap_vege )  
  
  -- munkaóra hozzáadása!
  select @aznapi_idopont = dateadd( HOUR,   @elt_elojel*@elt_munkaora,  @aznapi_idopont)
  -- munkaperc hozzáadása!
  select @aznapi_idopont = dateadd( MINUTE, @elt_elojel*@elt_munkaperc, @aznapi_idopont)
  
  if @aznapi_idopont BETWEEN @aznapi_kezd AND @aznapi_vege
  begin 
     -- OK, a nap már nem változik @kesobbi_idopont -ban, az óra:perc részt pedig átvenni @aznapi_idopont -ból!!!
     select @kesobbi_idopont = CONVERT(DATETIME, convert(varchar,@kesobbi_idopont,102) + '' '' + convert(varchar(5),@aznapi_idopont,108) )
  end
  
  if @aznapi_idopont > @aznapi_vege
  begin 
     -- OK, a következo nap kezdo idopontjához hozzá kell majd adni az (@aznapi_idopont - @aznapi_vege) különbséget!
     select @delta = datediff( MINUTE, @aznapi_vege, @aznapi_idopont)
     select @kovnapi_kezd    = convert( datetime, ''2000.01.03''+'' ''+@munkanap_kezd )  
     select @kovnapi_idopont = dateadd( MINUTE, @delta, @kovnapi_kezd)
     -- VENNI kell @kesobbi_idopont szerinti következo munkanapot, az óra:perc részt pedig átvenni @kovnapi_idopont -ból!!! 
     select @kesobbi_idopont = dbo.fn_kovetkezo_munkanap(@kesobbi_idopont,1)
     select @kesobbi_idopont = CONVERT(DATETIME, convert(varchar,@kesobbi_idopont,102) + '' '' + convert(varchar(5),@kovnapi_idopont,108) )
  end

  if @aznapi_idopont < @aznapi_kezd
  begin 
     -- OK, az elozo nap vég idopontjából le kell majd vonni az (@aznapi_kezd - @aznapi_idopont) különbséget!
     select @delta = datediff( MINUTE, @aznapi_idopont, @aznapi_kezd )
     select @elonapi_vege    = convert( datetime, ''2000.01.01''+'' ''+@munkanap_vege )  
     select @elonapi_idopont = dateadd( MINUTE, -@delta, @elonapi_vege)
     -- VENNI kell @kesobbi_idopont szerinti elozo munkanapot, az óra:perc részt pedig átvenni @elonapi_idopont -ból!!! 
     select @kesobbi_idopont = dbo.fn_kovetkezo_munkanap(@kesobbi_idopont,-1)
     select @kesobbi_idopont = CONVERT(DATETIME, convert(varchar,@kesobbi_idopont,102) + '' '' + convert(varchar(5),@elonapi_idopont,108) )
  end

------------------                                 
-- INNEN FOLYTATNI !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
------------------
-- NOS, szerintem most már KÉSZ ez az alapverzió,
-- csupán csak jól meg kéne tesztelni, meg csinálni rá egy csomó példát ...


  RETURN(@kesobbi_idopont)

-------------  
-------------
hiba_kezeles:  
/*
MJ: ezen a ponton a @hiba_sorszam egy egész számot tartalmaz (1,2,3, ...),
    amely az alábbi hibákat azonosítja:
    
       1 -- hibás konstans: @munkanap_kezd!
       2 -- hibás konstans: @munkanap_vege!
       3 -- hibás konstans: @munkanap_kezd >= @munkanap_vege!
       4 -- hibás paraméter: @eltolas_ora_perc! (az elojel nem az elso pozición áll)
       5 -- hibás paraméter: @eltolas_ora_perc! (az óra részben nem csak számjegy áll)
       6 -- hibás paraméter: @eltolas_ora_perc! (a perc részben nem csak számjegy áll)
       7 -- hibás paraméter: @eltolas_ora_perc! (az óra rész túl nagy)
       8 -- hibás paraméter: @eltolas_ora_perc! (az perc rész túl nagy)

    NOS, szabványosan a függvény bármilyen hiba esetén NULL-t ad vissza,
    de tesztelés céljából, hogy tudjuk mi is az esetleges baj,
    az 1900-as év annyiadik napját adjuk vissza, amekkora a hiba sorszám :)
    TEHÁT pl. 1990.01.08 -at, ha "@eltolas_ora_perc! (az perc rész túl nagy)" ...
*/    
  declare @teszt                     integer
  select  @teszt = 0

  select @kesobbi_idopont = NULL
  
  if @teszt = 1 AND @hiba_sorszam is not NULL
  begin 
     select @kesobbi_idopont =  convert(datetime, ''1900.01.01'')
     select @kesobbi_idopont =  dateadd(DAY, @hiba_sorszam-1, @kesobbi_idopont)
  end   

  RETURN(@kesobbi_idopont)
 
END

' 
END

GO
