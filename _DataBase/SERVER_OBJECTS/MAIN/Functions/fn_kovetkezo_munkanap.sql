IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_kovetkezo_munkanap]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_kovetkezo_munkanap]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_kovetkezo_munkanap]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_kovetkezo_munkanap] (@datum_tol datetime = null, @sorszam int = null)
returns datetime
/*
egy adott naptól (@datum_tol) kezdve meghatározza a valahányadik rákövetkezo munkanapot
ha az adott nap nincs megadva (NULL), akkor az aktuális napot veszi!

returns dátum -- ha @sorszam > 0    ==> az @sorszam-adik következo munkanap
                 ha @sorszam < 0    ==> az |@sorszam|-adik elozo munkanap
                 ha @sorszam = 0    ==> @datum_tol (bemeno dátum)
                 ha @sorszam = NULL ==> az 1.következo munkanap (DEFAULT módon)

figyeli az extra munkanapokat és munkaszüneti napokat is 
az KRT_EXTRA_NAPOK tábla alapján!

-- hívás példa:
select dbo.fn_kovetkezo_munkanap(''2007.10.18'',1) --> ''2007.10.19''
go
select dbo.fn_kovetkezo_munkanap(''2007.10.19'',1) --> ''2007.10.20''
go
select dbo.fn_kovetkezo_munkanap(''2007.10.20'',1) --> ''2007.10.24''
go
select dbo.fn_kovetkezo_munkanap(''2007.10.25'',1) --> ''2007.10.26''
go
select dbo.fn_kovetkezo_munkanap(''2007.10.26'',1) --> ''2007.10.27''
go
select dbo.fn_kovetkezo_munkanap(''2007.10.27'',1) --> ''2007.10.29''
go
select dbo.fn_kovetkezo_munkanap(''2007.10.30'',1) --> ''2007.10.31''
go
select dbo.fn_kovetkezo_munkanap(''2007.10.31'',1) --> ''2007.11.05''
go
select dbo.fn_kovetkezo_munkanap(''2009.09.08'',22) --> ''2009.10.08''
go
select dbo.fn_kovetkezo_munkanap(''2009.09.08'',1) --> ''2009.09.09''
go
select dbo.fn_kovetkezo_munkanap(''2009.09.08'',-22) --> ''2009.08.07''
go
select dbo.fn_kovetkezo_munkanap(getdate(),   1) --> a MA utáni munkanap
select dbo.fn_kovetkezo_munkanap(NULL,        1) --> a MA utáni munkanap -- ha NULL, akkor a megadott nap, az aktuális nap (MA)!
go

*/
AS
BEGIN
  --
  -- mj: egyelore ciklikusan oldjuk meg a feladatot,
  --     addig megyünk, amíg meg nem kapjuk az n. munkanapot ...
  --
  declare @osszes_munka_napszam   int 

  -- napra truncate-olás
  select @datum_tol = isnull(@datum_tol,getdate())
  select @datum_tol = convert(datetime,convert(varchar,@datum_tol,102))
  select @sorszam   = isnull(@sorszam, 1)
  
  if @sorszam = 0
     return (@datum_tol)
     
  ------
  select @osszes_munka_napszam = 0
  while  @osszes_munka_napszam < abs(@sorszam)
  begin
    select @datum_tol = dateadd(day, sign(@sorszam) ,@datum_tol)
    select @osszes_munka_napszam = @osszes_munka_napszam + dbo.fn_munkanap(@datum_tol)
  end
    
  ------
  return (@datum_tol)

END

' 
END

GO
