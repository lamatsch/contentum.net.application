IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_map_IratMetaDefinicio]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_map_IratMetaDefinicio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_map_IratMetaDefinicio]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_map_IratMetaDefinicio] (@IratMetaDefinicio_Id uniqueidentifier, @Org uniqueidentifier)
returns nvarchar(100)
/*
feladata , hogy egy adott irat metadefiníciót olvasható formára alakítson
ágazati jel ügykörkód/ügytípus kód/eljárási szakasz/irattípus alakban,
az elso null értékik haladva a hierarchiában (azaz ha nincs megadva eljárási szakasz,
akkor ágazati jel ügykörkód/ügytípus)
returns:
null ha az irat metadefiníció nem létezik
összefuzött érték

-- hívás példa:
select dbo.fn_map_IratMetaDefinicio(''950B33D2-625C-4E46-B1F2-5C5F67DC883D'',''450B510A-7CAA-46B0-83E3-18445C0C53A9'') --> C 272/01/Osztatlan/Utasítás
go


*/
AS
BEGIN
    declare @result nvarchar(100)

    declare @Ugykor_Id uniqueidentifier
    declare @UgykorKod nvarchar(10)
    declare @Ugytipus nvarchar(64)
    declare @EljarasiSzakasz nvarchar(64)
    declare @Irattipus nvarchar(64)

    if not exists(select Id
                  from EREC_IratMetaDefinicio
                  where Id = @IratMetaDefinicio_Id)
    begin
        set @result = null
    end
    else
    begin
        declare @AgazatiJel nvarchar(20)
        declare @EljarasiSzakasz_Nev nvarchar(100) -- kódtárból
        declare @Irattipus_Nev nvarchar(100) -- kódtárból

        select @Ugykor_Id = Ugykor_Id,
               @UgykorKod = UgykorKod,
               @Ugytipus = Ugytipus,
               @EljarasiSzakasz = EljarasiSzakasz,
               @Irattipus = Irattipus
        from EREC_IratMetaDefinicio
        where Id = @IratMetaDefinicio_Id
        
        if (@Ugykor_Id is not null)
        begin
            set @AgazatiJel = (select aj.Kod from EREC_AgazatiJelek aj where aj.Id = (select iit.AgazatiJel_Id from EREC_IraIrattariTetelek iit where iit.Id = @Ugykor_Id))
            set @result = isnull(@AgazatiJel, ''[?]'') + '' '' + isnull(@UgykorKod, ''[?]'')
            if (@Ugytipus is not null)
            begin
                set @result = @result + ''/'' + @Ugytipus
                if (@EljarasiSzakasz is not null)
                begin
                    set @EljarasiSzakasz_Nev = (select dbo.fn_KodtarErtekNeve(''ELJARASI_SZAKASZ'', @EljarasiSzakasz, @Org))
                    set @result = @result + ''/'' + isnull(@EljarasiSzakasz_Nev, ''['' + @EljarasiSzakasz + '']'')
                    if (@Irattipus is not null)
                    begin
                        set @Irattipus_Nev = (select dbo.fn_KodtarErtekNeve(''IRATTIPUS'', @Irattipus, @Org))
                        set @result = @result + ''/'' + isnull(@Irattipus_Nev, ''['' + @Irattipus + '']'')
                    end
                end
            end
        end
    end

    return @result

END

' 
END

GO
