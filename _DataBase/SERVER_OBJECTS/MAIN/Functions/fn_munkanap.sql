IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_munkanap]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_munkanap]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_munkanap]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_munkanap] (@datum datetime = null)
returns integer
/*
egy adott napról (@datum) megállapítja, hogy munkanap-e?
ha az adott nap nincs megadva (NULL), akkor az aktuális napot veszi!

returns 1 -- ha munkanap
        0 -- ha nem munkanap

figyeli az extra munkanapokat és munkaszüneti napokat is 
az KRT_EXTRA_NAPOK tábla alapján!

-- hívás példa:
declare @datum datetime select @datum=''2007.10.19'' -- péntek   - munkanap
select  convert(varchar,@datum,102),dbo.fn_munkanap_nev(@datum),dbo.fn_munkanap(@datum)
go
declare @datum datetime select @datum=''2007.10.20'' -- szombat  - munkanap!
select  convert(varchar,@datum,102),dbo.fn_munkanap_nev(@datum),dbo.fn_munkanap(@datum)
go
declare @datum datetime select @datum=''2007.10.21'' -- vasárnap - szünnap
select  convert(varchar,@datum,102),dbo.fn_munkanap_nev(@datum),dbo.fn_munkanap(@datum)
go
declare @datum datetime select @datum=''2007.10.22'' -- hétfo    - szünnap!
select  convert(varchar,@datum,102),dbo.fn_munkanap_nev(@datum),dbo.fn_munkanap(@datum)
go
declare @datum datetime select @datum=''2007.10.23'' -- kedd     - szünnap!
select  convert(varchar,@datum,102),dbo.fn_munkanap_nev(@datum),dbo.fn_munkanap(@datum)
go
declare @datum datetime select @datum=''2007.10.24'' -- szerda   - munkanap
select  convert(varchar,@datum,102),dbo.fn_munkanap_nev(@datum),dbo.fn_munkanap(@datum)
go
declare @datum datetime select @datum=getdate()    -- MA       - munkanap (ha már MA itt vagyok :)
select  convert(varchar,@datum,102),dbo.fn_munkanap_nev(@datum),dbo.fn_munkanap(@datum)
select  convert(varchar,@datum,102),dbo.fn_munkanap_nev(NULL),  dbo.fn_munkanap(NULL)   -- ha NULL a megadott nap, akkor annek az aktuális napot vesszük (MA)!
go
*/
AS
BEGIN
  declare @hetkoznap_flag integer
  declare @extranap_flag  integer

  -- napra truncate-olás
  select @datum = isnull(@datum,getdate())
  select @datum = convert(datetime,convert(varchar,@datum,102))

  if ( @@datefirst + DATEPART(dw, @datum) ) % 7   in   (2,3,4,5,6)
     select @hetkoznap_flag = 1
  else   
     select @hetkoznap_flag = 0
     
  if exists( select 1 from KRT_EXTRA_NAPOK where datum = @datum and getdate() between KRT_EXTRA_NAPOK.ErvKezd and KRT_EXTRA_NAPOK.ErvVege)
     select @extranap_flag = 1
  else   
     select @extranap_flag = 0
     
  return (@hetkoznap_flag + @extranap_flag) % 2

END

' 
END

GO
