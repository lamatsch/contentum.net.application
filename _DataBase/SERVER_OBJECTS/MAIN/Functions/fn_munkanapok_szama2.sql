IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_munkanapok_szama2]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fn_munkanapok_szama2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_munkanapok_szama2]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[fn_munkanapok_szama2] (@datum_tol datetime = null, @datum_ig datetime = null)
returns integer
/*
egy adott idoszakra [@datum_tol - @datum_ig) megállapítja, hogy hány munkanap esik bele
ha az elso nap ill. az utolsó nap nincs megadva (NULL), akkor az aktuális napot veszi helyette!

FIGYELEM! a @datum_ig már nem tartozik az idoszakba, tehát alulról zárt, felülrol nyílt az intervallum ...

returns n    -- az adott idoszakba eso munkanapok száma
        0    -- ha @datum_tol = @datum_ig
        NULL -- ha @datum_tol > @datum_ig

figyeli az extra munkanapokat és munkaszüneti napokat is 
az KRT_EXTRA_NAPOK tábla alapján!

-- hívás példa:
select dbo.fn_munkanapok_szama2(''2003.01.01'',''2004.01.01'') -- 253
go
select dbo.fn_munkanapok_szama2(''2004.01.01'',''2005.01.01'') -- 256
go
select dbo.fn_munkanapok_szama2(''2005.01.01'',''2006.01.01'') -- 255
go
select dbo.fn_munkanapok_szama2(''2006.01.01'',''2007.01.01'') -- 252
go
select dbo.fn_munkanapok_szama2(NULL,NULL)                 -- 0
go
select dbo.fn_munkanapok_szama2(''2006.01.02'',''2006.01.01'') -- NULL
go

*/
AS
BEGIN
  --
  -- mj: természetesen ciklikusan is megoldható a feladat, tehát úgy, 
  --     hogy az intervallum minden napjáról külön-külön lekérdezzük, hogy munkanap-e?
  --     csak így, egy hosszabb intervallumnál kicsit hosszabb is lesz a futásido ...
  --
  declare @osszes_munka_napszam   int 

  -- napra truncate-olás
  select @datum_tol = isnull(@datum_tol,getdate())
  select @datum_tol = convert(datetime,convert(varchar,@datum_tol,102))
  select @datum_ig  = isnull(@datum_ig,getdate())
  select @datum_ig  = convert(datetime,convert(varchar,@datum_ig ,102))
  
  if @datum_tol > @datum_ig
     return (NULL)   

  if @datum_tol = @datum_ig
     return (0)   
  ------
  select @osszes_munka_napszam = 0
  
  while  @datum_tol < @datum_ig
  begin
    select @osszes_munka_napszam = @osszes_munka_napszam + dbo.fn_munkanap(@datum_tol)
    select @datum_tol = dateadd(day,1,@datum_tol)
  end
    
  ------
  return (@osszes_munka_napszam)

END

' 
END

GO
