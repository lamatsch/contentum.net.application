@echo off

set sqlcmd="sqlcmd.exe"

if "%1"=="" goto :help
if "%2"=="" goto :help
if "%3"=="" goto :help

@echo on

%sqlcmd% -S %1 -d %2 -i KuldemenyekToElhisz_Job.sql -v DataBaseName="%2" Url="%3/KuldemenyekToElhisz"

%sqlcmd% -S %1 -d %2 -i KuldemenyekFromElhisz_Job.sql -v DataBaseName="%2" Url="%3/KuldemenyekFromElhisz"

@echo off

goto :EOF

:help
echo. Hasznalat:
echo.  1. parameter: szerver nev
echo.  2. parameter: adatbazis neve
echo.  3. parameter: Elhisz Service url
echo.
echo. Pelda:
echo. build.bat INTCSQL2T EDOK http://localhost:100/eIntegratorWebService/INT_ElhiszService.asmx

:EOF