@echo off

set sqlcmd="sqlcmd.exe"

if "%1"=="" goto :help
if "%2"=="" goto :help
if "%3"=="" goto :help

@echo on
%sqlcmd% -S %1 -d %2 -i sp_HivataliKapusUzenetekLetoltese.sql

%sqlcmd% -S %1 -i EnableWebServiceCall.sql

%sqlcmd% -S %1 -v DataBaseName="%2" KRServiceUrl="%3" -i HivataliKapusUzenetekLetoltese_Job.sql

@echo off

goto :EOF

:help
echo. Hasznalat:
echo.  1. parameter: szerver nev
echo.  2. parameter: adatbazis neve
echo.  3. parameter: hivatali kapus service url
echo.
echo. Pelda:
echo. build.bat INTCSQL2T EDOK http://intvhkt1/HKP_KRService/KRService.asmx
echo. build.bat INCSQL2 EDOK http://intvhkt1/HKP_KRService/KRService.asmx

:EOF