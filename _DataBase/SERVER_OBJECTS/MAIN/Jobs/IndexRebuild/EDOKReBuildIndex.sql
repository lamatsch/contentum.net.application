-- dbo.KRT_Funkciok ...
alter index all on dbo.KRT_Funkciok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_KodCsoportok ...
alter index all on dbo.KRT_KodCsoportok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_KodTarak ...
alter index all on dbo.KRT_KodTarak
rebuild with(fillfactor=85);
GO
-- dbo.KRT_ObjTipusok ...
alter index all on dbo.KRT_ObjTipusok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Orgok ...
alter index all on dbo.KRT_Orgok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Partnerek ...
alter index all on dbo.KRT_Partnerek
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Vallalkozasok ...
alter index all on dbo.KRT_Vallalkozasok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_PartnerKapcsolatok ...
alter index all on dbo.KRT_PartnerKapcsolatok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_PartnerCimek ...
alter index all on dbo.KRT_PartnerCimek
rebuild with(fillfactor=85);
GO
-- dbo.KRT_PartnerMinositesek ...
alter index all on dbo.KRT_PartnerMinositesek
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Telepulesek ...
alter index all on dbo.KRT_Telepulesek
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Parameterek ...
alter index all on dbo.KRT_Parameterek
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Szemelyek ...
alter index all on dbo.KRT_Szemelyek
rebuild with(fillfactor=85);
GO
-- dbo.KRT_FelhasznaloProfilok ...
alter index all on dbo.KRT_FelhasznaloProfilok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Szerepkorok ...
alter index all on dbo.KRT_Szerepkorok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Felhasznalo_Szerepkor ...
alter index all on dbo.KRT_Felhasznalo_Szerepkor
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Helyettesitesek ...
alter index all on dbo.KRT_Helyettesitesek
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Felhasznalok ...
alter index all on dbo.KRT_Felhasznalok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_CsoportTagok ...
alter index all on dbo.KRT_CsoportTagok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Jogosultak ...
alter index all on dbo.KRT_Jogosultak
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Csoportok ...
alter index all on dbo.KRT_Csoportok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Cimek ...
alter index all on dbo.KRT_Cimek
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Orszagok ...
alter index all on dbo.KRT_Orszagok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Kozteruletek ...
alter index all on dbo.KRT_Kozteruletek
rebuild with(fillfactor=85);
GO
-- dbo.KRT_KozteruletTipusok ...
alter index all on dbo.KRT_KozteruletTipusok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Tanusitvanyok ...
alter index all on dbo.KRT_Tanusitvanyok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_DokumentumMuveletek ...
alter index all on dbo.KRT_DokumentumMuveletek
rebuild with(fillfactor=85);
GO
-- dbo.KRT_AlairasTipusok ...
alter index all on dbo.KRT_AlairasTipusok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_EASZTipus ...
alter index all on dbo.KRT_EASZTipus
rebuild with(fillfactor=85);
GO
-- dbo.KRT_TanusitvanyTipusok ...
alter index all on dbo.KRT_TanusitvanyTipusok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Folyamatok ...
alter index all on dbo.KRT_Folyamatok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_TanusitvanyTipusAlairas ...
alter index all on dbo.KRT_TanusitvanyTipusAlairas
rebuild with(fillfactor=85);
GO
-- dbo.KRT_AlairasSzabalyok ...
alter index all on dbo.KRT_AlairasSzabalyok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_EASZ ...
alter index all on dbo.KRT_EASZ
rebuild with(fillfactor=85);
GO
-- dbo.KRT_EASZTanusitvanyok ...
alter index all on dbo.KRT_EASZTanusitvanyok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Esemenyek ...
alter index all on dbo.KRT_Esemenyek
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Tranz_Obj ...
alter index all on dbo.KRT_Tranz_Obj
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Barkodok ...
alter index all on dbo.KRT_Barkodok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Log_Login ...
alter index all on dbo.KRT_Log_Login
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Log_Page ...
alter index all on dbo.KRT_Log_Page
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Log_WebService ...
alter index all on dbo.KRT_Log_WebService
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Log_StoredProcedure ...
alter index all on dbo.KRT_Log_StoredProcedure
rebuild with(fillfactor=85);
GO
-- dbo.KRT_BarkodSavok ...
alter index all on dbo.KRT_BarkodSavok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Tartomanyok ...
alter index all on dbo.KRT_Tartomanyok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Tartomanyok_Szervezetek ...
alter index all on dbo.KRT_Tartomanyok_Szervezetek
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Szerepkor_Funkcio ...
alter index all on dbo.KRT_Szerepkor_Funkcio
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Nezetek ...
alter index all on dbo.KRT_Nezetek
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Muveletek ...
alter index all on dbo.KRT_Muveletek
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Menuk ...
alter index all on dbo.KRT_Menuk
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Modulok ...
alter index all on dbo.KRT_Modulok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Modul_Funkcio ...
alter index all on dbo.KRT_Modul_Funkcio
rebuild with(fillfactor=85);
GO
-- dbo.KRT_UIMezoObjektumErtekek ...
alter index all on dbo.KRT_UIMezoObjektumErtekek
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Alkalmazasok ...
alter index all on dbo.KRT_Alkalmazasok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Tranzakciok ...
alter index all on dbo.KRT_Tranzakciok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_FunkcioLista ...
alter index all on dbo.KRT_FunkcioLista
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Erintett_Tablak ...
alter index all on dbo.KRT_Erintett_Tablak
rebuild with(fillfactor=85);
GO
-- dbo.KRT_TemplateManager ...
alter index all on dbo.KRT_TemplateManager
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Extra_Napok ...
alter index all on dbo.KRT_Extra_Napok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_UgyUgyiratok ...
alter index all on dbo.EREC_UgyUgyiratok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IraIratok ...
alter index all on dbo.EREC_IraIratok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IratKapcsolatok ...
alter index all on dbo.EREC_IratKapcsolatok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IratAlairok ...
alter index all on dbo.EREC_IratAlairok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IraElosztoivek ...
alter index all on dbo.EREC_IraElosztoivek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IraElosztoivTetelek ...
alter index all on dbo.EREC_IraElosztoivTetelek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_Irat_Iktatokonyvei ...
alter index all on dbo.EREC_Irat_Iktatokonyvei
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IraIktatoKonyvek ...
alter index all on dbo.EREC_IraIktatoKonyvek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IraIrattariTetelek ...
alter index all on dbo.EREC_IraIrattariTetelek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IraJegyzekek ...
alter index all on dbo.EREC_IraJegyzekek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IraJegyzekTetelek ...
alter index all on dbo.EREC_IraJegyzekTetelek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IraKezFeljegyzesek ...
alter index all on dbo.EREC_IraKezFeljegyzesek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IraOnkormAdatok ...
alter index all on dbo.EREC_IraOnkormAdatok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_TargySzavak ...
alter index all on dbo.EREC_TargySzavak
rebuild with(fillfactor=85);
GO
-- dbo.EREC_KuldBekuldok ...
alter index all on dbo.EREC_KuldBekuldok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_KuldKezFeljegyzesek ...
alter index all on dbo.EREC_KuldKezFeljegyzesek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_KuldKuldemenyek ...
alter index all on dbo.EREC_KuldKuldemenyek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_KuldTertivevenyek ...
alter index all on dbo.EREC_KuldTertivevenyek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_PldIratPeldanyok ...
alter index all on dbo.EREC_PldIratPeldanyok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_eMailBoritekok ...
alter index all on dbo.EREC_eMailBoritekok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_eMailBoritekCimei ...
alter index all on dbo.EREC_eMailBoritekCimei
rebuild with(fillfactor=85);
GO
-- dbo.EREC_eMailFiokok ...
alter index all on dbo.EREC_eMailFiokok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_UgyKezFeljegyzesek ...
alter index all on dbo.EREC_UgyKezFeljegyzesek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_Obj_MetaAdatai ...
alter index all on dbo.EREC_Obj_MetaAdatai
rebuild with(fillfactor=85);
GO
-- dbo.EREC_UgyUgyiratdarabok ...
alter index all on dbo.EREC_UgyUgyiratdarabok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_Kuldemeny_IratPeldanyai ...
alter index all on dbo.EREC_Kuldemeny_IratPeldanyai
rebuild with(fillfactor=85);
GO
-- dbo.EREC_HataridosFeladatok ...
alter index all on dbo.EREC_HataridosFeladatok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_Hatarid_Objektumok ...
alter index all on dbo.EREC_Hatarid_Objektumok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IraKezbesitesiTetelek ...
alter index all on dbo.EREC_IraKezbesitesiTetelek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IrattariKikero ...
alter index all on dbo.EREC_IrattariKikero
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IratMetaDefinicio ...
alter index all on dbo.EREC_IratMetaDefinicio
rebuild with(fillfactor=85);
GO
-- dbo.EREC_KuldKapcsolatok ...
alter index all on dbo.EREC_KuldKapcsolatok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_AgazatiJelek ...
alter index all on dbo.EREC_AgazatiJelek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_eMailBoritekCsatolmanyok ...
alter index all on dbo.EREC_eMailBoritekCsatolmanyok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_UgyiratKapcsolatok ...
alter index all on dbo.EREC_UgyiratKapcsolatok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IrattariTetel_Iktatokonyv ...
alter index all on dbo.EREC_IrattariTetel_Iktatokonyv
rebuild with(fillfactor=85);
GO
-- dbo.EREC_FeladatDefinicio ...
alter index all on dbo.EREC_FeladatDefinicio
rebuild with(fillfactor=85);
GO
-- dbo.EREC_SzignalasiJegyzekek ...
alter index all on dbo.EREC_SzignalasiJegyzekek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_ObjektumTargyszavai ...
alter index all on dbo.EREC_ObjektumTargyszavai
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IraKezbesitesiFejek ...
alter index all on dbo.EREC_IraKezbesitesiFejek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_UgyiratObjKapcsolatok ...
alter index all on dbo.EREC_UgyiratObjKapcsolatok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_Obj_MetaDefinicio ...
alter index all on dbo.EREC_Obj_MetaDefinicio
rebuild with(fillfactor=85);
GO
-- dbo.EREC_IratelemKapcsolatok ...
alter index all on dbo.EREC_IratelemKapcsolatok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_Csatolmanyok ...
alter index all on dbo.EREC_Csatolmanyok
rebuild with(fillfactor=85);
GO
-- dbo.EREC_Mellekletek ...
alter index all on dbo.EREC_Mellekletek
rebuild with(fillfactor=85);
GO
-- dbo.EREC_StateMetaDefinicio ...
alter index all on dbo.EREC_StateMetaDefinicio
rebuild with(fillfactor=85);
GO
-- dbo.EREC_ObjStateValue ...
alter index all on dbo.EREC_ObjStateValue
rebuild with(fillfactor=85);
GO
-- dbo.EREC_PldKapjakMeg ...
alter index all on dbo.EREC_PldKapjakMeg
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Dokumentumok ...
alter index all on dbo.KRT_Dokumentumok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_Mappak ...
alter index all on dbo.KRT_Mappak
rebuild with(fillfactor=85);
GO
-- dbo.KRT_DokumentumKapcsolatok ...
alter index all on dbo.KRT_DokumentumKapcsolatok
rebuild with(fillfactor=85);
GO
-- dbo.KRT_MappaTartalmak ...
alter index all on dbo.KRT_MappaTartalmak
rebuild with(fillfactor=85);
GO
-- dbo.KRT_MappaUtak ...
alter index all on dbo.KRT_MappaUtak
rebuild with(fillfactor=85);
GO
-- dbo.KRT_DokumentumMegosztas ...
alter index all on dbo.KRT_DokumentumMegosztas
rebuild with(fillfactor=85);
GO
-- dbo.KRT_DokumentumAlairasok ...
alter index all on dbo.KRT_DokumentumAlairasok
rebuild with(fillfactor=85);
GO
