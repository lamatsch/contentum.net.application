-- dbo.KRT_Funkciok ...
alter index all on dbo.KRT_Funkciok
REORGANIZE;
GO
-- dbo.KRT_KodCsoportok ...
alter index all on dbo.KRT_KodCsoportok
REORGANIZE;
GO
-- dbo.KRT_KodTarak ...
alter index all on dbo.KRT_KodTarak
REORGANIZE;
GO
-- dbo.KRT_ObjTipusok ...
alter index all on dbo.KRT_ObjTipusok
REORGANIZE;
GO
-- dbo.KRT_Orgok ...
alter index all on dbo.KRT_Orgok
REORGANIZE;
GO
-- dbo.KRT_Partnerek ...
alter index all on dbo.KRT_Partnerek
REORGANIZE;
GO
-- dbo.KRT_Vallalkozasok ...
alter index all on dbo.KRT_Vallalkozasok
REORGANIZE;
GO
-- dbo.KRT_PartnerKapcsolatok ...
alter index all on dbo.KRT_PartnerKapcsolatok
REORGANIZE;
GO
-- dbo.KRT_PartnerCimek ...
alter index all on dbo.KRT_PartnerCimek
REORGANIZE;
GO
-- dbo.KRT_PartnerMinositesek ...
alter index all on dbo.KRT_PartnerMinositesek
REORGANIZE;
GO
-- dbo.KRT_Telepulesek ...
alter index all on dbo.KRT_Telepulesek
REORGANIZE;
GO
-- dbo.KRT_Parameterek ...
alter index all on dbo.KRT_Parameterek
REORGANIZE;
GO
-- dbo.KRT_Szemelyek ...
alter index all on dbo.KRT_Szemelyek
REORGANIZE;
GO
-- dbo.KRT_FelhasznaloProfilok ...
alter index all on dbo.KRT_FelhasznaloProfilok
REORGANIZE;
GO
-- dbo.KRT_Szerepkorok ...
alter index all on dbo.KRT_Szerepkorok
REORGANIZE;
GO
-- dbo.KRT_Felhasznalo_Szerepkor ...
alter index all on dbo.KRT_Felhasznalo_Szerepkor
REORGANIZE;
GO
-- dbo.KRT_Helyettesitesek ...
alter index all on dbo.KRT_Helyettesitesek
REORGANIZE;
GO
-- dbo.KRT_Felhasznalok ...
alter index all on dbo.KRT_Felhasznalok
REORGANIZE;
GO
-- dbo.KRT_CsoportTagok ...
alter index all on dbo.KRT_CsoportTagok
REORGANIZE;
GO
-- dbo.KRT_Jogosultak ...
alter index all on dbo.KRT_Jogosultak
REORGANIZE;
GO
-- dbo.KRT_Csoportok ...
alter index all on dbo.KRT_Csoportok
REORGANIZE;
GO
-- dbo.KRT_Cimek ...
alter index all on dbo.KRT_Cimek
REORGANIZE;
GO
-- dbo.KRT_Orszagok ...
alter index all on dbo.KRT_Orszagok
REORGANIZE;
GO
-- dbo.KRT_Kozteruletek ...
alter index all on dbo.KRT_Kozteruletek
REORGANIZE;
GO
-- dbo.KRT_KozteruletTipusok ...
alter index all on dbo.KRT_KozteruletTipusok
REORGANIZE;
GO
-- dbo.KRT_Tanusitvanyok ...
alter index all on dbo.KRT_Tanusitvanyok
REORGANIZE;
GO
-- dbo.KRT_DokumentumMuveletek ...
alter index all on dbo.KRT_DokumentumMuveletek
REORGANIZE;
GO
-- dbo.KRT_AlairasTipusok ...
alter index all on dbo.KRT_AlairasTipusok
REORGANIZE;
GO
-- dbo.KRT_EASZTipus ...
alter index all on dbo.KRT_EASZTipus
REORGANIZE;
GO
-- dbo.KRT_TanusitvanyTipusok ...
alter index all on dbo.KRT_TanusitvanyTipusok
REORGANIZE;
GO
-- dbo.KRT_Folyamatok ...
alter index all on dbo.KRT_Folyamatok
REORGANIZE;
GO
-- dbo.KRT_TanusitvanyTipusAlairas ...
alter index all on dbo.KRT_TanusitvanyTipusAlairas
REORGANIZE;
GO
-- dbo.KRT_AlairasSzabalyok ...
alter index all on dbo.KRT_AlairasSzabalyok
REORGANIZE;
GO
-- dbo.KRT_EASZ ...
alter index all on dbo.KRT_EASZ
REORGANIZE;
GO
-- dbo.KRT_EASZTanusitvanyok ...
alter index all on dbo.KRT_EASZTanusitvanyok
REORGANIZE;
GO-- dbo.KRT_Esemenyek ...
alter index all on dbo.KRT_Esemenyek
REORGANIZE;
GO
-- dbo.KRT_Tranz_Obj ...
alter index all on dbo.KRT_Tranz_Obj
REORGANIZE;
GO
-- dbo.KRT_Barkodok ...
alter index all on dbo.KRT_Barkodok
REORGANIZE;
GO
-- dbo.KRT_Log_Login ...
alter index all on dbo.KRT_Log_Login
REORGANIZE;
GO
-- dbo.KRT_Log_Page ...
alter index all on dbo.KRT_Log_Page
REORGANIZE;
GO
-- dbo.KRT_Log_WebService ...
alter index all on dbo.KRT_Log_WebService
REORGANIZE;
GO
-- dbo.KRT_Log_StoredProcedure ...
alter index all on dbo.KRT_Log_StoredProcedure
REORGANIZE;
GO
-- dbo.KRT_BarkodSavok ...
alter index all on dbo.KRT_BarkodSavok
REORGANIZE;
GO
-- dbo.KRT_Tartomanyok ...
alter index all on dbo.KRT_Tartomanyok
REORGANIZE;
GO
-- dbo.KRT_Tartomanyok_Szervezetek ...
alter index all on dbo.KRT_Tartomanyok_Szervezetek
REORGANIZE;
GO-- dbo.KRT_Szerepkor_Funkcio ...
alter index all on dbo.KRT_Szerepkor_Funkcio
REORGANIZE;
GO
-- dbo.KRT_Nezetek ...
alter index all on dbo.KRT_Nezetek
REORGANIZE;
GO
-- dbo.KRT_Muveletek ...
alter index all on dbo.KRT_Muveletek
REORGANIZE;
GO
-- dbo.KRT_Menuk ...
alter index all on dbo.KRT_Menuk
REORGANIZE;
GO
-- dbo.KRT_Modulok ...
alter index all on dbo.KRT_Modulok
REORGANIZE;
GO
-- dbo.KRT_Modul_Funkcio ...
alter index all on dbo.KRT_Modul_Funkcio
REORGANIZE;
GO
-- dbo.KRT_UIMezoObjektumErtekek ...
alter index all on dbo.KRT_UIMezoObjektumErtekek
REORGANIZE;
GO
-- dbo.KRT_Alkalmazasok ...
alter index all on dbo.KRT_Alkalmazasok
REORGANIZE;
GO
-- dbo.KRT_Tranzakciok ...
alter index all on dbo.KRT_Tranzakciok
REORGANIZE;
GO
-- dbo.KRT_FunkcioLista ...
alter index all on dbo.KRT_FunkcioLista
REORGANIZE;
GO
-- dbo.KRT_Erintett_Tablak ...
alter index all on dbo.KRT_Erintett_Tablak
REORGANIZE;
GO
-- dbo.KRT_TemplateManager ...
alter index all on dbo.KRT_TemplateManager
REORGANIZE;
GO
-- dbo.KRT_Extra_Napok ...
alter index all on dbo.KRT_Extra_Napok
REORGANIZE;
GO-- dbo.EREC_UgyUgyiratok ...
alter index all on dbo.EREC_UgyUgyiratok
REORGANIZE;
GO
-- dbo.EREC_IraIratok ...
alter index all on dbo.EREC_IraIratok
REORGANIZE;
GO
-- dbo.EREC_IratKapcsolatok ...
alter index all on dbo.EREC_IratKapcsolatok
REORGANIZE;
GO
-- dbo.EREC_IratAlairok ...
alter index all on dbo.EREC_IratAlairok
REORGANIZE;
GO
-- dbo.EREC_IraElosztoivek ...
alter index all on dbo.EREC_IraElosztoivek
REORGANIZE;
GO
-- dbo.EREC_IraElosztoivTetelek ...
alter index all on dbo.EREC_IraElosztoivTetelek
REORGANIZE;
GO
-- dbo.EREC_Irat_Iktatokonyvei ...
alter index all on dbo.EREC_Irat_Iktatokonyvei
REORGANIZE;
GO
-- dbo.EREC_IraIktatoKonyvek ...
alter index all on dbo.EREC_IraIktatoKonyvek
REORGANIZE;
GO
-- dbo.EREC_IraIrattariTetelek ...
alter index all on dbo.EREC_IraIrattariTetelek
REORGANIZE;
GO
-- dbo.EREC_IraJegyzekek ...
alter index all on dbo.EREC_IraJegyzekek
REORGANIZE;
GO
-- dbo.EREC_IraJegyzekTetelek ...
alter index all on dbo.EREC_IraJegyzekTetelek
REORGANIZE;
GO
-- dbo.EREC_IraKezFeljegyzesek ...
alter index all on dbo.EREC_IraKezFeljegyzesek
REORGANIZE;
GO
-- dbo.EREC_IraOnkormAdatok ...
alter index all on dbo.EREC_IraOnkormAdatok
REORGANIZE;
GO
-- dbo.EREC_TargySzavak ...
alter index all on dbo.EREC_TargySzavak
REORGANIZE;
GO
-- dbo.EREC_KuldBekuldok ...
alter index all on dbo.EREC_KuldBekuldok
REORGANIZE;
GO
-- dbo.EREC_KuldKezFeljegyzesek ...
alter index all on dbo.EREC_KuldKezFeljegyzesek
REORGANIZE;
GO
-- dbo.EREC_KuldKuldemenyek ...
alter index all on dbo.EREC_KuldKuldemenyek
REORGANIZE;
GO
-- dbo.EREC_KuldTertivevenyek ...
alter index all on dbo.EREC_KuldTertivevenyek
REORGANIZE;
GO
-- dbo.EREC_PldIratPeldanyok ...
alter index all on dbo.EREC_PldIratPeldanyok
REORGANIZE;
GO
-- dbo.EREC_eMailBoritekok ...
alter index all on dbo.EREC_eMailBoritekok
REORGANIZE;
GO
-- dbo.EREC_eMailBoritekCimei ...
alter index all on dbo.EREC_eMailBoritekCimei
REORGANIZE;
GO
-- dbo.EREC_eMailFiokok ...
alter index all on dbo.EREC_eMailFiokok
REORGANIZE;
GO
-- dbo.EREC_UgyKezFeljegyzesek ...
alter index all on dbo.EREC_UgyKezFeljegyzesek
REORGANIZE;
GO
-- dbo.EREC_Obj_MetaAdatai ...
alter index all on dbo.EREC_Obj_MetaAdatai
REORGANIZE;
GO
-- dbo.EREC_UgyUgyiratdarabok ...
alter index all on dbo.EREC_UgyUgyiratdarabok
REORGANIZE;
GO
-- dbo.EREC_Kuldemeny_IratPeldanyai ...
alter index all on dbo.EREC_Kuldemeny_IratPeldanyai
REORGANIZE;
GO
-- dbo.EREC_HataridosFeladatok ...
alter index all on dbo.EREC_HataridosFeladatok
REORGANIZE;
GO
-- dbo.EREC_Hatarid_Objektumok ...
alter index all on dbo.EREC_Hatarid_Objektumok
REORGANIZE;
GO
-- dbo.EREC_IraKezbesitesiTetelek ...
alter index all on dbo.EREC_IraKezbesitesiTetelek
REORGANIZE;
GO
-- dbo.EREC_IrattariKikero ...
alter index all on dbo.EREC_IrattariKikero
REORGANIZE;
GO
-- dbo.EREC_IratMetaDefinicio ...
alter index all on dbo.EREC_IratMetaDefinicio
REORGANIZE;
GO
-- dbo.EREC_KuldKapcsolatok ...
alter index all on dbo.EREC_KuldKapcsolatok
REORGANIZE;
GO
-- dbo.EREC_AgazatiJelek ...
alter index all on dbo.EREC_AgazatiJelek
REORGANIZE;
GO
-- dbo.EREC_eMailBoritekCsatolmanyok ...
alter index all on dbo.EREC_eMailBoritekCsatolmanyok
REORGANIZE;
GO
-- dbo.EREC_UgyiratKapcsolatok ...
alter index all on dbo.EREC_UgyiratKapcsolatok
REORGANIZE;
GO
-- dbo.EREC_IrattariTetel_Iktatokonyv ...
alter index all on dbo.EREC_IrattariTetel_Iktatokonyv
REORGANIZE;
GO
-- dbo.EREC_FeladatDefinicio ...
alter index all on dbo.EREC_FeladatDefinicio
REORGANIZE;
GO
-- dbo.EREC_SzignalasiJegyzekek ...
alter index all on dbo.EREC_SzignalasiJegyzekek
REORGANIZE;
GO
-- dbo.EREC_ObjektumTargyszavai ...
alter index all on dbo.EREC_ObjektumTargyszavai
REORGANIZE;
GO
-- dbo.EREC_IraKezbesitesiFejek ...
alter index all on dbo.EREC_IraKezbesitesiFejek
REORGANIZE;
GO
-- dbo.EREC_UgyiratObjKapcsolatok ...
alter index all on dbo.EREC_UgyiratObjKapcsolatok
REORGANIZE;
GO
-- dbo.EREC_Obj_MetaDefinicio ...
alter index all on dbo.EREC_Obj_MetaDefinicio
REORGANIZE;
GO
-- dbo.EREC_IratelemKapcsolatok ...
alter index all on dbo.EREC_IratelemKapcsolatok
REORGANIZE;
GO
-- dbo.EREC_Csatolmanyok ...
alter index all on dbo.EREC_Csatolmanyok
REORGANIZE;
GO
-- dbo.EREC_Mellekletek ...
alter index all on dbo.EREC_Mellekletek
REORGANIZE;
GO
-- dbo.EREC_StateMetaDefinicio ...
alter index all on dbo.EREC_StateMetaDefinicio
REORGANIZE;
GO
-- dbo.EREC_ObjStateValue ...
alter index all on dbo.EREC_ObjStateValue
REORGANIZE;
GO
-- dbo.EREC_PldKapjakMeg ...
alter index all on dbo.EREC_PldKapjakMeg
REORGANIZE;
GO-- dbo.KRT_Dokumentumok ...
alter index all on dbo.KRT_Dokumentumok
REORGANIZE;
GO
-- dbo.KRT_Mappak ...
alter index all on dbo.KRT_Mappak
REORGANIZE;
GO
-- dbo.KRT_DokumentumKapcsolatok ...
alter index all on dbo.KRT_DokumentumKapcsolatok
REORGANIZE;
GO
-- dbo.KRT_MappaTartalmak ...
alter index all on dbo.KRT_MappaTartalmak
REORGANIZE;
GO
-- dbo.KRT_MappaUtak ...
alter index all on dbo.KRT_MappaUtak
REORGANIZE;
GO
-- dbo.KRT_DokumentumMegosztas ...
alter index all on dbo.KRT_DokumentumMegosztas
REORGANIZE;
GO
-- dbo.KRT_DokumentumAlairasok ...
alter index all on dbo.KRT_DokumentumAlairasok
REORGANIZE;
GO