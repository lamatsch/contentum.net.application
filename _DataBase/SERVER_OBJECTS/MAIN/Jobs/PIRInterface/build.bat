@echo off

set sqlcmd="sqlcmd.exe"

if "%1"=="" goto :help
if "%2"=="" goto :help
if "%3"=="" goto :help

@echo on
%sqlcmd% -S %1 -d %2 -i sp_PirSzamlaKepFeltoltes.sql

%sqlcmd% -S %1 -i EnableWebServiceCall.sql

%sqlcmd% -S %1 -v DataBaseName="%2" PIRServiceUrl="%3" -i PirSzamlaKepFeltoltesJob.sql

@echo off

goto :EOF

:help
echo. Hasznalat:
echo.  1. parameter: szerver nev
echo.  2. parameter: adatbazis neve
echo.  3. parameter: eIntegrator pir service url
echo.
echo. Pelda:
echo. build.bat ax-vfphtst02 CONTENTUM_TEST_BOPMH http://ax-vfphtst03:81/Contentum/eIntegratorWebService/INT_PIRService.asmx
echo. build.bat INTCSQL2T EDOK http://edoktest/eIntegratorWebService/INT_PIRService.asmx
echo. build.bat INCSQL2 EDOK http://edok/eIntegratorWebService/INT_PIRService.asmx

:EOF