SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if exists (select 1 from sysobjects where id = object_id('sp_PirSzamlaKepFeltoltes') and type = 'P')
begin
  drop procedure sp_PirSzamlaKepFeltoltes;
end
go

CREATE PROCEDURE sp_PirSzamlaKepFeltoltes
	@PIRServiceUrl nvarchar(400),
	@UserName nvarchar(400) = '',
	@Password nvarchar(400) = ''
AS
BEGIN
	declare @getUrl nvarchar(400)
	set @getUrl = @PIRServiceUrl + '/PirSzamlaKepFeltoltesJob'

	Declare @obj INT
	DECLARE @hResult int
	DECLARE @source varchar(255), @desc varchar(255) 
	
	exec sp_OACreate 'MSXML2.ServerXMLHTTP', @obj OUT
	
	exec @hResult = sp_OAMethod @obj, 'Open', NULL, 'GET', @getUrl, false, @UserName, @Password
	
	IF @hResult <> 0 
	BEGIN
		  EXEC sp_OAGetErrorInfo @obj, @source OUT, @desc OUT
		  exec sp_OADestroy @obj
		  Raiserror('Open failed: %s - %s',16,1,@desc, @PIRServiceUrl)
		  return
	END

	exec @hResult = sp_OAMethod @obj, 'send'
	
	IF  @hResult <> 0 
	BEGIN
		  EXEC sp_OAGetErrorInfo @obj, @source OUT, @desc OUT
		  exec sp_OADestroy @obj
		  Raiserror('Send failed: %s - %s',16,1,@desc, @PIRServiceUrl)
		  return
	 END

	declare @statusText varchar(1000), @status varchar(1000), @response VARCHAR(8000)
	-- Get status, response text 
	exec sp_OAGetProperty @obj, 'StatusText', @statusText out
	exec sp_OAGetProperty @obj, 'Status', @status out
	exec sp_OAGetProperty @obj, 'responseText', @response OUT
	select @status, @statusText, @response

	exec sp_OADestroy @obj
	
	if @status <> '200'
		Raiserror('Response failed: %s - %s - %s', 16, 1, @status, @statusText, @PIRServiceUrl)
END
GO

-- exec sp_PirSzamlaKepFeltoltes 'http://lamatsch-pc:100/eIntegratorWebservice/INT_PIRService.asmx'