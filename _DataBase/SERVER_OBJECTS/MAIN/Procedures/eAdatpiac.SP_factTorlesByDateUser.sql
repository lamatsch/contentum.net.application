IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[SP_factTorlesByDateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [eAdatpiac].[SP_factTorlesByDateUser]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[SP_factTorlesByDateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [eAdatpiac].[SP_factTorlesByDateUser] AS' 
END
GO
ALTER procedure [eAdatpiac].[SP_factTorlesByDateUser]
(
    @FrissitesDat datetime,               -- frissites datuma
    @szervezet nvarchar(100)   = '-',     -- szervezet guid
    @felhasznalo nvarchar(100) = '-'      -- felhasznalo guid
)
as
begin

declare @rekordszam int
declare @felh_id as uniqueidentifier, @szerv_id as uniqueidentifier
set @felh_id = case when @felhasznalo ='-' then NULL 
                    else convert(uniqueidentifier, @felhasznalo) end
set @szerv_id = case when @szervezet ='-' then NULL
                    else convert(uniqueidentifier, @szervezet) end

delete eAdatpiac.Fact_Feladatok 
 from eAdatpiac.Fact_Feladatok fact
    inner join eAdatpiac.Dim_FrissitesDatum dat on fact.FrissitesDatum_Id = dat.datum_id
    inner join eAdatpiac.Dim_FeladatFelelos fel on fact.FeladatFelelos_Id = fel.id
 where dat.datum = @FrissitesDat 
       and 1 = case when @felh_id is null then 1 
                    when fel.Felhasznalo_Id = @felh_id then 1 else 0 end
       and 1 = case when @szerv_id is null then 1 
                    when fel.Szervezet_Id = @szerv_id then 1 else 0 end
end


GO
