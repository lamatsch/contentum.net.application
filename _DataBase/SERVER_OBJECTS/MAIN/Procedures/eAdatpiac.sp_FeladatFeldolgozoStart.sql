IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[sp_FeladatFeldolgozoStart]') AND type in (N'P', N'PC'))
DROP PROCEDURE [eAdatpiac].[sp_FeladatFeldolgozoStart]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[sp_FeladatFeldolgozoStart]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [eAdatpiac].[sp_FeladatFeldolgozoStart] AS' 
END
GO
ALTER procedure [eAdatpiac].[sp_FeladatFeldolgozoStart] 
  @keres_id int output
as
  begin

    update eAdatpiac.FeladatIgeny  
      set KezdesIdo = getdate(), 
          allapot = case
			when allapot = 0 then 1	-- folyamatban
			else allapot
            end -- case
    where keres_id = @keres_id
  end


GO
