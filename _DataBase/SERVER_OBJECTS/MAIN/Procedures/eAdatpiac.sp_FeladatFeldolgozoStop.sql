IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[sp_FeladatFeldolgozoStop]') AND type in (N'P', N'PC'))
DROP PROCEDURE [eAdatpiac].[sp_FeladatFeldolgozoStop]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[sp_FeladatFeldolgozoStop]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [eAdatpiac].[sp_FeladatFeldolgozoStop] AS' 
END
GO
ALTER procedure [eAdatpiac].[sp_FeladatFeldolgozoStop]
@keres_id int
as
begin
    update eAdatpiac.FeladatIgeny  
      set VegeIdo = getdate(),
        allapot = case
			when allapot = 1 then 2	-- befejezve
			else allapot
		end --case
    where keres_id = @keres_id
end


GO
