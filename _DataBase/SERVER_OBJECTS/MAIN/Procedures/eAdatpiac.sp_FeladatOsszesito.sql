IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[sp_FeladatOsszesito]') AND type in (N'P', N'PC'))
DROP PROCEDURE [eAdatpiac].[sp_FeladatOsszesito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[sp_FeladatOsszesito]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [eAdatpiac].[sp_FeladatOsszesito] AS' 
END
GO
ALTER procedure [eAdatpiac].[sp_FeladatOsszesito]
		@FrissitesIdo    datetime,                -- a frissítés referenciaideje
		@Csoport_Id      uniqueidentifier = NULL, -- a felhasználó csoport Id-je
		@Felhasznalo_Id  uniqueidentifier = NULL  -- a felhasználó Id-je
AS
/*
-- FELADATA:
	Az adott paramétereknek megfelelő, a feladatdefinícióban hivatkozott objektum metadefiníció
    lekérdezése.

-- Példa:
-- 
-- mindenki
select getdate()
declare @FrissitesIdo datetime
select @FrissitesIdo = getdate()
exec [eAdatpiac].[sp_FeladatOsszesito] 
     @FrissitesIdo
select getdate()
--
-- egy személy
select getdate()
truncate table eAdatpiac.FeladatLoad
declare @Felhasznalo_Id uniqueidentifier, @Csoport_Id uniqueidentifier,@FrissitesIdo datetime
select @Felhasznalo_Id = Id from KRT_Csoportok where Nev like 'Dörny%'
select @Csoport_Id = Csoport_Id_Felelos, @FrissitesIdo = getdate()-1 --'2008.11.20'
	    from dbo.fn_WF_GetFelelos(@Felhasznalo_Id, Null)
select @Felhasznalo_Id Felhasznalo_Id, @Csoport_Id Csoport_Id
exec [eAdatpiac].[sp_FeladatOsszesito]
     @FrissitesIdo, @Csoport_Id, @Felhasznalo_Id
select getdate()
--
*/
---------
---------
BEGIN TRY

set nocount on;

DECLARE @Org uniqueidentifier
IF @Felhasznalo_Id IS NOT NULL
BEGIN
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@Felhasznalo_Id)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	END
END	
ELSE
BEGIN
	SET @Org = NULL
END

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @CFrissitesDatum        char(10),
		@Szukites				char(1),
		@Funkcio_Id_Inditando   uniqueidentifier,
	    @Obj_Type				varchar(100),
	    @IdoszamitasKezd		datetime

select @CFrissitesDatum = convert(char(10),@FrissitesIdo,102),
	   @Szukites        = case when @Felhasznalo_Id is NULL then 'N' else 'I' end,
	   @IdoszamitasKezd = '2008.05.10'

-- load tábla törlése
-- truncate table eAdatpiac.FeladatLoad

print convert(varchar(25),getdate(),126)+ ': Start'
print convert(varchar(25),@FrissitesIdo,126)+ ': @FrissitesIdo'

--------------------------
-- munkatáblák definiálása
--------------------------
--
-- határidős tétel kigyűjtés munkatábla
--
declare @HataridosFeladatok Table
--create Table #HataridosFeladatok
	  (Tetel_Id				  uniqueidentifier,
       Funkcio_Id_Inditando   uniqueidentifier,
	   Csoport_Id_Felelos	  uniqueidentifier,
	   Felhasznalo_Id_Felelos uniqueidentifier,
	   Csoport_Id_Kiado		  uniqueidentifier,
	   Felhasznalo_Id_Kiado	  uniqueidentifier,
	   Prioritas			  smallint,
	   IntezkHatarido		  datetime,
  	   Obj_Id				  uniqueidentifier,
	   Obj_Type				  varchar(100),
  	   Kiado_Id				  uniqueidentifier,
	   Allapot				  nvarchar(64),
	   LetrehozasIdo		  datetime,
	   ModositasIdo			  datetime
      )

--
-- feladatdefiníciós adatok munkatábla
--
declare @FeladatDef Table
--create Table #FeladatDef
	  (Funkcio_Id_Inditando   uniqueidentifier,
	   KezdesIdo		      datetime,
	   IntezkHatarido		  datetime,
	   Obj_Type				  varchar(100)
      )

-----------------------------------
-- Határidős feladatok feldolgozása
-----------------------------------
--
-- Határidős feladatok leválogatása
--
insert @HataridosFeladatok
select Id as Tetel_Id,
       Funkcio_Id_Inditando,
	   Csoport_Id_Felelos,
	   Felhasznalo_Id_Felelos,
	   Csoport_Id_Kiado,
	   Felhasznalo_Id_Kiado,
	   isnull(Prioritas,'11'),
	   isnull(IntezkHatarido,getdate()+1000),
  	   Obj_Id,
	   Obj_Type,
	   Felhasznalo_Id_Kiado as Kiado_Id,
	   Allapot,
	   LetrehozasIdo,
	   ModositasIdo
  from EREC_HataridosFeladatok
 where @FrissitesIdo between ErvKezd and ErvVege
   and Allapot in('0','1','2')
   and Tipus = 'F' -- feladat
   and Memo = 0
   and (@Szukites = 'N' or
	    @Szukites = 'I'
		and	(Felhasznalo_Id_Kiado = @Felhasznalo_Id and Csoport_Id_Kiado = @Csoport_Id or
             isnull(Felhasznalo_Id_Felelos, @Felhasznalo_Id) = @Felhasznalo_Id and
			 isnull(Csoport_Id_Felelos, @Csoport_Id) = @Csoport_Id))
--  and LetrehozasIdo <= @FrissitesIdo

select @rowcount = @@rowcount
print 'Határidős feladatok: '+ convert(varchar(10),@rowcount)

--
-- Végrehajtandó feladatok töltése
--
insert eAdatpiac.FeladatLoad
	  (FrissitesIdo,
	   FeladatKod,
	   FelelosSzervezet_Id,
	   FelelosFelhasznalo_Id,
	   Prioritas,
	   LejaratDatum,
	   FeladatJel,
	   EredetKod,
	   KeletkezesKod,
	   KezelesSzintKod,
	   Funkcio_Id_Inditando,
	   Obj_Id,
	   Obj_Type,
	   Kiado_Id)
select @FrissitesIdo,
	   case when hf.Obj_Type = 'EREC_KuldKuldemenyek' and kd.PostazasIranya = '1' then '81%'
		    when hf.Obj_Type = 'EREC_KuldKuldemenyek' and kd.PostazasIranya = '2' then '85%'
		    when hf.Obj_Type = 'EREC_UgyUgyiratok'     then '82%'
		    when hf.Obj_Type = 'EREC_IraIratok'        then '83%'
		    when hf.Obj_Type = 'EREC_PldIratPeldanyok' then '84%'
		    when hf.Obj_Type is NULL then '8600'
		    else '0000' end as FeladatKod,
	   hf.Csoport_Id_Felelos as FelelosSzervezet,
	   hf.Felhasznalo_Id_Felelos as FelelosFelhasznalo,
	   hf.Prioritas,
	   hf.IntezkHatarido as LejaratDatum,
	   'E' Feladatjel,
	   case when hf.Obj_Type = 'EREC_KuldKuldemenyek' and kd.PostazasIranya = '1' then 'K'
			else NULL end as EredetKod,
	   case when hf.Allapot = '0' then 'U' else 'R' end as KeletkezesKod,
	   case when hf.Obj_Type = 'EREC_UgyUgyiratok'     and ui.Allapot in('0','1','2') then 'M'
			when hf.Obj_Type = 'EREC_IraIratok' and pl.Allapot in('0','1','2') then 'M'
			when hf.Obj_Type = 'EREC_PldIratPeldanyok' and pl.Allapot in('0','1','2') then 'M'
			else 'N' end as KezelesSzintKod,
	   hf.Funkcio_Id_Inditando,
	   hf.Tetel_Id,
	   hf.Obj_Type,
	   hf.Kiado_Id
  from @HataridosFeladatok hf
 	   left outer join EREC_KuldKuldemenyek kd
			on hf.Obj_Type  = 'EREC_KuldKuldemenyek'
			and hf.Obj_Id = kd.Id
	   left outer join EREC_UgyUgyiratok ui
			on hf.Obj_Type  = 'EREC_UgyUgyiratok'
			and hf.Obj_Id = ui.Id
	   left outer join EREC_UgyUgyiratok ir
			on hf.Obj_Type  = 'EREC_IraIratok'
			and hf.Obj_Id = ir.Id
	   left outer join EREC_PldIratPeldanyok pl
			on hf.Obj_Type  = 'EREC_PldIratPeldanyok'
			and hf.Obj_Id = pl.Id
 where 1=1 --and Funkcio_Id_Inditando is not null
   and (@Szukites = 'N'
	    or @Szukites = 'I'
		and	hf.Felhasznalo_Id_Felelos = @Felhasznalo_Id
		and	isnull(hf.Csoport_Id_Felelos, @Csoport_Id) = @Csoport_Id)

select @rowcount = @@rowcount
print 'ElvHatáridős feladatok: '+ convert(varchar(10),@rowcount)
--
-- Kiadott feladatok töltése
--
insert eAdatpiac.FeladatLoad
	  (FrissitesIdo,
	   FeladatKod,
	   FelelosSzervezet_Id,
	   FelelosFelhasznalo_Id,
	   Prioritas,
	   LejaratDatum,
	   FeladatJel,
	   EredetKod,
	   KeletkezesKod,
	   KezelesSzintKod,
	   Funkcio_Id_Inditando,
	   Obj_Id,
	   Obj_Type,
	   Kiado_Id)
select @FrissitesIdo,
	   case when hf.Obj_Type = 'EREC_KuldKuldemenyek' and kd.PostazasIranya = '1' then '91%'
		    when hf.Obj_Type = 'EREC_KuldKuldemenyek' and kd.PostazasIranya = '2' then '95%'
		    when hf.Obj_Type = 'EREC_UgyUgyiratok'     then '92%'
		    when hf.Obj_Type = 'EREC_IraIratok'        then '93%'
		    when hf.Obj_Type = 'EREC_PldIratPeldanyok' then '94%'
		    when hf.Obj_Type is NULL then '9600'
		    else '0000' end as FeladatKod,
	   hf.Csoport_Id_Kiado as FelelosSzervezet,
	   hf.Felhasznalo_Id_Kiado as FelelosFelhasznalo,
	   hf.Prioritas,
	   hf.IntezkHatarido LejaratDatum,
	   'E' Feladatjel,
	   case when hf.Obj_Type = 'EREC_KuldKuldemenyek' and kd.PostazasIranya = '1' then 'K'
			else NULL end as EredetKod,
	   case when hf.Allapot = '0' then 'U' else 'R' end as KeletkezesKod,
	   case when hf.Obj_Type = 'EREC_UgyUgyiratok'     and ui.Allapot in('0','1','2') then 'M'
			when hf.Obj_Type = 'EREC_IraIratok' and pl.Allapot in('0','1','2') then 'M'
			when hf.Obj_Type = 'EREC_PldIratPeldanyok' and pl.Allapot in('0','1','2') then 'M'
			else 'N' end as KezelesSzintKod,
	   hf.Funkcio_Id_Inditando,
	   hf.Tetel_Id,
	   hf.Obj_Type,
	   hf.Kiado_Id
  from @HataridosFeladatok hf
 	   left outer join EREC_KuldKuldemenyek kd
			on hf.Obj_Type  = 'EREC_KuldKuldemenyek'
			and hf.Obj_Id = kd.Id
	   left outer join EREC_UgyUgyiratok ui
			on hf.Obj_Type  = 'EREC_UgyUgyiratok'
			and hf.Obj_Id = ui.Id
	   left outer join EREC_UgyUgyiratok ir
			on hf.Obj_Type  = 'EREC_IraIratok'
			and hf.Obj_Id = ir.Id
	   left outer join EREC_PldIratPeldanyok pl
			on hf.Obj_Type  = 'EREC_PldIratPeldanyok'
			and hf.Obj_Id = pl.Id
 where 1=1 --and Funkcio_Id_Inditando is not null
   and (@Szukites = 'N'
	    or @Szukites = 'I'
		and	hf.Felhasznalo_Id_Kiado = @Felhasznalo_Id
		and	hf.Csoport_Id_Kiado     = @Csoport_Id)

select @rowcount = @@rowcount
print 'KiaHatáridős feladatok: '+ convert(varchar(10),@rowcount)
print convert(varchar(25),getdate(),126)+ ': Határidős feladatok feldolgozása kész'

-----------------------------------
-- Kézbesítési tételek feldolgozása
-----------------------------------
delete @HataridosFeladatok
delete @FeladatDef

declare @AtfutasMinAtvetel  int,
		@IrattarbaKuldott   nvarchar(64)
set @IrattarbaKuldott = '11'

--
-- Feladatdefiníciós adatok olvasása
--
insert into @FeladatDef
exec [dbo].[sp_WF_UtemezettTrigger] 'LejartAtvetelek', 'I'
select @Funkcio_Id_Inditando = Funkcio_Id_Inditando,
	   @Obj_Type			 = Obj_Type,
	   @AtfutasMinAtvetel = -datediff(mi, Kezdesido, IntezkHatarido)
  from @FeladatDef
--select @Funkcio_Id_Inditando f, @Obj_Type o, @AtfutasMin a

--
-- Kézbesítések leválogatása
--
insert @HataridosFeladatok
select kt.Id as Tetel_Id,
       @Funkcio_Id_Inditando as Funkcio_Id_Inditando,
	   case when kt.Allapot = '1' or cs.Tipus = '1' then @Csoport_Id
			else kt.Csoport_Id_Cel end as Csoport_Id_Felelos,
	   case when kt.Allapot = '1' then kt.Felhasznalo_Id_Atado_USER
			when cs.Tipus = '1' then kt.Csoport_Id_Cel
			else NULL end as Felhasznalo_Id_Felelos,
	   NULL as Csoport_Id_Kiado,
	   kt.Felhasznalo_Id_Atado_USER as Felhasznalo_Id_Kiado,
	   NULL as Prioritas,
	   case when kt.Allapot = '1' then kt.LetrehozasIdo
			when kt.Allapot = '3' and kt.AtadasDat is NULL then kt.LetrehozasIdo
			else dateadd(mi, @AtfutasMinAtvetel, kt.AtadasDat) end as IntezkHatarido,
  	   kt.Obj_Id,
	   kt.Obj_Type,
	   kt.Felhasznalo_Id_Atado_USER as Kiado_Id,
	   kt.Allapot,
	   kt.LetrehozasIdo,
	   kt.ModositasIdo
  from EREC_IraKezbesitesiTetelek kt
	   inner join KRT_Csoportok cs on kt.Csoport_Id_Cel = cs.Id
 where @CFrissitesDatum >= convert(char(10),kt.ErvKezd,102)
   and @CFrissitesDatum < convert(char(10),kt.ErvVege,102)
   and (kt.Allapot in ('1','2') or
		kt.Allapot = '3' and convert(char(10),kt.ModositasIdo,102) = @CFrissitesDatum)
   and (@Szukites = 'N' or
	    @Szukites = 'I' and
		(kt.Allapot = '1' and kt.Felhasznalo_Id_Atado_USER = @Felhasznalo_Id or
		 kt.Allapot <> '1' and (kt.Csoport_Id_Cel = @Felhasznalo_Id or kt.Csoport_Id_Cel = @Csoport_Id)))
--   and kt.LetrehozasIdo <= @FrissitesIdo
   and kt.LetrehozasIdo > @IdoszamitasKezd

select @rowcount = @@rowcount
print 'Kézbesítési tételek: '+ convert(varchar(10),@rowcount)

--
-- Kézbesítési tételek töltése
--
insert eAdatpiac.FeladatLoad
	  (FrissitesIdo,
	   FeladatKod,
	   FelelosSzervezet_Id,
	   FelelosFelhasznalo_Id,
	   Prioritas,
	   LejaratDatum,
	   FeladatJel,
	   EredetKod,
	   KeletkezesKod,
	   KezelesSzintKod,
	   Funkcio_Id_Inditando,
	   Obj_Id,
	   Obj_Type,
	   Kiado_Id)
select @FrissitesIdo,
	   case when hf.Allapot = '2' then '11%'
			when hf.Allapot = '3' then '12%'
			when hf.Allapot = '1' then '13%'
		end as FeladatKod,
	   case when hf.Allapot = '1' then hf.Csoport_Id_Kiado
			else hf.Csoport_Id_Felelos
		end as Csoport_Id_Felelos,
	   case when hf.Allapot = '1' then hf.Felhasznalo_Id_Kiado
			else hf.Felhasznalo_Id_Felelos
		end as Felhasznalo_Id_Felelos,
	   hf.Prioritas,
	   hf.IntezkHatarido as LejaratDatum,
	   'N' as Feladatjel,
	   case when hf.Obj_Type = 'EREC_KuldKuldemenyek' and kd.PostazasIranya = '1' then 'K'
			when hf.Allapot = '1' then 'S'
			else NULL end as EredetKod,
	   case when hf.Allapot = '2' and convert(char(10),hf.ModositasIdo,102) = @CFrissitesDatum then 'U'
			when hf.Allapot <> '2' and convert(char(10),hf.LetrehozasIdo,102) = @CFrissitesDatum then 'U'
			else 'R' end as KeletkezesKod,
	   case when hf.Obj_Type = 'EREC_UgyUgyiratok'     and ui.Allapot in('0','1','2') then 'M'
			when hf.Obj_Type = 'EREC_PldIratPeldanyok' and pl.Allapot in('0','1','2') then 'M'
			else 'N' end as KezelesSzintKod,
	   hf.Funkcio_Id_Inditando,
	   hf.Tetel_Id,
	   hf.Obj_Type,
	   case when hf.Allapot in ('2','3') then Felhasznalo_Id_Kiado
			else NULL end as Kiado_Id
  from @HataridosFeladatok hf
 	   left outer join EREC_KuldKuldemenyek kd
			on hf.Obj_Type  = 'EREC_KuldKuldemenyek'
			and hf.Obj_Id = kd.Id
	   left outer join EREC_UgyUgyiratok ui
			on hf.Obj_Type  = 'EREC_UgyUgyiratok'
			and hf.Obj_Id = ui.Id and ui.Allapot <> @IrattarbaKuldott
	   left outer join EREC_PldIratPeldanyok pl
			on hf.Obj_Type  = 'EREC_PldIratPeldanyok'
			and hf.Obj_Id = pl.Id

print convert(varchar(25),getdate(),126)+ ': Kézbesítési tételek feldolgozása kész'
--------------------------------
-- Irattári kikérők feldolgozása
--------------------------------
delete @HataridosFeladatok
delete @FeladatDef
--
-- ELőkészítés
--
declare @Funkcio_Id_InditandoKolcsJovah uniqueidentifier,
	    @Funkcio_Id_InditandoKolcsLejar uniqueidentifier,
		@AtfutasMinKolcsJovah			 int,
		@AtfutasMinKolcsLejar			 int

insert into @FeladatDef
exec [dbo].[sp_WF_UtemezettTrigger] 'LejartKolcsonzesJovahagyas', 'I'
select @Funkcio_Id_InditandoKolcsJovah = Funkcio_Id_Inditando,
	   @Obj_Type			           = Obj_Type,
	   @AtfutasMinKolcsJovah = -datediff(mi, Kezdesido, IntezkHatarido)
  from @FeladatDef
--select @Funkcio_Id_InditandoLejart f, @Obj_Type o, @AtfutasMin a

-- Irattárosok leválogatása
select cs.Id as Csoport_Id, fs.Felhasznalo_Id, count(*) as Db
 into #Irattarosok
 from KRT_Funkciok fu
      inner join KRT_Szerepkor_Funkcio sf on fu.Id = sf.Funkcio_Id and sf.ErvVege > getdate()
      inner join KRT_Felhasznalo_Szerepkor fs on sf.Szerepkor_Id = fs.Szerepkor_Id and fs.ErvVege > getdate()
      inner join KRT_Felhasznalok fh on fs.Felhasznalo_Id = fh.Id and fh.ErvVege > getdate()
      inner join KRT_CsoportTagok ct on fs.Felhasznalo_Id = ct.Csoport_Id_Jogalany and ct.Tipus = '2' and ct.ErvVege > getdate()
      inner join KRT_Csoportok cs on ct.Csoport_Id = cs.Id and cs.Nev = 'Központi Ügyiratkezelési Alosztály'
 where fu.Kod = 'IrattarKolcsonzesKiadasa'
   and fu.ErvVege > getdate()
group by cs.Id, fs.Felhasznalo_Id

--
-- Irattári kikérők leválogatása
--
insert @HataridosFeladatok
select ik.Id as Tetel_Id,
       @Funkcio_Id_Inditando as Funkcio_Id_Inditando,
	   @Csoport_Id as Csoport_Id_Felelos,
	   ik.FelhasznaloCsoport_Id_Jovahagy as Felhasznalo_Id_Felelos,
	   NULL as Csoport_Id_Kiado,
	   ik.FelhasznaloCsoport_Id_Kikero as Felhasznalo_Id_Kiado,
	   NULL as Prioritas,
	   case when ik.Allapot = '02' then dateadd(mi, @AtfutasMinKolcsJovah, ik.KeresDatuma)
			 end as IntezkHatarido,
  	   ik.UgyUgyirat_Id as Obj_Id,
	   'EREC_UgyUgyiratok' as Obj_Type,
	   NULL as Kiado_Id,
	   ik.Allapot,
	   ik.LetrehozasIdo,
	   ik.ModositasIdo
  from EREC_IrattariKikero ik
 where @FrissitesIdo between ik.ErvKezd and ik.ErvVege
   and ik.Allapot in ('02','03') -- 02-Engedélyezhető, '03'-Engedélyezett
   and ik.FelhasznaloCsoport_Id_Jovahagy is not NULL
   and (@Szukites = 'N' or
	    @Szukites = 'I' and ik.Allapot = '02' and ik.FelhasznaloCsoport_Id_Jovahagy = @Felhasznalo_Id or
        @Szukites = 'I' and ik.Allapot = '03' and @Felhasznalo_Id in (select Felhasznalo_Id from #Irattarosok))
   and ik.Irattar_Id in	(select Obj_Id
						   from KRT_KodTarak 
						  where Kod = 'SPEC_SZERVEK.KOZPONTIIRATTAR'
							and GETDATE() BETWEEN ErvKezd AND ErvVege
							and (@Szukites = 'N' or Org=@Org))
--   and ik.LetrehozasIdo <= @FrissitesIdo

select @rowcount = @@rowcount
print 'Irattári kikérők: '+ convert(varchar(10),@rowcount)

--
-- Jóváhagyandó kölcsönzések töltése
--
insert eAdatpiac.FeladatLoad
	  (FrissitesIdo,
	   FeladatKod,
	   FelelosSzervezet_Id,
	   FelelosFelhasznalo_Id,
	   Prioritas,
	   LejaratDatum,
	   FeladatJel,
	   EredetKod,
	   KeletkezesKod,
	   KezelesSzintKod,
	   Funkcio_Id_Inditando,
	   Obj_Id,
	   Obj_Type,
	   Kiado_Id)
select @FrissitesIdo,
	   '22%' as FeladatKod,
	   hf.Csoport_Id_Felelos,
	   hf.Felhasznalo_Id_Felelos,
	   hf.Prioritas,
	   hf.IntezkHatarido as LejaratDatum,
	   'N' as Feladatjel,
	   'H' as EredetKod,
	   case when convert(char(10),hf.ModositasIdo,102) = @CFrissitesDatum then 'U'
			else 'R' end as KeletkezesKod,
	   'N' as KezelesSzintKod,
	   hf.Funkcio_Id_Inditando,
	   hf.Tetel_Id,
	   hf.Obj_Type,
	   Felhasznalo_Id_Kiado
  from @HataridosFeladatok hf
	   left outer join EREC_UgyUgyiratok ui
			on hf.Obj_Type  = 'EREC_UgyUgyiratok' and hf.Obj_Id = ui.Id
 where hf.Allapot = '02'

select @rowcount = @@rowcount
print 'Jóváhagyandó irattári kikérők: '+ convert(varchar(10),@rowcount)

--
-- Jóváhagyott (kiadandó) kölcsönzések töltése
--
insert eAdatpiac.FeladatLoad
	  (FrissitesIdo,
	   FeladatKod,
	   FelelosSzervezet_Id,
	   FelelosFelhasznalo_Id,
	   Prioritas,
	   LejaratDatum,
	   FeladatJel,
	   EredetKod,
	   KeletkezesKod,
	   KezelesSzintKod,
	   Funkcio_Id_Inditando,
	   Obj_Id,
	   Obj_Type,
	   Kiado_Id)
select @FrissitesIdo,
	   '46%' as FeladatKod,
	   ir.Csoport_Id as Csoport_Id_Felelos,
	   ir.Felhasznalo_Id as Felhasznalo_Id_Felelos,
	   hf.Prioritas,
	   hf.IntezkHatarido as LejaratDatum,
	   'N' as Feladatjel,
	   'H' as EredetKod,
	   case when convert(char(10),hf.ModositasIdo,102) = @CFrissitesDatum then 'U'
			else 'R' end as KeletkezesKod,
	   'N' as KezelesSzintKod,
	   hf.Funkcio_Id_Inditando,
	   hf.Tetel_Id,
	   hf.Obj_Type,
	   Felhasznalo_Id_Kiado
  from @HataridosFeladatok hf
	   left outer join EREC_UgyUgyiratok ui
			on hf.Obj_Type  = 'EREC_UgyUgyiratok' and hf.Obj_Id = ui.Id
       inner join #Irattarosok ir on (@Szukites = 'N' or  -- a feladat mindenkié
									  @Szukites = 'I' and ir.Felhasznalo_Id = @Felhasznalo_Id)
where hf.Allapot = '03'

print convert(varchar(25),getdate(),126)+ ': Irattári kikérők feldolgozása kész'
-------------------------
-- Ügyiratok feldolgozása
-------------------------
delete @HataridosFeladatok
--
-- ELőkészítés
--
declare @Funkcio_Id_InditandoIrattarJovah uniqueidentifier,
		@AtfutasMinIrattarJovah			  int,
	    @Funkcio_Id_InditandoSkontroJovah uniqueidentifier,
		@AtfutasMinSkontroJovah			  int,
	    @Funkcio_Id_UgyiratModify         uniqueidentifier

delete @FeladatDef
insert into @FeladatDef
exec [dbo].[sp_WF_UtemezettTrigger] 'LejartIrattarozasJovahagyas', 'I'
select @Funkcio_Id_InditandoIrattarJovah = Funkcio_Id_Inditando,
	   @Obj_Type			             = Obj_Type,
	   @AtfutasMinIrattarJovah = -datediff(mi, Kezdesido, IntezkHatarido)
  from @FeladatDef

delete @FeladatDef
insert into @FeladatDef
exec [dbo].[sp_WF_UtemezettTrigger] 'LejartSkontroJovahagyas', 'I'
select @Funkcio_Id_InditandoSkontroJovah = Funkcio_Id_Inditando,
	   @AtfutasMinSkontroJovah = -datediff(mi, Kezdesido, IntezkHatarido)
  from @FeladatDef

select @Funkcio_Id_UgyiratModify = Id
  from KRT_Funkciok
 where Kod = 'UgyiratModify'

--
-- Ügyiratok leválogatása
--
insert @HataridosFeladatok
select ui.Id as Tetel_Id,
       case when ui.Allapot = '52' then @Funkcio_Id_InditandoIrattarJovah
			when ui.Allapot = '70' then @Funkcio_Id_InditandoSkontroJovah
			else @Funkcio_Id_UgyiratModify
		end as Funkcio_Id_Inditando,
	   @Csoport_Id as Csoport_Id_Felelos,
	   ui.Kovetkezo_Felelos_Id as Felhasznalo_Id_Felelos,
	   @Csoport_Id as Csoport_Id_Kiado,
	   ui.FelhasznaloCsoport_Id_Orzo as Felhasznalo_Id_Kiado,
	   NULL as Prioritas,
	   case when ui.Allapot = '52' then dateadd(mi, @AtfutasMinIrattarJovah, ui.ModositasIdo)
			when ui.Allapot = '70' then dateadd(mi, @AtfutasMinSkontroJovah, ui.SkontrobaDat)
			else ui.Hatarido
			 end as IntezkHatarido,
  	   ui.Id as Obj_Id,
	   'EREC_UgyUgyiratok' as Obj_Type,
	   NULL as Kiado_Id,
	   ui.Allapot,
	   ui.LetrehozasIdo,
 	   case when ui.Allapot in ('52','70') then ui.Hatarido
			else ui.ModositasIdo
			 end as ModositasIdo
 from EREC_UgyUgyiratok ui
	  inner join KRT_Csoportok cs on ui.FelhasznaloCsoport_Id_Orzo = cs.Id and cs.Tipus = '1'
 where @FrissitesIdo between ui.ErvKezd and ui.ErvVege
   and ui.Allapot in ('52','70' -- Irattározás jóváhagyás, Skontróba helyezés jóváhagyás
                      ,'0','03','04','06','07','09','11','13','50','57','60','99')
   and (@Szukites = 'N' or
	    @Szukites = 'I' and
        (ui.Allapot in ('52','70') and @Felhasznalo_Id = ui.Kovetkezo_Felelos_Id or 
         ui.Allapot not in ('52','70') and @Felhasznalo_Id = ui.FelhasznaloCsoport_Id_Orzo))
--   and ui.LetrehozasIdo <= @FrissitesIdo

select @rowcount = @@rowcount
print 'Ügyiratok: '+ convert(varchar(10),@rowcount)

--
-- Jóváhagyandó ügyirat tételek töltése
--
insert eAdatpiac.FeladatLoad
	  (FrissitesIdo,
	   FeladatKod,
	   FelelosSzervezet_Id,
	   FelelosFelhasznalo_Id,
	   Prioritas,
	   LejaratDatum,
	   FeladatJel,
	   EredetKod,
	   KeletkezesKod,
	   KezelesSzintKod,
	   Funkcio_Id_Inditando,
	   Obj_Id,
	   Obj_Type,
	   Kiado_Id)
select @FrissitesIdo,
	   case when Allapot = '52' then'21%' -- Irattározás jóváhagyása
			when Allapot = '70' then'23%' -- Skontro jóváhagyása
		end as FeladatKod,
       Csoport_Id_Felelos,
	   Felhasznalo_Id_Felelos,
	   Prioritas,
	   IntezkHatarido as LejaratDatum,
	   'N' as Feladatjel,
	   'S' as EredetKod,
	   case when convert(char(10),ModositasIdo,102) = @CFrissitesDatum then 'U'
			else 'R' end as KeletkezesKod,
	   case when Allapot in('0','1','2') then 'M'
			else 'N' end as KezelesSzintKod,
	   Funkcio_Id_Inditando,
	   Tetel_Id,
	   Obj_Type,
	   Felhasznalo_Id_Kiado
from @HataridosFeladatok
 where Allapot in('52','70')

--
-- Lejárati ügyirat tételek töltése
--
insert eAdatpiac.FeladatLoad
	  (FrissitesIdo,
	   FeladatKod,
	   FelelosSzervezet_Id,
	   FelelosFelhasznalo_Id,
	   Prioritas,
	   LejaratDatum,
	   FeladatJel,
	   EredetKod,
	   KeletkezesKod,
	   KezelesSzintKod,
	   Funkcio_Id_Inditando,
	   Obj_Id,
	   Obj_Type)
select @FrissitesIdo,
	   '42%' as FeladatKod,
	   Csoport_Id_Kiado,
	   Felhasznalo_Id_Kiado,
	   Prioritas,
	   case when Allapot in('52','70') then ModositasIdo
			else IntezkHatarido
		end as LejaratDatum,
	   'N' as Feladatjel,
	   'S' as EredetKod,
	   case when convert(char(10),ModositasIdo,102) = @CFrissitesDatum then 'U'
			else 'R' end as KeletkezesKod,
	   case when Allapot in('0','1','2') then 'M'
			else 'N' end as KezelesSzintKod,
	   @Funkcio_Id_UgyiratModify as Funkcio_Id_Inditando,
	   Tetel_Id,
	   Obj_Type
  from @HataridosFeladatok
 where Felhasznalo_Id_Kiado = @Felhasznalo_Id

print convert(varchar(25),getdate(),126)+ ': Ügyiratok feldolgozása kész'
---------------------------
-- Küldemények feldolgozása
---------------------------
delete @HataridosFeladatok
--
-- ELőkészítés
--
declare @Funkcio_Id_InditandoKuldemenyMod uniqueidentifier,
		@AtfutasMinKuldemenyMod		      int

delete @FeladatDef
insert into @FeladatDef
exec [dbo].[sp_WF_UtemezettTrigger] 'LejartKuldemenyKezeles', 'I'
select @Funkcio_Id_InditandoKuldemenyMod = Funkcio_Id_Inditando,
	   @Obj_Type			             = Obj_Type,
	   @AtfutasMinKuldemenyMod = -datediff(mi, Kezdesido, IntezkHatarido)
  from @FeladatDef

--
-- Küldemények leválogatása
--
insert @HataridosFeladatok
select kd.Id as Tetel_Id,
       @Funkcio_Id_InditandoKuldemenyMod as Funkcio_Id_Inditando,
	   @Csoport_Id as Csoport_Id_Felelos,
	   kd.FelhasznaloCsoport_Id_Orzo as Felhasznalo_Id_Felelos,
	   NULL as Csoport_Id_Kiado,
	   NULL  as Felhasznalo_Id_Kiado,
	   NULL as Prioritas,
	   dateadd(mi, @AtfutasMinKuldemenyMod, kd.LetrehozasIdo) as IntezkHatarido,
  	   kd.Id as Obj_Id,
	   'EREC_KuldKuldemenyek' as Obj_Type,
	   NULL as Kiado_Id,
	   kd.Allapot,
	   kd.LetrehozasIdo,
	   kd.ModositasIdo
  from EREC_KuldKuldemenyek kd
 where @FrissitesIdo between kd.ErvKezd and kd.ErvVege
   and kd.Allapot in ('00','01','03','05','51','52')
   and (@Szukites = 'N' or
	    @Szukites = 'I' and @Felhasznalo_Id = kd.FelhasznaloCsoport_Id_Orzo)
--   and kd.LetrehozasIdo <= @FrissitesIdo
   and kd.LetrehozasIdo >= @IdoszamitasKezd

select @rowcount = @@rowcount
print 'Küldemények: '+ convert(varchar(10),@rowcount)

--
-- Lejárati küldemény tételek töltése
--
insert eAdatpiac.FeladatLoad
	  (FrissitesIdo,
	   FeladatKod,
	   FelelosSzervezet_Id,
	   FelelosFelhasznalo_Id,
	   Prioritas,
	   LejaratDatum,
	   FeladatJel,
	   EredetKod,
	   KeletkezesKod,
	   KezelesSzintKod,
	   Funkcio_Id_Inditando,
	   Obj_Id,
	   Obj_Type)
select @FrissitesIdo,
	   case when Allapot in('51','52')then '44%' else '41%' end as FeladatKod,
	   Csoport_Id_Felelos,
	   Felhasznalo_Id_Felelos,
	   Prioritas,
	   IntezkHatarido as LejaratDatum,
	   'N' as Feladatjel,
	   'S' as EredetKod,
	   case when convert(char(10),ModositasIdo,102) = @CFrissitesDatum then 'U'
			else 'R' end as KeletkezesKod,
	   case when Allapot in('0','1','2') then 'M'
			else 'N' end as KezelesSzintKod,
	   @Funkcio_Id_UgyiratModify as Funkcio_Id_Inditando,
	   Tetel_Id,
	   Obj_Type
from @HataridosFeladatok 

print convert(varchar(25),getdate(),126)+ ': Küldemények feldolgozása kész'
-----------------------------
-- Iratpéldányok feldolgozása
-----------------------------
delete @HataridosFeladatok
--
-- ELőkészítés
--
declare @Funkcio_Id_IratPeldanyMod uniqueidentifier,
		@AtfutasMinIratPeldanyMod		      int

delete @FeladatDef
insert into @FeladatDef
exec [dbo].[sp_WF_UtemezettTrigger] 'LejartIratPeldanyKezeles', 'I'
select @Funkcio_Id_IratPeldanyMod = Funkcio_Id_Inditando,
	   @AtfutasMinIratPeldanyMod  = -datediff(mi, Kezdesido, IntezkHatarido)
  from @FeladatDef

--
-- Iratpéldányok leválogatása
--
insert @HataridosFeladatok
select pl.Id as Tetel_Id,
       @Funkcio_Id_IratPeldanyMod as Funkcio_Id_Inditando,
	   @Csoport_Id as Csoport_Id_Felelos,
	   pl.FelhasznaloCsoport_Id_Orzo as Felhasznalo_Id_Felelos,
	   NULL as Csoport_Id_Kiado,
	   NULL as Felhasznalo_Id_Kiado,
	   NULL as Prioritas,
	   isnull(dateadd(mi, @AtfutasMinIratPeldanyMod, ir.Hatarido),getdate()+1111) as IntezkHatarido,
  	   pl.Id as Obj_Id,
	   'EREC_PldIratPeldanyok' as Obj_Type,
	   NULL as Kiado_Id,
	   pl.Allapot,
	   pl.LetrehozasIdo,
	   pl.ModositasIdo
  from EREC_PldIratpeldanyok pl
	   inner join EREC_IraIratok ir on pl.IraIrat_Id = ir.Id
	   inner join EREC_UgyUgyiratdarabok ud on ir.UgyUgyiratDarab_id = ud.Id
	   inner join EREC_UgyUgyiratok ui on ud.UgyUgyirat_id = ui.Id
	   inner join KRT_Csoportok cs on pl.FelhasznaloCsoport_Id_Orzo = cs.Id and cs.Tipus = '1'
 where @FrissitesIdo between pl.ErvKezd and pl.ErvVege
   and pl.Allapot in ('0','04','30','50','60')
   and pl.FelhasznaloCsoport_Id_Orzo <> ui.FelhasznaloCsoport_Id_Orzo -- szóló pld
   and (@Szukites = 'N' or
	    @Szukites = 'I' and @Felhasznalo_Id = pl.FelhasznaloCsoport_Id_Orzo)
--   and f.LetrehozasIdo <= @FrissitesIdo
select @rowcount = @@rowcount
print 'Iratpéldányok: '+ convert(varchar(10),@rowcount)

--
-- Lejárati iratpéldány tételek töltése
--
insert eAdatpiac.FeladatLoad
	  (FrissitesIdo,
	   FeladatKod,
	   FelelosSzervezet_Id,
	   FelelosFelhasznalo_Id,
	   Prioritas,
	   LejaratDatum,
	   FeladatJel,
	   EredetKod,
	   KeletkezesKod,
	   KezelesSzintKod,
	   Funkcio_Id_Inditando,
	   Obj_Id,
	   Obj_Type)
select @FrissitesIdo,
	   '43%' as FeladatKod,
	   Csoport_Id_Felelos,
	   Felhasznalo_Id_Felelos,
	   Prioritas,
	   IntezkHatarido as LejaratDatum,
	   'N' as Feladatjel,
	   'S' as EredetKod,
	   case when convert(char(10),LetrehozasIdo,102) = @CFrissitesDatum then 'U'
			else 'R' end as KeletkezesKod,
	   case when Allapot in('0','1','2') then 'M'
			else 'N' end as KezelesSzintKod,
	   @Funkcio_Id_UgyiratModify as Funkcio_Id_Inditando,
	   Tetel_Id,
	   Obj_Type
from @HataridosFeladatok 

print convert(varchar(25),getdate(),126)+ ': Iratpéldányok feldolgozása kész'
-------------------------------
-- Kimeneti adatok kiegészítése
-------------------------------
--
-- Kimenet update
--
update eAdatpiac.FeladatLoad set
	   FelelosSzervezet_Id =
			case when fl.FelelosSzervezet_Id is NULL and fl.FelelosFelhasznalo_Id is not NULL and @Csoport_Id is not NULL
					  then @Csoport_Id
				 when fl.FelelosSzervezet_Id is NULL and fl.FelelosFelhasznalo_Id is not NULL and fc.Felhasznalo_Id is not NULL
					  then fc.Csoport_Id
				 when fl.FelelosSzervezet_Id is NULL and fl.FelelosFelhasznalo_Id is not NULL
					  then (select Csoport_Id_Felelos from dbo.fn_WF_GetFelelos(fl.FelelosFelhasznalo_Id, NULL))
				 else fl.FelelosSzervezet_Id end,
	   FelelosFelhasznalo_Id = isnull(fl.FelelosFelhasznalo_Id,'00000000-0000-0000-0000-000000000000'),
	   FeladatKod = df.FeladatKod,
	   EredetKod  = isnull(fl.EredetKod,'H'),
	   Prioritas  = isnull(fl.prioritas,df.AlapPrioritas),
	   LejaratDatum = isnull(LejaratDatum, getdate()+1000)
  from eAdatpiac.FeladatLoad fl
	   inner join eAdatpiac.Dim_FeladatTipus df
			on df.FeladatKod like fl.FeladatKod and isnull(df.Obj_Type,'_') = isnull(fl.Obj_Type,'_')
	   left outer join (select Csoport_Id, Felhasznalo_Id from dbo.fn_WF_GetFelhasznaloCsoport(@Felhasznalo_Id)) fc
			on fl.FelelosFelhasznalo_Id = fc.Felhasznalo_Id

print convert(varchar(25),getdate(),126)+ ': Kimeneti adatok kiegészítése kész'
--RAISERROR('Teszt!',16,1)

-- Vége --
set @error = 0
select @error OK

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------


GO
