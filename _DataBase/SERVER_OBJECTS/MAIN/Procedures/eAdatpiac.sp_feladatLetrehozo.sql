IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[sp_feladatLetrehozo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [eAdatpiac].[sp_feladatLetrehozo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[sp_feladatLetrehozo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [eAdatpiac].[sp_feladatLetrehozo] AS' 
END
GO
ALTER procedure [eAdatpiac].[sp_feladatLetrehozo]
/* Feladata: az eAdatpiac frissítési bejegyzés létrehozása adott szervezetre/felhaszálóra.
            Az eljárás beolvassa a paramétertábla FELADATOSSZESITO_FRISSITES_MIN_PERC értékét, 
            és megvizsgálja, hogy ezen az időn belül van e feldolgozatlan eAdatpiac.FeladatIgeny rekord, 
            ha nincs, megvizsgálja, hogy ezen az időtartamon belül futott e frissítés eljárás. Ezt a dim_felelosfelhasznalo 
            táblából olvassa ki. Ha van ilyen (bármelyik esetre), akkor hibaágon visszatér.           
*/             
    @szervezet as varchar(64) = null,   
    @felhasznalo as varchar(64) = null,
	@indito_id as varchar(64) = null,
	@LastUpdateTime datetime OUTPUT    
as
begin try

  set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(convert(uniqueidentifier, @felhasznalo))
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

  set @LastUpdateTime = null

  -- paraméterek ellenőrzése (nem lehet üres vagy null)
  IF (@indito_id is null or @indito_id='')
		RAISERROR('[80005]',16,1) -- Nincs megadva a végrehajtó felhasználó

  IF (@felhasznalo is null or @felhasznalo='')
		RAISERROR('[80006]',16,1) -- Nincs megadva a felhasználó, akire az adatokat frissíteni kell

  IF (@szervezet is null or @szervezet='')
		RAISERROR('[80007]',16,1) -- Nincs megadva a szervezet, amire az adatokat frissíteni kell

  declare @task as varchar(2000)
  declare @idopont datetime

  declare @futasiGyakorisag int        -- percben, paraméter táblából
  declare @FeldolgozatlanUtolso datetime -- az utolsó feldolgozatlan kérés időpontja
  declare @feldolgozottUtolso  datetime   -- az utolsó lefutott feldolgozás időpontja
  declare @elteltIdo           int      -- az utolsó futás óta eltelt idő (percben)
  
  set @idopont = getdate()  -- aktuális időpont

  select @futasiGyakorisag=ertek from dbo.KRT_Parameterek
      where Nev='FELADATOSSZESITO_FRISSITES_MIN_PERC' and Org=@Org
 
  -- ha nincs paraméter, 10 perc lesz a frissitési gyakorisága
  set @futasiGyakorisag = isnull(@futasiGyakorisag,10)
  print 'futásgyakoriság='+ cast(@futasiGyakorisag as varchar(10))
  
  -- van e a felhasználónak @futasiGyakorisag percen belüli feldolgozatlan igénye ?
  select top 1 @FeldolgozatlanUtolso = keresdatuma 
  from eAdatpiac.FeladatIgeny 
     where allapot in (0,1) and osszesre = 0 and felhasznalo_id = @felhasznalo and szervezet_id=@szervezet
     order by keresdatuma desc
  set @FeldolgozatlanUtolso=isnull(@FeldolgozatlanUtolso,dateadd(day,-1,getdate()))
  set @elteltIdo=datediff(minute, @FeldolgozatlanUtolso, @idopont)
  print 'utolsó feldolgozatlan='+cast(@FeldolgozatlanUtolso as varchar(20))
  print '--eltelt idő (perc)='+cast (@elteltido as varchar(10))
  -- todo: visszatérés
  if @elteltIdo < @futasiGyakorisag
  begin
       set @LastUpdateTime = @FeldolgozatlanUtolso
       RAISERROR('[80001]',16,1)   -- kevesebb idő telt el...
   end
  
  -- van e a felhasználónak feldolgozott kérése a futásgyakoriságon belül (Dim_FeladatFelelos alapján)
  select @feldolgozottUtolso = FrissitesIdo  
  from eAdatpiac.Dim_FeladatFelelos
       where felhasznalo_id = @felhasznalo and szervezet_id=@szervezet
  set @elteltIdo=datediff(minute, @FeldolgozottUtolso, @idopont)
  print 'utolsó feldolgozott='+cast(@feldolgozottUtolso as varchar(20))
  print '--eltelt idő (perc)='+cast (@elteltido as varchar(10))
  -- todo: ha időszakon belül, visszatérés...
  if @elteltIdo < @futasiGyakorisag
  begin
       set @LastUpdateTime = @feldolgozottUtolso
       RAISERROR('[80002]',16,1)   -- kevesebb idő telt el...
  end

  -- van e a felhasználónak feldolgozott kérése a futásgyakoriságon belül (FeladatIgeny alapján, ha nincs bejegyzés a Dim_FeladatFelelosben)
  select top 1 @feldolgozottUtolso = keresdatuma 
  from eAdatpiac.FeladatIgeny 
     where allapot = 2 and osszesre = 0 and felhasznalo_id = @felhasznalo and szervezet_id=@szervezet
     order by keresdatuma desc
  set @elteltIdo=datediff(minute, @FeldolgozottUtolso, @idopont)
  print 'utolsó feldolgozott='+cast(@feldolgozottUtolso as varchar(20))
  print '--eltelt idő (perc)='+cast (@elteltido as varchar(10))
  -- todo: ha időszakon belül, visszatérés...
  if @elteltIdo < @futasiGyakorisag
  begin
       set @LastUpdateTime = @feldolgozottUtolso
       RAISERROR('[80002]',16,1)   -- kevesebb idő telt el...
  end

  -- fut e éppen összesítő futás?
  if exists (select 1 from eAdatpiac.FeladatIgeny 
     where allapot =1 and osszesre = 1)
       RAISERROR('[80003]',16,1)   -- éppen fut egy összesítő futás...
  
  insert into eAdatpiac.FeladatIgeny 
         (Indito_id, Szervezet_id, Felhasznalo_id, KeresDatuma, allapot,Keresevhonap,Osszesre) 
      values (@indito_id, @Szervezet, @Felhasznalo,@idopont,0, convert(nvarchar(10),@idopont,102),0) 

  -- visszatérés a bejegyzett értékkel
  set @LastUpdateTime = @idopont

end try
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH


GO
