IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_depends]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_depends]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_depends]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[p_depends] AS' 
END
GO
-- =============================================
-- Author:		Tobak Jeno
-- Create date: 2007.06.21
-- Description:	Eljárások szintjeinek mélységét
--				meghatározó program
-- =============================================
ALTER PROCEDURE [dbo].[p_depends]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	create table #tmp (
		id		integer not null,
		depid	integer null
	)

	create table #szintek
	(   
		szint       integer not null,
		id          integer not null
	)

	declare @mehet      integer,
			@szint      integer

	select @szint = 0

	insert into #tmp
	select id,
		   NULL
	  from sysobjects
	 where id not in ( select id from sysdepends )
	--   and type in ('P', 'TR')

	insert into #tmp
	select id = d.id,
		   depid = d.depid
	  from sysdepends d,
		   sysobjects o 
	 where d.id = o.id

	select @mehet = count(*)
	  from #tmp
	 where depid is NULL

	while @mehet > 1
	begin
		select @szint = @szint + 1 
	    print 'Szint: '+ convert(varchar, @szint) + ' ' + convert(varchar, @mehet)
	
		insert into #szintek
		select @szint,
			   id
		  from #tmp t
		 where depid is NULL
		   and id not in ( select id from #szintek )
	    
		update #tmp
		   set depid = null
		 where depid in ( select id from #tmp where depid is null )

		if @@rowcount = 0
			select @mehet = 0
		else
			select @mehet = count(*)
			  from #tmp
			 where depid is NULL

	end

	select szint sz,
		   object = object_name(sz.id),
		   o.type
	  from #szintek sz,
		   sysobjects o
	 where sz.id = o.id
	--   and o.type in ('TR', 'P')
	 order by 1, 2

	drop table #szintek
	drop table #tmp

END


GO
