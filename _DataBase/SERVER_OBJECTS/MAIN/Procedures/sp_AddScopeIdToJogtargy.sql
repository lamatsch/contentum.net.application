IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AddScopeIdToJogtargy]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_AddScopeIdToJogtargy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AddScopeIdToJogtargy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_AddScopeIdToJogtargy] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_AddScopeIdToJogtargy]
	(
		@JogtargyId			uniqueidentifier,
		@JogtargyTablaNev	sysname,
		@JogtargySzuloId	uniqueidentifier = null,
		@CsoportOroklesMod	char(1) = 'S',
		@OroklottJogszint	char(1) = 'I',
		@CsoportId			uniqueidentifier = null,
		@ExecutorUserId		uniqueidentifier
	)
AS
BEGIN

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	declare		@ObjTipus_Id	uniqueidentifier;
	
	select @ObjTipus_Id = Id from KRT_ObjTipusok where Kod = @JogtargyTablaNev;

	--**********KRT_Jogosultak táblába csoport hozzávétele, ha @CsoportId nem null (tulajdonosi jogviszonnyal)
	if @CsoportId is not null
		exec sp_AddCsoportToJogtargy @JogtargyId,@CsoportId,@ExecutorUserId,'N','I','T';

	--**********Jogosultság default csoport(ok)nak
	insert into KRT_Jogosultak(Csoport_Id_Jogalany,Jogszint,Obj_Id,Tipus,Kezi)
		select KRT_Kodtarak.Obj_Id,'I',@JogtargyId,'D','N'
			from KRT_Kodtarak
				inner join KRT_Kodcsoportok on KRT_Kodcsoportok.Kod = 'ACL_DEFAULT_BINDINGS'
			where KRT_Kodtarak.Kodcsoport_Id = KRT_Kodcsoportok.Id and KRT_Kodtarak.ObjTip_Id_AdatElem = @ObjTipus_Id and KRT_KodTarak.Org=@Org
END


GO
