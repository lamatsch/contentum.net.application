IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CSVExport_IratokWith_Hatosagi_es_NemHatosagi]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CSVExport_IratokWith_Hatosagi_es_NemHatosagi]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CSVExport_IratokWith_Hatosagi_es_NemHatosagi]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_CSVExport_IratokWith_Hatosagi_es_NemHatosagi] AS' 
END
GO




ALTER procedure [dbo].[sp_CSVExport_IratokWith_Hatosagi_es_NemHatosagi]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraIratok.LetrehozasIdo',
  @ExecutorUserId				uniqueidentifier
as

begin

BEGIN TRY
		set nocount on
   DECLARE @Org uniqueidentifier
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
   if (@Org is null)
   begin
      RAISERROR('[50202]',16,1)
   end

	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''
--	SET @sqlcmd =  N'
--   select 
      
--      EREC_IraIratok.Id into #result
--       from EREC_IraIratok as EREC_IraIratok
--         LEFT JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok
--            ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
--         LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok 
--               ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id  
--         LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek 
--               ON EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id  
--         LEFT JOIN [EREC_KuldKuldemenyek] AS EREC_KuldKuldemenyek
--            ON EREC_IraIratok.[KuldKuldemenyek_Id] = EREC_KuldKuldemenyek.Id
--         left join KRT_Kodcsoportok as AllapotKodCsoport on AllapotKodCsoport.Kod = ''IRAT_ALLAPOT''
--            left join KRT_Kodtarak as AllapotKodTarak on AllapotKodTarak.Kodcsoport_Id = AllapotKodCsoport.Id and AllapotKodTarak.Kod = EREC_IraIratok.Allapot and AllapotKodTarak.Org=@Org
--         left join KRT_Kodcsoportok as AdathordozoTipusaKodCsoport on AdathordozoTipusaKodCsoport.Kod = ''UGYINTEZES_ALAPJA''
--            left join KRT_Kodtarak as AdathordozoTipusaKodTarak on AdathordozoTipusaKodTarak.Kodcsoport_Id = AdathordozoTipusaKodCsoport.Id and AdathordozoTipusaKodTarak.Kod = EREC_IraIratok.AdathordozoTipusa and AdathordozoTipusaKodTarak.Org=@Org
--         left join KRT_Kodcsoportok as KuldesModjaKodCsoport on KuldesModjaKodCsoport.Kod = ''KULDES_MODJA''
--            left join KRT_Kodtarak as KuldesModjaKodTarak on KuldesModjaKodTarak.Kodcsoport_Id = KuldesModjaKodCsoport.Id and KuldesModjaKodTarak.Kod = EREC_KuldKuldemenyek.KuldesMod and KuldesModjaKodtarak.Org =@Org
--		 left join KRT_Kodcsoportok as KezbesitesModjaKodCsoport on KezbesitesModjaKodCsoport.Kod = ''KEZBESITES_MODJA''
--            left join KRT_Kodtarak as KezbesitesModjaKodTarak on KezbesitesModjaKodTarak.Kodcsoport_Id = KezbesitesModjaKodCsoport.Id and KezbesitesModjaKodTarak.Kod collate database_default = EREC_KuldKuldemenyek.KezbesitesModja collate database_default and KezbesitesModjaKodtarak.Org =@Org
--		 left join KRT_Csoportok as Csoportok_IktatoNev on Csoportok_IktatoNev.Id = EREC_IraIratok.FelhasznaloCsoport_Id_Iktato
--         left join KRT_Csoportok as Csoportok_UgyintezoNev on Csoportok_UgyintezoNev.Id = EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez
--			left join KRT_Kodcsoportok as PostazasIranyaKodCsoport on PostazasIranyaKodCsoport.Kod = ''POSTAZAS_IRANYA''	-- nekrisz CR 2997 Postazas iránya szöveggel
--		 left join KRT_Kodtarak as PostazasIranyaKodTarak on PostazasIranyaKodTarak.Kodcsoport_Id = PostazasIranyaKodCsoport.Id and PostazasIranyaKodTarak.Kod  collate database_default = EREC_IraIratok.PostazasIranya  collate database_default and PostazasIranyaKodTarak.Org=@Org
         
--   OUTER APPLY (select top 1 Allapot,
--      (select top 1 Nev from KRT_Kodtarak kt where kt.Kod = p.Allapot and kt.Org = @Org
--      and kt.Kodcsoport_Id = (select top 1 Id from KRT_Kodcsoportok kcs where kcs.Kod = ''IRATPELDANY_ALLAPOT'')) as Allapot_Nev,
--      (select top 1 Nev from KRT_Csoportok cs where p.FelhasznaloCsoport_Id_Orzo=cs.Id and getdate() between cs.ErvKezd and cs.ErvVege) as Orzo_Nev,
--      (select top 1 Nev from KRT_Csoportok cs where p.Csoport_Id_Felelos=cs.Id and getdate() between cs.ErvKezd and cs.ErvVege) as Felelos_Nev,
--      BarCode from EREC_PldIratPeldanyok p
--      where IraIrat_Id=EREC_IraIratok.Id and Sorszam=1
--   ) EREC_PldIratPeldanyok
--'

	SET @sqlcmd = 
  'select 
	EREC_IraIratok.Azonosito as "Iktatószám",
	EREC_IraIratok.Targy as "Tárgy",
	dbo.fn_KodtarErtekNeve(''UGYINTEZES_ALAPJA'', EREC_IraIratok.AdathordozoTipusa,@Org) as "Adathordozó",
	dbo.fn_GetCsoportNev(EREC_IraIratok.FelhasznaloCsoport_Id_Iktato) as "Iktató",   
	dbo.fn_GetCsoportNev(EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez) as "Ügyintéző",
      dbo.fn_KodtarErtekNeve(''POSTAZAS_IRANYA'',  EREC_IraIratok.PostazasIranya,@Org) as "Postázás Iránya",
      CONVERT(nvarchar(10), EREC_IraIratok.IktatasDatuma, 102) as "Iktatás Időpontja", 
	  CONVERT(nvarchar(10), EREC_IraIratok.SztornirozasDat, 102) as "Sztornírozva",
	  EREC_IraIratok.Munkaallomas as "Munkaállomás",
	  dbo.fn_KodtarErtekNeve(''IRAT_ALLAPOT'', EREC_IraIratok.Allapot,@Org) as "Állapot",
	  
	  dbo.fn_KodtarErtekNeve(''UGY_FAJTAJA'', EREC_IraOnkormAdatok.Ugyfajtaja,@Org) as "Ügyfajta",
	  dbo.fn_KodtarErtekNeve(''DONTEST_HOZTA'', EREC_IraOnkormAdatok.DontestHozta,@Org) as "Döntést Hozta",
	  IsNull(dbo.fn_KodtarErtekNeve(''DONTES_FORMAJA_ONK'', EREC_IraOnkormAdatok.DontesFormaja,@Org), dbo.fn_KodtarErtekNeve(''DONTES_FORMAJA_ALLAMIG'', EREC_IraOnkormAdatok.DontesFormaja,@Org)) as "Döntés Formája",
	  dbo.fn_KodtarErtekNeve(''UGYINTEZES_IDOTARTAMA'', EREC_IraOnkormAdatok.UgyintezesHataridore,@Org) as "Ügyintézés Határidőre",
	  EREC_IraOnkormAdatok.HataridoTullepes "Határidő Túllépés",
	  dbo.fn_KodtarErtekNeve(''JOGORVOSLATI_ELJARAS_TIPUSA'', EREC_IraOnkormAdatok.JogorvoslatiEljarasTipusa,@Org) as "Jogorvoslati Eljárás Típusa",
	  dbo.fn_KodtarErtekNeve(''JOGORVOSLATI_DONTES_TIPUSA'', EREC_IraOnkormAdatok.JogorvoslatiDontesTipusa,@Org) as "Jogorvoslati Döntés Típusa",
	  dbo.fn_KodtarErtekNeve(''JOGORVOSLATI_DONTEST_HOZTA'', EREC_IraOnkormAdatok.JogorvoslatiDontestHozta,@Org) as "Jogorvoslati Döntést Hozta",
	  dbo.fn_KodtarErtekNeve(''JOGORVOSLATI_DONTES_TARTALMA'', EREC_IraOnkormAdatok.JogorvoslatiDontesTartalma,@Org) as "Jogorvoslati Döntés Tartalma",
	  dbo.fn_KodtarErtekNeve(''JOGORVOSLATI_DONTES'', EREC_IraOnkormAdatok.JogorvoslatiDontes,@Org) as "Jogorvoslati Döntés",
	  REPLACE(REPLACE(EREC_IraOnkormAdatok.HatosagiEllenorzes,''1'',''Igen''),''0'',''Nem'') as "Hatósági ellenőrzés",
	  EREC_IraOnkormAdatok.MunkaorakSzama as "Munkaórák száma",
	  EREC_IraOnkormAdatok.EljarasiKoltseg as "Eljárási költség",
	  EREC_IraOnkormAdatok.KozigazgatasiBirsagMerteke as "Közigazgatási Bírság Mértéke",
	  dbo.fn_KodtarErtekNeve(''SOMMAS_DONTES'', EREC_IraOnkormAdatok.SommasEljDontes,@Org) as "Sommás Elj. Döntés",
	  dbo.fn_KodtarErtekNeve(''8NAPON_BELUL_NEM_SOMMAS'', EREC_IraOnkormAdatok.NyolcNapBelulNemSommas,@Org) as "Nyolc Napon Belül Nem Sommás",
	  dbo.fn_KodtarErtekNeve(''FUGGO_HATALYU_HATAROZAT'', EREC_IraOnkormAdatok.FuggoHatalyuHatarozat,@Org) as "Függő Hatályú Határozat",
	  dbo.fn_KodtarErtekNeve(''HATAROZAT_HATALYBA_LEPESE'', EREC_IraOnkormAdatok.FuggoHatHatalybaLepes,@Org) as "Függő Hatályú Határozat Hatályba Lépés",
	  dbo.fn_KodtarErtekNeve(''FUGGO_HATALYU_VEGZES'', EREC_IraOnkormAdatok.FuggoHatalyuVegzes,@Org) as "Függő Hatályú Végzés",
	  dbo.fn_KodtarErtekNeve(''VEGZES_HATALYBA_LEPESE'', EREC_IraOnkormAdatok.FuggoVegzesHatalyba,@Org) as "Függő Hatályú Végzés Hatályba Lépés",
	  EREC_IraOnkormAdatok.HatAltalVisszafizOsszeg as "Hatóság Által Visszafizetendő Összeg",
	  EREC_IraOnkormAdatok.HatTerheloEljKtsg as "Hatóságot Terhelő Eljárási Költség",
	  dbo.fn_KodtarErtekNeve(''FELFUGGESZTETT_HATAROZAT'', EREC_IraOnkormAdatok.FelfuggHatarozat,@Org) as "Felfügesztett Határozat"
      
	  , (select dbo.fn_MergeIrattariTetelSzam(EREC_AgazatiJelek.Kod,EREC_IraIrattariTetelek.IrattariTetelszam,EREC_IraIrattariTetelek.MegorzesiIdo,EREC_IraIrattariTetelek.IrattariJel,@Org)
       from EREC_IraIrattariTetelek, EREC_Agazatijelek
            where EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_Agazatijelek.Id and EREC_UgyUgyiratok.IraIrattariTetel_Id = EREC_IraIrattariTetelek.Id) as ITSZ
      ,CONVERT(nvarchar(10), EREC_UgyUgyiratok.ElintezesDat, 102) as ElintezesDat
	  ,EREC_UgyUgyiratok.LezarasDat as LezarasDat
	  ,CONVERT(nvarchar(10), EREC_UgyUgyiratok.IrattarbaVetelDat, 102) as IrattarbaHelyezesDat
      ,EREC_IraIratok.HivatkozasiSzam
	  ,CONVERT(nvarchar(10), EREC_UgyUgyiratok.Hatarido, 102) as Ugyirat_Hatarido
	  ,EREC_IraIratok.Hatarido
	  ,EREC_UgyUgyiratok.Targy as Ugyirat_targy
	  ,dbo.fn_GetCsoportNev(EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez) as FelhasznaloCsoport_Id_Ugyintezo_Nev
	  ,EREC_PldIratPeldanyok.CimSTR_Cimzett 
      ,EREC_PldIratPeldanyok.NevSTR_Cimzett
	  ,EREC_PldIratPeldanyok.Orzo_Nev as ElsoIratPeldanyOrzo_Nev
	  ,EREC_IraIratok.IntezesIdopontja	  

	  ,dbo.fn_KodtarErtekNeve(''IRATTIPUS'', EREC_IraIratok.Irattipus,@Org) as "Irattipus"
   from 
	 EREC_UgyUgyiratok as EREC_UgyUgyiratok
     inner join
	 EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek on EREC_IraIktatoKonyvek.Id = EREC_UgyUgyiratok.IraIktatokonyv_Id
	 inner join
	 EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok on EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
	 inner join 
	 EREC_IraIratok as EREC_IraIratok  on  EREC_IraIratok.Ugyirat_Id = EREC_UgyUgyiratok.Id	 
	 left join 
	 EREC_IraOnkormAdatok as EREC_IraOnkormAdatok on EREC_IraOnkormAdatok.IraIratok_Id = EREC_IraIratok.Id
	 left join
	 EREC_KuldKuldemenyek as EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.IraIratok_Id = EREC_IraIratok.Id
	 
	OUTER APPLY (select top 1 Id, Allapot, TovabbitasAlattAllapot, FelhasznaloCsoport_Id_Orzo,
      (select top 1 Nev from KRT_Csoportok cs where p.FelhasznaloCsoport_Id_Orzo=cs.Id and getdate() between cs.ErvKezd and cs.ErvVege) as Orzo_Nev,
      (select top 1 Nev from KRT_Csoportok cs where p.Csoport_Id_Felelos=cs.Id and getdate() between cs.ErvKezd and cs.ErvVege) as Felelos_Nev,
      BarCode, CimSTR_Cimzett, NevSTR_Cimzett, KuldesMod, PostazasDatuma from EREC_PldIratPeldanyok p
      where IraIrat_Id=EREC_IraIratok.Id and Sorszam=1
   ) EREC_PldIratPeldanyok
	
	where EREC_IraOnkormAdatok.UgyFajtaja in (''0'',''1'',''2'') 
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + 'and ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	execute sp_executesql @sqlcmd,N'@ExecutorUserId uniqueidentifier, @Org uniqueidentifier'
      ,@ExecutorUserId = @ExecutorUserId, @Org = @Org
      ;
	--exec (@sqlcmd);
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end




GO
