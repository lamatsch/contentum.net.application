IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CSVExport_Postazas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CSVExport_Postazas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CSVExport_Postazas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_CSVExport_Postazas] AS' 
END
GO

ALTER procedure [dbo].[sp_CSVExport_Postazas]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_KuldKuldemenyek.LetrehozasIdo',
  @ExecutorUserId				uniqueidentifier
as

begin

BEGIN TRY


set nocount on

   DECLARE @sqlcmd nvarchar(MAX)
   SET @sqlcmd = ''
   DECLARE @LocalTopRow nvarchar(10)

	/************************************************************
	* Tényleges select											*
	************************************************************/
     
          
 SET @sqlcmd = @sqlcmd + 
  'select 
  	   ROW_NUMBER() over (ORDER BY EREC_KuldKuldemenyek.id) Sorsz
      ,EREC_KuldKuldemenyek.NevSTR_Bekuldo beküldő
      , KRT_Cimek.TelepulesNev település
      , case KRT_Cimek.tipus 
	when ''02'' then ''Pf. '' + KRT_Cimek.Hazszam
	else KRT_Cimek.KozteruletNev + '' '' + KRT_Cimek.KozteruletTipusNev + '' '' + KRT_Cimek.Hazszam
        end Cim
      , KRT_Cimek.IRSZ Irányítószám
      , EREC_PldIratpeldanyok.Azonosito Azonosító
    
    FROM dbo.EREC_KuldKuldemenyek EREC_KuldKuldemenyek
  left JOIN dbo.KRT_Cimek KRT_Cimek ON EREC_KuldKuldemenyek.Cim_Id = KRT_Cimek.Id
  JOIN dbo.EREC_Kuldemeny_IratPeldanyai EREC_Kuldemeny_IratPeldanyai ON EREC_KuldKuldemenyek.Id = EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id 
  JOIN dbo.EREC_PldIratpeldanyok EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.id = EREC_Kuldemeny_IratPeldanyai.Peldany_Id
	'
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
	print @sqlcmd
	exec (@sqlcmd);
	
	-- találatok száma és oldalszám

  

	
 


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
