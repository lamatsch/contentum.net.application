IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DokumentumAdatokGetToBeSignedDocumentsURL]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[sp_DokumentumAdatokGetToBeSignedDocumentsURL]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[sp_DokumentumAdatokGetToBeSignedDocumentsURL]
  --@ExecutorUserId				uniqueidentifier,
  @Proc_Id		uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
 
	SELECT 		krt.Id
			,	krt.Nev
			,	krt.External_Link
			,	krt.External_Info
			,	ft.IraIrat_Id
			,   csat.Id as Csatolmany_Id

	FROM		KRT_Dokumentumok	 krt 
	JOIN		EREC_Csatolmanyok	 csat on csat.Dokumentum_Id = krt.Id
	JOIN		EREC_Alairas_Folyamat_Tetelek ft on ft.Csatolmany_Id = csat.id
	where		ft.AlairasFolyamat_Id = @Proc_Id

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
