IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DokumentumHierarchiaGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DokumentumHierarchiaGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DokumentumHierarchiaGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_DokumentumHierarchiaGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_DokumentumHierarchiaGetAll]
		@DokumentumId	uniqueidentifier
as

begin

BEGIN TRY

    set nocount on;



 
    --legfobb szülo megkeresése a dokumentumhierarchiában
	WITH ParentHiearchy  AS
	(
	   -- Base case
	   SELECT Id, 
			  1 as ParentHierarchyLevel
	   FROM [KRT_Dokumentumok]
	   WHERE Id = @DokumentumId

	   UNION ALL

	   -- Recursive step
	   SELECT dokuKapcs.Dokumentum_Id_Fo AS Id, 
			  ph.ParentHierarchyLevel + 1 AS ParentHierarchyLevel
	   FROM [KRT_DokumentumKapcsolatok] AS dokuKapcs
		  INNER JOIN ParentHiearchy as ph ON
			 dokuKapcs.Dokumentum_Id_Al = ph.Id
	   WHERE GETDATE() BETWEEN dokuKapcs.[ErvKezd] AND dokuKapcs.[ErvVege]
		AND dokuKapcs.Tipus = '1'
	),	
		
    
    -- a szülotol lefelé végigmegyünk a hierarchián
	DokuHierarchy  AS
	(
	   -- Base case (A legfelso irat a láncban)
	   SELECT TOP 1 ParentHiearchy.Id AS Id,
			   CAST(NULL AS UNIQUEIDENTIFIER) AS SzuloId,
			   CAST(NULL AS NVARCHAR(400)) AS KapcsolatJelleg,
			  1 as HierarchyLevel
	   FROM ParentHiearchy
	   WHERE ParentHiearchy.ParentHierarchyLevel = (SELECT MAX(ParentHierarchyLevel) FROM ParentHiearchy)	   

	   UNION ALL

	   -- Recursive step
	   SELECT dokuKapcs.Dokumentum_Id_Al AS Id,
		  dokuKapcs.Dokumentum_Id_Fo AS SzuloId,
		  dokuKapcs.DokumentumKapcsolatJelleg AS KapcsolatJelleg,
		  dh.HierarchyLevel + 1 AS HierarchyLevel
	   FROM [KRT_DokumentumKapcsolatok] AS dokuKapcs
		  INNER JOIN DokuHierarchy as dh ON
			  dokuKapcs.Dokumentum_Id_Fo = dh.Id
	   WHERE GETDATE() BETWEEN dokuKapcs.[ErvKezd] AND dokuKapcs.[ErvVege]
			AND dokuKapcs.Tipus = '1'
	)

--	SELECT *
--	FROM ParentHiearchy
	
	SELECT HierarchyLevel,
		SzuloId,
		KapcsolatJelleg,
		[KRT_Dokumentumok].[Id],
		[KRT_Dokumentumok].[FajlNev],
		[KRT_Dokumentumok].[Formatum],
		[KRT_Dokumentumok].[VerzioJel],
		[KRT_Dokumentumok].[External_Link]
	FROM DokuHierarchy
		JOIN [KRT_Dokumentumok]
			ON [KRT_Dokumentumok].[Id] = [DokuHierarchy].Id    

		
END TRY
BEGIN CATCH
	
	-- hibakezelés:
	EXEC usp_HandleException
 
END CATCH

end


GO
