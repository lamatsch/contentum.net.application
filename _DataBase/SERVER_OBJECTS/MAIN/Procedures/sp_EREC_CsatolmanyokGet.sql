IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_CsatolmanyokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_CsatolmanyokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_CsatolmanyokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_CsatolmanyokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_CsatolmanyokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_Csatolmanyok.Id,
	   EREC_Csatolmanyok.KuldKuldemeny_Id,
	   EREC_Csatolmanyok.IraIrat_Id,
	   EREC_Csatolmanyok.Dokumentum_Id,
	   EREC_Csatolmanyok.Leiras,
	   EREC_Csatolmanyok.Lapszam,
	   EREC_Csatolmanyok.KapcsolatJelleg,
	   EREC_Csatolmanyok.DokumentumSzerep,
	   EREC_Csatolmanyok.IratAlairoJel,
	   EREC_Csatolmanyok.CustomId,
	   EREC_Csatolmanyok.Ver,
	   EREC_Csatolmanyok.Note,
	   EREC_Csatolmanyok.Stat_id,
	   EREC_Csatolmanyok.ErvKezd,
	   EREC_Csatolmanyok.ErvVege,
	   EREC_Csatolmanyok.Letrehozo_id,
	   EREC_Csatolmanyok.LetrehozasIdo,
	   EREC_Csatolmanyok.Modosito_id,
	   EREC_Csatolmanyok.ModositasIdo,
	   EREC_Csatolmanyok.Zarolo_id,
	   EREC_Csatolmanyok.ZarolasIdo,
	   EREC_Csatolmanyok.Tranz_id,
	   EREC_Csatolmanyok.UIAccessLog_id
	   from 
		 EREC_Csatolmanyok as EREC_Csatolmanyok 
	   where
		 EREC_Csatolmanyok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
