IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ExcelLogStatistics]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_ExcelLogStatistics]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ExcelLogStatistics]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_ExcelLogStatistics] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_EREC_ExcelLogStatistics]
    @ExecutorUserId UNIQUEIDENTIFIER,
	@KezdDat datetime,
	@VegeDat datetime
AS 
    BEGIN

        BEGIN TRY

            SET nocount ON
   
            DECLARE @sqlcmd NVARCHAR(MAX)

            
     
     
               

            SET @sqlcmd = 'select
''Login'' as Tipus,
'''' as HivasForras,
FelhasznaloNev as FelhasznaloNev,
'''' as ModulNev,
LoginType as Parameter,
'''' as Muvelet,
REPLACE(CONVERT(varchar(40),KRT_Log_Login.StartDate,120),'' '',''T'') as StartDate,
REPLACE(CONVERT(varchar(40),KRT_Log_Login.EndDate,120),'' '',''T'') as EndDate,
KRT_Log_Login.HibaUzenet as HibaUzenet
from KRT_Log_Login as KRT_Log_Login
where KRT_Log_Login.StartDate between ''' + CONVERT(nvarchar(40), @KezdDat,121) + ''' and ''' + CONVERT(nvarchar(40), @VegeDat,121) + '''
or KRT_Log_Login.EndDate between ''' + CONVERT(nvarchar(40), @KezdDat,121) + ''' and ''' + CONVERT(nvarchar(40), @VegeDat,121) + '''
union
select
''UI'' as Tipus,
'''' as HivasForras,
KRT_Log_Login.FelhasznaloNev as FelhasznaloNev,
Url as ModulNev,
QueryString as Parameter,
Command as Muvelet,
REPLACE(CONVERT(varchar(40),KRT_Log_Page.StartDate,120),'' '',''T'') as StartDate,
REPLACE(CONVERT(varchar(40),KRT_Log_Page.EndDate,120),'' '',''T'') as EndDate,
KRT_Log_Page.HibaUzenet as HibaUzenet
from KRT_Log_Page as KRT_Log_Page
left join KRT_Log_Login as KRT_Log_Login on KRT_Log_Page.LoginId = KRT_Log_Login.Id
where KRT_Log_Page.StartDate between ''' + CONVERT(nvarchar(40), @KezdDat,121) + ''' and ''' + CONVERT(nvarchar(40), @VegeDat,121) + '''
or KRT_Log_Page.EndDate between ''' + CONVERT(nvarchar(40), @KezdDat,121) + ''' and ''' + CONVERT(nvarchar(40), @VegeDat,121) + '''
union
select
''WS'' as Tipus,
'''' as HivasForras,
KRT_Log_Login.FelhasznaloNev as FelhasznaloNev,
ServiceName + ''.'' + MethodName as ModulNev,
'''' as Parameter,
'''' as Muvelet,
REPLACE(CONVERT(varchar(40),KRT_Log_WebService.StartDate,120),'' '',''T'') as StartDate,
REPLACE(CONVERT(varchar(40),KRT_Log_WebService.EndDate,120),'' '',''T'') as EndDate,
KRT_Log_WebService.HibaUzenet as HibaUzenet
from KRT_Log_WebService as KRT_Log_WebService
left join KRT_Log_Page as KRT_Log_Page on KRT_Log_WebService.PageId = KRT_Log_Page.Id
left join KRT_Log_Login as KRT_Log_Login on KRT_Log_Page.LoginId = KRT_Log_Login.Id
where KRT_Log_WebService.StartDate between ''' + CONVERT(nvarchar(40), @KezdDat,121) + ''' and ''' + CONVERT(nvarchar(40), @VegeDat,121) + '''
or KRT_Log_WebService.EndDate between ''' + CONVERT(nvarchar(40), @KezdDat,121) + ''' and ''' + CONVERT(nvarchar(40), @VegeDat,121) + '''
union
select
''SP'' as Tipus,
'''' as HivasForras,
KRT_Log_Login.FelhasznaloNev as FelhasznaloNev,
SpName as ModulNev,
'''' as Parameter,
'''' as Muvelet,
REPLACE(CONVERT(varchar(40),KRT_Log_StoredProcedure.StartDate,120),'' '',''T'') as StartDate,
REPLACE(CONVERT(varchar(40),KRT_Log_StoredProcedure.EndDate,120),'' '',''T'') as EndDate,
KRT_Log_StoredProcedure.HibaUzenet as HibaUzenet
from KRT_Log_StoredProcedure as KRT_Log_StoredProcedure
left join KRT_Log_WebService as KRT_Log_WebService on KRT_Log_StoredProcedure.WebServiceId = KRT_Log_WebService.Id
left join KRT_Log_Page as KRT_Log_Page on KRT_Log_WebService.PageId = KRT_Log_Page.Id
left join KRT_Log_Login as KRT_Log_Login on KRT_Log_Page.LoginId = KRT_Log_Login.Id
where KRT_Log_StoredProcedure.StartDate between ''' + CONVERT(nvarchar(40), @KezdDat,121) + ''' and ''' + CONVERT(nvarchar(40), @VegeDat,121) + '''
or KRT_Log_StoredProcedure.EndDate between ''' + CONVERT(nvarchar(40), @KezdDat,121) + ''' and ''' + CONVERT(nvarchar(40), @VegeDat,121) + '''
		'
  

            EXEC ( @sqlcmd ) ;

        END TRY
        BEGIN CATCH
            DECLARE @errorSeverity INT,
                @errorState INT
            DECLARE @errorCode NVARCHAR(1000)    
            SET @errorSeverity = ERROR_SEVERITY()
            SET @errorState = ERROR_STATE()
	
            IF ERROR_NUMBER() < 50000 
                SET @errorCode = '[' + CONVERT(NVARCHAR(10), ERROR_NUMBER())
                    + '] ' + ERROR_MESSAGE()
            ELSE 
                SET @errorCode = ERROR_MESSAGE()
      
            IF @errorState = 0 
                SET @errorState = 1

            RAISERROR ( @errorCode, @errorSeverity, @errorState )
 
        END CATCH

    END


GO
