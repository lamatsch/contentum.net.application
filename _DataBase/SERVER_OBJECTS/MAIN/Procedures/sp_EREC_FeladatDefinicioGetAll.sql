IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_FeladatDefinicioGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_FeladatDefinicioGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_FeladatDefinicioGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_FeladatDefinicioGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_FeladatDefinicioGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_FeladatDefinicio.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_FeladatDefinicio.Id,
	   EREC_FeladatDefinicio.Org,
	   EREC_FeladatDefinicio.Funkcio_Id_Kivalto,
	   EREC_FeladatDefinicio.Obj_Metadefinicio_Id,
	   EREC_FeladatDefinicio.ObjStateValue_Id,
	   EREC_FeladatDefinicio.FeladatDefinicioTipus,
	   EREC_FeladatDefinicio.FeladatSorszam,
	   EREC_FeladatDefinicio.MuveletDefinicio,
	   EREC_FeladatDefinicio.SpecMuveletDefinicio,
	   EREC_FeladatDefinicio.Funkcio_Id_Inditando,
	   EREC_FeladatDefinicio.Funkcio_Id_Futtatando,
	   EREC_FeladatDefinicio.BazisObjLeiro,
	   EREC_FeladatDefinicio.FelelosTipus,
	   EREC_FeladatDefinicio.Obj_Id_Felelos,
	   EREC_FeladatDefinicio.FelelosLeiro,
	   EREC_FeladatDefinicio.Idobazis,
	   EREC_FeladatDefinicio.ObjTip_Id_DateCol,
	   EREC_FeladatDefinicio.AtfutasiIdo,
	   EREC_FeladatDefinicio.Idoegyseg,
	   EREC_FeladatDefinicio.FeladatLeiras,
	   EREC_FeladatDefinicio.Tipus,
	   EREC_FeladatDefinicio.Prioritas,
	   EREC_FeladatDefinicio.LezarasPrioritas,
	   EREC_FeladatDefinicio.Allapot,
	   EREC_FeladatDefinicio.ObjTip_Id,
	   EREC_FeladatDefinicio.Obj_type,
	   EREC_FeladatDefinicio.Obj_SzukitoFeltetel,
	   EREC_FeladatDefinicio.Ver,
	   EREC_FeladatDefinicio.Note,
	   EREC_FeladatDefinicio.Stat_id,
	   EREC_FeladatDefinicio.ErvKezd,
	   EREC_FeladatDefinicio.ErvVege,
	   EREC_FeladatDefinicio.Letrehozo_id,
	   EREC_FeladatDefinicio.LetrehozasIdo,
	   EREC_FeladatDefinicio.Modosito_id,
	   EREC_FeladatDefinicio.ModositasIdo,
	   EREC_FeladatDefinicio.Zarolo_id,
	   EREC_FeladatDefinicio.ZarolasIdo,
	   EREC_FeladatDefinicio.Tranz_id,
	   EREC_FeladatDefinicio.UIAccessLog_id  
   from 
     EREC_FeladatDefinicio as EREC_FeladatDefinicio      
    Where EREC_FeladatDefinicio.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
