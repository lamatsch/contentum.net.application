IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_FeladatDefinicioGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_FeladatDefinicioGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_FeladatDefinicioGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_FeladatDefinicioGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_FeladatDefinicioGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by EREC_FeladatDefinicio.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_FeladatDefinicio.Id,
	   EREC_FeladatDefinicio.Funkcio_Id_Kivalto,
KRT_Funkciok_Kivalto.Nev as Funkcio_Nev_Kivalto,
	   EREC_FeladatDefinicio.Obj_Metadefinicio_Id,
	   EREC_FeladatDefinicio.ObjStateValue_Id,
	   EREC_FeladatDefinicio.FeladatDefinicioTipus,
KRT_KodTarak_DefinicioTipus.Nev as FeladatDefinicioTipus_Nev,
	   EREC_FeladatDefinicio.FeladatSorszam,
	   EREC_FeladatDefinicio.MuveletDefinicio,
	   EREC_FeladatDefinicio.SpecMuveletDefinicio,
	   EREC_FeladatDefinicio.Funkcio_Id_Inditando,
	   EREC_FeladatDefinicio.Funkcio_Id_Futtatando,
	   EREC_FeladatDefinicio.BazisObjLeiro,
	   EREC_FeladatDefinicio.FelelosTipus,
	   EREC_FeladatDefinicio.Obj_Id_Felelos,
	   EREC_FeladatDefinicio.FelelosLeiro,
	   EREC_FeladatDefinicio.Idobazis,
KRT_KodTarak_Idobazis.Nev as Idobazis_Nev,
	   EREC_FeladatDefinicio.ObjTip_Id_DateCol,
KRT_ObjTipusok_DateCol.Nev as ObjTip_Nev_DateCol,
	   EREC_FeladatDefinicio.AtfutasiIdo,
	   EREC_FeladatDefinicio.Idoegyseg,
KRT_KodTarak_Idoegyseg.Nev as Idoegyseg_Nev,
	   EREC_FeladatDefinicio.FeladatLeiras,
	   EREC_FeladatDefinicio.Tipus,
	   EREC_FeladatDefinicio.Prioritas,
KRT_KodTarak_Prioritas.Nev as Prioritas_Nev,
	   EREC_FeladatDefinicio.LezarasPrioritas,
KRT_KodTarak_LezarasPrioritas.Nev as LezarasPrioritas_Nev,
	   EREC_FeladatDefinicio.Allapot,
	   EREC_FeladatDefinicio.ObjTip_Id,
	   EREC_FeladatDefinicio.Obj_type,
	   EREC_FeladatDefinicio.Obj_SzukitoFeltetel,
	   EREC_FeladatDefinicio.Ver,
	   EREC_FeladatDefinicio.Note,
	   EREC_FeladatDefinicio.Stat_id,
	   EREC_FeladatDefinicio.ErvKezd,
	   EREC_FeladatDefinicio.ErvVege,
	   EREC_FeladatDefinicio.Letrehozo_id,
	   EREC_FeladatDefinicio.LetrehozasIdo,
	   EREC_FeladatDefinicio.Modosito_id,
	   EREC_FeladatDefinicio.ModositasIdo,
	   EREC_FeladatDefinicio.Zarolo_id,
	   EREC_FeladatDefinicio.ZarolasIdo,
	   EREC_FeladatDefinicio.Tranz_id,
	   EREC_FeladatDefinicio.UIAccessLog_id  
   from 
     EREC_FeladatDefinicio as EREC_FeladatDefinicio      
   '

  set @sqlcmd = @sqlcmd + 'LEFT JOIN KRT_Funkciok as KRT_Funkciok_Kivalto ON EREC_FeladatDefinicio.Funkcio_Id_Kivalto=KRT_Funkciok_Kivalto.Id
LEFT JOIN KRT_KodTarak as KRT_KodTarak_DefinicioTipus ON KRT_KodTarak_DefinicioTipus.KodCsoport_Id=(select top 1 Id from KRT_KodCsoportok where Kod=''FELADAT_DEFINICIO_TIPUS'')
	and EREC_FeladatDefinicio.FeladatDefinicioTipus=KRT_KodTarak_DefinicioTipus.Kod COLLATE Hungarian_CI_AS and KRT_KodTarak_DefinicioTipus.Org=''' + CAST(@Org as NVarChar(40)) + '''
LEFT JOIN KRT_KodTarak as KRT_KodTarak_Idobazis ON KRT_KodTarak_Idobazis.KodCsoport_Id=(select top 1 Id from KRT_KodCsoportok where Kod=''FELADAT_IDOBAZIS'')
	and EREC_FeladatDefinicio.Idobazis=KRT_KodTarak_Idobazis.Kod COLLATE Hungarian_CI_AS and KRT_KodTarak_Idobazis.Org=''' + CAST(@Org as NVarChar(40)) + '''
LEFT JOIN KRT_ObjTipusok as KRT_ObjTipusok_DateCol ON EREC_FeladatDefinicio.ObjTip_Id_DateCol=KRT_ObjTipusok_DateCol.Id
LEFT JOIN KRT_KodTarak as KRT_KodTarak_Idoegyseg ON KRT_KodTarak_Idoegyseg.KodCsoport_Id=(select top 1 Id from KRT_KodCsoportok where Kod=''IDOEGYSEG'')
	and EREC_FeladatDefinicio.Idoegyseg=KRT_KodTarak_Idoegyseg.Kod COLLATE Hungarian_CI_AS and KRT_KodTarak_Idoegyseg.Org=''' + CAST(@Org as NVarChar(40)) + '''
LEFT JOIN KRT_KodTarak as KRT_KodTarak_Prioritas ON KRT_KodTarak_Prioritas.KodCsoport_Id=(select top 1 Id from KRT_KodCsoportok where Kod=''FELADAT_PRIORITAS'')
	and EREC_FeladatDefinicio.Prioritas=KRT_KodTarak_Prioritas.Kod COLLATE Hungarian_CI_AS and KRT_KodTarak_Prioritas.Org=''' + CAST(@Org as NVarChar(40)) + '''
LEFT JOIN KRT_KodTarak as KRT_KodTarak_LezarasPrioritas ON KRT_KodTarak_LezarasPrioritas.KodCsoport_Id=(select top 1 Id from KRT_KodCsoportok where Kod=''FELADAT_PRIORITAS'')
	and EREC_FeladatDefinicio.LezarasPrioritas=KRT_KodTarak_LezarasPrioritas.Kod COLLATE Hungarian_CI_AS and KRT_KodTarak_LezarasPrioritas.Org=''' + CAST(@Org as NVarChar(40)) + '''
    Where EREC_FeladatDefinicio.Org=''' + CAST(@Org as Nvarchar(40)) + '''
'



      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
