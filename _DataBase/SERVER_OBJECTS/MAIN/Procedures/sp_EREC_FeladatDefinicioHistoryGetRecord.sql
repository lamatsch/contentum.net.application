IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_FeladatDefinicioHistoryGetRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_FeladatDefinicioHistoryGetRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_FeladatDefinicioHistoryGetRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_FeladatDefinicioHistoryGetRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_FeladatDefinicioHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_FeladatDefinicioHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_FeladatDefinicioHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Org' as ColumnName,               
               cast(Old.Org as nvarchar(99)) as OldValue,
               cast(New.Org as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Org != New.Org 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Funkcio_Id_Kivalto' as ColumnName,               
               cast(Old.Funkcio_Id_Kivalto as nvarchar(99)) as OldValue,
               cast(New.Funkcio_Id_Kivalto as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Funkcio_Id_Kivalto != New.Funkcio_Id_Kivalto 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Metadefinicio_Id' as ColumnName,               
               cast(Old.Obj_Metadefinicio_Id as nvarchar(99)) as OldValue,
               cast(New.Obj_Metadefinicio_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_Metadefinicio_Id != New.Obj_Metadefinicio_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ObjStateValue_Id' as ColumnName,               
               cast(Old.ObjStateValue_Id as nvarchar(99)) as OldValue,
               cast(New.ObjStateValue_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ObjStateValue_Id != New.ObjStateValue_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FeladatDefinicioTipus' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FeladatDefinicioTipus != New.FeladatDefinicioTipus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'FELADAT_TIPUS'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.FeladatDefinicioTipus and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.FeladatDefinicioTipus and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FeladatSorszam' as ColumnName,               
               cast(Old.FeladatSorszam as nvarchar(99)) as OldValue,
               cast(New.FeladatSorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FeladatSorszam != New.FeladatSorszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MuveletDefinicio' as ColumnName,               
               cast(Old.MuveletDefinicio as nvarchar(99)) as OldValue,
               cast(New.MuveletDefinicio as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MuveletDefinicio != New.MuveletDefinicio 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SpecMuveletDefinicio' as ColumnName,               
               cast(Old.SpecMuveletDefinicio as nvarchar(99)) as OldValue,
               cast(New.SpecMuveletDefinicio as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SpecMuveletDefinicio != New.SpecMuveletDefinicio 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Funkcio_Id_Inditando' as ColumnName,               
               cast(Old.Funkcio_Id_Inditando as nvarchar(99)) as OldValue,
               cast(New.Funkcio_Id_Inditando as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Funkcio_Id_Inditando != New.Funkcio_Id_Inditando 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Funkcio_Id_Futtatando' as ColumnName,               
               cast(Old.Funkcio_Id_Futtatando as nvarchar(99)) as OldValue,
               cast(New.Funkcio_Id_Futtatando as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Funkcio_Id_Futtatando != New.Funkcio_Id_Futtatando 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BazisObjLeiro' as ColumnName,               
               cast(Old.BazisObjLeiro as nvarchar(99)) as OldValue,
               cast(New.BazisObjLeiro as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BazisObjLeiro != New.BazisObjLeiro 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelelosTipus' as ColumnName,               
               cast(Old.FelelosTipus as nvarchar(99)) as OldValue,
               cast(New.FelelosTipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelelosTipus != New.FelelosTipus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Id_Felelos' as ColumnName,               
               cast(Old.Obj_Id_Felelos as nvarchar(99)) as OldValue,
               cast(New.Obj_Id_Felelos as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_Id_Felelos != New.Obj_Id_Felelos 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelelosLeiro' as ColumnName,               
               cast(Old.FelelosLeiro as nvarchar(99)) as OldValue,
               cast(New.FelelosLeiro as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelelosLeiro != New.FelelosLeiro 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Idobazis' as ColumnName,               
               cast(Old.Idobazis as nvarchar(99)) as OldValue,
               cast(New.Idobazis as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Idobazis != New.Idobazis 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ObjTip_Id_DateCol' as ColumnName,               
               cast(Old.ObjTip_Id_DateCol as nvarchar(99)) as OldValue,
               cast(New.ObjTip_Id_DateCol as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ObjTip_Id_DateCol != New.ObjTip_Id_DateCol 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AtfutasiIdo' as ColumnName,               
               cast(Old.AtfutasiIdo as nvarchar(99)) as OldValue,
               cast(New.AtfutasiIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AtfutasiIdo != New.AtfutasiIdo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Idoegyseg' as ColumnName,               
               cast(Old.Idoegyseg as nvarchar(99)) as OldValue,
               cast(New.Idoegyseg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Idoegyseg != New.Idoegyseg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FeladatLeiras' as ColumnName,               
               cast(Old.FeladatLeiras as nvarchar(99)) as OldValue,
               cast(New.FeladatLeiras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FeladatLeiras != New.FeladatLeiras 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tipus' as ColumnName,               
               cast(Old.Tipus as nvarchar(99)) as OldValue,
               cast(New.Tipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tipus != New.Tipus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Prioritas' as ColumnName,               
               cast(Old.Prioritas as nvarchar(99)) as OldValue,
               cast(New.Prioritas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Prioritas != New.Prioritas 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'LezarasPrioritas' as ColumnName,               
               cast(Old.LezarasPrioritas as nvarchar(99)) as OldValue,
               cast(New.LezarasPrioritas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.LezarasPrioritas != New.LezarasPrioritas 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               
               cast(Old.Allapot as nvarchar(99)) as OldValue,
               cast(New.Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Allapot != New.Allapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ObjTip_Id' as ColumnName,               
               cast(Old.ObjTip_Id as nvarchar(99)) as OldValue,
               cast(New.ObjTip_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ObjTip_Id != New.ObjTip_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_type' as ColumnName,               
               cast(Old.Obj_type as nvarchar(99)) as OldValue,
               cast(New.Obj_type as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_type != New.Obj_type 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_SzukitoFeltetel' as ColumnName,               
               cast(Old.Obj_SzukitoFeltetel as nvarchar(99)) as OldValue,
               cast(New.Obj_SzukitoFeltetel as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_FeladatDefinicioHistory Old
         inner join EREC_FeladatDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_SzukitoFeltetel != New.Obj_SzukitoFeltetel 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end


GO
