IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_FeladatDefinicioInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_FeladatDefinicioInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_FeladatDefinicioInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_FeladatDefinicioInsert] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_FeladatDefinicioInsert]    
                @Id      uniqueidentifier = null,    
                           @Org     uniqueidentifier = null,    
                @Funkcio_Id_Kivalto     uniqueidentifier  = null,
                @Obj_Metadefinicio_Id     uniqueidentifier  = null,
                @ObjStateValue_Id     uniqueidentifier  = null,
                @FeladatDefinicioTipus     nvarchar(64)  = null,
                @FeladatSorszam     int  = null,
                @MuveletDefinicio     nvarchar(64)  = null,
                @SpecMuveletDefinicio     nvarchar(64)  = null,
                @Funkcio_Id_Inditando     uniqueidentifier  = null,
                @Funkcio_Id_Futtatando     uniqueidentifier  = null,
                @BazisObjLeiro     Nvarchar(400)  = null,
                @FelelosTipus     nvarchar(64)  = null,
                @Obj_Id_Felelos     uniqueidentifier  = null,
                @FelelosLeiro     Nvarchar(100)  = null,
                @Idobazis     nvarchar(64)  = null,
                @ObjTip_Id_DateCol     uniqueidentifier  = null,
                @AtfutasiIdo     int  = null,
                @Idoegyseg     nvarchar(64)  = null,
                @FeladatLeiras     Nvarchar(400)  = null,
                @Tipus     nvarchar(64)  = null,
                @Prioritas     nvarchar(64)  = null,
                @LezarasPrioritas     nvarchar(64)  = null,
                @Allapot     nvarchar(64)  = null,
                @ObjTip_Id     uniqueidentifier  = null,
                @Obj_type     Nvarchar(100)  = null,
                @Obj_SzukitoFeltetel     Nvarchar(4000)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
 IF @ORG is NULL
 BEGIN 
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@Letrehozo_id)
   if (@Org is null)
   begin
   	RAISERROR('[50302]',16,1)
   end
END
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Org is not null
         begin
            SET @insertColumns = @insertColumns + ',Org'
            SET @insertValues = @insertValues + ',@Org'
         end 
       
         if @Funkcio_Id_Kivalto is not null
         begin
            SET @insertColumns = @insertColumns + ',Funkcio_Id_Kivalto'
            SET @insertValues = @insertValues + ',@Funkcio_Id_Kivalto'
         end 
       
         if @Obj_Metadefinicio_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Obj_Metadefinicio_Id'
            SET @insertValues = @insertValues + ',@Obj_Metadefinicio_Id'
         end 
       
         if @ObjStateValue_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',ObjStateValue_Id'
            SET @insertValues = @insertValues + ',@ObjStateValue_Id'
         end 
       
         if @FeladatDefinicioTipus is not null
         begin
            SET @insertColumns = @insertColumns + ',FeladatDefinicioTipus'
            SET @insertValues = @insertValues + ',@FeladatDefinicioTipus'
         end 
       
         if @FeladatSorszam is not null
         begin
            SET @insertColumns = @insertColumns + ',FeladatSorszam'
            SET @insertValues = @insertValues + ',@FeladatSorszam'
         end 
       
         if @MuveletDefinicio is not null
         begin
            SET @insertColumns = @insertColumns + ',MuveletDefinicio'
            SET @insertValues = @insertValues + ',@MuveletDefinicio'
         end 
       
         if @SpecMuveletDefinicio is not null
         begin
            SET @insertColumns = @insertColumns + ',SpecMuveletDefinicio'
            SET @insertValues = @insertValues + ',@SpecMuveletDefinicio'
         end 
       
         if @Funkcio_Id_Inditando is not null
         begin
            SET @insertColumns = @insertColumns + ',Funkcio_Id_Inditando'
            SET @insertValues = @insertValues + ',@Funkcio_Id_Inditando'
         end 
       
         if @Funkcio_Id_Futtatando is not null
         begin
            SET @insertColumns = @insertColumns + ',Funkcio_Id_Futtatando'
            SET @insertValues = @insertValues + ',@Funkcio_Id_Futtatando'
         end 
       
         if @BazisObjLeiro is not null
         begin
            SET @insertColumns = @insertColumns + ',BazisObjLeiro'
            SET @insertValues = @insertValues + ',@BazisObjLeiro'
         end 
       
         if @FelelosTipus is not null
         begin
            SET @insertColumns = @insertColumns + ',FelelosTipus'
            SET @insertValues = @insertValues + ',@FelelosTipus'
         end 
       
         if @Obj_Id_Felelos is not null
         begin
            SET @insertColumns = @insertColumns + ',Obj_Id_Felelos'
            SET @insertValues = @insertValues + ',@Obj_Id_Felelos'
         end 
       
         if @FelelosLeiro is not null
         begin
            SET @insertColumns = @insertColumns + ',FelelosLeiro'
            SET @insertValues = @insertValues + ',@FelelosLeiro'
         end 
       
         if @Idobazis is not null
         begin
            SET @insertColumns = @insertColumns + ',Idobazis'
            SET @insertValues = @insertValues + ',@Idobazis'
         end 
       
         if @ObjTip_Id_DateCol is not null
         begin
            SET @insertColumns = @insertColumns + ',ObjTip_Id_DateCol'
            SET @insertValues = @insertValues + ',@ObjTip_Id_DateCol'
         end 
       
         if @AtfutasiIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',AtfutasiIdo'
            SET @insertValues = @insertValues + ',@AtfutasiIdo'
         end 
       
         if @Idoegyseg is not null
         begin
            SET @insertColumns = @insertColumns + ',Idoegyseg'
            SET @insertValues = @insertValues + ',@Idoegyseg'
         end 
       
         if @FeladatLeiras is not null
         begin
            SET @insertColumns = @insertColumns + ',FeladatLeiras'
            SET @insertValues = @insertValues + ',@FeladatLeiras'
         end 
       
         if @Tipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Tipus'
            SET @insertValues = @insertValues + ',@Tipus'
         end 
       
         if @Prioritas is not null
         begin
            SET @insertColumns = @insertColumns + ',Prioritas'
            SET @insertValues = @insertValues + ',@Prioritas'
         end 
       
         if @LezarasPrioritas is not null
         begin
            SET @insertColumns = @insertColumns + ',LezarasPrioritas'
            SET @insertValues = @insertValues + ',@LezarasPrioritas'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @ObjTip_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',ObjTip_Id'
            SET @insertValues = @insertValues + ',@ObjTip_Id'
         end 
       
         if @Obj_type is not null
         begin
            SET @insertColumns = @insertColumns + ',Obj_type'
            SET @insertValues = @insertValues + ',@Obj_type'
         end 
       
         if @Obj_SzukitoFeltetel is not null
         begin
            SET @insertColumns = @insertColumns + ',Obj_SzukitoFeltetel'
            SET @insertValues = @insertValues + ',@Obj_SzukitoFeltetel'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_FeladatDefinicio ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Org uniqueidentifier,@Funkcio_Id_Kivalto uniqueidentifier,@Obj_Metadefinicio_Id uniqueidentifier,@ObjStateValue_Id uniqueidentifier,@FeladatDefinicioTipus nvarchar(64),@FeladatSorszam int,@MuveletDefinicio nvarchar(64),@SpecMuveletDefinicio nvarchar(64),@Funkcio_Id_Inditando uniqueidentifier,@Funkcio_Id_Futtatando uniqueidentifier,@BazisObjLeiro Nvarchar(400),@FelelosTipus nvarchar(64),@Obj_Id_Felelos uniqueidentifier,@FelelosLeiro Nvarchar(100),@Idobazis nvarchar(64),@ObjTip_Id_DateCol uniqueidentifier,@AtfutasiIdo int,@Idoegyseg nvarchar(64),@FeladatLeiras Nvarchar(400),@Tipus nvarchar(64),@Prioritas nvarchar(64),@LezarasPrioritas nvarchar(64),@Allapot nvarchar(64),@ObjTip_Id uniqueidentifier,@Obj_type Nvarchar(100),@Obj_SzukitoFeltetel Nvarchar(4000),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Org = @Org,@Funkcio_Id_Kivalto = @Funkcio_Id_Kivalto,@Obj_Metadefinicio_Id = @Obj_Metadefinicio_Id,@ObjStateValue_Id = @ObjStateValue_Id,@FeladatDefinicioTipus = @FeladatDefinicioTipus,@FeladatSorszam = @FeladatSorszam,@MuveletDefinicio = @MuveletDefinicio,@SpecMuveletDefinicio = @SpecMuveletDefinicio,@Funkcio_Id_Inditando = @Funkcio_Id_Inditando,@Funkcio_Id_Futtatando = @Funkcio_Id_Futtatando,@BazisObjLeiro = @BazisObjLeiro,@FelelosTipus = @FelelosTipus,@Obj_Id_Felelos = @Obj_Id_Felelos,@FelelosLeiro = @FelelosLeiro,@Idobazis = @Idobazis,@ObjTip_Id_DateCol = @ObjTip_Id_DateCol,@AtfutasiIdo = @AtfutasiIdo,@Idoegyseg = @Idoegyseg,@FeladatLeiras = @FeladatLeiras,@Tipus = @Tipus,@Prioritas = @Prioritas,@LezarasPrioritas = @LezarasPrioritas,@Allapot = @Allapot,@ObjTip_Id = @ObjTip_Id,@Obj_type = @Obj_type,@Obj_SzukitoFeltetel = @Obj_SzukitoFeltetel,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_FeladatDefinicio',@ResultUid
					,'EREC_FeladatDefinicioHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
