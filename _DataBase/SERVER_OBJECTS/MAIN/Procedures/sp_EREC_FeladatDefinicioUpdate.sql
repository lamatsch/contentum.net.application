IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_FeladatDefinicioUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_FeladatDefinicioUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_FeladatDefinicioUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_FeladatDefinicioUpdate] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_FeladatDefinicioUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Org     uniqueidentifier  = null,         
             @Funkcio_Id_Kivalto     uniqueidentifier  = null,         
             @Obj_Metadefinicio_Id     uniqueidentifier  = null,         
             @ObjStateValue_Id     uniqueidentifier  = null,         
             @FeladatDefinicioTipus     nvarchar(64)  = null,         
             @FeladatSorszam     int  = null,         
             @MuveletDefinicio     nvarchar(64)  = null,         
             @SpecMuveletDefinicio     nvarchar(64)  = null,         
             @Funkcio_Id_Inditando     uniqueidentifier  = null,         
             @Funkcio_Id_Futtatando     uniqueidentifier  = null,         
             @BazisObjLeiro     Nvarchar(400)  = null,         
             @FelelosTipus     nvarchar(64)  = null,         
             @Obj_Id_Felelos     uniqueidentifier  = null,         
             @FelelosLeiro     Nvarchar(100)  = null,         
             @Idobazis     nvarchar(64)  = null,         
             @ObjTip_Id_DateCol     uniqueidentifier  = null,         
             @AtfutasiIdo     int  = null,         
             @Idoegyseg     nvarchar(64)  = null,         
             @FeladatLeiras     Nvarchar(400)  = null,         
             @Tipus     nvarchar(64)  = null,         
             @Prioritas     nvarchar(64)  = null,         
             @LezarasPrioritas     nvarchar(64)  = null,         
             @Allapot     nvarchar(64)  = null,         
             @ObjTip_Id     uniqueidentifier  = null,         
             @Obj_type     Nvarchar(100)  = null,         
             @Obj_SzukitoFeltetel     Nvarchar(4000)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Org     uniqueidentifier         
     DECLARE @Act_Funkcio_Id_Kivalto     uniqueidentifier         
     DECLARE @Act_Obj_Metadefinicio_Id     uniqueidentifier         
     DECLARE @Act_ObjStateValue_Id     uniqueidentifier         
     DECLARE @Act_FeladatDefinicioTipus     nvarchar(64)         
     DECLARE @Act_FeladatSorszam     int         
     DECLARE @Act_MuveletDefinicio     nvarchar(64)         
     DECLARE @Act_SpecMuveletDefinicio     nvarchar(64)         
     DECLARE @Act_Funkcio_Id_Inditando     uniqueidentifier         
     DECLARE @Act_Funkcio_Id_Futtatando     uniqueidentifier         
     DECLARE @Act_BazisObjLeiro     Nvarchar(400)         
     DECLARE @Act_FelelosTipus     nvarchar(64)         
     DECLARE @Act_Obj_Id_Felelos     uniqueidentifier         
     DECLARE @Act_FelelosLeiro     Nvarchar(100)         
     DECLARE @Act_Idobazis     nvarchar(64)         
     DECLARE @Act_ObjTip_Id_DateCol     uniqueidentifier         
     DECLARE @Act_AtfutasiIdo     int         
     DECLARE @Act_Idoegyseg     nvarchar(64)         
     DECLARE @Act_FeladatLeiras     Nvarchar(400)         
     DECLARE @Act_Tipus     nvarchar(64)         
     DECLARE @Act_Prioritas     nvarchar(64)         
     DECLARE @Act_LezarasPrioritas     nvarchar(64)         
     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_ObjTip_Id     uniqueidentifier         
     DECLARE @Act_Obj_type     Nvarchar(100)         
     DECLARE @Act_Obj_SzukitoFeltetel     Nvarchar(4000)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Org = Org,
     @Act_Funkcio_Id_Kivalto = Funkcio_Id_Kivalto,
     @Act_Obj_Metadefinicio_Id = Obj_Metadefinicio_Id,
     @Act_ObjStateValue_Id = ObjStateValue_Id,
     @Act_FeladatDefinicioTipus = FeladatDefinicioTipus,
     @Act_FeladatSorszam = FeladatSorszam,
     @Act_MuveletDefinicio = MuveletDefinicio,
     @Act_SpecMuveletDefinicio = SpecMuveletDefinicio,
     @Act_Funkcio_Id_Inditando = Funkcio_Id_Inditando,
     @Act_Funkcio_Id_Futtatando = Funkcio_Id_Futtatando,
     @Act_BazisObjLeiro = BazisObjLeiro,
     @Act_FelelosTipus = FelelosTipus,
     @Act_Obj_Id_Felelos = Obj_Id_Felelos,
     @Act_FelelosLeiro = FelelosLeiro,
     @Act_Idobazis = Idobazis,
     @Act_ObjTip_Id_DateCol = ObjTip_Id_DateCol,
     @Act_AtfutasiIdo = AtfutasiIdo,
     @Act_Idoegyseg = Idoegyseg,
     @Act_FeladatLeiras = FeladatLeiras,
     @Act_Tipus = Tipus,
     @Act_Prioritas = Prioritas,
     @Act_LezarasPrioritas = LezarasPrioritas,
     @Act_Allapot = Allapot,
     @Act_ObjTip_Id = ObjTip_Id,
     @Act_Obj_type = Obj_type,
     @Act_Obj_SzukitoFeltetel = Obj_SzukitoFeltetel,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_FeladatDefinicio
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Org')=1
         SET @Act_Org = @Org
   IF @UpdatedColumns.exist('/root/Funkcio_Id_Kivalto')=1
         SET @Act_Funkcio_Id_Kivalto = @Funkcio_Id_Kivalto
   IF @UpdatedColumns.exist('/root/Obj_Metadefinicio_Id')=1
         SET @Act_Obj_Metadefinicio_Id = @Obj_Metadefinicio_Id
   IF @UpdatedColumns.exist('/root/ObjStateValue_Id')=1
         SET @Act_ObjStateValue_Id = @ObjStateValue_Id
   IF @UpdatedColumns.exist('/root/FeladatDefinicioTipus')=1
         SET @Act_FeladatDefinicioTipus = @FeladatDefinicioTipus
   IF @UpdatedColumns.exist('/root/FeladatSorszam')=1
         SET @Act_FeladatSorszam = @FeladatSorszam
   IF @UpdatedColumns.exist('/root/MuveletDefinicio')=1
         SET @Act_MuveletDefinicio = @MuveletDefinicio
   IF @UpdatedColumns.exist('/root/SpecMuveletDefinicio')=1
         SET @Act_SpecMuveletDefinicio = @SpecMuveletDefinicio
   IF @UpdatedColumns.exist('/root/Funkcio_Id_Inditando')=1
         SET @Act_Funkcio_Id_Inditando = @Funkcio_Id_Inditando
   IF @UpdatedColumns.exist('/root/Funkcio_Id_Futtatando')=1
         SET @Act_Funkcio_Id_Futtatando = @Funkcio_Id_Futtatando
   IF @UpdatedColumns.exist('/root/BazisObjLeiro')=1
         SET @Act_BazisObjLeiro = @BazisObjLeiro
   IF @UpdatedColumns.exist('/root/FelelosTipus')=1
         SET @Act_FelelosTipus = @FelelosTipus
   IF @UpdatedColumns.exist('/root/Obj_Id_Felelos')=1
         SET @Act_Obj_Id_Felelos = @Obj_Id_Felelos
   IF @UpdatedColumns.exist('/root/FelelosLeiro')=1
         SET @Act_FelelosLeiro = @FelelosLeiro
   IF @UpdatedColumns.exist('/root/Idobazis')=1
         SET @Act_Idobazis = @Idobazis
   IF @UpdatedColumns.exist('/root/ObjTip_Id_DateCol')=1
         SET @Act_ObjTip_Id_DateCol = @ObjTip_Id_DateCol
   IF @UpdatedColumns.exist('/root/AtfutasiIdo')=1
         SET @Act_AtfutasiIdo = @AtfutasiIdo
   IF @UpdatedColumns.exist('/root/Idoegyseg')=1
         SET @Act_Idoegyseg = @Idoegyseg
   IF @UpdatedColumns.exist('/root/FeladatLeiras')=1
         SET @Act_FeladatLeiras = @FeladatLeiras
   IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @Act_Tipus = @Tipus
   IF @UpdatedColumns.exist('/root/Prioritas')=1
         SET @Act_Prioritas = @Prioritas
   IF @UpdatedColumns.exist('/root/LezarasPrioritas')=1
         SET @Act_LezarasPrioritas = @LezarasPrioritas
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/ObjTip_Id')=1
         SET @Act_ObjTip_Id = @ObjTip_Id
   IF @UpdatedColumns.exist('/root/Obj_type')=1
         SET @Act_Obj_type = @Obj_type
   IF @UpdatedColumns.exist('/root/Obj_SzukitoFeltetel')=1
         SET @Act_Obj_SzukitoFeltetel = @Obj_SzukitoFeltetel
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_FeladatDefinicio
SET
     Org = @Act_Org,
     Funkcio_Id_Kivalto = @Act_Funkcio_Id_Kivalto,
     Obj_Metadefinicio_Id = @Act_Obj_Metadefinicio_Id,
     ObjStateValue_Id = @Act_ObjStateValue_Id,
     FeladatDefinicioTipus = @Act_FeladatDefinicioTipus,
     FeladatSorszam = @Act_FeladatSorszam,
     MuveletDefinicio = @Act_MuveletDefinicio,
     SpecMuveletDefinicio = @Act_SpecMuveletDefinicio,
     Funkcio_Id_Inditando = @Act_Funkcio_Id_Inditando,
     Funkcio_Id_Futtatando = @Act_Funkcio_Id_Futtatando,
     BazisObjLeiro = @Act_BazisObjLeiro,
     FelelosTipus = @Act_FelelosTipus,
     Obj_Id_Felelos = @Act_Obj_Id_Felelos,
     FelelosLeiro = @Act_FelelosLeiro,
     Idobazis = @Act_Idobazis,
     ObjTip_Id_DateCol = @Act_ObjTip_Id_DateCol,
     AtfutasiIdo = @Act_AtfutasiIdo,
     Idoegyseg = @Act_Idoegyseg,
     FeladatLeiras = @Act_FeladatLeiras,
     Tipus = @Act_Tipus,
     Prioritas = @Act_Prioritas,
     LezarasPrioritas = @Act_LezarasPrioritas,
     Allapot = @Act_Allapot,
     ObjTip_Id = @Act_ObjTip_Id,
     Obj_type = @Act_Obj_type,
     Obj_SzukitoFeltetel = @Act_Obj_SzukitoFeltetel,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_FeladatDefinicio',@Id
					,'EREC_FeladatDefinicioHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
