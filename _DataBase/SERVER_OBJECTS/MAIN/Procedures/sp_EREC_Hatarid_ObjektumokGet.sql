IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Hatarid_ObjektumokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_Hatarid_ObjektumokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Hatarid_ObjektumokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_Hatarid_ObjektumokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_Hatarid_ObjektumokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_Hatarid_Objektumok.Id,
	   EREC_Hatarid_Objektumok.HataridosFeladat_Id,
	   EREC_Hatarid_Objektumok.Obj_Id,
	   EREC_Hatarid_Objektumok.ObjTip_Id,
	   EREC_Hatarid_Objektumok.Obj_type,
	   EREC_Hatarid_Objektumok.Ver,
	   EREC_Hatarid_Objektumok.Note,
	   EREC_Hatarid_Objektumok.Stat_id,
	   EREC_Hatarid_Objektumok.ErvKezd,
	   EREC_Hatarid_Objektumok.ErvVege,
	   EREC_Hatarid_Objektumok.Letrehozo_id,
	   EREC_Hatarid_Objektumok.LetrehozasIdo,
	   EREC_Hatarid_Objektumok.Modosito_id,
	   EREC_Hatarid_Objektumok.ModositasIdo,
	   EREC_Hatarid_Objektumok.Zarolo_id,
	   EREC_Hatarid_Objektumok.ZarolasIdo,
	   EREC_Hatarid_Objektumok.Tranz_id,
	   EREC_Hatarid_Objektumok.UIAccessLog_id
	   from 
		 EREC_Hatarid_Objektumok as EREC_Hatarid_Objektumok 
	   where
		 EREC_Hatarid_Objektumok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
