IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_HataridosFeladatokGetAllReszfeladat]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_HataridosFeladatokGetAllReszfeladat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_HataridosFeladatokGetAllReszfeladat]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_HataridosFeladatokGetAllReszfeladat] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_HataridosFeladatokGetAllReszfeladat]
  @FeladatId UNIQUEIDENTIFIER,
  @ExecutorUserId uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier

as

begin

BEGIN TRY

   set nocount ON;
   
   WITH FeladatHierarchy  AS
	(
	   -- Base case
	   SELECT Id,HataridosFeladat_Id,
			  0 as HierarchyLevel
	   FROM [EREC_HataridosFeladatok]
	   WHERE Id = @FeladatId

	   UNION ALL

	   -- Recursive step
	   SELECT f.Id,f.HataridosFeladat_Id,
		  fh.HierarchyLevel + 1 AS HierarchyLevel
	   FROM [EREC_HataridosFeladatok] as f
		  INNER JOIN FeladatHierarchy as fh ON
			 f.HataridosFeladat_Id = fh.Id
	)
	
	SELECT *
	FROM FeladatHierarchy
   
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
