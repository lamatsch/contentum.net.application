IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_HataridosFeladatokInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_HataridosFeladatokInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_HataridosFeladatokInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_HataridosFeladatokInsert] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_HataridosFeladatokInsert]    
                @Id      uniqueidentifier = null,    
                               @Esemeny_Id_Kivalto     uniqueidentifier  = null,
                @Esemeny_Id_Lezaro     uniqueidentifier  = null,
                @Funkcio_Id_Inditando     uniqueidentifier  = null,
                @Csoport_Id_Felelos     uniqueidentifier  = null,
                @Felhasznalo_Id_Felelos     uniqueidentifier  = null,
                @Csoport_Id_Kiado     uniqueidentifier  = null,
                @Felhasznalo_Id_Kiado     uniqueidentifier  = null,
                @HataridosFeladat_Id     uniqueidentifier  = null,
                @ReszFeladatSorszam     int  = null,
                @FeladatDefinicio_Id     uniqueidentifier  = null,
                @FeladatDefinicio_Id_Lezaro     uniqueidentifier  = null,
                @Forras     char(1)  = null,
                @Tipus     nvarchar(64)  = null,
                @Altipus     nvarchar(64)  = null,
                @Memo     char(1)  = null,
                @Leiras     Nvarchar(400)  = null,
                @Prioritas     nvarchar(64)  = null,
                @LezarasPrioritas     nvarchar(64)  = null,
                @KezdesiIdo     datetime  = null,
                @IntezkHatarido     datetime  = null,
                @LezarasDatuma     datetime  = null,
                @Azonosito     Nvarchar(100)  = null,
                @Obj_Id     uniqueidentifier  = null,
                @ObjTip_Id     uniqueidentifier  = null,
                @Obj_type     Nvarchar(100)  = null,
                @Allapot     nvarchar(64)  = null,
                @Megoldas     Nvarchar(400)  = null,
                @Note     Nvarchar(4000)  = null,
                @Ver     int  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;

IF (@Obj_id IS NOT NULL) AND (@Obj_type IS NOT NULL)
BEGIN
	IF @ObjTip_Id IS NULL
	BEGIN
		SET @ObjTip_Id = dbo.fn_GetObjTipId(@Obj_type)
	END
	IF @Azonosito IS NULL
	BEGIN
		SET @Azonosito = dbo.fn_GetObjektumAzonosito(@Obj_type,@Obj_id)
	END
END
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Esemeny_Id_Kivalto is not null
         begin
            SET @insertColumns = @insertColumns + ',Esemeny_Id_Kivalto'
            SET @insertValues = @insertValues + ',@Esemeny_Id_Kivalto'
         end 
       
         if @Esemeny_Id_Lezaro is not null
         begin
            SET @insertColumns = @insertColumns + ',Esemeny_Id_Lezaro'
            SET @insertValues = @insertValues + ',@Esemeny_Id_Lezaro'
         end 
       
         if @Funkcio_Id_Inditando is not null
         begin
            SET @insertColumns = @insertColumns + ',Funkcio_Id_Inditando'
            SET @insertValues = @insertValues + ',@Funkcio_Id_Inditando'
         end 
       
         if @Csoport_Id_Felelos is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Felelos'
            SET @insertValues = @insertValues + ',@Csoport_Id_Felelos'
         end 
       
         if @Felhasznalo_Id_Felelos is not null
         begin
            SET @insertColumns = @insertColumns + ',Felhasznalo_Id_Felelos'
            SET @insertValues = @insertValues + ',@Felhasznalo_Id_Felelos'
         end 
       
         if @Csoport_Id_Kiado is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Kiado'
            SET @insertValues = @insertValues + ',@Csoport_Id_Kiado'
         end 
       
         if @Felhasznalo_Id_Kiado is not null
         begin
            SET @insertColumns = @insertColumns + ',Felhasznalo_Id_Kiado'
            SET @insertValues = @insertValues + ',@Felhasznalo_Id_Kiado'
         end 
       
         if @HataridosFeladat_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',HataridosFeladat_Id'
            SET @insertValues = @insertValues + ',@HataridosFeladat_Id'
         end 
       
         if @ReszFeladatSorszam is not null
         begin
            SET @insertColumns = @insertColumns + ',ReszFeladatSorszam'
            SET @insertValues = @insertValues + ',@ReszFeladatSorszam'
         end 
       
         if @FeladatDefinicio_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',FeladatDefinicio_Id'
            SET @insertValues = @insertValues + ',@FeladatDefinicio_Id'
         end 
       
         if @FeladatDefinicio_Id_Lezaro is not null
         begin
            SET @insertColumns = @insertColumns + ',FeladatDefinicio_Id_Lezaro'
            SET @insertValues = @insertValues + ',@FeladatDefinicio_Id_Lezaro'
         end 
       
         if @Forras is not null
         begin
            SET @insertColumns = @insertColumns + ',Forras'
            SET @insertValues = @insertValues + ',@Forras'
         end 
       
         if @Tipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Tipus'
            SET @insertValues = @insertValues + ',@Tipus'
         end 
       
         if @Altipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Altipus'
            SET @insertValues = @insertValues + ',@Altipus'
         end 
       
         if @Memo is not null
         begin
            SET @insertColumns = @insertColumns + ',Memo'
            SET @insertValues = @insertValues + ',@Memo'
         end 
       
         if @Leiras is not null
         begin
            SET @insertColumns = @insertColumns + ',Leiras'
            SET @insertValues = @insertValues + ',@Leiras'
         end 
       
         if @Prioritas is not null
         begin
            SET @insertColumns = @insertColumns + ',Prioritas'
            SET @insertValues = @insertValues + ',@Prioritas'
         end 
       
         if @LezarasPrioritas is not null
         begin
            SET @insertColumns = @insertColumns + ',LezarasPrioritas'
            SET @insertValues = @insertValues + ',@LezarasPrioritas'
         end 
       
         if @KezdesiIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',KezdesiIdo'
            SET @insertValues = @insertValues + ',@KezdesiIdo'
         end 
       
         if @IntezkHatarido is not null
         begin
            SET @insertColumns = @insertColumns + ',IntezkHatarido'
            SET @insertValues = @insertValues + ',@IntezkHatarido'
         end 
       
         if @LezarasDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',LezarasDatuma'
            SET @insertValues = @insertValues + ',@LezarasDatuma'
         end 
       
         if @Azonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Azonosito'
            SET @insertValues = @insertValues + ',@Azonosito'
         end 
       
         if @Obj_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Obj_Id'
            SET @insertValues = @insertValues + ',@Obj_Id'
         end 
       
         if @ObjTip_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',ObjTip_Id'
            SET @insertValues = @insertValues + ',@ObjTip_Id'
         end 
       
         if @Obj_type is not null
         begin
            SET @insertColumns = @insertColumns + ',Obj_type'
            SET @insertValues = @insertValues + ',@Obj_type'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @Megoldas is not null
         begin
            SET @insertColumns = @insertColumns + ',Megoldas'
            SET @insertValues = @insertValues + ',@Megoldas'
         end 
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_HataridosFeladatok ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Esemeny_Id_Kivalto uniqueidentifier,@Esemeny_Id_Lezaro uniqueidentifier,@Funkcio_Id_Inditando uniqueidentifier,@Csoport_Id_Felelos uniqueidentifier,@Felhasznalo_Id_Felelos uniqueidentifier,@Csoport_Id_Kiado uniqueidentifier,@Felhasznalo_Id_Kiado uniqueidentifier,@HataridosFeladat_Id uniqueidentifier,@ReszFeladatSorszam int,@FeladatDefinicio_Id uniqueidentifier,@FeladatDefinicio_Id_Lezaro uniqueidentifier,@Forras char(1),@Tipus nvarchar(64),@Altipus nvarchar(64),@Memo char(1),@Leiras Nvarchar(400),@Prioritas nvarchar(64),@LezarasPrioritas nvarchar(64),@KezdesiIdo datetime,@IntezkHatarido datetime,@LezarasDatuma datetime,@Azonosito Nvarchar(100),@Obj_Id uniqueidentifier,@ObjTip_Id uniqueidentifier,@Obj_type Nvarchar(100),@Allapot nvarchar(64),@Megoldas Nvarchar(400),@Note Nvarchar(4000),@Ver int,@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Esemeny_Id_Kivalto = @Esemeny_Id_Kivalto,@Esemeny_Id_Lezaro = @Esemeny_Id_Lezaro,@Funkcio_Id_Inditando = @Funkcio_Id_Inditando,@Csoport_Id_Felelos = @Csoport_Id_Felelos,@Felhasznalo_Id_Felelos = @Felhasznalo_Id_Felelos,@Csoport_Id_Kiado = @Csoport_Id_Kiado,@Felhasznalo_Id_Kiado = @Felhasznalo_Id_Kiado,@HataridosFeladat_Id = @HataridosFeladat_Id,@ReszFeladatSorszam = @ReszFeladatSorszam,@FeladatDefinicio_Id = @FeladatDefinicio_Id,@FeladatDefinicio_Id_Lezaro = @FeladatDefinicio_Id_Lezaro,@Forras = @Forras,@Tipus = @Tipus,@Altipus = @Altipus,@Memo = @Memo,@Leiras = @Leiras,@Prioritas = @Prioritas,@LezarasPrioritas = @LezarasPrioritas,@KezdesiIdo = @KezdesiIdo,@IntezkHatarido = @IntezkHatarido,@LezarasDatuma = @LezarasDatuma,@Azonosito = @Azonosito,@Obj_Id = @Obj_Id,@ObjTip_Id = @ObjTip_Id,@Obj_type = @Obj_type,@Allapot = @Allapot,@Megoldas = @Megoldas,@Note = @Note,@Ver = @Ver,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_HataridosFeladatok',@ResultUid
					,'EREC_HataridosFeladatokHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
