IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_HataridosFeladatokUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_HataridosFeladatokUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_HataridosFeladatokUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_HataridosFeladatokUpdate] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_HataridosFeladatokUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Esemeny_Id_Kivalto     uniqueidentifier  = null,         
             @Esemeny_Id_Lezaro     uniqueidentifier  = null,         
             @Funkcio_Id_Inditando     uniqueidentifier  = null,         
             @Csoport_Id_Felelos     uniqueidentifier  = null,         
             @Felhasznalo_Id_Felelos     uniqueidentifier  = null,         
             @Csoport_Id_Kiado     uniqueidentifier  = null,         
             @Felhasznalo_Id_Kiado     uniqueidentifier  = null,         
             @HataridosFeladat_Id     uniqueidentifier  = null,         
             @ReszFeladatSorszam     int  = null,         
             @FeladatDefinicio_Id     uniqueidentifier  = null,         
             @FeladatDefinicio_Id_Lezaro     uniqueidentifier  = null,         
             @Forras     char(1)  = null,         
             @Tipus     nvarchar(64)  = null,         
             @Altipus     nvarchar(64)  = null,         
             @Memo     char(1)  = null,         
             @Leiras     Nvarchar(400)  = null,         
             @Prioritas     nvarchar(64)  = null,         
             @LezarasPrioritas     nvarchar(64)  = null,         
             @KezdesiIdo     datetime  = null,         
             @IntezkHatarido     datetime  = null,         
             @LezarasDatuma     datetime  = null,         
             @Azonosito     Nvarchar(100)  = null,         
             @Obj_Id     uniqueidentifier  = null,         
             @ObjTip_Id     uniqueidentifier  = null,         
             @Obj_type     Nvarchar(100)  = null,         
             @Allapot     nvarchar(64)  = null,         
             @Megoldas     Nvarchar(400)  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Ver     int  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Esemeny_Id_Kivalto     uniqueidentifier         
     DECLARE @Act_Esemeny_Id_Lezaro     uniqueidentifier         
     DECLARE @Act_Funkcio_Id_Inditando     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Felelos     uniqueidentifier         
     DECLARE @Act_Felhasznalo_Id_Felelos     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Kiado     uniqueidentifier         
     DECLARE @Act_Felhasznalo_Id_Kiado     uniqueidentifier         
     DECLARE @Act_HataridosFeladat_Id     uniqueidentifier         
     DECLARE @Act_ReszFeladatSorszam     int         
     DECLARE @Act_FeladatDefinicio_Id     uniqueidentifier         
     DECLARE @Act_FeladatDefinicio_Id_Lezaro     uniqueidentifier         
     DECLARE @Act_Forras     char(1)         
     DECLARE @Act_Tipus     nvarchar(64)         
     DECLARE @Act_Altipus     nvarchar(64)         
     DECLARE @Act_Memo     char(1)         
     DECLARE @Act_Leiras     Nvarchar(400)         
     DECLARE @Act_Prioritas     nvarchar(64)         
     DECLARE @Act_LezarasPrioritas     nvarchar(64)         
     DECLARE @Act_KezdesiIdo     datetime         
     DECLARE @Act_IntezkHatarido     datetime         
     DECLARE @Act_LezarasDatuma     datetime         
     DECLARE @Act_Azonosito     Nvarchar(100)         
     DECLARE @Act_Obj_Id     uniqueidentifier         
     DECLARE @Act_ObjTip_Id     uniqueidentifier         
     DECLARE @Act_Obj_type     Nvarchar(100)         
     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_Megoldas     Nvarchar(400)         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Esemeny_Id_Kivalto = Esemeny_Id_Kivalto,
     @Act_Esemeny_Id_Lezaro = Esemeny_Id_Lezaro,
     @Act_Funkcio_Id_Inditando = Funkcio_Id_Inditando,
     @Act_Csoport_Id_Felelos = Csoport_Id_Felelos,
     @Act_Felhasznalo_Id_Felelos = Felhasznalo_Id_Felelos,
     @Act_Csoport_Id_Kiado = Csoport_Id_Kiado,
     @Act_Felhasznalo_Id_Kiado = Felhasznalo_Id_Kiado,
     @Act_HataridosFeladat_Id = HataridosFeladat_Id,
     @Act_ReszFeladatSorszam = ReszFeladatSorszam,
     @Act_FeladatDefinicio_Id = FeladatDefinicio_Id,
     @Act_FeladatDefinicio_Id_Lezaro = FeladatDefinicio_Id_Lezaro,
     @Act_Forras = Forras,
     @Act_Tipus = Tipus,
     @Act_Altipus = Altipus,
     @Act_Memo = Memo,
     @Act_Leiras = Leiras,
     @Act_Prioritas = Prioritas,
     @Act_LezarasPrioritas = LezarasPrioritas,
     @Act_KezdesiIdo = KezdesiIdo,
     @Act_IntezkHatarido = IntezkHatarido,
     @Act_LezarasDatuma = LezarasDatuma,
     @Act_Azonosito = Azonosito,
     @Act_Obj_Id = Obj_Id,
     @Act_ObjTip_Id = ObjTip_Id,
     @Act_Obj_type = Obj_type,
     @Act_Allapot = Allapot,
     @Act_Megoldas = Megoldas,
     @Act_Note = Note,
     @Act_Ver = Ver,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_HataridosFeladatok
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Esemeny_Id_Kivalto')=1
         SET @Act_Esemeny_Id_Kivalto = @Esemeny_Id_Kivalto
   IF @UpdatedColumns.exist('/root/Esemeny_Id_Lezaro')=1
         SET @Act_Esemeny_Id_Lezaro = @Esemeny_Id_Lezaro
   IF @UpdatedColumns.exist('/root/Funkcio_Id_Inditando')=1
         SET @Act_Funkcio_Id_Inditando = @Funkcio_Id_Inditando
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1
         SET @Act_Csoport_Id_Felelos = @Csoport_Id_Felelos
   IF @UpdatedColumns.exist('/root/Felhasznalo_Id_Felelos')=1
         SET @Act_Felhasznalo_Id_Felelos = @Felhasznalo_Id_Felelos
   IF @UpdatedColumns.exist('/root/Csoport_Id_Kiado')=1
         SET @Act_Csoport_Id_Kiado = @Csoport_Id_Kiado
   IF @UpdatedColumns.exist('/root/Felhasznalo_Id_Kiado')=1
         SET @Act_Felhasznalo_Id_Kiado = @Felhasznalo_Id_Kiado
   IF @UpdatedColumns.exist('/root/HataridosFeladat_Id')=1
         SET @Act_HataridosFeladat_Id = @HataridosFeladat_Id
   IF @UpdatedColumns.exist('/root/ReszFeladatSorszam')=1
         SET @Act_ReszFeladatSorszam = @ReszFeladatSorszam
   IF @UpdatedColumns.exist('/root/FeladatDefinicio_Id')=1
         SET @Act_FeladatDefinicio_Id = @FeladatDefinicio_Id
   IF @UpdatedColumns.exist('/root/FeladatDefinicio_Id_Lezaro')=1
         SET @Act_FeladatDefinicio_Id_Lezaro = @FeladatDefinicio_Id_Lezaro
   IF @UpdatedColumns.exist('/root/Forras')=1
         SET @Act_Forras = @Forras
   IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @Act_Tipus = @Tipus
   IF @UpdatedColumns.exist('/root/Altipus')=1
         SET @Act_Altipus = @Altipus
   IF @UpdatedColumns.exist('/root/Memo')=1
         SET @Act_Memo = @Memo
   IF @UpdatedColumns.exist('/root/Leiras')=1
         SET @Act_Leiras = @Leiras
   IF @UpdatedColumns.exist('/root/Prioritas')=1
         SET @Act_Prioritas = @Prioritas
   IF @UpdatedColumns.exist('/root/LezarasPrioritas')=1
         SET @Act_LezarasPrioritas = @LezarasPrioritas
   IF @UpdatedColumns.exist('/root/KezdesiIdo')=1
         SET @Act_KezdesiIdo = @KezdesiIdo
   IF @UpdatedColumns.exist('/root/IntezkHatarido')=1
         SET @Act_IntezkHatarido = @IntezkHatarido
   IF @UpdatedColumns.exist('/root/LezarasDatuma')=1
         SET @Act_LezarasDatuma = @LezarasDatuma
   IF @UpdatedColumns.exist('/root/Azonosito')=1
         SET @Act_Azonosito = @Azonosito
   IF @UpdatedColumns.exist('/root/Obj_Id')=1
         SET @Act_Obj_Id = @Obj_Id
   IF @UpdatedColumns.exist('/root/ObjTip_Id')=1
         SET @Act_ObjTip_Id = @ObjTip_Id
   IF @UpdatedColumns.exist('/root/Obj_type')=1
         SET @Act_Obj_type = @Obj_type
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/Megoldas')=1
         SET @Act_Megoldas = @Megoldas
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_HataridosFeladatok
SET
     Esemeny_Id_Kivalto = @Act_Esemeny_Id_Kivalto,
     Esemeny_Id_Lezaro = @Act_Esemeny_Id_Lezaro,
     Funkcio_Id_Inditando = @Act_Funkcio_Id_Inditando,
     Csoport_Id_Felelos = @Act_Csoport_Id_Felelos,
     Felhasznalo_Id_Felelos = @Act_Felhasznalo_Id_Felelos,
     Csoport_Id_Kiado = @Act_Csoport_Id_Kiado,
     Felhasznalo_Id_Kiado = @Act_Felhasznalo_Id_Kiado,
     HataridosFeladat_Id = @Act_HataridosFeladat_Id,
     ReszFeladatSorszam = @Act_ReszFeladatSorszam,
     FeladatDefinicio_Id = @Act_FeladatDefinicio_Id,
     FeladatDefinicio_Id_Lezaro = @Act_FeladatDefinicio_Id_Lezaro,
     Forras = @Act_Forras,
     Tipus = @Act_Tipus,
     Altipus = @Act_Altipus,
     Memo = @Act_Memo,
     Leiras = @Act_Leiras,
     Prioritas = @Act_Prioritas,
     LezarasPrioritas = @Act_LezarasPrioritas,
     KezdesiIdo = @Act_KezdesiIdo,
     IntezkHatarido = @Act_IntezkHatarido,
     LezarasDatuma = @Act_LezarasDatuma,
     Azonosito = @Act_Azonosito,
     Obj_Id = @Act_Obj_Id,
     ObjTip_Id = @Act_ObjTip_Id,
     Obj_type = @Act_Obj_type,
     Allapot = @Act_Allapot,
     Megoldas = @Act_Megoldas,
     Note = @Act_Note,
     Ver = @Act_Ver,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_HataridosFeladatok',@Id
					,'EREC_HataridosFeladatokHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
