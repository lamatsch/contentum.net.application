IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraElosztoivTetelekGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraElosztoivTetelekGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraElosztoivTetelekGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraElosztoivTetelekGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraElosztoivTetelekGetAllWithExtension]
  @Where   nvarchar(4000) = '',
  @OrderBy nvarchar(200)  = ' order by   EREC_IraElosztoivTetelek.LetrehozasIdo',
  @TopRow  nvarchar(5)    = '',
  @ExecutorUserId uniqueidentifier
as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
	'SELECT ' + @LocalTopRow + '
		EREC_IraElosztoivTetelek.Id,
		EREC_IraElosztoivTetelek.Sorszam,
		EREC_IraElosztoivTetelek.Partner_Id,
			KRT_Partnerek.Nev AS Partner_Nev,
		EREC_IraElosztoivTetelek.NevSTR,
		EREC_IraElosztoivTetelek.Cim_Id,
		EREC_IraElosztoivTetelek.CimSTR,
		EREC_IraElosztoivTetelek.Kuldesmod,
			kt1.Nev AS Kuldesmod_Nev,
		EREC_IraElosztoivTetelek.VisszavarasiIdo,
		EREC_IraElosztoivTetelek.Vissza_Nap_Mulva,
		EREC_IraElosztoivTetelek.AlairoSzerep,
			kt3.Nev AS AlairoSzerep_Nev
	FROM EREC_IraElosztoivTetelek
		LEFT JOIN KRT_Partnerek ON KRT_Partnerek.Id = EREC_IraElosztoivTetelek.Partner_Id
		LEFT JOIN KRT_Cimek ON KRT_Cimek.Id = EREC_IraElosztoivTetelek.Cim_Id
		LEFT JOIN KRT_KodCsoportok AS kcs1 ON kcs1.Kod = ''KULDEMENY_KULDES_MODJA''
		LEFT JOIN KRT_KodTarak AS kt1 ON kt1.KodCsoport_Id = kcs1.Id AND kt1.Kod = EREC_IraElosztoivTetelek.Kuldesmod and kt1.Org=''' + CAST(@Org as NVarChar(40)) + '''
		LEFT JOIN KRT_KodCsoportok AS kcs3 ON kcs3.Kod = ''ALAIRO_SZEREP''
		LEFT JOIN KRT_KodTarak AS kt3 ON kt3.KodCsoport_Id = kcs3.Id AND kt3.Kod = EREC_IraElosztoivTetelek.AlairoSzerep and kt3.Org=''' + CAST(@Org as NVarChar(40)) + '''
	WHERE 1 = 1'

   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
