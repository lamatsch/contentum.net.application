IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraElosztoivTetelekInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraElosztoivTetelekInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraElosztoivTetelekInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraElosztoivTetelekInsert] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraElosztoivTetelekInsert]    
                @Id      uniqueidentifier = null,    
               	            @ElosztoIv_Id     uniqueidentifier,
	            @Sorszam     int,
	            @Partner_Id     uniqueidentifier,
                @Cim_Id     uniqueidentifier  = null,
                @CimSTR     Nvarchar(400)  = null,
                @NevSTR     Nvarchar(400)  = null,
                @Kuldesmod     nvarchar(64)  = null,
                @Visszavarolag     char(1)  = null,
                @VisszavarasiIdo     datetime  = null,
                @Vissza_Nap_Mulva     int  = null,
                @AlairoSzerep     nvarchar(64)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @ElosztoIv_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',ElosztoIv_Id'
            SET @insertValues = @insertValues + ',@ElosztoIv_Id'
         end 
       
         if @Sorszam is not null
         begin
            SET @insertColumns = @insertColumns + ',Sorszam'
            SET @insertValues = @insertValues + ',@Sorszam'
         end 
       
         if @Partner_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Partner_Id'
            SET @insertValues = @insertValues + ',@Partner_Id'
         end 
       
         if @Cim_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Cim_Id'
            SET @insertValues = @insertValues + ',@Cim_Id'
         end 
       
         if @CimSTR is not null
         begin
            SET @insertColumns = @insertColumns + ',CimSTR'
            SET @insertValues = @insertValues + ',@CimSTR'
         end 
       
         if @NevSTR is not null
         begin
            SET @insertColumns = @insertColumns + ',NevSTR'
            SET @insertValues = @insertValues + ',@NevSTR'
         end 
       
         if @Kuldesmod is not null
         begin
            SET @insertColumns = @insertColumns + ',Kuldesmod'
            SET @insertValues = @insertValues + ',@Kuldesmod'
         end 
       
         if @Visszavarolag is not null
         begin
            SET @insertColumns = @insertColumns + ',Visszavarolag'
            SET @insertValues = @insertValues + ',@Visszavarolag'
         end 
       
         if @VisszavarasiIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',VisszavarasiIdo'
            SET @insertValues = @insertValues + ',@VisszavarasiIdo'
         end 
       
         if @Vissza_Nap_Mulva is not null
         begin
            SET @insertColumns = @insertColumns + ',Vissza_Nap_Mulva'
            SET @insertValues = @insertValues + ',@Vissza_Nap_Mulva'
         end 
       
         if @AlairoSzerep is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairoSzerep'
            SET @insertValues = @insertValues + ',@AlairoSzerep'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_IraElosztoivTetelek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@ElosztoIv_Id uniqueidentifier,@Sorszam int,@Partner_Id uniqueidentifier,@Cim_Id uniqueidentifier,@CimSTR Nvarchar(400),@NevSTR Nvarchar(400),@Kuldesmod nvarchar(64),@Visszavarolag char(1),@VisszavarasiIdo datetime,@Vissza_Nap_Mulva int,@AlairoSzerep nvarchar(64),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@ElosztoIv_Id = @ElosztoIv_Id,@Sorszam = @Sorszam,@Partner_Id = @Partner_Id,@Cim_Id = @Cim_Id,@CimSTR = @CimSTR,@NevSTR = @NevSTR,@Kuldesmod = @Kuldesmod,@Visszavarolag = @Visszavarolag,@VisszavarasiIdo = @VisszavarasiIdo,@Vissza_Nap_Mulva = @Vissza_Nap_Mulva,@AlairoSzerep = @AlairoSzerep,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_IraElosztoivTetelek',@ResultUid
					,'EREC_IraElosztoivTetelekHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
