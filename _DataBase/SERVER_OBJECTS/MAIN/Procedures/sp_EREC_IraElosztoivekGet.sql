IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraElosztoivekGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraElosztoivekGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraElosztoivekGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraElosztoivekGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraElosztoivekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IraElosztoivek.Id,
	   EREC_IraElosztoivek.Org,
	   EREC_IraElosztoivek.Ev,
	   EREC_IraElosztoivek.Fajta,
	   EREC_IraElosztoivek.Kod,
	   EREC_IraElosztoivek.NEV,
	   EREC_IraElosztoivek.Hasznalat,
	   EREC_IraElosztoivek.Csoport_Id_Tulaj,
	   EREC_IraElosztoivek.Ver,
	   EREC_IraElosztoivek.Note,
	   EREC_IraElosztoivek.Stat_id,
	   EREC_IraElosztoivek.ErvKezd,
	   EREC_IraElosztoivek.ErvVege,
	   EREC_IraElosztoivek.Letrehozo_id,
	   EREC_IraElosztoivek.LetrehozasIdo,
	   EREC_IraElosztoivek.Modosito_id,
	   EREC_IraElosztoivek.ModositasIdo,
	   EREC_IraElosztoivek.Zarolo_id,
	   EREC_IraElosztoivek.ZarolasIdo,
	   EREC_IraElosztoivek.Tranz_id,
	   EREC_IraElosztoivek.UIAccessLog_id
	   from 
		 EREC_IraElosztoivek as EREC_IraElosztoivek 
	   where
		 EREC_IraElosztoivek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
