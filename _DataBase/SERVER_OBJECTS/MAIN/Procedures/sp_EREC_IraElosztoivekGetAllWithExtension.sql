IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraElosztoivekGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraElosztoivekGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraElosztoivekGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraElosztoivekGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraElosztoivekGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraElosztoivek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
	'SELECT ' + @LocalTopRow + '
		EREC_IraElosztoivek.Id,
		EREC_IraElosztoivek.Ev,
		EREC_IraElosztoivek.Fajta,
			KRT_KodTarak.Nev AS Fajta_Nev,
		EREC_IraElosztoivek.Kod,
		EREC_IraElosztoivek.NEV,
		EREC_IraElosztoivek.Csoport_Id_Tulaj,
			KRT_Csoportok.Nev AS Csoport_Id_Tulaj_Nev,
	   EREC_IraElosztoivek.Zarolo_id,
	   EREC_IraElosztoivek.ZarolasIdo
	FROM EREC_IraElosztoivek 
		LEFT JOIN KRT_Csoportok ON KRT_Csoportok.Id = EREC_IraElosztoivek.Csoport_Id_Tulaj
		LEFT JOIN KRT_KodCsoportok ON KRT_KodCsoportok.Kod = ''ELOSZTOIV_FAJTA''
		LEFT JOIN KRT_KodTarak ON KRT_KodTarak.KodCsoport_Id = KRT_KodCsoportok.Id AND KRT_KodTarak.Kod = EREC_IraElosztoivek.Fajta and KRT_KodTarak.Org=''' + CAST(@Org as NVarChar(40)) + '''
    Where EREC_IraElosztoivek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''

   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
