IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraElosztoivekHistoryGetAllRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraElosztoivekHistoryGetAllRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraElosztoivekHistoryGetAllRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraElosztoivekHistoryGetAllRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraElosztoivekHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_IraElosztoivekHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivekHistory Old
         inner join EREC_IraElosztoivekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Org' as ColumnName,               cast(Old.Org as nvarchar(99)) as OldValue,
               cast(New.Org as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivekHistory Old
         inner join EREC_IraElosztoivekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Org != New.Org 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ev' as ColumnName,               cast(Old.Ev as nvarchar(99)) as OldValue,
               cast(New.Ev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivekHistory Old
         inner join EREC_IraElosztoivekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Ev != New.Ev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Fajta' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivekHistory Old
         inner join EREC_IraElosztoivekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Fajta != New.Fajta 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'ELOSZTOIV_FAJTA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Fajta and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Fajta and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kod' as ColumnName,               cast(Old.Kod as nvarchar(99)) as OldValue,
               cast(New.Kod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivekHistory Old
         inner join EREC_IraElosztoivekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Kod != New.Kod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'NEV' as ColumnName,               cast(Old.NEV as nvarchar(99)) as OldValue,
               cast(New.NEV as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivekHistory Old
         inner join EREC_IraElosztoivekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.NEV != New.NEV 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Hasznalat' as ColumnName,               cast(Old.Hasznalat as nvarchar(99)) as OldValue,
               cast(New.Hasznalat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivekHistory Old
         inner join EREC_IraElosztoivekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Hasznalat != New.Hasznalat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Tulaj' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Tulaj) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Tulaj) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivekHistory Old
         inner join EREC_IraElosztoivekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Tulaj != New.Csoport_Id_Tulaj 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.Csoport_Id_Tulaj --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Csoport_Id_Tulaj --and FTNew.Ver = New.Ver      
)
            
end


GO
