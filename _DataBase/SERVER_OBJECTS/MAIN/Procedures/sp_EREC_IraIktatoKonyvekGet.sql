IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIktatoKonyvekGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraIktatoKonyvekGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIktatoKonyvekGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraIktatoKonyvekGet] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IraIktatoKonyvekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IraIktatoKonyvek.Id,
	   EREC_IraIktatoKonyvek.Org,
	   EREC_IraIktatoKonyvek.Ev,
	   EREC_IraIktatoKonyvek.Azonosito,
	   EREC_IraIktatoKonyvek.DefaultIrattariTetelszam,
	   EREC_IraIktatoKonyvek.Nev,
	   EREC_IraIktatoKonyvek.MegkulJelzes,
	   EREC_IraIktatoKonyvek.Iktatohely,
	   EREC_IraIktatoKonyvek.KozpontiIktatasJelzo,
	   EREC_IraIktatoKonyvek.UtolsoFoszam,
	   EREC_IraIktatoKonyvek.UtolsoSorszam,
	   EREC_IraIktatoKonyvek.Csoport_Id_Olvaso,
	   EREC_IraIktatoKonyvek.Csoport_Id_Tulaj,
	   EREC_IraIktatoKonyvek.IktSzamOsztas,
	   EREC_IraIktatoKonyvek.FormatumKod,
	   EREC_IraIktatoKonyvek.Titkos,
	   EREC_IraIktatoKonyvek.IktatoErkezteto,
	   EREC_IraIktatoKonyvek.LezarasDatuma,
	   EREC_IraIktatoKonyvek.PostakonyvVevokod,
	   EREC_IraIktatoKonyvek.PostakonyvMegallapodasAzon,
	   EREC_IraIktatoKonyvek.EFeladoJegyzekUgyfelAdatok,
	   EREC_IraIktatoKonyvek.Ver,
	   EREC_IraIktatoKonyvek.Note,
	   EREC_IraIktatoKonyvek.Stat_id,
	   EREC_IraIktatoKonyvek.ErvKezd,
	   EREC_IraIktatoKonyvek.ErvVege,
	   EREC_IraIktatoKonyvek.Letrehozo_id,
	   EREC_IraIktatoKonyvek.LetrehozasIdo,
	   EREC_IraIktatoKonyvek.Modosito_id,
	   EREC_IraIktatoKonyvek.ModositasIdo,
	   EREC_IraIktatoKonyvek.Zarolo_id,
	   EREC_IraIktatoKonyvek.ZarolasIdo,
	   EREC_IraIktatoKonyvek.Tranz_id,
	   EREC_IraIktatoKonyvek.UIAccessLog_id,
	   EREC_IraIktatoKonyvek.Statusz,
	   -- CR3355
	   EREC_IraIktatoKonyvek.Terjedelem,
	   EREC_IraIktatoKonyvek.SelejtezesDatuma,
	   EREC_IraIktatoKonyvek.KezelesTipusa
	   from 
		 EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek 
	   where
		 EREC_IraIktatoKonyvek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
