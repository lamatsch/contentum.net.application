IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIktatoKonyvekGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraIktatoKonyvekGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIktatoKonyvekGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraIktatoKonyvekGetAll] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IraIktatoKonyvekGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraIktatoKonyvek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('50202',16,1)
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_IraIktatoKonyvek.Id,
	   EREC_IraIktatoKonyvek.Org,
	   EREC_IraIktatoKonyvek.Ev,
	   EREC_IraIktatoKonyvek.Azonosito,
	   EREC_IraIktatoKonyvek.DefaultIrattariTetelszam,
	   EREC_IraIktatoKonyvek.Nev,
	   EREC_IraIktatoKonyvek.MegkulJelzes,
	   EREC_IraIktatoKonyvek.Iktatohely,
	   EREC_IraIktatoKonyvek.KozpontiIktatasJelzo,
	   EREC_IraIktatoKonyvek.UtolsoFoszam,
	   EREC_IraIktatoKonyvek.UtolsoSorszam,
	   EREC_IraIktatoKonyvek.Csoport_Id_Olvaso,
	   EREC_IraIktatoKonyvek.Csoport_Id_Tulaj,
	   EREC_IraIktatoKonyvek.IktSzamOsztas,
	   EREC_IraIktatoKonyvek.FormatumKod,
	   EREC_IraIktatoKonyvek.Titkos,
	   EREC_IraIktatoKonyvek.IktatoErkezteto,
	   EREC_IraIktatoKonyvek.LezarasDatuma,
	   EREC_IraIktatoKonyvek.PostakonyvVevokod,
	   EREC_IraIktatoKonyvek.PostakonyvMegallapodasAzon,
	   EREC_IraIktatoKonyvek.EFeladoJegyzekUgyfelAdatok,
	   EREC_IraIktatoKonyvek.Ver,
	   EREC_IraIktatoKonyvek.Note,
	   EREC_IraIktatoKonyvek.Stat_id,
	   EREC_IraIktatoKonyvek.ErvKezd,
	   EREC_IraIktatoKonyvek.ErvVege,
	   EREC_IraIktatoKonyvek.Letrehozo_id,
	   EREC_IraIktatoKonyvek.LetrehozasIdo,
	   EREC_IraIktatoKonyvek.Modosito_id,
	   EREC_IraIktatoKonyvek.ModositasIdo,
	   EREC_IraIktatoKonyvek.Zarolo_id,
	   EREC_IraIktatoKonyvek.ZarolasIdo,
	   EREC_IraIktatoKonyvek.Tranz_id,
	   EREC_IraIktatoKonyvek.UIAccessLog_id,
	   EREC_IraIktatoKonyvek.Statusz,
	    -- CR3355
	   EREC_IraIktatoKonyvek.Terjedelem,
	   EREC_IraIktatoKonyvek.SelejtezesDatuma,
	   EREC_IraIktatoKonyvek.KezelesTipusa  
   from 
     EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek      
    Where EREC_IraIktatoKonyvek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
