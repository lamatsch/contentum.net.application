IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIktatoKonyvekGetAllWithCount]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraIktatoKonyvekGetAllWithCount]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIktatoKonyvekGetAllWithCount]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraIktatoKonyvekGetAllWithCount] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraIktatoKonyvekGetAllWithCount]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by  ugyiratok.Iktatohely,ugyiratok.Ev',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier,
  @Jogosultak	char(1) = '0',
  @KezdDate nvarchar(23),
  @VegeDate nvarchar(23)

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(max)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
  SET @sqlcmd = 
  '
	SELECT
		EREC_IraIktatoKonyvek.Iktatohely,
		EREC_IraIktatoKonyvek.Nev,
		EREC_IraIktatoKonyvek.Ev,
		EREC_IraIratok.Id,
		EREC_Ugyugyiratok.Allapot,
		EREC_Ugyugyiratok.TovabbitasAlattAllapot INTO #iratTemp
	from  EREC_IraIktatoKonyvek       
		left join EREC_Ugyugyiratok on EREC_Ugyugyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id       
		left join EREC_IraIratok on EREC_IraIratok.Ugyirat_Id = EREC_Ugyugyiratok.Id 
			and EREC_IraIratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''' ';
	set @sqlcmd = @sqlcmd + N'
   Where EREC_IraIktatoKonyvek.IktatoErkezteto=''I'' and EREC_IraIktatoKonyvek.Ev between YEAR(''' + @KezdDate + ''') and YEAR(''' + @VegeDate + ''')
	and EREC_IraIktatoKonyvek.Org=''' + CAST(@Org as Nvarchar(40)) + ''' '
	
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
	
	IF @Jogosultak = 1 AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
	BEGIN
		set @sqlcmd = @sqlcmd + N' AND EREC_IraIktatoKonyvek.Id IN 
						(select EREC_IraIktatoKonyvek.Id 
							from EREC_IraIktatoKonyvek
								INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_IraIktatoKonyvek.Id
								INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
						UNION ALL
						SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''', ''EREC_IraIktatoKonyvek'')
							 )';
	END

  
  set @sqlcmd = @sqlcmd +
  'select ' + @LocalTopRow + '
		ugyiratok.Iktatohely,
		ugyiratok.Nev,
		ugyiratok.Ev,
		ugyiratok.FoszamDb,
		iratok.AlszamDb,
		ugyiratok.IktatottDb,
		cast((CAST(ugyiratok.IktatottDb AS decimal(10,1)) * 100) / CAST((case when ugyiratok.FoszamDb = 0 then 1 else ugyiratok.FoszamDb end) as decimal(10,1)) as decimal(10,1)) as IktatottSzazalek,
		ugyiratok.UgyintezesAlattDb,
		cast((CAST(ugyiratok.UgyintezesAlattDb AS decimal(10,1)) * 100) / CAST((case when ugyiratok.FoszamDb = 0 then 1 else ugyiratok.FoszamDb end) as decimal(10,1)) as decimal(10,1)) as UgyintezesAlattSzazalek,
		ugyiratok.SkontrobanDb,
		cast((CAST(ugyiratok.SkontrobanDb AS decimal(10,1)) * 100) / CAST((case when ugyiratok.FoszamDb = 0 then 1 else ugyiratok.FoszamDb end) as decimal(10,1)) as decimal(10,1)) as SkontrobanSzazalek,
		ugyiratok.IrattarbanDb,
		cast((CAST(ugyiratok.IrattarbanDb AS decimal(10,1)) * 100) / CAST((case when ugyiratok.FoszamDb = 0 then 1 else ugyiratok.FoszamDb end) as decimal(10,1)) as decimal(10,1)) as IrattarbanSzazalek,
		ugyiratok.UgyintezesAlatt2Db,
		ugyiratok.SztornozottDb,
		ugyiratok.SzereltDb,
		ugyiratok.SelejtezettDb,
		ugyiratok.LeveltarbaAdottDb,
		ugyiratok.idoszaki_FoszamDb,
		ugyiratok.idoszaki_FoszamDb_sztornoval,
		iratok.idoszaki_AlszamDb,
		iratok.idoszaki_AlszamDb_sztornoval,
		ugyiratok.idoszaki_UgyintezesAlattDb,
		cast((CAST(ugyiratok.idoszaki_UgyintezesAlattDb AS decimal(10,1)) * 100) / CAST((case when ugyiratok.idoszaki_FoszamDb = 0 then 1 else ugyiratok.idoszaki_FoszamDb end) as decimal(10,1)) as decimal(10,1)) as idoszaki_UgyintezesAlattSzazalek,
		cast((CAST(ugyiratok.idoszaki_UgyintezesAlattDb AS decimal(10,1)) * 100) / CAST((case when ugyiratok.idoszaki_FoszamDb_sztornoval = 0 then 1 else ugyiratok.idoszaki_FoszamDb_sztornoval end) as decimal(10,1)) as decimal(10,1)) as idoszaki_UgyintezesAlattSzazalek_sztornoval,
		cast((CAST(ugyiratok.SkontrobanDb AS decimal(10,1)) * 100) / CAST((case when ugyiratok.idoszaki_FoszamDb = 0 then 1 else ugyiratok.idoszaki_FoszamDb end) as decimal(10,1)) as decimal(10,1)) as idoszaki_SkontrobanSzazalek,
		cast((CAST(ugyiratok.SkontrobanDb AS decimal(10,1)) * 100) / CAST((case when ugyiratok.idoszaki_FoszamDb_sztornoval = 0 then 1 else ugyiratok.idoszaki_FoszamDb_sztornoval end) as decimal(10,1)) as decimal(10,1)) as idoszaki_SkontrobanSzazalek_sztornoval,
		ugyiratok.idoszaki_IrattarbanDb,
		cast((CAST(ugyiratok.idoszaki_IrattarbanDb AS decimal(10,1)) * 100) / CAST((case when ugyiratok.idoszaki_FoszamDb = 0 then 1 else ugyiratok.idoszaki_FoszamDb end) as decimal(10,1)) as decimal(10,1)) as idoszaki_IrattarbanSzazalek,
		cast((CAST(ugyiratok.idoszaki_IrattarbanDb AS decimal(10,1)) * 100) / CAST((case when ugyiratok.idoszaki_FoszamDb_sztornoval = 0 then 1 else ugyiratok.idoszaki_FoszamDb_sztornoval end) as decimal(10,1)) as decimal(10,1)) as idoszaki_IrattarbanSzazalek_sztornoval,
		ugyiratok.EgyebDb,
		cast((CAST(ugyiratok.SzereltDb AS decimal(10,1)) * 100) / CAST((case when ugyiratok.idoszaki_FoszamDb = 0 then 1 else ugyiratok.idoszaki_FoszamDb end) as decimal(10,1)) as decimal(10,1)) as idoszaki_SzereltSzazalek,
		cast((CAST(ugyiratok.SzereltDb AS decimal(10,1)) * 100) / CAST((case when ugyiratok.idoszaki_FoszamDb_sztornoval = 0 then 1 else ugyiratok.idoszaki_FoszamDb_sztornoval end) as decimal(10,1)) as decimal(10,1)) as idoszaki_SzereltSzazalek_sztornoval,
		ugyiratok.idoszaki_SztornozottDb,
		cast((CAST(ugyiratok.idoszaki_SztornozottDb AS decimal(10,1)) * 100) / CAST((case when ugyiratok.idoszaki_FoszamDb_sztornoval = 0 then 1 else ugyiratok.idoszaki_FoszamDb_sztornoval end) as decimal(10,1)) as decimal(10,1)) as idoszaki_SztornozottSzazalek '
	
SET @sqlcmd = @sqlcmd + '
	from
	(	select
	   EREC_IraIktatoKonyvek.Iktatohely,
	   EREC_IraIktatoKonyvek.Nev,
	   EREC_IraIktatoKonyvek.Ev,
	   count (distinct EREC_UgyUgyiratok.Id) as FoszamDb,
	   count 
	   ( DISTINCT
			case
				when EREC_Ugyugyiratok.Allapot = ''04'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot=''04'') then EREC_UgyUgyiratok.Id
				else NULL
			end
	   ) as IktatottDb,
	   (
		   (count
		   ( DISTINCT
				case
					when EREC_Ugyugyiratok.Allapot = ''04'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot=''04'') then EREC_UgyUgyiratok.Id
					else NULL
				end
		   ) * 100) /
		   (CASE
				WHEN count (EREC_UgyUgyiratok.Id) = 0 THEN 1
				ELSE count (EREC_UgyUgyiratok.Id)
		   END)
	   ) AS IktatottSzazalek,
	   count
	   (
			case
				when EREC_Ugyugyiratok.Allapot = ''06'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot=''06'') then EREC_UgyUgyiratok.Id
				else NULL
			end
	   ) as UgyintezesAlattDb,
	   (
		   (count
		   (
				case
					when EREC_Ugyugyiratok.Allapot = ''06'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot=''06'') then EREC_UgyUgyiratok.Id
					else NULL
				end
		   ) * 100) /
		   (CASE
				WHEN count (DISTINCT EREC_UgyUgyiratok.Id) = 0 THEN 1
				ELSE count (DISTINCT EREC_UgyUgyiratok.Id)
		   END)
	   ) AS UgyintezesAlattSzazalek,
	   count
	   (
			case
				when EREC_Ugyugyiratok.Allapot in (''07'',''57'',''71'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''07'',''57'',''71'')) then EREC_UgyUgyiratok.Id
				else NULL
			end
	   ) as SkontrobanDb,
	   (
		   (count
		   (
				case
					when EREC_Ugyugyiratok.Allapot in (''07'',''57'',''71'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''07'',''57'',''71'')) then EREC_UgyUgyiratok.Id
					else NULL
				end
		   ) * 100) /
		   (CASE
				WHEN count (DISTINCT EREC_UgyUgyiratok.Id) = 0 THEN 1
				ELSE count (DISTINCT EREC_UgyUgyiratok.Id)
		   END)
	   ) AS SkontrobanSzazalek,
	   COUNT
	   (
			case
				when EREC_Ugyugyiratok.Allapot = ''10'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot=''10'') then EREC_UgyUgyiratok.Id
				else NULL
			end
	   ) as IrattarbanDb,
	   (
		   (count
		   (
				case
					when EREC_Ugyugyiratok.Allapot = ''10'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot=''10'') then EREC_UgyUgyiratok.Id
					else NULL
				end
		   ) * 100) /
		   (CASE
				WHEN count (DISTINCT EREC_UgyUgyiratok.Id) = 0 THEN 1
				ELSE count (DISTINCT EREC_UgyUgyiratok.Id)
		   END)
	   ) AS IrattarbanSzazalek,
	   '
SET @sqlcmd = @sqlcmd +
	  'count
	   (
			case
				when EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''52'',''55'',''56'',''11'',''13'',''99'',''09'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''52'',''55'',''56'',''11'',''13'',''99'',''09'')) then EREC_UgyUgyiratok.Id
				else NULL
			end
	   ) as UgyintezesAlatt2Db,
	   count
	   (
			case
				when EREC_Ugyugyiratok.Allapot = ''90'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''90'') then EREC_UgyUgyiratok.Id
				else NULL
			end
	   ) as SztornozottDb,
	   count
	   (
			case
				when EREC_Ugyugyiratok.Allapot = ''60'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''60'') then EREC_UgyUgyiratok.Id
				else NULL
			end
	   ) as SzereltDb,
	   count
	   (
			case
				when EREC_Ugyugyiratok.Allapot = ''32'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''32'') then EREC_UgyUgyiratok.Id
				else NULL
			end
	   ) as SelejtezettDb,
	   count
	   (
			case
				when EREC_Ugyugyiratok.Allapot = ''33'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''33'') then EREC_UgyUgyiratok.Id
				else NULL
			end
	   ) as LeveltarbaAdottDb,
	   count
	   (
			case
				when EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99'')) then EREC_UgyUgyiratok.Id
				else NULL
			end
	   ) as idoszaki_FoszamDb,
	   count
	   (
			case
				when EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99'')) then EREC_UgyUgyiratok.Id
				else NULL
			end
	   ) as idoszaki_FoszamDb_sztornoval,
	   count
	   (
			case
				when EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''13'',''55'',''56'',''52'',''70'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''13'',''55'',''56'',''52'',''70'',''99'')) then EREC_UgyUgyiratok.Id
				else NULL
			end
	   ) as idoszaki_UgyintezesAlattDb,'
SET @sqlcmd = @sqlcmd + 
	  'count
	   (
			case
				when EREC_Ugyugyiratok.Allapot in (''09'',''10'',''11'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''09'',''10'',''11'')) then EREC_UgyUgyiratok.Id
				else NULL
			end
	   ) as idoszaki_IrattarbanDb,
	   count
	   (
			case
				when EREC_Ugyugyiratok.Allapot in (''30'',''31'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''30'',''31'')) then EREC_UgyUgyiratok.Id
				else NULL
			end
	   ) as EgyebDb,
	   count
	   (
			case
				when EREC_Ugyugyiratok.Allapot = ''90'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''90'') then EREC_UgyUgyiratok.Id
				else NULL
			end
	   ) as idoszaki_SztornozottDb '

SET @sqlcmd = @sqlcmd +
   '
from 
     EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek 
		left join EREC_UgyUgyiratok
			on EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id
			and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''' '
     
set @sqlcmd = @sqlcmd + N'
   Where EREC_IraIktatoKonyvek.IktatoErkezteto=''I'' and EREC_IraIktatoKonyvek.Ev between YEAR(''' + @KezdDate + ''') and YEAR(''' + @VegeDate + ''')
	and EREC_IraIktatoKonyvek.Org=''' + CAST(@Org as Nvarchar(40)) + '''
'
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
    -- if @Jogosultak = 1 AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
	IF @Jogosultak = 1 AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
	BEGIN
		DECLARE @OjbTip_Id uniqueidentifier;
		select @OjbTip_Id = Id from KRT_Objtipusok where Kod = 'EREC_IraIktatoKonyvek';
		set @sqlcmd = @sqlcmd + N' AND EREC_IraIktatoKonyvek.Id IN 
						(select EREC_IraIktatoKonyvek.Id 
							from EREC_IraIktatoKonyvek
								INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_IraIktatoKonyvek.Id
								INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
						UNION ALL
						SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''', ''EREC_IraIktatoKonyvek'')
							 )';
	END

	SET @sqlcmd = @sqlcmd + ' GROUP BY EREC_IraIktatoKonyvek.Iktatohely,EREC_IraIktatoKonyvek.Nev,EREC_IraIktatoKonyvek.Ev '
	SET @sqlcmd = @sqlcmd + ' ) as ugyiratok '
	SET @sqlcmd = @sqlcmd + '
		left join
		(
			SELECT Iktatohely,Nev,Ev,SUM(AlszamDb) AS AlszamDb, SUM(idoszaki_AlszamDb) AS idoszaki_AlszamDb, SUM(idoszaki_AlszamDb_sztornoval) AS idoszaki_AlszamDb_sztornoval
				FROM
				(
					SELECT Iktatohely,Nev,Ev,COUNT(Id) AS AlszamDb,0 AS idoszaki_AlszamDb,0 AS idoszaki_AlszamDb_sztornoval FROM [#iratTemp] AS EREC_IraIratok GROUP BY Iktatohely,Nev,Ev
					UNION ALL
					SELECT Iktatohely,Nev,Ev,0,COUNT(Id),0 FROM [#iratTemp] AS EREC_IraIratok
						WHERE Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99'') or (Allapot=''50'' and TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99'')) GROUP BY Iktatohely,Nev,Ev
					UNION ALL
					SELECT Iktatohely,Nev,Ev,0,0,COUNT(Id) FROM [#iratTemp] AS EREC_IraIratok
						WHERE Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99'') or (Allapot=''50'' and TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99'')) GROUP BY Iktatohely,Nev,Ev
				) temp GROUP BY Iktatohely,Nev,Ev '
	SET @sqlcmd = @sqlcmd + ' ) as iratok on ugyiratok.Iktatohely = iratok.Iktatohely
					and ugyiratok.Nev = iratok.Nev
					and ugyiratok.Ev = iratok.Ev '
					
	SET @sqlcmd = @sqlcmd + @OrderBy
    SET @sqlcmd = @sqlcmd + ' drop table #iratTemp'

  exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
