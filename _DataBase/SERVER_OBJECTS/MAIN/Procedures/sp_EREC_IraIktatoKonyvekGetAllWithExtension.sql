IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIktatoKonyvekGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraIktatoKonyvekGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIktatoKonyvekGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraIktatoKonyvekGetAllWithExtension] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IraIktatoKonyvekGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraIktatoKonyvek.LetrehozasIdo',
  @FelhasznaloSzervezet_Id UNIQUEIDENTIFIER,
  @TopRow nvarchar(5) = '',
  @ExecutorUserId          uniqueidentifier,
  @Jogosultak  char(1) = '0'

as

begin

BEGIN TRY

   set nocount on

   DECLARE @Org uniqueidentifier
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
   if (@Org is null)
   begin
      RAISERROR('[50202]',16,1)
   end

   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
      EREC_IraIktatoKonyvek.Id,
      EREC_IraIktatoKonyvek.Org,
      EREC_IraIktatoKonyvek.Ev,
   EREC_IraIktatoKonyvek.Azonosito,
      EREC_IraIktatoKonyvek.Nev,
      EREC_IraIktatoKonyvek.MegkulJelzes,
      EREC_IraIktatoKonyvek.Iktatohely,
      EREC_IraIktatoKonyvek.UtolsoFoszam,
      EREC_IraIktatoKonyvek.UtolsoSorszam,
      EREC_IraIktatoKonyvek.DefaultIrattariTetelszam,
      EREC_IraIktatoKonyvek.Csoport_Id_Olvaso,
      EREC_IraIktatoKonyvek.Csoport_Id_Tulaj,
      EREC_IraIktatoKonyvek.IktSzamOsztas,
      KRT_KodTarak.Nev as IKT_SZAM_OSZTAS,
      EREC_IraIktatoKonyvek.Titkos,
      EREC_IraIktatoKonyvek.IktatoErkezteto,
      --EREC_IraIktatoKonyvek.LezarasDatuma,
	  CONVERT(nvarchar(10), EREC_IraIktatoKonyvek.LezarasDatuma, 102) as LezarasDatuma,
      EREC_IraIktatoKonyvek.PostakonyvVevokod,
      EREC_IraIktatoKonyvek.PostakonyvMegallapodasAzon,
      EREC_IraIktatoKonyvek.Ver,
      EREC_IraIktatoKonyvek.Note,
      EREC_IraIktatoKonyvek.Stat_id,
   CONVERT(nvarchar(10), EREC_IraIktatoKonyvek.ErvKezd, 102) as ErvKezd,
   CONVERT(nvarchar(10), EREC_IraIktatoKonyvek.ErvVege, 102) as ErvVege,
      EREC_IraIktatoKonyvek.Letrehozo_id,
      EREC_IraIktatoKonyvek.LetrehozasIdo,
      EREC_IraIktatoKonyvek.Modosito_id,
      EREC_IraIktatoKonyvek.ModositasIdo,
      EREC_IraIktatoKonyvek.Zarolo_id,
      EREC_IraIktatoKonyvek.ZarolasIdo,
      EREC_IraIktatoKonyvek.Tranz_id,
      EREC_IraIktatoKonyvek.UIAccessLog_id,
      EREC_IraIktatoKonyvek.Statusz,
	   -- CR3355
	   EREC_IraIktatoKonyvek.Terjedelem,
	   --EREC_IraIktatoKonyvek.SelejtezesDatuma,
	   CONVERT(nvarchar(10), EREC_IraIktatoKonyvek.SelejtezesDatuma, 102) as SelejtezesDatuma,
	   EREC_IraIktatoKonyvek.KezelesTipusa,
	   dbo.fn_MergeIrattariTetelSzam(EREC_AgazatiJelek.Kod,EREC_IraIrattariTetelek.IrattariTetelszam,EREC_IraIrattariTetelek.MegorzesiIdo,EREC_IraIrattariTetelek.IrattariJel,''' + CAST(@Org as Nvarchar(40)) + ''')
      as Merge_IrattariTetelszam  
   from 
     EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek   
	  left join EREC_IraIrattariTetelek as EREC_IraIrattariTetelek
            ON EREC_IraIktatoKonyvek.DefaultIrattariTetelszam = EREC_IraIrattariTetelek.Id
	  left join EREC_AgazatiJelek as EREC_Agazatijelek
            ON EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_Agazatijelek.Id     
   left join KRT_KodCsoportok on KRT_KodCsoportok.Kod=''IKT_SZAM_OSZTAS''
   left join KRT_KodTarak on KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and EREC_IraIktatoKonyvek.IktSzamOsztas = KRT_KodTarak.Kod
   and KRT_KodTarak.Org=''' + CAST(@Org as Nvarchar(40)) + '''
'

set @sqlcmd = @sqlcmd + N'
   Where EREC_IraIktatoKonyvek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
   if @Where is not null and @Where!=''
   begin 
      SET @sqlcmd = @sqlcmd + ' and ' + @Where
   end
     
    --if @Jogosultak = 1 AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
   IF @Jogosultak = '1' AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
   BEGIN
      DECLARE @OjbTip_Id uniqueidentifier;
      select @OjbTip_Id = Id from KRT_Objtipusok where Kod = 'EREC_IraIktatoKonyvek';
      set @sqlcmd = @sqlcmd + N' AND EREC_IraIktatoKonyvek.Id IN 
                  (select EREC_IraIktatoKonyvek.Id 
                     from EREC_IraIktatoKonyvek
                        INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_IraIktatoKonyvek.Id and EREC_IraIktatoKonyvek.Org=''' + CAST(@Org as Nvarchar(40)) + '''
                        INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
                  UNION ALL
                  SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''', ''EREC_IraIktatoKonyvek'')
                      )';
   END

   
   SET @sqlcmd = @sqlcmd + @OrderBy

--print @sqlcmd;

   exec (@sqlcmd);

END TRY
BEGIN CATCH
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
