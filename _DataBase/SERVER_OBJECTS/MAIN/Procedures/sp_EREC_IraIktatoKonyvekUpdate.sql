IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIktatoKonyvekUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraIktatoKonyvekUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIktatoKonyvekUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraIktatoKonyvekUpdate] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IraIktatoKonyvekUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Org     uniqueidentifier  = null,         
             @Ev     int  = null,         
             @Azonosito     Nvarchar(20)  = null,         
             @DefaultIrattariTetelszam     uniqueidentifier  = null,         
             @Nev     Nvarchar(100)  = null,         
             @MegkulJelzes     Nvarchar(20)  = null,         
             @Iktatohely     Nvarchar(100)  = null,         
             @KozpontiIktatasJelzo     char(1)  = null,         
             @UtolsoFoszam     int  = null,         
             @UtolsoSorszam     int  = null,         
             @Csoport_Id_Olvaso     uniqueidentifier  = null,         
             @Csoport_Id_Tulaj     uniqueidentifier  = null,         
             @IktSzamOsztas     nvarchar(64)  = null,         
             @FormatumKod     char(1)  = null,         
             @Titkos     char(1)  = null,         
             @IktatoErkezteto     char(1)  = null,         
             @LezarasDatuma     datetime  = null,         
             @PostakonyvVevokod     Nvarchar(100)  = null,         
             @PostakonyvMegallapodasAzon     Nvarchar(100)  = null,         
             @EFeladoJegyzekUgyfelAdatok     Nvarchar(4000)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
             @Statusz     char(1)  = null,         
			-- CR3355
			@Terjedelem nvarchar(20) = null,
			@SelejtezesDatuma datetime = null,
			@KezelesTipusa char(1) = 'E',   
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Org     uniqueidentifier         
     DECLARE @Act_Ev     int         
     DECLARE @Act_Azonosito     Nvarchar(20)         
     DECLARE @Act_DefaultIrattariTetelszam     uniqueidentifier         
     DECLARE @Act_Nev     Nvarchar(100)         
     DECLARE @Act_MegkulJelzes     Nvarchar(20)         
     DECLARE @Act_Iktatohely     Nvarchar(100)         
     DECLARE @Act_KozpontiIktatasJelzo     char(1)         
     DECLARE @Act_UtolsoFoszam     int         
     DECLARE @Act_UtolsoSorszam     int         
     DECLARE @Act_Csoport_Id_Olvaso     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Tulaj     uniqueidentifier         
     DECLARE @Act_IktSzamOsztas     nvarchar(64)         
     DECLARE @Act_FormatumKod     char(1)         
     DECLARE @Act_Titkos     char(1)         
     DECLARE @Act_IktatoErkezteto     char(1)         
     DECLARE @Act_LezarasDatuma     datetime         
     DECLARE @Act_PostakonyvVevokod     Nvarchar(100)         
     DECLARE @Act_PostakonyvMegallapodasAzon     Nvarchar(100)         
     DECLARE @Act_EFeladoJegyzekUgyfelAdatok     Nvarchar(4000)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier         
     DECLARE @Act_Statusz     char(1)           
  -- CR3355
	 DECLARE @Act_Terjedelem nvarchar(20)
	 DECLARE @Act_SelejtezesDatuma datetime
	 DECLARE @Act_KezelesTipusa char(1)   

set nocount on

select 
     @Act_Org = Org,
     @Act_Ev = Ev,
     @Act_Azonosito = Azonosito,
     @Act_DefaultIrattariTetelszam = DefaultIrattariTetelszam,
     @Act_Nev = Nev,
     @Act_MegkulJelzes = MegkulJelzes,
     @Act_Iktatohely = Iktatohely,
     @Act_KozpontiIktatasJelzo = KozpontiIktatasJelzo,
     @Act_UtolsoFoszam = UtolsoFoszam,
     @Act_UtolsoSorszam = UtolsoSorszam,
     @Act_Csoport_Id_Olvaso = Csoport_Id_Olvaso,
     @Act_Csoport_Id_Tulaj = Csoport_Id_Tulaj,
     @Act_IktSzamOsztas = IktSzamOsztas,
     @Act_FormatumKod = FormatumKod,
     @Act_Titkos = Titkos,
     @Act_IktatoErkezteto = IktatoErkezteto,
     @Act_LezarasDatuma = LezarasDatuma,
     @Act_PostakonyvVevokod = PostakonyvVevokod,
     @Act_PostakonyvMegallapodasAzon = PostakonyvMegallapodasAzon,
     @Act_EFeladoJegyzekUgyfelAdatok = EFeladoJegyzekUgyfelAdatok,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id,
     @Act_Statusz = Statusz,
	 -- CR3355
	 @Act_Terjedelem = Terjedelem,
	 @Act_SelejtezesDatuma = SelejtezesDatuma,
	 @Act_KezelesTipusa = KezelesTipusa
from EREC_IraIktatoKonyvek
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Org')=1
         SET @Act_Org = @Org
   IF @UpdatedColumns.exist('/root/Ev')=1
         SET @Act_Ev = @Ev
   IF @UpdatedColumns.exist('/root/Azonosito')=1
         SET @Act_Azonosito = @Azonosito
   IF @UpdatedColumns.exist('/root/DefaultIrattariTetelszam')=1
         SET @Act_DefaultIrattariTetelszam = @DefaultIrattariTetelszam
   IF @UpdatedColumns.exist('/root/Nev')=1
         SET @Act_Nev = @Nev
   IF @UpdatedColumns.exist('/root/MegkulJelzes')=1
         SET @Act_MegkulJelzes = @MegkulJelzes
   IF @UpdatedColumns.exist('/root/Iktatohely')=1
         SET @Act_Iktatohely = @Iktatohely
   IF @UpdatedColumns.exist('/root/KozpontiIktatasJelzo')=1
         SET @Act_KozpontiIktatasJelzo = @KozpontiIktatasJelzo
   IF @UpdatedColumns.exist('/root/UtolsoFoszam')=1
         SET @Act_UtolsoFoszam = @UtolsoFoszam
   IF @UpdatedColumns.exist('/root/UtolsoSorszam')=1
         SET @Act_UtolsoSorszam = @UtolsoSorszam
   IF @UpdatedColumns.exist('/root/Csoport_Id_Olvaso')=1
         SET @Act_Csoport_Id_Olvaso = @Csoport_Id_Olvaso
   IF @UpdatedColumns.exist('/root/Csoport_Id_Tulaj')=1
         SET @Act_Csoport_Id_Tulaj = @Csoport_Id_Tulaj
   IF @UpdatedColumns.exist('/root/IktSzamOsztas')=1
         SET @Act_IktSzamOsztas = @IktSzamOsztas
   IF @UpdatedColumns.exist('/root/FormatumKod')=1
         SET @Act_FormatumKod = @FormatumKod
   IF @UpdatedColumns.exist('/root/Titkos')=1
         SET @Act_Titkos = @Titkos
   IF @UpdatedColumns.exist('/root/IktatoErkezteto')=1
         SET @Act_IktatoErkezteto = @IktatoErkezteto
   IF @UpdatedColumns.exist('/root/LezarasDatuma')=1
         SET @Act_LezarasDatuma = @LezarasDatuma
   IF @UpdatedColumns.exist('/root/PostakonyvVevokod')=1
         SET @Act_PostakonyvVevokod = @PostakonyvVevokod
   IF @UpdatedColumns.exist('/root/PostakonyvMegallapodasAzon')=1
         SET @Act_PostakonyvMegallapodasAzon = @PostakonyvMegallapodasAzon
   IF @UpdatedColumns.exist('/root/EFeladoJegyzekUgyfelAdatok')=1
         SET @Act_EFeladoJegyzekUgyfelAdatok = @EFeladoJegyzekUgyfelAdatok
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id
   IF @UpdatedColumns.exist('/root/Statusz')=1
         SET @Act_Statusz = @Statusz 
   -- CR3355  
    IF @UpdatedColumns.exist('/root/Terjedelem')=1
        SET @Act_Terjedelem = @Terjedelem   
	IF @UpdatedColumns.exist('/root/SelejtezesDatuma')=1
		SET @Act_SelejtezesDatuma = @SelejtezesDatuma   
	IF @UpdatedColumns.exist('/root/KezelesTipusa')=1
		SET @Act_KezelesTipusa = @KezelesTipusa
		   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_IraIktatoKonyvek
SET
     Org = @Act_Org,
     Ev = @Act_Ev,
     Azonosito = @Act_Azonosito,
     DefaultIrattariTetelszam = @Act_DefaultIrattariTetelszam,
     Nev = @Act_Nev,
     MegkulJelzes = @Act_MegkulJelzes,
     Iktatohely = @Act_Iktatohely,
     KozpontiIktatasJelzo = @Act_KozpontiIktatasJelzo,
     UtolsoFoszam = @Act_UtolsoFoszam,
     UtolsoSorszam = @Act_UtolsoSorszam,
     Csoport_Id_Olvaso = @Act_Csoport_Id_Olvaso,
     Csoport_Id_Tulaj = @Act_Csoport_Id_Tulaj,
     IktSzamOsztas = @Act_IktSzamOsztas,
     FormatumKod = @Act_FormatumKod,
     Titkos = @Act_Titkos,
     IktatoErkezteto = @Act_IktatoErkezteto,
     LezarasDatuma = @Act_LezarasDatuma,
     PostakonyvVevokod = @Act_PostakonyvVevokod,
     PostakonyvMegallapodasAzon = @Act_PostakonyvMegallapodasAzon,
     EFeladoJegyzekUgyfelAdatok = @Act_EFeladoJegyzekUgyfelAdatok,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id,
     Statusz = @Act_Statusz,
	 -- CR3355
	 Terjedelem = @Act_Terjedelem,
	 SelejtezesDatuma = @Act_SelejtezesDatuma,
	 KezelesTipusa = @Act_KezelesTipusa
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_IraIktatoKonyvek',@Id
					,'EREC_IraIktatoKonyvekHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
