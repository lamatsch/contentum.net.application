IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIktatoKonyvek_LezarasMasolasTomeges]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraIktatoKonyvek_LezarasMasolasTomeges]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIktatoKonyvek_LezarasMasolasTomeges]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraIktatoKonyvek_LezarasMasolasTomeges] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraIktatoKonyvek_LezarasMasolasTomeges]
   @Ids           nvarchar(MAX),
   @Ids_Copy            nvarchar(MAX),
   @ExecutorUserId      uniqueidentifier,
   @IktatoErkezteto  char(1) = null
as

BEGIN TRY
   set nocount on
   /* dinamikus history log összeállításhoz */
   declare @row_ids varchar(MAX)

   /* hibakódok IktatoErkezteto szurés szerint */
   declare @errorCode_TomegesLezaras_NemMindenTetel varchar(10)
   declare @errorCode_TomegesMasolas_NemMindenTetel varchar(10)
   if (@IktatoErkezteto = 'I')
   begin
      set @errorCode_TomegesLezaras_NemMindenTetel = '[52947]'
      set @errorCode_TomegesMasolas_NemMindenTetel = '[52949]'
   end
   else if (@IktatoErkezteto = 'E')
   begin
      set @errorCode_TomegesLezaras_NemMindenTetel = '[52927]'
      set @errorCode_TomegesMasolas_NemMindenTetel = '[52929]'
   end
   else
   begin
      set @errorCode_TomegesLezaras_NemMindenTetel = '[52907]'
      set @errorCode_TomegesMasolas_NemMindenTetel = '[52909]'
   end

   declare @execTime datetime;
   declare @tempTable table(Id uniqueidentifier);
   declare @currentYear int;

   set @execTime = getdate();
   set @currentYear = datepart(yyyy, @execTime);

   -- temp tábla töltése az id-kkel
   declare @it int;
   declare @curId nvarchar(36);

   set @it = 0;
   while (@it < ((len(@Ids)+1) / 39))
   BEGIN
      set @curId = SUBSTRING(@Ids,@it*39+2,37);
      insert into @tempTable(Id) values(@curId);
      set @it = @it + 1;
   END


   declare @rowNumberTotal int;

   select @rowNumberTotal = count(Id) from @tempTable;

   /****** Lezárás *****/
   -- EREC_IraIktatoKonyvek UPDATE
   SELECT EREC_IraIktatoKonyvek.Id INTO #lezarasIds
      FROM EREC_IraIktatoKonyvek
      where Id in (select Id from @tempTable)
         and LezarasDatuma is null
         and Ev <= @currentYear
         and (ModositasIdo < @execTime or ModositasIdo is null)
         and (Zarolo_id is NULL or Zarolo_Id = @ExecutorUserId)
         and getdate() between ErvKezd and ErvVege
         and (@IktatoErkezteto is null or (IktatoErkezteto = @IktatoErkezteto))

      -- Ellenorzés
      if (@rowNumberTotal != (SELECT COUNT(*) FROM #lezarasIds) )
      BEGIN
         SELECT Id FROM @tempTable
         EXCEPT
         select Id from #lezarasIds
         DROP TABLE #lezarasIds;
         --Hiba az iktatókönyvek tömeges lezárása során:
         -- nem hajtható végre minden tételen a muvelet!
         RAISERROR(@errorCode_TomegesLezaras_NemMindenTetel,16,1);
      END
      
      -- iktatókönyvek lezárása
      UPDATE EREC_IraIktatoKonyvek 
         set LezarasDatuma = @execTime,
            Ver = Ver + 1,
            Modosito_id = @ExecutorUserId,
            ModositasIdo = @execTime,
            Statusz = '0' --bernat.laszlo added
         where Id in (select Id from #lezarasIds);
            
      /*** EREC_IraIktatoKonyvek history log ***/
--    declare IraIktatoKonyvekLezarasCursor cursor local fast_forward for 
--       select Id from #lezarasIds

--    declare @IktatoKonyvId uniqueidentifier;
--
--    open IraIktatoKonyvekLezarasCursor;
--    fetch next from IraIktatoKonyvekLezarasCursor into @IktatoKonyvId;
--    while @@FETCH_STATUS = 0
--    BEGIN
--       exec sp_LogRecordToHistory 'EREC_IraIktatoKonyvek',@IktatoKonyvId
--                ,'EREC_IraIktatoKonyvekHistory',1,@ExecutorUserId,@execTime;
--       fetch next from IraIktatoKonyvekLezarasCursor into @IktatoKonyvId;
--    END
--    
--    CLOSE IraIktatoKonyvekLezarasCursor;
--    DEALLOCATE IraIktatoKonyvekLezarasCursor;

      set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from #lezarasIds FOR XML PATH('')),1, 2, '')
      if @row_ids is not null
         set @row_ids = @row_ids + '''';

      exec sp_LogRecordsToHistory_Tomeges
       @TableName = 'EREC_IraIktatoKonyvek'
      ,@Row_Ids = @row_ids
      ,@Muvelet = 0
      ,@Vegrehajto_Id = @ExecutorUserId
      ,@VegrehajtasIdo = @execTime

      DROP TABLE #lezarasIds;

/* Következo évre létrehozandók (csak lezárt lehet)*/
   delete from @tempTable;

   set @it = 0;
   while (@it < ((len(@Ids_Copy)+1) / 39))
   BEGIN
      set @curId = SUBSTRING(@Ids_Copy,@it*39+2,37);
      insert into @tempTable(Id) values(@curId);
      set @it = @it + 1;
   END

   -- ellenorzés
   select @rowNumberTotal = count(Id) from @tempTable;

   /****** Következo évre létrehozás *****/
   -- EREC_IraIktatoKonyvek INSERT
   SELECT EREC_IraIktatoKonyvek.Id INTO #masolasIds
      FROM EREC_IraIktatoKonyvek
      where EREC_IraIktatoKonyvek.Id in (select Id from @tempTable)
         and EREC_IraIktatoKonyvek.LezarasDatuma is not null
         and EREC_IraIktatoKonyvek.Ev between @currentYear - 1 and @currentYear
         and getdate() between EREC_IraIktatoKonyvek.ErvKezd and EREC_IraIktatoKonyvek.ErvVege
         and not exists (select iik.Id from EREC_IraIktatoKonyvek iik
            where iik.Org = EREC_IraIktatoKonyvek.Org
               and iik.Nev = EREC_IraIktatoKonyvek.Nev
               and getdate() between iik.ErvKezd and iik.ErvVege
               and iik.Ev = iik.Ev + 1)
         and (@IktatoErkezteto is null or (EREC_IraIktatoKonyvek.IktatoErkezteto = @IktatoErkezteto))

      -- Ellenorzés
      if (@rowNumberTotal != (SELECT COUNT(*) FROM #masolasIds) )
      BEGIN
         SELECT Id FROM @tempTable
         EXCEPT
         select Id from #masolasIds
         DROP TABLE #masolasIds;
         --Hiba az iktatókönyvek tömeges másolása során:
         -- nem hajtható végre minden tételen a muvelet!
         RAISERROR(@errorCode_TomegesMasolas_NemMindenTetel,16,1);
      END
      
      -- iktatókönyvek létrehozása a lezártak mintájára, de a következo évre
      declare @IktatoKonyv_Id_Original uniqueidentifier
      declare @IktatoKonyv_Id_Copy uniqueidentifier
      declare @insertedRowId table (Id uniqueidentifier)

      declare IraIktatoKonyvekMasolasCursor cursor local fast_forward for 
         select Id from #masolasIds

      open IraIktatoKonyvekMasolasCursor;
      fetch next from IraIktatoKonyvekMasolasCursor into @IktatoKonyv_Id_Original;
      while @@FETCH_STATUS = 0
      BEGIN
         exec sp_EREC_IraIktatoKonyvek_MasolasKovetkezoEvre
             @Id = @IktatoKonyv_Id_Original, @ExecutorUserId = @ExecutorUserId, @IktatoErkezteto = @IktatoErkezteto, @ResultUid = @IktatoKonyv_Id_Copy output

         fetch next from IraIktatoKonyvekMasolasCursor into @IktatoKonyv_Id_Original;
      END
      
      CLOSE IraIktatoKonyvekMasolasCursor;
      DEALLOCATE IraIktatoKonyvekMasolasCursor;

      DROP TABLE #masolasIds;

END TRY
BEGIN CATCH

   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
