IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIratokDokumentumokGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraIratokDokumentumokGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIratokDokumentumokGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraIratokDokumentumokGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraIratokDokumentumokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraIratokDokumentumok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_IraIratokDokumentumok.Id,
	   EREC_IraIratokDokumentumok.Dokumentum_Id,
	   EREC_IraIratokDokumentumok.IraIrat_Id,
	   EREC_IraIratokDokumentumok.Leiras,
	   EREC_IraIratokDokumentumok.Lapszam,
	   EREC_IraIratokDokumentumok.BarCode,
	   EREC_IraIratokDokumentumok.Forras,
	   EREC_IraIratokDokumentumok.Formatum,
	   EREC_IraIratokDokumentumok.Vonalkodozas,
	   EREC_IraIratokDokumentumok.DokumentumSzerep,
	   EREC_IraIratokDokumentumok.Ver,
	   EREC_IraIratokDokumentumok.Note,
	   EREC_IraIratokDokumentumok.Stat_id,
	   EREC_IraIratokDokumentumok.ErvKezd,
	   EREC_IraIratokDokumentumok.ErvVege,
	   EREC_IraIratokDokumentumok.Letrehozo_id,
	   EREC_IraIratokDokumentumok.LetrehozasIdo,
	   EREC_IraIratokDokumentumok.Modosito_id,
	   EREC_IraIratokDokumentumok.ModositasIdo,
	   EREC_IraIratokDokumentumok.Zarolo_id,
	   EREC_IraIratokDokumentumok.ZarolasIdo,
	   EREC_IraIratokDokumentumok.Tranz_id,
	   EREC_IraIratokDokumentumok.UIAccessLog_id  
   from 
     EREC_IraIratokDokumentumok as EREC_IraIratokDokumentumok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
