IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIratokDokumentumokGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraIratokDokumentumokGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIratokDokumentumokGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraIratokDokumentumokGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraIratokDokumentumokGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraIratokDokumentumok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

     
-- A PKI_INTEGRACIO rendszerparameter erteketol fuggoen
-- eltero kodcsoport szerint dolgozunk az elektronikus
-- alairas kapcsan
DECLARE @kcsDokumentumAlairas nvarchar(64)

DECLARE @paramPKI_Integracio nvarchar(400)

SET @paramPKI_Integracio = (
	SELECT Ertek FROM KRT_Parameterek
	WHERE Nev ='PKI_INTEGRACIO'
	AND Org=@Org
)

IF @paramPKI_Integracio = 'Igen'
BEGIN
	SET @kcsDokumentumAlairas = 'DOKUMENTUM_ALAIRAS_PKI'
END
ELSE
BEGIN
	SET @kcsDokumentumAlairas = 'DOKUMENTUM_ALAIRAS_MANUALIS'
END     
               

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_IraIratokDokumentumok.Id,
	   EREC_IraIratokDokumentumok.Dokumentum_Id,
	   EREC_IraIratokDokumentumok.IraIrat_Id,
KRT_Dokumentumok.FajlNev as FajlNev,
KRT_Dokumentumok.External_Link,	   
KRT_Dokumentumok.VerzioJel,
KRT_Dokumentumok.Megnyithato,
KRT_Dokumentumok.Olvashato,
KRT_Dokumentumok.ElektronikusAlairas,
dbo.fn_KodtarErtekNeve(''' + @kcsDokumentumAlairas +
	''', KRT_Dokumentumok.ElektronikusAlairas,''' + CAST(@Org as NVarChar(40)) + ''') as ElektronikusAlairasNev,
KRT_Dokumentumok.AlairasFelulvizsgalat,
KRT_Dokumentumok.Titkositas,
KRT_Dokumentumok.Tipus,
KRT_Dokumentumok.Formatum,
KRT_Dokumentumok.SablonAzonosito,
EREC_IraIratok.Azonosito as IktatoSzam_Merge,
	   EREC_IraIratokDokumentumok.Leiras,
	   EREC_IraIratokDokumentumok.Lapszam,
	   EREC_IraIratokDokumentumok.Ver,
	   EREC_IraIratokDokumentumok.Note,
	   EREC_IraIratokDokumentumok.Stat_id,
	   EREC_IraIratokDokumentumok.ErvKezd,
CONVERT(nvarchar(10), EREC_IraIratokDokumentumok.ErvKezd, 102) as ErvKezd_f,
	   EREC_IraIratokDokumentumok.ErvVege,
CONVERT(nvarchar(10), EREC_IraIratokDokumentumok.ErvVege, 102) as ErvVege_f,
	   EREC_IraIratokDokumentumok.Letrehozo_id,
	   EREC_IraIratokDokumentumok.LetrehozasIdo,
	   EREC_IraIratokDokumentumok.Modosito_id,
	   EREC_IraIratokDokumentumok.ModositasIdo,
	   EREC_IraIratokDokumentumok.Zarolo_id,
	   EREC_IraIratokDokumentumok.ZarolasIdo,
	   EREC_IraIratokDokumentumok.Tranz_id,
	   EREC_IraIratokDokumentumok.UIAccessLog_id  
   from 
     EREC_IraIratokDokumentumok as EREC_IraIratokDokumentumok      
left join KRT_Dokumentumok as KRT_Dokumentumok on EREC_IraIratokDokumentumok.Dokumentum_Id = KRT_Dokumentumok.Id      
left join EREC_IraIratok on EREC_IraIratokDokumentumok.IraIrat_Id = EREC_IraIratok.Id
LEFT JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok
		ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
	LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok 
			ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id  
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek 
			ON EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
