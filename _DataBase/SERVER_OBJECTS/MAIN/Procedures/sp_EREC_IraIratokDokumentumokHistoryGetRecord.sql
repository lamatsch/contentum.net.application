IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIratokDokumentumokHistoryGetRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraIratokDokumentumokHistoryGetRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIratokDokumentumokHistoryGetRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraIratokDokumentumokHistoryGetRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraIratokDokumentumokHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokDokumentumokHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_IraIratokDokumentumokHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokDokumentumokHistory Old
         inner join EREC_IraIratokDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Dokumentum_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_DokumentumokAzonosito(Old.Dokumentum_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_DokumentumokAzonosito(New.Dokumentum_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokDokumentumokHistory Old
         inner join EREC_IraIratokDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Dokumentum_Id != New.Dokumentum_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Dokumentumok FTOld on FTOld.Id = Old.Dokumentum_Id and FTOld.Ver = Old.Ver
         left join KRT_Dokumentumok FTNew on FTNew.Id = New.Dokumentum_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIrat_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_IraIratokAzonosito(Old.IraIrat_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_IraIratokAzonosito(New.IraIrat_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokDokumentumokHistory Old
         inner join EREC_IraIratokDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IraIrat_Id != New.IraIrat_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_IraIratok FTOld on FTOld.Id = Old.IraIrat_Id and FTOld.Ver = Old.Ver
         left join EREC_IraIratok FTNew on FTNew.Id = New.IraIrat_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Leiras' as ColumnName,               
               cast(Old.Leiras as nvarchar(99)) as OldValue,
               cast(New.Leiras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokDokumentumokHistory Old
         inner join EREC_IraIratokDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Leiras != New.Leiras 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Lapszam' as ColumnName,               
               cast(Old.Lapszam as nvarchar(99)) as OldValue,
               cast(New.Lapszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokDokumentumokHistory Old
         inner join EREC_IraIratokDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Lapszam != New.Lapszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BarCode' as ColumnName,               
               cast(Old.BarCode as nvarchar(99)) as OldValue,
               cast(New.BarCode as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokDokumentumokHistory Old
         inner join EREC_IraIratokDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BarCode != New.BarCode 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Forras' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokDokumentumokHistory Old
         inner join EREC_IraIratokDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Forras != New.Forras 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'DOKUMENTUM_FORRAS'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Forras
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Forras      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Formatum' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokDokumentumokHistory Old
         inner join EREC_IraIratokDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Formatum != New.Formatum 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'DOKUMENTUM_FORMATUM'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Formatum
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Formatum      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Vonalkodozas' as ColumnName,               
               cast(Old.Vonalkodozas as nvarchar(99)) as OldValue,
               cast(New.Vonalkodozas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokDokumentumokHistory Old
         inner join EREC_IraIratokDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Vonalkodozas != New.Vonalkodozas 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DokumentumSzerep' as ColumnName,               
               cast(Old.DokumentumSzerep as nvarchar(99)) as OldValue,
               cast(New.DokumentumSzerep as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokDokumentumokHistory Old
         inner join EREC_IraIratokDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.DokumentumSzerep != New.DokumentumSzerep 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
end


GO
