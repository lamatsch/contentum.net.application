
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_IraIratokGetAll')
            and   type = 'P')
   drop procedure sp_EREC_IraIratokGetAll
go

create procedure sp_EREC_IraIratokGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraIratok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_IraIratok.Id,
	   EREC_IraIratok.PostazasIranya,
	   EREC_IraIratok.Alszam,
	   EREC_IraIratok.Sorszam,
	   EREC_IraIratok.UtolsoSorszam,
	   EREC_IraIratok.UgyUgyIratDarab_Id,
	   EREC_IraIratok.Kategoria,
	   EREC_IraIratok.HivatkozasiSzam,
	   EREC_IraIratok.IktatasDatuma,
	   EREC_IraIratok.ExpedialasDatuma,
	   EREC_IraIratok.ExpedialasModja,
	   EREC_IraIratok.FelhasznaloCsoport_Id_Expedial,
	   EREC_IraIratok.Targy,
	   EREC_IraIratok.Jelleg,
	   EREC_IraIratok.SztornirozasDat,
	   EREC_IraIratok.FelhasznaloCsoport_Id_Iktato,
	   EREC_IraIratok.FelhasznaloCsoport_Id_Kiadmany,
	   EREC_IraIratok.UgyintezesAlapja,
	   EREC_IraIratok.AdathordozoTipusa,
	   EREC_IraIratok.Csoport_Id_Felelos,
	   EREC_IraIratok.Csoport_Id_Ugyfelelos,
	   EREC_IraIratok.FelhasznaloCsoport_Id_Orzo,
	   EREC_IraIratok.KiadmanyozniKell,
	   EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez,
	   EREC_IraIratok.Hatarido,
	   EREC_IraIratok.MegorzesiIdo,
	   EREC_IraIratok.IratFajta,
	   EREC_IraIratok.Irattipus,
	   EREC_IraIratok.KuldKuldemenyek_Id,
	   EREC_IraIratok.Allapot,
	   EREC_IraIratok.Azonosito,
	   EREC_IraIratok.GeneraltTargy,
	   EREC_IraIratok.IratMetaDef_Id,
	   EREC_IraIratok.IrattarbaKuldDatuma ,
	   EREC_IraIratok.IrattarbaVetelDat,
	   EREC_IraIratok.Ugyirat_Id,
	   EREC_IraIratok.Minosites,
	   EREC_IraIratok.IratHatasaUgyintezesre,
	   EREC_IraIratok.Elintezett,
	   EREC_IraIratok.Ver,
	   EREC_IraIratok.Note,
	   EREC_IraIratok.Stat_id,
	   EREC_IraIratok.ErvKezd,
	   EREC_IraIratok.ErvVege,
	   EREC_IraIratok.Letrehozo_id,
	   EREC_IraIratok.LetrehozasIdo,
	   EREC_IraIratok.Modosito_id,
	   EREC_IraIratok.ModositasIdo,
	   EREC_IraIratok.Zarolo_id,
	   EREC_IraIratok.ZarolasIdo,
	   EREC_IraIratok.Tranz_id,
	   EREC_IraIratok.UIAccessLog_id,
	   EREC_IraIratok.Munkaallomas,
	   EREC_IraIratok.UgyintezesModja,
	   EREC_IraIratok.IntezesIdopontja  
   from 
     EREC_IraIratok as EREC_IraIratok      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go