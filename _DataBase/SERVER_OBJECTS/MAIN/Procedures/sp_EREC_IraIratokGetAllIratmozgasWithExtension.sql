IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIratokGetAllIratmozgasWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraIratokGetAllIratmozgasWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIratokGetAllIratmozgasWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraIratokGetAllIratmozgasWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraIratokGetAllIratmozgasWithExtension]
  @IratId			UNIQUEIDENTIFIER,
  @Where nvarchar(MAX) = '', -- KRT_Esemenyekre vonatkozó feltétel
  @OrderBy			nvarchar(200) = ' order by IratMozgas.HistoryVegrehajtasIdo', --' order by KRT_Esemenyek.LetrehozasIdo',
  @TopRow			nvarchar(5) = '',
  @ExecutorUserId	uniqueidentifier,
  @pageNumber		int = 0,
  @pageSize			int = -1,
  @SelectedRowId	uniqueidentifier = null

as

begin

BEGIN TRY
	set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	DECLARE @sqlcmd nvarchar(MAX)
	DECLARE @LocalTopRow nvarchar(10)
	declare @firstRow int
	declare @lastRow int

	IF @Where is null
	BEGIN
		set @Where = ''
	END

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
		if (@pageSize > @TopRow)
			SET @pageSize = @TopRow;
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
                  
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = 1000
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end

	IF @Where <> ''
	BEGIN
		set @Where = N' and ' + @Where
	END


	set @sqlcmd = N'
select row_number() over('+@OrderBy+') as RowNumber
, IratMozgas.Id
, IratMozgas.ObjTipusNev
, IratMozgas.Obj_Id, IratMozgas.Modosito_Id, IratMozgas.HistoryVegrehajtasIdo
, IratMozgas.FelhasznaloCsoport_Id_Orzo_Old, IratMozgas.FelhasznaloCsoport_Id_Orzo
, IratMozgas.Csoport_Id_Felelos_Old, IratMozgas.Csoport_Id_Felelos
, IratMozgas.OrzoNev_Old, IratMozgas.OrzoNev
, IratMozgas.FelelosNev_Old, IratMozgas.FelelosNev
, IratMozgas.FieldChanged, IratMozgas.FromField, IratMozgas.ToField
, IratMozgas.Ver_Old, IratMozgas.Ver
, KRT_Felhasznalok.Nev VegrehajtoNev
--, KRT_Funkciok.Nev FunkcioNev
, KRT_Esemenyek.FunkcioNev
, KRT_Esemenyek.Id EsemenyId, KRT_Esemenyek.Note EsemenyNote, KRT_Esemenyek.Ver EsemenyVer
into #result
from
(
'
-- Ugyiratok --
/*
set @sqlcmd = @sqlcmd + N'
select uh.HistoryId as Id
, ''EREC_UgyUgyiratok'' as ObjTipusNev
, uh.Id Obj_Id
, uh.Modosito_Id
, HistoryVegrehajtasIdo = case when uh.HistoryVegrehajtasIdo > uh.ModositasIdo
	then uh.ModositasIdo
	else uh.HistoryVegrehajtasIdo end
, uhOld.FelhasznaloCsoport_Id_Orzo FelhasznaloCsoport_Id_Orzo_Old, uh.FelhasznaloCsoport_Id_Orzo
, uhOld.Csoport_Id_Felelos Csoport_Id_Felelos_Old, uh.Csoport_Id_Felelos
, fOrzoOld.Nev OrzoNev_Old, fOrzo.Nev OrzoNev
, fFelelosOld.Nev FelelosNev_Old, fFelelos.Nev FelelosNev
, FieldChanged = case
	when uhOld.Csoport_Id_Felelos <> uh.Csoport_Id_Felelos then ''Csoport_Id_Felelos''
	when uhOld.FelhasznaloCsoport_Id_Orzo <> uh.FelhasznaloCsoport_Id_Orzo then ''FelhasznaloCsoport_Id_Orzo''
end
, FromField = case
	when uhOld.Csoport_Id_Felelos <> uh.Csoport_Id_Felelos then fFelelosOld.Nev
	when uhOld.FelhasznaloCsoport_Id_Orzo <> uh.FelhasznaloCsoport_Id_Orzo then fOrzoOld.Nev
end
, ToField = case
	when uhOld.Csoport_Id_Felelos <> uh.Csoport_Id_Felelos then fFelelos.Nev
	when uhOld.FelhasznaloCsoport_Id_Orzo <> uh.FelhasznaloCsoport_Id_Orzo then fOrzo.Nev
end
, uhOld.Ver Ver_Old, uh.Ver
from EREC_UgyUgyiratokHistory uh
inner join EREC_UgyUgyiratokHistory uhOld on uhOld.Id=uh.Id
	and uhOld.Ver=(select Max(Ver) from EREC_UgyUgyiratokHistory where Id=uh.Id and Ver < uh.Ver)
left join KRT_Csoportok fOrzo on fOrzo.Id = uh.FelhasznaloCsoport_Id_Orzo
left join KRT_Csoportok fFelelos on fFelelos.Id = uh.Csoport_Id_Felelos
left join KRT_Csoportok fOrzoOld on fOrzoOld.Id = uhOld.FelhasznaloCsoport_Id_Orzo
left join KRT_Csoportok fFelelosOld on fFelelosOld.Id = uhOld.Csoport_Id_Felelos
where uh.Id=(select EREC_IraIratok.Ugyirat_Id from EREC_IraIratok where Id=@IratId)
and (uh.FelhasznaloCsoport_Id_Orzo<> uhOld.FelhasznaloCsoport_Id_Orzo
or uh.Csoport_Id_Felelos<>uhOld.Csoport_Id_Felelos)
'
*/

-- Iratok --
/*
	set @sqlcmd = @sqlcmd + N'
UNION ALL
select ih.HistoryId as Id
, ''EREC_IraIratok'' as ObjTipusNev
, ih.Id Obj_Id
, ih.Modosito_Id
, HistoryVegrehajtasIdo = case when ih.HistoryVegrehajtasIdo > ih.ModositasIdo
	then ih.ModositasIdo
	else ih.HistoryVegrehajtasIdo end
, ihOld.FelhasznaloCsoport_Id_Orzo FelhasznaloCsoport_Id_Orzo_Old, ih.FelhasznaloCsoport_Id_Orzo
, ihOld.Csoport_Id_Felelos Csoport_Id_Felelos_Old, ih.Csoport_Id_Felelos
, fOrzoOld.Nev OrzoNev_Old, fOrzo.Nev OrzoNev
, fFelelosOld.Nev FelelosNev_Old, fFelelos.Nev  FelelosNev
, FieldChanged = case
	when ihOld.Csoport_Id_Felelos <> ih.Csoport_Id_Felelos then ''Csoport_Id_Felelos''
	when ihOld.FelhasznaloCsoport_Id_Orzo <> ih.FelhasznaloCsoport_Id_Orzo then ''FelhasznaloCsoport_Id_Orzo''
end
, FromField = case
	when ihOld.Csoport_Id_Felelos <> ih.Csoport_Id_Felelos then fFelelosOld.Nev
	when ihOld.FelhasznaloCsoport_Id_Orzo <> ih.FelhasznaloCsoport_Id_Orzo then fOrzoOld.Nev
end
, ToField = case
	when ihOld.Csoport_Id_Felelos <> ih.Csoport_Id_Felelos then fFelelos.Nev
	when ihOld.FelhasznaloCsoport_Id_Orzo <> ih.FelhasznaloCsoport_Id_Orzo then fOrzo.Nev
end
, ihOld.Ver Ver_Old, ih.Ver
from EREC_IraIratokHistory ih
inner join EREC_IraIratokHistory ihOld on ihOld.Id=ih.Id
	and ihOld.Ver=(select Max(Ver) from EREC_IraIratokHistory where Id=ih.Id and Ver < ih.Ver)
left join KRT_Csoportok fOrzo on fOrzo.Id = ih.FelhasznaloCsoport_Id_Orzo
left join KRT_Csoportok fFelelos on fFelelos.Id = ih.Csoport_Id_Felelos
left join KRT_Csoportok fOrzoOld on fOrzoOld.Id = ihOld.FelhasznaloCsoport_Id_Orzo
left join KRT_Csoportok fFelelosOld on fFelelosOld.Id = ihOld.Csoport_Id_Felelos
where ih.Id=@IratId
and (ih.FelhasznaloCsoport_Id_Orzo<> ihOld.FelhasznaloCsoport_Id_Orzo
or ih.Csoport_Id_Felelos<>ihOld.Csoport_Id_Felelos)
' */

--Iratpeldanyok --
	set @sqlcmd = @sqlcmd + N'
--UNION ALL
select ph.HistoryId as Id
, ''EREC_PldIratPeldanyok'' as ObjTipusNev
, ph.Id Obj_Id
, ph.Modosito_Id
, HistoryVegrehajtasIdo = case when ph.HistoryVegrehajtasIdo > ph.ModositasIdo
	then ph.ModositasIdo
	else ph.HistoryVegrehajtasIdo end
, phOld.FelhasznaloCsoport_Id_Orzo FelhasznaloCsoport_Id_Orzo_Old, ph.FelhasznaloCsoport_Id_Orzo
, phOld.Csoport_Id_Felelos Csoport_Id_Felelos_Old, ph.Csoport_Id_Felelos
, fOrzoOld.Nev OrzoNev_Old, fOrzo.Nev OrzoNev
, fFelelosOld.Nev FelelosNev_Old, fFelelos.Nev  FelelosNev
, FieldChanged = case
	when phOld.Csoport_Id_Felelos <> ph.Csoport_Id_Felelos then ''Csoport_Id_Felelos''
	when phOld.FelhasznaloCsoport_Id_Orzo <> ph.FelhasznaloCsoport_Id_Orzo then ''FelhasznaloCsoport_Id_Orzo''
end
, FromField = case
	when phOld.Csoport_Id_Felelos <> ph.Csoport_Id_Felelos then fFelelosOld.Nev
	when phOld.FelhasznaloCsoport_Id_Orzo <> ph.FelhasznaloCsoport_Id_Orzo then fOrzoOld.Nev
end
, ToField = case
	when phOld.Csoport_Id_Felelos <> ph.Csoport_Id_Felelos then fFelelos.Nev
	when phOld.FelhasznaloCsoport_Id_Orzo <> ph.FelhasznaloCsoport_Id_Orzo then fOrzo.Nev
end
, phOld.Ver Ver_Old, ph.Ver
from EREC_PldIratPeldanyokHistory ph
inner join EREC_PldIratPeldanyokHistory phOld on phOld.Id=ph.Id
	and phOld.Ver=(select Max(Ver) from EREC_PldIratPeldanyokHistory where Id=ph.Id and Ver < ph.Ver)
left join KRT_Csoportok fOrzo on fOrzo.Id = ph.FelhasznaloCsoport_Id_Orzo
left join KRT_Csoportok fFelelos on fFelelos.Id = ph.Csoport_Id_Felelos
left join KRT_Csoportok fOrzoOld on fOrzoOld.Id = phOld.FelhasznaloCsoport_Id_Orzo
left join KRT_Csoportok fFelelosOld on fFelelosOld.Id = phOld.Csoport_Id_Felelos
where ph.IraIrat_Id=@IratId and ph.Sorszam=1
and (ph.FelhasznaloCsoport_Id_Orzo<> phOld.FelhasznaloCsoport_Id_Orzo
or ph.Csoport_Id_Felelos<>phOld.Csoport_Id_Felelos)
'

	set @sqlcmd = @sqlcmd + N'
) IratMozgas
outer apply (select top 1 e.Funkcio_Id, f.Nev FunkcioNev, e.Id, e.Note, e.Ver,
OrderByKod=case 
when f.Kod like ''%Atadas%'' or f.Kod like ''%Atvetel%'' or f.Kod like ''%Visszakuldes%''
then 1
when f.Kod like ''%Szignalas%''
then 2
else 3 end
from KRT_Esemenyek e
left join dbo.KRT_Funkciok f on e.Funkcio_id=f.Id
		where e.Obj_Id=IratMozgas.Obj_Id
		and e.Felhasznalo_Id_User=IratMozgas.Modosito_Id
		and e.LetrehozasIdo between cast(convert(varchar,IratMozgas.HistoryVegrehajtasIdo,120) as datetime)
			and DateAdd(ss, 10, IratMozgas.HistoryVegrehajtasIdo)
		and f.Kod not in (
		''UgyiratokList'',''UgyiratNew'',''UgyiratView'',''UgyiratViewHistory'',''UgyiratInvalidate''
		, ''UgyiratRevalidate'',''UgyiratMaint'',''UgyiratLock'', ''UgyiratForceLock'',''UgyiratUnlock'',''UgyiratForceUnlock''
		, ''IratPeldanyokList'',''IratPeldanyNew'',''IratPeldanyView'',''IratPeldanyViewHistory'',''IratPeldanyInvalidate''
		, ''IratPeldanyRevalidate'',''IratPeldanyMaint'',''IratPeldanyLock'', ''IratPeldanyForceLock'',''IratPeldanyUnlock'',''IratPeldanyForceUnlock''
		)
		order by OrderByKod, e.LetrehozasIdo) KRT_Esemenyek

left join dbo.KRT_Felhasznalok KRT_Felhasznalok on KRT_Felhasznalok.Id=IratMozgas.Modosito_Id
'

		if (@SelectedRowId is not null)
			set @sqlcmd = @sqlcmd + N'
			if exists (select 1 from #result where Id = @SelectedRowId)
			BEGIN
				select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
				set @firstRow = (@pageNumber - 1) * @pageSize + 1;
				set @lastRow = @pageNumber*@pageSize;
				select @pageNumber = @pageNumber - 1
			END
			ELSE'
		--ELSE
			set @sqlcmd = @sqlcmd + N' 
				if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
				BEGIN
					select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
					set @firstRow = (@pageNumber - 1) * @pageSize + 1;
					set @lastRow = @pageNumber*@pageSize;
					select @pageNumber = @pageNumber - 1
				END ;'
			
		set @sqlcmd = @sqlcmd + N'
		select 
		#result.RowNumber,
		#result.Id,
		#result.ObjTipusNev,
		#result.Obj_Id,
		#result.Modosito_Id,
		#result.HistoryVegrehajtasIdo,
		#result.FelhasznaloCsoport_Id_Orzo_Old,
		#result.FelhasznaloCsoport_Id_Orzo,
		#result.Csoport_Id_Felelos_Old,
		#result.Csoport_Id_Felelos,
		#result.OrzoNev_Old,
		#result.OrzoNev,
		#result.FelelosNev_Old,
		#result.FelelosNev,
		#result.FieldChanged,
		#result.FromField,
		#result.ToField,
		#result.Ver_Old,
		#result.Ver,
		#result.VegrehajtoNev,
		#result.FunkcioNev,
		#result.EsemenyId,
		#result.EsemenyNote,
		#result.EsemenyVer
from #result
    where RowNumber between @firstRow and @lastRow
	ORDER BY #result.RowNumber;'
	
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';
	
	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier, @IratId uniqueidentifier'
		,@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId, @IratId = @IratId;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
