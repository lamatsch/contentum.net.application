

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_IraIratokGetWithRightCheck')
            and   type = 'P')
   drop procedure sp_EREC_IraIratokGetWithRightCheck
go

create procedure sp_EREC_IraIratokGetWithRightCheck
         @Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FelhasznaloSzervezet_Id   uniqueidentifier,
      @Jogszint   char(1)
         
as

begin
BEGIN TRY

   set nocount on

   DECLARE @Org uniqueidentifier
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
   if (@Org is null)
   begin
      RAISERROR('[50202]',16,1)
   end

   --if dbo.fn_IsAdmin(@ExecutorUserId) = 0 AND not exists
   IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0 AND not exists
   (
      SELECT top(1) 1 FROM krt_jogosultak
         INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
         where krt_jogosultak.Obj_Id = @Id
   ) and not exists
   (
      SELECT top(1) 1 from erec_irairatok
         WHERE erec_irairatok.Id = @Id
         AND erec_irairatok.Ugyirat_Id IN 
         (
            SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
               INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
               INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
            UNION ALL
            SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
               INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id --AND @Jogszint != 'I'
               INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
            UNION ALL
            SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
               INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
                  ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
            UNION ALL
            SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
               INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
                  ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
            UNION ALL
            SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
               INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
                  ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
            UNION ALL
            SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_UgyUgyiratok') as ids
            UNION ALL
            SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
               INNER JOIN KRT_MappaTartalmak on KRT_MappaTartalmak.Obj_Id = erec_ugyugyiratok.Id
               INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
               INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
               INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
               where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
               and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
         )
   )
   and not exists --CR1716: len�z�nk a p�ld�nyba, CR 2728: aki l�thatja a p�ld�nyt az l�thatja az iratot is
   (
      SELECT top(1) 1 from erec_irairatok
            INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = EREC_IraIratok.Id
         WHERE erec_irairatok.Id = @Id 
         AND EREC_PldIratpeldanyok.Id IN
         (
            SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
               INNER JOIN KRT_Jogosultak ON KRT_Jogosultak.Obj_Id = EREC_PldIratPeldanyok.Id
               INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
               ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
            UNION ALL
            SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
               INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
               ON CsoportTagokAll.Id = EREC_PldIratpeldanyok.Csoport_Id_Felelos
            UNION ALL
            SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
               INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
               ON CsoportTagokAll.Id = EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo
            UNION ALL
               SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_PldIratPeldanyok')
            UNION ALL
               SELECT EREC_PldIratpeldanyok.Id FROM EREC_PldIratpeldanyok
               INNER JOIN KRT_MappaTartalmak on KRT_MappaTartalmak.Obj_Id = EREC_PldIratpeldanyok.Id
               INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
               INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
               INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
               where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
               and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
         )
   ) AND NOT EXISTS
   (
      SELECT 1 FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_IraIratok') as ids
         WHERE ids.Id = @Id
   )
      RAISERROR('[50102]',16,1)

   -- Org sz�r�s
   IF not exists(select 1 from EREC_IraIktatoKonyvek
      where EREC_IraIktatoKonyvek.Org=@Org
      and EREC_IraIktatoKonyvek.Id=
      (select EREC_UgyUgyiratok.IraIktatoKonyv_Id from EREC_UgyUgyiratok
      where EREC_UgyUgyiratok.Id=
         (select EREC_IraIratok.Ugyirat_Id from EREC_IraIratok
         where EREC_IraIratok.Id=@Id
         )
      ))
   BEGIN
      RAISERROR('[50101]',16,1)
   END

   DECLARE @sqlcmd nvarchar(4000)

   SET @sqlcmd = 
     'select 
         EREC_IraIratok.Id,
      EREC_IraIratok.PostazasIranya,
      EREC_IraIratok.Alszam,
      EREC_IraIratok.Sorszam,
      EREC_IraIratok.UtolsoSorszam,
      EREC_IraIratok.UgyUgyIratDarab_Id,
      EREC_IraIratok.Kategoria,
      EREC_IraIratok.HivatkozasiSzam,
      EREC_IraIratok.IktatasDatuma,
      EREC_IraIratok.ExpedialasDatuma,
      EREC_IraIratok.ExpedialasModja,
      EREC_IraIratok.FelhasznaloCsoport_Id_Expedial,
      EREC_IraIratok.Targy,
      EREC_IraIratok.AdathordozoTipusa,
      EREC_IraIratok.Jelleg,
      EREC_IraIratok.SztornirozasDat,
      EREC_IraIratok.FelhasznaloCsoport_Id_Iktato,
      EREC_IraIratok.FelhasznaloCsoport_Id_Kiadmany,
      EREC_IraIratok.UgyintezesAlapja,
      EREC_IraIratok.Csoport_Id_Felelos,
      EREC_IraIratok.Csoport_Id_Ugyfelelos,
      EREC_IraIratok.FelhasznaloCsoport_Id_Orzo,
      EREC_IraIratok.KiadmanyozniKell,
      EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez,
      EREC_IraIratok.Hatarido,
      EREC_IraIratok.MegorzesiIdo,
      EREC_IraIratok.IratFajta,
      EREC_IraIratok.Irattipus,
      EREC_IraIratok.KuldKuldemenyek_Id,
      EREC_IraIratok.Allapot,
      EREC_IraIratok.Azonosito,
      EREC_IraIratok.GeneraltTargy,
      EREC_IraIratok.IratMetaDef_Id,
      EREC_IraIratok.IrattarbaKuldDatuma ,
      EREC_IraIratok.IrattarbaVetelDat,
      EREC_IraIratok.Ugyirat_Id,
      EREC_IraIratok.Minosites,
      EREC_IraIratok.Ver,
      EREC_IraIratok.Note,
      EREC_IraIratok.Stat_id,
      EREC_IraIratok.ErvKezd,
      EREC_IraIratok.ErvVege,
      EREC_IraIratok.Letrehozo_id,
      EREC_IraIratok.LetrehozasIdo,
      EREC_IraIratok.Modosito_id,
      EREC_IraIratok.ModositasIdo,
      EREC_IraIratok.Zarolo_id,
      EREC_IraIratok.ZarolasIdo,
      EREC_IraIratok.Tranz_id,
      EREC_IraIratok.UIAccessLog_id,
      EREC_IraIratok.Munkaallomas,
      EREC_IraIratok.UgyintezesModja,
      EREC_IraIratok.IntezesIdopontja,
	  EREC_IraIratok.Elintezett,
	  EREC_IraIratok.IratHatasaUgyintezesre
      from 
       EREC_IraIratok as EREC_IraIratok
      where
       EREC_IraIratok.Id = @Id'

   exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id
    
   if @@rowcount = 0
     begin
      RAISERROR('[50101]',16,1)
     end

END TRY
BEGIN CATCH
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
