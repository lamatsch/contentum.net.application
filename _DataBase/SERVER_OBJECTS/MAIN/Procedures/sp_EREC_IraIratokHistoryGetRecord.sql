set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_IraIratokHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_EREC_IraIratokHistoryGetRecord
go

create procedure sp_EREC_IraIratokHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_IraIratokHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_IraIratokHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PostazasIranya' as ColumnName,               
               cast(Old.PostazasIranya as nvarchar(99)) as OldValue,
               cast(New.PostazasIranya as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PostazasIranya != New.PostazasIranya 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Alszam' as ColumnName,               
               cast(Old.Alszam as nvarchar(99)) as OldValue,
               cast(New.Alszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Alszam != New.Alszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Sorszam' as ColumnName,               
               cast(Old.Sorszam as nvarchar(99)) as OldValue,
               cast(New.Sorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Sorszam != New.Sorszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UtolsoSorszam' as ColumnName,               
               cast(Old.UtolsoSorszam as nvarchar(99)) as OldValue,
               cast(New.UtolsoSorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UtolsoSorszam != New.UtolsoSorszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyUgyIratDarab_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratdarabokAzonosito(Old.UgyUgyIratDarab_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratdarabokAzonosito(New.UgyUgyIratDarab_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyUgyIratDarab_Id != New.UgyUgyIratDarab_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_UgyUgyiratdarabok FTOld on FTOld.Id = Old.UgyUgyIratDarab_Id and FTOld.Ver = Old.Ver
         left join EREC_UgyUgyiratdarabok FTNew on FTNew.Id = New.UgyUgyIratDarab_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kategoria' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Kategoria != New.Kategoria 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRATKATEGORIA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Kategoria and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Kategoria and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HivatkozasiSzam' as ColumnName,               
               cast(Old.HivatkozasiSzam as nvarchar(99)) as OldValue,
               cast(New.HivatkozasiSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HivatkozasiSzam != New.HivatkozasiSzam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IktatasDatuma' as ColumnName,               
               cast(Old.IktatasDatuma as nvarchar(99)) as OldValue,
               cast(New.IktatasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IktatasDatuma != New.IktatasDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ExpedialasDatuma' as ColumnName,               
               cast(Old.ExpedialasDatuma as nvarchar(99)) as OldValue,
               cast(New.ExpedialasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ExpedialasDatuma != New.ExpedialasDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ExpedialasModja' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ExpedialasModja != New.ExpedialasModja 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'EXPEDIALAS_MODJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.ExpedialasModja and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.ExpedialasModja and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Expedial' as ColumnName,               
               cast(Old.FelhasznaloCsoport_Id_Expedial as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Expedial as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Expedial != New.FelhasznaloCsoport_Id_Expedial 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Targy' as ColumnName,               
               cast(Old.Targy as nvarchar(99)) as OldValue,
               cast(New.Targy as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Targy != New.Targy 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Jelleg' as ColumnName,               
               cast(Old.Jelleg as nvarchar(99)) as OldValue,
               cast(New.Jelleg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Jelleg != New.Jelleg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SztornirozasDat' as ColumnName,               
               cast(Old.SztornirozasDat as nvarchar(99)) as OldValue,
               cast(New.SztornirozasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SztornirozasDat != New.SztornirozasDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Iktato' as ColumnName,               
               cast(Old.FelhasznaloCsoport_Id_Iktato as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Iktato as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Iktato != New.FelhasznaloCsoport_Id_Iktato 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Kiadmany' as ColumnName,               
               cast(Old.FelhasznaloCsoport_Id_Kiadmany as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Kiadmany as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Kiadmany != New.FelhasznaloCsoport_Id_Kiadmany 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesAlapja' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyintezesAlapja != New.UgyintezesAlapja 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYINTEZES_ALAPJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.UgyintezesAlapja and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.UgyintezesAlapja and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AdathordozoTipusa' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AdathordozoTipusa != New.AdathordozoTipusa 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYINTEZES_ALAPJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.AdathordozoTipusa and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.AdathordozoTipusa and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Felelos' as ColumnName,               
               cast(Old.Csoport_Id_Felelos as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Felelos as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Felelos != New.Csoport_Id_Felelos 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Ugyfelelos' as ColumnName,               
               cast(Old.Csoport_Id_Ugyfelelos as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Ugyfelelos as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Ugyfelelos != New.Csoport_Id_Ugyfelelos 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Orzo' as ColumnName,               
               cast(Old.FelhasznaloCsoport_Id_Orzo as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Orzo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Orzo != New.FelhasznaloCsoport_Id_Orzo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KiadmanyozniKell' as ColumnName,               
               cast(Old.KiadmanyozniKell as nvarchar(99)) as OldValue,
               cast(New.KiadmanyozniKell as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KiadmanyozniKell != New.KiadmanyozniKell 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Ugyintez' as ColumnName,               
               cast(Old.FelhasznaloCsoport_Id_Ugyintez as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Ugyintez as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Ugyintez != New.FelhasznaloCsoport_Id_Ugyintez 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Hatarido' as ColumnName,               
               cast(Old.Hatarido as nvarchar(99)) as OldValue,
               cast(New.Hatarido as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Hatarido != New.Hatarido 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MegorzesiIdo' as ColumnName,               
               cast(Old.MegorzesiIdo as nvarchar(99)) as OldValue,
               cast(New.MegorzesiIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MegorzesiIdo != New.MegorzesiIdo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IratFajta' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IratFajta != New.IratFajta 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRAT_FAJTA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.IratFajta and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.IratFajta and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Irattipus' as ColumnName,               
               cast(Old.Irattipus as nvarchar(99)) as OldValue,
               cast(New.Irattipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Irattipus != New.Irattipus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldKuldemenyek_Id' as ColumnName,               
               cast(Old.KuldKuldemenyek_Id as nvarchar(99)) as OldValue,
               cast(New.KuldKuldemenyek_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KuldKuldemenyek_Id != New.KuldKuldemenyek_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Allapot != New.Allapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRAT_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Allapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Allapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Azonosito' as ColumnName,               
               cast(Old.Azonosito as nvarchar(99)) as OldValue,
               cast(New.Azonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Azonosito != New.Azonosito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'GeneraltTargy' as ColumnName,               
               cast(Old.GeneraltTargy as nvarchar(99)) as OldValue,
               cast(New.GeneraltTargy as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.GeneraltTargy != New.GeneraltTargy 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IratMetaDef_Id' as ColumnName,               
               cast(Old.IratMetaDef_Id as nvarchar(99)) as OldValue,
               cast(New.IratMetaDef_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IratMetaDef_Id != New.IratMetaDef_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarbaKuldDatuma ' as ColumnName,               
               cast(Old.IrattarbaKuldDatuma  as nvarchar(99)) as OldValue,
               cast(New.IrattarbaKuldDatuma  as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IrattarbaKuldDatuma  != New.IrattarbaKuldDatuma  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarbaVetelDat' as ColumnName,               
               cast(Old.IrattarbaVetelDat as nvarchar(99)) as OldValue,
               cast(New.IrattarbaVetelDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IrattarbaVetelDat != New.IrattarbaVetelDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ugyirat_Id' as ColumnName,               
               cast(Old.Ugyirat_Id as nvarchar(99)) as OldValue,
               cast(New.Ugyirat_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Ugyirat_Id != New.Ugyirat_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Minosites' as ColumnName,               
               cast(Old.Minosites as nvarchar(99)) as OldValue,
               cast(New.Minosites as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Minosites != New.Minosites 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IratHatasaUgyintezesre' as ColumnName,               
               cast(Old.IratHatasaUgyintezesre as nvarchar(99)) as OldValue,
               cast(New.IratHatasaUgyintezesre as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IratHatasaUgyintezesre != New.IratHatasaUgyintezesre 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Elintezett' as ColumnName,               
               cast(Old.Elintezett as nvarchar(99)) as OldValue,
               cast(New.Elintezett as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Elintezett != New.Elintezett 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go