IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIratokWithExtensionSSRS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraIratokWithExtensionSSRS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIratokWithExtensionSSRS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraIratokWithExtensionSSRS] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraIratokWithExtensionSSRS]

  @Where			nvarchar(MAX) = '',
  @OrderBy			nvarchar(400) = ' order by iktatatoszam ',
  @ExecutorUserId	uniqueidentifier,
  @TopRow NVARCHAR(5) = ''

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  set @sqlcmd = 
  '
select 
EREC_IraIratok.Azonosito as iktatatoszam,
EREC_IraIktatokonyvek_sort.Iktatohely,
EREC_IraIktatokonyvek_sort.Ev,
EREC_UgyUgyiratdarabok.Foszam,
EREC_IraIratok.Alszam,
CONVERT(nvarchar(10), EREC_IraIratok.IktatasDatuma, 102) as iktatas_idopontja,
CONVERT(nvarchar(10), EREC_KuldKuldemenyek.BeerkezesIdeje, 102) as kuldemeny_erkezes_idopontja,
dbo.fn_KodtarErtekNeve(''KULDEMENY_KULDES_MODJA'', EREC_KuldKuldemenyek.KuldesMod,''' + CAST(@Org as NVarChar(40)) + ''') as kuldemeny_erkezes_modja,
EREC_IraIktatoKonyvek.MegkulJelzes + '' - '' + CAST(Erkezteto_Szam AS NVarchar(100)) + '' / '' + CAST(EREC_IraIktatoKonyvek.Ev AS NVarchar(100)) as kuldemeny_erkezes_erkeztetoszama,
CONVERT(nvarchar(10), EREC_PldIratPeldanyok.PostazasDatuma, 102) as kuldemeny_kuldes_idopontja,
dbo.fn_KodtarErtekNeve(''KULDEMENY_KULDES_MODJA'', EREC_PldIratPeldanyok.KuldesMod,''' + CAST(@Org as NVarChar(40)) + ''') as kuldemeny_kuldes_modja,
dbo.fn_KodtarErtekNeve(''UGYINTEZES_ALAPJA'', EREC_KuldKuldemenyek.AdathordozoTipusa,''' + CAST(@Org as NVarChar(40)) + ''') as kuldemeny_adathordozo_tipusa,
EREC_KuldKuldemenyek.NevSTR_Bekuldo as kuldo_nev,
EREC_KuldKuldemenyek.CimSTR_Bekuldo as kuldo_cim,
cimzett_nev = 
CASE EREC_IraIratok.PostazasIranya
WHEN ''1''
THEN dbo.fn_GetCsoportNev(EREC_KuldKuldemenyek.Csoport_Id_Cimzett)
ELSE EREC_PldIratPeldanyok.NevSTR_Cimzett
END,
cimzett_cim = 
CASE EREC_IraIratok.PostazasIranya
WHEN ''1''
THEN ''''
ELSE EREC_PldIratPeldanyok.CimSTR_Cimzett
END,
EREC_KuldKuldemenyek.HivatkozasiSzam as idegen_szam,
(SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege) as mellekletek_szama, 
dbo.fn_GetCsoportNev(EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez) as ugyintezo_nev,
EREC_IraIratok.Targy as irat_targy,
(SELECT TOP 1 EREC_UgyiratObjKapcsolatok.Leiras FROM EREC_UgyiratObjKapcsolatok WHERE EREC_UgyiratObjKapcsolatok.KapcsolatTipus = ''07'' AND EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt  = EREC_UgyUgyiratok.Id AND getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege) as elo_utoirat_iktatoszam,
ISNULL(LTRIM(RTRIM(REPLACE((
SELECT TOP 5 EREC_IraKezFeljegyzesek.Leiras AS [data()]
FROM EREC_IraKezFeljegyzesek 
WHERE EREC_IraKezFeljegyzesek.IraIrat_Id = EREC_IraIratok.Id AND getdate() BETWEEN EREC_IraKezFeljegyzesek.ErvKezd AND EREC_IraKezFeljegyzesek.ErvVege
ORDER BY EREC_IraKezFeljegyzesek.Id
FOR XML PATH ('''')), '''', '',''))), '''') as kezelesi_feljegyzes,
--(SELECT TOP 1 EREC_IraKezFeljegyzesek.Leiras FROM EREC_IraKezFeljegyzesek WHERE EREC_IraKezFeljegyzesek.IraIrat_Id = EREC_IraIratok.Id AND getdate() BETWEEN EREC_IraKezFeljegyzesek.ErvKezd AND EREC_IraKezFeljegyzesek.ErvVege) as kezelesi_feljegyzes,
CONVERT(nvarchar(10), EREC_UgyUgyiratok.Hatarido, 102) as ugyintezes_hatarideje,
CONVERT(nvarchar(10), EREC_UgyUgyiratok.ElintezesDat, 102) as vegrehajtas_idopontja,
dbo.fn_MergeIrattariTetelSzam(EREC_AgazatiJelek.Kod,EREC_IraIrattariTetelek.IrattariTetelszam,EREC_IraIrattariTetelek.MegorzesiIdo,EREC_IraIrattariTetelek.IrattariJel,''' + cast(@Org as NVarChar(40)) + ''') as irattari_tetelszam,
CONVERT(nvarchar(10), EREC_UgyUgyiratok.IrattarbaVetelDat, 102) as irattarba_helyezes_idopontja,
EREC_UgyUgyiratok.IraIktatokonyv_Id
'

set @sqlcmd = @sqlcmd + N'
from EREC_IraIratok as EREC_IraIratok				
		LEFT JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok
			ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
		LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok 
			ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id  	
		LEFT JOIN [EREC_KuldKuldemenyek] AS EREC_KuldKuldemenyek
			ON EREC_IraIratok.[KuldKuldemenyek_Id] = EREC_KuldKuldemenyek.[Id]
		left join EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek 
			on EREC_IraIktatoKonyvek.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
		LEFT JOIN EREC_IraIktatokonyvek as EREC_IraIktatokonyvek_sort
			ON EREC_IraIktatokonyvek_sort.Id = EREC_UgyUgyiratdarabok.IraIktatokonyv_Id
		LEFT JOIN EREC_PldIratPeldanyok as EREC_PldIratPeldanyok
			on EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id and EREC_PldIratPeldanyok.Sorszam = ''1''
		left join EREC_IraIrattariTetelek as EREC_IraIrattariTetelek
				ON EREC_UgyUgyiratok.IraIrattariTetel_Id = EREC_IraIrattariTetelek.Id
		left join EREC_AgazatiJelek as EREC_Agazatijelek
				ON EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_Agazatijelek.Id	 
		left join EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Ugyirat
				ON EREC_IraIktatoKonyvek_Ugyirat.Id = EREC_UgyUgyiratok.IraIktatokonyv_Id
WHERE EREC_IraIktatoKonyvek_Ugyirat.Org=''' + CAST(@Org as Nvarchar(40)) + ''''  

if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + @Where
	end

SET @sqlcmd = @sqlcmd + @OrderBy 
--print @sqlcmd
exec(@sqlcmd)


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
