IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIrattariTetelekGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraIrattariTetelekGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIrattariTetelekGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraIrattariTetelekGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraIrattariTetelekGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraIrattariTetelek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('50202',16,1)
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_IraIrattariTetelek.Id,
	   EREC_IraIrattariTetelek.Org,
	   EREC_IraIrattariTetelek.IrattariTetelszam,
	   EREC_IraIrattariTetelek.Nev,
	   EREC_IraIrattariTetelek.IrattariJel,
	   EREC_IraIrattariTetelek.MegorzesiIdo,
	   EREC_IraIrattariTetelek.Idoegyseg,
	   EREC_IraIrattariTetelek.Folyo_CM,
	   EREC_IraIrattariTetelek.AgazatiJel_Id,
	   EREC_IraIrattariTetelek.Ver,
	   EREC_IraIrattariTetelek.Note,
	   EREC_IraIrattariTetelek.Stat_id,
	   EREC_IraIrattariTetelek.ErvKezd,
	   EREC_IraIrattariTetelek.ErvVege,
	   EREC_IraIrattariTetelek.Letrehozo_id,
	   EREC_IraIrattariTetelek.LetrehozasIdo,
	   EREC_IraIrattariTetelek.Modosito_id,
	   EREC_IraIrattariTetelek.ModositasIdo,
	   EREC_IraIrattariTetelek.Zarolo_id,
	   EREC_IraIrattariTetelek.ZarolasIdo,
	   EREC_IraIrattariTetelek.Tranz_id,
	   EREC_IraIrattariTetelek.UIAccessLog_id,
	   EREC_IraIrattariTetelek.EvenTuliIktatas  
   from 
     EREC_IraIrattariTetelek as EREC_IraIrattariTetelek      
    Where EREC_IraIrattariTetelek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
