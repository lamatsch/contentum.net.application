IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIrattariTetelekGetAllWithIktathat]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraIrattariTetelekGetAllWithIktathat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraIrattariTetelekGetAllWithIktathat]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraIrattariTetelekGetAllWithIktathat] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraIrattariTetelekGetAllWithIktathat]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraIrattariTetelek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @FelhasznaloSzervezet_Id UNIQUEIDENTIFIER,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
  SET @sqlcmd = 
  'select distinct ' + @LocalTopRow + '
  	   EREC_IraIrattariTetelek.Id,
	   EREC_IraIrattariTetelek.Org,
	   EREC_IraIrattariTetelek.IrattariTetelszam,
	   EREC_IraIrattariTetelek.Nev,
	   EREC_IraIrattariTetelek.IrattariJel,
	   EREC_IraIrattariTetelek.MegorzesiIdo,
	   EREC_IraIrattariTetelek.Idoegyseg,
	   EREC_IraIrattariTetelek.Folyo_CM,
	   EREC_IraIrattariTetelek.AgazatiJel_Id,
	   EREC_IraIrattariTetelek.Ver,
	   EREC_IraIrattariTetelek.Note,
	   EREC_IraIrattariTetelek.Stat_id,
	   EREC_IraIrattariTetelek.ErvKezd,
	   EREC_IraIrattariTetelek.ErvVege,
	   EREC_IraIrattariTetelek.Letrehozo_id,
	   EREC_IraIrattariTetelek.LetrehozasIdo,
	   EREC_IraIrattariTetelek.Modosito_id,
	   EREC_IraIrattariTetelek.ModositasIdo,
	   EREC_IraIrattariTetelek.Zarolo_id,
	   EREC_IraIrattariTetelek.ZarolasIdo,
	   EREC_IraIrattariTetelek.Tranz_id,
	   EREC_IraIrattariTetelek.UIAccessLog_id  
   from 
     EREC_IraIrattariTetelek as EREC_IraIrattariTetelek  
	join EREC_IrattariTetel_Iktatokonyv as EREC_IrattariTetel_Iktatokonyv
		ON EREC_IrattariTetel_Iktatokonyv.IrattariTetel_Id = EREC_IraIrattariTetelek.Id	 
   Where EREC_IrattariTetel_Iktatokonyv.Iktatokonyv_Id 
			in (select Id from dbo.fn_GetAllIktathatoEREC_IraIktatokonyvekByCsoportId(@ExecutorUserId,@FelhasznaloSzervezet_Id)
				where Ev = datepart(year,getdate())
				) 
		and EREC_IraIrattariTetelek.Org=@Org
	'
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;

	exec sp_executesql @sqlcmd,N'@ExecutorUserId uniqueidentifier, @FelhasznaloSzervezet_Id uniqueidentifier, @Org uniqueidentifier',@ExecutorUserId = @ExecutorUserId, @FelhasznaloSzervezet_Id=@FelhasznaloSzervezet_Id, @Org = @Org;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
