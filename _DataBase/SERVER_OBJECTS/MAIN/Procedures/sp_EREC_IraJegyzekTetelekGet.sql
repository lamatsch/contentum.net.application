IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraJegyzekTetelekGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraJegyzekTetelekGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraJegyzekTetelekGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraJegyzekTetelekGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraJegyzekTetelekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IraJegyzekTetelek.Id,
	   EREC_IraJegyzekTetelek.Jegyzek_Id,
	   EREC_IraJegyzekTetelek.Sorszam,
	   EREC_IraJegyzekTetelek.Obj_Id,
	   EREC_IraJegyzekTetelek.ObjTip_Id,
	   EREC_IraJegyzekTetelek.Obj_type,
	   EREC_IraJegyzekTetelek.Allapot,
	   EREC_IraJegyzekTetelek.Megjegyzes,
	   EREC_IraJegyzekTetelek.Azonosito,
	   EREC_IraJegyzekTetelek.SztornozasDat,
	   EREC_IraJegyzekTetelek.Ver,
	   EREC_IraJegyzekTetelek.Note,
	   EREC_IraJegyzekTetelek.Stat_id,
	   EREC_IraJegyzekTetelek.ErvKezd,
	   EREC_IraJegyzekTetelek.ErvVege,
	   EREC_IraJegyzekTetelek.Letrehozo_id,
	   EREC_IraJegyzekTetelek.LetrehozasIdo,
	   EREC_IraJegyzekTetelek.Modosito_id,
	   EREC_IraJegyzekTetelek.ModositasIdo,
	   EREC_IraJegyzekTetelek.Zarolo_id,
	   EREC_IraJegyzekTetelek.ZarolasIdo,
	   EREC_IraJegyzekTetelek.Tranz_id,
	   EREC_IraJegyzekTetelek.UIAccessLog_id
	   from 
		 EREC_IraJegyzekTetelek as EREC_IraJegyzekTetelek 
	   where
		 EREC_IraJegyzekTetelek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
