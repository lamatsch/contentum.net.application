IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraJegyzekekGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraJegyzekekGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraJegyzekekGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraJegyzekekGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraJegyzekekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IraJegyzekek.Id,
	   EREC_IraJegyzekek.Org,
	   EREC_IraJegyzekek.Tipus,
	   EREC_IraJegyzekek.Minosites,
	   EREC_IraJegyzekek.Csoport_Id_felelos,
	   EREC_IraJegyzekek.FelhasznaloCsoport_Id_Vegrehaj,
	   EREC_IraJegyzekek.Partner_Id_LeveltariAtvevo,
	   EREC_IraJegyzekek.LezarasDatuma,
	   EREC_IraJegyzekek.SztornirozasDat,
	   EREC_IraJegyzekek.Nev,
	   EREC_IraJegyzekek.VegrehajtasDatuma,
	   EREC_IraJegyzekek.Ver,
	   EREC_IraJegyzekek.Note,
	   EREC_IraJegyzekek.Stat_id,
	   EREC_IraJegyzekek.ErvKezd,
	   EREC_IraJegyzekek.ErvVege,
	   EREC_IraJegyzekek.Letrehozo_id,
	   EREC_IraJegyzekek.LetrehozasIdo,
	   EREC_IraJegyzekek.Modosito_id,
	   EREC_IraJegyzekek.ModositasIdo,
	   EREC_IraJegyzekek.Zarolo_id,
	   EREC_IraJegyzekek.ZarolasIdo,
	   EREC_IraJegyzekek.Tranz_id,
	   EREC_IraJegyzekek.UIAccessLog_id
	   from 
		 EREC_IraJegyzekek as EREC_IraJegyzekek 
	   where
		 EREC_IraJegyzekek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
