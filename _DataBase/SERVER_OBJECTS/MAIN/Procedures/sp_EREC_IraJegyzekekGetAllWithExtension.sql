IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraJegyzekekGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraJegyzekekGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraJegyzekekGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraJegyzekekGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraJegyzekekGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraJegyzekek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_IraJegyzekek.Id,
	   EREC_IraJegyzekek.Tipus,
	   EREC_IraJegyzekek.Minosites,
	   EREC_IraJegyzekek.Csoport_Id_felelos,
       KRT_CsoportokFelelos.Nev as Csoport_Id_felelos_Nev,
	   EREC_IraJegyzekek.FelhasznaloCsoport_Id_Vegrehaj,
       KRT_CsoportokVegrehajto.Nev as FelhasznaloCsoport_Id_Vegrehajto_Nev,
	   EREC_IraJegyzekek.Partner_Id_LeveltariAtvevo,
       KRT_PartnerekAtvevo.Nev as Partner_Id_LeveltariAtvevo_Nev,
	   EREC_IraJegyzekek.LezarasDatuma,
	   EREC_IraJegyzekek.SztornirozasDat,
	   EREC_IraJegyzekek.Nev,
	   EREC_IraJegyzekek.VegrehajtasDatuma,
	   EREC_IraJegyzekek.Ver,
	   EREC_IraJegyzekek.Note,
	   EREC_IraJegyzekek.Stat_id,
	   EREC_IraJegyzekek.ErvKezd,
	   EREC_IraJegyzekek.ErvVege,
	   EREC_IraJegyzekek.Letrehozo_id,
	   EREC_IraJegyzekek.LetrehozasIdo,
	   EREC_IraJegyzekek.Modosito_id,
	   EREC_IraJegyzekek.ModositasIdo,
	   EREC_IraJegyzekek.Zarolo_id,
	   EREC_IraJegyzekek.ZarolasIdo,
	   EREC_IraJegyzekek.Tranz_id,
	   EREC_IraJegyzekek.UIAccessLog_id  
   from 
     EREC_IraJegyzekek as EREC_IraJegyzekek
     LEFT JOIN KRT_Csoportok as KRT_CsoportokFelelos ON KRT_CsoportokFelelos.Id =  EREC_IraJegyzekek.Csoport_Id_felelos
     LEFT JOIN KRT_Csoportok as KRT_CsoportokVegrehajto ON KRT_CsoportokVegrehajto.Id =  EREC_IraJegyzekek.FelhasznaloCsoport_Id_Vegrehaj
     LEFT JOIN KRT_Partnerek as KRT_PartnerekAtvevo ON KRT_PartnerekAtvevo .Id =  EREC_IraJegyzekek.Partner_Id_LeveltariAtvevo      
    Where EREC_IraJegyzekek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
