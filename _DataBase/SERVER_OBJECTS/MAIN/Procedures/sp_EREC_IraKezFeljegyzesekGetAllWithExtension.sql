IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraKezFeljegyzesekGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraKezFeljegyzesekGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraKezFeljegyzesekGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraKezFeljegyzesekGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraKezFeljegyzesekGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraKezFeljegyzesek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END


  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_IraKezFeljegyzesek.Id,
	   EREC_IraKezFeljegyzesek.IraIrat_Id,
	   EREC_IraKezFeljegyzesek.KezelesTipus,
KRT_KodTarak.Nev as KezelesTipusNev,
--EREC_IraIratok.Azonosito as IktatoSzam_Merge,
EREC_PldIratPeldanyok.Azonosito as IktatoSzam_Merge,
EREC_PldIratPeldanyok.Sorszam,
	   EREC_IraKezFeljegyzesek.Leiras,
	   EREC_IraKezFeljegyzesek.Ver,
	   EREC_IraKezFeljegyzesek.Note,
	   EREC_IraKezFeljegyzesek.Stat_id,
	   EREC_IraKezFeljegyzesek.ErvKezd,
	   EREC_IraKezFeljegyzesek.ErvVege,
	   EREC_IraKezFeljegyzesek.Letrehozo_id,
KRT_Felhasznalok.Nev as Letrehozo_Nev,
	   EREC_IraKezFeljegyzesek.LetrehozasIdo,
CONVERT(nvarchar(10), EREC_IraKezFeljegyzesek.LetrehozasIdo, 102) as LetrehozasIdo_f,
	   EREC_IraKezFeljegyzesek.Modosito_id,
	   EREC_IraKezFeljegyzesek.ModositasIdo,
	   EREC_IraKezFeljegyzesek.Zarolo_id,
	   EREC_IraKezFeljegyzesek.ZarolasIdo,
	   EREC_IraKezFeljegyzesek.Tranz_id,
	   EREC_IraKezFeljegyzesek.UIAccessLog_id 
   from 
     EREC_IraKezFeljegyzesek as EREC_IraKezFeljegyzesek   
		left join KRT_KodCsoportok on KRT_KodCsoportok.Kod=''KEZELESI_FELJEGYZESEK_TIPUSA''
		left join KRT_KodTarak on KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and EREC_IraKezFeljegyzesek.KezelesTipus = KRT_KodTarak.Kod and KRT_KodTarak.Org=''' + CAST(@Org as NVarChar(40)) + '''
	LEFT JOIN KRT_Felhasznalok
		ON EREC_IraKezFeljegyzesek.Letrehozo_id = KRT_Felhasznalok.Id
--left join EREC_IraIratok on EREC_IraKezFeljegyzesek.IraIrat_Id = EREC_IraIratok.Id
left join EREC_PldIratPeldanyok on EREC_IraKezFeljegyzesek.IraIrat_Id = EREC_PldIratPeldanyok.Id
left join EREC_IraIratok on EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id
LEFT JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok
		ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek 
			ON EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id	
LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok 
			ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id  
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
