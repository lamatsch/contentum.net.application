IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraKezbesitesiFejekHistoryGetAllRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraKezbesitesiFejekHistoryGetAllRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraKezbesitesiFejekHistoryGetAllRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraKezbesitesiFejekHistoryGetAllRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraKezbesitesiFejekHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_IraKezbesitesiFejekHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiFejekHistory Old
         inner join EREC_IraKezbesitesiFejekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CsomagAzonosito' as ColumnName,               cast(Old.CsomagAzonosito as nvarchar(99)) as OldValue,
               cast(New.CsomagAzonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiFejekHistory Old
         inner join EREC_IraKezbesitesiFejekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.CsomagAzonosito != New.CsomagAzonosito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tipus' as ColumnName,               cast(Old.Tipus as nvarchar(99)) as OldValue,
               cast(New.Tipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiFejekHistory Old
         inner join EREC_IraKezbesitesiFejekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tipus != New.Tipus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UtolsoNyomtatasIdo' as ColumnName,               cast(Old.UtolsoNyomtatasIdo as nvarchar(99)) as OldValue,
               cast(New.UtolsoNyomtatasIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiFejekHistory Old
         inner join EREC_IraKezbesitesiFejekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UtolsoNyomtatasIdo != New.UtolsoNyomtatasIdo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UtolsoNyomtatas_Id' as ColumnName,               cast(Old.UtolsoNyomtatas_Id as nvarchar(99)) as OldValue,
               cast(New.UtolsoNyomtatas_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiFejekHistory Old
         inner join EREC_IraKezbesitesiFejekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UtolsoNyomtatas_Id != New.UtolsoNyomtatas_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end


GO
