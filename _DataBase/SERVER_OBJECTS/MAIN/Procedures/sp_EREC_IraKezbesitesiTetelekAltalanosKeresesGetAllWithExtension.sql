IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraKezbesitesiTetelekAltalanosKeresesGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraKezbesitesiTetelekAltalanosKeresesGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraKezbesitesiTetelekAltalanosKeresesGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraKezbesitesiTetelekAltalanosKeresesGetAllWithExtension] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IraKezbesitesiTetelekAltalanosKeresesGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @Where_PldIratPeldanyok nvarchar(MAX) = '',
  @Where_KuldKuldemenyek nvarchar(MAX) = '',
  @Where_UgyUgyiratok nvarchar(MAX) = '',
  @Where_Mappak nvarchar(MAX) = '',
  @isObjectMode char(1) = '0', -- 0: csak kézbesítési tételekkel, 1: objektumok kézbesítési tétel nélküli objektumok is
  @KezbesitesiTetelekSearchMode nvarchar(30) = 'LastValidIfExists', -- LastValidIfExists, None, AllValidIfExists, AllIfExists, AllInvalidIfExists, ObjectHasNoValidItem, LastValid, AllValid, All, AllInvalid
  @OrderBy nvarchar(200) = ' order by LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier,
  @Jogosultak		char(1) = '0',
  @pageNumber		int = 0,
  @pageSize			int = -1,
  @SelectedRowId	uniqueidentifier = null


as

begin

BEGIN TRY

	set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
	
	DECLARE @OrgKod nvarchar(100)
   set @OrgKod = (select Kod from KRT_Orgok where Id = @Org)
   
   declare @OrgIsBOMPH int
   
   IF(@OrgKod = 'BOPMH')
	  set @OrgIsBOMPH = 1
    ELSE
      set @OrgIsBOMPH = 0
   
	declare @sqlcmd nvarchar(MAX)
	declare @LocalTopRow nvarchar(10)
	declare @firstRow int
	declare @lastRow INT

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
		if (@pageSize > @TopRow)
			SET @pageSize = @TopRow;
       SET @LocalTopRow = ' TOP ' + @TopRow
     END


	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = 1000
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end
	set @sqlcmd = '';

    declare @isAdminInSzervezet int
	set @isAdminInSzervezet = dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id)     
	/************************************************************
	* Szűrőtáblák összeállítása				*
	************************************************************/
	SET @sqlcmd = @sqlcmd + N'
	create table #filter_pld (Id uniqueidentifier);
	create table #filter_kuld (Id uniqueidentifier);
	create table #filter_ugyirat (Id uniqueidentifier);
	create table #filter_mappa (Id uniqueidentifier);
'

	declare @jogosultak_kezbesitesitetelek nvarchar(500)
	set @jogosultak_kezbesitesitetelek = '(1=1)'
	-- Jogosultságszűrés
	IF @isAdminInSzervezet = 0 AND @Jogosultak = '1'
	BEGIN			
		SET @jogosultak_kezbesitesitetelek = ' (EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER IN (@ExecutorUserId,@FelhasznaloSzervezet_Id)
OR EREC_IraKezbesitesiTetelek.Csoport_Id_Cel IN (@ExecutorUserId,@FelhasznaloSzervezet_Id))
'
		
	END

	IF (@Where_PldIratPeldanyok != '')
	BEGIN
		IF @Jogosultak = '1' AND @isAdminInSzervezet = 0
		begin
		SET @sqlcmd = @sqlcmd + '
;with CsoportTagokAll as
(
	select Id from dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id)
),
jogosult_objektumok as
(
	select krt_jogosultak.Obj_Id from krt_jogosultak
		INNER JOIN CsoportTagokAll 
	ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
)
'
		end


		SET @sqlcmd = @sqlcmd + N' insert into #filter_pld select EREC_PldIratPeldanyok.Id
	from EREC_PldIratPeldanyok
	LEFT JOIN EREC_IraIratok
		ON EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id
	LEFT JOIN EREC_UgyUgyiratdarabok
		ON EREC_IraIratok.UgyUgyIratDarab_Id = EREC_UgyUgyiratdarabok.Id
	LEFT JOIN EREC_IraIktatoKonyvek
		ON EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id
	Where EREC_IraIktatoKonyvek.Org=@Org
	'
		set @sqlcmd = @sqlcmd + ' and ' 
		if (@isObjectMode = '0')
		begin
			set @sqlcmd = @sqlcmd + ' exists(select 1 from EREC_IraKezbesitesiTetelek where Obj_Id=EREC_PldIratPeldanyok.Id and Obj_type=''EREC_PldIratPeldanyok'' and ' + @jogosultak_kezbesitesitetelek
			IF (@Where <> '')
				set @sqlcmd = @sqlcmd + ' and ' + @Where
			set @sqlcmd = @sqlcmd + ') and '
		end
		set @sqlcmd = @sqlcmd + @Where_PldIratPeldanyok

		--IF @Jogosultak = '1'  AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
		IF @Jogosultak = '1' AND @isAdminInSzervezet = 0
		begin
--			SET @sqlcmd = @sqlcmd + ' and EREC_PldIratPeldanyok.Id in
--	(
--		SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
--			INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_PldIratPeldanyok.Id
--			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll
--				ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--		UNION ALL
--		SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
--			INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_PldIratPeldanyok.IraIrat_Id
--			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
--				ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--		UNION ALL
--		SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
--			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
--				ON CsoportTagokAll.Id = EREC_PldIratPeldanyok.Csoport_Id_Felelos
--		UNION ALL
--		SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
--			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
--				ON CsoportTagokAll.Id = EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo
--		UNION ALL
--		SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
--			INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
--			WHERE EREC_IraIratok.Ugyirat_Id IN 
--			(
--				SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
--					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_UgyUgyiratok.Id
--					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--				UNION ALL
--				SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
--					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_UgyUgyiratok.IraIktatokonyv_Id
--					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--				UNION ALL
--				SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
--					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
--						ON CsoportTagokAll.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez
--				UNION ALL
--				SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
--					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
--						ON CsoportTagokAll.Id = EREC_UgyUgyiratok.Csoport_Id_Felelos
--				UNION ALL
--				SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
--					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
--						ON CsoportTagokAll.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo
--			)
--	)'
			SET @sqlcmd = @sqlcmd + ' and EREC_PldIratPeldanyok.Id in
	(
	SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
		INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_PldIratPeldanyok.Id
	UNION ALL
	SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
		INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_PldIratPeldanyok.IraIrat_Id
	UNION ALL
	SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
		INNER JOIN CsoportTagokAll 
			ON CsoportTagokAll.Id = EREC_PldIratPeldanyok.Csoport_Id_Felelos
	UNION ALL
	SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
		INNER JOIN CsoportTagokAll 
			ON CsoportTagokAll.Id = EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo
	UNION ALL
	SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
		INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
		WHERE EREC_IraIratok.Ugyirat_Id IN 
		(
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = erec_ugyugyiratok.Id
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN CsoportTagokAll 
					ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN CsoportTagokAll 
					ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN CsoportTagokAll 
					ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
			UNION ALL
			SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'')
		)
	UNION ALL
	SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_PldIratPeldanyok'')
	UNION ALL
	SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
		INNER JOIN dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_IraIratok'') ids 
			ON ids.Id = EREC_PldIratPeldanyok.IraIrat_Id
)'
		end

		if (@isObjectMode = '1') and (@Where = '') and @KezbesitesiTetelekSearchMode = 'ObjectHasNoValidItem'
		begin 
			-- ha nincs Where feltétel és a kézbesítési tétel nélkülieket keressük
			set @sqlcmd = @sqlcmd + ' delete from #filter_pld
			where exists(select Id from EREC_IraKezbesitesiTetelek
				where EREC_IraKezbesitesiTetelek.Obj_Id = #filter_pld.Id and getdate() between EREC_IraKezbesitesiTetelek.ErvKezd and EREC_IraKezbesitesiTetelek.ErvVege)
'
		end
	END

	IF (@Where_KuldKuldemenyek != '')
	BEGIN
		IF @Jogosultak = '1' AND @isAdminInSzervezet = 0
		begin
		SET @sqlcmd = @sqlcmd + '
;with CsoportTagokAll as
(
	select Id from dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id)
),
jogosult_objektumok as
(
	select krt_jogosultak.Obj_Id from krt_jogosultak
		INNER JOIN CsoportTagokAll 
	ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
),
jogosult_ugyiratok as
(
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = erec_ugyugyiratok.Id
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN CsoportTagokAll 
			ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN CsoportTagokAll 
			ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN CsoportTagokAll 
			ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
	UNION ALL
	SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId,@FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'')
)
'
		end

		SET @sqlcmd = @sqlcmd + N' insert into #filter_kuld select EREC_KuldKuldemenyek.Id
	from EREC_KuldKuldemenyek
	LEFT JOIN EREC_IraIktatoKonyvek
		ON EREC_KuldKuldemenyek.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id
	Where EREC_IraIktatoKonyvek.Org=@Org
	'
		set @sqlcmd = @sqlcmd + ' and '
		if (@isObjectMode = '0')
		begin
			set @sqlcmd = @sqlcmd + ' exists(select 1 from EREC_IraKezbesitesiTetelek where Obj_Id=EREC_KuldKuldemenyek.Id and Obj_type=''EREC_KuldKuldemenyek'' and ' + @jogosultak_kezbesitesitetelek
			IF (@Where <> '')
				set @sqlcmd = @sqlcmd + ' and ' + @Where
			set @sqlcmd = @sqlcmd + ') and 
	'
		end
		set @sqlcmd = @sqlcmd + @Where_KuldKuldemenyek

		--IF @Jogosultak = '1'  AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
		IF @Jogosultak = '1' AND @isAdminInSzervezet = 0
		begin
--			SET @sqlcmd = @sqlcmd + ' and EREC_KuldKuldemenyek.Id in
--	(
--		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
--			INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.Id
--			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll
--				ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--		UNION ALL
--		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
--			INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
--			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll
--				ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--		UNION ALL
--		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
--			INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIratok_Id
--			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
--				ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--		UNION ALL
--		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
--			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
--				ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.Csoport_Id_Felelos
--		UNION ALL
--		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
--			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
--				ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
--		UNION ALL										
--		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
--			INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_KuldKuldemenyek.IraIratok_Id
--			WHERE EREC_KuldKuldemenyek.IraIratok_Id IS NOT NULL
--				AND EREC_IraIratok.Ugyirat_Id IN 
--			(
--				SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
--					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_UgyUgyiratok.Id
--					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--				UNION ALL
--				SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
--					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_UgyUgyiratok.IraIktatokonyv_Id
--					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--				UNION ALL
--				SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
--					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
--						ON CsoportTagokAll.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez
--				UNION ALL
--				SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
--					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
--						ON CsoportTagokAll.Id = EREC_UgyUgyiratok.Csoport_Id_Felelos
--				UNION ALL
--				SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
--					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
--						ON CsoportTagokAll.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo
--			)
--	)'

			SET @sqlcmd = @sqlcmd + ' and EREC_KuldKuldemenyek.Id in
	(
		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
			INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_KuldKuldemenyek.Id
		UNION ALL
		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
			INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
		UNION ALL
		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
			INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_KuldKuldemenyek.IraIratok_Id
		UNION ALL
		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
			INNER JOIN CsoportTagokAll 
				ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.Csoport_Id_Felelos
		UNION ALL
		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
			INNER JOIN CsoportTagokAll 
				ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
		UNION ALL										
		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
			INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_KuldKuldemenyek.IraIratok_Id
			WHERE EREC_KuldKuldemenyek.IraIratok_Id IS NOT NULL
				AND EREC_IraIratok.Ugyirat_Id IN 
			(
				SELECT Id FROM jogosult_ugyiratok
			)
	)'
		end

		if (@isObjectMode = '1') and (@Where = '') and @KezbesitesiTetelekSearchMode = 'ObjectHasNoValidItem'
		begin 
			-- ha nincs Where feltétel és a kézbesítési tétel nélkülieket keressük
			set @sqlcmd = @sqlcmd + ' delete from #filter_kuld
			where exists(select Id from EREC_IraKezbesitesiTetelek
				where EREC_IraKezbesitesiTetelek.Obj_Id = #filter_kuld.Id and getdate() between EREC_IraKezbesitesiTetelek.ErvKezd and EREC_IraKezbesitesiTetelek.ErvVege)
'
		end
	END

	IF (@Where_UgyUgyiratok != '')
	BEGIN

		IF @Jogosultak = '1' AND @isAdminInSzervezet = 0
		begin
		SET @sqlcmd = @sqlcmd + '
;with CsoportTagokAll as
(
	select Id from dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id)
),
jogosult_objektumok as
(
	select krt_jogosultak.Obj_Id from krt_jogosultak
		INNER JOIN CsoportTagokAll 
	ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
)
'
		end

		SET @sqlcmd = @sqlcmd + N' insert into #filter_ugyirat select EREC_UgyUgyiratok.Id
	from EREC_UgyUgyiratok
	LEFT JOIN EREC_IraIktatoKonyvek
		ON EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id
	Where EREC_IraIktatoKonyvek.Org=@Org
	'
		set @sqlcmd = @sqlcmd + ' and '
		if (@isObjectMode = '0')
		begin
			set @sqlcmd = @sqlcmd + ' exists(select 1 from EREC_IraKezbesitesiTetelek where Obj_Id=EREC_UgyUgyiratok.Id and Obj_type=''EREC_UgyUgyiratok'' and ' + @jogosultak_kezbesitesitetelek
			IF (@Where <> '')
				set @sqlcmd = @sqlcmd + ' and ' + @Where
			set @sqlcmd = @sqlcmd + ') and '
		end
		set @sqlcmd = @sqlcmd + @Where_UgyUgyiratok

		--IF @Jogosultak = '1' AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
		IF @Jogosultak = '1' AND @isAdminInSzervezet = 0
		begin
--			SET @sqlcmd = @sqlcmd + ' and EREC_UgyUgyiratok.Id in
--	(
--		SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
--			INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_UgyUgyiratok.Id
--			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--		UNION ALL
--		SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
--			INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_UgyUgyiratok.IraIktatokonyv_Id
--			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
--		UNION ALL
--		SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
--			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
--				ON CsoportTagokAll.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez
--		UNION ALL
--		SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
--			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
--				ON CsoportTagokAll.Id = EREC_UgyUgyiratok.Csoport_Id_Felelos
--		UNION ALL
--		SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
--			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
--				ON CsoportTagokAll.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo
--	)'

			SET @sqlcmd = @sqlcmd + ' and EREC_UgyUgyiratok.Id in
	(
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = erec_ugyugyiratok.Id
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN CsoportTagokAll 
			ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN CsoportTagokAll 
			ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN CsoportTagokAll 
			ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
	UNION ALL
	SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId,@FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'')
)
'
		end

		if (@isObjectMode = '1') and (@Where = '') and @KezbesitesiTetelekSearchMode = 'ObjectHasNoValidItem'
		begin 
			-- ha nincs Where feltétel és a kézbesítési tétel nélkülieket keressük
			set @sqlcmd = @sqlcmd + ' delete from #filter_ugyirat
			where exists(select Id from EREC_IraKezbesitesiTetelek
				where EREC_IraKezbesitesiTetelek.Obj_Id = #filter_ugyirat.Id and getdate() between EREC_IraKezbesitesiTetelek.ErvKezd and EREC_IraKezbesitesiTetelek.ErvVege)
'
		end
	
	END

	IF (@Where_Mappak != '')
	BEGIN
		SET @sqlcmd = @sqlcmd + N' insert into #filter_mappa select KRT_Mappak.Id
	from KRT_Mappak
	'
		set @sqlcmd = @sqlcmd + ' where '
		if (@isObjectMode = '0')
		begin
			set @sqlcmd = @sqlcmd + ' exists(select 1 from EREC_IraKezbesitesiTetelek where Obj_Id=KRT_Mappak.Id and Obj_type=''KRT_Mappak'' and ' + @jogosultak_kezbesitesitetelek
			IF (@Where <> '')
				set @sqlcmd = @sqlcmd + ' and ' + @Where
			set @sqlcmd = @sqlcmd + ') and '
		end
		set @sqlcmd = @sqlcmd + @Where_Mappak

		--IF @Jogosultak = '1' AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
		IF @Jogosultak = '1' AND @isAdminInSzervezet = 0
		begin
			SET @sqlcmd = @sqlcmd + ' and KRT_Mappak.Id in
	(
		SELECT KRT_Mappak.Id
		FROM KRT_Mappak
			INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
		WHERE krt_jogosultak.Csoport_Id_Jogalany IN (@ExecutorUserId,@FelhasznaloSzervezet_Id)
		UNION ALL
		SELECT KRT_Mappak.Id
		FROM KRT_Mappak
		WHERE KRT_Mappak.Csoport_Id_Tulaj IN (@ExecutorUserId,@FelhasznaloSzervezet_Id)
	)'
		end

		if (@isObjectMode = '1') and (@Where = '') and @KezbesitesiTetelekSearchMode = 'ObjectHasNoValidItem'
		begin 
			-- ha nincs Where feltétel és a kézbesítési tétel nélkülieket keressük
			set @sqlcmd = @sqlcmd + ' delete from #filter_mappa
			where exists(select Id from EREC_IraKezbesitesiTetelek
				where EREC_IraKezbesitesiTetelek.Obj_Id = #filter_mappa.Id and getdate() between EREC_IraKezbesitesiTetelek.ErvKezd and EREC_IraKezbesitesiTetelek.ErvVege)
'
		end
	END
	/************************************************************
	* Végső szűrőtábla összeállítása az eddigiek alapján        *
	************************************************************/
	SET @sqlcmd = @sqlcmd + N'
	create table #merged_objects (Id uniqueidentifier, Obj_Id uniqueidentifier, Obj_type nvarchar(100) collate Hungarian_CI_AS);
'

	if (@isObjectMode = '0')
	begin
		SET @sqlcmd = @sqlcmd + N'
	insert into #merged_objects select EREC_IraKezbesitesiTetelek.Id as Id, EREC_IraKezbesitesiTetelek.Obj_Id as Obj_Id, EREC_IraKezbesitesiTetelek.Obj_type as Obj_type from EREC_IraKezbesitesiTetelek
'
	if (@Where <> '')
		SET @sqlcmd = @sqlcmd + N' where ' + @Where

		if (@Where <> '')
			SET @sqlcmd = @sqlcmd + N' and '
		else
			SET @sqlcmd = @sqlcmd + N' where '

	SET @sqlcmd = @sqlcmd + @jogosultak_kezbesitesitetelek

	if IsNull(@Where_PldIratPeldanyok,'') <> '' or IsNull(@Where_KuldKuldemenyek,'') <> '' or IsNull(@Where_UgyUgyiratok,'') <> '' or IsNull(@Where_Mappak,'') <> ''
	begin
--		if (@Where <> '')
			SET @sqlcmd = @sqlcmd + N' and '
--		else
--			SET @sqlcmd = @sqlcmd + N' where '

		declare @filtered_obj_types nvarchar(150)
		set @filtered_obj_types = ''
		if IsNull(@Where_PldIratPeldanyok,'') <> ''
		begin
			SET @filtered_obj_types = @filtered_obj_types + N'''EREC_PldIratPeldanyok'''
		end

		if IsNull(@Where_KuldKuldemenyek,'') <> ''
		begin
			if @filtered_obj_types <> ''
				set @filtered_obj_types = @filtered_obj_types + ','
			SET @filtered_obj_types = @filtered_obj_types + N'''EREC_KuldKuldemenyek'''
		end

		if IsNull(@Where_UgyUgyiratok,'') <> ''
		begin
			if @filtered_obj_types <> ''
				set @filtered_obj_types = @filtered_obj_types + ','
			SET @filtered_obj_types = @filtered_obj_types + N'''EREC_UgyUgyiratok'''
		end

		if IsNull(@Where_Mappak,'') <> ''
		begin
			if @filtered_obj_types <> ''
				set @filtered_obj_types = @filtered_obj_types + ','
			SET @filtered_obj_types = @filtered_obj_types + N'''KRT_Mappak'''
		end


		SET @sqlcmd = @sqlcmd + N' (Obj_type not in (' + @filtered_obj_types + ') '

		declare @filtered_obj_ids nvarchar(200)
		set @filtered_obj_ids = ''

		if IsNull(@Where_PldIratPeldanyok,'') <> ''
		begin
			SET @filtered_obj_ids = @filtered_obj_ids + N'select Id from #filter_pld'
		end

		if IsNull(@Where_KuldKuldemenyek,'') <> ''
		begin
			if @filtered_obj_ids <> ''
				set @filtered_obj_ids = @filtered_obj_ids + ' UNION ALL '
			SET @filtered_obj_ids = @filtered_obj_ids + N'select Id from #filter_kuld'
		end

		if IsNull(@Where_UgyUgyiratok,'') <> ''
		begin
			if @filtered_obj_ids <> ''
				set @filtered_obj_ids = @filtered_obj_ids + ' UNION ALL '
			SET @filtered_obj_ids = @filtered_obj_ids + N'select Id from #filter_ugyirat'
		end

		if IsNull(@Where_Mappak,'') <> ''
		begin
			if @filtered_obj_ids <> ''
				set @filtered_obj_ids = @filtered_obj_ids + ' UNION ALL '
			SET @filtered_obj_ids = @filtered_obj_ids + N'select Id from #filter_mappa'
		end

		SET @sqlcmd = @sqlcmd + N' or EREC_IraKezbesitesitetelek.Obj_Id in (select Id from (' + @filtered_obj_ids + ') tmp_objids))
'
	end

	end
	else
	begin
		if (@Where <> '')
		begin
			SET @sqlcmd = @sqlcmd + N'
	insert into #merged_objects select EREC_IraKezbesitesiTetelek.Id as Id, EREC_IraKezbesitesiTetelek.Obj_Id as Obj_Id, EREC_IraKezbesitesiTetelek.Obj_type as Obj_type from (
	select #filter_kuld.Id as Obj_Id, ''EREC_KuldKuldemenyek'' as Obj_type from #filter_kuld
	union all
	select #filter_ugyirat.Id as Obj_Id, ''EREC_UgyUgyiratok'' as Obj_type from #filter_ugyirat
	union all
	select #filter_pld.Id as Obj_Id, ''EREC_PldIratPeldanyok'' as Obj_type from #filter_pld
	union all
	select #filter_mappa.Id as Obj_Id, ''KRT_Mappak'' as Obj_type from #filter_mappa
	) tmp
	INNER JOIN EREC_IraKezbesitesiTetelek ON EREC_IraKezbesitesiTetelek.Obj_type=tmp.Obj_type and EREC_IraKezbesitesiTetelek.Obj_Id=tmp.Obj_Id
 where ' + @Where

		end
-- az alábbiakban #filter helyett a kézbesítési tételeket kapcsoljuk, ha kell
		else if (@KezbesitesiTetelekSearchMode='LastValidIfExists')
		begin
			SET @sqlcmd = @sqlcmd + N'
	insert into #merged_objects
	select tmp.Id as Id, tmp.Obj_Id as Obj_Id, tmp.Obj_type as Obj_type from
	(
		select Rank() over (partition by tmp_objects.Obj_Id order by EREC_IraKezbesitesiTetelek.LetrehozasIdo DESC) as Rank, EREC_IraKezbesitesiTetelek.Id as Id, tmp_objects.Obj_Id as Obj_Id, tmp_objects.Obj_type as Obj_type from
		(
			select #filter_kuld.Id as Obj_Id, ''EREC_KuldKuldemenyek'' as Obj_type from #filter_kuld
			union all
			select #filter_ugyirat.Id as Obj_Id, ''EREC_UgyUgyiratok'' as Obj_type from #filter_ugyirat
			union all
			select #filter_pld.Id as Obj_Id, ''EREC_PldIratPeldanyok'' as Obj_type from #filter_pld
			union all
			select #filter_mappa.Id as Obj_Id, ''KRT_Mappak'' as Obj_type from #filter_mappa
		) tmp_objects
		left join EREC_IraKezbesitesiTetelek on EREC_IraKezbesitesiTetelek.Obj_Id=tmp_objects.Obj_Id and getdate() between EREC_IraKezbesitesiTetelek.ErvKezd and EREC_IraKezbesitesiTetelek.ErvVege
	) tmp
	where Rank=1
'
		end
		else if (@KezbesitesiTetelekSearchMode='AllValidIfExists')
		begin
			-- minden objektum, és minden hozzájuk tartozó kézbesítési tétel, ha van
			SET @sqlcmd = @sqlcmd + N'
	insert into #merged_objects
	select EREC_IraKezbesitesiTetelek.Id as Id, tmp_objects.Obj_Id as Obj_Id, tmp_objects.Obj_type as Obj_type from
	(
		(select #filter_kuld.Id as Obj_Id, ''EREC_KuldKuldemenyek'' as Obj_type from #filter_kuld
		union all
		select #filter_ugyirat.Id as Obj_Id, ''EREC_UgyUgyiratok'' as Obj_type from #filter_ugyirat
		union all
		select #filter_pld.Id as Obj_Id, ''EREC_PldIratPeldanyok'' as Obj_type from #filter_pld
		union all
		select #filter_mappa.Id as Obj_Id, ''KRT_Mappak'' as Obj_type from #filter_mappa
		) tmp_objects
		left join EREC_IraKezbesitesiTetelek on EREC_IraKezbesitesiTetelek.Obj_Id=tmp_objects.Obj_Id and EREC_IraKezbesitesiTetelek.Obj_type=tmp_objects.Obj_type and getdate() between EREC_IraKezbesitesiTetelek.ErvKezd and EREC_IraKezbesitesiTetelek.ErvVege
	)
'
		end
		else if (@KezbesitesiTetelekSearchMode='None')
		begin
			-- egyszerűen visszaadjuk az objektumokat
			SET @sqlcmd = @sqlcmd + N'
	insert into #merged_objects select null as Id, tmp.Obj_Id as Obj_Id, tmp.Obj_type as Obj_type from (
	select #filter_kuld.Id as Obj_Id, ''EREC_KuldKuldemenyek'' as Obj_type from #filter_kuld
	union all
	select #filter_ugyirat.Id as Obj_Id, ''EREC_UgyUgyiratok'' as Obj_type from #filter_ugyirat 
	union all
	select #filter_pld.Id as Obj_Id, ''EREC_PldIratPeldanyok'' as Obj_type from #filter_pld
	union all
	select #filter_mappa.Id as Obj_Id, ''KRT_Mappak'' as Obj_type from #filter_mappa
	) tmp
'
		end
		else if (@KezbesitesiTetelekSearchMode='ObjectHasNoValidItem')
		begin
			-- már korábban kiszűrtük a kézbesítési tétel nélkülieket, egyszerűen visszaadjuk az objektumokat
			SET @sqlcmd = @sqlcmd + N'
	insert into #merged_objects select null as Id, tmp.Obj_Id as Obj_Id, tmp.Obj_type as Obj_type from (
	select #filter_kuld.Id as Obj_Id, ''EREC_KuldKuldemenyek'' as Obj_type from #filter_kuld
	union all
	select #filter_ugyirat.Id as Obj_Id, ''EREC_UgyUgyiratok'' as Obj_type from #filter_ugyirat 
	union all
	select #filter_pld.Id as Obj_Id, ''EREC_PldIratPeldanyok'' as Obj_type from #filter_pld
	union all
	select #filter_mappa.Id as Obj_Id, ''KRT_Mappak'' as Obj_type from #filter_mappa
	) tmp
'
		end
	end


	/************************************************************
	* Szűrt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/
		
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber, Id, Obj_Id, Obj_type into #result from
(select	#merged_objects.Id as Id, #merged_objects.Obj_Id as Obj_Id, #merged_objects.Obj_type as Obj_type,
    Azonosito =
		CASE #merged_objects.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.Azonosito
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.Azonosito
            WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.Azonosito
			WHEN ''KRT_Mappak'' THEN KRT_Mappak.Nev
			ELSE EREC_IraKezbesitesiTetelek.Azonosito_szoveges
		END,
	Targy =
		CASE #merged_objects.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.Targy
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIratok_IratPld.Targy
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.Targy            
		ELSE ''''
	END,
	CONVERT(nvarchar(10), EREC_IraKezbesitesiTetelek.AtadasDat, 102) as AtadasDat,
	CONVERT(nvarchar(10), EREC_IraKezbesitesiTetelek.AtvetelDat, 102) as AtvetelDat,
	KRT_Csoportok_Atado.Nev as Atado_Nev,
	KRT_Csoportok_Cel.Nev as Cimzett_Nev,
	KRT_Csoportok_Atvevo.Nev as Atvevo_Nev,
	Barkod =
		CASE #merged_objects.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.BARCODE
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.BarCode
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.BarCode
			WHEN ''KRT_Mappak'' THEN KRT_Mappak.Barcode                
			ELSE coalesce(EREC_UgyUgyiratok_Ugyirat.barcode,EREC_PldIratPeldanyok_IratPld.barcode,EREC_KuldKuldemenyek_Kuld.barcode,KRT_Mappak.Barcode)
		END,
EREC_IraKezbesitesiTetelek.Allapot Allapot,
CONVERT(nvarchar(10), EREC_IraKezbesitesiTetelek.LetrehozasIdo, 102) as LetrehozasIdo
from #merged_objects
	left join EREC_IraKezbesitesiTetelek
		on #merged_objects.Id = EREC_IraKezbesitesiTetelek.Id and #merged_objects.Obj_type=EREC_IraKezbesitesiTetelek.Obj_type
	left join KRT_Csoportok as KRT_Csoportok_Atado
		ON EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER = KRT_Csoportok_Atado.Id
	left join KRT_Csoportok as KRT_Csoportok_Atvevo
		ON EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoUser = KRT_Csoportok_Atvevo.Id
	left join KRT_Csoportok as KRT_Csoportok_Cel
		ON EREC_IraKezbesitesiTetelek.Csoport_Id_Cel = KRT_Csoportok_Cel.Id 
	LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok_Ugyirat
		ON #merged_objects.Obj_Id = EREC_UgyUgyiratok_Ugyirat.Id and #merged_objects.Obj_type=''EREC_UgyUgyiratok''  
	LEFT JOIN EREC_PldIratPeldanyok as EREC_PldIratPeldanyok_IratPld
		ON #merged_objects.Obj_Id = EREC_PldIratPeldanyok_IratPld.Id and #merged_objects.Obj_type=''EREC_PldIratPeldanyok''
	LEFT JOIN EREC_IraIratok as EREC_IraIratok_IratPld
		ON EREC_PldIratPeldanyok_IratPld.IraIrat_Id = EREC_IraIratok_IratPld.Id
	LEFT JOIN EREC_KuldKuldemenyek as EREC_KuldKuldemenyek_Kuld
		ON #merged_objects.Obj_Id = EREC_KuldKuldemenyek_Kuld.Id and #merged_objects.Obj_type=''EREC_KuldKuldemenyek''
	LEFT JOIN KRT_Mappak on #merged_objects.Obj_Id = KRT_Mappak.Id and #merged_objects.Obj_type = ''KRT_Mappak''
) tmp
'

	if (@SelectedRowId is not null)
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id =@SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	--ELSE
		set @sqlcmd = @sqlcmd + N' 
			if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
			BEGIN
				select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
				set @firstRow = (@pageNumber - 1) * @pageSize + 1;
				set @lastRow = @pageNumber*@pageSize;
				select @pageNumber = @pageNumber - 1
			END ;'

	/************************************************************
	* Bizalmas iratok őrző szerinti szűréséhez csoporttagok lekérése*
	************************************************************/
	--IF dbo.fn_IsAdmin(@ExecutorUserId) = 0
	IF @isAdminInSzervezet = 0 AND @Jogosultak = '1'
	begin
--	set @sqlcmd = @sqlcmd + N'; declare @iratok_executor table (Id uniqueidentifier)
--insert into @iratok_executor select Id from dbo.fn_GetAllIrat_ExecutorIsOrzoOrHisLeader(
--			''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
--			''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
--'
	set @sqlcmd = @sqlcmd + N'; declare @iratok_executor table (Id uniqueidentifier primary key)
'
--insert into @iratok_executor select Id from dbo.fn_GetAllConfidentialIrat_ExecutorIsOrzoOrHisLeader(
--			''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
--			''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
--'
	set @sqlcmd = @sqlcmd + N'; declare @kuldemenyek_executor table (Id uniqueidentifier primary key)
'

	set @sqlcmd = @sqlcmd + N'
	DECLARE @confidential varchar(4000)
	set @confidential = (select top 1 Ertek from KRT_Parameterek where Nev=''IRAT_MINOSITES_BIZALMAS''
							and Org=@Org and getdate() between ErvKezd and ErvVege)

	declare @Bizalmas table (val nvarchar(64) collate Hungarian_CS_AS )
	insert into @Bizalmas (val) (select Value from fn_Split(@confidential, '',''))

	DECLARE @csoporttagsag_tipus nvarchar(64)
	SET @csoporttagsag_tipus = (select top 1 KRT_CsoportTagok.Tipus from KRT_CsoportTagok
						where KRT_CsoportTagok.Csoport_Id_Jogalany=@ExecutorUserId
						and KRT_CsoportTagok.Csoport_Id=@FelhasznaloSzervezet_Id
						and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
						)

	; CREATE TABLE #jogosultak (Id uniqueidentifier)
	IF @csoporttagsag_tipus = ''3''
	BEGIN
		INSERT INTO #jogosultak select KRT_Csoportok.Id from KRT_Csoportok
							inner join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id = @ExecutorUserId 
							and KRT_Csoportok.Tipus = ''1'' and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
							where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege 
							and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
	END
	ELSE
	BEGIN
		INSERT INTO #jogosultak select @ExecutorUserId
	END
'

	set @sqlcmd = @sqlcmd + N'
	insert into @kuldemenyek_executor select #result.Obj_Id
	from EREC_KuldKuldemenyek
	INNER JOIN #result on #result.Obj_Id=EREC_KuldKuldemenyek.Id and #result.Obj_type=''EREC_KuldKuldemenyek'' and #result.RowNumber between @firstRow and @lastRow
	where 1=case when EREC_KuldKuldemenyek.Minosites in (select val from @Bizalmas)
		AND NOT EXISTS
				(select 1
					from KRT_Jogosultak where
						KRT_Jogosultak.Obj_Id=#result.Obj_Id
						and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
						and @ExecutorUserId = KRT_Jogosultak.Csoport_Id_Jogalany)
		AND EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo NOT IN (select Id from #jogosultak)
	then 0
	else 1 end
group by #result.Obj_Id;
'

	set @sqlcmd = @sqlcmd + N'
	;WITH tmp_result as
	(
		select * from #result where #result.RowNumber between @firstRow and @lastRow
	)
	insert into @iratok_executor select EREC_IraIratok.Id
	from EREC_IraIratok where Id in
	(select EREC_IraIratok.Id from EREC_IraIratok
	INNER JOIN tmp_result on tmp_result.Obj_Id=EREC_IraIratok.Ugyirat_Id and tmp_result.Obj_type=''EREC_UgyUgyiratok''
	UNION ALL
	select EREC_IraIratok.Id from EREC_IraIratok
	INNER JOIN EREC_PldIratPeldanyok on EREC_IraIratok.Id=EREC_PldIratPeldanyok.IraIrat_Id
	INNER JOIN tmp_result on tmp_result.Obj_Id=EREC_PldIratPeldanyok.Id and tmp_result.Obj_type=''EREC_PldIratPeldanyok''
	UNION ALL
	select EREC_IraIratok.Id from EREC_IraIratok
	INNER JOIN tmp_result on tmp_result.Obj_Id=EREC_IraIratok.KuldKuldemenyek_Id and tmp_result.Obj_type=''EREC_KuldKuldemenyek''
	)
	and 1=case when EREC_IraIratok.Minosites in (select val from @Bizalmas)
		AND NOT EXISTS
				(select 1
					from KRT_Jogosultak where
						KRT_Jogosultak.Obj_Id=EREC_IraIratok.Id
						and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
						and @ExecutorUserId = KRT_Jogosultak.Csoport_Id_Jogalany)
		AND NOT EXISTS
			(select 1
				from EREC_PldIratPeldanyok
				where EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id
					and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo in (select Id from #jogosultak)
			)
	then 0
	else 1 end;

	drop table #jogosultak
'
	end
               
	/************************************************************
	* Tényleges select											*
	************************************************************/
		set @sqlcmd = @sqlcmd + N'
		select 
		#result.RowNumber,
		#result.Id,
   #result.Obj_Id,
    EREC_IraKezbesitesiTetelek.ObjTip_Id,
    #result.Obj_type,
    Azonosito =
		CASE #result.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.Azonosito
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.Azonosito
            WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.Azonosito
			WHEN ''KRT_Mappak'' THEN KRT_Mappak.Nev
			ELSE EREC_IraKezbesitesiTetelek.Azonosito_szoveges
		END,
'

set @sqlcmd = @sqlcmd + 
'	Targy =
		CASE #result.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.Targy
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIratok_IratPld.Targy
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.Targy            
		ELSE ''''
	END,
	Barkod =
		CASE #result.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.BARCODE
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.BarCode
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.BarCode
			WHEN ''KRT_Mappak'' THEN KRT_Mappak.Barcode                
			ELSE coalesce(EREC_UgyUgyiratok_Ugyirat.barcode,EREC_PldIratPeldanyok_IratPld.barcode,EREC_KuldKuldemenyek_Kuld.barcode,KRT_Mappak.Barcode)
		END,
	UgyiratAllapota = 
			EREC_UgyUgyiratok_Ugyirat.Allapot,
	UgyiratTovabbitasAlattiAllapota = 
			EREC_UgyUgyiratok_Ugyirat.TovabbitasAlattAllapot,
	IratAllapota = 
			(SELECT EREC_IraIratok.Allapot FROM EREC_IraIratok WHERE EREC_IraIratok.Id = EREC_PldIratPeldanyok_IratPld.IraIrat_Id),
	Irat_Ugyirat_Allapota = 
			(SELECT EREC_UgyUgyiratok.Allapot FROM EREC_UgyUgyiratok 
			JOIN EREC_IraIratok ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id
			WHERE EREC_IraIratok.Id = EREC_PldIratPeldanyok_IratPld.IraIrat_Id),
	Irat_Ugyirat_TovabbitasAlattiAllapota = 
			(SELECT EREC_UgyUgyiratok.TovabbitasAlattAllapot FROM EREC_UgyUgyiratok 
			JOIN EREC_IraIratok ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id
			WHERE EREC_IraIratok.Id = EREC_PldIratPeldanyok_IratPld.IraIrat_Id),
	IratpeldanyAllapota = 
			EREC_PldIratPeldanyok_IratPld.Allapot,
	IratpeldanyTovabbitasAlattiAllapota = 
			EREC_PldIratPeldanyok_IratPld.TovabbitasAlattAllapot,		
	
'


-- megvizsgáljuk, hogy az irat bizalmas-e, és ezesetben a felhasználó az őrző vagy az őrző vezetője-e
--if dbo.fn_IsAdmin(@ExecutorUserId) = 0 --AND @Jogosultak = '1' 
IF @isAdminInSzervezet = 0 AND @Jogosultak = '1'
begin
--	set @sqlcmd = @sqlcmd + '
--	CsatolmanyCount =
--		CASE #result.Obj_type
--			WHEN ''EREC_UgyUgyiratok'' THEN (SELECT COUNT(*) FROM EREC_IraIratok
--               INNER JOIN EREC_Csatolmanyok
--				ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
--				and (1=case when dbo.fn_CheckIrat_IsConfidential(EREC_IraIratok.Id) = 0 then 1
--				when EREC_IraIratok.Id in (select Id from @iratok_executor) then 1 else 0 end)
--				WHERE getdate() BETWEEN EREC_Csatolmanyok.ErvKezd AND EREC_Csatolmanyok.ErvVege
--				AND EREC_IraIratok.Ugyirat_Id =  EREC_IraKezbesitesiTetelek.Obj_Id
--	        )--dbo.fn_GetCsatolmanyCountNotConfidential(EREC_UgyUgyiratok_Ugyirat.Id,''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''')
--			WHEN ''EREC_PldIratPeldanyok'' THEN (SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok_IratPld.Id
--				and (1=case when dbo.fn_CheckIrat_IsConfidential(EREC_IraIratok_IratPld.Id) = 0 then 1
--				when EREC_IraIratok_IratPld.Id in (select Id from @iratok_executor) then 1 else 0 end)
--				AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
--			WHEN ''EREC_KuldKuldemenyek'' THEN (SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek_Kuld.Id
--				and (1=case when [EREC_Csatolmanyok].IraIrat_Id is null then 1
--				when dbo.fn_CheckIrat_IsConfidential([EREC_Csatolmanyok].IraIrat_Id) = 0 then 1
--				when [EREC_Csatolmanyok].IraIrat_Id in (select Id from @iratok_executor) then 1 else 0 end)
--			  AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
--	ELSE 0
--	END,
--'

	set @sqlcmd = @sqlcmd + '
	CsatolmanyCount =
		CASE #result.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN ugyiratcsat.cnt
			WHEN ''EREC_PldIratPeldanyok'' THEN pldcsat.cnt
			WHEN ''EREC_KuldKuldemenyek'' THEN kuldcsat.cnt
	ELSE 0
	END,
'

	set @sqlcmd = @sqlcmd + '
	Dokumentum_Id =
		CASE #result.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN (case ugyiratcsat.cnt
				when 1 then 
				(SELECT top 1 EREC_Csatolmanyok.Dokumentum_Id
				FROM EREC_Csatolmanyok
				INNER JOIN EREC_IraIratok
				ON [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id
				WHERE EREC_IraIratok.Ugyirat_Id = EREC_IraKezbesitesiTetelek.Obj_Id
				AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
				)
				else cast(null as uniqueidentifier) end
	        )
			WHEN ''EREC_PldIratPeldanyok'' THEN (case pldcsat.cnt
				when 1 then
				(SELECT  top 1 EREC_Csatolmanyok.Dokumentum_Id
				FROM EREC_Csatolmanyok
				 WHERE [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok_IratPld.Id
				AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
				)
				else cast(null as uniqueidentifier) end
			)
			WHEN ''EREC_KuldKuldemenyek'' THEN (case kuldcsat.cnt
				when 1 then
				(SELECT top 1 EREC_Csatolmanyok.Dokumentum_Id FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek_Kuld.Id
						AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
				else cast(null as uniqueidentifier) end
			)
	ELSE cast(null as uniqueidentifier)
	END,
'
end
else
begin
	set @sqlcmd = @sqlcmd + '
	CsatolmanyCount =
		CASE #result.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN ugyiratcsat.cnt
			WHEN ''EREC_PldIratPeldanyok'' THEN pldcsat.cnt
			WHEN ''EREC_KuldKuldemenyek'' THEN kuldcsat.cnt
		ELSE 0
	END,
'

	set @sqlcmd = @sqlcmd + '
	Dokumentum_Id =
		CASE #result.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN (case ugyiratcsat.cnt
				when 1 then 
				(SELECT  top 1 EREC_Csatolmanyok.Dokumentum_Id
				FROM EREC_IraIratok
							   INNER JOIN EREC_Csatolmanyok
								ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
								WHERE getdate() BETWEEN EREC_Csatolmanyok.ErvKezd AND EREC_Csatolmanyok.ErvVege
								AND EREC_IraIratok.Ugyirat_Id =  EREC_UgyUgyiratok_Ugyirat.Id
				)
				else null end
			)
			WHEN ''EREC_PldIratPeldanyok'' THEN (case pldcsat.cnt
				when 1 then
				(SELECT  top 1 EREC_Csatolmanyok.Dokumentum_Id
				FROM EREC_Csatolmanyok
								WHERE EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok_IratPld.Id
								and getdate() BETWEEN EREC_Csatolmanyok.ErvKezd AND EREC_Csatolmanyok.ErvVege
				)
				else null end
			)
			WHEN ''EREC_KuldKuldemenyek'' THEN (case kuldcsat.cnt
				when 1 then
				(SELECT  top 1 EREC_Csatolmanyok.Dokumentum_Id FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek_Kuld.Id AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
				else null end
		)
		ELSE null
	END,
'
end

-- a max 4000 karakter miatt ketszer kell osszefuzni
SET @sqlcmd = @sqlcmd + '
	UgyintezesModja =
		CASE #result.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.UgyintezesModja
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.UgyintezesModja
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.UgyintezesModja
			ELSE ''''
		END,'

-- a max 4000 karakter miatt ketszer kell osszefuzni
SET @sqlcmd = @sqlcmd + '
	CONVERT(nvarchar(10), EREC_IraKezbesitesiTetelek.AtadasDat, 102) as AtadasDat,
	CONVERT(nvarchar(10), EREC_IraKezbesitesiTetelek.AtvetelDat, 102) as AtvetelDat,
	   EREC_IraKezbesitesiTetelek.Megjegyzes,
	   EREC_IraKezbesitesiTetelek.SztornirozasDat,
	   EREC_IraKezbesitesiTetelek.UtolsoNyomtatas_Id,
	   EREC_IraKezbesitesiTetelek.UtolsoNyomtatasIdo,
	KRT_Csoportok_Atado.Nev as Atado_Nev,
	   EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER,
	   EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_LOGIN,
	KRT_Csoportok_Cel.Nev as Cimzett_Nev,
	   EREC_IraKezbesitesiTetelek.Csoport_Id_Cel,
	KRT_Csoportok_Atvevo.Nev as Atvevo_Nev,
	   EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoUser,
	   EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoLogin,
	   EREC_IraKezbesitesiTetelek.Allapot,
	   EREC_IraKezbesitesiTetelek.Azonosito_szoveges,
coalesce(EREC_UgyUgyiratok_Ugyirat.barcode,EREC_PldIratPeldanyok_IratPld.barcode,EREC_KuldKuldemenyek_Kuld.barcode,KRT_Mappak.Barcode) as Barcode,
	   dbo.fn_GetEREC_HataridosFeladatokCount(EREC_IraKezbesitesiTetelek.Obj_Id,@ExecutorUserId, @FelhasznaloSzervezet_Id) as FeladatCount,
	   EREC_IraKezbesitesiTetelek.Ver,
	   EREC_IraKezbesitesiTetelek.Note,
	   EREC_IraKezbesitesiTetelek.Stat_id,
	   EREC_IraKezbesitesiTetelek.ErvKezd,
	   EREC_IraKezbesitesiTetelek.ErvVege,
	   EREC_IraKezbesitesiTetelek.Letrehozo_id,
	CONVERT(nvarchar(10), EREC_IraKezbesitesiTetelek.LetrehozasIdo, 102) as LetrehozasIdo,
	   EREC_IraKezbesitesiTetelek.Modosito_id,
	   EREC_IraKezbesitesiTetelek.ModositasIdo,
	   EREC_IraKezbesitesiTetelek.Zarolo_id,
	   EREC_IraKezbesitesiTetelek.ZarolasIdo,
	   EREC_IraKezbesitesiTetelek.Tranz_id,
	   EREC_IraKezbesitesiTetelek.UIAccessLog_id'
	   
    IF @OrgIsBOMPH = 1
		set @sqlcmd = @sqlcmd + '
			  ,(select top 1 dbo.fn_KodtarErtekNeve(''FELADAT_ALTIPUS'', EREC_HataridosFeladatok.Altipus, @Org)
		  from EREC_HataridosFeladatok where EREC_HataridosFeladatok.Obj_Id = EREC_IraKezbesitesiTetelek.Obj_Id
		  and EREC_HataridosFeladatok.Tipus = ''M''
		  order by EREC_HataridosFeladatok.LetrehozasIdo desc) as KezelesTipusNev'
	ELSE
		set @sqlcmd = @sqlcmd + '
			  ,null as KezelesTipusNev'
		  
   set @sqlcmd = @sqlcmd + '
   from #result '

-- a max 4000 karakter miatt ketszer kell osszefuzni
SET @sqlcmd = @sqlcmd + '
	left join EREC_IraKezbesitesiTetelek as EREC_IraKezbesitesiTetelek
		on #result.Id = EREC_IraKezbesitesiTetelek.Id
	left join KRT_Csoportok as KRT_Csoportok_Atado
		ON EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER = KRT_Csoportok_Atado.Id
	left join KRT_Csoportok as KRT_Csoportok_Atvevo
		ON EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoUser = KRT_Csoportok_Atvevo.Id
	left join KRT_Csoportok as KRT_Csoportok_Cel
		ON EREC_IraKezbesitesiTetelek.Csoport_Id_Cel = KRT_Csoportok_Cel.Id     
	LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok_Ugyirat
		ON #result.Obj_Id = EREC_UgyUgyiratok_Ugyirat.Id and #result.Obj_type=''EREC_UgyUgyiratok''  
	LEFT JOIN EREC_PldIratPeldanyok as EREC_PldIratPeldanyok_IratPld
		ON #result.Obj_Id = EREC_PldIratPeldanyok_IratPld.Id and #result.Obj_type=''EREC_PldIratPeldanyok''
	LEFT JOIN EREC_IraIratok as EREC_IraIratok_IratPld
		ON EREC_PldIratPeldanyok_IratPld.IraIrat_Id = EREC_IraIratok_IratPld.Id
	LEFT JOIN EREC_KuldKuldemenyek as EREC_KuldKuldemenyek_Kuld
		ON #result.Obj_Id = EREC_KuldKuldemenyek_Kuld.Id and #result.Obj_type=''EREC_KuldKuldemenyek''
	left join KRT_Mappak ON KRT_Mappak.Id = #result.Obj_Id and #result.Obj_type=''KRT_Mappak''
 '

if @isAdminInSzervezet = 0 AND @Jogosultak = '1'
begin
	SET @sqlcmd = @sqlcmd + '
CROSS APPLY (SELECT COUNT(*) cnt FROM [EREC_Csatolmanyok] INNER JOIN @iratok_executor ie
				ON ie.Id= EREC_Csatolmanyok.IraIrat_Id
				INNER JOIN EREC_IraIratok ON [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id
				AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
				where EREC_IraIratok.Ugyirat_Id = EREC_IraKezbesitesiTetelek.Obj_Id)
	ugyiratcsat
CROSS APPLY (SELECT COUNT(*) cnt FROM [EREC_Csatolmanyok] INNER JOIN @iratok_executor ie
				ON ie.Id= EREC_Csatolmanyok.IraIrat_Id
				WHERE [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok_IratPld.Id
				AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
	pldcsat
CROSS APPLY (select (SELECT COUNT(*) FROM [EREC_Csatolmanyok] INNER JOIN @kuldemenyek_executor ke
				ON ke.Id=[EREC_Csatolmanyok].KuldKuldemeny_Id and [EREC_Csatolmanyok].IraIrat_Id is null
				WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek_Kuld.Id
				AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
				+
				(SELECT COUNT(*) FROM [EREC_Csatolmanyok] INNER JOIN @iratok_executor ie
				ON ie.Id=[EREC_Csatolmanyok].IraIrat_Id
				WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek_Kuld.Id
				AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
				cnt)
	kuldcsat
'
end
else
begin
	SET @sqlcmd = @sqlcmd + '
CROSS APPLY (SELECT COUNT(*) cnt FROM EREC_IraIratok
               INNER JOIN EREC_Csatolmanyok
				ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
				WHERE getdate() BETWEEN EREC_Csatolmanyok.ErvKezd AND EREC_Csatolmanyok.ErvVege
				AND EREC_IraIratok.Ugyirat_Id = EREC_UgyUgyiratok_Ugyirat.Id)
	ugyiratcsat
CROSS APPLY (SELECT COUNT(*) cnt FROM [EREC_Csatolmanyok]
		WHERE [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok_IratPld.Id
		AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
	pldcsat
CROSS APPLY (SELECT COUNT(*) cnt FROM [EREC_Csatolmanyok]
	WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek_Kuld.Id
	AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
	kuldcsat
'
end
	SET @sqlcmd = @sqlcmd + '
    where RowNumber between @firstRow and @lastRow
	ORDER BY #result.RowNumber;'

		-- találatok száma és oldalszám
		set @sqlcmd = @sqlcmd + N' select count(*) as RecordNumber, @pageNumber as PageNumber from #result;';
---- nyomtatási blokk kezdete
--   if exists (select 1 from  sysobjects where id = object_id('sp_PrintText') and type = 'P')
--   begin
--       exec dbo.sp_PrintText @sqlcmd;
--   end
---- nyomtatási blokk vége
		execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @ExecutorUserId uniqueidentifier, @FelhasznaloSzervezet_Id uniqueidentifier, @SelectedRowId uniqueidentifier, @Org uniqueidentifier'
			,@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @ExecutorUserId = @ExecutorUserId, @FelhasznaloSzervezet_Id = @FelhasznaloSzervezet_Id, @SelectedRowId = @SelectedRowId, @Org = @Org;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

