IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraKezbesitesiTetelekHistoryGetRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraKezbesitesiTetelekHistoryGetRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraKezbesitesiTetelekHistoryGetRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraKezbesitesiTetelekHistoryGetRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraKezbesitesiTetelekHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_IraKezbesitesiTetelekHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_IraKezbesitesiTetelekHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KezbesitesFej_Id' as ColumnName,               
               cast(Old.KezbesitesFej_Id as nvarchar(99)) as OldValue,
               cast(New.KezbesitesFej_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KezbesitesFej_Id != New.KezbesitesFej_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AtveteliFej_Id' as ColumnName,               
               cast(Old.AtveteliFej_Id as nvarchar(99)) as OldValue,
               cast(New.AtveteliFej_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AtveteliFej_Id != New.AtveteliFej_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Id' as ColumnName,               
               cast(Old.Obj_Id as nvarchar(99)) as OldValue,
               cast(New.Obj_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_Id != New.Obj_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ObjTip_Id' as ColumnName,               
               cast(Old.ObjTip_Id as nvarchar(99)) as OldValue,
               cast(New.ObjTip_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ObjTip_Id != New.ObjTip_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_type' as ColumnName,               
               cast(Old.Obj_type as nvarchar(99)) as OldValue,
               cast(New.Obj_type as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_type != New.Obj_type 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AtadasDat' as ColumnName,               
               cast(Old.AtadasDat as nvarchar(99)) as OldValue,
               cast(New.AtadasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AtadasDat != New.AtadasDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AtvetelDat' as ColumnName,               
               cast(Old.AtvetelDat as nvarchar(99)) as OldValue,
               cast(New.AtvetelDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AtvetelDat != New.AtvetelDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Megjegyzes' as ColumnName,               
               cast(Old.Megjegyzes as nvarchar(99)) as OldValue,
               cast(New.Megjegyzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Megjegyzes != New.Megjegyzes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SztornirozasDat' as ColumnName,               
               cast(Old.SztornirozasDat as nvarchar(99)) as OldValue,
               cast(New.SztornirozasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SztornirozasDat != New.SztornirozasDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UtolsoNyomtatas_Id' as ColumnName,               
               cast(Old.UtolsoNyomtatas_Id as nvarchar(99)) as OldValue,
               cast(New.UtolsoNyomtatas_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UtolsoNyomtatas_Id != New.UtolsoNyomtatas_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UtolsoNyomtatasIdo' as ColumnName,               
               cast(Old.UtolsoNyomtatasIdo as nvarchar(99)) as OldValue,
               cast(New.UtolsoNyomtatasIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UtolsoNyomtatasIdo != New.UtolsoNyomtatasIdo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felhasznalo_Id_Atado_USER' as ColumnName,               
               cast(Old.Felhasznalo_Id_Atado_USER as nvarchar(99)) as OldValue,
               cast(New.Felhasznalo_Id_Atado_USER as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Felhasznalo_Id_Atado_USER != New.Felhasznalo_Id_Atado_USER 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felhasznalo_Id_Atado_LOGIN' as ColumnName,               
               cast(Old.Felhasznalo_Id_Atado_LOGIN as nvarchar(99)) as OldValue,
               cast(New.Felhasznalo_Id_Atado_LOGIN as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Felhasznalo_Id_Atado_LOGIN != New.Felhasznalo_Id_Atado_LOGIN 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Cel' as ColumnName,               
               cast(Old.Csoport_Id_Cel as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Cel as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Cel != New.Csoport_Id_Cel 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felhasznalo_Id_AtvevoUser' as ColumnName,               
               cast(Old.Felhasznalo_Id_AtvevoUser as nvarchar(99)) as OldValue,
               cast(New.Felhasznalo_Id_AtvevoUser as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Felhasznalo_Id_AtvevoUser != New.Felhasznalo_Id_AtvevoUser 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felhasznalo_Id_AtvevoLogin' as ColumnName,               
               cast(Old.Felhasznalo_Id_AtvevoLogin as nvarchar(99)) as OldValue,
               cast(New.Felhasznalo_Id_AtvevoLogin as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Felhasznalo_Id_AtvevoLogin != New.Felhasznalo_Id_AtvevoLogin 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Allapot != New.Allapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'KEZBESITESITETEL_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Allapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Allapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Azonosito_szoveges' as ColumnName,               
               cast(Old.Azonosito_szoveges as nvarchar(99)) as OldValue,
               cast(New.Azonosito_szoveges as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Azonosito_szoveges != New.Azonosito_szoveges 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesModja' as ColumnName,               
               cast(Old.UgyintezesModja as nvarchar(99)) as OldValue,
               cast(New.UgyintezesModja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraKezbesitesiTetelekHistory Old
         inner join EREC_IraKezbesitesiTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyintezesModja != New.UgyintezesModja 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end


GO
