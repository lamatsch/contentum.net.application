IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraKezbesitesiTetelekInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraKezbesitesiTetelekInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraKezbesitesiTetelekInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraKezbesitesiTetelekInsert] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraKezbesitesiTetelekInsert]    
                @Id      uniqueidentifier = null,    
                               @KezbesitesFej_Id     uniqueidentifier  = null,
                @AtveteliFej_Id     uniqueidentifier  = null,
                @Obj_Id     uniqueidentifier  = null,
                @ObjTip_Id     uniqueidentifier  = null,
                @Obj_type     Nvarchar(100)  = null,
                @AtadasDat     datetime  = null,
                @AtvetelDat     datetime  = null,
                @Megjegyzes     Nvarchar(400)  = null,
                @SztornirozasDat     datetime  = null,
                @UtolsoNyomtatas_Id     uniqueidentifier  = null,
                @UtolsoNyomtatasIdo     datetime  = null,
                @Felhasznalo_Id_Atado_USER     uniqueidentifier  = null,
                @Felhasznalo_Id_Atado_LOGIN     uniqueidentifier  = null,
                @Csoport_Id_Cel     uniqueidentifier  = null,
                @Felhasznalo_Id_AtvevoUser     uniqueidentifier  = null,
                @Felhasznalo_Id_AtvevoLogin     uniqueidentifier  = null,
                @Allapot     char(1)  = null,
                @Azonosito_szoveges     Nvarchar(100)  = null,
                @UgyintezesModja     nvarchar(64)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @KezbesitesFej_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',KezbesitesFej_Id'
            SET @insertValues = @insertValues + ',@KezbesitesFej_Id'
         end 
       
         if @AtveteliFej_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',AtveteliFej_Id'
            SET @insertValues = @insertValues + ',@AtveteliFej_Id'
         end 
       
         if @Obj_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Obj_Id'
            SET @insertValues = @insertValues + ',@Obj_Id'
         end 
       
         if @ObjTip_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',ObjTip_Id'
            SET @insertValues = @insertValues + ',@ObjTip_Id'
         end 
       
         if @Obj_type is not null
         begin
            SET @insertColumns = @insertColumns + ',Obj_type'
            SET @insertValues = @insertValues + ',@Obj_type'
         end 
       
         if @AtadasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',AtadasDat'
            SET @insertValues = @insertValues + ',@AtadasDat'
         end 
       
         if @AtvetelDat is not null
         begin
            SET @insertColumns = @insertColumns + ',AtvetelDat'
            SET @insertValues = @insertValues + ',@AtvetelDat'
         end 
       
         if @Megjegyzes is not null
         begin
            SET @insertColumns = @insertColumns + ',Megjegyzes'
            SET @insertValues = @insertValues + ',@Megjegyzes'
         end 
       
         if @SztornirozasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SztornirozasDat'
            SET @insertValues = @insertValues + ',@SztornirozasDat'
         end 
       
         if @UtolsoNyomtatas_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',UtolsoNyomtatas_Id'
            SET @insertValues = @insertValues + ',@UtolsoNyomtatas_Id'
         end 
       
         if @UtolsoNyomtatasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',UtolsoNyomtatasIdo'
            SET @insertValues = @insertValues + ',@UtolsoNyomtatasIdo'
         end 
       
         if @Felhasznalo_Id_Atado_USER is not null
         begin
            SET @insertColumns = @insertColumns + ',Felhasznalo_Id_Atado_USER'
            SET @insertValues = @insertValues + ',@Felhasznalo_Id_Atado_USER'
         end 
       
         if @Felhasznalo_Id_Atado_LOGIN is not null
         begin
            SET @insertColumns = @insertColumns + ',Felhasznalo_Id_Atado_LOGIN'
            SET @insertValues = @insertValues + ',@Felhasznalo_Id_Atado_LOGIN'
         end 
       
         if @Csoport_Id_Cel is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Cel'
            SET @insertValues = @insertValues + ',@Csoport_Id_Cel'
         end 
       
         if @Felhasznalo_Id_AtvevoUser is not null
         begin
            SET @insertColumns = @insertColumns + ',Felhasznalo_Id_AtvevoUser'
            SET @insertValues = @insertValues + ',@Felhasznalo_Id_AtvevoUser'
         end 
       
         if @Felhasznalo_Id_AtvevoLogin is not null
         begin
            SET @insertColumns = @insertColumns + ',Felhasznalo_Id_AtvevoLogin'
            SET @insertValues = @insertValues + ',@Felhasznalo_Id_AtvevoLogin'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @Azonosito_szoveges is not null
         begin
            SET @insertColumns = @insertColumns + ',Azonosito_szoveges'
            SET @insertValues = @insertValues + ',@Azonosito_szoveges'
         end 
       
         if @UgyintezesModja is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyintezesModja'
            SET @insertValues = @insertValues + ',@UgyintezesModja'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_IraKezbesitesiTetelek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@KezbesitesFej_Id uniqueidentifier,@AtveteliFej_Id uniqueidentifier,@Obj_Id uniqueidentifier,@ObjTip_Id uniqueidentifier,@Obj_type Nvarchar(100),@AtadasDat datetime,@AtvetelDat datetime,@Megjegyzes Nvarchar(400),@SztornirozasDat datetime,@UtolsoNyomtatas_Id uniqueidentifier,@UtolsoNyomtatasIdo datetime,@Felhasznalo_Id_Atado_USER uniqueidentifier,@Felhasznalo_Id_Atado_LOGIN uniqueidentifier,@Csoport_Id_Cel uniqueidentifier,@Felhasznalo_Id_AtvevoUser uniqueidentifier,@Felhasznalo_Id_AtvevoLogin uniqueidentifier,@Allapot char(1),@Azonosito_szoveges Nvarchar(100),@UgyintezesModja nvarchar(64),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@KezbesitesFej_Id = @KezbesitesFej_Id,@AtveteliFej_Id = @AtveteliFej_Id,@Obj_Id = @Obj_Id,@ObjTip_Id = @ObjTip_Id,@Obj_type = @Obj_type,@AtadasDat = @AtadasDat,@AtvetelDat = @AtvetelDat,@Megjegyzes = @Megjegyzes,@SztornirozasDat = @SztornirozasDat,@UtolsoNyomtatas_Id = @UtolsoNyomtatas_Id,@UtolsoNyomtatasIdo = @UtolsoNyomtatasIdo,@Felhasznalo_Id_Atado_USER = @Felhasznalo_Id_Atado_USER,@Felhasznalo_Id_Atado_LOGIN = @Felhasznalo_Id_Atado_LOGIN,@Csoport_Id_Cel = @Csoport_Id_Cel,@Felhasznalo_Id_AtvevoUser = @Felhasznalo_Id_AtvevoUser,@Felhasznalo_Id_AtvevoLogin = @Felhasznalo_Id_AtvevoLogin,@Allapot = @Allapot,@Azonosito_szoveges = @Azonosito_szoveges,@UgyintezesModja = @UgyintezesModja,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_IraKezbesitesiTetelek',@ResultUid
					,'EREC_IraKezbesitesiTetelekHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
