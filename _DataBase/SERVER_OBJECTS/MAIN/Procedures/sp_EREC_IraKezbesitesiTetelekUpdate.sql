IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraKezbesitesiTetelekUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraKezbesitesiTetelekUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraKezbesitesiTetelekUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraKezbesitesiTetelekUpdate] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraKezbesitesiTetelekUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @KezbesitesFej_Id     uniqueidentifier  = null,         
             @AtveteliFej_Id     uniqueidentifier  = null,         
             @Obj_Id     uniqueidentifier  = null,         
             @ObjTip_Id     uniqueidentifier  = null,         
             @Obj_type     Nvarchar(100)  = null,         
             @AtadasDat     datetime  = null,         
             @AtvetelDat     datetime  = null,         
             @Megjegyzes     Nvarchar(400)  = null,         
             @SztornirozasDat     datetime  = null,         
             @UtolsoNyomtatas_Id     uniqueidentifier  = null,         
             @UtolsoNyomtatasIdo     datetime  = null,         
             @Felhasznalo_Id_Atado_USER     uniqueidentifier  = null,         
             @Felhasznalo_Id_Atado_LOGIN     uniqueidentifier  = null,         
             @Csoport_Id_Cel     uniqueidentifier  = null,         
             @Felhasznalo_Id_AtvevoUser     uniqueidentifier  = null,         
             @Felhasznalo_Id_AtvevoLogin     uniqueidentifier  = null,         
             @Allapot     char(1)  = null,         
             @Azonosito_szoveges     Nvarchar(100)  = null,         
             @UgyintezesModja     nvarchar(64)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_KezbesitesFej_Id     uniqueidentifier         
     DECLARE @Act_AtveteliFej_Id     uniqueidentifier         
     DECLARE @Act_Obj_Id     uniqueidentifier         
     DECLARE @Act_ObjTip_Id     uniqueidentifier         
     DECLARE @Act_Obj_type     Nvarchar(100)         
     DECLARE @Act_AtadasDat     datetime         
     DECLARE @Act_AtvetelDat     datetime         
     DECLARE @Act_Megjegyzes     Nvarchar(400)         
     DECLARE @Act_SztornirozasDat     datetime         
     DECLARE @Act_UtolsoNyomtatas_Id     uniqueidentifier         
     DECLARE @Act_UtolsoNyomtatasIdo     datetime         
     DECLARE @Act_Felhasznalo_Id_Atado_USER     uniqueidentifier         
     DECLARE @Act_Felhasznalo_Id_Atado_LOGIN     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Cel     uniqueidentifier         
     DECLARE @Act_Felhasznalo_Id_AtvevoUser     uniqueidentifier         
     DECLARE @Act_Felhasznalo_Id_AtvevoLogin     uniqueidentifier         
     DECLARE @Act_Allapot     char(1)         
     DECLARE @Act_Azonosito_szoveges     Nvarchar(100)         
     DECLARE @Act_UgyintezesModja     nvarchar(64)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_KezbesitesFej_Id = KezbesitesFej_Id,
     @Act_AtveteliFej_Id = AtveteliFej_Id,
     @Act_Obj_Id = Obj_Id,
     @Act_ObjTip_Id = ObjTip_Id,
     @Act_Obj_type = Obj_type,
     @Act_AtadasDat = AtadasDat,
     @Act_AtvetelDat = AtvetelDat,
     @Act_Megjegyzes = Megjegyzes,
     @Act_SztornirozasDat = SztornirozasDat,
     @Act_UtolsoNyomtatas_Id = UtolsoNyomtatas_Id,
     @Act_UtolsoNyomtatasIdo = UtolsoNyomtatasIdo,
     @Act_Felhasznalo_Id_Atado_USER = Felhasznalo_Id_Atado_USER,
     @Act_Felhasznalo_Id_Atado_LOGIN = Felhasznalo_Id_Atado_LOGIN,
     @Act_Csoport_Id_Cel = Csoport_Id_Cel,
     @Act_Felhasznalo_Id_AtvevoUser = Felhasznalo_Id_AtvevoUser,
     @Act_Felhasznalo_Id_AtvevoLogin = Felhasznalo_Id_AtvevoLogin,
     @Act_Allapot = Allapot,
     @Act_Azonosito_szoveges = Azonosito_szoveges,
     @Act_UgyintezesModja = UgyintezesModja,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_IraKezbesitesiTetelek
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/KezbesitesFej_Id')=1
         SET @Act_KezbesitesFej_Id = @KezbesitesFej_Id
   IF @UpdatedColumns.exist('/root/AtveteliFej_Id')=1
         SET @Act_AtveteliFej_Id = @AtveteliFej_Id
   IF @UpdatedColumns.exist('/root/Obj_Id')=1
         SET @Act_Obj_Id = @Obj_Id
   IF @UpdatedColumns.exist('/root/ObjTip_Id')=1
         SET @Act_ObjTip_Id = @ObjTip_Id
   IF @UpdatedColumns.exist('/root/Obj_type')=1
         SET @Act_Obj_type = @Obj_type
   IF @UpdatedColumns.exist('/root/AtadasDat')=1
         SET @Act_AtadasDat = @AtadasDat
   IF @UpdatedColumns.exist('/root/AtvetelDat')=1
         SET @Act_AtvetelDat = @AtvetelDat
   IF @UpdatedColumns.exist('/root/Megjegyzes')=1
         SET @Act_Megjegyzes = @Megjegyzes
   IF @UpdatedColumns.exist('/root/SztornirozasDat')=1
         SET @Act_SztornirozasDat = @SztornirozasDat
   IF @UpdatedColumns.exist('/root/UtolsoNyomtatas_Id')=1
         SET @Act_UtolsoNyomtatas_Id = @UtolsoNyomtatas_Id
   IF @UpdatedColumns.exist('/root/UtolsoNyomtatasIdo')=1
         SET @Act_UtolsoNyomtatasIdo = @UtolsoNyomtatasIdo
   IF @UpdatedColumns.exist('/root/Felhasznalo_Id_Atado_USER')=1
         SET @Act_Felhasznalo_Id_Atado_USER = @Felhasznalo_Id_Atado_USER
   IF @UpdatedColumns.exist('/root/Felhasznalo_Id_Atado_LOGIN')=1
         SET @Act_Felhasznalo_Id_Atado_LOGIN = @Felhasznalo_Id_Atado_LOGIN
   IF @UpdatedColumns.exist('/root/Csoport_Id_Cel')=1
         SET @Act_Csoport_Id_Cel = @Csoport_Id_Cel
   IF @UpdatedColumns.exist('/root/Felhasznalo_Id_AtvevoUser')=1
         SET @Act_Felhasznalo_Id_AtvevoUser = @Felhasznalo_Id_AtvevoUser
   IF @UpdatedColumns.exist('/root/Felhasznalo_Id_AtvevoLogin')=1
         SET @Act_Felhasznalo_Id_AtvevoLogin = @Felhasznalo_Id_AtvevoLogin
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/Azonosito_szoveges')=1
         SET @Act_Azonosito_szoveges = @Azonosito_szoveges
   IF @UpdatedColumns.exist('/root/UgyintezesModja')=1
         SET @Act_UgyintezesModja = @UgyintezesModja
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_IraKezbesitesiTetelek
SET
     KezbesitesFej_Id = @Act_KezbesitesFej_Id,
     AtveteliFej_Id = @Act_AtveteliFej_Id,
     Obj_Id = @Act_Obj_Id,
     ObjTip_Id = @Act_ObjTip_Id,
     Obj_type = @Act_Obj_type,
     AtadasDat = @Act_AtadasDat,
     AtvetelDat = @Act_AtvetelDat,
     Megjegyzes = @Act_Megjegyzes,
     SztornirozasDat = @Act_SztornirozasDat,
     UtolsoNyomtatas_Id = @Act_UtolsoNyomtatas_Id,
     UtolsoNyomtatasIdo = @Act_UtolsoNyomtatasIdo,
     Felhasznalo_Id_Atado_USER = @Act_Felhasznalo_Id_Atado_USER,
     Felhasznalo_Id_Atado_LOGIN = @Act_Felhasznalo_Id_Atado_LOGIN,
     Csoport_Id_Cel = @Act_Csoport_Id_Cel,
     Felhasznalo_Id_AtvevoUser = @Act_Felhasznalo_Id_AtvevoUser,
     Felhasznalo_Id_AtvevoLogin = @Act_Felhasznalo_Id_AtvevoLogin,
     Allapot = @Act_Allapot,
     Azonosito_szoveges = @Act_Azonosito_szoveges,
     UgyintezesModja = @Act_UgyintezesModja,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_IraKezbesitesiTetelek',@Id
					,'EREC_IraKezbesitesiTetelekHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
