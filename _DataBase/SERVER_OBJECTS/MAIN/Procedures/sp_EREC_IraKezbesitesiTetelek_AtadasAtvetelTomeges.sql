IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraKezbesitesiTetelek_AtadasAtvetelTomeges]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraKezbesitesiTetelek_AtadasAtvetelTomeges]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraKezbesitesiTetelek_AtadasAtvetelTomeges]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraKezbesitesiTetelek_AtadasAtvetelTomeges] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraKezbesitesiTetelek_AtadasAtvetelTomeges]
	@Ids				nvarchar(MAX),
	@Vers				nvarchar(MAX),
	@ExecutorUserId		uniqueidentifier,
	@Mode				char(1)
as

BEGIN TRY
	set nocount on
	declare @execTime datetime;
	declare @tempTable table(id uniqueidentifier, ver int);

	set @execTime = getdate();

	/* dinamikus history log összeállításhoz */
	declare @row_ids varchar(MAX)

	-- temp tábla töltése az id-verzió párokkal
	declare @it	int;
	declare @verPosBegin int;
	declare @verPosEnd int;
	declare @curId	nvarchar(36);
	declare @curVer	nvarchar(6);
	declare @sqlcmd	nvarchar(500);

	set @it = 0;
	set @verPosBegin = 1;
	while (@it < ((len(@Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Ids,@it*39+2,37);
		set @verPosEnd =  CHARINDEX(',',@Vers,@verPosBegin);
		if @verPosEnd = 0 
			set @verPosEnd = len(@Vers) + 1;
		set @curVer = SUBSTRING(@vers, @verPosBegin, @verPosEnd - @verPosBegin);
		set @verPosBegin = @verPosEnd+1;
		insert into @tempTable(id,ver) values(@curId,convert(int,@curVer) );
		set @it = @it + 1;
	END
	
	-- verzióellenorzés
	declare @rowNumberTotal int;
	declare @rowNumberChecked int;

	select @rowNumberTotal = count(id) from @tempTable;
	select @rowNumberChecked = count(EREC_IraKezbesitesiTetelek.Id)
		from EREC_IraKezbesitesiTetelek
			inner join @tempTable t on t.id = EREC_IraKezbesitesiTetelek.id and t.ver = EREC_IraKezbesitesiTetelek.ver
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT EREC_IraKezbesitesiTetelek.Id FROM EREC_IraKezbesitesiTetelek
			inner join @tempTable t on t.id = EREC_IraKezbesitesiTetelek.id and t.ver = EREC_IraKezbesitesiTetelek.ver
		RAISERROR('[50402]',16,1);
	END



	/*** Atadas ***/
	if @Mode = '0'
	BEGIN
		-- Kézbesítési fej létrehozása
		declare @fejIdAtadas uniqueidentifier;
		set @fejIdAtadas = newid();
		insert into EREC_IraKezbesitesiFejek(Id,CsomagAzonosito,Tipus,Letrehozo_id) 
			values(@fejIdAtadas,'Tomeges atadas','A',@ExecutorUserId);

		/*** EREC_IraKezbesitesiFejek history log ***/
--		insert into EREC_IraKezbesitesiFejekHistory
--			select newid(),0,@ExecutorUserId,getdate(),k.*
--				from EREC_IraKezbesitesiFejek k
--				where k.id = @fejIdAtadas

		-- Az oszlopnevek sorrendje nem garantált, nem szabad *-gal naplózni!
		exec sp_LogRecordToHistory 'EREC_IraKezbesitesiFejek',@fejIdAtadas
							,'EREC_IraKezbesitesiFejekHistory',0,@ExecutorUserId,@execTime;

		-- EREC_IraKezbesitesiTetelek UPDATE
		SELECT EREC_IraKezbesitesiTetelek.Id INTO #atadasIds
			FROM EREC_IraKezbesitesiTetelek
			where Id in (select Id from @tempTable)
				and (ModositasIdo < @execTime or ModositasIdo is null)
				and Zarolo_id is null

		-- Utóellenorzés
		if (@rowNumberTotal != (SELECT COUNT(*) FROM #atadasIds) )
		BEGIN
			SELECT Obj_Id FROM EREC_IraKezbesitesiTetelek WHERE Id in
			(
				SELECT Id from @tempTable
				EXCEPT
				SELECT Id FROM #atadasIds
			)
			DROP TABLE #atadasIds;
			RAISERROR('[50403]',16,1);
		END

		UPDATE EREC_IraKezbesitesiTetelek
			set KezbesitesFej_Id = @fejIdAtadas,
				Allapot = '2',
				AtadasDat = @execTime,
				Ver = Ver + 1,
				Modosito_Id = @ExecutorUserId,
				ModositasIdo = getdate()
			where Id in (select Id from #atadasIds)
		DROP TABLE #atadasIds;
			
		SELECT @fejIdAtadas;
	END
	else
	BEGIN


	/*** Atvetel ***/
		-- Kezbesitesi fej letrehozasa
		declare @fejIdAtvetel uniqueidentifier;
		set @fejIdAtvetel = newid();
		insert into dbo.EREC_IraKezbesitesiFejek(Id,CsomagAzonosito,Tipus,Letrehozo_id)
			values(@fejIdAtvetel,'Tomeges atvetel','V',@ExecutorUserId)

		/*** EREC_IraKezbesitesiFejek history log ***/
--		insert into EREC_IraKezbesitesiFejekHistory 
--			select newid(),0,@ExecutorUserId,getdate(),k.*
--				from EREC_IraKezbesitesiFejek k
--				where k.id = @fejIdAtvetel
		-- Az oszlopnevek sorrendje nem garantált, nem szabad *-gal naplózni!
		exec sp_LogRecordToHistory 'EREC_IraKezbesitesiFejek',@fejIdAtadas
							,'EREC_IraKezbesitesiFejekHistory',0,@ExecutorUserId,@execTime;


		-- EREC_IraKezbesitesiTetelek UPDATE
		SELECT EREC_IraKezbesitesiTetelek.Id INTO #atvetelIds
			FROM EREC_IraKezbesitesiTetelek
			where Id in (select Id from @tempTable)
				and (ModositasIdo < @execTime or ModositasIdo is null)
				and Zarolo_id is null

		-- Utóellenorzés
		if (@rowNumberTotal != (SELECT COUNT(*) FROM #atvetelIds) )
		BEGIN
			SELECT Obj_Id FROM EREC_IraKezbesitesiTetelek WHERE Id in
			(
				SELECT Id from @tempTable
				EXCEPT
				SELECT Id FROM #atvetelIds
			)
			DROP TABLE #atvetelIds;
			RAISERROR('[50404]',16,1);
		END

		UPDATE EREC_IraKezbesitesiTetelek
			set Allapot = '3',
				AtveteliFej_Id = @fejIdAtvetel,
				AtvetelDat = @execTime,
				Felhasznalo_Id_AtvevoUser = @ExecutorUserId,
				Felhasznalo_Id_AtvevoLogin = @ExecutorUserId,
				Ver = Ver + 1,
				Modosito_Id = @ExecutorUserId,
				ModositasIdo = getdate()
			where Id in (select Id from #atvetelIds)
		DROP TABLE #atvetelIds;
			
		SELECT @fejIdAtvetel;
	END
--	
--	begin   /* History Log */
--		/*** EREC_IraKezbesitesiTetelek history log ***/
--		INSERT INTO EREC_IraKezbesitesiTetelekHistory 
--			SELECT	NEWID(),
--					'1',
--					@ExecutorUserId,
--					@execTime,
--					*
--					FROM EREC_IraKezbesitesiTetelek
--					WHERE Id IN (select Id from @tempTable)
--	end
		-- Az oszlopnevek sorrendje nem garantált, nem szabad *-gal naplózni!
		/*** EREC_IraKezbesitesiTetelek log ***/
		set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @tempTable FOR XML PATH('')),1, 2, '')
		if @row_ids is not null
			set @row_ids = @row_ids + '''';

		exec [sp_LogRecordsToHistory_Tomeges] 
				 @TableName = 'EREC_IraKezbesitesiTetelek'
				,@Row_Ids = @row_ids
				,@Muvelet = 1
				,@Vegrehajto_Id = @ExecutorUserId
				,@VegrehajtasIdo = @execTime


END TRY
BEGIN CATCH

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
