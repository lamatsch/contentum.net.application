IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraOnkormAdatokGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraOnkormAdatokGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraOnkormAdatokGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraOnkormAdatokGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraOnkormAdatokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraOnkormAdatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_IraOnkormAdatok.Id,
	   EREC_IraOnkormAdatok.IraIratok_Id,
	   EREC_IraOnkormAdatok.UgyFajtaja,
	   EREC_IraOnkormAdatok.DontestHozta,
	   EREC_IraOnkormAdatok.DontesFormaja,
	   EREC_IraOnkormAdatok.UgyintezesHataridore,
	   EREC_IraOnkormAdatok.HataridoTullepes,
	   EREC_IraOnkormAdatok.JogorvoslatiEljarasTipusa,
	   EREC_IraOnkormAdatok.JogorvoslatiDontesTipusa,
	   EREC_IraOnkormAdatok.JogorvoslatiDontestHozta,
	   EREC_IraOnkormAdatok.JogorvoslatiDontesTartalma,
	   EREC_IraOnkormAdatok.JogorvoslatiDontes,
	   EREC_IraOnkormAdatok.HatosagiEllenorzes,
	   EREC_IraOnkormAdatok.MunkaorakSzama,
	   EREC_IraOnkormAdatok.EljarasiKoltseg,
	   EREC_IraOnkormAdatok.KozigazgatasiBirsagMerteke,
	   EREC_IraOnkormAdatok.SommasEljDontes,
	   EREC_IraOnkormAdatok.NyolcNapBelulNemSommas,
	   EREC_IraOnkormAdatok.FuggoHatalyuHatarozat,
	   EREC_IraOnkormAdatok.FuggoHatHatalybaLepes,
	   EREC_IraOnkormAdatok.FuggoHatalyuVegzes,
	   EREC_IraOnkormAdatok.FuggoVegzesHatalyba,
	   EREC_IraOnkormAdatok.HatAltalVisszafizOsszeg,
	   EREC_IraOnkormAdatok.HatTerheloEljKtsg,
	   EREC_IraOnkormAdatok.FelfuggHatarozat,
	   EREC_IraOnkormAdatok.Ver,
	   EREC_IraOnkormAdatok.Note,
	   EREC_IraOnkormAdatok.Stat_id,
	   EREC_IraOnkormAdatok.ErvKezd,
	   EREC_IraOnkormAdatok.ErvVege,
	   EREC_IraOnkormAdatok.Letrehozo_id,
	   EREC_IraOnkormAdatok.LetrehozasIdo,
	   EREC_IraOnkormAdatok.Modosito_id,
	   EREC_IraOnkormAdatok.ModositasIdo,
	   EREC_IraOnkormAdatok.Zarolo_id,
	   EREC_IraOnkormAdatok.ZarolasIdo,
	   EREC_IraOnkormAdatok.Tranz_id,
	   EREC_IraOnkormAdatok.UIAccessLog_id  
   from 
     EREC_IraOnkormAdatok as EREC_IraOnkormAdatok      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
