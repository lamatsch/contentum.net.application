IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraOnkormAdatokHistoryGetRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraOnkormAdatokHistoryGetRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraOnkormAdatokHistoryGetRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraOnkormAdatokHistoryGetRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraOnkormAdatokHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_IraOnkormAdatokHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_IraOnkormAdatokHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIratok_Id' as ColumnName,               
               cast(Old.IraIratok_Id as nvarchar(99)) as OldValue,
               cast(New.IraIratok_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IraIratok_Id != New.IraIratok_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyFajtaja' as ColumnName,               
               cast(Old.UgyFajtaja as nvarchar(99)) as OldValue,
               cast(New.UgyFajtaja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyFajtaja != New.UgyFajtaja 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DontestHozta' as ColumnName,               
               cast(Old.DontestHozta as nvarchar(99)) as OldValue,
               cast(New.DontestHozta as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.DontestHozta != New.DontestHozta 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DontesFormaja' as ColumnName,               
               cast(Old.DontesFormaja as nvarchar(99)) as OldValue,
               cast(New.DontesFormaja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.DontesFormaja != New.DontesFormaja 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesHataridore' as ColumnName,               
               cast(Old.UgyintezesHataridore as nvarchar(99)) as OldValue,
               cast(New.UgyintezesHataridore as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyintezesHataridore != New.UgyintezesHataridore 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HataridoTullepes' as ColumnName,               
               cast(Old.HataridoTullepes as nvarchar(99)) as OldValue,
               cast(New.HataridoTullepes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HataridoTullepes != New.HataridoTullepes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'JogorvoslatiEljarasTipusa' as ColumnName,               
               cast(Old.JogorvoslatiEljarasTipusa as nvarchar(99)) as OldValue,
               cast(New.JogorvoslatiEljarasTipusa as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.JogorvoslatiEljarasTipusa != New.JogorvoslatiEljarasTipusa 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'JogorvoslatiDontesTipusa' as ColumnName,               
               cast(Old.JogorvoslatiDontesTipusa as nvarchar(99)) as OldValue,
               cast(New.JogorvoslatiDontesTipusa as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.JogorvoslatiDontesTipusa != New.JogorvoslatiDontesTipusa 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'JogorvoslatiDontestHozta' as ColumnName,               
               cast(Old.JogorvoslatiDontestHozta as nvarchar(99)) as OldValue,
               cast(New.JogorvoslatiDontestHozta as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.JogorvoslatiDontestHozta != New.JogorvoslatiDontestHozta 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'JogorvoslatiDontesTartalma' as ColumnName,               
               cast(Old.JogorvoslatiDontesTartalma as nvarchar(99)) as OldValue,
               cast(New.JogorvoslatiDontesTartalma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.JogorvoslatiDontesTartalma != New.JogorvoslatiDontesTartalma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'JogorvoslatiDontes' as ColumnName,               
               cast(Old.JogorvoslatiDontes as nvarchar(99)) as OldValue,
               cast(New.JogorvoslatiDontes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.JogorvoslatiDontes != New.JogorvoslatiDontes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatosagiEllenorzes' as ColumnName,               
               cast(Old.HatosagiEllenorzes as nvarchar(99)) as OldValue,
               cast(New.HatosagiEllenorzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HatosagiEllenorzes != New.HatosagiEllenorzes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MunkaorakSzama' as ColumnName,               
               cast(Old.MunkaorakSzama as nvarchar(99)) as OldValue,
               cast(New.MunkaorakSzama as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MunkaorakSzama != New.MunkaorakSzama 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EljarasiKoltseg' as ColumnName,               
               cast(Old.EljarasiKoltseg as nvarchar(99)) as OldValue,
               cast(New.EljarasiKoltseg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.EljarasiKoltseg != New.EljarasiKoltseg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KozigazgatasiBirsagMerteke' as ColumnName,               
               cast(Old.KozigazgatasiBirsagMerteke as nvarchar(99)) as OldValue,
               cast(New.KozigazgatasiBirsagMerteke as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KozigazgatasiBirsagMerteke != New.KozigazgatasiBirsagMerteke 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SommasEljDontes' as ColumnName,               
               cast(Old.SommasEljDontes as nvarchar(99)) as OldValue,
               cast(New.SommasEljDontes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SommasEljDontes != New.SommasEljDontes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'NyolcNapBelulNemSommas' as ColumnName,               
               cast(Old.NyolcNapBelulNemSommas as nvarchar(99)) as OldValue,
               cast(New.NyolcNapBelulNemSommas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.NyolcNapBelulNemSommas != New.NyolcNapBelulNemSommas 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FuggoHatalyuHatarozat' as ColumnName,               
               cast(Old.FuggoHatalyuHatarozat as nvarchar(99)) as OldValue,
               cast(New.FuggoHatalyuHatarozat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FuggoHatalyuHatarozat != New.FuggoHatalyuHatarozat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FuggoHatHatalybaLepes' as ColumnName,               
               cast(Old.FuggoHatHatalybaLepes as nvarchar(99)) as OldValue,
               cast(New.FuggoHatHatalybaLepes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FuggoHatHatalybaLepes != New.FuggoHatHatalybaLepes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FuggoHatalyuVegzes' as ColumnName,               
               cast(Old.FuggoHatalyuVegzes as nvarchar(99)) as OldValue,
               cast(New.FuggoHatalyuVegzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FuggoHatalyuVegzes != New.FuggoHatalyuVegzes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FuggoVegzesHatalyba' as ColumnName,               
               cast(Old.FuggoVegzesHatalyba as nvarchar(99)) as OldValue,
               cast(New.FuggoVegzesHatalyba as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FuggoVegzesHatalyba != New.FuggoVegzesHatalyba 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatAltalVisszafizOsszeg' as ColumnName,               
               cast(Old.HatAltalVisszafizOsszeg as nvarchar(99)) as OldValue,
               cast(New.HatAltalVisszafizOsszeg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HatAltalVisszafizOsszeg != New.HatAltalVisszafizOsszeg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatTerheloEljKtsg' as ColumnName,               
               cast(Old.HatTerheloEljKtsg as nvarchar(99)) as OldValue,
               cast(New.HatTerheloEljKtsg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HatTerheloEljKtsg != New.HatTerheloEljKtsg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelfuggHatarozat' as ColumnName,               
               cast(Old.FelfuggHatarozat as nvarchar(99)) as OldValue,
               cast(New.FelfuggHatarozat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelfuggHatarozat != New.FelfuggHatarozat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end


GO
