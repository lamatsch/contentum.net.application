IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraOnkormAdatokTesztSegedLista]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraOnkormAdatokTesztSegedLista]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraOnkormAdatokTesztSegedLista]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraOnkormAdatokTesztSegedLista] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraOnkormAdatokTesztSegedLista]
  @Where nvarchar(MAX) = '',
  @Where_EREC_IraIratok NVARCHAR(MAX) = '',
  @OrderBy nvarchar(200) = ' order by EREC_AgazatiJelek.Kod, EREC_IraIktatokonyvek.Ev DESC, EREC_UgyUgyiratok.Foszam DESC, EREC_IraIratok.Alszam DESC, EREC_IraIratok.Sorszam',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
	
	DECLARE @Org uniqueidentifier
	--SET @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
	   EREC_AgazatiJelek.Kod as AgazatiJel,
  	   EREC_IraIktatokonyvek.Iktatohely as IkatatoKonyv,
  	   EREC_UgyUgyiratok.Azonosito as UgyiratIktatatoSzam,
       EREC_IraIratok.Azonosito as IktatatoSzam,
       EREC_IraOnkormAdatok.Id,
	   EREC_IraOnkormAdatok.IraIratok_Id,
	   EREC_IraOnkormAdatok.UgyFajtaja,
	    EREC_IraOnkormAdatok.UgyFajtaja,
	   dbo.fn_HatosagiAdatKodtarErtek(''UGY_FAJTAJA'', EREC_IraOnkormAdatok.UgyFajtaja, @Org, EREC_IraOnkormAdatok.LetrehozasIdo) as UgyFajtaja_Kod,
	   EREC_IraOnkormAdatok.DontestHozta,
	   dbo.fn_HatosagiAdatKodtarErtek(''DONTEST_HOZTA'', EREC_IraOnkormAdatok.DontestHozta, @Org, EREC_IraOnkormAdatok.LetrehozasIdo) as DontestHozta_Kod,
	   EREC_IraOnkormAdatok.DontesFormaja,
	   dbo.fn_HatosagiAdatKodtarErtek(''DONTES_FORMAJA'', EREC_IraOnkormAdatok.DontesFormaja, @Org, EREC_IraOnkormAdatok.LetrehozasIdo) as DontesFormaja_Kod,
	   EREC_IraOnkormAdatok.UgyintezesHataridore,
	   dbo.fn_HatosagiAdatKodtarErtek(''UGYINTEZES_IDOTARTAMA'', EREC_IraOnkormAdatok.UgyintezesHataridore, @Org, EREC_IraOnkormAdatok.LetrehozasIdo) as UgyintezesHataridore_Kod,
	   EREC_IraOnkormAdatok.HataridoTullepes,
	   EREC_IraOnkormAdatok.JogorvoslatiEljarasTipusa,
	   dbo.fn_HatosagiAdatKodtarErtek(''JOGORVOSLATI_ELJARAS_TIPUSA'', EREC_IraOnkormAdatok.JogorvoslatiEljarasTipusa, @Org, EREC_IraOnkormAdatok.LetrehozasIdo) as JogorvoslatiEljarasTipusa_Kod,
	   EREC_IraOnkormAdatok.JogorvoslatiDontesTipusa,
	   dbo.fn_HatosagiAdatKodtarErtek(''JOGORVOSLATI_DONTES_TIPUSA'', EREC_IraOnkormAdatok.JogorvoslatiDontesTipusa, @Org, EREC_IraOnkormAdatok.LetrehozasIdo) as JogorvoslatiDontesTipusa_Kod,
	   EREC_IraOnkormAdatok.JogorvoslatiDontestHozta,
	   dbo.fn_HatosagiAdatKodtarErtek(''JOGORVOSLATI_DONTEST_HOZTA'', EREC_IraOnkormAdatok.JogorvoslatiDontestHozta, @Org, EREC_IraOnkormAdatok.LetrehozasIdo) as JogorvoslatiDontestHozta_Kod,
	   EREC_IraOnkormAdatok.JogorvoslatiDontesTartalma,
	   dbo.fn_HatosagiAdatKodtarErtek(''JOGORVOSLATI_DONTES_TARTALMA'', EREC_IraOnkormAdatok.JogorvoslatiDontesTartalma, @Org, EREC_IraOnkormAdatok.LetrehozasIdo) as JogorvoslatiDontesTartalma_Kod,
	   EREC_IraOnkormAdatok.JogorvoslatiDontes,
	   dbo.fn_HatosagiAdatKodtarErtek(''JOGORVOSLATI_DONTES'', EREC_IraOnkormAdatok.JogorvoslatiDontes, @Org, EREC_IraOnkormAdatok.LetrehozasIdo) as JogorvoslatiDontes_Kod,
	   EREC_IraOnkormAdatok.HatosagiEllenorzes,
	   EREC_IraOnkormAdatok.MunkaorakSzama,
	   EREC_IraOnkormAdatok.EljarasiKoltseg,
	   EREC_IraOnkormAdatok.KozigazgatasiBirsagMerteke,
	   EREC_IraOnkormAdatok.Ver,
	   EREC_IraOnkormAdatok.Note,
	   EREC_IraOnkormAdatok.Stat_id,
	   EREC_IraOnkormAdatok.ErvKezd,
	   EREC_IraOnkormAdatok.ErvVege,
	   EREC_IraOnkormAdatok.Letrehozo_id,
	   EREC_IraOnkormAdatok.LetrehozasIdo,
	   EREC_IraOnkormAdatok.Modosito_id,
	   EREC_IraOnkormAdatok.ModositasIdo,
	   EREC_IraOnkormAdatok.Zarolo_id,
	   EREC_IraOnkormAdatok.ZarolasIdo,
	   EREC_IraOnkormAdatok.Tranz_id,
	   EREC_IraOnkormAdatok.UIAccessLog_id  
	FROM EREC_IraIratok as EREC_IraIratok
	LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok
	ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek
	ON EREC_IraIktatoKonyvek.Id = EREC_UgyUgyiratok.IraIktatokonyv_Id
	join EREC_IraOnkormAdatok as EREC_IraOnkormAdatok
	ON EREC_IraOnkormAdatok.IraIratok_Id = EREC_IraIratok.Id
	left join EREC_IraIrattariTetelek as EREC_IraIrattariTetelek
	ON EREC_UgyUgyiratok.IraIrattariTetel_Id = EREC_IraIrattariTetelek.Id
    left join EREC_AgazatiJelek
	ON EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_AgazatiJelek.Id'
	
	set @sqlcmd = @sqlcmd + 
	'
	WHERE
	(
       EREC_IraOnkormAdatok.UgyFajtaja = ''1'' 
       or EREC_IraOnkormAdatok.UgyFajtaja = ''2''
    )
    and
    (
		EREC_IraOnkormAdatok.HatosagiEllenorzes is not null
		or
		EREC_IraOnkormAdatok.MunkaorakSzama is not null
		or
		EREC_IraOnkormAdatok.EljarasiKoltseg is not null
		or
		EREC_IraOnkormAdatok.KozigazgatasiBirsagMerteke is not null
    )
	'

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
	
	if @Where_EREC_IraIratok is not null and @Where_EREC_IraIratok!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where_EREC_IraIratok
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	
	execute sp_executesql @sqlcmd,N'@Org uniqueidentifier', @Org;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
