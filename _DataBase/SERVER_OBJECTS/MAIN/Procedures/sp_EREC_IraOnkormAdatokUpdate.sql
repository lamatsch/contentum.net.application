IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraOnkormAdatokUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IraOnkormAdatokUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IraOnkormAdatokUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IraOnkormAdatokUpdate] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IraOnkormAdatokUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @IraIratok_Id     uniqueidentifier  = null,         
             @UgyFajtaja     nvarchar(64)  = null,         
             @DontestHozta     nvarchar(64)  = null,         
             @DontesFormaja     nvarchar(64)  = null,         
             @UgyintezesHataridore     nvarchar(64)  = null,         
             @HataridoTullepes     int  = null,         
             @JogorvoslatiEljarasTipusa     nvarchar(64)  = null,         
             @JogorvoslatiDontesTipusa     nvarchar(64)  = null,         
             @JogorvoslatiDontestHozta     nvarchar(64)  = null,         
             @JogorvoslatiDontesTartalma     nvarchar(64)  = null,         
             @JogorvoslatiDontes     nvarchar(64)  = null,         
             @HatosagiEllenorzes     char(1)  = null,         
             @MunkaorakSzama     float  = null,         
             @EljarasiKoltseg     int  = null,         
             @KozigazgatasiBirsagMerteke     int  = null,         
             @SommasEljDontes     nvarchar(64)  = null,         
             @NyolcNapBelulNemSommas     nvarchar(64)  = null,         
             @FuggoHatalyuHatarozat     nvarchar(64)  = null,         
             @FuggoHatHatalybaLepes     nvarchar(64)  = null,         
             @FuggoHatalyuVegzes     nvarchar(64)  = null,         
             @FuggoVegzesHatalyba     nvarchar(64)  = null,         
             @HatAltalVisszafizOsszeg     int  = null,         
             @HatTerheloEljKtsg     int  = null,         
             @FelfuggHatarozat     nvarchar(64)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_IraIratok_Id     uniqueidentifier         
     DECLARE @Act_UgyFajtaja     nvarchar(64)         
     DECLARE @Act_DontestHozta     nvarchar(64)         
     DECLARE @Act_DontesFormaja     nvarchar(64)         
     DECLARE @Act_UgyintezesHataridore     nvarchar(64)         
     DECLARE @Act_HataridoTullepes     int         
     DECLARE @Act_JogorvoslatiEljarasTipusa     nvarchar(64)         
     DECLARE @Act_JogorvoslatiDontesTipusa     nvarchar(64)         
     DECLARE @Act_JogorvoslatiDontestHozta     nvarchar(64)         
     DECLARE @Act_JogorvoslatiDontesTartalma     nvarchar(64)         
     DECLARE @Act_JogorvoslatiDontes     nvarchar(64)         
     DECLARE @Act_HatosagiEllenorzes     char(1)         
     DECLARE @Act_MunkaorakSzama     float         
     DECLARE @Act_EljarasiKoltseg     int         
     DECLARE @Act_KozigazgatasiBirsagMerteke     int         
     DECLARE @Act_SommasEljDontes     nvarchar(64)         
     DECLARE @Act_NyolcNapBelulNemSommas     nvarchar(64)         
     DECLARE @Act_FuggoHatalyuHatarozat     nvarchar(64)         
     DECLARE @Act_FuggoHatHatalybaLepes     nvarchar(64)         
     DECLARE @Act_FuggoHatalyuVegzes     nvarchar(64)         
     DECLARE @Act_FuggoVegzesHatalyba     nvarchar(64)         
     DECLARE @Act_HatAltalVisszafizOsszeg     int         
     DECLARE @Act_HatTerheloEljKtsg     int         
     DECLARE @Act_FelfuggHatarozat     nvarchar(64)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_IraIratok_Id = IraIratok_Id,
     @Act_UgyFajtaja = UgyFajtaja,
     @Act_DontestHozta = DontestHozta,
     @Act_DontesFormaja = DontesFormaja,
     @Act_UgyintezesHataridore = UgyintezesHataridore,
     @Act_HataridoTullepes = HataridoTullepes,
     @Act_JogorvoslatiEljarasTipusa = JogorvoslatiEljarasTipusa,
     @Act_JogorvoslatiDontesTipusa = JogorvoslatiDontesTipusa,
     @Act_JogorvoslatiDontestHozta = JogorvoslatiDontestHozta,
     @Act_JogorvoslatiDontesTartalma = JogorvoslatiDontesTartalma,
     @Act_JogorvoslatiDontes = JogorvoslatiDontes,
     @Act_HatosagiEllenorzes = HatosagiEllenorzes,
     @Act_MunkaorakSzama = MunkaorakSzama,
     @Act_EljarasiKoltseg = EljarasiKoltseg,
     @Act_KozigazgatasiBirsagMerteke = KozigazgatasiBirsagMerteke,
     @Act_SommasEljDontes = SommasEljDontes,
     @Act_NyolcNapBelulNemSommas = NyolcNapBelulNemSommas,
     @Act_FuggoHatalyuHatarozat = FuggoHatalyuHatarozat,
     @Act_FuggoHatHatalybaLepes = FuggoHatHatalybaLepes,
     @Act_FuggoHatalyuVegzes = FuggoHatalyuVegzes,
     @Act_FuggoVegzesHatalyba = FuggoVegzesHatalyba,
     @Act_HatAltalVisszafizOsszeg = HatAltalVisszafizOsszeg,
     @Act_HatTerheloEljKtsg = HatTerheloEljKtsg,
     @Act_FelfuggHatarozat = FelfuggHatarozat,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_IraOnkormAdatok
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zĂˇrolĂˇs ellenĹ‘rzĂ©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/IraIratok_Id')=1
         SET @Act_IraIratok_Id = @IraIratok_Id
   IF @UpdatedColumns.exist('/root/UgyFajtaja')=1
         SET @Act_UgyFajtaja = @UgyFajtaja
   IF @UpdatedColumns.exist('/root/DontestHozta')=1
         SET @Act_DontestHozta = @DontestHozta
   IF @UpdatedColumns.exist('/root/DontesFormaja')=1
         SET @Act_DontesFormaja = @DontesFormaja
   IF @UpdatedColumns.exist('/root/UgyintezesHataridore')=1
         SET @Act_UgyintezesHataridore = @UgyintezesHataridore
   IF @UpdatedColumns.exist('/root/HataridoTullepes')=1
         SET @Act_HataridoTullepes = @HataridoTullepes
   IF @UpdatedColumns.exist('/root/JogorvoslatiEljarasTipusa')=1
         SET @Act_JogorvoslatiEljarasTipusa = @JogorvoslatiEljarasTipusa
   IF @UpdatedColumns.exist('/root/JogorvoslatiDontesTipusa')=1
         SET @Act_JogorvoslatiDontesTipusa = @JogorvoslatiDontesTipusa
   IF @UpdatedColumns.exist('/root/JogorvoslatiDontestHozta')=1
         SET @Act_JogorvoslatiDontestHozta = @JogorvoslatiDontestHozta
   IF @UpdatedColumns.exist('/root/JogorvoslatiDontesTartalma')=1
         SET @Act_JogorvoslatiDontesTartalma = @JogorvoslatiDontesTartalma
   IF @UpdatedColumns.exist('/root/JogorvoslatiDontes')=1
         SET @Act_JogorvoslatiDontes = @JogorvoslatiDontes
   IF @UpdatedColumns.exist('/root/HatosagiEllenorzes')=1
         SET @Act_HatosagiEllenorzes = @HatosagiEllenorzes
   IF @UpdatedColumns.exist('/root/MunkaorakSzama')=1
         SET @Act_MunkaorakSzama = @MunkaorakSzama
   IF @UpdatedColumns.exist('/root/EljarasiKoltseg')=1
         SET @Act_EljarasiKoltseg = @EljarasiKoltseg
   IF @UpdatedColumns.exist('/root/KozigazgatasiBirsagMerteke')=1
         SET @Act_KozigazgatasiBirsagMerteke = @KozigazgatasiBirsagMerteke
   IF @UpdatedColumns.exist('/root/SommasEljDontes')=1
         SET @Act_SommasEljDontes = @SommasEljDontes
   IF @UpdatedColumns.exist('/root/NyolcNapBelulNemSommas')=1
         SET @Act_NyolcNapBelulNemSommas = @NyolcNapBelulNemSommas
   IF @UpdatedColumns.exist('/root/FuggoHatalyuHatarozat')=1
         SET @Act_FuggoHatalyuHatarozat = @FuggoHatalyuHatarozat
   IF @UpdatedColumns.exist('/root/FuggoHatHatalybaLepes')=1
         SET @Act_FuggoHatHatalybaLepes = @FuggoHatHatalybaLepes
   IF @UpdatedColumns.exist('/root/FuggoHatalyuVegzes')=1
         SET @Act_FuggoHatalyuVegzes = @FuggoHatalyuVegzes
   IF @UpdatedColumns.exist('/root/FuggoVegzesHatalyba')=1
         SET @Act_FuggoVegzesHatalyba = @FuggoVegzesHatalyba
   IF @UpdatedColumns.exist('/root/HatAltalVisszafizOsszeg')=1
         SET @Act_HatAltalVisszafizOsszeg = @HatAltalVisszafizOsszeg
   IF @UpdatedColumns.exist('/root/HatTerheloEljKtsg')=1
         SET @Act_HatTerheloEljKtsg = @HatTerheloEljKtsg
   IF @UpdatedColumns.exist('/root/FelfuggHatarozat')=1
         SET @Act_FelfuggHatarozat = @FelfuggHatarozat
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_IraOnkormAdatok
SET
     IraIratok_Id = @Act_IraIratok_Id,
     UgyFajtaja = @Act_UgyFajtaja,
     DontestHozta = @Act_DontestHozta,
     DontesFormaja = @Act_DontesFormaja,
     UgyintezesHataridore = @Act_UgyintezesHataridore,
     HataridoTullepes = @Act_HataridoTullepes,
     JogorvoslatiEljarasTipusa = @Act_JogorvoslatiEljarasTipusa,
     JogorvoslatiDontesTipusa = @Act_JogorvoslatiDontesTipusa,
     JogorvoslatiDontestHozta = @Act_JogorvoslatiDontestHozta,
     JogorvoslatiDontesTartalma = @Act_JogorvoslatiDontesTartalma,
     JogorvoslatiDontes = @Act_JogorvoslatiDontes,
     HatosagiEllenorzes = @Act_HatosagiEllenorzes,
     MunkaorakSzama = @Act_MunkaorakSzama,
     EljarasiKoltseg = @Act_EljarasiKoltseg,
     KozigazgatasiBirsagMerteke = @Act_KozigazgatasiBirsagMerteke,
     SommasEljDontes = @Act_SommasEljDontes,
     NyolcNapBelulNemSommas = @Act_NyolcNapBelulNemSommas,
     FuggoHatalyuHatarozat = @Act_FuggoHatalyuHatarozat,
     FuggoHatHatalybaLepes = @Act_FuggoHatHatalybaLepes,
     FuggoHatalyuVegzes = @Act_FuggoHatalyuVegzes,
     FuggoVegzesHatalyba = @Act_FuggoVegzesHatalyba,
     HatAltalVisszafizOsszeg = @Act_HatAltalVisszafizOsszeg,
     HatTerheloEljKtsg = @Act_HatTerheloEljKtsg,
     FelfuggHatarozat = @Act_FelfuggHatarozat,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_IraOnkormAdatok',@Id
					,'EREC_IraOnkormAdatokHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
