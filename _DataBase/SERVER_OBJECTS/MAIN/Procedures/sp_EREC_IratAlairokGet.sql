IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratAlairokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IratAlairokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratAlairokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IratAlairokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IratAlairokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IratAlairok.Id,
	   EREC_IratAlairok.PldIratPeldany_Id,
	   EREC_IratAlairok.Objtip_Id,
	   EREC_IratAlairok.Obj_Id,
	   EREC_IratAlairok.AlairasDatuma,
	   EREC_IratAlairok.Leiras,
	   EREC_IratAlairok.AlairoSzerep,
	   EREC_IratAlairok.AlairasSorrend,
	   EREC_IratAlairok.FelhasznaloCsoport_Id_Alairo,
	   EREC_IratAlairok.FelhaszCsoport_Id_Helyettesito,
	   EREC_IratAlairok.Azonosito,
	   EREC_IratAlairok.AlairasMod,
	   EREC_IratAlairok.AlairasSzabaly_Id,
	   EREC_IratAlairok.Allapot,
	   EREC_IratAlairok.Ver,
	   EREC_IratAlairok.Note,
	   EREC_IratAlairok.Stat_id,
	   EREC_IratAlairok.ErvKezd,
	   EREC_IratAlairok.ErvVege,
	   EREC_IratAlairok.Letrehozo_id,
	   EREC_IratAlairok.LetrehozasIdo,
	   EREC_IratAlairok.Modosito_id,
	   EREC_IratAlairok.ModositasIdo,
	   EREC_IratAlairok.Zarolo_id,
	   EREC_IratAlairok.ZarolasIdo,
	   EREC_IratAlairok.Tranz_id,
	   EREC_IratAlairok.UIAccessLog_id
	   from 
		 EREC_IratAlairok as EREC_IratAlairok 
	   where
		 EREC_IratAlairok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
