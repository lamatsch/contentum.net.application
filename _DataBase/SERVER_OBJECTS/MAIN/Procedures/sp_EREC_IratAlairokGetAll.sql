IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratAlairokGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IratAlairokGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratAlairokGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IratAlairokGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IratAlairokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IratAlairok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_IratAlairok.Id,
	   EREC_IratAlairok.PldIratPeldany_Id,
	   EREC_IratAlairok.Objtip_Id,
	   EREC_IratAlairok.Obj_Id,
	   EREC_IratAlairok.AlairasDatuma,
	   EREC_IratAlairok.Leiras,
	   EREC_IratAlairok.AlairoSzerep,
	   EREC_IratAlairok.AlairasSorrend,
	   EREC_IratAlairok.FelhasznaloCsoport_Id_Alairo,
	   EREC_IratAlairok.FelhaszCsoport_Id_Helyettesito,
	   EREC_IratAlairok.Azonosito,
	   EREC_IratAlairok.AlairasMod,
	   EREC_IratAlairok.AlairasSzabaly_Id,
	   EREC_IratAlairok.Allapot,
	   EREC_IratAlairok.Ver,
	   EREC_IratAlairok.Note,
	   EREC_IratAlairok.Stat_id,
	   EREC_IratAlairok.ErvKezd,
	   EREC_IratAlairok.ErvVege,
	   EREC_IratAlairok.Letrehozo_id,
	   EREC_IratAlairok.LetrehozasIdo,
	   EREC_IratAlairok.Modosito_id,
	   EREC_IratAlairok.ModositasIdo,
	   EREC_IratAlairok.Zarolo_id,
	   EREC_IratAlairok.ZarolasIdo,
	   EREC_IratAlairok.Tranz_id,
	   EREC_IratAlairok.UIAccessLog_id  
   from 
     EREC_IratAlairok as EREC_IratAlairok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
