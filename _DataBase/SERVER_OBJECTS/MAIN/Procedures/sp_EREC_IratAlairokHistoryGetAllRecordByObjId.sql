IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratAlairokHistoryGetAllRecordByObjId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IratAlairokHistoryGetAllRecordByObjId]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratAlairokHistoryGetAllRecordByObjId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IratAlairokHistoryGetAllRecordByObjId] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IratAlairokHistoryGetAllRecordByObjId]
	@Obj_Id			nvarchar(255) = '%',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@Org              uniqueidentifier
		
as
begin

select EREC_IratAlairokHistory.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = EREC_IratAlairokHistory.AlairasSzabaly_Id) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',EREC_IratAlairokHistory.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',EREC_IratAlairokHistory.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo	where EREC_IratAlairokHistory.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id ) as Alairo_Nev,
				HistoryId as Id, 1 as Ver,  case EREC_IratAlairokHistory.HistoryMuvelet_Id 
				  when 0 then 'Létrehozás'
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_IratAlairokHistory  inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where  Obj_Id like @Obj_Id
union all 
      (select Old.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = Old.AlairasSzabaly_Id) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',Old.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',Old.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo
				where Old.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id   ) ,New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
				  when 0 then 'Létrehozás'
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratAlairokHistory Old 
         inner join EREC_IratAlairokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id
            and Old.Id = New.Id
			and Old.Obj_Id like @Obj_Id AND New.HistoryMuvelet_Id != 0
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
		
)
union all
(select Old.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = Old.AlairasSzabaly_Id) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',Old.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',Old.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo
				where Old.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id   ) ,New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
				  when 0 then 'Létrehozás'
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratAlairokHistory Old 
         inner join EREC_IratAlairokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id
            and Old.Id = New.Id
			and Old.Obj_Id like @Obj_Id AND New.HistoryMuvelet_Id != 0
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
		
)
            
      union all 
      (select Old.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = Old.AlairasSzabaly_Id) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',Old.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',Old.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo
				where Old.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id   ) ,New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
				  when 0 then 'Létrehozás'
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PldIratPeldany_Id' as ColumnName,               cast(Old.PldIratPeldany_Id as nvarchar(99)) as OldValue,
               cast(New.PldIratPeldany_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratAlairokHistory Old 
         inner join EREC_IratAlairokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PldIratPeldany_Id != New.PldIratPeldany_Id 
            and Old.Id = New.Id
			and Old.Obj_Id like @Obj_Id AND New.HistoryMuvelet_Id != 0
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
		  
)
            
      union all 
      (select Old.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = Old.AlairasSzabaly_Id) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',Old.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',Old.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo
				where Old.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id   ) AS Alairo_Nev, New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
			   when 0 then 'Létrehozás'
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Objtip_Id' as ColumnName,               cast(Old.Objtip_Id as nvarchar(99)) as OldValue,
               cast(New.Objtip_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratAlairokHistory Old
         inner join EREC_IratAlairokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Objtip_Id != New.Objtip_Id 
            and Old.Id = New.Id
			and Old.Obj_Id like @Obj_Id AND New.HistoryMuvelet_Id != 0
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
		  
)
            
      union all 
      (select Old.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = Old.AlairasSzabaly_Id) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',Old.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',Old.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo
				where Old.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id   ) AS Alairo_Nev, New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id
			   when 0 then 'Létrehozás' 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Id' as ColumnName,               cast(Old.Obj_Id as nvarchar(99)) as OldValue,
               cast(New.Obj_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratAlairokHistory Old
         inner join EREC_IratAlairokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_Id != New.Obj_Id 
            and Old.Id = New.Id
			and Old.Obj_Id like @Obj_Id AND New.HistoryMuvelet_Id != 0
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
		  
)
            
      union all 
      (select Old.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = Old.AlairasSzabaly_Id) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',Old.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',Old.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo
				where Old.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id   ) AS Alairo_Nev, New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id
			      when 0 then 'Létrehozás' 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasDatuma' as ColumnName,               cast(Old.AlairasDatuma as nvarchar(99)) as OldValue,
               cast(New.AlairasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratAlairokHistory Old
         inner join EREC_IratAlairokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasDatuma != New.AlairasDatuma 
            and Old.Id = New.Id
			and Old.Obj_Id like @Obj_Id AND New.HistoryMuvelet_Id != 0
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
		  
)
            
      union all 
      (select Old.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = Old.AlairasSzabaly_Id) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',Old.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',Old.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo
				where Old.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id   ) AS Alairo_Nev, New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id
			   when 0 then 'Létrehozás' 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Leiras' as ColumnName,               cast(Old.Leiras as nvarchar(99)) as OldValue,
               cast(New.Leiras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratAlairokHistory Old
         inner join EREC_IratAlairokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Leiras != New.Leiras 
            and Old.Id = New.Id
			and Old.Obj_Id like @Obj_Id AND New.HistoryMuvelet_Id != 0
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
		  
)
            
      union all 
      (select Old.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = Old.AlairasSzabaly_Id) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',Old.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',Old.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo
				where Old.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id   ) AS Alairo_Nev, New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id
			      when 0 then 'Létrehozás' 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairoSzerep' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratAlairokHistory Old
         inner join EREC_IratAlairokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairoSzerep != New.AlairoSzerep 
            and Old.Id = New.Id
			and Old.Obj_Id like @Obj_Id AND New.HistoryMuvelet_Id != 0
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'ALAIRO_SZEREP'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.AlairoSzerep and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.AlairoSzerep and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
		  
)
            
      union all 
      (select Old.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = Old.AlairasSzabaly_Id) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',Old.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',Old.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo
				where Old.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id   ) AS Alairo_Nev, New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id
			      when 0 then 'Létrehozás' 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasSorrend' as ColumnName,               cast(Old.AlairasSorrend as nvarchar(99)) as OldValue,
               cast(New.AlairasSorrend as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratAlairokHistory Old
         inner join EREC_IratAlairokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasSorrend != New.AlairasSorrend 
            and Old.Id = New.Id
			and Old.Obj_Id like @Obj_Id AND New.HistoryMuvelet_Id != 0
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
		   
)
            
      union all 
      (select Old.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = Old.AlairasSzabaly_Id) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',Old.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',Old.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo
				where Old.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id   ) AS Alairo_Nev, New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id
		   	      when 0 then 'Létrehozás' 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Alairo' as ColumnName,               cast(Old.FelhasznaloCsoport_Id_Alairo as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Alairo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratAlairokHistory Old
         inner join EREC_IratAlairokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Alairo != New.FelhasznaloCsoport_Id_Alairo 
            and Old.Id = New.Id
			and Old.Obj_Id like @Obj_Id AND New.HistoryMuvelet_Id != 0
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
		   
)
            
      union all 
      (select Old.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = Old.AlairasSzabaly_Id) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',Old.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',Old.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo
				where Old.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id   ) AS Alairo_Nev, New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id
			      when 0 then 'Létrehozás' 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhaszCsoport_Id_Helyettesito' as ColumnName,               cast(Old.FelhaszCsoport_Id_Helyettesito as nvarchar(99)) as OldValue,
               cast(New.FelhaszCsoport_Id_Helyettesito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratAlairokHistory Old
         inner join EREC_IratAlairokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhaszCsoport_Id_Helyettesito != New.FelhaszCsoport_Id_Helyettesito 
            and Old.Id = New.Id
			and Old.Obj_Id like @Obj_Id AND New.HistoryMuvelet_Id != 0
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
	    	  
)
            
      union all 
      (select Old.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = Old.AlairasSzabaly_Id) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',Old.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',Old.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo
				where Old.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id   ) AS Alairo_Nev, New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id
			      when 0 then 'Létrehozás' 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Azonosito' as ColumnName,               cast(Old.Azonosito as nvarchar(99)) as OldValue,
               cast(New.Azonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratAlairokHistory Old
         inner join EREC_IratAlairokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Azonosito != New.Azonosito 
            and Old.Id = New.Id
			and Old.Obj_Id like @Obj_Id AND New.HistoryMuvelet_Id != 0
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
		   
)
            
      union all 
      (select Old.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = Old.AlairasSzabaly_Id) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',Old.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',Old.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo
				where Old.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id   ) AS Alairo_Nev, New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id
			   when 0 then 'Létrehozás' 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasMod' as ColumnName,               cast(Old.AlairasMod as nvarchar(99)) as OldValue,
               cast(New.AlairasMod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratAlairokHistory Old
         inner join EREC_IratAlairokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasMod != New.AlairasMod 
            and Old.Id = New.Id
			and Old.Obj_Id like @Obj_Id AND New.HistoryMuvelet_Id != 0
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
		   
)
            
      union all 
      (select Old.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = Old.AlairasSzabaly_Id) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',Old.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',Old.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo
				where Old.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id   ) AS Alairo_Nev, New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id
			   when 0 then 'Létrehozás' 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasSzabaly_Id' as ColumnName,               cast(Old.AlairasSzabaly_Id as nvarchar(99)) as OldValue,
               cast(New.AlairasSzabaly_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratAlairokHistory Old
         inner join EREC_IratAlairokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasSzabaly_Id != New.AlairasSzabaly_Id 
            and Old.Id = New.Id
			and Old.Obj_Id like @Obj_Id AND New.HistoryMuvelet_Id != 0
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
		  
)
      union all 
      (select Old.*,(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = Old.AlairasSzabaly_Id ) AS AlairasTipus,
	dbo.[fn_KodtarErtekNeve]('IRATALAIRAS_ALLAPOT',Old.Allapot,@Org) AS Allapot_Nev,
	dbo.[fn_KodtarErtekNeve]('ALAIRO_SZEREP',Old.AlairoSzerep,@Org) AS AlairoSzerep_Nev,
	(select Nev from KRT_Csoportok as KRT_Csoportok_Alairo
				where Old.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id   ) AS Alairo_Nev, New.HistoryId as RowId, 
               New.Ver as Ver,                
			   case 
			   When New.HistoryMuvelet_Id = '0' then 'Létrehozás'	
			   When New.HistoryMuvelet_Id = '3' then 'Érvénytelenítés'
			   WHEN New.HistoryMuvelet_Id = 1 AND Old.Allapot = '1' and New.Allapot = '2'  then 'Aláírás'			   
			   WHEN New.HistoryMuvelet_Id = 1 AND Old.Allapot = '2' and New.Allapot = '1'  then 'Aláírás visszavonása'
			   WHEN New.HistoryMuvelet_Id = 1 AND Old.Allapot != '3'  and New.Allapot = '3' then 'Aláírás Visszautasítása'
			   end as Operation,
               'Allapot' as ColumnName,               cast(Old.Allapot as nvarchar(99)) as OldValue,
               cast(New.Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratAlairokHistory Old
         inner join EREC_IratAlairokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Allapot != New.Allapot 
            and Old.Id = New.Id
			and Old.Obj_Id like @Obj_Id AND New.HistoryMuvelet_Id != 0
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      	 
)  order by ExecutionTime DESC  
end


GO
