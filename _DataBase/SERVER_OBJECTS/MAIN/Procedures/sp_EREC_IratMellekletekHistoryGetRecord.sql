IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratMellekletekHistoryGetRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IratMellekletekHistoryGetRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratMellekletekHistoryGetRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IratMellekletekHistoryGetRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IratMellekletekHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratMellekletekHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_IratMellekletekHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratMellekletekHistory Old
         inner join EREC_IratMellekletekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIrat_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_IraIratokAzonosito(Old.IraIrat_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_IraIratokAzonosito(New.IraIrat_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratMellekletekHistory Old
         inner join EREC_IratMellekletekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IraIrat_Id != New.IraIrat_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_IraIratok FTOld on FTOld.Id = Old.IraIrat_Id and FTOld.Ver = Old.Ver
         left join EREC_IraIratok FTNew on FTNew.Id = New.IraIrat_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldMellekletek_Id' as ColumnName,               
               cast(Old.KuldMellekletek_Id as nvarchar(99)) as OldValue,
               cast(New.KuldMellekletek_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratMellekletekHistory Old
         inner join EREC_IratMellekletekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KuldMellekletek_Id != New.KuldMellekletek_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AdathordozoTipus' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratMellekletekHistory Old
         inner join EREC_IratMellekletekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AdathordozoTipus != New.AdathordozoTipus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'ADATHORDOZO_TIPUSA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.AdathordozoTipus
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.AdathordozoTipus      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Megjegyzes' as ColumnName,               
               cast(Old.Megjegyzes as nvarchar(99)) as OldValue,
               cast(New.Megjegyzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratMellekletekHistory Old
         inner join EREC_IratMellekletekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Megjegyzes != New.Megjegyzes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SztornirozasDat' as ColumnName,               
               cast(Old.SztornirozasDat as nvarchar(99)) as OldValue,
               cast(New.SztornirozasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratMellekletekHistory Old
         inner join EREC_IratMellekletekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SztornirozasDat != New.SztornirozasDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MennyisegiEgyseg' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratMellekletekHistory Old
         inner join EREC_IratMellekletekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MennyisegiEgyseg != New.MennyisegiEgyseg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'MENNYISEGI_EGYSEG'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.MennyisegiEgyseg
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.MennyisegiEgyseg      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Mennyiseg' as ColumnName,               
               cast(Old.Mennyiseg as nvarchar(99)) as OldValue,
               cast(New.Mennyiseg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratMellekletekHistory Old
         inner join EREC_IratMellekletekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Mennyiseg != New.Mennyiseg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BarCode' as ColumnName,               
               cast(Old.BarCode as nvarchar(99)) as OldValue,
               cast(New.BarCode as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratMellekletekHistory Old
         inner join EREC_IratMellekletekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BarCode != New.BarCode 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
end


GO
