IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratMellekletekUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IratMellekletekUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratMellekletekUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IratMellekletekUpdate] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IratMellekletekUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @IraIrat_Id     uniqueidentifier  = null,         
             @KuldMellekletek_Id     uniqueidentifier  = null,         
             @AdathordozoTipus     nvarchar(64)  = null,         
             @Megjegyzes     Nvarchar(400)  = null,         
             @SztornirozasDat     datetime  = null,         
             @MennyisegiEgyseg     nvarchar(64)  = null,         
             @Mennyiseg     int  = null,         
             @BarCode     Nvarchar(100)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_IraIrat_Id     uniqueidentifier         
     DECLARE @Act_KuldMellekletek_Id     uniqueidentifier         
     DECLARE @Act_AdathordozoTipus     nvarchar(64)         
     DECLARE @Act_Megjegyzes     Nvarchar(400)         
     DECLARE @Act_SztornirozasDat     datetime         
     DECLARE @Act_MennyisegiEgyseg     nvarchar(64)         
     DECLARE @Act_Mennyiseg     int         
     DECLARE @Act_BarCode     Nvarchar(100)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_IraIrat_Id = IraIrat_Id,
     @Act_KuldMellekletek_Id = KuldMellekletek_Id,
     @Act_AdathordozoTipus = AdathordozoTipus,
     @Act_Megjegyzes = Megjegyzes,
     @Act_SztornirozasDat = SztornirozasDat,
     @Act_MennyisegiEgyseg = MennyisegiEgyseg,
     @Act_Mennyiseg = Mennyiseg,
     @Act_BarCode = BarCode,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_IratMellekletek
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zĂˇrolĂˇs ellenĹ‘rzĂ©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/IraIrat_Id')=1
         SET @Act_IraIrat_Id = @IraIrat_Id
   IF @UpdatedColumns.exist('/root/KuldMellekletek_Id')=1
         SET @Act_KuldMellekletek_Id = @KuldMellekletek_Id
   IF @UpdatedColumns.exist('/root/AdathordozoTipus')=1
         SET @Act_AdathordozoTipus = @AdathordozoTipus
   IF @UpdatedColumns.exist('/root/Megjegyzes')=1
         SET @Act_Megjegyzes = @Megjegyzes
   IF @UpdatedColumns.exist('/root/SztornirozasDat')=1
         SET @Act_SztornirozasDat = @SztornirozasDat
   IF @UpdatedColumns.exist('/root/MennyisegiEgyseg')=1
         SET @Act_MennyisegiEgyseg = @MennyisegiEgyseg
   IF @UpdatedColumns.exist('/root/Mennyiseg')=1
         SET @Act_Mennyiseg = @Mennyiseg
   IF @UpdatedColumns.exist('/root/BarCode')=1
         SET @Act_BarCode = @BarCode
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_IratMellekletek
SET
     IraIrat_Id = @Act_IraIrat_Id,
     KuldMellekletek_Id = @Act_KuldMellekletek_Id,
     AdathordozoTipus = @Act_AdathordozoTipus,
     Megjegyzes = @Act_Megjegyzes,
     SztornirozasDat = @Act_SztornirozasDat,
     MennyisegiEgyseg = @Act_MennyisegiEgyseg,
     Mennyiseg = @Act_Mennyiseg,
     BarCode = @Act_BarCode,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_IratMellekletek',@Id
					,'EREC_IratMellekletekHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH




GO
