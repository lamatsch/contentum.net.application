IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratMetaDefinicioGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IratMetaDefinicioGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratMetaDefinicioGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IratMetaDefinicioGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IratMetaDefinicioGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IratMetaDefinicio.Id,
	   EREC_IratMetaDefinicio.Org,
	   EREC_IratMetaDefinicio.Ugykor_Id,
	   EREC_IratMetaDefinicio.UgykorKod,
	   EREC_IratMetaDefinicio.Ugytipus,
	   EREC_IratMetaDefinicio.UgytipusNev,
	   EREC_IratMetaDefinicio.EljarasiSzakasz,
	   EREC_IratMetaDefinicio.Irattipus,
	   EREC_IratMetaDefinicio.GeneraltTargy,
	   EREC_IratMetaDefinicio.Rovidnev,
	   EREC_IratMetaDefinicio.UgyFajta,
	   EREC_IratMetaDefinicio.UgyiratIntezesiIdo,
	   EREC_IratMetaDefinicio.Idoegyseg,
	   EREC_IratMetaDefinicio.UgyiratIntezesiIdoKotott,
	   EREC_IratMetaDefinicio.UgyiratHataridoKitolas,
	   EREC_IratMetaDefinicio.Ver,
	   EREC_IratMetaDefinicio.Note,
	   EREC_IratMetaDefinicio.Stat_id,
	   EREC_IratMetaDefinicio.ErvKezd,
	   EREC_IratMetaDefinicio.ErvVege,
	   EREC_IratMetaDefinicio.Letrehozo_id,
	   EREC_IratMetaDefinicio.LetrehozasIdo,
	   EREC_IratMetaDefinicio.Modosito_id,
	   EREC_IratMetaDefinicio.ModositasIdo,
	   EREC_IratMetaDefinicio.Zarolo_id,
	   EREC_IratMetaDefinicio.ZarolasIdo,
	   EREC_IratMetaDefinicio.Tranz_id,
	   EREC_IratMetaDefinicio.UIAccessLog_id
	   from 
		 EREC_IratMetaDefinicio as EREC_IratMetaDefinicio 
	   where
		 EREC_IratMetaDefinicio.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
