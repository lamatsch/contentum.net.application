IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratMetaDefinicioGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IratMetaDefinicioGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratMetaDefinicioGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IratMetaDefinicioGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IratMetaDefinicioGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IratMetaDefinicio.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
         
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	    EREC_IratMetaDefinicio.Id,
	   EREC_IratMetaDefinicio.Ugykor_Id,
	   EREC_IratMetaDefinicio.UgykorKod,
	   EREC_IraIrattariTetelek.Nev as UgykorKodNev,
	dbo.fn_MergeIrattariTetelSzam(EREC_AgazatiJelek.Kod,EREC_IraIrattariTetelek.IrattariTetelszam,EREC_IraIrattariTetelek.MegorzesiIdo,EREC_IraIrattariTetelek.IrattariJel,''' + cast(@Org as NVarChar(40)) + ''')
			as Merge_IrattariTetelszam,
	EREC_AgazatiJelek.Kod as AgazatiJel_Kod,
	EREC_AgazatiJelek.Nev as AgazatiJel_Nev,
	   EREC_IratMetaDefinicio.Ugytipus,
	   EREC_IratMetaDefinicio.UgytipusNev,
	   EREC_IratMetaDefinicio.EljarasiSzakasz as EljarasiSzakaszKod,
	   KRT_Kodtarak_EljarasiSzakasz.Nev as EljarasiSzakasz,
	   EREC_IratMetaDefinicio.Irattipus as Irattipus,
	   KRT_Kodtarak_Irattipus.Nev as IrattipusNev,
	   EREC_IratMetaDefinicio.GeneraltTargy,
	   EREC_IratMetaDefinicio.Rovidnev,
	   EREC_IratMetaDefinicio.UgyiratIntezesiIdo,
       EREC_IratMetaDefinicio.Idoegyseg,
       KRT_Kodtarak_Idoegyseg.Nev as IdoegysegNev,
	   EREC_IratMetaDefinicio.UgyiratIntezesiIdoKotott,
	   EREC_IratMetaDefinicio.UgyiratHataridoKitolas,
	   EREC_IratMetaDefinicio.Ver,
	   EREC_IratMetaDefinicio.Note,
	   EREC_IratMetaDefinicio.Stat_id,
	   EREC_IratMetaDefinicio.ErvKezd,
	   EREC_IratMetaDefinicio.ErvVege,
	   EREC_IratMetaDefinicio.Letrehozo_id,
	   EREC_IratMetaDefinicio.LetrehozasIdo,
	   EREC_IratMetaDefinicio.Modosito_id,
	   EREC_IratMetaDefinicio.ModositasIdo,
	   EREC_IratMetaDefinicio.Zarolo_id,
	   EREC_IratMetaDefinicio.ZarolasIdo,
	   EREC_IratMetaDefinicio.Tranz_id,
	KRT_Kodtarak_Szignalas.Nev as SzignalasTipusa,
	KRT_Csoportok.Nev as JavasoltFelelos,
	EREC_SzignalasiJegyzekek.SzignalasTipusa as SzignalasTipusa_Kod,
	EREC_SzignalasiJegyzekek.Csoport_Id_Felelos as SzignalasiJegyzek_JavasoltFelelos,
'

set @sqlcmd = @sqlcmd +
	'(select imd.Id FROM EREC_IratMetaDefinicio imd WHERE EREC_IratMetaDefinicio.Ugykor_Id = imd.Ugykor_Id
       and (
         (EREC_IratMetaDefinicio.Irattipus is not null and imd.Ugytipus=EREC_IratMetaDefinicio.Ugytipus and imd.EljarasiSzakasz=EREC_IratMetaDefinicio.EljarasiSzakasz and imd.Irattipus is null)
         OR
         (EREC_IratMetaDefinicio.Irattipus is null and EREC_IratMetaDefinicio.EljarasiSzakasz is not null and imd.Ugytipus=EREC_IratMetaDefinicio.Ugytipus and imd.EljarasiSzakasz is null and imd.Irattipus is null)
          OR
         (EREC_IratMetaDefinicio.Irattipus is null and EREC_IratMetaDefinicio.EljarasiSzakasz is null and EREC_IratMetaDefinicio.Ugytipus is not null and imd.Ugytipus is null and imd.EljarasiSzakasz is null and imd.Irattipus is null)
           )
		and imd.Org=''' + cast(@Org as NVarChar(40)) + '''
       and getdate() between imd.ErvKezd AND imd.ErvVege
    ) AS IratMetaDefinicio_Id_Szulo
   from 
	EREC_IratMetaDefinicio as EREC_IratMetaDefinicio  
	 left join EREC_SzignalasiJegyzekek 
			on EREC_SzignalasiJegyzekek.UgykorTargykor_Id = EREC_IratMetaDefinicio.Id 
	 join EREC_IraIrattariTetelek 
			on EREC_IratMetaDefinicio.Ugykor_Id = EREC_IraIrattariTetelek.Id and EREC_IraIrattariTetelek.Org=''' + CAST(@Org as NVarChar(40)) + '''
	left join EREC_AgazatiJelek as EREC_AgazatiJelek
			ON EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_AgazatiJelek.Id
	 left join KRT_Csoportok on EREC_SzignalasiJegyzekek.Csoport_Id_Felelos = KRT_Csoportok.Id

	 left join KRT_Kodcsoportok as KRT_Kodcsoportok_Szignalas on KRT_Kodcsoportok_Szignalas.Kod = ''SZIGNALAS_TIPUSA''
	 left join KRT_Kodtarak as KRT_Kodtarak_Szignalas on KRT_Kodtarak_Szignalas.kodcsoport_id = KRT_Kodcsoportok_Szignalas.Id and EREC_SzignalasiJegyzekek.SzignalasTipusa = KRT_Kodtarak_Szignalas.Kod and KRT_Kodtarak_Szignalas.Org=''' + CAST(@Org as NVarChar(40)) + '''

	 left join KRT_Kodcsoportok as KRT_Kodcsoportok_EljarasiSzakasz on KRT_Kodcsoportok_EljarasiSzakasz.Kod = ''ELJARASI_SZAKASZ''
	 left join KRT_Kodtarak as KRT_Kodtarak_EljarasiSzakasz on KRT_Kodtarak_EljarasiSzakasz.kodcsoport_id = KRT_Kodcsoportok_EljarasiSzakasz.Id and EREC_IratMetaDefinicio.EljarasiSzakasz = KRT_Kodtarak_EljarasiSzakasz.Kod and KRT_KodTarak_EljarasiSzakasz.Org=''' + CAST(@Org as NVarChar(40)) + '''

	 left join KRT_Kodcsoportok as KRT_Kodcsoportok_Irattipus on KRT_Kodcsoportok_Irattipus.Kod = ''IRATTIPUS''
	 left join KRT_Kodtarak as KRT_Kodtarak_Irattipus on KRT_Kodtarak_Irattipus.kodcsoport_id = KRT_Kodcsoportok_Irattipus.Id and EREC_IratMetaDefinicio.Irattipus = KRT_Kodtarak_Irattipus.Kod and KRT_KodTarak_Irattipus.Org=''' + CAST(@Org as NVarChar(40)) + '''

	 left join KRT_Kodcsoportok as KRT_Kodcsoportok_Idoegyseg on KRT_Kodcsoportok_Idoegyseg.Kod = ''IDOEGYSEG''
	 left join KRT_Kodtarak as KRT_Kodtarak_Idoegyseg on KRT_Kodtarak_Idoegyseg.kodcsoport_id = KRT_Kodcsoportok_Idoegyseg.Id and EREC_IratMetaDefinicio.Idoegyseg = KRT_Kodtarak_Idoegyseg.Kod and KRT_KodTarak_Idoegyseg.Org=''' + CAST(@Org as NVarChar(40)) + '''
where EREC_IratMetaDefinicio.Org=''' + cast(@Org as NVarChar(40)) + '''
and EREC_AgazatiJelek.Org=''' + CAST(@Org as NVarChar(40)) + '''
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

   --select @sqlcmd

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
