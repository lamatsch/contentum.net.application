IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratMetaDefinicioUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IratMetaDefinicioUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratMetaDefinicioUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IratMetaDefinicioUpdate] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IratMetaDefinicioUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Org     uniqueidentifier  = null,         
             @Ugykor_Id     uniqueidentifier  = null,         
             @UgykorKod     Nvarchar(10)  = null,         
             @Ugytipus     nvarchar(64)  = null,         
             @UgytipusNev     Nvarchar(400)  = null,         
             @EljarasiSzakasz     nvarchar(64)  = null,         
             @Irattipus     nvarchar(64)  = null,         
             @GeneraltTargy     Nvarchar(400)  = null,         
             @Rovidnev     Nvarchar(100)  = null,         
             @UgyFajta     nvarchar(64)  = null,         
             @UgyiratIntezesiIdo     int  = null,         
             @Idoegyseg     nvarchar(64)  = null,         
             @UgyiratIntezesiIdoKotott     char(1)  = null,         
             @UgyiratHataridoKitolas     char(1)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Org     uniqueidentifier         
     DECLARE @Act_Ugykor_Id     uniqueidentifier         
     DECLARE @Act_UgykorKod     Nvarchar(10)         
     DECLARE @Act_Ugytipus     nvarchar(64)         
     DECLARE @Act_UgytipusNev     Nvarchar(400)         
     DECLARE @Act_EljarasiSzakasz     nvarchar(64)         
     DECLARE @Act_Irattipus     nvarchar(64)         
     DECLARE @Act_GeneraltTargy     Nvarchar(400)         
     DECLARE @Act_Rovidnev     Nvarchar(100)         
     DECLARE @Act_UgyFajta     nvarchar(64)         
     DECLARE @Act_UgyiratIntezesiIdo     int         
     DECLARE @Act_Idoegyseg     nvarchar(64)         
     DECLARE @Act_UgyiratIntezesiIdoKotott     char(1)         
     DECLARE @Act_UgyiratHataridoKitolas     char(1)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Org = Org,
     @Act_Ugykor_Id = Ugykor_Id,
     @Act_UgykorKod = UgykorKod,
     @Act_Ugytipus = Ugytipus,
     @Act_UgytipusNev = UgytipusNev,
     @Act_EljarasiSzakasz = EljarasiSzakasz,
     @Act_Irattipus = Irattipus,
     @Act_GeneraltTargy = GeneraltTargy,
     @Act_Rovidnev = Rovidnev,
     @Act_UgyFajta = UgyFajta,
     @Act_UgyiratIntezesiIdo = UgyiratIntezesiIdo,
     @Act_Idoegyseg = Idoegyseg,
     @Act_UgyiratIntezesiIdoKotott = UgyiratIntezesiIdoKotott,
     @Act_UgyiratHataridoKitolas = UgyiratHataridoKitolas,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_IratMetaDefinicio
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Org')=1
         SET @Act_Org = @Org
   IF @UpdatedColumns.exist('/root/Ugykor_Id')=1
         SET @Act_Ugykor_Id = @Ugykor_Id
   IF @UpdatedColumns.exist('/root/UgykorKod')=1
         SET @Act_UgykorKod = @UgykorKod
   IF @UpdatedColumns.exist('/root/Ugytipus')=1
         SET @Act_Ugytipus = @Ugytipus
   IF @UpdatedColumns.exist('/root/UgytipusNev')=1
         SET @Act_UgytipusNev = @UgytipusNev
   IF @UpdatedColumns.exist('/root/EljarasiSzakasz')=1
         SET @Act_EljarasiSzakasz = @EljarasiSzakasz
   IF @UpdatedColumns.exist('/root/Irattipus')=1
         SET @Act_Irattipus = @Irattipus
   IF @UpdatedColumns.exist('/root/GeneraltTargy')=1
         SET @Act_GeneraltTargy = @GeneraltTargy
   IF @UpdatedColumns.exist('/root/Rovidnev')=1
         SET @Act_Rovidnev = @Rovidnev
   IF @UpdatedColumns.exist('/root/UgyFajta')=1
         SET @Act_UgyFajta = @UgyFajta
   IF @UpdatedColumns.exist('/root/UgyiratIntezesiIdo')=1
         SET @Act_UgyiratIntezesiIdo = @UgyiratIntezesiIdo
   IF @UpdatedColumns.exist('/root/Idoegyseg')=1
         SET @Act_Idoegyseg = @Idoegyseg
   IF @UpdatedColumns.exist('/root/UgyiratIntezesiIdoKotott')=1
         SET @Act_UgyiratIntezesiIdoKotott = @UgyiratIntezesiIdoKotott
   IF @UpdatedColumns.exist('/root/UgyiratHataridoKitolas')=1
         SET @Act_UgyiratHataridoKitolas = @UgyiratHataridoKitolas
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_IratMetaDefinicio
SET
     Org = @Act_Org,
     Ugykor_Id = @Act_Ugykor_Id,
     UgykorKod = @Act_UgykorKod,
     Ugytipus = @Act_Ugytipus,
     UgytipusNev = @Act_UgytipusNev,
     EljarasiSzakasz = @Act_EljarasiSzakasz,
     Irattipus = @Act_Irattipus,
     GeneraltTargy = @Act_GeneraltTargy,
     Rovidnev = @Act_Rovidnev,
     UgyFajta = @Act_UgyFajta,
     UgyiratIntezesiIdo = @Act_UgyiratIntezesiIdo,
     Idoegyseg = @Act_Idoegyseg,
     UgyiratIntezesiIdoKotott = @Act_UgyiratIntezesiIdoKotott,
     UgyiratHataridoKitolas = @Act_UgyiratHataridoKitolas,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_IratMetaDefinicio',@Id
					,'EREC_IratMetaDefinicioHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
