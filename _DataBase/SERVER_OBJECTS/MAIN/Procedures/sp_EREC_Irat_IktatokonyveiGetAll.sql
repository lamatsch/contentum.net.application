IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Irat_IktatokonyveiGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_Irat_IktatokonyveiGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Irat_IktatokonyveiGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_Irat_IktatokonyveiGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_Irat_IktatokonyveiGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_Irat_Iktatokonyvei.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_Irat_Iktatokonyvei.Id,
	   EREC_Irat_Iktatokonyvei.IraIktatokonyv_Id,
	   EREC_Irat_Iktatokonyvei.Csoport_Id_Iktathat,
	   EREC_Irat_Iktatokonyvei.Ver,
	   EREC_Irat_Iktatokonyvei.Note,
	   EREC_Irat_Iktatokonyvei.Stat_id,
	   EREC_Irat_Iktatokonyvei.ErvKezd,
	   EREC_Irat_Iktatokonyvei.ErvVege,
	   EREC_Irat_Iktatokonyvei.Letrehozo_id,
	   EREC_Irat_Iktatokonyvei.LetrehozasIdo,
	   EREC_Irat_Iktatokonyvei.Modosito_id,
	   EREC_Irat_Iktatokonyvei.ModositasIdo,
	   EREC_Irat_Iktatokonyvei.Zarolo_id,
	   EREC_Irat_Iktatokonyvei.ZarolasIdo,
	   EREC_Irat_Iktatokonyvei.Tranz_id,
	   EREC_Irat_Iktatokonyvei.UIAccessLog_id  
   from 
     EREC_Irat_Iktatokonyvei as EREC_Irat_Iktatokonyvei      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
