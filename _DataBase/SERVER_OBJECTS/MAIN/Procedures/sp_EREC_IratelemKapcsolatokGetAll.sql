IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratelemKapcsolatokGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IratelemKapcsolatokGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratelemKapcsolatokGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IratelemKapcsolatokGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IratelemKapcsolatokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IratelemKapcsolatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_IratelemKapcsolatok.Id,
	   EREC_IratelemKapcsolatok.Melleklet_Id,
	   EREC_IratelemKapcsolatok.Csatolmany_Id,
	   EREC_IratelemKapcsolatok.Ver,
	   EREC_IratelemKapcsolatok.Note,
	   EREC_IratelemKapcsolatok.Stat_id,
	   EREC_IratelemKapcsolatok.ErvKezd,
	   EREC_IratelemKapcsolatok.ErvVege,
	   EREC_IratelemKapcsolatok.Letrehozo_id,
	   EREC_IratelemKapcsolatok.LetrehozasIdo,
	   EREC_IratelemKapcsolatok.Modosito_id,
	   EREC_IratelemKapcsolatok.ModositasIdo,
	   EREC_IratelemKapcsolatok.Zarolo_id,
	   EREC_IratelemKapcsolatok.ZarolasIdo,
	   EREC_IratelemKapcsolatok.Tranz_id,
	   EREC_IratelemKapcsolatok.UIAccessLog_id  
   from 
     EREC_IratelemKapcsolatok as EREC_IratelemKapcsolatok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
