IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratelemKapcsolatokHistoryGetAllRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IratelemKapcsolatokHistoryGetAllRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IratelemKapcsolatokHistoryGetAllRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IratelemKapcsolatokHistoryGetAllRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IratelemKapcsolatokHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_IratelemKapcsolatokHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratelemKapcsolatokHistory Old
         inner join EREC_IratelemKapcsolatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Melleklet_Id' as ColumnName,               cast(Old.Melleklet_Id as nvarchar(99)) as OldValue,
               cast(New.Melleklet_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratelemKapcsolatokHistory Old
         inner join EREC_IratelemKapcsolatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Melleklet_Id != New.Melleklet_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csatolmany_Id' as ColumnName,               cast(Old.Csatolmany_Id as nvarchar(99)) as OldValue,
               cast(New.Csatolmany_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IratelemKapcsolatokHistory Old
         inner join EREC_IratelemKapcsolatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csatolmany_Id != New.Csatolmany_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end


GO
