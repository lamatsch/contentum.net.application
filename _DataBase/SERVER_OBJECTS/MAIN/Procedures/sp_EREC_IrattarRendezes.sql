IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattarRendezes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IrattarRendezes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattarRendezes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IrattarRendezes] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IrattarRendezes]
        @UgyiratIds						NVARCHAR(MAX)    ,
		@ExecutorUserId				uniqueidentifier,
		@ExecutionTime					datetime ,
		@IrattarId				uniqueidentifier,
		@IrattariHelyErtek     Nvarchar(100)
as

BEGIN TRY
	update EREC_UgyUgyiratok
	set EREC_UgyUgyiratok.IrattariHely = @IrattariHelyErtek,
		EREC_UgyUgyiratok.IrattarId = @IrattarId,
		EREC_UgyUgyiratok.ModositasIdo = @ExecutionTime,
		EREC_UgyUgyiratok.Modosito_id = @ExecutorUserId
	where EREC_UgyUgyiratok.Id IN (SELECT convert(uniqueidentifier, Value) from dbo.fn_Split(@UgyiratIds,','))

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InvalidateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH



GO
