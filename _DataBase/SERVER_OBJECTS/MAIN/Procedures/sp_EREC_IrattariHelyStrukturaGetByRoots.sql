IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariHelyStrukturaGetByRoots]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IrattariHelyStrukturaGetByRoots]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariHelyStrukturaGetByRoots]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IrattariHelyStrukturaGetByRoots] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IrattariHelyStrukturaGetByRoots]
  @RootId uniqueidentifier,
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	
  ;with c as 
(--param
  select t1.id, 
         t1.szuloid as szuloid, 
		 t1.ertek,
         t1.vonalkod
  from EREC_IrattariHelyek t1
  where t1.id = @RootId
	and t1.ErvKezd<=getdate() and t1.ErvVege>=getdate()
  union all

  select c1.id, 
         c1.szuloid as szuloid,
		 c1.ertek,
         c1.vonalkod
  from EREC_IrattariHelyek c1
    join c p on p.id = c1.szuloid
  where c1.ErvKezd<=getdate() and c1.ErvVege>=getdate()
)
select id,szuloid,ertek,vonalkod
from c 
where id != @RootId
order by c.szuloid;
   

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
