IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariHelyekGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IrattariHelyekGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariHelyekGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IrattariHelyekGet] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IrattariHelyekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IrattariHelyek.Id,
	   EREC_IrattariHelyek.Ertek,
	   EREC_IrattariHelyek.Nev,
	   EREC_IrattariHelyek.Vonalkod,
	   EREC_IrattariHelyek.SzuloId,
	   EREC_IrattariHelyek.Ver,
	   EREC_IrattariHelyek.Note,
	   EREC_IrattariHelyek.Stat_id,
	   EREC_IrattariHelyek.ErvKezd,
	   EREC_IrattariHelyek.ErvVege,
	   EREC_IrattariHelyek.Letrehozo_id,
	   EREC_IrattariHelyek.LetrehozasIdo,
	   EREC_IrattariHelyek.Modosito_id,
	   EREC_IrattariHelyek.ModositasIdo,
	   EREC_IrattariHelyek.Zarolo_id,
	   EREC_IrattariHelyek.ZarolasIdo,
	   EREC_IrattariHelyek.Tranz_id,
	   EREC_IrattariHelyek.UIAccessLog_id,
	   -- CR3246
--	   EREC_IrattariHelyek.IrattarTipus
	   EREC_IrattariHelyek.Felelos_Csoport_Id
	   from 
		 EREC_IrattariHelyek as EREC_IrattariHelyek 
	   where
		 EREC_IrattariHelyek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end

GO
