IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariHelyekGetAllRoots]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IrattariHelyekGetAllRoots]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariHelyekGetAllRoots]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IrattariHelyekGetAllRoots] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IrattariHelyekGetAllRoots]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IrattariHelyek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_IrattariHelyek.Id,
	   EREC_IrattariHelyek.Ertek,
	   EREC_IrattariHelyek.Vonalkod,
	   EREC_IrattariHelyek.SzuloId,
	   case
		when id not in (
			SELECT DISTINCT szuloid FROM EREC_IrattariHelyek WHERE SzuloId IS NOT NULL and EREC_IrattariHelyek.ErvKezd<=getdate() and EREC_IrattariHelyek.ErvVege>=getdate()) then ''0''
			else ''1'' 
	   end isExistChild
   from 
     EREC_IrattariHelyek as EREC_IrattariHelyek 
	 where  EREC_IrattariHelyek.SzuloId IS NULL and EREC_IrattariHelyek.ErvKezd<=getdate() and EREC_IrattariHelyek.ErvVege>=getdate()
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
