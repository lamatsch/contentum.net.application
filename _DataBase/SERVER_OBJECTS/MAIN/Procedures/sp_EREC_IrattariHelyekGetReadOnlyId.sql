IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariHelyekGetReadOnlyId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IrattariHelyekGetReadOnlyId]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariHelyekGetReadOnlyId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IrattariHelyekGetReadOnlyId] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IrattariHelyekGetReadOnlyId]
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
    
	;with C as
(
  select ID,
         Ertek,
        Convert(nvarchar(1000),'')  as ParentNames
		,
		Convert(nvarchar(1000),'')  as ParentIds
  from EREC_IrattariHelyek
  where szuloid is null and ErvKezd<=getdate() and ErvVege >=GETDATE()
  
  union all
  select T.ID,
         T.Ertek,
         CONVERT(nvarchar(1000),C.ParentNames + '/' +C.Ertek)
		 ,
		 CONVERT(nvarchar(1000),Convert(nvarchar(1000),C.ParentIds) + '/' +Convert(nvarchar(1000),C.Id))
  from EREC_IrattariHelyek as T         
    inner join C
      on C.ID = T.Szuloid
   where ErvKezd<=getdate() and ErvVege >=GETDATE()
)      
select C.IdPath from
(
select distinct c.Id,
	--case
	--	when C.ParentNames like '/%' then stuff(C.ParentNames+'/'+C.Ertek, 1, 1, '')
	--	else C.Ertek
 --   end Ertek,
	case
		when C.ParentIds like '/%' then stuff(C.ParentIds+'/'+Convert(nvarchar(1000),C.Id), 1, 1, '')
		else Convert(nvarchar(1000),C.Id)
    end IdPath
from C
where C.Id NOT IN (
    SELECT DISTINCT szuloid FROM EREC_IrattariHelyek WHERE SzuloId IS NOT NULL)
) C
inner join 
( SELECT * from 
EREC_UgyUgyiratok where  EREC_UgyUgyiratok.IrattariHely is not null and ErvKezd<=getdate() and ErvVege >=GETDATE())EREC_UgyUgyiratok on EREC_UgyUgyiratok.IrattarId = C.Id
where EREC_UgyUgyiratok.Id is not null 
group by c.IdPath


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
