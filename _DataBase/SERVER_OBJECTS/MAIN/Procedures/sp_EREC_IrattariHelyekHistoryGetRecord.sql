IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariHelyekHistoryGetRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IrattariHelyekHistoryGetRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariHelyekHistoryGetRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IrattariHelyekHistoryGetRecord] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IrattariHelyekHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_IrattariHelyekHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariHelyekHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_IrattariHelyekHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariHelyekHistory Old
         inner join EREC_IrattariHelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ertek' as ColumnName,               
               cast(Old.Ertek as nvarchar(99)) as OldValue,
               cast(New.Ertek as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariHelyekHistory Old
         inner join EREC_IrattariHelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Ertek != New.Ertek 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Nev' as ColumnName,               
               cast(Old.Nev as nvarchar(99)) as OldValue,
               cast(New.Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariHelyekHistory Old
         inner join EREC_IrattariHelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Nev != New.Nev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Vonalkod' as ColumnName,               
               cast(Old.Vonalkod as nvarchar(99)) as OldValue,
               cast(New.Vonalkod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariHelyekHistory Old
         inner join EREC_IrattariHelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Vonalkod != New.Vonalkod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuloId' as ColumnName,               
               cast(Old.SzuloId as nvarchar(99)) as OldValue,
               cast(New.SzuloId as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariHelyekHistory Old
         inner join EREC_IrattariHelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SzuloId != New.SzuloId 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end


GO
