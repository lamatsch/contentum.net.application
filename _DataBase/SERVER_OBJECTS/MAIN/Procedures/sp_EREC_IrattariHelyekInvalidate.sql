IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariHelyekInvalidate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IrattariHelyekInvalidate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariHelyekInvalidate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IrattariHelyekInvalidate] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IrattariHelyekInvalidate]
        @Id							uniqueidentifier,
		@ExecutorUserId				uniqueidentifier,
		@ExecutionTime					datetime 	

as

BEGIN TRY
BEGIN TRANSACTION InvalidateTransaction

  
	set nocount on
   
   SET @ExecutionTime = getdate()

	-- Zarolas ellenorzese:
	DECLARE @IsLockedByOtherUser BIT
	exec sp_GetLockingInfo 'EREC_IrattariHelyek',@Id,@ExecutorUserId,@IsLockedByOtherUser OUTPUT,null

	if (@IsLockedByOtherUser = 0)
	BEGIN
   
      DECLARE @Act_ErvKezd datetime
      DECLARE @Act_ErvVege datetime
      DECLARE @Act_Ver int
      DECLARE @NodeId uniqueidentifier
	  DECLARE @BarcodeId uniqueidentifier
      select
         @Act_ErvKezd = ErvKezd,
         @Act_ErvVege = ErvVege,
         @Act_Ver = Ver
      from EREC_IrattariHelyek
      where Id = @Id
      
      if @@rowcount = 0
	   begin
	    	RAISERROR('[50602]',16,1)
	   end
      
      if @Act_ErvVege <= @ExecutionTime
      begin
         -- mĂˇr Ă©rvĂ©nytelenĂ­tve van
         RAISERROR('[50603]',16,1)
      end
      
      IF @Act_Ver is null
	   	SET @Act_Ver = 2	
      ELSE
	   	SET @Act_Ver = @Act_Ver+1
      
      if @Act_ErvKezd > @ExecutionTime
      begin
      
         -- ha a jĂ¶vĹ‘ben kezdĹ‘dĂ¶tt volna el, az ErvKezd-et is beĂˇllĂ­tjuk
      
		   UPDATE EREC_IrattariHelyek
		   SET		 					
		   		 ErvVege = @ExecutionTime,
                ErvKezd = @ExecutionTime,
                Ver     = @Act_Ver
		   WHERE
		         Id = @Id;
		
      end
      else
      begin
   
		   UPDATE EREC_IrattariHelyek
		   SET		 					
		   		 ErvVege = @ExecutionTime,
                 Ver = @Act_Ver
		   WHERE
		         Id = @Id;
        
			
      end
	  if @@rowcount != 1
		begin
			RAISERROR('[50401]',16,1)
		end
		else
		begin   
		   if (select count(KRT_Barkodok.Id) from KRT_Barkodok where KRT_Barkodok.Obj_Id=@Id)=1
		   begin
			   update KRT_Barkodok 
					set Obj_Id = null,
					    Obj_type = null,
					    Allapot = 'S',
						Ver = Ver+1,
						ModositasIdo = @ExecutionTime,
						Modosito_id = @ExecutorUserId
				where KRT_Barkodok.Obj_Id=@Id
			
				SET @BarcodeId = (select KRT_Barkodok.Id from KRT_Barkodok where KRT_Barkodok.Obj_Id=@Id)
				exec sp_LogRecordToHistory 'KRT_Barkodok',@BarcodeId
								,'KRT_BarkodokHistory',1,@ExecutorUserId,@ExecutionTime   
			end
			/* History Log */
		   exec sp_LogRecordToHistory 'EREC_IrattariHelyek',@Id
							,'EREC_IrattariHelyekHistory',1,@ExecutorUserId,@ExecutionTime   
		end
    
	 
	--gyerekek Ă©rĂ©vnytelenĂ­tĂ©se
		DECLARE c CURSOR  for 
		with cc as 
			(--param
			  select t1.id, 
					 t1.szuloid as szuloid, 
					 t1.ertek,
					 t1.vonalkod,
					 t1.ErvVege,
					 t1.Ver
			  from EREC_IrattariHelyek t1
			  where t1.id = @Id
				--and t1.ErvKezd<=getdate() and t1.ErvVege>=getdate()
			  union all

			  select c1.id, 
					 c1.szuloid as szuloid,
					 c1.ertek,
					 c1.vonalkod,
					 c1.ErvVege,
					 c1.Ver
			  from EREC_IrattariHelyek c1
				join cc p on p.id = c1.szuloid
			  where c1.ErvKezd<=getdate() and c1.ErvVege>=getdate()
			)  select id NodeId from cc where id!=@Id;
			
			-- Open the cursor
			OPEN c

			FETCH NEXT FROM c INTO @NodeId
			
			WHILE (@@FETCH_STATUS = 0)
			BEGIN
				update EREC_IrattariHelyek
				set 
					ErvVege = @ExecutionTime,
					ErvKezd = @ExecutionTime,
					Ver = Ver+1
				where id = @NodeId and ErvKezd>@ExecutionTime
				update EREC_IrattariHelyek
				set 
					ErvVege = @ExecutionTime,
					ErvKezd = @ExecutionTime,
					Ver = Ver+1
				where id = @NodeId and ErvKezd<=@ExecutionTime
				if (select count(KRT_Barkodok.Id) from KRT_Barkodok where KRT_Barkodok.Obj_Id=@NodeId)=1
				begin
					update KRT_Barkodok 
					set Obj_Id = null,
					    Obj_type = null,
					    Allapot = 'S',
						Ver = Ver+1,
						ModositasIdo = @ExecutionTime,
						Modosito_id = @ExecutorUserId
					where KRT_Barkodok.Obj_Id=@NodeId
				
					SET @BarcodeId = (select KRT_Barkodok.Id from KRT_Barkodok where KRT_Barkodok.Obj_Id=@NodeId)
					exec sp_LogRecordToHistory 'KRT_Barkodok',@BarcodeId
								,'KRT_BarkodokHistory',1,@ExecutorUserId,@ExecutionTime
				end
				-- do other stuff
				exec sp_LogRecordToHistory 'EREC_IrattariHelyek',@NodeId
									,'EREC_IrattariHelyekHistory',1,@ExecutorUserId,@ExecutionTime   
				
			
				FETCH NEXT FROM c INTO @NodeId
			end
			END

			-- Close and deallocate the cursor
			CLOSE c
			DEALLOCATE c


COMMIT TRANSACTION InvalidateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InvalidateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH



GO
