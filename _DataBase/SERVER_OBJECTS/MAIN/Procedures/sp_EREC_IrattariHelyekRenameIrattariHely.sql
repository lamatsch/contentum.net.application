IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariHelyekRenameIrattariHely]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IrattariHelyekRenameIrattariHely]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariHelyekRenameIrattariHely]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IrattariHelyekRenameIrattariHely] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IrattariHelyekRenameIrattariHely]
		@ExecutorUserId uniqueidentifier ,
		@ModifyRootId uniqueidentifier 
		AS

DECLARE @Temp TABLE (Id uniqueidentifier,Ertek nvarchar(1000))
DECLARE @rownum int,
		@Id uniqueidentifier, 
		@Ertek nvarchar(1000),
		@OldIrattariHely nvarchar(1000)		

BEGIN TRY
		;with C as
	(
	  select ID,
			 Ertek,
			Convert(nvarchar(1000),'')  as ParentNames
			,
			Convert(nvarchar(1000),'')  as ParentIds
	  from EREC_IrattariHelyek
	  where szuloid is null and ErvKezd<=getdate() and ErvVege >=GETDATE()
  
	  union all
	  select T.ID,
			 T.Ertek,
			 CONVERT(nvarchar(1000),C.ParentNames + '/' +C.Ertek)
			 ,
			 CONVERT(nvarchar(1000),Convert(nvarchar(1000),C.ParentIds) + '/' +Convert(nvarchar(1000),C.Id))
	  from EREC_IrattariHelyek as T         
		inner join C
		  on C.ID = T.Szuloid
	   where ErvKezd<=getdate() and ErvVege >=GETDATE()
	)      
	insert into @Temp (Id,Ertek) 
	select distinct C.Id,C.Ertek from
	(
	select distinct c.Id,
		case
			when C.ParentNames like '/%' then stuff(C.ParentNames+'/'+C.Ertek, 1, 1, '')
			else C.Ertek
	   end Ertek,
		case
			when C.ParentIds like '/%' then stuff(C.ParentIds+'/'+Convert(nvarchar(1000),C.Id), 1, 1, '')
			else Convert(nvarchar(1000),C.Id)
		end IdPath
	from C
	where C.Id NOT IN (
		SELECT DISTINCT szuloid FROM EREC_IrattariHelyek WHERE SzuloId IS NOT NULL)
	) C
	inner join EREC_UgyUgyiratok u  ON u.IrattarId = C.Id AND IdPath like '%'+CAST(@ModifyRootId as NVARCHAR(50))+'%'

	set @rownum = (select Count(Id) from @Temp)
	while (@rownum != 0)
	begin
		set @Id = (select top 1 Id from @Temp)
		set @OldIrattariHely = (select top 1 IrattariHely from EREC_UgyUgyiratok  where IrattarId = @Id)
		set @Ertek = (select top 1 Ertek from @Temp) 
		set @rownum = @rownum-1
		--Updateljük az ügyiratoknál a Az irattari hely mezőt.
		print 'ID :'+CAST(@Id AS NVARCHAR(50)) +' IrattariHely :'+@Ertek +' OldIrattariHely :'+@OldIrattariHely
		if (@Ertek != @OldIrattariHely)
			begin
				UPDATE EREC_UgyUgyiratok SET IrattariHely = @Ertek, Ver = Ver+1, ModositasIdo = GETDATE(), Modosito_id = @ExecutorUserId WHERE IrattarId is not null and IrattarId = @Id			
			end 
		else
			print 'Nincs Update, mert megegyezik'
		set nocount on
		delete from @Temp where Id = @Id 
		set nocount off
	end 	
END TRY
BEGIN CATCH  

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH



GO
