IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariKikeroGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IrattariKikeroGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariKikeroGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IrattariKikeroGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IrattariKikeroGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by EREC_IrattariKikero.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_IrattariKikero.Id,
	   EREC_IrattariKikero.Keres_note,
	   EREC_IrattariKikero.DokumentumTipus,
Case EREC_IrattariKikero.DokumentumTipus 
		When ''U'' Then ''Ügyirat'' 
		When ''T'' Then ''Tértivevény''
End
as DokumentumTipusNev,
	   EREC_IrattariKikero.Indoklas_note,
	   EREC_IrattariKikero.FelhasznaloCsoport_Id_Kikero,
KRT_FelhasznalokFelhasznaloCsoport_Id_Kikero.Nev as Kikero_IdNev,
			CONVERT(nvarchar(10), EREC_IrattariKikero.KeresDatuma, 102) as KeresDatuma,
	   EREC_IrattariKikero.KulsoAzonositok,
	   EREC_IrattariKikero.UgyUgyirat_Id,
Case EREC_IrattariKikero.DokumentumTipus 
		When ''U'' Then EREC_UgyUgyiratok.Azonosito
		When ''T'' Then ''TODO: Tertiveveny azonosito''
End
as Azonosito,
EREC_IrattariKikero.FelhasznalasiCel,
Case EREC_IrattariKikero.FelhasznalasiCel 
		When ''B'' Then ''Betekintésre''
		When ''U'' Then ''Ügyintézésre''
End
as FelhasznalasiCelNev,
	   EREC_IrattariKikero.Tertiveveny_Id,
			CONVERT(nvarchar(10), EREC_IrattariKikero.KikerKezd, 102) as KikerKezd,
			CONVERT(nvarchar(10), EREC_IrattariKikero.KikerVege, 102) as KikerVege,
	   EREC_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy,
KRT_Felhasznalok_Jovahagy.Nev as Jovahagyo_IdNev,

			CONVERT(nvarchar(10), EREC_IrattariKikero.JovagyasDatuma, 102) as JovagyasDatuma,
	   EREC_IrattariKikero.FelhasznaloCsoport_Id_Kiado,
KRT_FelhasznalokKapta_Id.Nev as Kapta_IdNev,	   
			CONVERT(nvarchar(10), EREC_IrattariKikero.KiadasDatuma, 102) as KiadasDatuma,
	   EREC_IrattariKikero.FelhasznaloCsoport_Id_Visszaad,
KRT_FelhasznalokVisszaado_ID.Nev as FelhasznaloCsoport_Id_VisszaadNev,
	   EREC_IrattariKikero.FelhasznaloCsoport_Id_Visszave,
KRT_FelhasznalokVisszavevo_ID.Nev as FelhasznaloCsoport_Id_VisszaveNev,
			CONVERT(nvarchar(10), EREC_IrattariKikero.VisszaadasDatuma, 102) as VisszaadasDatuma,
			VisszavetelDatuma = CASE WHEN EREC_IrattariKikero.Allapot = ''05'' THEN CONVERT(nvarchar(10), EREC_IrattariKikero.ModositasIdo, 102) ELSE NULL END,
	   EREC_IrattariKikero.SztornirozasDat,
	   EREC_IrattariKikero.Allapot,
	   	    KRT_KodTarakAllapot.Nev as AllapotNev,
	   EREC_IrattariKikero.BarCode,
	EREC_IrattariKikero.Irattar_Id,
	KRT_Csoportok.Nev as Irattar_neve,
	   EREC_IrattariKikero.Ver,
	   EREC_IrattariKikero.Note,
	   EREC_IrattariKikero.Stat_id,
	   EREC_IrattariKikero.ErvKezd,
	   EREC_IrattariKikero.ErvVege,
	   EREC_IrattariKikero.Letrehozo_id,
	   EREC_IrattariKikero.LetrehozasIdo,
	   EREC_IrattariKikero.Modosito_id,
	   EREC_IrattariKikero.ModositasIdo,
	   EREC_IrattariKikero.Zarolo_id,
	   EREC_IrattariKikero.ZarolasIdo,
	   EREC_IrattariKikero.Tranz_id,
	   EREC_IrattariKikero.UIAccessLog_id  '
SET @sqlcmd = @sqlcmd + '
   from 
     EREC_IrattariKikero as EREC_IrattariKikero

	left join KRT_KodCsoportok as KRT_KodCsoportokAllapot on KRT_KodCsoportokAllapot.Kod=''IRATTARIKIKEROALLAPOT''
	left join KRT_KodTarak as KRT_KodTarakAllapot on KRT_KodCsoportokAllapot.Id = KRT_KodTarakAllapot.KodCsoport_Id and EREC_IrattariKikero.Allapot = KRT_KodTarakAllapot.Kod and KRT_KodTarakAllapot.Org=''' + CAST(@Org as NVarChar(40)) + '''

	left join KRT_Felhasznalok as KRT_FelhasznalokFelhasznaloCsoport_Id_Kikero on KRT_FelhasznalokFelhasznaloCsoport_Id_Kikero.Id = EREC_IrattariKikero.FelhasznaloCsoport_Id_Kikero
	left join KRT_Felhasznalok as KRT_Felhasznalok_Jovahagy on KRT_Felhasznalok_Jovahagy.Id = EREC_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy
	left join KRT_Felhasznalok as KRT_FelhasznalokKapta_Id on KRT_FelhasznalokKapta_Id.Id = EREC_IrattariKikero.FelhasznaloCsoport_Id_Kiado	
	left join KRT_Felhasznalok as KRT_FelhasznalokVisszaado_ID on KRT_FelhasznalokVisszaado_ID.Id = EREC_IrattariKikero.FelhasznaloCsoport_Id_Visszaad
	left join KRT_Felhasznalok as KRT_FelhasznalokVisszavevo_ID on KRT_FelhasznalokVisszavevo_ID.Id = EREC_IrattariKikero.FelhasznaloCsoport_Id_Visszave
	left join KRT_Csoportok as KRT_Csoportok on KRT_Csoportok.Id = EREC_IrattariKikero.Irattar_Id
 	left join EREC_UgyUgyiratok as EREC_UgyUgyiratok on EREC_IrattariKikero.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
	left join EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek on EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id

           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy

  --print @sqlcmd

   exec (@sqlcmd);   

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
