IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariKikeroHistoryGetRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IrattariKikeroHistoryGetRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariKikeroHistoryGetRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IrattariKikeroHistoryGetRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IrattariKikeroHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_IrattariKikeroHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_IrattariKikeroHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Keres_note' as ColumnName,               
               cast(Old.Keres_note as nvarchar(99)) as OldValue,
               cast(New.Keres_note as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Keres_note != New.Keres_note 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznalasiCel' as ColumnName,               
               cast(Old.FelhasznalasiCel as nvarchar(99)) as OldValue,
               cast(New.FelhasznalasiCel as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznalasiCel != New.FelhasznalasiCel 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DokumentumTipus' as ColumnName,               
               cast(Old.DokumentumTipus as nvarchar(99)) as OldValue,
               cast(New.DokumentumTipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.DokumentumTipus != New.DokumentumTipus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Indoklas_note' as ColumnName,               
               cast(Old.Indoklas_note as nvarchar(99)) as OldValue,
               cast(New.Indoklas_note as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Indoklas_note != New.Indoklas_note 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Kikero' as ColumnName,               
               cast(Old.FelhasznaloCsoport_Id_Kikero as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Kikero as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Kikero != New.FelhasznaloCsoport_Id_Kikero 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KeresDatuma' as ColumnName,               
               cast(Old.KeresDatuma as nvarchar(99)) as OldValue,
               cast(New.KeresDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KeresDatuma != New.KeresDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KulsoAzonositok' as ColumnName,               
               cast(Old.KulsoAzonositok as nvarchar(99)) as OldValue,
               cast(New.KulsoAzonositok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KulsoAzonositok != New.KulsoAzonositok 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyUgyirat_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratokAzonosito(Old.UgyUgyirat_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratokAzonosito(New.UgyUgyirat_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyUgyirat_Id != New.UgyUgyirat_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_UgyUgyiratok FTOld on FTOld.Id = Old.UgyUgyirat_Id and FTOld.Ver = Old.Ver
         left join EREC_UgyUgyiratok FTNew on FTNew.Id = New.UgyUgyirat_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tertiveveny_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_KuldTertivevenyekAzonosito(Old.Tertiveveny_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_KuldTertivevenyekAzonosito(New.Tertiveveny_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tertiveveny_Id != New.Tertiveveny_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_KuldTertivevenyek FTOld on FTOld.Id = Old.Tertiveveny_Id and FTOld.Ver = Old.Ver
         left join EREC_KuldTertivevenyek FTNew on FTNew.Id = New.Tertiveveny_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Id' as ColumnName,               
               cast(Old.Obj_Id as nvarchar(99)) as OldValue,
               cast(New.Obj_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_Id != New.Obj_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ObjTip_Id' as ColumnName,               
               cast(Old.ObjTip_Id as nvarchar(99)) as OldValue,
               cast(New.ObjTip_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ObjTip_Id != New.ObjTip_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KikerKezd' as ColumnName,               
               cast(Old.KikerKezd as nvarchar(99)) as OldValue,
               cast(New.KikerKezd as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KikerKezd != New.KikerKezd 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KikerVege' as ColumnName,               
               cast(Old.KikerVege as nvarchar(99)) as OldValue,
               cast(New.KikerVege as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KikerVege != New.KikerVege 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Jovahagy' as ColumnName,               
               cast(Old.FelhasznaloCsoport_Id_Jovahagy as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Jovahagy as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Jovahagy != New.FelhasznaloCsoport_Id_Jovahagy 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'JovagyasDatuma' as ColumnName,               
               cast(Old.JovagyasDatuma as nvarchar(99)) as OldValue,
               cast(New.JovagyasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.JovagyasDatuma != New.JovagyasDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Kiado' as ColumnName,               
               cast(Old.FelhasznaloCsoport_Id_Kiado as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Kiado as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Kiado != New.FelhasznaloCsoport_Id_Kiado 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KiadasDatuma' as ColumnName,               
               cast(Old.KiadasDatuma as nvarchar(99)) as OldValue,
               cast(New.KiadasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KiadasDatuma != New.KiadasDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Visszaad' as ColumnName,               
               cast(Old.FelhasznaloCsoport_Id_Visszaad as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Visszaad as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Visszaad != New.FelhasznaloCsoport_Id_Visszaad 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Visszave' as ColumnName,               
               cast(Old.FelhasznaloCsoport_Id_Visszave as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Visszave as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Visszave != New.FelhasznaloCsoport_Id_Visszave 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VisszaadasDatuma' as ColumnName,               
               cast(Old.VisszaadasDatuma as nvarchar(99)) as OldValue,
               cast(New.VisszaadasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.VisszaadasDatuma != New.VisszaadasDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SztornirozasDat' as ColumnName,               
               cast(Old.SztornirozasDat as nvarchar(99)) as OldValue,
               cast(New.SztornirozasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SztornirozasDat != New.SztornirozasDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Allapot != New.Allapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRATKIKERES_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Allapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Allapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BarCode' as ColumnName,               
               cast(Old.BarCode as nvarchar(99)) as OldValue,
               cast(New.BarCode as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BarCode != New.BarCode 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Irattar_Id' as ColumnName,               
               cast(Old.Irattar_Id as nvarchar(99)) as OldValue,
               cast(New.Irattar_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Irattar_Id != New.Irattar_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end


GO
