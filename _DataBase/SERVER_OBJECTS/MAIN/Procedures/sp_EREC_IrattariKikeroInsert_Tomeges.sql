IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariKikeroInsert_Tomeges]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IrattariKikeroInsert_Tomeges]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariKikeroInsert_Tomeges]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IrattariKikeroInsert_Tomeges] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IrattariKikeroInsert_Tomeges]
                @UgyUgyirat_Ids NVARCHAR(MAX) = NULL,    
                @Keres_note     Nvarchar(4000)  = null,
                @FelhasznalasiCel     char(1)  = null,
                @DokumentumTipus     char(1)  = null,
                @Indoklas_note     Nvarchar(400)  = null,
                @FelhasznaloCsoport_Id_Kikero     uniqueidentifier  = null,
                @KeresDatuma     datetime  = null,
                @KulsoAzonositok     Nvarchar(100)  = null,
                @UgyUgyirat_Id     uniqueidentifier  = null,
                @Tertiveveny_Id     uniqueidentifier  = null,
                @Obj_Id     uniqueidentifier  = null,
                @ObjTip_Id     uniqueidentifier  = null,
                @KikerKezd     datetime  = null,
                @KikerVege     datetime  = null,
                @FelhasznaloCsoport_Id_Jovahagy     uniqueidentifier  = null,
                @JovagyasDatuma     datetime  = null,
                @FelhasznaloCsoport_Id_Kiado     uniqueidentifier  = null,
                @KiadasDatuma     datetime  = null,
                @FelhasznaloCsoport_Id_Visszaad     uniqueidentifier  = null,
                @FelhasznaloCsoport_Id_Visszave     uniqueidentifier  = null,
                @VisszaadasDatuma     datetime  = null,
                @SztornirozasDat     datetime  = null,
	            @Allapot     nvarchar(64),
                @BarCode     Nvarchar(100)  = null,
                @Irattar_Id     uniqueidentifier  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,
		@UpdatedColumns              xml = NULL,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
       
         if @Keres_note is not null
         begin
            SET @insertColumns = @insertColumns + 'Keres_note'
            SET @insertValues = @insertValues + '@Keres_note'
         end 
       
         if @FelhasznalasiCel is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznalasiCel'
            SET @insertValues = @insertValues + ',@FelhasznalasiCel'
         end 
       
         if @DokumentumTipus is not null
         begin
            SET @insertColumns = @insertColumns + ',DokumentumTipus'
            SET @insertValues = @insertValues + ',@DokumentumTipus'
         end 
       
         if @Indoklas_note is not null
         begin
            SET @insertColumns = @insertColumns + ',Indoklas_note'
            SET @insertValues = @insertValues + ',@Indoklas_note'
         end 
       
         if @FelhasznaloCsoport_Id_Kikero is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Kikero'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Kikero'
         end 
       
         if @KeresDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',KeresDatuma'
            SET @insertValues = @insertValues + ',@KeresDatuma'
         end 
       
         if @KulsoAzonositok is not null
         begin
            SET @insertColumns = @insertColumns + ',KulsoAzonositok'
            SET @insertValues = @insertValues + ',@KulsoAzonositok'
         end 
       
         if @UgyUgyirat_Ids is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyUgyirat_Id'
            SET @insertValues = @insertValues + ',t.id'
         end 
       
         if @Tertiveveny_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tertiveveny_Id'
            SET @insertValues = @insertValues + ',@Tertiveveny_Id'
         end 
       
         if @Obj_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Obj_Id'
            SET @insertValues = @insertValues + ',@Obj_Id'
         end 
       
         if @ObjTip_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',ObjTip_Id'
            SET @insertValues = @insertValues + ',@ObjTip_Id'
         end 
       
         if @KikerKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',KikerKezd'
            SET @insertValues = @insertValues + ',@KikerKezd'
         end 
       
         if @KikerVege is not null
         begin
            SET @insertColumns = @insertColumns + ',KikerVege'
            SET @insertValues = @insertValues + ',@KikerVege'
         end 
       
         if @FelhasznaloCsoport_Id_Jovahagy is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Jovahagy'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Jovahagy'
         end 
       
         if @JovagyasDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',JovagyasDatuma'
            SET @insertValues = @insertValues + ',@JovagyasDatuma'
         end 
       
         if @FelhasznaloCsoport_Id_Kiado is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Kiado'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Kiado'
         end 
       
         if @KiadasDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',KiadasDatuma'
            SET @insertValues = @insertValues + ',@KiadasDatuma'
         end 
       
         if @FelhasznaloCsoport_Id_Visszaad is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Visszaad'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Visszaad'
         end 
       
         if @FelhasznaloCsoport_Id_Visszave is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Visszave'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Visszave'
         end 
       
         if @VisszaadasDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',VisszaadasDatuma'
            SET @insertValues = @insertValues + ',@VisszaadasDatuma'
         end 
       
         if @SztornirozasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SztornirozasDat'
            SET @insertValues = @insertValues + ',@SztornirozasDat'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @BarCode is not null
         begin
            SET @insertColumns = @insertColumns + ',BarCode'
            SET @insertValues = @insertValues + ',@BarCode'
         end 
       
         if @Irattar_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Irattar_Id'
            SET @insertValues = @insertValues + ',@Irattar_Id'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
     
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )

DECLARE @InsertCommand NVARCHAR(4000)

IF (@UgyUgyirat_Ids IS NOT NULL)
BEGIN
	SET @InsertCommand = 'declare @tempTable table(id uniqueidentifier);
		declare @it	int;
		declare @curId	nvarchar(36);
		set @it = 0;
		while (@it < ((len(@UgyUgyirat_Ids)+1) / 39))
		BEGIN
			set @curId = SUBSTRING(@UgyUgyirat_Ids,@it*39+2,37);
			insert into @tempTable(id) values(@curId);
			set @it = @it + 1;
		END
		'
END
ELSE
BEGIN
	SET @InsertCommand = 'declare @tempTable table(id uniqueidentifier);
		insert into @tempTable(id) values(NULL);
		'
END

SET @InsertCommand = @InsertCommand + 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_IrattariKikero ('+@insertColumns+') output inserted.id into @InsertedRow select '+@insertValues+' from @tempTable t
'

SET @InsertCommand = @InsertCommand + 'INSERT INTO #ResultUid (Id) select id from @InsertedRow'


CREATE TABLE #ResultUid(id UNIQUEIDENTIFIER)

exec sp_executesql @InsertCommand, 
                             N'@UgyUgyirat_Ids NVARCHAR(MAX),@Keres_note Nvarchar(4000),@FelhasznalasiCel char(1),@DokumentumTipus char(1),@Indoklas_note Nvarchar(400),@FelhasznaloCsoport_Id_Kikero uniqueidentifier,@KeresDatuma datetime,@KulsoAzonositok Nvarchar(100),@Tertiveveny_Id uniqueidentifier,@Obj_Id uniqueidentifier,@ObjTip_Id uniqueidentifier,@KikerKezd datetime,@KikerVege datetime,@FelhasznaloCsoport_Id_Jovahagy uniqueidentifier,@JovagyasDatuma datetime,@FelhasznaloCsoport_Id_Kiado uniqueidentifier,@KiadasDatuma datetime,@FelhasznaloCsoport_Id_Visszaad uniqueidentifier,@FelhasznaloCsoport_Id_Visszave uniqueidentifier,@VisszaadasDatuma datetime,@SztornirozasDat datetime,@Allapot nvarchar(64),@BarCode Nvarchar(100),@Irattar_Id uniqueidentifier,@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier'
,@UgyUgyirat_Ids = @UgyUgyirat_Ids,@Keres_note = @Keres_note,@FelhasznalasiCel = @FelhasznalasiCel,@DokumentumTipus = @DokumentumTipus,@Indoklas_note = @Indoklas_note,@FelhasznaloCsoport_Id_Kikero = @FelhasznaloCsoport_Id_Kikero,@KeresDatuma = @KeresDatuma,@KulsoAzonositok = @KulsoAzonositok,@Tertiveveny_Id = @Tertiveveny_Id,@Obj_Id = @Obj_Id,@ObjTip_Id = @ObjTip_Id,@KikerKezd = @KikerKezd,@KikerVege = @KikerVege,@FelhasznaloCsoport_Id_Jovahagy = @FelhasznaloCsoport_Id_Jovahagy,@JovagyasDatuma = @JovagyasDatuma,@FelhasznaloCsoport_Id_Kiado = @FelhasznaloCsoport_Id_Kiado,@KiadasDatuma = @KiadasDatuma,@FelhasznaloCsoport_Id_Visszaad = @FelhasznaloCsoport_Id_Visszaad,@FelhasznaloCsoport_Id_Visszave = @FelhasznaloCsoport_Id_Visszave,@VisszaadasDatuma = @VisszaadasDatuma,@SztornirozasDat = @SztornirozasDat,@Allapot = @Allapot,@BarCode = @BarCode,@Irattar_Id = @Irattar_Id,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   EXEC sp_LogEREC_IrattariKikeroHistory_Tomeges 0,@Letrehozo_id,@LetrehozasIdo

END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH



GO
