IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariKikeroUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IrattariKikeroUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariKikeroUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IrattariKikeroUpdate] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_IrattariKikeroUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Keres_note     Nvarchar(4000)  = null,         
             @FelhasznalasiCel     char(1)  = null,         
             @DokumentumTipus     char(1)  = null,         
             @Indoklas_note     Nvarchar(400)  = null,         
             @FelhasznaloCsoport_Id_Kikero     uniqueidentifier  = null,         
             @KeresDatuma     datetime  = null,         
             @KulsoAzonositok     Nvarchar(100)  = null,         
             @UgyUgyirat_Id     uniqueidentifier  = null,         
             @Tertiveveny_Id     uniqueidentifier  = null,         
             @Obj_Id     uniqueidentifier  = null,         
             @ObjTip_Id     uniqueidentifier  = null,         
             @KikerKezd     datetime  = null,         
             @KikerVege     datetime  = null,         
             @FelhasznaloCsoport_Id_Jovahagy     uniqueidentifier  = null,         
             @JovagyasDatuma     datetime  = null,         
             @FelhasznaloCsoport_Id_Kiado     uniqueidentifier  = null,         
             @KiadasDatuma     datetime  = null,         
             @FelhasznaloCsoport_Id_Visszaad     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Visszave     uniqueidentifier  = null,         
             @VisszaadasDatuma     datetime  = null,         
             @SztornirozasDat     datetime  = null,         
             @Allapot     nvarchar(64)  = null,         
             @BarCode     Nvarchar(100)  = null,         
             @Irattar_Id     uniqueidentifier  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Keres_note     Nvarchar(4000)         
     DECLARE @Act_FelhasznalasiCel     char(1)         
     DECLARE @Act_DokumentumTipus     char(1)         
     DECLARE @Act_Indoklas_note     Nvarchar(400)         
     DECLARE @Act_FelhasznaloCsoport_Id_Kikero     uniqueidentifier         
     DECLARE @Act_KeresDatuma     datetime         
     DECLARE @Act_KulsoAzonositok     Nvarchar(100)         
     DECLARE @Act_UgyUgyirat_Id     uniqueidentifier         
     DECLARE @Act_Tertiveveny_Id     uniqueidentifier         
     DECLARE @Act_Obj_Id     uniqueidentifier         
     DECLARE @Act_ObjTip_Id     uniqueidentifier         
     DECLARE @Act_KikerKezd     datetime         
     DECLARE @Act_KikerVege     datetime         
     DECLARE @Act_FelhasznaloCsoport_Id_Jovahagy     uniqueidentifier         
     DECLARE @Act_JovagyasDatuma     datetime         
     DECLARE @Act_FelhasznaloCsoport_Id_Kiado     uniqueidentifier         
     DECLARE @Act_KiadasDatuma     datetime         
     DECLARE @Act_FelhasznaloCsoport_Id_Visszaad     uniqueidentifier         
     DECLARE @Act_FelhasznaloCsoport_Id_Visszave     uniqueidentifier         
     DECLARE @Act_VisszaadasDatuma     datetime         
     DECLARE @Act_SztornirozasDat     datetime         
     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_BarCode     Nvarchar(100)         
     DECLARE @Act_Irattar_Id     uniqueidentifier         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Keres_note = Keres_note,
     @Act_FelhasznalasiCel = FelhasznalasiCel,
     @Act_DokumentumTipus = DokumentumTipus,
     @Act_Indoklas_note = Indoklas_note,
     @Act_FelhasznaloCsoport_Id_Kikero = FelhasznaloCsoport_Id_Kikero,
     @Act_KeresDatuma = KeresDatuma,
     @Act_KulsoAzonositok = KulsoAzonositok,
     @Act_UgyUgyirat_Id = UgyUgyirat_Id,
     @Act_Tertiveveny_Id = Tertiveveny_Id,
     @Act_Obj_Id = Obj_Id,
     @Act_ObjTip_Id = ObjTip_Id,
     @Act_KikerKezd = KikerKezd,
     @Act_KikerVege = KikerVege,
     @Act_FelhasznaloCsoport_Id_Jovahagy = FelhasznaloCsoport_Id_Jovahagy,
     @Act_JovagyasDatuma = JovagyasDatuma,
     @Act_FelhasznaloCsoport_Id_Kiado = FelhasznaloCsoport_Id_Kiado,
     @Act_KiadasDatuma = KiadasDatuma,
     @Act_FelhasznaloCsoport_Id_Visszaad = FelhasznaloCsoport_Id_Visszaad,
     @Act_FelhasznaloCsoport_Id_Visszave = FelhasznaloCsoport_Id_Visszave,
     @Act_VisszaadasDatuma = VisszaadasDatuma,
     @Act_SztornirozasDat = SztornirozasDat,
     @Act_Allapot = Allapot,
     @Act_BarCode = BarCode,
     @Act_Irattar_Id = Irattar_Id,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_IrattariKikero
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zĂˇrolĂˇs ellenĹ‘rzĂ©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Keres_note')=1
         SET @Act_Keres_note = @Keres_note
   IF @UpdatedColumns.exist('/root/FelhasznalasiCel')=1
         SET @Act_FelhasznalasiCel = @FelhasznalasiCel
   IF @UpdatedColumns.exist('/root/DokumentumTipus')=1
         SET @Act_DokumentumTipus = @DokumentumTipus
   IF @UpdatedColumns.exist('/root/Indoklas_note')=1
         SET @Act_Indoklas_note = @Indoklas_note
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Kikero')=1
         SET @Act_FelhasznaloCsoport_Id_Kikero = @FelhasznaloCsoport_Id_Kikero
   IF @UpdatedColumns.exist('/root/KeresDatuma')=1
         SET @Act_KeresDatuma = @KeresDatuma
   IF @UpdatedColumns.exist('/root/KulsoAzonositok')=1
         SET @Act_KulsoAzonositok = @KulsoAzonositok
   IF @UpdatedColumns.exist('/root/UgyUgyirat_Id')=1
         SET @Act_UgyUgyirat_Id = @UgyUgyirat_Id
   IF @UpdatedColumns.exist('/root/Tertiveveny_Id')=1
         SET @Act_Tertiveveny_Id = @Tertiveveny_Id
   IF @UpdatedColumns.exist('/root/Obj_Id')=1
         SET @Act_Obj_Id = @Obj_Id
   IF @UpdatedColumns.exist('/root/ObjTip_Id')=1
         SET @Act_ObjTip_Id = @ObjTip_Id
   IF @UpdatedColumns.exist('/root/KikerKezd')=1
         SET @Act_KikerKezd = @KikerKezd
   IF @UpdatedColumns.exist('/root/KikerVege')=1
         SET @Act_KikerVege = @KikerVege
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Jovahagy')=1
         SET @Act_FelhasznaloCsoport_Id_Jovahagy = @FelhasznaloCsoport_Id_Jovahagy
   IF @UpdatedColumns.exist('/root/JovagyasDatuma')=1
         SET @Act_JovagyasDatuma = @JovagyasDatuma
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Kiado')=1
         SET @Act_FelhasznaloCsoport_Id_Kiado = @FelhasznaloCsoport_Id_Kiado
   IF @UpdatedColumns.exist('/root/KiadasDatuma')=1
         SET @Act_KiadasDatuma = @KiadasDatuma
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Visszaad')=1
         SET @Act_FelhasznaloCsoport_Id_Visszaad = @FelhasznaloCsoport_Id_Visszaad
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Visszave')=1
         SET @Act_FelhasznaloCsoport_Id_Visszave = @FelhasznaloCsoport_Id_Visszave
   IF @UpdatedColumns.exist('/root/VisszaadasDatuma')=1
         SET @Act_VisszaadasDatuma = @VisszaadasDatuma
   IF @UpdatedColumns.exist('/root/SztornirozasDat')=1
         SET @Act_SztornirozasDat = @SztornirozasDat
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/BarCode')=1
         SET @Act_BarCode = @BarCode
   IF @UpdatedColumns.exist('/root/Irattar_Id')=1
         SET @Act_Irattar_Id = @Irattar_Id
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_IrattariKikero
SET
     Keres_note = @Act_Keres_note,
     FelhasznalasiCel = @Act_FelhasznalasiCel,
     DokumentumTipus = @Act_DokumentumTipus,
     Indoklas_note = @Act_Indoklas_note,
     FelhasznaloCsoport_Id_Kikero = @Act_FelhasznaloCsoport_Id_Kikero,
     KeresDatuma = @Act_KeresDatuma,
     KulsoAzonositok = @Act_KulsoAzonositok,
     UgyUgyirat_Id = @Act_UgyUgyirat_Id,
     Tertiveveny_Id = @Act_Tertiveveny_Id,
     Obj_Id = @Act_Obj_Id,
     ObjTip_Id = @Act_ObjTip_Id,
     KikerKezd = @Act_KikerKezd,
     KikerVege = @Act_KikerVege,
     FelhasznaloCsoport_Id_Jovahagy = @Act_FelhasznaloCsoport_Id_Jovahagy,
     JovagyasDatuma = @Act_JovagyasDatuma,
     FelhasznaloCsoport_Id_Kiado = @Act_FelhasznaloCsoport_Id_Kiado,
     KiadasDatuma = @Act_KiadasDatuma,
     FelhasznaloCsoport_Id_Visszaad = @Act_FelhasznaloCsoport_Id_Visszaad,
     FelhasznaloCsoport_Id_Visszave = @Act_FelhasznaloCsoport_Id_Visszave,
     VisszaadasDatuma = @Act_VisszaadasDatuma,
     SztornirozasDat = @Act_SztornirozasDat,
     Allapot = @Act_Allapot,
     BarCode = @Act_BarCode,
     Irattar_Id = @Act_Irattar_Id,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_IrattariKikero',@Id
					,'EREC_IrattariKikeroHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH




GO
