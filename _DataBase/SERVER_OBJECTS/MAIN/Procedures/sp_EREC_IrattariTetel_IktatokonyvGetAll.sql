IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariTetel_IktatokonyvGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_IrattariTetel_IktatokonyvGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_IrattariTetel_IktatokonyvGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_IrattariTetel_IktatokonyvGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_IrattariTetel_IktatokonyvGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IrattariTetel_Iktatokonyv.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_IrattariTetel_Iktatokonyv.Id,
	   EREC_IrattariTetel_Iktatokonyv.IrattariTetel_Id,
	   EREC_IrattariTetel_Iktatokonyv.Iktatokonyv_Id,
	   EREC_IrattariTetel_Iktatokonyv.Ver,
	   EREC_IrattariTetel_Iktatokonyv.Note,
	   EREC_IrattariTetel_Iktatokonyv.Stat_id,
	   EREC_IrattariTetel_Iktatokonyv.ErvKezd,
	   EREC_IrattariTetel_Iktatokonyv.ErvVege,
	   EREC_IrattariTetel_Iktatokonyv.Letrehozo_id,
	   EREC_IrattariTetel_Iktatokonyv.LetrehozasIdo,
	   EREC_IrattariTetel_Iktatokonyv.Modosito_id,
	   EREC_IrattariTetel_Iktatokonyv.ModositasIdo,
	   EREC_IrattariTetel_Iktatokonyv.Zarolo_id,
	   EREC_IrattariTetel_Iktatokonyv.ZarolasIdo,
	   EREC_IrattariTetel_Iktatokonyv.Tranz_id,
	   EREC_IrattariTetel_Iktatokonyv.UIAccessLog_id  
   from 
     EREC_IrattariTetel_Iktatokonyv as EREC_IrattariTetel_Iktatokonyv      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
