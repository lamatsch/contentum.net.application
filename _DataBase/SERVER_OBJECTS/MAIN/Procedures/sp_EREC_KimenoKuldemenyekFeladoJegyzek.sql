IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KimenoKuldemenyekFeladoJegyzek]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KimenoKuldemenyekFeladoJegyzek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KimenoKuldemenyekFeladoJegyzek]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KimenoKuldemenyekFeladoJegyzek] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_KimenoKuldemenyekFeladoJegyzek]
     @Where nvarchar(MAX) = '',
     @TopRow NVARCHAR(5) = '',
	 @PostaKonyv_Id uniqueidentifier = null,
     @KezdDat datetime = null,
     @VegeDat datetime = null,
     @ExecutorUserId uniqueidentifier
as

begin

BEGIN TRY

   set nocount on


DECLARE @Org uniqueidentifier
SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
if (@Org is null)
begin
	RAISERROR('[50202]',16,1)
end

-- Nyilvántartott küldemények
select
row_number() over( order by Ragszam) Sorszam,
EREC_KuldKuldemenyek.RagSzam as Azonosito, 
EREC_KuldKuldemenyek.NevSTR_Bekuldo as CimzettNeve,
(select 
case when IsNULL(OrszagNev,'Magyarország') = 'Magyarország'
then IRSZ
else IRSZ + '/' + OrszagNev end 
from KRT_Cimek
where KRT_Cimek.Id = EREC_KuldKuldemenyek.Cim_Id) IranyitoszamOrszag,
(select dbo.fn_MergeCim(
   KRT_Cimek.Tipus,
   null,
   null,
   KRT_Cimek.TelepulesNev,
   KRT_Cimek.KozteruletNev,
   KRT_Cimek.KozteruletTipusNev,
   KRT_Cimek.Hazszam,
   KRT_Cimek.Hazszamig,
   KRT_Cimek.HazszamBetujel,
   KRT_Cimek.Lepcsohaz,
   KRT_Cimek.Szint,
   KRT_Cimek.Ajto,
   KRT_Cimek.AjtoBetujel,
   KRT_Cimek.CimTobbi)
from KRT_Cimek
where KRT_Cimek.Id = EREC_KuldKuldemenyek.Cim_Id
)
TelepulesCimhely,
(case when KimenoKuldemenyFajta in ('01','K1_01','K2_01')
then 'SZ'
else null end
) Tomeg,
null as GepiFeld,
null as Ertek,
null as FizetestKoveto,
EREC_KuldKuldemenyek.Ar as BermentesitesiDij,
STUFF(
case when KimenoKuldemenyFajta = '19' -- Hivatalos irat
then ', HIV'
else '' end +
case when KimenoKuldemenyFajta = '18' -- Levelezolap
then ', LEVL'
else '' end +
case When EREC_KuldKuldemenyek.Elsobbsegi = '1'
Then ', PRI'
else '' end +
case When EREC_KuldKuldemenyek.Ajanlott = '1'
Then ', AJL'
else '' end +
case When EREC_KuldKuldemenyek.E_ertesites = '1'
Then ', ÉRT'
else '' end +
case When EREC_KuldKuldemenyek.PostaiLezaroSzolgalat = '1'
Then ', LEZ'
else '' end +
case When EREC_KuldKuldemenyek.SajatKezbe = '1'
or KimenoKuldemenyFajta = '20' -- Hivatalos irat saját kézbe
Then ', SK'
else '' end +
case When EREC_KuldKuldemenyek.Tertiveveny = '1'
Then ', TV'
else '' end
,1,2,'') Szolgaltatas
from
EREC_KuldKuldemenyek
join dbo.EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Postakonyv on EREC_IraIktatoKonyvek_Postakonyv.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
where
EREC_IraIktatoKonyvek_PostaKonyv.IktatoErkezteto='P'
and EREC_IraIktatoKonyvek_PostaKonyv.Id = IsNULL(@PostaKonyv_Id, EREC_KuldKuldemenyek.IraIktatokonyv_Id)
and EREC_KuldKuldemenyek.PostazasIranya = '2' -- kimeno
and EREC_KuldKuldemenyek.Allapot='06' -- Postázott
and 1 = case
when KimenoKuldemenyFajta in ('19', '20') then 1
when IsNull(Ajanlott, 0) + IsNull(Tertiveveny, 0) + IsNull(SajatKezbe, 0) + IsNull(E_ertesites, 0) + IsNull(E_elorejelzes, 0) + IsNull(PostaiLezaroSzolgalat, 0) > 0 then 1
else 0
end
and EREC_KuldKuldemenyek.BelyegzoDatuma between IsNull(@KezdDat, '1900-01-01') and IsNull(@VegeDat, getdate())
order by Sorszam


-- Közönséges küldemények
select
row_number() over( order by EREC_KuldKuldemenyek.Elsobbsegi desc, KRT_KodTarak.Sorrend) Sorszam,
(case when Isnull(EREC_KuldKuldemenyek.Elsobbsegi,'0') = '1'
then 'Postai elsobbségi - ' + KRT_KodTarak.Nev
else 'Postai sima - ' + KRT_KodTarak.Nev END)
+
(
	case when left(EREC_KuldKuldemenyek.KimenoKuldemenyFajta,2) = 'K1'
	then ', Európai'
	when left(EREC_KuldKuldemenyek.KimenoKuldemenyFajta,2) = 'K2'
	then ', Egyéb külföldi'
	else '' end
) as Azonosito, 
sum(isnull(EREC_KuldKuldemenyek.PeldanySzam, 1)) as CimzettNeve,
null IranyitoszamOrszag,
null TelepulesCimhely,
null Tomeg,
null as GepiFeld,
null as Ertek,
null as FizetestKoveto,
sum(cast(EREC_KuldKuldemenyek.Ar as int)) as BermentesitesiDij,
(case    
when KimenoKuldemenyFajta='01' then 'LEV 30'
		when KimenoKuldemenyFajta in ('02','K1_03','K2_03') then 'LEV 50'
		when KimenoKuldemenyFajta in ('03','K1_04','K2_04') then 'LEV 100'
		when KimenoKuldemenyFajta in ('04','K1_05','K2_05') then 'LEV 250'
		when KimenoKuldemenyFajta in ('05','K1_06','K2_06') then 'LEV 500'
		when KimenoKuldemenyFajta='06' then 'LEV 750'
		when KimenoKuldemenyFajta in ('07','K1_09','K2_09') then 'LEV 2000'
		when KimenoKuldemenyFajta='18' then 'LEVL'
		when KimenoKuldemenyFajta in ('K1_01','K2_01','K1_02','K2_02','K1_10','K2_10') then 'LEV 20'
		when KimenoKuldemenyFajta in ('K1_07','K2_07') then 'LEV 1000'
		when KimenoKuldemenyFajta in ('K1_08','K2_08') then 'LEV 1500'
end)
+
(
case when  Isnull(EREC_KuldKuldemenyek.Elsobbsegi,'0') = '1'
then ', PRI'
else ''
end
) Szolgaltatas
from
EREC_KuldKuldemenyek
join dbo.EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Postakonyv on EREC_IraIktatoKonyvek_Postakonyv.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
left join KRT_Kodcsoportok as KRT_Kodcsoportok on KRT_Kodcsoportok.Kod='KIMENO_KULDEMENY_FAJTA'
left join KRT_KodTarak as KRT_KodTarak on KRT_Kodcsoportok.Id = KRT_KodTarak.KodCsoport_Id and KRT_KodTarak.Org=@Org
and EREC_KuldKuldemenyek.KimenoKuldemenyFajta = KRT_KodTarak.Kod COLLATE Hungarian_CI_AS
where
EREC_IraIktatoKonyvek_PostaKonyv.IktatoErkezteto='P'
and EREC_IraIktatoKonyvek_PostaKonyv.Id = IsNULL(@PostaKonyv_Id, EREC_KuldKuldemenyek.IraIktatokonyv_Id)
and EREC_KuldKuldemenyek.PostazasIranya = '2' -- kimeno
and EREC_KuldKuldemenyek.Allapot='06' -- Postázott
and KimenoKuldemenyFajta not in ('19', '20')
and Ajanlott = 0
and Tertiveveny = 0
and SajatKezbe = 0
and E_ertesites = 0
and E_elorejelzes = 0
and PostaiLezaroSzolgalat = 0
and EREC_KuldKuldemenyek.BelyegzoDatuma between IsNull(@KezdDat, '1900-01-01') and IsNull(@VegeDat, getdate())
group by EREC_KuldKuldemenyek.Elsobbsegi, EREC_KuldKuldemenyek.KimenoKuldemenyFajta, KRT_KodTarak.Sorrend, KRT_KodTarak.Nev
order by Sorszam

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
