IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KimenoKuldemenyekGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KimenoKuldemenyekGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KimenoKuldemenyekGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KimenoKuldemenyekGetAllWithExtension] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_KimenoKuldemenyekGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @Where_IratPeldany_Cimzett nvarchar(4000) = '',
  @Where_UgyUgyiratdarabok_Foszam nvarchar(4000) = '',
  @Where_IraIratok_Alszam nvarchar(4000) = '',
  @Where_IraIktatokonyvek nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by EREC_KuldKuldemenyek.LetrehozasIdo desc',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier,
  @Jogosultak		char(1) = '0',
  @pageNumber		int = 0,
  @pageSize			int = -1,
  @SelectedRowId	uniqueidentifier = null
as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(MAX)
   DECLARE @LocalTopRow nvarchar(10)
   declare @firstRow int
   declare @lastRow INT

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
     ELSE
     BEGIN
		if (@pageSize > @TopRow)
			SET @pageSize = @TopRow;
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
                  
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = 1000
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end   
                  
  SET @sqlcmd = 'SELECT distinct EREC_KuldKuldemenyek.Id into #filter from EREC_KuldKuldemenyek
LEFT JOIN EREC_IraIktatoKonyvek on EREC_KuldKuldemenyek.IraIktatoKonyv_Id=EREC_IraiktatoKonyvek.Id
Where @Org=IsNull(EREC_IraIktatoKonyvek.Org, (select Org from KRT_Felhasznalok where EREC_KuldKuldemenyek.Letrehozo_id=KRT_Felhasznalok.Id))
'
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where;
	END
  
  IF @Jogosultak = 1 AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
  BEGIN
	set @sqlcmd = @sqlcmd + N'
	DELETE FROM #filter WHERE Id NOT IN
	(
		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
			INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.Id
			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
		UNION ALL
		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
			INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
		UNION ALL
		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
				ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.Csoport_Id_Felelos
		UNION ALL
		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
				ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
		UNION ALL
		SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_KuldKuldemenyek'')

	)'
  END
  
  if @Where_IratPeldany_Cimzett is not null and @Where_IratPeldany_Cimzett != ''
	begin
		SET @sqlcmd = @sqlcmd + '
		DELETE From #filter WHERE ID NOT IN 
	    (
	     select EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id
		 from EREC_Kuldemeny_IratPeldanyai
		 join EREC_PldIratPeldanyok on EREC_Kuldemeny_IratPeldanyai.Peldany_Id = EREC_PldIratPeldanyok.Id
		 where getdate() between EREC_Kuldemeny_IratPeldanyai.ErvKezd and EREC_Kuldemeny_IratPeldanyai.ErvVege
		 and EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id is not null
		 and ' + @Where_IratPeldany_Cimzett + '
		)'				
	end
    if @Where_UgyUgyiratdarabok_Foszam is not null and @Where_UgyUgyiratdarabok_Foszam != ''
	begin
		SET @sqlcmd = @sqlcmd + '
		DELETE From #filter WHERE ID NOT IN 
	    (
	     select EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id
		 from EREC_Kuldemeny_IratPeldanyai
		 join EREC_PldIratPeldanyok on EREC_Kuldemeny_IratPeldanyai.Peldany_Id = EREC_PldIratPeldanyok.Id
		 join EREC_IraIratok on EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id
		 JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
		 where getdate() between EREC_Kuldemeny_IratPeldanyai.ErvKezd and EREC_Kuldemeny_IratPeldanyai.ErvVege
		 and EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id is not null
		 and ' + @Where_UgyUgyiratdarabok_Foszam + '
		)'				
	end
	if @Where_IraIktatokonyvek is not null and @Where_IraIktatokonyvek != ''
	begin
		SET @sqlcmd = @sqlcmd + '
		DELETE From #filter WHERE ID NOT IN 
	    (
	     select EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id
		 from EREC_Kuldemeny_IratPeldanyai
		 join EREC_PldIratPeldanyok on EREC_Kuldemeny_IratPeldanyai.Peldany_Id = EREC_PldIratPeldanyok.Id
		 join EREC_IraIratok on EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id	 
		 JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
		 JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek ON EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id
		 where getdate() between EREC_Kuldemeny_IratPeldanyai.ErvKezd and EREC_Kuldemeny_IratPeldanyai.ErvVege
		 and EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id is not null
		 and ' + @Where_IraIktatokonyvek + '
		)'				
	end
	if @Where_IraIratok_Alszam is not null and @Where_IraIratok_Alszam != ''
	begin
		SET @sqlcmd = @sqlcmd + '
		DELETE From #filter WHERE ID NOT IN 
	    (
	     select EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id
		 from EREC_Kuldemeny_IratPeldanyai
		 join EREC_PldIratPeldanyok on EREC_Kuldemeny_IratPeldanyai.Peldany_Id = EREC_PldIratPeldanyok.Id
		 join EREC_IraIratok on EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id		 		 
		 where getdate() between EREC_Kuldemeny_IratPeldanyai.ErvKezd and EREC_Kuldemeny_IratPeldanyai.ErvVege
		 and EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id is not null
		 and ' + @Where_IraIratok_Alszam + '
		)'				
	end
  
  
    /************************************************************
	* Szűrt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/
		SET @sqlcmd = @sqlcmd + N'
	    select 
		row_number() over('+@OrderBy+') as RowNumber,
  	    EREC_KuldKuldemenyek.Id into #result
        from EREC_KuldKuldemenyek as EREC_KuldKuldemenyek
		LEFT JOIN EREC_KuldTertivevenyek on EREC_KuldTertivevenyek.Kuldemeny_Id=EREC_KuldKuldemenyek.Id and getdate() between EREC_KuldTertivevenyek.ErvKezd and EREC_KuldTertivevenyek.ErvVege
        LEFT JOIN KRT_KodCsoportok AS KRT_KodCsoportokKuldesMod ON KRT_KodCsoportokKuldesMod.Kod = ''KULDEMENY_KULDES_MODJA''
        LEFT JOIN KRT_KodTarak AS KRT_KodTarakKuldesMod ON KRT_KodTarakKuldesMod.Kodcsoport_Id = KRT_KodCsoportokKuldesMod.Id AND KRT_KodTarakKuldesMod.Kod = EREC_KuldKuldemenyek.KuldesMod and KRT_KodTarakKuldesMod.Org=@Org and EREC_KuldKuldemenyek.ExpedialasIdeje between KRT_KodTarakKuldesMod.ErvKezd and KRT_KodTarakKuldesMod.ErvVege 
        LEFT JOIN KRT_KodCsoportok AS KRT_KodCsoportokAllapot ON KRT_KodCsoportokAllapot.Kod = ''KULDEMENY_ALLAPOT''
        LEFT JOIN KRT_KodTarak AS KRT_KodTarakAllapot ON KRT_KodTarakAllapot.Kodcsoport_Id = KRT_KodCsoportokAllapot.Id AND KRT_KodTarakAllapot.Kod = EREC_KuldKuldemenyek.Allapot and KRT_KodTarakAllapot.Org=@Org
		LEFT JOIN KRT_Csoportok as Csoportok_KikuldoNev on Csoportok_KikuldoNev.Id = EREC_KuldKuldemenyek.Csoport_Id_Cimzett
        LEFT JOIN KRT_Csoportok as KRT_CsoportokOrzo on KRT_CsoportokOrzo.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
		LEFT JOIN KRT_Csoportok as KRT_CsoportokExpedialo on KRT_CsoportokExpedialo.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Expedial
		LEFT JOIN (SELECT Id, ISNULL(EREC_KuldKuldemenyek.RagSzam,''zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz'') as order_RagSzam FROM EREC_KuldKuldemenyek) AS order_RagSzam_table ON order_RagSzam_table.Id = EREC_KuldKuldemenyek.Id
		
		where EREC_KuldKuldemenyek.Id in (select Id from #filter); '

		if (@SelectedRowId is not null)
			set @sqlcmd = @sqlcmd + N'
			if exists (select 1 from #result where Id = @SelectedRowId)
			BEGIN
				select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
				set @firstRow = (@pageNumber - 1) * @pageSize + 1;
				set @lastRow = @pageNumber*@pageSize;
				select @pageNumber = @pageNumber - 1
			END
			ELSE'
		--ELSE
			set @sqlcmd = @sqlcmd + N' 
				if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
				BEGIN
					select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
					set @firstRow = (@pageNumber - 1) * @pageSize + 1;
					set @lastRow = @pageNumber*@pageSize;
					select @pageNumber = @pageNumber - 1
				END ;'
  
  /************************************************************
  * Tényleges select											*
  ************************************************************/
  SET @sqlcmd = @sqlcmd + N'
  select
       #result.RowNumber,
	   #result.Id,
	   EREC_KuldKuldemenyek.Allapot,
	  dbo.fn_KodtarErtekNeve(''KULDEMENY_ALLAPOT'', EREC_KuldKuldemenyek.Allapot,@Org) as AllapotNev,
	   EREC_KuldKuldemenyek.TovabbitasAlattAllapot,
	  dbo.fn_KodtarErtekNeve(''KULDEMENY_ALLAPOT'', EREC_KuldKuldemenyek.TovabbitasAlattAllapot,@Org) as TovabbitasAlattAllapotNev,
	   CONVERT(nvarchar(10), EREC_KuldKuldemenyek.BeerkezesIdeje, 102) as BeerkezesIdeje,		
		EREC_KuldKuldemenyek.NevSTR_Bekuldo as Cimzett_Nev_Partner,
	Cimzett_Nev_Partner_Print = 
		CASE WHEN EREC_KuldKuldemenyek.NevSTR_Bekuldo is not null THEN
			EREC_KuldKuldemenyek.NevSTR_Bekuldo
		ELSE
			 CASE dbo.fn_KodtarErtekNeve(''KULDEMENY_KULDES_MODJA'', EREC_KuldKuldemenyek.KuldesMod,@Org) 
				WHEN ''Ügyfél'' THEN 
					isnull(EREC_KuldKuldemenyek.NevSTR_Bekuldo, ''Ügyfél'') + '', '' +
					''Postai elsőbbségi''
					+ '' - '' + dbo.fn_KodtarErtekNeve(''KIMENO_KULDEMENY_FAJTA'', EREC_KuldKuldemenyek.KimenoKuldemenyFajta,@Org)
					+ '' ('' + cast(EREC_KuldKuldemenyek.PeldanySzam as NVARCHAR) + '' darab)''
				ELSE
					isnull(EREC_KuldKuldemenyek.NevSTR_Bekuldo, ''Ügyfél'') + '', '' +
					dbo.fn_KodtarErtekNeve(''KULDEMENY_KULDES_MODJA'', EREC_KuldKuldemenyek.KuldesMod,@Org) 
					+ '' - '' + dbo.fn_KodtarErtekNeve(''KIMENO_KULDEMENY_FAJTA'', EREC_KuldKuldemenyek.KimenoKuldemenyFajta,@Org)
					+ '' ('' + cast(EREC_KuldKuldemenyek.PeldanySzam as NVARCHAR) + '' darab)''
		END		END,	'

-- a max 4000 karakter miatt ketszer kell osszefuzni
SET @sqlcmd = @sqlcmd + '		
		EREC_KuldKuldemenyek.CimSTR_Bekuldo as Cimzett_Cim_Partner,		
		EREC_KuldKuldemenyek.Partner_Id_Bekuldo	as Cimzett_Id_Partner,	   		
		EREC_KuldKuldemenyek.Cim_Id	as Cimzett_CimId_Partner,	   
	   EREC_KuldKuldemenyek.IraIktatokonyv_Id,
	EREC_IraIktatoKonyvek_Postakonyv.Ev EREC_IraIktatoKonyvek_Ev,
	EREC_IraIktatoKonyvek_Postakonyv.Nev EREC_IraIktatoKonyvek_Nev,
	   EREC_KuldKuldemenyek.KuldesMod,
	KuldesMod_Nev = 
		CASE EREC_KuldKuldemenyek.PeldanySzam
			WHEN 1 THEN dbo.fn_KodtarErtekNeve(''KULDEMENY_KULDES_MODJA'', EREC_KuldKuldemenyek.KuldesMod,@Org)
				 + ISNULL('' - '' + dbo.fn_KodtarErtekNeve(''KIMENO_KULDEMENY_FAJTA'', EREC_KuldKuldemenyek.KimenoKuldemenyFajta,@Org),'''')
			ELSE dbo.fn_KodtarErtekNeve(''KULDEMENY_KULDES_MODJA'', EREC_KuldKuldemenyek.KuldesMod,@Org) 
				 + '' - '' + dbo.fn_KodtarErtekNeve(''KIMENO_KULDEMENY_FAJTA'', EREC_KuldKuldemenyek.KimenoKuldemenyFajta,@Org)
					+ '' ('' + cast(EREC_KuldKuldemenyek.PeldanySzam as NVARCHAR) + '' darab)''
		END,
	KuldesMod_Nev_Print = 
		CASE 
			WHEN EREC_KuldKuldemenyek.KimenoKuldemenyFajta IN (''19'',''20'') THEN ''Hiv.Irat''
			ELSE ''Levél''
		END,
	   EREC_KuldKuldemenyek.Erkezteto_Szam,
	   EREC_KuldKuldemenyek.HivatkozasiSzam,
	   EREC_KuldKuldemenyek.Targy,
	   EREC_KuldKuldemenyek.Tartalom,
	   EREC_KuldKuldemenyek.RagSzam,
	ISNULL(EREC_KuldKuldemenyek.RagSzam,''zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz'') as order_RagSzam,
	   EREC_KuldKuldemenyek.Surgosseg,'

-- a max 4000 karakter miatt ketszer kell osszefuzni
SET @sqlcmd = @sqlcmd + '
	   EREC_KuldKuldemenyek.BelyegzoDatuma,
	CONVERT(nvarchar(10), EREC_KuldKuldemenyek.BelyegzoDatuma, 102) as BelyegzoDatuma_rovid,
	   EREC_KuldKuldemenyek.UgyintezesModja,
	   EREC_KuldKuldemenyek.PostazasIranya,
	   EREC_KuldKuldemenyek.Tovabbito,
	   EREC_KuldKuldemenyek.PeldanySzam,
	   EREC_KuldKuldemenyek.IktatniKell,
	   EREC_KuldKuldemenyek.Iktathato,
	   EREC_KuldKuldemenyek.SztornirozasDat,
	   EREC_KuldKuldemenyek.KuldKuldemeny_Id_Szulo,
	   EREC_KuldKuldemenyek.Erkeztetes_Ev,
	   EREC_KuldKuldemenyek.Csoport_Id_Cimzett,
dbo.fn_GetCsoportNev(EREC_KuldKuldemenyek.Csoport_Id_Cimzett) as Csoport_Id_KikuldoNev,
	   EREC_KuldKuldemenyek.Csoport_Id_Felelos,
dbo.fn_GetCsoportNev(EREC_KuldKuldemenyek.Csoport_Id_Felelos) as Csoport_Id_FelelosNev,
       EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Expedial,
	   dbo.fn_GetCsoportNev(EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Expedial) as FelhasznaloCsoport_Id_ExpedialNev,
	   EREC_KuldKuldemenyek.ExpedialasIdeje,
	   CONVERT(nvarchar(19), EREC_KuldKuldemenyek.ExpedialasIdeje, 102) as ExpedialasIdeje_rovid,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo,
dbo.fn_GetCsoportNev(EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo) as FelhasznaloCsoport_Id_OrzoNev,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Atvevo,
	   EREC_KuldKuldemenyek.Partner_Id_Bekuldo,
       EREC_KuldKuldemenyek.NevSTR_Bekuldo,
	   EREC_KuldKuldemenyek.AdathordozoTipusa,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Alairo,	  
	   EREC_KuldKuldemenyek.BarCode,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto,
	   EREC_KuldKuldemenyek.Cim_Id,
       EREC_KuldKuldemenyek.CimSTR_Bekuldo,
	   EREC_KuldKuldemenyek.CsoportFelelosEloszto_Id,
	   EREC_KuldKuldemenyek.MegtagadasIndoka,
	   EREC_KuldKuldemenyek.Megtagado_Id,
	   EREC_KuldKuldemenyek.MegtagadasDat,'

	set @sqlcmd = @sqlcmd + '(SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege) as CsatolmanyCount,
'

	set @sqlcmd = @sqlcmd + 'Dokumentum_Id = case (SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
when 1 then
(SELECT  top 1 EREC_Csatolmanyok.Dokumentum_Id FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
else null end,
'
SET @sqlcmd = @sqlcmd + '
	EREC_KuldTertivevenyek.BarCode TertivevenyVonalkod,
'
-- a max 4000 karakter miatt ketszer kell osszefuzni
SET @sqlcmd = @sqlcmd + '
dbo.fn_KodtarErtekNeve(''IRAT_FAJTA'', EREC_IraIratok.IratFajta,@Org) as IratFajta,
EREC_KuldKuldemenyek.Ar,
(CASE WHEN EREC_KuldKuldemenyek.Elsobbsegi = 1 THEN ''I''
	ELSE ''N'' END) as Elsobbsegi,
KRT_KodTarakEredetiAr.Nev as EredetiAr,
(CASE WHEN EREC_KuldKuldemenyek.PeldanySzam = 0 THEN 0
 ELSE (CONVERT(bigint, EREC_KuldKuldemenyek.Ar) - (CONVERT(bigint, KRT_KodTarakEredetiAr.Nev) * EREC_KuldKuldemenyek.PeldanySzam)) / EREC_KuldKuldemenyek.PeldanySzam
 END) as Kulonbseg,
ISNULL(EREC_IraIratok.Azonosito,EREC_KuldKuldemenyek.Note) as IktatoSzam_Merge,
	   EREC_KuldKuldemenyek.Ver,
	   EREC_KuldKuldemenyek.Note,
	   EREC_KuldKuldemenyek.Stat_id,
	   EREC_KuldKuldemenyek.ErvKezd,
	   EREC_KuldKuldemenyek.ErvVege,
	   EREC_KuldKuldemenyek.Letrehozo_id,
	   CONVERT(nvarchar(19), EREC_KuldKuldemenyek.LetrehozasIdo, 120) as LetrehozasIdo_rovid,
	   EREC_KuldKuldemenyek.LetrehozasIdo,
	   EREC_KuldKuldemenyek.Modosito_id,
	   EREC_KuldKuldemenyek.ModositasIdo,
	   EREC_KuldKuldemenyek.Zarolo_id,
	   EREC_KuldKuldemenyek.ZarolasIdo,
	   EREC_KuldKuldemenyek.Tranz_id,
	   EREC_KuldKuldemenyek.UIAccessLog_id,
	   EREC_IraIratok.Alszam,
	   EREC_UgyUgyiratdarabok.Foszam,
		'''' as IktatoSzam
   from '

-- a max 4000 karakter miatt ketszer kell osszefuzni
SET @sqlcmd = @sqlcmd + '
     EREC_KuldKuldemenyek as EREC_KuldKuldemenyek
     inner join #result on #result.Id = EREC_KuldKuldemenyek.Id
	 left join EREC_IraIratok as EREC_IraIratok on EREC_IraIratok.Id = (select TOP 1 IraIrat_Id from EREC_PldIratPeldanyok join EREC_Kuldemeny_IratPeldanyai on EREC_Kuldemeny_IratPeldanyai.Peldany_Id = EREC_PldIratPeldanyok.Id where EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id and getdate() between EREC_Kuldemeny_IratPeldanyai.ErvKezd and EREC_Kuldemeny_IratPeldanyai.ErvVege)
	 LEFT JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
	 LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek ON EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id
	 left join EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Postakonyv on EREC_IraIktatoKonyvek_Postakonyv.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
	LEFT JOIN EREC_KuldTertivevenyek on EREC_KuldTertivevenyek.Kuldemeny_Id=EREC_KuldKuldemenyek.Id and getdate() between EREC_KuldTertivevenyek.ErvKezd and EREC_KuldTertivevenyek.ErvVege
	 left join KRT_KodCsoportok as KRT_KodcsoportokErdetiAr on KRT_KodcsoportokErdetiAr.Kod=''KIMENO_KULDEMENY_AR_NEM_ELSOBBSEGI''
	 left join KRT_KodTarak as KRT_KodTarakEredetiAr on KRT_KodcsoportokErdetiAr.Id = KRT_KodTarakEredetiAr.KodCsoport_Id and EREC_KuldKuldemenyek.KimenoKuldemenyFajta = CAST( KRT_KodTarakEredetiAr.Kod as varchar ) COLLATE Hungarian_CI_AS
		and KRT_KodTarakEredetiAr.Org=@Org
	 
	 
	 
	 WHERE IsNull(EREC_IraIktatoKonyvek.Org,@Org)=@Org
		and IsNull(EREC_IraIktatoKonyvek_Postakonyv.Org,@Org)=@Org
		and RowNumber between @firstRow and @lastRow
	 ORDER BY #result.RowNumber
'
             
		-- találatok száma és oldalszám
		set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter;';

		execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @ExecutorUserId uniqueidentifier, @FelhasznaloSzervezet_Id uniqueidentifier, @SelectedRowId uniqueidentifier, @Org uniqueidentifier'
		,@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @ExecutorUserId = @ExecutorUserId, @FelhasznaloSzervezet_Id = @FelhasznaloSzervezet_Id, @SelectedRowId = @SelectedRowId, @Org = @Org

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
