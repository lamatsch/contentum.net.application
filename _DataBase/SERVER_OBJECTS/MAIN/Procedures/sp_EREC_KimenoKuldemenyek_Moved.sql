IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KimenoKuldemenyek_Moved]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KimenoKuldemenyek_Moved]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KimenoKuldemenyek_Moved]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KimenoKuldemenyek_Moved] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_KimenoKuldemenyek_Moved]
        @Id							UNIQUEIDENTIFIER = NULL,
		@ExecutorUserId				uniqueidentifier,
		@ExecutionTime			    DATETIME

as

BEGIN TRY

  
	set nocount on
	
	IF @Id IS NOT NULL
	BEGIN
		CREATE TABLE #kuldIds_kimeno(Id UNIQUEIDENTIFIER)
		
		INSERT INTO [#kuldIds_kimeno] (
			[Id]
		) VALUES (@Id) 
	END

    -- Az iratok lekéréséhez tárolja a példány azonosítókat
	declare @pldIds table(Id UNIQUEIDENTIFIER)

-- Kódtáras érték konstansok
	declare @kt_POSTAZAS_IRANYA_Kimeno nvarchar(64)
	set @kt_POSTAZAS_IRANYA_Kimeno = '2'

	declare @kt_IRAT_ALLAPOT_Felszabaditva nvarchar(64)
	set @kt_IRAT_ALLAPOT_Felszabaditva = '08'

	declare @kt_IRAT_ALLAPOT_Atiktatott nvarchar(64)
	set @kt_IRAT_ALLAPOT_Atiktatott = '06'

	declare @kt_IRATPELDANY_ALLAPOT_TovabbitasAlatt nvarchar(64)
	set @kt_IRATPELDANY_ALLAPOT_TovabbitasAlatt = '50'

	declare @kt_IRATPELDANY_ALLAPOT_Sztornozott nvarchar(64)
	set @kt_IRATPELDANY_ALLAPOT_Sztornozott = '90'

	declare @kt_KULDEMENY_ALLAPOT_TovabbitasAlatt nvarchar(64)
	set @kt_KULDEMENY_ALLAPOT_TovabbitasAlatt = '03'

	--IratPeldanyok Update
	UPDATE [EREC_PldIratPeldanyok]
		SET Csoport_Id_Felelos = [EREC_KuldKuldemenyek].Csoport_Id_Felelos,
			FelhasznaloCsoport_Id_Orzo = [EREC_KuldKuldemenyek].FelhasznaloCsoport_Id_Orzo,
			TovabbitasAlattAllapot = case [EREC_KuldKuldemenyek].Allapot
				when @kt_KULDEMENY_ALLAPOT_TovabbitasAlatt then
					case [EREC_PldIratPeldanyok].Allapot
						when @kt_IRATPELDANY_ALLAPOT_TovabbitasAlatt then [EREC_PldIratPeldanyok].TovabbitasAlattAllapot
						else [EREC_PldIratPeldanyok].Allapot
					end
				else
					case [EREC_PldIratPeldanyok].Allapot
						when @kt_IRATPELDANY_ALLAPOT_TovabbitasAlatt then null
						else [EREC_PldIratPeldanyok].TovabbitasAlattAllapot
					end
				end,
			Allapot = case [EREC_KuldKuldemenyek].Allapot
				when @kt_KULDEMENY_ALLAPOT_TovabbitasAlatt then @kt_IRATPELDANY_ALLAPOT_TovabbitasAlatt -- mindenképp továbbítás alatti lesz
				else
					case [EREC_PldIratPeldanyok].Allapot
						when @kt_IRATPELDANY_ALLAPOT_TovabbitasAlatt then [EREC_PldIratPeldanyok].TovabbitasAlattAllapot
						else [EREC_PldIratPeldanyok].Allapot
					end
				end,
			Modosito_id = @ExecutorUserId,
			ModositasIdo = @ExecutionTime
			--,Ver = [EREC_PldIratPeldanyok].Ver + 1
		OUTPUT inserted.id INTO @pldIds
		FROM [EREC_KuldKuldemenyek]
			 INNER JOIN [#kuldIds_kimeno]
			 ON [EREC_KuldKuldemenyek].[Id]=[#kuldIds_kimeno].Id and [EREC_KuldKuldemenyek].PostazasIranya = @kt_POSTAZAS_IRANYA_Kimeno
			 INNER JOIN [EREC_Kuldemeny_IratPeldanyai]
			 ON [EREC_KuldKuldemenyek].[Id]=[EREC_Kuldemeny_IratPeldanyai].KuldKuldemeny_Id
			 INNER JOIN [EREC_PldIratPeldanyok]
			 ON [EREC_PldIratPeldanyok].[Id]=[EREC_Kuldemeny_IratPeldanyai].Peldany_Id
		WHERE  getdate() between [EREC_Kuldemeny_IratPeldanyai].ErvKezd and [EREC_Kuldemeny_IratPeldanyai].ErvVege
			and [EREC_PldIratPeldanyok].Allapot <> @kt_IRATPELDANY_ALLAPOT_Sztornozott

	--Iratok Update
	UPDATE [EREC_IraIratok]
		SET Csoport_Id_Felelos = [EREC_PldIratPeldanyok].Csoport_Id_Felelos,
			FelhasznaloCsoport_Id_Orzo = [EREC_PldIratPeldanyok].FelhasznaloCsoport_Id_Orzo,
			Modosito_id = @ExecutorUserId,
			ModositasIdo = @ExecutionTime
			--,Ver = EREC_IraIratok.Ver + 1
		FROM [EREC_PldIratPeldanyok]
			 INNER JOIN @pldIds pIds
			 ON [EREC_PldIratPeldanyok].[Id]=pIds.Id
		WHERE [EREC_PldIratPeldanyok].[Sorszam]  = 1 
		AND [EREC_PldIratPeldanyok].IraIrat_Id = [EREC_IraIratok].Id
		AND [EREC_IraIratok].Allapot NOT IN (@kt_IRAT_ALLAPOT_Felszabaditva,@kt_IRAT_ALLAPOT_Atiktatott) --felszabadított, átiktatott
			 
			 
	IF @Id IS NOT NULL
	BEGIN
		DROP TABLE [#kuldIds_kimeno]
	END		 


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
