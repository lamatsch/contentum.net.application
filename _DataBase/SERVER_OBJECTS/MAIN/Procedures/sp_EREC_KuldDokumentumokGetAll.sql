IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldDokumentumokGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KuldDokumentumokGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldDokumentumokGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KuldDokumentumokGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_KuldDokumentumokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_KuldDokumentumok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_KuldDokumentumok.Id,
	   EREC_KuldDokumentumok.Dokumentum_Id,
	   EREC_KuldDokumentumok.KuldKuldemeny_Id,
	   EREC_KuldDokumentumok.Leiras,
	   EREC_KuldDokumentumok.Lapszam,
	   EREC_KuldDokumentumok.BarCode,
	   EREC_KuldDokumentumok.Forras,
	   EREC_KuldDokumentumok.Formatum,
	   EREC_KuldDokumentumok.Vonalkodozas,
	   EREC_KuldDokumentumok.DokumentumSzerep,
	   EREC_KuldDokumentumok.Ver,
	   EREC_KuldDokumentumok.Note,
	   EREC_KuldDokumentumok.Stat_id,
	   EREC_KuldDokumentumok.ErvKezd,
	   EREC_KuldDokumentumok.ErvVege,
	   EREC_KuldDokumentumok.Letrehozo_id,
	   EREC_KuldDokumentumok.LetrehozasIdo,
	   EREC_KuldDokumentumok.Modosito_id,
	   EREC_KuldDokumentumok.ModositasIdo,
	   EREC_KuldDokumentumok.Zarolo_id,
	   EREC_KuldDokumentumok.ZarolasIdo,
	   EREC_KuldDokumentumok.Tranz_id,
	   EREC_KuldDokumentumok.UIAccessLog_id  
   from 
     EREC_KuldDokumentumok as EREC_KuldDokumentumok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
