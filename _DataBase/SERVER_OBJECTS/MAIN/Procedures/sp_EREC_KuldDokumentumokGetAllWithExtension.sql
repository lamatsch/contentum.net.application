IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldDokumentumokGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KuldDokumentumokGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldDokumentumokGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KuldDokumentumokGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_KuldDokumentumokGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_KuldDokumentumok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

-- A PKI_INTEGRACIO rendszerparameter erteketol fuggoen
-- eltero kodcsoport szerint dolgozunk az elektronikus
-- alairas kapcsan
DECLARE @kcsDokumentumAlairas nvarchar(64)

DECLARE @paramPKI_Integracio nvarchar(400)

SET @paramPKI_Integracio = (
	SELECT Ertek FROM KRT_Parameterek
	WHERE Nev ='PKI_INTEGRACIO'
	AND Org=@Org
)

IF @paramPKI_Integracio = 'Igen'
BEGIN
	SET @kcsDokumentumAlairas = 'DOKUMENTUM_ALAIRAS_PKI'
END
ELSE
BEGIN
	SET @kcsDokumentumAlairas = 'DOKUMENTUM_ALAIRAS_MANUALIS'
END

               
 SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_KuldDokumentumok.Id,
	   EREC_KuldDokumentumok.Dokumentum_Id,
KRT_Dokumentumok.FajlNev as FajlNev,
KRT_Dokumentumok.External_Link,
KRT_Dokumentumok.VerzioJel,
KRT_Dokumentumok.Megnyithato,
KRT_Dokumentumok.Olvashato,
KRT_Dokumentumok.ElektronikusAlairas,
dbo.fn_KodtarErtekNeve(''' + @kcsDokumentumAlairas +
	''', KRT_Dokumentumok.ElektronikusAlairas,''' + CAST(@Org as NVarChar(40)) + ''') as ElektronikusAlairasNev,
KRT_Dokumentumok.AlairasFelulvizsgalat,
KRT_Dokumentumok.Titkositas,
KRT_Dokumentumok.Tipus,
KRT_Dokumentumok.Formatum,
	   EREC_KuldDokumentumok.KuldKuldemeny_Id,
	   EREC_KuldDokumentumok.Leiras,
	   EREC_KuldDokumentumok.Lapszam,
	   EREC_KuldDokumentumok.Ver,
	   EREC_KuldDokumentumok.Note,
	   EREC_KuldDokumentumok.Stat_id,
CONVERT(nvarchar(10), EREC_KuldDokumentumok.ErvKezd, 102) as ErvKezd,
CONVERT(nvarchar(10), EREC_KuldDokumentumok.ErvVege, 102) as ErvVege,
	   EREC_KuldDokumentumok.ErvKezd,
	   EREC_KuldDokumentumok.ErvVege,
	   EREC_KuldDokumentumok.Letrehozo_id,
	   EREC_KuldDokumentumok.LetrehozasIdo,
	   EREC_KuldDokumentumok.Modosito_id,
	   EREC_KuldDokumentumok.ModositasIdo,
	   EREC_KuldDokumentumok.Zarolo_id,
	   EREC_KuldDokumentumok.ZarolasIdo,
	   EREC_KuldDokumentumok.Tranz_id,
	   EREC_KuldDokumentumok.UIAccessLog_id  
   from 
     EREC_KuldDokumentumok as EREC_KuldDokumentumok
left join KRT_Dokumentumok as KRT_Dokumentumok on EREC_KuldDokumentumok.Dokumentum_Id = KRT_Dokumentumok.Id      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
