IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldDokumentumokInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KuldDokumentumokInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldDokumentumokInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KuldDokumentumokInsert] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_KuldDokumentumokInsert]    
                @Id      uniqueidentifier = null,    
               	            @Dokumentum_Id     uniqueidentifier,
	            @KuldKuldemeny_Id     uniqueidentifier,
                @Leiras     Nvarchar(100)  = null,
                @Lapszam     int  = null,
                @BarCode     Nvarchar(100)  = null,
                @Forras     nvarchar(64)  = null,
                @Formatum     nvarchar(64)  = null,
                @Vonalkodozas     char(1)  = null,
                @DokumentumSzerep     nvarchar(64)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @myid uniqueidentifier
Set @myid   = NEWID()

if @Id is not null
begin
   SET @myid = @Id
end

DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = 'Id'
SET @insertValues = '@myid' 
       
         if @Dokumentum_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Dokumentum_Id'
            SET @insertValues = @insertValues + ',@Dokumentum_Id'
         end 
       
         if @KuldKuldemeny_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',KuldKuldemeny_Id'
            SET @insertValues = @insertValues + ',@KuldKuldemeny_Id'
         end 
       
         if @Leiras is not null
         begin
            SET @insertColumns = @insertColumns + ',Leiras'
            SET @insertValues = @insertValues + ',@Leiras'
         end 
       
         if @Lapszam is not null
         begin
            SET @insertColumns = @insertColumns + ',Lapszam'
            SET @insertValues = @insertValues + ',@Lapszam'
         end 
       
         if @BarCode is not null
         begin
            SET @insertColumns = @insertColumns + ',BarCode'
            SET @insertValues = @insertValues + ',@BarCode'
         end 
       
         if @Forras is not null
         begin
            SET @insertColumns = @insertColumns + ',Forras'
            SET @insertValues = @insertValues + ',@Forras'
         end 
       
         if @Formatum is not null
         begin
            SET @insertColumns = @insertColumns + ',Formatum'
            SET @insertValues = @insertValues + ',@Formatum'
         end 
       
         if @Vonalkodozas is not null
         begin
            SET @insertColumns = @insertColumns + ',Vonalkodozas'
            SET @insertValues = @insertValues + ',@Vonalkodozas'
         end 
       
         if @DokumentumSzerep is not null
         begin
            SET @insertColumns = @insertColumns + ',DokumentumSzerep'
            SET @insertValues = @insertValues + ',@DokumentumSzerep'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end      

DECLARE @InsertCommand NVARCHAR(4000)
SET @InsertCommand = 'insert into EREC_KuldDokumentumok ('+@insertColumns+') values ('+@insertValues+')'
exec sp_executesql @InsertCommand, 
                             N'@myid uniqueidentifier,@Dokumentum_Id uniqueidentifier,@KuldKuldemeny_Id uniqueidentifier,@Leiras Nvarchar(100),@Lapszam int,@BarCode Nvarchar(100),@Forras nvarchar(64),@Formatum nvarchar(64),@Vonalkodozas char(1),@DokumentumSzerep nvarchar(64),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier'
     ,@myid = @myid,@Dokumentum_Id = @Dokumentum_Id,@KuldKuldemeny_Id = @KuldKuldemeny_Id,@Leiras = @Leiras,@Lapszam = @Lapszam,@BarCode = @BarCode,@Forras = @Forras,@Formatum = @Formatum,@Vonalkodozas = @Vonalkodozas,@DokumentumSzerep = @DokumentumSzerep,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id

If @@error = 0
BEGIN
   SET @ResultUid = @myid
      /* History Log */
   exec sp_LogRecordToHistory 'EREC_KuldDokumentumok',@myid
					,'EREC_KuldDokumentumokHistory',0,@Letrehozo_id,@LetrehozasIdo   
END
ELSE
BEGIN
   RAISERROR('[50301]',16,1)
END

--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH



GO
