IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKapcsolatokGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KuldKapcsolatokGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKapcsolatokGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KuldKapcsolatokGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_KuldKapcsolatokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_KuldKapcsolatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_KuldKapcsolatok.Id,
	   EREC_KuldKapcsolatok.KapcsolatTipus,
	   EREC_KuldKapcsolatok.Leiras,
	   EREC_KuldKapcsolatok.Kezi,
	   EREC_KuldKapcsolatok.Kuld_Kuld_Beepul,
	   EREC_KuldKapcsolatok.Kuld_Kuld_Felepul,
	   EREC_KuldKapcsolatok.Ver,
	   EREC_KuldKapcsolatok.Note,
	   EREC_KuldKapcsolatok.Stat_id,
	   EREC_KuldKapcsolatok.ErvKezd,
	   EREC_KuldKapcsolatok.ErvVege,
	   EREC_KuldKapcsolatok.Letrehozo_id,
	   EREC_KuldKapcsolatok.LetrehozasIdo,
	   EREC_KuldKapcsolatok.Modosito_id,
	   EREC_KuldKapcsolatok.ModositasIdo,
	   EREC_KuldKapcsolatok.Zarolo_id,
	   EREC_KuldKapcsolatok.ZarolasIdo,
	   EREC_KuldKapcsolatok.Tranz_id,
	   EREC_KuldKapcsolatok.UIAccessLog_id  
   from 
     EREC_KuldKapcsolatok as EREC_KuldKapcsolatok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
