IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKezFeljegyzesekGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KuldKezFeljegyzesekGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKezFeljegyzesekGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KuldKezFeljegyzesekGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_KuldKezFeljegyzesekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_KuldKezFeljegyzesek.Id,
	   EREC_KuldKezFeljegyzesek.KuldKuldemeny_Id,
	   EREC_KuldKezFeljegyzesek.KezelesTipus,
	   EREC_KuldKezFeljegyzesek.Leiras,
	   EREC_KuldKezFeljegyzesek.Ver,
	   EREC_KuldKezFeljegyzesek.Note,
	   EREC_KuldKezFeljegyzesek.Stat_id,
	   EREC_KuldKezFeljegyzesek.ErvKezd,
	   EREC_KuldKezFeljegyzesek.ErvVege,
	   EREC_KuldKezFeljegyzesek.Letrehozo_id,
	   EREC_KuldKezFeljegyzesek.LetrehozasIdo,
	   EREC_KuldKezFeljegyzesek.Modosito_id,
	   EREC_KuldKezFeljegyzesek.ModositasIdo,
	   EREC_KuldKezFeljegyzesek.Zarolo_id,
	   EREC_KuldKezFeljegyzesek.ZarolasIdo,
	   EREC_KuldKezFeljegyzesek.Tranz_id,
	   EREC_KuldKezFeljegyzesek.UIAccessLog_id
	   from 
		 EREC_KuldKezFeljegyzesek as EREC_KuldKezFeljegyzesek 
	   where
		 EREC_KuldKezFeljegyzesek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
