IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKezFeljegyzesekGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KuldKezFeljegyzesekGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKezFeljegyzesekGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KuldKezFeljegyzesekGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_KuldKezFeljegyzesekGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_KuldKezFeljegyzesek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_KuldKezFeljegyzesek.Id,
	   EREC_KuldKezFeljegyzesek.KuldKuldemeny_Id,
	   EREC_KuldKezFeljegyzesek.KezelesTipus,
	   EREC_KuldKezFeljegyzesek.Leiras,
	   EREC_KuldKezFeljegyzesek.Ver,
	   EREC_KuldKezFeljegyzesek.Note,
	   EREC_KuldKezFeljegyzesek.Stat_id,
	   EREC_KuldKezFeljegyzesek.ErvKezd,
	   EREC_KuldKezFeljegyzesek.ErvVege,
	   EREC_KuldKezFeljegyzesek.Letrehozo_id,
	   EREC_KuldKezFeljegyzesek.LetrehozasIdo,
	   EREC_KuldKezFeljegyzesek.Modosito_id,
	   EREC_KuldKezFeljegyzesek.ModositasIdo,
	   EREC_KuldKezFeljegyzesek.Zarolo_id,
	   EREC_KuldKezFeljegyzesek.ZarolasIdo,
	   EREC_KuldKezFeljegyzesek.Tranz_id,
	   EREC_KuldKezFeljegyzesek.UIAccessLog_id  
   from 
     EREC_KuldKezFeljegyzesek as EREC_KuldKezFeljegyzesek      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
