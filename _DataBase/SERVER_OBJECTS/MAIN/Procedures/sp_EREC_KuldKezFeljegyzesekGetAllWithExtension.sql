IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKezFeljegyzesekGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KuldKezFeljegyzesekGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKezFeljegyzesekGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KuldKezFeljegyzesekGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_KuldKezFeljegyzesekGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_KuldKezFeljegyzesek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
               

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_KuldKezFeljegyzesek.Id,
	   EREC_KuldKezFeljegyzesek.Leiras,
	   EREC_KuldKezFeljegyzesek.KuldKuldemeny_Id,	   
	   EREC_KuldKezFeljegyzesek.KezelesTipus,
KRT_KodTarak.Nev as KezelesTipusNev,
EREC_IraIktatoKonyvek.MegkulJelzes + '' - '' + CAST(EREC_KuldKuldemenyek.Erkezteto_Szam AS NVarchar(100)) + '' / '' + CAST(EREC_IraIktatoKonyvek.Ev AS NVarchar(100)) FullErkeztetoSzam,
0 as Sorszam,	-- kompatibilitás a példányokkal
	   EREC_KuldKezFeljegyzesek.Ver,
	   EREC_KuldKezFeljegyzesek.Note,
	   EREC_KuldKezFeljegyzesek.Stat_id,
	   EREC_KuldKezFeljegyzesek.ErvKezd,
	   EREC_KuldKezFeljegyzesek.ErvVege,
	   EREC_KuldKezFeljegyzesek.Letrehozo_id,
KRT_Felhasznalok.Nev as Letrehozo_Nev,
CONVERT(nvarchar(10), EREC_KuldKezFeljegyzesek.LetrehozasIdo, 102) as LetrehozasIdo,
	   EREC_KuldKezFeljegyzesek.Modosito_id,
	   EREC_KuldKezFeljegyzesek.ModositasIdo,
	   EREC_KuldKezFeljegyzesek.Zarolo_id,
	   EREC_KuldKezFeljegyzesek.ZarolasIdo,
	   EREC_KuldKezFeljegyzesek.Tranz_id,
	   EREC_KuldKezFeljegyzesek.UIAccessLog_id  
   from 
    EREC_KuldKezFeljegyzesek as EREC_KuldKezFeljegyzesek 
		left join KRT_KodCsoportok on KRT_KodCsoportok.Kod=''KEZELESI_FELJEGYZESEK_TIPUSA''
		left join KRT_KodTarak on KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and EREC_KuldKezFeljegyzesek.KezelesTipus = KRT_KodTarak.Kod and KRT_KodTarak.Org=''' + CAST(@Org as NVarChar(40)) + '''
 	LEFT JOIN KRT_Felhasznalok
		ON EREC_KuldKezFeljegyzesek.Letrehozo_id = KRT_Felhasznalok.Id
left join EREC_KuldKuldemenyek on EREC_KuldKezFeljegyzesek.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
left join EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek on EREC_IraIktatoKonyvek.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
LEFT JOIN EREC_IraIratok ON EREC_KuldKuldemenyek.Id = EREC_IraIratok.KuldKuldemenyek_Id
LEFT JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok
		ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
	LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok 
			ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
