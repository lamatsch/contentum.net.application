IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKuldemenyekGetAllEsemenyWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KuldKuldemenyekGetAllEsemenyWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKuldemenyekGetAllEsemenyWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KuldKuldemenyekGetAllEsemenyWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_KuldKuldemenyekGetAllEsemenyWithExtension]
  @KuldemenyId			UNIQUEIDENTIFIER,
  @Where nvarchar(MAX) = '', -- KRT_Esemenyekre vonatkozó feltétel
  @OrderBy			nvarchar(200) = ' order by KRT_Esemenyek.LetrehozasIdo',
  @TopRow			nvarchar(5) = '',
  @ExecutorUserId	uniqueidentifier,
  @pageNumber		int = 0,
  @pageSize			int = -1,
  @SelectedRowId	uniqueidentifier = null

as

begin

BEGIN TRY
	set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	DECLARE @sqlcmd nvarchar(MAX)
	DECLARE @LocalTopRow nvarchar(10)
	declare @firstRow int
	declare @lastRow int

	IF @Where is null
	BEGIN
		set @Where = ''
	END

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
		if (@pageSize > @TopRow)
			SET @pageSize = @TopRow;
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
                 
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = 1000
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end

	IF @Where <> ''
	BEGIN
		set @Where = N' and ' + @Where
	END

	set @sqlcmd = N'
	SELECT Id into #filter FROM
	( 
		SELECT	KRT_Esemenyek.Id
			FROM EREC_KuldKuldemenyek
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_KuldKuldemenyek.Id
			WHERE EREC_KuldKuldemenyek.Id = ''' + CAST(@KuldemenyId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL ' + @Where + '
		union all
		SELECT KRT_Esemenyek.Id
            FROM  EREC_KuldKuldemenyek
			    INNER JOIN HKP_DokumentumAdatok ON EREC_KuldKuldemenyek.Id = HKP_DokumentumAdatok.KuldKuldemeny_Id 
				INNER JOIN KRT_Esemenyek ON dbo.HKP_DokumentumAdatok.Id = dbo.KRT_Esemenyek.Obj_Id 
            WHERE EREC_KuldKuldemenyek.Id = ''' + CAST(@KuldemenyId AS CHAR(36)) + N'''
			 AND KRT_Esemenyek.Funkcio_Id IS NOT NULL ' + @Where + '
	) as KRT_Esemenyek ';
	
	set @sqlcmd = @sqlcmd + N'
	INSERT INTO #filter SELECT Id FROM
	( 
		SELECT	KRT_Esemenyek.Id
			FROM EREC_KuldKuldemenyek
				INNER JOIN EREC_HataridosFeladatok on EREC_HataridosFeladatok.Obj_Id = EREC_KuldKuldemenyek.Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_HataridosFeladatok.Id
			WHERE EREC_KuldKuldemenyek.Id = ''' + CAST(@KuldemenyId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL ' + @Where + '
	) as KRT_Esemenyek ';

	
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   KRT_Esemenyek.Id into #result
		from KRT_Esemenyek as KRT_Esemenyek
			inner join KRT_Felhasznalok as KRT_Felhasznalok on KRT_Esemenyek.Felhasznalo_Id_User = KRT_Felhasznalok.Id
			inner join KRT_Felhasznalok as KRT_Felhasznalok1 on KRT_Esemenyek.Felhasznalo_Id_Login = KRT_Felhasznalok1.Id 
			left join KRT_Csoportok on KRT_Csoportok.Id = KRT_Esemenyek.Csoport_Id_FelelosUserSzerveze
			inner join KRT_ObjTipusok on KRT_ObjTipusok.Id = KRT_Esemenyek.ObjTip_Id
			inner join KRT_Funkciok as KRT_Funkciok on KRT_Esemenyek.Funkcio_Id = KRT_Funkciok.Id    
        where KRT_Esemenyek.Id in (select Id from #filter); '

		if (@SelectedRowId is not null)
			set @sqlcmd = @sqlcmd + N'
			if exists (select 1 from #result where Id = ''' + cast(@SelectedRowId as nvarchar(36)) + ''')
			BEGIN
				select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = ''' + cast(@SelectedRowId as nvarchar(36)) + ''';
				set @firstRow = (@pageNumber - 1) * @pageSize + 1;
				set @lastRow = @pageNumber*@pageSize;
				select @pageNumber = @pageNumber - 1
			END
			ELSE'
		--ELSE
			set @sqlcmd = @sqlcmd + N' 
				if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
				BEGIN
					select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
					set @firstRow = (@pageNumber - 1) * @pageSize + 1;
					set @lastRow = @pageNumber*@pageSize;
					select @pageNumber = @pageNumber - 1
				END ;'
			
		set @sqlcmd = @sqlcmd + N'
		select 
		#result.RowNumber,
		#result.Id,
		
	   KRT_Esemenyek.Obj_Id,
	   KRT_Esemenyek.ObjTip_Id,
	   --KRT_ObjTipusok.Nev as ObjTip_Id_Nev,
	   CASE KRT_ObjTipusok.Kod
			WHEN ''EREC_UgyUgyiratok'' THEN ''Ügyirat''
			WHEN ''EREC_UgyUgyiratdarabok'' THEN ''Ügyiratdarab''
			WHEN ''EREC_IraIratok'' THEN ''Irat''
			WHEN ''EREC_PldIratpeldanyok'' THEN ''Iratpéldány''
			WHEN ''EREC_KuldKuldemenyek'' THEN ''Küldemény''
			WHEN ''EREC_HataridosFeladatok'' THEN ''Feljegyzés''
			WHEN ''HKP_DokumentumAdatok'' THEN ''Hivatali kapu üzenet''
			ELSE KRT_ObjTipusok.Nev
	   END as ObjTip_Id_Nev,
	   KRT_Esemenyek.Azonositoja,
	   KRT_Esemenyek.Felhasznalo_Id_User,
KRT_Felhasznalok.Nev as Felhasznalo_Id_User_Nev,
	   KRT_Esemenyek.Csoport_Id_FelelosUserSzerveze,
KRT_Csoportok.Nev as Csoport_Id_FelelosUserSzerveze_Nev,
	   KRT_Esemenyek.Felhasznalo_Id_Login,
KRT_Felhasznalok1.Nev as Felhasznalo_Id_Login_Nev,
	   KRT_Esemenyek.Csoport_Id_Cel,
	   KRT_Esemenyek.Helyettesites_Id,
KRT_Helyettesitesek.HelyettesitesMod as HelyettesitesMod,
dbo.fn_KodTarErtekNeve(''HELYETTESITES_MOD'',KRT_Helyettesitesek.HelyettesitesMod,''' + CAST(@Org as NVarChar(40)) + ''') as HelyettesitesMod_Nev,
	   KRT_Esemenyek.Tranzakcio_Id,
	   KRT_Esemenyek.Funkcio_Id,
KRT_Funkciok.Nev as Funkciok_Nev,
	   KRT_Esemenyek.Ver,
	   KRT_Esemenyek.Note,
	   KRT_Esemenyek.Stat_id,
	   KRT_Esemenyek.ErvKezd,
	   KRT_Esemenyek.ErvVege,
	   KRT_Esemenyek.Letrehozo_id,
KRT_Esemenyek.LetrehozasIdo,
	   KRT_Esemenyek.Modosito_id,
	   KRT_Esemenyek.ModositasIdo,
	   KRT_Esemenyek.Zarolo_id,
	   KRT_Esemenyek.ZarolasIdo,
	   KRT_Esemenyek.Tranz_id,
	   KRT_Esemenyek.UIAccessLog_id,
	   KRT_Esemenyek.Munkaallomas,
	   KRT_Esemenyek.MuveletKimenete  
   from 
     KRT_Esemenyek as KRT_Esemenyek
     		inner join #result on #result.Id = KRT_Esemenyek.Id				
inner join KRT_Felhasznalok as KRT_Felhasznalok on KRT_Esemenyek.Felhasznalo_Id_User = KRT_Felhasznalok.Id
inner join KRT_Felhasznalok as KRT_Felhasznalok1 on KRT_Esemenyek.Felhasznalo_Id_Login = KRT_Felhasznalok1.Id 
left join KRT_Csoportok on KRT_Csoportok.Id = KRT_Esemenyek.Csoport_Id_FelelosUserSzerveze
inner join KRT_ObjTipusok on KRT_ObjTipusok.Id = KRT_Esemenyek.ObjTip_Id
inner join KRT_Funkciok as KRT_Funkciok on KRT_Esemenyek.Funkcio_Id = KRT_Funkciok.Id
left join KRT_Helyettesitesek as KRT_Helyettesitesek on KRT_Esemenyek.Helyettesites_Id = KRT_Helyettesitesek.Id    
           		where RowNumber between @firstRow and @lastRow
	ORDER BY #result.RowNumber;'
	
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter;';
	
	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
