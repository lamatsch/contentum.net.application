IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKuldemenyekGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KuldKuldemenyekGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKuldemenyekGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KuldKuldemenyekGetAllWithExtension] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_KuldKuldemenyekGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @Where_KuldKuldemenyek_Csatolt nvarchar(MAX) = '',
  @Where_Dosszie  NVARCHAR(MAX) = '',
  @Where_Szamlak NVARCHAR(MAX) = '',
  @OrderBy nvarchar(200) = ' order by EREC_KuldKuldemenyek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
--  @ObjektumTargyszavai_ObjIdFilter nvarchar(MAX) = '', -- SELECT ... FROM EREC_ObjektumTargySzavai ...
  @ExecutorUserId          uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier,
  @Jogosultak     char(1) = '0',
  @pageNumber     int = 0,
  @pageSize       int = -1,
  @SelectedRowId  uniqueidentifier = NULL,
  @AlSzervezetId UNIQUEIDENTIFIER = null

as

begin

BEGIN TRY

   set nocount on

   DECLARE @Org uniqueidentifier
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
   if (@Org is null)
   begin
      RAISERROR('[50202]',16,1)
   end
   
   DECLARE @OrgKod nvarchar(100)
   set @OrgKod = (select Kod from KRT_Orgok where Id = @Org)
   
   declare @OrgIsBOMPH int
   
   IF(@OrgKod = 'BOPMH')
	  set @OrgIsBOMPH = 1
    ELSE
      set @OrgIsBOMPH = 0
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)
   declare @firstRow int
   declare @lastRow int

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
      if (@pageSize > @TopRow)
         SET @pageSize = @TopRow;
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

   if (@pageSize > 0 and @pageNumber > -1)
   begin
      set @firstRow = (@pageNumber)*@pageSize + 1
      set @lastRow = @firstRow + @pageSize - 1
   end
   else begin
      if (@TopRow = '' or @TopRow = '0')
      begin
         set @firstRow = 1
         set @lastRow = 1000
      end 
      else
      begin
         set @firstRow = 1
         set @lastRow = @TopRow
      end   
   END
   
   DECLARE @FelhasznaloId UNIQUEIDENTIFIER
   DECLARE @SzervezetId UNIQUEIDENTIFIER
   
   IF @AlSzervezetId IS NULL
   BEGIN
      SET @FelhasznaloId = @ExecutorUserId
      SET @SzervezetId = @FelhasznaloSzervezet_Id
   END
   ELSE
   BEGIN
      SET @FelhasznaloId = dbo.fn_GetLeader(@AlSzervezetId)
      
      IF @FelhasznaloId is NULL
      BEGIN
         -- Az alszervezetnek nincsen vezetoje
         RAISERROR('[53203]',16,1)
      END

      SET @SzervezetId = @AlSzervezetId
   END
   
   set @sqlcmd = '';
   

   /************************************************************
   * Szűrési tábla összeállítása                      *
   ************************************************************/
-- DECLARE @ObjTipId uniqueidentifier;
-- select @ObjTipId = Id from KRT_Objtipusok where Kod = 'EREC_KuldKuldemenyek';

   SET @sqlcmd = ';with mappa_tartalmak as
            (
               SELECT KRT_MappaTartalmak.Obj_Id 
               FROM KRT_MappaTartalmak
               INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
               INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
               INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
               where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
               and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
            )
            select EREC_KuldKuldemenyek.Id into #filter from EREC_KuldKuldemenyek
            left join KRT_Cimek as KRT_Cimek on KRT_Cimek.Id = EREC_KuldKuldemenyek.Cim_Id
            left join EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek on EREC_IraIktatoKonyvek.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
            left join KRT_Partnerek as KRT_PartnerekBekuldok on KRT_PartnerekBekuldok.Id = EREC_KuldKuldemenyek.Partner_Id_Bekuldo
Where @Org=IsNull(EREC_IraIktatoKonyvek.Org, (select Org from KRT_Felhasznalok where EREC_KuldKuldemenyek.Letrehozo_id=KRT_Felhasznalok.Id))
'

    if @Where is not null and @Where!=''
   begin 
      SET @sqlcmd = @sqlcmd + ' and ' + @Where;
   END

   --IF @Jogosultak = '1' AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
   IF @Jogosultak = '1' AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
   BEGIN
      SET @sqlcmd = @sqlcmd + ' 
      and EREC_KuldKuldemenyek.Id IN
      (
         SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
            INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.Id
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll
               ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany'

     SET @sqlcmd = @sqlcmd + dbo.fn_GetJogosultakFilter(@Org, 'krt_jogosultak')

     SET @sqlcmd = @sqlcmd + '
         UNION ALL
         SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
            INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll
               ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
         UNION ALL
         SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
            INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIratok_Id
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
               ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
         UNION ALL
         SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
               ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.Csoport_Id_Felelos
         UNION ALL
         SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
               ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
         UNION ALL
         SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
             INNER JOIN mappa_tartalmak ON mappa_tartalmak.Obj_Id = EREC_KuldKuldemenyek.Id
         UNION ALL
         '
/* belső iratokra, elvileg ez is kéne                 
      SET @sqlcmd = @sqlcmd + 'SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
            INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_KuldKuldemenyek.IraIratok_Id
            WHERE EREC_KuldKuldemenyek.IraIratok_Id IS NOT NULL
            AND EREC_IraIratok.Id IN
            ( 
            SELECT EREC_IraIratok.Id FROM EREC_IraIratok
            WHERE EREC_IraIratok.Ugyirat_Id IN 
               (
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
                        ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
                        ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
                        ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
                  UNION ALL
                  SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@FelhasznaloId,  @SzervezetId, ''EREC_UgyUgyiratok'')
               )
            UNION ALL --CR2407: lenézünk a példányba, ha látja az iratot, akkor a küldeményt is lássa
             SELECT EREC_IraIratok.Id FROM EREC_IraIratok
            INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = EREC_IraIratok.Id
            WHERE EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo = @FelhasznaloId and EREC_PldIratpeldanyok.Csoport_Id_Felelos = @FelhasznaloId
            )     
         UNION ALL
         '
*/                            
      SET @sqlcmd = @sqlcmd + 'SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
            INNER JOIN EREC_IraIratok ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id
            WHERE EREC_IraIratok.Id IN
            (
               SELECT EREC_IraIratok.Id FROM EREC_IraIratok
               WHERE EREC_IraIratok.Ugyirat_Id IN 
               (
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany'
       
	   SET @sqlcmd = @sqlcmd + dbo.fn_GetJogosultakFilter(@Org, 'krt_jogosultak')

	   SET @sqlcmd = @sqlcmd + '
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
                        ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
                        ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
                        ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
                  UNION ALL
                  SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@FelhasznaloId,  @SzervezetId, ''EREC_UgyUgyiratok'')
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN mappa_tartalmak ON mappa_tartalmak.Obj_Id = erec_ugyugyiratok.Id
               )
               UNION ALL --CR2407: lenézünk a példányba, ha látja az iratot, akkor a küldeményt is lássa
               SELECT EREC_IraIratok.Id FROM EREC_IraIratok
               INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = EREC_IraIratok.Id
               WHERE EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo = @FelhasznaloId and EREC_PldIratpeldanyok.Csoport_Id_Felelos = @FelhasznaloId
            )  
         UNION ALL
         SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@FelhasznaloId,  @SzervezetId, ''EREC_KuldKuldemenyek'')
      )'
   END

   IF @Where_Dosszie IS NOT NULL AND @Where_Dosszie != ''
      SET @sqlcmd = @sqlcmd + ' delete from #filter where Id not in (
         SELECT KRT_MappaTartalmak.Obj_Id
         FROM KRT_MappaTartalmak
            INNER JOIN KRT_Mappak on KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
         WHERE GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
            AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
            AND KRT_MappaTartalmak.Obj_type = ''EREC_KuldKuldemenyek''
            AND ' + @Where_Dosszie + '); '

   IF @Where_Szamlak IS NOT NULL AND @Where_Szamlak != ''
      SET @sqlcmd = @sqlcmd + ' delete from #filter where Id not in (
         SELECT EREC_Szamlak.KuldKuldemeny_Id
         FROM EREC_Szamlak
         WHERE GETDATE() BETWEEN EREC_Szamlak.ErvKezd AND EREC_Szamlak.ErvVege
            AND EREC_Szamlak.KuldKuldemeny_Id is not null
            AND ' + @Where_Szamlak + '); '


   IF @Where_KuldKuldemenyek_Csatolt IS NOT NULL AND @Where_KuldKuldemenyek_Csatolt != ''
      SET @sqlcmd = @sqlcmd + ' delete from #filter where Id not in (
         SELECT EREC_KuldKuldemenyek.Id
         FROM EREC_KuldKuldemenyek k
            LEFT JOIN EREC_UgyiratObjKapcsolatok uok on k.Id in (uok.Obj_Id_Elozmeny, uok.Obj_Id_Kapcsolt)
            LEFT JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id in (uok.Obj_Id_Elozmeny, uok.Obj_Id_Kapcsolt)
            and EREC_KuldKuldemenyek.Id <> k.Id
         WHERE getdate() between k.ErvKezd AND k.ErvVege
            AND getdate() between uok.ErvKezd AND uok.ErvVege
            AND getdate() between EREC_KuldKuldemenyek.ErvKezd AND EREC_KuldKuldemenyek.ErvVege
            AND ' + @Where_KuldKuldemenyek_Csatolt + '); '


-- IF @ObjektumTargyszavai_ObjIdFilter is not null and @ObjektumTargyszavai_ObjIdFilter != ''
-- BEGIN
--    SET @sqlcmd = @sqlcmd + '
--;delete from #filter where Id not in (' + @ObjektumTargyszavai_ObjIdFilter + ' AND getdate() BETWEEN EREC_ObjektumTargyszavai.ErvKezd AND EREC_ObjektumTargyszavai.ErvVege)
--'
-- END

            
   /************************************************************
   * Szűrt adatokhoz rendezés és sorszám összeállítása         *
   ************************************************************/
      SET @sqlcmd = @sqlcmd + N'
   select 
      row_number() over('+@OrderBy+') as RowNumber,
      EREC_KuldKuldemenyek.Id into #result
       from EREC_KuldKuldemenyek as EREC_KuldKuldemenyek 
         left join KRT_Cimek as KRT_Cimek on KRT_Cimek.Id = EREC_KuldKuldemenyek.Cim_Id
         left join EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek on EREC_IraIktatoKonyvek.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
         left join KRT_Partnerek as KRT_PartnerekBekuldok on KRT_PartnerekBekuldok.Id = EREC_KuldKuldemenyek.Partner_Id_Bekuldo
         left join KRT_Csoportok as Csoportok_CimzettNev on Csoportok_CimzettNev.Id = EREC_KuldKuldemenyek.Csoport_Id_Cimzett
         left join KRT_Csoportok as Csoportok_FelelosNev on Csoportok_FelelosNev.Id = EREC_KuldKuldemenyek.Csoport_Id_Felelos
         left join KRT_Csoportok as Csoportok_OrzoNev on Csoportok_OrzoNev.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
         left join KRT_Csoportok as Csoportok_ErkeztetoNev on Csoportok_ErkeztetoNev.Id = EREC_KuldKuldemenyek.Letrehozo_id
         left join KRT_Kodcsoportok as AllapotKodCsoport on AllapotKodCsoport.Kod = ''KULDEMENY_ALLAPOT''
            left join KRT_Kodtarak as AllapotKodTarak on AllapotKodTarak.Kodcsoport_Id = AllapotKodCsoport.Id and AllapotKodTarak.Kod = EREC_KuldKuldemenyek.Allapot and AllapotKodTarak.Org=@Org
         left join KRT_Kodcsoportok as IktatasiKotelezettsegKodCsoport on IktatasiKotelezettsegKodCsoport.Kod = ''KULDEMENY_KULDES_MODJA''
            left join KRT_Kodtarak as IktatasiKotelezettsegKodTarak on IktatasiKotelezettsegKodTarak.Kodcsoport_Id = IktatasiKotelezettsegKodCsoport.Id and IktatasiKotelezettsegKodTarak.Kod = EREC_KuldKuldemenyek.IktatniKell
and IktatasiKotelezettsegKodTarak.Org=@Org
      left join EREC_Szamlak on EREC_Szamlak.KuldKuldemeny_Id=EREC_KuldKuldemenyek.Id	  
	  where EREC_KuldKuldemenyek.Id in (select Id from #filter); '

      if (@SelectedRowId is not null)
         set @sqlcmd = @sqlcmd + N'
         if exists (select 1 from #result where Id = @SelectedRowId)
         BEGIN
            select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
            set @firstRow = (@pageNumber - 1) * @pageSize + 1;
            set @lastRow = @pageNumber*@pageSize;
            select @pageNumber = @pageNumber - 1
         END
         ELSE'
      --ELSE
         set @sqlcmd = @sqlcmd + N' 
            if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
            BEGIN
               select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
               set @firstRow = (@pageNumber - 1) * @pageSize + 1;
               set @lastRow = @pageNumber*@pageSize;
               select @pageNumber = @pageNumber - 1
            END ;'

   /************************************************************
   * Bizalmas iratok őrző szerinti szűréséhez csoporttagok lekérése*
   ************************************************************/
   --IF dbo.fn_IsAdmin(@ExecutorUserId) = 0
   IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
   begin
-- set @sqlcmd = @sqlcmd + N'; declare @iratok_executor table (Id uniqueidentifier)
--insert into @iratok_executor select Id from dbo.fn_GetAllIrat_ExecutorIsOrzoOrHisLeader(
--       ''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
--       ''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
--'
   set @sqlcmd = @sqlcmd + N'; declare @iratok_executor table (Id uniqueidentifier primary key)
'
--insert into @iratok_executor select Id from dbo.fn_GetAllConfidentialIrat_ExecutorIsOrzoOrHisLeader(
--       ''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
--       ''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
--'

   set @sqlcmd = @sqlcmd + N'; declare @kuldemenyek_executor table (Id uniqueidentifier primary key)
'
--insert into @kuldemenyek_executor select Id from dbo.fn_GetAllConfidentialKuldemeny_ExecutorIsOrzoOrHisLeader(
--       ''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
--       ''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
--'
   set @sqlcmd = @sqlcmd + N'
   DECLARE @confidential varchar(4000)
   set @confidential = (select top 1 Ertek from KRT_Parameterek where Nev=''IRAT_MINOSITES_BIZALMAS''
                     and Org=@Org and getdate() between ErvKezd and ErvVege)

   declare @Bizalmas table (val nvarchar(64) collate Hungarian_CS_AS )
   insert into @Bizalmas (val) (select Value from fn_Split(@confidential, '',''))

   DECLARE @csoporttagsag_tipus nvarchar(64)
   SET @csoporttagsag_tipus = (select top 1 KRT_CsoportTagok.Tipus from KRT_CsoportTagok
                  where KRT_CsoportTagok.Csoport_Id_Jogalany=@FelhasznaloId
                  and KRT_CsoportTagok.Csoport_Id= @SzervezetId
                  and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
                  )

   ; CREATE TABLE #jogosultak (Id uniqueidentifier)
   IF @csoporttagsag_tipus = ''3''
   BEGIN
      INSERT INTO #jogosultak select KRT_Csoportok.Id from KRT_Csoportok
                     inner join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id =  @SzervezetId 
                     and KRT_Csoportok.Tipus = ''1'' and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
                     where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege 
                     and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
   END
   ELSE
   BEGIN
      INSERT INTO #jogosultak select @FelhasznaloId
   END
'
   set @sqlcmd = @sqlcmd + N'
   insert into @kuldemenyek_executor select #result.Id
   from EREC_KuldKuldemenyek
   INNER JOIN #result on #result.Id=EREC_KuldKuldemenyek.Id and #result.RowNumber between @firstRow and @lastRow
   where 1=case when EREC_KuldKuldemenyek.Minosites in (select val from @Bizalmas)
      AND NOT EXISTS
            (select 1
               from KRT_Jogosultak where
                  KRT_Jogosultak.Obj_Id=#result.Id
                  and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
                  and @FelhasznaloId = KRT_Jogosultak.Csoport_Id_Jogalany)
      AND EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo NOT IN (select Id from #jogosultak)
   then 0
   else 1 end;
'
   set @sqlcmd = @sqlcmd + N'
   insert into @iratok_executor select EREC_IraIratok.Id
   from EREC_IraIratok
   INNER JOIN #result on #result.Id=EREC_IraIratok.KuldKuldemenyek_Id and #result.RowNumber between @firstRow and @lastRow
   where 1=case when EREC_IraIratok.Minosites in (select val from @Bizalmas)
      AND NOT EXISTS
            (select 1
               from KRT_Jogosultak where
                  KRT_Jogosultak.Obj_Id=EREC_IraIratok.Id
                  and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
                  and @FelhasznaloId = KRT_Jogosultak.Csoport_Id_Jogalany)
      AND NOT EXISTS
         (select 1
            from EREC_PldIratPeldanyok
            where EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id
               and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo in (select Id from #jogosultak)
         )
   then 0
   else 1 end;

   drop table #jogosultak
'
   end
   /************************************************************
   * Tényleges select                                 *
   ************************************************************/
      set @sqlcmd = @sqlcmd + N'
   select 
      #result.RowNumber,
      #result.Id,
      --EREC_KuldKuldemenyek.Id,
      EREC_KuldKuldemenyek.Allapot,
dbo.fn_KodtarErtekNeve(''KULDEMENY_ALLAPOT'', EREC_KuldKuldemenyek.Allapot,@Org) as AllapotNev,
dbo.fn_KodtarErtekNeve(''KULDEMENY_ALLAPOT'', EREC_KuldKuldemenyek.TovabbitasAlattAllapot,@Org) as TovabbitasAlattAllapotNev,
AllapotNev_print = dbo.fn_KodtarErtekNeve(''KULDEMENY_ALLAPOT'', EREC_KuldKuldemenyek.Allapot,@Org) +
CASE EREC_KuldKuldemenyek.Allapot
   WHEN ''04'' THEN  '' ('' +
            (SELECT TOP 1 [EREC_IraIratok].Azonosito
             FROM [EREC_IraIratok] 
             WHERE [EREC_IraIratok].[KuldKuldemenyek_Id] = [EREC_KuldKuldemenyek].[Id]
               and erec_IraIratok.Allapot != ''08'' -- hogy ne a felszabadított irat jöjjön
             ) + '')''
   WHEN ''03'' THEN '' - '' + dbo.fn_KodtarErtekNeve(''KULDEMENY_ALLAPOT'', EREC_KuldKuldemenyek.TovabbitasAlattAllapot,@Org) +
         (CASE EREC_KuldKuldemenyek.TovabbitasAlattAllapot WHEN  ''04''
            THEN '' ('' +
               (SELECT TOP 1 [EREC_IraIratok].Azonosito
                  FROM [EREC_IraIratok] 
                  WHERE [EREC_IraIratok].[KuldKuldemenyek_Id] = [EREC_KuldKuldemenyek].[Id]
                     and erec_IraIratok.Allapot != ''08'' -- hogy ne a felszabadított irat jöjjön
               ) + '')''
            ELSE ''''
         END)
      ELSE ''''
   END,
      EREC_KuldKuldemenyek.TovabbitasAlattAllapot,
      CONVERT(nvarchar(16), EREC_KuldKuldemenyek.BeerkezesIdeje, 120) as BeerkezesIdeje,
       EREC_KuldKuldemenyek.IraIktatokonyv_Id,
      EREC_KuldKuldemenyek.KuldesMod,
dbo.fn_KodtarErtekNeve(''KULDEMENY_KULDES_MODJA'', EREC_KuldKuldemenyek.KuldesMod,@Org) as KuldesModNev,
--KRT_KodTarakKuldesMod.Nev as KuldesModNev,
EREC_IraIktatoKonyvek.MegkulJelzes EREC_IraIktatoKonyvek_MegkulJelzes,
EREC_IraIktatoKonyvek.MegkulJelzes,    
EREC_KuldKuldemenyek.Erkezteto_Szam,
EREC_IraIktatoKonyvek.MegkulJelzes + '' - '' + CAST(Erkezteto_Szam AS NVarchar(100)) + '' / '' + CAST(EREC_IraIktatoKonyvek.Ev AS NVarchar(100)) FullErkeztetoSzam,
EREC_IraIktatoKonyvek.Ev EREC_IraIktatoKonyvek_Ev,
EREC_IraIktatoKonyvek.Ev,
EREC_IraIktatoKonyvek.Nev EREC_IraIktatoKonyvek_Nev,
      EREC_KuldKuldemenyek.HivatkozasiSzam,
      EREC_KuldKuldemenyek.Targy,
      EREC_KuldKuldemenyek.Tartalom,
      EREC_KuldKuldemenyek.RagSzam,
      EREC_KuldKuldemenyek.Surgosseg,
dbo.fn_KodtarErtekNeve(''SURGOSSEG'', EREC_KuldKuldemenyek.Surgosseg,@Org) as SurgossegNev,
      EREC_KuldKuldemenyek.BelyegzoDatuma,
      EREC_KuldKuldemenyek.UgyintezesModja,
      EREC_KuldKuldemenyek.PostazasIranya,
      EREC_KuldKuldemenyek.Tovabbito,
      EREC_KuldKuldemenyek.PeldanySzam,
      EREC_KuldKuldemenyek.IktatniKell,
dbo.fn_KodtarErtekNeve(''IKTATASI_KOTELEZETTSEG'', EREC_KuldKuldemenyek.IktatniKell,@Org) as IktatniKellNev,
      EREC_KuldKuldemenyek.Iktathato,
      EREC_KuldKuldemenyek.SztornirozasDat,
      EREC_KuldKuldemenyek.KuldKuldemeny_Id_Szulo,
      EREC_KuldKuldemenyek.Erkeztetes_Ev,
      EREC_KuldKuldemenyek.Csoport_Id_Cimzett,
dbo.fn_GetCsoportNev(EREC_KuldKuldemenyek.Csoport_Id_Cimzett) as Csoport_Id_CimzettNev,
      EREC_KuldKuldemenyek.Csoport_Id_Felelos,
dbo.fn_GetCsoportNev(EREC_KuldKuldemenyek.Csoport_Id_Felelos) as Csoport_Id_FelelosNev,
      EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo,
dbo.fn_GetCsoportNev(EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo) as FelhasznaloCsoport_Id_OrzoNev,
      EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Atvevo,
      EREC_KuldKuldemenyek.Partner_Id_Bekuldo,
       EREC_KuldKuldemenyek.NevSTR_Bekuldo,
(CASE WHEN EREC_KuldKuldemenyek.NevSTR_Bekuldo is NULL THEN EREC_KuldKuldemenyek.NevSTR_Bekuldo
ELSE EREC_KuldKuldemenyek.NevSTR_Bekuldo END) as Bekuldo_Id_Nev,
      EREC_KuldKuldemenyek.AdathordozoTipusa,
dbo.fn_KodtarErtekNeve(''ADATHORDOZO_TIPUSA'', EREC_KuldKuldemenyek.AdathordozoTipusa,@Org) as AdathordozoTipusa_Nev,
(select count(*) from EREC_Mellekletek where 
EREC_Mellekletek.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id) as MellekletekSzama,
      EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Alairo,   
      EREC_KuldKuldemenyek.BarCode,
      EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto,
(CASE WHEN ''04'' in (EREC_KuldKuldemenyek.Allapot, EREC_KuldKuldemenyek.TovabbitasAlattAllapot)
      THEN   (SELECT TOP 1 [EREC_IraIratok].Azonosito + ''***'' + convert(NVARCHAR(40), [EREC_IraIratok].[Id])
             FROM [EREC_IraIratok] 
             WHERE [EREC_IraIratok].[KuldKuldemenyek_Id] = [EREC_KuldKuldemenyek].[Id]
               and erec_IraIratok.Allapot not in (''06'', ''08'') -- hogy ne a felszabadított vagy átiktatott irat jöjjön
             )        
     ELSE ''''
  END) AS IratIktatoszamEsId,'

SET @sqlcmd = @sqlcmd + '
      EREC_KuldKuldemenyek.Cim_Id,
       EREC_KuldKuldemenyek.CimSTR_Bekuldo,
(CASE WHEN EREC_KuldKuldemenyek.CimSTR_Bekuldo = NULL THEN dbo.fn_MergeCim(
   KRT_Cimek.Tipus,
   KRT_Cimek.OrszagNev,
   KRT_Cimek.IRSZ,
   KRT_Cimek.TelepulesNev,
   KRT_Cimek.KozteruletNev,
   KRT_Cimek.KozteruletTipusNev,
   KRT_Cimek.Hazszam,
   KRT_Cimek.Hazszamig,
   KRT_Cimek.HazszamBetujel,
   KRT_Cimek.Lepcsohaz,
   KRT_Cimek.Szint,
   KRT_Cimek.Ajto,
   KRT_Cimek.AjtoBetujel,
   KRT_Cimek.CimTobbi) ELSE EREC_KuldKuldemenyek.CimSTR_Bekuldo END) as Cim_Id_Nev,
      EREC_KuldKuldemenyek.CsoportFelelosEloszto_Id,
      EREC_KuldKuldemenyek.Tipus,
      EREC_KUldKuldemenyek.Minosites,
      EREC_KuldKuldemenyek.MegtagadasIndoka,
      EREC_KuldKuldemenyek.Megtagado_Id,
      EREC_KuldKuldemenyek.MegtagadasDat,
   dbo.fn_GetCsoportNev(EREC_KuldKuldemenyek.Letrehozo_id) as ErkeztetoNev,
'
-- megvizsgáljuk, hogy az irat bizalmas-e, és ezesetben a felhasználó az őrző vagy az őrző vezetője-e
--if dbo.fn_IsAdmin(@ExecutorUserId) = 0 --AND @Jogosultak = '1' 
IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0 --AND @Jogosultak = '1'
begin
-- set @sqlcmd = @sqlcmd + '(SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
--and ([EREC_Csatolmanyok].IraIrat_Id is null or
--   dbo.fn_CheckIrat_IsConfidential([EREC_Csatolmanyok].IraIrat_Id) = 0 or
--   [EREC_Csatolmanyok].IraIrat_Id in (select Id from @iratok_executor))
--    AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege) as CsatolmanyCount,
--'

--set @sqlcmd = @sqlcmd + '(SELECT COUNT(*) FROM
--(select Id from [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
--and dbo.fn_CheckKuldemeny_IsConfidential([EREC_Csatolmanyok].KuldKuldemeny_Id) = 0
--and [EREC_Csatolmanyok].IraIrat_Id is null
--AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
--UNION
--select Id from [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
--and [EREC_Csatolmanyok].KuldKuldemeny_Id in (select Id from @kuldemenyek_executor)
--AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
--UNION
--select Id from [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
--and [EREC_Csatolmanyok].IraIrat_Id is not null and
--   (dbo.fn_CheckIrat_IsConfidential([EREC_Csatolmanyok].IraIrat_Id) = 0 or
--   [EREC_Csatolmanyok].IraIrat_Id in (select Id from @iratok_executor))
--AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
--) tmp) as CsatolmanyCount,
--'
--
-- set @sqlcmd = @sqlcmd + 'Dokumentum_Id = case (SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
--and ([EREC_Csatolmanyok].IraIrat_Id is null or
--   dbo.fn_CheckIrat_IsConfidential([EREC_Csatolmanyok].IraIrat_Id) = 0 or
--   [EREC_Csatolmanyok].IraIrat_Id in (select Id from @iratok_executor))
--    AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
--when 1 then
--(SELECT top 1 EREC_Csatolmanyok.Dokumentum_Id FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
--and ([EREC_Csatolmanyok].IraIrat_Id is null or
--   dbo.fn_CheckIrat_IsConfidential([EREC_Csatolmanyok].IraIrat_Id) = 0 or
--   [EREC_Csatolmanyok].IraIrat_Id in (select Id from @iratok_executor))
--    AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
--else null end,
--'

set @sqlcmd = @sqlcmd + '(
(SELECT COUNT(*) FROM [EREC_Csatolmanyok] INNER JOIN @kuldemenyek_executor ke
ON ke.Id=[EREC_Csatolmanyok].KuldKuldemeny_Id and [EREC_Csatolmanyok].IraIrat_Id is null
WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
+
(SELECT COUNT(*) FROM [EREC_Csatolmanyok] INNER JOIN @iratok_executor ie
ON ie.Id=[EREC_Csatolmanyok].IraIrat_Id
WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
) as CsatolmanyCount,
'

   set @sqlcmd = @sqlcmd + 'Dokumentum_Id = case (
(SELECT COUNT(*) FROM [EREC_Csatolmanyok] INNER JOIN @kuldemenyek_executor ke
ON ke.Id=[EREC_Csatolmanyok].KuldKuldemeny_Id and [EREC_Csatolmanyok].IraIrat_Id is null
WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
+
(SELECT COUNT(*) FROM [EREC_Csatolmanyok] INNER JOIN @iratok_executor ie
ON ie.Id=[EREC_Csatolmanyok].IraIrat_Id
WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
)
when 1 then
(SELECT top 1 EREC_Csatolmanyok.Dokumentum_Id FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
      AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
else null end,
'

end
else
begin
   set @sqlcmd = @sqlcmd + '(SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege) as CsatolmanyCount,
'

   set @sqlcmd = @sqlcmd + 'Dokumentum_Id = case (SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
when 1 then
(SELECT  top 1 EREC_Csatolmanyok.Dokumentum_Id FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
else null end,
'

end
   set @sqlcmd = @sqlcmd + 'dbo.fn_GetEREC_HataridosFeladatokCount(EREC_KuldKuldemenyek.Id,@ExecutorUserId, @FelhasznaloSzervezet_Id) as FeladatCount,
       EREC_Szamlak.PIRAllapot,
   dbo.fn_KodtarErtekNeve(''PIR_ALLAPOT'', EREC_Szamlak.PIRAllapot,@Org) as PIRAllapotNev,
      EREC_KuldKuldemenyek.Ver,
      EREC_KuldKuldemenyek.Note,
      EREC_KuldKuldemenyek.Stat_id,
      EREC_KuldKuldemenyek.ErvKezd,
      EREC_KuldKuldemenyek.ErvVege,
      EREC_KuldKuldemenyek.Letrehozo_id,
      CONVERT(nvarchar(19), EREC_KuldKuldemenyek.LetrehozasIdo, 120) as LetrehozasIdo,
      EREC_KuldKuldemenyek.Modosito_id,
      EREC_KuldKuldemenyek.ModositasIdo,
      EREC_KuldKuldemenyek.Zarolo_id,
      EREC_KuldKuldemenyek.ZarolasIdo,
      EREC_KuldKuldemenyek.Tranz_id,
      EREC_KuldKuldemenyek.UIAccessLog_id,
      EREC_KuldKuldemenyek.IktatastNemIgenyel,
      EREC_KuldKuldemenyek.KezbesitesModja,
      EREC_KuldKuldemenyek.Munkaallomas,
      EREC_KuldKuldemenyek.SerultKuldemeny,
      EREC_KuldKuldemenyek.TevesCimzes,
      EREC_KuldKuldemenyek.TevesErkeztetes,
      EREC_KuldKuldemenyek.CimzesTipusa,
	  EREC_KuldKuldemenyek.FutarJegyzekListaSzama,
	  EREC_KuldKuldemenyek.Minosito,
	  KRT_PartnerMinosito.Nev as MinositoNev,
	  EREC_KuldKuldemenyek.MinositesErvenyessegiIdeje,
	  EREC_KuldKuldemenyek.TerjedelemMennyiseg,
	  EREC_KuldKuldemenyek.TerjedelemMennyisegiEgyseg,
	  EREC_KuldKuldemenyek.TerjedelemMegjegyzes,
	  dbo.fn_KodtarErtekNeve(''IRAT_MINOSITES'', EREC_KuldKuldemenyek.Minosites,@Org) as MinositesNev,
      '''' as IktatoSzam,
(select COUNT(*) FROM EREC_UgyiratObjKapcsolatok WHERE
EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt=EREC_KuldKuldemenyek.Id
and EREC_UgyiratObjKapcsolatok.Obj_Type_Kapcsolt=''EREC_KuldKuldemenyek''  
and getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege) as CsatolasCount_Elozmeny,
(select COUNT(*) FROM EREC_UgyiratObjKapcsolatok WHERE
EREC_UgyiratObjKapcsolatok.Obj_Id_Elozmeny=EREC_KuldKuldemenyek.Id
and EREC_UgyiratObjKapcsolatok.Obj_Type_Elozmeny=''EREC_KuldKuldemenyek''  
and getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege) as CsatolasCount_Kapcsolt'

IF @OrgIsBOMPH = 1
	set @sqlcmd = @sqlcmd + '
		  ,(select top 1 dbo.fn_KodtarErtekNeve(''FELADAT_ALTIPUS'', EREC_HataridosFeladatok.Altipus, @Org)
      from EREC_HataridosFeladatok where EREC_HataridosFeladatok.Obj_Id = EREC_KuldKuldemenyek.Id
      and EREC_HataridosFeladatok.Tipus = ''M''
      order by EREC_HataridosFeladatok.LetrehozasIdo desc) as KezelesTipusNev'
ELSE
	set @sqlcmd = @sqlcmd + '
		  ,null as KezelesTipusNev'
		  
set @sqlcmd = @sqlcmd + '
   from EREC_KuldKuldemenyek as EREC_KuldKuldemenyek
      inner join #result on #result.Id = EREC_KuldKuldemenyek.Id  
      left join KRT_Cimek as KRT_Cimek on KRT_Cimek.Id = EREC_KuldKuldemenyek.Cim_Id
      left join EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek on EREC_IraIktatoKonyvek.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
      left join KRT_Partnerek as KRT_PartnerekBekuldok on KRT_PartnerekBekuldok.Id = EREC_KuldKuldemenyek.Partner_Id_Bekuldo
      left join EREC_Szamlak on EREC_Szamlak.KuldKuldemeny_Id=EREC_KuldKuldemenyek.Id
	  LEFT JOIN KRT_Partnerek KRT_PartnerMinosito on EREC_KuldKuldemenyek.Minosito = KRT_PartnerMinosito.Id
   WHERE RowNumber between @firstRow and @lastRow
   ORDER BY #result.RowNumber;'

      -- találatok száma és oldalszám
      set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter;';

      execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @ExecutorUserId uniqueidentifier, @FelhasznaloSzervezet_Id uniqueidentifier, @SelectedRowId uniqueidentifier, @Org uniqueidentifier, @FelhasznaloId uniqueidentifier, @SzervezetId uniqueidentifier'
      ,@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @ExecutorUserId = @ExecutorUserId, @FelhasznaloSzervezet_Id = @FelhasznaloSzervezet_Id, @SelectedRowId = @SelectedRowId, @Org = @Org
      ,@FelhasznaloId = @FelhasznaloId, @SzervezetId = @SzervezetId;

END TRY
BEGIN CATCH
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
