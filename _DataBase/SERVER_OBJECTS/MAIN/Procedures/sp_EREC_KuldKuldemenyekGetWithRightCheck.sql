IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKuldemenyekGetWithRightCheck]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KuldKuldemenyekGetWithRightCheck]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKuldemenyekGetWithRightCheck]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KuldKuldemenyekGetWithRightCheck] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_KuldKuldemenyekGetWithRightCheck]
         @Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FelhasznaloSzervezet_Id   uniqueidentifier,
      @Jogszint   char(1),
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

   set nocount on

   DECLARE @Org uniqueidentifier
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
   if (@Org is null)
   begin
      RAISERROR('[50202]',16,1)
   end

   --if dbo.fn_IsAdmin(@ExecutorUserId) = 0 AND not exists
   IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0 AND not exists
   (
      SELECT top(1) 1 FROM krt_jogosultak
         INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
         where krt_jogosultak.Obj_Id = @Id
   ) AND NOT EXISTS
   (
      SELECT 1 FROM EREC_KuldKuldemenyek
         INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
         INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
         where EREC_KuldKuldemenyek.Id = @Id --AND @Jogszint != 'I'
   ) AND NOT EXISTS
   (
      SELECT 1 FROM EREC_KuldKuldemenyek
         INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
            ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.Csoport_Id_Felelos
         where EREC_KuldKuldemenyek.Id = @Id
   ) AND NOT EXISTS
   (
      SELECT 1 FROM EREC_KuldKuldemenyek
         INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
            ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
         where EREC_KuldKuldemenyek.Id = @Id
   ) AND NOT EXISTS
   (
      SELECT 1 FROM EREC_KuldKuldemenyek
         INNER JOIN KRT_MappaTartalmak on KRT_MappaTartalmak.Obj_Id = EREC_KuldKuldemenyek.Id
         INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
         INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
         INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
         where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
         and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
         and EREC_KuldKuldemenyek.Id = @Id
   )
   /*Belso iratokra, elvileg ez is kéne
    AND NOT EXISTS
   (
      SELECT top(1) 1 FROM EREC_KuldKuldemenyek
         INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_KuldKuldemenyek.IraIratok_Id
         INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
         WHERE EREC_KuldKuldemenyek.Id = @Id
         AND EREC_IraIratok.Id IN
         (
             SELECT [EREC_IraIratok].[Id] FROM [EREC_IraIratok]
            WHERE [EREC_IraIratok].[Id] IN 
            (
               SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                  INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
                  INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
               UNION ALL
               SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                  INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id AND @Jogszint != 'I'
                  INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
               UNION ALL
               SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                  INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
                     ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
               UNION ALL
               SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                  INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
                     ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
               UNION ALL
               SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                  INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
                     ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
               UNION ALL
               SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_UgyUgyiratok')
            )
         UNION ALL --CR2407: lenézünk a példányba, ha látja az iratot, akkor a küldeményt is lássa
          SELECT [EREC_IraIratok].[Id] FROM [EREC_IraIratok]
         INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = EREC_IraIratok.Id
         WHERE EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo = @ExecutorUserId and EREC_PldIratpeldanyok.Csoport_Id_Felelos = @ExecutorUserId
         )  
   ) 
   */
   AND NOT EXISTS
   (
      SELECT 1 FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_KuldKuldemenyek') as ids
         WHERE ids.Id = @Id
   ) AND NOT EXISTS
   (
      SELECT top(1) 1 FROM EREC_KuldKuldemenyek
         INNER JOIN EREC_IraIratok ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id
         WHERE EREC_KuldKuldemenyek.Id = @Id
         AND EREC_IraIratok.Id IN
         (
             SELECT [EREC_IraIratok].[Id] FROM [EREC_IraIratok]
            WHERE [EREC_IraIratok].[Ugyirat_Id] IN 
            (
               SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                  INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
                  INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
               UNION ALL
               SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                  INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id AND @Jogszint != 'I'
                  INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
               UNION ALL
               SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                  INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
                     ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
               UNION ALL
               SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                  INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
                     ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
               UNION ALL
               SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                  INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
                     ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
               UNION ALL
               SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_UgyUgyiratok')
               UNION ALL
               SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                  INNER JOIN KRT_MappaTartalmak on KRT_MappaTartalmak.Obj_Id = erec_ugyugyiratok.Id
                  INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
                  INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
                  INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
                  where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
                  and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
            )
         UNION ALL --CR2407: lenézünk a példányba, ha látja az iratot, akkor a küldeményt is lássa
          SELECT [EREC_IraIratok].[Id] FROM [EREC_IraIratok]
         INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = EREC_IraIratok.Id
         WHERE EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo = @ExecutorUserId and EREC_PldIratpeldanyok.Csoport_Id_Felelos = @ExecutorUserId
         )
   )
      RAISERROR('[50102]',16,1)

   -- Org szurés
   IF(@FromHistory='')
   BEGIN
      IF not exists(select 1 from EREC_IraIktatoKonyvek
         where EREC_IraIktatoKonyvek.Org=@Org
         and EREC_IraIktatoKonyvek.Id=
         (select EREC_KuldKuldemenyek.IraIktatoKonyv_Id from EREC_KuldKuldemenyek
         where EREC_KuldKuldemenyek.Id=@Id
         ))
         and not exists(select 1 from KRT_Felhasznalok
         where KRT_Felhasznalok.Org=@Org
         and KRT_Felhasznalok.Id=
         (select EREC_KuldKuldemenyek.Letrehozo_id from EREC_KuldKuldemenyek
         where EREC_KuldKuldemenyek.Id=@Id
         ))
      BEGIN
		 print 'fromHistory'
         RAISERROR('[50101]',16,1)
      END
   END
   ELSE
   BEGIN
      IF not exists(select 1 from EREC_IraIktatoKonyvek
         where EREC_IraIktatoKonyvek.Org=@Org
         and EREC_IraIktatoKonyvek.Id=
         (select EREC_KuldKuldemenyekHistory.IraIktatoKonyv_Id from EREC_KuldKuldemenyekHistory
         where EREC_KuldKuldemenyekHistory.Id=@Id
         ))
         and not exists(select 1 from KRT_Felhasznalok
         where KRT_Felhasznalok.Org=@Org
         and KRT_Felhasznalok.Id=
         (select EREC_KuldKuldemenyekHistory.Letrehozo_id from EREC_KuldKuldemenyekHistory
         where EREC_KuldKuldemenyekHistory.Id=@Id
         ))
      BEGIN
		 print 'iktatokonyv'
         RAISERROR('[50101]',16,1)
      END
   END

   DECLARE @sqlcmd nvarchar(4000)

/*************************Begin simple get**************************/  
   if (@FromHistory='')
   begin
	select 
        EK.Id,
      EK.Allapot,
      EK.BeerkezesIdeje,
      EK.FelbontasDatuma,
      EK.IraIktatokonyv_Id,
      EK.KuldesMod,
      EK.Erkezteto_Szam,
      EK.HivatkozasiSzam,
      EK.Targy,
      EK.Tartalom,
      EK.RagSzam,
      EK.Surgosseg,
      EK.BelyegzoDatuma,
      EK.UgyintezesModja,
      EK.PostazasIranya,
      EK.Tovabbito,
      EK.PeldanySzam,
      EK.IktatniKell,
      EK.Iktathato,
      EK.SztornirozasDat,
      EK.KuldKuldemeny_Id_Szulo,
      EK.Erkeztetes_Ev,
      EK.Csoport_Id_Cimzett,
      EK.Csoport_Id_Felelos,
      EK.FelhasznaloCsoport_Id_Expedial,
      EK.ExpedialasIdeje,
      EK.FelhasznaloCsoport_Id_Orzo,
      EK.FelhasznaloCsoport_Id_Atvevo,
      EK.Partner_Id_Bekuldo,
      EK.Cim_Id,
      EK.CimSTR_Bekuldo,
      EK.NevSTR_Bekuldo,
      EK.AdathordozoTipusa,
      EK.ElsodlegesAdathordozoTipusa,
      EK.FelhasznaloCsoport_Id_Alairo,
      EK.BarCode,
      EK.FelhasznaloCsoport_Id_Bonto,
      EK.CsoportFelelosEloszto_Id,
      EK.Csoport_Id_Felelos_Elozo,
      EK.Kovetkezo_Felelos_Id,
      EK.Elektronikus_Kezbesitesi_Allap,
      EK.Kovetkezo_Orzo_Id,
      EK.Fizikai_Kezbesitesi_Allapot,
      EK.IraIratok_Id,
      EK.BontasiMegjegyzes,
      EK.Tipus,
      EK.Minosites,
      EK.MegtagadasIndoka,
      EK.Megtagado_Id,
      EK.MegtagadasDat,
      EK.KimenoKuldemenyFajta,
      EK.Elsobbsegi,
      EK.Ajanlott,
      EK.Tertiveveny,
      EK.SajatKezbe,
      EK.E_ertesites,
      EK.E_elorejelzes,
      EK.PostaiLezaroSzolgalat,
      EK.Ar,
      EK.KimenoKuld_Sorszam ,
      EK.Ver,
      EK.TovabbitasAlattAllapot,
      EK.Azonosito,
      EK.BoritoTipus,
      EK.MegorzesJelzo,
      EK.Note,
      EK.Stat_id,
      EK.ErvKezd,
      EK.ErvVege,
      EK.Letrehozo_id,
      EK.LetrehozasIdo,
      EK.Modosito_id,
      EK.ModositasIdo,
      EK.Zarolo_id,
      EK.ZarolasIdo,
      EK.Tranz_id,
      EK.UIAccessLog_id,
      EK.IktatastNemIgenyel,
      EK.KezbesitesModja,
      EK.Munkaallomas,
      EK.SerultKuldemeny,
      EK.TevesCimzes,
      EK.TevesErkeztetes,
      EK.CimzesTipusa,
	  EK.FutarJegyzekListaSzama,
	  EK.Minosito,
	  EK.MinositesErvenyessegiIdeje,
	  EK.TerjedelemMennyiseg,
	  EK.TerjedelemMennyisegiEgyseg,
	  EK.TerjedelemMegjegyzes
      from 
       EREC_KuldKuldemenyek as EK
      where
       EK.Id =  cast(@Id as nvarchar(40)) ;
   end
/*************************End simple get**************************/  

/*************************Begin history get**************************/  
   else
   begin

      select
      EREC_KuldKuldemenyek.HistoryId,
      EREC_KuldKuldemenyek.HistoryMuvelet_Id,
      EREC_KuldKuldemenyek.HistoryVegrehajto_Id,
      EREC_KuldKuldemenyek.HistoryVegrehajtasIdo,       
	   EREC_KuldKuldemenyek.Id,
       EREC_KuldKuldemenyek.Allapot,
       EREC_KuldKuldemenyek.BeerkezesIdeje,
       EREC_KuldKuldemenyek.FelbontasDatuma,
       EREC_KuldKuldemenyek.IraIktatokonyv_Id,
       EREC_KuldKuldemenyek.KuldesMod,
       EREC_KuldKuldemenyek.Erkezteto_Szam,
       EREC_KuldKuldemenyek.HivatkozasiSzam,
       EREC_KuldKuldemenyek.Targy,
       EREC_KuldKuldemenyek.RagSzam,
       EREC_KuldKuldemenyek.Surgosseg,
       EREC_KuldKuldemenyek.BelyegzoDatuma,
       EREC_KuldKuldemenyek.UgyintezesModja,
       EREC_KuldKuldemenyek.PostazasIranya,
       EREC_KuldKuldemenyek.Tovabbito,
       EREC_KuldKuldemenyek.PeldanySzam,
       EREC_KuldKuldemenyek.IktatniKell,
       EREC_KuldKuldemenyek.SztornirozasDat,
       EREC_KuldKuldemenyek.KuldKuldemeny_Id_Szulo,
       EREC_KuldKuldemenyek.Erkeztetes_Ev,
       EREC_KuldKuldemenyek.Csoport_Id_Cimzett,
       EREC_KuldKuldemenyek.Csoport_Id_Felelos,
       EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo,
       EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Atvevo,
       EREC_KuldKuldemenyek.Partner_Id_Bekuldo,
       EREC_KuldKuldemenyek.Cim_Id,
       EREC_KuldKuldemenyek.CimSTR_Bekuldo,
       EREC_KuldKuldemenyek.NevSTR_Bekuldo,
       EREC_KuldKuldemenyek.AdathordozoTipusa,
       EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Alairo,
       EREC_KuldKuldemenyek.BarCode,
       EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto,
       EREC_KuldKuldemenyek.CsoportFelelosEloszto_Id,
       EREC_KuldKuldemenyek.Csoport_Id_Felelos_Elozo,
       EREC_KuldKuldemenyek.Ver,
       EREC_KuldKuldemenyek.TovabbitasAlattAllapot,
       EREC_KuldKuldemenyek.Note,
       EREC_KuldKuldemenyek.Stat_id,
       EREC_KuldKuldemenyek.ErvKezd,
       EREC_KuldKuldemenyek.ErvVege,
       EREC_KuldKuldemenyek.Letrehozo_id,
       EREC_KuldKuldemenyek.LetrehozasIdo,
       EREC_KuldKuldemenyek.Modosito_id,
       EREC_KuldKuldemenyek.ModositasIdo,
       EREC_KuldKuldemenyek.Zarolo_id,
       EREC_KuldKuldemenyek.ZarolasIdo,
       EREC_KuldKuldemenyek.Tranz_id,
       EREC_KuldKuldemenyek.UIAccessLog_id,
       EREC_KuldKuldemenyek.IktatastNemIgenyel,
       EREC_KuldKuldemenyek.KezbesitesModja,
       EREC_KuldKuldemenyek.Munkaallomas,
       EREC_KuldKuldemenyek.SerultKuldemeny,
       EREC_KuldKuldemenyek.TevesCimzes,
       EREC_KuldKuldemenyek.TevesErkeztetes,
       EREC_KuldKuldemenyek.CimzesTipusa,
	   EREC_KuldKuldemenyek.FutarJegyzekListaSzama,
	   EREC_KuldKuldemenyek.Minosito,
	   EREC_KuldKuldemenyek.MinositesErvenyessegiIdeje,
	   EREC_KuldKuldemenyek.TerjedelemMennyiseg,
	   EREC_KuldKuldemenyek.TerjedelemMennyisegiEgyseg,
	   EREC_KuldKuldemenyek.TerjedelemMegjegyzes
      from 
       EREC_KuldKuldemenyekHistory as EREC_KuldKuldemenyek
      where
       EREC_KuldKuldemenyek.HistoryId = cast(@Id as nvarchar(40)) ;
   end;


/*************************End history get**************************/  

   exec sp_executesql @sqlcmd;
    
   if @@rowcount = 0
     begin
	 print 'nincs sor'
      RAISERROR('[50101]',16,1)
     end

END TRY
BEGIN CATCH
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
