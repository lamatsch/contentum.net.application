
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_KuldKuldemenyekInsert')
            and   type = 'P')
   drop procedure sp_EREC_KuldKuldemenyekInsert
go

create procedure sp_EREC_KuldKuldemenyekInsert    
                @Id      uniqueidentifier = null,    
                               @Allapot     nvarchar(64)  = null,
	            @BeerkezesIdeje     datetime,
                @FelbontasDatuma     datetime  = null,
                @IraIktatokonyv_Id     uniqueidentifier  = null,
	            @KuldesMod     nvarchar(64),
                @Erkezteto_Szam     int  = null,
                @HivatkozasiSzam     Nvarchar(100)  = null,
                @Targy     Nvarchar(4000)  = null,
                @Tartalom     Nvarchar(4000)  = null,
                @RagSzam     Nvarchar(400)  = null,
	            @Surgosseg     nvarchar(64),
                @BelyegzoDatuma     datetime  = null,
                @UgyintezesModja     nvarchar(64)  = null,
	            @PostazasIranya     nvarchar(64),
                @Tovabbito     nvarchar(64)  = null,
	            @PeldanySzam     int,
                @IktatniKell     nvarchar(64)  = null,
                @Iktathato     nvarchar(64)  = null,
                @SztornirozasDat     datetime  = null,
                @KuldKuldemeny_Id_Szulo     uniqueidentifier  = null,
                @Erkeztetes_Ev     int  = null,
                @Csoport_Id_Cimzett     uniqueidentifier  = null,
                @Csoport_Id_Felelos     uniqueidentifier  = null,
                @FelhasznaloCsoport_Id_Expedial     uniqueidentifier  = null,
                @ExpedialasIdeje     datetime  = null,
                @FelhasznaloCsoport_Id_Orzo     uniqueidentifier  = null,
                @FelhasznaloCsoport_Id_Atvevo     uniqueidentifier  = null,
                @Partner_Id_Bekuldo     uniqueidentifier  = null,
                @Cim_Id     uniqueidentifier  = null,
                @CimSTR_Bekuldo     Nvarchar(400)  = null,
                @NevSTR_Bekuldo     Nvarchar(400)  = null,
                @AdathordozoTipusa     nvarchar(64)  = null,
                @ElsodlegesAdathordozoTipusa     nvarchar(64)  = null,
                @FelhasznaloCsoport_Id_Alairo     uniqueidentifier  = null,
                @BarCode     Nvarchar(20)  = null,
                @FelhasznaloCsoport_Id_Bonto     uniqueidentifier  = null,
                @CsoportFelelosEloszto_Id     uniqueidentifier  = null,
                @Csoport_Id_Felelos_Elozo     uniqueidentifier  = null,
                @Kovetkezo_Felelos_Id     uniqueidentifier  = null,
                @Elektronikus_Kezbesitesi_Allap     nvarchar(64)  = null,
                @Kovetkezo_Orzo_Id     uniqueidentifier  = null,
                @Fizikai_Kezbesitesi_Allapot     nvarchar(64)  = null,
                @IraIratok_Id     uniqueidentifier  = null,
                @BontasiMegjegyzes     Nvarchar(400)  = null,
                @Tipus     nvarchar(64)  = null,
                @Minosites     nvarchar(64)  = null,
                @MegtagadasIndoka     Nvarchar(4000)  = null,
                @Megtagado_Id     uniqueidentifier  = null,
                @MegtagadasDat     datetime  = null,
                @KimenoKuldemenyFajta     nvarchar(64)  = null,
                @Elsobbsegi     char(1)  = null,
                @Ajanlott     char(1)  = null,
                @Tertiveveny     char(1)  = null,
                @SajatKezbe     char(1)  = null,
                @E_ertesites     char(1)  = null,
                @E_elorejelzes     char(1)  = null,
                @PostaiLezaroSzolgalat     char(1)  = null,
                @Ar     Nvarchar(100)  = null,
                @KimenoKuld_Sorszam      int  = null,
                @Ver     int  = null,
                @TovabbitasAlattAllapot     nvarchar(64)  = null,
                @Azonosito     Nvarchar(100)  = null,
                @BoritoTipus     nvarchar(64)  = null,
                @MegorzesJelzo     char(1)  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,
                @IktatastNemIgenyel     char(1)  = null,
                @KezbesitesModja     Nvarchar(100)  = null,
                @Munkaallomas     Nvarchar(100)  = null,
                @SerultKuldemeny     char(1)  = null,
                @TevesCimzes     char(1)  = null,
                @TevesErkeztetes     char(1)  = null,
                @CimzesTipusa     nvarchar(64)  = null,
                @FutarJegyzekListaSzama     Nvarchar(100)  = null,
                @Minosito     uniqueidentifier  = null,
                @MinositesErvenyessegiIdeje     datetime  = null,
                @TerjedelemMennyiseg     int  = null,
                @TerjedelemMennyisegiEgyseg     nvarchar(64)  = null,
                @TerjedelemMegjegyzes     Nvarchar(400)  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @BeerkezesIdeje is not null
         begin
            SET @insertColumns = @insertColumns + ',BeerkezesIdeje'
            SET @insertValues = @insertValues + ',@BeerkezesIdeje'
         end 
       
         if @FelbontasDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',FelbontasDatuma'
            SET @insertValues = @insertValues + ',@FelbontasDatuma'
         end 
       
         if @IraIktatokonyv_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IraIktatokonyv_Id'
            SET @insertValues = @insertValues + ',@IraIktatokonyv_Id'
         end 
       
         if @KuldesMod is not null
         begin
            SET @insertColumns = @insertColumns + ',KuldesMod'
            SET @insertValues = @insertValues + ',@KuldesMod'
         end 
       
         if @Erkezteto_Szam is not null
         begin
            SET @insertColumns = @insertColumns + ',Erkezteto_Szam'
            SET @insertValues = @insertValues + ',@Erkezteto_Szam'
         end 
       
         if @HivatkozasiSzam is not null
         begin
            SET @insertColumns = @insertColumns + ',HivatkozasiSzam'
            SET @insertValues = @insertValues + ',@HivatkozasiSzam'
         end 
       
         if @Targy is not null
         begin
            SET @insertColumns = @insertColumns + ',Targy'
            SET @insertValues = @insertValues + ',@Targy'
         end 
       
         if @Tartalom is not null
         begin
            SET @insertColumns = @insertColumns + ',Tartalom'
            SET @insertValues = @insertValues + ',@Tartalom'
         end 
       
         if @RagSzam is not null
         begin
            SET @insertColumns = @insertColumns + ',RagSzam'
            SET @insertValues = @insertValues + ',@RagSzam'
         end 
       
         if @Surgosseg is not null
         begin
            SET @insertColumns = @insertColumns + ',Surgosseg'
            SET @insertValues = @insertValues + ',@Surgosseg'
         end 
       
         if @BelyegzoDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',BelyegzoDatuma'
            SET @insertValues = @insertValues + ',@BelyegzoDatuma'
         end 
       
         if @UgyintezesModja is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyintezesModja'
            SET @insertValues = @insertValues + ',@UgyintezesModja'
         end 
       
         if @PostazasIranya is not null
         begin
            SET @insertColumns = @insertColumns + ',PostazasIranya'
            SET @insertValues = @insertValues + ',@PostazasIranya'
         end 
       
         if @Tovabbito is not null
         begin
            SET @insertColumns = @insertColumns + ',Tovabbito'
            SET @insertValues = @insertValues + ',@Tovabbito'
         end 
       
         if @PeldanySzam is not null
         begin
            SET @insertColumns = @insertColumns + ',PeldanySzam'
            SET @insertValues = @insertValues + ',@PeldanySzam'
         end 
       
         if @IktatniKell is not null
         begin
            SET @insertColumns = @insertColumns + ',IktatniKell'
            SET @insertValues = @insertValues + ',@IktatniKell'
         end 
       
         if @Iktathato is not null
         begin
            SET @insertColumns = @insertColumns + ',Iktathato'
            SET @insertValues = @insertValues + ',@Iktathato'
         end 
       
         if @SztornirozasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SztornirozasDat'
            SET @insertValues = @insertValues + ',@SztornirozasDat'
         end 
       
         if @KuldKuldemeny_Id_Szulo is not null
         begin
            SET @insertColumns = @insertColumns + ',KuldKuldemeny_Id_Szulo'
            SET @insertValues = @insertValues + ',@KuldKuldemeny_Id_Szulo'
         end 
       
         if @Erkeztetes_Ev is not null
         begin
            SET @insertColumns = @insertColumns + ',Erkeztetes_Ev'
            SET @insertValues = @insertValues + ',@Erkeztetes_Ev'
         end 
       
         if @Csoport_Id_Cimzett is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Cimzett'
            SET @insertValues = @insertValues + ',@Csoport_Id_Cimzett'
         end 
       
         if @Csoport_Id_Felelos is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Felelos'
            SET @insertValues = @insertValues + ',@Csoport_Id_Felelos'
         end 
       
         if @FelhasznaloCsoport_Id_Expedial is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Expedial'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Expedial'
         end 
       
         if @ExpedialasIdeje is not null
         begin
            SET @insertColumns = @insertColumns + ',ExpedialasIdeje'
            SET @insertValues = @insertValues + ',@ExpedialasIdeje'
         end 
       
         if @FelhasznaloCsoport_Id_Orzo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Orzo'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Orzo'
         end 
       
         if @FelhasznaloCsoport_Id_Atvevo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Atvevo'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Atvevo'
         end 
       
         if @Partner_Id_Bekuldo is not null
         begin
            SET @insertColumns = @insertColumns + ',Partner_Id_Bekuldo'
            SET @insertValues = @insertValues + ',@Partner_Id_Bekuldo'
         end 
       
         if @Cim_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Cim_Id'
            SET @insertValues = @insertValues + ',@Cim_Id'
         end 
       
         if @CimSTR_Bekuldo is not null
         begin
            SET @insertColumns = @insertColumns + ',CimSTR_Bekuldo'
            SET @insertValues = @insertValues + ',@CimSTR_Bekuldo'
         end 
       
         if @NevSTR_Bekuldo is not null
         begin
            SET @insertColumns = @insertColumns + ',NevSTR_Bekuldo'
            SET @insertValues = @insertValues + ',@NevSTR_Bekuldo'
         end 
       
         if @AdathordozoTipusa is not null
         begin
            SET @insertColumns = @insertColumns + ',AdathordozoTipusa'
            SET @insertValues = @insertValues + ',@AdathordozoTipusa'
         end 
       
         if @ElsodlegesAdathordozoTipusa is not null
         begin
            SET @insertColumns = @insertColumns + ',ElsodlegesAdathordozoTipusa'
            SET @insertValues = @insertValues + ',@ElsodlegesAdathordozoTipusa'
         end 
       
         if @FelhasznaloCsoport_Id_Alairo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Alairo'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Alairo'
         end 
       
         if @BarCode is not null
         begin
            SET @insertColumns = @insertColumns + ',BarCode'
            SET @insertValues = @insertValues + ',@BarCode'
         end 
       
         if @FelhasznaloCsoport_Id_Bonto is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Bonto'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Bonto'
         end 
       
         if @CsoportFelelosEloszto_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',CsoportFelelosEloszto_Id'
            SET @insertValues = @insertValues + ',@CsoportFelelosEloszto_Id'
         end 
       
         if @Csoport_Id_Felelos_Elozo is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Felelos_Elozo'
            SET @insertValues = @insertValues + ',@Csoport_Id_Felelos_Elozo'
         end 
       
         if @Kovetkezo_Felelos_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Kovetkezo_Felelos_Id'
            SET @insertValues = @insertValues + ',@Kovetkezo_Felelos_Id'
         end 
       
         if @Elektronikus_Kezbesitesi_Allap is not null
         begin
            SET @insertColumns = @insertColumns + ',Elektronikus_Kezbesitesi_Allap'
            SET @insertValues = @insertValues + ',@Elektronikus_Kezbesitesi_Allap'
         end 
       
         if @Kovetkezo_Orzo_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Kovetkezo_Orzo_Id'
            SET @insertValues = @insertValues + ',@Kovetkezo_Orzo_Id'
         end 
       
         if @Fizikai_Kezbesitesi_Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Fizikai_Kezbesitesi_Allapot'
            SET @insertValues = @insertValues + ',@Fizikai_Kezbesitesi_Allapot'
         end 
       
         if @IraIratok_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IraIratok_Id'
            SET @insertValues = @insertValues + ',@IraIratok_Id'
         end 
       
         if @BontasiMegjegyzes is not null
         begin
            SET @insertColumns = @insertColumns + ',BontasiMegjegyzes'
            SET @insertValues = @insertValues + ',@BontasiMegjegyzes'
         end 
       
         if @Tipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Tipus'
            SET @insertValues = @insertValues + ',@Tipus'
         end 
       
         if @Minosites is not null
         begin
            SET @insertColumns = @insertColumns + ',Minosites'
            SET @insertValues = @insertValues + ',@Minosites'
         end 
       
         if @MegtagadasIndoka is not null
         begin
            SET @insertColumns = @insertColumns + ',MegtagadasIndoka'
            SET @insertValues = @insertValues + ',@MegtagadasIndoka'
         end 
       
         if @Megtagado_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Megtagado_Id'
            SET @insertValues = @insertValues + ',@Megtagado_Id'
         end 
       
         if @MegtagadasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',MegtagadasDat'
            SET @insertValues = @insertValues + ',@MegtagadasDat'
         end 
       
         if @KimenoKuldemenyFajta is not null
         begin
            SET @insertColumns = @insertColumns + ',KimenoKuldemenyFajta'
            SET @insertValues = @insertValues + ',@KimenoKuldemenyFajta'
         end 
       
         if @Elsobbsegi is not null
         begin
            SET @insertColumns = @insertColumns + ',Elsobbsegi'
            SET @insertValues = @insertValues + ',@Elsobbsegi'
         end 
       
         if @Ajanlott is not null
         begin
            SET @insertColumns = @insertColumns + ',Ajanlott'
            SET @insertValues = @insertValues + ',@Ajanlott'
         end 
       
         if @Tertiveveny is not null
         begin
            SET @insertColumns = @insertColumns + ',Tertiveveny'
            SET @insertValues = @insertValues + ',@Tertiveveny'
         end 
       
         if @SajatKezbe is not null
         begin
            SET @insertColumns = @insertColumns + ',SajatKezbe'
            SET @insertValues = @insertValues + ',@SajatKezbe'
         end 
       
         if @E_ertesites is not null
         begin
            SET @insertColumns = @insertColumns + ',E_ertesites'
            SET @insertValues = @insertValues + ',@E_ertesites'
         end 
       
         if @E_elorejelzes is not null
         begin
            SET @insertColumns = @insertColumns + ',E_elorejelzes'
            SET @insertValues = @insertValues + ',@E_elorejelzes'
         end 
       
         if @PostaiLezaroSzolgalat is not null
         begin
            SET @insertColumns = @insertColumns + ',PostaiLezaroSzolgalat'
            SET @insertValues = @insertValues + ',@PostaiLezaroSzolgalat'
         end 
       
         if @Ar is not null
         begin
            SET @insertColumns = @insertColumns + ',Ar'
            SET @insertValues = @insertValues + ',@Ar'
         end 
       
         if @KimenoKuld_Sorszam  is not null
         begin
            SET @insertColumns = @insertColumns + ',KimenoKuld_Sorszam '
            SET @insertValues = @insertValues + ',@KimenoKuld_Sorszam '
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @TovabbitasAlattAllapot is not null
         begin
            SET @insertColumns = @insertColumns + ',TovabbitasAlattAllapot'
            SET @insertValues = @insertValues + ',@TovabbitasAlattAllapot'
         end 
       
         if @Azonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Azonosito'
            SET @insertValues = @insertValues + ',@Azonosito'
         end 
       
         if @BoritoTipus is not null
         begin
            SET @insertColumns = @insertColumns + ',BoritoTipus'
            SET @insertValues = @insertValues + ',@BoritoTipus'
         end 
       
         if @MegorzesJelzo is not null
         begin
            SET @insertColumns = @insertColumns + ',MegorzesJelzo'
            SET @insertValues = @insertValues + ',@MegorzesJelzo'
         end 
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end 
       
         if @IktatastNemIgenyel is not null
         begin
            SET @insertColumns = @insertColumns + ',IktatastNemIgenyel'
            SET @insertValues = @insertValues + ',@IktatastNemIgenyel'
         end 
       
         if @KezbesitesModja is not null
         begin
            SET @insertColumns = @insertColumns + ',KezbesitesModja'
            SET @insertValues = @insertValues + ',@KezbesitesModja'
         end 
       
         if @Munkaallomas is not null
         begin
            SET @insertColumns = @insertColumns + ',Munkaallomas'
            SET @insertValues = @insertValues + ',@Munkaallomas'
         end 
       
         if @SerultKuldemeny is not null
         begin
            SET @insertColumns = @insertColumns + ',SerultKuldemeny'
            SET @insertValues = @insertValues + ',@SerultKuldemeny'
         end 
       
         if @TevesCimzes is not null
         begin
            SET @insertColumns = @insertColumns + ',TevesCimzes'
            SET @insertValues = @insertValues + ',@TevesCimzes'
         end 
       
         if @TevesErkeztetes is not null
         begin
            SET @insertColumns = @insertColumns + ',TevesErkeztetes'
            SET @insertValues = @insertValues + ',@TevesErkeztetes'
         end 
       
         if @CimzesTipusa is not null
         begin
            SET @insertColumns = @insertColumns + ',CimzesTipusa'
            SET @insertValues = @insertValues + ',@CimzesTipusa'
         end 
       
         if @FutarJegyzekListaSzama is not null
         begin
            SET @insertColumns = @insertColumns + ',FutarJegyzekListaSzama'
            SET @insertValues = @insertValues + ',@FutarJegyzekListaSzama'
         end 
       
         if @Minosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Minosito'
            SET @insertValues = @insertValues + ',@Minosito'
         end 
       
         if @MinositesErvenyessegiIdeje is not null
         begin
            SET @insertColumns = @insertColumns + ',MinositesErvenyessegiIdeje'
            SET @insertValues = @insertValues + ',@MinositesErvenyessegiIdeje'
         end 
       
         if @TerjedelemMennyiseg is not null
         begin
            SET @insertColumns = @insertColumns + ',TerjedelemMennyiseg'
            SET @insertValues = @insertValues + ',@TerjedelemMennyiseg'
         end 
       
         if @TerjedelemMennyisegiEgyseg is not null
         begin
            SET @insertColumns = @insertColumns + ',TerjedelemMennyisegiEgyseg'
            SET @insertValues = @insertValues + ',@TerjedelemMennyisegiEgyseg'
         end 
       
         if @TerjedelemMegjegyzes is not null
         begin
            SET @insertColumns = @insertColumns + ',TerjedelemMegjegyzes'
            SET @insertValues = @insertValues + ',@TerjedelemMegjegyzes'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_KuldKuldemenyek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Allapot nvarchar(64),@BeerkezesIdeje datetime,@FelbontasDatuma datetime,@IraIktatokonyv_Id uniqueidentifier,@KuldesMod nvarchar(64),@Erkezteto_Szam int,@HivatkozasiSzam Nvarchar(100),@Targy Nvarchar(4000),@Tartalom Nvarchar(4000),@RagSzam Nvarchar(400),@Surgosseg nvarchar(64),@BelyegzoDatuma datetime,@UgyintezesModja nvarchar(64),@PostazasIranya nvarchar(64),@Tovabbito nvarchar(64),@PeldanySzam int,@IktatniKell nvarchar(64),@Iktathato nvarchar(64),@SztornirozasDat datetime,@KuldKuldemeny_Id_Szulo uniqueidentifier,@Erkeztetes_Ev int,@Csoport_Id_Cimzett uniqueidentifier,@Csoport_Id_Felelos uniqueidentifier,@FelhasznaloCsoport_Id_Expedial uniqueidentifier,@ExpedialasIdeje datetime,@FelhasznaloCsoport_Id_Orzo uniqueidentifier,@FelhasznaloCsoport_Id_Atvevo uniqueidentifier,@Partner_Id_Bekuldo uniqueidentifier,@Cim_Id uniqueidentifier,@CimSTR_Bekuldo Nvarchar(400),@NevSTR_Bekuldo Nvarchar(400),@AdathordozoTipusa nvarchar(64),@ElsodlegesAdathordozoTipusa nvarchar(64),@FelhasznaloCsoport_Id_Alairo uniqueidentifier,@BarCode Nvarchar(20),@FelhasznaloCsoport_Id_Bonto uniqueidentifier,@CsoportFelelosEloszto_Id uniqueidentifier,@Csoport_Id_Felelos_Elozo uniqueidentifier,@Kovetkezo_Felelos_Id uniqueidentifier,@Elektronikus_Kezbesitesi_Allap nvarchar(64),@Kovetkezo_Orzo_Id uniqueidentifier,@Fizikai_Kezbesitesi_Allapot nvarchar(64),@IraIratok_Id uniqueidentifier,@BontasiMegjegyzes Nvarchar(400),@Tipus nvarchar(64),@Minosites nvarchar(64),@MegtagadasIndoka Nvarchar(4000),@Megtagado_Id uniqueidentifier,@MegtagadasDat datetime,@KimenoKuldemenyFajta nvarchar(64),@Elsobbsegi char(1),@Ajanlott char(1),@Tertiveveny char(1),@SajatKezbe char(1),@E_ertesites char(1),@E_elorejelzes char(1),@PostaiLezaroSzolgalat char(1),@Ar Nvarchar(100),@KimenoKuld_Sorszam  int,@Ver int,@TovabbitasAlattAllapot nvarchar(64),@Azonosito Nvarchar(100),@BoritoTipus nvarchar(64),@MegorzesJelzo char(1),@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@IktatastNemIgenyel char(1),@KezbesitesModja Nvarchar(100),@Munkaallomas Nvarchar(100),@SerultKuldemeny char(1),@TevesCimzes char(1),@TevesErkeztetes char(1),@CimzesTipusa nvarchar(64),@FutarJegyzekListaSzama Nvarchar(100),@Minosito uniqueidentifier,@MinositesErvenyessegiIdeje datetime,@TerjedelemMennyiseg int,@TerjedelemMennyisegiEgyseg nvarchar(64),@TerjedelemMegjegyzes Nvarchar(400),@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Allapot = @Allapot,@BeerkezesIdeje = @BeerkezesIdeje,@FelbontasDatuma = @FelbontasDatuma,@IraIktatokonyv_Id = @IraIktatokonyv_Id,@KuldesMod = @KuldesMod,@Erkezteto_Szam = @Erkezteto_Szam,@HivatkozasiSzam = @HivatkozasiSzam,@Targy = @Targy,@Tartalom = @Tartalom,@RagSzam = @RagSzam,@Surgosseg = @Surgosseg,@BelyegzoDatuma = @BelyegzoDatuma,@UgyintezesModja = @UgyintezesModja,@PostazasIranya = @PostazasIranya,@Tovabbito = @Tovabbito,@PeldanySzam = @PeldanySzam,@IktatniKell = @IktatniKell,@Iktathato = @Iktathato,@SztornirozasDat = @SztornirozasDat,@KuldKuldemeny_Id_Szulo = @KuldKuldemeny_Id_Szulo,@Erkeztetes_Ev = @Erkeztetes_Ev,@Csoport_Id_Cimzett = @Csoport_Id_Cimzett,@Csoport_Id_Felelos = @Csoport_Id_Felelos,@FelhasznaloCsoport_Id_Expedial = @FelhasznaloCsoport_Id_Expedial,@ExpedialasIdeje = @ExpedialasIdeje,@FelhasznaloCsoport_Id_Orzo = @FelhasznaloCsoport_Id_Orzo,@FelhasznaloCsoport_Id_Atvevo = @FelhasznaloCsoport_Id_Atvevo,@Partner_Id_Bekuldo = @Partner_Id_Bekuldo,@Cim_Id = @Cim_Id,@CimSTR_Bekuldo = @CimSTR_Bekuldo,@NevSTR_Bekuldo = @NevSTR_Bekuldo,@AdathordozoTipusa = @AdathordozoTipusa,@ElsodlegesAdathordozoTipusa = @ElsodlegesAdathordozoTipusa,@FelhasznaloCsoport_Id_Alairo = @FelhasznaloCsoport_Id_Alairo,@BarCode = @BarCode,@FelhasznaloCsoport_Id_Bonto = @FelhasznaloCsoport_Id_Bonto,@CsoportFelelosEloszto_Id = @CsoportFelelosEloszto_Id,@Csoport_Id_Felelos_Elozo = @Csoport_Id_Felelos_Elozo,@Kovetkezo_Felelos_Id = @Kovetkezo_Felelos_Id,@Elektronikus_Kezbesitesi_Allap = @Elektronikus_Kezbesitesi_Allap,@Kovetkezo_Orzo_Id = @Kovetkezo_Orzo_Id,@Fizikai_Kezbesitesi_Allapot = @Fizikai_Kezbesitesi_Allapot,@IraIratok_Id = @IraIratok_Id,@BontasiMegjegyzes = @BontasiMegjegyzes,@Tipus = @Tipus,@Minosites = @Minosites,@MegtagadasIndoka = @MegtagadasIndoka,@Megtagado_Id = @Megtagado_Id,@MegtagadasDat = @MegtagadasDat,@KimenoKuldemenyFajta = @KimenoKuldemenyFajta,@Elsobbsegi = @Elsobbsegi,@Ajanlott = @Ajanlott,@Tertiveveny = @Tertiveveny,@SajatKezbe = @SajatKezbe,@E_ertesites = @E_ertesites,@E_elorejelzes = @E_elorejelzes,@PostaiLezaroSzolgalat = @PostaiLezaroSzolgalat,@Ar = @Ar,@KimenoKuld_Sorszam  = @KimenoKuld_Sorszam ,@Ver = @Ver,@TovabbitasAlattAllapot = @TovabbitasAlattAllapot,@Azonosito = @Azonosito,@BoritoTipus = @BoritoTipus,@MegorzesJelzo = @MegorzesJelzo,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id,@IktatastNemIgenyel = @IktatastNemIgenyel,@KezbesitesModja = @KezbesitesModja,@Munkaallomas = @Munkaallomas,@SerultKuldemeny = @SerultKuldemeny,@TevesCimzes = @TevesCimzes,@TevesErkeztetes = @TevesErkeztetes,@CimzesTipusa = @CimzesTipusa,@FutarJegyzekListaSzama = @FutarJegyzekListaSzama,@Minosito = @Minosito,@MinositesErvenyessegiIdeje = @MinositesErvenyessegiIdeje,@TerjedelemMennyiseg = @TerjedelemMennyiseg,@TerjedelemMennyisegiEgyseg = @TerjedelemMennyisegiEgyseg,@TerjedelemMegjegyzes = @TerjedelemMegjegyzes ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_KuldKuldemenyek',@ResultUid
					,'EREC_KuldKuldemenyekHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
go

/* használt errorcode-ok:
	50301:
	50302: 'Org' oszlop értéke nem lehet NULL
*/
