

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_KuldKuldemenyekUpdate')
            and   type = 'P')
   drop procedure sp_EREC_KuldKuldemenyekUpdate
go

create procedure sp_EREC_KuldKuldemenyekUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Allapot     nvarchar(64)  = null,         
             @BeerkezesIdeje     datetime  = null,         
             @FelbontasDatuma     datetime  = null,         
             @IraIktatokonyv_Id     uniqueidentifier  = null,         
             @KuldesMod     nvarchar(64)  = null,         
             @Erkezteto_Szam     int  = null,         
             @HivatkozasiSzam     Nvarchar(100)  = null,         
             @Targy     Nvarchar(4000)  = null,         
             @Tartalom     Nvarchar(4000)  = null,         
             @RagSzam     Nvarchar(400)  = null,         
             @Surgosseg     nvarchar(64)  = null,         
             @BelyegzoDatuma     datetime  = null,         
             @UgyintezesModja     nvarchar(64)  = null,         
             @PostazasIranya     nvarchar(64)  = null,         
             @Tovabbito     nvarchar(64)  = null,         
             @PeldanySzam     int  = null,         
             @IktatniKell     nvarchar(64)  = null,         
             @Iktathato     nvarchar(64)  = null,         
             @SztornirozasDat     datetime  = null,         
             @KuldKuldemeny_Id_Szulo     uniqueidentifier  = null,         
             @Erkeztetes_Ev     int  = null,         
             @Csoport_Id_Cimzett     uniqueidentifier  = null,         
             @Csoport_Id_Felelos     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Expedial     uniqueidentifier  = null,         
             @ExpedialasIdeje     datetime  = null,         
             @FelhasznaloCsoport_Id_Orzo     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Atvevo     uniqueidentifier  = null,         
             @Partner_Id_Bekuldo     uniqueidentifier  = null,         
             @Cim_Id     uniqueidentifier  = null,         
             @CimSTR_Bekuldo     Nvarchar(400)  = null,         
             @NevSTR_Bekuldo     Nvarchar(400)  = null,         
             @AdathordozoTipusa     nvarchar(64)  = null,         
             @ElsodlegesAdathordozoTipusa     nvarchar(64)  = null,         
             @FelhasznaloCsoport_Id_Alairo     uniqueidentifier  = null,         
             @BarCode     Nvarchar(20)  = null,         
             @FelhasznaloCsoport_Id_Bonto     uniqueidentifier  = null,         
             @CsoportFelelosEloszto_Id     uniqueidentifier  = null,         
             @Csoport_Id_Felelos_Elozo     uniqueidentifier  = null,         
             @Kovetkezo_Felelos_Id     uniqueidentifier  = null,         
             @Elektronikus_Kezbesitesi_Allap     nvarchar(64)  = null,         
             @Kovetkezo_Orzo_Id     uniqueidentifier  = null,         
             @Fizikai_Kezbesitesi_Allapot     nvarchar(64)  = null,         
             @IraIratok_Id     uniqueidentifier  = null,         
             @BontasiMegjegyzes     Nvarchar(400)  = null,         
             @Tipus     nvarchar(64)  = null,         
             @Minosites     nvarchar(64)  = null,         
             @MegtagadasIndoka     Nvarchar(4000)  = null,         
             @Megtagado_Id     uniqueidentifier  = null,         
             @MegtagadasDat     datetime  = null,         
             @KimenoKuldemenyFajta     nvarchar(64)  = null,         
             @Elsobbsegi     char(1)  = null,         
             @Ajanlott     char(1)  = null,         
             @Tertiveveny     char(1)  = null,         
             @SajatKezbe     char(1)  = null,         
             @E_ertesites     char(1)  = null,         
             @E_elorejelzes     char(1)  = null,         
             @PostaiLezaroSzolgalat     char(1)  = null,         
             @Ar     Nvarchar(100)  = null,         
             @KimenoKuld_Sorszam      int  = null,         
             @Ver     int  = null,         
             @TovabbitasAlattAllapot     nvarchar(64)  = null,         
             @Azonosito     Nvarchar(100)  = null,         
             @BoritoTipus     nvarchar(64)  = null,         
             @MegorzesJelzo     char(1)  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
             @IktatastNemIgenyel     char(1)  = null,         
             @KezbesitesModja     Nvarchar(100)  = null,         
             @Munkaallomas     Nvarchar(100)  = null,         
             @SerultKuldemeny     char(1)  = null,         
             @TevesCimzes     char(1)  = null,         
             @TevesErkeztetes     char(1)  = null,         
             @CimzesTipusa     nvarchar(64)  = null,         
             @FutarJegyzekListaSzama     Nvarchar(100)  = null,         
             @Minosito     uniqueidentifier  = null,         
             @MinositesErvenyessegiIdeje     datetime  = null,         
             @TerjedelemMennyiseg     int  = null,         
             @TerjedelemMennyisegiEgyseg     nvarchar(64)  = null,         
             @TerjedelemMegjegyzes     Nvarchar(400)  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_BeerkezesIdeje     datetime         
     DECLARE @Act_FelbontasDatuma     datetime         
     DECLARE @Act_IraIktatokonyv_Id     uniqueidentifier         
     DECLARE @Act_KuldesMod     nvarchar(64)         
     DECLARE @Act_Erkezteto_Szam     int         
     DECLARE @Act_HivatkozasiSzam     Nvarchar(100)         
     DECLARE @Act_Targy     Nvarchar(4000)         
     DECLARE @Act_Tartalom     Nvarchar(4000)         
     DECLARE @Act_RagSzam     Nvarchar(400)         
     DECLARE @Act_Surgosseg     nvarchar(64)         
     DECLARE @Act_BelyegzoDatuma     datetime         
     DECLARE @Act_UgyintezesModja     nvarchar(64)         
     DECLARE @Act_PostazasIranya     nvarchar(64)         
     DECLARE @Act_Tovabbito     nvarchar(64)         
     DECLARE @Act_PeldanySzam     int         
     DECLARE @Act_IktatniKell     nvarchar(64)         
     DECLARE @Act_Iktathato     nvarchar(64)         
     DECLARE @Act_SztornirozasDat     datetime         
     DECLARE @Act_KuldKuldemeny_Id_Szulo     uniqueidentifier         
     DECLARE @Act_Erkeztetes_Ev     int         
     DECLARE @Act_Csoport_Id_Cimzett     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Felelos     uniqueidentifier         
     DECLARE @Act_FelhasznaloCsoport_Id_Expedial     uniqueidentifier         
     DECLARE @Act_ExpedialasIdeje     datetime         
     DECLARE @Act_FelhasznaloCsoport_Id_Orzo     uniqueidentifier         
     DECLARE @Act_FelhasznaloCsoport_Id_Atvevo     uniqueidentifier         
     DECLARE @Act_Partner_Id_Bekuldo     uniqueidentifier         
     DECLARE @Act_Cim_Id     uniqueidentifier         
     DECLARE @Act_CimSTR_Bekuldo     Nvarchar(400)         
     DECLARE @Act_NevSTR_Bekuldo     Nvarchar(400)         
     DECLARE @Act_AdathordozoTipusa     nvarchar(64)         
     DECLARE @Act_ElsodlegesAdathordozoTipusa     nvarchar(64)         
     DECLARE @Act_FelhasznaloCsoport_Id_Alairo     uniqueidentifier         
     DECLARE @Act_BarCode     Nvarchar(20)         
     DECLARE @Act_FelhasznaloCsoport_Id_Bonto     uniqueidentifier         
     DECLARE @Act_CsoportFelelosEloszto_Id     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Felelos_Elozo     uniqueidentifier         
     DECLARE @Act_Kovetkezo_Felelos_Id     uniqueidentifier         
     DECLARE @Act_Elektronikus_Kezbesitesi_Allap     nvarchar(64)         
     DECLARE @Act_Kovetkezo_Orzo_Id     uniqueidentifier         
     DECLARE @Act_Fizikai_Kezbesitesi_Allapot     nvarchar(64)         
     DECLARE @Act_IraIratok_Id     uniqueidentifier         
     DECLARE @Act_BontasiMegjegyzes     Nvarchar(400)         
     DECLARE @Act_Tipus     nvarchar(64)         
     DECLARE @Act_Minosites     nvarchar(64)         
     DECLARE @Act_MegtagadasIndoka     Nvarchar(4000)         
     DECLARE @Act_Megtagado_Id     uniqueidentifier         
     DECLARE @Act_MegtagadasDat     datetime         
     DECLARE @Act_KimenoKuldemenyFajta     nvarchar(64)         
     DECLARE @Act_Elsobbsegi     char(1)         
     DECLARE @Act_Ajanlott     char(1)         
     DECLARE @Act_Tertiveveny     char(1)         
     DECLARE @Act_SajatKezbe     char(1)         
     DECLARE @Act_E_ertesites     char(1)         
     DECLARE @Act_E_elorejelzes     char(1)         
     DECLARE @Act_PostaiLezaroSzolgalat     char(1)         
     DECLARE @Act_Ar     Nvarchar(100)         
     DECLARE @Act_KimenoKuld_Sorszam      int         
     DECLARE @Act_Ver     int         
     DECLARE @Act_TovabbitasAlattAllapot     nvarchar(64)         
     DECLARE @Act_Azonosito     Nvarchar(100)         
     DECLARE @Act_BoritoTipus     nvarchar(64)         
     DECLARE @Act_MegorzesJelzo     char(1)         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier         
     DECLARE @Act_IktatastNemIgenyel     char(1)         
     DECLARE @Act_KezbesitesModja     Nvarchar(100)         
     DECLARE @Act_Munkaallomas     Nvarchar(100)         
     DECLARE @Act_SerultKuldemeny     char(1)         
     DECLARE @Act_TevesCimzes     char(1)         
     DECLARE @Act_TevesErkeztetes     char(1)         
     DECLARE @Act_CimzesTipusa     nvarchar(64)         
     DECLARE @Act_FutarJegyzekListaSzama     Nvarchar(100)         
     DECLARE @Act_Minosito     uniqueidentifier         
     DECLARE @Act_MinositesErvenyessegiIdeje     datetime         
     DECLARE @Act_TerjedelemMennyiseg     int         
     DECLARE @Act_TerjedelemMennyisegiEgyseg     nvarchar(64)         
     DECLARE @Act_TerjedelemMegjegyzes     Nvarchar(400)           
  
set nocount on

select 
     @Act_Allapot = Allapot,
     @Act_BeerkezesIdeje = BeerkezesIdeje,
     @Act_FelbontasDatuma = FelbontasDatuma,
     @Act_IraIktatokonyv_Id = IraIktatokonyv_Id,
     @Act_KuldesMod = KuldesMod,
     @Act_Erkezteto_Szam = Erkezteto_Szam,
     @Act_HivatkozasiSzam = HivatkozasiSzam,
     @Act_Targy = Targy,
     @Act_Tartalom = Tartalom,
     @Act_RagSzam = RagSzam,
     @Act_Surgosseg = Surgosseg,
     @Act_BelyegzoDatuma = BelyegzoDatuma,
     @Act_UgyintezesModja = UgyintezesModja,
     @Act_PostazasIranya = PostazasIranya,
     @Act_Tovabbito = Tovabbito,
     @Act_PeldanySzam = PeldanySzam,
     @Act_IktatniKell = IktatniKell,
     @Act_Iktathato = Iktathato,
     @Act_SztornirozasDat = SztornirozasDat,
     @Act_KuldKuldemeny_Id_Szulo = KuldKuldemeny_Id_Szulo,
     @Act_Erkeztetes_Ev = Erkeztetes_Ev,
     @Act_Csoport_Id_Cimzett = Csoport_Id_Cimzett,
     @Act_Csoport_Id_Felelos = Csoport_Id_Felelos,
     @Act_FelhasznaloCsoport_Id_Expedial = FelhasznaloCsoport_Id_Expedial,
     @Act_ExpedialasIdeje = ExpedialasIdeje,
     @Act_FelhasznaloCsoport_Id_Orzo = FelhasznaloCsoport_Id_Orzo,
     @Act_FelhasznaloCsoport_Id_Atvevo = FelhasznaloCsoport_Id_Atvevo,
     @Act_Partner_Id_Bekuldo = Partner_Id_Bekuldo,
     @Act_Cim_Id = Cim_Id,
     @Act_CimSTR_Bekuldo = CimSTR_Bekuldo,
     @Act_NevSTR_Bekuldo = NevSTR_Bekuldo,
     @Act_AdathordozoTipusa = AdathordozoTipusa,
     @Act_ElsodlegesAdathordozoTipusa = ElsodlegesAdathordozoTipusa,
     @Act_FelhasznaloCsoport_Id_Alairo = FelhasznaloCsoport_Id_Alairo,
     @Act_BarCode = BarCode,
     @Act_FelhasznaloCsoport_Id_Bonto = FelhasznaloCsoport_Id_Bonto,
     @Act_CsoportFelelosEloszto_Id = CsoportFelelosEloszto_Id,
     @Act_Csoport_Id_Felelos_Elozo = Csoport_Id_Felelos_Elozo,
     @Act_Kovetkezo_Felelos_Id = Kovetkezo_Felelos_Id,
     @Act_Elektronikus_Kezbesitesi_Allap = Elektronikus_Kezbesitesi_Allap,
     @Act_Kovetkezo_Orzo_Id = Kovetkezo_Orzo_Id,
     @Act_Fizikai_Kezbesitesi_Allapot = Fizikai_Kezbesitesi_Allapot,
     @Act_IraIratok_Id = IraIratok_Id,
     @Act_BontasiMegjegyzes = BontasiMegjegyzes,
     @Act_Tipus = Tipus,
     @Act_Minosites = Minosites,
     @Act_MegtagadasIndoka = MegtagadasIndoka,
     @Act_Megtagado_Id = Megtagado_Id,
     @Act_MegtagadasDat = MegtagadasDat,
     @Act_KimenoKuldemenyFajta = KimenoKuldemenyFajta,
     @Act_Elsobbsegi = Elsobbsegi,
     @Act_Ajanlott = Ajanlott,
     @Act_Tertiveveny = Tertiveveny,
     @Act_SajatKezbe = SajatKezbe,
     @Act_E_ertesites = E_ertesites,
     @Act_E_elorejelzes = E_elorejelzes,
     @Act_PostaiLezaroSzolgalat = PostaiLezaroSzolgalat,
     @Act_Ar = Ar,
     @Act_KimenoKuld_Sorszam  = KimenoKuld_Sorszam ,
     @Act_Ver = Ver,
     @Act_TovabbitasAlattAllapot = TovabbitasAlattAllapot,
     @Act_Azonosito = Azonosito,
     @Act_BoritoTipus = BoritoTipus,
     @Act_MegorzesJelzo = MegorzesJelzo,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id,
     @Act_IktatastNemIgenyel = IktatastNemIgenyel,
     @Act_KezbesitesModja = KezbesitesModja,
     @Act_Munkaallomas = Munkaallomas,
     @Act_SerultKuldemeny = SerultKuldemeny,
     @Act_TevesCimzes = TevesCimzes,
     @Act_TevesErkeztetes = TevesErkeztetes,
     @Act_CimzesTipusa = CimzesTipusa,
     @Act_FutarJegyzekListaSzama = FutarJegyzekListaSzama,
     @Act_Minosito = Minosito,
     @Act_MinositesErvenyessegiIdeje = MinositesErvenyessegiIdeje,
     @Act_TerjedelemMennyiseg = TerjedelemMennyiseg,
     @Act_TerjedelemMennyisegiEgyseg = TerjedelemMennyisegiEgyseg,
     @Act_TerjedelemMegjegyzes = TerjedelemMegjegyzes
from EREC_KuldKuldemenyek
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zárolás ellenőrzés:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/BeerkezesIdeje')=1
         SET @Act_BeerkezesIdeje = @BeerkezesIdeje
   IF @UpdatedColumns.exist('/root/FelbontasDatuma')=1
         SET @Act_FelbontasDatuma = @FelbontasDatuma
   IF @UpdatedColumns.exist('/root/IraIktatokonyv_Id')=1
         SET @Act_IraIktatokonyv_Id = @IraIktatokonyv_Id
   IF @UpdatedColumns.exist('/root/KuldesMod')=1
         SET @Act_KuldesMod = @KuldesMod
   IF @UpdatedColumns.exist('/root/Erkezteto_Szam')=1
         SET @Act_Erkezteto_Szam = @Erkezteto_Szam
   IF @UpdatedColumns.exist('/root/HivatkozasiSzam')=1
         SET @Act_HivatkozasiSzam = @HivatkozasiSzam
   IF @UpdatedColumns.exist('/root/Targy')=1
         SET @Act_Targy = @Targy
   IF @UpdatedColumns.exist('/root/Tartalom')=1
         SET @Act_Tartalom = @Tartalom
   IF @UpdatedColumns.exist('/root/RagSzam')=1
         SET @Act_RagSzam = @RagSzam
   IF @UpdatedColumns.exist('/root/Surgosseg')=1
         SET @Act_Surgosseg = @Surgosseg
   IF @UpdatedColumns.exist('/root/BelyegzoDatuma')=1
         SET @Act_BelyegzoDatuma = @BelyegzoDatuma
   IF @UpdatedColumns.exist('/root/UgyintezesModja')=1
         SET @Act_UgyintezesModja = @UgyintezesModja
   IF @UpdatedColumns.exist('/root/PostazasIranya')=1
         SET @Act_PostazasIranya = @PostazasIranya
   IF @UpdatedColumns.exist('/root/Tovabbito')=1
         SET @Act_Tovabbito = @Tovabbito
   IF @UpdatedColumns.exist('/root/PeldanySzam')=1
         SET @Act_PeldanySzam = @PeldanySzam
   IF @UpdatedColumns.exist('/root/IktatniKell')=1
         SET @Act_IktatniKell = @IktatniKell
   IF @UpdatedColumns.exist('/root/Iktathato')=1
         SET @Act_Iktathato = @Iktathato
   IF @UpdatedColumns.exist('/root/SztornirozasDat')=1
         SET @Act_SztornirozasDat = @SztornirozasDat
   IF @UpdatedColumns.exist('/root/KuldKuldemeny_Id_Szulo')=1
         SET @Act_KuldKuldemeny_Id_Szulo = @KuldKuldemeny_Id_Szulo
   IF @UpdatedColumns.exist('/root/Erkeztetes_Ev')=1
         SET @Act_Erkeztetes_Ev = @Erkeztetes_Ev
   IF @UpdatedColumns.exist('/root/Csoport_Id_Cimzett')=1
         SET @Act_Csoport_Id_Cimzett = @Csoport_Id_Cimzett
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1
         SET @Act_Csoport_Id_Felelos = @Csoport_Id_Felelos
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Expedial')=1
         SET @Act_FelhasznaloCsoport_Id_Expedial = @FelhasznaloCsoport_Id_Expedial
   IF @UpdatedColumns.exist('/root/ExpedialasIdeje')=1
         SET @Act_ExpedialasIdeje = @ExpedialasIdeje
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Orzo')=1
         SET @Act_FelhasznaloCsoport_Id_Orzo = @FelhasznaloCsoport_Id_Orzo
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Atvevo')=1
         SET @Act_FelhasznaloCsoport_Id_Atvevo = @FelhasznaloCsoport_Id_Atvevo
   IF @UpdatedColumns.exist('/root/Partner_Id_Bekuldo')=1
         SET @Act_Partner_Id_Bekuldo = @Partner_Id_Bekuldo
   IF @UpdatedColumns.exist('/root/Cim_Id')=1
         SET @Act_Cim_Id = @Cim_Id
   IF @UpdatedColumns.exist('/root/CimSTR_Bekuldo')=1
         SET @Act_CimSTR_Bekuldo = @CimSTR_Bekuldo
   IF @UpdatedColumns.exist('/root/NevSTR_Bekuldo')=1
         SET @Act_NevSTR_Bekuldo = @NevSTR_Bekuldo
   IF @UpdatedColumns.exist('/root/AdathordozoTipusa')=1
         SET @Act_AdathordozoTipusa = @AdathordozoTipusa
   IF @UpdatedColumns.exist('/root/ElsodlegesAdathordozoTipusa')=1
         SET @Act_ElsodlegesAdathordozoTipusa = @ElsodlegesAdathordozoTipusa
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Alairo')=1
         SET @Act_FelhasznaloCsoport_Id_Alairo = @FelhasznaloCsoport_Id_Alairo
   IF @UpdatedColumns.exist('/root/BarCode')=1
         SET @Act_BarCode = @BarCode
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Bonto')=1
         SET @Act_FelhasznaloCsoport_Id_Bonto = @FelhasznaloCsoport_Id_Bonto
   IF @UpdatedColumns.exist('/root/CsoportFelelosEloszto_Id')=1
         SET @Act_CsoportFelelosEloszto_Id = @CsoportFelelosEloszto_Id
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos_Elozo')=1
         SET @Act_Csoport_Id_Felelos_Elozo = @Csoport_Id_Felelos_Elozo
   IF @UpdatedColumns.exist('/root/Kovetkezo_Felelos_Id')=1
         SET @Act_Kovetkezo_Felelos_Id = @Kovetkezo_Felelos_Id
   IF @UpdatedColumns.exist('/root/Elektronikus_Kezbesitesi_Allap')=1
         SET @Act_Elektronikus_Kezbesitesi_Allap = @Elektronikus_Kezbesitesi_Allap
   IF @UpdatedColumns.exist('/root/Kovetkezo_Orzo_Id')=1
         SET @Act_Kovetkezo_Orzo_Id = @Kovetkezo_Orzo_Id
   IF @UpdatedColumns.exist('/root/Fizikai_Kezbesitesi_Allapot')=1
         SET @Act_Fizikai_Kezbesitesi_Allapot = @Fizikai_Kezbesitesi_Allapot
   IF @UpdatedColumns.exist('/root/IraIratok_Id')=1
         SET @Act_IraIratok_Id = @IraIratok_Id
   IF @UpdatedColumns.exist('/root/BontasiMegjegyzes')=1
         SET @Act_BontasiMegjegyzes = @BontasiMegjegyzes
   IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @Act_Tipus = @Tipus
   IF @UpdatedColumns.exist('/root/Minosites')=1
         SET @Act_Minosites = @Minosites
   IF @UpdatedColumns.exist('/root/MegtagadasIndoka')=1
         SET @Act_MegtagadasIndoka = @MegtagadasIndoka
   IF @UpdatedColumns.exist('/root/Megtagado_Id')=1
         SET @Act_Megtagado_Id = @Megtagado_Id
   IF @UpdatedColumns.exist('/root/MegtagadasDat')=1
         SET @Act_MegtagadasDat = @MegtagadasDat
   IF @UpdatedColumns.exist('/root/KimenoKuldemenyFajta')=1
         SET @Act_KimenoKuldemenyFajta = @KimenoKuldemenyFajta
   IF @UpdatedColumns.exist('/root/Elsobbsegi')=1
         SET @Act_Elsobbsegi = @Elsobbsegi
   IF @UpdatedColumns.exist('/root/Ajanlott')=1
         SET @Act_Ajanlott = @Ajanlott
   IF @UpdatedColumns.exist('/root/Tertiveveny')=1
         SET @Act_Tertiveveny = @Tertiveveny
   IF @UpdatedColumns.exist('/root/SajatKezbe')=1
         SET @Act_SajatKezbe = @SajatKezbe
   IF @UpdatedColumns.exist('/root/E_ertesites')=1
         SET @Act_E_ertesites = @E_ertesites
   IF @UpdatedColumns.exist('/root/E_elorejelzes')=1
         SET @Act_E_elorejelzes = @E_elorejelzes
   IF @UpdatedColumns.exist('/root/PostaiLezaroSzolgalat')=1
         SET @Act_PostaiLezaroSzolgalat = @PostaiLezaroSzolgalat
   IF @UpdatedColumns.exist('/root/Ar')=1
         SET @Act_Ar = @Ar
   IF @UpdatedColumns.exist('/root/KimenoKuld_Sorszam ')=1
         SET @Act_KimenoKuld_Sorszam  = @KimenoKuld_Sorszam 
   IF @UpdatedColumns.exist('/root/TovabbitasAlattAllapot')=1
         SET @Act_TovabbitasAlattAllapot = @TovabbitasAlattAllapot
   IF @UpdatedColumns.exist('/root/Azonosito')=1
         SET @Act_Azonosito = @Azonosito
   IF @UpdatedColumns.exist('/root/BoritoTipus')=1
         SET @Act_BoritoTipus = @BoritoTipus
   IF @UpdatedColumns.exist('/root/MegorzesJelzo')=1
         SET @Act_MegorzesJelzo = @MegorzesJelzo
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id
   IF @UpdatedColumns.exist('/root/IktatastNemIgenyel')=1
         SET @Act_IktatastNemIgenyel = @IktatastNemIgenyel
   IF @UpdatedColumns.exist('/root/KezbesitesModja')=1
         SET @Act_KezbesitesModja = @KezbesitesModja
   IF @UpdatedColumns.exist('/root/Munkaallomas')=1
         SET @Act_Munkaallomas = @Munkaallomas
   IF @UpdatedColumns.exist('/root/SerultKuldemeny')=1
         SET @Act_SerultKuldemeny = @SerultKuldemeny
   IF @UpdatedColumns.exist('/root/TevesCimzes')=1
         SET @Act_TevesCimzes = @TevesCimzes
   IF @UpdatedColumns.exist('/root/TevesErkeztetes')=1
         SET @Act_TevesErkeztetes = @TevesErkeztetes
   IF @UpdatedColumns.exist('/root/CimzesTipusa')=1
         SET @Act_CimzesTipusa = @CimzesTipusa
   IF @UpdatedColumns.exist('/root/FutarJegyzekListaSzama')=1
         SET @Act_FutarJegyzekListaSzama = @FutarJegyzekListaSzama
   IF @UpdatedColumns.exist('/root/Minosito')=1
         SET @Act_Minosito = @Minosito
   IF @UpdatedColumns.exist('/root/MinositesErvenyessegiIdeje')=1
         SET @Act_MinositesErvenyessegiIdeje = @MinositesErvenyessegiIdeje
   IF @UpdatedColumns.exist('/root/TerjedelemMennyiseg')=1
         SET @Act_TerjedelemMennyiseg = @TerjedelemMennyiseg
   IF @UpdatedColumns.exist('/root/TerjedelemMennyisegiEgyseg')=1
         SET @Act_TerjedelemMennyisegiEgyseg = @TerjedelemMennyisegiEgyseg
   IF @UpdatedColumns.exist('/root/TerjedelemMegjegyzes')=1
         SET @Act_TerjedelemMegjegyzes = @TerjedelemMegjegyzes   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_KuldKuldemenyek
SET
     Allapot = @Act_Allapot,
     BeerkezesIdeje = @Act_BeerkezesIdeje,
     FelbontasDatuma = @Act_FelbontasDatuma,
     IraIktatokonyv_Id = @Act_IraIktatokonyv_Id,
     KuldesMod = @Act_KuldesMod,
     Erkezteto_Szam = @Act_Erkezteto_Szam,
     HivatkozasiSzam = @Act_HivatkozasiSzam,
     Targy = @Act_Targy,
     Tartalom = @Act_Tartalom,
     RagSzam = @Act_RagSzam,
     Surgosseg = @Act_Surgosseg,
     BelyegzoDatuma = @Act_BelyegzoDatuma,
     UgyintezesModja = @Act_UgyintezesModja,
     PostazasIranya = @Act_PostazasIranya,
     Tovabbito = @Act_Tovabbito,
     PeldanySzam = @Act_PeldanySzam,
     IktatniKell = @Act_IktatniKell,
     Iktathato = @Act_Iktathato,
     SztornirozasDat = @Act_SztornirozasDat,
     KuldKuldemeny_Id_Szulo = @Act_KuldKuldemeny_Id_Szulo,
     Erkeztetes_Ev = @Act_Erkeztetes_Ev,
     Csoport_Id_Cimzett = @Act_Csoport_Id_Cimzett,
     Csoport_Id_Felelos = @Act_Csoport_Id_Felelos,
     FelhasznaloCsoport_Id_Expedial = @Act_FelhasznaloCsoport_Id_Expedial,
     ExpedialasIdeje = @Act_ExpedialasIdeje,
     FelhasznaloCsoport_Id_Orzo = @Act_FelhasznaloCsoport_Id_Orzo,
     FelhasznaloCsoport_Id_Atvevo = @Act_FelhasznaloCsoport_Id_Atvevo,
     Partner_Id_Bekuldo = @Act_Partner_Id_Bekuldo,
     Cim_Id = @Act_Cim_Id,
     CimSTR_Bekuldo = @Act_CimSTR_Bekuldo,
     NevSTR_Bekuldo = @Act_NevSTR_Bekuldo,
     AdathordozoTipusa = @Act_AdathordozoTipusa,
     ElsodlegesAdathordozoTipusa = @Act_ElsodlegesAdathordozoTipusa,
     FelhasznaloCsoport_Id_Alairo = @Act_FelhasznaloCsoport_Id_Alairo,
     BarCode = @Act_BarCode,
     FelhasznaloCsoport_Id_Bonto = @Act_FelhasznaloCsoport_Id_Bonto,
     CsoportFelelosEloszto_Id = @Act_CsoportFelelosEloszto_Id,
     Csoport_Id_Felelos_Elozo = @Act_Csoport_Id_Felelos_Elozo,
     Kovetkezo_Felelos_Id = @Act_Kovetkezo_Felelos_Id,
     Elektronikus_Kezbesitesi_Allap = @Act_Elektronikus_Kezbesitesi_Allap,
     Kovetkezo_Orzo_Id = @Act_Kovetkezo_Orzo_Id,
     Fizikai_Kezbesitesi_Allapot = @Act_Fizikai_Kezbesitesi_Allapot,
     IraIratok_Id = @Act_IraIratok_Id,
     BontasiMegjegyzes = @Act_BontasiMegjegyzes,
     Tipus = @Act_Tipus,
     Minosites = @Act_Minosites,
     MegtagadasIndoka = @Act_MegtagadasIndoka,
     Megtagado_Id = @Act_Megtagado_Id,
     MegtagadasDat = @Act_MegtagadasDat,
     KimenoKuldemenyFajta = @Act_KimenoKuldemenyFajta,
     Elsobbsegi = @Act_Elsobbsegi,
     Ajanlott = @Act_Ajanlott,
     Tertiveveny = @Act_Tertiveveny,
     SajatKezbe = @Act_SajatKezbe,
     E_ertesites = @Act_E_ertesites,
     E_elorejelzes = @Act_E_elorejelzes,
     PostaiLezaroSzolgalat = @Act_PostaiLezaroSzolgalat,
     Ar = @Act_Ar,
     KimenoKuld_Sorszam  = @Act_KimenoKuld_Sorszam ,
     Ver = @Act_Ver,
     TovabbitasAlattAllapot = @Act_TovabbitasAlattAllapot,
     Azonosito = @Act_Azonosito,
     BoritoTipus = @Act_BoritoTipus,
     MegorzesJelzo = @Act_MegorzesJelzo,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id,
     IktatastNemIgenyel = @Act_IktatastNemIgenyel,
     KezbesitesModja = @Act_KezbesitesModja,
     Munkaallomas = @Act_Munkaallomas,
     SerultKuldemeny = @Act_SerultKuldemeny,
     TevesCimzes = @Act_TevesCimzes,
     TevesErkeztetes = @Act_TevesErkeztetes,
     CimzesTipusa = @Act_CimzesTipusa,
     FutarJegyzekListaSzama = @Act_FutarJegyzekListaSzama,
     Minosito = @Act_Minosito,
     MinositesErvenyessegiIdeje = @Act_MinositesErvenyessegiIdeje,
     TerjedelemMennyiseg = @Act_TerjedelemMennyiseg,
     TerjedelemMennyisegiEgyseg = @Act_TerjedelemMennyisegiEgyseg,
     TerjedelemMegjegyzes = @Act_TerjedelemMegjegyzes
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_KuldKuldemenyek',@Id
					,'EREC_KuldKuldemenyekHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

go

/* használt errorcode-ok:
	50401:
	50402: 'Ver' oszlop értéke nem egyezik a meglévő rekordéval
   50403: érvényesség már lejárt a rekordra
	50499: rekord zárolva
*/
