IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKuldemenyekUpdateTomeges]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KuldKuldemenyekUpdateTomeges]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldKuldemenyekUpdateTomeges]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KuldKuldemenyekUpdateTomeges] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_KuldKuldemenyekUpdateTomeges]
        @Ids                  NVARCHAR(MAX),
        @Vers                 NVARCHAR(MAX),
        @RagSzam_List     NVARCHAR(MAX) = NULL,
        @BarCode_List     NVARCHAR(MAX) = NULL,    
      @ExecutorUserId            uniqueidentifier,
      @ExecutionTime             datetime,
             @Allapot     nvarchar(64)  = null,         
             @BeerkezesIdeje     datetime  = null,         
             @FelbontasDatuma     datetime  = null,         
             @IraIktatokonyv_Id     uniqueidentifier  = null,         
             @KuldesMod     nvarchar(64)  = null,         
--             @Erkezteto_Szam     int  = null,         
             @HivatkozasiSzam     Nvarchar(100)  = null,         
             @Targy     Nvarchar(4000)  = null,         
             @Tartalom     Nvarchar(4000)  = null,         
             @RagSzam     Nvarchar(400)  = null,         
             @Surgosseg     nvarchar(64)  = null,         
             @BelyegzoDatuma     datetime  = null,         
             @UgyintezesModja     nvarchar(64)  = null,         
             @PostazasIranya     nvarchar(64)  = null,         
             @Tovabbito     nvarchar(64)  = null,         
             @PeldanySzam     int  = null,         
             @IktatniKell     nvarchar(64)  = null,         
             @Iktathato     nvarchar(64)  = null,         
             @SztornirozasDat     datetime  = null,         
             @KuldKuldemeny_Id_Szulo     uniqueidentifier  = null,         
             @Erkeztetes_Ev     int  = null,         
             @Csoport_Id_Cimzett     uniqueidentifier  = null,         
             @Csoport_Id_Felelos     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Expedial     uniqueidentifier  = null,         
             @ExpedialasIdeje     datetime  = null,         
             @FelhasznaloCsoport_Id_Orzo     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Atvevo     uniqueidentifier  = null,         
             @Partner_Id_Bekuldo     uniqueidentifier  = null,         
             @Cim_Id     uniqueidentifier  = null,         
             @CimSTR_Bekuldo     Nvarchar(400)  = null,         
             @NevSTR_Bekuldo     Nvarchar(400)  = null,         
             @AdathordozoTipusa     nvarchar(64)  = null,         
             @ElsodlegesAdathordozoTipusa     nvarchar(64)  = null,         
             @FelhasznaloCsoport_Id_Alairo     uniqueidentifier  = null,         
             @BarCode     Nvarchar(100)  = null,         
             @FelhasznaloCsoport_Id_Bonto     uniqueidentifier  = null,         
             @CsoportFelelosEloszto_Id     uniqueidentifier  = null,         
             @Csoport_Id_Felelos_Elozo     uniqueidentifier  = null,         
             @Kovetkezo_Felelos_Id     uniqueidentifier  = null,         
             @Elektronikus_Kezbesitesi_Allap     nvarchar(64)  = null,         
             @Kovetkezo_Orzo_Id     uniqueidentifier  = null,         
             @Fizikai_Kezbesitesi_Allapot     nvarchar(64)  = null,         
             @IraIratok_Id     uniqueidentifier  = null,         
             @BontasiMegjegyzes     Nvarchar(400)  = null,         
             @Tipus     nvarchar(64)  = null,         
             @Minosites     nvarchar(64)  = null,         
             @MegtagadasIndoka     Nvarchar(4000)  = null,         
             @Megtagado_Id     uniqueidentifier  = null,         
             @MegtagadasDat     datetime  = null,         
             @KimenoKuldemenyFajta     nvarchar(64)  = null,         
             @Elsobbsegi     char(1)  = null,         
             @Ajanlott     char(1)  = null,         
             @Tertiveveny     char(1)  = null,         
             @SajatKezbe     char(1)  = null,         
             @E_ertesites     char(1)  = null,         
             @E_elorejelzes     char(1)  = null,         
             @PostaiLezaroSzolgalat     char(1)  = null,         
             @Ar     Nvarchar(100)  = null,         
             @KimenoKuld_Sorszam      int  = null,         
             @Ver     int  = null,         
             @TovabbitasAlattAllapot     nvarchar(64)  = null,         
             @Azonosito     Nvarchar(100)  = null,         
             @BoritoTipus     nvarchar(64)  = null,         
             @MegorzesJelzo     char(1)  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
             @IktatastNemIgenyel     char(1)  = null,         
             @KezbesitesModja     Nvarchar(100)  = null,         
             @Munkaallomas     Nvarchar(100)  = null,         
             @SerultKuldemeny     char(1)  = null,         
             @TevesCimzes     char(1)  = null,         
             @TevesErkeztetes     char(1)  = null,         
             @CimzesTipusa     Nvarchar(64)  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

   create table #tempTable (id uniqueidentifier, ver INT, BarCode nvarchar(100), RagSzam nvarchar(400));
   
   declare @it int;
   declare @verPosBegin int;
   declare @verPosEnd int;
   DECLARE @BarCode_List_Begin INT;
   DECLARE @RagSzam_List_Begin INT;
   DECLARE @BarCode_List_End INT;
   DECLARE @RagSzam_List_End INT;
   declare @curId nvarchar(36);
   declare @curVer   nvarchar(6);
   DECLARE @curRagSzam NVARCHAR(400);
   DECLARE @curBarCode NVARCHAR(100);

   set @it = 0;
   set @verPosBegin = 1;
   set @BarCode_List_Begin = 1;
   set @RagSzam_List_Begin = 1;
   IF @UpdatedColumns.exist('/root/RagSzam')=1
   BEGIN
      SET @curRagSzam = @RagSzam;
   END
   ELSE
   BEGIN
      SET @curRagSzam = NULL;
   END
   IF @UpdatedColumns.exist('/root/BarCode')=1
   BEGIN
      SET @curBarCode = @BarCode;
   END
   ELSE
   BEGIN
      SET @curBarCode = NULL;
   END
   while (@it < ((len(@Ids)+1) / 39))
   BEGIN
      set @curId = SUBSTRING(@Ids,@it*39+2,37);
      set @verPosEnd =  CHARINDEX(',',@Vers,@verPosBegin);
      if @verPosEnd = 0 
         set @verPosEnd = len(@Vers) + 1;
      set @curVer = SUBSTRING(@vers, @verPosBegin, @verPosEnd - @verPosBegin);
      set @verPosBegin = @verPosEnd+1;

      IF @BarCode_List IS NOT NULL
      BEGIN
         set @BarCode_List_End = CHARINDEX(',',@BarCode_List,@BarCode_List_Begin);
         if @BarCode_List_End = 0
            set @BarCode_List_End = len(@BarCode_List) + 1
         set @curBarCode = SUBSTRING(@BarCode_List, @BarCode_List_Begin, @BarCode_List_End-@BarCode_List_Begin);
         set @BarCode_List_Begin = @BarCode_List_End + 1
      END
      IF @RagSzam_List IS NOT NULL
      BEGIN
         set @RagSzam_List_End = CHARINDEX(',',@RagSzam_List,@RagSzam_List_Begin);
         if @RagSzam_List_End = 0
            set @RagSzam_List_End = len(@RagSzam_List) + 1
         set @curRagSzam = SUBSTRING(@RagSzam_List, @RagSzam_List_Begin, @RagSzam_List_End-@RagSzam_List_Begin);
         set @RagSzam_List_Begin = @RagSzam_List_End + 1
      END
      insert into #tempTable(id,ver, BarCode, RagSzam) values(@curId,convert(int,@curVer), @curBarCode, @curRagSzam );
      set @it = @it + 1;
   END
   

   declare @rowNumberTotal int;
   declare @rowNumberChecked int;

   select @rowNumberTotal = count(id) from #tempTable;
   
   
   /******************************************
   * Ellenőrzések
   ******************************************/
   -- verzióellenőrzés
   select @rowNumberChecked = count(EREC_KuldKuldemenyek.Id)
      from EREC_KuldKuldemenyek
         inner JOIN #tempTable t on t.id = EREC_KuldKuldemenyek.id and t.ver = EREC_KuldKuldemenyek.ver
         
   if (@rowNumberTotal != @rowNumberChecked)
   BEGIN
      SELECT Id FROM #tempTable
      EXCEPT
      SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
         inner join #tempTable t on t.id = EREC_KuldKuldemenyek.id and t.ver = EREC_KuldKuldemenyek.ver
      RAISERROR('[50402]',16,1);
   END
   
   -- zárolásellenőrzés
   select @rowNumberChecked = count(EREC_KuldKuldemenyek.Id)
      from EREC_KuldKuldemenyek
         inner JOIN #tempTable t on t.id = EREC_KuldKuldemenyek.id and ISNULL(EREC_KuldKuldemenyek.Zarolo_id,@ExecutorUserId) = @ExecutorUserId
         
   if (@rowNumberTotal != @rowNumberChecked)
   BEGIN
      SELECT Id FROM #tempTable
      EXCEPT
      SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
         inner join #tempTable t on t.id = EREC_KuldKuldemenyek.id and ISNULL(EREC_KuldKuldemenyek.Zarolo_id,@ExecutorUserId) = @ExecutorUserId
      RAISERROR('[50499]',16,1);
   END
   
   -- érvényesség vége figyelése
   select @rowNumberChecked = count(EREC_KuldKuldemenyek.Id)
      from EREC_KuldKuldemenyek
         inner JOIN #tempTable t on t.id = EREC_KuldKuldemenyek.id and EREC_KuldKuldemenyek.ErvVege > @ExecutionTime
         
   if (@rowNumberTotal != @rowNumberChecked)
   BEGIN
      SELECT Id FROM #tempTable
      EXCEPT
      SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
         inner join #tempTable t on t.id = EREC_KuldKuldemenyek.id and EREC_KuldKuldemenyek.ErvVege > @ExecutionTime
      RAISERROR('[50403]',16,1);
   END
   
   -- mozgatás esetén fizikai mappa ellenőrés
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1 AND EXISTS
      (
         SELECT 1
            FROM EREC_KuldKuldemenyek
               INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = EREC_KuldKuldemenyek.Id
               INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
            WHERE EREC_KuldKuldemenyek.Id IN (SELECT Id FROM #tempTable)
               AND EREC_KuldKuldemenyek.Csoport_Id_Felelos != @Csoport_Id_Felelos
               AND KRT_Mappak.Tipus = '01'
               AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
               AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
      )
   BEGIN
      SELECT EREC_KuldKuldemenyek.Id
         FROM EREC_KuldKuldemenyek
            INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = EREC_KuldKuldemenyek.Id
            INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
         WHERE EREC_KuldKuldemenyek.Id IN (SELECT Id FROM #tempTable)
            AND EREC_KuldKuldemenyek.Csoport_Id_Felelos != @Csoport_Id_Felelos
            AND KRT_Mappak.Tipus = '01'
            AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
            AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
      RAISERROR('[64016]',16,1); -- A küldemény  csak dossziéval együtt mozgatható!
   END
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Orzo')=1 AND EXISTS
      (
         SELECT 1
            FROM EREC_KuldKuldemenyek
               INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = EREC_KuldKuldemenyek.Id
               INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
            WHERE EREC_KuldKuldemenyek.Id IN (SELECT Id FROM #tempTable)
               AND EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo != FelhasznaloCsoport_Id_Orzo
               AND KRT_Mappak.Tipus = '01'
               AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
               AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
      )
   BEGIN
      SELECT EREC_KuldKuldemenyek.Id
         FROM EREC_KuldKuldemenyek
            INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = EREC_KuldKuldemenyek.Id
            INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
         WHERE EREC_KuldKuldemenyek.Id IN (SELECT Id FROM #tempTable)
            AND EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo != FelhasznaloCsoport_Id_Orzo
            AND KRT_Mappak.Tipus = '01'
            AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
            AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
      RAISERROR('[64016]',16,1); --A küldemény  csak dossziéval együtt mozgatható!
   END
   
   
   /* Kézbesítési tétel létrehozása ha szükséges */
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1
   BEGIN
      UPDATE EREC_IraKezbesitesiTetelek SET ErvVege = GETDATE() WHERE Obj_Id IN
         (SELECT Id FROM EREC_KuldKuldemenyek WHERE Id IN (SELECT Id FROM #tempTable) AND Csoport_Id_Felelos != @Csoport_Id_Felelos)
      INSERT INTO EREC_IraKezbesitesiTetelek(Obj_Id, Obj_type, Felhasznalo_Id_Atado_LOGIN, Felhasznalo_Id_Atado_USER, Csoport_Id_Cel, Allapot)
         SELECT Id, 'EREC_KuldKuldemenyek', @ExecutorUserId, @ExecutorUserId, @Csoport_Id_Felelos, '1' FROM EREC_KuldKuldemenyek WHERE Id IN (SELECT Id FROM #tempTable) AND Csoport_Id_Felelos != @Csoport_Id_Felelos
   END
----------------------------------------------
   /******************************************
   * UPDATE string összeállítás
   ******************************************/
   DECLARE @sqlcmd   NVARCHAR(MAX);
   SET @sqlcmd = N'UPDATE EREC_KuldKuldemenyek SET';
   
    IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @sqlcmd = @sqlcmd + N' Allapot = @Allapot,'
    IF @UpdatedColumns.exist('/root/BeerkezesIdeje')=1
         SET @sqlcmd = @sqlcmd + N' BeerkezesIdeje = @BeerkezesIdeje,'
    IF @UpdatedColumns.exist('/root/FelbontasDatuma')=1
         SET @sqlcmd = @sqlcmd + N' FelbontasDatuma = @FelbontasDatuma,'
    IF @UpdatedColumns.exist('/root/IraIktatokonyv_Id')=1
         SET @sqlcmd = @sqlcmd + N' IraIktatokonyv_Id = @IraIktatokonyv_Id,'
    IF @UpdatedColumns.exist('/root/KuldesMod')=1
         SET @sqlcmd = @sqlcmd + N' KuldesMod = @KuldesMod,'
--    IF @UpdatedColumns.exist('/root/Erkezteto_Szam')=1
--         SET @sqlcmd = @sqlcmd + N' Erkezteto_Szam = @Erkezteto_Szam,'
    IF @UpdatedColumns.exist('/root/HivatkozasiSzam')=1
         SET @sqlcmd = @sqlcmd + N' HivatkozasiSzam = @HivatkozasiSzam,'
    IF @UpdatedColumns.exist('/root/Targy')=1
         SET @sqlcmd = @sqlcmd + N' Targy = @Targy,'
    IF @UpdatedColumns.exist('/root/Tartalom')=1
         SET @sqlcmd = @sqlcmd + N' Tartalom = @Tartalom,'
    IF @UpdatedColumns.exist('/root/RagSzam')=1
         SET @sqlcmd = @sqlcmd + N' RagSzam = #tempTable.RagSzam,'
    IF @UpdatedColumns.exist('/root/Surgosseg')=1
         SET @sqlcmd = @sqlcmd + N' Surgosseg = @Surgosseg,'
    IF @UpdatedColumns.exist('/root/BelyegzoDatuma')=1
         SET @sqlcmd = @sqlcmd + N' BelyegzoDatuma = @BelyegzoDatuma,'
    IF @UpdatedColumns.exist('/root/UgyintezesModja')=1
         SET @sqlcmd = @sqlcmd + N' UgyintezesModja = @UgyintezesModja,'
    IF @UpdatedColumns.exist('/root/PostazasIranya')=1
         SET @sqlcmd = @sqlcmd + N' PostazasIranya = @PostazasIranya,'
    IF @UpdatedColumns.exist('/root/Tovabbito')=1
         SET @sqlcmd = @sqlcmd + N' Tovabbito = @Tovabbito,'
    IF @UpdatedColumns.exist('/root/PeldanySzam')=1
         SET @sqlcmd = @sqlcmd + N' PeldanySzam = @PeldanySzam,'
    IF @UpdatedColumns.exist('/root/IktatniKell')=1
         SET @sqlcmd = @sqlcmd + N' IktatniKell = @IktatniKell,'
    IF @UpdatedColumns.exist('/root/Iktathato')=1
         SET @sqlcmd = @sqlcmd + N' Iktathato = @Iktathato,'
    IF @UpdatedColumns.exist('/root/SztornirozasDat')=1
         SET @sqlcmd = @sqlcmd + N' SztornirozasDat = @SztornirozasDat,'
    IF @UpdatedColumns.exist('/root/KuldKuldemeny_Id_Szulo')=1
         SET @sqlcmd = @sqlcmd + N' KuldKuldemeny_Id_Szulo = @KuldKuldemeny_Id_Szulo,'
    IF @UpdatedColumns.exist('/root/Erkeztetes_Ev')=1
         SET @sqlcmd = @sqlcmd + N' Erkeztetes_Ev = @Erkeztetes_Ev,'
    IF @UpdatedColumns.exist('/root/Csoport_Id_Cimzett')=1
         SET @sqlcmd = @sqlcmd + N' Csoport_Id_Cimzett = @Csoport_Id_Cimzett,'
    IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1
         SET @sqlcmd = @sqlcmd + N' Csoport_Id_Felelos = @Csoport_Id_Felelos,'
    IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Expedial')=1
         SET @sqlcmd = @sqlcmd + N' FelhasznaloCsoport_Id_Expedial = @FelhasznaloCsoport_Id_Expedial,'
    IF @UpdatedColumns.exist('/root/ExpedialasIdeje')=1
         SET @sqlcmd = @sqlcmd + N' ExpedialasIdeje = @ExpedialasIdeje,'
    IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Orzo')=1
         SET @sqlcmd = @sqlcmd + N' FelhasznaloCsoport_Id_Orzo = @FelhasznaloCsoport_Id_Orzo,'
    IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Atvevo')=1
         SET @sqlcmd = @sqlcmd + N' FelhasznaloCsoport_Id_Atvevo = @FelhasznaloCsoport_Id_Atvevo,'
    IF @UpdatedColumns.exist('/root/Partner_Id_Bekuldo')=1
         SET @sqlcmd = @sqlcmd + N' Partner_Id_Bekuldo = @Partner_Id_Bekuldo,'
    IF @UpdatedColumns.exist('/root/Cim_Id')=1
         SET @sqlcmd = @sqlcmd + N' Cim_Id = @Cim_Id,'
    IF @UpdatedColumns.exist('/root/CimSTR_Bekuldo')=1
         SET @sqlcmd = @sqlcmd + N' CimSTR_Bekuldo = @CimSTR_Bekuldo,'
    IF @UpdatedColumns.exist('/root/NevSTR_Bekuldo')=1
         SET @sqlcmd = @sqlcmd + N' NevSTR_Bekuldo = @NevSTR_Bekuldo,'
    IF @UpdatedColumns.exist('/root/AdathordozoTipusa')=1
         SET @sqlcmd = @sqlcmd + N' AdathordozoTipusa = @AdathordozoTipusa,'
    IF @UpdatedColumns.exist('/root/ElsodlegesAdathordozoTipusa')=1
         SET @sqlcmd = @sqlcmd + N' ElsodlegesAdathordozoTipusa = @ElsodlegesAdathordozoTipusa,'
    IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Alairo')=1
         SET @sqlcmd = @sqlcmd + N' FelhasznaloCsoport_Id_Alairo = @FelhasznaloCsoport_Id_Alairo,'
    IF @UpdatedColumns.exist('/root/BarCode')=1
         SET @sqlcmd = @sqlcmd + N' BarCode = #tempTable.BarCode,'
    IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Bonto')=1
         SET @sqlcmd = @sqlcmd + N' FelhasznaloCsoport_Id_Bonto = @FelhasznaloCsoport_Id_Bonto,'
    IF @UpdatedColumns.exist('/root/CsoportFelelosEloszto_Id')=1
         SET @sqlcmd = @sqlcmd + N' CsoportFelelosEloszto_Id = @CsoportFelelosEloszto_Id,'
    IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos_Elozo')=1
         SET @sqlcmd = @sqlcmd + N' Csoport_Id_Felelos_Elozo = @Csoport_Id_Felelos_Elozo,'
    IF @UpdatedColumns.exist('/root/Kovetkezo_Felelos_Id')=1
         SET @sqlcmd = @sqlcmd + N' Kovetkezo_Felelos_Id = @Kovetkezo_Felelos_Id,'
    IF @UpdatedColumns.exist('/root/Elektronikus_Kezbesitesi_Allap')=1
         SET @sqlcmd = @sqlcmd + N' Elektronikus_Kezbesitesi_Allap = @Elektronikus_Kezbesitesi_Allap,'
    IF @UpdatedColumns.exist('/root/Kovetkezo_Orzo_Id')=1
         SET @sqlcmd = @sqlcmd + N' Kovetkezo_Orzo_Id = @Kovetkezo_Orzo_Id,'
    IF @UpdatedColumns.exist('/root/Fizikai_Kezbesitesi_Allapot')=1
         SET @sqlcmd = @sqlcmd + N' Fizikai_Kezbesitesi_Allapot = @Fizikai_Kezbesitesi_Allapot,'
    IF @UpdatedColumns.exist('/root/IraIratok_Id')=1
         SET @sqlcmd = @sqlcmd + N' IraIratok_Id = @IraIratok_Id,'
    IF @UpdatedColumns.exist('/root/BontasiMegjegyzes')=1
         SET @sqlcmd = @sqlcmd + N' BontasiMegjegyzes = @BontasiMegjegyzes,'
    IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @sqlcmd = @sqlcmd + N' Tipus = @Tipus,'
    IF @UpdatedColumns.exist('/root/Minosites')=1
         SET @sqlcmd = @sqlcmd + N' Minosites = @Minosites,'
    IF @UpdatedColumns.exist('/root/MegtagadasIndoka')=1
         SET @sqlcmd = @sqlcmd + N' MegtagadasIndoka = @MegtagadasIndoka,'
    IF @UpdatedColumns.exist('/root/Megtagado_Id')=1
         SET @sqlcmd = @sqlcmd + N' Megtagado_Id = @Megtagado_Id,'
    IF @UpdatedColumns.exist('/root/MegtagadasDat')=1
         SET @sqlcmd = @sqlcmd + N' MegtagadasDat = @MegtagadasDat,'
    IF @UpdatedColumns.exist('/root/KimenoKuldemenyFajta')=1
         SET @sqlcmd = @sqlcmd + N' KimenoKuldemenyFajta = @KimenoKuldemenyFajta,'
    IF @UpdatedColumns.exist('/root/Elsobbsegi')=1
         SET @sqlcmd = @sqlcmd + N' Elsobbsegi = @Elsobbsegi,'
    IF @UpdatedColumns.exist('/root/Ajanlott')=1
         SET @sqlcmd = @sqlcmd + N' Ajanlott = @Ajanlott,'
    IF @UpdatedColumns.exist('/root/Tertiveveny')=1
         SET @sqlcmd = @sqlcmd + N' Tertiveveny = @Tertiveveny,'
    IF @UpdatedColumns.exist('/root/SajatKezbe')=1
         SET @sqlcmd = @sqlcmd + N' SajatKezbe = @SajatKezbe,'
    IF @UpdatedColumns.exist('/root/E_ertesites')=1
         SET @sqlcmd = @sqlcmd + N' E_ertesites = @E_ertesites,'
    IF @UpdatedColumns.exist('/root/E_elorejelzes')=1
         SET @sqlcmd = @sqlcmd + N' E_elorejelzes = @E_elorejelzes,'
    IF @UpdatedColumns.exist('/root/PostaiLezaroSzolgalat')=1
         SET @sqlcmd = @sqlcmd + N' PostaiLezaroSzolgalat = @PostaiLezaroSzolgalat,'
    IF @UpdatedColumns.exist('/root/Ar')=1
         SET @sqlcmd = @sqlcmd + N' Ar = @Ar,'
    IF @UpdatedColumns.exist('/root/KimenoKuld_Sorszam ')=1
         SET @sqlcmd = @sqlcmd + N' KimenoKuld_Sorszam  = @KimenoKuld_Sorszam ,'
--    IF @UpdatedColumns.exist('/root/Ver')=1
--         SET @sqlcmd = @sqlcmd + N' Ver = @Ver,'
    IF @UpdatedColumns.exist('/root/TovabbitasAlattAllapot')=1
         SET @sqlcmd = @sqlcmd + N' TovabbitasAlattAllapot = @TovabbitasAlattAllapot,'
    IF @UpdatedColumns.exist('/root/Azonosito')=1
         SET @sqlcmd = @sqlcmd + N' Azonosito = @Azonosito,'
    IF @UpdatedColumns.exist('/root/BoritoTipus')=1
         SET @sqlcmd = @sqlcmd + N' BoritoTipus = @BoritoTipus,'
    IF @UpdatedColumns.exist('/root/MegorzesJelzo')=1
         SET @sqlcmd = @sqlcmd + N' MegorzesJelzo = @MegorzesJelzo,'
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @sqlcmd = @sqlcmd + N' Note = @Note,'
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @sqlcmd = @sqlcmd + N' Stat_id = @Stat_id,'
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @sqlcmd = @sqlcmd + N' ErvKezd = @ErvKezd,'
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @sqlcmd = @sqlcmd + N' ErvVege = @ErvVege,'
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @sqlcmd = @sqlcmd + N' Letrehozo_id = @Letrehozo_id,'
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @sqlcmd = @sqlcmd + N' LetrehozasIdo = @LetrehozasIdo,'
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @sqlcmd = @sqlcmd + N' Modosito_id = @Modosito_id,'
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @sqlcmd = @sqlcmd + N' ModositasIdo = @ModositasIdo,'
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @sqlcmd = @sqlcmd + N' Zarolo_id = @Zarolo_id,'
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @sqlcmd = @sqlcmd + N' ZarolasIdo = @ZarolasIdo,'
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @sqlcmd = @sqlcmd + N' Tranz_id = @Tranz_id,'
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @sqlcmd = @sqlcmd + N' UIAccessLog_id = @UIAccessLog_id,'   
   IF @UpdatedColumns.exist('/root/IktatastNemIgenyel')=1
         SET @sqlcmd = @sqlcmd + N' IktatastNemIgenyel = @IktatastNemIgenyel,'   
   IF @UpdatedColumns.exist('/root/KezbesitesModja')=1
         SET @sqlcmd = @sqlcmd + N' KezbesitesModja = @KezbesitesModja,'   
   IF @UpdatedColumns.exist('/root/Munkaallomas')=1
         SET @sqlcmd = @sqlcmd + N' Munkaallomas = @Munkaallomas,'   
   IF @UpdatedColumns.exist('/root/SerultKuldemeny')=1
         SET @sqlcmd = @sqlcmd + N' SerultKuldemeny = @SerultKuldemeny,'   
   IF @UpdatedColumns.exist('/root/TevesCimzes')=1
         SET @sqlcmd = @sqlcmd + N' TevesCimzes = @TevesCimzes,'   
   IF @UpdatedColumns.exist('/root/TevesErkeztetes')=1
         SET @sqlcmd = @sqlcmd + N' TevesErkeztetes = @TevesErkeztetes,'   
   IF @UpdatedColumns.exist('/root/CimzesTipusa')=1
         SET @sqlcmd = @sqlcmd + N' CimzesTipusa = @CimzesTipusa,'   
   SET @sqlcmd = @sqlcmd + N' Ver = EREC_KuldKuldemenyek.Ver + 1'   

   SET @sqlcmd = @sqlcmd + N'
    FROM #tempTable
   WHERE EREC_KuldKuldemenyek.Id = #tempTable.Id;'

   EXECUTE sp_executesql @sqlcmd, N'
       @Allapot     nvarchar(64),         
       @BeerkezesIdeje     datetime,         
       @FelbontasDatuma     datetime,         
       @IraIktatokonyv_Id     uniqueidentifier,         
       @KuldesMod     nvarchar(64),         
--     @Erkezteto_Szam     int,         
       @HivatkozasiSzam     Nvarchar(100),         
       @Targy     Nvarchar(4000),         
       @Tartalom     Nvarchar(4000),         
       @RagSzam     Nvarchar(400),         
       @Surgosseg     nvarchar(64),         
       @BelyegzoDatuma     datetime,         
       @UgyintezesModja     nvarchar(64),         
       @PostazasIranya     nvarchar(64),         
       @Tovabbito     nvarchar(64),         
       @PeldanySzam     int,         
       @IktatniKell     nvarchar(64),         
       @Iktathato     nvarchar(64),         
       @SztornirozasDat     datetime,         
       @KuldKuldemeny_Id_Szulo     uniqueidentifier,         
       @Erkeztetes_Ev     int,         
       @Csoport_Id_Cimzett     uniqueidentifier,         
       @Csoport_Id_Felelos     uniqueidentifier,         
       @FelhasznaloCsoport_Id_Expedial     uniqueidentifier,         
       @ExpedialasIdeje     datetime,         
       @FelhasznaloCsoport_Id_Orzo     uniqueidentifier,         
       @FelhasznaloCsoport_Id_Atvevo     uniqueidentifier,         
       @Partner_Id_Bekuldo     uniqueidentifier,         
       @Cim_Id     uniqueidentifier,         
       @CimSTR_Bekuldo     Nvarchar(400),         
       @NevSTR_Bekuldo     Nvarchar(400),         
       @AdathordozoTipusa     nvarchar(64),         
       @ElsodlegesAdathordozoTipusa     nvarchar(64),         
       @FelhasznaloCsoport_Id_Alairo     uniqueidentifier,         
       @BarCode     Nvarchar(100),         
       @FelhasznaloCsoport_Id_Bonto     uniqueidentifier,         
       @CsoportFelelosEloszto_Id     uniqueidentifier,         
       @Csoport_Id_Felelos_Elozo     uniqueidentifier,         
       @Kovetkezo_Felelos_Id     uniqueidentifier,         
       @Elektronikus_Kezbesitesi_Allap     nvarchar(64),         
       @Kovetkezo_Orzo_Id     uniqueidentifier,         
       @Fizikai_Kezbesitesi_Allapot     nvarchar(64),         
       @IraIratok_Id     uniqueidentifier,         
       @BontasiMegjegyzes     Nvarchar(400),         
       @Tipus     nvarchar(64),         
       @Minosites     nvarchar(64),         
       @MegtagadasIndoka     Nvarchar(4000),         
       @Megtagado_Id     uniqueidentifier,         
       @MegtagadasDat     datetime,         
       @KimenoKuldemenyFajta     nvarchar(64),         
       @Elsobbsegi     char(1),         
       @Ajanlott     char(1),         
       @Tertiveveny     char(1),         
       @SajatKezbe     char(1),         
       @E_ertesites     char(1),         
       @E_elorejelzes     char(1),         
       @PostaiLezaroSzolgalat     char(1),         
       @Ar     Nvarchar(100),         
       @KimenoKuld_Sorszam      int,         
       @Ver     int,         
       @TovabbitasAlattAllapot     nvarchar(64),         
       @Azonosito     Nvarchar(100),         
       @BoritoTipus     nvarchar(64),         
       @MegorzesJelzo     char(1),         
       @Note     Nvarchar(4000),         
       @Stat_id     uniqueidentifier,         
       @ErvKezd     datetime,         
       @ErvVege     datetime,         
       @Letrehozo_id     uniqueidentifier,         
       @LetrehozasIdo     datetime,         
       @Modosito_id     uniqueidentifier,         
       @ModositasIdo     datetime,         
       @Zarolo_id     uniqueidentifier,         
       @ZarolasIdo     datetime,         
       @Tranz_id     uniqueidentifier,
       @UIAccessLog_id     uniqueidentifier,
       @IktatastNemIgenyel     char(1),
       @KezbesitesModja     Nvarchar(100),
       @Munkaallomas     Nvarchar(100),
       @SerultKuldemeny     char(1),
       @TevesCimzes     char(1),
       @TevesErkeztetes     char(1),
       @CimzesTipusa     Nvarchar(64)',         
       --
       @Allapot = @Allapot,
       @BeerkezesIdeje = @BeerkezesIdeje,
       @FelbontasDatuma = @FelbontasDatuma,
       @IraIktatokonyv_Id = @IraIktatokonyv_Id,
       @KuldesMod = @KuldesMod,
--     @Erkezteto_Szam = @Erkezteto_Szam,
       @HivatkozasiSzam = @HivatkozasiSzam,
       @Targy = @Targy,
       @Tartalom = @Tartalom,
       @RagSzam = @RagSzam,
       @Surgosseg = @Surgosseg,
       @BelyegzoDatuma = @BelyegzoDatuma,
       @UgyintezesModja = @UgyintezesModja,
       @PostazasIranya = @PostazasIranya,
       @Tovabbito = @Tovabbito,
       @PeldanySzam = @PeldanySzam,
       @IktatniKell = @IktatniKell,
       @Iktathato = @Iktathato,
       @SztornirozasDat = @SztornirozasDat,
       @KuldKuldemeny_Id_Szulo = @KuldKuldemeny_Id_Szulo,
       @Erkeztetes_Ev = @Erkeztetes_Ev,
       @Csoport_Id_Cimzett = @Csoport_Id_Cimzett,
       @Csoport_Id_Felelos = @Csoport_Id_Felelos,
       @FelhasznaloCsoport_Id_Expedial = @FelhasznaloCsoport_Id_Expedial,
       @ExpedialasIdeje = @ExpedialasIdeje,
       @FelhasznaloCsoport_Id_Orzo = @FelhasznaloCsoport_Id_Orzo,
       @FelhasznaloCsoport_Id_Atvevo = @FelhasznaloCsoport_Id_Atvevo,
       @Partner_Id_Bekuldo = @Partner_Id_Bekuldo,
       @Cim_Id = @Cim_Id,
       @CimSTR_Bekuldo = @CimSTR_Bekuldo,
       @NevSTR_Bekuldo = @NevSTR_Bekuldo,
       @AdathordozoTipusa = @AdathordozoTipusa,
       @ElsodlegesAdathordozoTipusa = @ElsodlegesAdathordozoTipusa,
       @FelhasznaloCsoport_Id_Alairo = @FelhasznaloCsoport_Id_Alairo,
       @BarCode = @BarCode,
       @FelhasznaloCsoport_Id_Bonto = @FelhasznaloCsoport_Id_Bonto,
       @CsoportFelelosEloszto_Id = @CsoportFelelosEloszto_Id,
       @Csoport_Id_Felelos_Elozo = @Csoport_Id_Felelos_Elozo,
       @Kovetkezo_Felelos_Id = @Kovetkezo_Felelos_Id,
       @Elektronikus_Kezbesitesi_Allap = @Elektronikus_Kezbesitesi_Allap,
       @Kovetkezo_Orzo_Id = @Kovetkezo_Orzo_Id,
       @Fizikai_Kezbesitesi_Allapot = @Fizikai_Kezbesitesi_Allapot,
       @IraIratok_Id = @IraIratok_Id,
       @BontasiMegjegyzes = @BontasiMegjegyzes,
       @Tipus = @Tipus,
       @Minosites = @Minosites,
       @MegtagadasIndoka = @MegtagadasIndoka,
       @Megtagado_Id = @Megtagado_Id,
       @MegtagadasDat = @MegtagadasDat,
       @KimenoKuldemenyFajta = @KimenoKuldemenyFajta,
       @Elsobbsegi = @Elsobbsegi,
       @Ajanlott = @Ajanlott,
       @Tertiveveny = @Tertiveveny,
       @SajatKezbe = @SajatKezbe,
       @E_ertesites = @E_ertesites,
       @E_elorejelzes = @E_elorejelzes,
       @PostaiLezaroSzolgalat = @PostaiLezaroSzolgalat,
       @Ar = @Ar,
       @KimenoKuld_Sorszam  = @KimenoKuld_Sorszam,
       @Ver = @Ver,
       @TovabbitasAlattAllapot = @TovabbitasAlattAllapot,
       @Azonosito = @Azonosito,
       @BoritoTipus = @BoritoTipus,
       @MegorzesJelzo = @MegorzesJelzo,
       @Note = @Note,
       @Stat_id = @Stat_id,
       @ErvKezd = @ErvKezd,
       @ErvVege = @ErvVege,
       @Letrehozo_id = @Letrehozo_id,
       @LetrehozasIdo = @LetrehozasIdo,
       @Modosito_id = @Modosito_id,
       @ModositasIdo = @ModositasIdo,
       @Zarolo_id = @Zarolo_id,
       @ZarolasIdo = @ZarolasIdo,
       @Tranz_id = @Tranz_id,
       @UIAccessLog_id = @UIAccessLog_id,
       @IktatastNemIgenyel = @IktatastNemIgenyel,
       @KezbesitesModja = @KezbesitesModja,
       @Munkaallomas = @Munkaallomas,
       @SerultKuldemeny = @SerultKuldemeny,
       @TevesCimzes = @TevesCimzes,
       @TevesErkeztetes = @TevesErkeztetes,
       @CimzesTipusa = @CimzesTipusa

       
   if @@rowcount != @rowNumberTotal
   BEGIN
      RAISERROR('[50401]',16,1)
   END
   ELSE
   BEGIN
      /* History Log */

      declare @row_ids varchar(MAX)
      /*** EREC_KuldKuldemenyek history log ***/

      set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from #tempTable FOR XML PATH('')),1, 2, '')
      if @row_ids is not null
         set @row_ids = @row_ids + '''';

      exec sp_LogRecordsToHistory_Tomeges
       @TableName = 'EREC_KuldKuldemenyek'
      ,@Row_Ids = @row_ids
      ,@Muvelet = 1
      ,@Vegrehajto_Id = @ExecutorUserId
      ,@VegrehajtasIdo = @ExecutionTime
   END

drop table #tempTable
--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH




GO
