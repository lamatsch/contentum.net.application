IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldMellekletekGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KuldMellekletekGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldMellekletekGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KuldMellekletekGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_KuldMellekletekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_KuldMellekletek.Id,
	   EREC_KuldMellekletek.KuldKuldemeny_Id,
	   EREC_KuldMellekletek.Mennyiseg,
	   EREC_KuldMellekletek.Megjegyzes,
	   EREC_KuldMellekletek.SztornirozasDat,
	   EREC_KuldMellekletek.AdathordozoTipus,
	   EREC_KuldMellekletek.MennyisegiEgyseg,
	   EREC_KuldMellekletek.BarCode,
	   EREC_KuldMellekletek.Ver,
	   EREC_KuldMellekletek.Note,
	   EREC_KuldMellekletek.Stat_id,
	   EREC_KuldMellekletek.ErvKezd,
	   EREC_KuldMellekletek.ErvVege,
	   EREC_KuldMellekletek.Letrehozo_id,
	   EREC_KuldMellekletek.LetrehozasIdo,
	   EREC_KuldMellekletek.Modosito_id,
	   EREC_KuldMellekletek.ModositasIdo,
	   EREC_KuldMellekletek.Zarolo_id,
	   EREC_KuldMellekletek.ZarolasIdo,
	   EREC_KuldMellekletek.Tranz_id,
	   EREC_KuldMellekletek.UIAccessLog_id
	   from 
		 EREC_KuldMellekletek as EREC_KuldMellekletek 
	   where
		 EREC_KuldMellekletek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
