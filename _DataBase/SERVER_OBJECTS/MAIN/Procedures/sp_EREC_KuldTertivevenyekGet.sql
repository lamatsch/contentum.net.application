IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldTertivevenyekGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KuldTertivevenyekGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldTertivevenyekGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KuldTertivevenyekGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_KuldTertivevenyekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_KuldTertivevenyek.Id,
	   EREC_KuldTertivevenyek.Kuldemeny_Id,
	   EREC_KuldTertivevenyek.Ragszam,
	   EREC_KuldTertivevenyek.TertivisszaKod,
	   EREC_KuldTertivevenyek.TertivisszaDat,
	   EREC_KuldTertivevenyek.AtvevoSzemely,
	   EREC_KuldTertivevenyek.AtvetelDat,
	   EREC_KuldTertivevenyek.BarCode,
	   EREC_KuldTertivevenyek.Allapot,
	   EREC_KuldTertivevenyek.Ver,
	   EREC_KuldTertivevenyek.Note,
	   EREC_KuldTertivevenyek.Stat_id,
	   EREC_KuldTertivevenyek.ErvKezd,
	   EREC_KuldTertivevenyek.ErvVege,
	   EREC_KuldTertivevenyek.Letrehozo_id,
	   EREC_KuldTertivevenyek.LetrehozasIdo,
	   EREC_KuldTertivevenyek.Modosito_id,
	   EREC_KuldTertivevenyek.ModositasIdo,
	   EREC_KuldTertivevenyek.Zarolo_id,
	   EREC_KuldTertivevenyek.ZarolasIdo,
	   EREC_KuldTertivevenyek.Tranz_id,
	   EREC_KuldTertivevenyek.UIAccessLog_id,
	   EREC_KuldTertivevenyek.KezbVelelemBeallta,
	   EREC_KuldTertivevenyek.KezbVelelemDatuma
	   from 
		 EREC_KuldTertivevenyek as EREC_KuldTertivevenyek 
	   where
		 EREC_KuldTertivevenyek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
