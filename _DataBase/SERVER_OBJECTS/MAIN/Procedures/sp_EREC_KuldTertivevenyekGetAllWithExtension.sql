IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldTertivevenyekGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KuldTertivevenyekGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldTertivevenyekGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KuldTertivevenyekGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_KuldTertivevenyekGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @Where_IratPeldany_Cimzett nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by EREC_KuldTertivevenyek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId          uniqueidentifier,
  --@Id    uniqueidentifier = null,
  @pageNumber     int = 0,
  @pageSize    int = -1,
  @SelectedRowId  uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on

   DECLARE @Org uniqueidentifier
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
   if (@Org is null)
   begin
      RAISERROR('[50202]',16,1)
   end
   
   DECLARE @sqlcmd nvarchar(MAX)
   SET @sqlcmd = ''

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
   BEGIN
      SET @LocalTopRow = ''
   END
   ELSE
   BEGIN
      SET @LocalTopRow = ' TOP ' + @TopRow
   END
   DECLARE @firstRow INT
   DECLARE @lastRow INT
   
   if (@pageSize > 0 and @pageNumber > -1)
   begin
      set @firstRow = (@pageNumber)*@pageSize + 1
      set @lastRow = @firstRow + @pageSize - 1
   end
   else begin
      if (@TopRow = '' or @TopRow = '0')
      begin
         set @firstRow = 1
         set @lastRow = 1000
      end 
      else
      begin
         set @firstRow = 1
         set @lastRow = @TopRow
      end   
   end

   /************************************************************
   * Szurt adatokhoz rendezés és sorszám összeállítása         *
   ************************************************************/
   SET @sqlcmd = @sqlcmd + N'
   select 
      row_number() over('+@OrderBy+') as RowNumber,
      EREC_KuldTertivevenyek.Id into #result
      from dbo.EREC_KuldTertivevenyek as EREC_KuldTertivevenyek
         JOIN dbo.EREC_KuldKuldemenyek AS EREC_KuldKuldemenyek ON EREC_KuldKuldemenyek.Id = EREC_KuldTertivevenyek.Kuldemeny_Id
      left join dbo.KRT_KodCsoportok AS KRT_KodCsoportok_TERTIVEVENY_ALLAPOT on KRT_KodCsoportok_TERTIVEVENY_ALLAPOT.Kod=''TERTIVEVENY_ALLAPOT''
      left join dbo.KRT_KodTarak AS KRT_KodTarak_TERTIVEVENY_ALLAPOT on KRT_KodCsoportok_TERTIVEVENY_ALLAPOT.Id = KRT_KodTarak_TERTIVEVENY_ALLAPOT.KodCsoport_Id and KRT_KodTarak_TERTIVEVENY_ALLAPOT.Org=@Org
         and EREC_KuldTertivevenyek.Allapot = KRT_KodTarak_TERTIVEVENY_ALLAPOT.Kod
      left join dbo.KRT_KodCsoportok AS KRT_KodCsoportok_TERTIVEVENY_VISSZA_KOD on KRT_KodCsoportok_TERTIVEVENY_VISSZA_KOD.Kod=''TERTIVEVENY_VISSZA_KOD''
      left join dbo.KRT_KodTarak AS KRT_KodTarak_TERTIVEVENY_VISSZA_KOD on KRT_KodCsoportok_TERTIVEVENY_VISSZA_KOD.Id = KRT_KodTarak_TERTIVEVENY_VISSZA_KOD.KodCsoport_Id and KRT_KodTarak_TERTIVEVENY_VISSZA_KOD.Org=@Org
         and EREC_KuldTertivevenyek.TertivisszaKod = KRT_KodTarak_TERTIVEVENY_VISSZA_KOD.Kod
      OUTER APPLY (select top 1 EREC_PldIratPeldanyok.* from dbo.EREC_PldIratPeldanyok AS EREC_PldIratPeldanyok
         inner join dbo.EREC_Kuldemeny_IratPeldanyai as EREC_Kuldemeny_IratPeldanyai on EREC_Kuldemeny_IratPeldanyai.Peldany_Id = EREC_PldIratPeldanyok.Id
            and getdate() between EREC_Kuldemeny_IratPeldanyai.ErvKezd and EREC_Kuldemeny_IratPeldanyai.ErvVege
         where EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id) as EREC_PldIratPeldanyok
'

   if @Where is not null and @Where!=''
   begin 
      SET @sqlcmd = @sqlcmd + ' Where ' + @Where
   end

   if @Where_IratPeldany_Cimzett is not null and @Where_IratPeldany_Cimzett != ''
   begin
      if @Where is null or @Where=''
         SET @sqlcmd = @sqlcmd + ' Where '
      else SET @sqlcmd = @sqlcmd + ' and '

--    SET @sqlcmd = @sqlcmd + '
-- EREC_KuldKuldemenyek.Id in 
-- (select EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id
--    from EREC_Kuldemeny_IratPeldanyai
--       join EREC_PldIratPeldanyok on EREC_Kuldemeny_IratPeldanyai.Peldany_Id = EREC_PldIratPeldanyok.Id
--       where getdate() between EREC_Kuldemeny_IratPeldanyai.ErvKezd and EREC_Kuldemeny_IratPeldanyai.ErvVege
--          and ' + @Where_IratPeldany_Cimzett + '
-- )
--'
      SET @sqlcmd = @sqlcmd + @Where_IratPeldany_Cimzett
            
   END

   if (@SelectedRowId is not null)
   begin
      set @sqlcmd = @sqlcmd + N'
      if exists (select 1 from #result where Id = @SelectedRowId)
      BEGIN
         select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
         set @firstRow = (@pageNumber - 1) * @pageSize + 1;
         set @lastRow = @pageNumber*@pageSize;
         select @pageNumber = @pageNumber - 1
      END
      ELSE'
   end

   set @sqlcmd = @sqlcmd + N' 
   if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
   BEGIN
      select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
      set @firstRow = (@pageNumber - 1) * @pageSize + 1;
      set @lastRow = @pageNumber*@pageSize;
      select @pageNumber = @pageNumber - 1
   END ;'

          
   /************************************************************
   * Tényleges select                                 *
   ************************************************************/
   SET @sqlcmd = @sqlcmd + 
  'select 
      #result.RowNumber,
      EREC_KuldTertivevenyek.Id,
      EREC_KuldTertivevenyek.Kuldemeny_Id,
      EREC_KuldTertivevenyek.Ragszam,
      EREC_KuldTertivevenyek.TertivisszaKod,
      dbo.fn_KodtarErtekNeve(''TERTIVEVENY_VISSZA_KOD'', EREC_KuldTertivevenyek.TertivisszaKod,@Org) as TertivisszaNev,
      EREC_KuldTertivevenyek.TertivisszaDat,
      CONVERT(nvarchar(10), EREC_KuldTertivevenyek.TertivisszaDat, 102) as TertivisszaDat_rovid,
      EREC_KuldTertivevenyek.AtvevoSzemely,
      EREC_KuldTertivevenyek.AtvetelDat,
      EREC_KuldTertivevenyek.BarCode,
      EREC_KuldTertivevenyek.Allapot,
      dbo.fn_KodtarErtekNeve(''TERTIVEVENY_ALLAPOT'', EREC_KuldTertivevenyek.Allapot,@Org) as TertivevenyAllapotNev,
      IsNull((select TOP 1 NevSTR_Cimzett from EREC_PldIratPeldanyok
         join EREC_Kuldemeny_IratPeldanyai on EREC_Kuldemeny_IratPeldanyai.Peldany_Id = EREC_PldIratPeldanyok.Id
         where EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
         and getdate() between EREC_Kuldemeny_IratPeldanyai.ErvKezd and EREC_Kuldemeny_IratPeldanyai.ErvVege),
         EREC_KuldKuldemenyek.NevSTR_Bekuldo) as Cimzett_Nev_Partner,
      IsNull((select TOP 1 CimSTR_Cimzett from EREC_PldIratPeldanyok
         join EREC_Kuldemeny_IratPeldanyai on EREC_Kuldemeny_IratPeldanyai.Peldany_Id = EREC_PldIratPeldanyok.Id
         where EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
         and getdate() between EREC_Kuldemeny_IratPeldanyai.ErvKezd and EREC_Kuldemeny_IratPeldanyai.ErvVege),
         EREC_KuldKuldemenyek.CimSTR_Bekuldo) as Cimzett_Cim_Partner,
      EREC_KuldKuldemenyek.BelyegzoDatuma,
      CONVERT(nvarchar(10), EREC_KuldKuldemenyek.BelyegzoDatuma, 102) as BelyegzoDatuma_rovid,
      EREC_KuldKuldemenyek.RagSzam AS RagSzam_Kuld,
      EREC_KuldTertivevenyek.Ver,
      EREC_KuldTertivevenyek.Note,
      EREC_KuldTertivevenyek.Stat_id,
      EREC_KuldTertivevenyek.ErvKezd,
      EREC_KuldTertivevenyek.ErvVege,
      EREC_KuldTertivevenyek.Letrehozo_id,
      EREC_KuldTertivevenyek.LetrehozasIdo,
      EREC_KuldTertivevenyek.Modosito_id,
      EREC_KuldTertivevenyek.ModositasIdo,
      EREC_KuldTertivevenyek.Zarolo_id,
      EREC_KuldTertivevenyek.ZarolasIdo,
      EREC_KuldTertivevenyek.Tranz_id,
      EREC_KuldTertivevenyek.UIAccessLog_id,
      EREC_KuldTertivevenyek.KezbVelelemBeallta,
      EREC_KuldTertivevenyek.KezbVelelemDatuma
   from 
     EREC_KuldTertivevenyek as EREC_KuldTertivevenyek 
      inner join #result on #result.Id = EREC_KuldTertivevenyek.Id     
   JOIN EREC_KuldKuldemenyek AS EREC_KuldKuldemenyek ON 
      EREC_KuldKuldemenyek.Id = EREC_KuldTertivevenyek.Kuldemeny_Id
    where RowNumber between @firstRow and @lastRow
   ORDER BY #result.RowNumber;'
   
   -- találatok száma és oldalszám
   set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';
   execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier, @Org uniqueidentifier',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId, @Org = @Org;  


END TRY
BEGIN CATCH
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
