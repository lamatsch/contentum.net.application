IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldTertivevenyekKuldemenyei]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_KuldTertivevenyekKuldemenyei]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_KuldTertivevenyekKuldemenyei]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_KuldTertivevenyekKuldemenyei] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_KuldTertivevenyekKuldemenyei]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by dbo.EREC_KuldKuldemenyek.BelyegzoDatuma',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
  SET @sqlcmd = 
'select ' + @LocalTopRow + '
        dbo.EREC_KuldKuldemenyek.Id as Id,
        dbo.EREC_KuldKuldemenyek.BarCode as Vonalkod,
        dbo.EREC_KuldKuldemenyek.RagSzam as RagSzam,
        CONVERT(nvarchar(10), dbo.EREC_KuldKuldemenyek.BelyegzoDatuma, 102)  as PostazasDatuma,
        dbo.EREC_KuldKuldemenyek.NevSTR_Bekuldo as CimzettNeve,
        dbo.EREC_KuldKuldemenyek.CimSTR_Bekuldo as CimzettCime,
        dbo.EREC_KuldKuldemenyek.Allapot,
        dbo.fn_KodtarErtekNeve(''KULDEMENY_ALLAPOT'', dbo.erec_kuldKuldemenyek.Allapot,''' + CAST(@Org as NVarChar(40))+ ''') as AllapotNev,
        dbo.EREC_KuldKuldemenyek.Kuldesmod,
        dbo.fn_KodtarErtekNeve(''KULDEMENY_KULDES_MODJA'', dbo.erec_kuldKuldemenyek.Kuldesmod,''' + CAST(@Org as NVarChar(40))+ ''') as KuldesmodNev,
        dbo.EREC_KuldKuldemenyek.Postazasiranya,
        dbo.fn_KodtarErtekNeve(''POSTAZAS_IRANYA'', dbo.erec_kuldKuldemenyek.Postazasiranya,''' + CAST(@Org as NVarChar(40))+ ''') as PostazasiranyaNev
   from dbo.EREC_KuldKuldemenyek
	join EREC_IraIktatoKonyvek on EREC_KuldKuldemenyek.IraIktatokonyv_Id=EREC_IraIktatoKonyek.Id and EREC:IraIktatoKonyvek.Org=''' + CAST(@Org as NVarChar(40)) + '''
  where dbo.EREC_KuldKuldemenyek.Allapot = ''52''
    and dbo.EREC_KuldKuldemenyek.Kuldesmod = ''02''
    and dbo.EREC_KuldKuldemenyek.Postazasiranya = ''0''
'
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
