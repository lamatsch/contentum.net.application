IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Kuldemeny_IratPeldanyaiGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_Kuldemeny_IratPeldanyaiGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Kuldemeny_IratPeldanyaiGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_Kuldemeny_IratPeldanyaiGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_Kuldemeny_IratPeldanyaiGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_Kuldemeny_IratPeldanyai.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_Kuldemeny_IratPeldanyai.Id,
	   EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id,
	   EREC_Kuldemeny_IratPeldanyai.Peldany_Id,
	   EREC_Kuldemeny_IratPeldanyai.Ver,
	   EREC_Kuldemeny_IratPeldanyai.Note,
	   EREC_Kuldemeny_IratPeldanyai.Stat_id,
	   EREC_Kuldemeny_IratPeldanyai.ErvKezd,
	   EREC_Kuldemeny_IratPeldanyai.ErvVege,
	   EREC_Kuldemeny_IratPeldanyai.Letrehozo_id,
	   EREC_Kuldemeny_IratPeldanyai.LetrehozasIdo,
	   EREC_Kuldemeny_IratPeldanyai.Modosito_id,
	   EREC_Kuldemeny_IratPeldanyai.ModositasIdo,
	   EREC_Kuldemeny_IratPeldanyai.Zarolo_id,
	   EREC_Kuldemeny_IratPeldanyai.ZarolasIdo,
	   EREC_Kuldemeny_IratPeldanyai.Tranz_id,
	   EREC_Kuldemeny_IratPeldanyai.UIAccessLog_id  
   from 
     EREC_Kuldemeny_IratPeldanyai as EREC_Kuldemeny_IratPeldanyai      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
