IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Kuldemeny_IratPeldanyaiGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_Kuldemeny_IratPeldanyaiGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Kuldemeny_IratPeldanyaiGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_Kuldemeny_IratPeldanyaiGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_Kuldemeny_IratPeldanyaiGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @WhereKuldemeny NVARCHAR(4000) = '',
  @WherePeldany NVARCHAR(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_Kuldemeny_IratPeldanyai.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	print @Org

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
	EREC_Kuldemeny_IratPeldanyai.Id,
	EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id,
	EREC_Kuldemeny_IratPeldanyai.Peldany_Id,
	EREC_PldIratPeldanyok.IraIrat_Id,
	EREC_PldIratPeldanyok.Azonosito,
	EREC_PldIratPeldanyok.BarCode,
	EREC_Kuldemeny_IratPeldanyai.Ver,
	EREC_Kuldemeny_IratPeldanyai.Note,
	EREC_Kuldemeny_IratPeldanyai.Stat_id,
	EREC_Kuldemeny_IratPeldanyai.ErvKezd,
	EREC_Kuldemeny_IratPeldanyai.ErvVege,
	EREC_Kuldemeny_IratPeldanyai.Letrehozo_id,
	EREC_Kuldemeny_IratPeldanyai.LetrehozasIdo,
	EREC_Kuldemeny_IratPeldanyai.Modosito_id,
	EREC_Kuldemeny_IratPeldanyai.ModositasIdo,
	EREC_Kuldemeny_IratPeldanyai.Zarolo_id,
	EREC_Kuldemeny_IratPeldanyai.ZarolasIdo,
	EREC_Kuldemeny_IratPeldanyai.Tranz_id,
	EREC_Kuldemeny_IratPeldanyai.UIAccessLog_id,
	EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Expedial AS FelhasznaloCsoport_Id_Expedial,
	EREC_KuldKuldemenyek.ExpedialasIdeje AS ExpedialasIdeje,
	EREC_KuldKuldemenyek.Allapot AS Kuldemeny_Allapot,
	dbo.fn_KodtarErtekNeve(''KULDEMENY_ALLAPOT'', EREC_KuldKuldemenyek.Allapot,''' + CAST(@Org as NVarChar(40)) + ''') as Kuldemeny_Allapot_Nev,
	CONVERT(nvarchar(10), EREC_KuldKuldemenyek.[BelyegzoDatuma], 102) as BelyegzoDatuma,		--Kimenonél Postazas dátuma
	EREC_KuldKuldemenyek.[KimenoKuld_Sorszam],
	EREC_KuldKuldemenyek.[PostazasIranya], 
	EREC_KuldKuldemenyek.[FelhasznaloCsoport_Id_Orzo],
	EREC_KuldKuldemenyek.[Csoport_Id_Felelos],
	EREC_KuldKuldemenyek.[KuldesMod],
	EREC_KuldKuldemenyek.[IktatniKell],
	EREC_KuldKuldemenyek.Csoport_Id_Cimzett,
	dbo.fn_GetCsoportNev(EREC_KuldKuldemenyek.Csoport_Id_Cimzett) as Csoport_Id_CimzettNev, -- Kimenonél Kiküldo
	EREC_KuldKuldemenyek.NevSTR_Bekuldo, -- Kimenonél Címzett
	EREC_KuldKuldemenyek.NevSTR_Bekuldo as Bekuldo_Id_Nev, -- Kimenonél Címzett
	EREC_KuldKuldemenyek.Cim_Id, 
    EREC_KuldKuldemenyek.CimSTR_Bekuldo,	-- Kimenonél Címzett címe
(CASE WHEN EREC_KuldKuldemenyek.CimSTR_Bekuldo = NULL THEN dbo.fn_MergeCim(
   KRT_Cimek.Tipus,
   KRT_Cimek.OrszagNev,
   KRT_Cimek.IRSZ,
   KRT_Cimek.TelepulesNev,
   KRT_Cimek.KozteruletNev,
   KRT_Cimek.KozteruletTipusNev,
   KRT_Cimek.Hazszam,
   KRT_Cimek.Hazszamig,
   KRT_Cimek.HazszamBetujel,
   KRT_Cimek.Lepcsohaz,
   KRT_Cimek.Szint,
   KRT_Cimek.Ajto,
   KRT_Cimek.AjtoBetujel,
   KRT_Cimek.CimTobbi) ELSE EREC_KuldKuldemenyek.CimSTR_Bekuldo END) as Cim_Id_Nev,   -- Kimenonél Címzett címe
	-- Tertivevény (nincs csatolmány)
	[dbo].[fn_Get_KuldemenyIratPeldanyai_Tertiveveny](EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id,''0'') Tertiveveny_Id,
	-- Tertivevény-csatolmány
	[dbo].[fn_Get_KuldemenyIratPeldanyai_Tertiveveny](EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id,''1'') As Tertiveveny_Dokumentum_Id
   from 
     EREC_Kuldemeny_IratPeldanyai as EREC_Kuldemeny_IratPeldanyai
     LEFT JOIN EREC_KuldKuldemenyek AS EREC_KuldKuldemenyek ON EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
     Left JOIN EREC_PldIratPeldanyok as EREC_PldIratPeldanyok ON EREC_Kuldemeny_IratPeldanyai.Peldany_Id = EREC_PldIratPeldanyok.Id    
     left join KRT_Cimek as KRT_Cimek on KRT_Cimek.Id = EREC_KuldKuldemenyek.Cim_Id '

    if (@Where is not null and @Where!='') OR (@WhereKuldemeny is not null and @WhereKuldemeny!='') OR (@WherePeldany is not null and @WherePeldany!='')
	begin 
		SET @sqlcmd = @sqlcmd + ' Where '
	END
	
	DECLARE @FullWhere NVARCHAR(MAX)
	SET @FullWhere = ''
	
	if (@Where is not null and @Where!='')
	begin 
		SET @FullWhere = @FullWhere + @Where
	END
	
	if (@WhereKuldemeny is not null and @WhereKuldemeny!='')
	begin 
		IF(@FullWhere != '')
	    BEGIN
			SET @FullWhere = @FullWhere + ' and '
	    END
		SET @FullWhere = @FullWhere + @WhereKuldemeny
	END
	
	if (@WherePeldany is not null and @WherePeldany!='')
	begin 
	    IF(@FullWhere != '')
	    BEGIN
			SET @FullWhere = @FullWhere + ' and '
	    END
		SET @FullWhere = @FullWhere + @WherePeldany
	END
	
	IF(@FullWhere != '')
    BEGIN
		SET @sqlcmd = @sqlcmd + @FullWhere
    END
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;

   print @sqlcmd

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
