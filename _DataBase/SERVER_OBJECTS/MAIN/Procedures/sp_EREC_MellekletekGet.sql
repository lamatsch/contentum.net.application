IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_MellekletekGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_MellekletekGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_MellekletekGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_MellekletekGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_MellekletekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_Mellekletek.Id,
	   EREC_Mellekletek.KuldKuldemeny_Id,
	   EREC_Mellekletek.IraIrat_Id,
	   EREC_Mellekletek.AdathordozoTipus,
	   EREC_Mellekletek.Megjegyzes,
	   EREC_Mellekletek.SztornirozasDat,
	   EREC_Mellekletek.MennyisegiEgyseg,
	   EREC_Mellekletek.Mennyiseg,
	   EREC_Mellekletek.BarCode,
	   EREC_Mellekletek.Ver,
	   EREC_Mellekletek.Note,
	   EREC_Mellekletek.Stat_id,
	   EREC_Mellekletek.ErvKezd,
	   EREC_Mellekletek.ErvVege,
	   EREC_Mellekletek.Letrehozo_id,
	   EREC_Mellekletek.LetrehozasIdo,
	   EREC_Mellekletek.Modosito_id,
	   EREC_Mellekletek.ModositasIdo,
	   EREC_Mellekletek.Zarolo_id,
	   EREC_Mellekletek.ZarolasIdo,
	   EREC_Mellekletek.Tranz_id,
	   EREC_Mellekletek.UIAccessLog_id,
	   EREC_Mellekletek.MellekletAdathordozoTipus
	   from 
		 EREC_Mellekletek as EREC_Mellekletek 
	   where
		 EREC_Mellekletek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
