IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_MellekletekInsertTertivevenyTomeges]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_MellekletekInsertTertivevenyTomeges]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_MellekletekInsertTertivevenyTomeges]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_MellekletekInsertTertivevenyTomeges] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_MellekletekInsertTertivevenyTomeges]   
--        @Ids	NVARCHAR(MAX),
--        @Vers	NVARCHAR(MAX),
				@Kuldemeny_Id_List	NVARCHAR(MAX),    
                @BarCode_List	NVARCHAR(MAX) = null,
                @KuldKuldemeny_Id     uniqueidentifier  = null, -- csak egyedi lehet, nem vesszük figyelembe
                @IraIrat_Id     uniqueidentifier  = null, -- csak egyedi (és csak küldemény) lehet, nem vesszük figyelembe
	            @AdathordozoTipus     nvarchar(64) = '14', -- Tertiveveny
                @Megjegyzes     Nvarchar(400)  = null,
                @SztornirozasDat     datetime  = null,
                @MennyisegiEgyseg     nvarchar(64)  = '02', -- Darab
	            @Mennyiseg     int = 1, -- 1
                @BarCode     Nvarchar(100)  = null, -- csak egyedi lehet, nem vesszük figyelembe
                @Ver     int  = null, -- csak 1 lehet, nem vesszük figyelembe
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null, -- ha nincs megadva, generáljuk
                @UIAccessLog_id     uniqueidentifier  = null,
		@UpdatedColumns              xml = null -- nem használt
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
	create table #tempTable(Kuldemeny_Id uniqueidentifier
		, BarCode nvarchar(100));

	declare @it	int;
--	declare @verPosBegin int;
--	declare @verPosEnd int;
	DECLARE @BarCode_List_Begin INT;
	DECLARE @BarCode_List_End INT;
--	declare @curId	nvarchar(36);
--	declare @curVer	nvarchar(6);
	DECLARE @curKuldemeny_Id NVARCHAR(36);
	declare @curBarCode nvarchar(100);

	set @it = 0;
--	set @verPosBegin = 1;
	set @BarCode_List_Begin = 1;
	--while (@it < ((len(@Ids)+1) / 39))
	while (@it < ((len(@Kuldemeny_Id_List)+1) / 39))
	BEGIN
--		set @curId = SUBSTRING(@Ids,@it*39+2,37);
--		set @verPosEnd =  CHARINDEX(',',@Vers,@verPosBegin);
--		if @verPosEnd = 0 
--			set @verPosEnd = len(@Vers) + 1;
--		set @curVer = SUBSTRING(@vers, @verPosBegin, @verPosEnd - @verPosBegin);
--		set @verPosBegin = @verPosEnd+1;

		set @curKuldemeny_Id = SUBSTRING(@Kuldemeny_Id_List,@it*39+2,37);

		IF @BarCode_List IS NOT NULL
		BEGIN
			set @BarCode_List_End = CHARINDEX(',',@BarCode_List,@BarCode_List_Begin);
			if @BarCode_List_End = 0
				set @BarCode_List_End = len(@BarCode_List) + 1
			set @curBarCode = SUBSTRING(@BarCode_List, @BarCode_List_Begin, @BarCode_List_End-@BarCode_List_Begin);
			set @BarCode_List_Begin = @BarCode_List_End + 1
		END

		insert into #tempTable(Kuldemeny_Id, BarCode) values(@curKuldemeny_Id, @curBarCode);
		set @it = @it + 1;
	END
	
	if @Tranz_id IS NULL
	BEGIN 
		set @Tranz_id = newid();
	END

	DECLARE @insertColumns NVARCHAR(4000)
	DECLARE @insertValues NVARCHAR(4000)
	SET @insertColumns = ''
	SET @insertValues = '' 
       
--         if @Id is not null
--         begin
--            SET @insertColumns = @insertColumns + ',Id'
--            SET @insertValues = @insertValues + ',@Id'
--         end 
       
--         if @KuldKuldemeny_Id is not null
--         begin
            SET @insertColumns = @insertColumns + ',KuldKuldemeny_Id'
            SET @insertValues = @insertValues + ',#tempTable.Kuldemeny_Id'
--         end 
       
--         if @IraIrat_Id is not null
--         begin
--            SET @insertColumns = @insertColumns + ',IraIrat_Id'
--            SET @insertValues = @insertValues + ',@IraIrat_Id'
--         end 
       
         if @AdathordozoTipus is not null
         begin
            SET @insertColumns = @insertColumns + ',AdathordozoTipus'
            SET @insertValues = @insertValues + ',@AdathordozoTipus'
         end 
       
         if @Megjegyzes is not null
         begin
            SET @insertColumns = @insertColumns + ',Megjegyzes'
            SET @insertValues = @insertValues + ',@Megjegyzes'
         end 
       
         if @SztornirozasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SztornirozasDat'
            SET @insertValues = @insertValues + ',@SztornirozasDat'
         end 
       
         if @MennyisegiEgyseg is not null
         begin
            SET @insertColumns = @insertColumns + ',MennyisegiEgyseg'
            SET @insertValues = @insertValues + ',@MennyisegiEgyseg'
         end 
       
         if @Mennyiseg is not null
         begin
            SET @insertColumns = @insertColumns + ',Mennyiseg'
            SET @insertValues = @insertValues + ',@Mennyiseg'
         end 
       
--         if @BarCode is not null
--         begin
            SET @insertColumns = @insertColumns + ',BarCode'
            SET @insertValues = @insertValues + ',#tempTable.BarCode'
--         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_Mellekletek ('+@insertColumns+') output inserted.id into @InsertedRow select '+@insertValues+' from #tempTable
'
--SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'
SET @InsertCommand = @InsertCommand + 'select id from @InsertedRow'


exec sp_executesql @InsertCommand, 
                             N'@AdathordozoTipus nvarchar(64),@Megjegyzes Nvarchar(400),@SztornirozasDat datetime,@MennyisegiEgyseg nvarchar(64),@Mennyiseg int,@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier'
,@AdathordozoTipus = @AdathordozoTipus,@Megjegyzes = @Megjegyzes,@SztornirozasDat = @SztornirozasDat,@MennyisegiEgyseg = @MennyisegiEgyseg,@Mennyiseg = @Mennyiseg,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN

	/* History Log */
	declare @row_ids varchar(MAX)
	/*** EREC_Mellekletek history log ***/

	set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from EREC_Mellekletek where Tranz_id = @Tranz_id FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
		set @row_ids = @row_ids + '''';

	exec sp_LogRecordsToHistory_Tomeges
	 @TableName = 'EREC_Mellekletek'
	,@Row_Ids = @row_ids
	,@Muvelet = 0
	,@Vegrehajto_Id = @Letrehozo_id
	,@VegrehajtasIdo = @LetrehozasIdo

END

drop table #tempTable            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH



GO
