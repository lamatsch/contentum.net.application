IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Obj_MetaAdataiGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_Obj_MetaAdataiGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Obj_MetaAdataiGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_Obj_MetaAdataiGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_Obj_MetaAdataiGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_Obj_MetaAdatai.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_Obj_MetaAdatai.Id,
	   EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id,
	   EREC_Obj_MetaAdatai.Targyszavak_Id,
	   EREC_Obj_MetaAdatai.AlapertelmezettErtek,
	   EREC_Obj_MetaAdatai.Sorszam,
	   EREC_Obj_MetaAdatai.Opcionalis,
	   EREC_Obj_MetaAdatai.Ismetlodo,
	   EREC_Obj_MetaAdatai.Funkcio,
	   EREC_Obj_MetaAdatai.ControlTypeSource,
	   EREC_Obj_MetaAdatai.ControlTypeDataSource,
	   EREC_Obj_MetaAdatai.SPSSzinkronizalt,
	   EREC_Obj_MetaAdatai.SPS_Field_Id,
	   EREC_Obj_MetaAdatai.Ver,
	   EREC_Obj_MetaAdatai.Note,
	   EREC_Obj_MetaAdatai.Stat_id,
	   EREC_Obj_MetaAdatai.ErvKezd,
	   EREC_Obj_MetaAdatai.ErvVege,
	   EREC_Obj_MetaAdatai.Letrehozo_Id,
	   EREC_Obj_MetaAdatai.LetrehozasIdo,
	   EREC_Obj_MetaAdatai.Modosito_Id,
	   EREC_Obj_MetaAdatai.ModositasIdo,
	   EREC_Obj_MetaAdatai.Zarolo_Id,
	   EREC_Obj_MetaAdatai.ZarolasIdo,
	   EREC_Obj_MetaAdatai.Tranz_Id,
	   EREC_Obj_MetaAdatai.UIAccessLog_Id  
   from 
     EREC_Obj_MetaAdatai as EREC_Obj_MetaAdatai      
   '

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
