IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Obj_MetaAdataiGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_Obj_MetaAdataiGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Obj_MetaAdataiGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_Obj_MetaAdataiGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_Obj_MetaAdataiGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_Obj_MetaAdatai.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null,
  @Obj_MetaDefinicio_Id uniqueidentifier = null,
  @CsakSajatSzint char(1) = '1' -- ha 1, csak az adott metadefiníció leszármazottai, ha 0, a metadefiníció feletteseinek leszármazottai is

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
  declare @kcs_OBJMETADEFINICIO_TIPUS nvarchar(100)
  set @kcs_OBJMETADEFINICIO_TIPUS = 'OBJMETADEFINICIO_TIPUS'

  if @Obj_MetaDefinicio_Id is not null
  begin
      if @CsakSajatSzint = '0'
      begin
          set @sqlcmd = 'select Id, Szint into #Obj_MetaDefiniciok from dbo.fn_GetObjMetaDefinicioFelettesekById(''' + convert(nvarchar(36), @Obj_MetaDefinicio_Id) + ''');'
      end
      else
      begin
          set @sqlcmd = 'select ''' + convert(nvarchar(36), @Obj_MetaDefinicio_Id) + ''' as Id, 0 as Szint into #Obj_MetaDefiniciok;'
      end
  end
  else
  begin
      set @sqlcmd = ''
  end
      
  SET @sqlcmd = @sqlcmd + '
   select ' + @LocalTopRow + '
  	   EREC_Obj_MetaAdatai.Id,
  	   EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id,
       EREC_Obj_MetaDefinicio.ContentType as ContentType,
       EREC_Obj_MetaDefinicio.DefinicioTipus as DefinicioTipus,
       dbo.fn_KodtarErtekNeve(''' + @kcs_OBJMETADEFINICIO_TIPUS + ''', EREC_Obj_MetaDefinicio.DefinicioTipus,@Org) as DefinicioTipus_Nev,
	   EREC_Obj_MetaAdatai.Targyszavak_Id,
       EREC_Obj_MetaAdatai.AlapertelmezettErtek,
case IsNull(EREC_Obj_MetaAdatai.ControlTypeSource, EREC_TargySzavak.ControlTypeSource)
when ''~/Component/EditablePartnerTextBox.ascx'' then p.Nev 
when ''~/Component/KodTarakDropDownList.ascx'' then kt.Nev
else IsNull(EREC_Obj_MetaAdatai.AlapertelmezettErtek, EREC_TargySzavak.AlapertelmezettErtek) end as AlapertelmezettErtek_Lekepezett,
	   EREC_Obj_MetaAdatai.Sorszam,
	   EREC_Obj_MetaAdatai.Opcionalis,
	   EREC_Obj_MetaAdatai.Ismetlodo,
	   EREC_Obj_MetaAdatai.Funkcio,
	   EREC_Obj_MetaAdatai.ControlTypeSource,
	   EREC_Obj_MetaAdatai.ControlTypeDataSource,
IsNull(EREC_Obj_MetaAdatai.ControlTypeSource, EREC_TargySzavak.ControlTypeSource) as ControlTypeSource_Lekepezett,
IsNull(EREC_Obj_MetaAdatai.ControlTypeDataSource, EREC_TargySzavak.ControlTypeDataSource) as ControlTypeDataSource_Lekepezett,
	   EREC_Obj_MetaAdatai.SPSSzinkronizalt,
	   EREC_Obj_MetaAdatai.SPS_Field_Id,
	   EREC_Obj_MetaAdatai.Ver,
	   EREC_Obj_MetaAdatai.Note,
	   EREC_Obj_MetaAdatai.Stat_id,
	   EREC_Obj_MetaAdatai.ErvKezd,
	   EREC_Obj_MetaAdatai.ErvVege,
	   EREC_Obj_MetaAdatai.Letrehozo_Id,
	   EREC_Obj_MetaAdatai.LetrehozasIdo,
	   EREC_Obj_MetaAdatai.Modosito_Id,
	   EREC_Obj_MetaAdatai.ModositasIdo,
	   EREC_Obj_MetaAdatai.Zarolo_Id,
	   EREC_Obj_MetaAdatai.ZarolasIdo,
	   EREC_Obj_MetaAdatai.Tranz_Id,
	   EREC_Obj_MetaAdatai.UIAccessLog_Id,
	   EREC_TargySzavak.TargySzavak as TargySzavak,
	   EREC_TargySzavak.Csoport_Id_Tulaj as Csoport_Id_Tulaj,
	   EREC_TargySzavak.Tipus,
       '
    if @Obj_MetaDefinicio_Id is not null
    begin
        set @sqlcmd = @sqlcmd +  '#Obj_MetaDefiniciok.Szint as Szint
    '
    end
    else
    begin
        set @sqlcmd = @sqlcmd +  '0 as Szint
    '
    end

    set @sqlcmd = @sqlcmd + 'from 
     EREC_Obj_MetaAdatai as EREC_Obj_MetaAdatai
	 LEFT JOIN EREC_TargySzavak as EREC_TargySzavak
	 ON EREC_Obj_MetaAdatai.Targyszavak_Id = EREC_TargySzavak.Id
     LEFT JOIN EREC_Obj_MetaDefinicio as EREC_Obj_MetaDefinicio
     ON EREC_Obj_MetaDefinicio.Id = EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id
     '
    if @Obj_MetaDefinicio_Id is not null
    begin
        set @sqlcmd = @sqlcmd +  'JOIN #Obj_MetaDefiniciok
     ON #Obj_MetaDefiniciok.Id = EREC_Obj_MetaDefinicio.Id
    '
    end
      
	set @sqlcmd = @sqlcmd + 'left join KRT_Partnerek p on convert(varchar(36), p.Id) = IsNull(EREC_Obj_MetaAdatai.AlapertelmezettErtek, EREC_TargySzavak.AlapertelmezettErtek)
left join KRT_KodTarak kt on kt.Kod=IsNull(EREC_Obj_MetaAdatai.AlapertelmezettErtek, EREC_TargySzavak.AlapertelmezettErtek) collate Hungarian_CS_AS
		and kt.KodCsoport_Id = (select top 1 Id from KRT_KodCsoportok
				where Kod=IsNull(EREC_Obj_MetaAdatai.ControlTypeDataSource, EREC_TargySzavak.ControlTypeDataSource) collate Hungarian_CS_AS
				and getdate() between ErvKezd and ErvVege)
		and kt.Org=@Org
		and getdate() between kt.ErvKezd and kt.ErvVege      
       Where EREC_TargySzavak.Org=@Org
			and EREC_Obj_MetaDefinicio.Org=@Org
'

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
---- nyomtatási blokk kezdete
--   if exists (select 1 from  sysobjects where id = object_id('sp_PrintText') and type = 'P')
--   begin
--       exec dbo.sp_PrintText @sqlcmd;
--   end
---- nyomtatási blokk vége

--   exec (@sqlcmd);
-- Hozzáadjuk az @Org azonosítót is:
   execute sp_executesql @sqlcmd,N'@ExecutorUserId UNIQUEIDENTIFIER, @Org UNIQUEIDENTIFIER',
		@ExecutorUserId = @ExecutorUserId, @Org = @Org;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
