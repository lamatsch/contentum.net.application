IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Obj_MetaAdataiInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_Obj_MetaAdataiInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Obj_MetaAdataiInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_Obj_MetaAdataiInsert] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_Obj_MetaAdataiInsert]    
                @Id      uniqueidentifier = null,    
                               @Obj_MetaDefinicio_Id     uniqueidentifier  = null,
                @Targyszavak_Id     uniqueidentifier  = null,
                @AlapertelmezettErtek     Nvarchar(100)  = null,
	            @Sorszam     int,
                @Opcionalis     char(1)  = null,
                @Ismetlodo     char(1)  = null,
                @Funkcio     Nvarchar(100)  = null,
                @ControlTypeSource     nvarchar(64)  = null,
                @ControlTypeDataSource     Nvarchar(4000)  = null,
                @SPSSzinkronizalt     char(1)  = null,
                @SPS_Field_Id     uniqueidentifier  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_Id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_Id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_Id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_Id     uniqueidentifier  = null,
                @UIAccessLog_Id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Obj_MetaDefinicio_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Obj_MetaDefinicio_Id'
            SET @insertValues = @insertValues + ',@Obj_MetaDefinicio_Id'
         end 
       
         if @Targyszavak_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Targyszavak_Id'
            SET @insertValues = @insertValues + ',@Targyszavak_Id'
         end 
       
         if @AlapertelmezettErtek is not null
         begin
            SET @insertColumns = @insertColumns + ',AlapertelmezettErtek'
            SET @insertValues = @insertValues + ',@AlapertelmezettErtek'
         end 
       
         if @Sorszam is not null
         begin
            SET @insertColumns = @insertColumns + ',Sorszam'
            SET @insertValues = @insertValues + ',@Sorszam'
         end 
       
         if @Opcionalis is not null
         begin
            SET @insertColumns = @insertColumns + ',Opcionalis'
            SET @insertValues = @insertValues + ',@Opcionalis'
         end 
       
         if @Ismetlodo is not null
         begin
            SET @insertColumns = @insertColumns + ',Ismetlodo'
            SET @insertValues = @insertValues + ',@Ismetlodo'
         end 
       
         if @Funkcio is not null
         begin
            SET @insertColumns = @insertColumns + ',Funkcio'
            SET @insertValues = @insertValues + ',@Funkcio'
         end 
       
         if @ControlTypeSource is not null
         begin
            SET @insertColumns = @insertColumns + ',ControlTypeSource'
            SET @insertValues = @insertValues + ',@ControlTypeSource'
         end 
       
         if @ControlTypeDataSource is not null
         begin
            SET @insertColumns = @insertColumns + ',ControlTypeDataSource'
            SET @insertValues = @insertValues + ',@ControlTypeDataSource'
         end 
       
         if @SPSSzinkronizalt is not null
         begin
            SET @insertColumns = @insertColumns + ',SPSSzinkronizalt'
            SET @insertValues = @insertValues + ',@SPSSzinkronizalt'
         end 
       
         if @SPS_Field_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',SPS_Field_Id'
            SET @insertValues = @insertValues + ',@SPS_Field_Id'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_Id'
            SET @insertValues = @insertValues + ',@Letrehozo_Id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_Id'
            SET @insertValues = @insertValues + ',@Modosito_Id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_Id'
            SET @insertValues = @insertValues + ',@Zarolo_Id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_Id'
            SET @insertValues = @insertValues + ',@Tranz_Id'
         end 
       
         if @UIAccessLog_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_Id'
            SET @insertValues = @insertValues + ',@UIAccessLog_Id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_Obj_MetaAdatai ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Obj_MetaDefinicio_Id uniqueidentifier,@Targyszavak_Id uniqueidentifier,@AlapertelmezettErtek Nvarchar(100),@Sorszam int,@Opcionalis char(1),@Ismetlodo char(1),@Funkcio Nvarchar(100),@ControlTypeSource nvarchar(64),@ControlTypeDataSource Nvarchar(4000),@SPSSzinkronizalt char(1),@SPS_Field_Id uniqueidentifier,@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_Id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_Id uniqueidentifier,@ModositasIdo datetime,@Zarolo_Id uniqueidentifier,@ZarolasIdo datetime,@Tranz_Id uniqueidentifier,@UIAccessLog_Id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id,@Targyszavak_Id = @Targyszavak_Id,@AlapertelmezettErtek = @AlapertelmezettErtek,@Sorszam = @Sorszam,@Opcionalis = @Opcionalis,@Ismetlodo = @Ismetlodo,@Funkcio = @Funkcio,@ControlTypeSource = @ControlTypeSource,@ControlTypeDataSource = @ControlTypeDataSource,@SPSSzinkronizalt = @SPSSzinkronizalt,@SPS_Field_Id = @SPS_Field_Id,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_Id = @Letrehozo_Id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_Id = @Modosito_Id,@ModositasIdo = @ModositasIdo,@Zarolo_Id = @Zarolo_Id,@ZarolasIdo = @ZarolasIdo,@Tranz_Id = @Tranz_Id,@UIAccessLog_Id = @UIAccessLog_Id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_Obj_MetaAdatai',@ResultUid
					,'EREC_Obj_MetaAdataiHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
