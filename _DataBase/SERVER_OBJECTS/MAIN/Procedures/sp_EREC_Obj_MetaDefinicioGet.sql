IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Obj_MetaDefinicioGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_Obj_MetaDefinicioGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Obj_MetaDefinicioGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_Obj_MetaDefinicioGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_Obj_MetaDefinicioGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_Obj_MetaDefinicio.Id,
	   EREC_Obj_MetaDefinicio.Org,
	   EREC_Obj_MetaDefinicio.Objtip_Id,
	   EREC_Obj_MetaDefinicio.Objtip_Id_Column,
	   EREC_Obj_MetaDefinicio.ColumnValue,
	   EREC_Obj_MetaDefinicio.DefinicioTipus,
	   EREC_Obj_MetaDefinicio.Felettes_Obj_Meta,
	   EREC_Obj_MetaDefinicio.MetaXSD,
	   EREC_Obj_MetaDefinicio.ImportXML,
	   EREC_Obj_MetaDefinicio.EgyebXML,
	   EREC_Obj_MetaDefinicio.ContentType,
	   EREC_Obj_MetaDefinicio.SPSSzinkronizalt,
	   EREC_Obj_MetaDefinicio.SPS_CTT_Id,
	   EREC_Obj_MetaDefinicio.WorkFlowVezerles,
	   EREC_Obj_MetaDefinicio.Ver,
	   EREC_Obj_MetaDefinicio.Note,
	   EREC_Obj_MetaDefinicio.Stat_id,
	   EREC_Obj_MetaDefinicio.ErvKezd,
	   EREC_Obj_MetaDefinicio.ErvVege,
	   EREC_Obj_MetaDefinicio.Letrehozo_id,
	   EREC_Obj_MetaDefinicio.LetrehozasIdo,
	   EREC_Obj_MetaDefinicio.Modosito_id,
	   EREC_Obj_MetaDefinicio.ModositasIdo,
	   EREC_Obj_MetaDefinicio.Zarolo_id,
	   EREC_Obj_MetaDefinicio.ZarolasIdo,
	   EREC_Obj_MetaDefinicio.Tranz_id,
	   EREC_Obj_MetaDefinicio.UIAccessLog_id
	   from 
		 EREC_Obj_MetaDefinicio as EREC_Obj_MetaDefinicio 
	   where
		 EREC_Obj_MetaDefinicio.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
