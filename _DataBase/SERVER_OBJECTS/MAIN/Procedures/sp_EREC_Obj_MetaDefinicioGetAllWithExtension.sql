IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Obj_MetaDefinicioGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_Obj_MetaDefinicioGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Obj_MetaDefinicioGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_Obj_MetaDefinicioGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_Obj_MetaDefinicioGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_Obj_MetaDefinicio.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null,
  @NoAncestorLoopWithId uniqueidentifier = null, -- ha meg van adva Id, akkor kizárja mindazon rekordokat,
                                        -- melyek körképzés miatt nem lehetnek felettesei az Id-vel
                                        -- megadott rekordnak
  @NoSuccessorLoopWithId uniqueidentifier = null -- ha meg van adva Id, akkor kizárja mindazon rekordokat,
                                        -- melyek körképzés miatt nem lehetnek alárendeltjei az Id-vel
                                        -- megadott rekordnak

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  declare @kcs_OBJMETADEFINICIO_TIPUS nvarchar(100)
  set @kcs_OBJMETADEFINICIO_TIPUS = 'OBJMETADEFINICIO_TIPUS'

  declare @IdsCausingLoop nvarchar(4000)

  SET @sqlcmd = ''

  if @NoAncestorLoopWithId is not null
  begin
      -- leszármazottak meghatározása
      set @sqlcmd = @sqlcmd + '
      declare @SzintAncestor int
      set @SzintAncestor = 0
      select Id, @SzintAncestor as Szint into #temp_IdsCausingAncestorLoop
      from EREC_Obj_MetaDefinicio
      where Id = ''' + convert(nvarchar(36), @NoAncestorLoopWithId) + '''
    and EREC_Obj_MetaDefinicio.Org=''' + CAST(@Org as Nvarchar(40)) + '''
      and getdate() between ErvKezd and ErvVege

      while @@rowcount > 0
      begin
          set @SzintAncestor = @SzintAncestor + 1
          insert into #temp_IdsCausingAncestorLoop
          select Id, @SzintAncestor as Szint from EREC_Obj_MetaDefinicio
          where getdate() between ErvKezd and ErvVege
		and EREC_Obj_MetaDefinicio.Org=''' + CAST(@Org as Nvarchar(40)) + '''
          and Felettes_Obj_Meta in (select Id from #temp_IdsCausingAncestorLoop
                                    where Szint = @SzintAncestor - 1)
          and Id not in (select Id from #temp_IdsCausingAncestorLoop)
      end'
  end

  if @NoSuccessorLoopWithId is not null
  begin
      -- leszármazottak és felettesek meghatározása
      set @sqlcmd = @sqlcmd + '
      declare @SzintSuccessor int
      set @SzintSuccessor = 0
      select Id, @SzintSuccessor as Szint into #temp_IdsCausingSuccessorLoop
      from EREC_Obj_MetaDefinicio
      where Id = ''' + convert(nvarchar(36), @NoSuccessorLoopWithId) + '''
      and getdate() between ErvKezd and ErvVege

      while @@rowcount > 0
      begin
          set @SzintSuccessor = @SzintSuccessor + 1
          insert into #temp_IdsCausingSuccessorLoop
          select Id, @SzintSuccessor as Szint from EREC_Obj_MetaDefinicio
          where getdate() between ErvKezd and ErvVege
		and EREC_Obj_MetaDefinicio.Org=''' + CAST(@Org as Nvarchar(40)) + '''
          and Felettes_Obj_Meta in (select Id from #temp_IdsCausingSuccessorLoop
                                    where Szint = @SzintSuccessor - 1)
          and Id not in (select Id from #temp_IdsCausingSuccessorLoop)
          UNION
          select EREC_Obj_MetaDefinicio.Felettes_Obj_Meta as Id, @SzintSuccessor as Szint from EREC_Obj_MetaDefinicio
          where getdate() between EREC_Obj_MetaDefinicio.ErvKezd and EREC_Obj_MetaDefinicio.ErvVege
		and EREC_Obj_MetaDefinicio.Org=''' + CAST(@Org as Nvarchar(40)) + '''
          and EREC_Obj_MetaDefinicio.Felettes_Obj_Meta is not null
          and EREC_Obj_MetaDefinicio.Id in (select Id from #temp_IdsCausingSuccessorLoop
                                    where Szint = @SzintSuccessor - 1)
          and EREC_Obj_MetaDefinicio.Felettes_Obj_Meta not in (select Id from #temp_IdsCausingSuccessorLoop)
      end'
  end
      
          
  SET @sqlcmd = @sqlcmd + '
   select ' + @LocalTopRow + '
  	   EREC_Obj_MetaDefinicio.Id,
	   EREC_Obj_MetaDefinicio.Objtip_Id,
	   IsNull((select Kod from KRT_ObjTipusok where Id = EREC_Obj_MetaDefinicio.Objtip_Id), '''') as ObjTipusok_Tabla,
	   EREC_Obj_MetaDefinicio.Objtip_Id_Column,
	   IsNull((select Kod from KRT_ObjTipusok where Id = EREC_Obj_MetaDefinicio.Objtip_Id_Column), '''') as ObjTipusok_Oszlop,
	   EREC_Obj_MetaDefinicio.ColumnValue,
       IsNull(dbo.fn_mapcolumnvalue_bymetadefinicio(EREC_Obj_MetaDefinicio.Id,''' + cast(@Org as NVarChar(40)) + '''), '''') as ColumnValue_Lekepezett,
	   EREC_Obj_MetaDefinicio.DefinicioTipus,
       IsNull(dbo.fn_KodtarErtekNeve(''' + @kcs_OBJMETADEFINICIO_TIPUS + ''', EREC_Obj_MetaDefinicio.DefinicioTipus,''' + cast(@Org as NVarChar(40))+ '''), '''') as DefinicioTipus_Nev,
	   EREC_Obj_MetaDefinicio.Felettes_Obj_Meta,
	   IsNull((select omd.ContentType from EREC_Obj_MetaDefinicio omd where omd.Id = EREC_Obj_MetaDefinicio.Felettes_Obj_Meta), '''') as Felettes_Obj_Meta_CTT,
	   EREC_Obj_MetaDefinicio.MetaXSD,
	   EREC_Obj_MetaDefinicio.ImportXML,
	   EREC_Obj_MetaDefinicio.EgyebXML,
	   EREC_Obj_MetaDefinicio.ContentType,
	   EREC_Obj_MetaDefinicio.SPSSzinkronizalt,
	   EREC_Obj_MetaDefinicio.SPS_CTT_Id,
	   EREC_Obj_MetaDefinicio.Ver,
	   EREC_Obj_MetaDefinicio.Note,
	   EREC_Obj_MetaDefinicio.Stat_id,
	   EREC_Obj_MetaDefinicio.ErvKezd,
	   EREC_Obj_MetaDefinicio.ErvVege,
	   EREC_Obj_MetaDefinicio.Letrehozo_id,
	   EREC_Obj_MetaDefinicio.LetrehozasIdo,
	   EREC_Obj_MetaDefinicio.Modosito_id,
	   EREC_Obj_MetaDefinicio.ModositasIdo,
	   EREC_Obj_MetaDefinicio.Zarolo_id,
	   EREC_Obj_MetaDefinicio.ZarolasIdo,
	   EREC_Obj_MetaDefinicio.Tranz_id,
	   EREC_Obj_MetaDefinicio.UIAccessLog_id  
   from 
     EREC_Obj_MetaDefinicio as EREC_Obj_MetaDefinicio
    Where EREC_Obj_MetaDefinicio.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
    if @NoAncestorLoopWithId is not null
    begin
        SET @sqlcmd = @sqlcmd + ' and EREC_Obj_MetaDefinicio.Id not in (select Id from #temp_IdsCausingAncestorLoop) '
    end

    if @NoSuccessorLoopWithId is not null
    begin
        SET @sqlcmd = @sqlcmd + ' and EREC_Obj_MetaDefinicio.Id not in (select Id from #temp_IdsCausingSuccessorLoop) '
    end
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
--print @sqlcmd;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
