IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Obj_MetaDefinicioHistoryGetAllRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_Obj_MetaDefinicioHistoryGetAllRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Obj_MetaDefinicioHistoryGetAllRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_Obj_MetaDefinicioHistoryGetAllRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_Obj_MetaDefinicioHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_Obj_MetaDefinicioHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Org' as ColumnName,               cast(Old.Org as nvarchar(99)) as OldValue,
               cast(New.Org as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Org != New.Org 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Objtip_Id' as ColumnName,               cast(Old.Objtip_Id as nvarchar(99)) as OldValue,
               cast(New.Objtip_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Objtip_Id != New.Objtip_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Objtip_Id_Column' as ColumnName,               cast(Old.Objtip_Id_Column as nvarchar(99)) as OldValue,
               cast(New.Objtip_Id_Column as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Objtip_Id_Column != New.Objtip_Id_Column 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ColumnValue' as ColumnName,               cast(Old.ColumnValue as nvarchar(99)) as OldValue,
               cast(New.ColumnValue as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ColumnValue != New.ColumnValue 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DefinicioTipus' as ColumnName,               cast(Old.DefinicioTipus as nvarchar(99)) as OldValue,
               cast(New.DefinicioTipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.DefinicioTipus != New.DefinicioTipus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felettes_Obj_Meta' as ColumnName,               cast(Old.Felettes_Obj_Meta as nvarchar(99)) as OldValue,
               cast(New.Felettes_Obj_Meta as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Felettes_Obj_Meta != New.Felettes_Obj_Meta 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
--      union all 
--      (select  New.HistoryId as RowId, 
--               New.Ver as Ver, 
--               case New.HistoryMuvelet_Id 
--                  when 1 then 'Módosítás'
--                  when 2 then 'Érvénytelenítés'
--               end as Operation, 
--               'MetaXSD' as ColumnName,               cast(Old.MetaXSD as nvarchar(99)) as OldValue,
--               cast(New.MetaXSD as nvarchar(99)) as NewValue,               
--               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
--      from EREC_Obj_MetaDefinicioHistory Old
--         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = New.Ver-1
--            and Old.Id = New.Id
--            and Old.MetaXSD != New.MetaXSD 
--            and Old.Id = New.Id
--         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
--)
--            
--      union all 
--      (select  New.HistoryId as RowId, 
--               New.Ver as Ver, 
--               case New.HistoryMuvelet_Id 
--                  when 1 then 'Módosítás'
--                  when 2 then 'Érvénytelenítés'
--               end as Operation, 
--               'ImportXML' as ColumnName,               cast(Old.ImportXML as nvarchar(99)) as OldValue,
--               cast(New.ImportXML as nvarchar(99)) as NewValue,               
--               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
--      from EREC_Obj_MetaDefinicioHistory Old
--         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = New.Ver-1
--            and Old.Id = New.Id
--            and Old.ImportXML != New.ImportXML 
--            and Old.Id = New.Id
--         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
--)
--            
--      union all 
--      (select  New.HistoryId as RowId, 
--               New.Ver as Ver, 
--               case New.HistoryMuvelet_Id 
--                  when 1 then 'Módosítás'
--                  when 2 then 'Érvénytelenítés'
--               end as Operation, 
--               'EgyebXML' as ColumnName,               cast(Old.EgyebXML as nvarchar(99)) as OldValue,
--               cast(New.EgyebXML as nvarchar(99)) as NewValue,               
--               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
--      from EREC_Obj_MetaDefinicioHistory Old
--         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = New.Ver-1
--            and Old.Id = New.Id
--            and Old.EgyebXML != New.EgyebXML 
--            and Old.Id = New.Id
--         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
--)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ContentType' as ColumnName,               cast(Old.ContentType as nvarchar(99)) as OldValue,
               cast(New.ContentType as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ContentType != New.ContentType 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SPSSzinkronizalt' as ColumnName,               cast(Old.SPSSzinkronizalt as nvarchar(99)) as OldValue,
               cast(New.SPSSzinkronizalt as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SPSSzinkronizalt != New.SPSSzinkronizalt 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SPS_CTT_Id' as ColumnName,               cast(Old.SPS_CTT_Id as nvarchar(99)) as OldValue,
               cast(New.SPS_CTT_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SPS_CTT_Id != New.SPS_CTT_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'WorkFlowVezerles' as ColumnName,               cast(Old.WorkFlowVezerles as nvarchar(99)) as OldValue,
               cast(New.WorkFlowVezerles as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.WorkFlowVezerles != New.WorkFlowVezerles 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end


GO
