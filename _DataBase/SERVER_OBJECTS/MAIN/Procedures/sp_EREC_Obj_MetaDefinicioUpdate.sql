IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Obj_MetaDefinicioUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_Obj_MetaDefinicioUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Obj_MetaDefinicioUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_Obj_MetaDefinicioUpdate] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_Obj_MetaDefinicioUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Org     uniqueidentifier  = null,         
             @Objtip_Id     uniqueidentifier  = null,         
             @Objtip_Id_Column     uniqueidentifier  = null,         
             @ColumnValue     Nvarchar(100)  = null,         
             @DefinicioTipus     nvarchar(64)  = null,         
             @Felettes_Obj_Meta     uniqueidentifier  = null,         
             @MetaXSD     xml  = null,         
             @ImportXML     xml  = null,         
             @EgyebXML     xml  = null,         
             @ContentType     Nvarchar(100)  = null,         
             @SPSSzinkronizalt     char(1)  = null,         
             @SPS_CTT_Id     Nvarchar(400)  = null,         
             @WorkFlowVezerles     char(1)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Org     uniqueidentifier         
     DECLARE @Act_Objtip_Id     uniqueidentifier         
     DECLARE @Act_Objtip_Id_Column     uniqueidentifier         
     DECLARE @Act_ColumnValue     Nvarchar(100)         
     DECLARE @Act_DefinicioTipus     nvarchar(64)         
     DECLARE @Act_Felettes_Obj_Meta     uniqueidentifier         
     DECLARE @Act_MetaXSD     xml         
     DECLARE @Act_ImportXML     xml         
     DECLARE @Act_EgyebXML     xml         
     DECLARE @Act_ContentType     Nvarchar(100)         
     DECLARE @Act_SPSSzinkronizalt     char(1)         
     DECLARE @Act_SPS_CTT_Id     Nvarchar(400)         
     DECLARE @Act_WorkFlowVezerles     char(1)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Org = Org,
     @Act_Objtip_Id = Objtip_Id,
     @Act_Objtip_Id_Column = Objtip_Id_Column,
     @Act_ColumnValue = ColumnValue,
     @Act_DefinicioTipus = DefinicioTipus,
     @Act_Felettes_Obj_Meta = Felettes_Obj_Meta,
     @Act_MetaXSD = MetaXSD,
     @Act_ImportXML = ImportXML,
     @Act_EgyebXML = EgyebXML,
     @Act_ContentType = ContentType,
     @Act_SPSSzinkronizalt = SPSSzinkronizalt,
     @Act_SPS_CTT_Id = SPS_CTT_Id,
     @Act_WorkFlowVezerles = WorkFlowVezerles,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_Obj_MetaDefinicio
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Org')=1
         SET @Act_Org = @Org
   IF @UpdatedColumns.exist('/root/Objtip_Id')=1
         SET @Act_Objtip_Id = @Objtip_Id
   IF @UpdatedColumns.exist('/root/Objtip_Id_Column')=1
         SET @Act_Objtip_Id_Column = @Objtip_Id_Column
   IF @UpdatedColumns.exist('/root/ColumnValue')=1
         SET @Act_ColumnValue = @ColumnValue
   IF @UpdatedColumns.exist('/root/DefinicioTipus')=1
         SET @Act_DefinicioTipus = @DefinicioTipus
   IF @UpdatedColumns.exist('/root/Felettes_Obj_Meta')=1
         SET @Act_Felettes_Obj_Meta = @Felettes_Obj_Meta
   IF @UpdatedColumns.exist('/root/MetaXSD')=1
         SET @Act_MetaXSD = @MetaXSD
   IF @UpdatedColumns.exist('/root/ImportXML')=1
         SET @Act_ImportXML = @ImportXML
   IF @UpdatedColumns.exist('/root/EgyebXML')=1
         SET @Act_EgyebXML = @EgyebXML
   IF @UpdatedColumns.exist('/root/ContentType')=1
         SET @Act_ContentType = @ContentType
   IF @UpdatedColumns.exist('/root/SPSSzinkronizalt')=1
         SET @Act_SPSSzinkronizalt = @SPSSzinkronizalt
   IF @UpdatedColumns.exist('/root/SPS_CTT_Id')=1
         SET @Act_SPS_CTT_Id = @SPS_CTT_Id
   IF @UpdatedColumns.exist('/root/WorkFlowVezerles')=1
         SET @Act_WorkFlowVezerles = @WorkFlowVezerles
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_Obj_MetaDefinicio
SET
     Org = @Act_Org,
     Objtip_Id = @Act_Objtip_Id,
     Objtip_Id_Column = @Act_Objtip_Id_Column,
     ColumnValue = @Act_ColumnValue,
     DefinicioTipus = @Act_DefinicioTipus,
     Felettes_Obj_Meta = @Act_Felettes_Obj_Meta,
     MetaXSD = @Act_MetaXSD,
     ImportXML = @Act_ImportXML,
     EgyebXML = @Act_EgyebXML,
     ContentType = @Act_ContentType,
     SPSSzinkronizalt = @Act_SPSSzinkronizalt,
     SPS_CTT_Id = @Act_SPS_CTT_Id,
     WorkFlowVezerles = @Act_WorkFlowVezerles,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_Obj_MetaDefinicio',@Id
					,'EREC_Obj_MetaDefinicioHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
