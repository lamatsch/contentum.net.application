IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiAssignByDefinicioTipus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiAssignByDefinicioTipus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiAssignByDefinicioTipus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiAssignByDefinicioTipus] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_ObjektumTargyszavaiAssignByDefinicioTipus]
  @Obj_Id uniqueidentifier, -- az adott objektum egy rekordjának azonosítója (Id)
  @ObjTip_Id uniqueidentifier = null,
  @ObjTip_Kod nvarchar(100) = '',
  @DefinicioTipus nvarchar(2) = '', -- OBJMETADEFINICIO_TIPUS @DefinicioTipus = 'B1' (jelenleg B1 és C2 használt)
  @CsakSajatSzint varchar(1) = '0',        -- '0' minden szinten hozzárendelés, '1' csak a saját szinten
  @ExecutorUserId uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   declare @kt_Automatikus nvarchar(64)
   set @kt_Automatikus = '01' -- Automatikus (kcs: TARGYSZO_FORRAS)

   -- Dátum
   declare @Now datetime
   set @Now = getdate()

---- struktúra kinyerése a temporary eredménytáblába
--   if OBJECT_ID('tempdb..#temp_ResultTable') is not null
--   begin
--       drop table #temp_ResultTable
--   end
--       select top 0 * into #temp_ResultTable 
--       from EREC_ObjektumTargyszavai


   if @Obj_Id is null
      RAISERROR('[56100]',16,1) -- Nincs megadva az objektum azonosítója!
   if @ObjTip_Id is null AND @ObjTip_Kod is null
      RAISERROR('[56101]',16,1) -- Nincs megadva az objektum típusa (azonosító vagy kód)!

   if (@ObjTip_Id IS null)
   begin
      SET @ObjTip_Id = (select top 1 Id from KRT_ObjTipusok where Kod = @ObjTip_Kod)
      if @ObjTip_Id is null
         RAISERROR('[56102]',16,1) -- Nem azonosítható az objektum típusa (nem létezo kód)!

   end
   else
   begin
     SET @ObjTip_Kod = (select Kod from KRT_ObjTipusok where Id = @ObjTip_Id)
      if @ObjTip_Kod is null
         RAISERROR('[56103]',16,1) -- Nem azonosítható az objektum típusa (nem létezo id)!
   end

-- Objektum metadefiniciok
          declare @Szint int
          set @Szint = 0
          
          -- EREC_Obj_MetaDefinicio tábla szurése típus, objektum típus és érvényesség szerint
          -- ha van column_id, ellenorizzük, hogy létezik-e a táblában az oszlop
              select omd.Id as Id, omd.Felettes_Obj_Meta as Felettes_Obj_Meta, @Szint as Szint into #Obj_MetaDefiniciok
                  from EREC_Obj_MetaDefinicio omd
                  where ((@DefinicioTipus is not null and @DefinicioTipus != '' and omd.DefinicioTipus = @DefinicioTipus)
                         or
                         (@DefinicioTipus is null or @DefinicioTipus = ''))
                  and (Objtip_Id_Column is null
                       or
                       dbo.fn_dbcolumn_value_equals(
                            @Obj_Id,
                            omd.ColumnValue,
                            (select top 1 Kod from KRT_ObjTipusok where Id = @ObjTip_Id),
                            (select top 1 Kod from KRT_ObjTipusok where Id = omd.Objtip_Id_Column)
                          ) = 1
                      )
                  and omd.Objtip_Id = @ObjTip_Id
				and omd.Org=@Org
                  and getdate() between omd.ErvKezd and omd.ErvVege
              
          -- fölérendelt szintek leválogatása
          if @@rowcount > 0 and @CsakSajatSzint = '0'
          begin
              declare @Found char(1)
              set @Found = '1'
              while (@Found = '1')
              begin
                  insert into #Obj_MetaDefiniciok
                      select omd.Id, omd.Felettes_Obj_Meta, @Szint+1 from EREC_Obj_MetaDefinicio omd
                          where omd.Id in (select Felettes_Obj_Meta from #Obj_MetaDefiniciok
                                                              where #Obj_MetaDefiniciok.Szint = @Szint)
						and omd.Org=@Org
                          and getdate() between omd.ErvKezd and omd.ErvVege
                  if @@rowcount = 0
                      set @Found = '0'
                  else
                      set @Szint = @Szint + 1
              end
          end

-- Objektum metaadatai
         select oma.Id, oma.Sorszam, oma.Targyszavak_Id, tsz.TargySzavak, oma.ErvKezd, oma.ErvVege, oma.Modosito_Id, oma.ModositasIdo
             into #Obj_MetaAdatai
             from EREC_Obj_MetaAdatai oma
                  join EREC_TargySzavak tsz
                  on oma.Targyszavak_Id = tsz.Id and tsz.Org=@Org
                  where oma.Obj_MetaDefinicio_Id in (select Id from #Obj_MetaDefiniciok)
                  and oma.Opcionalis = '0'
                  and getdate() between oma.ErvKezd and oma.ErvVege
                  and getdate() between tsz.ErvKezd and tsz.ErvVege

-- Nem létezo hozzárendelések létrehozása
		declare @insertedRowIds table (Id uniqueidentifier)

		insert into EREC_ObjektumTargyszavai
			(Targyszo_Id
			, Obj_Metaadatai_Id
			, Obj_Id
			, ObjTip_Id
			, Targyszo
			, Forras
			, Sorszam
			, Letrehozo_id
			, LetrehozasIdo
			, Modosito_Id
			, ModositasIdo
			, ErvKezd
			, ErvVege)
		output inserted.Id into @insertedRowIds
		select Targyszavak_Id
			, Id -- Obj_Metaadatai_Id
			, @Obj_Id
			, @ObjTip_Id
			, TargySzavak
			, @kt_Automatikus -- Forras
			, Sorszam 
			, @ExecutorUserId -- Letrehozo_Id
			, @Now -- LetrehozasIdo
			, Modosito_Id
			, ModositasIdo
			, ErvKezd
			, ErvVege
		from #Obj_MetaAdatai
		where not exists
		(select 1 from EREC_ObjektumTargyszavai
			   where Obj_Id = #Obj_MetaAdatai.Obj_Id
			   and ObjTip_Id = #Obj_MetaAdatai.ObjTip_Id
			   and Obj_Metaadatai_Id = #Obj_MetaAdatai.Obj_Metaadatai_Id
			   and Forras = @kt_Automatikus
			   and getdate() between ErvKezd and ErvVege)


		/* dinamikus history log összeállításhoz */
		declare @row_ids varchar(MAX)

		/*** EREC_ObjektumTargyszavaiHistory log ***/
		set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @insertedRowIds FOR XML PATH('')),1, 2, '')
		if @row_ids is not null
			set @row_ids = @row_ids + '''';

		exec [sp_LogRecordsToHistory_Tomeges] 
				 @TableName = 'EREC_ObjektumTargyszavai'
				,@Row_Ids = @row_ids
				,@Muvelet = 0
				,@Vegrehajto_Id = @ExecutorUserId
				,@VegrehajtasIdo = @Now

-- új rekordok visszaadása
      select * from EREC_ObjektumTargyszavai
		where Id in (select Id from @insertedRowIds)
                 
---- Nem létezo hozzárendelések létrehozása
--    -- valtozok deklaralasa
--    declare  @Obj_Metaadatai_Id uniqueidentifier,
--             @Sorszam int,
--             @Targyszavak_Id uniqueidentifier,
--             @TargySzavak nvarchar(100),
--             @ErvKezd datetime,
--             @ErvVege datetime,
--             @Modosito_Id uniqueidentifier,
--             @ModositasIdo datetime,
--             @currentRecordId uniqueidentifier
--        -- cursor deklaralasa
--        declare objmetaadatai_sor cursor local for
--                select Id, Sorszam, Targyszavak_Id, TargySzavak, ErvKezd, ErvVege, Modosito_Id, ModositasIdo from #Obj_MetaAdatai
--        open objmetaadatai_sor
--
--        -- ciklus kezdete elotti fetch
--        fetch objmetaadatai_sor into @Obj_Metaadatai_Id, @Sorszam, @Targyszavak_Id, @TargySzavak, @ErvKezd, @ErvVege, @Modosito_Id, @ModositasIdo
--
--        while @@Fetch_Status = 0
--        begin
--           if (select Id from EREC_ObjektumTargyszavai
--                   where Obj_Id = @Obj_Id
--                   and ObjTip_Id = @ObjTip_Id
--                   and Obj_Metaadatai_Id = @Obj_Metaadatai_Id
--                   and Forras = @kt_Automatikus
--                   and getdate() between ErvKezd and ErvVege
--               ) is null
--           begin
--               exec sp_EREC_ObjektumTargyszavaiInsert
--                   @Targyszo_Id = @Targyszavak_Id,
--                   @Obj_Metaadatai_Id = @Obj_Metaadatai_Id,
--                   @Obj_Id = @Obj_Id,
--                   @ObjTip_Id = @ObjTip_Id,
--                   @Targyszo = @TargySzavak,
--                   @Forras = @kt_Automatikus,
--                   @Sorszam = @Sorszam,
--                   @Letrehozo_id = @ExecutorUserId,
--                   @LetrehozasIdo = @Now,
--                   @Modosito_Id = @Modosito_Id,
--                   @ModositasIdo = @ModositasIdo,
--                   @ErvKezd = @ErvKezd,
--                   @ErvVege = @ErvVege,
--                   @ResultUid = @currentRecordId OUTPUT
--
--                   if (@@error <> 0)
--                      RAISERROR('[56110]',16,1) -- Hiba történt az automatikus metaadat hozzárendelés során!
--                   else
--                   begin
--                       insert into #temp_ResultTable
--                           select * from EREC_ObjektumTargyszavai ot
--                               where ot.Id = @currentRecordId
--                   end
--           end
--           fetch objmetaadatai_sor into @Obj_Metaadatai_Id, @Sorszam, @Targyszavak_Id, @TargySzavak, @ErvKezd, @ErvVege, @Modosito_Id, @ModositasIdo
--       end
--
--       close objmetaadatai_sor
--       deallocate objmetaadatai_sor
--
---- új rekordok visszaadása
--      select * from #temp_ResultTable
---- takarítás
--        if OBJECT_ID('tempdb..#temp_ResultTable') is not null 
--        begin
--           drop table #temp_ResultTable
--        end 


END TRY
BEGIN CATCH
        DECLARE @errorSeverity INT, @errorState INT
        DECLARE @errorCode NVARCHAR(1000)    
        SET @errorSeverity = ERROR_SEVERITY()
        SET @errorState = ERROR_STATE()
        
        if ERROR_NUMBER()<50000        
                SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
        else
                SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1
   
    

        RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
