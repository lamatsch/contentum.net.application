IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiAssignToIratByUgyUgyiratok]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiAssignToIratByUgyUgyiratok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiAssignToIratByUgyUgyiratok]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiAssignToIratByUgyUgyiratok] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_ObjektumTargyszavaiAssignToIratByUgyUgyiratok]
  @Obj_Id uniqueidentifier,
  @Targyszo_Ids nvarchar(4000) = null, -- az atmasolando targyszavak szuresehez
  @ExecutorUserId uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	-- Dátum
	DECLARE @Now datetime
	SET @Now = getdate()

   -- Hibanyilvántartás
   DECLARE @Hiba int
   SET @Hiba = 0

   if @Obj_Id IS null
      RAISERROR('Nincs megadva az objektum azonosítója!',16,1)

	if (SELECT Id FROM EREC_IraIratok WHERE Id = @Obj_Id AND ErvKezd < getdate() AND ErvVege > getdate()) IS NULL
		RAISERROR('Nem létezik az irat!',16,1)

    DECLARE @Irat_ObjTip_Id uniqueidentifier
    SET @Irat_ObjTip_Id = (SELECT Id FROM KRT_ObjTipusok WHERE Kod='EREC_IraIratok')
    IF @Irat_ObjTip_Id IS NULL
        RAISERROR('Nem található az irat objektum típus azonosítója!',16,1)

    DECLARE @ForrasUgyiratAuto nvarchar(64)
    SET @ForrasUgyiratAuto = '06' -- Ügyirat (auto)

-- struktúra kinyerése a temporary eredménytáblába
	IF OBJECT_ID('tempdb..#temp_ResultTable') IS NOT NULL 
	BEGIN
	   DROP TABLE #temp_ResultTable
	END
	SELECT TOP 0 * into #temp_ResultTable 
	from EREC_ObjektumTargyszavai


-- temporary tábla a tárgyszavak id alapján való szurésére
	IF OBJECT_ID('tempdb..#temp_FilterTable') IS NOT NULL 
	BEGIN
	   DROP TABLE #temp_FilterTable
	END
-- csak a Targyszo_Id oszlop létrehozása
    SELECT TOP 0 Targyszo_Id into #temp_FilterTable 
	from EREC_ObjektumTargyszavai

	DECLARE @sqlcmd_filter nvarchar(max)
	SET @sqlcmd_filter = 'select distinct Targyszo_Id from EREC_ObjektumTargyszavai'
	if @Targyszo_Ids is not null and @Targyszo_Ids != ''
	BEGIN
		SET @sqlcmd_filter = @sqlcmd_filter + ' where Targyszo_Id IN (' + @Targyszo_Ids + ')'
	END
	INSERT INTO #temp_FilterTable (
		Targyszo_Id
    ) EXEC(@sqlcmd_filter)

-- EREC_UgyUgyirat rekord(ok) meghatározása, ill. az ehhez tartozó tárgyszó hozzárendelések

	-- cursor deklaralasa
	DECLARE objektumtargyszavai_sor CURSOR LOCAL FOR
	SELECT Targyszo_Id, Obj_Metaadatai_id, Targyszo,
           Forras, Ertek, Sorszam, Note, ErvKezd, ErvVege,
           Letrehozo_id, LetrehozasIdo, Modosito_Id, ModositasIdo 
	FROM EREC_ObjektumTargyszavai ot
    WHERE ot.Obj_Id IN (
        SELECT u.Id 
		  FROM EREC_IraIratok i
		   JOIN EREC_UgyUgyiratdarabok ud
		   ON i.UgyUgyiratDarab_Id = ud.Id
		   JOIN EREC_UgyUgyiratok u
		   ON ud.UgyUgyirat_Id = u.Id
		WHERE i.Id = @Obj_Id
		AND u.ErvKezd < getdate() AND u.ErvVege > getdate()
		AND i.ErvKezd < getdate() AND i.ErvVege > getdate()
		AND ud.ErvKezd < getdate() AND ud.ErvVege > getdate()
        )
        AND ot.ErvKezd < getdate() AND ot.ErvVege > getdate()
		AND ot.Targyszo_Id IS NOT NULL -- a kéziek nem öröklodnek, a szervezeti és autok igen
        AND ot.Targyszo_Id IN (SELECT Targyszo_Id FROM #temp_FilterTable)

-- Hozzárendelések
	DECLARE  @Targyszo_Id uniqueidentifier,
			 @Obj_Metaadatai_id uniqueidentifier,
             @ObjTip_Id uniqueidentifier,
             @Targyszo nvarchar(100),
             @Forras nvarchar(64),
             @Ertek nvarchar(100),
             @Sorszam int,
             @Note nvarchar(4000),
             @ErvKezd datetime,
             @ErvVege datetime,
             @Letrehozo_id uniqueidentifier,
             @LetrehozasIdo datetime,
             @Modosito_Id uniqueidentifier,
             @ModositasIdo datetime,
             @currentRecordId uniqueidentifier

    DECLARE @ObjektumTargyszavaiRecordId uniqueidentifier,
            @ObjektumTargyszavaiRecordVer int

	OPEN objektumtargyszavai_sor

	-- ciklus kezdete elotti fetch
	FETCH objektumtargyszavai_sor INTO @Targyszo_Id, @Obj_Metaadatai_id,
           @Targyszo, @Forras, @Ertek, @Sorszam, @Note, @ErvKezd, @ErvVege,
           @Letrehozo_id, @LetrehozasIdo, @Modosito_Id, @ModositasIdo 

	WHILE @@Fetch_Status = 0
		BEGIN
           SET @ObjektumTargyszavaiRecordId = (
             SELECT TOP 1 Id FROM EREC_ObjektumTargyszavai ot
               WHERE ot.Targyszo_Id = @Targyszo_Id AND ot.Targyszo_Id IS NOT NULL
                   AND (ot.Obj_Metaadatai_Id = @Obj_Metaadatai_id OR ot.Obj_Metaadatai_Id IS NULL AND @Obj_Metaadatai_id IS NULL)
                   AND ot.Obj_Id = @Obj_Id
                   AND ot.ObjTip_Id = @Irat_ObjTip_Id
                   AND ot.ErvKezd < getdate() AND ot.ErvVege > getdate()
             )

           IF @ObjektumTargyszavaiRecordId IS NULL
           BEGIN
			   exec sp_EREC_ObjektumTargyszavaiInsert
				   @Targyszo_Id = @Targyszo_Id,
				   @Obj_Metaadatai_Id = @Obj_Metaadatai_id,
				   @Obj_Id = @Obj_Id,
				   @ObjTip_Id = @Irat_ObjTip_Id,
				   @Targyszo = @Targyszo,
				   @Forras = @ForrasUgyiratAuto,
				   @Ertek = @Ertek,
				   @Sorszam = @Sorszam,
				   @Note = @Note,
				   @Letrehozo_id = @Letrehozo_id,
				   @LetrehozasIdo = @LetrehozasIdo,
				   @Modosito_Id = @Modosito_Id,
				   @ModositasIdo = @ModositasIdo,
				   @ErvKezd = @ErvKezd,
				   @ErvVege = @ErvVege,
				   @ResultUid = @currentRecordId OUTPUT

			if (@@ERROR <> 0)
              SET @Hiba = @Hiba + 1
            else
			begin
				INSERT INTO #temp_ResultTable
				    SELECT * FROM EREC_ObjektumTargyszavai ot
					WHERE ot.Id = @currentRecordId
            end
          END -- nem volt még ilyen hozzárendelés
          ELSE
          BEGIN -- volt már ilyen, update
			   SET @ObjektumTargyszavaiRecordVer = (
					SELECT TOP 1 Ver
                        FROM EREC_ObjektumTargyszavai ot
					    WHERE ot.Id = @ObjektumTargyszavaiRecordId
                    )
			   exec sp_EREC_ObjektumTargyszavaiUpdate
                   @Id = @ObjektumTargyszavaiRecordId,
                   @ExecutorUserId	= @ExecutorUserId,
                   @ExecutionTime = @Now,
				   --@Targyszo_Id = @Targyszo_Id,
				   --@Obj_Metaadatai_Id = @Obj_Metaadatai_id,
				   --@Obj_Id = @Obj_Id,
				   --@ObjTip_Id = @Irat_ObjTip_Id,
				   @Targyszo = @Targyszo,
				   @Forras = @ForrasUgyiratAuto,
				   @Ertek = @Ertek,
				   @Sorszam = @Sorszam,
				   @Ver = @ObjektumTargyszavaiRecordVer,
				   @Note = @Note,
				   @Letrehozo_id = @Letrehozo_id,
				   @LetrehozasIdo = @LetrehozasIdo,
				   @Modosito_Id = @Modosito_Id,
				   @ModositasIdo = @ModositasIdo,
				   @ErvKezd = @ErvKezd,
				   @ErvVege = @ErvVege,
				   @UpdatedColumns = '<root><Targyszo/><Forras/><Ertek/><Sorszam/><Note/><Letrehozo_id/><LetrehozasIdo/><Modosito_Id/><ModositasIdo/><ErvKezd/><ErvVege/></root>'

			if (@@ERROR <> 0)
              SET @Hiba = @Hiba + 1
            else
			begin
				INSERT INTO #temp_ResultTable
				    SELECT * FROM EREC_ObjektumTargyszavai ot
					WHERE ot.Id = @ObjektumTargyszavaiRecordId
            end
          END


	FETCH objektumtargyszavai_sor INTO @Targyszo_Id, @Obj_Metaadatai_id,
           @Targyszo, @Forras, @Ertek, @Sorszam, @Note, @ErvKezd, @ErvVege,
           @Letrehozo_id, @LetrehozasIdo, @Modosito_Id, @ModositasIdo

		END

	CLOSE objektumtargyszavai_sor

	DEALLOCATE objektumtargyszavai_sor


-- update-elt rekordok visszaadása
	SELECT * FROM #temp_ResultTable
-- takarítás
	IF OBJECT_ID('tempdb..#temp_ResultTable') IS NOT NULL 
	BEGIN
	   DROP TABLE #temp_ResultTable
	END 

	IF OBJECT_ID('tempdb..#temp_FilterTable') IS NOT NULL 
	BEGIN
	   DROP TABLE #temp_FilterTable
	END

-- hiba
	if @Hiba > 0
    begin
       RAISERROR ('Az automatikus hozzárendelés során hibák léptek fel. A hibák száma: %d', 16, 1, @Hiba)
    end


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1
   
    

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
