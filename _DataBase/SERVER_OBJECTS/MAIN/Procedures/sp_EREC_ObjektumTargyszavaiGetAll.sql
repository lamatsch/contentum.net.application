IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_ObjektumTargyszavaiGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_ObjektumTargyszavai.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_ObjektumTargyszavai.Id,
	   EREC_ObjektumTargyszavai.Targyszo_Id,
	   EREC_ObjektumTargyszavai.Obj_Metaadatai_Id,
	   EREC_ObjektumTargyszavai.Obj_Id,
	   EREC_ObjektumTargyszavai.ObjTip_Id,
	   EREC_ObjektumTargyszavai.Targyszo,
	   EREC_ObjektumTargyszavai.Forras,
	   EREC_ObjektumTargyszavai.SPSSzinkronizalt,
	   EREC_ObjektumTargyszavai.Ertek,
	   EREC_ObjektumTargyszavai.Sorszam,
	   EREC_ObjektumTargyszavai.Targyszo_XML,
	   EREC_ObjektumTargyszavai.Targyszo_XML_Ervenyes,
	   EREC_ObjektumTargyszavai.Ver,
	   EREC_ObjektumTargyszavai.Note,
	   EREC_ObjektumTargyszavai.Stat_id,
	   EREC_ObjektumTargyszavai.ErvKezd,
	   EREC_ObjektumTargyszavai.ErvVege,
	   EREC_ObjektumTargyszavai.Letrehozo_id,
	   EREC_ObjektumTargyszavai.LetrehozasIdo,
	   EREC_ObjektumTargyszavai.Modosito_id,
	   EREC_ObjektumTargyszavai.ModositasIdo,
	   EREC_ObjektumTargyszavai.Zarolo_id,
	   EREC_ObjektumTargyszavai.ZarolasIdo,
	   EREC_ObjektumTargyszavai.Tranz_id,
	   EREC_ObjektumTargyszavai.UIAccessLog_id  
   from 
     EREC_ObjektumTargyszavai as EREC_ObjektumTargyszavai      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
