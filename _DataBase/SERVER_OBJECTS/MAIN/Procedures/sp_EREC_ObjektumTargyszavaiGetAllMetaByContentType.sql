IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByContentType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByContentType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByContentType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByContentType] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByContentType]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) =  ' order by Szint_Hierarchia, Szint_Felettesek, Sorszam', --' order by Sorszam',
  @TopRow nvarchar(5) = '',
  @ObjTip_Id uniqueidentifier = null,
  @ObjTip_Kod nvarchar(100) = '',
  @ContentType nvarchar(100) = '', -- egyedi azonosító az EREC_Obj_MetaDefinicio táblában!
  @CsakSajatSzint varchar(1) = '0', -- '0' minden szinten hozzárendelés, '1' csak a saját szinten
  @CsakAutomatikus varchar(1) = '1', -- '0' hozzáveszi a kézi és listás tárgyszavakat is,
                                     -- '1' csak a metadefiníció alapján létrejött hozzárendelések vizsgálata
  @ExecutorUserId uniqueidentifier,
  @Obj_Id    uniqueidentifier = null -- az adott objektum egy rekordjának azonosítója (Id)

as

begin

BEGIN TRY

    set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
    declare @sqlcmd nvarchar(MAX)

    declare @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
   begin
       set @LocalTopRow = ''
   end
   else
   begin
       set @LocalTopRow = ' TOP ' + @TopRow
   end

   declare @kt_Automatikus nvarchar(64)
   set @kt_Automatikus = '01' -- Automatikus (kcs: TARGYSZO_FORRAS)

   if (@ContentType is null)
         RAISERROR('[56104]',16,1) -- Nincs megadva ContentType 

--   if @Obj_Id is null
--      RAISERROR('[56100]',16,1) -- Nincs megadva az objektum azonosítója!
   if @ObjTip_Id is null and @ObjTip_Kod is null
      RAISERROR('[56101]',16,1) -- Nincs megadva az objektum típusa (azonosító vagy kód)!

   if @ObjTip_Id is null and @ObjTip_Kod is null
      RAISERROR('[56101]',16,1) -- Nincs megadva az objektum típusa (azonosító vagy kód)!

   if (@ObjTip_Id is null)
   begin
      set @ObjTip_Id = (select top 1 Id from KRT_ObjTipusok where Kod = @ObjTip_Kod and ObjTipus_Id_Tipus =
                       (select top 1 Id from KRT_ObjTipusok where Kod = 'dbtable'))
      if @ObjTip_Id is null
         RAISERROR('[56102]',16,1) -- Nem azonosítható az objektum típusa (nem létezo kód)!

   end
   else
   begin
      set @ObjTip_Kod = (select Kod from KRT_ObjTipusok where Id = @ObjTip_Id)
      if @ObjTip_Kod is null
         RAISERROR('[56103]',16,1) -- Nem azonosítható az objektum típusa (nem létezo id)!
   end 

-- leválogatjuk a contenttype szerint releváns
-- objektum metadefiníciókat, függetlenül a definíció tartalmától (pl. nem vizsgálunk oszlopértékeket)
-- Végso Objektum metadefiníció select felépítése

-- A Szint_Hierarchia az ügyiratnyilvántartáshoz kötodoen, azaz az irat metadefiníók alapján
-- meghatározott hierarchia szerint határozza meg a hierarchiát
-- A Szint_Felettesek ezen belül az egyes objektum metaadat szintek esetleges sablonjait rendezi sorba
-- A kötések során alulról felfelé haladva a Szint_Hierarchia, majd Szint_Felettesek szerint keressük
-- metaadatokat

    set @sqlcmd = 'select Id, ContentType, 0 as Szint_Hierarchia, Szint as Szint_Felettesek
       into #Obj_MetaDefiniciok
       from dbo.fn_GetObjMetaDefinicioFelettesekByContentType(''' + CAST(@Org as NVarChar(40)) + ''',''' + @ContentType + ''')
'


    -- fölérendelt szintek leválogatása
    set @sqlcmd = @sqlcmd + '
if ''' + @CsakSajatSzint + ''' = ''0''
begin
    declare @Szint_Hierarchia int
    set @Szint_Hierarchia = 0
    declare @CurrentContentType nvarchar(100)

    select ContentType, Szint as Szint_Hierarchia
        into #ParentContentTypesFromIratMetaDefinicio
        from dbo.fn_GetAllContentTypesFromIratMetaDefinicio(''' + CAST(@Org as NVarChar(40)) + ''',''' + @ContentType + ''')

    declare parentcontenttype_row cursor local for
        select ContentType, Szint_Hierarchia
        from #ParentContentTypesFromIratMetaDefinicio
        order by Szint_Hierarchia
    open parentcontenttype_row
    fetch parentcontenttype_row into @CurrentContentType, @Szint_Hierarchia
            
    while @@Fetch_Status = 0
    begin
        insert into #Obj_MetaDefiniciok
            select Id, ContentType, @Szint_Hierarchia as Szint_Hierarchia, Szint as Szint_Felettesek
            from dbo.fn_GetObjMetaDefinicioFelettesekByContentType(''' + CAST(@Org as NVarChar(40)) + ''',@CurrentContentType)
        fetch parentcontenttype_row into @CurrentContentType, @Szint_Hierarchia
    end

    close parentcontenttype_row
    deallocate parentcontenttype_row
'
   -- B1 típusú (Objektum kiegészítés) meták lekérése hierarchikusan
   -- az objektum típus alapján alapján minden fölérendelt szint
    set @sqlcmd = @sqlcmd + '
    set @Szint_Hierarchia = @Szint_Hierarchia + 1
    select Id, ContentType, @Szint_Hierarchia + Szint as Szint_Hierarchia, 0 as Szint_Felettesek
            into #KiegeszitoObjMetaDefinicio
            from dbo.fn_GetAllKiegeszitoObjMetaDefinicioByObjTipId(''' + CAST(@Org as NVarChar(40)) + ''',''' + convert(varchar(36), @ObjTip_Id) + ''')
            where Id not in (select Id from  #Obj_MetaDefiniciok)

    declare extendingcontenttype_row cursor local for
        select ContentType, Szint_Hierarchia
        from #KiegeszitoObjMetaDefinicio
        order by Szint_Hierarchia
    open extendingcontenttype_row
    fetch extendingcontenttype_row into @CurrentContentType, @Szint_Hierarchia
            
    while @@Fetch_Status = 0
    begin
        insert into #Obj_MetaDefiniciok
            select Id, ContentType, @Szint_Hierarchia as Szint_Hierarchia, Szint as Szint_Felettesek
            from dbo.fn_GetObjMetaDefinicioFelettesekByContentType(''' + CAST(@Org as NVarChar(40)) + ''',@CurrentContentType)

        fetch extendingcontenttype_row into @CurrentContentType, @Szint_Hierarchia
    end

    close extendingcontenttype_row
    deallocate extendingcontenttype_row
end
'

-- Kibovítés az Objtip_Id-val: A0 típusnál (Szint_Felettesek > 0) a hivatkozó Obj_MetaDefinicióból
-- (Szint_Felettesek = 0) vesszük az Objtip_Id-t
   set @sqlcmd = @sqlcmd + '
alter table #Obj_MetaDefiniciok add Objtip_Id uniqueidentifier null
'
-- a temporary táblát összekapcsoljuk saját magával úgy, hogy az ObjTip_Id nélküli (A0 típusú) definíciók
-- mellé (felettesek) a rájuk közvetlenül vagy közvetve hivatkozó valódi objektum metadefiníció kerüljön,
-- a többi mellé saját maga, majd a hozzákapcsolt táblából kiemelt Id segítségével lekérjük
-- az objektum típust az eredeti objektum metadefiníciós táblából
set @sqlcmd = @sqlcmd + '
update #Obj_MetaDefiniciok set Objtip_Id =
    (select EREC_Obj_MetaDefinicio.ObjTip_Id from EREC_Obj_MetaDefinicio
     where EREC_Obj_MetaDefinicio.Id = (
     select om2.Id from #Obj_MetaDefiniciok om1
     join #Obj_MetaDefiniciok om2
     on om1.Szint_Hierarchia = om2.Szint_Hierarchia and om2.Szint_Felettesek = 0
     where om1.Id = #Obj_MetaDefiniciok.Id))
'
-- 

-- Objektum metaadatai
    set @sqlcmd = @sqlcmd + ' 
select oma.Id, #Obj_MetaDefiniciok.Objtip_Id, #Obj_MetaDefiniciok.Szint_Hierarchia as Szint_Hierarchia, #Obj_MetaDefiniciok.Szint_Felettesek as Szint_Felettesek
    into #Obj_MetaAdatai
    from EREC_Obj_MetaAdatai oma
    join #Obj_MetaDefiniciok
    on oma.Obj_MetaDefinicio_Id = #Obj_MetaDefiniciok.Id
    where oma.Obj_MetaDefinicio_Id in (select Id from #Obj_MetaDefiniciok)
    and getdate() between oma.ErvKezd and oma.ErvVege
'


   set @sqlcmd = @sqlcmd +
'
select convert(uniqueidentifier, null) as Id,
       --''' + convert(varchar(36), @ObjTip_Id) + ''' as ObjTip_Id,
       #Obj_MetaAdatai.Objtip_Id as ObjTip_Id,
       convert(nvarchar(100), null) as Ertek,
       EREC_Obj_MetaAdatai.Id as Obj_Metaadatai_Id,
       EREC_Obj_MetaAdatai.Targyszavak_Id as Targyszo_Id,
       EREC_Obj_MetaAdatai.Sorszam,
       ''' + @kt_Automatikus + ''' as Forras,
       dbo.fn_KodtarErtekNeve(''TARGYSZO_FORRAS'', ''' + @kt_Automatikus + ''',''' + cast(@Org as NVarChar(40)) + ''') as ForrasNev,
       EREC_TargySzavak.TargySzavak as Targyszo,
       EREC_TargySzavak.Csoport_Id_Tulaj,
       EREC_TargySzavak.Tipus,
       EREC_TargySzavak.BelsoAzonosito,
       EREC_TargySzavak.RegExp,
       EREC_TargySzavak.SPSSzinkronizalt,
       EREC_TargySzavak.SPS_Field_Id,
       EREC_Obj_MetaAdatai.Funkcio,
       EREC_Obj_MetaAdatai.ErvKezd,
       EREC_Obj_MetaAdatai.ErvVege,
       ''0'' as Ver,
       convert(nvarchar(4000), null) as Note,
       #Obj_MetaAdatai.Szint_Hierarchia as Szint_Hierarchia,
       #Obj_MetaAdatai.Szint_Felettesek as Szint_Felettesek
    into #Obj_MetaAdatai_Targyszavak
    from
       #Obj_MetaAdatai join 
       (select * from EREC_Obj_MetaAdatai where Id in (select Id from #Obj_MetaAdatai)) as EREC_Obj_MetaAdatai
       on #Obj_MetaAdatai.Id = EREC_Obj_MetaAdatai.Id
       join EREC_TargySzavak as EREC_TargySzavak
       on EREC_Obj_MetaAdatai.Targyszavak_Id = EREC_TargySzavak.Id and EREC_TargySzavak.Org=''' + cast(@Org as NVarChar(40)) + '''
       --and getdate() between EREC_Obj_MetaAdatai.ErvKezd and EREC_Obj_MetaAdatai.ErvVege
       and getdate() between EREC_TargySzavak.ErvKezd and EREC_TargySzavak.ErvVege
    order by #Obj_MetaAdatai.Szint_Hierarchia, #Obj_MetaAdatai.Szint_Felettesek, EREC_Obj_MetaAdatai.Sorszam

'
-- Összefésülés a valódi objektum tárgyszavai hozzárendelésekkel, ha van objektum
   if @Obj_Id is not null
   begin
       set @sqlcmd = @sqlcmd + 'update #Obj_MetaAdatai_Targyszavak
set #Obj_MetaAdatai_Targyszavak.Id = (select top 1 ot.Id from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Obj_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.ObjTip_Id = (select top 1 ot.ObjTip_Id from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Obj_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.Targyszo_Id = (select top 1 ot.Targyszo_Id from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Obj_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.SPSSzinkronizalt = (select top 1 ot.SPSSzinkronizalt from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Obj_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.Forras = (select top 1 ot.Forras from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Obj_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.ForrasNev = (select dbo.fn_KodtarErtekNeve(''TARGYSZO_FORRAS'', (select top 1 ot.Forras from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Obj_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),''' + cast(@Org as NVarChar(40)) + ''')),
    #Obj_MetaAdatai_Targyszavak.Ertek  =  (select top 1 ot.Ertek from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Obj_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.Sorszam = (select top 1 ot.Sorszam from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Obj_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.ErvKezd = (select top 1 ot.ErvKezd from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Obj_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.ErvVege = (select top 1 ot.ErvVege from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Obj_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.Ver = (select top 1 ot.Ver from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Obj_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.Note = (select top 1 ot.Note from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Obj_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege)
where exists(select ot.Id from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Obj_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege)
'
        if @CsakSajatSzint = '0'
        begin
            -- ha van a listán fölérendelt objektum típushoz tartozó metaadat (iratnál pl. ügyiratdarab és ügyirat),
            -- és annak van tényleges elofordulása az adott objektum tényleges fölérendelt objektumával kapcsolva
            -- az EREC_ObjektumTargyszavai táblában, akkor azokhoz is bekötjük az Id-t és a pontos értékeket
            
            -- meghatározzuk, hogy az aktuális objektum mi (irat, ügyiratdarab vagy ügyirat)
            -- és ennek megfeleloen a szülo objektum típusokat, illetve szülo objektumokat
            declare @Ugyirat_Id uniqueidentifier
            declare @Ugyiratdarab_Id uniqueidentifier
            declare @Irat_Id uniqueidentifier

            declare @Objtip_Id_Ugyiratok uniqueidentifier
            declare @Objtip_Id_Ugyiratdarabok uniqueidentifier
            declare @Objtip_Id_Iratok uniqueidentifier

            -- objektum típusok azonosítóinak lekérése
            set @Objtip_Id_Iratok = (select objtip.Id from KRT_ObjTipusok objtip
                                     where objtip.Kod = 'EREC_IraIratok'
                                     and objtip.ObjTipus_Id_Tipus = (select Id from KRT_ObjTipusok objtip2
                                                                     where objtip2.Kod = 'DbTable'
                                                                     and getdate() between objtip2.ErvKezd and objtip2.ErvVege)
                                     and getdate() between objtip.ErvKezd and objtip.ErvVege)

            set @Objtip_Id_Ugyiratdarabok = (select objtip.Id from KRT_ObjTipusok objtip
                                     where objtip.Kod = 'EREC_UgyUgyiratdarabok'
                                     and objtip.ObjTipus_Id_Tipus = (select Id from KRT_ObjTipusok objtip2
                                                                     where objtip2.Kod = 'DbTable'
                                                                     and getdate() between objtip2.ErvKezd and objtip2.ErvVege)
                                     and getdate() between objtip.ErvKezd and objtip.ErvVege)

            set @Objtip_Id_Ugyiratok = (select objtip.Id from KRT_ObjTipusok objtip
                                     where objtip.Kod = 'EREC_UgyUgyiratok'
                                     and objtip.ObjTipus_Id_Tipus = (select Id from KRT_ObjTipusok objtip2
                                                                     where objtip2.Kod = 'DbTable'
                                                                     and getdate() between objtip2.ErvKezd and objtip2.ErvVege)
                                     and getdate() between objtip.ErvKezd and objtip.ErvVege)

            if @ObjTip_Kod = 'EREC_IraIratok'
            begin
                set @Irat_Id = @Obj_Id
                set @Ugyiratdarab_Id = (select UgyUgyIratDarab_Id from EREC_IraIratok where Id = @Obj_Id)
                set @Ugyirat_Id = (select UgyUgyirat_Id from EREC_UgyUgyiratdarabok where Id = @Ugyiratdarab_Id)
            end
            else if @ObjTip_Kod = 'EREC_UgyUgyiratdarabok'
            begin
                set @Ugyiratdarab_Id = @Obj_Id
                set @Ugyirat_Id = (select UgyUgyirat_Id from EREC_UgyUgyiratdarabok where Id = @Ugyiratdarab_Id)
            end
            else if @ObjTip_Kod = 'EREC_UgyUgyiratok'
            begin
                set @Ugyirat_Id = @Obj_Id
            end

            if @Ugyirat_Id is not null and @Ugyirat_Id != @Obj_Id
            begin
                set @sqlcmd = @sqlcmd + '
if exists(select Id from #Obj_MetaAdatai_Targyszavak where ObjTip_Id = ''' + convert(varchar(36), @ObjTip_Id_Ugyiratok) + ''')
begin
    update #Obj_MetaAdatai_Targyszavak
    set #Obj_MetaAdatai_Targyszavak.Id = (select top 1 ot.Id from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.ObjTip_Id = (select top 1 ot.ObjTip_Id from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.Targyszo_Id = (select top 1 ot.Targyszo_Id from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.SPSSzinkronizalt = (select top 1 ot.SPSSzinkronizalt from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.Forras = (select top 1 ot.Forras from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.ForrasNev = (select dbo.fn_KodtarErtekNeve(''TARGYSZO_FORRAS'', (select top 1 ot.Forras from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),''' + cast(@Org as NVarChar(40))  + ''')),
    #Obj_MetaAdatai_Targyszavak.Ertek  =  (select top 1 ot.Ertek from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.Sorszam = (select top 1 ot.Sorszam from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.ErvKezd = (select top 1 ot.ErvKezd from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.ErvVege = (select top 1 ot.ErvVege from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.Ver = (select top 1 ot.Ver from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.Note = (select top 1 ot.Note from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege)
    where exists(select ot.Id from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege)
end
'
            end

            if @Ugyiratdarab_Id is not null and @Ugyiratdarab_Id != @Obj_Id
            begin
                set @sqlcmd = @sqlcmd + '
if exists(select Id from #Obj_MetaAdatai_Targyszavak where ObjTip_Id = ''' + convert(varchar(36), @ObjTip_Id_Ugyiratdarabok) + ''')
begin
    update #Obj_MetaAdatai_Targyszavak
    set #Obj_MetaAdatai_Targyszavak.Id = (select top 1 ot.Id from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.ObjTip_Id = (select top 1 ot.ObjTip_Id from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.Targyszo_Id = (select top 1 ot.Targyszo_Id from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.SPSSzinkronizalt = (select top 1 ot.SPSSzinkronizalt from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.Forras = (select top 1 ot.Forras from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.ForrasNev = (select dbo.fn_KodtarErtekNeve(''TARGYSZO_FORRAS'', (select top 1 ot.Forras from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege))),
    #Obj_MetaAdatai_Targyszavak.Ertek  =  (select top 1 ot.Ertek from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.Sorszam = (select top 1 ot.Sorszam from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.ErvKezd = (select top 1 ot.ErvKezd from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.ErvVege = (select top 1 ot.ErvVege from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.Ver = (select top 1 ot.Ver from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege),
    #Obj_MetaAdatai_Targyszavak.Note = (select top 1 ot.Note from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege)
    where exists(select ot.Id from EREC_ObjektumTargyszavai ot where ot.Obj_Id = ''' + convert(varchar(36), @Ugyirat_Id) + ''' and ot.Obj_MetaAdatai_Id = #Obj_MetaAdatai_Targyszavak.Obj_Metaadatai_Id and ot.Targyszo_Id = #Obj_MetaAdatai_Targyszavak.Targyszo_Id and getdate() between ot.ErvKezd and ot.ErvVege)
end
'
            end

        end

        if @CsakAutomatikus = '0'
        begin
            set @sqlcmd = @sqlcmd + '
alter table #Obj_MetaAdatai_Targyszavak alter column Obj_Metaadatai_Id uniqueidentifier null
alter table #Obj_MetaAdatai_Targyszavak alter column Targyszo_Id uniqueidentifier null
alter table #Obj_MetaAdatai_Targyszavak alter column Csoport_Id_Tulaj uniqueidentifier null

insert into #Obj_MetaAdatai_Targyszavak
    select EREC_ObjektumTargyszavai.Id as Id,
           EREC_ObjektumTargyszavai.ObjTip_Id as ObjTip_Id,
           EREC_ObjektumTargyszavai.Ertek as Ertek,
           EREC_ObjektumTargyszavai.Obj_Metaadatai_Id as Obj_Metaadatai_Id,
           EREC_ObjektumTargyszavai.Targyszo_Id as Targyszo_Id,
           EREC_ObjektumTargyszavai.Sorszam as Sorszam,
           EREC_ObjektumTargyszavai.Forras as Forras,
           dbo.fn_KodtarErtekNeve(''TARGYSZO_FORRAS'', EREC_ObjektumTargyszavai.Forras,''' + cast(@Org as NVarChar(40)) + ''') as ForrasNev,
           EREC_ObjektumTargyszavai.Targyszo as Targyszo,
           (select Csoport_Id_Tulaj from EREC_TargySzavak where Id = EREC_ObjektumTargyszavai.Targyszo_Id) as Csoport_Id_Tulaj,
           isnull((select Tipus from EREC_TargySzavak where Id = EREC_ObjektumTargyszavai.Targyszo_Id), ''0'') as Tipus,
           (select BelsoAzonosito from EREC_TargySzavak where Id = EREC_ObjektumTargyszavai.Targyszo_Id) as BelsoAzonosito,
           (select RegExp from EREC_TargySzavak where Id = EREC_ObjektumTargyszavai.Targyszo_Id) as RegExp,
           EREC_ObjektumTargyszavai.SPSSzinkronizalt as SPSSzinkronizalt,
           (select SPS_Field_Id from EREC_TargySzavak where Id = EREC_ObjektumTargyszavai.Targyszo_Id) as SPS_Field_Id,
           (select Funkcio from EREC_Obj_MetaAdatai where Id = EREC_ObjektumTargyszavai.Obj_MetaAdatai_Id) as Funkcio,
           EREC_ObjektumTargyszavai.ErvKezd as ErvKezd,
           EREC_ObjektumTargyszavai.ErvVege as ErvVege,
           EREC_ObjektumTargyszavai.Ver as Ver,
           EREC_ObjektumTargyszavai.Note as Note,
           @Szint_Hierarchia + 1 as Szint_Hierarchia,
           0 as Szint_Felettesek
    from EREC_ObjektumTargyszavai
    where EREC_ObjektumTargyszavai.Obj_Id = ''' + convert(varchar(36), @Obj_Id) + '''
          and EREC_ObjektumTargyszavai.Id not in (select #Obj_MetaAdatai_Targyszavak.Id from #Obj_MetaAdatai_Targyszavak where #Obj_MetaAdatai_Targyszavak.Id is not null)
          and getdate() between EREC_ObjektumTargyszavai.ErvKezd and EREC_ObjektumTargyszavai.ErvVege
'

        end
    end -- if @Obj_Id is not null

    set @sqlcmd = @sqlcmd + 'select ' + @LocalTopRow + ' * from #Obj_MetaAdatai_Targyszavak as EREC_ObjektumTargyszavai
'
    if @Where is not null and @Where!=''
    begin 
        set @sqlcmd = @sqlcmd + ' Where ' + @Where
    end
     
   
   set @sqlcmd = @sqlcmd + @OrderBy;

---- nyomtatási blokk kezdete
--   if exists (select 1 from  sysobjects where id = object_id('sp_PrintText') and type = 'P')
--   begin
--       exec dbo.sp_PrintText @sqlcmd;
--   end
---- nyomtatási blokk vége

   exec (@sqlcmd);

END TRY
BEGIN CATCH
        declare @errorSeverity INT, @errorState INT
        declare @errorCode NVARCHAR(1000)    
        SET @errorSeverity = ERROR_SEVERITY()
        SET @errorState = ERROR_STATE()
        
        if ERROR_NUMBER()<50000        
                SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
        else
                SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

        RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
