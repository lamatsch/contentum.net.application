IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByDefinicioTipus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByDefinicioTipus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByDefinicioTipus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByDefinicioTipus] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByDefinicioTipus]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by Sorszam',
  @TopRow nvarchar(5) = '',
  @ObjTip_Id uniqueidentifier = null,
  @ObjTip_Kod nvarchar(100) = '',
  @DefinicioTipus nvarchar(2) = '', -- OBJMETADEFINICIO_TIPUS @DefinicioTipus = 'B1' (jelenleg B1 és C2 használt)
  @CsakSajatSzint varchar(1) = '0', -- '0' minden szinten hozzárendelés, '1' csak a saját szinten
  @CsakAutomatikus varchar(1) = '1', -- '0' hozzáveszi a kézi és listás tárgyszavakat is,
                                     -- '1' csak a metadefiníció alapján létrejött hozzárendelések vizsgálata
  @ExecutorUserId uniqueidentifier,
  @Obj_Id    uniqueidentifier = null -- az adott objektum egy rekordjának azonosítója (Id)

as

begin

BEGIN TRY

    set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
    declare @sqlcmd nvarchar(MAX)
    declare @sql_omd_select nvarchar(MAX)
    set @sql_omd_select = ''

    declare @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
   begin
       set @LocalTopRow = ''
   end
   else
   begin
       set @LocalTopRow = ' TOP ' + @TopRow
   end

   declare @kt_Automatikus nvarchar(64)
   declare @kt_Automatikus_ForrasNev nvarchar(400)
   set @kt_Automatikus = '01' -- Automatikus (kcs: TARGYSZO_FORRAS)
   set @kt_Automatikus_ForrasNev = dbo.fn_KodtarErtekNeve('TARGYSZO_FORRAS', @kt_Automatikus,@Org)

--   if @Obj_Id is null
--      RAISERROR('[56100]',16,1) -- Nincs megadva az objektum azonosítója!
   if @ObjTip_Id is null and @ObjTip_Kod is null
      RAISERROR('[56101]',16,1) -- Nincs megadva az objektum típusa (azonosító vagy kód)!

   if (@ObjTip_Id is null)
   begin
      set @ObjTip_Id = (select top 1 Id from KRT_ObjTipusok where Kod = @ObjTip_Kod and ObjTipus_Id_Tipus =
                       (select top 1 Id from KRT_ObjTipusok where Kod = 'dbtable'))
      if @ObjTip_Id is null
         RAISERROR('[56102]',16,1) -- Nem azonosítható az objektum típusa (nem létezo kód)!

   end
   else
   begin
      set @ObjTip_Kod = (select Kod from KRT_ObjTipusok where Id = @ObjTip_Id)
      if @ObjTip_Kod is null
         RAISERROR('[56103]',16,1) -- Nem azonosítható az objektum típusa (nem létezo id)!
   end     

-- Objektum metadefiniciok, ha konkrét objektumról van szó
   if @Obj_Id is not null
   begin
-- leválogatjuk az objektum típus és (ha meg van adva) definíció típus szerint releváns
-- objektum metadefiníciókat, ahol meg van adva oszlop és az átadott objektumtípusban
-- (táblában) az adott oszlop létezik - de itt még nem vizsgáljuk az értéket
    select omd.Id as Id,
           (select top 1 Kod from KRT_ObjTipusok where Id = omd.Objtip_Id_Column) as ColumnName,
           omd.ColumnValue as ColumnValue
                  into #Obj_MetaDefiniciokTopLevel
                  from EREC_Obj_MetaDefinicio omd
                  where omd.Objtip_Id = @ObjTip_Id
                        and 
                        (
                            (@DefinicioTipus is null or @DefinicioTipus = '')
                            or
                            (omd.DefinicioTipus = @DefinicioTipus)
                         )
                         and 
                         (   @Obj_Id is not null
                             and Objtip_Id_Column is not null
                             and dbo.fn_dbcolumn_exists(
                                      @ObjTip_Kod,
                                      (select top 1 Kod from KRT_ObjTipusok where Id = omd.Objtip_Id_Column)
                                  ) = 1
                         )
                         and omd.Org=@Org
                         and getdate() between omd.ErvKezd and omd.ErvVege

-- változók és cursor a select összeállításához 
        declare @omd_Id uniqueidentifier,
                @omd_ColumnName nvarchar(100),
                @omd_ColumnValue nvarchar(100)
                
        -- cursor deklaralasa
        declare obj_metadefiniciotoplevel_sor cursor local for
                select Id, ColumnName, ColumnValue
                    from #Obj_MetaDefiniciokTopLevel

        open obj_metadefiniciotoplevel_sor

        -- ciklus kezdete elotti fetch
        fetch obj_metadefiniciotoplevel_sor into @omd_Id, @omd_ColumnName, @omd_ColumnValue

        while @@Fetch_Status = 0
        begin
           if @sql_omd_select != ''
           begin
               set @sql_omd_select = @sql_omd_select + '
UNION
'
           end
           else
           begin
               set @sql_omd_select = @sql_omd_select + '  insert into #Obj_MetaDefiniciok
'
           end
           set @sql_omd_select = @sql_omd_select + '
select omd.Id as Id, omd.ContentType as ContentType
from EREC_Obj_MetaDefinicio omd
where omd.Id = ''' + convert(nvarchar(36), @omd_Id) + '''
and exists(select Id from ' + @ObjTip_Kod + ' where Id = @Obj_Id and '
     + @omd_ColumnName + ' = ''' + @omd_ColumnValue + ''' and getdate() between ' + @ObjTip_Kod + '.ErvKezd and ' + @ObjTip_Kod + '.ErvVege)'

           fetch obj_metadefiniciotoplevel_sor into @omd_Id, @omd_ColumnName, @omd_ColumnValue
       end

       close obj_metadefiniciotoplevel_sor
       deallocate obj_metadefiniciotoplevel_sor
   end -- if @Obj_Id is not null

    -- Végso Objektum metadefiníció select felépítése

    -- EREC_Obj_MetaDefinicio tábla szurése típus, objektum típus és érvényesség szerint
    -- kiválasztjuk az oszlop megadás nélküli, majd az oszlopértéket definiáló rekordokat
    set @sqlcmd = 'select omd.Id as Id, omd.ContentType as ContentType
    into #Obj_MetaDefiniciok
    from EREC_Obj_MetaDefinicio omd
    where omd.Objtip_Id_Column is null
    and omd.Objtip_Id = @Objtip_Id
    and omd.Org=@Org
    and getdate() between omd.ErvKezd and omd.ErvVege
    '
    if @DefinicioTipus is not null and @DefinicioTipus != ''
    begin
        set @sqlcmd = @sqlcmd + 'and omd.DefinicioTipus = @DefinicioTipus
    '
    end
    else
    begin
    -- A0 típus csak mint felettes rendelheto hozzá!!! (oszlopdefiníciót pedig nem tartalmazhat)
        set @sqlcmd = @sqlcmd + 'and omd.DefinicioTipus != ''A0''
    '
    end

   set @sqlcmd = @sqlcmd + @sql_omd_select

          -- fölérendelt szintek leválogatása
	if @CsakSajatSzint = '0'
	begin
    set @sqlcmd = @sqlcmd + '
if @@rowcount > 0
begin
    declare @Obj_MetaDefinicio_Id uniqueidentifier
    
    declare objmetadefinicio_row cursor local for
        select Id
        from #Obj_MetaDefiniciok

    open objmetadefinicio_row
    fetch objmetadefinicio_row into @Obj_MetaDefinicio_Id
            
    while @@Fetch_Status = 0
    begin
        insert into #Obj_MetaDefiniciok
            select Id, ContentType
            from dbo.fn_GetObjMetaDefinicioFelettesekById(@Obj_MetaDefinicio_Id)
            where Id not in (select Id from #Obj_MetaDefiniciok)

        fetch objmetadefinicio_row into @Obj_MetaDefinicio_Id
    end

    close objmetadefinicio_row
    deallocate objmetadefinicio_row
end
    '
	end
-- Objektum metaadatai
    set @sqlcmd = @sqlcmd + ' 
select oma.Id
    into #Obj_MetaAdatai
    from EREC_Obj_MetaAdatai oma
    join #Obj_MetaDefiniciok
    on oma.Obj_MetaDefinicio_Id = #Obj_MetaDefiniciok.Id
    and getdate() between oma.ErvKezd and oma.ErvVege
     '

-- Kapcsolt tárgyszavak lekérése
   if @Obj_Id is null
   begin
   set @sqlcmd = @sqlcmd +
  'select 
	convert(uniqueidentifier, null) as Id,
	@Objtip_Id as ObjTip_Id,
	convert(nvarchar(100), null) as Ertek,
	EREC_Obj_MetaAdatai.Id as Obj_Metaadatai_Id,
	EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id as Obj_MetaDefinicio_Id,
	(select DefinicioTipus from EREC_Obj_MetaDefinicio where EREC_Obj_MetaDefinicio.Id = EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id) as DefinicioTipus,
	(select ContentType from EREC_Obj_MetaDefinicio where EREC_Obj_MetaDefinicio.Id = EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id) as ContentType,
	EREC_Obj_MetaAdatai.SPSSzinkronizalt as SPSSzinkronizalt,
	EREC_Obj_MetaAdatai.Targyszavak_Id as Targyszo_Id,
	EREC_Obj_MetaAdatai.Sorszam,
	@kt_Automatikus as Forras,
	@kt_Automatikus_ForrasNev as ForrasNev,
	EREC_TargySzavak.TargySzavak as Targyszo,
	EREC_TargySzavak.Csoport_Id_Tulaj,
	EREC_TargySzavak.Tipus,
	EREC_TargySzavak.BelsoAzonosito,
	EREC_TargySzavak.RegExp,
	EREC_TargySzavak.ToolTip,
	IsNull(EREC_Obj_MetaAdatai.ControlTypeSource, EREC_TargySzavak.ControlTypeSource) as ControlTypeSource,
	IsNull(EREC_Obj_MetaAdatai.ControlTypeDataSource, EREC_TargySzavak.ControlTypeDataSource) as ControlTypeDataSource,
	convert(nvarchar(100), null) as ErtekByControlSourceType,
	EREC_Obj_MetaAdatai.Funkcio,
	EREC_Obj_MetaAdatai.ErvKezd,
	EREC_Obj_MetaAdatai.ErvVege,
	0 as Ver,
	convert(nvarchar(4000), null) as Note,
	convert(char(1), ''1'') as FromMetaDefinicio,
	IsNull(EREC_Obj_MetaAdatai.AlapertelmezettErtek, EREC_TargySzavak.AlapertelmezettErtek) as AlapertelmezettErtek,
	EREC_Obj_MetaAdatai.Opcionalis as Opcionalis,
	EREC_Obj_MetaAdatai.Ismetlodo as Ismetlodo
   into #Obj_MetaAdatai_Targyszavak
   from 
       #Obj_MetaAdatai
       inner join EREC_Obj_MetaAdatai
       on #Obj_MetaAdatai.Id = EREC_Obj_MetaAdatai.Id
       inner join EREC_TargySzavak as EREC_TargySzavak
       on EREC_Obj_MetaAdatai.Targyszavak_Id = EREC_TargySzavak.Id and EREC_TargySzavak.Org=@Org
       --and getdate() between EREC_Obj_MetaAdatai.ErvKezd and EREC_Obj_MetaAdatai.ErvVege
       and getdate() between EREC_TargySzavak.ErvKezd and EREC_TargySzavak.ErvVege
'
	end
	else --   if @Obj_Id is not null
   begin
	-- Összefésülés a valódi objektum tárgyszavai hozzárendelésekkel, ha van objektum
   set @sqlcmd = @sqlcmd +'
	select 
		convert(uniqueidentifier, ot.Id) as Id,
		case when ot.Id is null then @Objtip_Id else ot.Objtip_Id end as ObjTip_Id,
		case when ot.Id is null then convert(nvarchar(100), null) else ot.Ertek end as Ertek,
		EREC_Obj_MetaAdatai.Id as Obj_Metaadatai_Id,
		EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id as Obj_MetaDefinicio_Id,
		(select DefinicioTipus from EREC_Obj_MetaDefinicio where EREC_Obj_MetaDefinicio.Id = EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id) as DefinicioTipus,
		(select ContentType from EREC_Obj_MetaDefinicio where EREC_Obj_MetaDefinicio.Id = EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id) as ContentType,
		case when ot.Id is null then EREC_Obj_MetaAdatai.SPSSzinkronizalt else ot.SPSSzinkronizalt end as SPSSzinkronizalt,
		case when ot.Id is null then EREC_Obj_MetaAdatai.Targyszavak_Id else ot.Targyszo_Id end as Targyszo_Id,
		case when ot.Id is null then EREC_Obj_MetaAdatai.Sorszam else ot.Sorszam end as Sorszam,
		case when ot.Id is null then @kt_Automatikus else ot.Forras end as Forras,
		case when ot.Id is null then @kt_Automatikus_ForrasNev else dbo.fn_KodtarErtekNeve(''TARGYSZO_FORRAS'',ot.Forras,@Org) end as ForrasNev,
		EREC_TargySzavak.TargySzavak as Targyszo,
		EREC_TargySzavak.Csoport_Id_Tulaj,
		EREC_TargySzavak.Tipus,
		EREC_TargySzavak.BelsoAzonosito,
		EREC_TargySzavak.RegExp,
		EREC_TargySzavak.ToolTip,
		IsNull(EREC_Obj_MetaAdatai.ControlTypeSource, EREC_TargySzavak.ControlTypeSource) as ControlTypeSource,
		IsNull(EREC_Obj_MetaAdatai.ControlTypeDataSource, EREC_TargySzavak.ControlTypeDataSource) as ControlTypeDataSource,
		case IsNull(EREC_Obj_MetaAdatai.ControlTypeSource, EREC_TargySzavak.ControlTypeSource)
		when ''~/Component/EditablePartnerTextBox.ascx'' then (select top 1 Nev
			from KRT_Partnerek
				where Id=ot.Ertek
					and getdate() between ErvKezd and ErvVege)
		when ''~/Component/FelhasznaloCsoportTextBox.ascx'' then (select top 1 Nev
				from KRT_Felhasznalok
				where Id=ot.Ertek
					and getdate() between ErvKezd and ErvVege)
		when ''~/Component/KodTarakDropDownList.ascx'' then (select Nev
				from KRT_KodTarak
				where KodCsoport_Id = (select top 1 Id from KRT_KodCsoportok
						where Kod=IsNull(EREC_Obj_MetaAdatai.ControlTypeDataSource, EREC_TargySzavak.ControlTypeDataSource) collate Hungarian_CS_AS
						and getdate() between ErvKezd and ErvVege)
				and Org=@Org
				and Kod=ot.Ertek collate Hungarian_CS_AS
				and getdate() between ErvKezd and ErvVege)
		else convert(nvarchar(100), ot.Ertek) end as ErtekByControlSourceType,
		EREC_Obj_MetaAdatai.Funkcio,
		case when ot.Id is null then EREC_Obj_MetaAdatai.ErvKezd else ot.ErvKezd end as ErvKezd,
		case when ot.Id is null then EREC_Obj_MetaAdatai.ErvVege else ot.ErvVege end as ErvVege,
		case when ot.Id is null then 0 else ot.Ver end as Ver,
		case when ot.Id is null then convert(nvarchar(4000), null) else ot.Note end as Note,
		convert(char(1), ''1'') as FromMetaDefinicio,
		IsNull(EREC_Obj_MetaAdatai.AlapertelmezettErtek, EREC_TargySzavak.AlapertelmezettErtek) as AlapertelmezettErtek,
		EREC_Obj_MetaAdatai.Opcionalis as Opcionalis,
		EREC_Obj_MetaAdatai.Ismetlodo as Ismetlodo
	into #Obj_MetaAdatai_Targyszavak
	from 
		#Obj_MetaAdatai 
		inner join EREC_Obj_MetaAdatai
		on #Obj_MetaAdatai.Id = EREC_Obj_MetaAdatai.Id
		inner join EREC_TargySzavak as EREC_TargySzavak
		on EREC_Obj_MetaAdatai.Targyszavak_Id = EREC_TargySzavak.Id and EREC_TargySzavak.Org=@Org
		and getdate() between EREC_TargySzavak.ErvKezd and EREC_TargySzavak.ErvVege
	   left join EREC_ObjektumTargyszavai ot on ot.Obj_Id = @Obj_Id and ot.Obj_MetaAdatai_Id = EREC_Obj_MetaAdatai.Id and ot.Targyszo_Id = EREC_TargySzavak.Id and getdate() between ot.ErvKezd and ot.ErvVege
'

        if @CsakAutomatikus = '0'
        begin
            set @sqlcmd = @sqlcmd + '
alter table #Obj_MetaAdatai_Targyszavak alter column Obj_Metaadatai_Id uniqueidentifier null
alter table #Obj_MetaAdatai_Targyszavak alter column Targyszo_Id uniqueidentifier null
alter table #Obj_MetaAdatai_Targyszavak alter column Csoport_Id_Tulaj uniqueidentifier null
alter table #Obj_MetaAdatai_Targyszavak alter column Obj_MetaDefinicio_Id uniqueidentifier null
alter table #Obj_MetaAdatai_Targyszavak alter column Sorszam int null
'
			set @sqlcmd = @sqlcmd + '
insert into #Obj_MetaAdatai_Targyszavak
    select EREC_ObjektumTargyszavai.Id as Id,
		EREC_ObjektumTargyszavai.ObjTip_Id as ObjTip_Id,
		EREC_ObjektumTargyszavai.Ertek as Ertek,
		EREC_ObjektumTargyszavai.Obj_Metaadatai_Id as Obj_Metaadatai_Id,
		null as Obj_MetaDefinicio_Id,
		null as DefinicioTipus,
		null as ContentType,
		EREC_ObjektumTargyszavai.SPSSzinkronizalt as SPSSzinkronizalt,
		EREC_ObjektumTargyszavai.Targyszo_Id as Targyszo_Id,
		EREC_ObjektumTargyszavai.Sorszam as Sorszam,
		EREC_ObjektumTargyszavai.Forras as Forras,
		dbo.fn_KodtarErtekNeve(''TARGYSZO_FORRAS'', EREC_ObjektumTargyszavai.Forras,@Org) as ForrasNev,
		EREC_ObjektumTargyszavai.Targyszo as Targyszo,
		tsz.Csoport_Id_Tulaj as Csoport_Id_Tulaj,
		IsNull(tsz.Tipus, ''0'') as Tipus,
		tsz.BelsoAzonosito as BelsoAzonosito,
		tsz.RegExp as RegExp,
		tsz.ToolTip as ToolTip,
		IsNull(oma.ControlTypeSource, tsz.ControlTypeSource) as ControlTypeSource,
		IsNull(oma.ControlTypeDataSource, tsz.ControlTypeDataSource) as ControlTypeDataSource,
		case IsNull(oma.ControlTypeSource, tsz.ControlTypeSource)
		when ''~/Component/EditablePartnerTextBox.ascx'' then (select top 1 Nev
			from KRT_Partnerek
				where Id=EREC_ObjektumTargyszavai.Ertek
					and getdate() between ErvKezd and ErvVege)
		when ''~/Component/FelhasznaloCsoportTextBox.ascx'' then (select top 1 Nev
				from KRT_Felhasznalok
				where Id=EREC_ObjektumTargyszavai.Ertek
					and getdate() between ErvKezd and ErvVege)
		when ''~/Component/KodTarakDropDownList.ascx'' then (select Nev
				from KRT_KodTarak
				where KodCsoport_Id = (select top 1 Id from KRT_KodCsoportok
						where Kod=IsNull(oma.ControlTypeDataSource, tsz.ControlTypeDataSource) collate Hungarian_CS_AS
						and getdate() between ErvKezd and ErvVege)
				and Org=@Org
				and Kod=EREC_ObjektumTargyszavai.Ertek collate Hungarian_CS_AS
				and getdate() between ErvKezd and ErvVege)
		else EREC_ObjektumTargyszavai.Ertek end as ErtekByControlSourceType,
		oma.Funkcio as Funkcio,
		EREC_ObjektumTargyszavai.ErvKezd as ErvKezd,
		EREC_ObjektumTargyszavai.ErvVege as ErvVege,
		EREC_ObjektumTargyszavai.Ver as Ver,
		EREC_ObjektumTargyszavai.Note as Note,
		''0'' as FromMetaDefinicio,
		null as AlapertelmezettErtek,
		oma.Opcionalis as Opcionalis,
		oma.Ismetlodo as Ismetlodo
	from EREC_ObjektumTargyszavai
	left join EREC_Obj_MetaAdatai oma on EREC_ObjektumTargyszavai.Obj_Metaadatai_Id = oma.Id
	left join EREC_TargySzavak tsz on EREC_ObjektumTargyszavai.Targyszo_Id = tsz.Id
	where EREC_ObjektumTargyszavai.Obj_Id = @Obj_Id
		and EREC_ObjektumTargyszavai.Id not in (select #Obj_MetaAdatai_Targyszavak.Id from #Obj_MetaAdatai_Targyszavak where #Obj_MetaAdatai_Targyszavak.Id is not null)
		and getdate() between EREC_ObjektumTargyszavai.ErvKezd and EREC_ObjektumTargyszavai.ErvVege
'
		end -- if @CsakAutomatikus = '0'
end

    set @sqlcmd = @sqlcmd + 'select ' + @LocalTopRow + ' * from #Obj_MetaAdatai_Targyszavak as EREC_ObjektumTargyszavai
'
    if @Where is not null and @Where!=''
    begin 
        set @sqlcmd = @sqlcmd + ' Where ' + @Where
    end
     
   
   set @sqlcmd = @sqlcmd + @OrderBy;

---- nyomtatási blokk kezdete
--   if exists (select 1 from  sysobjects where id = object_id('sp_PrintText') and type = 'P')
--   begin
--       exec dbo.sp_PrintText @sqlcmd;
--   end
---- nyomtatási blokk vége

	execute sp_executesql @sqlcmd,N'@Org uniqueidentifier, @DefinicioTipus nvarchar(2), @Obj_Id uniqueidentifier, @ObjTip_Id uniqueidentifier, @kt_Automatikus nvarchar(64), @kt_Automatikus_ForrasNev nvarchar(400)',
		@Org = @Org, @DefinicioTipus = @DefinicioTipus, @Obj_Id  = @Obj_Id, @ObjTip_Id = @ObjTip_Id, @kt_Automatikus = @kt_Automatikus, @kt_Automatikus_ForrasNev = @kt_Automatikus_ForrasNev;

END TRY
BEGIN CATCH
        declare @errorSeverity INT, @errorState INT
        declare @errorCode NVARCHAR(1000)    
        SET @errorSeverity = ERROR_SEVERITY()
        SET @errorState = ERROR_STATE()
        
        if ERROR_NUMBER()<50000        
                SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
        else
                SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

        RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
