IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByObjMetaDefinicio]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByObjMetaDefinicio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByObjMetaDefinicio]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByObjMetaDefinicio] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByObjMetaDefinicio]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by Sorszam',
  @TopRow nvarchar(5) = '',
  @ObjMetaDefinicio_Id uniqueidentifier = null,
  @TableName nvarchar(100) = null,
  @ColumnName nvarchar(100) = null,
  @ColumnValuesInclude nvarchar(400) = null,  -- in operátorhoz használható formában megadott felsorolás, pozitív szurés, @ColumnValuesExclude csak akkor érvényesül, ha ez null vagy üres
  @ColumnValuesExclude nvarchar(400) = null,  -- in operátorhoz használható formában megadott felsorolás, negatív szurés, @ColumnValuesInclude mellett nem érvényesül
  @DefinicioTipus nvarchar(2) = null, -- OBJMETADEFINICIO_TIPUS @DefinicioTipus = 'B1' (A0, B1, B2, C2 lehet, A0 objektum típus nélkül megadva, vagy mint felettes, ha @CsakSajatSzint='0')
  @CsakSajatSzint varchar(1) = '0', -- '0' minden szinten hozzárendelés, '1' csak a saját szinten
  @CsakAutomatikus varchar(1) = '1', -- '0' hozzáveszi a kézi és listás tárgyszavakat is,
                                     -- '1' csak a metadefiníció alapján létrejött hozzárendelések vizsgálata
  @ExecutorUserId uniqueidentifier,
  @Obj_Id    uniqueidentifier = null -- az adott objektum egy rekordjának azonosítója (Id)

as

begin

BEGIN TRY

    set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
    declare @sqlcmd nvarchar(MAX)
	set @sqlcmd = ''

    declare @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
   begin
       set @LocalTopRow = ''
   end
   else
   begin
       set @LocalTopRow = ' TOP ' + @TopRow
   end

   declare @kt_Automatikus nvarchar(64)
   declare @kt_Automatikus_ForrasNev nvarchar(400)
   set @kt_Automatikus = '01' -- Automatikus (kcs: TARGYSZO_FORRAS)
   set @kt_Automatikus_ForrasNev = dbo.fn_KodtarErtekNeve('TARGYSZO_FORRAS', @kt_Automatikus,@Org)

  if @ObjMetaDefinicio_Id is null and @TableName is null and @DefinicioTipus is null
	RAISERROR('[56104]',16,1) -- Nincs megadva a ContentType!
-- TODO Nem azonosítható az objektum metadefiníció (nincs megadva az azonosító vagy az objektum leíró adatok)

  declare @table_id uniqueidentifier
  declare @column_id uniqueidentifier

  if @ObjMetaDefinicio_Id is not null
  begin
	if not exists(select 1 from EREC_Obj_MetaDefinicio
		where Id=@ObjMetaDefinicio_Id
		and Org=@Org
		and getdate() between ErvKezd and ErvVege)
			RAISERROR('[56104]',16,1) -- Nincs megadva a ContentType!
-- TODO: Nem azonosítható az objektum metadefiníció!

		set @sqlcmd = @sqlcmd + '
		select Id into #Obj_MetaDefiniciokTopLevel
		    from EREC_Obj_MetaDefinicio omd
		    where omd.Id = @ObjMetaDefinicio_Id
			and omd.Org=@Org
'
	
  end
  else if @TableName is not null
  begin
	if @TableName != '*'
	begin
		set @table_id = (select top 1 Id from KRT_ObjTipusok where Kod = @TableName and ObjTipus_Id_Tipus =
						   (select top 1 Id from KRT_ObjTipusok where Kod = 'dbtable'))

		if @table_id is null
			RAISERROR('[56102]',16,1) -- Nem azonosítható az objektum típusa (nem létezo kód)!

		if @ColumnName is not null
		begin
			if @ColumnName != '*'
			begin
				set @column_id = (select top 1 Id from KRT_ObjTipusok where Kod = @ColumnName
										and Obj_Id_Szulo = @table_id
										and ObjTipus_Id_Tipus = (select top 1 Id from KRT_ObjTipusok
												where Kod = 'dbcolumn'))

				if @column_id is null
					RAISERROR('[56102]',16,1) -- Nem azonosítható az objektum típusa (nem létezo kód)!

				set @sqlcmd = @sqlcmd + '
	select omd.Id into #Obj_MetaDefiniciokTopLevel
		    from EREC_Obj_MetaDefinicio omd
		    where omd.Objtip_Id = @table_id and omd.Objtip_Id_Column = @column_id
			and omd.Org=@Org
			and getdate() between ErvKezd and ErvVege
			'
				if @ColumnValuesInclude is not null and @ColumnValuesInclude <> ''
				begin
					set @sqlcmd = @sqlcmd + 'and omd.ColumnValue in (' + @ColumnValuesInclude + ')
'
				end
				else if @ColumnValuesExclude is not null and @ColumnValuesExclude <> ''
				begin
					set @sqlcmd = @sqlcmd + 'and omd.ColumnValue not in (' + @ColumnValuesExclude + ')
'
				end
			end
			else -- if @ColumnName = '*'
			begin
				set @sqlcmd = @sqlcmd + '
		select omd.Id into #Obj_MetaDefiniciokTopLevel
				from EREC_Obj_MetaDefinicio omd
				where omd.Objtip_Id = @table_id
				and omd.Org=@Org
				and getdate() between ErvKezd and ErvVege
'
			end
		end
		else -- if @ColumnName is null
		begin
			set @sqlcmd = @sqlcmd + '
		select omd.Id into #Obj_MetaDefiniciokTopLevel
				  from EREC_Obj_MetaDefinicio omd
				  where omd.Objtip_Id = @table_id
					and omd.Objtip_Id_Column is null
					and omd.Org=@Org
					and getdate() between ErvKezd and ErvVege
'
		end
	end
	else -- if @TableName = '*'
	begin
		set @sqlcmd = @sqlcmd + '
		select omd.Id into #Obj_MetaDefiniciokTopLevel
				from EREC_Obj_MetaDefinicio omd
				where getdate() between ErvKezd and ErvVege
				and Org=@Org
'
	end

  end
  else if @DefinicioTipus is not null
  begin
		set @sqlcmd = @sqlcmd + '
		select omd.Id into #Obj_MetaDefiniciokTopLevel
				  from EREC_Obj_MetaDefinicio omd
				  where omd.DefinicioTipus = @DefinicioTipus
					and Org=@Org
					and getdate() between ErvKezd and ErvVege
'	
  end

  -- Végso Objektum metadefiníció select felépítése
  if @CsakSajatSzint = '0'
  begin
	set @sqlcmd = @sqlcmd + '
		;with OMD_Hierarchia as
		(
		    -- Base case
		    select Id, Felettes_Obj_Meta, IsNull(Objtip_Id_Column, Objtip_Id) as ObjTip_Id, 0 as Szint
		    from EREC_Obj_MetaDefinicio
		    where Id in (select Id from #Obj_MetaDefiniciokTopLevel)
		    and getdate() between ErvKezd and ErvVege

		    union all

		    -- Recursive step
			-- az ObjTip_Id-t örököltetjük a felettesekre is
		    select omd.Id, omd.Felettes_Obj_Meta, omdh.ObjTip_Id, omdh.Szint + 1 as Szint
		    from EREC_Obj_MetaDefinicio as omd
		    join OMD_Hierarchia as omdh
		    on omdh.Felettes_Obj_Meta = omd.Id
		    and getdate() between omd.ErvKezd and omd.ErvVege
					and (IsNull(@DefinicioTipus, '''') = ''''
						    or
						 omd.DefinicioTipus = @DefinicioTipus)
		)
		select distinct Id, ObjTip_Id into #Obj_MetaDefiniciok from OMD_Hierarchia
'
  end
  else
  begin
	set @sqlcmd = @sqlcmd + '
	select distinct omd.Id, IsNull(omd.Objtip_Id_Column, omd.Objtip_Id) as ObjTip_Id into #Obj_MetaDefiniciok
	from #Obj_MetaDefiniciokTopLevel inner join EREC_Obj_MetaDefinicio omd on #Obj_MetaDefiniciokTopLevel.Id=omd.Id
'
  end

	if @DefinicioTipus is not null
	begin
	set @sqlcmd = @sqlcmd + '
	delete from #Obj_MetaDefiniciok
	where Id not in (select omd.Id from #Obj_MetaDefiniciok
		inner join EREC_Obj_MetaDefinicio omd on #Obj_MetaDefiniciok.Id=omd.Id and omd.DefinicioTipus=@DefinicioTipus)
'		
	end

	set @sqlcmd = @sqlcmd + '
	select oma.Id, #Obj_MetaDefiniciok.ObjTip_Id
		into #Obj_MetaAdatai
		from EREC_Obj_MetaAdatai oma
		inner join #Obj_MetaDefiniciok
		on oma.Obj_MetaDefinicio_Id = #Obj_MetaDefiniciok.Id
		where oma.Obj_MetaDefinicio_Id in (select Id from #Obj_MetaDefiniciok)
		and getdate() between oma.ErvKezd and oma.ErvVege
'

-- Kapcsolt tárgyszavak lekérése
   if @Obj_Id is null
   begin
	set @sqlcmd = @sqlcmd + '
	select 
		convert(uniqueidentifier, null) as Id,
		#Obj_MetaAdatai.ObjTip_Id as ObjTip_Id,
		convert(nvarchar(100), null) as Ertek,
		EREC_Obj_MetaAdatai.Id as Obj_Metaadatai_Id,
		EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id as Obj_MetaDefinicio_Id,
		(select DefinicioTipus from EREC_Obj_MetaDefinicio where EREC_Obj_MetaDefinicio.Id = EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id) as DefinicioTipus,
		(select ContentType from EREC_Obj_MetaDefinicio where EREC_Obj_MetaDefinicio.Id = EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id) as ContentType,
		EREC_Obj_MetaAdatai.SPSSzinkronizalt as SPSSzinkronizalt,
		EREC_Obj_MetaAdatai.Targyszavak_Id as Targyszo_Id,
		EREC_Obj_MetaAdatai.Sorszam,
		@kt_Automatikus as Forras,
	@kt_Automatikus_ForrasNev as ForrasNev,
		EREC_TargySzavak.TargySzavak as Targyszo,
		EREC_TargySzavak.Csoport_Id_Tulaj,
		EREC_TargySzavak.Tipus,
		EREC_TargySzavak.BelsoAzonosito,
		EREC_TargySzavak.RegExp,
		EREC_TargySzavak.ToolTip,
		IsNull(EREC_Obj_MetaAdatai.ControlTypeSource, EREC_TargySzavak.ControlTypeSource) as ControlTypeSource,
		IsNull(EREC_Obj_MetaAdatai.ControlTypeDataSource, EREC_TargySzavak.ControlTypeDataSource) as ControlTypeDataSource,
		EREC_Obj_MetaAdatai.Funkcio,
		EREC_Obj_MetaAdatai.ErvKezd,
		EREC_Obj_MetaAdatai.ErvVege,
		0 as Ver,
		convert(nvarchar(4000), null) as Note,
		convert(char(1), ''1'') as FromMetaDefinicio,
		IsNull(EREC_Obj_MetaAdatai.AlapertelmezettErtek, EREC_TargySzavak.AlapertelmezettErtek) as AlapertelmezettErtek,
		EREC_Obj_MetaAdatai.Opcionalis as Opcionalis,
		EREC_Obj_MetaAdatai.Ismetlodo as Ismetlodo
   into #Obj_MetaAdatai_Targyszavak
   from 
       #Obj_MetaAdatai
	   inner join EREC_Obj_MetaAdatai
       on #Obj_MetaAdatai.Id = EREC_Obj_MetaAdatai.Id
       inner join EREC_TargySzavak as EREC_TargySzavak
       on EREC_Obj_MetaAdatai.Targyszavak_Id = EREC_TargySzavak.Id and EREC_TargySzavak.Org=@Org
       --and getdate() between EREC_Obj_MetaAdatai.ErvKezd and EREC_Obj_MetaAdatai.ErvVege
       and getdate() between EREC_TargySzavak.ErvKezd and EREC_TargySzavak.ErvVege
'
	end
	else --   if @Obj_Id is not null
   begin
	-- Összefésülés a valódi objektum tárgyszavai hozzárendelésekkel, ha van objektum
	set @sqlcmd = @sqlcmd + '
	select 
		convert(uniqueidentifier, ot.Id) as Id,
		case when ot.Id is null then #Obj_MetaAdatai.ObjTip_Id else ot.Objtip_Id end as ObjTip_Id,
		case when ot.Id is null then convert(nvarchar(100), null) else ot.Ertek end as Ertek,
		EREC_Obj_MetaAdatai.Id as Obj_Metaadatai_Id,
		EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id as Obj_MetaDefinicio_Id,
		(select DefinicioTipus from EREC_Obj_MetaDefinicio where EREC_Obj_MetaDefinicio.Id = EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id) as DefinicioTipus,
		(select ContentType from EREC_Obj_MetaDefinicio where EREC_Obj_MetaDefinicio.Id = EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id) as ContentType,
		case when ot.Id is null then EREC_Obj_MetaAdatai.SPSSzinkronizalt else ot.SPSSzinkronizalt end as SPSSzinkronizalt,
		case when ot.Id is null then EREC_Obj_MetaAdatai.Targyszavak_Id else ot.Targyszo_Id end as Targyszo_Id,
		case when ot.Id is null then EREC_Obj_MetaAdatai.Sorszam else ot.Sorszam end as Sorszam,
		case when ot.Id is null then @kt_Automatikus else ot.Forras end as Forras,
		case when ot.Id is null then @kt_Automatikus_ForrasNev else dbo.fn_KodtarErtekNeve(''TARGYSZO_FORRAS'',ot.Forras,@Org) end as ForrasNev,
		EREC_TargySzavak.TargySzavak as Targyszo,
		EREC_TargySzavak.Csoport_Id_Tulaj,
		EREC_TargySzavak.Tipus,
		EREC_TargySzavak.BelsoAzonosito,
		EREC_TargySzavak.RegExp,
		EREC_TargySzavak.ToolTip,
		IsNull(EREC_Obj_MetaAdatai.ControlTypeSource, EREC_TargySzavak.ControlTypeSource) as ControlTypeSource,
		IsNull(EREC_Obj_MetaAdatai.ControlTypeDataSource, EREC_TargySzavak.ControlTypeDataSource) as ControlTypeDataSource,
		EREC_Obj_MetaAdatai.Funkcio,
		case when ot.Id is null then EREC_Obj_MetaAdatai.ErvKezd else ot.ErvKezd end as ErvKezd,
		case when ot.Id is null then EREC_Obj_MetaAdatai.ErvVege else ot.ErvVege end as ErvVege,
		case when ot.Id is null then 0 else ot.Ver end as Ver,
		case when ot.Id is null then convert(nvarchar(4000), null) else ot.Note end as Note,
		convert(char(1), ''1'') as FromMetaDefinicio,
		IsNull(EREC_Obj_MetaAdatai.AlapertelmezettErtek, EREC_TargySzavak.AlapertelmezettErtek) as AlapertelmezettErtek,
		EREC_Obj_MetaAdatai.Opcionalis as Opcionalis,
		EREC_Obj_MetaAdatai.Ismetlodo as Ismetlodo
   into #Obj_MetaAdatai_Targyszavak
   from 
       #Obj_MetaAdatai
	   inner join EREC_Obj_MetaAdatai
       on #Obj_MetaAdatai.Id = EREC_Obj_MetaAdatai.Id
       inner join EREC_TargySzavak as EREC_TargySzavak
       on EREC_Obj_MetaAdatai.Targyszavak_Id = EREC_TargySzavak.Id and EREC_TargySzavak.Org=@Org
       and getdate() between EREC_TargySzavak.ErvKezd and EREC_TargySzavak.ErvVege
	   left join EREC_ObjektumTargyszavai ot on ot.Obj_Id = @Obj_Id and ot.Obj_MetaAdatai_Id = EREC_Obj_MetaAdatai.Id and ot.Targyszo_Id = EREC_TargySzavak.Id and getdate() between ot.ErvKezd and ot.ErvVege
'

		if @CsakAutomatikus = '0'
		begin
			set @sqlcmd = @sqlcmd + '
alter table #Obj_MetaAdatai_Targyszavak alter column Obj_Metaadatai_Id uniqueidentifier null
alter table #Obj_MetaAdatai_Targyszavak alter column Targyszo_Id uniqueidentifier null
alter table #Obj_MetaAdatai_Targyszavak alter column Csoport_Id_Tulaj uniqueidentifier null
alter table #Obj_MetaAdatai_Targyszavak alter column Obj_MetaDefinicio_Id uniqueidentifier null
alter table #Obj_MetaAdatai_Targyszavak alter column Sorszam int null
'
			set @sqlcmd = @sqlcmd + '
insert into #Obj_MetaAdatai_Targyszavak
	select EREC_ObjektumTargyszavai.Id as Id,
		EREC_ObjektumTargyszavai.ObjTip_Id as ObjTip_Id,
		EREC_ObjektumTargyszavai.Ertek as Ertek,
		EREC_ObjektumTargyszavai.Obj_Metaadatai_Id as Obj_Metaadatai_Id,
		null as Obj_MetaDefinicio_Id,
		null as DefinicioTipus,
		null as ContentType,
		EREC_ObjektumTargyszavai.SPSSzinkronizalt as SPSSzinkronizalt,
		EREC_ObjektumTargyszavai.Targyszo_Id as Targyszo_Id,
		EREC_ObjektumTargyszavai.Sorszam as Sorszam,
		EREC_ObjektumTargyszavai.Forras as Forras,
		dbo.fn_KodtarErtekNeve(''TARGYSZO_FORRAS'', EREC_ObjektumTargyszavai.Forras,@Org) as ForrasNev,
		EREC_ObjektumTargyszavai.Targyszo as Targyszo,
		tsz.Csoport_Id_Tulaj as Csoport_Id_Tulaj,
		IsNull(tsz.Tipus, ''0'') as Tipus,
		tsz.BelsoAzonosito as BelsoAzonosito,
		tsz.RegExp as RegExp,
		tsz.ToolTip as ToolTip,
		IsNull(oma.ControlTypeSource, tsz.ControlTypeSource) as ControlTypeSource,
		IsNull(oma.ControlTypeDataSource, tsz.ControlTypeDataSource) as ControlTypeDataSource,
		oma.Funkcio as Funkcio,
		EREC_ObjektumTargyszavai.ErvKezd as ErvKezd,
		EREC_ObjektumTargyszavai.ErvVege as ErvVege,
		EREC_ObjektumTargyszavai.Ver as Ver,
		EREC_ObjektumTargyszavai.Note as Note,
		''0'' as FromMetaDefinicio,
		null as AlapertelmezettErtek,
		oma.Opcionalis as Opcionalis,
		oma.Ismetlodo as Ismetlodo
	from EREC_ObjektumTargyszavai
	left join EREC_Obj_MetaAdatai oma on EREC_ObjektumTargyszavai.Obj_Metaadatai_Id = oma.Id
	left join EREC_TargySzavak tsz on EREC_ObjektumTargyszavai.Targyszo_Id = tsz.Id
	where EREC_ObjektumTargyszavai.Obj_Id = @Obj_Id
		and EREC_ObjektumTargyszavai.Id not in (select #Obj_MetaAdatai_Targyszavak.Id from #Obj_MetaAdatai_Targyszavak where #Obj_MetaAdatai_Targyszavak.Id is not null)
		and getdate() between EREC_ObjektumTargyszavai.ErvKezd and EREC_ObjektumTargyszavai.ErvVege
'
		end -- if @CsakAutomatikus = '0'
end

    set @sqlcmd = @sqlcmd + 'select ' + @LocalTopRow + ' * from #Obj_MetaAdatai_Targyszavak as EREC_ObjektumTargyszavai
'
    if @Where is not null and @Where!=''
    begin 
		set @sqlcmd = @sqlcmd + ' Where ' + @Where
    end
     
   
   set @sqlcmd = @sqlcmd + @OrderBy;

---- nyomtatási blokk kezdete
--   if exists (select 1 from  sysobjects where id = object_id('sp_PrintText') and type = 'P')
--   begin
--       exec dbo.sp_PrintText @sqlcmd;
--   end
---- nyomtatási blokk vége

	execute sp_executesql @sqlcmd,N'@Org uniqueidentifier, @ObjMetaDefinicio_Id uniqueidentifier, @DefinicioTipus nvarchar(2), @Obj_Id uniqueidentifier, @table_id uniqueidentifier, @column_id uniqueidentifier, @kt_Automatikus nvarchar(64), @kt_Automatikus_ForrasNev nvarchar(400)',
		@Org = @Org, @ObjMetaDefinicio_Id = @ObjMetaDefinicio_Id, @DefinicioTipus = @DefinicioTipus, @Obj_Id = @Obj_Id, @table_id = @table_id, @column_id = @column_id, @kt_Automatikus = @kt_Automatikus, @kt_Automatikus_ForrasNev = @kt_Automatikus_ForrasNev;;

END TRY
BEGIN CATCH
        declare @errorSeverity INT, @errorState INT
        declare @errorCode NVARCHAR(1000)    
        SET @errorSeverity = ERROR_SEVERITY()
        SET @errorState = ERROR_STATE()
        
        if ERROR_NUMBER()<50000        
                SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
        else
                SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

        RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
