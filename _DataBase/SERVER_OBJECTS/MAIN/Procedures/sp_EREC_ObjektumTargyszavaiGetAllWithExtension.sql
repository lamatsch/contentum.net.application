IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_ObjektumTargyszavaiGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by EREC_ObjektumTargyszavai.Sorszam',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end 

   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END   
          
  SET @sqlcmd = 
'

select ' + @LocalTopRow + '
       EREC_ObjektumTargyszavai.Id,
       EREC_ObjektumTargyszavai.Targyszo_Id,
       EREC_ObjektumTargyszavai.Obj_Id,
       EREC_ObjektumTargyszavai.ObjTip_Id,
       EREC_ObjektumTargyszavai.Obj_Metaadatai_Id,
       EREC_ObjektumTargyszavai.Targyszo,
       EREC_ObjektumTargyszavai.Ertek,
       EREC_ObjektumTargyszavai.Sorszam,
       EREC_ObjektumTargyszavai.Forras,
       dbo.fn_KodtarErtekNeve(''TARGYSZO_FORRAS'', EREC_ObjektumTargyszavai.Forras,''' + cast(@Org as NVarChar(40)) + ''') as ForrasNev,
       EREC_ObjektumTargyszavai.Note,
       EREC_TargySzavak.Tipus,
       EREC_TargySzavak.BelsoAzonosito,
       EREC_TargySzavak.RegExp,
       EREC_TargySzavak.ToolTip,
       EREC_TargySzavak.SPSSzinkronizalt,
       EREC_Obj_MetaAdatai.Funkcio,
       EREC_Obj_MetaAdatai.Opcionalis,
       EREC_Obj_MetaAdatai.Ismetlodo
  from dbo.EREC_ObjektumTargyszavai as EREC_ObjektumTargyszavai
	LEFT JOIN EREC_TargySzavak ON EREC_ObjektumTargyszavai.Targyszo_Id = EREC_TargySzavak.Id and EREC_TargySzavak.Org=''' + cast(@Org as NVarChar(40)) + '''
	LEFT JOIN EREC_Obj_MetaAdatai ON EREC_ObjektumTargyszavai.Obj_Metaadatai_Id = EREC_Obj_MetaAdatai.Id
 where EREC_ObjektumTargyszavai.Targyszo_Id is null or EREC_ObjektumTargyszavai.Targyszo_Id=EREC_TargySzavak.Id
'
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
