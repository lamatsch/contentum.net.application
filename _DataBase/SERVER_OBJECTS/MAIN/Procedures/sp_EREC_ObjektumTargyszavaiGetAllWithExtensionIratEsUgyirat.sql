IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiGetAllWithExtensionIratEsUgyirat]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiGetAllWithExtensionIratEsUgyirat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiGetAllWithExtensionIratEsUgyirat]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiGetAllWithExtensionIratEsUgyirat] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_ObjektumTargyszavaiGetAllWithExtensionIratEsUgyirat]
  @Irat_Id uniqueidentifier,
  @Where nvarchar(4000) = '', -- nem használjuk fel
  @OrderBy nvarchar(200) = ' order by EREC_ObjektumTargyszavai.Sorszam',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)
    
          
  SET @sqlcmd = 
'
declare @ObjTipId_Irat uniqueidentifier
declare @ObjTipId_Ugyirat uniqueidentifier
declare @ObjId_Irat uniqueidentifier
declare @ObjId_Ugyirat uniqueidentifier
declare @WhereParam_Irat nvarchar(MAX)
declare @WhereParam_Ugyirat nvarchar(MAX)

set @ObjTipId_Irat = (select Id from KRT_ObjTipusok WHERE Kod=''EREC_IraIratok'')
set @ObjTipId_Ugyirat = (select Id from KRT_ObjTipusok WHERE Kod=''EREC_UgyUgyiratok'')
set @ObjId_Irat = ''' + convert(nvarchar(36), @Irat_Id) + '''
set @ObjId_Ugyirat = (select uu.Id from
(select Id from EREC_UgyUgyiratok where getdate() between ErvKezd and ErvVege) as uu
    JOIN
    (select Id, UgyUgyirat_Id from EREC_UgyUgyiratDarabok where getdate() between ErvKezd and ErvVege) as uud
    ON
    uu.Id = uud.UgyUgyirat_Id
    JOIN
    (select Id, UgyUgyiratDarab_Id from EREC_IraIratok where  getdate() between ErvKezd and ErvVege) as ii
    ON
    uud.Id = ii.UgyUgyiratDarab_Id
 where ii.Id = @ObjId_Irat)

set @WhereParam_Irat=''(((EREC_ObjektumTargyszavai.Obj_Id = '''''' + convert(nvarchar(36), @ObjId_Irat) + '''''') and (EREC_ObjektumTargyszavai.ObjTip_Id = '''''' + convert(nvarchar(36), @ObjTipId_Irat) + '''''') and (EREC_ObjektumTargyszavai.ErvKezd <= getdate()) and (EREC_ObjektumTargyszavai.ErvVege >= getdate())))''
set @WhereParam_Ugyirat=''(((EREC_ObjektumTargyszavai.Obj_Id = '''''' + convert(nvarchar(36), @ObjId_Ugyirat) + '''''') and (EREC_ObjektumTargyszavai.ObjTip_Id = '''''' + convert(nvarchar(36), @ObjTipId_Ugyirat) + '''''') and (EREC_ObjektumTargyszavai.ErvKezd <= getdate()) and (EREC_ObjektumTargyszavai.ErvVege >= getdate())))''

exec [sp_EREC_ObjektumTargyszavaiGetAllWithExtension]
@Where=@WhereParam_Irat,
@OrderBy=N''' + @OrderBy + ''',
@TopRow=N''' + @TopRow + ''',
@ExecutorUserId=''' + convert(nvarchar(36), @ExecutorUserId) + '''

exec [sp_EREC_ObjektumTargyszavaiGetAllWithExtension]
@Where=@WhereParam_Ugyirat,
@OrderBy=N''' + @OrderBy + ''',
@TopRow=N''' + @TopRow + ''',
@ExecutorUserId=''' + convert(nvarchar(36), @ExecutorUserId) + ''''

   exec (@sqlcmd);


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
