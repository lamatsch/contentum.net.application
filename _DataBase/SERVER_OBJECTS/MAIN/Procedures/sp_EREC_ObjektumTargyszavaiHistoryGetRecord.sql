IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiHistoryGetRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiHistoryGetRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiHistoryGetRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiHistoryGetRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_ObjektumTargyszavaiHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin
select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_ObjektumTargyszavaiHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_ObjektumTargyszavaiHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_ObjektumTargyszavaiHistory Old
         inner join EREC_ObjektumTargyszavaiHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Targyszo_Id' as ColumnName,               
               cast(Old.Targyszo_Id as nvarchar(99)) as OldValue,
               cast(New.Targyszo_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_ObjektumTargyszavaiHistory Old
         inner join EREC_ObjektumTargyszavaiHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Targyszo_Id != New.Targyszo_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Metaadatai_Id' as ColumnName,               
               cast(Old.Obj_Metaadatai_Id as nvarchar(99)) as OldValue,
               cast(New.Obj_Metaadatai_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_ObjektumTargyszavaiHistory Old
         inner join EREC_ObjektumTargyszavaiHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_Metaadatai_Id != New.Obj_Metaadatai_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Id' as ColumnName,               
               cast(Old.Obj_Id as nvarchar(99)) as OldValue,
               cast(New.Obj_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_ObjektumTargyszavaiHistory Old
         inner join EREC_ObjektumTargyszavaiHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_Id != New.Obj_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ObjTip_Id' as ColumnName,               
               cast(Old.ObjTip_Id as nvarchar(99)) as OldValue,
               cast(New.ObjTip_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_ObjektumTargyszavaiHistory Old
         inner join EREC_ObjektumTargyszavaiHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ObjTip_Id != New.ObjTip_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Targyszo' as ColumnName,               
               cast(Old.Targyszo as nvarchar(99)) as OldValue,
               cast(New.Targyszo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_ObjektumTargyszavaiHistory Old
         inner join EREC_ObjektumTargyszavaiHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Targyszo != New.Targyszo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Forras' as ColumnName,               
               cast(Old.Forras as nvarchar(99)) as OldValue,
               cast(New.Forras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_ObjektumTargyszavaiHistory Old
         inner join EREC_ObjektumTargyszavaiHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Forras != New.Forras 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SPSSzinkronizalt' as ColumnName,               
               cast(Old.SPSSzinkronizalt as nvarchar(99)) as OldValue,
               cast(New.SPSSzinkronizalt as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_ObjektumTargyszavaiHistory Old
         inner join EREC_ObjektumTargyszavaiHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SPSSzinkronizalt != New.SPSSzinkronizalt 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ertek' as ColumnName,               
               cast(Old.Ertek as nvarchar(99)) as OldValue,
               cast(New.Ertek as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_ObjektumTargyszavaiHistory Old
         inner join EREC_ObjektumTargyszavaiHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Ertek != New.Ertek 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Sorszam' as ColumnName,               
               cast(Old.Sorszam as nvarchar(99)) as OldValue,
               cast(New.Sorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_ObjektumTargyszavaiHistory Old
         inner join EREC_ObjektumTargyszavaiHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Sorszam != New.Sorszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      /*
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Targyszo_XML' as ColumnName,               
               cast(Old.Targyszo_XML as nvarchar(99)) as OldValue,
               cast(New.Targyszo_XML as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_ObjektumTargyszavaiHistory Old
         inner join EREC_ObjektumTargyszavaiHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Targyszo_XML != New.Targyszo_XML 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId) 
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Targyszo_XML_Ervenyes' as ColumnName,               
               cast(Old.Targyszo_XML_Ervenyes as nvarchar(99)) as OldValue,
               cast(New.Targyszo_XML_Ervenyes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_ObjektumTargyszavaiHistory Old
         inner join EREC_ObjektumTargyszavaiHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Targyszo_XML_Ervenyes != New.Targyszo_XML_Ervenyes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)*/   ) t order by t.Ver ASC
end


GO
