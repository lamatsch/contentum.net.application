IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiInvalidateByDefinicioTipus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiInvalidateByDefinicioTipus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_ObjektumTargyszavaiInvalidateByDefinicioTipus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_ObjektumTargyszavaiInvalidateByDefinicioTipus] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_ObjektumTargyszavaiInvalidateByDefinicioTipus]
  @Obj_Id uniqueidentifier, -- az adott objektum egy rekordjának azonosítója (Id)
  @ObjTip_Id uniqueidentifier = null,
  @ObjTip_Kod nvarchar(100) = '',
  @DefinicioTipus nvarchar(2) = '', -- OBJMETADEFINICIO_TIPUS @DefinicioTipus = 'B1' (jelenleg B1 és C2 használt)
  @CsakSajatSzint varchar(1) = '0',        -- '0' minden szinten érvénytelenítés, '1' csak a saját szinten
  @CsakErvenytelen varchar(1) = '0', -- Ha '0', a metadefiníció alapján minden minden (a definíciótípusba tartozó) hozzárendelést érvénytelenít (módosítás elott célszeru)
                                     -- ha '1', megkeresi azon objektum metaadatokat, melyeket a jelenlegi állapotban nem rendelnénk hozzá (módosítás után célszeru)
  @ExecutorUserId uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByCsoportId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	END

   declare @kt_Automatikus nvarchar(64)
   set @kt_Automatikus = '01' -- Automatikus (kcs: TARGYSZO_FORRAS)

   -- Dátum
   declare @Now datetime
   set @Now = getdate()

---- struktúra kinyerése a temporary eredménytáblába
--   if OBJECT_ID('tempdb..#temp_ResultTable') is not null
--   begin
--       drop table #temp_ResultTable
--   end
--       select top 0 * into #temp_ResultTable 
--       from EREC_ObjektumTargyszavai


   if @Obj_Id is null
      RAISERROR('[56100]',16,1) -- Nincs megadva az objektum azonosítója!
   if @ObjTip_Id is null AND @ObjTip_Kod is null
      RAISERROR('[56101]',16,1) -- Nincs megadva az objektum típusa (azonosító vagy kód)!

   if (@ObjTip_Id IS null)
   begin
      SET @ObjTip_Id = (select top 1 Id from KRT_ObjTipusok where Kod = @ObjTip_Kod)
      if @ObjTip_Id is null
         RAISERROR('[56102]',16,1) -- Nem azonosítható az objektum típusa (nem létezo kód)!

   end
   else
   begin
     SET @ObjTip_Kod = (select Kod from KRT_ObjTipusok where Id = @ObjTip_Id)
      if @ObjTip_Kod is null
         RAISERROR('[56103]',16,1) -- Nem azonosítható az objektum típusa (nem létezo id)!
   end

-- érvénytelenítendo hozzárendelések kinyerése temporary eredménytáblába
   if OBJECT_ID('tempdb..#temp_ObjektumTargyszavai') is not null
   begin
       drop table #temp_ObjektumTargyszavai
   end
   -- insert-exec: le kell képezni a tárolt eljárás result setjének pontos szerkezetét!!!
   create table #temp_ObjektumTargyszavai
   (
      Id uniqueidentifier not null,
      ObjTip_Id uniqueidentifier null,
      Ertek nvarchar(100) null,
      Obj_Metaadatai_Id uniqueidentifier null,
      Obj_MetaDefinicio_Id uniqueidentifier null,
      DefinicioTipus char(2) null,
      ContentType nvarchar(100) null,
      SPSSzinkronizalt char(1) null,
      Targyszo_Id uniqueidentifier null,
      Sorszam int,
      Forras nvarchar(100) null,
      ForrasNev nvarchar(100) null,
      Targyszo nvarchar(100) null,
      Csoport_Id_Tulaj uniqueidentifier null,
      Tipus char(1) null,
      BelsoAzonosito nvarchar(100) null,
      RegExp nvarchar(4000) null,
      ToolTip nvarchar(400) null,
      ControlTypeSource nvarchar(64),
      ControlTypeDataSource nvarchar(4000),
      ErtekByControlSourceType nvarchar(100),
      Funkcio nvarchar(100) null,
      ErvKezd datetime,
      ErvVege datetime,
      Ver int,
      Note nvarchar(4000) null,
      FromMetaDefinicio char(1) null,
      AlapertelmezettErtek nvarchar(100) null,
      Opcionalis char(1) null,
      Ismetlodo char(1) null
    )

   insert into #temp_ObjektumTargyszavai
       exec sp_EREC_ObjektumTargyszavaiGetAllMetaByDefinicioTipus
           @Where=N' Id is not null and SPSSzinkronizalt != ''1''',
           @ExecutorUserId = @ExecutorUserId,
           @Obj_Id = @Obj_Id,
           @ObjTip_Id = @ObjTip_Id,
           @DefinicioTipus = @DefinicioTipus,
           @CsakSajatSzint = @CsakSajatSzint,
           @CsakAutomatikus='1'

   if OBJECT_ID('tempdb..#temp_ObjektumTargyszavaiToInvalidate') is not null
   begin
       drop table #temp_ObjektumTargyszavaiToInvalidate
   end

    create table #temp_ObjektumTargyszavaiToInvalidate
   (
      Id uniqueidentifier not null--,
      --SPSSzinkronizalt char(1) null
   )

    if @CsakErvenytelen = '1'
    begin
        insert into #temp_ObjektumTargyszavaiToInvalidate
            select EREC_ObjektumTargyszavai.Id--, EREC_ObjektumTargyszavai.SPSSzinkronizalt
            from EREC_ObjektumTargyszavai
        where
            EREC_ObjektumTargyszavai.Obj_Id = @Obj_Id
            and EREC_ObjektumTargyszavai.ObjTip_Id= @ObjTip_Id
            and EREC_ObjektumTargyszavai.Forras = @kt_Automatikus
            and EREC_ObjektumTargyszavai.SPSSzinkronizalt != '1'
            and getdate() between EREC_ObjektumTargyszavai.ErvKezd and EREC_ObjektumTargyszavai.ErvVege
            and exists(
                select omd.Id from EREC_Obj_MetaDefinicio omd
                where omd.Id in
                    (select oma.Obj_MetaDefinicio_Id from EREC_Obj_MetaAdatai oma
                     where oma.Id in
                        (select ot.Obj_Metaadatai_Id from EREC_ObjektumTargyszavai ot
                                 where ot.Id = EREC_ObjektumTargyszavai.Id
                         )
                         and getdate() between oma.ErvKezd and oma.ErvVege
                     )
					 and omd.Org=@Org
                     and getdate() between omd.ErvKezd and omd.ErvVege
                     and (
                            (@DefinicioTipus is null or @DefinicioTipus = '')
                             or
                             omd.DefinicioTipus = @DefinicioTipus
                        )
                     and omd.Id not in (select #temp_ObjektumTargyszavai.Obj_MetaDefinicio_Id from #temp_ObjektumTargyszavai)
                )
    end
    else -- @CsakErvenytelen = '0'
    begin
        insert into #temp_ObjektumTargyszavaiToInvalidate
             select Id--, SPSSzinkronizalt
             from #temp_ObjektumTargyszavai
			where SPSSzinkronizalt != '1'
				and getdate() < #temp_ObjektumTargyszavai.ErvVege
    end

                  
-- Érvényes hozzárendelések meghatározása
--    -- valtozok deklaralasa
--    declare @ObjektumTargyszavai_RecordId uniqueidentifier
--    
--    declare  @SPSSzinkronizalt char(1)
--
--        -- cursor deklaralasa
--        declare objektumtargyszavai_sor cursor local for
--                select Id, SPSSzinkronizalt from #temp_ObjektumTargyszavaiToInvalidate
--
--        open objektumtargyszavai_sor
--
--        -- ciklus kezdete elotti fetch
--        fetch objektumtargyszavai_sor into @ObjektumTargyszavai_RecordId, @SPSSzinkronizalt
--
--        while @@Fetch_Status = 0
--        begin
--           if (select Id from EREC_ObjektumTargyszavai
--                   where Id = @ObjektumTargyszavai_RecordId
--               ) is not null
--               and @SPSSzinkronizalt != '1'
--           begin
--               exec sp_EREC_ObjektumTargyszavaiInvalidate
--                   @Id = @ObjektumTargyszavai_RecordId,
--                   @ExecutorUserId = @ExecutorUserId,
--                   @ExecutionTime = @Now
--
--                   if (@@error <> 0)
--                       RAISERROR('[56111]',16,1) -- Hiba történt az automatikus metaadat érvénytelenítés során!
--                   else
--                   begin
--                       insert into #temp_ResultTable
--                           select * from EREC_ObjektumTargyszavai ot
--                               where ot.Id = @ObjektumTargyszavai_RecordId
--                   end
--           end
--           fetch objektumtargyszavai_sor into @ObjektumTargyszavai_RecordId, @SPSSzinkronizalt
--       end
--
--       close objektumtargyszavai_sor
--       deallocate objektumtargyszavai_sor

-- Feleslegessé vált hozzárendelések érvénytelnítése
		declare @updatedRowIds table (Id uniqueidentifier)


	if exists(select Id from #temp_ObjektumTargyszavaiToInvalidate)
	begin
		update EREC_ObjektumTargyszavai
		set ErvVege = @Now,
			ErvKezd = case when ErvKezd > @Now then @Now else ErvKezd end
		output inserted.Id into @updatedRowIds
		from #temp_ObjektumTargyszavaiToInvalidate
		inner join EREC_ObjektumTargyszavai
			on EREC_ObjektumTargyszavai.Id = #temp_ObjektumTargyszavaiToInvalidate.Id
		--where #temp_ObjektumTargyszavaiToInvalidate.SPSSzinkronizalt != '1'

		/* dinamikus history log összeállításhoz */
		declare @row_ids varchar(MAX)

		/*** EREC_ObjektumTargyszavaiHistory log ***/
		set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @updatedRowIds FOR XML PATH('')),1, 2, '')
		if @row_ids is not null
			set @row_ids = @row_ids + '''';

		exec [sp_LogRecordsToHistory_Tomeges] 
				 @TableName = 'EREC_ObjektumTargyszavai'
				,@Row_Ids = @row_ids
				,@Muvelet = 2
				,@Vegrehajto_Id = @ExecutorUserId
				,@VegrehajtasIdo = @Now

		-- érvénytelenített rekordok visszaadása
		select ot.* from EREC_ObjektumTargyszavai ot
			inner join @updatedRowIds u on ot.Id=u.Id
	end
	else
	begin
		-- csak oszlopnevek visszaadása (nem történt érvénytelenítés)
		select top 0 ot.* from EREC_ObjektumTargyszavai ot
	end

---- érvénytelenített rekordok visszaadása
--      select * from #temp_ResultTable
-- takarítás
       if OBJECT_ID('tempdb..#temp_ObjektumTargyszavai') is not null
       begin
           drop table #temp_ObjektumTargyszavai
       end

       if OBJECT_ID('tempdb..#temp_ObjektumTargyszavaiToInvalidate') is not null
       begin
           drop table #temp_ObjektumTargyszavaiToInvalidate
       end
       
--       if OBJECT_ID('tempdb..#temp_ResultTable') is not null 
--       begin
--          drop table #temp_ResultTable
--       end 


END TRY
BEGIN CATCH
        DECLARE @errorSeverity INT, @errorState INT
        DECLARE @errorCode NVARCHAR(1000)    
        SET @errorSeverity = ERROR_SEVERITY()
        SET @errorState = ERROR_STATE()
        
        if ERROR_NUMBER()<50000        
                SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
        else
                SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1
   
    

        RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
