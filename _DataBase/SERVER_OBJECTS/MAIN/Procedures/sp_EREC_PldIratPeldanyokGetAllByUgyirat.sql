IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_PldIratPeldanyokGetAllByUgyirat]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_PldIratPeldanyokGetAllByUgyirat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_PldIratPeldanyokGetAllByUgyirat]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_PldIratPeldanyokGetAllByUgyirat] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_PldIratPeldanyokGetAllByUgyirat]
  @Where nvarchar(MAX) = '',
  @PeldanyJoinFilter nvarchar(MAX) = '',
  @UgyiratWhere nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = '',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
       EREC_UgyUgyiratok.Id as Ugyirat_Id,
       EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo as Ugyirat_FelhasznaloCsoport_Id_Orzo,
       EREC_UgyUgyiratok.Csoport_Id_Felelos as Ugyirat_Csoport_Id_Felelos,
       EREC_UgyUgyiratok.Allapot as Ugyirat_Allapot,
       EREC_UgyUgyiratok.Foszam as Ugyirat_Foszam,
       EREC_IraIratok.Allapot as Irat_Allapot,
       EREC_IraIratok.Alszam as Irat_Alszam,
  	   EREC_PldIratPeldanyok.Id,
	   EREC_IraIratok.Id as IraIrat_Id,
	   EREC_PldIratPeldanyok.UgyUgyirat_Id_Kulso,
	   EREC_PldIratPeldanyok.Sorszam,
	   EREC_PldIratPeldanyok.SztornirozasDat,
	   EREC_PldIratPeldanyok.AtvetelDatuma,
	   EREC_PldIratPeldanyok.Eredet,
	   EREC_PldIratPeldanyok.KuldesMod,
	   EREC_PldIratPeldanyok.Ragszam,
	   EREC_PldIratPeldanyok.UgyintezesModja,
	   EREC_PldIratPeldanyok.VisszaerkezesiHatarido,
	   EREC_PldIratPeldanyok.Visszavarolag,
	   EREC_PldIratPeldanyok.VisszaerkezesDatuma,
	   EREC_PldIratPeldanyok.Cim_id_Cimzett,
	   EREC_PldIratPeldanyok.Partner_Id_Cimzett,
	   EREC_PldIratPeldanyok.Csoport_Id_Felelos,
	   EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo,
	   EREC_PldIratPeldanyok.Csoport_Id_Felelos_Elozo,
	   EREC_PldIratPeldanyok.CimSTR_Cimzett,
	   EREC_PldIratPeldanyok.NevSTR_Cimzett,
	   EREC_PldIratPeldanyok.Tovabbito,
	   EREC_PldIratPeldanyok.IraIrat_Id_Kapcsolt,
	   EREC_PldIratPeldanyok.IrattariHely,
	   EREC_PldIratPeldanyok.Gener_Id,
	   EREC_PldIratPeldanyok.PostazasDatuma,
	   EREC_PldIratPeldanyok.BarCode,
	   EREC_PldIratPeldanyok.Allapot,
	   EREC_PldIratPeldanyok.Kovetkezo_Felelos_Id,
	   EREC_PldIratPeldanyok.Elektronikus_Kezbesitesi_Allap,
	   EREC_PldIratPeldanyok.Kovetkezo_Orzo_Id,
	   EREC_PldIratPeldanyok.Fizikai_Kezbesitesi_Allapot,
	   EREC_PldIratPeldanyok.TovabbitasAlattAllapot,
	   EREC_PldIratPeldanyok.PostazasAllapot,
	   EREC_PldIratPeldanyok.ValaszElektronikus,
	   EREC_PldIratPeldanyok.Ver,
	   EREC_PldIratPeldanyok.Note,
	   EREC_PldIratPeldanyok.Stat_id,
	   EREC_PldIratPeldanyok.ErvKezd,
	   EREC_PldIratPeldanyok.ErvVege,
	   EREC_PldIratPeldanyok.Letrehozo_id,
	   EREC_PldIratPeldanyok.LetrehozasIdo,
	   EREC_PldIratPeldanyok.Modosito_id,
	   EREC_PldIratPeldanyok.ModositasIdo,
	   EREC_PldIratPeldanyok.Zarolo_id,
	   EREC_PldIratPeldanyok.ZarolasIdo,
	   EREC_PldIratPeldanyok.Tranz_id,
	   EREC_PldIratPeldanyok.UIAccessLog_id  
   from 
     [EREC_UgyUgyiratok] 
     JOIN [EREC_UgyUgyiratdarabok] ON [EREC_UgyUgyiratok].[Id] = [EREC_UgyUgyiratdarabok].[UgyUgyirat_Id]
     JOIN [EREC_IraIratok] ON [EREC_UgyUgyiratdarabok].[Id] = [EREC_IraIratok].[UgyUgyIratDarab_Id]
     LEFT JOIN [EREC_PldIratPeldanyok] ON [EREC_IraIratok].[Id] = [EREC_PldIratPeldanyok].[IraIrat_Id]   
     '
      
    if @PeldanyJoinFilter is not null and @PeldanyJoinFilter!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' AND ' + @PeldanyJoinFilter
	END
	
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
		if @UgyiratWhere is not null and @UgyiratWhere!=''
	    begin 
		    SET @sqlcmd = @sqlcmd + ' AND ' + @UgyiratWhere
	    end
	END
	ELSE
	BEGIN
	   if @UgyiratWhere is not null and @UgyiratWhere!=''
	   begin 
		  SET @sqlcmd = @sqlcmd + ' Where ' + @UgyiratWhere
	   end
    END 
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   
   --PRINT @sqlcmd;
   
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
