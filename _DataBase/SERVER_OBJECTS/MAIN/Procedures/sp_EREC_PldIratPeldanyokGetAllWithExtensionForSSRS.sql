IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_PldIratPeldanyokGetAllWithExtensionForSSRS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_PldIratPeldanyokGetAllWithExtensionForSSRS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_PldIratPeldanyokGetAllWithExtensionForSSRS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_PldIratPeldanyokGetAllWithExtensionForSSRS] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_PldIratPeldanyokGetAllWithExtensionForSSRS]

  @Where			nvarchar(MAX) = '',
  @OrderBy			nvarchar(400) = ' order by  EREC_IraIktatokonyvek.Iktatohely,EREC_IraIktatokonyvek.Ev,EREC_IraIratok.Alszam,EREC_PldIratPeldanyok.Sorszam',
  @ExecutorUserId	uniqueidentifier,
  @TopRow NVARCHAR(5) = ''

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  '		
select ' + @LocalTopRow + '
EREC_PldIratPeldanyok.Azonosito as IktatoSzam_Merge,
CONVERT(nvarchar(10), EREC_IraIratok.IktatasDatuma, 102) as IktatasDatuma,
EREC_IraIratok.Targy as EREC_IraIratok_Targy,
Allapot_Nev = 
		CASE EREC_PldIratPeldanyok.Allapot
			WHEN ''50'' THEN dbo.fn_KodtarErtekNeve(''IRATPELDANY_ALLAPOT'', EREC_PldIratPeldanyok.Allapot,''' + CAST(@Org as nVarChar(40)) + ''')  
						+ '' (''+dbo.fn_KodtarErtekNeve(''IRATPELDANY_ALLAPOT'', EREC_PldIratPeldanyok.TovabbitasAlattAllapot,''' + CAST(@Org as nVarChar(40)) + ''')+'')''			
			ELSE dbo.fn_KodtarErtekNeve(''IRATPELDANY_ALLAPOT'', EREC_PldIratPeldanyok.Allapot,''' + CAST(@Org as nVarChar(40)) + ''')
		END
FROM EREC_PldIratPeldanyok as EREC_PldIratPeldanyok
LEFT JOIN EREC_IraIratok as EREC_IraIratok
			ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
LEFT JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok
			ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
		LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok 
				ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id  
		LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek 
				ON EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id
Where EREC_IraIktatoKonyvek.Org=''' + CAST(@Org as nVarChar(40)) + '''
'
if @Where is not null and @Where!=''
	begin 
		--SET @sqlcmd = @sqlcmd + ' Where ' + @Where
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
	SET @sqlcmd = @sqlcmd + @OrderBy

   exec(@sqlcmd)


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
