IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_PldIratPeldanyokGetWithRightCheck]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_PldIratPeldanyokGetWithRightCheck]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_PldIratPeldanyokGetWithRightCheck]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_PldIratPeldanyokGetWithRightCheck] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_PldIratPeldanyokGetWithRightCheck]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FelhasznaloSzervezet_Id	uniqueidentifier,
		@Jogszint	char(1),
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	--if dbo.fn_IsAdmin(@ExecutorUserId) = 0 AND not exists
	IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0 AND not exists
	(
		SELECT top(1) 1 FROM krt_jogosultak
			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			where krt_jogosultak.Obj_Id = @Id
	) AND NOT EXISTS
	(
		SELECT 1 FROM EREC_PldIratpeldanyok
			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
				ON CsoportTagokAll.Id = EREC_PldIratpeldanyok.Csoport_Id_Felelos
			where EREC_PldIratpeldanyok.Id = @Id
	) AND NOT EXISTS
	(
		SELECT 1 FROM EREC_PldIratpeldanyok
			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
				ON CsoportTagokAll.Id = EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo
			where EREC_PldIratpeldanyok.Id = @Id
	) and not exists
	(
		SELECT top(1) 1 FROM EREC_PldIratPeldanyok
			INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
			WHERE EREC_PldIratPeldanyok.Id = @Id
				AND EREC_IraIratok.Ugyirat_Id IN 
			(
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
				UNION ALL
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id --AND @Jogszint != 'I'
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
				UNION ALL
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
						ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
				UNION ALL
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
						ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
				UNION ALL
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
						ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
				UNION ALL
				SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_UgyUgyiratok')
				UNION ALL
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN KRT_MappaTartalmak on KRT_MappaTartalmak.Obj_Id = erec_ugyugyiratok.Id
					INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
					where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
					and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
			)
	) AND NOT EXISTS
	(
		SELECT 1 FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_PldIratPeldanyok') as ids
			WHERE ids.Id = @Id
	) AND NOT EXISTS
	(
		SELECT 1 FROM EREC_PldIratpeldanyok
			INNER JOIN dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_IraIratok') as ids
				ON ids.Id = EREC_PldIratpeldanyok.IraIrat_Id
			where EREC_PldIratpeldanyok.Id = @Id
	) AND NOT EXISTS
	(
		SELECT 1 FROM EREC_PldIratpeldanyok
			INNER JOIN KRT_MappaTartalmak on KRT_MappaTartalmak.Obj_Id = EREC_PldIratpeldanyok.Id
			INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
			INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
			and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
			and EREC_PldIratpeldanyok.Id = @Id
	)
		RAISERROR('[50102]',16,1)

	-- Org szurés
	IF(@FromHistory='')
	BEGIN
		IF not exists(select 1 from EREC_IraIktatoKonyvek
			where EREC_IraIktatoKonyvek.Org=@Org
			and EREC_IraIktatoKonyvek.Id=
			(select EREC_UgyUgyiratdarabok.IraIktatoKonyv_Id from EREC_UgyUgyiratdarabok
			where EREC_UgyUgyiratdarabok.Id=
				(select EREC_IraIratok.UgyUgyIratDarab_Id from EREC_IraIratok
				where EREC_IraIratok.Id=
					(select EREC_PldIratPeldanyok.IraIrat_Id from EREC_PldIratPeldanyok
					where Id=@Id)
				)
			))
		BEGIN
			RAISERROR('[50101]',16,1)
		END
	END
	ELSE
	BEGIN
		IF not exists(select 1 from EREC_IraIktatoKonyvek
			where EREC_IraIktatoKonyvek.Org=@Org
			and EREC_IraIktatoKonyvek.Id=
			(select EREC_UgyUgyiratdarabok.IraIktatoKonyv_Id from EREC_UgyUgyiratdarabok
			where EREC_UgyUgyiratdarabok.Id=
				(select EREC_IraIratok.UgyUgyIratDarab_Id from EREC_IraIratok
				where EREC_IraIratok.Id=
					(select EREC_PldIratPeldanyokHistory.IraIrat_Id from EREC_PldIratPeldanyokHistory
					where Id=@Id)
				)
			))
		BEGIN
			RAISERROR('[50101]',16,1)
		END
	END

	DECLARE @sqlcmd nvarchar(4000)

/*************************Begin simple get**************************/  
   if (@FromHistory='')
   begin
	SET @sqlcmd = 
	  'select 
     	    EREC_PldIratPeldanyok.Id,
	   EREC_PldIratPeldanyok.IraIrat_Id,
	   EREC_PldIratPeldanyok.UgyUgyirat_Id_Kulso,
	   EREC_PldIratPeldanyok.Sorszam,
	   EREC_PldIratPeldanyok.SztornirozasDat,
	   EREC_PldIratPeldanyok.AtvetelDatuma,
	   EREC_PldIratPeldanyok.Eredet,
	   EREC_PldIratPeldanyok.KuldesMod,
	   EREC_PldIratPeldanyok.Ragszam,
	   EREC_PldIratPeldanyok.UgyintezesModja,
	   EREC_PldIratPeldanyok.VisszaerkezesiHatarido,
	   EREC_PldIratPeldanyok.Visszavarolag,
	   EREC_PldIratPeldanyok.VisszaerkezesDatuma,
	   EREC_PldIratPeldanyok.Cim_id_Cimzett,
	   EREC_PldIratPeldanyok.Partner_Id_Cimzett,
	   EREC_PldIratPeldanyok.Csoport_Id_Felelos,
	   EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo,
	   EREC_PldIratPeldanyok.Csoport_Id_Felelos_Elozo,
	   EREC_PldIratPeldanyok.CimSTR_Cimzett,
	   EREC_PldIratPeldanyok.NevSTR_Cimzett,
	   EREC_PldIratPeldanyok.Tovabbito,
	   EREC_PldIratPeldanyok.IraIrat_Id_Kapcsolt,
	   EREC_PldIratPeldanyok.IrattariHely,
	   EREC_PldIratPeldanyok.Gener_Id,
	   EREC_PldIratPeldanyok.PostazasDatuma,
	   EREC_PldIratPeldanyok.BarCode,
	   EREC_PldIratPeldanyok.Allapot,
	   EREC_PldIratPeldanyok.Azonosito,
	   EREC_PldIratPeldanyok.Kovetkezo_Felelos_Id,
	   EREC_PldIratPeldanyok.Elektronikus_Kezbesitesi_Allap,
	   EREC_PldIratPeldanyok.Kovetkezo_Orzo_Id,
	   EREC_PldIratPeldanyok.Fizikai_Kezbesitesi_Allapot,
	   EREC_PldIratPeldanyok.TovabbitasAlattAllapot,
	   EREC_PldIratPeldanyok.PostazasAllapot,
	   EREC_PldIratPeldanyok.ValaszElektronikus,
	   EREC_PldIratPeldanyok.Ver,
	   EREC_PldIratPeldanyok.Note,
	   EREC_PldIratPeldanyok.Stat_id,
	   EREC_PldIratPeldanyok.ErvKezd,
	   EREC_PldIratPeldanyok.ErvVege,
	   EREC_PldIratPeldanyok.Letrehozo_id,
	   EREC_PldIratPeldanyok.LetrehozasIdo,
	   EREC_PldIratPeldanyok.Modosito_id,
	   EREC_PldIratPeldanyok.ModositasIdo,
	   EREC_PldIratPeldanyok.Zarolo_id,
	   EREC_PldIratPeldanyok.ZarolasIdo,
	   EREC_PldIratPeldanyok.Tranz_id,
	   EREC_PldIratPeldanyok.UIAccessLog_id
	   from 
		 EREC_PldIratPeldanyok as EREC_PldIratPeldanyok 
	   where
		 EREC_PldIratPeldanyok.Id = ''' + cast(@Id as nvarchar(40)) + '''';
   end
/*************************End simple get**************************/  

/*************************Begin history get**************************/  
	else
   begin
   SET @sqlcmd = 
	  'select 
      EREC_PldIratPeldanyok.HistoryId,
      EREC_PldIratPeldanyok.HistoryMuvelet_Id,
      EREC_PldIratPeldanyok.HistoryVegrehajto_Id,
      EREC_PldIratPeldanyok.HistoryVegrehajtasIdo,       EREC_PldIratPeldanyok.Id,
       EREC_PldIratPeldanyok.IraIrat_Id,
       EREC_PldIratPeldanyok.UgyUgyirat_Id_Kulso,
       EREC_PldIratPeldanyok.Sorszam,
       EREC_PldIratPeldanyok.SztornirozasDat,
       EREC_PldIratPeldanyok.AtvetelDatuma,
       EREC_PldIratPeldanyok.Eredet,
       EREC_PldIratPeldanyok.KuldesMod,
       EREC_PldIratPeldanyok.Ragszam,
       EREC_PldIratPeldanyok.UgyintezesModja,
       EREC_PldIratPeldanyok.VisszaerkezesiHatarido,
       EREC_PldIratPeldanyok.Visszavarolag,
       EREC_PldIratPeldanyok.VisszaerkezesDatuma,
       EREC_PldIratPeldanyok.Cim_id_Cimzett,
       EREC_PldIratPeldanyok.Partner_Id_Cimzett,
       EREC_PldIratPeldanyok.Csoport_Id_Felelos,
       EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo,
       EREC_PldIratPeldanyok.Csoport_Id_Felelos_Elozo,
       EREC_PldIratPeldanyok.CimSTR_Cimzett,
       EREC_PldIratPeldanyok.NevSTR_Cimzett,
       EREC_PldIratPeldanyok.Tovabbito,
       EREC_PldIratPeldanyok.IraIrat_Id_Kapcsolt,
       EREC_PldIratPeldanyok.IrattariHely,
       EREC_PldIratPeldanyok.Gener_Id,
       EREC_PldIratPeldanyok.PostazasDatuma,
       EREC_PldIratPeldanyok.BarCode,
       EREC_PldIratPeldanyok.Allapot,
       EREC_PldIratPeldanyok.TovabbitasAlattAllapot,
       EREC_PldIratPeldanyok.Ver,
       EREC_PldIratPeldanyok.Note,
       EREC_PldIratPeldanyok.Stat_id,
       EREC_PldIratPeldanyok.ErvKezd,
       EREC_PldIratPeldanyok.ErvVege,
       EREC_PldIratPeldanyok.Letrehozo_id,
       EREC_PldIratPeldanyok.LetrehozasIdo,
       EREC_PldIratPeldanyok.Modosito_id,
       EREC_PldIratPeldanyok.ModositasIdo,
       EREC_PldIratPeldanyok.Zarolo_id,
       EREC_PldIratPeldanyok.ZarolasIdo,
       EREC_PldIratPeldanyok.Tranz_id,
       EREC_PldIratPeldanyok.UIAccessLog_id
	   from 
		 EREC_PldIratPeldanyokHistory as EREC_PldIratPeldanyok
	   where
		 EREC_PldIratPeldanyok.HistoryId = ''' + cast(@Id as nvarchar(40)) + '''';
   end;


/*************************End history get**************************/  
	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
