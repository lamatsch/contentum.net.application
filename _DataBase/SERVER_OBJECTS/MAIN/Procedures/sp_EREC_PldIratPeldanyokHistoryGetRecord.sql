IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_PldIratPeldanyokHistoryGetRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_PldIratPeldanyokHistoryGetRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_PldIratPeldanyokHistoryGetRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_PldIratPeldanyokHistoryGetRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_PldIratPeldanyokHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_PldIratPeldanyokHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_PldIratPeldanyokHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIrat_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_IraIratokAzonosito(Old.IraIrat_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_IraIratokAzonosito(New.IraIrat_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IraIrat_Id != New.IraIrat_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_IraIratok FTOld on FTOld.Id = Old.IraIrat_Id and FTOld.Ver = Old.Ver
         left join EREC_IraIratok FTNew on FTNew.Id = New.IraIrat_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyUgyirat_Id_Kulso' as ColumnName,               
               cast(Old.UgyUgyirat_Id_Kulso as nvarchar(99)) as OldValue,
               cast(New.UgyUgyirat_Id_Kulso as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyUgyirat_Id_Kulso != New.UgyUgyirat_Id_Kulso 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Sorszam' as ColumnName,               
               cast(Old.Sorszam as nvarchar(99)) as OldValue,
               cast(New.Sorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Sorszam != New.Sorszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SztornirozasDat' as ColumnName,               
               cast(Old.SztornirozasDat as nvarchar(99)) as OldValue,
               cast(New.SztornirozasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SztornirozasDat != New.SztornirozasDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AtvetelDatuma' as ColumnName,               
               cast(Old.AtvetelDatuma as nvarchar(99)) as OldValue,
               cast(New.AtvetelDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AtvetelDatuma != New.AtvetelDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Eredet' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Eredet != New.Eredet 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRAT_FAJTA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Eredet and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Eredet and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldesMod' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KuldesMod != New.KuldesMod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'KULDEMENY_KULDES_MODJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.KuldesMod and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.KuldesMod and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ragszam' as ColumnName,               
               cast(Old.Ragszam as nvarchar(99)) as OldValue,
               cast(New.Ragszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Ragszam != New.Ragszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesModja' as ColumnName,               
               cast(Old.UgyintezesModja as nvarchar(99)) as OldValue,
               cast(New.UgyintezesModja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyintezesModja != New.UgyintezesModja 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VisszaerkezesiHatarido' as ColumnName,               
               cast(Old.VisszaerkezesiHatarido as nvarchar(99)) as OldValue,
               cast(New.VisszaerkezesiHatarido as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.VisszaerkezesiHatarido != New.VisszaerkezesiHatarido 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Visszavarolag' as ColumnName,               
               cast(Old.Visszavarolag as nvarchar(99)) as OldValue,
               cast(New.Visszavarolag as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Visszavarolag != New.Visszavarolag 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VisszaerkezesDatuma' as ColumnName,               
               cast(Old.VisszaerkezesDatuma as nvarchar(99)) as OldValue,
               cast(New.VisszaerkezesDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.VisszaerkezesDatuma != New.VisszaerkezesDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Cim_id_Cimzett' as ColumnName,               
               cast(Old.Cim_id_Cimzett as nvarchar(99)) as OldValue,
               cast(New.Cim_id_Cimzett as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Cim_id_Cimzett != New.Cim_id_Cimzett 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id_Cimzett' as ColumnName,               
               cast(Old.Partner_Id_Cimzett as nvarchar(99)) as OldValue,
               cast(New.Partner_Id_Cimzett as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Partner_Id_Cimzett != New.Partner_Id_Cimzett 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Felelos' as ColumnName,               
               cast(Old.Csoport_Id_Felelos as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Felelos as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Felelos != New.Csoport_Id_Felelos 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Orzo' as ColumnName,               
               cast(Old.FelhasznaloCsoport_Id_Orzo as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Orzo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Orzo != New.FelhasznaloCsoport_Id_Orzo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Felelos_Elozo' as ColumnName,               
               cast(Old.Csoport_Id_Felelos_Elozo as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Felelos_Elozo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Felelos_Elozo != New.Csoport_Id_Felelos_Elozo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CimSTR_Cimzett' as ColumnName,               
               cast(Old.CimSTR_Cimzett as nvarchar(99)) as OldValue,
               cast(New.CimSTR_Cimzett as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.CimSTR_Cimzett != New.CimSTR_Cimzett 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'NevSTR_Cimzett' as ColumnName,               
               cast(Old.NevSTR_Cimzett as nvarchar(99)) as OldValue,
               cast(New.NevSTR_Cimzett as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.NevSTR_Cimzett != New.NevSTR_Cimzett 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tovabbito' as ColumnName,               
               cast(Old.Tovabbito as nvarchar(99)) as OldValue,
               cast(New.Tovabbito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tovabbito != New.Tovabbito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIrat_Id_Kapcsolt' as ColumnName,               
               cast(Old.IraIrat_Id_Kapcsolt as nvarchar(99)) as OldValue,
               cast(New.IraIrat_Id_Kapcsolt as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IraIrat_Id_Kapcsolt != New.IraIrat_Id_Kapcsolt 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattariHely' as ColumnName,               
               cast(Old.IrattariHely as nvarchar(99)) as OldValue,
               cast(New.IrattariHely as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IrattariHely != New.IrattariHely 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Gener_Id' as ColumnName,               
               cast(Old.Gener_Id as nvarchar(99)) as OldValue,
               cast(New.Gener_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Gener_Id != New.Gener_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PostazasDatuma' as ColumnName,               
               cast(Old.PostazasDatuma as nvarchar(99)) as OldValue,
               cast(New.PostazasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PostazasDatuma != New.PostazasDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BarCode' as ColumnName,               
               cast(Old.BarCode as nvarchar(99)) as OldValue,
               cast(New.BarCode as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BarCode != New.BarCode 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Allapot != New.Allapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRATPELDANY_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Allapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Allapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Azonosito' as ColumnName,               
               cast(Old.Azonosito as nvarchar(99)) as OldValue,
               cast(New.Azonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Azonosito != New.Azonosito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kovetkezo_Felelos_Id' as ColumnName,               
               cast(Old.Kovetkezo_Felelos_Id as nvarchar(99)) as OldValue,
               cast(New.Kovetkezo_Felelos_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Kovetkezo_Felelos_Id != New.Kovetkezo_Felelos_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Elektronikus_Kezbesitesi_Allap' as ColumnName,               
               cast(Old.Elektronikus_Kezbesitesi_Allap as nvarchar(99)) as OldValue,
               cast(New.Elektronikus_Kezbesitesi_Allap as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Elektronikus_Kezbesitesi_Allap != New.Elektronikus_Kezbesitesi_Allap 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kovetkezo_Orzo_Id' as ColumnName,               
               cast(Old.Kovetkezo_Orzo_Id as nvarchar(99)) as OldValue,
               cast(New.Kovetkezo_Orzo_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Kovetkezo_Orzo_Id != New.Kovetkezo_Orzo_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Fizikai_Kezbesitesi_Allapot' as ColumnName,               
               cast(Old.Fizikai_Kezbesitesi_Allapot as nvarchar(99)) as OldValue,
               cast(New.Fizikai_Kezbesitesi_Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Fizikai_Kezbesitesi_Allapot != New.Fizikai_Kezbesitesi_Allapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TovabbitasAlattAllapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.TovabbitasAlattAllapot != New.TovabbitasAlattAllapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRATPELDANY_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.TovabbitasAlattAllapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.TovabbitasAlattAllapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PostazasAllapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PostazasAllapot != New.PostazasAllapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRATPELDANY_POSTAZAS_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.PostazasAllapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.PostazasAllapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ValaszElektronikus' as ColumnName,               
               cast(Old.ValaszElektronikus as nvarchar(99)) as OldValue,
               cast(New.ValaszElektronikus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ValaszElektronikus != New.ValaszElektronikus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end


GO
