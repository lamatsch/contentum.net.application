IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_PldIratPeldanyokInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_PldIratPeldanyokInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_PldIratPeldanyokInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_PldIratPeldanyokInsert] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_PldIratPeldanyokInsert]    
                @Id      uniqueidentifier = null,    
               	            @IraIrat_Id     uniqueidentifier,
                @UgyUgyirat_Id_Kulso     uniqueidentifier  = null,
	            @Sorszam     int,
                @SztornirozasDat     datetime  = null,
                @AtvetelDatuma     datetime  = null,
	            @Eredet     nvarchar(64),
                @KuldesMod     nvarchar(64)  = null,
                @Ragszam     Nvarchar(100)  = null,
                @UgyintezesModja     nvarchar(64)  = null,
                @VisszaerkezesiHatarido     datetime  = null,
                @Visszavarolag     nvarchar(64)  = null,
                @VisszaerkezesDatuma     datetime  = null,
                @Cim_id_Cimzett     uniqueidentifier  = null,
                @Partner_Id_Cimzett     uniqueidentifier  = null,
                @Csoport_Id_Felelos     uniqueidentifier  = null,
                @FelhasznaloCsoport_Id_Orzo     uniqueidentifier  = null,
                @Csoport_Id_Felelos_Elozo     uniqueidentifier  = null,
                @CimSTR_Cimzett     Nvarchar(400)  = null,
                @NevSTR_Cimzett     Nvarchar(400)  = null,
                @Tovabbito     nvarchar(64)  = null,
                @IraIrat_Id_Kapcsolt     uniqueidentifier  = null,
                @IrattariHely     Nvarchar(100)  = null,
                @Gener_Id     uniqueidentifier  = null,
                @PostazasDatuma     datetime  = null,
                @BarCode     Nvarchar(100)  = null,
                @Allapot     nvarchar(64)  = null,
                @Azonosito     Nvarchar(100)  = null,
                @Kovetkezo_Felelos_Id     uniqueidentifier  = null,
                @Elektronikus_Kezbesitesi_Allap     nvarchar(64)  = null,
                @Kovetkezo_Orzo_Id     uniqueidentifier  = null,
                @Fizikai_Kezbesitesi_Allapot     nvarchar(64)  = null,
                @TovabbitasAlattAllapot     nvarchar(64)  = null,
                @PostazasAllapot     nvarchar(64)  = null,
                @ValaszElektronikus     char(1)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @IraIrat_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IraIrat_Id'
            SET @insertValues = @insertValues + ',@IraIrat_Id'
         end 
       
         if @UgyUgyirat_Id_Kulso is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyUgyirat_Id_Kulso'
            SET @insertValues = @insertValues + ',@UgyUgyirat_Id_Kulso'
         end 
       
         if @Sorszam is not null
         begin
            SET @insertColumns = @insertColumns + ',Sorszam'
            SET @insertValues = @insertValues + ',@Sorszam'
         end 
       
         if @SztornirozasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SztornirozasDat'
            SET @insertValues = @insertValues + ',@SztornirozasDat'
         end 
       
         if @AtvetelDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',AtvetelDatuma'
            SET @insertValues = @insertValues + ',@AtvetelDatuma'
         end 
       
         if @Eredet is not null
         begin
            SET @insertColumns = @insertColumns + ',Eredet'
            SET @insertValues = @insertValues + ',@Eredet'
         end 
       
         if @KuldesMod is not null
         begin
            SET @insertColumns = @insertColumns + ',KuldesMod'
            SET @insertValues = @insertValues + ',@KuldesMod'
         end 
       
         if @Ragszam is not null
         begin
            SET @insertColumns = @insertColumns + ',Ragszam'
            SET @insertValues = @insertValues + ',@Ragszam'
         end 
       
         if @UgyintezesModja is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyintezesModja'
            SET @insertValues = @insertValues + ',@UgyintezesModja'
         end 
       
         if @VisszaerkezesiHatarido is not null
         begin
            SET @insertColumns = @insertColumns + ',VisszaerkezesiHatarido'
            SET @insertValues = @insertValues + ',@VisszaerkezesiHatarido'
         end 
       
         if @Visszavarolag is not null
         begin
            SET @insertColumns = @insertColumns + ',Visszavarolag'
            SET @insertValues = @insertValues + ',@Visszavarolag'
         end 
       
         if @VisszaerkezesDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',VisszaerkezesDatuma'
            SET @insertValues = @insertValues + ',@VisszaerkezesDatuma'
         end 
       
         if @Cim_id_Cimzett is not null
         begin
            SET @insertColumns = @insertColumns + ',Cim_id_Cimzett'
            SET @insertValues = @insertValues + ',@Cim_id_Cimzett'
         end 
       
         if @Partner_Id_Cimzett is not null
         begin
            SET @insertColumns = @insertColumns + ',Partner_Id_Cimzett'
            SET @insertValues = @insertValues + ',@Partner_Id_Cimzett'
         end 
       
         if @Csoport_Id_Felelos is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Felelos'
            SET @insertValues = @insertValues + ',@Csoport_Id_Felelos'
         end 
       
         if @FelhasznaloCsoport_Id_Orzo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Orzo'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Orzo'
         end 
       
         if @Csoport_Id_Felelos_Elozo is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Felelos_Elozo'
            SET @insertValues = @insertValues + ',@Csoport_Id_Felelos_Elozo'
         end 
       
         if @CimSTR_Cimzett is not null
         begin
            SET @insertColumns = @insertColumns + ',CimSTR_Cimzett'
            SET @insertValues = @insertValues + ',@CimSTR_Cimzett'
         end 
       
         if @NevSTR_Cimzett is not null
         begin
            SET @insertColumns = @insertColumns + ',NevSTR_Cimzett'
            SET @insertValues = @insertValues + ',@NevSTR_Cimzett'
         end 
       
         if @Tovabbito is not null
         begin
            SET @insertColumns = @insertColumns + ',Tovabbito'
            SET @insertValues = @insertValues + ',@Tovabbito'
         end 
       
         if @IraIrat_Id_Kapcsolt is not null
         begin
            SET @insertColumns = @insertColumns + ',IraIrat_Id_Kapcsolt'
            SET @insertValues = @insertValues + ',@IraIrat_Id_Kapcsolt'
         end 
       
         if @IrattariHely is not null
         begin
            SET @insertColumns = @insertColumns + ',IrattariHely'
            SET @insertValues = @insertValues + ',@IrattariHely'
         end 
       
         if @Gener_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Gener_Id'
            SET @insertValues = @insertValues + ',@Gener_Id'
         end 
       
         if @PostazasDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',PostazasDatuma'
            SET @insertValues = @insertValues + ',@PostazasDatuma'
         end 
       
         if @BarCode is not null
         begin
            SET @insertColumns = @insertColumns + ',BarCode'
            SET @insertValues = @insertValues + ',@BarCode'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @Azonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Azonosito'
            SET @insertValues = @insertValues + ',@Azonosito'
         end 
       
         if @Kovetkezo_Felelos_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Kovetkezo_Felelos_Id'
            SET @insertValues = @insertValues + ',@Kovetkezo_Felelos_Id'
         end 
       
         if @Elektronikus_Kezbesitesi_Allap is not null
         begin
            SET @insertColumns = @insertColumns + ',Elektronikus_Kezbesitesi_Allap'
            SET @insertValues = @insertValues + ',@Elektronikus_Kezbesitesi_Allap'
         end 
       
         if @Kovetkezo_Orzo_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Kovetkezo_Orzo_Id'
            SET @insertValues = @insertValues + ',@Kovetkezo_Orzo_Id'
         end 
       
         if @Fizikai_Kezbesitesi_Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Fizikai_Kezbesitesi_Allapot'
            SET @insertValues = @insertValues + ',@Fizikai_Kezbesitesi_Allapot'
         end 
       
         if @TovabbitasAlattAllapot is not null
         begin
            SET @insertColumns = @insertColumns + ',TovabbitasAlattAllapot'
            SET @insertValues = @insertValues + ',@TovabbitasAlattAllapot'
         end 
       
         if @PostazasAllapot is not null
         begin
            SET @insertColumns = @insertColumns + ',PostazasAllapot'
            SET @insertValues = @insertValues + ',@PostazasAllapot'
         end 
       
         if @ValaszElektronikus is not null
         begin
            SET @insertColumns = @insertColumns + ',ValaszElektronikus'
            SET @insertValues = @insertValues + ',@ValaszElektronikus'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_PldIratPeldanyok ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@IraIrat_Id uniqueidentifier,@UgyUgyirat_Id_Kulso uniqueidentifier,@Sorszam int,@SztornirozasDat datetime,@AtvetelDatuma datetime,@Eredet nvarchar(64),@KuldesMod nvarchar(64),@Ragszam Nvarchar(100),@UgyintezesModja nvarchar(64),@VisszaerkezesiHatarido datetime,@Visszavarolag nvarchar(64),@VisszaerkezesDatuma datetime,@Cim_id_Cimzett uniqueidentifier,@Partner_Id_Cimzett uniqueidentifier,@Csoport_Id_Felelos uniqueidentifier,@FelhasznaloCsoport_Id_Orzo uniqueidentifier,@Csoport_Id_Felelos_Elozo uniqueidentifier,@CimSTR_Cimzett Nvarchar(400),@NevSTR_Cimzett Nvarchar(400),@Tovabbito nvarchar(64),@IraIrat_Id_Kapcsolt uniqueidentifier,@IrattariHely Nvarchar(100),@Gener_Id uniqueidentifier,@PostazasDatuma datetime,@BarCode Nvarchar(100),@Allapot nvarchar(64),@Azonosito Nvarchar(100),@Kovetkezo_Felelos_Id uniqueidentifier,@Elektronikus_Kezbesitesi_Allap nvarchar(64),@Kovetkezo_Orzo_Id uniqueidentifier,@Fizikai_Kezbesitesi_Allapot nvarchar(64),@TovabbitasAlattAllapot nvarchar(64),@PostazasAllapot nvarchar(64),@ValaszElektronikus char(1),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@IraIrat_Id = @IraIrat_Id,@UgyUgyirat_Id_Kulso = @UgyUgyirat_Id_Kulso,@Sorszam = @Sorszam,@SztornirozasDat = @SztornirozasDat,@AtvetelDatuma = @AtvetelDatuma,@Eredet = @Eredet,@KuldesMod = @KuldesMod,@Ragszam = @Ragszam,@UgyintezesModja = @UgyintezesModja,@VisszaerkezesiHatarido = @VisszaerkezesiHatarido,@Visszavarolag = @Visszavarolag,@VisszaerkezesDatuma = @VisszaerkezesDatuma,@Cim_id_Cimzett = @Cim_id_Cimzett,@Partner_Id_Cimzett = @Partner_Id_Cimzett,@Csoport_Id_Felelos = @Csoport_Id_Felelos,@FelhasznaloCsoport_Id_Orzo = @FelhasznaloCsoport_Id_Orzo,@Csoport_Id_Felelos_Elozo = @Csoport_Id_Felelos_Elozo,@CimSTR_Cimzett = @CimSTR_Cimzett,@NevSTR_Cimzett = @NevSTR_Cimzett,@Tovabbito = @Tovabbito,@IraIrat_Id_Kapcsolt = @IraIrat_Id_Kapcsolt,@IrattariHely = @IrattariHely,@Gener_Id = @Gener_Id,@PostazasDatuma = @PostazasDatuma,@BarCode = @BarCode,@Allapot = @Allapot,@Azonosito = @Azonosito,@Kovetkezo_Felelos_Id = @Kovetkezo_Felelos_Id,@Elektronikus_Kezbesitesi_Allap = @Elektronikus_Kezbesitesi_Allap,@Kovetkezo_Orzo_Id = @Kovetkezo_Orzo_Id,@Fizikai_Kezbesitesi_Allapot = @Fizikai_Kezbesitesi_Allapot,@TovabbitasAlattAllapot = @TovabbitasAlattAllapot,@PostazasAllapot = @PostazasAllapot,@ValaszElektronikus = @ValaszElektronikus,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_PldIratPeldanyok',@ResultUid
					,'EREC_PldIratPeldanyokHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH



GO
