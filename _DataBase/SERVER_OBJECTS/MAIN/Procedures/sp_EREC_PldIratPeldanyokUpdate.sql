IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_PldIratPeldanyokUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_PldIratPeldanyokUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_PldIratPeldanyokUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_PldIratPeldanyokUpdate] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_PldIratPeldanyokUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @IraIrat_Id     uniqueidentifier  = null,         
             @UgyUgyirat_Id_Kulso     uniqueidentifier  = null,         
             @Sorszam     int  = null,         
             @SztornirozasDat     datetime  = null,         
             @AtvetelDatuma     datetime  = null,         
             @Eredet     nvarchar(64)  = null,         
             @KuldesMod     nvarchar(64)  = null,         
             @Ragszam     Nvarchar(100)  = null,         
             @UgyintezesModja     nvarchar(64)  = null,         
             @VisszaerkezesiHatarido     datetime  = null,         
             @Visszavarolag     nvarchar(64)  = null,         
             @VisszaerkezesDatuma     datetime  = null,         
             @Cim_id_Cimzett     uniqueidentifier  = null,         
             @Partner_Id_Cimzett     uniqueidentifier  = null,         
             @Csoport_Id_Felelos     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Orzo     uniqueidentifier  = null,         
             @Csoport_Id_Felelos_Elozo     uniqueidentifier  = null,         
             @CimSTR_Cimzett     Nvarchar(400)  = null,         
             @NevSTR_Cimzett     Nvarchar(400)  = null,         
             @Tovabbito     nvarchar(64)  = null,         
             @IraIrat_Id_Kapcsolt     uniqueidentifier  = null,         
             @IrattariHely     Nvarchar(100)  = null,         
             @Gener_Id     uniqueidentifier  = null,         
             @PostazasDatuma     datetime  = null,         
             @BarCode     Nvarchar(100)  = null,         
             @Allapot     nvarchar(64)  = null,         
             @Azonosito     Nvarchar(100)  = null,         
             @Kovetkezo_Felelos_Id     uniqueidentifier  = null,         
             @Elektronikus_Kezbesitesi_Allap     nvarchar(64)  = null,         
             @Kovetkezo_Orzo_Id     uniqueidentifier  = null,         
             @Fizikai_Kezbesitesi_Allapot     nvarchar(64)  = null,         
             @TovabbitasAlattAllapot     nvarchar(64)  = null,         
             @PostazasAllapot     nvarchar(64)  = null,         
             @ValaszElektronikus     char(1)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_IraIrat_Id     uniqueidentifier         
     DECLARE @Act_UgyUgyirat_Id_Kulso     uniqueidentifier         
     DECLARE @Act_Sorszam     int         
     DECLARE @Act_SztornirozasDat     datetime         
     DECLARE @Act_AtvetelDatuma     datetime         
     DECLARE @Act_Eredet     nvarchar(64)         
     DECLARE @Act_KuldesMod     nvarchar(64)         
     DECLARE @Act_Ragszam     Nvarchar(100)         
     DECLARE @Act_UgyintezesModja     nvarchar(64)         
     DECLARE @Act_VisszaerkezesiHatarido     datetime         
     DECLARE @Act_Visszavarolag     nvarchar(64)         
     DECLARE @Act_VisszaerkezesDatuma     datetime         
     DECLARE @Act_Cim_id_Cimzett     uniqueidentifier         
     DECLARE @Act_Partner_Id_Cimzett     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Felelos     uniqueidentifier         
     DECLARE @Act_FelhasznaloCsoport_Id_Orzo     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Felelos_Elozo     uniqueidentifier         
     DECLARE @Act_CimSTR_Cimzett     Nvarchar(400)         
     DECLARE @Act_NevSTR_Cimzett     Nvarchar(400)         
     DECLARE @Act_Tovabbito     nvarchar(64)         
     DECLARE @Act_IraIrat_Id_Kapcsolt     uniqueidentifier         
     DECLARE @Act_IrattariHely     Nvarchar(100)         
     DECLARE @Act_Gener_Id     uniqueidentifier         
     DECLARE @Act_PostazasDatuma     datetime         
     DECLARE @Act_BarCode     Nvarchar(100)         
     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_Azonosito     Nvarchar(100)         
     DECLARE @Act_Kovetkezo_Felelos_Id     uniqueidentifier         
     DECLARE @Act_Elektronikus_Kezbesitesi_Allap     nvarchar(64)         
     DECLARE @Act_Kovetkezo_Orzo_Id     uniqueidentifier         
     DECLARE @Act_Fizikai_Kezbesitesi_Allapot     nvarchar(64)         
     DECLARE @Act_TovabbitasAlattAllapot     nvarchar(64)         
     DECLARE @Act_PostazasAllapot     nvarchar(64)         
     DECLARE @Act_ValaszElektronikus     char(1)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_IraIrat_Id = IraIrat_Id,
     @Act_UgyUgyirat_Id_Kulso = UgyUgyirat_Id_Kulso,
     @Act_Sorszam = Sorszam,
     @Act_SztornirozasDat = SztornirozasDat,
     @Act_AtvetelDatuma = AtvetelDatuma,
     @Act_Eredet = Eredet,
     @Act_KuldesMod = KuldesMod,
     @Act_Ragszam = Ragszam,
     @Act_UgyintezesModja = UgyintezesModja,
     @Act_VisszaerkezesiHatarido = VisszaerkezesiHatarido,
     @Act_Visszavarolag = Visszavarolag,
     @Act_VisszaerkezesDatuma = VisszaerkezesDatuma,
     @Act_Cim_id_Cimzett = Cim_id_Cimzett,
     @Act_Partner_Id_Cimzett = Partner_Id_Cimzett,
     @Act_Csoport_Id_Felelos = Csoport_Id_Felelos,
     @Act_FelhasznaloCsoport_Id_Orzo = FelhasznaloCsoport_Id_Orzo,
     @Act_Csoport_Id_Felelos_Elozo = Csoport_Id_Felelos_Elozo,
     @Act_CimSTR_Cimzett = CimSTR_Cimzett,
     @Act_NevSTR_Cimzett = NevSTR_Cimzett,
     @Act_Tovabbito = Tovabbito,
     @Act_IraIrat_Id_Kapcsolt = IraIrat_Id_Kapcsolt,
     @Act_IrattariHely = IrattariHely,
     @Act_Gener_Id = Gener_Id,
     @Act_PostazasDatuma = PostazasDatuma,
     @Act_BarCode = BarCode,
     @Act_Allapot = Allapot,
     @Act_Azonosito = Azonosito,
     @Act_Kovetkezo_Felelos_Id = Kovetkezo_Felelos_Id,
     @Act_Elektronikus_Kezbesitesi_Allap = Elektronikus_Kezbesitesi_Allap,
     @Act_Kovetkezo_Orzo_Id = Kovetkezo_Orzo_Id,
     @Act_Fizikai_Kezbesitesi_Allapot = Fizikai_Kezbesitesi_Allapot,
     @Act_TovabbitasAlattAllapot = TovabbitasAlattAllapot,
     @Act_PostazasAllapot = PostazasAllapot,
     @Act_ValaszElektronikus = ValaszElektronikus,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_PldIratPeldanyok
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zĂˇrolĂˇs ellenĹ‘rzĂ©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/IraIrat_Id')=1
         SET @Act_IraIrat_Id = @IraIrat_Id
   IF @UpdatedColumns.exist('/root/UgyUgyirat_Id_Kulso')=1
         SET @Act_UgyUgyirat_Id_Kulso = @UgyUgyirat_Id_Kulso
   IF @UpdatedColumns.exist('/root/Sorszam')=1
         SET @Act_Sorszam = @Sorszam
   IF @UpdatedColumns.exist('/root/SztornirozasDat')=1
         SET @Act_SztornirozasDat = @SztornirozasDat
   IF @UpdatedColumns.exist('/root/AtvetelDatuma')=1
         SET @Act_AtvetelDatuma = @AtvetelDatuma
   IF @UpdatedColumns.exist('/root/Eredet')=1
         SET @Act_Eredet = @Eredet
   IF @UpdatedColumns.exist('/root/KuldesMod')=1
         SET @Act_KuldesMod = @KuldesMod
   IF @UpdatedColumns.exist('/root/Ragszam')=1
         SET @Act_Ragszam = @Ragszam
   IF @UpdatedColumns.exist('/root/UgyintezesModja')=1
         SET @Act_UgyintezesModja = @UgyintezesModja
   IF @UpdatedColumns.exist('/root/VisszaerkezesiHatarido')=1
         SET @Act_VisszaerkezesiHatarido = @VisszaerkezesiHatarido
   IF @UpdatedColumns.exist('/root/Visszavarolag')=1
         SET @Act_Visszavarolag = @Visszavarolag
   IF @UpdatedColumns.exist('/root/VisszaerkezesDatuma')=1
         SET @Act_VisszaerkezesDatuma = @VisszaerkezesDatuma
   IF @UpdatedColumns.exist('/root/Cim_id_Cimzett')=1
         SET @Act_Cim_id_Cimzett = @Cim_id_Cimzett
   IF @UpdatedColumns.exist('/root/Partner_Id_Cimzett')=1
         SET @Act_Partner_Id_Cimzett = @Partner_Id_Cimzett
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1
         SET @Act_Csoport_Id_Felelos = @Csoport_Id_Felelos
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Orzo')=1
         SET @Act_FelhasznaloCsoport_Id_Orzo = @FelhasznaloCsoport_Id_Orzo
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos_Elozo')=1
         SET @Act_Csoport_Id_Felelos_Elozo = @Csoport_Id_Felelos_Elozo
   IF @UpdatedColumns.exist('/root/CimSTR_Cimzett')=1
         SET @Act_CimSTR_Cimzett = @CimSTR_Cimzett
   IF @UpdatedColumns.exist('/root/NevSTR_Cimzett')=1
         SET @Act_NevSTR_Cimzett = @NevSTR_Cimzett
   IF @UpdatedColumns.exist('/root/Tovabbito')=1
         SET @Act_Tovabbito = @Tovabbito
   IF @UpdatedColumns.exist('/root/IraIrat_Id_Kapcsolt')=1
         SET @Act_IraIrat_Id_Kapcsolt = @IraIrat_Id_Kapcsolt
   IF @UpdatedColumns.exist('/root/IrattariHely')=1
         SET @Act_IrattariHely = @IrattariHely
   IF @UpdatedColumns.exist('/root/Gener_Id')=1
         SET @Act_Gener_Id = @Gener_Id
   IF @UpdatedColumns.exist('/root/PostazasDatuma')=1
         SET @Act_PostazasDatuma = @PostazasDatuma
   IF @UpdatedColumns.exist('/root/BarCode')=1
         SET @Act_BarCode = @BarCode
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/Azonosito')=1
         SET @Act_Azonosito = @Azonosito
   IF @UpdatedColumns.exist('/root/Kovetkezo_Felelos_Id')=1
         SET @Act_Kovetkezo_Felelos_Id = @Kovetkezo_Felelos_Id
   IF @UpdatedColumns.exist('/root/Elektronikus_Kezbesitesi_Allap')=1
         SET @Act_Elektronikus_Kezbesitesi_Allap = @Elektronikus_Kezbesitesi_Allap
   IF @UpdatedColumns.exist('/root/Kovetkezo_Orzo_Id')=1
         SET @Act_Kovetkezo_Orzo_Id = @Kovetkezo_Orzo_Id
   IF @UpdatedColumns.exist('/root/Fizikai_Kezbesitesi_Allapot')=1
         SET @Act_Fizikai_Kezbesitesi_Allapot = @Fizikai_Kezbesitesi_Allapot
   IF @UpdatedColumns.exist('/root/TovabbitasAlattAllapot')=1
         SET @Act_TovabbitasAlattAllapot = @TovabbitasAlattAllapot
   IF @UpdatedColumns.exist('/root/PostazasAllapot')=1
         SET @Act_PostazasAllapot = @PostazasAllapot
   IF @UpdatedColumns.exist('/root/ValaszElektronikus')=1
         SET @Act_ValaszElektronikus = @ValaszElektronikus
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_PldIratPeldanyok
SET
     IraIrat_Id = @Act_IraIrat_Id,
     UgyUgyirat_Id_Kulso = @Act_UgyUgyirat_Id_Kulso,
     Sorszam = @Act_Sorszam,
     SztornirozasDat = @Act_SztornirozasDat,
     AtvetelDatuma = @Act_AtvetelDatuma,
     Eredet = @Act_Eredet,
     KuldesMod = @Act_KuldesMod,
     Ragszam = @Act_Ragszam,
     UgyintezesModja = @Act_UgyintezesModja,
     VisszaerkezesiHatarido = @Act_VisszaerkezesiHatarido,
     Visszavarolag = @Act_Visszavarolag,
     VisszaerkezesDatuma = @Act_VisszaerkezesDatuma,
     Cim_id_Cimzett = @Act_Cim_id_Cimzett,
     Partner_Id_Cimzett = @Act_Partner_Id_Cimzett,
     Csoport_Id_Felelos = @Act_Csoport_Id_Felelos,
     FelhasznaloCsoport_Id_Orzo = @Act_FelhasznaloCsoport_Id_Orzo,
     Csoport_Id_Felelos_Elozo = @Act_Csoport_Id_Felelos_Elozo,
     CimSTR_Cimzett = @Act_CimSTR_Cimzett,
     NevSTR_Cimzett = @Act_NevSTR_Cimzett,
     Tovabbito = @Act_Tovabbito,
     IraIrat_Id_Kapcsolt = @Act_IraIrat_Id_Kapcsolt,
     IrattariHely = @Act_IrattariHely,
     Gener_Id = @Act_Gener_Id,
     PostazasDatuma = @Act_PostazasDatuma,
     BarCode = @Act_BarCode,
     Allapot = @Act_Allapot,
     Azonosito = @Act_Azonosito,
     Kovetkezo_Felelos_Id = @Act_Kovetkezo_Felelos_Id,
     Elektronikus_Kezbesitesi_Allap = @Act_Elektronikus_Kezbesitesi_Allap,
     Kovetkezo_Orzo_Id = @Act_Kovetkezo_Orzo_Id,
     Fizikai_Kezbesitesi_Allapot = @Act_Fizikai_Kezbesitesi_Allapot,
     TovabbitasAlattAllapot = @Act_TovabbitasAlattAllapot,
     PostazasAllapot = @Act_PostazasAllapot,
     ValaszElektronikus = @Act_ValaszElektronikus,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_PldIratPeldanyok',@Id
					,'EREC_PldIratPeldanyokHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH




GO
