IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_PldKapjakMegGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_PldKapjakMegGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_PldKapjakMegGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_PldKapjakMegGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_PldKapjakMegGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_PldKapjakMeg.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_PldKapjakMeg.Id,
	   EREC_PldKapjakMeg.PldIratPeldany_Id,
	   EREC_PldKapjakMeg.Csoport_Id,
	   EREC_PldKapjakMeg.Ver,
	   EREC_PldKapjakMeg.Note,
	   EREC_PldKapjakMeg.Stat_id,
	   EREC_PldKapjakMeg.ErvKezd,
	   EREC_PldKapjakMeg.ErvVege,
	   EREC_PldKapjakMeg.Letrehozo_id,
	   EREC_PldKapjakMeg.LetrehozasIdo,
	   EREC_PldKapjakMeg.Modosito_id,
	   EREC_PldKapjakMeg.ModositasIdo,
	   EREC_PldKapjakMeg.Zarolo_id,
	   EREC_PldKapjakMeg.ZarolasIdo,
	   EREC_PldKapjakMeg.Tranz_id,
	   EREC_PldKapjakMeg.UIAccessLog_id  
   from 
     EREC_PldKapjakMeg as EREC_PldKapjakMeg      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
