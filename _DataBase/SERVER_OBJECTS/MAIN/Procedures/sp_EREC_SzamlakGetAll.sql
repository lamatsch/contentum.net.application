IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_SzamlakGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_SzamlakGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_SzamlakGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_SzamlakGetAll] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_SzamlakGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_Szamlak.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_Szamlak.Id,
	   EREC_Szamlak.KuldKuldemeny_Id,
	   EREC_Szamlak.Dokumentum_Id,
	   EREC_Szamlak.Partner_Id_Szallito,
	   EREC_Szamlak.Cim_Id_Szallito,
	   EREC_Szamlak.Bankszamlaszam_Id,
	   EREC_Szamlak.SzamlaSorszam,
	   EREC_Szamlak.FizetesiMod,
	   EREC_Szamlak.TeljesitesDatuma,
	   EREC_Szamlak.BizonylatDatuma,
	   EREC_Szamlak.FizetesiHatarido,
	   EREC_Szamlak.DevizaKod,
	   EREC_Szamlak.VisszakuldesDatuma,
	   EREC_Szamlak.EllenorzoOsszeg,
	   EREC_Szamlak.PIRAllapot,
	   EREC_Szamlak.VevoBesorolas,   --CR3359
	   EREC_Szamlak.Ver,
	   EREC_Szamlak.Note,
	   EREC_Szamlak.Stat_id,
	   EREC_Szamlak.ErvKezd,
	   EREC_Szamlak.ErvVege,
	   EREC_Szamlak.Letrehozo_id,
	   EREC_Szamlak.LetrehozasIdo,
	   EREC_Szamlak.Modosito_id,
	   EREC_Szamlak.ModositasIdo,
	   EREC_Szamlak.Zarolo_id,
	   EREC_Szamlak.ZarolasIdo,
	   EREC_Szamlak.Tranz_id,
	   EREC_Szamlak.UIAccessLog_id  
   from 
     EREC_Szamlak as EREC_Szamlak      
   '

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

GO
