IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_SzamlakGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_SzamlakGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_SzamlakGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_SzamlakGetAllWithExtension] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_SzamlakGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_Szamlak.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @Where_KuldKuldemenyek	nvarchar(MAX) = '',
  @Where_Dokumentumok	nvarchar(MAX) = '',
  @ExecutorUserId				uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier,
  @Jogosultak		char(1) = '0',
  @pageNumber		int = 0,
  @pageSize			int = -1,
  @SelectedRowId	uniqueidentifier = NULL

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	DECLARE @sqlcmd nvarchar(MAX)
	DECLARE @LocalTopRow nvarchar(10)
	declare @firstRow int
	declare @lastRow INT

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
		if (@pageSize > @TopRow)
			SET @pageSize = @TopRow;
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
                  
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = 1000
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	END
   
	set @sqlcmd = ''; 

	SET @sqlcmd = 'select EREC_Szamlak.Id into #filter from EREC_Szamlak
Where ''' + cast(@Org as NVarChar(40)) + '''=
(select Org from KRT_Felhasznalok where EREC_Szamlak.Letrehozo_id=KRT_Felhasznalok.Id)
'

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where;
	END

	IF @Jogosultak = '1' AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
	BEGIN
		SET @sqlcmd = @sqlcmd + ' DELETE FROM #filter WHERE Id NOT IN
(
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_KuldKuldemenyek ON EREC_Szamlak.KuldKuldemeny_Id=EREC_KuldKuldemenyek.Id
		INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.Id
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll
			ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
	UNION ALL
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_KuldKuldemenyek ON EREC_Szamlak.KuldKuldemeny_Id=EREC_KuldKuldemenyek.Id
		INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll
			ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
	UNION ALL
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_KuldKuldemenyek ON EREC_Szamlak.KuldKuldemeny_Id=EREC_KuldKuldemenyek.Id
		INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIratok_Id
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
			ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
	UNION ALL
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_KuldKuldemenyek ON EREC_Szamlak.KuldKuldemeny_Id=EREC_KuldKuldemenyek.Id
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
			ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.Csoport_Id_Felelos
	UNION ALL
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_KuldKuldemenyek ON EREC_Szamlak.KuldKuldemeny_Id=EREC_KuldKuldemenyek.Id
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
			ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
'

SET @sqlcmd = @sqlcmd + '	UNION ALL										
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_KuldKuldemenyek ON EREC_Szamlak.KuldKuldemeny_Id=EREC_KuldKuldemenyek.Id
		INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_KuldKuldemenyek.IraIratok_Id
		WHERE EREC_KuldKuldemenyek.IraIratok_Id IS NOT NULL
			AND EREC_IraIratok.Ugyirat_Id IN 
		(
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
					ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
					ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
					ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
			UNION ALL
			SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'')
		)
'
/* -- Elvileg ez is kéne ide, ezek a belső iratokhoz tartozó küldemények
	UNION ALL										
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_KuldKuldemenyek ON EREC_Szamlak.KuldKuldemeny_Id=EREC_KuldKuldemenyek.Id
		INNER JOIN EREC_IraIratok ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id
		WHERE 1 = 1
			AND EREC_IraIratok.Ugyirat_Id IN 
		(
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
					ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
					ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
					ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
			UNION ALL
			SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'')
		)*/
SET @sqlcmd = @sqlcmd + '
	UNION ALL
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_KuldKuldemenyek ON EREC_Szamlak.KuldKuldemeny_Id=EREC_KuldKuldemenyek.Id
		where EREC_KuldKuldemenyek.Id IN (Select Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_KuldKuldemenyek''))
'

-- Dokumentumok szerinti jogosultsás:
		SET @sqlcmd = @sqlcmd + 'UNION ALL
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_Csatolmanyok EREC_Csatolmanyok on EREC_Csatolmanyok.Dokumentum_Id=EREC_Szamlak.Dokumentum_Id
		INNER JOIN EREC_IraIratok ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
		INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_IraIratok.Id
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
	UNION ALL
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_Csatolmanyok EREC_Csatolmanyok on EREC_Csatolmanyok.Dokumentum_Id=EREC_Szamlak.Dokumentum_Id
		INNER JOIN EREC_IraIratok ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
		WHERE EREC_IraIratok.Ugyirat_Id IN 
		(
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
					ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
					ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
					ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
			UNION ALL
			SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'')
		)'
		
	SET @sqlcmd = @sqlcmd + '	
	UNION ALL --CR1716: lenézünk a példányba, CR 2728: aki láthatja a példányt az láthatja az iratot is
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_Csatolmanyok EREC_Csatolmanyok on EREC_Csatolmanyok.Dokumentum_Id=EREC_Szamlak.Dokumentum_Id
		INNER JOIN EREC_IraIratok ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
		INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = EREC_IraIratok.Id
		WHERE EREC_PldIratpeldanyok.Id IN
		(
			SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
				INNER JOIN KRT_Jogosultak ON KRT_Jogosultak.Obj_Id = EREC_PldIratPeldanyok.Id
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
				ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			UNION ALL
			SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
				ON CsoportTagokAll.Id = EREC_PldIratpeldanyok.Csoport_Id_Felelos
			UNION ALL
			SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
				ON CsoportTagokAll.Id = EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo
			UNION ALL
				SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_PldIratPeldanyok'')
		)
	UNION ALL
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_Csatolmanyok EREC_Csatolmanyok on EREC_Csatolmanyok.Dokumentum_Id=EREC_Szamlak.Dokumentum_Id
		WHERE EREC_Csatolmanyok.IraIrat_Id IN (SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_IraIratok''))

	UNION ALL';
										
		SET @sqlcmd = @sqlcmd + '
SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_Csatolmanyok EREC_Csatolmanyok on EREC_Csatolmanyok.Dokumentum_Id=EREC_Szamlak.Dokumentum_Id
		INNER JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_Csatolmanyok.KuldKuldemeny_Id
		INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.Id
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll
			ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
	UNION ALL
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_Csatolmanyok EREC_Csatolmanyok on EREC_Csatolmanyok.Dokumentum_Id=EREC_Szamlak.Dokumentum_Id
		INNER JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_Csatolmanyok.KuldKuldemeny_Id
		INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll
			ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
	UNION ALL
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_Csatolmanyok EREC_Csatolmanyok on EREC_Csatolmanyok.Dokumentum_Id=EREC_Szamlak.Dokumentum_Id
		INNER JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_Csatolmanyok.KuldKuldemeny_Id
		INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIratok_Id
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
			ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
	UNION ALL
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_Csatolmanyok EREC_Csatolmanyok on EREC_Csatolmanyok.Dokumentum_Id=EREC_Szamlak.Dokumentum_Id
		INNER JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_Csatolmanyok.KuldKuldemeny_Id
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
			ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.Csoport_Id_Felelos
	UNION ALL
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_Csatolmanyok EREC_Csatolmanyok on EREC_Csatolmanyok.Dokumentum_Id=EREC_Szamlak.Dokumentum_Id
		INNER JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_Csatolmanyok.KuldKuldemeny_Id
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
			ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
'
		SET @sqlcmd = @sqlcmd + '	UNION ALL										
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_Csatolmanyok EREC_Csatolmanyok on EREC_Csatolmanyok.Dokumentum_Id=EREC_Szamlak.Dokumentum_Id
		INNER JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_Csatolmanyok.KuldKuldemeny_Id
		INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_KuldKuldemenyek.IraIratok_Id
		WHERE EREC_KuldKuldemenyek.IraIratok_Id IS NOT NULL
			AND EREC_IraIratok.Ugyirat_Id IN 
		(
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
					ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
					ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
					ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
			UNION ALL
			SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'')
		)
	UNION ALL
	SELECT EREC_Szamlak.Id from EREC_Szamlak
		INNER JOIN EREC_Csatolmanyok EREC_Csatolmanyok on EREC_Csatolmanyok.Dokumentum_Id=EREC_Szamlak.Dokumentum_Id
		WHERE EREC_Csatolmanyok.KuldKuldemeny_Id IN (SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_KuldKuldemenyek''))
)
'

	END -- jogosultságszűrés


	IF @Where_KuldKuldemenyek IS NOT NULL AND @Where_KuldKuldemenyek != ''
		SET @sqlcmd = @sqlcmd + ' delete from #filter where Id not in (
			SELECT EREC_Szamlak.Id
			FROM EREC_Szamlak
			inner join EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id=EREC_Szamlak.KuldKuldemeny_Id
			WHERE GETDATE() BETWEEN EREC_KuldKuldemenyek.ErvKezd AND EREC_KuldKuldemenyek.ErvVege
				AND ' + @Where_KuldKuldemenyek + '); '

	IF @Where_Dokumentumok IS NOT NULL AND @Where_Dokumentumok != ''
		SET @sqlcmd = @sqlcmd + ' delete from #filter where Id not in (
			SELECT EREC_Szamlak.Id
			FROM EREC_Szamlak
			inner join KRT_Dokumentumok on KRT_Dokumentumok.Id=EREC_Szamlak.Dokumentum_Id
			FROM KRT_Dokumentumok
			WHERE GETDATE() BETWEEN KRT_Dokumentumok.ErvKezd AND KRT_Dokumentumok.ErvVege
				AND ' + @Where_Dokumentumok + '); '

	/************************************************************
	* Szűrt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/
		SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   EREC_Szamlak.Id into #result
       from EREC_Szamlak as EREC_Szamlak
		left join EREC_KuldKuldemenyek on EREC_Szamlak.KuldKuldemeny_Id=EREC_KuldKuldemenyek.Id
		left join KRT_Dokumentumok on EREC_Szamlak.Dokumentum_Id=KRT_Dokumentumok.Id
		where EREC_Szamlak.Id in (select Id from #filter); '

		if (@SelectedRowId is not null)
			set @sqlcmd = @sqlcmd + N'
			if exists (select 1 from #result where Id = ''' + cast(@SelectedRowId as nvarchar(36)) + ''')
			BEGIN
				select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = ''' + cast(@SelectedRowId as nvarchar(36)) + ''';
				set @firstRow = (@pageNumber - 1) * @pageSize + 1;
				set @lastRow = @pageNumber*@pageSize;
				select @pageNumber = @pageNumber - 1
			END
			ELSE'
		--ELSE
			set @sqlcmd = @sqlcmd + N' 
				if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
				BEGIN
					select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
					set @firstRow = (@pageNumber - 1) * @pageSize + 1;
					set @lastRow = @pageNumber*@pageSize;
					select @pageNumber = @pageNumber - 1
				END ;'

          
	/************************************************************
	* Tényleges select											*
	************************************************************/
		set @sqlcmd = @sqlcmd + N'
	select 
		#result.RowNumber,
		#result.Id,
		--EREC_Szamlak.Id,
		EREC_Szamlak.KuldKuldemeny_Id,
		EREC_Szamlak.Dokumentum_Id,
		EREC_Szamlak.Partner_Id_Szallito,
		EREC_Szamlak.Cim_Id_Szallito,
		EREC_Szamlak.Bankszamlaszam_Id,
		EREC_Szamlak.SzamlaSorszam,
		EREC_Szamlak.FizetesiMod,
		EREC_Szamlak.TeljesitesDatuma,
		EREC_Szamlak.BizonylatDatuma,
		EREC_Szamlak.FizetesiHatarido,
		EREC_Szamlak.DevizaKod,
		EREC_Szamlak.VisszakuldesDatuma,
		EREC_Szamlak.EllenorzoOsszeg,
		EREC_Szamlak.PIRAllapot,
		KRT_Bankszamlaszamok.Bankszamlaszam as Bankszamlaszam,
		Bankszamlaszam1 = case when len(KRT_Bankszamlaszamok.Bankszamlaszam)>=8 then substring(KRT_Bankszamlaszamok.Bankszamlaszam,1,8)
		else '''' end,
		Bankszamlaszam2 = case when len(KRT_Bankszamlaszamok.Bankszamlaszam)>=16 then substring(KRT_Bankszamlaszamok.Bankszamlaszam,9,8)
		else '''' end,
		Bankszamlaszam3 = case when len(KRT_Bankszamlaszamok.Bankszamlaszam)>=24 then substring(KRT_Bankszamlaszamok.Bankszamlaszam,17,8)
		else '''' end,
		BarCode=case when EREC_Szamlak.KuldKuldemeny_Id is not null then EREC_KuldKuldemenyek.BarCode
		when EREC_Szamlak.Dokumentum_Id is not null then KRT_Dokumentumok.BarCode
		else null end,
		EREC_Szamlak.VevoBesorolas,   --CR3359
		EREC_Szamlak.Ver,
		EREC_Szamlak.Note,
		EREC_Szamlak.Stat_id,
		EREC_Szamlak.ErvKezd,
		EREC_Szamlak.ErvVege,
		EREC_Szamlak.Letrehozo_id,
		EREC_Szamlak.LetrehozasIdo,
		EREC_Szamlak.Modosito_id,
		EREC_Szamlak.ModositasIdo,
		EREC_Szamlak.Zarolo_id,
		EREC_Szamlak.ZarolasIdo,
		EREC_Szamlak.Tranz_id,
		EREC_Szamlak.UIAccessLog_id  
	from 
		EREC_Szamlak as EREC_Szamlak
		inner join #result on #result.Id = EREC_Szamlak.Id
	left join KRT_Bankszamlaszamok on KRT_Bankszamlaszamok.Id=EREC_Szamlak.Bankszamlaszam_Id
	left join EREC_KuldKuldemenyek on EREC_Szamlak.KuldKuldemeny_Id=EREC_KuldKuldemenyek.Id
	left join KRT_Dokumentumok on EREC_Szamlak.KuldKuldemeny_Id=KRT_Dokumentumok.Id

	WHERE RowNumber between @firstRow and @lastRow
	ORDER BY #result.RowNumber;'

	-- találatok száma és oldalszám
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter;';

	execute sp_executesql @sqlcmd,N'@ExecutorUserId uniqueidentifier, @FelhasznaloSzervezet_Id uniqueidentifier,@firstRow int, @lastRow int, @pageSize int, @pageNumber int'
		,@ExecutorUserId=@ExecutorUserId,@FelhasznaloSzervezet_Id=@FelhasznaloSzervezet_Id,@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber;


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

GO
