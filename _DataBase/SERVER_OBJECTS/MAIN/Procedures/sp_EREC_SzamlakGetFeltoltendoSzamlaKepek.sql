IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_SzamlakGetFeltoltendoSzamlaKepek]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_SzamlakGetFeltoltendoSzamlaKepek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_SzamlakGetFeltoltendoSzamlaKepek]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_SzamlakGetFeltoltendoSzamlaKepek] AS' 
END
GO

ALTER procedure  [dbo].[sp_EREC_SzamlakGetFeltoltendoSzamlaKepek]

         
as

begin
BEGIN TRY

	set nocount on

	select
            s.modosito_id, -- ez csak az execparam-hoz kell, hogy kivel hívjuk
            s.szamlasorszam, -- ez a számla azonosítója
			s.vevobesorolas, -- vevő csoportosító mező, interface oldalon felhasználható; CR3359
            c.dokumentum_id, -- ezzel meg kell update-elni a számlák táblát, hogy tudjuk mit küldtünk már át
            d.external_link, -- a számlaképo URL-je
            d.fajlnev, -- a számlakép fájl neve
            s.ver, -- számla update-hez kell
            s.id, -- számla update-hez kell
            d.org -- exec paramhoz kell
     from 
            erec_szamlak s inner join 
            erec_csatolmanyok c on c.kuldkuldemeny_id = s.kuldkuldemeny_id inner join
            krt_dokumentumok d on c.dokumentum_id = d.id
     where 
            s.PIRAllapot = 'V'
	    and	s.dokumentum_id is null 
            and c.dokumentumszerep = '01' 
          

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end

GO
