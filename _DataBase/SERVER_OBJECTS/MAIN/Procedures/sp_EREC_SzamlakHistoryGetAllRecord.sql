IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_SzamlakHistoryGetAllRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_SzamlakHistoryGetAllRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_SzamlakHistoryGetAllRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_SzamlakHistoryGetAllRecord] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_SzamlakHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_SzamlakHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldKuldemeny_Id' as ColumnName,               cast(Old.KuldKuldemeny_Id as nvarchar(99)) as OldValue,
               cast(New.KuldKuldemeny_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KuldKuldemeny_Id != New.KuldKuldemeny_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Dokumentum_Id' as ColumnName,               cast(Old.Dokumentum_Id as nvarchar(99)) as OldValue,
               cast(New.Dokumentum_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Dokumentum_Id != New.Dokumentum_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id_Szallito' as ColumnName,               cast(Old.Partner_Id_Szallito as nvarchar(99)) as OldValue,
               cast(New.Partner_Id_Szallito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Partner_Id_Szallito != New.Partner_Id_Szallito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Cim_Id_Szallito' as ColumnName,               cast(Old.Cim_Id_Szallito as nvarchar(99)) as OldValue,
               cast(New.Cim_Id_Szallito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Cim_Id_Szallito != New.Cim_Id_Szallito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Bankszamlaszam_Id' as ColumnName,               cast(Old.Bankszamlaszam_Id as nvarchar(99)) as OldValue,
               cast(New.Bankszamlaszam_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Bankszamlaszam_Id != New.Bankszamlaszam_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzamlaSorszam' as ColumnName,               cast(Old.SzamlaSorszam as nvarchar(99)) as OldValue,
               cast(New.SzamlaSorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SzamlaSorszam != New.SzamlaSorszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FizetesiMod' as ColumnName,               cast(Old.FizetesiMod as nvarchar(99)) as OldValue,
               cast(New.FizetesiMod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FizetesiMod != New.FizetesiMod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TeljesitesDatuma' as ColumnName,               cast(Old.TeljesitesDatuma as nvarchar(99)) as OldValue,
               cast(New.TeljesitesDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.TeljesitesDatuma != New.TeljesitesDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BizonylatDatuma' as ColumnName,               cast(Old.BizonylatDatuma as nvarchar(99)) as OldValue,
               cast(New.BizonylatDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BizonylatDatuma != New.BizonylatDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FizetesiHatarido' as ColumnName,               cast(Old.FizetesiHatarido as nvarchar(99)) as OldValue,
               cast(New.FizetesiHatarido as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FizetesiHatarido != New.FizetesiHatarido 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DevizaKod' as ColumnName,               cast(Old.DevizaKod as nvarchar(99)) as OldValue,
               cast(New.DevizaKod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.DevizaKod != New.DevizaKod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VisszakuldesDatuma' as ColumnName,               cast(Old.VisszakuldesDatuma as nvarchar(99)) as OldValue,
               cast(New.VisszakuldesDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.VisszakuldesDatuma != New.VisszakuldesDatuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EllenorzoOsszeg' as ColumnName,               cast(Old.EllenorzoOsszeg as nvarchar(99)) as OldValue,
               cast(New.EllenorzoOsszeg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.EllenorzoOsszeg != New.EllenorzoOsszeg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PIRAllapot' as ColumnName,               cast(Old.PIRAllapot as nvarchar(99)) as OldValue,
               cast(New.PIRAllapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PIRAllapot != New.PIRAllapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
   -- CR3359
    union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VevoBesorolas' as ColumnName,               cast(Old.VevoBesorolas as nvarchar(99)) as OldValue,
               cast(New.VevoBesorolas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.VevoBesorolas != New.VevoBesorolas 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)         
end

GO
