IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Szamlak_UpdatePIRAllapot_Tomeges]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_Szamlak_UpdatePIRAllapot_Tomeges]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_Szamlak_UpdatePIRAllapot_Tomeges]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_Szamlak_UpdatePIRAllapot_Tomeges] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_Szamlak_UpdatePIRAllapot_Tomeges]
        @Ids					NVARCHAR(MAX),
        @Vers					NVARCHAR(MAX),
        @PIRAllapotok			NVARCHAR(MAX),
		@ExecutorUserId			uniqueidentifier,
		@ExecutionTime			datetime,
		@Tranz_Id				uniqueidentifier = null

as

BEGIN TRY

	if @Tranz_Id is null
		set @Tranz_Id = newid();

	declare @tempTable table(Id uniqueidentifier, Ver INT, PIRAllapot char(1));
	
	declare @it	int;
	declare @verPosBegin int;
	declare @verPosEnd int;
	declare @curId	nvarchar(36);
	declare @curVer	nvarchar(6);
	DECLARE @curPIRAllapot CHAR(1);

	set @it = 0;
	set @verPosBegin = 1;

	while (@it < ((len(@Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Ids,@it*39+2,37);
		set @verPosEnd =  CHARINDEX(',',@Vers,@verPosBegin);
		if @verPosEnd = 0 
			set @verPosEnd = len(@Vers) + 1;
		set @curVer = SUBSTRING(@vers, @verPosBegin, @verPosEnd - @verPosBegin);
		set @verPosBegin = @verPosEnd+1;
		IF @PIRAllapotok IS NOT NULL
		BEGIN
		   SET @curPIRAllapot = SUBSTRING(@PIRAllapotok,@it*4+2,2);
		END
		insert into @tempTable(Id, Ver, PIRAllapot) values(@curId, convert(int,@curVer), @curPIRAllapot);
		set @it = @it + 1;
	END
	
	
	SELECT sz.Id,sz.PIRAllapot,sz.Ver, t.Id AS temp_Id, t.Ver AS temp_Ver, t.PIRAllapot AS New_PIRAllapot
	INTO #tempJoinTable
	from EREC_Szamlak sz
	inner JOIN @tempTable t on t.Id = sz.Id
	

	declare @rowNumberTotal int;
	declare @rowNumberChecked int;

	select @rowNumberTotal = count(Id) from @tempTable;
	
	
	/******************************************
	* EllenL‘rzA©sek
	******************************************/
	-- verziAlellenL‘rzA©s
	select @rowNumberChecked = count(EREC_Szamlak.Id)
		from EREC_Szamlak
			inner JOIN @tempTable t on t.Id = EREC_Szamlak.Id and t.Ver = EREC_Szamlak.Ver
			
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT EREC_Szamlak.Id FROM EREC_Szamlak
			inner join @tempTable t on t.Id = EREC_Szamlak.Id and t.Ver = EREC_Szamlak.Ver
		RAISERROR('[50402]',16,1);
	END
	
	-- zA?rolA?sellenL‘rzA©s
	select @rowNumberChecked = count(EREC_Szamlak.Id)
		from EREC_Szamlak
			inner JOIN @tempTable t on t.Id = EREC_Szamlak.Id and ISNULL(EREC_Szamlak.Zarolo_id,@ExecutorUserId) = @ExecutorUserId
			
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT EREC_Szamlak.Id FROM EREC_Szamlak
			inner join @tempTable t on t.Id = EREC_Szamlak.Id and ISNULL(EREC_Szamlak.Zarolo_id,@ExecutorUserId) = @ExecutorUserId
		RAISERROR('[50499]',16,1);
	END
	
	-- A©rvA©nyessA©g vA©ge figyelA©se
	select @rowNumberChecked = count(EREC_Szamlak.Id)
		from EREC_Szamlak
			inner JOIN @tempTable t on t.Id = EREC_Szamlak.Id and EREC_Szamlak.ErvVege > @ExecutionTime
			
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT EREC_Szamlak.Id FROM EREC_Szamlak
			inner join @tempTable t on t.Id = EREC_Szamlak.Id and EREC_Szamlak.ErvVege > @ExecutionTime
		RAISERROR('[50403]',16,1);
	END
	
	/******************************************
	* UPDATE string A¶sszeA?llA­tA?s
	******************************************/
	DECLARE @sqlcmd	NVARCHAR(MAX);
	SET @sqlcmd = N'UPDATE EREC_Szamlak SET ';
	
         SET @sqlcmd = @sqlcmd + N' PIRAllapot = #tempJoinTable.New_PIRAllapot,'
         SET @sqlcmd = @sqlcmd + N' Modosito_id = @Modosito_id,'
         SET @sqlcmd = @sqlcmd + N' ModositasIdo = @ModositasIdo,'
         SET @sqlcmd = @sqlcmd + N' Tranz_id = @Tranz_id,'
   SET @sqlcmd = @sqlcmd + N' Ver = EREC_Szamlak.Ver + 1'   

   SET @sqlcmd = @sqlcmd + N'
	FROM EREC_Szamlak, #tempJoinTable
	WHERE EREC_Szamlak.Id = #tempJoinTable.Id;'

	EXECUTE sp_executesql @sqlcmd, N'     
	@Modosito_id	uniqueidentifier,         
	@ModositasIdo	datetime,
	@Tranz_Id	uniqueidentifier',
    @Modosito_id=@ExecutorUserId,         
    @ModositasIdo=@ExecutionTime,
	@Tranz_Id=@Tranz_Id;

	if @@rowcount != @rowNumberTotal
	begin
		RAISERROR('[50401]',16,1)
	END

	DROP TABLE #tempJoinTable

 	/* dinamikus history log A¶sszeA?llA­tA?shoz */
	declare @row_ids varchar(MAX)

	/*** EREC_SzamlakHistory log ***/
	set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @tempTable FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
		set @row_ids = @row_ids + '''';

	exec sp_LogRecordsToHistory_Tomeges
	 @TableName = 'EREC_Szamlak'
	,@Row_Ids = @row_ids
	,@Muvelet = 1
	,@Vegrehajto_Id = @ExecutorUserId
	,@VegrehajtasIdo = @ExecutionTime


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
END CATCH


GO
