IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_TargySzavakGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_TargySzavakGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_TargySzavakGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_TargySzavakGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_TargySzavakGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_TargySzavak.Id,
	   EREC_TargySzavak.Org,
	   EREC_TargySzavak.Tipus,
	   EREC_TargySzavak.TargySzavak,
	   EREC_TargySzavak.AlapertelmezettErtek,
	   EREC_TargySzavak.BelsoAzonosito,
	   EREC_TargySzavak.SPSSzinkronizalt,
	   EREC_TargySzavak.SPS_Field_Id,
	   EREC_TargySzavak.Csoport_Id_Tulaj,
	   EREC_TargySzavak.RegExp,
	   EREC_TargySzavak.ToolTip,
	   EREC_TargySzavak.ControlTypeSource,
	   EREC_TargySzavak.ControlTypeDataSource,
	   EREC_TargySzavak.XSD,
	   EREC_TargySzavak.Ver,
	   EREC_TargySzavak.Note,
	   EREC_TargySzavak.Stat_id,
	   EREC_TargySzavak.ErvKezd,
	   EREC_TargySzavak.ErvVege,
	   EREC_TargySzavak.Letrehozo_id,
	   EREC_TargySzavak.LetrehozasIdo,
	   EREC_TargySzavak.Modosito_id,
	   EREC_TargySzavak.ModositasIdo,
	   EREC_TargySzavak.Zarolo_id,
	   EREC_TargySzavak.ZarolasIdo,
	   EREC_TargySzavak.Tranz_id,
	   EREC_TargySzavak.UIAccessLog_id
	   from 
		 EREC_TargySzavak as EREC_TargySzavak 
	   where
		 EREC_TargySzavak.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
