IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_TargySzavakGetAllByCsoportIdTulaj]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_TargySzavakGetAllByCsoportIdTulaj]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_TargySzavakGetAllByCsoportIdTulaj]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_TargySzavakGetAllByCsoportIdTulaj] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_TargySzavakGetAllByCsoportIdTulaj]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_TargySzavak.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId	uniqueidentifier,
  @Csoport_Id_Tulaj	uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
	
   
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_TargySzavak.Id,
	   EREC_TargySzavak.Org,
	   EREC_TargySzavak.Tipus,
	   EREC_TargySzavak.TargySzavak,
	   EREC_TargySzavak.AlapertelmezettErtek,
	   EREC_TargySzavak.BelsoAzonosito,
	   EREC_TargySzavak.SPSSzinkronizalt,
	   EREC_TargySzavak.SPS_Field_Id,
	   EREC_TargySzavak.Csoport_Id_Tulaj,
       EREC_TargySzavak.RegExp,
       EREC_TargySzavak.ToolTip,
	   EREC_TargySzavak.ControlTypeSource,
	   EREC_TargySzavak.ControlTypeDataSource,
	   EREC_TargySzavak.XSD,
	   EREC_TargySzavak.Ver,
	   EREC_TargySzavak.Note,
	   EREC_TargySzavak.Stat_id,
	   EREC_TargySzavak.ErvKezd,
	   EREC_TargySzavak.ErvVege,
	   EREC_TargySzavak.Letrehozo_id,
	   EREC_TargySzavak.LetrehozasIdo,
	   EREC_TargySzavak.Modosito_id,
	   EREC_TargySzavak.ModositasIdo,
	   EREC_TargySzavak.Zarolo_id,
	   EREC_TargySzavak.ZarolasIdo,
	   EREC_TargySzavak.Tranz_id,
	   EREC_TargySzavak.UIAccessLog_id  
   from 
     EREC_TargySzavak as EREC_TargySzavak      
            Where EREC_TargySzavak.Org=''' + CAST(@Org as Nvarchar(40)) + '''
            AND EREC_TargySzavak.Csoport_Id_Tulaj = ''' + CAST(@Csoport_Id_Tulaj as Nvarchar(40)) + ''''
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
