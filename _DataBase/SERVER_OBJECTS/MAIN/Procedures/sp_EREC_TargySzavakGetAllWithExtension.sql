IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_TargySzavakGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_TargySzavakGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_TargySzavakGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_TargySzavakGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_TargySzavakGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_TargySzavak.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId	uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
	
   
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_TargySzavak.Id,
	   EREC_TargySzavak.Org,
	   EREC_TargySzavak.Tipus,
	   EREC_TargySzavak.TargySzavak,
	   EREC_TargySzavak.Csoport_Id_Tulaj,
	   (select Nev FROM KRT_Csoportok WHERE KRT_Csoportok.ID = EREC_TargySzavak.Csoport_Id_Tulaj) AS CsoportNevTulaj,
	   EREC_TargySzavak.AlapertelmezettErtek,
case EREC_TargySzavak.ControlTypeSource
when ''~/Component/EditablePartnerTextBox.ascx'' then p.Nev 
when ''~/Component/KodTarakDropDownList.ascx'' then kt.Nev
else AlapertelmezettErtek end as AlapertelmezettErtek_Lekepezett,
	   EREC_TargySzavak.BelsoAzonosito,
	   EREC_TargySzavak.SPSSzinkronizalt,
	   EREC_TargySzavak.SPS_Field_Id,
	   EREC_TargySzavak.RegExp,
       EREC_TargySzavak.ToolTip,
	   EREC_TargySzavak.ControlTypeSource,
	   EREC_TargySzavak.ControlTypeDataSource,
	   EREC_TargySzavak.XSD,
	   EREC_TargySzavak.Ver,
	   EREC_TargySzavak.Note,
	   EREC_TargySzavak.Stat_id,
	   EREC_TargySzavak.ErvKezd,
	   EREC_TargySzavak.ErvVege,
	   EREC_TargySzavak.Letrehozo_id,
	   EREC_TargySzavak.LetrehozasIdo,
	   EREC_TargySzavak.Modosito_id,
	   EREC_TargySzavak.ModositasIdo,
	   EREC_TargySzavak.Zarolo_id,
	   EREC_TargySzavak.ZarolasIdo,
	   EREC_TargySzavak.Tranz_id,
	   EREC_TargySzavak.UIAccessLog_id  
   from 
     EREC_TargySzavak as EREC_TargySzavak
left join KRT_Partnerek p on convert(varchar(36), p.Id) = EREC_TargySzavak.AlapertelmezettErtek
left join KRT_KodTarak kt on kt.Kod=EREC_TargySzavak.AlapertelmezettErtek collate Hungarian_CS_AS
		and kt.KodCsoport_Id = (select top 1 Id from KRT_KodCsoportok
				where Kod=EREC_TargySzavak.ControlTypeDataSource collate Hungarian_CS_AS
				and getdate() between ErvKezd and ErvVege)
		and kt.Org=@Org
		and getdate() between kt.ErvKezd and kt.ErvVege      
            Where EREC_TargySzavak.Org=@Org
'
            
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
--   exec (@sqlcmd);

-- Hozzáadjuk az @Org azonosítót is:
   execute sp_executesql @sqlcmd,N'@ExecutorUserId UNIQUEIDENTIFIER, @Org UNIQUEIDENTIFIER',
		@ExecutorUserId = @ExecutorUserId, @Org = @Org;


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
