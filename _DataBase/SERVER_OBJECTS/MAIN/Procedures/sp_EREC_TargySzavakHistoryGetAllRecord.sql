IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_TargySzavakHistoryGetAllRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_TargySzavakHistoryGetAllRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_TargySzavakHistoryGetAllRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_TargySzavakHistoryGetAllRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_TargySzavakHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_TargySzavakHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Org' as ColumnName,               cast(Old.Org as nvarchar(99)) as OldValue,
               cast(New.Org as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Org != New.Org 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tipus' as ColumnName,               cast(Old.Tipus as nvarchar(99)) as OldValue,
               cast(New.Tipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tipus != New.Tipus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TargySzavak' as ColumnName,               cast(Old.TargySzavak as nvarchar(99)) as OldValue,
               cast(New.TargySzavak as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.TargySzavak != New.TargySzavak 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlapertelmezettErtek' as ColumnName,               cast(Old.AlapertelmezettErtek as nvarchar(99)) as OldValue,
               cast(New.AlapertelmezettErtek as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlapertelmezettErtek != New.AlapertelmezettErtek 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BelsoAzonosito' as ColumnName,               cast(Old.BelsoAzonosito as nvarchar(99)) as OldValue,
               cast(New.BelsoAzonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BelsoAzonosito != New.BelsoAzonosito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SPSSzinkronizalt' as ColumnName,               cast(Old.SPSSzinkronizalt as nvarchar(99)) as OldValue,
               cast(New.SPSSzinkronizalt as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SPSSzinkronizalt != New.SPSSzinkronizalt 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SPS_Field_Id' as ColumnName,               cast(Old.SPS_Field_Id as nvarchar(99)) as OldValue,
               cast(New.SPS_Field_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SPS_Field_Id != New.SPS_Field_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Tulaj' as ColumnName,               cast(Old.Csoport_Id_Tulaj as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Tulaj as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Tulaj != New.Csoport_Id_Tulaj 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'RegExp' as ColumnName,               cast(Old.RegExp as nvarchar(99)) as OldValue,
               cast(New.RegExp as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.RegExp != New.RegExp 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ToolTip' as ColumnName,               cast(Old.ToolTip as nvarchar(99)) as OldValue,
               cast(New.ToolTip as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ToolTip != New.ToolTip 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ControlTypeSource' as ColumnName,               cast(Old.ControlTypeSource as nvarchar(99)) as OldValue,
               cast(New.ControlTypeSource as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ControlTypeSource != New.ControlTypeSource 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ControlTypeDataSource' as ColumnName,               cast(Old.ControlTypeDataSource as nvarchar(99)) as OldValue,
               cast(New.ControlTypeDataSource as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ControlTypeDataSource != New.ControlTypeDataSource 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
--      union all 
--      (select  New.HistoryId as RowId, 
--               New.Ver as Ver, 
--               case New.HistoryMuvelet_Id 
--                  when 1 then 'Módosítás'
--                  when 2 then 'Érvénytelenítés'
--               end as Operation, 
--               'XSD' as ColumnName,               cast(Old.XSD as nvarchar(99)) as OldValue,
--               cast(New.XSD as nvarchar(99)) as NewValue,               
--               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
--      from EREC_TargySzavakHistory Old
--         inner join EREC_TargySzavakHistory New on Old.Ver = New.Ver-1
--            and Old.Id = New.Id
--            and Old.XSD != New.XSD 
--            and Old.Id = New.Id
--         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
--)
            
end


GO
