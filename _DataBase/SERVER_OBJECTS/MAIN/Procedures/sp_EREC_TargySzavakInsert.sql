IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_TargySzavakInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_TargySzavakInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_TargySzavakInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_TargySzavakInsert] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_TargySzavakInsert]    
                @Id      uniqueidentifier = null,    
                           @Org     uniqueidentifier = null,    
                @Tipus     char(1)  = null,
	            @TargySzavak     Nvarchar(100),
                @AlapertelmezettErtek     Nvarchar(100)  = null,
                @BelsoAzonosito     Nvarchar(100)  = null,
                @SPSSzinkronizalt     char(1)  = null,
                @SPS_Field_Id     uniqueidentifier  = null,
                @Csoport_Id_Tulaj     uniqueidentifier  = null,
                @RegExp     Nvarchar(4000)  = null,
                @ToolTip     Nvarchar(400)  = null,
                @ControlTypeSource     nvarchar(64)  = null,
                @ControlTypeDataSource     Nvarchar(4000)  = null,
                @XSD     xml  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
 IF @ORG is NULL
 BEGIN 
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@Letrehozo_id)
   if (@Org is null)
   begin
   	RAISERROR('[50302]',16,1)
   end
END
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Org is not null
         begin
            SET @insertColumns = @insertColumns + ',Org'
            SET @insertValues = @insertValues + ',@Org'
         end 
       
         if @Tipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Tipus'
            SET @insertValues = @insertValues + ',@Tipus'
         end 
       
         if @TargySzavak is not null
         begin
            SET @insertColumns = @insertColumns + ',TargySzavak'
            SET @insertValues = @insertValues + ',@TargySzavak'
         end 
       
         if @AlapertelmezettErtek is not null
         begin
            SET @insertColumns = @insertColumns + ',AlapertelmezettErtek'
            SET @insertValues = @insertValues + ',@AlapertelmezettErtek'
         end 
       
         if @BelsoAzonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',BelsoAzonosito'
            SET @insertValues = @insertValues + ',@BelsoAzonosito'
         end 
       
         if @SPSSzinkronizalt is not null
         begin
            SET @insertColumns = @insertColumns + ',SPSSzinkronizalt'
            SET @insertValues = @insertValues + ',@SPSSzinkronizalt'
         end 
       
         if @SPS_Field_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',SPS_Field_Id'
            SET @insertValues = @insertValues + ',@SPS_Field_Id'
         end 
       
         if @Csoport_Id_Tulaj is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Tulaj'
            SET @insertValues = @insertValues + ',@Csoport_Id_Tulaj'
         end 
       
         if @RegExp is not null
         begin
            SET @insertColumns = @insertColumns + ',RegExp'
            SET @insertValues = @insertValues + ',@RegExp'
         end 
       
         if @ToolTip is not null
         begin
            SET @insertColumns = @insertColumns + ',ToolTip'
            SET @insertValues = @insertValues + ',@ToolTip'
         end 
       
         if @ControlTypeSource is not null
         begin
            SET @insertColumns = @insertColumns + ',ControlTypeSource'
            SET @insertValues = @insertValues + ',@ControlTypeSource'
         end 
       
         if @ControlTypeDataSource is not null
         begin
            SET @insertColumns = @insertColumns + ',ControlTypeDataSource'
            SET @insertValues = @insertValues + ',@ControlTypeDataSource'
         end 
       
         if @XSD is not null
         begin
            SET @insertColumns = @insertColumns + ',XSD'
            SET @insertValues = @insertValues + ',@XSD'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_TargySzavak ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Org uniqueidentifier,@Tipus char(1),@TargySzavak Nvarchar(100),@AlapertelmezettErtek Nvarchar(100),@BelsoAzonosito Nvarchar(100),@SPSSzinkronizalt char(1),@SPS_Field_Id uniqueidentifier,@Csoport_Id_Tulaj uniqueidentifier,@RegExp Nvarchar(4000),@ToolTip Nvarchar(400),@ControlTypeSource nvarchar(64),@ControlTypeDataSource Nvarchar(4000),@XSD xml,@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Org = @Org,@Tipus = @Tipus,@TargySzavak = @TargySzavak,@AlapertelmezettErtek = @AlapertelmezettErtek,@BelsoAzonosito = @BelsoAzonosito,@SPSSzinkronizalt = @SPSSzinkronizalt,@SPS_Field_Id = @SPS_Field_Id,@Csoport_Id_Tulaj = @Csoport_Id_Tulaj,@RegExp = @RegExp,@ToolTip = @ToolTip,@ControlTypeSource = @ControlTypeSource,@ControlTypeDataSource = @ControlTypeDataSource,@XSD = @XSD,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_TargySzavak',@ResultUid
					,'EREC_TargySzavakHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
