IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_TargySzavakUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_TargySzavakUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_TargySzavakUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_TargySzavakUpdate] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_TargySzavakUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Org     uniqueidentifier  = null,         
             @Tipus     char(1)  = null,         
             @TargySzavak     Nvarchar(100)  = null,         
             @AlapertelmezettErtek     Nvarchar(100)  = null,         
             @BelsoAzonosito     Nvarchar(100)  = null,         
             @SPSSzinkronizalt     char(1)  = null,         
             @SPS_Field_Id     uniqueidentifier  = null,         
             @Csoport_Id_Tulaj     uniqueidentifier  = null,         
             @RegExp     Nvarchar(4000)  = null,         
             @ToolTip     Nvarchar(400)  = null,         
             @ControlTypeSource     nvarchar(64)  = null,         
             @ControlTypeDataSource     Nvarchar(4000)  = null,         
             @XSD     xml  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Org     uniqueidentifier         
     DECLARE @Act_Tipus     char(1)         
     DECLARE @Act_TargySzavak     Nvarchar(100)         
     DECLARE @Act_AlapertelmezettErtek     Nvarchar(100)         
     DECLARE @Act_BelsoAzonosito     Nvarchar(100)         
     DECLARE @Act_SPSSzinkronizalt     char(1)         
     DECLARE @Act_SPS_Field_Id     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Tulaj     uniqueidentifier         
     DECLARE @Act_RegExp     Nvarchar(4000)         
     DECLARE @Act_ToolTip     Nvarchar(400)         
     DECLARE @Act_ControlTypeSource     nvarchar(64)         
     DECLARE @Act_ControlTypeDataSource     Nvarchar(4000)         
     DECLARE @Act_XSD     xml         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Org = Org,
     @Act_Tipus = Tipus,
     @Act_TargySzavak = TargySzavak,
     @Act_AlapertelmezettErtek = AlapertelmezettErtek,
     @Act_BelsoAzonosito = BelsoAzonosito,
     @Act_SPSSzinkronizalt = SPSSzinkronizalt,
     @Act_SPS_Field_Id = SPS_Field_Id,
     @Act_Csoport_Id_Tulaj = Csoport_Id_Tulaj,
     @Act_RegExp = RegExp,
     @Act_ToolTip = ToolTip,
     @Act_ControlTypeSource = ControlTypeSource,
     @Act_ControlTypeDataSource = ControlTypeDataSource,
     @Act_XSD = XSD,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_TargySzavak
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Org')=1
         SET @Act_Org = @Org
   IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @Act_Tipus = @Tipus
   IF @UpdatedColumns.exist('/root/TargySzavak')=1
         SET @Act_TargySzavak = @TargySzavak
   IF @UpdatedColumns.exist('/root/AlapertelmezettErtek')=1
         SET @Act_AlapertelmezettErtek = @AlapertelmezettErtek
   IF @UpdatedColumns.exist('/root/BelsoAzonosito')=1
         SET @Act_BelsoAzonosito = @BelsoAzonosito
   IF @UpdatedColumns.exist('/root/SPSSzinkronizalt')=1
         SET @Act_SPSSzinkronizalt = @SPSSzinkronizalt
   IF @UpdatedColumns.exist('/root/SPS_Field_Id')=1
         SET @Act_SPS_Field_Id = @SPS_Field_Id
   IF @UpdatedColumns.exist('/root/Csoport_Id_Tulaj')=1
         SET @Act_Csoport_Id_Tulaj = @Csoport_Id_Tulaj
   IF @UpdatedColumns.exist('/root/RegExp')=1
         SET @Act_RegExp = @RegExp
   IF @UpdatedColumns.exist('/root/ToolTip')=1
         SET @Act_ToolTip = @ToolTip
   IF @UpdatedColumns.exist('/root/ControlTypeSource')=1
         SET @Act_ControlTypeSource = @ControlTypeSource
   IF @UpdatedColumns.exist('/root/ControlTypeDataSource')=1
         SET @Act_ControlTypeDataSource = @ControlTypeDataSource
   IF @UpdatedColumns.exist('/root/XSD')=1
         SET @Act_XSD = @XSD
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_TargySzavak
SET
     Org = @Act_Org,
     Tipus = @Act_Tipus,
     TargySzavak = @Act_TargySzavak,
     AlapertelmezettErtek = @Act_AlapertelmezettErtek,
     BelsoAzonosito = @Act_BelsoAzonosito,
     SPSSzinkronizalt = @Act_SPSSzinkronizalt,
     SPS_Field_Id = @Act_SPS_Field_Id,
     Csoport_Id_Tulaj = @Act_Csoport_Id_Tulaj,
     RegExp = @Act_RegExp,
     ToolTip = @Act_ToolTip,
     ControlTypeSource = @Act_ControlTypeSource,
     ControlTypeDataSource = @Act_ControlTypeDataSource,
     XSD = @Act_XSD,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_TargySzavak',@Id
					,'EREC_TargySzavakHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
