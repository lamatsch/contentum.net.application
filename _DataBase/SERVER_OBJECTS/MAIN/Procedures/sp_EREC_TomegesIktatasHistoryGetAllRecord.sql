IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_TomegesIktatasHistoryGetAllRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_TomegesIktatasHistoryGetAllRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_TomegesIktatasHistoryGetAllRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_TomegesIktatasHistoryGetAllRecord] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_TomegesIktatasHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_TomegesIktatasHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ForrasTipusNev' as ColumnName,               cast(Old.ForrasTipusNev as nvarchar(99)) as OldValue,
               cast(New.ForrasTipusNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ForrasTipusNev != New.ForrasTipusNev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelelosCsoport_Id' as ColumnName,               cast(Old.FelelosCsoport_Id as nvarchar(99)) as OldValue,
               cast(New.FelelosCsoport_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelelosCsoport_Id != New.FelelosCsoport_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AgazatiJelek_Id' as ColumnName,               cast(Old.AgazatiJelek_Id as nvarchar(99)) as OldValue,
               cast(New.AgazatiJelek_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AgazatiJelek_Id != New.AgazatiJelek_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIrattariTetelek_Id' as ColumnName,               cast(Old.IraIrattariTetelek_Id as nvarchar(99)) as OldValue,
               cast(New.IraIrattariTetelek_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IraIrattariTetelek_Id != New.IraIrattariTetelek_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ugytipus_Id' as ColumnName,               cast(Old.Ugytipus_Id as nvarchar(99)) as OldValue,
               cast(New.Ugytipus_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Ugytipus_Id != New.Ugytipus_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Irattipus_Id' as ColumnName,               cast(Old.Irattipus_Id as nvarchar(99)) as OldValue,
               cast(New.Irattipus_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Irattipus_Id != New.Irattipus_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TargyPrefix' as ColumnName,               cast(Old.TargyPrefix as nvarchar(99)) as OldValue,
               cast(New.TargyPrefix as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.TargyPrefix != New.TargyPrefix 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasKell' as ColumnName,               cast(Old.AlairasKell as nvarchar(99)) as OldValue,
               cast(New.AlairasKell as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasKell != New.AlairasKell 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasMod' as ColumnName,               cast(Old.AlairasMod as nvarchar(99)) as OldValue,
               cast(New.AlairasMod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasMod != New.AlairasMod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Alairo' as ColumnName,               cast(Old.FelhasznaloCsoport_Id_Alairo as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Alairo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhasznaloCsoport_Id_Alairo != New.FelhasznaloCsoport_Id_Alairo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhaszCsoport_Id_Helyettesito' as ColumnName,               cast(Old.FelhaszCsoport_Id_Helyettesito as nvarchar(99)) as OldValue,
               cast(New.FelhaszCsoport_Id_Helyettesito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelhaszCsoport_Id_Helyettesito != New.FelhaszCsoport_Id_Helyettesito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasSzabaly_Id' as ColumnName,               cast(Old.AlairasSzabaly_Id as nvarchar(99)) as OldValue,
               cast(New.AlairasSzabaly_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasSzabaly_Id != New.AlairasSzabaly_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatosagiAdatlapKell' as ColumnName,               cast(Old.HatosagiAdatlapKell as nvarchar(99)) as OldValue,
               cast(New.HatosagiAdatlapKell as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HatosagiAdatlapKell != New.HatosagiAdatlapKell 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyFajtaja' as ColumnName,               cast(Old.UgyFajtaja as nvarchar(99)) as OldValue,
               cast(New.UgyFajtaja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyFajtaja != New.UgyFajtaja 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DontestHozta' as ColumnName,               cast(Old.DontestHozta as nvarchar(99)) as OldValue,
               cast(New.DontestHozta as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.DontestHozta != New.DontestHozta 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DontesFormaja' as ColumnName,               cast(Old.DontesFormaja as nvarchar(99)) as OldValue,
               cast(New.DontesFormaja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.DontesFormaja != New.DontesFormaja 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesHataridore' as ColumnName,               cast(Old.UgyintezesHataridore as nvarchar(99)) as OldValue,
               cast(New.UgyintezesHataridore as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UgyintezesHataridore != New.UgyintezesHataridore 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HataridoTullepes' as ColumnName,               cast(Old.HataridoTullepes as nvarchar(99)) as OldValue,
               cast(New.HataridoTullepes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HataridoTullepes != New.HataridoTullepes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatosagiEllenorzes' as ColumnName,               cast(Old.HatosagiEllenorzes as nvarchar(99)) as OldValue,
               cast(New.HatosagiEllenorzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HatosagiEllenorzes != New.HatosagiEllenorzes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MunkaorakSzama' as ColumnName,               cast(Old.MunkaorakSzama as nvarchar(99)) as OldValue,
               cast(New.MunkaorakSzama as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MunkaorakSzama != New.MunkaorakSzama 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EljarasiKoltseg' as ColumnName,               cast(Old.EljarasiKoltseg as nvarchar(99)) as OldValue,
               cast(New.EljarasiKoltseg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.EljarasiKoltseg != New.EljarasiKoltseg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KozigazgatasiBirsagMerteke' as ColumnName,               cast(Old.KozigazgatasiBirsagMerteke as nvarchar(99)) as OldValue,
               cast(New.KozigazgatasiBirsagMerteke as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KozigazgatasiBirsagMerteke != New.KozigazgatasiBirsagMerteke 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SommasEljDontes' as ColumnName,               cast(Old.SommasEljDontes as nvarchar(99)) as OldValue,
               cast(New.SommasEljDontes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SommasEljDontes != New.SommasEljDontes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'NyolcNapBelulNemSommas' as ColumnName,               cast(Old.NyolcNapBelulNemSommas as nvarchar(99)) as OldValue,
               cast(New.NyolcNapBelulNemSommas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.NyolcNapBelulNemSommas != New.NyolcNapBelulNemSommas 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FuggoHatalyuHatarozat' as ColumnName,               cast(Old.FuggoHatalyuHatarozat as nvarchar(99)) as OldValue,
               cast(New.FuggoHatalyuHatarozat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FuggoHatalyuHatarozat != New.FuggoHatalyuHatarozat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FuggoHatalyuVegzes' as ColumnName,               cast(Old.FuggoHatalyuVegzes as nvarchar(99)) as OldValue,
               cast(New.FuggoHatalyuVegzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FuggoHatalyuVegzes != New.FuggoHatalyuVegzes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatAltalVisszafizOsszeg' as ColumnName,               cast(Old.HatAltalVisszafizOsszeg as nvarchar(99)) as OldValue,
               cast(New.HatAltalVisszafizOsszeg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HatAltalVisszafizOsszeg != New.HatAltalVisszafizOsszeg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatTerheloEljKtsg' as ColumnName,               cast(Old.HatTerheloEljKtsg as nvarchar(99)) as OldValue,
               cast(New.HatTerheloEljKtsg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HatTerheloEljKtsg != New.HatTerheloEljKtsg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelfuggHatarozat' as ColumnName,               cast(Old.FelfuggHatarozat as nvarchar(99)) as OldValue,
               cast(New.FelfuggHatarozat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FelfuggHatarozat != New.FelfuggHatarozat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end


GO
