IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyKezFeljegyzesekGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyKezFeljegyzesekGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyKezFeljegyzesekGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyKezFeljegyzesekGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyKezFeljegyzesekGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_UgyKezFeljegyzesek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_UgyKezFeljegyzesek.Id,
	   EREC_UgyKezFeljegyzesek.UgyUgyirat_Id,
	   EREC_UgyKezFeljegyzesek.KezelesTipus,
KRT_KodTarak.Nev as KezelesTipusNev,
EREC_UgyUgyiratok.Azonosito as Foszam_Merge,
	   EREC_UgyKezFeljegyzesek.Leiras,
0 as Sorszam,	-- kompatibilitás a példányokkal
	   EREC_UgyKezFeljegyzesek.Ver,
	   EREC_UgyKezFeljegyzesek.Note,
	   EREC_UgyKezFeljegyzesek.Stat_id,
	   EREC_UgyKezFeljegyzesek.ErvKezd,
	   EREC_UgyKezFeljegyzesek.ErvVege,
	   EREC_UgyKezFeljegyzesek.Letrehozo_id,
KRT_Felhasznalok.Nev as Letrehozo_Nev,
	   EREC_UgyKezFeljegyzesek.LetrehozasIdo,
CONVERT(nvarchar(10), EREC_UgyKezFeljegyzesek.LetrehozasIdo, 102) as LetrehozasIdo_f,
	   EREC_UgyKezFeljegyzesek.Modosito_id,
	   EREC_UgyKezFeljegyzesek.ModositasIdo,
	   EREC_UgyKezFeljegyzesek.Zarolo_id,
	   EREC_UgyKezFeljegyzesek.ZarolasIdo,
	   EREC_UgyKezFeljegyzesek.Tranz_id,
	   EREC_UgyKezFeljegyzesek.UIAccessLog_id  
   from 
     EREC_UgyKezFeljegyzesek as EREC_UgyKezFeljegyzesek      
        left join KRT_KodCsoportok on KRT_KodCsoportok.Kod=''KEZELESI_FELJEGYZESEK_TIPUSA''
		left join KRT_KodTarak on KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and EREC_UgyKezFeljegyzesek.KezelesTipus = KRT_KodTarak.Kod
			and KRT_KodTarak.Org=''' + cast(@Org as NVarChar(40)) + '''
	LEFT JOIN KRT_Felhasznalok
		ON EREC_UgyKezFeljegyzesek.Letrehozo_id = KRT_Felhasznalok.Id
left join EREC_UgyUgyiratok on EREC_UgyKezFeljegyzesek.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek
				ON EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id
where KRT_Felhasznalok.Org=''' + cast(@Org as NVarChar(40)) + '''
and EREC_IraIktatokonyvek.Org=''' + cast(@Org as NVarChar(40)) + '''
          '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
