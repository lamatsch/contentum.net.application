IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratdarabokGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratdarabokGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratdarabokGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratdarabokGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyUgyiratdarabokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_UgyUgyiratdarabok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_UgyUgyiratdarabok.Id,
	   EREC_UgyUgyiratdarabok.UgyUgyirat_Id,
	   EREC_UgyUgyiratdarabok.EljarasiSzakasz,
	   EREC_UgyUgyiratdarabok.Sorszam,
	   EREC_UgyUgyiratdarabok.Azonosito,
	   EREC_UgyUgyiratdarabok.Hatarido,
	   EREC_UgyUgyiratdarabok.FelhasznaloCsoport_Id_Ugyintez,
	   EREC_UgyUgyiratdarabok.ElintezesDat,
	   EREC_UgyUgyiratdarabok.LezarasDat,
	   EREC_UgyUgyiratdarabok.ElintezesMod,
	   EREC_UgyUgyiratdarabok.LezarasOka,
	   EREC_UgyUgyiratdarabok.Leiras,
	   EREC_UgyUgyiratdarabok.UgyUgyirat_Id_Elozo,
	   EREC_UgyUgyiratdarabok.IraIktatokonyv_Id,
	   EREC_UgyUgyiratdarabok.Foszam,
	   EREC_UgyUgyiratdarabok.UtolsoAlszam,
	   EREC_UgyUgyiratdarabok.Csoport_Id_Felelos,
	   EREC_UgyUgyiratdarabok.Csoport_Id_Felelos_Elozo,
	   EREC_UgyUgyiratdarabok.Allapot,
	   EREC_UgyUgyiratdarabok.Ver,
	   EREC_UgyUgyiratdarabok.Note,
	   EREC_UgyUgyiratdarabok.Stat_id,
	   EREC_UgyUgyiratdarabok.ErvKezd,
	   EREC_UgyUgyiratdarabok.ErvVege,
	   EREC_UgyUgyiratdarabok.Letrehozo_id,
	   EREC_UgyUgyiratdarabok.LetrehozasIdo,
	   EREC_UgyUgyiratdarabok.Modosito_id,
	   EREC_UgyUgyiratdarabok.ModositasIdo,
	   EREC_UgyUgyiratdarabok.Zarolo_id,
	   EREC_UgyUgyiratdarabok.ZarolasIdo,
	   EREC_UgyUgyiratdarabok.Tranz_id,
	   EREC_UgyUgyiratdarabok.UIAccessLog_id  
   from 
     EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
