IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratdarabokGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratdarabokGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratdarabokGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratdarabokGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyUgyiratdarabokGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_UgyUgyiratdarabok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @FelhasznaloSzervezet_Id	uniqueidentifier,  
  @Jogosultak		char(1) = '0',
  @pageNumber		int = 0,
  @pageSize			int = -1,
  @SelectedRowId	uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
  
   DECLARE @sqlcmd nvarchar(MAX)


   DECLARE @LocalTopRow nvarchar(10)
	declare @firstRow int
	declare @lastRow int

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
		if (@pageSize > @TopRow)
			SET @pageSize = @TopRow;
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
                  
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = 1000
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end     
    set @sqlcmd = '';
    

	/************************************************************
	* Szurési tábla összeállítása								*
	************************************************************/
	DECLARE @ObjTipId uniqueidentifier;
	select @ObjTipId = Id from KRT_Objtipusok where Kod = 'EREC_UgyUgyiratdarabok';

	SET @sqlcmd = 'select EREC_UgyUgyiratdarabok.Id into #filter from EREC_UgyUgyiratdarabok
							 left join EREC_UgyUgyiratok as EREC_UgyUgyiratok 
									on EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id  
							 left join EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek 
									on EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id
						where EREC_IraIktatoKonyvek.Org=''' + cast(@Org as NVarChar(40)) + '''
'
	if (@Where is not null and @Where <> '')
	begin
		SET @sqlcmd = @sqlcmd + ' and ' + @Where;
	end

	--IF @Jogosultak = '1' AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
    IF @Jogosultak = '1' AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
		SET @sqlcmd = @sqlcmd + ' DELETE FROM #filter WHERE Id NOT IN
									(
										SELECT EREC_UgyUgyiratdarabok.Id FROM EREC_UgyUgyiratdarabok
											INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_UgyUgyiratdarabok.Id
											INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
										UNION ALL
										SELECT erec_ugyugyiratdarabok.Id
											FROM erec_ugyugyiratdarabok
											WHERE erec_ugyugyiratdarabok.UgyUgyirat_Id IN
											(
												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
													INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
												UNION ALL
												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
													INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
												UNION ALL
												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll 
														ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
												UNION ALL
												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll 
														ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
												UNION ALL
												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll 
														ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
												UNION ALL
												SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''', ''EREC_UgyUgyiratok'')
											)
										UNION ALL
										SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''', ''EREC_UgyUgyiratdarabok'')

									)'

	/************************************************************
	* Szurt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/
		SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   EREC_UgyUgyiratdarabok.Id into #result
       from EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok	
			 left join EREC_UgyUgyiratok as EREC_UgyUgyiratok 
					on EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id  
			 left join EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek 
					on EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id
			left join KRT_Kodcsoportok as AllapotKodCsoport on AllapotKodCsoport.Kod = ''UGYIRATDARAB_ALLAPOT''
				left join KRT_Kodtarak as AllapotKodTarak on AllapotKodTarak.Kodcsoport_Id = AllapotKodCsoport.Id and AllapotKodTarak.Kod = EREC_UgyUgyiratdarabok.Allapot
					 and AllapotKodtarak.Org=''' + CAST(@Org as NVarChar(40)) + '''
			left join KRT_Kodcsoportok as EljarasiSzakaszKodCsoport on EljarasiSzakaszKodCsoport.Kod = ''ELJARASI_SZAKASZ''
				left join KRT_Kodtarak as EljarasiSzakaszKodTarak on EljarasiSzakaszKodTarak.Kodcsoport_Id = EljarasiSzakaszKodCsoport.Id and EljarasiSzakaszKodTarak.Kod = EREC_UgyUgyiratdarabok.EljarasiSzakasz
					and EljarasiSzakaszkodTarak.Org=''' + CAST(@Org as NVarChar(40)) + '''

		where EREC_UgyUgyiratdarabok.Id in (select Id from #filter); '

		if (@SelectedRowId is not null)
			set @sqlcmd = @sqlcmd + N'
			if exists (select 1 from #result where Id = ''' + cast(@SelectedRowId as nvarchar(36)) + ''')
			BEGIN
				select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = ''' + cast(@SelectedRowId as nvarchar(36)) + ''';
				set @firstRow = (@pageNumber - 1) * @pageSize + 1;
				set @lastRow = @pageNumber*@pageSize;
				select @pageNumber = @pageNumber - 1
			END
			ELSE'
		--ELSE
			set @sqlcmd = @sqlcmd + N' 
				if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
				BEGIN
					select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
					set @firstRow = (@pageNumber - 1) * @pageSize + 1;
					set @lastRow = @pageNumber*@pageSize;
					select @pageNumber = @pageNumber - 1
				END ;'


	/************************************************************
	* Tényleges select											*
	************************************************************/
		set @sqlcmd = @sqlcmd + N'
		select 
		#result.RowNumber,
		#result.Id,
  	   EREC_UgyUgyiratdarabok.Id,
	   EREC_UgyUgyiratdarabok.UgyUgyirat_Id,
		dbo.fn_MergeFoszam(EREC_IraIktatokonyvek.MegkulJelzes,EREC_IraIktatokonyvek.Iktatohely,EREC_IraIktatokonyvek.KozpontiIktatasJelzo
			,EREC_IraIktatokonyvek.Ev,EREC_UgyUgyiratdarabok.Foszam) as Foszam_Merge,
	   EREC_UgyUgyiratdarabok.EljarasiSzakasz,
dbo.fn_KodtarErtekNeve(''ELJARASI_SZAKASZ'', EREC_UgyUgyiratdarabok.EljarasiSzakasz,''' + CAST(@Org as NVarChar(40)) + ''') as EljarasiSzakasz_Nev,
	--KRT_KodTarak_EljarasiSzakasz.Nev as EljarasiSzakasz_Nev,
	   EREC_UgyUgyiratdarabok.Azonosito,
	   CONVERT(nvarchar(10), EREC_UgyUgyiratdarabok.Hatarido, 102) as Hatarido,
	   EREC_UgyUgyiratdarabok.FelhasznaloCsoport_Id_Ugyintez,
CONVERT(nvarchar(10), EREC_UgyUgyiratdarabok.ElintezesDat, 102) as ElintezesDat,	   
CONVERT(nvarchar(10), EREC_UgyUgyiratdarabok.LezarasDat, 102) as LezarasDat,	   
	   EREC_UgyUgyiratdarabok.ElintezesMod,
	   EREC_UgyUgyiratdarabok.LezarasOka,
	   EREC_UgyUgyiratdarabok.Leiras,
	   EREC_UgyUgyiratdarabok.UgyUgyirat_Id_Elozo,
	   EREC_UgyUgyiratdarabok.IraIktatokonyv_Id,
	   EREC_UgyUgyiratdarabok.Foszam,
	   EREC_UgyUgyiratdarabok.UtolsoAlszam,
	   EREC_UgyUgyiratdarabok.Csoport_Id_Felelos,
	   EREC_UgyUgyiratdarabok.Csoport_Id_Felelos_Elozo,
	   EREC_UgyUgyiratdarabok.Allapot,
dbo.fn_KodtarErtekNeve(''UGYIRATDARAB_ALLAPOT'', EREC_UgyUgyiratdarabok.Allapot,''' + CAST(@Org as NVarChar(40)) + ''') as Allapot_Nev,
	--KRT_KodTarak_Allapot.Nev as Allapot_Nev,
	   EREC_UgyUgyiratdarabok.Ver,
	   EREC_UgyUgyiratdarabok.Note,
	   EREC_UgyUgyiratdarabok.Stat_id,
	   EREC_UgyUgyiratdarabok.ErvKezd,
	   EREC_UgyUgyiratdarabok.ErvVege,
	   EREC_UgyUgyiratdarabok.Letrehozo_id,
	   EREC_UgyUgyiratdarabok.LetrehozasIdo,
	   EREC_UgyUgyiratdarabok.Modosito_id,
	   EREC_UgyUgyiratdarabok.ModositasIdo,
	   EREC_UgyUgyiratdarabok.Zarolo_id,
	   EREC_UgyUgyiratdarabok.ZarolasIdo,
	   EREC_UgyUgyiratdarabok.Tranz_id,
	   EREC_UgyUgyiratdarabok.UIAccessLog_id  
   from EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok    
	inner join #result on #result.Id = EREC_UgyUgyiratdarabok.Id	
	 left join EREC_UgyUgyiratok as EREC_UgyUgyiratok 
			on EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id  
	 left join EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek 
			on EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id 
	where RowNumber between @firstRow and @lastRow
	ORDER BY #result.RowNumber;'
  
		-- találatok száma és oldalszám
		set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter;';

		execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
