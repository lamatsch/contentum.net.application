IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratdarabokGetWithRightCheck]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratdarabokGetWithRightCheck]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratdarabokGetWithRightCheck]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratdarabokGetWithRightCheck] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyUgyiratdarabokGetWithRightCheck]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FelhasznaloSzervezet_Id	uniqueidentifier,
		@Jogszint	char(1)
         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	--if dbo.fn_IsAdmin(@ExecutorUserId) = 0 AND not exists
	IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0 AND not exists
	(
		SELECT top(1) 1 FROM krt_jogosultak
			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			where krt_jogosultak.Obj_Id = @Id
	) and not exists
	(
		SELECT top(1) 1	from EREC_UgyUgyiratdarabok
			WHERE EREC_UgyUgyiratdarabok.Id = @Id
				AND EREC_UgyUgyiratdarabok.UgyUgyirat_Id IN 
			(
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
				UNION ALL
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id AND @Jogszint != 'I'
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
				UNION ALL
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
						ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
				UNION ALL
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
						ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
				UNION ALL
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
						ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
				UNION ALL
				SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_UgyUgyiratok')

			)
	) AND NOT EXISTS
	(
		SELECT 1 FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_UgyUgyiratdarabok') as ids
			WHERE ids.Id = @Id
	)
		RAISERROR('[50102]',16,1)

	-- Org szurés
	IF not exists(select 1 from EREC_IraIktatoKonyvek
		where EREC_IraIktatoKonyvek.Org=@Org
		and EREC_IraIktatoKonyvek.Id=
		(select EREC_UgyUgyiratdarabok.IraIktatoKonyv_Id from EREC_UgyUgyiratdarabok
		where EREC_UgyUgyiratdarabok.Id=@Id
		))
	BEGIN
		RAISERROR('[50101]',16,1)
	END
	
	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_UgyUgyiratdarabok.Id,
	   EREC_UgyUgyiratdarabok.UgyUgyirat_Id,
	   EREC_UgyUgyiratdarabok.EljarasiSzakasz,
	   EREC_UgyUgyiratdarabok.Sorszam,
	   EREC_UgyUgyiratdarabok.Azonosito,
	   EREC_UgyUgyiratdarabok.Hatarido,
	   EREC_UgyUgyiratdarabok.FelhasznaloCsoport_Id_Ugyintez,
	   EREC_UgyUgyiratdarabok.ElintezesDat,
	   EREC_UgyUgyiratdarabok.LezarasDat,
	   EREC_UgyUgyiratdarabok.ElintezesMod,
	   EREC_UgyUgyiratdarabok.LezarasOka,
	   EREC_UgyUgyiratdarabok.Leiras,
	   EREC_UgyUgyiratdarabok.UgyUgyirat_Id_Elozo,
	   EREC_UgyUgyiratdarabok.IraIktatokonyv_Id,
	   EREC_UgyUgyiratdarabok.Foszam,
	   EREC_UgyUgyiratdarabok.UtolsoAlszam,
	   EREC_UgyUgyiratdarabok.Csoport_Id_Felelos,
	   EREC_UgyUgyiratdarabok.Csoport_Id_Felelos_Elozo,
	   EREC_UgyUgyiratdarabok.Allapot,
	   EREC_UgyUgyiratdarabok.Ver,
	   EREC_UgyUgyiratdarabok.Note,
	   EREC_UgyUgyiratdarabok.Stat_id,
	   EREC_UgyUgyiratdarabok.ErvKezd,
	   EREC_UgyUgyiratdarabok.ErvVege,
	   EREC_UgyUgyiratdarabok.Letrehozo_id,
	   EREC_UgyUgyiratdarabok.LetrehozasIdo,
	   EREC_UgyUgyiratdarabok.Modosito_id,
	   EREC_UgyUgyiratdarabok.ModositasIdo,
	   EREC_UgyUgyiratdarabok.Zarolo_id,
	   EREC_UgyUgyiratdarabok.ZarolasIdo,
	   EREC_UgyUgyiratdarabok.Tranz_id,
	   EREC_UgyUgyiratdarabok.UIAccessLog_id
	   from 
		 EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok
	   where
		 EREC_UgyUgyiratdarabok.Id = @Id'

	exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
