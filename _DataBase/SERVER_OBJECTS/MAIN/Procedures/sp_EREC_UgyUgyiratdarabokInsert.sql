IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratdarabokInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratdarabokInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratdarabokInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratdarabokInsert] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyUgyiratdarabokInsert]    
                @Id      uniqueidentifier = null,    
               	            @UgyUgyirat_Id     uniqueidentifier,
                @EljarasiSzakasz     nvarchar(64)  = null,
                @Sorszam     int  = null,
                @Azonosito     Nvarchar(100)  = null,
                @Hatarido     datetime  = null,
                @FelhasznaloCsoport_Id_Ugyintez     uniqueidentifier  = null,
                @ElintezesDat     datetime  = null,
                @LezarasDat     datetime  = null,
                @ElintezesMod     nvarchar(64)  = null,
                @LezarasOka     nvarchar(64)  = null,
                @Leiras     Nvarchar(4000)  = null,
                @UgyUgyirat_Id_Elozo     uniqueidentifier  = null,
                @IraIktatokonyv_Id     uniqueidentifier  = null,
                @Foszam     int  = null,
                @UtolsoAlszam     int  = null,
                @Csoport_Id_Felelos     uniqueidentifier  = null,
                @Csoport_Id_Felelos_Elozo     uniqueidentifier  = null,
                @IratMetadefinicio_Id     uniqueidentifier  = null,
                @Allapot     nvarchar(64)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @UgyUgyirat_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyUgyirat_Id'
            SET @insertValues = @insertValues + ',@UgyUgyirat_Id'
         end 
       
         if @EljarasiSzakasz is not null
         begin
            SET @insertColumns = @insertColumns + ',EljarasiSzakasz'
            SET @insertValues = @insertValues + ',@EljarasiSzakasz'
         end 
       
         if @Sorszam is not null
         begin
            SET @insertColumns = @insertColumns + ',Sorszam'
            SET @insertValues = @insertValues + ',@Sorszam'
         end 
       
         if @Azonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Azonosito'
            SET @insertValues = @insertValues + ',@Azonosito'
         end 
       
         if @Hatarido is not null
         begin
            SET @insertColumns = @insertColumns + ',Hatarido'
            SET @insertValues = @insertValues + ',@Hatarido'
         end 
       
         if @FelhasznaloCsoport_Id_Ugyintez is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Ugyintez'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Ugyintez'
         end 
       
         if @ElintezesDat is not null
         begin
            SET @insertColumns = @insertColumns + ',ElintezesDat'
            SET @insertValues = @insertValues + ',@ElintezesDat'
         end 
       
         if @LezarasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',LezarasDat'
            SET @insertValues = @insertValues + ',@LezarasDat'
         end 
       
         if @ElintezesMod is not null
         begin
            SET @insertColumns = @insertColumns + ',ElintezesMod'
            SET @insertValues = @insertValues + ',@ElintezesMod'
         end 
       
         if @LezarasOka is not null
         begin
            SET @insertColumns = @insertColumns + ',LezarasOka'
            SET @insertValues = @insertValues + ',@LezarasOka'
         end 
       
         if @Leiras is not null
         begin
            SET @insertColumns = @insertColumns + ',Leiras'
            SET @insertValues = @insertValues + ',@Leiras'
         end 
       
         if @UgyUgyirat_Id_Elozo is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyUgyirat_Id_Elozo'
            SET @insertValues = @insertValues + ',@UgyUgyirat_Id_Elozo'
         end 
       
         if @IraIktatokonyv_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IraIktatokonyv_Id'
            SET @insertValues = @insertValues + ',@IraIktatokonyv_Id'
         end 
       
         if @Foszam is not null
         begin
            SET @insertColumns = @insertColumns + ',Foszam'
            SET @insertValues = @insertValues + ',@Foszam'
         end 
       
         if @UtolsoAlszam is not null
         begin
            SET @insertColumns = @insertColumns + ',UtolsoAlszam'
            SET @insertValues = @insertValues + ',@UtolsoAlszam'
         end 
       
         if @Csoport_Id_Felelos is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Felelos'
            SET @insertValues = @insertValues + ',@Csoport_Id_Felelos'
         end 
       
         if @Csoport_Id_Felelos_Elozo is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Felelos_Elozo'
            SET @insertValues = @insertValues + ',@Csoport_Id_Felelos_Elozo'
         end 
       
         if @IratMetadefinicio_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IratMetadefinicio_Id'
            SET @insertValues = @insertValues + ',@IratMetadefinicio_Id'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_UgyUgyiratdarabok ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@UgyUgyirat_Id uniqueidentifier,@EljarasiSzakasz nvarchar(64),@Sorszam int,@Azonosito Nvarchar(100),@Hatarido datetime,@FelhasznaloCsoport_Id_Ugyintez uniqueidentifier,@ElintezesDat datetime,@LezarasDat datetime,@ElintezesMod nvarchar(64),@LezarasOka nvarchar(64),@Leiras Nvarchar(4000),@UgyUgyirat_Id_Elozo uniqueidentifier,@IraIktatokonyv_Id uniqueidentifier,@Foszam int,@UtolsoAlszam int,@Csoport_Id_Felelos uniqueidentifier,@Csoport_Id_Felelos_Elozo uniqueidentifier,@IratMetadefinicio_Id uniqueidentifier,@Allapot nvarchar(64),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@UgyUgyirat_Id = @UgyUgyirat_Id,@EljarasiSzakasz = @EljarasiSzakasz,@Sorszam = @Sorszam,@Azonosito = @Azonosito,@Hatarido = @Hatarido,@FelhasznaloCsoport_Id_Ugyintez = @FelhasznaloCsoport_Id_Ugyintez,@ElintezesDat = @ElintezesDat,@LezarasDat = @LezarasDat,@ElintezesMod = @ElintezesMod,@LezarasOka = @LezarasOka,@Leiras = @Leiras,@UgyUgyirat_Id_Elozo = @UgyUgyirat_Id_Elozo,@IraIktatokonyv_Id = @IraIktatokonyv_Id,@Foszam = @Foszam,@UtolsoAlszam = @UtolsoAlszam,@Csoport_Id_Felelos = @Csoport_Id_Felelos,@Csoport_Id_Felelos_Elozo = @Csoport_Id_Felelos_Elozo,@IratMetadefinicio_Id = @IratMetadefinicio_Id,@Allapot = @Allapot,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_UgyUgyiratdarabok',@ResultUid
					,'EREC_UgyUgyiratdarabokHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
