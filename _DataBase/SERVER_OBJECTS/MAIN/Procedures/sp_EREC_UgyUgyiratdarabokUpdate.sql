IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratdarabokUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratdarabokUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratdarabokUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratdarabokUpdate] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyUgyiratdarabokUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @UgyUgyirat_Id     uniqueidentifier  = null,         
             @EljarasiSzakasz     nvarchar(64)  = null,         
             @Sorszam     int  = null,         
             @Azonosito     Nvarchar(100)  = null,         
             @Hatarido     datetime  = null,         
             @FelhasznaloCsoport_Id_Ugyintez     uniqueidentifier  = null,         
             @ElintezesDat     datetime  = null,         
             @LezarasDat     datetime  = null,         
             @ElintezesMod     nvarchar(64)  = null,         
             @LezarasOka     nvarchar(64)  = null,         
             @Leiras     Nvarchar(4000)  = null,         
             @UgyUgyirat_Id_Elozo     uniqueidentifier  = null,         
             @IraIktatokonyv_Id     uniqueidentifier  = null,         
             @Foszam     int  = null,         
             @UtolsoAlszam     int  = null,         
             @Csoport_Id_Felelos     uniqueidentifier  = null,         
             @Csoport_Id_Felelos_Elozo     uniqueidentifier  = null,         
             @Allapot     nvarchar(64)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_UgyUgyirat_Id     uniqueidentifier         
     DECLARE @Act_EljarasiSzakasz     nvarchar(64)         
     DECLARE @Act_Sorszam     int         
     DECLARE @Act_Azonosito     Nvarchar(100)         
     DECLARE @Act_Hatarido     datetime         
     DECLARE @Act_FelhasznaloCsoport_Id_Ugyintez     uniqueidentifier         
     DECLARE @Act_ElintezesDat     datetime         
     DECLARE @Act_LezarasDat     datetime         
     DECLARE @Act_ElintezesMod     nvarchar(64)         
     DECLARE @Act_LezarasOka     nvarchar(64)         
     DECLARE @Act_Leiras     Nvarchar(4000)         
     DECLARE @Act_UgyUgyirat_Id_Elozo     uniqueidentifier         
     DECLARE @Act_IraIktatokonyv_Id     uniqueidentifier         
     DECLARE @Act_Foszam     int         
     DECLARE @Act_UtolsoAlszam     int         
     DECLARE @Act_Csoport_Id_Felelos     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Felelos_Elozo     uniqueidentifier         
     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_UgyUgyirat_Id = UgyUgyirat_Id,
     @Act_EljarasiSzakasz = EljarasiSzakasz,
     @Act_Sorszam = Sorszam,
     @Act_Azonosito = Azonosito,
     @Act_Hatarido = Hatarido,
     @Act_FelhasznaloCsoport_Id_Ugyintez = FelhasznaloCsoport_Id_Ugyintez,
     @Act_ElintezesDat = ElintezesDat,
     @Act_LezarasDat = LezarasDat,
     @Act_ElintezesMod = ElintezesMod,
     @Act_LezarasOka = LezarasOka,
     @Act_Leiras = Leiras,
     @Act_UgyUgyirat_Id_Elozo = UgyUgyirat_Id_Elozo,
     @Act_IraIktatokonyv_Id = IraIktatokonyv_Id,
     @Act_Foszam = Foszam,
     @Act_UtolsoAlszam = UtolsoAlszam,
     @Act_Csoport_Id_Felelos = Csoport_Id_Felelos,
     @Act_Csoport_Id_Felelos_Elozo = Csoport_Id_Felelos_Elozo,
     @Act_Allapot = Allapot,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_UgyUgyiratdarabok
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/UgyUgyirat_Id')=1
         SET @Act_UgyUgyirat_Id = @UgyUgyirat_Id
   IF @UpdatedColumns.exist('/root/EljarasiSzakasz')=1
         SET @Act_EljarasiSzakasz = @EljarasiSzakasz
   IF @UpdatedColumns.exist('/root/Sorszam')=1
         SET @Act_Sorszam = @Sorszam
   IF @UpdatedColumns.exist('/root/Azonosito')=1
         SET @Act_Azonosito = @Azonosito
   IF @UpdatedColumns.exist('/root/Hatarido')=1
         SET @Act_Hatarido = @Hatarido
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Ugyintez')=1
         SET @Act_FelhasznaloCsoport_Id_Ugyintez = @FelhasznaloCsoport_Id_Ugyintez
   IF @UpdatedColumns.exist('/root/ElintezesDat')=1
         SET @Act_ElintezesDat = @ElintezesDat
   IF @UpdatedColumns.exist('/root/LezarasDat')=1
         SET @Act_LezarasDat = @LezarasDat
   IF @UpdatedColumns.exist('/root/ElintezesMod')=1
         SET @Act_ElintezesMod = @ElintezesMod
   IF @UpdatedColumns.exist('/root/LezarasOka')=1
         SET @Act_LezarasOka = @LezarasOka
   IF @UpdatedColumns.exist('/root/Leiras')=1
         SET @Act_Leiras = @Leiras
   IF @UpdatedColumns.exist('/root/UgyUgyirat_Id_Elozo')=1
         SET @Act_UgyUgyirat_Id_Elozo = @UgyUgyirat_Id_Elozo
   IF @UpdatedColumns.exist('/root/IraIktatokonyv_Id')=1
         SET @Act_IraIktatokonyv_Id = @IraIktatokonyv_Id
   IF @UpdatedColumns.exist('/root/Foszam')=1
         SET @Act_Foszam = @Foszam
   IF @UpdatedColumns.exist('/root/UtolsoAlszam')=1
         SET @Act_UtolsoAlszam = @UtolsoAlszam
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1
         SET @Act_Csoport_Id_Felelos = @Csoport_Id_Felelos
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos_Elozo')=1
         SET @Act_Csoport_Id_Felelos_Elozo = @Csoport_Id_Felelos_Elozo
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_UgyUgyiratdarabok
SET
     UgyUgyirat_Id = @Act_UgyUgyirat_Id,
     EljarasiSzakasz = @Act_EljarasiSzakasz,
     Sorszam = @Act_Sorszam,
     Azonosito = @Act_Azonosito,
     Hatarido = @Act_Hatarido,
     FelhasznaloCsoport_Id_Ugyintez = @Act_FelhasznaloCsoport_Id_Ugyintez,
     ElintezesDat = @Act_ElintezesDat,
     LezarasDat = @Act_LezarasDat,
     ElintezesMod = @Act_ElintezesMod,
     LezarasOka = @Act_LezarasOka,
     Leiras = @Act_Leiras,
     UgyUgyirat_Id_Elozo = @Act_UgyUgyirat_Id_Elozo,
     IraIktatokonyv_Id = @Act_IraIktatokonyv_Id,
     Foszam = @Act_Foszam,
     UtolsoAlszam = @Act_UtolsoAlszam,
     Csoport_Id_Felelos = @Act_Csoport_Id_Felelos,
     Csoport_Id_Felelos_Elozo = @Act_Csoport_Id_Felelos_Elozo,
     Allapot = @Act_Allapot,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_UgyUgyiratdarabok',@Id
					,'EREC_UgyUgyiratdarabokHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
