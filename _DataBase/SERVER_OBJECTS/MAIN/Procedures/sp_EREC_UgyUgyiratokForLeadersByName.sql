IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokForLeadersByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratokForLeadersByName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokForLeadersByName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratokForLeadersByName] AS' 
END
GO
-- HF

ALTER PROCEDURE [dbo].[sp_EREC_UgyUgyiratokForLeadersByName]
    @ExecutorUserName nvarchar(100),
	@KezdDat datetime,
	@VegeDat datetime
AS 

    BEGIN
        BEGIN TRY

            declare @ExecutorUserId uniqueidentifier

            SET nocount ON

			set @ExecutorUserId =
             (select Id from KRT_Felhasznalok where Nev= @ExecutorUserName)
            IF @ExecutorUserId IS NOT NULL 
			BEGIN
			   exec [dbo].[sp_EREC_UgyUgyiratokForLeaders] 
                  @ExecutorUserId = @ExecutorUserId, 
                  @KezdDat = @KezdDat,
	              @VegeDat = @VegeDat
			END

        END TRY
        BEGIN CATCH
            DECLARE @errorSeverity INT,
                @errorState INT
            DECLARE @errorCode NVARCHAR(1000)    
            SET @errorSeverity = ERROR_SEVERITY()
            SET @errorState = ERROR_STATE()
	
            IF ERROR_NUMBER() < 50000 
                SET @errorCode = '[' + CONVERT(NVARCHAR(10), ERROR_NUMBER())
                    + '] ' + ERROR_MESSAGE()
            ELSE 
                SET @errorCode = ERROR_MESSAGE()
      
            IF @errorState = 0 
                SET @errorState = 1

            RAISERROR ( @errorCode, @errorSeverity, @errorState )
 
        END CATCH

    END


GO
