IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokForXml_SEE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratokForXml_SEE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokForXml_SEE]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratokForXml_SEE] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyUgyiratokForXml_SEE]
  @Where			nvarchar(MAX) = '((((EREC_UgyUgyiratok.ErvKezd <= getdate()) and (EREC_UgyUgyiratok.ErvVege >= getdate()))) and (EREC_UgyUgyiratok.Allapot in (''0'',''03'',''04'',''06'',''09'',''11'',''13'',''52'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''09'',''11'',''13'',''52'',''99''))) and EREC_UgyUgyiratok.LetrehozasIdo BETWEEN ''2000-02-02 00:00:00'' and ''2010-11-24 23:59:59''  and EREC_UgyUgyiratok.IratSzam > 0 )',
  @OrderBy			nvarchar(400) = ' order by EREC_IraIktatokonyvek.Ev,EREC_IraIktatokonyvek.Iktatohely,EREC_UgyUgyiratok.Foszam',
  @ExecutorUserId	uniqueidentifier ='2F50DAF0-1600-47D3-A49C-1626B1FB22A5',
  @FelhasznaloSzervezet_Id	uniqueidentifier='ECB902E5-FD38-4D06-9E0E-A0B2733FB2E9',
  @TopRow NVARCHAR(5) = '100'

as
begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  '
--select (
  select ' + @LocalTopRow + ' EREC_UgyUgyiratok.Azonosito as Foszam_Merge,
			   CONVERT(nvarchar(10), EREC_UgyUgyiratok.LetrehozasIdo, 102) as LetrehozasIdo_Rovid,
			   EREC_UgyUgyiratok.NevSTR_Ugyindito,
			   EREC_UgyUgyiratok.CimSTR_Ugyindito,
			   EREC_UgyUgyiratok.Targy,
			   dbo.fn_GetCsoportNev(EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez) as KRT_Csoportok_Orzo_Nev,
			   (select UgytipusNev from EREC_IratMetaDefinicio where EREC_UgyUgyiratok.IratMetadefinicio_Id = EREC_IratMetaDefinicio.Id) as UgyTipus_Nev,
			   Ugyirat_helye = 
			   CASE 
					WHEN EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''09'',''11'',''13'',''52'',''55'',''56'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''09'',''11'',''13'',''52'',''55'',''56'',''99''))
					THEN ''Ügyintézonél''
					ELSE 
						CASE WHEN EREC_Ugyugyiratok.Allapot = ''07'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''07'')
							THEN ''Skontróban''
						END
			   END
		from EREC_UgyUgyiratok
			inner join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek 
				ON EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id and EREC_IraIktatokonyvek.Org=''' + cast(@Org as NVarChar(40)) + '''
where EREC_UgyUgyiratok.Id IN 
(
										SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
											INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
											INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
										UNION ALL
										SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
											INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
											INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
										UNION ALL
										SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
											INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll 
												ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
										UNION ALL
										SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
											INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll 
												ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
										UNION ALL
										SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
											INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll 
												ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
										UNION ALL
										SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''', ''EREC_UgyUgyiratok'')
									) 
'
	
if dbo.fn_IsLeaderInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
		begin
			if dbo.fn_HasFunctionRight(@ExecutorUserId,@FelhasznaloSzervezet_Id, 'IrattarKolcsonzesKiadasa') = 0
				and dbo.fn_HasFunctionRight(@ExecutorUserId,@FelhasznaloSzervezet_Id, 'AtmenetiIrattarAtadasKozpontiIrattarba') = 0
			begin
				-- Irattár szurés, In és nem NOT IN!
				-- Irattározott állapotok: Irattárba küldött, Irattárban orzött, Irattárból elkért, Átmeneti irattárból kikölcsönzött, Engedélyezett kikéron lévo
				SET @sqlcmd = @sqlcmd + '
							and EREC_UgyUgyiratok.Id NOT IN 
							(
								select Id from EREC_UgyUgyiratok where
								(Allapot in (''11'', ''10'', ''55'', ''54'', ''56'') or TovabbitasAlattAllapot in (''11'', ''10'', ''55'', ''54'', ''56''))
								and Csoport_Id_Felelos in (select Id from KRT_Csoportok where IsNull(JogosultsagOroklesMod, ''0'') = ''0'')
								and EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez != ''' + CAST(@ExecutorUserId AS CHAR(36)) + '''
								and EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != ''' + CAST(@ExecutorUserId AS CHAR(36)) + '''
								and EREC_UgyUgyiratok.Id not in
								(
									SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
										INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
										INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
									UNION ALL
									SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
										INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
										INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
									UNION ALL
									SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''', ''EREC_UgyUgyiratok'')
								)
							)
 '
			end
		end
		
			if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end

    
   SET @sqlcmd = @sqlcmd + @OrderBy + '
-- FOR XML AUTO, ELEMENTS, ROOT(''NewDataSet'')) as string_xml '
 
   exec(@sqlcmd)


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
