IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetAllElokeszitettSzereltByNode]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetAllElokeszitettSzereltByNode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetAllElokeszitettSzereltByNode]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetAllElokeszitettSzereltByNode] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyUgyiratokGetAllElokeszitettSzereltByNode]
		--@NodeId	uniqueidentifier,
		@NodeIdList	NVARCHAR(MAX), -- Id lista
		@Delimeter NVARCHAR(1) = ',',
		@OnlyFirstInHierarchy  char(1) = 0 -- ha értéke 1, megállunk az elso elokészített szerelésnél a láncban, annak gyermekeit már nem kérjük le


as

begin

BEGIN TRY

    set nocount on;
    
    --  Csak az elokészített szerelteket hozzuk, bárhol legyenek a láncban - az állapot nem Szerelt!
    -- (Azokat kell hozni, ahol UgyUgyirat_Id_Szulo ki van töltve, és sem az Allapot, sem a TovabbitasAlattAllapot nem szerelt(60) )

	if @OnlyFirstInHierarchy = 1
	BEGIN
		-- elso nem szerelt szülo megkeresése
		WITH ParentHierarchy  AS
		(
		   -- Base case
		   SELECT Id, UgyUgyirat_Id_Szulo, Allapot, TovabbitasAlattAllapot
--				  , -1 as HierarchyLevel
		   FROM EREC_UgyUgyiratok
		   WHERE Id in (SELECT Value FROM dbo.fn_Split(@NodeIdList, @Delimeter) WHERE Value != '') --= @NodeId

		   UNION ALL

		   -- Recursive step
		   SELECT u.Id, u.UgyUgyirat_Id_Szulo, u.Allapot, u.TovabbitasAlattAllapot
--				, ph.HierarchyLevel - 1 AS HierarchyLevel
		   FROM EREC_UgyUgyiratok as u
			  INNER JOIN ParentHierarchy as ph ON
				 u.Id = ph.UgyUgyirat_Id_Szulo
		   WHERE (ph.Allapot = '60' OR ph.TovabbitasAlattAllapot = '60')
		),
		-- összes saját gyerek megkeresése az elso szintig
		UgyiratHierarchy  AS
		(
		   -- Base case	   
		   SELECT Id, Allapot, TovabbitasAlattAllapot
--				  , 1 as HierarchyLevel
		   FROM EREC_UgyUgyiratok
		   WHERE UgyUgyirat_Id_Szulo in (SELECT Value FROM dbo.fn_Split(@NodeIdList, @Delimeter) WHERE Value != '') --= @NodeId

		   UNION ALL

		   -- Recursive step
		   SELECT u.Id, u.Allapot, u.TovabbitasAlattAllapot
--			  , uh.HierarchyLevel + 1 AS HierarchyLevel
		   FROM EREC_UgyUgyiratok as u
			  INNER JOIN UgyiratHierarchy as uh ON
				 u.UgyUgyirat_Id_Szulo = uh.Id
		   WHERE (uh.Allapot = '60' OR uh.TovabbitasAlattAllapot = '60')
		)

		SELECT u.*
		FROM (select distinct Id from UgyiratHierarchy
				UNION ALL
				select distinct Id from ParentHierarchy) uh
		INNER JOIN EREC_UgyUgyiratok u on uh.Id=u.Id
		where u.Allapot <> '60' and IsNull(u.TovabbitasAlattAllapot, '') <> '60'
		and u.UgyUgyirat_Id_Szulo is not null

	END
	ELSE
	BEGIN
		-- legfobb szülo megkeresése
		WITH ParentHierarchy  AS
		(
		   -- Base case
		   SELECT Id, UgyUgyirat_Id_Szulo
--				  , -1 as HierarchyLevel
		   FROM EREC_UgyUgyiratok
		   WHERE Id in (SELECT Value FROM dbo.fn_Split(@NodeIdList, @Delimeter) WHERE Value != '') -- = @NodeId

		   UNION ALL

		   -- Recursive step
		   SELECT u.Id, u.UgyUgyirat_Id_Szulo
--				, ph.HierarchyLevel - 1 AS HierarchyLevel
		   FROM EREC_UgyUgyiratok as u
			  INNER JOIN ParentHierarchy as ph ON
				 u.Id = ph.UgyUgyirat_Id_Szulo
		   --WHERE (ph.Allapot = '60' OR ph.TovabbitasAlattAllapot = '60')
		),
	    
		-- szülo összes gyerekének megkeresése
		UgyiratHierarchy  AS
		(
		   -- Base case
		   SELECT Id, UgyUgyirat_Id_Szulo
--				  , 1 as HierarchyLevel
		   FROM ParentHierarchy
		   WHERE UgyUgyirat_Id_Szulo is NULL

		   UNION ALL

		   -- Recursive step
		   SELECT u.Id, u.UgyUgyirat_Id_Szulo
--			  , uh.HierarchyLevel + 1 AS HierarchyLevel
		   FROM EREC_UgyUgyiratok as u
			  INNER JOIN UgyiratHierarchy as uh ON
				 u.UgyUgyirat_Id_Szulo = uh.Id
		   --WHERE (u.Allapot = '60' OR u.TovabbitasAlattAllapot = '60')
		)

		SELECT u.*
		FROM UgyiratHierarchy uh
		INNER JOIN EREC_UgyUgyiratok u on uh.Id=u.Id
		where u.Allapot <> '60' and IsNull(u.TovabbitasAlattAllapot, '') <> '60'
		and u.UgyUgyirat_Id_Szulo is not null
	END



--	if @OnlyFirstInHierarchy = 1
--	BEGIN
--		-- szülok megkeresése
--		WITH ParentHierarchy  AS
--		(
--		   -- Base case
--		   SELECT Id, UgyUgyirat_Id_Szulo, Allapot, TovabbitasAlattAllapot
----				  , -1 as HierarchyLevel
--		   FROM EREC_UgyUgyiratok
--		   WHERE Id = (select UgyUgyirat_Id_Szulo from EREC_UgyUgyiratok where Id = @NodeId)
--
--		   UNION ALL
--
--		   -- Recursive step
--		   SELECT u.Id, u.UgyUgyirat_Id_Szulo, u.Allapot, u.TovabbitasAlattAllapot
----				, ph.HierarchyLevel - 1 AS HierarchyLevel
--		   FROM EREC_UgyUgyiratok as u
--			  INNER JOIN ParentHierarchy as ph ON
--				 u.Id = ph.UgyUgyirat_Id_Szulo
--		   WHERE (ph.Allapot = '60' OR IsNull(ph.TovabbitasAlattAllapot, '') = '60')
--		),
--	    
--		-- összes gyerek megkeresése
--		UgyiratHierarchy  AS
--		(
--		   -- Base case	   
--		   SELECT Id, Allapot, TovabbitasAlattAllapot
----				  , 1 as HierarchyLevel
--		   FROM EREC_UgyUgyiratok
--		   WHERE UgyUgyirat_Id_Szulo = @NodeId
--
--
--		   UNION ALL
--
--		   -- Recursive step
--		   SELECT u.Id, u.Allapot, u.TovabbitasAlattAllapot
----			  , uh.HierarchyLevel + 1 AS HierarchyLevel
--		   FROM EREC_UgyUgyiratok as u
--			  INNER JOIN UgyiratHierarchy as uh ON
--				 u.UgyUgyirat_Id_Szulo = uh.Id
--		   WHERE (uh.Allapot = '60' OR uh.TovabbitasAlattAllapot = '60')
--		)
--
--		SELECT u.*
--		FROM (select distinct Id from UgyiratHierarchy
--				UNION ALL
--				select distinct Id from ParentHierarchy) uh
--		INNER JOIN EREC_UgyUgyiratok u on uh.Id=u.Id
--		where u.Allapot <> '60' and IsNull(u.TovabbitasAlattAllapot, '') <> '60'
--
--	END
--	ELSE
--	BEGIN
--		-- szülok megkeresése
--		WITH ParentHierarchy  AS
--		(
--		   -- Base case
--		   SELECT Id, UgyUgyirat_Id_Szulo
----				  , -1 as HierarchyLevel
--		   FROM EREC_UgyUgyiratok
--		   WHERE Id = (select UgyUgyirat_Id_Szulo from EREC_UgyUgyiratok where Id = @NodeId)
--
--		   UNION ALL
--
--		   -- Recursive step
--		   SELECT u.Id, u.UgyUgyirat_Id_Szulo
----				, ph.HierarchyLevel - 1 AS HierarchyLevel
--		   FROM EREC_UgyUgyiratok as u
--			  INNER JOIN ParentHierarchy as ph ON
--				 u.Id = ph.UgyUgyirat_Id_Szulo
--		   --WHERE (ph.Allapot = '60' OR ph.TovabbitasAlattAllapot = '60')
--		),
--	    
--		-- összes gyerek megkeresése
--		UgyiratHierarchy  AS
--		(
--		   -- Base case
--		   SELECT Id
----				  , 1 as HierarchyLevel
--		   FROM EREC_UgyUgyiratok
--		   WHERE UgyUgyirat_Id_Szulo = @NodeId
--		   --AND (u.Allapot = '60' OR u.TovabbitasAlattAllapot = '60')
--
--		   UNION ALL
--
--		   -- Recursive step
--		   SELECT u.Id
----			  , uh.HierarchyLevel + 1 AS HierarchyLevel
--		   FROM EREC_UgyUgyiratok as u
--			  INNER JOIN UgyiratHierarchy as uh ON
--				 u.UgyUgyirat_Id_Szulo = uh.Id
--		   --WHERE (u.Allapot = '60' OR u.TovabbitasAlattAllapot = '60')			 
--		)
--
--		SELECT u.*
--		FROM (select distinct Id from UgyiratHierarchy
--				UNION ALL
--				select distinct Id from ParentHierarchy) uh
--		INNER JOIN EREC_UgyUgyiratok u on uh.Id=u.Id
--		where u.Allapot <> '60' and IsNull(u.TovabbitasAlattAllapot, '') <> '60'
--	END

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
