IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetAllElozmeny]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetAllElozmeny]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetAllElozmeny]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetAllElozmeny] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyUgyiratokGetAllElozmeny]
  @UgyiratIds     NVARCHAR(MAX), -- Ügyirat Id lista inner operátornak megfelelo formátumban
  @OrderBy nvarchar(200) = '',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY
	set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
       EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt as Id,
       EREC_UgyiratObjKapcsolatok.Obj_Id_Elozmeny as ElozmenyId,
	   EREC_UgyiratObjKapcsolatok.Leiras as ElozmenyAzonosito,
	   EREC_UgyiratObjKapcsolatok.Obj_Type_Elozmeny as ElozmenyType
   from 
     EREC_UgyiratObjKapcsolatok as EREC_UgyiratObjKapcsolatok  
     Where EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt in ' + @UgyiratIds + '
     AND EREC_UgyiratObjKapcsolatok.KapcsolatTipus = ''07''
	AND getdate() between EREC_UgyiratObjKapcsolatok.ErvKezd and EREC_UgyiratObjKapcsolatok.ErvVege
   UNION ALL
   select ' + @LocalTopRow + '
       EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo as Id,
       EREC_UgyUgyiratok.Id as ElozmenyId,
	   EREC_UgyUgyiratok.Azonosito as ElozmenyAzonosito,
	   ''EREC_UgyUgyiratok'' as ElozmenyType
   from 
     EREC_UgyUgyiratok as EREC_UgyUgyiratok
     JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek on EREC_IraIktatoKonyvek.Id = EREC_UgyUgyiratok.IraIktatokonyv_Id
     Where EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo in ' + @UgyiratIds + '
     '
      
--    if @Where is not null and @Where!=''
--	begin 
--		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
--	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
