IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetAllElsodlegesIratpeldany]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetAllElsodlegesIratpeldany]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetAllElsodlegesIratpeldany]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetAllElsodlegesIratpeldany] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyUgyiratokGetAllElsodlegesIratpeldany]
	@UgyiratId			UNIQUEIDENTIFIER
as
BEGIN TRY
	SELECT *
		FROM EREC_PldIratPeldanyok
			INNER JOIN EREC_IraIratok ON EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id
			INNER JOIN EREC_UgyUgyiratdarabok ON EREC_IraIratok.UgyUgyIratDarab_Id = EREC_UgyUgyiratdarabok.Id
			INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
		WHERE EREC_UgyUgyiratok.Id = @UgyiratId
			AND EREC_PldIratPeldanyok.Sorszam = 1;
END TRY
BEGIN CATCH

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
