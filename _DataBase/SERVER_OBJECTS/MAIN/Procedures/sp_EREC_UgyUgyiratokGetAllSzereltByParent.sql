IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetAllSzereltByParent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetAllSzereltByParent]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetAllSzereltByParent]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetAllSzereltByParent] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyUgyiratokGetAllSzereltByParent]
		@ParentIdList	NVARCHAR(MAX), -- Id lista
		@Delimeter NVARCHAR(1) = ','

as

begin

BEGIN TRY

    set nocount on;
    
    -- Elokészített szerelésre figyelni kell, azokat nem hozzuk
    -- (Azokat kell hozni, ahol UgyUgyirat_Id_Szulo ki van töltve, és vagy az Allapot, vagy a TovabbitasAlattAllapot szerelt(60) )
   
	WITH UgyiratHierarchy  AS
	(
	   -- Base case	   
	   SELECT *,
			  1 as HierarchyLevel
	   FROM EREC_UgyUgyiratok
	   WHERE UgyUgyirat_Id_Szulo IN 
	   (SELECT Value FROM dbo.fn_Split(@ParentIdList, @Delimeter) WHERE Value != '')
			AND (Allapot = '60' OR TovabbitasAlattAllapot = '60')

	   UNION ALL

	   -- Recursive step
	   SELECT u.*,
		  uh.HierarchyLevel + 1 AS HierarchyLevel
	   FROM EREC_UgyUgyiratok as u
		  INNER JOIN UgyiratHierarchy as uh ON
			 u.UgyUgyirat_Id_Szulo = uh.Id
	   WHERE (u.Allapot = '60' OR u.TovabbitasAlattAllapot = '60')
	)

	SELECT *
	FROM UgyiratHierarchy
  

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
