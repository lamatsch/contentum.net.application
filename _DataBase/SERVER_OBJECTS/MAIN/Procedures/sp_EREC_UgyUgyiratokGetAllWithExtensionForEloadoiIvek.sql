IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForEloadoiIvek]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForEloadoiIvek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForEloadoiIvek]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForEloadoiIvek] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForEloadoiIvek]
  @Where			nvarchar(MAX) = '',
  @OrderBy			nvarchar(200) = '',
  @TopRow			nvarchar(5) = '',
  @ExecutorUserId	uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = '
SELECT ' + @LocalTopRow + '
EREC_UgyUgyiratok.Id,
EREC_UgyUgyiratok.Azonosito as Foszam_Merge,
EREC_UgyUgyiratok.LetrehozasIdo,
dbo.fn_GetCsoportNev(EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez) as Ugyintezo_Nev,
(SELECT TOP 1 EREC_UgyiratObjKapcsolatok.Leiras FROM EREC_UgyiratObjKapcsolatok WHERE EREC_UgyiratObjKapcsolatok.KapcsolatTipus = ''07'' AND EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt  = EREC_UgyUgyiratok.Id AND getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege) as MIG_EloIrat,
(SELECT TOP 1 EREC_UgyUgyiratok_Szereltek.Azonosito FROM EREC_UgyUgyiratok as EREC_UgyUgyiratok_Szereltek WHERE EREC_UgyUgyiratok_Szereltek.UgyUgyirat_Id_Szulo = EREC_UgyUgyiratok.Id) as EREC_EloIrat,
'''' as kap_Foszam_Merge,
EREC_IraIktatokonyvek.Nev as szervezeti_egyseg,
EREC_UgyUgyiratok.NevSTR_Ugyindito,
EREC_UgyUgyiratok.CimSTR_Ugyindito,
EREC_UgyUgyiratok.Targy,
CONVERT(nvarchar(10), EREC_IraIratok.IktatasDatuma, 102) as IktatasDatuma,
EREC_IraIrattariTetelek.MegorzesiIdo,
EREC_UgyUgyiratok.BARCODE,
EREC_IraIrattariTetelek.IrattariTetelszam,
'''' as Itsz1,
'''' as Itsz2,
'''' as Itsz3,
'''' as Itsz4,
'''' as selejtkod,
EREC_Agazatijelek.Kod as AgazatiJel,
EREC_IraIrattariTetelek.IrattariJel,
EREC_IratMetaDefinicio.UgyFajta as UgyFajtaja
FROM
EREC_UgyUgyiratok as EREC_UgyUgyiratok
left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek
	ON EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id			
left join EREC_IraIrattariTetelek as EREC_IraIrattariTetelek
	ON EREC_UgyUgyiratok.IraIrattariTetel_Id = EREC_IraIrattariTetelek.Id
left join EREC_AgazatiJelek as EREC_Agazatijelek
	ON EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_Agazatijelek.Id	  
left join EREC_UgyUgyiratok as EREC_UgyUgyiratok_Szulo 
	ON EREC_UgyUgyiratok_Szulo.Id = EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo
left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek_Szulo 
	ON EREC_UgyUgyiratok_Szulo.IraIktatokonyv_Id = EREC_IraIktatokonyvek_Szulo.Id
left join EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok
	ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
left join EREC_IraIratok as EREC_IraIratok
	ON EREC_IraIratok.UgyUgyiratDarab_Id = EREC_UgyUgyiratdarabok.Id
left join EREC_PldIratPeldanyok as EREC_PldIratPeldanyok
	ON EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id
left join EREC_IratMetaDefinicio as EREC_IratMetaDefinicio
on EREC_IratMetaDefinicio.Id = EREC_UgyUgyiratok.IratMetadefinicio_Id
WHERE EREC_IraIratok.Alszam = ''1'' and (EREC_PldIratPeldanyok.Sorszam = ''1'' or EREC_PldIratPeldanyok.Sorszam is NULL)
and EREC_IraIktatokonyvek.Org = ''' + cast(@Org as NVarChar(40)) + '''
and EREC_IraIrattariTetelek.Org = ''' + cast(@Org as NVarChar(40)) + '''
'

if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' AND ' + @Where
	end

   SET @sqlcmd = @sqlcmd + @OrderBy

  --print @sqlcmd

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
