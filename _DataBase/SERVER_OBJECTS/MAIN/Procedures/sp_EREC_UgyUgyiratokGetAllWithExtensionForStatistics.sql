IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForStatistics]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForStatistics]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForStatistics]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForStatistics] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForStatistics]
  @Where			nvarchar(4000) = '',
  @OrderBy			nvarchar(200) = ' order by EREC_UgyUgyiratok.UgyTipus',
  @TopRow			nvarchar(5) = '',
  @ExecutorUserId	uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = '
SELECT DISTINCT ' + @LocalTopRow + '
EREC_UgyUgyiratok.UgyTipus,
--KRT_KodTarak_UgyTipus.Nev,
EREC_IratMetaDefinicio.UgytipusNev as Nev,
Avg(DATEDIFF(day, EREC_UgyUgyiratok.LetrehozasIdo, EREC_UgyUgyiratok.ElintezesDat)) as elint1,
AVG((DATEDIFF(day, EREC_UgyUgyiratok.LetrehozasIdo, EREC_UgyUgyiratok.ElintezesDat)-ISNULL (EREC_UgyUgyiratok.SkontrobanOsszesen,0))) as elint2
FROM
EREC_UgyUgyiratok as EREC_UgyUgyiratok
inner join EREC_IraIktatoKonyvek on EREC_UgyUgyiratok.IraIktatoKonyv_Id=EREC_IraIktatoKonyvek.Id and EREC_IraIktatoKonyvek.Org=''' + cast(@Org as NVarChar(40)) + '''
left join EREC_IratMetaDefinicio on EREC_IratMetaDefinicio.Id=EREC_UgyUgyiratok.IratMetaDefinicio_Id and EREC_IratMEtaDefinicio.Org=''' + cast(@Org as NVarChar(40)) + '''
--left join KRT_KodCsoportok as KRT_KodCsoportok_UgyTipus on KRT_KodCsoportok_UgyTipus.Kod=''UGYTIPUS''
--left join KRT_KodTarak as KRT_KodTarak_UgyTipus on KRT_KodCsoportok_UgyTipus.Id = KRT_KodTarak_UgyTipus.KodCsoport_Id and EREC_UgyUgyiratok.UgyTipus=KRT_KodTarak_UgyTipus.Kod
--and KRT_KodTarak_UgyTipus.Org= ''' + cast(@Org as NVarChar(40)) + '''
'

if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
   
   SET @sqlcmd = @sqlcmd + ' GROUP BY
EREC_UgyUgyiratok.UgyTipus,
--KRT_KodTarak_UgyTipus.Nev,
EREC_IratMetaDefinicio.UgytipusNev '

   SET @sqlcmd = @sqlcmd + @OrderBy

  --print @sqlcmd

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
