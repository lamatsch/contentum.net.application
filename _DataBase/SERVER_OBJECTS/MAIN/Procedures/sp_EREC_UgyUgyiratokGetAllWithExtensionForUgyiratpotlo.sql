IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForUgyiratpotlo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForUgyiratpotlo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForUgyiratpotlo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForUgyiratpotlo] AS' 
END
GO


ALTER procedure [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForUgyiratpotlo]
  @Where			nvarchar(MAX) = '',
  @OrderBy			nvarchar(200) = '',
  @TopRow			nvarchar(5) = '',
  @ExecutorUserId	uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = '
SELECT ' + @LocalTopRow + '
	   EREC_UgyUgyiratok.Azonosito as Foszam_Merge,
	   EREC_UgyUgyiratok.Targy,
	    ISNULL(KRT_Csoportok.Nev,'''') +
		CASE WHEN EREC_UgyUgyiratok.IrattariHely IS NOT NULL
			THEN '' / ''+EREC_UgyUgyiratok.IrattariHely
			ELSE ''''
		END
	   AS Irattar_neve,
	   ISNULL(KRT_Csoportok.Nev,'''') +'' ''+ ISNULL(EREC_UgyUgyiratok.IrattariHely,'''') as Irattar_neve,
	   CONVERT(nvarchar(10), EREC_IrattariKikero.KikerKezd, 102) as KikerKezd,
	   CONVERT(nvarchar(10), EREC_IrattariKikero.KiadasDatuma, 102) as KiadasDatuma,
	   Case EREC_IrattariKikero.FelhasznalasiCel 
			When ''B'' Then ''Betekintésre''
			When ''U'' Then ''Ügyintézésre''
	   End
	   as FelhasznalasiCelNev,
	   KRT_FelhasznalokFelhasznaloCsoport_Id_Kikero.Nev as Kikero_IdNev,
	   KRT_FelhasznalokKapta_Id.Nev as Kapta_IdNev,
	   KRT_FelhasznalokFelhasznaloCsoport_Id_Kiado.Nev as Kiado_IdNev,
	   CONVERT(nvarchar(10), EREC_IrattariKikero.KikerVege, 102) as KikerVege,
	   EREC_IraIrattariTetelek.IrattariTetelszam as IrattariTetelszam,
	   EREC_UgyUgyiratok.BARCODE as BARCODE
from EREC_UgyUgyiratok as EREC_UgyUgyiratok
			left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek
				ON EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id			
			left join EREC_IrattariKikero as EREC_IrattariKikero
				ON EREC_IrattariKikero.UgyUgyirat_Id = EREC_UgyUgyiratok.Id and EREC_IrattariKikero.Allapot = ''04''
			left join KRT_Csoportok as KRT_Csoportok 
				on KRT_Csoportok.Id = EREC_IrattariKikero.Irattar_Id
			left join KRT_Felhasznalok as KRT_FelhasznalokKapta_Id 
				on KRT_FelhasznalokKapta_Id.Id = EREC_IrattariKikero.FelhasznaloCsoport_Id_Kiado	
			left join KRT_Felhasznalok as KRT_FelhasznalokFelhasznaloCsoport_Id_Kikero 
				on KRT_FelhasznalokFelhasznaloCsoport_Id_Kikero.Id = EREC_IrattariKikero.FelhasznaloCsoport_Id_Kikero
			left join KRT_Felhasznalok as KRT_FelhasznalokFelhasznaloCsoport_Id_Kiado 
				on KRT_FelhasznalokFelhasznaloCsoport_Id_Kiado.Id = EREC_IrattariKikero.FelhasznaloCsoport_Id_Kiado
			left join EREC_IraIrattariTetelek as EREC_IraIrattariTetelek
				on EREC_UgyUgyiratok.IraIrattariTetel_Id = EREC_IraIrattariTetelek.Id
where EREC_IraIktatokonyvek.Org=''' + cast(@Org as NVarChar(40)) + '''
'

if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end

   SET @sqlcmd = @sqlcmd + @OrderBy

  print @sqlcmd

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

GO
