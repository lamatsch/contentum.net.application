IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetUgyiratObjektumai]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetUgyiratObjektumai]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetUgyiratObjektumai]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetUgyiratObjektumai] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyUgyiratokGetUgyiratObjektumai]
	     @Obj_Id uniqueidentifier,
	     @Obj_type NVARCHAR(100),
         @ExecutorUserId UNIQUEIDENTIFIER,
         @WithParent CHAR(1) = '0'

         
as

begin
BEGIN TRY

	set nocount ON
	
	DECLARE @UgyiratId UNIQUEIDENTIFIER
    SET @UgyiratId = NULL
    
    DECLARE @IratId UNIQUEIDENTIFIER
    SET @IratId = NULL
    
    IF @Obj_type = 'EREC_KuldKuldemenyek'
    BEGIN
        IF @WithParent = '0'
		BEGIN
			SELECT @Obj_Id AS Id, 'EREC_KuldKuldemenyek' AS Obj_type
			union
			SELECT [EREC_IraIratok].Id AS Id, 'EREC_IraIratok' AS Obj_type FROM [EREC_IraIratok]
			WHERE [EREC_IraIratok].[KuldKuldemenyek_Id] =  @Obj_Id
			union
			SELECT [EREC_PldIratPeldanyok].Id AS Id, 'EREC_PldIratPeldanyok' AS Obj_type FROM [EREC_IraIratok]
			JOIN [EREC_PldIratPeldanyok] ON [EREC_IraIratok].[Id] = [EREC_PldIratPeldanyok].[IraIrat_Id]
			WHERE [EREC_IraIratok].[KuldKuldemenyek_Id] =  @Obj_Id
		END
		ELSE
		BEGIN
		    SET @IratId = (SELECT TOP 1 [EREC_IraIratok].[Id] FROM [EREC_IraIratok]
		    WHERE [EREC_IraIratok].[KuldKuldemenyek_Id] = @Obj_Id)
			
			IF @IratId IS NULL
			BEGIN
				SELECT @Obj_Id AS Id, 'EREC_KuldKuldemenyek' AS Obj_type
			END
			ELSE
			BEGIN
				SET @UgyiratId = (SELECT TOP 1 [EREC_UgyUgyiratdarabok].[UgyUgyirat_Id] FROM [EREC_UgyUgyiratdarabok]
				JOIN [EREC_IraIratok] ON [EREC_UgyUgyiratdarabok].[Id] = [EREC_IraIratok].[UgyUgyIratDarab_Id]
				WHERE [EREC_IraIratok].[Id] =  @IratId)
			END
		END 
	END

	IF @Obj_type = 'EREC_IraIratok'
    BEGIN
		IF @WithParent = '0'
		BEGIN
			SELECT @Obj_Id AS Id, 'EREC_IraIratok' AS Obj_type
			UNION
			SELECT [EREC_KuldKuldemenyek].Id AS Id, 'EREC_KuldKuldemenyek' AS Obj_type FROM [EREC_IraIratok]
			JOIN [EREC_KuldKuldemenyek] ON [EREC_KuldKuldemenyek].[Id] = [EREC_IraIratok].[KuldKuldemenyek_Id]
			WHERE [EREC_IraIratok].[Id] =  @Obj_Id
			UNION
			SELECT [EREC_PldIratPeldanyok].Id AS Id, 'EREC_PldIratPeldanyok' AS Obj_type FROM [EREC_IraIratok]
			JOIN [EREC_PldIratPeldanyok] ON [EREC_IraIratok].[Id] = [EREC_PldIratPeldanyok].[IraIrat_Id]
			WHERE [EREC_IraIratok].[Id] =  @Obj_Id
		END
		ELSE
		BEGIN
			SET @UgyiratId = (SELECT TOP 1 [EREC_UgyUgyiratdarabok].[UgyUgyirat_Id] FROM [EREC_UgyUgyiratdarabok]
			JOIN [EREC_IraIratok] ON [EREC_UgyUgyiratdarabok].[Id] = [EREC_IraIratok].[UgyUgyIratDarab_Id]
			WHERE [EREC_IraIratok].[Id] =  @Obj_Id)
		END 
    END

    IF @Obj_type = 'EREC_PldIratPeldanyok'
    BEGIN
        IF @WithParent = '0'
		BEGIN
			SELECT @Obj_Id AS Id, 'EREC_PldIratPeldanyok' AS Obj_type
		END
		ELSE
		BEGIN
			SET @UgyiratId = (SELECT TOP 1 [EREC_UgyUgyiratdarabok].[UgyUgyirat_Id] FROM [EREC_UgyUgyiratdarabok]
			JOIN [EREC_IraIratok] ON [EREC_UgyUgyiratdarabok].[Id] = [EREC_IraIratok].[UgyUgyIratDarab_Id]
			JOIN [EREC_PldIratPeldanyok] ON [EREC_IraIratok].[Id] = [EREC_PldIratPeldanyok].[IraIrat_Id]
			WHERE [EREC_PldIratPeldanyok].[Id] =  @Obj_Id)
		END
    END
    

    IF @Obj_type = 'EREC_UgyUgyiratok'
    BEGIN
		SET @UgyiratId = @Obj_Id
    END
    
    IF @UgyiratId IS NOT NULL
    BEGIN
		SELECT @UgyiratId AS Id, 'EREC_UgyUgyiratok' AS Obj_type
		UNION
		SELECT [EREC_IraIratok].Id AS Id, 'EREC_IraIratok' AS Obj_type FROM [EREC_UgyUgyiratdarabok]
		JOIN [EREC_IraIratok] ON [EREC_UgyUgyiratdarabok].[Id] = [EREC_IraIratok].[UgyUgyIratDarab_Id]
		WHERE [EREC_UgyUgyiratdarabok].[UgyUgyirat_Id] =  @UgyiratId
		UNION
		SELECT [EREC_KuldKuldemenyek].Id AS Id, 'EREC_KuldKuldemenyek' AS Obj_type FROM [EREC_UgyUgyiratdarabok]
		JOIN [EREC_IraIratok] ON [EREC_UgyUgyiratdarabok].[Id] = [EREC_IraIratok].[UgyUgyIratDarab_Id]
		JOIN [EREC_KuldKuldemenyek] ON [EREC_KuldKuldemenyek].[Id] = [EREC_IraIratok].[KuldKuldemenyek_Id]
		WHERE [EREC_UgyUgyiratdarabok].[UgyUgyirat_Id] =  @UgyiratId
		UNION
		SELECT [EREC_PldIratPeldanyok].Id AS Id, 'EREC_PldIratPeldanyok' AS Obj_type FROM [EREC_UgyUgyiratdarabok]
		JOIN [EREC_IraIratok] ON [EREC_UgyUgyiratdarabok].[Id] = [EREC_IraIratok].[UgyUgyIratDarab_Id]
		JOIN [EREC_PldIratPeldanyok] ON [EREC_IraIratok].[Id] = [EREC_PldIratPeldanyok].[IraIrat_Id]
		WHERE [EREC_UgyUgyiratdarabok].[UgyUgyirat_Id] = @UgyiratId
    END
	

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
