IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokUpdateAllIraIratIrattarFields]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratokUpdateAllIraIratIrattarFields]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokUpdateAllIraIratIrattarFields]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratokUpdateAllIraIratIrattarFields] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyUgyiratokUpdateAllIraIratIrattarFields]
        @UgyiratIds			NVARCHAR(MAX),
		@IrattarbaKuldes	datetime = null,
		@IrattarbaVetel		datetime = null		
as
BEGIN
  
	if @IrattarbaKuldes is not NULL OR @IrattarbaVetel is not null
	begin
		update erec_irairatok 
		set erec_irairatok.IrattarbaKuldDatuma = ISNULL(@IrattarbaKuldes, erec_irairatok.IrattarbaKuldDatuma),
			erec_irairatok.IrattarbaVetelDat = ISNULL (@IrattarbaVetel, erec_irairatok.IrattarbaVetelDat)
		where [EREC_IraIratok].[Ugyirat_Id] IN (SELECT Value from dbo.fn_Split(@UgyiratIds,','))
	end

END


GO
