SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_UgyUgyiratok_FelfuggesztettHataridoNoveles')
            and   type = 'P')
   drop procedure sp_EREC_UgyUgyiratok_FelfuggesztettHataridoNoveles 
go

-- =============================================
-- Author:		AXIS REDNSZERHAZ KFT
-- Create date: 2017.07
-- Description: N�veli a felf�ggesztett �gyiat 
--              hat�ridej�t a k�etkez� munkanapra.
-- =============================================
CREATE PROCEDURE sp_EREC_UgyUgyiratok_FelfuggesztettHataridoNoveles 
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ID			UNIQUEIDENTIFIER
	DECLARE @ALLAPOT	NVARCHAR(MAX)
	DECLARE @HATARIDO	DATETIME

	DECLARE db_cursor CURSOR FOR  
	SELECT U.Id, u.Allapot, U.hatarido
	FROM dbo.EREC_UgyUgyiratok U
	WHERE U.Allapot = '62'	--Felf�ggesztett �llapot

	OPEN db_cursor   
	FETCH NEXT FROM db_cursor INTO @ID, @ALLAPOT,@HATARIDO

	DECLARE @MEGVAN_KOV_DATUM BIT 
	DECLARE @LIMIT BIT 
	DECLARE @KOV_DATUM DATETIME

	WHILE @@FETCH_STATUS = 0   
	BEGIN   
		 SET @KOV_DATUM = @HATARIDO
		 SET @MEGVAN_KOV_DATUM = 0
		 SET @LIMIT = 0

		 WHILE(@MEGVAN_KOV_DATUM = 0 OR @LIMIT > 365)
		 BEGIN

			SET @KOV_DATUM = DATEADD(DAY,1,@KOV_DATUM)

			IF ([dbo].[fn_munkanap] (@KOV_DATUM) = 1)
			BEGIN
				SET @MEGVAN_KOV_DATUM = 1
				BREAK
			END
			SET @LIMIT = @LIMIT + 1
		 
		 END

		 UPDATE dbo.EREC_UgyUgyiratok
		 SET Hatarido = @KOV_DATUM,
		 FelfuggesztettNapokSzama = ISNULL(FelfuggesztettNapokSzama, 0) + 1
		 WHERE Id = @ID 
			AND 
			   Hatarido < @KOV_DATUM

		 FETCH NEXT FROM db_cursor INTO @ID, @ALLAPOT,@HATARIDO   
	END   

	CLOSE db_cursor   
	DEALLOCATE db_cursor

END

GO
