IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratok_SkontrobolKiTomeges]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratok_SkontrobolKiTomeges]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratok_SkontrobolKiTomeges]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratok_SkontrobolKiTomeges] AS' 
END
GO

ALTER procedure [dbo].[sp_EREC_UgyUgyiratok_SkontrobolKiTomeges]
        @Ids						NVARCHAR(MAX)    ,
        @Vers						NVARCHAR(MAX)	 ,
		@FelhasznaloId				UNIQUEIDENTIFIER,		
		@FelhasznaloSzervezete UNIQUEIDENTIFIER = null,
		@LoginUserId UNIQUEIDENTIFIER,
        @SkontroVege			   DATETIME,         
        @Tranz_id     uniqueidentifier  = null

as

BEGIN TRY

	DECLARE @ExecutionTime DATETIME	
	set	@ExecutionTime = GETDATE()
	
	declare @tempTable table(id uniqueidentifier, ver int);
	
	declare @it	int;
	declare @verPosBegin int;
	declare @verPosEnd int;
	declare @curId	nvarchar(36);
	declare @curVer	nvarchar(6);

	set @it = 0;
	set @verPosBegin = 1;
	while (@it < ((len(@Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Ids,@it*39+2,37);
		set @verPosEnd =  CHARINDEX(',',@Vers,@verPosBegin);
		if @verPosEnd = 0 
			set @verPosEnd = len(@Vers) + 1;
		set @curVer = SUBSTRING(@vers, @verPosBegin, @verPosEnd - @verPosBegin);
		set @verPosBegin = @verPosEnd+1;
		insert into @tempTable(id,ver) values(@curId,convert(int,@curVer) );
		set @it = @it + 1;
	END
	

	declare @rowNumberTotal int;
	declare @rowNumberChecked int;

	select @rowNumberTotal = count(id) from @tempTable;
	
	
	/******************************************
	* Ellenorzések
	******************************************/
	-- verzióellenorzés
	select @rowNumberChecked = count(EREC_UgyUgyiratok.Id)
		from EREC_UgyUgyiratok
			inner JOIN @tempTable t on t.id = EREC_UgyUgyiratok.id and t.ver = EREC_UgyUgyiratok.ver
			
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
			inner join @tempTable t on t.id = EREC_UgyUgyiratok.id and t.ver = EREC_UgyUgyiratok.ver
		RAISERROR('[50402]',16,1);
	END
	
	-- zárolásellenorzés
	select @rowNumberChecked = count(EREC_UgyUgyiratok.Id)
		from EREC_UgyUgyiratok
			inner JOIN @tempTable t on t.id = EREC_UgyUgyiratok.id and ISNULL(EREC_UgyUgyiratok.Zarolo_id,@FelhasznaloId) = @FelhasznaloId
			
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
			inner join @tempTable t on t.id = EREC_UgyUgyiratok.id and ISNULL(EREC_UgyUgyiratok.Zarolo_id,@FelhasznaloId) = @FelhasznaloId
		RAISERROR('[50499]',16,1);
	END
	
	-- érvényesség vége figyelése
	select @rowNumberChecked = count(EREC_UgyUgyiratok.Id)
		from EREC_UgyUgyiratok
			inner JOIN @tempTable t on t.id = EREC_UgyUgyiratok.id and EREC_UgyUgyiratok.ErvVege > @ExecutionTime
			
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
			inner join @tempTable t on t.id = EREC_UgyUgyiratok.id and EREC_UgyUgyiratok.ErvVege > @ExecutionTime
		RAISERROR('[50403]',16,1);
	END
	

	/******************************************
	* UPDATE string összeállítás
	******************************************/
	DECLARE @sqlcmd	NVARCHAR(MAX);
	SET @sqlcmd = N'UPDATE EREC_UgyUgyiratok SET';
		
		-- SkontrobanOsszesen-t megnöveljük a skontróban eltöltött napok számával:
		 SET @sqlcmd = @sqlcmd + N' SkontrobanOsszesen = 
				ISNULL(SkontrobanOsszesen,0) + DATEDIFF ( day , ISNULL(SkontrobaDat, @SkontroVege) , @SkontroVege),'
		-- Határidot annyi nappal növeljük meg, amennyit skontróban töltött az ügyirat:
         SET @sqlcmd = @sqlcmd + N' Hatarido = 
			DATEADD(DAY, DATEDIFF ( day , ISNULL(SkontrobaDat, @SkontroVege) , @SkontroVege ), Hatarido),'
         SET @sqlcmd = @sqlcmd + N' SkontrobaDat = NULL,'
         SET @sqlcmd = @sqlcmd + N' SkontroOka = NULL,'
		 -- CR3407
		 SET @sqlcmd = @sqlcmd + N' SkontroOka_Kod = NULL,'
         SET @sqlcmd = @sqlcmd + N' SkontroVege = NULL,'
         -- Állapot: Ügyintézés alatt (06) lesz       
         SET @sqlcmd = @sqlcmd + N' Allapot = ''06'','
         SET @sqlcmd = @sqlcmd + N' Modosito_id = @FelhasznaloId,'
         SET @sqlcmd = @sqlcmd + N' ModositasIdo = @ExecutionTime,'
		 SET @sqlcmd = @sqlcmd + N' Ver = Ver + 1'   

   SET @sqlcmd = @sqlcmd + N'
	WHERE Id IN (' + @Ids + ');'

	EXECUTE sp_executesql @sqlcmd, N'	        
             @SkontroVege     datetime,      
             @ExecutionTime datetime,
             @FelhasznaloId uniqueidentifier
             '   
          ,            
             @SkontroVege = @SkontroVege,    
             @ExecutionTime = @ExecutionTime,
             @FelhasznaloId = @FelhasznaloId
            ;

if @@rowcount != @rowNumberTotal
begin
	RAISERROR('[50401]',16,1)
END

DECLARE @objTipId_Ugyirat UNIQUEIDENTIFIER
select @objTipId_Ugyirat = KRT_ObjTipusok.Id from KRT_ObjTipusok where KRT_ObjTipusok.Kod = 'EREC_UgyUgyiratok'

DECLARE @funkcioId UNIQUEIDENTIFIER
SELECT @funkcioId = KRT_Funkciok.Id from KRT_Funkciok where KRT_Funkciok.Kod = 'UgyiratSkontrobolKivetel'

DECLARE @LetrehozasIdo DATETIME
	SET @LetrehozasIdo = GETDATE()
	
DECLARE @ResultUid UNIQUEIDENTIFIER

/* History Log + Eseménynaplóba rekordok beszúrása */
/* dinamikus history log összeállításhoz */
declare @row_ids varchar(MAX)

/*** EREC_UgyUgyiratokHistory log ***/
set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @tempTable FOR XML PATH('')),1, 2, '')
if @row_ids is not null
	set @row_ids = @row_ids + '''';

exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_UgyUgyiratok'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @FelhasznaloId
		,@VegrehajtasIdo = @ExecutionTime

DECLARE cur CURSOR FOR
	SELECT Id FROM @tempTable

OPEN cur;
FETCH NEXT FROM cur INTO @curId;
WHILE @@FETCH_STATUS = 0
BEGIN
PRINT @curId
--	exec sp_LogRecordToHistory 'EREC_UgyUgyiratok',@curId
--					,'EREC_UgyUgyiratokHistory',1,@FelhasznaloId,@ExecutionTime
	
	DECLARE @ugyiratAzonosito NVARCHAR(400)
	SET @ugyiratAzonosito = dbo.fn_GetEREC_UgyUgyiratokAzonosito(@curId)
	
	EXEC [sp_KRT_EsemenyekInsert]
		@Obj_Id = @curId,
		@ObjTip_Id = @objTipId_Ugyirat,
		@Azonositoja = @ugyiratAzonosito,
		@Felhasznalo_Id_User = @FelhasznaloId,
		@Csoport_Id_FelelosUserSzerveze = @FelhasznaloSzervezete, 
		@Felhasznalo_Id_Login = @LoginUserId,	
		@Tranzakcio_Id = @Tranz_id, 
		@Funkcio_Id = @funkcioId,
		@Letrehozo_id =  @FelhasznaloId,
		@LetrehozasIdo = @LetrehozasIdo,
		@ResultUid = @ResultUid OUTPUT
	
	FETCH NEXT FROM cur INTO @curId;
END
CLOSE cur;
DEALLOCATE cur;





END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
END CATCH


GO
