IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyiratObjKapcsolatokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyiratObjKapcsolatokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyiratObjKapcsolatokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyiratObjKapcsolatokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyiratObjKapcsolatokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_UgyiratObjKapcsolatok.Id,
	   EREC_UgyiratObjKapcsolatok.KapcsolatTipus,
	   EREC_UgyiratObjKapcsolatok.Leiras,
	   EREC_UgyiratObjKapcsolatok.Kezi,
	   EREC_UgyiratObjKapcsolatok.Obj_Id_Elozmeny,
	   EREC_UgyiratObjKapcsolatok.Obj_Tip_Id_Elozmeny,
	   EREC_UgyiratObjKapcsolatok.Obj_Type_Elozmeny,
	   EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt,
	   EREC_UgyiratObjKapcsolatok.Obj_Tip_Id_Kapcsolt,
	   EREC_UgyiratObjKapcsolatok.Obj_Type_Kapcsolt,
	   EREC_UgyiratObjKapcsolatok.Ver,
	   EREC_UgyiratObjKapcsolatok.Note,
	   EREC_UgyiratObjKapcsolatok.Stat_id,
	   EREC_UgyiratObjKapcsolatok.ErvKezd,
	   EREC_UgyiratObjKapcsolatok.ErvVege,
	   EREC_UgyiratObjKapcsolatok.Letrehozo_id,
	   EREC_UgyiratObjKapcsolatok.LetrehozasIdo,
	   EREC_UgyiratObjKapcsolatok.Modosito_id,
	   EREC_UgyiratObjKapcsolatok.ModositasIdo,
	   EREC_UgyiratObjKapcsolatok.Zarolo_id,
	   EREC_UgyiratObjKapcsolatok.ZarolasIdo,
	   EREC_UgyiratObjKapcsolatok.Tranz_id,
	   EREC_UgyiratObjKapcsolatok.UIAccessLog_id
	   from 
		 EREC_UgyiratObjKapcsolatok as EREC_UgyiratObjKapcsolatok 
	   where
		 EREC_UgyiratObjKapcsolatok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
