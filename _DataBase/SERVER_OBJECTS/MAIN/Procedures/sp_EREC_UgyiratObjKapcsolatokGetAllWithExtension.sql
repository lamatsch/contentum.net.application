IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyiratObjKapcsolatokGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyiratObjKapcsolatokGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyiratObjKapcsolatokGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyiratObjKapcsolatokGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyiratObjKapcsolatokGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_UgyiratObjKapcsolatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
                   
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_UgyiratObjKapcsolatok.Id,
	   EREC_UgyiratObjKapcsolatok.KapcsolatTipus,
	   dbo.fn_KodtarErtekNeve(''UGYIRAT_OBJ_KAPCSOLATTIPUS'',EREC_UgyiratObjKapcsolatok.KapcsolatTipus,''' + CAST(@Org as NVarChar(40)) + ''') as KapcsolatTipusNev,
	   EREC_UgyiratObjKapcsolatok.Leiras,
	   EREC_UgyiratObjKapcsolatok.Kezi,
	   EREC_UgyiratObjKapcsolatok.Obj_Id_Elozmeny,
	   Obj_Elozmeny_Azonosito = 
	     CASE EREC_UgyiratObjKapcsolatok.Obj_Type_Elozmeny
			WHEN ''EREC_UgyUgyiratok'' THEN 
				dbo.fn_GetFoszam(Obj_Id_Elozmeny)	
		    WHEN ''EREC_IraIratok'' THEN 
				dbo.fn_GetIratIktatoszam(Obj_Id_Elozmeny)
		    WHEN ''EREC_PldIratPeldanyok'' THEN 
				dbo.fn_GetIratPeldanyIktatoszam(Obj_Id_Elozmeny)
		    WHEN ''EREC_KuldKuldemenyek'' THEN 
				dbo.fn_GetKuldemenyErkeztetoszam(Obj_Id_Elozmeny)	
		    ELSE ''''
	     END,
	   EREC_UgyiratObjKapcsolatok.Obj_Tip_Id_Elozmeny,
	   EREC_UgyiratObjKapcsolatok.Obj_Type_Elozmeny,
	   EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt,
	   Obj_Kapcsolt_Azonosito = 
	     CASE EREC_UgyiratObjKapcsolatok.Obj_Type_Kapcsolt
			WHEN ''EREC_UgyUgyiratok'' THEN 
				dbo.fn_GetFoszam(Obj_Id_Kapcsolt)	
		    WHEN ''EREC_IraIratok'' THEN 
				dbo.fn_GetIratIktatoszam(Obj_Id_Kapcsolt)
		    WHEN ''EREC_PldIratPeldanyok'' THEN 
				dbo.fn_GetIratPeldanyIktatoszam(Obj_Id_Kapcsolt)
		    WHEN ''EREC_KuldKuldemenyek'' THEN 
				dbo.fn_GetKuldemenyErkeztetoszam(Obj_Id_Kapcsolt)	
		    ELSE ''''
	     END,
	   EREC_UgyiratObjKapcsolatok.Obj_Tip_Id_Kapcsolt,
	   EREC_UgyiratObjKapcsolatok.Obj_Type_Kapcsolt,
	   EREC_UgyiratObjKapcsolatok.Ver,
	   EREC_UgyiratObjKapcsolatok.Note,
	   EREC_UgyiratObjKapcsolatok.Stat_id,
	   EREC_UgyiratObjKapcsolatok.ErvKezd,
	   EREC_UgyiratObjKapcsolatok.ErvVege,
	   EREC_UgyiratObjKapcsolatok.Letrehozo_id,
	   EREC_UgyiratObjKapcsolatok.LetrehozasIdo,
	   EREC_UgyiratObjKapcsolatok.Modosito_id,
	   EREC_UgyiratObjKapcsolatok.ModositasIdo,
	   EREC_UgyiratObjKapcsolatok.Zarolo_id,
	   EREC_UgyiratObjKapcsolatok.ZarolasIdo,
	   EREC_UgyiratObjKapcsolatok.Tranz_id,
	   EREC_UgyiratObjKapcsolatok.UIAccessLog_id  
   from 
     EREC_UgyiratObjKapcsolatok as EREC_UgyiratObjKapcsolatok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
