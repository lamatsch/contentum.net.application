IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyiratObjKapcsolatokHistoryGetRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyiratObjKapcsolatokHistoryGetRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyiratObjKapcsolatokHistoryGetRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_UgyiratObjKapcsolatokHistoryGetRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_UgyiratObjKapcsolatokHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_UgyiratObjKapcsolatokHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_UgyiratObjKapcsolatokHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KapcsolatTipus' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KapcsolatTipus != New.KapcsolatTipus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'KAPCSOLATTIPUS '
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.KapcsolatTipus and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.KapcsolatTipus and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Leiras' as ColumnName,               
               cast(Old.Leiras as nvarchar(99)) as OldValue,
               cast(New.Leiras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Leiras != New.Leiras 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kezi' as ColumnName,               
               cast(Old.Kezi as nvarchar(99)) as OldValue,
               cast(New.Kezi as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Kezi != New.Kezi 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Id_Elozmeny' as ColumnName,               
               cast(Old.Obj_Id_Elozmeny as nvarchar(99)) as OldValue,
               cast(New.Obj_Id_Elozmeny as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_Id_Elozmeny != New.Obj_Id_Elozmeny 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Tip_Id_Elozmeny' as ColumnName,               
               cast(Old.Obj_Tip_Id_Elozmeny as nvarchar(99)) as OldValue,
               cast(New.Obj_Tip_Id_Elozmeny as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_Tip_Id_Elozmeny != New.Obj_Tip_Id_Elozmeny 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Type_Elozmeny' as ColumnName,               
               cast(Old.Obj_Type_Elozmeny as nvarchar(99)) as OldValue,
               cast(New.Obj_Type_Elozmeny as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_Type_Elozmeny != New.Obj_Type_Elozmeny 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Id_Kapcsolt' as ColumnName,               
               cast(Old.Obj_Id_Kapcsolt as nvarchar(99)) as OldValue,
               cast(New.Obj_Id_Kapcsolt as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_Id_Kapcsolt != New.Obj_Id_Kapcsolt 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Tip_Id_Kapcsolt' as ColumnName,               
               cast(Old.Obj_Tip_Id_Kapcsolt as nvarchar(99)) as OldValue,
               cast(New.Obj_Tip_Id_Kapcsolt as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_Tip_Id_Kapcsolt != New.Obj_Tip_Id_Kapcsolt 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Type_Kapcsolt' as ColumnName,               
               cast(Old.Obj_Type_Kapcsolt as nvarchar(99)) as OldValue,
               cast(New.Obj_Type_Kapcsolt as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_Type_Kapcsolt != New.Obj_Type_Kapcsolt 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end


GO
