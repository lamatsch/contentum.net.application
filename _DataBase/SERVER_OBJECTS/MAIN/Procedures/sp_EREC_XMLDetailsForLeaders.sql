IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_XMLDetailsForLeaders]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_XMLDetailsForLeaders]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_XMLDetailsForLeaders]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_XMLDetailsForLeaders] AS' 
END
GO
-- Excel 'Reszletek' tabla export vezetoi statisztikához (Template Manager)
ALTER PROCEDURE [dbo].[sp_EREC_XMLDetailsForLeaders]
	@ExecutorUserId UNIQUEIDENTIFIER,
	@FelhasznaloSzervezet_Id UNIQUEIDENTIFIER,
	@KezdDat datetime,
	@VegeDat datetime
AS 
	BEGIN
		BEGIN TRY

			SET nocount ON
   
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

			IF OBJECT_ID('tempdb..#temp_SzervezetTagok') IS NOT NULL 
			BEGIN
			   DROP TABLE #temp_SzervezetTagok
			END

			CREATE TABLE #temp_SzervezetTagok
			(CsoportId uniqueidentifier not null)

			INSERT INTO #temp_SzervezetTagok 
					SELECT distinct KRT_CsoportTagokAll.Id
					FROM dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as KRT_CsoportTagokAll
						JOIN KRT_Csoportok ON KRT_CsoportTagokAll.Id = KRT_Csoportok.Id and KRT_Csoportok.Tipus='1'

			DECLARE @ResultTable TABLE 
			(
				Datum datetime, 
				Ugyintezo nvarchar(400),
				Felelos nvarchar(400),
				Orzo nvarchar(400),
				ObjektumTipus nvarchar(400),
				Tipus nvarchar(400),
				Allapot nvarchar(400),
				Hatarido datetime,
				AtadasIdopontja datetime,
				AtvetelIdopontja datetime,
				ElintezesIdopontja datetime,
				SkontrobanToltott int,
				Azonosito nvarchar(400)
			)
		
		INSERT INTO @ResultTable (Datum,
				Ugyintezo,
				Felelos,
				Orzo,
				ObjektumTipus,
				Tipus ,
				Allapot,
				Hatarido,
				AtadasIdopontja,
				AtvetelIdopontja,
				ElintezesIdopontja,
				SkontrobanToltott,
				Azonosito)
		--- KÜLDEMÉNYEK ---
		(SELECT 
			k.BeerkezesIdeje AS Datum,
			ISNULL((SELECT cs.Nev --- Ugyintezo kötelezo: NULL értékek helyettesítése
					FROM [dbo].[KRT_Csoportok] cs
					WHERE cs.Id = k.Csoport_Id_Felelos
			),'-') AS Ugyintezo, -- ertelmezni kuldemenyre
			(SELECT cs.Nev
					FROM [dbo].[KRT_Csoportok] cs
					WHERE cs.Id = k.Csoport_Id_Felelos
			) AS Felelos,
			(SELECT cs.Nev
					FROM [dbo].[KRT_Csoportok] cs
					WHERE cs.Id = k.FelhasznaloCsoport_Id_Orzo
			) AS Orzo,
			'Küldemény' AS ObjektumTipus,
			ISNULL((SELECT Nev -- kuldemenytipus (kimeno, bejovo) szoveges azonositasa
					FROM KRT_Kodtarak
					WHERE KodCsoport_Id = (SELECT Id 
												FROM KRT_KodCsoportok
												WHERE Kod = 'POSTAZAS_IRANYA'
											) AND
					Kod = k.PostazasIranya
					AND Org=@Org
			),'-') AS Tipus, --- Tipus kötelezo: NULL értékek helyettesítése       
			(SELECT Nev -- allapot szoveges azonositasa
				FROM KRT_Kodtarak
				WHERE KodCsoport_Id = (SELECT Id 
												FROM KRT_KodCsoportok
												WHERE Kod = 'KULDEMENY_ALLAPOT'
										) AND
				Kod = k.Allapot
				AND Org=@Org
			) AS Allapot,
			NULL AS Hatarido, -- nincs ertelme kuldemeny eseten, csak az UNION miatt kell  
			t.AtadasDat AS AtadasIdopontja,
			t.AtvetelDat AS AtvetelIdopontja,
			NULL AS ElintezesIdopontja,
			NULL AS SkontrobanToltott,
			--CAST(k.Erkezteto_Szam AS nvarchar(100)) AS Azonosito
			(SELECT MegKulJelzes FROM EREC_IraIktatokonyvek WHERE Id = k.IraIktatokonyv_Id) + '/' + CAST(k.Erkezteto_Szam AS varchar) + '/' 
				+ CAST((SELECT Ev FROM EREC_IraIktatokonyvek WHERE Id = k.IraIktatokonyv_Id) AS varchar) AS Azonosito
		FROM [dbo].[EREC_KuldKuldemenyek] k
			LEFT JOIN EREC_IraKezbesitesiTetelek t
			ON t.Obj_Id = k.Id
		WHERE k.Csoport_Id_Felelos IN (SELECT CsoportId
											FROM #temp_SzervezetTagok
										) AND
			k.BeerkezesIdeje BETWEEN CONVERT(nvarchar(40), @KezdDat,121) AND CONVERT(nvarchar(40), @VegeDat,121) 
	UNION
		--- ÜGYIRATOK ---
		SELECT
			u.LetrehozasIdo AS Datum, 
			ISNULL((SELECT cs.Nev --- Ugyintezo kötelezo: NULL értékek helyettesítése
					FROM [dbo].[KRT_Csoportok] cs
					WHERE cs.Id = u.FelhasznaloCsoport_Id_Ugyintez
			),'-') AS Ugyintezo,
			(SELECT cs.Nev
					FROM [dbo].[KRT_Csoportok] cs
					WHERE cs.Id = u.Csoport_Id_Felelos
			) AS Felelos,
			(SELECT cs.Nev
					FROM [dbo].[KRT_Csoportok] cs
					WHERE cs.Id = u.FelhasznaloCsoport_Id_Orzo
			) AS Orzo,
			'Ügy' AS ObjektumTipus,
			ISNULL((SELECT Nev -- ugytipus szoveges azonositasa
					FROM KRT_Kodtarak
					WHERE KodCsoport_Id = (SELECT Id 
												FROM KRT_KodCsoportok
												WHERE Kod = 'UGYTIPUS'
										) AND
					Kod = u.UgyTipus
					AND Org=@Org
			),'-')  AS Tipus,  --- Tipus kötelezo: NULL értékek helyettesítése
			(SELECT Nev -- allapot szoveges azonositasa
					FROM KRT_Kodtarak
					WHERE KodCsoport_Id = (SELECT Id 
												FROM KRT_KodCsoportok
												WHERE Kod = 'UGYIRAT_ALLAPOT'
											) AND
					Kod = u.Allapot
					AND Org=@Org
			) AS Allapot,
			u.Hatarido AS Hatarido,  
			t.AtadasDat AS AtadasIdopontja,
			t.AtvetelDat AS AtvetelIdopontja,
			u.ElintezesDat AS ElintezesIdopontja,
			u.SkontrobanOsszesen AS SkontrobanToltott, 
			-- u.Azonosito AS Azonosito
			(SELECT MegKulJelzes FROM EREC_IraIktatokonyvek WHERE Id = u.IraIktatokonyv_Id) + '/' + CAST(u.Foszam AS varchar) + '/' 
				+ CAST((SELECT Ev FROM EREC_IraIktatokonyvek WHERE Id = u.IraIktatokonyv_Id) AS varchar) AS Azonosito
		FROM [dbo].[EREC_UgyUgyiratok] u
			LEFT JOIN EREC_IraKezbesitesiTetelek t
			ON t.Obj_Id = u.Id
		WHERE u.Csoport_Id_Felelos IN (SELECT CsoportId
												FROM #temp_SzervezetTagok
										)
			AND u.LetrehozasIdo BETWEEN CONVERT(nvarchar(40), @KezdDat,121) AND CONVERT(nvarchar(40), @VegeDat,121) 
		) -- SELECT Küldemenyek és Ügyek vége
		ORDER BY Datum DESC


		--- explicit XML-struktúra ---
		SELECT 1 AS Tag,
			NULL AS Parent,
			--NULL AS [DetailsForStatistics!1],
			NULL AS [Row!1],
			REPLACE(CONVERT(varchar(10),Datum,102),'.','-') AS [Row!1!Datum!element],
			Ugyintezo AS [Row!1!Ugyintezo!element],
			Felelos AS [Row!1!Felelos!element],
			Orzo AS [Row!1!Orzo!element],
			ObjektumTipus AS [Row!1!ObjektumTipus!element],
			Tipus AS [Row!1!Tipus!element],
			Allapot AS [Row!1!Allapot!element],
			REPLACE(CONVERT(varchar(10),Hatarido,102),'.','-') AS [Row!1!Hatarido!element],
			REPLACE(CONVERT(varchar(10),AtadasIdopontja,102),'.','-') AS [Row!1!AtadasIdopontja!element],
			REPLACE(CONVERT(varchar(10),AtvetelIdopontja,102),'.','-') AS [Row!1!AtvetelIdopontja!element],
			REPLACE(CONVERT(varchar(10),ElintezesIdopontja,102),'.','-') AS [Row!1!ElintezesIdopontja!element],
			SkontrobanToltott AS [Row!1!SkontrobanToltott!element],
			Azonosito AS [Row!1!Azonosito!element]
		FROM @ResultTable
		FOR XML EXPLICIT

		IF OBJECT_ID('tempdb..#temp_SzervezetTagok') IS NOT NULL 
			BEGIN
			   DROP TABLE #temp_SzervezetTagok
			END

	END TRY
	BEGIN CATCH
		DECLARE @errorSeverity INT,
						@errorState INT
		DECLARE @errorCode NVARCHAR(1000)
		SET @errorSeverity = ERROR_SEVERITY()
		SET @errorState = ERROR_STATE()
	
		IF ERROR_NUMBER() < 50000 
			SET @errorCode = '[' + CONVERT(NVARCHAR(10), ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
		ELSE 
			SET @errorCode = ERROR_MESSAGE()
			
		IF @errorState = 0 
			SET @errorState = 1
				
		RAISERROR ( @errorCode, @errorSeverity, @errorState )
 
	END CATCH

END


GO
