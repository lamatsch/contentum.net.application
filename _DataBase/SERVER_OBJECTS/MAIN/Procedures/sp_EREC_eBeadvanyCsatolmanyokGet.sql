

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_eBeadvanyCsatolmanyokGet')
            and   type = 'P')
   drop procedure sp_EREC_eBeadvanyCsatolmanyokGet
go

create procedure sp_EREC_eBeadvanyCsatolmanyokGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_eBeadvanyCsatolmanyok.Id,
	   EREC_eBeadvanyCsatolmanyok.eBeadvany_Id,
	   EREC_eBeadvanyCsatolmanyok.Dokumentum_Id,
	   EREC_eBeadvanyCsatolmanyok.Nev,
	   EREC_eBeadvanyCsatolmanyok.Ver,
	   EREC_eBeadvanyCsatolmanyok.Note,
	   EREC_eBeadvanyCsatolmanyok.Stat_id,
	   EREC_eBeadvanyCsatolmanyok.ErvKezd,
	   EREC_eBeadvanyCsatolmanyok.ErvVege,
	   EREC_eBeadvanyCsatolmanyok.Letrehozo_id,
	   EREC_eBeadvanyCsatolmanyok.LetrehozasIdo,
	   EREC_eBeadvanyCsatolmanyok.Modosito_id,
	   EREC_eBeadvanyCsatolmanyok.ModositasIdo,
	   EREC_eBeadvanyCsatolmanyok.Zarolo_id,
	   EREC_eBeadvanyCsatolmanyok.ZarolasIdo,
	   EREC_eBeadvanyCsatolmanyok.Tranz_id,
	   EREC_eBeadvanyCsatolmanyok.UIAccessLog_id
	   from 
		 EREC_eBeadvanyCsatolmanyok as EREC_eBeadvanyCsatolmanyok 
	   where
		 EREC_eBeadvanyCsatolmanyok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
