set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_eBeadvanyCsatolmanyokHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_EREC_eBeadvanyCsatolmanyokHistoryGetAllRecord
go

create procedure sp_EREC_eBeadvanyCsatolmanyokHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'L�trehoz�s' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_eBeadvanyCsatolmanyokHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyCsatolmanyokHistory Old
         inner join EREC_eBeadvanyCsatolmanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'eBeadvany_Id' as ColumnName,               cast(Old.eBeadvany_Id as nvarchar(99)) as OldValue,
               cast(New.eBeadvany_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyCsatolmanyokHistory Old
         inner join EREC_eBeadvanyCsatolmanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.eBeadvany_Id != New.eBeadvany_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Dokumentum_Id' as ColumnName,               cast(Old.Dokumentum_Id as nvarchar(99)) as OldValue,
               cast(New.Dokumentum_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyCsatolmanyokHistory Old
         inner join EREC_eBeadvanyCsatolmanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Dokumentum_Id != New.Dokumentum_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Nev' as ColumnName,               cast(Old.Nev as nvarchar(99)) as OldValue,
               cast(New.Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyCsatolmanyokHistory Old
         inner join EREC_eBeadvanyCsatolmanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Nev != New.Nev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go