
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_eBeadvanyokGetAllWithExtension')
            and   type = 'P')
   drop procedure sp_EREC_eBeadvanyokGetAllWithExtension
go

create procedure sp_EREC_eBeadvanyokGetAllWithExtension
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_eBeadvanyok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @pageNumber		int = 0,
  @pageSize		int = -1,
  @SelectedRowId	uniqueidentifier = null,
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount ON
    
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 	DECLARE @firstRow INT
	DECLARE @lastRow INT
   
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow != '' and @TopRow != '0')
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end
 
	/************************************************************
	* Sz�rt adatokhoz rendez�s �s sorsz�m �ssze�ll�t�sa			*
	************************************************************/
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   EREC_eBeadvanyok.Id into #result
		from EREC_eBeadvanyok as EREC_eBeadvanyok
 '
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end

	if (@SelectedRowId is not null)
	begin
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id = @SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	end

	set @sqlcmd = @sqlcmd + N' 
	if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
	BEGIN
		select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
		set @firstRow = (@pageNumber - 1) * @pageSize + 1;
		set @lastRow = @pageNumber*@pageSize;
		select @pageNumber = @pageNumber - 1
	END ;'

	/************************************************************
	* T�nyleges select											*
	************************************************************/
	SET @sqlcmd = @sqlcmd + 
  'select 
		#result.RowNumber,
  	   EREC_eBeadvanyok.Id,
	   EREC_eBeadvanyok.Irany,
	   EREC_eBeadvanyok.Allapot,
	   ktAllapot.Nev as AllapotNev,
	   EREC_eBeadvanyok.KuldoRendszer,
	   EREC_eBeadvanyok.UzenetTipusa,
	   EREC_eBeadvanyok.FeladoTipusa,
	   ktFeladoTipusa.Nev as FeladoTipusaNev,
	   EREC_eBeadvanyok.PartnerKapcsolatiKod,
	   EREC_eBeadvanyok.PartnerNev,
	   EREC_eBeadvanyok.PartnerEmail,
	   EREC_eBeadvanyok.PartnerRovidNev,
	   EREC_eBeadvanyok.PartnerMAKKod,
	   EREC_eBeadvanyok.PartnerKRID,
	   EREC_eBeadvanyok.Partner_Id,
	   EREC_eBeadvanyok.Cim_Id,
	   EREC_eBeadvanyok.KR_HivatkozasiSzam,
	   EREC_eBeadvanyok.KR_ErkeztetesiSzam,
	   EREC_eBeadvanyok.Contentum_HivatkozasiSzam,
	   EREC_eBeadvanyok.PR_HivatkozasiSzam,
	   EREC_eBeadvanyok.PR_ErkeztetesiSzam,
	   EREC_eBeadvanyok.KR_DokTipusHivatal,
	   EREC_eBeadvanyok.KR_DokTipusAzonosito,
	   EREC_eBeadvanyok.KR_DokTipusLeiras,
	   EREC_eBeadvanyok.KR_Megjegyzes,
	   EREC_eBeadvanyok.KR_ErvenyessegiDatum,
	   EREC_eBeadvanyok.KR_ErkeztetesiDatum,
	   EREC_eBeadvanyok.KR_FileNev,
	   EREC_eBeadvanyok.KR_Kezbesitettseg,
	   ktKezbesitettseg.Nev as KR_KezbesitettsegNev,
	   EREC_eBeadvanyok.KR_Idopecset,
	   EREC_eBeadvanyok.KR_Valasztitkositas,
	   EREC_eBeadvanyok.KR_Valaszutvonal,
	   ktValaszUtvonal.Nev as KR_ValaszUtvonalNev,
	   EREC_eBeadvanyok.KR_Rendszeruzenet,
	   EREC_eBeadvanyok.KR_Tarterulet,
	   ktTarterulet.Nev as KR_TarteruletNev,
	   EREC_eBeadvanyok.KR_ETertiveveny,
	   EREC_eBeadvanyok.KR_Lenyomat,
	   EREC_eBeadvanyok.KuldKuldemeny_Id,
	   (SELECT Azonosito
		 FROM EREC_KuldKuldemenyek
		 WHERE EREC_KuldKuldemenyek.Id = EREC_eBeadvanyok.KuldKuldemeny_Id) as ErkeztetoSzam,
	   EREC_eBeadvanyok.IraIrat_Id,
	   IratIktatoszamEsId = case 
	   WHEN coalesce(EREC_IraIratok_Irat.Id, EREC_IraIratok_Kuld.Id) is null then ''''
	   ELSE coalesce(EREC_IraIratok_Irat.Azonosito, EREC_IraIratok_Kuld.Azonosito) + ''***'' + convert(NVARCHAR(40), coalesce(EREC_IraIratok_Irat.Id, EREC_IraIratok_Kuld.Id)) end,
	   EREC_eBeadvanyok.IratPeldany_Id,
	   EREC_PldIratPeldanyok.Azonosito as IratPeldany_Azonosito,
	   EREC_eBeadvanyok.Cel,
	   EREC_eBeadvanyok.Ver,
	   EREC_eBeadvanyok.Note,
	   EREC_eBeadvanyok.Stat_id,
	   EREC_eBeadvanyok.ErvKezd,
	   EREC_eBeadvanyok.ErvVege,
	   EREC_eBeadvanyok.Letrehozo_id,
	   EREC_eBeadvanyok.LetrehozasIdo,
	   EREC_eBeadvanyok.Modosito_id,
	   EREC_eBeadvanyok.ModositasIdo,
	   EREC_eBeadvanyok.Zarolo_id,
	   EREC_eBeadvanyok.ZarolasIdo,
	   EREC_eBeadvanyok.Tranz_id,
	   EREC_eBeadvanyok.UIAccessLog_id,
	   (select count(Id) from EREC_eBeadvanyCsatolmanyok where EREC_eBeadvanyCsatolmanyok.eBeadvany_id = EREC_eBeadvanyok.Id
	   and getdate() between EREC_eBeadvanyCsatolmanyok.ErvKezd and EREC_eBeadvanyCsatolmanyok.ErvVege) as CsatolmanyCount,
	   Dokumentum_Id = (SELECT top 1 EREC_eBeadvanyCsatolmanyok.Dokumentum_Id
                        FROM EREC_eBeadvanyCsatolmanyok
				        where EREC_eBeadvanyCsatolmanyok.eBeadvany_id = EREC_eBeadvanyok.Id
						and EREC_eBeadvanyCsatolmanyok.Nev = EREC_eBeadvanyok.KR_FileNev
				        and getdate() BETWEEN EREC_eBeadvanyCsatolmanyok.ErvKezd AND EREC_eBeadvanyCsatolmanyok.ErvVege),
	   External_Link = (SELECT top 1 KRT_Dokumentumok.External_Link
                        FROM EREC_eBeadvanyCsatolmanyok
						join KRT_Dokumentumok 
						on EREC_eBeadvanyCsatolmanyok.Dokumentum_Id = KRT_Dokumentumok.Id
				        where EREC_eBeadvanyCsatolmanyok.eBeadvany_id = EREC_eBeadvanyok.Id
						and EREC_eBeadvanyCsatolmanyok.Nev = EREC_eBeadvanyok.KR_FileNev
				        and getdate() BETWEEN EREC_eBeadvanyCsatolmanyok.ErvKezd AND EREC_eBeadvanyCsatolmanyok.ErvVege)
   from'
   set @sqlcmd = @sqlcmd + ' 
     EREC_eBeadvanyok as EREC_eBeadvanyok
	 inner join #result on #result.Id = EREC_eBeadvanyok.Id      
		 LEFT JOIN (select Id, KuldKuldemenyek_Id, Azonosito from EREC_IraIratok where EREC_IraIratok.Allapot not in (''06'', ''08'')) as EREC_IraIratok_Irat
		on EREC_eBeadvanyok.IraIrat_Id is not null and EREC_eBeadvanyok.IraIrat_Id=EREC_IraIratok_Irat.Id
    LEFT JOIN (select Id, KuldKuldemenyek_Id, Azonosito from EREC_IraIratok where EREC_IraIratok.Allapot not in (''06'', ''08'')) as EREC_IraIratok_Kuld
		on EREC_eBeadvanyok.IraIrat_Id is null and EREC_eBeadvanyok.KuldKuldemeny_Id is not null
		and EREC_IraIratok_Kuld.KuldKuldemenyek_Id=EREC_eBeadvanyok.KuldKuldemeny_Id
	LEFT JOIN EREC_PldIratPeldanyok on EREC_eBeadvanyok.IratPeldany_Id = EREC_PldIratPeldanyok.Id
	LEFT JOIN KRT_KodCsoportok as kcsFeladoTipusa on kcsFeladoTipusa.Kod=''HKP_FELADO_TIPUSA''
    LEFT JOIN KRT_KodTarak as ktFeladoTipusa on EREC_eBeadvanyok.FeladoTipusa = ktFeladoTipusa.Kod and kcsFeladoTipusa.Id = ktFeladoTipusa.KodCsoport_Id and ktFeladoTipusa.Org=@Org
    LEFT JOIN KRT_KodCsoportok as kcsKezbesitettseg on kcsKezbesitettseg.Kod=''HKP_KEZBESITETTSEG''
	LEFT JOIN KRT_KodTarak as ktKezbesitettseg on EREC_eBeadvanyok.KR_Kezbesitettseg = ktKezbesitettseg.Kod and kcsKezbesitettseg.Id = ktKezbesitettseg.KodCsoport_Id and ktKezbesitettseg.Org=@Org
	LEFT JOIN KRT_KodCsoportok as kcsValaszutvonal on kcsValaszutvonal.Kod=''HKP_VALASZUTVONAL''
	LEFT JOIN KRT_KodTarak as ktValaszutvonal on EREC_eBeadvanyok.KR_Valaszutvonal = ktValaszutvonal.Kod and kcsValaszutvonal.Id = ktValaszutvonal.KodCsoport_Id and ktValaszutvonal.Org=@Org
	LEFT JOIN KRT_KodCsoportok as kcsTarterulet on kcsTarterulet.Kod=''HKP_TARTERULET''
	LEFT JOIN KRT_KodTarak as ktTarterulet on EREC_eBeadvanyok.KR_Tarterulet = ktTarterulet.Kod and kcsTarterulet.Id = ktTarterulet.KodCsoport_Id and ktTarterulet.Org=@Org
	LEFT JOIN KRT_KodCsoportok as kcsAllapot on kcsAllapot.Kod=''HKP_ALLAPOT''
	LEFT JOIN KRT_KodTarak as ktAllapot on EREC_eBeadvanyok.Allapot = ktAllapot.Kod and kcsAllapot.Id = ktAllapot.KodCsoport_Id and ktAllapot.Org=@Org
'
	if (@firstRow is not null and @lastRow is not null)
	BEGIN
		set @sqlcmd = @sqlcmd + '	where RowNumber between @firstRow and @lastRow
'
	END

	set @sqlcmd = @sqlcmd + '	ORDER BY #result.RowNumber;'

	-- tal�latok sz�ma �s oldalsz�m
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';
 	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier,  @Org uniqueidentifier',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId, @Org = @Org;
  
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go