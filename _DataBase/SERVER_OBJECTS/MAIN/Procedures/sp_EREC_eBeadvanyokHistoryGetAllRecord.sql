set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_eBeadvanyokHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_EREC_eBeadvanyokHistoryGetAllRecord
go

create procedure sp_EREC_eBeadvanyokHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_eBeadvanyokHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Irany' as ColumnName,               cast(Old.Irany as nvarchar(99)) as OldValue,
               cast(New.Irany as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Irany != New.Irany 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Allapot != New.Allapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'eBEADVANY_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Allapot and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Allapot and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldoRendszer' as ColumnName,               cast(Old.KuldoRendszer as nvarchar(99)) as OldValue,
               cast(New.KuldoRendszer as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KuldoRendszer != New.KuldoRendszer 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UzenetTipusa' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UzenetTipusa != New.UzenetTipusa 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UZENET_TIPUS'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.UzenetTipusa and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.UzenetTipusa and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FeladoTipusa' as ColumnName,               cast(Old.FeladoTipusa as nvarchar(99)) as OldValue,
               cast(New.FeladoTipusa as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.FeladoTipusa != New.FeladoTipusa 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PartnerKapcsolatiKod' as ColumnName,               cast(Old.PartnerKapcsolatiKod as nvarchar(99)) as OldValue,
               cast(New.PartnerKapcsolatiKod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PartnerKapcsolatiKod != New.PartnerKapcsolatiKod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PartnerNev' as ColumnName,               cast(Old.PartnerNev as nvarchar(99)) as OldValue,
               cast(New.PartnerNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PartnerNev != New.PartnerNev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PartnerEmail' as ColumnName,               cast(Old.PartnerEmail as nvarchar(99)) as OldValue,
               cast(New.PartnerEmail as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PartnerEmail != New.PartnerEmail 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PartnerRovidNev' as ColumnName,               cast(Old.PartnerRovidNev as nvarchar(99)) as OldValue,
               cast(New.PartnerRovidNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PartnerRovidNev != New.PartnerRovidNev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PartnerMAKKod' as ColumnName,               cast(Old.PartnerMAKKod as nvarchar(99)) as OldValue,
               cast(New.PartnerMAKKod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PartnerMAKKod != New.PartnerMAKKod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PartnerKRID' as ColumnName,               cast(Old.PartnerKRID as nvarchar(99)) as OldValue,
               cast(New.PartnerKRID as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PartnerKRID != New.PartnerKRID 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id' as ColumnName,               cast(Old.Partner_Id as nvarchar(99)) as OldValue,
               cast(New.Partner_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Partner_Id != New.Partner_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Cim_Id' as ColumnName,               cast(Old.Cim_Id as nvarchar(99)) as OldValue,
               cast(New.Cim_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Cim_Id != New.Cim_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_HivatkozasiSzam' as ColumnName,               cast(Old.KR_HivatkozasiSzam as nvarchar(99)) as OldValue,
               cast(New.KR_HivatkozasiSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_HivatkozasiSzam != New.KR_HivatkozasiSzam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_ErkeztetesiSzam' as ColumnName,               cast(Old.KR_ErkeztetesiSzam as nvarchar(99)) as OldValue,
               cast(New.KR_ErkeztetesiSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_ErkeztetesiSzam != New.KR_ErkeztetesiSzam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Contentum_HivatkozasiSzam' as ColumnName,               cast(Old.Contentum_HivatkozasiSzam as nvarchar(99)) as OldValue,
               cast(New.Contentum_HivatkozasiSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Contentum_HivatkozasiSzam != New.Contentum_HivatkozasiSzam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PR_HivatkozasiSzam' as ColumnName,               cast(Old.PR_HivatkozasiSzam as nvarchar(99)) as OldValue,
               cast(New.PR_HivatkozasiSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PR_HivatkozasiSzam != New.PR_HivatkozasiSzam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PR_ErkeztetesiSzam' as ColumnName,               cast(Old.PR_ErkeztetesiSzam as nvarchar(99)) as OldValue,
               cast(New.PR_ErkeztetesiSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.PR_ErkeztetesiSzam != New.PR_ErkeztetesiSzam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_DokTipusHivatal' as ColumnName,               cast(Old.KR_DokTipusHivatal as nvarchar(99)) as OldValue,
               cast(New.KR_DokTipusHivatal as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_DokTipusHivatal != New.KR_DokTipusHivatal 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_DokTipusAzonosito' as ColumnName,               cast(Old.KR_DokTipusAzonosito as nvarchar(99)) as OldValue,
               cast(New.KR_DokTipusAzonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_DokTipusAzonosito != New.KR_DokTipusAzonosito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_DokTipusLeiras' as ColumnName,               cast(Old.KR_DokTipusLeiras as nvarchar(99)) as OldValue,
               cast(New.KR_DokTipusLeiras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_DokTipusLeiras != New.KR_DokTipusLeiras 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Megjegyzes' as ColumnName,               cast(Old.KR_Megjegyzes as nvarchar(99)) as OldValue,
               cast(New.KR_Megjegyzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_Megjegyzes != New.KR_Megjegyzes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_ErvenyessegiDatum' as ColumnName,               cast(Old.KR_ErvenyessegiDatum as nvarchar(99)) as OldValue,
               cast(New.KR_ErvenyessegiDatum as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_ErvenyessegiDatum != New.KR_ErvenyessegiDatum 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_ErkeztetesiDatum' as ColumnName,               cast(Old.KR_ErkeztetesiDatum as nvarchar(99)) as OldValue,
               cast(New.KR_ErkeztetesiDatum as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_ErkeztetesiDatum != New.KR_ErkeztetesiDatum 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_FileNev' as ColumnName,               cast(Old.KR_FileNev as nvarchar(99)) as OldValue,
               cast(New.KR_FileNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_FileNev != New.KR_FileNev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Kezbesitettseg' as ColumnName,               cast(Old.KR_Kezbesitettseg as nvarchar(99)) as OldValue,
               cast(New.KR_Kezbesitettseg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_Kezbesitettseg != New.KR_Kezbesitettseg 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Idopecset' as ColumnName,               cast(Old.KR_Idopecset as nvarchar(99)) as OldValue,
               cast(New.KR_Idopecset as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_Idopecset != New.KR_Idopecset 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Valasztitkositas' as ColumnName,               cast(Old.KR_Valasztitkositas as nvarchar(99)) as OldValue,
               cast(New.KR_Valasztitkositas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_Valasztitkositas != New.KR_Valasztitkositas 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Valaszutvonal' as ColumnName,               cast(Old.KR_Valaszutvonal as nvarchar(99)) as OldValue,
               cast(New.KR_Valaszutvonal as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_Valaszutvonal != New.KR_Valaszutvonal 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Rendszeruzenet' as ColumnName,               cast(Old.KR_Rendszeruzenet as nvarchar(99)) as OldValue,
               cast(New.KR_Rendszeruzenet as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_Rendszeruzenet != New.KR_Rendszeruzenet 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Tarterulet' as ColumnName,               cast(Old.KR_Tarterulet as nvarchar(99)) as OldValue,
               cast(New.KR_Tarterulet as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_Tarterulet != New.KR_Tarterulet 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_ETertiveveny' as ColumnName,               cast(Old.KR_ETertiveveny as nvarchar(99)) as OldValue,
               cast(New.KR_ETertiveveny as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_ETertiveveny != New.KR_ETertiveveny 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Lenyomat' as ColumnName,               cast(Old.KR_Lenyomat as nvarchar(99)) as OldValue,
               cast(New.KR_Lenyomat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KR_Lenyomat != New.KR_Lenyomat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldKuldemeny_Id' as ColumnName,               cast(Old.KuldKuldemeny_Id as nvarchar(99)) as OldValue,
               cast(New.KuldKuldemeny_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KuldKuldemeny_Id != New.KuldKuldemeny_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIrat_Id' as ColumnName,               cast(Old.IraIrat_Id as nvarchar(99)) as OldValue,
               cast(New.IraIrat_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IraIrat_Id != New.IraIrat_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IratPeldany_Id' as ColumnName,               cast(Old.IratPeldany_Id as nvarchar(99)) as OldValue,
               cast(New.IratPeldany_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IratPeldany_Id != New.IratPeldany_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Cel' as ColumnName,               cast(Old.Cel as nvarchar(99)) as OldValue,
               cast(New.Cel as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Cel != New.Cel 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go