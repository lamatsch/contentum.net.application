

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_eBeadvanyokUpdate')
            and   type = 'P')
   drop procedure sp_EREC_eBeadvanyokUpdate
go

create procedure sp_EREC_eBeadvanyokUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Irany     char(1)  = null,         
             @Allapot     nvarchar(64)  = null,         
             @KuldoRendszer     Nvarchar(400)  = null,         
             @UzenetTipusa     nvarchar(64)  = null,         
             @FeladoTipusa     int  = null,         
             @PartnerKapcsolatiKod     Nvarchar(400)  = null,         
             @PartnerNev     Nvarchar(400)  = null,         
             @PartnerEmail     Nvarchar(400)  = null,         
             @PartnerRovidNev     Nvarchar(400)  = null,         
             @PartnerMAKKod     Nvarchar(400)  = null,         
             @PartnerKRID     Nvarchar(400)  = null,         
             @Partner_Id     uniqueidentifier  = null,         
             @Cim_Id     uniqueidentifier  = null,         
             @KR_HivatkozasiSzam     Nvarchar(400)  = null,         
             @KR_ErkeztetesiSzam     Nvarchar(400)  = null,         
             @Contentum_HivatkozasiSzam     uniqueidentifier  = null,         
             @PR_HivatkozasiSzam     Nvarchar(400)  = null,         
             @PR_ErkeztetesiSzam     Nvarchar(400)  = null,         
             @KR_DokTipusHivatal     Nvarchar(400)  = null,         
             @KR_DokTipusAzonosito     Nvarchar(400)  = null,         
             @KR_DokTipusLeiras     Nvarchar(400)  = null,         
             @KR_Megjegyzes     Nvarchar(4000)  = null,         
             @KR_ErvenyessegiDatum     datetime  = null,         
             @KR_ErkeztetesiDatum     datetime  = null,         
             @KR_FileNev     Nvarchar(400)  = null,         
             @KR_Kezbesitettseg     int  = null,         
             @KR_Idopecset     Nvarchar(4000)  = null,         
             @KR_Valasztitkositas     char(1)  = null,         
             @KR_Valaszutvonal     int  = null,         
             @KR_Rendszeruzenet     char(1)  = null,         
             @KR_Tarterulet     int  = null,         
             @KR_ETertiveveny     char(1)  = null,         
             @KR_Lenyomat     Nvarchar(Max)  = null,         
             @KuldKuldemeny_Id     uniqueidentifier  = null,         
             @IraIrat_Id     uniqueidentifier  = null,         
             @IratPeldany_Id     uniqueidentifier  = null,         
             @Cel     Nvarchar(400)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Irany     char(1)         
     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_KuldoRendszer     Nvarchar(400)         
     DECLARE @Act_UzenetTipusa     nvarchar(64)         
     DECLARE @Act_FeladoTipusa     int         
     DECLARE @Act_PartnerKapcsolatiKod     Nvarchar(400)         
     DECLARE @Act_PartnerNev     Nvarchar(400)         
     DECLARE @Act_PartnerEmail     Nvarchar(400)         
     DECLARE @Act_PartnerRovidNev     Nvarchar(400)         
     DECLARE @Act_PartnerMAKKod     Nvarchar(400)         
     DECLARE @Act_PartnerKRID     Nvarchar(400)         
     DECLARE @Act_Partner_Id     uniqueidentifier         
     DECLARE @Act_Cim_Id     uniqueidentifier         
     DECLARE @Act_KR_HivatkozasiSzam     Nvarchar(400)         
     DECLARE @Act_KR_ErkeztetesiSzam     Nvarchar(400)         
     DECLARE @Act_Contentum_HivatkozasiSzam     uniqueidentifier         
     DECLARE @Act_PR_HivatkozasiSzam     Nvarchar(400)         
     DECLARE @Act_PR_ErkeztetesiSzam     Nvarchar(400)         
     DECLARE @Act_KR_DokTipusHivatal     Nvarchar(400)         
     DECLARE @Act_KR_DokTipusAzonosito     Nvarchar(400)         
     DECLARE @Act_KR_DokTipusLeiras     Nvarchar(400)         
     DECLARE @Act_KR_Megjegyzes     Nvarchar(4000)         
     DECLARE @Act_KR_ErvenyessegiDatum     datetime         
     DECLARE @Act_KR_ErkeztetesiDatum     datetime         
     DECLARE @Act_KR_FileNev     Nvarchar(400)         
     DECLARE @Act_KR_Kezbesitettseg     int         
     DECLARE @Act_KR_Idopecset     Nvarchar(4000)         
     DECLARE @Act_KR_Valasztitkositas     char(1)         
     DECLARE @Act_KR_Valaszutvonal     int         
     DECLARE @Act_KR_Rendszeruzenet     char(1)         
     DECLARE @Act_KR_Tarterulet     int         
     DECLARE @Act_KR_ETertiveveny     char(1)         
     DECLARE @Act_KR_Lenyomat     Nvarchar(Max)         
     DECLARE @Act_KuldKuldemeny_Id     uniqueidentifier         
     DECLARE @Act_IraIrat_Id     uniqueidentifier         
     DECLARE @Act_IratPeldany_Id     uniqueidentifier         
     DECLARE @Act_Cel     Nvarchar(400)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Irany = Irany,
     @Act_Allapot = Allapot,
     @Act_KuldoRendszer = KuldoRendszer,
     @Act_UzenetTipusa = UzenetTipusa,
     @Act_FeladoTipusa = FeladoTipusa,
     @Act_PartnerKapcsolatiKod = PartnerKapcsolatiKod,
     @Act_PartnerNev = PartnerNev,
     @Act_PartnerEmail = PartnerEmail,
     @Act_PartnerRovidNev = PartnerRovidNev,
     @Act_PartnerMAKKod = PartnerMAKKod,
     @Act_PartnerKRID = PartnerKRID,
     @Act_Partner_Id = Partner_Id,
     @Act_Cim_Id = Cim_Id,
     @Act_KR_HivatkozasiSzam = KR_HivatkozasiSzam,
     @Act_KR_ErkeztetesiSzam = KR_ErkeztetesiSzam,
     @Act_Contentum_HivatkozasiSzam = Contentum_HivatkozasiSzam,
     @Act_PR_HivatkozasiSzam = PR_HivatkozasiSzam,
     @Act_PR_ErkeztetesiSzam = PR_ErkeztetesiSzam,
     @Act_KR_DokTipusHivatal = KR_DokTipusHivatal,
     @Act_KR_DokTipusAzonosito = KR_DokTipusAzonosito,
     @Act_KR_DokTipusLeiras = KR_DokTipusLeiras,
     @Act_KR_Megjegyzes = KR_Megjegyzes,
     @Act_KR_ErvenyessegiDatum = KR_ErvenyessegiDatum,
     @Act_KR_ErkeztetesiDatum = KR_ErkeztetesiDatum,
     @Act_KR_FileNev = KR_FileNev,
     @Act_KR_Kezbesitettseg = KR_Kezbesitettseg,
     @Act_KR_Idopecset = KR_Idopecset,
     @Act_KR_Valasztitkositas = KR_Valasztitkositas,
     @Act_KR_Valaszutvonal = KR_Valaszutvonal,
     @Act_KR_Rendszeruzenet = KR_Rendszeruzenet,
     @Act_KR_Tarterulet = KR_Tarterulet,
     @Act_KR_ETertiveveny = KR_ETertiveveny,
     @Act_KR_Lenyomat = KR_Lenyomat,
     @Act_KuldKuldemeny_Id = KuldKuldemeny_Id,
     @Act_IraIrat_Id = IraIrat_Id,
     @Act_IratPeldany_Id = IratPeldany_Id,
     @Act_Cel = Cel,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_eBeadvanyok
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zárolás ellenőrzés:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Irany')=1
         SET @Act_Irany = @Irany
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/KuldoRendszer')=1
         SET @Act_KuldoRendszer = @KuldoRendszer
   IF @UpdatedColumns.exist('/root/UzenetTipusa')=1
         SET @Act_UzenetTipusa = @UzenetTipusa
   IF @UpdatedColumns.exist('/root/FeladoTipusa')=1
         SET @Act_FeladoTipusa = @FeladoTipusa
   IF @UpdatedColumns.exist('/root/PartnerKapcsolatiKod')=1
         SET @Act_PartnerKapcsolatiKod = @PartnerKapcsolatiKod
   IF @UpdatedColumns.exist('/root/PartnerNev')=1
         SET @Act_PartnerNev = @PartnerNev
   IF @UpdatedColumns.exist('/root/PartnerEmail')=1
         SET @Act_PartnerEmail = @PartnerEmail
   IF @UpdatedColumns.exist('/root/PartnerRovidNev')=1
         SET @Act_PartnerRovidNev = @PartnerRovidNev
   IF @UpdatedColumns.exist('/root/PartnerMAKKod')=1
         SET @Act_PartnerMAKKod = @PartnerMAKKod
   IF @UpdatedColumns.exist('/root/PartnerKRID')=1
         SET @Act_PartnerKRID = @PartnerKRID
   IF @UpdatedColumns.exist('/root/Partner_Id')=1
         SET @Act_Partner_Id = @Partner_Id
   IF @UpdatedColumns.exist('/root/Cim_Id')=1
         SET @Act_Cim_Id = @Cim_Id
   IF @UpdatedColumns.exist('/root/KR_HivatkozasiSzam')=1
         SET @Act_KR_HivatkozasiSzam = @KR_HivatkozasiSzam
   IF @UpdatedColumns.exist('/root/KR_ErkeztetesiSzam')=1
         SET @Act_KR_ErkeztetesiSzam = @KR_ErkeztetesiSzam
   IF @UpdatedColumns.exist('/root/Contentum_HivatkozasiSzam')=1
         SET @Act_Contentum_HivatkozasiSzam = @Contentum_HivatkozasiSzam
   IF @UpdatedColumns.exist('/root/PR_HivatkozasiSzam')=1
         SET @Act_PR_HivatkozasiSzam = @PR_HivatkozasiSzam
   IF @UpdatedColumns.exist('/root/PR_ErkeztetesiSzam')=1
         SET @Act_PR_ErkeztetesiSzam = @PR_ErkeztetesiSzam
   IF @UpdatedColumns.exist('/root/KR_DokTipusHivatal')=1
         SET @Act_KR_DokTipusHivatal = @KR_DokTipusHivatal
   IF @UpdatedColumns.exist('/root/KR_DokTipusAzonosito')=1
         SET @Act_KR_DokTipusAzonosito = @KR_DokTipusAzonosito
   IF @UpdatedColumns.exist('/root/KR_DokTipusLeiras')=1
         SET @Act_KR_DokTipusLeiras = @KR_DokTipusLeiras
   IF @UpdatedColumns.exist('/root/KR_Megjegyzes')=1
         SET @Act_KR_Megjegyzes = @KR_Megjegyzes
   IF @UpdatedColumns.exist('/root/KR_ErvenyessegiDatum')=1
         SET @Act_KR_ErvenyessegiDatum = @KR_ErvenyessegiDatum
   IF @UpdatedColumns.exist('/root/KR_ErkeztetesiDatum')=1
         SET @Act_KR_ErkeztetesiDatum = @KR_ErkeztetesiDatum
   IF @UpdatedColumns.exist('/root/KR_FileNev')=1
         SET @Act_KR_FileNev = @KR_FileNev
   IF @UpdatedColumns.exist('/root/KR_Kezbesitettseg')=1
         SET @Act_KR_Kezbesitettseg = @KR_Kezbesitettseg
   IF @UpdatedColumns.exist('/root/KR_Idopecset')=1
         SET @Act_KR_Idopecset = @KR_Idopecset
   IF @UpdatedColumns.exist('/root/KR_Valasztitkositas')=1
         SET @Act_KR_Valasztitkositas = @KR_Valasztitkositas
   IF @UpdatedColumns.exist('/root/KR_Valaszutvonal')=1
         SET @Act_KR_Valaszutvonal = @KR_Valaszutvonal
   IF @UpdatedColumns.exist('/root/KR_Rendszeruzenet')=1
         SET @Act_KR_Rendszeruzenet = @KR_Rendszeruzenet
   IF @UpdatedColumns.exist('/root/KR_Tarterulet')=1
         SET @Act_KR_Tarterulet = @KR_Tarterulet
   IF @UpdatedColumns.exist('/root/KR_ETertiveveny')=1
         SET @Act_KR_ETertiveveny = @KR_ETertiveveny
   IF @UpdatedColumns.exist('/root/KR_Lenyomat')=1
         SET @Act_KR_Lenyomat = @KR_Lenyomat
   IF @UpdatedColumns.exist('/root/KuldKuldemeny_Id')=1
         SET @Act_KuldKuldemeny_Id = @KuldKuldemeny_Id
   IF @UpdatedColumns.exist('/root/IraIrat_Id')=1
         SET @Act_IraIrat_Id = @IraIrat_Id
   IF @UpdatedColumns.exist('/root/IratPeldany_Id')=1
         SET @Act_IratPeldany_Id = @IratPeldany_Id
   IF @UpdatedColumns.exist('/root/Cel')=1
         SET @Act_Cel = @Cel
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_eBeadvanyok
SET
     Irany = @Act_Irany,
     Allapot = @Act_Allapot,
     KuldoRendszer = @Act_KuldoRendszer,
     UzenetTipusa = @Act_UzenetTipusa,
     FeladoTipusa = @Act_FeladoTipusa,
     PartnerKapcsolatiKod = @Act_PartnerKapcsolatiKod,
     PartnerNev = @Act_PartnerNev,
     PartnerEmail = @Act_PartnerEmail,
     PartnerRovidNev = @Act_PartnerRovidNev,
     PartnerMAKKod = @Act_PartnerMAKKod,
     PartnerKRID = @Act_PartnerKRID,
     Partner_Id = @Act_Partner_Id,
     Cim_Id = @Act_Cim_Id,
     KR_HivatkozasiSzam = @Act_KR_HivatkozasiSzam,
     KR_ErkeztetesiSzam = @Act_KR_ErkeztetesiSzam,
     Contentum_HivatkozasiSzam = @Act_Contentum_HivatkozasiSzam,
     PR_HivatkozasiSzam = @Act_PR_HivatkozasiSzam,
     PR_ErkeztetesiSzam = @Act_PR_ErkeztetesiSzam,
     KR_DokTipusHivatal = @Act_KR_DokTipusHivatal,
     KR_DokTipusAzonosito = @Act_KR_DokTipusAzonosito,
     KR_DokTipusLeiras = @Act_KR_DokTipusLeiras,
     KR_Megjegyzes = @Act_KR_Megjegyzes,
     KR_ErvenyessegiDatum = @Act_KR_ErvenyessegiDatum,
     KR_ErkeztetesiDatum = @Act_KR_ErkeztetesiDatum,
     KR_FileNev = @Act_KR_FileNev,
     KR_Kezbesitettseg = @Act_KR_Kezbesitettseg,
     KR_Idopecset = @Act_KR_Idopecset,
     KR_Valasztitkositas = @Act_KR_Valasztitkositas,
     KR_Valaszutvonal = @Act_KR_Valaszutvonal,
     KR_Rendszeruzenet = @Act_KR_Rendszeruzenet,
     KR_Tarterulet = @Act_KR_Tarterulet,
     KR_ETertiveveny = @Act_KR_ETertiveveny,
     KR_Lenyomat = @Act_KR_Lenyomat,
     KuldKuldemeny_Id = @Act_KuldKuldemeny_Id,
     IraIrat_Id = @Act_IraIrat_Id,
     IratPeldany_Id = @Act_IratPeldany_Id,
     Cel = @Act_Cel,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_eBeadvanyok',@Id
					,'EREC_eBeadvanyokHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

go

/* használt errorcode-ok:
	50401:
	50402: 'Ver' oszlop értéke nem egyezik a meglévő rekordéval
   50403: érvényesség már lejárt a rekordra
	50499: rekord zárolva
*/
