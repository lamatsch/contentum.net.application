IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_eMailBoritekCsatolmanyokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_eMailBoritekCsatolmanyokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_eMailBoritekCsatolmanyokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_eMailBoritekCsatolmanyokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_eMailBoritekCsatolmanyokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_eMailBoritekCsatolmanyok.Id,
	   EREC_eMailBoritekCsatolmanyok.eMailBoritek_Id,
	   EREC_eMailBoritekCsatolmanyok.Dokumentum_Id,
	   EREC_eMailBoritekCsatolmanyok.Nev,
	   EREC_eMailBoritekCsatolmanyok.Tomoritve,
	   EREC_eMailBoritekCsatolmanyok.Ver,
	   EREC_eMailBoritekCsatolmanyok.Note,
	   EREC_eMailBoritekCsatolmanyok.Stat_id,
	   EREC_eMailBoritekCsatolmanyok.ErvKezd,
	   EREC_eMailBoritekCsatolmanyok.ErvVege,
	   EREC_eMailBoritekCsatolmanyok.Letrehozo_id,
	   EREC_eMailBoritekCsatolmanyok.LetrehozasIdo,
	   EREC_eMailBoritekCsatolmanyok.Modosito_id,
	   EREC_eMailBoritekCsatolmanyok.ModositasIdo,
	   EREC_eMailBoritekCsatolmanyok.Zarolo_id,
	   EREC_eMailBoritekCsatolmanyok.ZarolasIdo,
	   EREC_eMailBoritekCsatolmanyok.Tranz_id,
	   EREC_eMailBoritekCsatolmanyok.UIAccessLog_id
	   from 
		 EREC_eMailBoritekCsatolmanyok as EREC_eMailBoritekCsatolmanyok 
	   where
		 EREC_eMailBoritekCsatolmanyok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
