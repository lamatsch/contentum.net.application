IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_eMailBoritekCsatolmanyokGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_eMailBoritekCsatolmanyokGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_eMailBoritekCsatolmanyokGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_eMailBoritekCsatolmanyokGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_eMailBoritekCsatolmanyokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_eMailBoritekCsatolmanyok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_eMailBoritekCsatolmanyok.Id,
	   EREC_eMailBoritekCsatolmanyok.eMailBoritek_Id,
	   EREC_eMailBoritekCsatolmanyok.Dokumentum_Id,
	   EREC_eMailBoritekCsatolmanyok.Nev,
	   EREC_eMailBoritekCsatolmanyok.Tomoritve,
	   EREC_eMailBoritekCsatolmanyok.Ver,
	   EREC_eMailBoritekCsatolmanyok.Note,
	   EREC_eMailBoritekCsatolmanyok.Stat_id,
	   EREC_eMailBoritekCsatolmanyok.ErvKezd,
	   EREC_eMailBoritekCsatolmanyok.ErvVege,
	   EREC_eMailBoritekCsatolmanyok.Letrehozo_id,
	   EREC_eMailBoritekCsatolmanyok.LetrehozasIdo,
	   EREC_eMailBoritekCsatolmanyok.Modosito_id,
	   EREC_eMailBoritekCsatolmanyok.ModositasIdo,
	   EREC_eMailBoritekCsatolmanyok.Zarolo_id,
	   EREC_eMailBoritekCsatolmanyok.ZarolasIdo,
	   EREC_eMailBoritekCsatolmanyok.Tranz_id,
	   EREC_eMailBoritekCsatolmanyok.UIAccessLog_id  
   from 
     EREC_eMailBoritekCsatolmanyok as EREC_eMailBoritekCsatolmanyok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
