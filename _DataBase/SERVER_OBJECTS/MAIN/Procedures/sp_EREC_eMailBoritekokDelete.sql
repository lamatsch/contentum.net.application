IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_eMailBoritekokDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_eMailBoritekokDelete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_eMailBoritekokDelete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_eMailBoritekokDelete] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_eMailBoritekokDelete] 
  @Id uniqueidentifier,
  @ExecutorUserId uniqueidentifier,
  @ExecutionTime datetime

as
begin

BEGIN TRY
--BEGIN TRANSACTION DeleteTransaction
  
	set nocount on

-- Zarolas ellenorzese:
	DECLARE @IsLockedByOtherUser BIT
	exec sp_GetLockingInfo 'EREC_eMailBoritekok',@Id,@ExecutorUserId,@IsLockedByOtherUser OUTPUT,null

	if (@IsLockedByOtherUser = 0)
	BEGIN	
         /* History Log */
      exec sp_LogDeleteToHistory 'EREC_eMailBoritekok',@Id
                 ,'EREC_eMailBoritekokHistory',@ExecutorUserId,@ExecutionTime   
		delete from EREC_eMailBoritekok 
		where Id = @Id

		if @@rowcount != 1
		begin
			RAISERROR('[50501]',16,1)
			return @@error
		end
	END
	ELSE BEGIN
		RAISERROR('[50599]',16,1)			
	END

--COMMIT TRANSACTION DeleteTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION DeleteTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()	
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
