IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_eMailBoritekokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_eMailBoritekokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_eMailBoritekokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_eMailBoritekokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_eMailBoritekokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_eMailBoritekok.Id,
	   EREC_eMailBoritekok.Felado,
	   EREC_eMailBoritekok.Cimzett,
	   EREC_eMailBoritekok.CC,
	   EREC_eMailBoritekok.Targy,
	   EREC_eMailBoritekok.FeladasDatuma,
	   EREC_eMailBoritekok.ErkezesDatuma,
	   EREC_eMailBoritekok.Fontossag,
	   EREC_eMailBoritekok.KuldKuldemeny_Id,
	   EREC_eMailBoritekok.IraIrat_Id,
	   EREC_eMailBoritekok.DigitalisAlairas,
	   EREC_eMailBoritekok.Uzenet,
	   EREC_eMailBoritekok.EmailForras,
	   EREC_eMailBoritekok.EmailGuid,
	   EREC_eMailBoritekok.FeldolgozasIdo,
	   EREC_eMailBoritekok.ForrasTipus,
	   EREC_eMailBoritekok.Allapot,
	   EREC_eMailBoritekok.Ver,
	   EREC_eMailBoritekok.Note,
	   EREC_eMailBoritekok.Stat_id,
	   EREC_eMailBoritekok.ErvKezd,
	   EREC_eMailBoritekok.ErvVege,
	   EREC_eMailBoritekok.Letrehozo_id,
	   EREC_eMailBoritekok.LetrehozasIdo,
	   EREC_eMailBoritekok.Modosito_id,
	   EREC_eMailBoritekok.ModositasIdo,
	   EREC_eMailBoritekok.Zarolo_id,
	   EREC_eMailBoritekok.ZarolasIdo,
	   EREC_eMailBoritekok.Tranz_id,
	   EREC_eMailBoritekok.UIAccessLog_id
	   from 
		 EREC_eMailBoritekok as EREC_eMailBoritekok 
	   where
		 EREC_eMailBoritekok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
