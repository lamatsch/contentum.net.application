IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_eMailBoritekokInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_eMailBoritekokInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_eMailBoritekokInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_eMailBoritekokInsert] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_eMailBoritekokInsert]    
                @Id      uniqueidentifier = null,    
                               @Felado     Nvarchar(4000)  = null,
                @Cimzett     Nvarchar(4000)  = null,
                @CC     Nvarchar(4000)  = null,
                @Targy     Nvarchar(4000)  = null,
                @FeladasDatuma     datetime  = null,
                @ErkezesDatuma     datetime  = null,
                @Fontossag     nvarchar(64)  = null,
                @KuldKuldemeny_Id     uniqueidentifier  = null,
                @IraIrat_Id     uniqueidentifier  = null,
                @DigitalisAlairas     Nvarchar(4000)  = null,
                @Uzenet     Nvarchar(Max)  = null,
                @EmailForras     Nvarchar(Max)  = null,
                @EmailGuid     Nvarchar(4000)  = null,
                @FeldolgozasIdo     datetime  = null,
                @ForrasTipus     nvarchar(64)  = null,
                @Allapot     nvarchar(64)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Felado is not null
         begin
            SET @insertColumns = @insertColumns + ',Felado'
            SET @insertValues = @insertValues + ',@Felado'
         end 
       
         if @Cimzett is not null
         begin
            SET @insertColumns = @insertColumns + ',Cimzett'
            SET @insertValues = @insertValues + ',@Cimzett'
         end 
       
         if @CC is not null
         begin
            SET @insertColumns = @insertColumns + ',CC'
            SET @insertValues = @insertValues + ',@CC'
         end 
       
         if @Targy is not null
         begin
            SET @insertColumns = @insertColumns + ',Targy'
            SET @insertValues = @insertValues + ',@Targy'
         end 
       
         if @FeladasDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',FeladasDatuma'
            SET @insertValues = @insertValues + ',@FeladasDatuma'
         end 
       
         if @ErkezesDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',ErkezesDatuma'
            SET @insertValues = @insertValues + ',@ErkezesDatuma'
         end 
       
         if @Fontossag is not null
         begin
            SET @insertColumns = @insertColumns + ',Fontossag'
            SET @insertValues = @insertValues + ',@Fontossag'
         end 
       
         if @KuldKuldemeny_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',KuldKuldemeny_Id'
            SET @insertValues = @insertValues + ',@KuldKuldemeny_Id'
         end 
       
         if @IraIrat_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IraIrat_Id'
            SET @insertValues = @insertValues + ',@IraIrat_Id'
         end 
       
         if @DigitalisAlairas is not null
         begin
            SET @insertColumns = @insertColumns + ',DigitalisAlairas'
            SET @insertValues = @insertValues + ',@DigitalisAlairas'
         end 
       
         if @Uzenet is not null
         begin
            SET @insertColumns = @insertColumns + ',Uzenet'
            SET @insertValues = @insertValues + ',@Uzenet'
         end 
       
         if @EmailForras is not null
         begin
            SET @insertColumns = @insertColumns + ',EmailForras'
            SET @insertValues = @insertValues + ',@EmailForras'
         end 
       
         if @EmailGuid is not null
         begin
            SET @insertColumns = @insertColumns + ',EmailGuid'
            SET @insertValues = @insertValues + ',@EmailGuid'
         end 
       
         if @FeldolgozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',FeldolgozasIdo'
            SET @insertValues = @insertValues + ',@FeldolgozasIdo'
         end 
       
         if @ForrasTipus is not null
         begin
            SET @insertColumns = @insertColumns + ',ForrasTipus'
            SET @insertValues = @insertValues + ',@ForrasTipus'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_eMailBoritekok ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Felado Nvarchar(4000),@Cimzett Nvarchar(4000),@CC Nvarchar(4000),@Targy Nvarchar(4000),@FeladasDatuma datetime,@ErkezesDatuma datetime,@Fontossag nvarchar(64),@KuldKuldemeny_Id uniqueidentifier,@IraIrat_Id uniqueidentifier,@DigitalisAlairas Nvarchar(4000),@Uzenet Nvarchar(Max),@EmailForras Nvarchar(Max),@EmailGuid Nvarchar(4000),@FeldolgozasIdo datetime,@ForrasTipus nvarchar(64),@Allapot nvarchar(64),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Felado = @Felado,@Cimzett = @Cimzett,@CC = @CC,@Targy = @Targy,@FeladasDatuma = @FeladasDatuma,@ErkezesDatuma = @ErkezesDatuma,@Fontossag = @Fontossag,@KuldKuldemeny_Id = @KuldKuldemeny_Id,@IraIrat_Id = @IraIrat_Id,@DigitalisAlairas = @DigitalisAlairas,@Uzenet = @Uzenet,@EmailForras = @EmailForras,@EmailGuid = @EmailGuid,@FeldolgozasIdo = @FeldolgozasIdo,@ForrasTipus = @ForrasTipus,@Allapot = @Allapot,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_eMailBoritekok',@ResultUid
					,'EREC_eMailBoritekokHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
