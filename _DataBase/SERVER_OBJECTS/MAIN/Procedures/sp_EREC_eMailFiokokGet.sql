IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_eMailFiokokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_eMailFiokokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_eMailFiokokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_eMailFiokokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_eMailFiokokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_eMailFiokok.Id,
	   EREC_eMailFiokok.Nev,
	   EREC_eMailFiokok.UserNev,
	   EREC_eMailFiokok.Csoportok_Id,
	   EREC_eMailFiokok.Jelszo,
	   EREC_eMailFiokok.Tipus,
	   EREC_eMailFiokok.NapiMax,
	   EREC_eMailFiokok.NapiTeny,
	   EREC_eMailFiokok.EmailCim,
	   EREC_eMailFiokok.Modul_Id,
	   EREC_eMailFiokok.MappaNev,
	   EREC_eMailFiokok.Ver,
	   EREC_eMailFiokok.Note,
	   EREC_eMailFiokok.Stat_id,
	   EREC_eMailFiokok.ErvKezd,
	   EREC_eMailFiokok.ErvVege,
	   EREC_eMailFiokok.Letrehozo_id,
	   EREC_eMailFiokok.LetrehozasIdo,
	   EREC_eMailFiokok.Modosito_id,
	   EREC_eMailFiokok.ModositasIdo,
	   EREC_eMailFiokok.Zarolo_id,
	   EREC_eMailFiokok.ZarolasIdo,
	   EREC_eMailFiokok.Tranz_id,
	   EREC_eMailFiokok.UIAccessLog_id
	   from 
		 EREC_eMailFiokok as EREC_eMailFiokok 
	   where
		 EREC_eMailFiokok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
