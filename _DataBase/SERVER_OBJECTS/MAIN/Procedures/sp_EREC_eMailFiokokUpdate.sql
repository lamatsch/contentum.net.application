IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_eMailFiokokUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_eMailFiokokUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_eMailFiokokUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_EREC_eMailFiokokUpdate] AS' 
END
GO
ALTER procedure [dbo].[sp_EREC_eMailFiokokUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Nev     Nvarchar(100)  = null,         
             @UserNev     Nvarchar(100)  = null,         
             @Csoportok_Id     uniqueidentifier  = null,         
             @Jelszo     Nvarchar(20)  = null,         
             @Tipus     char(1)  = null,         
             @NapiMax     int  = null,         
             @NapiTeny     int  = null,         
             @EmailCim     Nvarchar(100)  = null,         
             @Modul_Id     uniqueidentifier  = null,         
             @MappaNev     Nvarchar(100)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Nev     Nvarchar(100)         
     DECLARE @Act_UserNev     Nvarchar(100)         
     DECLARE @Act_Csoportok_Id     uniqueidentifier         
     DECLARE @Act_Jelszo     Nvarchar(20)         
     DECLARE @Act_Tipus     char(1)         
     DECLARE @Act_NapiMax     int         
     DECLARE @Act_NapiTeny     int         
     DECLARE @Act_EmailCim     Nvarchar(100)         
     DECLARE @Act_Modul_Id     uniqueidentifier         
     DECLARE @Act_MappaNev     Nvarchar(100)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Nev = Nev,
     @Act_UserNev = UserNev,
     @Act_Csoportok_Id = Csoportok_Id,
     @Act_Jelszo = Jelszo,
     @Act_Tipus = Tipus,
     @Act_NapiMax = NapiMax,
     @Act_NapiTeny = NapiTeny,
     @Act_EmailCim = EmailCim,
     @Act_Modul_Id = Modul_Id,
     @Act_MappaNev = MappaNev,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_eMailFiokok
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Nev')=1
         SET @Act_Nev = @Nev
   IF @UpdatedColumns.exist('/root/UserNev')=1
         SET @Act_UserNev = @UserNev
   IF @UpdatedColumns.exist('/root/Csoportok_Id')=1
         SET @Act_Csoportok_Id = @Csoportok_Id
   IF @UpdatedColumns.exist('/root/Jelszo')=1
         SET @Act_Jelszo = @Jelszo
   IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @Act_Tipus = @Tipus
   IF @UpdatedColumns.exist('/root/NapiMax')=1
         SET @Act_NapiMax = @NapiMax
   IF @UpdatedColumns.exist('/root/NapiTeny')=1
         SET @Act_NapiTeny = @NapiTeny
   IF @UpdatedColumns.exist('/root/EmailCim')=1
         SET @Act_EmailCim = @EmailCim
   IF @UpdatedColumns.exist('/root/Modul_Id')=1
         SET @Act_Modul_Id = @Modul_Id
   IF @UpdatedColumns.exist('/root/MappaNev')=1
         SET @Act_MappaNev = @MappaNev
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_eMailFiokok
SET
     Nev = @Act_Nev,
     UserNev = @Act_UserNev,
     Csoportok_Id = @Act_Csoportok_Id,
     Jelszo = @Act_Jelszo,
     Tipus = @Act_Tipus,
     NapiMax = @Act_NapiMax,
     NapiTeny = @Act_NapiTeny,
     EmailCim = @Act_EmailCim,
     Modul_Id = @Act_Modul_Id,
     MappaNev = @Act_MappaNev,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_eMailFiokok',@Id
					,'EREC_eMailFiokokHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
