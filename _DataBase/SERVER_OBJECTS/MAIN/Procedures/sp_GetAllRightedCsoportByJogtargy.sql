IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAllRightedCsoportByJogtargy]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetAllRightedCsoportByJogtargy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetAllRightedCsoportByJogtargy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_GetAllRightedCsoportByJogtargy] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_GetAllRightedCsoportByJogtargy]
 ( @JogtargyId		uniqueidentifier,
   @Where nvarchar(4000) = '',
   @OrderBy nvarchar(200) = ' order by LetrehozasIdo',
   @TopRow nvarchar(5) = '',
   @ExecutorUserId	uniqueidentifier
)
AS
BEGIN
	declare @sqlcmd	nvarchar(max);
	set @sqlcmd = N'
		WITH temp AS
		(
			SELECT CAST(''' + cast(@JogtargyId as nvarchar(40)) + ''' AS UNIQUEIDENTIFIER) AS Id

			UNION ALL

			SELECT EREC_KuldKuldemenyek.IraIktatokonyv_Id
				FROM EREC_KuldKuldemenyek
					INNER JOIN temp ON EREC_KuldKuldemenyek.Id = temp.Id

			UNION ALL

			SELECT EREC_KuldKuldemenyek.IraIratok_Id
				FROM EREC_KuldKuldemenyek
					INNER JOIN temp ON EREC_KuldKuldemenyek.Id = temp.Id

			UNION ALL

			SELECT EREC_PldIratPeldanyok.IraIrat_Id
				FROM EREC_PldIratPeldanyok
					INNER JOIN temp ON EREC_PldIratPeldanyok.Id = temp.Id

			UNION ALL

			SELECT EREC_eMailBoritekok.IraIrat_Id
				FROM EREC_eMailBoritekok
					INNER JOIN temp ON EREC_eMailBoritekok.Id = temp.Id

			UNION ALL

			SELECT EREC_IraIratok.UgyUgyIratDarab_Id
				FROM EREC_IraIratok
					INNER JOIN temp ON EREC_IraIratok.Id = temp.Id

			UNION ALL

			SELECT EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				FROM EREC_UgyUgyiratdarabok
					INNER JOIN temp ON EREC_UgyUgyiratdarabok.Id = temp.Id

			UNION ALL

			SELECT EREC_UgyUgyiratok.IraIktatokonyv_Id
				FROM EREC_UgyUgyiratok
					INNER JOIN temp ON EREC_UgyUgyiratok.Id = temp.Id
		)
		select KRT_Jogosultak.Id,
				KRT_Jogosultak.Csoport_Id_Jogalany,
				CASE KRT_Jogosultak.Jogszint
					when ''I'' then ''Írás''
					when ''O'' then ''Olvasás''
				end as Jogszint,
				KRT_Jogosultak.Kezi,
				KRT_Jogosultak.ErvKezd,
				KRT_Jogosultak.ErvVege,
				KRT_Csoportok.Nev,
				KRT_Jogosultak.LetrehozasIdo,
				case KRT_Jogosultak.Obj_Id
					when ''' + cast(@JogtargyId as nvarchar(40)) + ''' then ''N''
					else ''I''
				end as Oroklott
			FROM KRT_Jogosultak
				INNER JOIN KRT_Csoportok ON KRT_Csoportok.Id = KRT_Jogosultak.Csoport_Id_Jogalany
			WHERE KRT_Jogosultak.Kezi = ''I''
				AND KRT_Jogosultak.Obj_Id IN (SELECT Id FROM temp)';

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
	SET @sqlcmd = @sqlcmd + @OrderBy	
	print @sqlcmd;
	exec (@sqlcmd);
END


GO
