IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCsoportIdByFelhasznalo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetCsoportIdByFelhasznalo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetCsoportIdByFelhasznalo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_GetCsoportIdByFelhasznalo] AS' 
END
GO
-- =============================================
-- Author:      <Author,,Name>
-- Create date: <Create Date,,>
-- Description: <Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_GetCsoportIdByFelhasznalo]
  ( @Felhasznalo_Id  uniqueidentifier,
    @p_teszt         int = 0
  )
AS
BEGIN
begin try
  declare @v_proc varchar(272),

          @v_inscnt               numeric(10),
          @v_delcnt               numeric(10),
          @v_Szint                integer,
          @v_ErvKezd              datetime,
          @v_ErvVege              datetime,
          @v_defErvKezd           datetime,
          @v_defErvVege           datetime,

          @v_numrows                 integer,
          @v_currszint               integer

  SET NOCOUNT ON; -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
  select @v_proc = object_name(@@procid) + '@' + convert(varchar, @@spid), @v_currszint = 0,
         @v_defErvKezd = convert(datetime, '1900-01-01', 102), @v_defErvVege = convert(datetime, '4700-12-31', 102);
/**/  if @p_teszt > 0 print '==vv ' + @v_proc + ' [' + convert(varchar, getdate(), 121) + '] - '
/**/                      + 'BEGIN: p_teszt:' + convert(varchar, @p_teszt) + '!';

    select kcs.Id Csoport_Id, kcs.Id Csoport_Id_Jogalany,
           0 Szint, kcs.ErvKezd ErvKezd, kcs.ErvVege ErvVege
      into #csoport
      from KRT_Csoportok kcs
     where kcs.Id = @Felhasznalo_Id
       and getdate() between kcs.ErvKezd and kcs.ErvVege
  ;
  select @v_numrows = @@rowcount;
  if (@v_numrows != 1)
  begin --nincs ilyen azonosítójú, érvényes felhasználó
    raiserror('[50401]', 16, 1);
    return @@error;
  end

  while @v_numrows > 0 and @v_currszint < 500
  begin

    insert into #csoport
        ( Csoport_Id, Csoport_Id_Jogalany, Szint, ErvKezd, ErvVege
        )
      select ap.Csoport_Id, a.Csoport_Id_Jogalany, @v_currszint + 1,
             (select max(ek.ErvKezd) from (select isnull(a.ErvKezd, @v_defErvKezd) ErvKezd union select isnull(ap.ErvKezd, @v_defErvKezd) union select isnull(apd.ErvKezd, @v_defErvKezd)) ek),
             (select min(ev.ErvVege) from (select isnull(a.ErvVege, @v_defErvVege) ErvVege union select isnull(ap.ErvVege, @v_defErvVege) union select isnull(apd.ErvVege, @v_defErvVege)) ev)
        from #csoport a, KRT_CsoportTagok ap, KRT_Csoportok apd
       where a.Csoport_Id = ap.Csoport_Id_Jogalany
         and ap.Csoport_Id = apd.Id
         and a.Szint = @v_currszint
         and ap.Csoport_Id is not null
         and getdate() between
                 (select max(ek.ErvKezd) from (select isnull(a.ErvKezd, @v_defErvKezd) ErvKezd union select isnull(ap.ErvKezd, @v_defErvKezd) union select isnull(apd.ErvKezd, @v_defErvKezd)) ek)
             and (select min(ev.ErvVege) from (select isnull(a.ErvVege, @v_defErvVege) ErvVege union select isnull(ap.ErvVege, @v_defErvVege) union select isnull(apd.ErvVege, @v_defErvVege)) ev)
    ;
    select @v_numrows = @@rowcount;
    select @v_currszint = @v_currszint + 1;
  end

  select distinct Csoport_Id from #csoport;

  if @p_teszt > 0 print '==^^ ' + @v_proc + ' [' + convert(varchar, getdate(), 121) + '] - END - OK!';

end try
begin catch
  declare @errorSeverity integer,
          @errorState integer,
          @errorCode nvarchar(1000);
  select @errorSeverity = ERROR_SEVERITY(), @errorState = ERROR_STATE();

  if ERROR_NUMBER() < 50000
    set @errorCode = '[' + convert(nvarchar(10), ERROR_NUMBER()) + '] ' + '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();
  else
    set @errorCode = '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();

  if @p_teszt > 0 print '==^^ ' + @v_proc + ' [' + convert(varchar, getdate(), 121) + '] - END - ERROR! [' + convert(nvarchar(10), ERROR_NUMBER()) + '/' + convert(nvarchar(10), ERROR_LINE()) + '] ' + ERROR_MESSAGE();

  if @errorState = 0 set @errorState = 1
  raiserror(@errorCode, @errorSeverity, @errorState)
end catch
END --procedure sp_GetCsoportIdByFelhasznalo


GO
