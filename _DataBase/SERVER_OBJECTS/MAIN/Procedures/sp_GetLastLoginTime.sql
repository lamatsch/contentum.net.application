IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLastLoginTime]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLastLoginTime]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLastLoginTime]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_GetLastLoginTime] AS' 
END
GO
ALTER procedure [dbo].[sp_GetLastLoginTime]
  @FelhasznaloId				uniqueidentifier
as

begin

BEGIN TRY

   set nocount on
 
	 
	SELECT MAX([KRT_Log_Login].[Date]) AS LastLoginTime
	FROM [KRT_Log_Login]
	WHERE [KRT_Log_Login].[Felhasznalo_Id] = @FelhasznaloId


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
