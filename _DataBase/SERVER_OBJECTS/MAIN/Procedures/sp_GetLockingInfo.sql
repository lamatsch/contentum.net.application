IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLockingInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetLockingInfo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetLockingInfo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_GetLockingInfo] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_GetLockingInfo] 
		 @TableName NVARCHAR(100)
		,@Id uniqueidentifier
		,@ExecutorUserId uniqueidentifier
		,@IsLockedByOtherUser BIT OUTPUT
		,@Zarolo_id uniqueidentifier OUTPUT
AS
BEGIN
	DECLARE @selectCommand NVARCHAR(1000)
	SET @selectCommand = 
		'select 	    				
			 @Zarolo_id = Zarolo_id
		from '+@TableName+
		' where Id = @Id'
	
	exec sp_executesql @selectCommand, 
						 N'@Zarolo_id uniqueidentifier OUTPUT,@Id uniqueidentifier'
						,@Zarolo_id = @Zarolo_id OUTPUT,@Id = @Id
	--print @Zarolo_id

	if (@Zarolo_id is not null)
	begin
		if (@ExecutorUserId is null or @Zarolo_id != @ExecutorUserId)
		BEGIN
			SET @IsLockedByOtherUser = 1
		END
		else
		BEGIN
			SET @IsLockedByOtherUser = 0
		END	
	end
	else begin
		SET @IsLockedByOtherUser = 0
	end
END


GO
