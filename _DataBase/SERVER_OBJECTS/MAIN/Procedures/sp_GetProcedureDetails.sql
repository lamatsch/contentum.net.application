IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetProcedureDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetProcedureDetails]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetProcedureDetails]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_GetProcedureDetails] AS' 
END
GO
ALTER  Procedure [dbo].[sp_GetProcedureDetails]  
@ProcedureName NVARCHAR(50)
AS 

SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;

BEGIN TRY
--IF @@trancount = 0 BEGIN BEGIN TRAN END
select p.name,t.name as ctype, CASE t.name WHEN 'nvarchar' THEN p.max_length/2 ELSE p.max_length END as length from SYS.Parameters p, SYS.TYPES t where 
p.System_Type_ID = t.user_type_id
and p.is_output = 0
and OBJECT_ID= OBJECT_ID(@ProcedureName) 
--IF @@trancount > 0 BEGIN Commit Tran END

END TRY
BEGIN CATCH
--IF @@trancount > 0 BEGIN Rollback Tran END
raiserror(999999,1,1)
END CATCH

--[sp_GetProcedureDetails] 'xyfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff'


GO
