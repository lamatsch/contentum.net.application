IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetSzignaltCsoport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetSzignaltCsoport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetSzignaltCsoport]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_GetSzignaltCsoport] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_GetSzignaltCsoport]
 ( @JogtargyId		uniqueidentifier,
   @CsoportId		uniqueidentifier,
   @ExecutorUserId	uniqueidentifier
)
AS
BEGIN
BEGIN TRY
	if not exists (select 1 from dbo.fn_GetAllSubCsoportByCsoportId(@ExecutorUserId) where Id = @CsoportId)
		raiserror('[52153]',16,1); 
	
	if exists (select 1 from KRT_Csoportok where Id = @CsoportId and Tipus = '0')
	BEGIN
		select KRT_Csoportok.*
			from KRT_Csoporttagok 
				inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_Csoporttagok.Csoport_Id_Jogalany
			where KRT_Csoporttagok.Csoport_Id = @CsoportId and KRT_Csoporttagok.Tipus = '3'
	END		
	else
		select KRT_Csoportok.* from KRT_Csoportok where Id = @CsoportId;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
END CATCH
END


GO
