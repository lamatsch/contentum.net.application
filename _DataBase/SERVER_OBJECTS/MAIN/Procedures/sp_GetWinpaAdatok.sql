IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetWinpaAdatok]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GetWinpaAdatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GetWinpaAdatok]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_GetWinpaAdatok] AS' 
END
GO
ALTER procedure [dbo].[sp_GetWinpaAdatok]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_KuldKuldemenyek.LetrehozasIdo',
  @ExecutorUserId				uniqueidentifier
as

begin
BEGIN TRY


set nocount on

   DECLARE @sqlcmd nvarchar(MAX)
   SET @sqlcmd = ''
   DECLARE @LocalTopRow nvarchar(10)

	/************************************************************
	* Tényleges select											*
	************************************************************/
     
          
 SET @sqlcmd = @sqlcmd + 
  ' with kuldemeny_peldanyok_work as
	(
		select EREC_KuldKuldemenyek.Id as KuldKuldemeny_Id, 
			    (
					select replace(EREC_PldIratpeldanyok.Azonosito, '' '','''') + '',''
					From EREC_Kuldemeny_IratPeldanyai EREC_Kuldemeny_IratPeldanyai
					join EREC_PldIratpeldanyok EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.id = EREC_Kuldemeny_IratPeldanyai.Peldany_Id
					where getdate() between EREC_Kuldemeny_IratPeldanyai.ErvKezd and EREC_Kuldemeny_IratPeldanyai.ErvVege
					and getdate() between EREC_PldIratpeldanyok.ErvKezd and EREC_PldIratpeldanyok.ErvVege
					and EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
					ORDER BY EREC_PldIratpeldanyok.Azonosito
					For XML PATH ('''')
			    ) as Azonosito
		from EREC_KuldKuldemenyek EREC_KuldKuldemenyek
	),
	kuldemeny_peldanyok as
	(
		select KuldKuldemeny_Id, left(left(Azonosito, len(Azonosito) -1), 100) as Azonosito
		from kuldemeny_peldanyok_work
	)
    select 
	EREC_KuldKuldemenyek.Id as Id,
	kuldemeny_peldanyok.Azonosito IKTATOSZAM
  	,ROW_NUMBER() over (ORDER BY EREC_KuldKuldemenyek.id) OBJ_ID
	,EREC_KuldKuldemenyek.RagSzam RAGSZAM
    ,left(EREC_KuldKuldemenyek.NevSTR_Bekuldo, 100) NEV
    , KRT_Cimek.TelepulesNev TELEPULES
	, KRT_Cimek.IRSZ IRSZ
    -- BLG_984
	--, case KRT_Cimek.tipus 
	--when ''02'' then ''Pf. '' + KRT_Cimek.Hazszam
	--	else KRT_Cimek.KozteruletNev + '' '' + KRT_Cimek.KozteruletTipusNev + '' '' + KRT_Cimek.Hazszam
 --   end CIM
	--, Cast(CASE WHEN KRT_Cimek.HRSZ is NULL OR KRT_Cimek.HRSZ='' THEN '' ELSE 
	--	   CASE WHEN KRT_Cimek.TelepulesNev is NULL OR KRT_Cimek.TelepulesNev = '''' THEN '''' ELSE KRT_Cimek.TelepulesNev + '' HRSZ.'' + KRT_Cimek.HRSZ END 
	--	   END as varchar(100)) CIM 
	, Cast(CASE WHEN KRT_Cimek.KozteruletNev IS NULL OR KRT_Cimek.KozteruletNev='''' THEN 
			CASE WHEN KRT_Cimek.HRSZ is NULL OR KRT_Cimek.HRSZ='''' THEN '''' ELSE 
				CASE WHEN KRT_Cimek.TelepulesNev is NULL OR KRT_Cimek.TelepulesNev = '''' THEN '''' ELSE KRT_Cimek.TelepulesNev + '' HRSZ.'' + KRT_Cimek.HRSZ END 
		    END 
		   ELSE KRT_Cimek.KozteruletNev END	as varchar(45)) CIM
	, Cast(KRT_Cimek.KozteruletTipusNev as varchar(200)) KOZTERULET_JELLEG
	, Cast(CASE WHEN KRT_Cimek.Tipus = ''02'' THEN NULL ELSE KRT_Cimek.Hazszam + CASE WHEN  KRT_Cimek.Hazszamig is NULL OR  KRT_Cimek.Hazszamig = '''' THEN '''' ELSE '' - '' +  KRT_Cimek.Hazszamig END END as varchar(200)) HAZSZAM
	, Cast(KRT_Cimek.HazszamBetujel as varchar(200)) EPULET
	, Cast(KRT_Cimek.Lepcsohaz as varchar(200)) LEPCSOHAZ
	, Cast(KRT_Cimek.Szint as varchar(200)) EMELET
	, Cast(KRT_Cimek.Ajto + CASE WHEN KRT_Cimek.AjtoBetujel is NULL OR KRT_Cimek.AjtoBetujel = '''' THEN '''' ELSE '' / '' + KRT_Cimek.AjtoBetujel END as varchar(200)) AJTO
	, CAST(CASE WHEN KRT_Cimek.Tipus = ''02'' THEN KRT_Cimek.Hazszam ELSE NULL END as varchar(200)) POSTAFIOK
	--  
    , EREC_KuldKuldemenyek.KimenoKuldemenyFajta
    , EREC_KuldKuldemenyek.Elsobbsegi Elsobbsegi
	, EREC_KuldKuldemenyek.Ajanlott Ajanlott
	, EREC_KuldKuldemenyek.Tertiveveny Tertiveveny
	, EREC_KuldKuldemenyek.SajatKezbe SajatKezbe
	, EREC_KuldKuldemenyek.E_ertesites E_ertesites
	, EREC_KuldKuldemenyek.E_elorejelzes E_elorejelzes 
	, EREC_KuldKuldemenyek.PostaiLezaroSzolgalat PostaiLezaroSzolgalat
    , KRT_Orszagok.Viszonylatkod RENDELTETESI_HELY
	,EREC_KuldKuldemenyek.Tipus KULDEMENY_TIPUS
	, KRT_Orszagok.Nev ORSZAG_NEV
	, KRT_Orszagok.Kod ORSZAG_KOD
    FROM dbo.EREC_KuldKuldemenyek EREC_KuldKuldemenyek
  left JOIN dbo.KRT_Cimek KRT_Cimek ON EREC_KuldKuldemenyek.Cim_Id = KRT_Cimek.Id
  left join dbo.KRT_Telepulesek KRT_Telepulesek ON KRT_Cimek.Telepules_Id = KRT_Telepulesek.Id
  left JOIN dbo.KRT_Orszagok KRT_Orszagok ON KRT_Telepulesek.Orszag_Id = KRT_Orszagok.Id 
  join kuldemeny_peldanyok kuldemeny_peldanyok on EREC_KuldKuldemenyek.Id = kuldemeny_peldanyok.KuldKuldemeny_Id
  '
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
	print @sqlcmd
	exec (@sqlcmd);
	
	-- találatok száma és oldalszám

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end




GO
