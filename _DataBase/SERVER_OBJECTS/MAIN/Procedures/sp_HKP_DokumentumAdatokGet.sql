IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_HKP_DokumentumAdatokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_HKP_DokumentumAdatokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_HKP_DokumentumAdatokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_HKP_DokumentumAdatokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_HKP_DokumentumAdatokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   HKP_DokumentumAdatok.Id,
	   HKP_DokumentumAdatok.Irany,
	   HKP_DokumentumAdatok.Allapot,
	   HKP_DokumentumAdatok.FeladoTipusa,
	   HKP_DokumentumAdatok.KapcsolatiKod,
	   HKP_DokumentumAdatok.Nev,
	   HKP_DokumentumAdatok.Email,
	   HKP_DokumentumAdatok.RovidNev,
	   HKP_DokumentumAdatok.MAKKod,
	   HKP_DokumentumAdatok.KRID,
	   HKP_DokumentumAdatok.ErkeztetesiSzam,
	   HKP_DokumentumAdatok.HivatkozasiSzam,
	   HKP_DokumentumAdatok.DokTipusHivatal,
	   HKP_DokumentumAdatok.DokTipusAzonosito,
	   HKP_DokumentumAdatok.DokTipusLeiras,
	   HKP_DokumentumAdatok.Megjegyzes,
	   HKP_DokumentumAdatok.FileNev,
	   HKP_DokumentumAdatok.ErvenyessegiDatum,
	   HKP_DokumentumAdatok.ErkeztetesiDatum,
	   HKP_DokumentumAdatok.Kezbesitettseg,
	   HKP_DokumentumAdatok.Idopecset,
	   HKP_DokumentumAdatok.ValaszTitkositas,
	   HKP_DokumentumAdatok.ValaszUtvonal,
	   HKP_DokumentumAdatok.Rendszeruzenet,
	   HKP_DokumentumAdatok.Tarterulet,
	   HKP_DokumentumAdatok.ETertiveveny,
	   HKP_DokumentumAdatok.Lenyomat,
	   HKP_DokumentumAdatok.Dokumentum_Id,
	   HKP_DokumentumAdatok.KuldKuldemeny_Id,
	   HKP_DokumentumAdatok.IraIrat_Id,
	   HKP_DokumentumAdatok.IratPeldany_Id,
	   HKP_DokumentumAdatok.Ver,
	   HKP_DokumentumAdatok.Note,
	   HKP_DokumentumAdatok.Stat_id,
	   HKP_DokumentumAdatok.ErvKezd,
	   HKP_DokumentumAdatok.ErvVege,
	   HKP_DokumentumAdatok.Letrehozo_id,
	   HKP_DokumentumAdatok.LetrehozasIdo,
	   HKP_DokumentumAdatok.Modosito_id,
	   HKP_DokumentumAdatok.ModositasIdo,
	   HKP_DokumentumAdatok.Zarolo_id,
	   HKP_DokumentumAdatok.ZarolasIdo,
	   HKP_DokumentumAdatok.Tranz_id,
	   HKP_DokumentumAdatok.UIAccessLog_id
	   from 
		 HKP_DokumentumAdatok as HKP_DokumentumAdatok 
	   where
		 HKP_DokumentumAdatok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
