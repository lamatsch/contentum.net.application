IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_HKP_DokumentumAdatokGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_HKP_DokumentumAdatokGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_HKP_DokumentumAdatokGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_HKP_DokumentumAdatokGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_HKP_DokumentumAdatokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   HKP_DokumentumAdatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @pageNumber		int = 0,
  @pageSize		int = -1,
  @SelectedRowId	uniqueidentifier = null,
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 	DECLARE @firstRow INT
	DECLARE @lastRow INT
   
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow != '' and @TopRow != '0')
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end
 
	/************************************************************
	* SzL±rt adatokhoz rendezA©s A©s sorszA?m A¶sszeA?llA­tA?sa			*
	************************************************************/
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   HKP_DokumentumAdatok.Id into #result
		from HKP_DokumentumAdatok as HKP_DokumentumAdatok
 '
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end

	if (@SelectedRowId is not null)
	begin
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id = @SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	end

	set @sqlcmd = @sqlcmd + N' 
	if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
	BEGIN
		select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
		set @firstRow = (@pageNumber - 1) * @pageSize + 1;
		set @lastRow = @pageNumber*@pageSize;
		select @pageNumber = @pageNumber - 1
	END ;'

	/************************************************************
	* TA©nyleges select											*
	************************************************************/
	SET @sqlcmd = @sqlcmd + 
  'select 
		#result.RowNumber,
  	   HKP_DokumentumAdatok.Id,
	   HKP_DokumentumAdatok.Irany,
	   HKP_DokumentumAdatok.Allapot,
	   HKP_DokumentumAdatok.FeladoTipusa,
	   HKP_DokumentumAdatok.KapcsolatiKod,
	   HKP_DokumentumAdatok.Nev,
	   HKP_DokumentumAdatok.Email,
	   HKP_DokumentumAdatok.RovidNev,
	   HKP_DokumentumAdatok.MAKKod,
	   HKP_DokumentumAdatok.KRID,
	   HKP_DokumentumAdatok.ErkeztetesiSzam,
	   HKP_DokumentumAdatok.HivatkozasiSzam,
	   HKP_DokumentumAdatok.DokTipusHivatal,
	   HKP_DokumentumAdatok.DokTipusAzonosito,
	   HKP_DokumentumAdatok.DokTipusLeiras,
	   HKP_DokumentumAdatok.Megjegyzes,
	   HKP_DokumentumAdatok.FileNev,
	   HKP_DokumentumAdatok.ErvenyessegiDatum,
	   HKP_DokumentumAdatok.ErkeztetesiDatum,
	   HKP_DokumentumAdatok.Kezbesitettseg,
	   HKP_DokumentumAdatok.Idopecset,
	   HKP_DokumentumAdatok.ValaszTitkositas,
	   HKP_DokumentumAdatok.ValaszUtvonal,
	   HKP_DokumentumAdatok.Rendszeruzenet,
	   HKP_DokumentumAdatok.Tarterulet,
	   HKP_DokumentumAdatok.ETertiveveny,
	   HKP_DokumentumAdatok.Lenyomat,
	   HKP_DokumentumAdatok.Dokumentum_Id,
	   HKP_DokumentumAdatok.KuldKuldemeny_Id,
	   HKP_DokumentumAdatok.IraIrat_Id,
	   HKP_DokumentumAdatok.IratPeldany_Id,
	   HKP_DokumentumAdatok.Ver,
	   HKP_DokumentumAdatok.Note,
	   HKP_DokumentumAdatok.Stat_id,
	   HKP_DokumentumAdatok.ErvKezd,
	   HKP_DokumentumAdatok.ErvVege,
	   HKP_DokumentumAdatok.Letrehozo_id,
	   HKP_DokumentumAdatok.LetrehozasIdo,
	   HKP_DokumentumAdatok.Modosito_id,
	   HKP_DokumentumAdatok.ModositasIdo,
	   HKP_DokumentumAdatok.Zarolo_id,
	   HKP_DokumentumAdatok.ZarolasIdo,
	   HKP_DokumentumAdatok.Tranz_id,
	   HKP_DokumentumAdatok.UIAccessLog_id  
   from 
     HKP_DokumentumAdatok as HKP_DokumentumAdatok      
     	inner join #result on #result.Id = HKP_DokumentumAdatok.Id
'
	if (@firstRow is not null and @lastRow is not null)
	BEGIN
		set @sqlcmd = @sqlcmd + '	where RowNumber between @firstRow and @lastRow
'
	END

	set @sqlcmd = @sqlcmd + '	ORDER BY #result.RowNumber;'

	-- talA?latok szA?ma A©s oldalszA?m
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';
 	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId;
  
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
