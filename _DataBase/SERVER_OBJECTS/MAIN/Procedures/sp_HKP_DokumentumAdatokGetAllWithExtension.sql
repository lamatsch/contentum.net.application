IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_HKP_DokumentumAdatokGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_HKP_DokumentumAdatokGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_HKP_DokumentumAdatokGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_HKP_DokumentumAdatokGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_HKP_DokumentumAdatokGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   HKP_DokumentumAdatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @pageNumber		int = 0,
  @pageSize		int = -1,
  @SelectedRowId	uniqueidentifier = null,
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
	
	
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 	DECLARE @firstRow INT
	DECLARE @lastRow INT
   
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow != '' and @TopRow != '0')
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end
 
	/************************************************************
	* SzL±rt adatokhoz rendezA©s A©s sorszA?m A¶sszeA?llA­tA?sa			*
	************************************************************/
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   HKP_DokumentumAdatok.Id into #result
		from HKP_DokumentumAdatok as HKP_DokumentumAdatok
 '
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end

	if (@SelectedRowId is not null)
	begin
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id = @SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	end

	set @sqlcmd = @sqlcmd + N' 
	if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
	BEGIN
		select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
		set @firstRow = (@pageNumber - 1) * @pageSize + 1;
		set @lastRow = @pageNumber*@pageSize;
		select @pageNumber = @pageNumber - 1
	END ;'

	/************************************************************
	* TA©nyleges select											*
	************************************************************/
	SET @sqlcmd = @sqlcmd + 
  'select 
		#result.RowNumber,
  	   HKP_DokumentumAdatok.Id,
	   HKP_DokumentumAdatok.Irany,
	   HKP_DokumentumAdatok.Allapot,
	   ktAllapot.Nev as AllapotNev,
	   HKP_DokumentumAdatok.FeladoTipusa,
	   ktFeladoTipusa.Nev as FeladoTipusaNev,
	   HKP_DokumentumAdatok.KapcsolatiKod,
	   HKP_DokumentumAdatok.Nev,
	   HKP_DokumentumAdatok.Email,
	   HKP_DokumentumAdatok.RovidNev,
	   HKP_DokumentumAdatok.MAKKod,
	   HKP_DokumentumAdatok.KRID,
	   HKP_DokumentumAdatok.ErkeztetesiSzam,
	   HKP_DokumentumAdatok.HivatkozasiSzam,
	   HKP_DokumentumAdatok.DokTipusHivatal,
	   HKP_DokumentumAdatok.DokTipusAzonosito,
	   HKP_DokumentumAdatok.DokTipusLeiras,
	   HKP_DokumentumAdatok.Megjegyzes,
	   HKP_DokumentumAdatok.FileNev,
	   HKP_DokumentumAdatok.ErvenyessegiDatum,
	   HKP_DokumentumAdatok.ErkeztetesiDatum,
	   HKP_DokumentumAdatok.Kezbesitettseg,
	   ktKezbesitettseg.Nev as KezbesitettsegNev,
	   HKP_DokumentumAdatok.Idopecset,
	   HKP_DokumentumAdatok.ValaszTitkositas,
	   HKP_DokumentumAdatok.ValaszUtvonal,
	   ktValaszUtvonal.Nev as ValaszUtvonalNev,
	   HKP_DokumentumAdatok.Rendszeruzenet,
	   HKP_DokumentumAdatok.Tarterulet,
	   ktTarterulet.Nev as TarteruletNev,
	   HKP_DokumentumAdatok.ETertiveveny,
	   HKP_DokumentumAdatok.Lenyomat,
	   HKP_DokumentumAdatok.Dokumentum_Id,
	   KRT_Dokumentumok.External_Link,
	   HKP_DokumentumAdatok.KuldKuldemeny_Id,
	   (SELECT Azonosito
		 FROM EREC_KuldKuldemenyek
		 WHERE EREC_KuldKuldemenyek.Id = HKP_DokumentumAdatok.KuldKuldemeny_Id) as ErkeztetoSzam,
	   HKP_DokumentumAdatok.IraIrat_Id,
	   IratIktatoszamEsId = case 
		when coalesce(EREC_IraIratok_Irat.Id, EREC_IraIratok_Kuld.Id) is null then ''''
		else coalesce(EREC_IraIratok_Irat.Azonosito, EREC_IraIratok_Kuld.Azonosito) + ''***'' + convert(NVARCHAR(40), coalesce(EREC_IraIratok_Irat.Id, EREC_IraIratok_Kuld.Id)) end,
		HKP_DokumentumAdatok.IraIrat_Id,
	   HKP_DokumentumAdatok.IratPeldany_Id,
	   EREC_PldIratPeldanyok.Azonosito as IratPeldany_Azonosito,
	   HKP_DokumentumAdatok.Ver,
	   HKP_DokumentumAdatok.Note,
	   HKP_DokumentumAdatok.Stat_id,
	   HKP_DokumentumAdatok.ErvKezd,
	   HKP_DokumentumAdatok.ErvVege,
	   HKP_DokumentumAdatok.Letrehozo_id,
	   HKP_DokumentumAdatok.LetrehozasIdo,
	   HKP_DokumentumAdatok.Modosito_id,
	   HKP_DokumentumAdatok.ModositasIdo,
	   HKP_DokumentumAdatok.Zarolo_id,
	   HKP_DokumentumAdatok.ZarolasIdo,
	   HKP_DokumentumAdatok.Tranz_id,
	   HKP_DokumentumAdatok.UIAccessLog_id  
   from'
   set @sqlcmd = @sqlcmd + ' 
     HKP_DokumentumAdatok as HKP_DokumentumAdatok      
     	inner join #result on #result.Id = HKP_DokumentumAdatok.Id
    LEFT JOIN (select Id, KuldKuldemenyek_Id, Azonosito from EREC_IraIratok where EREC_IraIratok.Allapot not in (''06'', ''08'')) as EREC_IraIratok_Irat
		on HKP_DokumentumAdatok.IraIrat_Id is not null and HKP_DokumentumAdatok.IraIrat_Id=EREC_IraIratok_Irat.Id
    LEFT JOIN (select Id, KuldKuldemenyek_Id, Azonosito from EREC_IraIratok where EREC_IraIratok.Allapot not in (''06'', ''08'')) as EREC_IraIratok_Kuld
		on HKP_DokumentumAdatok.IraIrat_Id is null and HKP_DokumentumAdatok.KuldKuldemeny_Id is not null
		and EREC_IraIratok_Kuld.KuldKuldemenyek_Id=HKP_DokumentumAdatok.KuldKuldemeny_Id
		LEFT JOIN KRT_Dokumentumok on HKP_DokumentumAdatok.Dokumentum_Id = KRT_Dokumentumok.Id
	LEFT JOIN EREC_PldIratPeldanyok on HKP_DokumentumAdatok.IratPeldany_Id = EREC_PldIratPeldanyok.Id
	LEFT JOIN KRT_KodCsoportok as kcsFeladoTipusa on kcsFeladoTipusa.Kod=''HKP_FELADO_TIPUSA''
    LEFT JOIN KRT_KodTarak as ktFeladoTipusa on HKP_DokumentumAdatok.FeladoTipusa = ktFeladoTipusa.Kod and kcsFeladoTipusa.Id = ktFeladoTipusa.KodCsoport_Id and ktFeladoTipusa.Org=@Org
    LEFT JOIN KRT_KodCsoportok as kcsKezbesitettseg on kcsKezbesitettseg.Kod=''HKP_KEZBESITETTSEG''
	LEFT JOIN KRT_KodTarak as ktKezbesitettseg on HKP_DokumentumAdatok.Kezbesitettseg = ktKezbesitettseg.Kod and kcsKezbesitettseg.Id = ktKezbesitettseg.KodCsoport_Id and ktKezbesitettseg.Org=@Org
	LEFT JOIN KRT_KodCsoportok as kcsValaszutvonal on kcsValaszutvonal.Kod=''HKP_VALASZUTVONAL''
	LEFT JOIN KRT_KodTarak as ktValaszutvonal on HKP_DokumentumAdatok.Valaszutvonal = ktValaszutvonal.Kod and kcsValaszutvonal.Id = ktValaszutvonal.KodCsoport_Id and ktValaszutvonal.Org=@Org
	LEFT JOIN KRT_KodCsoportok as kcsTarterulet on kcsTarterulet.Kod=''HKP_TARTERULET''
	LEFT JOIN KRT_KodTarak as ktTarterulet on HKP_DokumentumAdatok.Tarterulet = ktTarterulet.Kod and kcsTarterulet.Id = ktTarterulet.KodCsoport_Id and ktTarterulet.Org=@Org
	LEFT JOIN KRT_KodCsoportok as kcsAllapot on kcsAllapot.Kod=''HKP_ALLAPOT''
	LEFT JOIN KRT_KodTarak as ktAllapot on HKP_DokumentumAdatok.Allapot = ktAllapot.Kod and kcsAllapot.Id = ktAllapot.KodCsoport_Id and ktAllapot.Org=@Org
'
	if (@firstRow is not null and @lastRow is not null)
	BEGIN
		set @sqlcmd = @sqlcmd + '	where RowNumber between @firstRow and @lastRow
'
	END

	set @sqlcmd = @sqlcmd + '	ORDER BY #result.RowNumber;'

	-- talA?latok szA?ma A©s oldalszA?m
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';
 	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier, @Org uniqueidentifier',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId, @Org = @Org;
  
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
