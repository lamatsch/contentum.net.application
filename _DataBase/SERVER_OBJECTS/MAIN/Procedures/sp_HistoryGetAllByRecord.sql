IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_HistoryGetAllByRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_HistoryGetAllByRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_HistoryGetAllByRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_HistoryGetAllByRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_HistoryGetAllByRecord]
   @TableName nvarchar(200)
  ,@Record_Id uniqueidentifier

as
begin
BEGIN TRY

--    set nocount on
--
--	DECLARE @local_Record_Id nvarchar(40)
--
--	SET @local_Record_Id = CAST(@Record_Id AS nvarchar(40))
--
--	Exec 
--		('SELECT 
--		'+@TableName+'History.MasterId as MasterId,
--		'+@TableName+'History.Row_Id as Row_Id,
--		'+@TableName+'History.Muvelet as Muvelet,		
--		'+@TableName+'History.VegrehajtasIdo as VegrehajtasIdo,
--		'+@TableName+'History.Ver as MasterVer,
--		 T2.Loc.query(''/'+@TableName+'/*'')
--		FROM '+@TableName+'History
--		CROSS APPLY Record.nodes(''/'+@TableName+''') as T2(Loc)				
--		where		
--		'+@TableName+'History.row_id='''+ @local_Record_Id + '''		
--		for XML AUTO, ELEMENTS, ROOT(''Root'')')



    set nocount on
	DECLARE @HistoryTableName nvarchar(200)
	SET @HistoryTableName = @TableName + 'History'
	
    DECLARE @selectCommand NVARCHAR(1000)
	SET @selectCommand = 
		'select history.MasterId, history.Row_Id, history.Muvelet_Id, muvelet.Nev as Muvelet_Nev, history.Ver, history.Record, '+
		' history.Vegrehajto_Id, history.VegrehajtasIdo, vegrehajto.Nev as VegrehajtoNev '+  	    				
		' from '+@HistoryTableName + ' as history ' +
		' left outer join KRT_Felhasznalok as vegrehajto ' +
		' on history.Vegrehajto_Id = vegrehajto.Id ' + 
		' left outer join KRT_Muveletek as muvelet on history.Muvelet_Id = muvelet.Id ' +
		' where history.Row_Id = @Record_Id'
	
	
	SET @selectCommand = @selectCommand + ' order by VegrehajtasIdo Desc'

	--print @selectCommand

	exec sp_executesql @selectCommand, N'@Record_Id uniqueidentifier',@Record_Id = @Record_Id



END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
