IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_INT_GetLastLogEntry]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_INT_GetLastLogEntry]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_INT_GetLastLogEntry]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_INT_GetLastLogEntry] AS' 
END
GO
ALTER procedure [dbo].[sp_INT_GetLastLogEntry]
				@Modul_id   uniqueidentifier  -- INT_Modul azonosító
AS
/*
-- FELADATA:
	Adott Modul utolsó futtatásának lekérdezése
*/
---------
---------
BEGIN TRY

set nocount on;


Select TOP 1 * from INT_Log
WHERE Modul_Id = @Modul_id
ORDER BY Sync_StartDate

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------


GO
