

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_ManagedEmailAddressGet')
            and   type = 'P')
   drop procedure sp_INT_ManagedEmailAddressGet
go

create procedure sp_INT_ManagedEmailAddressGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   INT_ManagedEmailAddress.Id,
	   INT_ManagedEmailAddress.Org,
	   INT_ManagedEmailAddress.Felhasznalo_id,
	   INT_ManagedEmailAddress.Nev,
	   INT_ManagedEmailAddress.Ertek,
	   INT_ManagedEmailAddress.Karbantarthato,
	   INT_ManagedEmailAddress.Ver,
	   INT_ManagedEmailAddress.Note,
	   INT_ManagedEmailAddress.Stat_id,
	   INT_ManagedEmailAddress.ErvKezd,
	   INT_ManagedEmailAddress.ErvVege,
	   INT_ManagedEmailAddress.Letrehozo_id,
	   INT_ManagedEmailAddress.LetrehozasIdo,
	   INT_ManagedEmailAddress.Modosito_id,
	   INT_ManagedEmailAddress.ModositasIdo,
	   INT_ManagedEmailAddress.Zarolo_id,
	   INT_ManagedEmailAddress.ZarolasIdo,
	   INT_ManagedEmailAddress.Tranz_id,
	   INT_ManagedEmailAddress.UIAccessLog_id
	   from 
		 INT_ManagedEmailAddress as INT_ManagedEmailAddress 
	   where
		 INT_ManagedEmailAddress.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
