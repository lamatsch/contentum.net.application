
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_INT_ManagedEmailAddressGetAll')
            and   type = 'P')
   drop procedure sp_INT_ManagedEmailAddressGetAll
go

create procedure sp_INT_ManagedEmailAddressGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   INT_ManagedEmailAddress.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('50202',16,1)
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   INT_ManagedEmailAddress.Id,
	   INT_ManagedEmailAddress.Org,
	   INT_ManagedEmailAddress.Felhasznalo_id,
	   INT_ManagedEmailAddress.Nev,
	   INT_ManagedEmailAddress.Ertek,
	   INT_ManagedEmailAddress.Karbantarthato,
	   INT_ManagedEmailAddress.Ver,
	   INT_ManagedEmailAddress.Note,
	   INT_ManagedEmailAddress.Stat_id,
	   INT_ManagedEmailAddress.ErvKezd,
	   INT_ManagedEmailAddress.ErvVege,
	   INT_ManagedEmailAddress.Letrehozo_id,
	   INT_ManagedEmailAddress.LetrehozasIdo,
	   INT_ManagedEmailAddress.Modosito_id,
	   INT_ManagedEmailAddress.ModositasIdo,
	   INT_ManagedEmailAddress.Zarolo_id,
	   INT_ManagedEmailAddress.ZarolasIdo,
	   INT_ManagedEmailAddress.Tranz_id,
	   INT_ManagedEmailAddress.UIAccessLog_id  
   from 
     INT_ManagedEmailAddress as INT_ManagedEmailAddress      
    Where INT_ManagedEmailAddress.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go