IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_AdatkiegeszitesekGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_AdatkiegeszitesekGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_AdatkiegeszitesekGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_AdatkiegeszitesekGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_AdatkiegeszitesekGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Adatkiegeszitesek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
               

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Adatkiegeszitesek.Id,
	   KRT_Adatkiegeszitesek.ObjTip_Id,
	   KRT_Adatkiegeszitesek.Obj_Id,
	   KRT_Adatkiegeszitesek.ObjTip_Id_Kapcsolt,
	   KRT_Adatkiegeszitesek.Obj_Id_Kapcsolt,
	   KRT_Adatkiegeszitesek.ObjTip_Id_Adat,
	   KRT_Adatkiegeszitesek.Obj_Id_Adat,
	   KRT_Adatkiegeszitesek.Ver,
	   KRT_Adatkiegeszitesek.Note,
	   KRT_Adatkiegeszitesek.Stat_id,
	   KRT_Adatkiegeszitesek.ErvKezd,
	   KRT_Adatkiegeszitesek.ErvVege,
	   KRT_Adatkiegeszitesek.Letrehozo_id,
	   KRT_Adatkiegeszitesek.LetrehozasIdo,
	   KRT_Adatkiegeszitesek.Modosito_id,
	   KRT_Adatkiegeszitesek.ModositasIdo,
	   KRT_Adatkiegeszitesek.Zarolo_id,
	   KRT_Adatkiegeszitesek.ZarolasIdo,
	   KRT_Adatkiegeszitesek.Tranz_id,
	   KRT_Adatkiegeszitesek.UIAccessLog_id  
   from 
     KRT_Adatkiegeszitesek as KRT_Adatkiegeszitesek      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
