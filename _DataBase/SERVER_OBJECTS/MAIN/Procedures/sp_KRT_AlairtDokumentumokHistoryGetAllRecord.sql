IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_AlairtDokumentumokHistoryGetAllRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_AlairtDokumentumokHistoryGetAllRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_AlairtDokumentumokHistoryGetAllRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_AlairtDokumentumokHistoryGetAllRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_AlairtDokumentumokHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from KRT_AlairtDokumentumokHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasMod' as ColumnName,               cast(Old.AlairasMod as nvarchar(99)) as OldValue,
               cast(New.AlairasMod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasMod != New.AlairasMod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairtFajlnev' as ColumnName,               cast(Old.AlairtFajlnev as nvarchar(99)) as OldValue,
               cast(New.AlairtFajlnev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairtFajlnev != New.AlairtFajlnev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairtTartalomHash' as ColumnName,               cast(Old.AlairtTartalomHash as nvarchar(99)) as OldValue,
               cast(New.AlairtTartalomHash as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairtTartalomHash != New.AlairtTartalomHash 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IratAlairasSzabaly_Id' as ColumnName,               cast(Old.IratAlairasSzabaly_Id as nvarchar(99)) as OldValue,
               cast(New.IratAlairasSzabaly_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IratAlairasSzabaly_Id != New.IratAlairasSzabaly_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasRendben' as ColumnName,               cast(Old.AlairasRendben as nvarchar(99)) as OldValue,
               cast(New.AlairasRendben as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasRendben != New.AlairasRendben 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KivarasiIdoVege' as ColumnName,               cast(Old.KivarasiIdoVege as nvarchar(99)) as OldValue,
               cast(New.KivarasiIdoVege as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KivarasiIdoVege != New.KivarasiIdoVege 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasVeglegRendben' as ColumnName,               cast(Old.AlairasVeglegRendben as nvarchar(99)) as OldValue,
               cast(New.AlairasVeglegRendben as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasVeglegRendben != New.AlairasVeglegRendben 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Idopecset' as ColumnName,               cast(Old.Idopecset as nvarchar(99)) as OldValue,
               cast(New.Idopecset as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Idopecset != New.Idopecset 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Alairo' as ColumnName,               cast(Old.Csoport_Id_Alairo as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Alairo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Alairo != New.Csoport_Id_Alairo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasTulajdonos' as ColumnName,               cast(Old.AlairasTulajdonos as nvarchar(99)) as OldValue,
               cast(New.AlairasTulajdonos as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasTulajdonos != New.AlairasTulajdonos 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tanusitvany_Id' as ColumnName,               cast(Old.Tanusitvany_Id as nvarchar(99)) as OldValue,
               cast(New.Tanusitvany_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tanusitvany_Id != New.Tanusitvany_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'External_Link' as ColumnName,               cast(Old.External_Link as nvarchar(99)) as OldValue,
               cast(New.External_Link as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.External_Link != New.External_Link 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'External_Id' as ColumnName,               cast(Old.External_Id as nvarchar(99)) as OldValue,
               cast(New.External_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.External_Id != New.External_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'External_Source' as ColumnName,               cast(Old.External_Source as nvarchar(99)) as OldValue,
               cast(New.External_Source as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.External_Source != New.External_Source 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'External_Info' as ColumnName,               cast(Old.External_Info as nvarchar(99)) as OldValue,
               cast(New.External_Info as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.External_Info != New.External_Info 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               cast(Old.Allapot as nvarchar(99)) as OldValue,
               cast(New.Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_AlairtDokumentumokHistory Old
         inner join KRT_AlairtDokumentumokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Allapot != New.Allapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end


GO
