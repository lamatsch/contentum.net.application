IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_AlkalmazasokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_AlkalmazasokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_AlkalmazasokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_AlkalmazasokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_AlkalmazasokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Alkalmazasok.Id,
	   KRT_Alkalmazasok.KRT_Id,
	   KRT_Alkalmazasok.Kod,
	   KRT_Alkalmazasok.Nev,
	   KRT_Alkalmazasok.Kulso,
	   KRT_Alkalmazasok.Org,
	   KRT_Alkalmazasok.Ver,
	   KRT_Alkalmazasok.Note,
	   KRT_Alkalmazasok.Stat_id,
	   KRT_Alkalmazasok.ErvKezd,
	   KRT_Alkalmazasok.ErvVege,
	   KRT_Alkalmazasok.Letrehozo_id,
	   KRT_Alkalmazasok.LetrehozasIdo,
	   KRT_Alkalmazasok.Modosito_id,
	   KRT_Alkalmazasok.ModositasIdo,
	   KRT_Alkalmazasok.Zarolo_id,
	   KRT_Alkalmazasok.ZarolasIdo,
	   KRT_Alkalmazasok.Tranz_id,
	   KRT_Alkalmazasok.UIAccessLog_id
	   from 
		 KRT_Alkalmazasok as KRT_Alkalmazasok 
	   where
		 KRT_Alkalmazasok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
