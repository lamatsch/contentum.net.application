IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_BankszamlaszamokGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_BankszamlaszamokGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_BankszamlaszamokGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_BankszamlaszamokGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_BankszamlaszamokGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Bankszamlaszamok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  		KRT_Bankszamlaszamok.Id,
		KRT_Bankszamlaszamok.Partner_Id,
		KRT_Bankszamlaszamok.Bankszamlaszam,
		Bankszamlaszam1 = case when len(KRT_Bankszamlaszamok.Bankszamlaszam)>=8 then substring(KRT_Bankszamlaszamok.Bankszamlaszam,1,8)
		else '''' end,
		Bankszamlaszam2 = case when len(KRT_Bankszamlaszamok.Bankszamlaszam)>=16 then substring(KRT_Bankszamlaszamok.Bankszamlaszam,9,8)
		else '''' end,
		Bankszamlaszam3 = case when len(KRT_Bankszamlaszamok.Bankszamlaszam)>=24 then substring(KRT_Bankszamlaszamok.Bankszamlaszam,17,8)
		else '''' end,
		KRT_Bankszamlaszamok.Partner_Id_Bank,
		KRT_Partnerek.Nev as Partner_Nev,
		KRT_Partnerek_Bank.Nev as Partner_Nev_Bank,
		KRT_Bankszamlaszamok.Ver,
		KRT_Bankszamlaszamok.Note,
		KRT_Bankszamlaszamok.Stat_id,
		KRT_Bankszamlaszamok.ErvKezd,
		KRT_Bankszamlaszamok.ErvVege,
		KRT_Bankszamlaszamok.Letrehozo_id,
		KRT_Bankszamlaszamok.LetrehozasIdo,
		KRT_Bankszamlaszamok.Modosito_id,
		KRT_Bankszamlaszamok.ModositasIdo,
		KRT_Bankszamlaszamok.Zarolo_id,
		KRT_Bankszamlaszamok.ZarolasIdo,
		KRT_Bankszamlaszamok.Tranz_id,
		KRT_Bankszamlaszamok.UIAccessLog_id  
   from 
     KRT_Bankszamlaszamok as KRT_Bankszamlaszamok
left join KRT_Partnerek on KRT_Partnerek.Id=KRT_Bankszamlaszamok.Partner_Id
left join KRT_Partnerek KRT_Partnerek_Bank on KRT_Partnerek_Bank.Id=KRT_Bankszamlaszamok.Partner_Id_Bank       
   '

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
