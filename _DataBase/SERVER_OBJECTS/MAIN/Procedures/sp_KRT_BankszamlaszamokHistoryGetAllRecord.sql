IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_BankszamlaszamokHistoryGetAllRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_BankszamlaszamokHistoryGetAllRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_BankszamlaszamokHistoryGetAllRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_BankszamlaszamokHistoryGetAllRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_BankszamlaszamokHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from KRT_BankszamlaszamokHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_BankszamlaszamokHistory Old
         inner join KRT_BankszamlaszamokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_ParameterekAzonosito(Old.Partner_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_ParameterekAzonosito(New.Partner_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_BankszamlaszamokHistory Old
         inner join KRT_BankszamlaszamokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Partner_Id != New.Partner_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Parameterek FTOld on FTOld.Id = Old.Partner_Id --and FTOld.Ver = Old.Ver
         left join KRT_Parameterek FTNew on FTNew.Id = New.Partner_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Bankszamlaszam' as ColumnName,               cast(Old.Bankszamlaszam as nvarchar(99)) as OldValue,
               cast(New.Bankszamlaszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_BankszamlaszamokHistory Old
         inner join KRT_BankszamlaszamokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Bankszamlaszam != New.Bankszamlaszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id_Bank' as ColumnName,
/*FK*/           dbo.fn_GetKRT_ParameterekAzonosito(Old.Partner_Id_Bank) as OldValue,
/*FK*/           dbo.fn_GetKRT_ParameterekAzonosito(New.Partner_Id_Bank) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_BankszamlaszamokHistory Old
         inner join KRT_BankszamlaszamokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Partner_Id_Bank != New.Partner_Id_Bank 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Parameterek FTOld on FTOld.Id = Old.Partner_Id_Bank --and FTOld.Ver = Old.Ver
         left join KRT_Parameterek FTNew on FTNew.Id = New.Partner_Id_Bank --and FTNew.Ver = New.Ver      
)
            
end


GO
