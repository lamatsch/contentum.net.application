IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_BarkodSavokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_BarkodSavokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_BarkodSavokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_BarkodSavokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_BarkodSavokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_BarkodSavok.Id,
	   KRT_BarkodSavok.Org,
	   KRT_BarkodSavok.Csoport_Id_Felelos,
	   KRT_BarkodSavok.SavKezd,
	   KRT_BarkodSavok.SavVege,
	   KRT_BarkodSavok.SavType,
	   KRT_BarkodSavok.SavAllapot,
	   KRT_BarkodSavok.Ver,
	   KRT_BarkodSavok.Note,
	   KRT_BarkodSavok.Stat_id,
	   KRT_BarkodSavok.ErvKezd,
	   KRT_BarkodSavok.ErvVege,
	   KRT_BarkodSavok.Letrehozo_id,
	   KRT_BarkodSavok.LetrehozasIdo,
	   KRT_BarkodSavok.Modosito_id,
	   KRT_BarkodSavok.ModositasIdo,
	   KRT_BarkodSavok.Zarolo_id,
	   KRT_BarkodSavok.ZarolasIdo,
	   KRT_BarkodSavok.Tranz_id,
	   KRT_BarkodSavok.UIAccessLog_id
	   from 
		 KRT_BarkodSavok as KRT_BarkodSavok 
	   where
		 KRT_BarkodSavok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
