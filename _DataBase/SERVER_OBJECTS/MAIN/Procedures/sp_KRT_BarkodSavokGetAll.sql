IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_BarkodSavokGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_BarkodSavokGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_BarkodSavokGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_BarkodSavokGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_BarkodSavokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_BarkodSavok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_BarkodSavok.Id,
	   KRT_BarkodSavok.Org,
	   KRT_BarkodSavok.Csoport_Id_Felelos,
	   KRT_BarkodSavok.SavKezd,
	   KRT_BarkodSavok.SavVege,
	   KRT_BarkodSavok.SavType,
	   KRT_BarkodSavok.SavAllapot,
	   KRT_BarkodSavok.Ver,
	   KRT_BarkodSavok.Note,
	   KRT_BarkodSavok.Stat_id,
	   KRT_BarkodSavok.ErvKezd,
	   KRT_BarkodSavok.ErvVege,
	   KRT_BarkodSavok.Letrehozo_id,
	   KRT_BarkodSavok.LetrehozasIdo,
	   KRT_BarkodSavok.Modosito_id,
	   KRT_BarkodSavok.ModositasIdo,
	   KRT_BarkodSavok.Zarolo_id,
	   KRT_BarkodSavok.ZarolasIdo,
	   KRT_BarkodSavok.Tranz_id,
	   KRT_BarkodSavok.UIAccessLog_id  
   from 
     KRT_BarkodSavok as KRT_BarkodSavok      
    Where KRT_BarkodSavok.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
