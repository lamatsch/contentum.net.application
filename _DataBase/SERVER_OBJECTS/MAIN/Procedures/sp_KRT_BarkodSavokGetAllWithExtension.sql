IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_BarkodSavokGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_BarkodSavokGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_BarkodSavokGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_BarkodSavokGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_BarkodSavokGetAllWithExtension]
  @Where nvarchar(4000) = '',
--@OrderBy nvarchar(200) = ' order by convert(numeric(13),KRT_BarkodSavok.SavKezd)',
  @OrderBy nvarchar(200) = ' order by KRT_BarkodSavok.SavKezd',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
       KRT_BarkodSavok.Id,
       KRT_BarkodSavok.Csoport_Id_Felelos,
          KRT_Csoportok.Nev as Csoport_Felelos,
       KRT_BarkodSavok.SavKezd,
       KRT_BarkodSavok.SavVege,
       convert(numeric(12), substring(savvege, 1, 12))- convert(numeric(12), substring(savkezd, 1, 12))+1 as IgenyeltDarab,
       KRT_BarkodSavok.SavType,
       	  case KRT_BarkodSavok.SavType 
       	  when ''G'' then ''Generált'' 
       	  when ''N'' then ''Nyomdai'' 
       	  else ''Printelt'' 
       	  end SavTypeName,
       KRT_BarkodSavok.SavAllapot,
       	  case KRT_BarkodSavok.SavAllapot 
       	  when ''A'' then ''Allokált'' 
       	  when ''F'' then ''Felhasznált'' 
          when ''H'' then ''Használható'' 
       	  else ''Törölt'' 
       	  end SavAllapotName,
       KRT_BarkodSavok.Ver,
       KRT_BarkodSavok.Note,
       KRT_BarkodSavok.Stat_id,
       KRT_BarkodSavok.ErvKezd,
       KRT_BarkodSavok.ErvVege,
       KRT_BarkodSavok.Letrehozo_id,
       KRT_BarkodSavok.LetrehozasIdo,
       KRT_BarkodSavok.Modosito_id,
       KRT_BarkodSavok.ModositasIdo,
       KRT_BarkodSavok.Zarolo_id,
       KRT_BarkodSavok.ZarolasIdo,
       KRT_BarkodSavok.Tranz_id,
       KRT_BarkodSavok.UIAccessLog_id
  from dbo.KRT_BarkodSavok as KRT_BarkodSavok,
       dbo.KRT_Csoportok as KRT_Csoportok
 where KRT_BarkodSavok.Csoport_Id_Felelos = KRT_Csoportok.Id
	and KRT_BarkodSavok.Org=''' + CAST(@Org as Nvarchar(40)) + '''
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
