IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_BarkodokGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_BarkodokGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_BarkodokGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_BarkodokGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_BarkodokGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Barkodok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Barkodok.Id,
	   KRT_Barkodok.Kod,
	   KRT_Barkodok.Obj_Id,
	   KRT_Barkodok.ObjTip_Id,
	   KRT_Barkodok.Obj_type,
	   KRT_Barkodok.KodType,
	   KRT_Barkodok.Allapot,
	   Azonosito = 
		CASE KRT_Barkodok.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN 
				(SELECT	EREC_UgyUgyiratok_Ugyirat.Azonosito
				 FROM  EREC_UgyUgyiratok as EREC_UgyUgyiratok_Ugyirat					   
				 WHERE EREC_UgyUgyiratok_Ugyirat.Id = KRT_Barkodok.Obj_Id
				)	
			WHEN ''EREC_PldIratPeldanyok'' THEN 
				(SELECT	EREC_PldIratPeldanyok_IratPld.Azonosito
				 FROM EREC_PldIratPeldanyok as EREC_PldIratPeldanyok_IratPld								
				 WHERE EREC_PldIratPeldanyok_IratPld.Id = KRT_Barkodok.Obj_Id
				)
			WHEN ''EREC_KuldKuldemenyek'' THEN 
				(SELECT	EREC_KuldKuldemenyek_Kuld.Azonosito
				 FROM EREC_KuldKuldemenyek as EREC_KuldKuldemenyek_Kuld							
				 WHERE EREC_KuldKuldemenyek_Kuld.Id = KRT_Barkodok.Obj_Id
				)
			WHEN ''KRT_Mappak'' THEN
				(SELECT KRT_Mappak.Nev
				 FROM KRT_Mappak
				 WHERE KRT_Mappak.Id = KRT_Barkodok.Obj_Id 
				)'
SET @sqlcmd = @sqlcmd +	'
			WHEN ''EREC_KuldMellekletek'' THEN 
				(SELECT
                    Azonosito =
                    CASE EREC_KuldKuldemenyek_Kuld.Allapot
						WHEN ''04'' THEN 
							 (SELECT EREC_IraIratok.Azonosito
							  FROM EREC_IraIratok                          
							  WHERE EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek_Kuld.Id
							 )
						ELSE (EREC_KuldKuldemenyek_Kuld.Azonosito)			
						END
                    FROM EREC_KuldKuldemenyek as EREC_KuldKuldemenyek_Kuld		
					INNER JOIN EREC_Mellekletek as EREC_Mellekletek  ON EREC_Mellekletek.KuldKuldemeny_Id = EREC_KuldKuldemenyek_Kuld.Id					
				WHERE EREC_Mellekletek.Id = KRT_Barkodok.Obj_Id
				)
			WHEN ''EREC_IratMellekletek'' THEN 
				(SELECT	EREC_IraIratok_Irat.Azonosito
				 FROM [EREC_Mellekletek]
						JOIN [EREC_IraIratok] AS EREC_IraIratok_Irat
							ON [EREC_Mellekletek].[IraIrat_Id] = EREC_IraIratok_Irat.[Id]																				
				 WHERE [EREC_Mellekletek].[Id] = KRT_Barkodok.Obj_Id
				)
			ELSE ''''
		END,
	   '
SET @sqlcmd = @sqlcmd +
       ' 
       MellekletParentID =
			CASE KRT_Barkodok.Obj_type
            WHEN ''EREC_KuldMellekletek'' THEN 
				(SELECT
                    CASE EREC_KuldKuldemenyek_Kuld.Allapot
					WHEN ''04'' THEN 
                   (SELECT 
                          EREC_IraIratok.Id 
                          FROM EREC_IraIratok
                    WHERE EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek_Kuld.Id
                    )
					ELSE (EREC_KuldKuldemenyek_Kuld.Id)			
				    END
                    FROM EREC_KuldKuldemenyek as EREC_KuldKuldemenyek_Kuld		
					INNER JOIN EREC_Mellekletek as EREC_Mellekletek  ON EREC_Mellekletek.KuldKuldemeny_Id = EREC_KuldKuldemenyek_Kuld.Id
				WHERE EREC_Mellekletek.Id = KRT_Barkodok.Obj_Id
				)
			WHEN ''EREC_IratMellekletek'' THEN 
				(SELECT [EREC_Mellekletek].[IraIrat_Id]
                    FROM [EREC_Mellekletek]					
					WHERE [EREC_Mellekletek].[Id] = KRT_Barkodok.Obj_Id
				)
			ELSE NULL
		END,
	   MellekletTipus = 
			CASE KRT_Barkodok.Obj_type
            WHEN ''EREC_KuldMellekletek'' THEN 
				(SELECT
                    CASE EREC_KuldKuldemenyek_Kuld.Allapot
					WHEN ''04'' THEN 
                    ( ''EREC_IratMellekletek'')
					ELSE (''EREC_KuldMellekletek'')			
				    END
                    FROM EREC_KuldKuldemenyek as EREC_KuldKuldemenyek_Kuld		
					INNER JOIN EREC_Mellekletek as EREC_Mellekletek  ON EREC_Mellekletek.KuldKuldemeny_Id = EREC_KuldKuldemenyek_Kuld.Id
					WHERE EREC_Mellekletek.Id = KRT_Barkodok.Obj_Id
				)
			ELSE ''''
		END,  
	   KRT_Barkodok.Ver,
	   KRT_Barkodok.Note,
	   KRT_Barkodok.Stat_id,
	   KRT_Barkodok.ErvKezd,
	   KRT_Barkodok.ErvVege,
	   KRT_Barkodok.Letrehozo_id,
	   KRT_Barkodok.LetrehozasIdo,
	   KRT_Barkodok.Modosito_id,
	   KRT_Barkodok.ModositasIdo,
	   KRT_Barkodok.Zarolo_id,
	   KRT_Barkodok.ZarolasIdo,
	   KRT_Barkodok.Tranz_id,
	   KRT_Barkodok.UIAccessLog_id  
   from 
     KRT_Barkodok as KRT_Barkodok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
        
   
   SET @sqlcmd = @sqlcmd + @OrderBy;

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
