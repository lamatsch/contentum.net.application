IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CimekGetAllByPartner]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_CimekGetAllByPartner]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CimekGetAllByPartner]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_CimekGetAllByPartner] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_CimekGetAllByPartner]
  @Partner_Id uniqueidentifier,
  @Cim_Tipus nvarchar(100	) = '',
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_PartnerCimek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_PartnerCimek.Id,
	   KRT_PartnerCimek.Partner_id,
	   KRT_PartnerCimek.Cim_Id,
	   KRT_PartnerCimek.UtolsoHasznalatSiker,
	   KRT_PartnerCimek.UtolsoHasznalatiIdo,
	   KRT_PartnerCimek.eMailKuldes,
	   KRT_PartnerCimek.Fajta,
dbo.fn_KodtarErtekNeve(''CIM_FAJTA'', KRT_PartnerCimek.Fajta,''' + cast(@Org as NVarChar(40)) + ''') as PartnerCimFajta_Nev,
	   KRT_PartnerCimek.Ver,
	   KRT_PartnerCimek.Note,
	   KRT_PartnerCimek.Stat_id,
	   KRT_PartnerCimek.ErvKezd,
	   KRT_PartnerCimek.ErvVege,
	   KRT_PartnerCimek.Letrehozo_id,
	   KRT_PartnerCimek.LetrehozasIdo,
	   KRT_PartnerCimek.Modosito_id,
	   KRT_PartnerCimek.ModositasIdo,
	   KRT_PartnerCimek.Zarolo_id,
	   KRT_PartnerCimek.ZarolasIdo,
	   KRT_PartnerCimek.Tranz_id,
	   KRT_PartnerCimek.UIAccessLog_id,
	   KRT_Cimek.Id,
	   KRT_Cimek.Org,
	   KRT_Cimek.Kategoria,
	   KRT_Cimek.Tipus,
       KRT_KodTarak.Nev as TipusNev,
	   KRT_Cimek.Nev,
	   KRT_Cimek.KulsoAzonositok,
	   KRT_Cimek.Forras,
	   KRT_Cimek.Orszag_Id,
	   KRT_Cimek.OrszagNev,
	   KRT_Cimek.Telepules_Id,
	   KRT_Cimek.TelepulesNev,
	   KRT_Cimek.IRSZ,
	   KRT_Cimek.CimTobbi,
	   KRT_Cimek.Kozterulet_Id,
	   KRT_Cimek.KozteruletNev,
	   KRT_Cimek.KozteruletTipus_Id,
	   KRT_Cimek.KozteruletTipusNev,
	   KRT_Cimek.Hazszam,
	   KRT_Cimek.Hazszamig,
	   KRT_Cimek.HazszamBetujel,
	   KRT_Cimek.MindketOldal,
	   KRT_Cimek.HRSZ,
	   KRT_Cimek.Lepcsohaz,
	   KRT_Cimek.Szint,
	   KRT_Cimek.Ajto,
	   KRT_Cimek.AjtoBetujel,
	   KRT_Cimek.Tobbi,
	   KRT_Cimek.ErvKezd,
	   KRT_Cimek.ErvVege
   from 
     KRT_PartnerCimek as KRT_PartnerCimek      
	join KRT_Cimek as KRT_Cimek on KRT_PartnerCimek.Cim_Id = KRT_Cimek.Id
	left join KRT_KodCsoportok on KRT_KodCsoportok.Kod=''CIM_TIPUS''
	left join KRT_KodTarak on KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and KRT_KodTarak.Org=''' + cast(@Org as NVarChar(40)) +'''
		and KRT_Cimek.Tipus = KRT_KodTarak.Kod 
	where KRT_PartnerCimek.Partner_id = ''' + CAST(@Partner_Id as Nvarchar(40)) + '''       
	and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege
    '
    
	if @Cim_Tipus is not null and @Cim_Tipus!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + 'KRT_Cimek.Tipus = ''' + @Cim_Tipus + ''' '
	end
  
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy 

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
