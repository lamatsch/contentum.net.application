IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CimekHistoryGetAllRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_CimekHistoryGetAllRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CimekHistoryGetAllRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_CimekHistoryGetAllRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_CimekHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from KRT_CimekHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Org' as ColumnName,               cast(Old.Org as nvarchar(99)) as OldValue,
               cast(New.Org as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Org != New.Org 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kategoria' as ColumnName,               cast(Old.Kategoria as nvarchar(99)) as OldValue,
               cast(New.Kategoria as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Kategoria != New.Kategoria 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tipus' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tipus != New.Tipus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'CÍM TÍPUS'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Tipus and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Tipus and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Nev' as ColumnName,               cast(Old.Nev as nvarchar(99)) as OldValue,
               cast(New.Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Nev != New.Nev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KulsoAzonositok' as ColumnName,               cast(Old.KulsoAzonositok as nvarchar(99)) as OldValue,
               cast(New.KulsoAzonositok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KulsoAzonositok != New.KulsoAzonositok 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Forras' as ColumnName,               cast(Old.Forras as nvarchar(99)) as OldValue,
               cast(New.Forras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Forras != New.Forras 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Orszag_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_OrszagokAzonosito(Old.Orszag_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_OrszagokAzonosito(New.Orszag_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Orszag_Id != New.Orszag_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Orszagok FTOld on FTOld.Id = Old.Orszag_Id --and FTOld.Ver = Old.Ver
         left join KRT_Orszagok FTNew on FTNew.Id = New.Orszag_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'OrszagNev' as ColumnName,               cast(Old.OrszagNev as nvarchar(99)) as OldValue,
               cast(New.OrszagNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.OrszagNev != New.OrszagNev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Telepules_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_TelepulesekAzonosito(Old.Telepules_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_TelepulesekAzonosito(New.Telepules_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Telepules_Id != New.Telepules_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Telepulesek FTOld on FTOld.Id = Old.Telepules_Id --and FTOld.Ver = Old.Ver
         left join KRT_Telepulesek FTNew on FTNew.Id = New.Telepules_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TelepulesNev' as ColumnName,               cast(Old.TelepulesNev as nvarchar(99)) as OldValue,
               cast(New.TelepulesNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.TelepulesNev != New.TelepulesNev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IRSZ' as ColumnName,               cast(Old.IRSZ as nvarchar(99)) as OldValue,
               cast(New.IRSZ as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IRSZ != New.IRSZ 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CimTobbi' as ColumnName,               cast(Old.CimTobbi as nvarchar(99)) as OldValue,
               cast(New.CimTobbi as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.CimTobbi != New.CimTobbi 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kozterulet_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_KozteruletekAzonosito(Old.Kozterulet_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_KozteruletekAzonosito(New.Kozterulet_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Kozterulet_Id != New.Kozterulet_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Kozteruletek FTOld on FTOld.Id = Old.Kozterulet_Id --and FTOld.Ver = Old.Ver
         left join KRT_Kozteruletek FTNew on FTNew.Id = New.Kozterulet_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KozteruletNev' as ColumnName,               cast(Old.KozteruletNev as nvarchar(99)) as OldValue,
               cast(New.KozteruletNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KozteruletNev != New.KozteruletNev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KozteruletTipus_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_KozteruletTipusokAzonosito(Old.KozteruletTipus_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_KozteruletTipusokAzonosito(New.KozteruletTipus_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KozteruletTipus_Id != New.KozteruletTipus_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_KozteruletTipusok FTOld on FTOld.Id = Old.KozteruletTipus_Id --and FTOld.Ver = Old.Ver
         left join KRT_KozteruletTipusok FTNew on FTNew.Id = New.KozteruletTipus_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KozteruletTipusNev' as ColumnName,               cast(Old.KozteruletTipusNev as nvarchar(99)) as OldValue,
               cast(New.KozteruletTipusNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KozteruletTipusNev != New.KozteruletTipusNev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Hazszam' as ColumnName,               cast(Old.Hazszam as nvarchar(99)) as OldValue,
               cast(New.Hazszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Hazszam != New.Hazszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Hazszamig' as ColumnName,               cast(Old.Hazszamig as nvarchar(99)) as OldValue,
               cast(New.Hazszamig as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Hazszamig != New.Hazszamig 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HazszamBetujel' as ColumnName,               cast(Old.HazszamBetujel as nvarchar(99)) as OldValue,
               cast(New.HazszamBetujel as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HazszamBetujel != New.HazszamBetujel 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MindketOldal' as ColumnName,               cast(Old.MindketOldal as nvarchar(99)) as OldValue,
               cast(New.MindketOldal as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MindketOldal != New.MindketOldal 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HRSZ' as ColumnName,               cast(Old.HRSZ as nvarchar(99)) as OldValue,
               cast(New.HRSZ as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HRSZ != New.HRSZ 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Lepcsohaz' as ColumnName,               cast(Old.Lepcsohaz as nvarchar(99)) as OldValue,
               cast(New.Lepcsohaz as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Lepcsohaz != New.Lepcsohaz 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Szint' as ColumnName,               cast(Old.Szint as nvarchar(99)) as OldValue,
               cast(New.Szint as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Szint != New.Szint 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ajto' as ColumnName,               cast(Old.Ajto as nvarchar(99)) as OldValue,
               cast(New.Ajto as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Ajto != New.Ajto 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AjtoBetujel' as ColumnName,               cast(Old.AjtoBetujel as nvarchar(99)) as OldValue,
               cast(New.AjtoBetujel as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AjtoBetujel != New.AjtoBetujel 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tobbi' as ColumnName,               cast(Old.Tobbi as nvarchar(99)) as OldValue,
               cast(New.Tobbi as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_CimekHistory Old
         inner join KRT_CimekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tobbi != New.Tobbi 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end


GO
