IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoportTagokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_CsoportTagokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoportTagokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_CsoportTagokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_CsoportTagokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_CsoportTagok.Id,
	   KRT_CsoportTagok.Tipus,
	   KRT_CsoportTagok.Csoport_Id,
	   KRT_CsoportTagok.Csoport_Id_Jogalany,
	   KRT_CsoportTagok.ObjTip_Id_Jogalany,
	   KRT_CsoportTagok.ErtesitesMailCim,
	   KRT_CsoportTagok.ErtesitesKell,
	   KRT_CsoportTagok.System,
	   KRT_CsoportTagok.Orokolheto,
	   KRT_CsoportTagok.Ver,
	   KRT_CsoportTagok.Note,
	   KRT_CsoportTagok.Stat_id,
	   KRT_CsoportTagok.ErvKezd,
	   KRT_CsoportTagok.ErvVege,
	   KRT_CsoportTagok.Letrehozo_id,
	   KRT_CsoportTagok.LetrehozasIdo,
	   KRT_CsoportTagok.Modosito_id,
	   KRT_CsoportTagok.ModositasIdo,
	   KRT_CsoportTagok.Zarolo_id,
	   KRT_CsoportTagok.ZarolasIdo,
	   KRT_CsoportTagok.Tranz_id,
	   KRT_CsoportTagok.UIAccessLog_id
	   from 
		 KRT_CsoportTagok as KRT_CsoportTagok 
	   where
		 KRT_CsoportTagok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
