IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoportTagokGetSzervezet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_CsoportTagokGetSzervezet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoportTagokGetSzervezet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_CsoportTagokGetSzervezet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_CsoportTagokGetSzervezet]
	@ExecutorUserId      uniqueidentifier    
AS
BEGIN TRY
	select * from KRT_CsoportTagok
		inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id
	where KRT_CsoportTagok.Csoport_Id_Jogalany = @ExecutorUserId AND KRT_CsoportTagok.Tipus is not null AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
	ORDER BY [KRT_CsoportTagok].[Tipus] DESC, KRT_CsoportTagok.Id ASC
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
