IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoportokGetAllWithFK]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_CsoportokGetAllWithFK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoportokGetAllWithFK]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_CsoportokGetAllWithFK] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_CsoportokGetAllWithFK]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Csoportok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @pageNumber		int = 0,
  @pageSize		int = -1,
  @SelectedRowId	uniqueidentifier = null

as

begin

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 	DECLARE @firstRow INT
	DECLARE @lastRow INT
   
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = 1000
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end
 
	/************************************************************
	* Szurt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   KRT_Csoportok.Id into #result
		from KRT_Csoportok as KRT_Csoportok
		left join KRT_KodCsoportok on KRT_KodCsoportok.Kod=''CSOPORTTIPUS''
		left join KRT_KodTarak on KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and KRT_KodTarak.Org=@Org
			and KRT_Csoportok.Tipus = KRT_KodTarak.Kod
    Where KRT_Csoportok.Org=@Org'

   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end

	if (@SelectedRowId is not null)
	begin
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id = @SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	end

	set @sqlcmd = @sqlcmd + N' 
	if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
	BEGIN
		select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
		set @firstRow = (@pageNumber - 1) * @pageSize + 1;
		set @lastRow = @pageNumber*@pageSize;
		select @pageNumber = @pageNumber - 1
	END ;'

	/************************************************************
	* Tényleges select											*
	************************************************************/
	SET @sqlcmd = @sqlcmd + 
  'select 
		#result.RowNumber,
  	   KRT_Csoportok.Id,
	   KRT_Csoportok.Org,
	   KRT_Csoportok.Kod,
	   KRT_Csoportok.Nev,
	   KRT_Csoportok.Tipus,
		KRT_KodTarak.Nev as Csoport_Tipus,
	   KRT_Csoportok.Jogalany,
	   KRT_Csoportok.System,
	   KRT_Csoportok.ObjTipus_Id_Szulo,
	   KRT_Csoportok.Kiszolgalhato,
	   KRT_Csoportok.JogosultsagOroklesMod,
	   KRT_Csoportok.Ver,
	   KRT_Csoportok.Note,
	   KRT_Csoportok.Stat_id,
	   KRT_Csoportok.ErvKezd,
	   KRT_Csoportok.ErvVege,
	   KRT_Csoportok.Letrehozo_id,
	   KRT_Csoportok.LetrehozasIdo,
	   KRT_Csoportok.Modosito_id,
	   KRT_Csoportok.ModositasIdo,
	   KRT_Csoportok.Zarolo_id,
	   KRT_Csoportok.ZarolasIdo,
	   KRT_Csoportok.Tranz_id,
	   KRT_Csoportok.UIAccessLog_id  
    from 
     KRT_Csoportok as KRT_Csoportok
     	inner join #result on #result.Id = KRT_Csoportok.Id
		left join KRT_KodCsoportok on KRT_KodCsoportok.Kod=''CSOPORTTIPUS''
		left join KRT_KodTarak on KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and KRT_KodTarak.Org=@Org
			and KRT_Csoportok.Tipus = KRT_KodTarak.Kod 
    where RowNumber between @firstRow and @lastRow
	ORDER BY #result.RowNumber;'
   
	-- találatok száma és oldalszám
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';
 	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier, @Org uniqueidentifier',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId, @Org = @Org;  

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
