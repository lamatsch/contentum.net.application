IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoportokGetByKod]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_CsoportokGetByKod]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoportokGetByKod]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_CsoportokGetByKod] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_CsoportokGetByKod]
			@csoportkod  nvarchar(200),
         @ExecutorUserId uniqueidentifier
         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Csoportok.Id,
	   KRT_Csoportok.Org,
	   KRT_Csoportok.Kod,
	   KRT_Csoportok.Nev,
	   KRT_Csoportok.Tipus,
	   KRT_Csoportok.Jogalany,
	   KRT_Csoportok.ErtesitesEmail,
	   KRT_Csoportok.System,
	   KRT_Csoportok.Adatforras,
	   KRT_Csoportok.ObjTipus_Id_Szulo,
	   KRT_Csoportok.Kiszolgalhato,
	   KRT_Csoportok.JogosultsagOroklesMod,
	   KRT_Csoportok.Ver,
	   KRT_Csoportok.Note,
	   KRT_Csoportok.Stat_id,
	   KRT_Csoportok.ErvKezd,
	   KRT_Csoportok.ErvVege,
	   KRT_Csoportok.Letrehozo_id,
	   KRT_Csoportok.LetrehozasIdo,
	   KRT_Csoportok.Modosito_id,
	   KRT_Csoportok.ModositasIdo,
	   KRT_Csoportok.Zarolo_id,
	   KRT_Csoportok.ZarolasIdo,
	   KRT_Csoportok.Tranz_id,
	   KRT_Csoportok.UIAccessLog_id
	   from 
		 KRT_Csoportok as KRT_Csoportok 
	   where
		 KRT_Csoportok.Kod = @csoportkod'

	exec sp_executesql @sqlcmd, N'@csoportkod nvarchar(200)',@csoportkod = @csoportkod
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
