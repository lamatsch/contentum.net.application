IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoportokGetFirstRightedLeader]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_CsoportokGetFirstRightedLeader]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoportokGetFirstRightedLeader]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_CsoportokGetFirstRightedLeader] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_KRT_CsoportokGetFirstRightedLeader]
	@ExecutorUserId		uniqueidentifier,
	@FelhasznaloSzervezet_Id	uniqueidentifier,
	@Helyettesites_Id	uniqueidentifier = null,
	@FunkcioKod	nvarchar(100) = null,
	@IncludeSelfIfRightedLeader	tinyint = 1
AS
BEGIN
begin try
	SET NOCOUNT ON;


	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	declare @CsoportTag_Id uniqueidentifier
	
	-- helyettesítés rekord lekérdezése:
	declare @Id uniqueidentifier
	declare @HelyettesitesMod nvarchar(64)
	declare @Felhasznalo_ID_helyettesitett uniqueidentifier
	declare @CsoportTag_ID_helyettesitett uniqueidentifier
	declare @Felhasznalo_ID_helyettesito uniqueidentifier

	if @Helyettesites_Id is not null
	begin
		select @Id = Id,
			@Felhasznalo_ID_helyettesitett = Felhasznalo_ID_helyettesitett,
			@CsoportTag_ID_helyettesitett = CsoportTag_ID_helyettesitett,
			@Felhasznalo_ID_helyettesito = Felhasznalo_ID_helyettesito, @HelyettesitesMod = HelyettesitesMod
		from KRT_Helyettesitesek where Id = @Helyettesites_Id
		and getdate() between KRT_Helyettesitesek.ErvKezd and KRT_Helyettesitesek.ErvVege
		and getdate() between KRT_Helyettesitesek.HelyettesitesKezd and KRT_Helyettesitesek.HelyettesitesVege

		-- a rekord nem található
		if (@Id is null)
		begin
			RAISERROR('[50101]',16,1)
		end

		-- felhasználók ellenorzése:
		declare @Helyettesitett_Id uniqueidentifier
		select @Helyettesitett_Id = Id
			from KRT_Felhasznalok
			where Id = @Felhasznalo_ID_helyettesitett
			and getdate() between ErvKezd and ErvVege

		if (@Helyettesitett_Id is null)
		begin
			RAISERROR('[53510]',16,1) -- A helyettesített felhasználó nem létezik!
		end

		declare @Helyettesito_Id uniqueidentifier
		select @Helyettesito_Id = Id
			from KRT_Felhasznalok
			where Id = @Felhasznalo_ID_helyettesito
			and getdate() between ErvKezd and ErvVege

		if (@Helyettesito_Id is null)
		begin
			RAISERROR('[53511]',16,1) -- A helyettesíto felhasználó nem létezik!
		end
	end

	if @HelyettesitesMod = '2' -- megbízás
	begin
		-- Csoporttagság ellenorzése
		set @CsoportTag_Id = (select top 1 Id from KRT_CsoportTagok where Csoport_Id_Jogalany = @Felhasznalo_ID_helyettesitett
		and Csoport_Id=@FelhasznaloSzervezet_Id and getdate() between ErvKezd and ErvVege)
	end
	else
	begin
		-- Csoporttagság ellenorzése
		set @CsoportTag_Id = (select top 1 Id from KRT_CsoportTagok where Csoport_Id_Jogalany = @ExecutorUserId
		and Csoport_Id=@FelhasznaloSzervezet_Id and getdate() between ErvKezd and ErvVege)
	end

	if @CsoportTag_Id is null
	begin
		RAISERROR('[53501]',16,1) -- Nem létezo csoporttagság!
	end

	;WITH felettesek AS
	(
		SELECT @FelhasznaloSzervezet_Id AS Id, cast(NULL as uniqueidentifier) AS jogalanyId, 0 as Level
		
		UNION ALL
		
		SELECT KRT_CsoportTagok.Csoport_Id AS Id, KRT_CsoportTagok.Csoport_Id_Jogalany AS jogalanyId, felettesek.Level + 1 as Level
			FROM KRT_CsoportTagok
				INNER JOIN felettesek ON felettesek.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
				INNER JOIN KRT_Csoportok ON KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id
			WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
				AND KRT_Csoportok.Tipus IN ('0','2','4','6','R') and KRT_Csoportok.Org=@Org
	) SELECT TOP 1 KRT_Csoportok.*
	FROM felettesek --WHERE jogalanyId IS NOT NULL;
	left join KRT_CsoportTagok
	on felettesek.Id = KRT_CsoportTagok.Csoport_id
			left join KRT_Csoportok
			on KRT_CsoportTagok.Csoport_Id_Jogalany = KRT_Csoportok.Id
			AND KRT_CsoportTagok.Tipus = '3'
			and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
			and getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege
			and 1 = case when IsNull(@funkcioKod, '') = '' then 1
							when exists(select 1 from KRT_Csoportok cs
							left join KRT_Felhasznalok fh
							on fh.Id = cs.Id
							left join KRT_Felhasznalo_Szerepkor fsz
							on fh.Id = fsz.Felhasznalo_Id
								and IsNull(fsz.Csoport_Id, KRT_CsoportTagok.Csoport_Id)=KRT_CsoportTagok.Csoport_Id
							left join KRT_Szerepkorok sz
							on sz.Id = fsz.Szerepkor_Id
							left join KRT_Szerepkor_Funkcio szf
							on sz.Id = szf.Szerepkor_Id
							left join KRT_Funkciok f
							on f.Id = szf.Funkcio_Id
							where getdate() between cs.ErvKezd and cs.ErvVege
							and getdate() between fh.ErvKezd and fh.ErvVege
							and getdate() between fsz.ErvKezd and fsz.ErvVege
							and getdate() between f.ErvKezd and f.ErvVege
							and getdate() between sz.ErvKezd and sz.ErvVege
							and getdate() between szf.ErvKezd and szf.ErvVege
							and cs.Id = KRT_Csoportok.Id
							and f.Kod=@FunkcioKod) then 1
						else 0
					end -- case
	where KRT_Csoportok.Id = KRT_Csoporttagok.Csoport_Id_Jogalany
	and ((@IncludeSelfIfRightedLeader = 1) or (@ExecutorUserId <> KRT_CsoportTagok.Csoport_Id_Jogalany))
	order by felettesek.Level

end try
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
END


GO
