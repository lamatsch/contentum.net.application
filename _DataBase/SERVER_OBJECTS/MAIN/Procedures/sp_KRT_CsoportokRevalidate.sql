IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoportokRevalidate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_CsoportokRevalidate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoportokRevalidate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_CsoportokRevalidate] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_CsoportokRevalidate]
      @Id	               uniqueidentifier,
      @ExecutorUserId		uniqueidentifier,
      @ExecutionTime			datetime	= NULL,
      @ErvKezd             DATETIME = null,
      @ErvVege             DATETIME = null		

as

BEGIN TRY

	set nocount on
   
   SET @ExecutionTime = getdate()
  
   IF @ErvKezd IS NULL
   BEGIN
      SET @ErvKezd = GETDATE()
   END
	
	IF @ErvVege IS NULL
	BEGIN
		SET @ErvVege = '4700-12-31'
	END
   
   DECLARE @Act_ErvKezd datetime
   DECLARE @Act_ErvVege datetime
   DECLARE @Act_Ver INT
   DECLARE @Tipus NVARCHAR(64)
      
   select
      @Act_ErvKezd = ErvKezd,
      @Act_ErvVege = ErvVege,
      @Tipus = Tipus,
      @Act_Ver = Ver
   from KRT_Csoportok
   where Id = @Id
      
   if @@rowcount = 0
	begin
	 RAISERROR('[50652]',16,1)
	end
      
   if GETDATE() BETWEEN @Act_ErvKezd AND @Act_ErvVege
   begin
     -- érvényes
     RAISERROR('[50653]',16,1)
   end
      
   IF @Act_Ver is null
	   SET @Act_Ver = 2	
   ELSE
	   SET @Act_Ver = @Act_Ver+1
      
	UPDATE KRT_Csoportok
	SET		 					
	  ErvKezd = @ErvKezd,
     ErvVege = @ErvVege,
     Ver     = @Act_Ver
   WHERE
	  Id = @Id

   if @@rowcount != 1
   begin
	   RAISERROR('[50651]',16,1)
   end   
   else
   begin 
	    DECLARE @tempCsoportTagok TABLE(Id UNIQUEIDENTIFIER)
	    
	    INSERT INTO @tempCsoportTagok (Id) 
	    SELECT KRT_CsoportTagok.Id
		FROM KRT_CsoportTagok
		INNER JOIN KRT_Csoportok csp1 ON csp1.Id = KRT_CsoportTagok.Csoport_Id AND GETDATE() BETWEEN csp1.ErvKezd AND csp1.ErvVege
		INNER JOIN KRT_Csoportok csp2 ON csp2.Id = KRT_CsoportTagok.Csoport_Id_Jogalany AND GETDATE() BETWEEN csp2.ErvKezd AND csp2.ErvVege
		WHERE (Csoport_Id = @Id OR Csoport_Id_Jogalany = @Id)
		AND KRT_CsoportTagok.ErvVege = @Act_ErvVege
		
		UPDATE KRT_CsoportTagok SET ErvKezd = @ErvKezd, ErvVege = @ErvVege
	    WHERE Id IN (SELECT Id FROM @tempCsoportTagok)
		
		/* KRT_CsoportTagok */
	    DECLARE @CsoportTagId UNIQUEIDENTIFIER
	    DECLARE cur CURSOR FOR
		SELECT Id FROM @tempCsoportTagok
		  	
		OPEN cur;
		FETCH NEXT FROM cur INTO @CsoportTagId;
		WHILE @@FETCH_STATUS = 0
		BEGIN
	  
		  exec sp_LogRecordToHistory 'KRT_CsoportTagok',@CsoportTagId
           ,'KRT_CsoportTagokHistory',-2,@ExecutorUserId,@ExecutionTime
                 
		  FETCH NEXT FROM cur INTO @CsoportTagId;
	    END
		  	
        CLOSE cur
        DEALLOCATE cur
        
         /* History Log */
        exec sp_LogRecordToHistory 'KRT_Csoportok',@Id
        ,'KRT_CsoportokHistory',-2,@ExecutorUserId,@ExecutionTime    
   end
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InvalidateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
