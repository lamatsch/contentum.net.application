IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoporttagokAllBuildFromCsoportTagok]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_CsoporttagokAllBuildFromCsoportTagok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoporttagokAllBuildFromCsoportTagok]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_CsoporttagokAllBuildFromCsoportTagok] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_CsoporttagokAllBuildFromCsoportTagok]
as
BEGIN
	SET NOCOUNT ON
	delete from KRT_CsoportTagokAll;
	
	insert into KRT_CsoportTagokAll(Csoport_Id,Csoport_Id_Jogalany,Tipus,ErvKezd,ErvVege)
		select Id,Id,Tipus,ErvKezd,ErvVege
			from krt_csoportok;
--	print 'inicializálás vége';

	--delete from krt_csoporttagok where csoport_id_jogalany is null

	select newid()as Id,Csoport_Id,Csoport_Id_Jogalany,ErvKezd,ErvVege into #curLevel 
		from krt_csoporttagok 
		where csoport_Id not in 
			(select csoport_Id_jogalany 
				from krt_csoporttagok 
				where getdate() between Ervkezd and ervvege 
					and tipus is not null
			)
			and getdate() between ervkezd and ervvege 
			and Tipus is not null
--	print 'Top node-ok száma: ' + cast(@@ROWCOUNT as nvarchar(100))
	declare @Level int;
	set @Level = 0;

	while exists (select 1 from #curLevel)
	begin
		set @Level = @Level + 1;
		insert into KRT_CsoportTagokAll(Csoport_Id,Csoport_Id_Jogalany,LetrehozasIdo,ErvKezd,ErvVege) 
			select	
					KRT_CsoportTagokAll.Csoport_Id,
					#curLevel.Csoport_Id_Jogalany,
					getdate(),
					CASE 
						when KRT_CsoportTagokAll.ErvKezd > #curLevel.ErvKezd then KRT_CsoportTagokAll.ErvKezd
						else #curLevel.ErvKezd
					END as ErvKezd,
					CASE
						when KRT_CsoportTagokAll.ErvVege < #curLevel.ErvVege then KRT_CsoportTagokAll.ErvVege
						else #curLevel.ErvVege
					END as ErvVege
				from #curLevel,
					KRT_CsoportTagokAll
				where KRT_CsoportTagokAll.Csoport_id_jogalany = #curLevel.Csoport_id 
				and not exists (select 1 from krt_csoporttagokall csta where csta.csoport_id = KRT_CsoportTagokAll.Csoport_Id and csta.csoport_id_jogalany = #curLevel.Csoport_Id_Jogalany)

--		print cast(@Level as nvarchar(100)) + '. körben KRT_CsoportTagokAll-ba beszúrt rekordok száma: ' + cast(@@ROWCOUNT as nvarchar(100))

		select Csoport_Id_Jogalany into #nextLevel 
			from #curLevel
			where Csoport_Id_Jogalany not in (select Csoport_Id from KRT_CsoportTagokAll where Csoport_Id != Csoport_Id_Jogalany);
		delete from #curLevel;
		
		insert into #curLevel(Id,Csoport_Id,Csoport_Id_Jogalany,ErvKezd,ErvVege)
			select newid() as Id,Csoport_Id,Csoport_Id_Jogalany,ErvKezd,ErvVege
				from krt_csoporttagok 
				where csoport_Id in (select * from #nextLevel) 
					and getdate() between ervkezd and ervvege 
					and Tipus is not null
--		print cast(@Level as nvarchar(100)) + '. következo kör ' + cast(@@ROWCOUNT as nvarchar(100)) + ' db rekorddal indul'

		drop table #nextLevel;
	end

	drop table #curLevel;
END


GO
