IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoporttagokIsMember]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_CsoporttagokIsMember]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_CsoporttagokIsMember]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_CsoporttagokIsMember] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_KRT_CsoporttagokIsMember]
 (  @CsoportId		uniqueidentifier,
	@UserId			uniqueidentifier
)
AS
BEGIN
begin try
	SET NOCOUNT ON;
	declare @Return int;
	set @Return = 0;
	
	;WITH temp AS
	(
		SELECT @CsoportId AS Id
		
		UNION ALL
		
		SELECT KRT_Csoporttagok.Csoport_Id_Jogalany
			FROM KRT_Csoporttagok
				INNER JOIN temp ON temp.Id = KRT_Csoporttagok.Csoport_Id
			WHERE KRT_Csoporttagok.Tipus IS NOT NULL
				AND GETDATE() BETWEEN KRT_Csoporttagok.ErvKezd AND KRT_Csoporttagok.ErvVege
	) select @Return = 1 FROM temp WHERE Id = @UserId
	
	SELECT ISNULL(@Return,'0');

end try
begin catch
  declare @errorSeverity integer,
          @errorState integer,
          @errorCode nvarchar(1000);
  select @errorSeverity = ERROR_SEVERITY(), @errorState = ERROR_STATE();

  if ERROR_NUMBER() < 50000
    set @errorCode = '[' + convert(nvarchar(10), ERROR_NUMBER()) + '] ' + '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();
  else
    set @errorCode = '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();

  if @errorState = 0 set @errorState = 1
  raiserror(@errorCode, @errorSeverity, @errorState)
end catch

END


GO
