IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_DokuAlairasKapcsolatokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_DokuAlairasKapcsolatokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_DokuAlairasKapcsolatokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_DokuAlairasKapcsolatokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_DokuAlairasKapcsolatokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_DokuAlairasKapcsolatok.Id,
	   KRT_DokuAlairasKapcsolatok.Dokumentum_id,
	   KRT_DokuAlairasKapcsolatok.DokumentumAlairas_Id,
	   KRT_DokuAlairasKapcsolatok.DokuKapcsolatSorrend,
	   KRT_DokuAlairasKapcsolatok.Ver,
	   KRT_DokuAlairasKapcsolatok.Note,
	   KRT_DokuAlairasKapcsolatok.Stat_id,
	   KRT_DokuAlairasKapcsolatok.ErvKezd,
	   KRT_DokuAlairasKapcsolatok.ErvVege,
	   KRT_DokuAlairasKapcsolatok.Letrehozo_id,
	   KRT_DokuAlairasKapcsolatok.LetrehozasIdo,
	   KRT_DokuAlairasKapcsolatok.Modosito_id,
	   KRT_DokuAlairasKapcsolatok.ModositasIdo,
	   KRT_DokuAlairasKapcsolatok.Zarolo_id,
	   KRT_DokuAlairasKapcsolatok.ZarolasIdo,
	   KRT_DokuAlairasKapcsolatok.Tranz_id,
	   KRT_DokuAlairasKapcsolatok.UIAccessLog_id
	   from 
		 KRT_DokuAlairasKapcsolatok as KRT_DokuAlairasKapcsolatok 
	   where
		 KRT_DokuAlairasKapcsolatok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
