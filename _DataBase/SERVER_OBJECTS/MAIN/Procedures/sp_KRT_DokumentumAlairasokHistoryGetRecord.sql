IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_DokumentumAlairasokHistoryGetRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_DokumentumAlairasokHistoryGetRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_DokumentumAlairasokHistoryGetRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_DokumentumAlairasokHistoryGetRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_DokumentumAlairasokHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin
select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumAlairasokHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and KRT_DokumentumAlairasokHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumAlairasokHistory Old
         inner join KRT_DokumentumAlairasokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Dokumentum_Id_Alairt' as ColumnName,               
               cast(Old.Dokumentum_Id_Alairt as nvarchar(99)) as OldValue,
               cast(New.Dokumentum_Id_Alairt as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumAlairasokHistory Old
         inner join KRT_DokumentumAlairasokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Dokumentum_Id_Alairt != New.Dokumentum_Id_Alairt 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasMod' as ColumnName,               
               cast(Old.AlairasMod as nvarchar(99)) as OldValue,
               cast(New.AlairasMod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumAlairasokHistory Old
         inner join KRT_DokumentumAlairasokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasMod != New.AlairasMod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasSzabaly_Id' as ColumnName,               
               cast(Old.AlairasSzabaly_Id as nvarchar(99)) as OldValue,
               cast(New.AlairasSzabaly_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumAlairasokHistory Old
         inner join KRT_DokumentumAlairasokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasSzabaly_Id != New.AlairasSzabaly_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasRendben' as ColumnName,               
               cast(Old.AlairasRendben as nvarchar(99)) as OldValue,
               cast(New.AlairasRendben as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumAlairasokHistory Old
         inner join KRT_DokumentumAlairasokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasRendben != New.AlairasRendben 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KivarasiIdoVege' as ColumnName,               
               cast(Old.KivarasiIdoVege as nvarchar(99)) as OldValue,
               cast(New.KivarasiIdoVege as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumAlairasokHistory Old
         inner join KRT_DokumentumAlairasokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KivarasiIdoVege != New.KivarasiIdoVege 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasVeglegRendben' as ColumnName,               
               cast(Old.AlairasVeglegRendben as nvarchar(99)) as OldValue,
               cast(New.AlairasVeglegRendben as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumAlairasokHistory Old
         inner join KRT_DokumentumAlairasokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasVeglegRendben != New.AlairasVeglegRendben 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Idopecset' as ColumnName,               
               cast(Old.Idopecset as nvarchar(99)) as OldValue,
               cast(New.Idopecset as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumAlairasokHistory Old
         inner join KRT_DokumentumAlairasokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Idopecset != New.Idopecset 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Alairo' as ColumnName,               
               cast(Old.Csoport_Id_Alairo as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Alairo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumAlairasokHistory Old
         inner join KRT_DokumentumAlairasokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Alairo != New.Csoport_Id_Alairo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairoSzemely' as ColumnName,               
               cast(Old.AlairoSzemely as nvarchar(99)) as OldValue,
               cast(New.AlairoSzemely as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumAlairasokHistory Old
         inner join KRT_DokumentumAlairasokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairoSzemely != New.AlairoSzemely 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasTulajdonos' as ColumnName,               
               cast(Old.AlairasTulajdonos as nvarchar(99)) as OldValue,
               cast(New.AlairasTulajdonos as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumAlairasokHistory Old
         inner join KRT_DokumentumAlairasokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AlairasTulajdonos != New.AlairasTulajdonos 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tanusitvany_Id' as ColumnName,               
               cast(Old.Tanusitvany_Id as nvarchar(99)) as OldValue,
               cast(New.Tanusitvany_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumAlairasokHistory Old
         inner join KRT_DokumentumAlairasokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tanusitvany_Id != New.Tanusitvany_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               
               cast(Old.Allapot as nvarchar(99)) as OldValue,
               cast(New.Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_DokumentumAlairasokHistory Old
         inner join KRT_DokumentumAlairasokHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Allapot != New.Allapot 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end


GO
