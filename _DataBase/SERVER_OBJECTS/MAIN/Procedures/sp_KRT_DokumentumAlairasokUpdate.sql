IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_DokumentumAlairasokUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_DokumentumAlairasokUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_DokumentumAlairasokUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_DokumentumAlairasokUpdate] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_DokumentumAlairasokUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Dokumentum_Id_Alairt     uniqueidentifier  = null,         
             @AlairasMod     nvarchar(64)  = null,         
             @AlairasSzabaly_Id     uniqueidentifier  = null,         
             @AlairasRendben     char(1)  = null,         
             @KivarasiIdoVege     datetime  = null,         
             @AlairasVeglegRendben     char(1)  = null,         
             @Idopecset     datetime  = null,         
             @Csoport_Id_Alairo     uniqueidentifier  = null,         
             @AlairasTulajdonos     Nvarchar(100)  = null,         
             @Tanusitvany_Id     uniqueidentifier  = null,         
             @Allapot     nvarchar(64)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Dokumentum_Id_Alairt     uniqueidentifier         
     DECLARE @Act_AlairasMod     nvarchar(64)         
     DECLARE @Act_AlairasSzabaly_Id     uniqueidentifier         
     DECLARE @Act_AlairasRendben     char(1)         
     DECLARE @Act_KivarasiIdoVege     datetime         
     DECLARE @Act_AlairasVeglegRendben     char(1)         
     DECLARE @Act_Idopecset     datetime         
     DECLARE @Act_Csoport_Id_Alairo     uniqueidentifier         
     DECLARE @Act_AlairasTulajdonos     Nvarchar(100)         
     DECLARE @Act_Tanusitvany_Id     uniqueidentifier         
     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Dokumentum_Id_Alairt = Dokumentum_Id_Alairt,
     @Act_AlairasMod = AlairasMod,
     @Act_AlairasSzabaly_Id = AlairasSzabaly_Id,
     @Act_AlairasRendben = AlairasRendben,
     @Act_KivarasiIdoVege = KivarasiIdoVege,
     @Act_AlairasVeglegRendben = AlairasVeglegRendben,
     @Act_Idopecset = Idopecset,
     @Act_Csoport_Id_Alairo = Csoport_Id_Alairo,
     @Act_AlairasTulajdonos = AlairasTulajdonos,
     @Act_Tanusitvany_Id = Tanusitvany_Id,
     @Act_Allapot = Allapot,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from KRT_DokumentumAlairasok
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Dokumentum_Id_Alairt')=1
         SET @Act_Dokumentum_Id_Alairt = @Dokumentum_Id_Alairt
   IF @UpdatedColumns.exist('/root/AlairasMod')=1
         SET @Act_AlairasMod = @AlairasMod
   IF @UpdatedColumns.exist('/root/AlairasSzabaly_Id')=1
         SET @Act_AlairasSzabaly_Id = @AlairasSzabaly_Id
   IF @UpdatedColumns.exist('/root/AlairasRendben')=1
         SET @Act_AlairasRendben = @AlairasRendben
   IF @UpdatedColumns.exist('/root/KivarasiIdoVege')=1
         SET @Act_KivarasiIdoVege = @KivarasiIdoVege
   IF @UpdatedColumns.exist('/root/AlairasVeglegRendben')=1
         SET @Act_AlairasVeglegRendben = @AlairasVeglegRendben
   IF @UpdatedColumns.exist('/root/Idopecset')=1
         SET @Act_Idopecset = @Idopecset
   IF @UpdatedColumns.exist('/root/Csoport_Id_Alairo')=1
         SET @Act_Csoport_Id_Alairo = @Csoport_Id_Alairo
   IF @UpdatedColumns.exist('/root/AlairasTulajdonos')=1
         SET @Act_AlairasTulajdonos = @AlairasTulajdonos
   IF @UpdatedColumns.exist('/root/Tanusitvany_Id')=1
         SET @Act_Tanusitvany_Id = @Tanusitvany_Id
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE KRT_DokumentumAlairasok
SET
     Dokumentum_Id_Alairt = @Act_Dokumentum_Id_Alairt,
     AlairasMod = @Act_AlairasMod,
     AlairasSzabaly_Id = @Act_AlairasSzabaly_Id,
     AlairasRendben = @Act_AlairasRendben,
     KivarasiIdoVege = @Act_KivarasiIdoVege,
     AlairasVeglegRendben = @Act_AlairasVeglegRendben,
     Idopecset = @Act_Idopecset,
     Csoport_Id_Alairo = @Act_Csoport_Id_Alairo,
     AlairasTulajdonos = @Act_AlairasTulajdonos,
     Tanusitvany_Id = @Act_Tanusitvany_Id,
     Allapot = @Act_Allapot,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'KRT_DokumentumAlairasok',@Id
					,'KRT_DokumentumAlairasokHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
