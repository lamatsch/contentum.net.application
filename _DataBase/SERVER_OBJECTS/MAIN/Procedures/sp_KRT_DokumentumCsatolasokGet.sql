IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_DokumentumCsatolasokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_DokumentumCsatolasokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_DokumentumCsatolasokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_DokumentumCsatolasokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_DokumentumCsatolasokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier
         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_DokumentumCsatolasok.Id,
	   KRT_DokumentumCsatolasok.ObjTip_Id_Dokumentum,
	   KRT_DokumentumCsatolasok.Obj_Id_Dokumentum,
	   KRT_DokumentumCsatolasok.ObjTip_Id,
	   KRT_DokumentumCsatolasok.Obj_Id,
	   KRT_DokumentumCsatolasok.Tipus,
	   KRT_DokumentumCsatolasok.Tipus_Id,
	   KRT_DokumentumCsatolasok.Leiras,
	   KRT_DokumentumCsatolasok.Ver,
	   KRT_DokumentumCsatolasok.Note,
	   KRT_DokumentumCsatolasok.Stat_id,
	   KRT_DokumentumCsatolasok.ErvKezd,
	   KRT_DokumentumCsatolasok.ErvVege,
	   KRT_DokumentumCsatolasok.Letrehozo_id,
	   KRT_DokumentumCsatolasok.LetrehozasIdo,
	   KRT_DokumentumCsatolasok.Modosito_id,
	   KRT_DokumentumCsatolasok.ModositasIdo,
	   KRT_DokumentumCsatolasok.Zarolo_id,
	   KRT_DokumentumCsatolasok.ZarolasIdo,
	   KRT_DokumentumCsatolasok.Tranz_id,
	   KRT_DokumentumCsatolasok.UIAccessLog_id
	   from 
		 KRT_DokumentumCsatolasok as KRT_DokumentumCsatolasok 
	   where
		 KRT_DokumentumCsatolasok.Id = @Id'

	exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
