IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_DokumentumokGetWithRightCheck]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_DokumentumokGetWithRightCheck]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_DokumentumokGetWithRightCheck]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_DokumentumokGetWithRightCheck] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_DokumentumokGetWithRightCheck]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FelhasznaloSzervezet_Id uniqueidentifier,
         @Jogszint	char(1)

         
as

begin
BEGIN TRY

	set nocount on	
	
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	--if dbo.fn_IsAdmin(@ExecutorUserId) = 0 
	IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
		AND exists(
			-- Nem különálló dokumentum
			SELECT 1 FROM EREC_Csatolmanyok
			WHERE Dokumentum_Id = @Id and getdate() between ErvKezd and ErvVege
		)
		AND not exists
		(
			-- Irat alapján
			SELECT TOP(1) 1
			FROM EREC_Csatolmanyok
				INNER JOIN EREC_IraIratok ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
				INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_IraIratok.Id
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll 
					ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			WHERE EREC_Csatolmanyok.[Dokumentum_Id] = @Id											
		)
		AND not exists
		(
			--CR1716: lenézünk a példányba, CR 2728: aki láthatja a példányt az láthatja az iratot is
			SELECT TOP(1) 1
				FROM EREC_Csatolmanyok
				INNER JOIN EREC_IraIratok ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
				INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = EREC_IraIratok.Id
				WHERE EREC_Csatolmanyok.[Dokumentum_Id] = @Id
				AND EREC_PldIratpeldanyok.Id in
				(
					SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
						INNER JOIN KRT_Jogosultak ON KRT_Jogosultak.Obj_Id = EREC_PldIratPeldanyok.Id
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
						ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
					UNION ALL
						SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
						ON CsoportTagokAll.Id = EREC_PldIratpeldanyok.Csoport_Id_Felelos
					UNION ALL
					SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
						ON CsoportTagokAll.Id = EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo
					UNION ALL
					SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_PldIratPeldanyok')
						UNION ALL
						SELECT EREC_PldIratpeldanyok.Id FROM EREC_PldIratpeldanyok
						INNER JOIN KRT_MappaTartalmak on KRT_MappaTartalmak.Obj_Id = EREC_PldIratpeldanyok.Id
						INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
						INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
						where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
						and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
				)
		)
		and not exists 
		(		
			-- Ügyirat, Iktatókönyv alapján
			SELECT TOP(1) 1																		
			FROM EREC_Csatolmanyok
				INNER JOIN EREC_IraIratok 
					ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id										
				WHERE EREC_Csatolmanyok.[Dokumentum_Id] = @Id
				and EREC_IraIratok.Ugyirat_Id IN 
				(
					SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
						INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll
							ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
					UNION ALL
					SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
						INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll
							ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
					UNION ALL
					SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll 
							ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
					UNION ALL
					SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll 
							ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
					UNION ALL
					SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll 
							ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
					UNION ALL
					SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId,@FelhasznaloSzervezet_Id, 'EREC_UgyUgyiratok')
					UNION ALL
					SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
						INNER JOIN KRT_MappaTartalmak on KRT_MappaTartalmak.Obj_Id = erec_ugyugyiratok.Id
						INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
						INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
						where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
						and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege																		
				)									
		 )
		 AND not exists
		 (
			-- Küldemény alapján
			SELECT TOP(1) 1
			FROM EREC_Csatolmanyok
				INNER JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_Csatolmanyok.KuldKuldemeny_Id
			WHERE EREC_Csatolmanyok.[Dokumentum_Id] = @Id
			and EREC_KuldKuldemenyek.Id IN 
			(
				SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.Id
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll
						ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
				UNION ALL
				SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll
						ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
				UNION ALL
				SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIratok_Id
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll 
						ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
				UNION ALL
				SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll 
						ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.Csoport_Id_Felelos
				UNION ALL
				SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll 
						ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
				UNION ALL										
				SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
					INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_KuldKuldemenyek.IraIratok_Id
				WHERE EREC_KuldKuldemenyek.IraIratok_Id IS NOT NULL
					AND EREC_IraIratok.Ugyirat_Id IN 
				(
					SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
						INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
					UNION ALL
					SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
						INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
					UNION ALL
					SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll 
							ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
					UNION ALL
					SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll 
							ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
					UNION ALL
					SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll 
							ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
					UNION ALL
					SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId,@FelhasznaloSzervezet_Id, 'EREC_UgyUgyiratok')
					UNION ALL
					SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
						INNER JOIN KRT_MappaTartalmak on KRT_MappaTartalmak.Obj_Id = erec_ugyugyiratok.Id
						INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
						INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
						INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
						where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
						and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
				)
				UNION ALL
				SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId,@FelhasznaloSzervezet_Id, 'EREC_KuldKuldemenyek')
				union all
				SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
					INNER JOIN KRT_MappaTartalmak on KRT_MappaTartalmak.Obj_Id = EREC_KuldKuldemenyek.Id
					INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
					where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
					and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
			)
		)
		and not exists
		(
			-- Érkeztetokönyv alapján
			SELECT TOP(1) 1										
			FROM EREC_Csatolmanyok
				INNER JOIN EREC_KuldKuldemenyek
					ON EREC_Csatolmanyok.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id										
				INNER JOIN krt_jogosultak 
					ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as CsoportTagokAll
					ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			WHERE EREC_Csatolmanyok.[Dokumentum_Id] = @Id
		 )
		AND not exists
		(
			-- Irat feladat alapján alapján
			SELECT TOP(1) 1
				FROM EREC_Csatolmanyok
				WHERE EREC_Csatolmanyok.IraIrat_Id IN (SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId,@FelhasznaloSzervezet_Id, 'EREC_IraIratok'))
				AND EREC_Csatolmanyok.[Dokumentum_Id] = @Id
		)


	BEGIN
		RAISERROR('[50102]',16,1)
	END

	--if dbo.fn_IsAdmin(@ExecutorUserId) = 0 and exists
	IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0 and exists
	(
			-- Bizalmas irat orzo vagy annak vezetoje alapján
			SELECT EREC_Csatolmanyok.Id
			FROM EREC_Csatolmanyok
				INNER JOIN EREC_IraIratok 
					ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
	and dbo.fn_CheckIrat_IsConfidential(EREC_IraIratok.Id) = 1
	and EREC_Csatolmanyok.[Dokumentum_Id] = @Id
-- végrehajtó felhasználó nem az orzo vagy annak vezetoje
	and dbo.fn_CheckIrat_ExecutorIsOrzoOrHisLeader(EREC_IraIratok.Id, @ExecutorUserId, @FelhasznaloSzervezet_Id) = 0	
	UNION ALL
			SELECT EREC_Csatolmanyok.Id
			FROM EREC_Csatolmanyok
				INNER JOIN EREC_KuldKuldemenyek 
					ON EREC_Csatolmanyok.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
	and EREC_Csatolmanyok.IraIrat_Id is null -- nincs iktatva
	and dbo.fn_CheckKuldemeny_IsConfidential(EREC_KuldKuldemenyek.Id) = 1
	and EREC_Csatolmanyok.[Dokumentum_Id] = @Id
-- végrehajtó felhasználó nem az orzo vagy annak vezetoje
	and dbo.fn_CheckKuldemeny_ExecutorIsOrzoOrHisLeader(EREC_KuldKuldemenyek.Id, @ExecutorUserId, @FelhasznaloSzervezet_Id) = 0	

	)
	BEGIN
		RAISERROR('[52792]',16,1) -- Önnek nincs joga, hogy megtekintse ezt az információt! A bizalmas iratnak nem Ön vagy az Ön beosztottja az orzoje.
	END

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Dokumentumok.Id,
	   KRT_Dokumentumok.Org,
	   KRT_Dokumentumok.FajlNev,
	   KRT_Dokumentumok.VerzioJel,
	   KRT_Dokumentumok.Nev,
	   KRT_Dokumentumok.Tipus,
	   KRT_Dokumentumok.Formatum,
	   KRT_Dokumentumok.Leiras,
	   KRT_Dokumentumok.Meret,
	   KRT_Dokumentumok.TartalomHash,
	   KRT_Dokumentumok.AlairtTartalomHash,
	   KRT_Dokumentumok.KivonatHash,
	   KRT_Dokumentumok.Dokumentum_Id,
	   KRT_Dokumentumok.Dokumentum_Id_Kovetkezo,
	   KRT_Dokumentumok.Alkalmazas_Id,
	   KRT_Dokumentumok.Csoport_Id_Tulaj,
	   KRT_Dokumentumok.KulsoAzonositok,
	   KRT_Dokumentumok.External_Source,
	   KRT_Dokumentumok.External_Link,
	   KRT_Dokumentumok.External_Id,
	   KRT_Dokumentumok.External_Info,
	   KRT_Dokumentumok.CheckedOut,
	   KRT_Dokumentumok.CheckedOutTime,
	   KRT_Dokumentumok.BarCode,
	   KRT_Dokumentumok.OCRPrioritas,
	   KRT_Dokumentumok.OCRAllapot,
	   KRT_Dokumentumok.Allapot,
	   KRT_Dokumentumok.WorkFlowAllapot,
	   KRT_Dokumentumok.Megnyithato,
	   KRT_Dokumentumok.Olvashato,
	   KRT_Dokumentumok.ElektronikusAlairas,
	   KRT_Dokumentumok.AlairasFelulvizsgalat,
	   KRT_Dokumentumok.Titkositas,
	   KRT_Dokumentumok.SablonAzonosito,
	   KRT_Dokumentumok.Ver,
	   KRT_Dokumentumok.Note,
	   KRT_Dokumentumok.Stat_id,
	   KRT_Dokumentumok.ErvKezd,
	   KRT_Dokumentumok.ErvVege,
	   KRT_Dokumentumok.Letrehozo_id,
	   KRT_Dokumentumok.LetrehozasIdo,
	   KRT_Dokumentumok.Modosito_id,
	   KRT_Dokumentumok.ModositasIdo,
	   KRT_Dokumentumok.Zarolo_Id,
	   KRT_Dokumentumok.ZarolasIdo,
	   KRT_Dokumentumok.Tranz_Id,
	   KRT_Dokumentumok.UIAccessLog_Id
	   from 
		 KRT_Dokumentumok as KRT_Dokumentumok 
	   where
		 KRT_Dokumentumok.Id = ''' + cast(@Id as nvarchar(40)) + '''
		 and KRT_Dokumentumok.Org=''' + cast(@Org as NVarChar(40)) + '''
';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	
	-- hibakezelés:
	EXEC usp_HandleException
 
END CATCH
end


GO
