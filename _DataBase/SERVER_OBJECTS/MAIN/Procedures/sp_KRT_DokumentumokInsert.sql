IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_DokumentumokInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_DokumentumokInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_DokumentumokInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_DokumentumokInsert] AS' 
END
GO

ALTER procedure [dbo].[sp_KRT_DokumentumokInsert]    
                @Id      uniqueidentifier = null,    
                           @Org     uniqueidentifier = null,    
	            @FajlNev     Nvarchar(400),
                @VerzioJel     Nvarchar(100)  = null,
                @Nev     Nvarchar(100)  = null,
	            @Tipus     Nvarchar(100),
                @Formatum     nvarchar(64)  = null,
                @Leiras     Nvarchar(400)  = null,
                @Meret     int  = null,
                @TartalomHash     Nvarchar(100)  = null,
                @AlairtTartalomHash     varbinary(4000)  = null,
                @KivonatHash     Nvarchar(100)  = null,
                @Dokumentum_Id     uniqueidentifier  = null,
                @Dokumentum_Id_Kovetkezo     uniqueidentifier  = null,
                @Alkalmazas_Id     uniqueidentifier  = null,
	            @Csoport_Id_Tulaj     uniqueidentifier,
                @KulsoAzonositok     Nvarchar(100)  = null,
                @External_Source     Nvarchar(100)  = null,
                @External_Link     Nvarchar(400)  = null,
                @External_Id     uniqueidentifier  = null,
                @External_Info     Nvarchar(4000)  = null,
                @CheckedOut     uniqueidentifier  = null,
                @CheckedOutTime     datetime  = null,
                @BarCode     Nvarchar(100)  = null,
                @OCRPrioritas     int  = null,
                @OCRAllapot     char(1)  = null,
	            @Allapot     nvarchar(64),
                @WorkFlowAllapot     nvarchar(64)  = null,
                @Megnyithato     char(1)  = null,
                @Olvashato     char(1)  = null,
                @ElektronikusAlairas     nvarchar(64)  = null,
                @AlairasFelulvizsgalat     datetime  = null,
                @Titkositas     char(1)  = null,
                @SablonAzonosito     Nvarchar(100)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_Id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_Id     uniqueidentifier  = null,
                @UIAccessLog_Id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
 IF @ORG is NULL
 BEGIN 
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@Letrehozo_id)
   if (@Org is null)
   begin
   	RAISERROR('[50302]',16,1)
   end
END
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Org is not null
         begin
            SET @insertColumns = @insertColumns + ',Org'
            SET @insertValues = @insertValues + ',@Org'
         end 
       
         if @FajlNev is not null
         begin
            SET @insertColumns = @insertColumns + ',FajlNev'
            SET @insertValues = @insertValues + ',@FajlNev'
         end 
       
         if @VerzioJel is not null
         begin
            SET @insertColumns = @insertColumns + ',VerzioJel'
            SET @insertValues = @insertValues + ',@VerzioJel'
         end 
       
         if @Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Nev'
            SET @insertValues = @insertValues + ',@Nev'
         end 
       
         if @Tipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Tipus'
            SET @insertValues = @insertValues + ',@Tipus'
         end 
       
         if @Formatum is not null
         begin
            SET @insertColumns = @insertColumns + ',Formatum'
            SET @insertValues = @insertValues + ',@Formatum'
         end 
       
         if @Leiras is not null
         begin
            SET @insertColumns = @insertColumns + ',Leiras'
            SET @insertValues = @insertValues + ',@Leiras'
         end 
       
         if @Meret is not null
         begin
            SET @insertColumns = @insertColumns + ',Meret'
            SET @insertValues = @insertValues + ',@Meret'
         end 
       
         if @TartalomHash is not null
         begin
            SET @insertColumns = @insertColumns + ',TartalomHash'
            SET @insertValues = @insertValues + ',@TartalomHash'
         end 
       
         if @AlairtTartalomHash is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairtTartalomHash'
            SET @insertValues = @insertValues + ',@AlairtTartalomHash'
         end 
       
         if @KivonatHash is not null
         begin
            SET @insertColumns = @insertColumns + ',KivonatHash'
            SET @insertValues = @insertValues + ',@KivonatHash'
         end 
       
         if @Dokumentum_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Dokumentum_Id'
            SET @insertValues = @insertValues + ',@Dokumentum_Id'
         end 
       
         if @Dokumentum_Id_Kovetkezo is not null
         begin
            SET @insertColumns = @insertColumns + ',Dokumentum_Id_Kovetkezo'
            SET @insertValues = @insertValues + ',@Dokumentum_Id_Kovetkezo'
         end 
       
         if @Alkalmazas_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Alkalmazas_Id'
            SET @insertValues = @insertValues + ',@Alkalmazas_Id'
         end 
       
         if @Csoport_Id_Tulaj is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Tulaj'
            SET @insertValues = @insertValues + ',@Csoport_Id_Tulaj'
         end 
       
         if @KulsoAzonositok is not null
         begin
            SET @insertColumns = @insertColumns + ',KulsoAzonositok'
            SET @insertValues = @insertValues + ',@KulsoAzonositok'
         end 
       
         if @External_Source is not null
         begin
            SET @insertColumns = @insertColumns + ',External_Source'
            SET @insertValues = @insertValues + ',@External_Source'
         end 
       
         if @External_Link is not null
         begin
            SET @insertColumns = @insertColumns + ',External_Link'
            SET @insertValues = @insertValues + ',@External_Link'
         end 
       
         if @External_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',External_Id'
            SET @insertValues = @insertValues + ',@External_Id'
         end 
       
         if @External_Info is not null
         begin
            SET @insertColumns = @insertColumns + ',External_Info'
            SET @insertValues = @insertValues + ',@External_Info'
         end 
       
         if @CheckedOut is not null
         begin
            SET @insertColumns = @insertColumns + ',CheckedOut'
            SET @insertValues = @insertValues + ',@CheckedOut'
         end 
       
         if @CheckedOutTime is not null
         begin
            SET @insertColumns = @insertColumns + ',CheckedOutTime'
            SET @insertValues = @insertValues + ',@CheckedOutTime'
         end 
       
         if @BarCode is not null
         begin
            SET @insertColumns = @insertColumns + ',BarCode'
            SET @insertValues = @insertValues + ',@BarCode'
         end 
       
         if @OCRPrioritas is not null
         begin
            SET @insertColumns = @insertColumns + ',OCRPrioritas'
            SET @insertValues = @insertValues + ',@OCRPrioritas'
         end 
       
         if @OCRAllapot is not null
         begin
            SET @insertColumns = @insertColumns + ',OCRAllapot'
            SET @insertValues = @insertValues + ',@OCRAllapot'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @WorkFlowAllapot is not null
         begin
            SET @insertColumns = @insertColumns + ',WorkFlowAllapot'
            SET @insertValues = @insertValues + ',@WorkFlowAllapot'
         end 
       
         if @Megnyithato is not null
         begin
            SET @insertColumns = @insertColumns + ',Megnyithato'
            SET @insertValues = @insertValues + ',@Megnyithato'
         end 
       
         if @Olvashato is not null
         begin
            SET @insertColumns = @insertColumns + ',Olvashato'
            SET @insertValues = @insertValues + ',@Olvashato'
         end 
       
         if @ElektronikusAlairas is not null
         begin
            SET @insertColumns = @insertColumns + ',ElektronikusAlairas'
            SET @insertValues = @insertValues + ',@ElektronikusAlairas'
         end 
       
         if @AlairasFelulvizsgalat is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairasFelulvizsgalat'
            SET @insertValues = @insertValues + ',@AlairasFelulvizsgalat'
         end 
       
         if @Titkositas is not null
         begin
            SET @insertColumns = @insertColumns + ',Titkositas'
            SET @insertValues = @insertValues + ',@Titkositas'
         end 
       
         if @SablonAzonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',SablonAzonosito'
            SET @insertValues = @insertValues + ',@SablonAzonosito'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_Id'
            SET @insertValues = @insertValues + ',@Zarolo_Id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_Id'
            SET @insertValues = @insertValues + ',@Tranz_Id'
         end 
       
         if @UIAccessLog_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_Id'
            SET @insertValues = @insertValues + ',@UIAccessLog_Id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into KRT_Dokumentumok ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Org uniqueidentifier,@FajlNev Nvarchar(400),@VerzioJel Nvarchar(100),@Nev Nvarchar(100),@Tipus Nvarchar(100),@Formatum nvarchar(64),@Leiras Nvarchar(400),@Meret int,@TartalomHash Nvarchar(100),@AlairtTartalomHash varbinary(4000),@KivonatHash Nvarchar(100),@Dokumentum_Id uniqueidentifier,@Dokumentum_Id_Kovetkezo uniqueidentifier,@Alkalmazas_Id uniqueidentifier,@Csoport_Id_Tulaj uniqueidentifier,@KulsoAzonositok Nvarchar(100),@External_Source Nvarchar(100),@External_Link Nvarchar(400),@External_Id uniqueidentifier,@External_Info Nvarchar(4000),@CheckedOut uniqueidentifier,@CheckedOutTime datetime,@BarCode Nvarchar(100),@OCRPrioritas int,@OCRAllapot char(1),@Allapot nvarchar(64),@WorkFlowAllapot nvarchar(64),@Megnyithato char(1),@Olvashato char(1),@ElektronikusAlairas nvarchar(64),@AlairasFelulvizsgalat datetime,@Titkositas char(1),@SablonAzonosito Nvarchar(100),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_Id uniqueidentifier,@ZarolasIdo datetime,@Tranz_Id uniqueidentifier,@UIAccessLog_Id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Org = @Org,@FajlNev = @FajlNev,@VerzioJel = @VerzioJel,@Nev = @Nev,@Tipus = @Tipus,@Formatum = @Formatum,@Leiras = @Leiras,@Meret = @Meret,@TartalomHash = @TartalomHash,@AlairtTartalomHash = @AlairtTartalomHash,@KivonatHash = @KivonatHash,@Dokumentum_Id = @Dokumentum_Id,@Dokumentum_Id_Kovetkezo = @Dokumentum_Id_Kovetkezo,@Alkalmazas_Id = @Alkalmazas_Id,@Csoport_Id_Tulaj = @Csoport_Id_Tulaj,@KulsoAzonositok = @KulsoAzonositok,@External_Source = @External_Source,@External_Link = @External_Link,@External_Id = @External_Id,@External_Info = @External_Info,@CheckedOut = @CheckedOut,@CheckedOutTime = @CheckedOutTime,@BarCode = @BarCode,@OCRPrioritas = @OCRPrioritas,@OCRAllapot = @OCRAllapot,@Allapot = @Allapot,@WorkFlowAllapot = @WorkFlowAllapot,@Megnyithato = @Megnyithato,@Olvashato = @Olvashato,@ElektronikusAlairas = @ElektronikusAlairas,@AlairasFelulvizsgalat = @AlairasFelulvizsgalat,@Titkositas = @Titkositas,@SablonAzonosito = @SablonAzonosito,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_Id = @Zarolo_Id,@ZarolasIdo = @ZarolasIdo,@Tranz_Id = @Tranz_Id,@UIAccessLog_Id = @UIAccessLog_Id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'KRT_Dokumentumok',@ResultUid
					,'KRT_DokumentumokHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH



GO
