IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_EsemenyekGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_EsemenyekGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_EsemenyekGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_EsemenyekGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_EsemenyekGetAllWithExtension]
   @Where nvarchar(4000) = '',
   @OrderBy nvarchar(200) = ' order by   KRT_Esemenyek.LetrehozasIdo',
   @TopRow nvarchar(5) = '',
   @Where_ObjTip nvarchar(4000) = '',
   @Where_Funkcio nvarchar(4000) = '',
   @Where_Helyettesitesek nvarchar(4000) = '',
   @ExecutorUserId            UNIQUEIDENTIFIER,
    @pageNumber      int = 0,
    @pageSize     int = -1,
    @SelectedRowId   uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on

   DECLARE @Org uniqueidentifier
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
   if (@Org is null)
   begin
      RAISERROR('[50202]',16,1)
   end

   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)
   declare @firstRow int
   declare @lastRow INT
   
   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
   if (@pageSize > 0 and @pageNumber > -1)
   begin
      set @firstRow = (@pageNumber)*@pageSize + 1
      set @lastRow = @firstRow + @pageSize - 1
   end
   else begin
      if (@TopRow = '' or @TopRow = '0')
      begin
         set @firstRow = 1
         set @lastRow = 1000
      end 
      else
      begin
         set @firstRow = 1
         set @lastRow = @TopRow
      end   
   end     
   set @sqlcmd = '';               

   /************************************************************
   * Szurési tábla összeállítása                      *
   ************************************************************/
   DECLARE @ObjTipId uniqueidentifier;
   select @ObjTipId = Id from KRT_Objtipusok where Kod = 'KRT_Esemenyek';

   SET @sqlcmd = 'select KRT_Esemenyek.Id into #filter from 
                                        KRT_Esemenyek as KRT_Esemenyek
                                          inner join KRT_Felhasznalok as KRT_Felhasznalok on KRT_Esemenyek.Felhasznalo_Id_User = KRT_Felhasznalok.Id and KRT_Felhasznalok.Org=''' + cast(@Org as NVarChar(40)) + '''
                                          inner join KRT_Felhasznalok as KRT_Felhasznalok1 on KRT_Esemenyek.Felhasznalo_Id_Login = KRT_Felhasznalok1.Id and KRT_Felhasznalok1.Org=''' + cast(@Org as NVarChar(40)) + '''
                                          inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_Esemenyek.Csoport_Id_FelelosUserSzerveze and KRT_Csoportok.Org=''' + cast(@Org as NVarChar(40)) + '''
                                          inner join KRT_ObjTipusok on KRT_ObjTipusok.Id = KRT_Esemenyek.ObjTip_Id
                                          inner join KRT_Funkciok as KRT_Funkciok on KRT_Esemenyek.Funkcio_Id = KRT_Funkciok.Id
                                          left join KRT_Helyettesitesek as KRT_Helyettesitesek on KRT_Esemenyek.Helyettesites_Id = KRT_Helyettesitesek.Id   
                                 where ' + @Where;

    IF @Where_ObjTip is not null and @Where_ObjTip! = ''
      set @sqlcmd = @sqlcmd + ' delete from #filter where Id NOT in (SELECT KRT_Esemenyek.Id FROM KRT_Esemenyek inner join KRT_ObjTipusok on KRT_ObjTipusok.Id = KRT_Esemenyek.ObjTip_Id WHERE ' + @Where_ObjTip + '  );'
      
   IF @Where_Funkcio is not null and @Where_Funkcio! = ''
    BEGIN
      SET @sqlcmd = @sqlcmd + N' delete from #filter where Id not in (SELECT KRT_Esemenyek.Id FROM KRT_Esemenyek inner join KRT_Funkciok as KRT_Funkciok on KRT_Esemenyek.Funkcio_Id = KRT_Funkciok.Id WHERE ' + @Where_Funkcio + '  );'
    END  

   IF @Where_Helyettesitesek is not null and @Where_Helyettesitesek! = ''
    BEGIN
-- itt inner join, ha van feltétel a helyettesítésre
      SET @sqlcmd = @sqlcmd + N' delete from #filter where Id not in (SELECT KRT_Esemenyek.Id FROM KRT_Esemenyek inner join KRT_Helyettesitesek as KRT_Helyettesitesek on KRT_Esemenyek.Helyettesites_Id = KRT_Helyettesitesek.Id WHERE ' + @Where_Helyettesitesek + '  );'
    END  
   exec sp_executesql @sqlcmd;


   /************************************************************
   * Szurt adatokhoz rendezés és sorszám összeállítása         *
   ************************************************************/
   SET @sqlcmd = @sqlcmd + N'
   select 
      row_number() over('+@OrderBy+') as RowNumber,
      KRT_Esemenyek.Id into #result
      from KRT_Esemenyek as KRT_Esemenyek
         inner join KRT_Felhasznalok as KRT_Felhasznalok on KRT_Esemenyek.Felhasznalo_Id_User = KRT_Felhasznalok.Id
         inner join KRT_Felhasznalok as KRT_Felhasznalok1 on KRT_Esemenyek.Felhasznalo_Id_Login = KRT_Felhasznalok1.Id
         inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_Esemenyek.Csoport_Id_FelelosUserSzerveze
         inner join KRT_ObjTipusok on KRT_ObjTipusok.Id = KRT_Esemenyek.ObjTip_Id
         inner join KRT_Funkciok as KRT_Funkciok on KRT_Esemenyek.Funkcio_Id = KRT_Funkciok.Id
         left join KRT_Helyettesitesek as KRT_Helyettesitesek on KRT_Esemenyek.Helyettesites_Id = KRT_Helyettesitesek.Id   
        where KRT_Esemenyek.Id in (select Id from #filter); '

      if (@SelectedRowId is not null)
         set @sqlcmd = @sqlcmd + N'
         if exists (select 1 from #result where Id = ''' + cast(@SelectedRowId as nvarchar(36)) + ''')
         BEGIN
            select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = ''' + cast(@SelectedRowId as nvarchar(36)) + ''';
            set @firstRow = (@pageNumber - 1) * @pageSize + 1;
            set @lastRow = @pageNumber*@pageSize;
            select @pageNumber = @pageNumber - 1
         END
         ELSE'
      --ELSE
         set @sqlcmd = @sqlcmd + N' 
            if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
            BEGIN
               select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
               set @firstRow = (@pageNumber - 1) * @pageSize + 1;
               set @lastRow = @pageNumber*@pageSize;
               select @pageNumber = @pageNumber - 1
            END ;'



   /************************************************************
   * Tényleges select                                 *
   ************************************************************/
   set @sqlcmd = @sqlcmd + N'
      select 
      #result.RowNumber,
      #result.Id,

      KRT_Esemenyek.Obj_Id,
      KRT_Esemenyek.ObjTip_Id,
KRT_ObjTipusok.Nev as ObjTip_Id_Nev,
      KRT_Esemenyek.Azonositoja,
      KRT_Esemenyek.Felhasznalo_Id_User,
KRT_Felhasznalok.Nev as Felhasznalo_Id_User_Nev,
      KRT_Esemenyek.Csoport_Id_FelelosUserSzerveze,
KRT_Csoportok.Nev as Csoport_Id_FelelosUserSzerveze_Nev,
      KRT_Esemenyek.Felhasznalo_Id_Login,
KRT_Felhasznalok1.Nev as Felhasznalo_Id_Login_Nev,
      KRT_Esemenyek.Csoport_Id_Cel,
      KRT_Esemenyek.Helyettesites_Id,
KRT_Helyettesitesek.HelyettesitesMod as HelyettesitesMod,
(select Nev from KRT_KodTarak where KRT_KodTarak.Kod=KRT_Helyettesitesek.HelyettesitesMod and KRT_KodTarak.KodCsoport_Id in (select Id from KRT_KodCsoportok where Kod=''HELYETTESITES_MOD'') and KRT_KodTarak.Org=''' + cast(@Org as NVarChar(40)) + ''') as HelyettesitesMod_Nev,
      KRT_Esemenyek.Tranzakcio_Id,
      KRT_Esemenyek.Funkcio_Id,
KRT_Funkciok.Nev as Funkciok_Nev,
      KRT_Esemenyek.Ver,
      KRT_Esemenyek.Note,
      KRT_Esemenyek.Stat_id,
      KRT_Esemenyek.ErvKezd,
      KRT_Esemenyek.ErvVege,
      KRT_Esemenyek.Letrehozo_id,
KRT_Esemenyek.LetrehozasIdo,
      KRT_Esemenyek.Modosito_id,
      KRT_Esemenyek.ModositasIdo,
      KRT_Esemenyek.Zarolo_id,
      KRT_Esemenyek.ZarolasIdo,
      KRT_Esemenyek.Tranz_id,
      KRT_Esemenyek.UIAccessLog_id,
      KRT_Esemenyek.Munkaallomas,
      KRT_Esemenyek.MuveletKimenete
   from 
     KRT_Esemenyek as KRT_Esemenyek
         inner join #result on #result.Id = KRT_Esemenyek.Id            
inner join KRT_Felhasznalok as KRT_Felhasznalok on KRT_Esemenyek.Felhasznalo_Id_User = KRT_Felhasznalok.Id
inner join KRT_Felhasznalok as KRT_Felhasznalok1 on KRT_Esemenyek.Felhasznalo_Id_Login = KRT_Felhasznalok1.Id 
inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_Esemenyek.Csoport_Id_FelelosUserSzerveze
inner join KRT_ObjTipusok on KRT_ObjTipusok.Id = KRT_Esemenyek.ObjTip_Id
inner join KRT_Funkciok as KRT_Funkciok on KRT_Esemenyek.Funkcio_Id = KRT_Funkciok.Id
left join KRT_Helyettesitesek as KRT_Helyettesitesek on KRT_Esemenyek.Helyettesites_Id = KRT_Helyettesitesek.Id     
               where RowNumber between @firstRow and @lastRow
   ORDER BY #result.RowNumber;'

      -- találatok száma és oldalszám
      set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter;';

      execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber;

END TRY
BEGIN CATCH
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
