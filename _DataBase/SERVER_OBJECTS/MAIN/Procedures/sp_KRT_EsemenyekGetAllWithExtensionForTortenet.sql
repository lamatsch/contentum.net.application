IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_EsemenyekGetAllWithExtensionForTortenet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_EsemenyekGetAllWithExtensionForTortenet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_EsemenyekGetAllWithExtensionForTortenet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_EsemenyekGetAllWithExtensionForTortenet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_EsemenyekGetAllWithExtensionForTortenet]
	@ObjId UNIQUEIDENTIFIER,
	@Where nvarchar(MAX) = '', -- KRT_Esemenyekre vonatkozó feltétel
	@OrderBy nvarchar(200) = ' order by   KRT_Esemenyek.LetrehozasIdo',
	@TopRow nvarchar(5) = '',
	@ExecutorUserId		UNIQUEIDENTIFIER,
    @pageNumber		int = 0,
    @pageSize		int = -1,
    @SelectedRowId	uniqueidentifier = NULL

as

begin
BEGIN TRY
   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000);
   DECLARE @tableName NVARCHAR(100);

	IF @Where is null
	BEGIN
		set @Where = ''
	END
   
   SELECT TOP 1 @tableName = KRT_ObjTipusok.Kod
	FROM KRT_Esemenyek
		INNER JOIN KRT_ObjTipusok ON KRT_Esemenyek.ObjTip_Id = KRT_ObjTipusok.Id
		AND KRT_ObjTipusok.Kod IN ('EREC_IraIratok','EREC_UgyUgyiratok','EREC_PldIratpeldanyok','EREC_KuldKuldemenyek')
	WHERE KRT_Esemenyek.Obj_Id = @ObjId;
	
	IF(@tableName = 'EREC_IraIratok' OR @tableName = 'EREC_UgyUgyiratok' OR @tableName = 'EREC_PldIratpeldanyok' OR @tableName='EREC_KuldKuldemenyek')
	BEGIN
	
	SET @sqlcmd = N'EXEC sp_' + @tableName + 'GetAllEsemenyWithExtension ''' + CAST(@ObjId AS CHAR(36)) + ''', @Where = ''' + replace(@Where, '''', '''''') + ''', @OrderBy = ''' + @OrderBy + ''', @TopRow = ''' + @TopRow + ''', @ExecutorUserId = ''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', @pageNumber = ' + CAST(@pageNumber AS NVARCHAR(5)) + ', @pageSize = ' + CAST(@pageSize AS NVARCHAR(5));
	IF @SelectedRowId IS NOT NULL
		SET @sqlcmd = @sqlcmd + ', @SelectedRowId=''' + CAST(@SelectedRowId AS CHAR(36)) + '''';

	PRINT @sqlcmd;
	EXEC (@sqlcmd);
	
	END
	ELSE
	BEGIN
		DECLARE @concWhere NVARCHAR(MAX);
		SET @concWhere = ' (Obj_Id = ''' + CAST(@ObjId AS NVARCHAR(36)) + ''')';

		IF (@Where is not null and @Where != '')
			SET @concWhere = @concWhere + ' and ' + @Where; 
		
		EXEC sp_KRT_EsemenyekGetAllWithExtension
			@where = @concWhere,
			@OrderBy = @OrderBy,
			@TopRow = @TopRow,
			@ExecutorUserId = @ExecutorUserId,
			@pageNumber = @pageNumber,
			@pageSize = @pageSize,
			@SelectedRowId = @SelectedRowId
	END
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
