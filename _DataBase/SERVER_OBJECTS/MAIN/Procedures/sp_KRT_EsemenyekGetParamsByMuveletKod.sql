IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_EsemenyekGetParamsByMuveletKod]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_EsemenyekGetParamsByMuveletKod]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_EsemenyekGetParamsByMuveletKod]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_EsemenyekGetParamsByMuveletKod] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_KRT_EsemenyekGetParamsByMuveletKod]
	@ObjId		uniqueidentifier,
	@TablaNev	nvarchar(400),
	@MuveletKod	nvarchar(400)
AS
BEGIN
	declare @ObjTipId	uniqueidentifier;
	declare @FunkcioId	uniqueidentifier;
	declare @Azonosito	nvarchar(400);
	declare @MuveletId	uniqueidentifier;

	select @ObjTipId = KRT_ObjTipusok.Id from KRT_ObjTipusok where KRT_ObjTipusok.Kod = @TablaNev
	select @MuveletId = KRT_Muveletek.Id from KRT_Muveletek where KRT_Muveletek.Kod = @MuveletKod
	select @FunkcioId = KRT_Funkciok.Id from KRT_Funkciok where KRT_Funkciok.ObjTipus_Id_Adatelem = @ObjTipId AND KRT_Funkciok.Muvelet_Id = @MuveletId

	if (@ObjId is not null)
	BEGIN
		declare @sqlcmd	nvarchar(max);
		set @sqlcmd = N' select @ObjTipId as ObjTipId,@FunkcioId as FunkcioId,'
		-- Ellenorzés, van-e az adatbázisban @Tabla nevu tábla
		if exists(select 1 from sys.objects where object_id = OBJECT_ID(@Tablanev) and type='U')
		begin
			-- Ellenorzés, van-e az adatbázisban hozzátartozó GetAzonosito fv.
			if exists(select * from sys.objects where object_id = OBJECT_ID(N'dbo.fn_Get' + @Tablanev + N'Azonosito')
				and type in (N'FN', N'IF', N'TF', N'FS', N'FT'))			
			begin
				set @sqlcmd = @sqlcmd+'dbo.fn_Get'+@TablaNev+'Azonosito('''+cast(@ObjId as nvarchar(40))+''') as Azonosito'
			end
			else
			begin
				set @sqlcmd = @sqlcmd+'''[?' + @TablaNev + ': GetAzonosito?]'' as Azonosito'
				raiserror('Function does not exist in the database: dbo.fn_Get%sAzonosito', 5, 1, @TablaNev)
			end
		end
		else
		begin
			set @sqlcmd = @sqlcmd+'''[?' + @TablaNev + '?]'' as Azonosito'
			raiserror('Table does not exist in the database: ''%s''', 5, 1, @TablaNev)
		end
		exec sp_executesql @sqlcmd,N'@ObjTipId uniqueidentifier,@FunkcioId uniqueidentifier',@ObjTipId=@ObjTipId,@FunkcioId=@FunkcioId
	END
	else
	BEGIN
		select @ObjTipId as ObjTipId, @FunkcioId  as FunkcioId, null as Azonosito;
	END

END


GO
