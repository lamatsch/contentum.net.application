IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_EsemenyekInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_EsemenyekInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_EsemenyekInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_EsemenyekInsert] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_EsemenyekInsert]    
                @Id      uniqueidentifier = null,    
                               @Obj_Id     uniqueidentifier  = null,
                @ObjTip_Id     uniqueidentifier  = null,
                @Azonositoja     Nvarchar(400)  = null,
               @Felhasznalo_Id_User     uniqueidentifier,
                @Csoport_Id_FelelosUserSzerveze     uniqueidentifier  = null,
               @Felhasznalo_Id_Login     uniqueidentifier,
                @Csoport_Id_Cel     uniqueidentifier  = null,
                @Helyettesites_Id     uniqueidentifier  = null,
                @Tranzakcio_Id     uniqueidentifier  = null,
                @Funkcio_Id     uniqueidentifier  = null,
                @CsoportHierarchia     Nvarchar(4000)  = null,
                @KeresesiFeltetel     Nvarchar(4000)  = null,
                @TalalatokSzama     int  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
               @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,
                @Munkaallomas     Nvarchar(100)  = null,
                @MuveletKimenete     Nvarchar(100)  = null,   

      @UpdatedColumns              xml = null,
      @ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Obj_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Obj_Id'
            SET @insertValues = @insertValues + ',@Obj_Id'
         end 
       
         if @ObjTip_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',ObjTip_Id'
            SET @insertValues = @insertValues + ',@ObjTip_Id'
         end 
       
         if @Azonositoja is not null
         begin
            SET @insertColumns = @insertColumns + ',Azonositoja'
            SET @insertValues = @insertValues + ',@Azonositoja'
         end 
       
         if @Felhasznalo_Id_User is not null
         begin
            SET @insertColumns = @insertColumns + ',Felhasznalo_Id_User'
            SET @insertValues = @insertValues + ',@Felhasznalo_Id_User'
         end 
       
         if @Csoport_Id_FelelosUserSzerveze is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_FelelosUserSzerveze'
            SET @insertValues = @insertValues + ',@Csoport_Id_FelelosUserSzerveze'
         end 
       
         if @Felhasznalo_Id_Login is not null
         begin
            SET @insertColumns = @insertColumns + ',Felhasznalo_Id_Login'
            SET @insertValues = @insertValues + ',@Felhasznalo_Id_Login'
         end 
       
         if @Csoport_Id_Cel is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Cel'
            SET @insertValues = @insertValues + ',@Csoport_Id_Cel'
         end

         if @Helyettesites_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Helyettesites_Id'
            SET @insertValues = @insertValues + ',@Helyettesites_Id'
         end 
       
         if @Tranzakcio_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranzakcio_Id'
            SET @insertValues = @insertValues + ',@Tranzakcio_Id'
         end 
       
         if @Funkcio_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Funkcio_Id'
            SET @insertValues = @insertValues + ',@Funkcio_Id'
         end 
       
         if @CsoportHierarchia is not null
         begin
            SET @insertColumns = @insertColumns + ',CsoportHierarchia'
            SET @insertValues = @insertValues + ',@CsoportHierarchia'
         end 
       
         if @KeresesiFeltetel is not null
         begin
            SET @insertColumns = @insertColumns + ',KeresesiFeltetel'
            SET @insertValues = @insertValues + ',@KeresesiFeltetel'
         end 
       
         if @TalalatokSzama is not null
         begin
            SET @insertColumns = @insertColumns + ',TalalatokSzama'
            SET @insertValues = @insertValues + ',@TalalatokSzama'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
       
         if @Munkaallomas is not null
         begin
            SET @insertColumns = @insertColumns + ',Munkaallomas'
            SET @insertValues = @insertValues + ',@Munkaallomas'
         end    
       
         if @MuveletKimenete is not null
         begin
            SET @insertColumns = @insertColumns + ',MuveletKimenete'
            SET @insertValues = @insertValues + ',@MuveletKimenete'
         end    

IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into KRT_Esemenyek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
N'@Id uniqueidentifier,@Obj_Id uniqueidentifier,@ObjTip_Id uniqueidentifier,@Azonositoja Nvarchar(400),@Felhasznalo_Id_User uniqueidentifier,@Csoport_Id_FelelosUserSzerveze uniqueidentifier,@Felhasznalo_Id_Login uniqueidentifier,@Csoport_Id_Cel uniqueidentifier,@Helyettesites_Id uniqueidentifier,@Tranzakcio_Id uniqueidentifier,@Funkcio_Id uniqueidentifier,@CsoportHierarchia Nvarchar(4000),@KeresesiFeltetel Nvarchar(4000),@TalalatokSzama int,@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@Munkaallomas Nvarchar(100),@MuveletKimenete Nvarchar(100),@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Obj_Id = @Obj_Id,@ObjTip_Id = @ObjTip_Id,@Azonositoja = @Azonositoja,@Felhasznalo_Id_User = @Felhasznalo_Id_User,@Csoport_Id_FelelosUserSzerveze = @Csoport_Id_FelelosUserSzerveze,@Felhasznalo_Id_Login = @Felhasznalo_Id_Login,@Csoport_Id_Cel = @Csoport_Id_Cel,@Helyettesites_Id = @Helyettesites_Id,@Tranzakcio_Id = @Tranzakcio_Id,@Funkcio_Id = @Funkcio_Id,@CsoportHierarchia = @CsoportHierarchia,@KeresesiFeltetel = @KeresesiFeltetel,@TalalatokSzama = @TalalatokSzama,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@Munkaallomas = @Munkaallomas ,@MuveletKimenete = @MuveletKimenete ,@ResultUid = @ResultUid OUTPUT

IF EXISTS (SELECT 1 FROM KRT_Funkciok WHERE KRT_Funkciok.FeladatJelzo = '1' AND KRT_Funkciok.Id = ISNULL(@Funkcio_Id, NEWID()))
BEGIN
    DECLARE @Funkcio_Kod_Futtatando nvarchar(100)
    DECLARE @NextFeladatSorszam     SMALLINT
   EXEC sp_WF_EsemenyTrigger @ResultUid, @Obj_Id, @Funkcio_Id,1
END

if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END

--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
