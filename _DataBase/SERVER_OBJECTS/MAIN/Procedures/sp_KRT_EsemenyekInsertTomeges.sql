IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_EsemenyekInsertTomeges]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_EsemenyekInsertTomeges]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_EsemenyekInsertTomeges]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_EsemenyekInsertTomeges] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_KRT_EsemenyekInsertTomeges]
	@ObjIds					nvarchar(MAX),
	@TablaNev				nvarchar(400),
	@MuveletKod				nvarchar(400),
	@FelhasznaloId			uniqueidentifier,
	@HelyettesitettId		uniqueidentifier,
	@HelyettesitesId		uniqueidentifier = NULL,
	@FelettesSzervezetId	uniqueidentifier = NULL,
	@tranzId				uniqueidentifier = NULL,
	@munkaallomas			nvarchar(100) = NULL,
	@MuveletKimenete     nvarchar(100)  = null
AS
BEGIN
	declare @ObjTipId	uniqueidentifier;
	declare @FunkcioId	uniqueidentifier;
	--declare @Azonosito	nvarchar(400);
	declare @MuveletId	uniqueidentifier;

	select @ObjTipId = KRT_ObjTipusok.Id from KRT_ObjTipusok where KRT_ObjTipusok.Kod = @TablaNev
	select @MuveletId = KRT_Muveletek.Id from KRT_Muveletek where KRT_Muveletek.Kod = @MuveletKod
	select @FunkcioId = KRT_Funkciok.Id from KRT_Funkciok where KRT_Funkciok.ObjTipus_Id_Adatelem = @ObjTipId AND KRT_Funkciok.Muvelet_Id = @MuveletId

	declare @sqlcmd	nvarchar(max);
	set @sqlcmd = N'
		declare @tempTable table(id uniqueidentifier);
		declare @it	int;
		declare @curId	nvarchar(36);
		set @it = 0;
		while (@it < ((len(@ObjIds)+1) / 39))
		BEGIN
			set @curId = SUBSTRING(@ObjIds,@it*39+2,37);
			insert into @tempTable(id) values(@curId);
			set @it = @it + 1;
		END

		--declare @tranzId uniqueidentifier;
		if @tranzId IS NULL
		BEGIN 
			set @tranzId = newid();
		END
		DECLARE @InsertedRows TABLE (id uniqueidentifier)
		insert into KRT_Esemenyek(
				Obj_Id,
				ObjTip_Id,
				Azonositoja,
				Felhasznalo_Id_User,
				Csoport_Id_FelelosUserSzerveze,
				Felhasznalo_Id_Login,
				Helyettesites_Id,
				Funkcio_Id,
				Tranzakcio_Id,
				Letrehozo_Id,
				MunkaAllomas,
				MuveletKimenete) output inserted.id into @InsertedRows
			select
				t.id,
				@ObjTipId,
				dbo.fn_Get'+@TablaNev+N'Azonosito(t.id),
				@FelhasznaloId,
				@FelettesSzervezetId,
				@HelyettesitettId,
				@HelyettesitesId,
				@FunkcioId,
				@tranzId,
				@FelhasznaloId,
				@munkaallomas,
				@MuveletKimenete
			from @tempTable t
			
			IF EXISTS (SELECT 1 FROM KRT_Funkciok WHERE KRT_Funkciok.FeladatJelzo = ''1'' AND KRT_Funkciok.Id = ISNULL(@FunkcioId, NEWID()))
			BEGIN
				DECLARE @Esemeny_Id UNIQUEIDENTIFIER
				DECLARE @Obj_Id uniqueidentifier

				DECLARE EsemenyekCursor CURSOR LOCAL FAST_FORWARD READ_ONLY FOR
				SELECT Id, Obj_Id from KRT_Esemenyek
				WHERE Id IN (SELECT Id FROM @InsertedRows)

				OPEN EsemenyekCursor

				FETCH NEXT FROM EsemenyekCursor INTO @Esemeny_Id, @Obj_Id

				WHILE @@FETCH_STATUS = 0
				BEGIN
				
				EXEC sp_WF_EsemenyTrigger @Esemeny_Id, @Obj_Id, @FunkcioId,1
				
				FETCH NEXT FROM EsemenyekCursor INTO @Esemeny_Id, @Obj_Id

				END

				CLOSE EsemenyekCursor
				DEALLOCATE EsemenyekCursor
		   END'
	--print @sqlcmd
	exec sp_executesql @sqlcmd,N'@ObjTipId uniqueidentifier,@FunkcioId uniqueidentifier,@ObjIds nvarchar(MAX),@tranzId uniqueidentifier
		, @FelhasznaloId uniqueidentifier, @FelettesSzervezetId uniqueidentifier, @HelyettesitettId uniqueidentifier, @HelyettesitesId uniqueidentifier, @munkaallomas nvarchar(100), @MuveletKimenete nvarchar(100)'
		,@ObjTipId=@ObjTipId,@FunkcioId=@FunkcioId,@ObjIds=@ObjIds, @tranzId = @tranzId
		, @FelhasznaloId = @FelhasznaloId, @FelettesSzervezetId = @FelettesSzervezetId, @HelyettesitettId = @HelyettesitettId, @HelyettesitesId = @HelyettesitesId, @munkaallomas=@munkaallomas, @MuveletKimenete=@MuveletKimenete


END


GO
