IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Extra_NapokKovetkezoMunkanap]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Extra_NapokKovetkezoMunkanap]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Extra_NapokKovetkezoMunkanap]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Extra_NapokKovetkezoMunkanap] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Extra_NapokKovetkezoMunkanap]
		 @DatumTol DATETIME = NULL,
		 @Sorszam INT = NULL,
         @ExecutorUserId UNIQUEIDENTIFIER = NULL

         
as

begin
BEGIN TRY

	set nocount ON
	
	SELECT dbo.[fn_kovetkezo_munkanap](@DatumTol, @Sorszam)
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
