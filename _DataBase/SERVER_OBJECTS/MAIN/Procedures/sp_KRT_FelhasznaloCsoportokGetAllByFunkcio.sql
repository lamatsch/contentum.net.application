IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FelhasznaloCsoportokGetAllByFunkcio]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_FelhasznaloCsoportokGetAllByFunkcio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FelhasznaloCsoportokGetAllByFunkcio]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_FelhasznaloCsoportokGetAllByFunkcio] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_FelhasznaloCsoportokGetAllByFunkcio]
  @FunkcioKod nvarchar(100),
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by KRT_Csoportok.Nev',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'select distinct ' + @LocalTopRow + '
  	    KRT_Csoportok.Id,
	   KRT_Csoportok.Org,
	   KRT_Csoportok.Kod,
	   KRT_Csoportok.Nev,
	   KRT_Csoportok.Tipus,
	   KRT_Csoportok.Jogalany,
	   KRT_Csoportok.ErtesitesEmail,
	   KRT_Csoportok.System,
	   KRT_Csoportok.Adatforras,
	   KRT_Csoportok.ObjTipus_Id_Szulo,
	   KRT_Csoportok.Kiszolgalhato,
	   KRT_Csoportok.JogosultsagOroklesMod,
	   KRT_Csoportok.Ver,
	   KRT_Csoportok.Note,
	   KRT_Csoportok.Stat_id,
	   KRT_Csoportok.ErvKezd,
	   KRT_Csoportok.ErvVege,
	   KRT_Csoportok.Letrehozo_id,
	   KRT_Csoportok.LetrehozasIdo,
	   KRT_Csoportok.Modosito_id,
	   KRT_Csoportok.ModositasIdo,
	   KRT_Csoportok.Zarolo_id,
	   KRT_Csoportok.ZarolasIdo,
	   KRT_Csoportok.Tranz_id,
	   KRT_Csoportok.UIAccessLog_id  
   from 
     KRT_Funkciok as KRT_Funkciok
	 JOIN KRT_Szerepkor_Funkcio as KRT_Szerepkor_Funkcio
		ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id
	JOIN KRT_Szerepkorok as KRT_Szerepkorok
		ON KRT_Szerepkorok.Id = KRT_Szerepkor_Funkcio.Szerepkor_Id
	JOIN KRT_Felhasznalo_Szerepkor as KRT_Felhasznalo_Szerepkor
		ON KRT_Felhasznalo_Szerepkor.Szerepkor_Id = KRT_Szerepkorok.Id
	JOIN KRT_Csoportok as KRT_Csoportok
		ON KRT_Csoportok.Id = KRT_Felhasznalo_Szerepkor.Felhasznalo_Id
	WHERE 
		KRT_Funkciok.Kod = ''' + @FunkcioKod + '''
		and getdate() between KRT_Funkciok.ErvKezd and KRT_Funkciok.ErvVege
		and getdate() between KRT_Szerepkor_Funkcio.ErvKezd and KRT_Szerepkor_Funkcio.ErvVege
		and getdate() between KRT_Szerepkorok.ErvKezd and KRT_Szerepkorok.ErvVege
		and getdate() between KRT_Felhasznalo_Szerepkor.ErvKezd and KRT_Felhasznalo_Szerepkor.ErvVege
		and getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege
		and KRT_Szerepkorok.Org=''' + cast(@Org as NVarChar(40)) + '''
		and KRT_Csoportok.Org=''' + cast(@Org as NVarChar(40)) + '''
 '
     
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
   
   SET @sqlcmd = @sqlcmd + @OrderBy
 
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
