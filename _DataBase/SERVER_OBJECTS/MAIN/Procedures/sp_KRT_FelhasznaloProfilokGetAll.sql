IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FelhasznaloProfilokGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_FelhasznaloProfilokGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FelhasznaloProfilokGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_FelhasznaloProfilokGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_FelhasznaloProfilokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_FelhasznaloProfilok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_FelhasznaloProfilok.Id,
	   KRT_FelhasznaloProfilok.Org,
	   KRT_FelhasznaloProfilok.Tipus,
	   KRT_FelhasznaloProfilok.Felhasznalo_id,
	   KRT_FelhasznaloProfilok.Obj_Id,
	   KRT_FelhasznaloProfilok.Nev,
	   KRT_FelhasznaloProfilok.XML,
	   KRT_FelhasznaloProfilok.Ver,
	   KRT_FelhasznaloProfilok.Note,
	   KRT_FelhasznaloProfilok.Stat_id,
	   KRT_FelhasznaloProfilok.ErvKezd,
	   KRT_FelhasznaloProfilok.ErvVege,
	   KRT_FelhasznaloProfilok.Letrehozo_id,
	   KRT_FelhasznaloProfilok.LetrehozasIdo,
	   KRT_FelhasznaloProfilok.Modosito_id,
	   KRT_FelhasznaloProfilok.ModositasIdo,
	   KRT_FelhasznaloProfilok.Zarolo_id,
	   KRT_FelhasznaloProfilok.ZarolasIdo,
	   KRT_FelhasznaloProfilok.Tranz_id,
	   KRT_FelhasznaloProfilok.UIAccessLog_id  
   from 
     KRT_FelhasznaloProfilok as KRT_FelhasznaloProfilok      
            Where KRT_FelhasznaloProfilok.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
