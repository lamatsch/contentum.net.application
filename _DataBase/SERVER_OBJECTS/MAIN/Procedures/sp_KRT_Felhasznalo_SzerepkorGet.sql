IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Felhasznalo_SzerepkorGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Felhasznalo_SzerepkorGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Felhasznalo_SzerepkorGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Felhasznalo_SzerepkorGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Felhasznalo_SzerepkorGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Felhasznalo_Szerepkor.Id,
	   KRT_Felhasznalo_Szerepkor.CsoportTag_Id,
	   KRT_Felhasznalo_Szerepkor.Csoport_Id,
	   KRT_Felhasznalo_Szerepkor.Felhasznalo_Id,
	   KRT_Felhasznalo_Szerepkor.Szerepkor_Id,
	   KRT_Felhasznalo_Szerepkor.Helyettesites_Id,
	   KRT_Felhasznalo_Szerepkor.Ver,
	   KRT_Felhasznalo_Szerepkor.Note,
	   KRT_Felhasznalo_Szerepkor.Stat_id,
	   KRT_Felhasznalo_Szerepkor.ErvKezd,
	   KRT_Felhasznalo_Szerepkor.ErvVege,
	   KRT_Felhasznalo_Szerepkor.Letrehozo_id,
	   KRT_Felhasznalo_Szerepkor.LetrehozasIdo,
	   KRT_Felhasznalo_Szerepkor.Modosito_id,
	   KRT_Felhasznalo_Szerepkor.ModositasIdo,
	   KRT_Felhasznalo_Szerepkor.Zarolo_id,
	   KRT_Felhasznalo_Szerepkor.ZarolasIdo,
	   KRT_Felhasznalo_Szerepkor.Tranz_id,
	   KRT_Felhasznalo_Szerepkor.UIAccessLog_id
	   from 
		 KRT_Felhasznalo_Szerepkor as KRT_Felhasznalo_Szerepkor 
	   where
		 KRT_Felhasznalo_Szerepkor.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
