IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Felhasznalo_SzerepkorGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Felhasznalo_SzerepkorGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Felhasznalo_SzerepkorGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Felhasznalo_SzerepkorGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Felhasznalo_SzerepkorGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by KRT_Szerepkorok.Nev',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Felhasznalo_Szerepkor.Id,
	   KRT_Felhasznalo_Szerepkor.Felhasznalo_Id,
	   KRT_Felhasznalo_Szerepkor.Szerepkor_Id,
	   KRT_Felhasznalo_Szerepkor.Csoport_Id,
	   KRT_Felhasznalo_Szerepkor.CsoportTag_Id,
	   KRT_Szerepkorok.Nev as Szerepkor_Nev,
	   KRT_Felhasznalok.Nev as Felhasznalo_Nev,
       KRT_Felhasznalo_Szerepkor.Helyettesites_Id,
       KRT_Helyettesitesek.CsoportTag_Id_helyettesitett,
       KRT_Csoportok_Megbizo.Nev as Csoport_Nev_helyettesitett,
	   KRT_Felhasznalo_Szerepkor.Ver,
	   KRT_Felhasznalo_Szerepkor.Note,
	   KRT_Felhasznalo_Szerepkor.Stat_id,
	   KRT_Felhasznalo_Szerepkor.ErvKezd,
	   KRT_Felhasznalo_Szerepkor.ErvVege,
	   KRT_Felhasznalo_Szerepkor.Letrehozo_id,
	   KRT_Felhasznalo_Szerepkor.LetrehozasIdo,
	   KRT_Felhasznalo_Szerepkor.Modosito_id,
	   KRT_Felhasznalo_Szerepkor.ModositasIdo,
	   KRT_Felhasznalo_Szerepkor.Zarolo_id,
	   KRT_Felhasznalo_Szerepkor.ZarolasIdo,
	   KRT_Felhasznalo_Szerepkor.Tranz_id,
	   KRT_Felhasznalo_Szerepkor.UIAccessLog_id  
   from 
     KRT_Felhasznalo_Szerepkor as KRT_Felhasznalo_Szerepkor
	 left join KRT_Csoportok as KRT_Csoportok on KRT_Felhasznalo_Szerepkor.Csoport_Id = KRT_Csoportok.Id
	 left outer join KRT_Felhasznalok as KRT_Felhasznalok on KRT_Felhasznalo_Szerepkor.Felhasznalo_Id = KRT_Felhasznalok.Id and KRT_Felhasznalok.Org=''' + cast(@Org as NVarChar(40))+ ''' 
	 left outer join KRT_Szerepkorok as KRT_Szerepkorok on KRT_Felhasznalo_Szerepkor.Szerepkor_Id = KRT_Szerepkorok.Id and KRT_Szerepkorok.Org=''' + cast(@Org as NVarChar(40)) + '''
     left join KRT_Helyettesitesek on KRT_Helyettesitesek.Id = KRT_Felhasznalo_Szerepkor.Helyettesites_Id
     left join KRT_CsoportTagok on KRT_Helyettesitesek.CsoportTag_ID_helyettesitett = KRT_CsoportTagok.Id
     left join KRT_Csoportok as KRT_Csoportok_Megbizo on KRT_Csoportok_Megbizo.Id = KRT_CsoportTagok.Csoport_Id
'
     
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' where ' + @Where
	end
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
