IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Felhasznalo_SzerepkorHistoryGetAllRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Felhasznalo_SzerepkorHistoryGetAllRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Felhasznalo_SzerepkorHistoryGetAllRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Felhasznalo_SzerepkorHistoryGetAllRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Felhasznalo_SzerepkorHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from KRT_Felhasznalo_SzerepkorHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Felhasznalo_SzerepkorHistory Old
         inner join KRT_Felhasznalo_SzerepkorHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CsoportTag_Id' as ColumnName,               cast(Old.CsoportTag_Id as nvarchar(99)) as OldValue,
               cast(New.CsoportTag_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Felhasznalo_SzerepkorHistory Old
         inner join KRT_Felhasznalo_SzerepkorHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.CsoportTag_Id != New.CsoportTag_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Felhasznalo_SzerepkorHistory Old
         inner join KRT_Felhasznalo_SzerepkorHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id != New.Csoport_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.Csoport_Id --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Csoport_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felhasznalo_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_FelhasznalokAzonosito(Old.Felhasznalo_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_FelhasznalokAzonosito(New.Felhasznalo_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Felhasznalo_SzerepkorHistory Old
         inner join KRT_Felhasznalo_SzerepkorHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Felhasznalo_Id != New.Felhasznalo_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Felhasznalok FTOld on FTOld.Id = Old.Felhasznalo_Id --and FTOld.Ver = Old.Ver
         left join KRT_Felhasznalok FTNew on FTNew.Id = New.Felhasznalo_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Szerepkor_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_SzerepkorokAzonosito(Old.Szerepkor_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_SzerepkorokAzonosito(New.Szerepkor_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Felhasznalo_SzerepkorHistory Old
         inner join KRT_Felhasznalo_SzerepkorHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Szerepkor_Id != New.Szerepkor_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Szerepkorok FTOld on FTOld.Id = Old.Szerepkor_Id --and FTOld.Ver = Old.Ver
         left join KRT_Szerepkorok FTNew on FTNew.Id = New.Szerepkor_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Helyettesites_Id' as ColumnName,               cast(Old.Helyettesites_Id as nvarchar(99)) as OldValue,
               cast(New.Helyettesites_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Felhasznalo_SzerepkorHistory Old
         inner join KRT_Felhasznalo_SzerepkorHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Helyettesites_Id != New.Helyettesites_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end


GO
