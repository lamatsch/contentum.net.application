IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Felhasznalo_SzerepkorInsertToMegbizas_Tomeges]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Felhasznalo_SzerepkorInsertToMegbizas_Tomeges]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Felhasznalo_SzerepkorInsertToMegbizas_Tomeges]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Felhasznalo_SzerepkorInsertToMegbizas_Tomeges] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Felhasznalo_SzerepkorInsertToMegbizas_Tomeges]
  @Megbizas_Id uniqueidentifier,
  @Ids nvarchar(MAX) = null,
  @ExecutorUserId uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

   -- NULLITÁS-vizsgálat
   if @Megbizas_Id is NULL
      RAISERROR('[50405]',16,1)

   if @ExecutorUserId is NULL
      RAISERROR('[53503]',16,1) -- Az @ExecutorUserId paraméter nem lehet NULL!

   if @Ids is NULL or @Ids =''
      RAISERROR('[53504]',16,1) -- Nincs megadva másolandó szerepkör hozzárendelés!

    declare @strNow nvarchar(20)
    set @strNow = convert(nvarchar, getdate(), 120);

    declare @sqlcmd nvarchar(MAX)
    set @sqlcmd = 'declare @MegbizasId uniqueidentifier
declare @MegbizasKezd datetime
	, @MegbizasVege datetime
	, @MegbizasErvVege datetime
	, @Megbizo uniqueidentifier
	, @Megbizott uniqueidentifier
	, @MegbizoCsoportTagId uniqueidentifier

select @MegbizasId=Id, @MegbizasKezd=HelyettesitesKezd, @MegbizasVege=HelyettesitesVege, @MegbizasErvVege=ErvVege,
@Megbizo=Felhasznalo_ID_helyettesitett, @MegbizoCsoportTagId=CsoportTag_ID_helyettesitett, @Megbizott=Felhasznalo_ID_helyettesito
from KRT_Helyettesitesek
where Id = ''' + convert(nvarchar(36), @Megbizas_Id) + ''' and HelyettesitesMod=''2''

if (@MegbizasId is NULL)
    RAISERROR(''[50101]'',16,1)

if (@MegbizasErvVege < getdate())
    RAISERROR(''[50403]'',16,1)

if (@MegbizasErvVege < getdate())
    RAISERROR(''[50403]'',16,1)
'

set @sqlcmd = @sqlcmd + '
declare @fsz_Id uniqueidentifier
	, @Szerepkor_Id uniqueidentifier
	, @fsz_ErvKezd datetime
	, @fsz_ErvVege datetime

declare @currentRecordId uniqueidentifier
	, @KezdDate datetime
	, @VegeDate datetime

declare felhasznalo_szerepkor_row cursor local fast_forward for
    select fsz.Id, fsz.Szerepkor_Id, fsz.ErvKezd, fsz.ErvVege
    from KRT_Felhasznalo_Szerepkor fsz
    where fsz.Felhasznalo_Id = @Megbizo and (fsz.CsoportTag_Id=@MegbizoCsoportTagId or fsz.CsoportTag_Id is null)
    and fsz.Helyettesites_Id is null and fsz.Id in (' + @Ids + ')
    and getdate() < fsz.ErvVege;

declare @resultTable table (Id uniqueidentifier)
'

set @sqlcmd = @sqlcmd + '
open felhasznalo_szerepkor_row
fetch next from felhasznalo_szerepkor_row
    into @fsz_Id, @Szerepkor_Id, @fsz_ErvKezd, @fsz_ErvVege

while (@@Fetch_Status = 0)
begin
	set @KezdDate = @MegbizasKezd
	set @VegeDate = @MegbizasVege
        
	if @KezdDate < @fsz_ErvKezd
		set @KezdDate = @fsz_ErvKezd
	if @KezdDate < getdate()
		set @KezdDate = getdate()

	if @VegeDate > @fsz_ErvVege
		set @VegeDate = @fsz_ErvVege

	exec sp_KRT_Felhasznalo_SzerepkorInsert
		@Felhasznalo_Id = @Megbizott,
		@Szerepkor_Id = @Szerepkor_Id,
		@Helyettesites_Id = @MegbizasId,
		@Letrehozo_id = '''+convert(nvarchar(36),@ExecutorUserId)+''',
		@LetrehozasIdo = ''' + @strNow + ''',
		@ErvKezd = @KezdDate,
		@ErvVege = @VegeDate,
		@ResultUid = @currentRecordId OUTPUT

    insert into @resultTable (Id) values (@currentRecordId)


    fetch next from felhasznalo_szerepkor_row
        into @fsz_Id, @Szerepkor_Id, @fsz_ErvKezd, @fsz_ErvVege
end

close felhasznalo_szerepkor_row
deallocate felhasznalo_szerepkor_row

select * from @resultTable
'
  
---- nyomtatási blokk kezdete
--   if exists (select 1 from  sysobjects where id = object_id('sp_PrintText') and type = 'P')
--   begin
--       exec dbo.sp_PrintText @sqlcmd;
--   end
---- nyomtatási blokk vége
   exec (@sqlcmd)

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
