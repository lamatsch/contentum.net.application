IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Felhasznalo_SzerepkorUpdateAtruhazottBySzerepkor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Felhasznalo_SzerepkorUpdateAtruhazottBySzerepkor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Felhasznalo_SzerepkorUpdateAtruhazottBySzerepkor]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Felhasznalo_SzerepkorUpdateAtruhazottBySzerepkor] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Felhasznalo_SzerepkorUpdateAtruhazottBySzerepkor]
  @Id uniqueidentifier,
  @ExecutorUserId uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

   -- NULLITÁS-vizsgálat
   if @Id is NULL
      RAISERROR('[50405]',16,1)

   if @ExecutorUserId is NULL
      RAISERROR('[53503]',16,1) -- Az @ExecutorUserId paraméter nem lehet NULL!

    declare @strNow nvarchar(20)
    set @strNow = convert(nvarchar, getdate(), 120);

    declare @sqlcmd nvarchar(MAX)
    set @sqlcmd = 'declare @fsz_Id uniqueidentifier
	, @Szerepkor_Id uniqueidentifier
	, @fsz_felhasznalo_id uniqueidentifier
	, @fsz_csoporttag_id uniqueidentifier
	, @fsz_ErvKezd datetime
	, @fsz_ErvVege datetime

select @fsz_Id = Id, @Szerepkor_Id = Szerepkor_Id, @fsz_felhasznalo_id = Felhasznalo_Id,
@fsz_csoporttag_id = CsoportTag_Id, @fsz_ErvKezd = ErvKezd, @fsz_ErvVege = ErvVege
from KRT_Felhasznalo_Szerepkor where Id=''' + convert(nvarchar(36), @Id) + ''' and Helyettesites_Id is null

if (@fsz_Id is NULL)
    RAISERROR(''[50101]'',16,1)

if (@fsz_ErvVege < getdate())
    RAISERROR(''[50403]'',16,1)

declare @sz_Id uniqueidentifier
	, @sz_ErvKezd datetime
	, @sz_ErvVege datetime
	, @sz_Ver int
	, @MegbizasKezd datetime
	, @MegbizasVege datetime
'

set @sqlcmd = @sqlcmd + '

declare felhasznalo_szerepkor_row cursor local fast_forward for
    select fsz.Id, fsz.ErvKezd, fsz.ErvVege, fsz.Ver, h.HelyettesitesKezd, h.HelyettesitesVege
    from KRT_Felhasznalo_Szerepkor fsz
	join KRT_Helyettesitesek h on fsz.Helyettesites_Id = h.Id
    where h.HelyettesitesMod=''2'' and fsz.Szerepkor_Id = @Szerepkor_Id
	and h.Felhasznalo_ID_helyettesitett = @fsz_felhasznalo_id
	and (@fsz_csoporttag_id is null or h.CsoportTag_ID_helyettesitett = @fsz_csoporttag_id)
	and getdate() between h.ErvKezd and h.ErvVege
	and fsz.ErvVege >= getdate()

declare @resultTableUpdate table (Id uniqueidentifier)
declare @resultTableInvalidate table (Id uniqueidentifier)

declare @KezdDate datetime
	, @VegeDate datetime
'

set @sqlcmd = @sqlcmd + '
open felhasznalo_szerepkor_row
fetch next from felhasznalo_szerepkor_row
    into @sz_Id, @sz_ErvKezd, @sz_ErvVege, @sz_Ver, @MegbizasKezd, @MegbizasVege

while (@@Fetch_Status = 0)
begin

    if (@fsz_ErvVege < getdate())
    begin
        exec sp_KRT_Felhasznalo_SzerepkorInvalidate
        @Id=@sz_Id,
		@ExecutorUserId='''+convert(nvarchar(36),@ExecutorUserId)+''',
		@ExecutionTime='''+@strNow+'''

		insert into @resultTableInvalidate (Id) values (@sz_Id)
    end'
set @sqlcmd = @sqlcmd + '
    else
    begin
		set @KezdDate = @sz_ErvKezd
		if ((@MegbizasKezd > getdate() or @KezdDate > getdate()) and @KezdDate < @fsz_ErvKezd)
		begin
			set @KezdDate = @fsz_ErvKezd
		end

		set @VegeDate = @Megbizasvege
		if (@VegeDate > @fsz_ErvVege)
			set @VegeDate = @fsz_ErvVege

		exec sp_KRT_Felhasznalo_SzerepkorUpdate
		@Id=@sz_Id,
		@ExecutorUserId='''+convert(nvarchar(36),@ExecutorUserId)+''',
		@ExecutionTime='''+@strNow+''',
		@ErvKezd=@KezdDate,
		@ErvVege=@VegeDate,
		@Ver=@sz_Ver,
		@UpdatedColumns=''<root><ErvKezd/><ErvVege/></root>''

		insert into @resultTableUpdate (Id) values (@sz_Id)
	end

    fetch next from felhasznalo_szerepkor_row
		into @sz_Id, @sz_ErvKezd, @sz_ErvVege, @sz_Ver, @MegbizasKezd, @MegbizasVege
end

close felhasznalo_szerepkor_row
deallocate felhasznalo_szerepkor_row

select * from @resultTableUpdate
select * from @resultTableInvalidate
'
---- nyomtatási blokk kezdete
--   if exists (select 1 from  sysobjects where id = object_id('sp_PrintText') and type = 'P')
--   begin
--       exec dbo.sp_PrintText @sqlcmd;
--   end
---- nyomtatási blokk vége
   exec (@sqlcmd)

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
