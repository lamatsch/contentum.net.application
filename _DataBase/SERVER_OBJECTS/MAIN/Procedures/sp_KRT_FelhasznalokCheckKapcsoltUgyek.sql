IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FelhasznalokCheckKapcsoltUgyek]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_FelhasznalokCheckKapcsoltUgyek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FelhasznalokCheckKapcsoltUgyek]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_FelhasznalokCheckKapcsoltUgyek] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_FelhasznalokCheckKapcsoltUgyek]
  @Id uniqueidentifier,
  @ExecutorUserId uniqueidentifier
  
as

begin

-- CR3328 : Felhasználóhoz tartozó Ügyirat, Irat és Küldeményellenörzés

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
	
	Declare @UgyIratok int
	Declare @Iratok int
	Declare @Kuldemenyek int
	Declare @Felhaszn_nev nvarchar(100)

	SELECT @felhaszn_nev = nev from [dbo].[KRT_Felhasznalok]
		where id = @id
		and getdate() between ErvKezd and ErvVege
	
	SELECT @Ugyiratok = count(id) from [EREC_UgyUgyiratok]
		where [Csoport_Id_Felelos] = @id
		and getdate() between ErvKezd and ErvVege

	SELECT @Iratok = count(id) from [EREC_IraIratok]
		where [Csoport_Id_Felelos] = @id
		and getdate() between ErvKezd and ErvVege

	SELECT @Kuldemenyek = count(id) from [EREC_KuldKuldemenyek]
		where [Csoport_Id_Felelos] = @id
		and getdate() between ErvKezd and ErvVege


	SELECT @id as felhasznaloId, @felhaszn_nev as nev,  @Ugyiratok as ugyiratok_count, @Iratok as iratok_count, @Kuldemenyek as kuldemenyek_count

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
