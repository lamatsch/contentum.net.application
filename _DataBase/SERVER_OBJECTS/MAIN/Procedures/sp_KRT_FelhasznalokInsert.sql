SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT 1 FROM sys.objects where name = 'sp_KRT_FelhasznalokInsert' and type = 'P')
drop procedure [sp_KRT_FelhasznalokInsert]
GO
create procedure [dbo].[sp_KRT_FelhasznalokInsert]    
                @Id      uniqueidentifier = null,    
                           @Org     uniqueidentifier = null,    
                @Partner_id     uniqueidentifier  = null,
                @Tipus     Nvarchar(10)  = null,
	            @UserNev     Nvarchar(100),
                @Nev     Nvarchar(100)  = null,
                @Jelszo     Nvarchar(100)  = null,
	            @JelszoLejaratIdo     datetime,
                @System     char(1)  = null,
                @Kiszolgalo     char(1)  = null,
                @DefaultPrivatKulcs     Nvarchar(4000)  = null,
                @Partner_Id_Munkahely     uniqueidentifier  = null,
                @MaxMinosites     Nvarchar(2)  = null,
                @EMail     Nvarchar(100)  = null,
                @Engedelyezett     char(1)  = null,
                @Telefonszam     Nvarchar(100)  = null,
                @Beosztas     Nvarchar(100)  = null,
                @Ver     int  = null,
				@WrongPassCnt int = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @ZarolasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,				

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
 IF @ORG is NULL
 BEGIN 
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@Letrehozo_id)
   if (@Org is null)
   begin
   	RAISERROR('[50302]',16,1)
   end
END

DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Org is not null
         begin
            SET @insertColumns = @insertColumns + ',Org'
            SET @insertValues = @insertValues + ',@Org'
         end 
       
         if @Partner_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Partner_id'
            SET @insertValues = @insertValues + ',@Partner_id'
         end 
       
         if @Tipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Tipus'
            SET @insertValues = @insertValues + ',@Tipus'
         end 
       
         if @UserNev is not null
         begin
            SET @insertColumns = @insertColumns + ',UserNev'
            SET @insertValues = @insertValues + ',@UserNev'
         end 
       
         if @Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Nev'
            SET @insertValues = @insertValues + ',@Nev'
         end 
       
         if @Jelszo is not null
         begin
            SET @insertColumns = @insertColumns + ',Jelszo'
            SET @insertValues = @insertValues + ',@Jelszo'
         end 
       
         if @JelszoLejaratIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',JelszoLejaratIdo'
            SET @insertValues = @insertValues + ',@JelszoLejaratIdo'
         end 
       
         if @System is not null
         begin
            SET @insertColumns = @insertColumns + ',System'
            SET @insertValues = @insertValues + ',@System'
         end 
       
         if @Kiszolgalo is not null
         begin
            SET @insertColumns = @insertColumns + ',Kiszolgalo'
            SET @insertValues = @insertValues + ',@Kiszolgalo'
         end 
       
         if @DefaultPrivatKulcs is not null
         begin
            SET @insertColumns = @insertColumns + ',DefaultPrivatKulcs'
            SET @insertValues = @insertValues + ',@DefaultPrivatKulcs'
         end 
       
         if @Partner_Id_Munkahely is not null
         begin
            SET @insertColumns = @insertColumns + ',Partner_Id_Munkahely'
            SET @insertValues = @insertValues + ',@Partner_Id_Munkahely'
         end 
       
         if @MaxMinosites is not null
         begin
            SET @insertColumns = @insertColumns + ',MaxMinosites'
            SET @insertValues = @insertValues + ',@MaxMinosites'
         end 
       
         if @EMail is not null
         begin
            SET @insertColumns = @insertColumns + ',EMail'
            SET @insertValues = @insertValues + ',@EMail'
         end 
       
         if @Engedelyezett is not null
         begin
            SET @insertColumns = @insertColumns + ',Engedelyezett'
            SET @insertValues = @insertValues + ',@Engedelyezett'
         end 
       
         if @Telefonszam is not null
         begin
            SET @insertColumns = @insertColumns + ',Telefonszam'
            SET @insertValues = @insertValues + ',@Telefonszam'
         end 
       
         if @Beosztas is not null
         begin
            SET @insertColumns = @insertColumns + ',Beosztas'
            SET @insertValues = @insertValues + ',@Beosztas'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               

         SET @insertColumns = @insertColumns + ',WrongPassCnt'
         SET @insertValues = @insertValues + ',0'               

       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into KRT_Felhasznalok ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Org uniqueidentifier,@Partner_id uniqueidentifier,@Tipus Nvarchar(10),@UserNev Nvarchar(100),@Nev Nvarchar(100),@Jelszo Nvarchar(100),@JelszoLejaratIdo datetime,@System char(1),@Kiszolgalo char(1),@DefaultPrivatKulcs Nvarchar(4000),@Partner_Id_Munkahely uniqueidentifier,@MaxMinosites Nvarchar(2),@EMail Nvarchar(100),@Engedelyezett char(1),@Telefonszam Nvarchar(100),@Beosztas Nvarchar(100),@Ver int,@WrongPassCnt int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@ZarolasIdo datetime,@Zarolo_id uniqueidentifier,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Org = @Org,@Partner_id = @Partner_id,@Tipus = @Tipus,@UserNev = @UserNev,@Nev = @Nev,@Jelszo = @Jelszo,@JelszoLejaratIdo = @JelszoLejaratIdo,@System = @System,@Kiszolgalo = @Kiszolgalo,@DefaultPrivatKulcs = @DefaultPrivatKulcs,@Partner_Id_Munkahely = @Partner_Id_Munkahely,@MaxMinosites = @MaxMinosites,@EMail = @EMail,@Engedelyezett = @Engedelyezett,@Telefonszam = @Telefonszam,@Beosztas = @Beosztas,@Ver = @Ver,@WrongPassCnt = @WrongPassCnt,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@ZarolasIdo = @ZarolasIdo,@Zarolo_id = @Zarolo_id,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'KRT_Felhasznalok',@ResultUid
					,'KRT_FelhasznalokHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


