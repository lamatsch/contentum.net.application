IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Felhasznalok_GetByNyomtato]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Felhasznalok_GetByNyomtato]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Felhasznalok_GetByNyomtato]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Felhasznalok_GetByNyomtato] AS' 
END
GO
--GetByNyomtato

ALTER procedure [dbo].[sp_KRT_Felhasznalok_GetByNyomtato]
			  @Id uniqueidentifier,
			  @Where nvarchar(4000) = '',
			  @OrderBy nvarchar(200) = ' order by   KRT_Felhasznalok.LetrehozasIdo',
			  @TopRow nvarchar(5) = '',

         @ExecutorUserId uniqueidentifier
         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)
	DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
		'select ' + @LocalTopRow + '
  		   KRT_Felhasznalok.Id,
		   KRT_Felhasznalok.Org,
		   KRT_Felhasznalok.Partner_id,
		   KRT_Felhasznalok.Tipus,
		   KRT_Felhasznalok.UserNev,
		   KRT_Felhasznalok.Nev,
		   KRT_Felhasznalok.Jelszo,
		   KRT_Felhasznalok.JelszoLejaratIdo,
		   KRT_Felhasznalok.System,
		   KRT_Felhasznalok.Kiszolgalo,
		   KRT_Felhasznalok.DefaultPrivatKulcs,
		   KRT_Felhasznalok.Partner_Id_Munkahely,
		   KRT_Felhasznalok.MaxMinosites,
		   KRT_Felhasznalok.EMail,
		   KRT_Felhasznalok.Engedelyezett,
		   KRT_Felhasznalok.Telefonszam,
		   KRT_Felhasznalok.Beosztas,
		   KRT_Felhasznalok.Ver,
		   KRT_Felhasznalok.Note,
		   KRT_Felhasznalok.Stat_id,
		   KRT_Felhasznalok.ErvKezd,
		   KRT_Felhasznalok.ErvVege,
		   KRT_Felhasznalok.Letrehozo_id,
		   KRT_Felhasznalok.LetrehozasIdo,
		   KRT_Felhasznalok.Modosito_id,
		   KRT_Felhasznalok.ModositasIdo,
		   KRT_Felhasznalok.ZarolasIdo,
		   KRT_Felhasznalok.Zarolo_id,
		   KRT_Felhasznalok.Tranz_id,
		   KRT_Felhasznalok.UIAccessLog_id
	   from 
		 KRT_Felhasznalok as KRT_Felhasznalok 
		 inner join 
		 KRT_Felhasznalok_Halozati_Nyomtatoi as KRT_Felhasznalok_Halozati_Nyomtatoi
		 on KRT_Felhasznalok.Id = KRT_Felhasznalok_Halozati_Nyomtatoi.Felhasznalo_Id
		 inner join
		 KRT_Halozati_Nyomtatok as KRT_Halozati_Nyomtatok
		 on KRT_Halozati_Nyomtatok.Id = KRT_Felhasznalok_Halozati_Nyomtatoi.Halozati_Nyomtato_Id
	   where
		 KRT_Felhasznalok_Halozati_Nyomtatoi.Halozati_Nyomtato_Id = @Id
		 and KRT_Felhasznalok_Halozati_Nyomtatoi.ErvKezd <= getdate() and KRT_Felhasznalok_Halozati_Nyomtatoi.ErvVege >= getdate()
		 and KRT_Halozati_Nyomtatok.ErvKezd <= getdate() and KRT_Halozati_Nyomtatok.ErvVege >= getdate()
		 and KRT_Felhasznalok.ErvKezd <= getdate() and KRT_Felhasznalok.ErvVege >= getdate()'
		 

   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  
	exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
SET ANSI_NULLS ON



GO
