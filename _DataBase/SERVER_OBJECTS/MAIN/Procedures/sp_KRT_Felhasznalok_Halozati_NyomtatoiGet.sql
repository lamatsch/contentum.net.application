IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Felhasznalok_Halozati_NyomtatoiGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Felhasznalok_Halozati_NyomtatoiGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Felhasznalok_Halozati_NyomtatoiGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Felhasznalok_Halozati_NyomtatoiGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Felhasznalok_Halozati_NyomtatoiGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

        
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Felhasznalok_Halozati_Nyomtatoi.Id,
		   KRT_Felhasznalok_Halozati_Nyomtatoi.Felhasznalo_Id,
		   KRT_Felhasznalok_Halozati_Nyomtatoi.Halozati_Nyomtato_Id,
		   KRT_Felhasznalok_Halozati_Nyomtatoi.Ver,
		   KRT_Felhasznalok_Halozati_Nyomtatoi.Note,
		   KRT_Felhasznalok_Halozati_Nyomtatoi.Stat_id,
		   KRT_Felhasznalok_Halozati_Nyomtatoi.ErvKezd,
		   KRT_Felhasznalok_Halozati_Nyomtatoi.ErvVege,
		   KRT_Felhasznalok_Halozati_Nyomtatoi.Letrehozo_id,
		   KRT_Felhasznalok_Halozati_Nyomtatoi.LetrehozasIdo,
		   KRT_Felhasznalok_Halozati_Nyomtatoi.Modosito_id,
		   KRT_Felhasznalok_Halozati_Nyomtatoi.ModositasIdo,
		   KRT_Felhasznalok_Halozati_Nyomtatoi.Zarolo_id,
		   KRT_Felhasznalok_Halozati_Nyomtatoi.ZarolasIdo,
		   KRT_Felhasznalok_Halozati_Nyomtatoi.Tranz_id,
		   KRT_Felhasznalok_Halozati_Nyomtatoi.UIAccessLog_id
	   from 
		 KRT_Felhasznalok_Halozati_Nyomtatoi as KRT_Felhasznalok_Halozati_Nyomtatoi 
	   where
		 KRT_Felhasznalok_Halozati_Nyomtatoi.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
