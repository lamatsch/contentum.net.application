

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_ForditasokGet')
            and   type = 'P')
   drop procedure sp_KRT_ForditasokGet
go

create procedure sp_KRT_ForditasokGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Forditasok.Id,
	   KRT_Forditasok.NyelvKod,
	   KRT_Forditasok.OrgKod,
	   KRT_Forditasok.Modul,
	   KRT_Forditasok.Komponens,
	   KRT_Forditasok.ObjAzonosito,
	   KRT_Forditasok.Forditas,
	   KRT_Forditasok.Ver,
	   KRT_Forditasok.Note,
	   KRT_Forditasok.Stat_id,
	   KRT_Forditasok.ErvKezd,
	   KRT_Forditasok.ErvVege,
	   KRT_Forditasok.Letrehozo_id,
	   KRT_Forditasok.LetrehozasIdo,
	   KRT_Forditasok.Modosito_id,
	   KRT_Forditasok.ModositasIdo,
	   KRT_Forditasok.Zarolo_id,
	   KRT_Forditasok.ZarolasIdo,
	   KRT_Forditasok.Tranz_id,
	   KRT_Forditasok.UIAccessLog_id
	   from 
		 KRT_Forditasok as KRT_Forditasok 
	   where
		 KRT_Forditasok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
