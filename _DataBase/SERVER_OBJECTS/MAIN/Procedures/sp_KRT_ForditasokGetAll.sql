
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_ForditasokGetAll')
            and   type = 'P')
   drop procedure sp_KRT_ForditasokGetAll
go

create procedure sp_KRT_ForditasokGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Forditasok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @pageNumber		int = 0,
  @pageSize		int = -1,
  @SelectedRowId	uniqueidentifier = null,
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 	DECLARE @firstRow INT
	DECLARE @lastRow INT
   
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow != '' and @TopRow != '0')
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end
 
	/************************************************************
	* Szűrt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   KRT_Forditasok.Id into #result
		from KRT_Forditasok as KRT_Forditasok
 '
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end

	if (@SelectedRowId is not null)
	begin
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id = @SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	end

	set @sqlcmd = @sqlcmd + N' 
	if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
	BEGIN
		select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
		set @firstRow = (@pageNumber - 1) * @pageSize + 1;
		set @lastRow = @pageNumber*@pageSize;
		select @pageNumber = @pageNumber - 1
	END ;'

	/************************************************************
	* Tényleges select											*
	************************************************************/
	SET @sqlcmd = @sqlcmd + 
  'select 
		#result.RowNumber,
  	   KRT_Forditasok.Id,
	   KRT_Forditasok.NyelvKod,
	   KRT_Forditasok.OrgKod,
	   KRT_Forditasok.Modul,
	   KRT_Forditasok.Komponens,
	   KRT_Forditasok.ObjAzonosito,
	   KRT_Forditasok.Forditas,
	   KRT_Forditasok.Ver,
	   KRT_Forditasok.Note,
	   KRT_Forditasok.Stat_id,
	   KRT_Forditasok.ErvKezd,
	   KRT_Forditasok.ErvVege,
	   KRT_Forditasok.Letrehozo_id,
	   KRT_Forditasok.LetrehozasIdo,
	   KRT_Forditasok.Modosito_id,
	   KRT_Forditasok.ModositasIdo,
	   KRT_Forditasok.Zarolo_id,
	   KRT_Forditasok.ZarolasIdo,
	   KRT_Forditasok.Tranz_id,
	   KRT_Forditasok.UIAccessLog_id  
   from 
     KRT_Forditasok as KRT_Forditasok      
     	inner join #result on #result.Id = KRT_Forditasok.Id
'
	if (@firstRow is not null and @lastRow is not null)
	BEGIN
		set @sqlcmd = @sqlcmd + '	where RowNumber between @firstRow and @lastRow
'
	END

	set @sqlcmd = @sqlcmd + '	ORDER BY #result.RowNumber;'

	-- találatok száma és oldalszám
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';
 	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId;
  
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go