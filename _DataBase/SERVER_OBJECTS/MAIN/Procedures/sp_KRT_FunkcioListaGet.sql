IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FunkcioListaGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_FunkcioListaGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FunkcioListaGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_FunkcioListaGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_FunkcioListaGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_FunkcioLista.Id,
	   KRT_FunkcioLista.Funkcio_Id,
	   KRT_FunkcioLista.Funkcio_Id_Hivott,
	   KRT_FunkcioLista.FutasiSorrend,
	   KRT_FunkcioLista.Ver,
	   KRT_FunkcioLista.Note,
	   KRT_FunkcioLista.Stat_id,
	   KRT_FunkcioLista.ErvKezd,
	   KRT_FunkcioLista.ErvVege,
	   KRT_FunkcioLista.Letrehozo_id,
	   KRT_FunkcioLista.LetrehozasIdo,
	   KRT_FunkcioLista.Modosito_id,
	   KRT_FunkcioLista.ModositasIdo,
	   KRT_FunkcioLista.Zarolo_id,
	   KRT_FunkcioLista.ZarolasIdo,
	   KRT_FunkcioLista.UIAccessLog_id
	   from 
		 KRT_FunkcioLista as KRT_FunkcioLista 
	   where
		 KRT_FunkcioLista.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
