IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FunkciokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_FunkciokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FunkciokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_FunkciokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_FunkciokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Funkciok.Id,
	   KRT_Funkciok.Kod,
	   KRT_Funkciok.Nev,
	   KRT_Funkciok.ObjTipus_Id_AdatElem,
	   KRT_Funkciok.ObjStat_Id_Kezd,
	   KRT_Funkciok.Alkalmazas_Id,
	   KRT_Funkciok.Muvelet_Id,
	   KRT_Funkciok.ObjStat_Id_Veg,
	   KRT_Funkciok.Leiras,
	   KRT_Funkciok.Funkcio_Id_Szulo,
	   KRT_Funkciok.Csoportosito,
	   KRT_Funkciok.Modosithato,
	   KRT_Funkciok.Org,
	   KRT_Funkciok.MunkanaploJelzo,
	   KRT_Funkciok.FeladatJelzo,
	   KRT_Funkciok.KeziFeladatJelzo,
	   KRT_Funkciok.Ver,
	   KRT_Funkciok.Note,
	   KRT_Funkciok.Stat_id,
	   KRT_Funkciok.ErvKezd,
	   KRT_Funkciok.ErvVege,
	   KRT_Funkciok.Letrehozo_id,
	   KRT_Funkciok.LetrehozasIdo,
	   KRT_Funkciok.Modosito_id,
	   KRT_Funkciok.ModositasIdo,
	   KRT_Funkciok.Zarolo_id,
	   KRT_Funkciok.ZarolasIdo,
	   KRT_Funkciok.Tranz_id,
	   KRT_Funkciok.UIAccessLog_id
	   from 
		 KRT_Funkciok as KRT_Funkciok 
	   where
		 KRT_Funkciok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
