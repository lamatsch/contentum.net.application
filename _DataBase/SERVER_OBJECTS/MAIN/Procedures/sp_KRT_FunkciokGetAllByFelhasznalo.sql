IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FunkciokGetAllByFelhasznalo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_FunkciokGetAllByFelhasznalo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FunkciokGetAllByFelhasznalo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_FunkciokGetAllByFelhasznalo] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_FunkciokGetAllByFelhasznalo]
  @Felhasznalo_Id UniqueIdentifier ,
  @Helyettesites_Id uniqueidentifier = null,
  @OrderBy nvarchar(200) = ' order by KRT_Funkciok.Nev',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
	KRT_Funkciok.Id as Id,
	KRT_Funkciok.Kod as Kod	
   from 
	 KRT_Felhasznalok as KRT_Felhasznalok,
     KRT_Felhasznalo_Szerepkor as KRT_Felhasznalo_Szerepkor,
	 KRT_Szerepkorok as KRT_Szerepkorok,
	 KRT_Szerepkor_Funkcio as KRT_Szerepkor_Funkcio,
	 KRT_Funkciok as KRT_Funkciok
	where
'
  if @Helyettesites_Id is not null
  begin
      SET @sqlcmd = @sqlcmd + '	KRT_Felhasznalo_Szerepkor.Helyettesites_Id = ''' + convert(nvarchar(36), @Helyettesites_Id) + ''''
  end
  else
  begin
      SET @sqlcmd = @sqlcmd + '	KRT_Felhasznalo_Szerepkor.Helyettesites_Id is null'
  end

  SET @sqlcmd = @sqlcmd + ' 
	and KRT_Felhasznalok.Id = ''' + CAST(@Felhasznalo_Id as Nvarchar(40)) + '''
	and KRT_Felhasznalok.Id = KRT_Felhasznalo_Szerepkor.Felhasznalo_Id
	and KRT_Felhasznalo_Szerepkor.Szerepkor_Id = KRT_Szerepkorok.Id
	and KRT_Szerepkorok.Id = KRT_Szerepkor_Funkcio.Szerepkor_Id
	and KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id
	-- default ervenyesseg ellenorzes
	and KRT_Felhasznalok.ErvKezd <= getdate() and KRT_Felhasznalok.ErvVege >= getdate()
	and KRT_Felhasznalo_Szerepkor.ErvKezd <= getdate() and KRT_Felhasznalo_Szerepkor.ErvVege >= getdate()
	and KRT_Szerepkorok.ErvKezd <= getdate() and KRT_Szerepkorok.ErvVege >= getdate()
	and KRT_Szerepkor_Funkcio.ErvKezd <= getdate() and KRT_Szerepkor_Funkcio.ErvVege >= getdate()
	and KRT_Funkciok.ErvKezd <= getdate() and KRT_Funkciok.ErvVege >= getdate()
	--and KRT_Funkciok.Org=''' + CAST(@Org as Nvarchar(40)) + '''
'

   SET @sqlcmd = @sqlcmd + @OrderBy
  
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
