IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FunkciokGetAllByMegbizas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_FunkciokGetAllByMegbizas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FunkciokGetAllByMegbizas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_FunkciokGetAllByMegbizas] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_FunkciokGetAllByMegbizas]
  @Helyettesites_Id uniqueidentifier,
  @OrderBy nvarchar(200) = ' order by KRT_Funkciok.Nev',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END


if (@Helyettesites_Id is null)
begin
    RAISERROR('[53506]',16,1) -- A @Helyettesites_Id paraméter értéke nem lehet null!
end


-- helyettesítés rekord lekérdezése:
declare @Id uniqueidentifier
declare @Felhasznalo_ID_helyettesitett uniqueidentifier
declare @CsoportTag_ID_helyettesitett uniqueidentifier
declare @Felhasznalo_ID_helyettesito uniqueidentifier
declare @HelyettesitesKezd datetime
declare @HelyettesitesVege datetime
declare @HelyettesitesMod nvarchar(64)
declare @ErvKezd datetime
declare @ErvVege datetime

select @Id = Id, @ErvKezd = ErvKezd, @ErvVege = ErvVege,
@Felhasznalo_ID_helyettesitett = Felhasznalo_ID_helyettesitett,
@CsoportTag_ID_helyettesitett = CsoportTag_ID_helyettesitett,
@Felhasznalo_ID_helyettesito = Felhasznalo_ID_helyettesito, @HelyettesitesMod = HelyettesitesMod,
@HelyettesitesKezd = HelyettesitesKezd, @HelyettesitesVege = HelyettesitesVege
from KRT_Helyettesitesek where Id = @Helyettesites_Id

-- a rekord nem található
if (@Id is null)
begin
    RAISERROR('[50101]',16,1)
end

if (@HelyettesitesMod <> '2')
begin
    RAISERROR('[53507]',16,1) -- A helyettesítés nem megbízás!
end

if not(getdate() between @ErvKezd and @ErvVege)
begin
    RAISERROR('[53508]',16,1) -- A megbízás érvénytelen!
end

if not(getdate() between @HelyettesitesKezd and @HelyettesitesVege)
begin
    RAISERROR('[53509]',16,1) -- A megbízás nem áll fenn!
end
-- felhasználók ellenorzése:
declare @Helyettesitett_Id uniqueidentifier
select @Helyettesitett_Id = Id
    from KRT_Felhasznalok
    where Id = @Felhasznalo_ID_helyettesitett
    and getdate() between ErvKezd and ErvVege

if (@Helyettesitett_Id is null)
begin
    RAISERROR('[53510]',16,1) -- A helyettesített felhasználó nem létezik!
end

declare @Helyettesito_Id uniqueidentifier
select @Helyettesito_Id = Id
    from KRT_Felhasznalok
    where Id = @Felhasznalo_ID_helyettesito
    and getdate() between ErvKezd and ErvVege

if (@Helyettesito_Id is null)
begin
    RAISERROR('[53511]',16,1) -- A helyettesíto felhasználó nem létezik!
end

-- csoporttagság fennállásának ellenorzése
declare @CsoportTag_Id uniqueidentifier
select @CsoportTag_Id = KRT_CsoportTagok.Id
	from KRT_CsoportTagok
	join KRT_Csoportok on KRT_CsoportTagok.Csoport_Id = KRT_Csoportok.Id
	where KRT_CsoportTagok.Id = @CsoportTag_Id_helyettesitett
	and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
	and getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege

if (@CsoportTag_Id is null)
begin
    RAISERROR('[53501]',16,1) --  Nem létezo csoporttagság!
end

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
	KRT_Funkciok.Id as Id,
	KRT_Funkciok.Kod as Kod	
   from 
     KRT_Felhasznalo_Szerepkor as KRT_Felhasznalo_Szerepkor,
	 KRT_Szerepkorok as KRT_Szerepkorok,
	 KRT_Szerepkor_Funkcio as KRT_Szerepkor_Funkcio,
	 KRT_Funkciok as KRT_Funkciok
	where
	KRT_Felhasznalo_Szerepkor.Helyettesites_Id = ''' + convert(nvarchar(36), @Helyettesites_Id) + '''
    and KRT_Felhasznalo_Szerepkor.Felhasznalo_Id = ''' + convert(nvarchar(36), @Felhasznalo_Id_helyettesito) + '''
	and KRT_Felhasznalo_Szerepkor.Szerepkor_Id = KRT_Szerepkorok.Id
	and KRT_Szerepkorok.Id = KRT_Szerepkor_Funkcio.Szerepkor_Id
	and KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id
	-- default ervenyesseg ellenorzes
	and KRT_Felhasznalo_Szerepkor.ErvKezd <= getdate() and KRT_Felhasznalo_Szerepkor.ErvVege >= getdate()
	and KRT_Szerepkorok.ErvKezd <= getdate() and KRT_Szerepkorok.ErvVege >= getdate()
	and KRT_Szerepkor_Funkcio.ErvKezd <= getdate() and KRT_Szerepkor_Funkcio.ErvVege >= getdate()
	and KRT_Funkciok.ErvKezd <= getdate() and KRT_Funkciok.ErvVege >= getdate()
	--and KRT_Funkciok.Org=''' + CAST(@Org as Nvarchar(40)) + '''
'

   SET @sqlcmd = @sqlcmd + @OrderBy
  
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
