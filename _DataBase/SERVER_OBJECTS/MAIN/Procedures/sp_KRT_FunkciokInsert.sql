IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FunkciokInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_FunkciokInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FunkciokInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_FunkciokInsert] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_FunkciokInsert]    
                @Id      uniqueidentifier = null,    
               	            @Kod     Nvarchar(100),
	            @Nev     Nvarchar(100),
                @ObjTipus_Id_AdatElem     uniqueidentifier  = null,
                @ObjStat_Id_Kezd     uniqueidentifier  = null,
                @Alkalmazas_Id     uniqueidentifier  = null,
                @Muvelet_Id     uniqueidentifier  = null,
                @ObjStat_Id_Veg     uniqueidentifier  = null,
                @Leiras     Nvarchar(4000)  = null,
                @Funkcio_Id_Szulo     uniqueidentifier  = null,
                @Csoportosito     char(1)  = null,
                @Modosithato     char(1)  = null,
            @Org     uniqueidentifier = null,    
                @MunkanaploJelzo     char(1)  = null,
                @FeladatJelzo     char(1)  = null,
                @KeziFeladatJelzo     char(1)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Kod is not null
         begin
            SET @insertColumns = @insertColumns + ',Kod'
            SET @insertValues = @insertValues + ',@Kod'
         end 
       
         if @Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Nev'
            SET @insertValues = @insertValues + ',@Nev'
         end 
       
         if @ObjTipus_Id_AdatElem is not null
         begin
            SET @insertColumns = @insertColumns + ',ObjTipus_Id_AdatElem'
            SET @insertValues = @insertValues + ',@ObjTipus_Id_AdatElem'
         end 
       
         if @ObjStat_Id_Kezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ObjStat_Id_Kezd'
            SET @insertValues = @insertValues + ',@ObjStat_Id_Kezd'
         end 
       
         if @Alkalmazas_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Alkalmazas_Id'
            SET @insertValues = @insertValues + ',@Alkalmazas_Id'
         end 
       
         if @Muvelet_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Muvelet_Id'
            SET @insertValues = @insertValues + ',@Muvelet_Id'
         end 
       
         if @ObjStat_Id_Veg is not null
         begin
            SET @insertColumns = @insertColumns + ',ObjStat_Id_Veg'
            SET @insertValues = @insertValues + ',@ObjStat_Id_Veg'
         end 
       
         if @Leiras is not null
         begin
            SET @insertColumns = @insertColumns + ',Leiras'
            SET @insertValues = @insertValues + ',@Leiras'
         end 
       
         if @Funkcio_Id_Szulo is not null
         begin
            SET @insertColumns = @insertColumns + ',Funkcio_Id_Szulo'
            SET @insertValues = @insertValues + ',@Funkcio_Id_Szulo'
         end 
       
         if @Csoportosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoportosito'
            SET @insertValues = @insertValues + ',@Csoportosito'
         end 
       
         if @Modosithato is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosithato'
            SET @insertValues = @insertValues + ',@Modosithato'
         end 
       
         if @Org is not null
         begin
            SET @insertColumns = @insertColumns + ',Org'
            SET @insertValues = @insertValues + ',@Org'
         end 
       
         if @MunkanaploJelzo is not null
         begin
            SET @insertColumns = @insertColumns + ',MunkanaploJelzo'
            SET @insertValues = @insertValues + ',@MunkanaploJelzo'
         end 
       
         if @FeladatJelzo is not null
         begin
            SET @insertColumns = @insertColumns + ',FeladatJelzo'
            SET @insertValues = @insertValues + ',@FeladatJelzo'
         end 
       
         if @KeziFeladatJelzo is not null
         begin
            SET @insertColumns = @insertColumns + ',KeziFeladatJelzo'
            SET @insertValues = @insertValues + ',@KeziFeladatJelzo'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into KRT_Funkciok ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Kod Nvarchar(100),@Nev Nvarchar(100),@ObjTipus_Id_AdatElem uniqueidentifier,@ObjStat_Id_Kezd uniqueidentifier,@Alkalmazas_Id uniqueidentifier,@Muvelet_Id uniqueidentifier,@ObjStat_Id_Veg uniqueidentifier,@Leiras Nvarchar(4000),@Funkcio_Id_Szulo uniqueidentifier,@Csoportosito char(1),@Modosithato char(1),@Org uniqueidentifier,@MunkanaploJelzo char(1),@FeladatJelzo char(1),@KeziFeladatJelzo char(1),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Kod = @Kod,@Nev = @Nev,@ObjTipus_Id_AdatElem = @ObjTipus_Id_AdatElem,@ObjStat_Id_Kezd = @ObjStat_Id_Kezd,@Alkalmazas_Id = @Alkalmazas_Id,@Muvelet_Id = @Muvelet_Id,@ObjStat_Id_Veg = @ObjStat_Id_Veg,@Leiras = @Leiras,@Funkcio_Id_Szulo = @Funkcio_Id_Szulo,@Csoportosito = @Csoportosito,@Modosithato = @Modosithato,@Org = @Org,@MunkanaploJelzo = @MunkanaploJelzo,@FeladatJelzo = @FeladatJelzo,@KeziFeladatJelzo = @KeziFeladatJelzo,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'KRT_Funkciok',@ResultUid
					,'KRT_FunkciokHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
