IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FunkciokUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_FunkciokUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_FunkciokUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_FunkciokUpdate] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_FunkciokUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Kod     Nvarchar(100)  = null,         
             @Nev     Nvarchar(100)  = null,         
             @ObjTipus_Id_AdatElem     uniqueidentifier  = null,         
             @ObjStat_Id_Kezd     uniqueidentifier  = null,         
             @Alkalmazas_Id     uniqueidentifier  = null,         
             @Muvelet_Id     uniqueidentifier  = null,         
             @ObjStat_Id_Veg     uniqueidentifier  = null,         
             @Leiras     Nvarchar(4000)  = null,         
             @Funkcio_Id_Szulo     uniqueidentifier  = null,         
             @Csoportosito     char(1)  = null,         
             @Modosithato     char(1)  = null,         
             @Org     uniqueidentifier  = null,         
             @MunkanaploJelzo     char(1)  = null,         
             @FeladatJelzo     char(1)  = null,         
             @KeziFeladatJelzo     char(1)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Kod     Nvarchar(100)         
     DECLARE @Act_Nev     Nvarchar(100)         
     DECLARE @Act_ObjTipus_Id_AdatElem     uniqueidentifier         
     DECLARE @Act_ObjStat_Id_Kezd     uniqueidentifier         
     DECLARE @Act_Alkalmazas_Id     uniqueidentifier         
     DECLARE @Act_Muvelet_Id     uniqueidentifier         
     DECLARE @Act_ObjStat_Id_Veg     uniqueidentifier         
     DECLARE @Act_Leiras     Nvarchar(4000)         
     DECLARE @Act_Funkcio_Id_Szulo     uniqueidentifier         
     DECLARE @Act_Csoportosito     char(1)         
     DECLARE @Act_Modosithato     char(1)         
     DECLARE @Act_Org     uniqueidentifier         
     DECLARE @Act_MunkanaploJelzo     char(1)         
     DECLARE @Act_FeladatJelzo     char(1)         
     DECLARE @Act_KeziFeladatJelzo     char(1)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Kod = Kod,
     @Act_Nev = Nev,
     @Act_ObjTipus_Id_AdatElem = ObjTipus_Id_AdatElem,
     @Act_ObjStat_Id_Kezd = ObjStat_Id_Kezd,
     @Act_Alkalmazas_Id = Alkalmazas_Id,
     @Act_Muvelet_Id = Muvelet_Id,
     @Act_ObjStat_Id_Veg = ObjStat_Id_Veg,
     @Act_Leiras = Leiras,
     @Act_Funkcio_Id_Szulo = Funkcio_Id_Szulo,
     @Act_Csoportosito = Csoportosito,
     @Act_Modosithato = Modosithato,
     @Act_Org = Org,
     @Act_MunkanaploJelzo = MunkanaploJelzo,
     @Act_FeladatJelzo = FeladatJelzo,
     @Act_KeziFeladatJelzo = KeziFeladatJelzo,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from KRT_Funkciok
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Kod')=1
         SET @Act_Kod = @Kod
   IF @UpdatedColumns.exist('/root/Nev')=1
         SET @Act_Nev = @Nev
   IF @UpdatedColumns.exist('/root/ObjTipus_Id_AdatElem')=1
         SET @Act_ObjTipus_Id_AdatElem = @ObjTipus_Id_AdatElem
   IF @UpdatedColumns.exist('/root/ObjStat_Id_Kezd')=1
         SET @Act_ObjStat_Id_Kezd = @ObjStat_Id_Kezd
   IF @UpdatedColumns.exist('/root/Alkalmazas_Id')=1
         SET @Act_Alkalmazas_Id = @Alkalmazas_Id
   IF @UpdatedColumns.exist('/root/Muvelet_Id')=1
         SET @Act_Muvelet_Id = @Muvelet_Id
   IF @UpdatedColumns.exist('/root/ObjStat_Id_Veg')=1
         SET @Act_ObjStat_Id_Veg = @ObjStat_Id_Veg
   IF @UpdatedColumns.exist('/root/Leiras')=1
         SET @Act_Leiras = @Leiras
   IF @UpdatedColumns.exist('/root/Funkcio_Id_Szulo')=1
         SET @Act_Funkcio_Id_Szulo = @Funkcio_Id_Szulo
   IF @UpdatedColumns.exist('/root/Csoportosito')=1
         SET @Act_Csoportosito = @Csoportosito
   IF @UpdatedColumns.exist('/root/Modosithato')=1
         SET @Act_Modosithato = @Modosithato
   IF @UpdatedColumns.exist('/root/Org')=1
         SET @Act_Org = @Org
   IF @UpdatedColumns.exist('/root/MunkanaploJelzo')=1
         SET @Act_MunkanaploJelzo = @MunkanaploJelzo
   IF @UpdatedColumns.exist('/root/FeladatJelzo')=1
         SET @Act_FeladatJelzo = @FeladatJelzo
   IF @UpdatedColumns.exist('/root/KeziFeladatJelzo')=1
         SET @Act_KeziFeladatJelzo = @KeziFeladatJelzo
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE KRT_Funkciok
SET
     Kod = @Act_Kod,
     Nev = @Act_Nev,
     ObjTipus_Id_AdatElem = @Act_ObjTipus_Id_AdatElem,
     ObjStat_Id_Kezd = @Act_ObjStat_Id_Kezd,
     Alkalmazas_Id = @Act_Alkalmazas_Id,
     Muvelet_Id = @Act_Muvelet_Id,
     ObjStat_Id_Veg = @Act_ObjStat_Id_Veg,
     Leiras = @Act_Leiras,
     Funkcio_Id_Szulo = @Act_Funkcio_Id_Szulo,
     Csoportosito = @Act_Csoportosito,
     Modosithato = @Act_Modosithato,
     Org = @Act_Org,
     MunkanaploJelzo = @Act_MunkanaploJelzo,
     FeladatJelzo = @Act_FeladatJelzo,
     KeziFeladatJelzo = @Act_KeziFeladatJelzo,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'KRT_Funkciok',@Id
					,'KRT_FunkciokHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
