IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Halozati_NyomtatokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Halozati_NyomtatokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Halozati_NyomtatokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Halozati_NyomtatokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Halozati_NyomtatokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Halozati_Nyomtatok.Id,
		   KRT_Halozati_Nyomtatok.Nev,
		   KRT_Halozati_Nyomtatok.Cim,
		   KRT_Halozati_Nyomtatok.Ver,
		   KRT_Halozati_Nyomtatok.Note,
		   KRT_Halozati_Nyomtatok.Stat_id,
		   KRT_Halozati_Nyomtatok.ErvKezd,
		   KRT_Halozati_Nyomtatok.ErvVege,
		   KRT_Halozati_Nyomtatok.Letrehozo_id,
		   KRT_Halozati_Nyomtatok.LetrehozasIdo,
		   KRT_Halozati_Nyomtatok.Modosito_id,
		   KRT_Halozati_Nyomtatok.ModositasIdo,
		   KRT_Halozati_Nyomtatok.Zarolo_id,
		   KRT_Halozati_Nyomtatok.ZarolasIdo,
		   KRT_Halozati_Nyomtatok.Tranz_id,
		   KRT_Halozati_Nyomtatok.UIAccessLog_id
	   from 
		 KRT_Halozati_Nyomtatok as KRT_Halozati_Nyomtatok 
	   where
		 KRT_Halozati_Nyomtatok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end



GO
