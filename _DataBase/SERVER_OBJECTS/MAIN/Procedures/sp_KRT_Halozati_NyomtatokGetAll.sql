IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Halozati_NyomtatokGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Halozati_NyomtatokGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Halozati_NyomtatokGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Halozati_NyomtatokGetAll] AS' 
END
GO

ALTER procedure [dbo].[sp_KRT_Halozati_NyomtatokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Halozati_Nyomtatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @pageNumber		int = 0,
  @pageSize		int = -1,
  @SelectedRowId	uniqueidentifier = null,
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)
   SET @sqlcmd = ''
   DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 	DECLARE @firstRow INT
	DECLARE @lastRow INT
   
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = 1000
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end
 
 /************************************************************
	* Szűrt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   KRT_Halozati_Nyomtatok.Id into #result
		from KRT_Halozati_Nyomtatok as KRT_Halozati_Nyomtatok
 '
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end

	if (@SelectedRowId is not null)
	begin
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id = @SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	end

	set @sqlcmd = @sqlcmd + N' 
	if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
	BEGIN
		select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
		set @firstRow = (@pageNumber - 1) * @pageSize + 1;
		set @lastRow = @pageNumber*@pageSize;
		select @pageNumber = @pageNumber - 1
	END ;'

	/************************************************************
	* Tényleges select											*
	************************************************************/
     
          
 SET @sqlcmd = @sqlcmd + 
  'select 
		#result.RowNumber,
  	   KRT_Halozati_Nyomtatok.Id,
	   KRT_Halozati_Nyomtatok.Nev,
	   KRT_Halozati_Nyomtatok.Cim,
	   KRT_Halozati_Nyomtatok.Ver,
	   KRT_Halozati_Nyomtatok.Note,
	   KRT_Halozati_Nyomtatok.Stat_id,
	   KRT_Halozati_Nyomtatok.ErvKezd,
	   KRT_Halozati_Nyomtatok.ErvVege,
	   KRT_Halozati_Nyomtatok.Letrehozo_id,
	   KRT_Halozati_Nyomtatok.LetrehozasIdo,
	   KRT_Halozati_Nyomtatok.Modosito_id,
	   KRT_Halozati_Nyomtatok.ModositasIdo,
	   KRT_Halozati_Nyomtatok.Zarolo_id,
	   KRT_Halozati_Nyomtatok.ZarolasIdo,
	   KRT_Halozati_Nyomtatok.Tranz_id,
	   KRT_Halozati_Nyomtatok.UIAccessLog_id
   from 
     KRT_Halozati_Nyomtatok as KRT_Halozati_Nyomtatok     	
	 inner join #result on #result.Id = KRT_Halozati_Nyomtatok.Id
    where RowNumber between @firstRow and @lastRow
	ORDER BY #result.RowNumber;'
	print @sqlcmd
	-- találatok száma és oldalszám
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';
 	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId;
  

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

SET ANSI_NULLS ON



GO
