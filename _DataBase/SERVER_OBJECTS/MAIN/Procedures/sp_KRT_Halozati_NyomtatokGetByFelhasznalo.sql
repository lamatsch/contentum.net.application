IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Halozati_NyomtatokGetByFelhasznalo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Halozati_NyomtatokGetByFelhasznalo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Halozati_NyomtatokGetByFelhasznalo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Halozati_NyomtatokGetByFelhasznalo] AS' 
END
GO

ALTER procedure [dbo].[sp_KRT_Halozati_NyomtatokGetByFelhasznalo]
			  @Id uniqueidentifier,
			  @Where nvarchar(4000) = '',
			  @OrderBy nvarchar(200) = ' order by   KRT_Halozati_Nyomtatok.LetrehozasIdo',
			  @TopRow nvarchar(5) = '',

         @ExecutorUserId uniqueidentifier
         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
		'select ' + @LocalTopRow + '
   		   KRT_Halozati_Nyomtatok.Id,
		   KRT_Halozati_Nyomtatok.Nev,
		   KRT_Halozati_Nyomtatok.Cim,
		   KRT_Halozati_Nyomtatok.Ver,
		   KRT_Halozati_Nyomtatok.Note,
		   KRT_Halozati_Nyomtatok.Stat_id,
		   KRT_Halozati_Nyomtatok.ErvKezd,
		   KRT_Halozati_Nyomtatok.ErvVege,
		   KRT_Halozati_Nyomtatok.Letrehozo_id,
		   KRT_Halozati_Nyomtatok.LetrehozasIdo,
		   KRT_Halozati_Nyomtatok.Modosito_id,
		   KRT_Halozati_Nyomtatok.ModositasIdo,
		   KRT_Halozati_Nyomtatok.Zarolo_id,
		   KRT_Halozati_Nyomtatok.ZarolasIdo,
		   KRT_Halozati_Nyomtatok.Tranz_id,
		   KRT_Halozati_Nyomtatok.UIAccessLog_id
	   from 
		 KRT_Halozati_Nyomtatok as KRT_Halozati_Nyomtatok 
		 inner join 
		 KRT_Felhasznalok_Halozati_Nyomtatoi as KRT_Felhasznalok_Halozati_Nyomtatoi
		 on KRT_Halozati_Nyomtatok.Id = KRT_Felhasznalok_Halozati_Nyomtatoi.Halozati_Nyomtato_Id
		 inner join 
		 KRT_Felhasznalok as KRT_Felhasznalok
		 on KRT_Felhasznalok.Id = KRT_Felhasznalok_Halozati_Nyomtatoi.Felhasznalo_Id
	   where
			KRT_Felhasznalok_Halozati_Nyomtatoi.Felhasznalo_Id = @Id
			and KRT_Felhasznalok_Halozati_Nyomtatoi.ErvKezd <= getdate() and KRT_Felhasznalok_Halozati_Nyomtatoi.ErvVege >= getdate()
			and KRT_Halozati_Nyomtatok.ErvKezd <= getdate() and KRT_Halozati_Nyomtatok.ErvVege >= getdate()
			and KRT_Felhasznalok.ErvKezd <= getdate() and KRT_Felhasznalok.ErvVege >= getdate()'
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  
	exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
SET ANSI_NULLS ON



GO
