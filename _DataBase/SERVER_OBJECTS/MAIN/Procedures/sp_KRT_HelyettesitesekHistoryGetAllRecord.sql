IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_HelyettesitesekHistoryGetAllRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_HelyettesitesekHistoryGetAllRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_HelyettesitesekHistoryGetAllRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_HelyettesitesekHistoryGetAllRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_HelyettesitesekHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from KRT_HelyettesitesekHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_HelyettesitesekHistory Old
         inner join KRT_HelyettesitesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felhasznalo_ID_helyettesitett' as ColumnName,
/*FK*/           dbo.fn_GetKRT_FelhasznalokAzonosito(Old.Felhasznalo_ID_helyettesitett) as OldValue,
/*FK*/           dbo.fn_GetKRT_FelhasznalokAzonosito(New.Felhasznalo_ID_helyettesitett) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_HelyettesitesekHistory Old
         inner join KRT_HelyettesitesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Felhasznalo_ID_helyettesitett != New.Felhasznalo_ID_helyettesitett 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Felhasznalok FTOld on FTOld.Id = Old.Felhasznalo_ID_helyettesitett --and FTOld.Ver = Old.Ver
         left join KRT_Felhasznalok FTNew on FTNew.Id = New.Felhasznalo_ID_helyettesitett --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CsoportTag_ID_helyettesitett' as ColumnName,               cast(Old.CsoportTag_ID_helyettesitett as nvarchar(99)) as OldValue,
               cast(New.CsoportTag_ID_helyettesitett as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_HelyettesitesekHistory Old
         inner join KRT_HelyettesitesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.CsoportTag_ID_helyettesitett != New.CsoportTag_ID_helyettesitett 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felhasznalo_ID_helyettesito' as ColumnName,
/*FK*/           dbo.fn_GetKRT_FelhasznalokAzonosito(Old.Felhasznalo_ID_helyettesito) as OldValue,
/*FK*/           dbo.fn_GetKRT_FelhasznalokAzonosito(New.Felhasznalo_ID_helyettesito) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_HelyettesitesekHistory Old
         inner join KRT_HelyettesitesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Felhasznalo_ID_helyettesito != New.Felhasznalo_ID_helyettesito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Felhasznalok FTOld on FTOld.Id = Old.Felhasznalo_ID_helyettesito --and FTOld.Ver = Old.Ver
         left join KRT_Felhasznalok FTNew on FTNew.Id = New.Felhasznalo_ID_helyettesito --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HelyettesitesMod' as ColumnName,               cast(Old.HelyettesitesMod as nvarchar(99)) as OldValue,
               cast(New.HelyettesitesMod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_HelyettesitesekHistory Old
         inner join KRT_HelyettesitesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HelyettesitesMod != New.HelyettesitesMod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HelyettesitesKezd' as ColumnName,               cast(Old.HelyettesitesKezd as nvarchar(99)) as OldValue,
               cast(New.HelyettesitesKezd as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_HelyettesitesekHistory Old
         inner join KRT_HelyettesitesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HelyettesitesKezd != New.HelyettesitesKezd 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HelyettesitesVege' as ColumnName,               cast(Old.HelyettesitesVege as nvarchar(99)) as OldValue,
               cast(New.HelyettesitesVege as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_HelyettesitesekHistory Old
         inner join KRT_HelyettesitesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HelyettesitesVege != New.HelyettesitesVege 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Megjegyzes' as ColumnName,               cast(Old.Megjegyzes as nvarchar(99)) as OldValue,
               cast(New.Megjegyzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_HelyettesitesekHistory Old
         inner join KRT_HelyettesitesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Megjegyzes != New.Megjegyzes 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end


GO
