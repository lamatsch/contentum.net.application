IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_HelyettesitesekInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_HelyettesitesekInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_HelyettesitesekInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_HelyettesitesekInsert] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_HelyettesitesekInsert]    
                @Id      uniqueidentifier = null,    
               	            @Felhasznalo_ID_helyettesitett     uniqueidentifier,
                @CsoportTag_ID_helyettesitett     uniqueidentifier  = null,
	            @Felhasznalo_ID_helyettesito     uniqueidentifier,
                @HelyettesitesMod     nvarchar(64)  = null,
                @HelyettesitesKezd     datetime  = null,
                @HelyettesitesVege     datetime  = null,
                @Megjegyzes     Nvarchar(4000)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Felhasznalo_ID_helyettesitett is not null
         begin
            SET @insertColumns = @insertColumns + ',Felhasznalo_ID_helyettesitett'
            SET @insertValues = @insertValues + ',@Felhasznalo_ID_helyettesitett'
         end 
       
         if @CsoportTag_ID_helyettesitett is not null
         begin
            SET @insertColumns = @insertColumns + ',CsoportTag_ID_helyettesitett'
            SET @insertValues = @insertValues + ',@CsoportTag_ID_helyettesitett'
         end 
       
         if @Felhasznalo_ID_helyettesito is not null
         begin
            SET @insertColumns = @insertColumns + ',Felhasznalo_ID_helyettesito'
            SET @insertValues = @insertValues + ',@Felhasznalo_ID_helyettesito'
         end 
       
         if @HelyettesitesMod is not null
         begin
            SET @insertColumns = @insertColumns + ',HelyettesitesMod'
            SET @insertValues = @insertValues + ',@HelyettesitesMod'
         end 
       
         if @HelyettesitesKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',HelyettesitesKezd'
            SET @insertValues = @insertValues + ',@HelyettesitesKezd'
         end 
       
         if @HelyettesitesVege is not null
         begin
            SET @insertColumns = @insertColumns + ',HelyettesitesVege'
            SET @insertValues = @insertValues + ',@HelyettesitesVege'
         end 
       
         if @Megjegyzes is not null
         begin
            SET @insertColumns = @insertColumns + ',Megjegyzes'
            SET @insertValues = @insertValues + ',@Megjegyzes'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into KRT_Helyettesitesek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Felhasznalo_ID_helyettesitett uniqueidentifier,@CsoportTag_ID_helyettesitett uniqueidentifier,@Felhasznalo_ID_helyettesito uniqueidentifier,@HelyettesitesMod nvarchar(64),@HelyettesitesKezd datetime,@HelyettesitesVege datetime,@Megjegyzes Nvarchar(4000),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Felhasznalo_ID_helyettesitett = @Felhasznalo_ID_helyettesitett,@CsoportTag_ID_helyettesitett = @CsoportTag_ID_helyettesitett,@Felhasznalo_ID_helyettesito = @Felhasznalo_ID_helyettesito,@HelyettesitesMod = @HelyettesitesMod,@HelyettesitesKezd = @HelyettesitesKezd,@HelyettesitesVege = @HelyettesitesVege,@Megjegyzes = @Megjegyzes,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'KRT_Helyettesitesek',@ResultUid
					,'KRT_HelyettesitesekHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
