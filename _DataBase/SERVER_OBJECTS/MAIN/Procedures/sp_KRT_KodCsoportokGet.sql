IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_KodCsoportokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_KodCsoportokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_KodCsoportokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_KodCsoportokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_KodCsoportokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_KodCsoportok.Id,
	   KRT_KodCsoportok.Org,
	   KRT_KodCsoportok.KodTarak_Id_KodcsoportTipus,
	   KRT_KodCsoportok.Kod,
	   KRT_KodCsoportok.Nev,
	   KRT_KodCsoportok.Modosithato,
	   KRT_KodCsoportok.Hossz,
	   KRT_KodCsoportok.Csoport_Id_Tulaj,
	   KRT_KodCsoportok.Leiras,
	   KRT_KodCsoportok.BesorolasiSema,
	   KRT_KodCsoportok.KiegAdat,
	   KRT_KodCsoportok.KiegMezo,
	   KRT_KodCsoportok.KiegAdattipus,
	   KRT_KodCsoportok.KiegSzotar,
	   KRT_KodCsoportok.Ver,
	   KRT_KodCsoportok.Note,
	   KRT_KodCsoportok.Stat_id,
	   KRT_KodCsoportok.ErvKezd,
	   KRT_KodCsoportok.ErvVege,
	   KRT_KodCsoportok.Letrehozo_id,
	   KRT_KodCsoportok.LetrehozasIdo,
	   KRT_KodCsoportok.Modosito_id,
	   KRT_KodCsoportok.ModositasIdo,
	   KRT_KodCsoportok.Zarolo_id,
	   KRT_KodCsoportok.ZarolasIdo,
	   KRT_KodCsoportok.Tranz_id,
	   KRT_KodCsoportok.UIAccessLog_id
	   from 
		 KRT_KodCsoportok as KRT_KodCsoportok 
	   where
		 KRT_KodCsoportok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
