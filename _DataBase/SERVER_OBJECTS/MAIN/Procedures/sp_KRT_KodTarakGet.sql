IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_KodTarakGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_KodTarakGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_KodTarakGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_KodTarakGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_KodTarakGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_KodTarak.Id,
	   KRT_KodTarak.Org,
	   KRT_KodTarak.KodCsoport_Id,
	   KRT_KodTarak.ObjTip_Id_AdatElem,
	   KRT_KodTarak.Obj_Id,
	   KRT_KodTarak.Kod,
	   KRT_KodTarak.Nev,
	   KRT_KodTarak.RovidNev,
	   KRT_KodTarak.Egyeb,
	   KRT_KodTarak.Modosithato,
	   KRT_KodTarak.Sorrend,
	   KRT_KodTarak.Ver,
	   KRT_KodTarak.Note,
	   KRT_KodTarak.Stat_id,
	   KRT_KodTarak.ErvKezd,
	   KRT_KodTarak.ErvVege,
	   KRT_KodTarak.Letrehozo_id,
	   KRT_KodTarak.LetrehozasIdo,
	   KRT_KodTarak.Modosito_id,
	   KRT_KodTarak.ModositasIdo,
	   KRT_KodTarak.Zarolo_id,
	   KRT_KodTarak.ZarolasIdo,
	   KRT_KodTarak.Tranz_id,
	   KRT_KodTarak.UIAccessLog_id
	   from 
		 KRT_KodTarak as KRT_KodTarak 
	   where
		 KRT_KodTarak.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
