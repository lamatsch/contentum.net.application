IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_KodTarakGetAllByKodcsoportKod]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_KodTarakGetAllByKodcsoportKod]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_KodTarakGetAllByKodcsoportKod]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_KodTarakGetAllByKodcsoportKod] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_KodTarakGetAllByKodcsoportKod]
  @KodcsoportKod nvarchar(200),
  --@Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_KodTarak.Sorrend',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_KodTarak.Id,
	   KRT_KodTarak.Org,
	   KRT_KodTarak.KodCsoport_Id,
	   KRT_KodTarak.ObjTip_Id_AdatElem,
	   KRT_KodTarak.Obj_Id,
	   KRT_KodTarak.Kod,
	   KRT_KodTarak.Nev,
	   KRT_KodCsoportok.Id as Kodcsoportok_Id,
	   KRT_KodCsoportok.Kod as Kodcsoportok_Kod,
	   KRT_KodCsoportok.Nev as Kodcsoportok_Nev,
	   KRT_KodTarak.RovidNev,
	   KRT_KodTarak.Egyeb,
	   KRT_KodTarak.Modosithato,
	   KRT_KodTarak.Sorrend,
	   KRT_KodTarak.Ver,
	   KRT_KodTarak.Note,
	   KRT_KodTarak.Stat_id,
	   KRT_KodTarak.ErvKezd,
	   KRT_KodTarak.ErvVege,
	   KRT_KodTarak.Letrehozo_id,
	   KRT_KodTarak.LetrehozasIdo,
	   KRT_KodTarak.Modosito_id,
	   KRT_KodTarak.ModositasIdo,
	   KRT_KodTarak.Zarolo_id,
	   KRT_KodTarak.ZarolasIdo,
	   KRT_KodTarak.Tranz_id,
	   KRT_KodTarak.UIAccessLog_id  
   from 
     KRT_KodTarak as KRT_KodTarak
	join KRT_KodCsoportok as KRT_KodCsoportok on KRT_KodTarak.KodCsoport_Id = KRT_KodCsoportok.Id
   where 
		KRT_KodCsoportok.Kod='''+@KodcsoportKod+'''
		and getdate() between KRT_KodCsoportok.ErvKezd and KRT_KodCsoportok.ErvVege
		and KRT_KodTarak.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
	
		
     
--   if @Where is not null and @Where!=''
--	begin 
--		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
--	end
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
