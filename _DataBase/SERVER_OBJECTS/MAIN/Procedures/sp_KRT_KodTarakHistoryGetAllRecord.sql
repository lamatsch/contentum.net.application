IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_KodTarakHistoryGetAllRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_KodTarakHistoryGetAllRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_KodTarakHistoryGetAllRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_KodTarakHistoryGetAllRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_KodTarakHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from KRT_KodTarakHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodTarakHistory Old
         inner join KRT_KodTarakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Org' as ColumnName,               cast(Old.Org as nvarchar(99)) as OldValue,
               cast(New.Org as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodTarakHistory Old
         inner join KRT_KodTarakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Org != New.Org 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KodCsoport_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_KodCsoportokAzonosito(Old.KodCsoport_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_KodCsoportokAzonosito(New.KodCsoport_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodTarakHistory Old
         inner join KRT_KodTarakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KodCsoport_Id != New.KodCsoport_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_KodCsoportok FTOld on FTOld.Id = Old.KodCsoport_Id --and FTOld.Ver = Old.Ver
         left join KRT_KodCsoportok FTNew on FTNew.Id = New.KodCsoport_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ObjTip_Id_AdatElem' as ColumnName,
/*FK*/           dbo.fn_GetKRT_ObjTipusokAzonosito(Old.ObjTip_Id_AdatElem) as OldValue,
/*FK*/           dbo.fn_GetKRT_ObjTipusokAzonosito(New.ObjTip_Id_AdatElem) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodTarakHistory Old
         inner join KRT_KodTarakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ObjTip_Id_AdatElem != New.ObjTip_Id_AdatElem 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_ObjTipusok FTOld on FTOld.Id = Old.ObjTip_Id_AdatElem --and FTOld.Ver = Old.Ver
         left join KRT_ObjTipusok FTNew on FTNew.Id = New.ObjTip_Id_AdatElem --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Id' as ColumnName,               cast(Old.Obj_Id as nvarchar(99)) as OldValue,
               cast(New.Obj_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodTarakHistory Old
         inner join KRT_KodTarakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Obj_Id != New.Obj_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kod' as ColumnName,               cast(Old.Kod as nvarchar(99)) as OldValue,
               cast(New.Kod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodTarakHistory Old
         inner join KRT_KodTarakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Kod != New.Kod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Nev' as ColumnName,               cast(Old.Nev as nvarchar(99)) as OldValue,
               cast(New.Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodTarakHistory Old
         inner join KRT_KodTarakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Nev != New.Nev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'RovidNev' as ColumnName,               cast(Old.RovidNev as nvarchar(99)) as OldValue,
               cast(New.RovidNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodTarakHistory Old
         inner join KRT_KodTarakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.RovidNev != New.RovidNev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Egyeb' as ColumnName,               cast(Old.Egyeb as nvarchar(99)) as OldValue,
               cast(New.Egyeb as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodTarakHistory Old
         inner join KRT_KodTarakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Egyeb != New.Egyeb 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Modosithato' as ColumnName,               cast(Old.Modosithato as nvarchar(99)) as OldValue,
               cast(New.Modosithato as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodTarakHistory Old
         inner join KRT_KodTarakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Modosithato != New.Modosithato 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Sorrend' as ColumnName,               cast(Old.Sorrend as nvarchar(99)) as OldValue,
               cast(New.Sorrend as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_KodTarakHistory Old
         inner join KRT_KodTarakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Sorrend != New.Sorrend 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end


GO
