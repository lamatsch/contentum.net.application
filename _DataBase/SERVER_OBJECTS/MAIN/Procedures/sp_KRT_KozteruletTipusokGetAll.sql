IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_KozteruletTipusokGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_KozteruletTipusokGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_KozteruletTipusokGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_KozteruletTipusokGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_KozteruletTipusokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_KozteruletTipusok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_KozteruletTipusok.Id,
	   KRT_KozteruletTipusok.Org,
	   KRT_KozteruletTipusok.KozterTipNev_Id_Szinonima,
	   KRT_KozteruletTipusok.Nev,
	   KRT_KozteruletTipusok.Ver,
	   KRT_KozteruletTipusok.Note,
	   KRT_KozteruletTipusok.Stat_id,
	   KRT_KozteruletTipusok.ErvKezd,
	   KRT_KozteruletTipusok.ErvVege,
	   KRT_KozteruletTipusok.Letrehozo_id,
	   KRT_KozteruletTipusok.LetrehozasIdo,
	   KRT_KozteruletTipusok.Modosito_id,
	   KRT_KozteruletTipusok.ModositasIdo,
	   KRT_KozteruletTipusok.Zarolo_id,
	   KRT_KozteruletTipusok.Tranz_id,
	   KRT_KozteruletTipusok.UIAccessLog_id  
   from 
     KRT_KozteruletTipusok as KRT_KozteruletTipusok      
   '

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
