IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_KozteruletekGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_KozteruletekGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_KozteruletekGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_KozteruletekGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_KozteruletekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Kozteruletek.Id,
	   KRT_Kozteruletek.Org,
	   KRT_Kozteruletek.Nev,
	   KRT_Kozteruletek.Ver,
	   KRT_Kozteruletek.Note,
	   KRT_Kozteruletek.Stat_id,
	   KRT_Kozteruletek.ErvKezd,
	   KRT_Kozteruletek.ErvVege,
	   KRT_Kozteruletek.Letrehozo_id,
	   KRT_Kozteruletek.LetrehozasIdo,
	   KRT_Kozteruletek.Modosito_id,
	   KRT_Kozteruletek.ModositasIdo,
	   KRT_Kozteruletek.Zarolo_id,
	   KRT_Kozteruletek.ZarolasIdo,
	   KRT_Kozteruletek.Tranz_id,
	   KRT_Kozteruletek.UIAccessLog_id
	   from 
		 KRT_Kozteruletek as KRT_Kozteruletek 
	   where
		 KRT_Kozteruletek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
