IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_LoginGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Log_LoginGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_LoginGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Log_LoginGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Log_LoginGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Log_Login.Id,
	   KRT_Log_Login.FelhasznaloId,
	   KRT_Log_Login.FelhasznaloNev,
	   KRT_Log_Login.CsoportTag_Id,
	   KRT_Log_Login.Helyettesito_Id,
	   KRT_Log_Login.Helyettesito_Nev,
	   KRT_Log_Login.Helyettesites_Id,
	   KRT_Log_Login.StartDate,
	   KRT_Log_Login.EndDate,
	   KRT_Log_Login.HibaKod,
	   KRT_Log_Login.HibaUzenet,
	   KRT_Log_Login.LoginType,
	   KRT_Log_Login.Severity,
	   KRT_Log_Login.Ver,
	   KRT_Log_Login.Note,
	   KRT_Log_Login.Letrehozo_id,
	   KRT_Log_Login.Tranz_id
	   from 
		 KRT_Log_Login as KRT_Log_Login 
	   where
		 KRT_Log_Login.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
