IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_LoginGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Log_LoginGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_LoginGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Log_LoginGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Log_LoginGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Log_Login.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Log_Login.Date,
	   KRT_Log_Login.Status,
	   KRT_Log_Login.StartDate,
	   KRT_Log_Login.Felhasznalo_Id,
	   KRT_Log_Login.Felhasznalo_Nev,
	   KRT_Log_Login.CsoportTag_Id,
	   KRT_Log_Login.Helyettesito_Id,
	   KRT_Log_Login.Helyettesito_Nev,
	   KRT_Log_Login.Helyettesites_Id,
	   KRT_Log_Login.Helyettesites_Mod,
	   KRT_Log_Login.LoginType,
	   KRT_Log_Login.UserHostAddress,
	   KRT_Log_Login.Level,
	   KRT_Log_Login.Letrehozo_id,
	   KRT_Log_Login.Tranz_id,
	   KRT_Log_Login.Message,
	   KRT_Log_Login.HibaKod,
	   KRT_Log_Login.HibaUzenet  
   from 
     KRT_Log_Login as KRT_Log_Login      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
