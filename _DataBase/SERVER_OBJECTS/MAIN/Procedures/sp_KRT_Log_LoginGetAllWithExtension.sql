IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_LoginGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Log_LoginGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_LoginGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Log_LoginGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Log_LoginGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @Where_Log_Page nvarchar(4000) = '',
  @Where_Log_WebService	nvarchar(4000) = '',
  @Where_Log_StoredProcedure nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Log_Login.Date',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @FromHistory char(1) = '',
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
    
     
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Log_Login.Date,
  	   KRT_Log_Login.Status,
  	   KRT_Log_Login.StartDate,
	   KRT_Log_Login.Felhasznalo_Id,
	   KRT_Log_Login.Felhasznalo_Nev,
 	   KRT_Log_Login.CsoportTag_Id,
	   KRT_Log_Login.Helyettesito_Id,
	   KRT_Log_Login.Helyettesito_Nev,
 	   KRT_Log_Login.Helyettesites_Id,
 	   KRT_Log_Login.Helyettesites_Mod,
 	   dbo.fn_KodtarErtekNeve(''HELYETTESITES_MOD'', KRT_Log_Login.Helyettesites_Mod,''' + CAST(@Org as NVarChar(40)) + ''') as HelyettesitesMod_Nev,
	   KRT_Log_Login.HibaKod,
	   KRT_Log_Login.HibaUzenet,
	   KRT_Log_Login.LoginType,
	   KRT_Log_Login.Level,
	   KRT_Log_Login.UserHostAddress,
	   KRT_Log_Login.Message,
	   KRT_Log_Login.Letrehozo_id,
	   KRT_Log_Login.Tranz_id
   from 
     KRT_Log_Login as KRT_Log_Login 
           '
      
    if (@Where is not null and @Where!='') or (@Where_Log_WebService is not null and @Where_Log_WebService !='') or (@Where_Log_StoredProcedure is not null and @Where_Log_StoredProcedure !='') or (@Where_Log_Page is not null and @Where_Log_Page !='')
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
		if (@Where is null or @Where='')
		begin
		  SET @sqlcmd = @sqlcmd + ' 1=1 '	
		end
	end

     
    if @Where_Log_Page is not null and @Where_Log_Page!=''
    begin
        SET @sqlcmd = @sqlcmd + ' AND KRT_Log_Login.Tranz_id IN (
        SELECT KRT_Log_Page.Login_Tranz_id FROM KRT_Log_Page
        WHERE ' + @Where_Log_Page + '' 
        
        if   (@Where_Log_WebService is null or @Where_Log_WebService ='') and  (@Where_Log_StoredProcedure is null or @Where_Log_StoredProcedure ='')
        begin
           SET @sqlcmd = @sqlcmd + ')'
        end     
    end
    else
    begin
       if   (@Where_Log_WebService is not null and @Where_Log_WebService !='') or (@Where_Log_StoredProcedure is not null and @Where_Log_StoredProcedure !='')
       begin
             SET @sqlcmd = @sqlcmd + ' AND KRT_Log_Login.Tranz_id IN (
             SELECT KRT_Log_Page.Login_Tranz_id FROM KRT_Log_Page
             WHERE 1=1'
       end     
    end
   
   
    if   @Where_Log_WebService is not null and @Where_Log_WebService !=''
    begin
       SET @sqlcmd = @sqlcmd + ' AND KRT_Log_Page.Tranz_id IN (
       SELECT KRT_Log_WebService.Tranz_id FROM KRT_Log_WebService
       WHERE ' + @Where_Log_WebService + '' 
       
       if   @Where_Log_StoredProcedure is null or @Where_Log_StoredProcedure =''
       begin
         SET @sqlcmd = @sqlcmd + '))'
       end    
    end
    else
    begin
       if @Where_Log_StoredProcedure is not null and @Where_Log_StoredProcedure !=''
       begin
          SET @sqlcmd = @sqlcmd + ' AND KRT_Log_Page.Tranz_id IN (
          SELECT KRT_Log_WebService.Tranz_id FROM KRT_Log_WebService
          WHERE 1=1' 
       end
    end
    
    

   if   @Where_Log_StoredProcedure is not null and @Where_Log_StoredProcedure !=''
   begin
	   SET @sqlcmd = @sqlcmd + ' AND KRT_Log_WebService.StartDate IN (
	   SELECT KRT_Log_StoredProcedure.WS_StartDate FROM KRT_Log_StoredProcedure 
	   WHERE ' + @Where_Log_StoredProcedure + ''
       SET @sqlcmd = @sqlcmd + ')))' 
   end 
             
   SET @sqlcmd = @sqlcmd + @OrderBy;

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
