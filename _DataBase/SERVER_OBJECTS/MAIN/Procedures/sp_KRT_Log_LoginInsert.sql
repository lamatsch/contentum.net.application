IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_LoginInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Log_LoginInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_LoginInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Log_LoginInsert] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Log_LoginInsert]    
                @Id      uniqueidentifier = null,    
                               @FelhasznaloId     uniqueidentifier  = null,
                @FelhasznaloNev     Nvarchar(100)  = null,
                @CsoportTag_Id     uniqueidentifier  = null,
                @Helyettesito_Id     uniqueidentifier  = null,
                @Helyettesito_Nev     Nvarchar(100)  = null,
                @Helyettesites_Id     uniqueidentifier  = null,
                @StartDate     datetime  = null,
                @EndDate     datetime  = null,
                @HibaKod     Nvarchar(4000)  = null,
                @HibaUzenet     Nvarchar(4000)  = null,
                @LoginType     Nvarchar(100)  = null,
                @Severity     int  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Letrehozo_id     uniqueidentifier  = null,
                @Tranz_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;

DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = ''

		 IF(@Id IS NOT NULL)
		 BEGIN
			SET @insertColumns = 'Id'
			SET @insertValues = '@Id'
		 END
       
         if @FelhasznaloId is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloId'
            SET @insertValues = @insertValues + ',@FelhasznaloId'
         end 
       
         if @FelhasznaloNev is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloNev'
            SET @insertValues = @insertValues + ',@FelhasznaloNev'
         end 

         if @CsoportTag_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',CsoportTag_Id'
            SET @insertValues = @insertValues + ',@CsoportTag_Id'
         end  

         if @Helyettesito_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Helyettesito_Id'
            SET @insertValues = @insertValues + ',@Helyettesito_Id'
         end 
       
         if @Helyettesito_Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Helyettesito_Nev'
            SET @insertValues = @insertValues + ',@Helyettesito_Nev'
         end

         if @Helyettesites_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Helyettesites_Id'
            SET @insertValues = @insertValues + ',@Helyettesites_Id'
         end  
       
         if @StartDate is not null
         begin
            SET @insertColumns = @insertColumns + ',StartDate'
            SET @insertValues = @insertValues + ',@StartDate'
         end 
       
         if @EndDate is not null
         begin
            SET @insertColumns = @insertColumns + ',EndDate'
            SET @insertValues = @insertValues + ',@EndDate'
         end 
       
         if @HibaKod is not null
         begin
            SET @insertColumns = @insertColumns + ',HibaKod'
            SET @insertValues = @insertValues + ',@HibaKod'
         end 
       
         if @HibaUzenet is not null
         begin
            SET @insertColumns = @insertColumns + ',HibaUzenet'
            SET @insertValues = @insertValues + ',@HibaUzenet'
         end 
       
         if @LoginType is not null
         begin
            SET @insertColumns = @insertColumns + ',LoginType'
            SET @insertValues = @insertValues + ',@LoginType'
         end 
       
         if @Severity is not null
         begin
            SET @insertColumns = @insertColumns + ',Severity'
            SET @insertValues = @insertValues + ',@Severity'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         END
         
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )               

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)'
SET @InsertCommand = @InsertCommand + 'insert into KRT_Log_Login ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@FelhasznaloId uniqueidentifier,@FelhasznaloNev Nvarchar(100),@CsoportTag_Id uniqueidentifier,@Helyettesito_Id uniqueidentifier,@Helyettesito_Nev Nvarchar(100),@Helyettesites_Id uniqueidentifier,@StartDate datetime,@EndDate datetime,@HibaKod Nvarchar(4000),@HibaUzenet Nvarchar(4000),@LoginType Nvarchar(100),@Severity int,@Ver int,@Note Nvarchar(4000),@Letrehozo_id uniqueidentifier,@Tranz_id uniqueidentifier, @ResultUid uniqueidentifier OUTPUT'
    ,@Id = @Id,@FelhasznaloId = @FelhasznaloId,@FelhasznaloNev = @FelhasznaloNev,@CsoportTag_Id = @CsoportTag_Id,@Helyettesito_Id = @Helyettesito_Id,@Helyettesito_Nev = @Helyettesito_Nev,@Helyettesites_Id = @Helyettesites_Id,@StartDate = @StartDate,@EndDate = @EndDate,@HibaKod = @HibaKod,@HibaUzenet = @HibaUzenet,@LoginType = @LoginType,@Severity = @Severity,@Ver = @Ver,@Note = @Note,@Letrehozo_id = @Letrehozo_id,@Tranz_id = @Tranz_id, @ResultUid = @ResultUid OUTPUT

If @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END

--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
