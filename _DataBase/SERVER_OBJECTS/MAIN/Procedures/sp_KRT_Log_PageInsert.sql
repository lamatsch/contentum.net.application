IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_PageInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Log_PageInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_PageInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Log_PageInsert] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Log_PageInsert]    
                @Id      uniqueidentifier = null,    
                               @ModulId     uniqueidentifier  = null,
                @LoginId     uniqueidentifier  = null,
                @Name     Nvarchar(100)  = null,
                @Url     Nvarchar(4000)  = null,
                @PrevUrl     Nvarchar(4000)  = null,
                @QueryString     Nvarchar(4000)  = null,
                @Command     Nvarchar(100)  = null,
                @IsPostBack     char(1)  = null,
                @IsAsync     char(1)  = null,
                @StartDate     datetime  = null,
                @EndDate     datetime  = null,
                @HibaKod     Nvarchar(4000)  = null,
                @HibaUzenet     Nvarchar(4000)  = null,
                @Severity     int  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Letrehozo_id     uniqueidentifier  = null,
                @Tranz_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;

-- modul id-jA?na kitA¶ltA©se
DECLARE @getModulId uniqueidentifier
SET @getModulId = null
SELECT
    @getModulId = Id
FROM
    KRT_Modulok
WHERE
    Kod = @Name

if @getModulId is not null and @ModulId is null
begin
   SET @ModulId = @getModulId
end 

DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = ''

		 IF(@Id IS NOT NULL)
		 BEGIN
			SET @insertColumns = 'Id'
			SET @insertValues = '@Id'
		 END
       
         if @ModulId is not null
         begin
            SET @insertColumns = @insertColumns + ',ModulId'
            SET @insertValues = @insertValues + ',@ModulId'
         end 
       
         if @LoginId is not null
         begin
            SET @insertColumns = @insertColumns + ',LoginId'
            SET @insertValues = @insertValues + ',@LoginId'
         end 
       
         if @Name is not null
         begin
            SET @insertColumns = @insertColumns + ',Name'
            SET @insertValues = @insertValues + ',@Name'
         end 
       
         if @Url is not null
         begin
            SET @insertColumns = @insertColumns + ',Url'
            SET @insertValues = @insertValues + ',@Url'
         end 
       
         if @PrevUrl is not null
         begin
            SET @insertColumns = @insertColumns + ',PrevUrl'
            SET @insertValues = @insertValues + ',@PrevUrl'
         end 
       
         if @QueryString is not null
         begin
            SET @insertColumns = @insertColumns + ',QueryString'
            SET @insertValues = @insertValues + ',@QueryString'
         end 
       
         if @Command is not null
         begin
            SET @insertColumns = @insertColumns + ',Command'
            SET @insertValues = @insertValues + ',@Command'
         end 
       
         if @IsPostBack is not null
         begin
            SET @insertColumns = @insertColumns + ',IsPostBack'
            SET @insertValues = @insertValues + ',@IsPostBack'
         end 
       
         if @IsAsync is not null
         begin
            SET @insertColumns = @insertColumns + ',IsAsync'
            SET @insertValues = @insertValues + ',@IsAsync'
         end 
       
         if @StartDate is not null
         begin
            SET @insertColumns = @insertColumns + ',StartDate'
            SET @insertValues = @insertValues + ',@StartDate'
         end 
       
         if @EndDate is not null
         begin
            SET @insertColumns = @insertColumns + ',EndDate'
            SET @insertValues = @insertValues + ',@EndDate'
         end 
       
         if @HibaKod is not null
         begin
            SET @insertColumns = @insertColumns + ',HibaKod'
            SET @insertValues = @insertValues + ',@HibaKod'
         end 
       
         if @HibaUzenet is not null
         begin
            SET @insertColumns = @insertColumns + ',HibaUzenet'
            SET @insertValues = @insertValues + ',@HibaUzenet'
         end 
       
         if @Severity is not null
         begin
            SET @insertColumns = @insertColumns + ',Severity'
            SET @insertValues = @insertValues + ',@Severity'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end      

IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1)
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1)  
   
DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier) '
SET @InsertCommand = @InsertCommand + 'insert into KRT_Log_Page ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+') '
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                            N'@Id uniqueidentifier,@ModulId uniqueidentifier,@LoginId uniqueidentifier,@Name Nvarchar(100),@Url Nvarchar(4000),@PrevUrl Nvarchar(4000),@QueryString Nvarchar(4000),@Command Nvarchar(100),@IsPostBack char(1),@IsAsync char(1),@StartDate datetime,@EndDate datetime,@HibaKod Nvarchar(4000),@HibaUzenet Nvarchar(4000),@Severity int,@Ver int,@Note Nvarchar(4000),@Letrehozo_id uniqueidentifier,@Tranz_id uniqueidentifier, @ResultUid uniqueidentifier OUTPUT'
    ,@Id = @Id,@ModulId = @ModulId,@LoginId = @LoginId,@Name = @Name,@Url = @Url,@PrevUrl = @PrevUrl,@QueryString = @QueryString,@Command = @Command,@IsPostBack = @IsPostBack,@IsAsync = @IsAsync,@StartDate = @StartDate,@EndDate = @EndDate,@HibaKod = @HibaKod,@HibaUzenet = @HibaUzenet,@Severity = @Severity,@Ver = @Ver,@Note = @Note,@Letrehozo_id = @Letrehozo_id,@Tranz_id = @Tranz_id, @ResultUid = @ResultUid OUTPUT

If @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END

--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
