IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_StoredProcedureGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Log_StoredProcedureGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_StoredProcedureGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Log_StoredProcedureGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Log_StoredProcedureGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Log_StoredProcedure.Id,
	   KRT_Log_StoredProcedure.WebServiceId,
	   KRT_Log_StoredProcedure.SpName,
	   KRT_Log_StoredProcedure.StartDate,
	   KRT_Log_StoredProcedure.EndDate,
	   KRT_Log_StoredProcedure.HibaUzenet,
	   KRT_Log_StoredProcedure.HibaKod,
	   KRT_Log_StoredProcedure.Severity,
	   KRT_Log_StoredProcedure.Ver,
	   KRT_Log_StoredProcedure.Note,
	   KRT_Log_StoredProcedure.Stat_id,
	   KRT_Log_StoredProcedure.Letrehozo_id,
	   KRT_Log_StoredProcedure.Tranz_id
	   from 
		 KRT_Log_StoredProcedure as KRT_Log_StoredProcedure 
	   where
		 KRT_Log_StoredProcedure.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
