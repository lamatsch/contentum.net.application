IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_StoredProcedureHistoryGetRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Log_StoredProcedureHistoryGetRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_StoredProcedureHistoryGetRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Log_StoredProcedureHistoryGetRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Log_StoredProcedureHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Log_StoredProcedureHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and KRT_Log_StoredProcedureHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Log_StoredProcedureHistory Old
         inner join KRT_Log_StoredProcedureHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'WebServiceId' as ColumnName,               
               cast(Old.WebServiceId as nvarchar(99)) as OldValue,
               cast(New.WebServiceId as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Log_StoredProcedureHistory Old
         inner join KRT_Log_StoredProcedureHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.WebServiceId != New.WebServiceId 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SpName' as ColumnName,               
               cast(Old.SpName as nvarchar(99)) as OldValue,
               cast(New.SpName as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Log_StoredProcedureHistory Old
         inner join KRT_Log_StoredProcedureHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SpName != New.SpName 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'StartDate' as ColumnName,               
               cast(Old.StartDate as nvarchar(99)) as OldValue,
               cast(New.StartDate as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Log_StoredProcedureHistory Old
         inner join KRT_Log_StoredProcedureHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.StartDate != New.StartDate 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EndDate' as ColumnName,               
               cast(Old.EndDate as nvarchar(99)) as OldValue,
               cast(New.EndDate as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Log_StoredProcedureHistory Old
         inner join KRT_Log_StoredProcedureHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.EndDate != New.EndDate 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HibaUzenet' as ColumnName,               
               cast(Old.HibaUzenet as nvarchar(99)) as OldValue,
               cast(New.HibaUzenet as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Log_StoredProcedureHistory Old
         inner join KRT_Log_StoredProcedureHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HibaUzenet != New.HibaUzenet 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HibaKod' as ColumnName,               
               cast(Old.HibaKod as nvarchar(99)) as OldValue,
               cast(New.HibaKod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Log_StoredProcedureHistory Old
         inner join KRT_Log_StoredProcedureHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HibaKod != New.HibaKod 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Severity' as ColumnName,               
               cast(Old.Severity as nvarchar(99)) as OldValue,
               cast(New.Severity as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Log_StoredProcedureHistory Old
         inner join KRT_Log_StoredProcedureHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Severity != New.Severity 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
end


GO
