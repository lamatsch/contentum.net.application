IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_WebServiceGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Log_WebServiceGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Log_WebServiceGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Log_WebServiceGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Log_WebServiceGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Log_WebService.Id,
	   KRT_Log_WebService.PageId,
	   KRT_Log_WebService.PrevId,
	   KRT_Log_WebService.Url,
	   KRT_Log_WebService.MethodName,
	   KRT_Log_WebService.StartDate,
	   KRT_Log_WebService.EndDate,
	   KRT_Log_WebService.HibaKod,
	   KRT_Log_WebService.HibaUzenet,
	   KRT_Log_WebService.ServiceName,
	   KRT_Log_WebService.Severity,
	   KRT_Log_WebService.Ver,
	   KRT_Log_WebService.Note,
	   KRT_Log_WebService.Letrehozo_id,
	   KRT_Log_WebService.Tranz_id
	   from 
		 KRT_Log_WebService as KRT_Log_WebService 
	   where
		 KRT_Log_WebService.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
