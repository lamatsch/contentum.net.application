IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappaSzuleiGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_MappaSzuleiGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappaSzuleiGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_MappaSzuleiGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_MappaSzuleiGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier
         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_MappaSzulei.Id,
	   KRT_MappaSzulei.Mappa_Id,
	   KRT_MappaSzulei.Mappa_Id_Szulo,
	   KRT_MappaSzulei.Ver,
	   KRT_MappaSzulei.Note,
	   KRT_MappaSzulei.Stat_id,
	   KRT_MappaSzulei.ErvKezd,
	   KRT_MappaSzulei.ErvVege,
	   KRT_MappaSzulei.Letrehozo_id,
	   KRT_MappaSzulei.LetrehozasIdo,
	   KRT_MappaSzulei.Modosito_id,
	   KRT_MappaSzulei.ModositasIdo,
	   KRT_MappaSzulei.Zarolo_id,
	   KRT_MappaSzulei.ZarolasIdo,
	   KRT_MappaSzulei.Tranz_id,
	   KRT_MappaSzulei.UIAccessLog_id
	   from 
		 KRT_MappaSzulei as KRT_MappaSzulei 
	   where
		 KRT_MappaSzulei.Id = @Id'

	exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
