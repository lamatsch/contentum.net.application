IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappaTartalmakInvalidateTomeges]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_MappaTartalmakInvalidateTomeges]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappaTartalmakInvalidateTomeges]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_MappaTartalmakInvalidateTomeges] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_KRT_MappaTartalmakInvalidateTomeges]
	@Ugyirat_Ids			nvarchar(MAX) = NULL,
	@IratPeldany_Ids		nvarchar(MAX) = NULL,
	@Kuldemeny_Ids			nvarchar(MAX) = NULL,
	@Mappa_Id				uniqueidentifier,
	@ExecutorUserId			uniqueidentifier,
	@TranzId				uniqueidentifier = NULL
AS
BEGIN
	declare @it	int;
	declare @curId	nvarchar(36);

	-- Ellenorzés fizikai mappa esetén, hogy mozgásban van-e
	IF EXISTS
	(
		SELECT 1 FROM dbo.fn_GetDosszieStatus(@Mappa_Id) AS Statusz
			WHERE Statusz.Tipus = '01'
				AND Statusz.Allapot = 1
	)
	BEGIN
		RAISERROR('[64030]',16,1);
	END

	-- Ügyiratok
	declare @ugyiratTable table(id uniqueidentifier);
	set @it = 0;
	while (@it < ((len(@Ugyirat_Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Ugyirat_Ids,@it*39+2,37);
		insert into @ugyiratTable(id) values(@curId);
		set @it = @it + 1;
	END
	
	-- Iratpéldányok
	declare @iratPeldanyTable table(id uniqueidentifier);
	set @it = 0;
	while (@it < ((len(@IratPeldany_Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@IratPeldany_Ids,@it*39+2,37);
		insert into @iratPeldanyTable(id) values(@curId);
		set @it = @it + 1;
	END
	
	-- Küldemények
	declare @kuldemenyTable table(id uniqueidentifier);
	set @it = 0;
	while (@it < ((len(@Kuldemeny_Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Kuldemeny_Ids,@it*39+2,37);
		insert into @kuldemenyTable(id) values(@curId);
		set @it = @it + 1;
	END


	if @tranzId IS NULL
	BEGIN 
		set @tranzId = newid();
	END
	
	-- Ellenorzés, benne van-e az ojjektum a mappában
	-- Ügyiratok
	IF EXISTS
	(
		SELECT COUNT(DISTINCT Id) FROM dbo.fn_GetAllUgyirat_InDosszie(@Mappa_Id,@Ugyirat_Ids)
		EXCEPT
		SELECT COUNT(DISTINCT Id) FROM @ugyiratTable
	)
	BEGIN
		SELECT DISTINCT Id FROM dbo.fn_GetAllUgyirat_InDosszie(@Mappa_Id,@Ugyirat_Ids)
		EXCEPT
		SELECT DISTINCT Id FROM @ugyiratTable
		
		RAISERROR('[64004]',16,1);
	END
	-- KUldemenyek
	IF EXISTS
	(
		SELECT COUNT(DISTINCT Id) FROM dbo.fn_GetAllKuldemeny_InDosszie(@Mappa_Id,@Kuldemeny_Ids)
		EXCEPT
		SELECT COUNT(DISTINCT Id) FROM @kuldemenyTable
	)
	BEGIN
		SELECT DISTINCT Id FROM dbo.fn_GetAllKuldemeny_InDosszie(@Mappa_Id,@Kuldemeny_Ids)
		EXCEPT
		SELECT DISTINCT Id FROM @kuldemenyTable
		
		RAISERROR('[64014]',16,1);
	END	
	-- Iratpéldányok
	IF EXISTS
	(
		SELECT COUNT(DISTINCT Id) FROM dbo.fn_GetAllIratPeldany_InDosszie(@Mappa_Id,@IratPeldany_Ids)
		EXCEPT
		SELECT COUNT(DISTINCT Id) FROM @iratPeldanyTable
	)
	BEGIN
		SELECT DISTINCT Id FROM dbo.fn_GetAllIratPeldany_InDosszie(@Mappa_Id,@IratPeldany_Ids)
		EXCEPT
		SELECT DISTINCT Id FROM @iratPeldanyTable
		
		RAISERROR('[64024]',16,1);
	END	
	
	
	-- Ellenorzések fizikai mappa esetén
	IF EXISTS (SELECT 1 FROM KRT_Mappak WHERE Id = @Mappa_Id AND Tipus = '01')
	BEGIN
		-- Ügyiratok
		IF EXISTS
		(
			SELECT 1
				FROM @ugyiratTable AS ugyiratTemp
					INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = ugyiratTemp.id
				WHERE EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId
					AND EREC_UgyUgyiratok.Csoport_Id_Felelos != @ExecutorUserId
		)
		BEGIN
			SELECT DISTINCT ugyiratTemp.Id
				FROM @ugyiratTable AS ugyiratTemp
					INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = ugyiratTemp.id
				WHERE EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId
					AND EREC_UgyUgyiratok.Csoport_Id_Felelos != @ExecutorUserId
			RAISERROR('[64003]',16,1);
		END
		-- Kuldemeny
		IF EXISTS
		(
			SELECT 1
				FROM @kuldemenyTable AS kuldemenyTemp
					INNER JOIN EREC_KuldKuldemenyek ON EREC_KuldKuldemenyek.Id = kuldemenyTemp.id
				WHERE EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId
					AND EREC_KuldKuldemenyek.Csoport_Id_Felelos != @ExecutorUserId
		)
		BEGIN
			SELECT DISTINCT kuldemenyTemp.Id
				FROM @kuldemenyTable AS kuldemenyTemp
					INNER JOIN EREC_KuldKuldemenyek ON EREC_KuldKuldemenyek.Id = kuldemenyTemp.id
				WHERE EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId
					AND EREC_KuldKuldemenyek.Csoport_Id_Felelos != @ExecutorUserId
			RAISERROR('[64013]',16,1);
		END
		-- Iratpeldany
		IF EXISTS
		(
			SELECT 1
				FROM @iratPeldanyTable AS iratpeldanyTemp
					INNER JOIN EREC_PldIratPeldanyok ON EREC_PldIratPeldanyok.Id = iratpeldanyTemp.id
				WHERE EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId
					AND EREC_PldIratPeldanyok.Csoport_Id_Felelos != @ExecutorUserId
		)
		BEGIN
			SELECT DISTINCT iratpeldanyTemp.Id
				FROM @iratPeldanyTable AS iratpeldanyTemp
					INNER JOIN EREC_PldIratPeldanyok ON EREC_PldIratPeldanyok.Id = iratpeldanyTemp.id
				WHERE EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId
					AND EREC_PldIratPeldanyok.Csoport_Id_Felelos != @ExecutorUserId
			RAISERROR('[64023]',16,1);
		END
		
	END
	
	
	UPDATE KRT_MappaTartalmak SET
		ErvVege = 
			CASE
				WHEN ErvVege < GETDATE() THEN ErvVege
				ELSE GETDATE()
			END,
		ErvKezd =
			CASE
				WHEN ErvKezd > GETDATE() THEN GETDATE()
				ELSE ErvKezd
			END,
		Ver = Ver + 1,
		KRT_MappaTartalmak.Modosito_id = @ExecutorUserId,
		ModositasIdo = GETDATE(),
		Tranz_id = @TranzId
		WHERE Obj_Id IN
		(
			SELECT Id FROM @ugyiratTable
			UNION ALL
			SELECT Id FROM @kuldemenyTable
			UNION ALL
			SELECT Id FROM @iratPeldanyTable
		);

END


GO
