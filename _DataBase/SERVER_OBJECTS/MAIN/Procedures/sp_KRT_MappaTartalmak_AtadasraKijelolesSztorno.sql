IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappaTartalmak_AtadasraKijelolesSztorno]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_MappaTartalmak_AtadasraKijelolesSztorno]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappaTartalmak_AtadasraKijelolesSztorno]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_MappaTartalmak_AtadasraKijelolesSztorno] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_MappaTartalmak_AtadasraKijelolesSztorno]
	@Id					UNIQUEIDENTIFIER,
	@ExecutorUserId		uniqueidentifier
as

BEGIN TRY
	set nocount on
	declare @execTime datetime;
	set @execTime = getdate();

	declare @updatedUgyiratIds table (Id uniqueidentifier)
	declare @updatedKuldemenyIds table (Id uniqueidentifier)
	declare @elsodlegesPldIds table (Id uniqueidentifier)

	-- Iratpéldány mozgatáshoz
	CREATE TABLE #pldIds(Id UNIQUEIDENTIFIER)

	-- Kimeno küldemény mozgatáshoz
	CREATE TABLE #kuldIds_kimeno(Id UNIQUEIDENTIFIER)

	declare @tempTable table(id uniqueidentifier);
	-- Hierarchia együttmozgatása:
	;WITH MappaHierarchy
	as	(
		-- Base case
		select @Id Id

		UNION ALL

		-- Recursive step
		select m.Id
		from KRT_Mappak m
			inner join MappaHierarchy mh
			on mh.Id = m.SzuloMappa_Id
			where getdate() between m.ErvKezd and m.ErvVege
	)
	insert into @tempTable(id)
		select mh.Id from MappaHierarchy mh --where mh.Id not in (select Id from @tempTable)

	declare @ugyiratIds table(id uniqueidentifier)
	INSERT INTO @ugyiratIds(id)
	SELECT Id from EREC_UgyUgyiratok 
			WHERE EREC_UgyUgyiratok.Id IN (
					SELECT KRT_MappaTartalmak.Obj_Id
						FROM KRT_MappaTartalmak
							INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
							INNER JOIN @tempTable t on t.Id = KRT_Mappak.Id
						WHERE GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
							--AND KRT_MappaTartalmak.Mappa_Id = @Id
	--						AND KRT_Mappak.Tipus = '01'
							AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
			)

    -- Elokészített szerelésre figyelni kell, azokat nem hozzuk
    -- (Azokat kell hozni, ahol UgyUgyirat_Id_Szulo ki van töltve, és vagy az Allapot, vagy a TovabbitasAlattAllapot szerelt(60) )

	;WITH UgyiratHierarchy  AS
	(
	   -- Base case	   
	   SELECT Id
	   FROM EREC_UgyUgyiratok
	   WHERE UgyUgyirat_Id_Szulo IN 
	   (SELECT Id from @ugyiratIds)
			AND (Allapot = '60' OR TovabbitasAlattAllapot = '60')
		AND Id not in (SELECT Id from @ugyiratIds) -- amik benne vannak, már nem hozzuk

	   UNION ALL

	   -- Recursive step
	   SELECT u.Id
	   FROM EREC_UgyUgyiratok as u
		  INNER JOIN UgyiratHierarchy as uh ON
			 u.UgyUgyirat_Id_Szulo = uh.Id
	   WHERE (u.Allapot = '60' OR u.TovabbitasAlattAllapot = '60')
	)
	INSERT INTO @ugyiratIds(id)
	SELECT Id
	FROM UgyiratHierarchy

	-- Az ügyiratok módosítása elott az elsodleges példányok kigyujtése
	INSERT INTO @elsodlegesPldIds SELECT p.Id
	FROM EREC_PldIratPeldanyok p
		INNER JOIN EREC_IraIratok i ON p.IraIrat_Id = i.Id
		INNER JOIN EREC_UgyUgyiratok u ON i.Ugyirat_Id = u.Id
	WHERE u.Id in (select Id from @ugyiratIds)
	AND p.Allapot = '50'
	AND p.Sorszam = 1
	AND u.FelhasznaloCsoport_Id_Orzo = p.FelhasznaloCsoport_Id_Orzo
	AND u.Csoport_Id_Felelos = p.Csoport_Id_Felelos



	-- Ügyiratok sztornó
	UPDATE EREC_UgyUgyiratok
		SET Csoport_Id_Felelos = Csoport_Id_Felelos_Elozo,
			Csoport_Id_Felelos_Elozo = Csoport_Id_Felelos,
			Allapot = 
				CASE
					WHEN Allapot = '60' THEN '60'
					WHEN TovabbitasAlattAllapot IS NOT NULL AND TovabbitasAlattAllapot != '' THEN TovabbitasAlattAllapot
					ELSE Allapot
				END,
			TovabbitasAlattAllapot =
				CASE
					WHEN Allapot = '60' THEN TovabbitasAlattAllapot
					WHEN TovabbitasAlattAllapot IS NOT NULL THEN NULL
					ELSE TovabbitasAlattAllapot
				END,
			Ver = Ver + 1,
			ModositasIdo = @exectime,
			Modosito_id = @ExecutorUserId
		OUTPUT inserted.Id INTO @updatedUgyiratIds
		WHERE EREC_UgyUgyiratok.Id IN
			(
				select Id from @ugyiratIds
--				SELECT KRT_MappaTartalmak.Obj_Id
--					FROM KRT_MappaTartalmak
--						INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
--						INNER JOIN @tempTable t on t.Id = KRT_Mappak.Id
--					WHERE GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
--						--AND KRT_MappaTartalmak.Mappa_Id = @Id
----						AND KRT_Mappak.Tipus = '01'
--						AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
			)
	/* History Log */
	declare @row_ids varchar(MAX)
	/*** EREC_UgyUgyiratok history log ***/

	set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @updatedUgyiratIds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
		set @row_ids = @row_ids + '''';

	exec sp_LogRecordsToHistory_Tomeges
	 @TableName = 'EREC_UgyUgyiratok'
	,@Row_Ids = @row_ids
	,@Muvelet = 1
	,@Vegrehajto_Id = @ExecutorUserId
	,@VegrehajtasIdo = @execTime
		
			
	-- Küldemények sztornó
	UPDATE EREC_KuldKuldemenyek
		SET Csoport_Id_Felelos = Csoport_Id_Felelos_Elozo,
			Csoport_Id_Felelos_Elozo = Csoport_Id_Felelos,
			Allapot = 
				CASE
					WHEN TovabbitasAlattAllapot IS NOT NULL AND TovabbitasAlattAllapot != '' THEN TovabbitasAlattAllapot
					ELSE Allapot
				END,
			TovabbitasAlattAllapot =
				CASE
					WHEN TovabbitasAlattAllapot IS NOT NULL THEN NULL
					ELSE TovabbitasAlattAllapot
				END,
			Ver = Ver + 1,
			ModositasIdo = @exectime,
			Modosito_id = @ExecutorUserId
		OUTPUT inserted.Id INTO @updatedKuldemenyIds
		WHERE EREC_KuldKuldemenyek.Id IN
			(
				SELECT KRT_MappaTartalmak.Obj_Id
					FROM KRT_MappaTartalmak
						INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
						INNER JOIN @tempTable t on t.Id = KRT_Mappak.Id
					WHERE GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
--						AND KRT_MappaTartalmak.Mappa_Id = @Id
--						AND KRT_Mappak.Tipus = '01'
						AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
			)

	-- Kimeno küldemények leválogatása példány és iratmozgatáshoz
	INSERT INTO #kuldIds_kimeno
		select EREC_KuldKuldemenyek.Id
			from @updatedKuldemenyIds uk
			inner join EREC_KuldKuldemenyek on uk.Id=EREC_KuldKuldemenyek.Id and EREC_KuldKuldemenyek.PostazasIranya='2' -- kimeno
			

	/* History Log */
	/*** EREC_KuldKuldemenyek history log ***/
	set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @updatedKuldemenyIds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
		set @row_ids = @row_ids + '''';

	exec sp_LogRecordsToHistory_Tomeges
	 @TableName = 'EREC_KuldKuldemenyek'
	,@Row_Ids = @row_ids
	,@Muvelet = 1
	,@Vegrehajto_Id = @ExecutorUserId
	,@VegrehajtasIdo = @execTime
	
	
	UPDATE EREC_PldIratPeldanyok
		SET Csoport_Id_Felelos = Csoport_Id_Felelos_Elozo,
			Csoport_Id_Felelos_Elozo = Csoport_Id_Felelos,
			Allapot = 
				CASE
					WHEN TovabbitasAlattAllapot IS NOT NULL AND TovabbitasAlattAllapot != '' THEN TovabbitasAlattAllapot
					ELSE Allapot
				END,
			TovabbitasAlattAllapot =
				CASE
					WHEN TovabbitasAlattAllapot IS NOT NULL THEN NULL
					ELSE TovabbitasAlattAllapot
				END,
			Ver = Ver + 1,
			ModositasIdo = @exectime,
			Modosito_id = @ExecutorUserId
		OUTPUT inserted.Id INTO #pldIds--@updatedPldIds
		--WHERE EREC_PldIratPeldanyok.Id IN (SELECT Id FROM [#pldIds])
		WHERE EREC_PldIratPeldanyok.Id IN
			(
				SELECT KRT_MappaTartalmak.Obj_Id
					FROM KRT_MappaTartalmak
						INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
						INNER JOIN @tempTable t on t.Id = KRT_Mappak.Id
					WHERE GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
--						AND KRT_MappaTartalmak.Mappa_Id = @Id
--						AND KRT_Mappak.Tipus = '01'
						AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
				UNION ALL
				select Id from @elsodlegesPldIds
			)
	/* History Log */
	/*** EREC_PldIratPeldanyok history log ***/
	set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from #pldIds FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
		set @row_ids = @row_ids + '''';

	exec sp_LogRecordsToHistory_Tomeges
	 @TableName = 'EREC_PldIratPeldanyok'
	,@Row_Ids = @row_ids
	,@Muvelet = 1
	,@Vegrehajto_Id = @ExecutorUserId
	,@VegrehajtasIdo = @execTime

	-- iratpéldány mozgásának jelzése
	exec sp_EREC_PldIratPeldanyok_Moved @ExecutorUserId = @ExecutorUserId, @ExecutionTime = @exectime
		
	DROP TABLE [#pldIds]

	-- kimeno küldemény mozgásának jelzése
	if exists(select 1 from #kuldIds_kimeno)
	begin
		exec sp_EREC_KimenoKuldemenyek_Moved @ExecutorUserId = @ExecutorUserId, @ExecutionTime = @exectime
	end
	DROP TABLE #kuldIds_kimeno;
	
END TRY
BEGIN CATCH

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
