IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappaUtakGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_MappaUtakGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappaUtakGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_MappaUtakGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_MappaUtakGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_MappaUtak.Id,
	   KRT_MappaUtak.Mappa_Id,
	   KRT_MappaUtak.PathFromORG,
	   KRT_MappaUtak.Ver,
	   KRT_MappaUtak.Note,
	   KRT_MappaUtak.Stat_id,
	   KRT_MappaUtak.ErvKezd,
	   KRT_MappaUtak.ErvVege,
	   KRT_MappaUtak.Letrehozo_id,
	   KRT_MappaUtak.LetrehozasIdo,
	   KRT_MappaUtak.Modosito_id,
	   KRT_MappaUtak.ModositasIdo,
	   KRT_MappaUtak.Zarolo_id,
	   KRT_MappaUtak.ZarolasIdo,
	   KRT_MappaUtak.Tranz_id,
	   KRT_MappaUtak.UIAccessLog_id
	   from 
		 KRT_MappaUtak as KRT_MappaUtak 
	   where
		 KRT_MappaUtak.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
