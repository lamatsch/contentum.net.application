IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappakGetAllParents]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_MappakGetAllParents]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappakGetAllParents]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_MappakGetAllParents] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_MappakGetAllParents]
        @Id	uniqueidentifier,
		@ExecutorUserId UNIQUEIDENTIFIER,
		@FelhasznaloSzervezet_Id	UNIQUEIDENTIFIER

as

BEGIN TRY
  
set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	;with sz as
	(
		select Id, SzuloMappa_Id,
			0 as HierarchyLevel
			from KRT_Mappak where Id = @Id--(select SzuloMappa_Id from KRT_Mappak where Id = @Id)
		UNION ALL
		select m.Id, m.SzuloMappa_Id,
			sz.HierarchyLevel + 1 as HierarchyLevel
			from KRT_Mappak m
			inner join sz on m.Id = sz.SzuloMappa_Id
			and getdate() between m.ErvKezd and m.ErvVege
	)
	select 
  	   KRT_Mappak.Id,
	   KRT_Mappak.SzuloMappa_Id,
	   KRT_Mappak.BarCode,
	   KRT_Mappak.Tipus,
	   KRT_Mappak.Nev,
	   KRT_Mappak.Leiras,
	   KRT_Mappak.Csoport_Id_Tulaj,
	   KRT_Mappak.Ver,
	   KRT_Mappak.Note,
	   KRT_Mappak.Stat_id,
	   KRT_Mappak.ErvKezd,
	   KRT_Mappak.ErvVege,
	   KRT_Mappak.Letrehozo_id,
	   KRT_Mappak.LetrehozasIdo,
	   KRT_Mappak.Modosito_id,
	   KRT_Mappak.ModositasIdo,
	   KRT_Mappak.Zarolo_id,
	   KRT_Mappak.ZarolasIdo,
	   KRT_Mappak.Tranz_id,
	   KRT_Mappak.UIAccessLog_id 
	 from KRT_Mappak
			inner join sz on sz.Id = KRT_Mappak.Id
		order by sz.HierarchyLevel


END TRY
BEGIN CATCH

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
