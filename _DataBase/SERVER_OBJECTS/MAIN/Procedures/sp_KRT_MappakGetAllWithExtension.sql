IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappakGetAllWithExtension]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_MappakGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappakGetAllWithExtension]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_MappakGetAllWithExtension] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_MappakGetAllWithExtension]
		@Where nvarchar(MAX) = '',
		@OrderBy nvarchar(200) = ' order by   KRT_Mappak.LetrehozasIdo',
		@TopRow nvarchar(5) = '',
		@ExecutorUserId UNIQUEIDENTIFIER,
		@FelhasznaloSzervezet_Id	UNIQUEIDENTIFIER,
		@Jogosultak CHAR(1) = '0'
as

begin
BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
   
  SET @sqlcmd = '';
  
  --IF @Jogosultak = '1' AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
  IF @Jogosultak = '1' AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
  BEGIN
	  SET @sqlcmd = '
		SELECT Id into #filter FROM
		(
		SELECT KRT_Mappak.Id
		FROM KRT_Mappak
			INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
		WHERE krt_jogosultak.Csoport_Id_Jogalany IN (''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''')
		UNION
		SELECT KRT_Mappak.Id
		FROM KRT_Mappak
		WHERE KRT_Mappak.Csoport_Id_Tulaj IN (''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''')
		) AS temp
		'
  END
          
  SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   KRT_Mappak.Id,
	   KRT_Mappak.SzuloMappa_Id,
	   KRT_Mappak.BarCode,
	   KRT_Mappak.Tipus,
	   KRT_Mappak.Nev,
	   KRT_Mappak.Leiras,
	   KRT_Mappak.Csoport_Id_Tulaj,
	   KRT_Csoportok.Nev AS Csoport_Id_Tulaj_Nev,
	   KRT_Mappak.Ver,
	   KRT_Mappak.Note,
	   KRT_Mappak.Stat_id,
	   KRT_Mappak.ErvKezd,
	   KRT_Mappak.ErvVege,
	   KRT_Mappak.Letrehozo_id,
	   KRT_Mappak.LetrehozasIdo,
	   KRT_Mappak.Modosito_id,
	   KRT_Mappak.ModositasIdo,
	   KRT_Mappak.Zarolo_id,
	   KRT_Mappak.ZarolasIdo,
	   KRT_Mappak.Tranz_id,
	   KRT_Mappak.UIAccessLog_id  
   from 
     KRT_Mappak as KRT_Mappak 
		inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_Mappak.Csoport_Id_Tulaj and KRT_Csoportok.Org=''' + cast(@Org as NVarChar(40)) + '''
'
   --IF @Jogosultak = '1' AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
	IF @Jogosultak = '1' AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
		SET @sqlcmd = @sqlcmd + '
		inner join #filter on #filter.Id = KRT_Mappak.Id '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   
   --IF @Jogosultak = '1' AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
	IF @Jogosultak = '1' AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
	SET @sqlcmd = @sqlcmd + ' drop table #filter; ';
	
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
