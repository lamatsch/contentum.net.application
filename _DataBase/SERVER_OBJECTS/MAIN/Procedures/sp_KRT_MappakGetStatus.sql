IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappakGetStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_MappakGetStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappakGetStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_MappakGetStatus] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_KRT_MappakGetStatus]
	@Id	UNIQUEIDENTIFIER
AS
BEGIN
	SELECT * FROM dbo.fn_GetDosszieStatus(@Id);
END


GO
