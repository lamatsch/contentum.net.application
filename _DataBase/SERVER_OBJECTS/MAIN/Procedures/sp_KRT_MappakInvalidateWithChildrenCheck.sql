IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappakInvalidateWithChildrenCheck]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_MappakInvalidateWithChildrenCheck]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappakInvalidateWithChildrenCheck]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_MappakInvalidateWithChildrenCheck] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_MappakInvalidateWithChildrenCheck]
        @Id							uniqueidentifier,
		@ExecutorUserId				uniqueidentifier,
		@ExecutionTime					datetime			

as

BEGIN TRY
 
	set nocount on
   
	IF EXISTS (SELECT 1 FROM dbo.fn_GetDosszieStatus(@Id) WHERE Allapot = 1 AND Tipus = '01')
	BEGIN
		RAISERROR('[64008]',16,1)
	END

	IF EXISTS (SELECT 1 FROM KRT_Mappak WHERE SzuloMappa_Id = @Id AND GETDATE() BETWEEN ErvKezd AND ErvVege )
	BEGIN
		RAISERROR('[64001]',16,1)
	END
	ELSE
	BEGIN
		-- TODO: KRT_MappaTartalmak multiInvalidate

		EXEC sp_KRT_MappakInvalidate	@Id = @Id,	@ExecutorUserId = @ExecutorUserId,	@ExecutionTime = @ExecutionTime
		
		-- KRT_MappaTartalmak érvénytelenítése
		UPDATE KRT_MappaTartalmak SET
			ErvVege = 
				CASE
					WHEN ErvVege < GETDATE() THEN ErvVege
					ELSE GETDATE()
				END,
			ErvKezd =
				CASE
					WHEN ErvKezd > GETDATE() THEN GETDATE()
					ELSE ErvKezd
				END,
			Ver = Ver + 1,
			KRT_MappaTartalmak.Modosito_id = @ExecutorUserId,
			ModositasIdo = @ExecutionTime
		WHERE Mappa_Id = @Id
		
	END
	
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InvalidateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
