IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappakUpdateWithCheck]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_MappakUpdateWithCheck]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MappakUpdateWithCheck]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_MappakUpdateWithCheck] AS' 
END
GO

ALTER procedure [dbo].[sp_KRT_MappakUpdateWithCheck]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @SzuloMappa_Id     uniqueidentifier  = null,         
             @BarCode     Nvarchar(100)  = null,         
             @Tipus     nvarchar(64)  = null,         
             @Nev     Nvarchar(400)  = null,         
             @Leiras     Nvarchar(4000)  = null,         
             @Csoport_Id_Tulaj     uniqueidentifier  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_SzuloMappa_Id     uniqueidentifier         
     DECLARE @Act_BarCode     Nvarchar(100)         
     DECLARE @Act_Tipus     nvarchar(64)         
     DECLARE @Act_Nev     Nvarchar(400)         
     DECLARE @Act_Leiras     Nvarchar(4000)         
     DECLARE @Act_Csoport_Id_Tulaj     uniqueidentifier         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_SzuloMappa_Id = SzuloMappa_Id,
     @Act_BarCode = BarCode,
     @Act_Tipus = Tipus,
     @Act_Nev = Nev,
     @Act_Leiras = Leiras,
     @Act_Csoport_Id_Tulaj = Csoport_Id_Tulaj,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from KRT_Mappak
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zĂˇrolĂˇs ellenĹ‘rzĂ©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/SzuloMappa_Id')=1
         SET @Act_SzuloMappa_Id = @SzuloMappa_Id
   IF @UpdatedColumns.exist('/root/BarCode')=1
         SET @Act_BarCode = @BarCode
   IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @Act_Tipus = @Tipus
   IF @UpdatedColumns.exist('/root/Nev')=1
         SET @Act_Nev = @Nev
   IF @UpdatedColumns.exist('/root/Leiras')=1
         SET @Act_Leiras = @Leiras
   IF @UpdatedColumns.exist('/root/Csoport_Id_Tulaj')=1
         SET @Act_Csoport_Id_Tulaj = @Csoport_Id_Tulaj
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1


declare @currentSzuloMappa_Id uniqueidentifier -- az eredeti vagy az Ă©Ă©pen mĂłdosĂ­tandĂł szĂĽlĹ‘mappa

IF @UpdatedColumns.exist('/root/SzuloMappa_Id')=1 
BEGIN
	SET @currentSzuloMappa_Id = @SzuloMappa_Id
END
ELSE
BEGIN
	SET @currentSzuloMappa_Id = (select SzuloMappa_Id from KRT_Mappak where Id = @Id)
END
-- Tulajdonos ellenĹ‘rzĂ©se fizikai mappa Ă©s mĂłdosĂ­tott szĂĽlĹ‘ esetĂ©n
IF @UpdatedColumns.exist('/root/SzuloMappa_Id')=1 AND @currentSzuloMappa_Id is not null
BEGIN
--	IF exists(select 1 from KRT_Mappak
--			where Id = @SzuloMappa_Id
--			and Tipus = '01'
--			and Csoport_Id_Tulaj != @ExecutorUserId)
--	BEGIN
--		-- A dossziĂ© nem helyezhetĹ‘ a kivĂˇlasztott fizikai dossziĂ©ba, mert az nem Ă–nnĂ©l van!
--		RAISERROR('[64037]', 16, 1)
--	END

	IF @UpdatedColumns.exist('/root/Csoport_Id_Tulaj')=1
	BEGIN
		IF @Csoport_Id_Tulaj is not null
		and exists(select 1 from KRT_Mappak
			where Id = @currentSzuloMappa_Id
			and Tipus = '01'
			and Csoport_Id_Tulaj != @Csoport_Id_Tulaj)
		BEGIN
			-- A dossziĂ© nem helyezhetĹ‘ a szĂĽlĹ‘nek kivĂˇlaszott fizikai dossziĂ©ba, mert a dossziĂ©k tulajdonosai kĂĽlĂ¶nbĂ¶znek!
			RAISERROR('[64038]', 16, 1)
		END
	END
	ELSE
	BEGIN
		IF exists(select 1 from KRT_Mappak
			where Id = @currentSzuloMappa_Id
			and Tipus = '01'
			and Csoport_Id_Tulaj != (select Csoport_Id_Tulaj from KRT_Mappak where Id=@Id))
		BEGIN
			-- A dossziĂ© nem helyezhetĹ‘ a szĂĽlĹ‘nek kivĂˇlaszott fizikai dossziĂ©ba, mert a dossziĂ©k tulajdonosai kĂĽlĂ¶nbĂ¶znek!
			RAISERROR('[64038]', 16, 1)
		END
	END
END

   
-- Tartalom ellenĹ‘rzĂ©se fizikai mappĂˇra update esetĂ©n
IF @UpdatedColumns.exist('/root/Tipus')=1 AND @Tipus = '01'
BEGIN
--	IF EXISTS
--	(
--		SELECT 1
--			FROM KRT_MappaTartalmak
--				INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
--				INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = KRT_MappaTartalmak.Obj_Id
--			WHERE KRT_MappaTartalmak.Mappa_Id = @Id
--				AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
--				AND (EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != KRT_Mappak.Csoport_Id_Tulaj
--					OR EREC_UgyUgyiratok.Csoport_Id_Felelos != KRT_Mappak.Csoport_Id_Tulaj
--					OR EREC_UgyUgyiratok.Allapot = '50')
--		UNION ALL
--		SELECT 1
--			FROM KRT_MappaTartalmak
--				INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
--				INNER JOIN EREC_PldIratPeldanyok ON EREC_PldIratPeldanyok.Id = KRT_MappaTartalmak.Obj_Id
--			WHERE KRT_MappaTartalmak.Mappa_Id = @Id
--				AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
--				AND (EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo != KRT_Mappak.Csoport_Id_Tulaj
--					OR EREC_PldIratPeldanyok.Csoport_Id_Felelos != KRT_Mappak.Csoport_Id_Tulaj
--					OR EREC_PldIratPeldanyok.Allapot = '50')
--		UNION ALL
--		SELECT 1
--			FROM KRT_MappaTartalmak
--				INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
--				INNER JOIN EREC_KuldKuldemenyek ON EREC_KuldKuldemenyek.Id = KRT_MappaTartalmak.Obj_Id
--			WHERE KRT_MappaTartalmak.Mappa_Id = @Id
--				AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
--				AND (EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo != KRT_Mappak.Csoport_Id_Tulaj
--					OR EREC_KuldKuldemenyek.Csoport_Id_Felelos != KRT_Mappak.Csoport_Id_Tulaj
--					OR EREC_KuldKuldemenyek.Allapot = '03')
--	)
--	BEGIN
--		SELECT KRT_MappaTartalmak.Obj_Id
--			FROM KRT_MappaTartalmak
--				INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
--				INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = KRT_MappaTartalmak.Obj_Id
--			WHERE KRT_MappaTartalmak.Mappa_Id = @Id
--				AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
--				AND (EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != KRT_Mappak.Csoport_Id_Tulaj
--					OR EREC_UgyUgyiratok.Csoport_Id_Felelos != KRT_Mappak.Csoport_Id_Tulaj
--					OR EREC_UgyUgyiratok.Allapot = '50')
--		UNION ALL
--		SELECT KRT_MappaTartalmak.Obj_Id
--			FROM KRT_MappaTartalmak
--				INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
--				INNER JOIN EREC_PldIratPeldanyok ON EREC_PldIratPeldanyok.Id = KRT_MappaTartalmak.Obj_Id
--			WHERE KRT_MappaTartalmak.Mappa_Id = @Id
--				AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
--				AND (EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo != KRT_Mappak.Csoport_Id_Tulaj
--					OR EREC_PldIratPeldanyok.Csoport_Id_Felelos != KRT_Mappak.Csoport_Id_Tulaj
--					OR EREC_PldIratPeldanyok.Allapot = '50')
--		UNION ALL
--		SELECT KRT_MappaTartalmak.Obj_Id
--			FROM KRT_MappaTartalmak
--				INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
--				INNER JOIN EREC_KuldKuldemenyek ON EREC_KuldKuldemenyek.Id = KRT_MappaTartalmak.Obj_Id
--			WHERE KRT_MappaTartalmak.Mappa_Id = @Id
--				AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
--				AND (EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo != KRT_Mappak.Csoport_Id_Tulaj
--					OR EREC_KuldKuldemenyek.Csoport_Id_Felelos != KRT_Mappak.Csoport_Id_Tulaj
--					OR EREC_KuldKuldemenyek.Allapot = '03')
		IF EXISTS (SELECT 1 FROM dbo.fn_GetDosszieStatus(@Id) AS status WHERE Allapot = 1)
		BEGIN

			declare @tempTable table(id uniqueidentifier);
			-- Hierarchia egyĂĽttmozgatĂˇsa:
			;WITH MappaHierarchy
			as	(
				-- Base case
				select @Id Id

				UNION ALL

				-- Recursive step
				select m.Id
				from KRT_Mappak m
					inner join MappaHierarchy mh
					on mh.Id = m.SzuloMappa_Id
					where getdate() between m.ErvKezd and m.ErvVege
			)
			insert into @tempTable(id)
				select mh.Id from MappaHierarchy mh

		SELECT KRT_MappaTartalmak.Obj_Id
			FROM KRT_MappaTartalmak
				INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
				INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = KRT_MappaTartalmak.Obj_Id
			WHERE KRT_MappaTartalmak.Mappa_Id in (select Id from @tempTable) --= @Id
				AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
				AND (EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != KRT_Mappak.Csoport_Id_Tulaj
					OR EREC_UgyUgyiratok.Csoport_Id_Felelos != KRT_Mappak.Csoport_Id_Tulaj
					OR EREC_UgyUgyiratok.Allapot = '50')
		UNION ALL
		SELECT KRT_MappaTartalmak.Obj_Id
			FROM KRT_MappaTartalmak
				INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
				INNER JOIN EREC_PldIratPeldanyok ON EREC_PldIratPeldanyok.Id = KRT_MappaTartalmak.Obj_Id
			WHERE KRT_MappaTartalmak.Mappa_Id in (select Id from @tempTable) --= @Id
				AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
				AND (EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo != KRT_Mappak.Csoport_Id_Tulaj
					OR EREC_PldIratPeldanyok.Csoport_Id_Felelos != KRT_Mappak.Csoport_Id_Tulaj
					OR EREC_PldIratPeldanyok.Allapot = '50')
		UNION ALL
		SELECT KRT_MappaTartalmak.Obj_Id
			FROM KRT_MappaTartalmak
				INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
				INNER JOIN EREC_KuldKuldemenyek ON EREC_KuldKuldemenyek.Id = KRT_MappaTartalmak.Obj_Id
			WHERE KRT_MappaTartalmak.Mappa_Id in (select Id from @tempTable) --= @Id
				AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
				AND (EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo != KRT_Mappak.Csoport_Id_Tulaj
					OR EREC_KuldKuldemenyek.Csoport_Id_Felelos != KRT_Mappak.Csoport_Id_Tulaj
					OR EREC_KuldKuldemenyek.Allapot = '03')

		RAISERROR('[64005]',16,1)
	END
END

---- Tartalom ellenĹ‘rzĂ©se virtuĂˇlis mappĂˇra update esetĂ©n
--IF @UpdatedColumns.exist('/root/Tipus')=1 AND @Tipus = '01'
--BEGIN
--	IF EXISTS (SELECT 1 FROM dbo.fn_GetDosszieStatus(@Id) AS status WHERE Allapot = 1)
--		RAISERROR('[64007]',16,1)
--END

-- Fizikai mappĂˇban csak fizikai mappa lehet
IF @UpdatedColumns.exist('/root/Tipus')=1 AND @Tipus = '02'
BEGIN
	declare @tempParents table(id uniqueidentifier, tipus nvarchar(64))
	;with sz as
	(
		select Id, SzuloMappa_Id, Tipus
			from KRT_Mappak where Id = @currentSzuloMappa_Id --(select SzuloMappa_Id from KRT_Mappak where Id = @Id)
		UNION ALL
		select m.Id, m.SzuloMappa_Id, m.Tipus
			from KRT_Mappak m
			inner join sz on m.Id = sz.SzuloMappa_Id
			where sz.Tipus = '02'
			and getdate() between m.ErvKezd and m.ErvVege
	)
	insert into @tempParents(id, tipus)
		select Id, Tipus from sz

	if exists(select 1 from @tempParents t
		where t.Tipus='01') -- Fizikai dossziĂ©ban nem hozhatĂł lĂ©tre virtuĂˇlis mappa!
		RAISERROR('[64040]', 16, 1)
	
END

UPDATE KRT_Mappak
SET
     SzuloMappa_Id = @Act_SzuloMappa_Id,
     BarCode = @Act_BarCode,
     Tipus = @Act_Tipus,
     Nev = @Act_Nev,
     Leiras = @Act_Leiras,
     Csoport_Id_Tulaj = @Act_Csoport_Id_Tulaj,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'KRT_Mappak',@Id
					,'KRT_MappakHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH




GO
