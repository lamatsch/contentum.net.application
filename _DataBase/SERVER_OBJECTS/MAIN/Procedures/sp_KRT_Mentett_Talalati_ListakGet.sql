IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Mentett_Talalati_ListakGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Mentett_Talalati_ListakGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Mentett_Talalati_ListakGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Mentett_Talalati_ListakGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Mentett_Talalati_ListakGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Mentett_Talalati_Listak.Id,
	   KRT_Mentett_Talalati_Listak.ListName,
	   KRT_Mentett_Talalati_Listak.SaveDate,
	   KRT_Mentett_Talalati_Listak.Searched_Table,
	   KRT_Mentett_Talalati_Listak.Requestor,
	   KRT_Mentett_Talalati_Listak.HeaderXML,
	   KRT_Mentett_Talalati_Listak.BodyXML,
	   KRT_Mentett_Talalati_Listak.WorkStation,
	   KRT_Mentett_Talalati_Listak.Ver,
	   KRT_Mentett_Talalati_Listak.Note,
	   KRT_Mentett_Talalati_Listak.Stat_id,
	   KRT_Mentett_Talalati_Listak.ErvKezd,
	   KRT_Mentett_Talalati_Listak.ErvVege,
	   KRT_Mentett_Talalati_Listak.Letrehozo_id,
	   KRT_Mentett_Talalati_Listak.LetrehozasIdo,
	   KRT_Mentett_Talalati_Listak.Modosito_id,
	   KRT_Mentett_Talalati_Listak.ModositasIdo,
	   KRT_Mentett_Talalati_Listak.Zarolo_id,
	   KRT_Mentett_Talalati_Listak.ZarolasIdo,
	   KRT_Mentett_Talalati_Listak.Tranz_id,
	   KRT_Mentett_Talalati_Listak.UIAccessLog_id
	   from 
		 KRT_Mentett_Talalati_Listak as KRT_Mentett_Talalati_Listak 
	   where
		 KRT_Mentett_Talalati_Listak.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
