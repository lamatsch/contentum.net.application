IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Mentett_Talalati_ListakHistoryGetRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Mentett_Talalati_ListakHistoryGetRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Mentett_Talalati_ListakHistoryGetRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Mentett_Talalati_ListakHistoryGetRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Mentett_Talalati_ListakHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from KRT_Mentett_Talalati_ListakHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and KRT_Mentett_Talalati_ListakHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ListName' as ColumnName,               
               cast(Old.ListName as nvarchar(99)) as OldValue,
               cast(New.ListName as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ListName != New.ListName 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SaveDate' as ColumnName,               
               cast(Old.SaveDate as nvarchar(99)) as OldValue,
               cast(New.SaveDate as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SaveDate != New.SaveDate 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Searched_Table' as ColumnName,               
               cast(Old.Searched_Table as nvarchar(99)) as OldValue,
               cast(New.Searched_Table as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Searched_Table != New.Searched_Table 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Requestor' as ColumnName,               
               cast(Old.Requestor as nvarchar(99)) as OldValue,
               cast(New.Requestor as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Requestor != New.Requestor 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HeaderXML' as ColumnName,               
               cast(Old.HeaderXML as nvarchar(99)) as OldValue,
               cast(New.HeaderXML as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.HeaderXML != New.HeaderXML 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BodyXML' as ColumnName,               
               cast(Old.BodyXML as nvarchar(99)) as OldValue,
               cast(New.BodyXML as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.BodyXML != New.BodyXML 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'WorkStation' as ColumnName,               
               cast(Old.WorkStation as nvarchar(99)) as OldValue,
               cast(New.WorkStation as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Mentett_Talalati_ListakHistory Old
         inner join KRT_Mentett_Talalati_ListakHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.WorkStation != New.WorkStation 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end


GO
