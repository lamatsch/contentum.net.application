IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MenukGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_MenukGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MenukGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_MenukGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_MenukGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Menuk.Id,
	   KRT_Menuk.Org,
	   KRT_Menuk.Menu_Id_Szulo,
	   KRT_Menuk.Kod,
	   KRT_Menuk.Nev,
	   KRT_Menuk.Funkcio_Id,
	   KRT_Menuk.Modul_Id,
	   KRT_Menuk.Parameter,
	   KRT_Menuk.Ver,
	   KRT_Menuk.Note,
	   KRT_Menuk.Stat_id,
	   KRT_Menuk.ImageURL,
	   KRT_Menuk.Sorrend,
	   KRT_Menuk.ErvKezd,
	   KRT_Menuk.ErvVege,
	   KRT_Menuk.Letrehozo_id,
	   KRT_Menuk.LetrehozasIdo,
	   KRT_Menuk.Modosito_id,
	   KRT_Menuk.ModositasIdo,
	   KRT_Menuk.Zarolo_id,
	   KRT_Menuk.ZarolasIdo,
	   KRT_Menuk.Tranz_id,
	   KRT_Menuk.UIAccessLog_id
	   from 
		 KRT_Menuk as KRT_Menuk 
	   where
		 KRT_Menuk.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
