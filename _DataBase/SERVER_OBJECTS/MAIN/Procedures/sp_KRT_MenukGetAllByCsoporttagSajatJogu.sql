IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MenukGetAllByCsoporttagSajatJogu]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_MenukGetAllByCsoporttagSajatJogu]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_MenukGetAllByCsoporttagSajatJogu]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_MenukGetAllByCsoporttagSajatJogu] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_MenukGetAllByCsoporttagSajatJogu]
  @CsoportTag_Id uniqueidentifier ,
  @Alkalmazas_Kod NVARCHAR(100) , -- 'eAdmin', 'eRecord', ...
  @ExecutorUserId uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(4000)

if (@CsoportTag_Id is null)
begin
    RAISERROR('[53505]',16,1) -- A CsoportTag_Id paraméter értéke nem lehet null!
end

-- csoporttag rekord lekérdezése:
declare @Id uniqueidentifier
declare @Csoport_Id_Jogalany uniqueidentifier
declare @Csoport_Id uniqueidentifier
declare @ErvKezd datetime
declare @ErvVege datetime

select @Id = Id, @Csoport_Id = Csoport_Id, @Csoport_Id_Jogalany = Csoport_Id_Jogalany, @ErvKezd = ErvKezd, @ErvVege = ErvVege
from KRT_CsoportTagok where Id = @CsoportTag_Id

-- a rekord nem található
if (@Id is null)
begin
    RAISERROR('[50101]',16,1)
end

if not(getdate() between @ErvKezd and @ErvVege)
begin
    RAISERROR('[53501]',16,1) -- Nem létezo csoporttagság! (itt: nem érvényes)
end
-- felhasználó ellenorzése:
declare @Felhasznalo_Id uniqueidentifier
select @Felhasznalo_Id = Id
    from KRT_Felhasznalok
    where Id = @Csoport_Id_Jogalany
    and getdate() between ErvKezd and ErvVege

if (@Felhasznalo_Id is null)
begin
    RAISERROR('[53300]',16,1) -- A felhasználó nem létezik!
end

  SET @sqlcmd = 
  '
	select m.Id as KRT_Menuk_Id,m.Menu_Id_Szulo as KRT_Menuk_Menu_Id_Szulo
	 , m.kod as KRT_Menuk_Kod,m.nev as KRT_Menuk_Nev,m.parameter as KRT_Menuk_Parameter,m.imageurl as KRT_Menuk_ImageURL
	 ,f.Kod as KRT_Funkciok_Kod,mdl.kod as KRT_Modulok_Kod 
from krt_menuk m
	left outer join krt_funkciok f
	on (m.Funkcio_Id = f.id)
	left outer join krt_modulok mdl on m.Modul_Id = mdl.id	
where m.Kod = '''+@Alkalmazas_Kod+'''
	and getdate() between m.ErvKezd and m.ErvVege
	and (getdate() between f.ErvKezd and f.ErvVege or m.Funkcio_Id is null)
	and (getdate() between mdl.ErvKezd and mdl.ErvVege or mdl.Id is null)
	and (m.Funkcio_id is null or f.kod in 
		(select f_.kod 
		 from krt_funkciok f_
			join krt_szerepkor_funkcio szf_ on (szf_.Funkcio_Id = f_.Id)
			join krt_szerepkorok sz_ on (szf_.Szerepkor_Id = sz_.id)
			join krt_felhasznalo_szerepkor fsz_ on (fsz_.Szerepkor_Id = sz_.Id)
		 where fsz_.Felhasznalo_Id = ''' + convert(nvarchar(36), @Csoport_Id_Jogalany) + '''
			and (fsz_.CsoportTag_Id = ''' + convert(nvarchar(36), @CsoportTag_Id) + ''' or fsz_.CsoportTag_Id is null)
			and IsNull(fsz_.CsoportTag_Id, ''' + convert(nvarchar(36), @CsoportTag_Id) + ''') =  ''' + convert(nvarchar(36), @CsoportTag_Id) + '''
			and IsNull(fsz_.Csoport_Id, ''' + convert(nvarchar(36), @Csoport_Id) + ''') =  ''' + convert(nvarchar(36), @Csoport_Id) + '''
			and fsz_.Helyettesites_Id is null
			and (getdate() between f_.ErvKezd and f_.ErvVege)
			and (getdate() between szf_.ErvKezd and szf_.ErvVege)
			and (getdate() between sz_.ErvKezd and sz_.ErvVege)
			and (getdate() between fsz_.ErvKezd and fsz_.ErvVege)
		))
	--and m.Org=''' + CAST(@Org as Nvarchar(40)) + '''
	order by m.sorrend
  '

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
