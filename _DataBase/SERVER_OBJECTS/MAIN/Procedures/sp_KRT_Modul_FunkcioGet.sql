IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Modul_FunkcioGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Modul_FunkcioGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Modul_FunkcioGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Modul_FunkcioGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Modul_FunkcioGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Modul_Funkcio.Id,
	   KRT_Modul_Funkcio.Funkcio_Id,
	   KRT_Modul_Funkcio.Modul_Id,
	   KRT_Modul_Funkcio.Ver,
	   KRT_Modul_Funkcio.Note,
	   KRT_Modul_Funkcio.Stat_id,
	   KRT_Modul_Funkcio.ErvKezd,
	   KRT_Modul_Funkcio.ErvVege,
	   KRT_Modul_Funkcio.Letrehozo_id,
	   KRT_Modul_Funkcio.LetrehozasIdo,
	   KRT_Modul_Funkcio.Modosito_id,
	   KRT_Modul_Funkcio.ModositasIdo,
	   KRT_Modul_Funkcio.Zarolo_id,
	   KRT_Modul_Funkcio.ZarolasIdo,
	   KRT_Modul_Funkcio.Tranz_id,
	   KRT_Modul_Funkcio.UIAccessLog_id
	   from 
		 KRT_Modul_Funkcio as KRT_Modul_Funkcio 
	   where
		 KRT_Modul_Funkcio.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
