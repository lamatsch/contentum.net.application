IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_NezetKontrollokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_NezetKontrollokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_NezetKontrollokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_NezetKontrollokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_NezetKontrollokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier
         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_NezetKontrollok.Id,
	   KRT_NezetKontrollok.Nezet_Id,
	   KRT_NezetKontrollok.Muvelet_Id,
	   KRT_NezetKontrollok.Csoport_Id,
	   KRT_NezetKontrollok.Feltetel,
	   KRT_NezetKontrollok.ObjTipus_id_UIElem,
	   KRT_NezetKontrollok.Tiltas,
	   KRT_NezetKontrollok.ObjTipus_id_Adatelem,
	   KRT_NezetKontrollok.ObjStat_Id_Adatelem,
	   KRT_NezetKontrollok.Ver,
	   KRT_NezetKontrollok.Note,
	   KRT_NezetKontrollok.Stat_id,
	   KRT_NezetKontrollok.ErvKezd,
	   KRT_NezetKontrollok.ErvVege,
	   KRT_NezetKontrollok.Letrehozo_id,
	   KRT_NezetKontrollok.LetrehozasIdo,
	   KRT_NezetKontrollok.Modosito_id,
	   KRT_NezetKontrollok.ModositasIdo,
	   KRT_NezetKontrollok.Zarolo_id,
	   KRT_NezetKontrollok.ZarolasIdo,
	   KRT_NezetKontrollok.Tranz_id,
	   KRT_NezetKontrollok.UIAccessLog_id
	   from 
		 KRT_NezetKontrollok as KRT_NezetKontrollok 
	   where
		 KRT_NezetKontrollok.Id = @Id'

	exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
