IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_NezetKontrollokInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_NezetKontrollokInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_NezetKontrollokInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_NezetKontrollokInsert] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_NezetKontrollokInsert]    
	            @Nezet_Id     uniqueidentifier,
	            @Muvelet_Id     uniqueidentifier,
	            @Csoport_Id     uniqueidentifier,
                @Feltetel     Nvarchar(4000)  = null,
                @ObjTipus_id_UIElem     uniqueidentifier  = null,
                @Tiltas     Nvarchar(100)  = null,
                @ObjTipus_id_Adatelem     uniqueidentifier  = null,
                @ObjStat_Id_Adatelem     uniqueidentifier  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @myid uniqueidentifier
Set @myid   = NEWID()

DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = 'Id'
SET @insertValues = '@myid' 
       
         if @Nezet_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Nezet_Id'
            SET @insertValues = @insertValues + ',@Nezet_Id'
         end 
       
         if @Muvelet_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Muvelet_Id'
            SET @insertValues = @insertValues + ',@Muvelet_Id'
         end 
       
         if @Csoport_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id'
            SET @insertValues = @insertValues + ',@Csoport_Id'
         end 
       
         if @Feltetel is not null
         begin
            SET @insertColumns = @insertColumns + ',Feltetel'
            SET @insertValues = @insertValues + ',@Feltetel'
         end 
       
         if @ObjTipus_id_UIElem is not null
         begin
            SET @insertColumns = @insertColumns + ',ObjTipus_id_UIElem'
            SET @insertValues = @insertValues + ',@ObjTipus_id_UIElem'
         end 
       
         if @Tiltas is not null
         begin
            SET @insertColumns = @insertColumns + ',Tiltas'
            SET @insertValues = @insertValues + ',@Tiltas'
         end 
       
         if @ObjTipus_id_Adatelem is not null
         begin
            SET @insertColumns = @insertColumns + ',ObjTipus_id_Adatelem'
            SET @insertValues = @insertValues + ',@ObjTipus_id_Adatelem'
         end 
       
         if @ObjStat_Id_Adatelem is not null
         begin
            SET @insertColumns = @insertColumns + ',ObjStat_Id_Adatelem'
            SET @insertValues = @insertValues + ',@ObjStat_Id_Adatelem'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end      

DECLARE @InsertCommand NVARCHAR(4000)
SET @InsertCommand = 'insert into KRT_NezetKontrollok ('+@insertColumns+') values ('+@insertValues+')'
exec sp_executesql @InsertCommand, 
                             N'@myid uniqueidentifier,@Nezet_Id uniqueidentifier,@Muvelet_Id uniqueidentifier,@Csoport_Id uniqueidentifier,@Feltetel Nvarchar(4000),@ObjTipus_id_UIElem uniqueidentifier,@Tiltas Nvarchar(100),@ObjTipus_id_Adatelem uniqueidentifier,@ObjStat_Id_Adatelem uniqueidentifier,@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier'
     ,@myid = @myid,@Nezet_Id = @Nezet_Id,@Muvelet_Id = @Muvelet_Id,@Csoport_Id = @Csoport_Id,@Feltetel = @Feltetel,@ObjTipus_id_UIElem = @ObjTipus_id_UIElem,@Tiltas = @Tiltas,@ObjTipus_id_Adatelem = @ObjTipus_id_Adatelem,@ObjStat_Id_Adatelem = @ObjStat_Id_Adatelem,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id

If @@error = 0
BEGIN
   SET @ResultUid = @myid
      /* History Log */
   exec sp_LogRecordToHistory 'KRT_NezetKontrollok',@myid
					,'KRT_NezetKontrollokHistory',0,@Letrehozo_id,@LetrehozasIdo   
END
ELSE
BEGIN
   RAISERROR('[50301]',16,1)
END

COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
