IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_NezetekGetAllByMuvelet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_NezetekGetAllByMuvelet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_NezetekGetAllByMuvelet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_NezetekGetAllByMuvelet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_NezetekGetAllByMuvelet]
  @MuveletKod nvarchar(20),
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Nezetek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Nezetek.Id,
	   KRT_Nezetek.Org,
	   KRT_Nezetek.Form_Id,
	   KRT_Nezetek.Nev,
	   KRT_Nezetek.Leiras,
	   KRT_Nezetek.Csoport_Id,
	   KRT_Nezetek.Muvelet_Id,
	   KRT_Nezetek.DisableControls,
	   KRT_Nezetek.InvisibleControls,
	   KRT_Nezetek.ReadOnlyControls,
	   KRT_Nezetek.Prioritas,
	   KRT_Nezetek.Ver,
	   KRT_Nezetek.Note,
	   KRT_Nezetek.Stat_id,
	   KRT_Nezetek.ErvKezd,
	   KRT_Nezetek.ErvVege,
	   KRT_Nezetek.Letrehozo_id,
	   KRT_Nezetek.LetrehozasIdo,
	   KRT_Nezetek.Modosito_id,
	   KRT_Nezetek.ModositasIdo,
	   KRT_Nezetek.Zarolo_id,
	   KRT_Nezetek.ZarolasIdo,
	   KRT_Nezetek.Tranz_id,
	   KRT_Nezetek.UIAccessLog_id  
   from 
     KRT_Nezetek as KRT_Nezetek 

left join KRT_Muveletek on KRT_Nezetek.Muvelet_Id = KRT_Muveletek.Id
Where KRT_Nezetek.Org=''' + CAST(@Org as Nvarchar(40)) + '''
AND (KRT_Muveletek.Kod = ''' + @MuveletKod + ''' OR KRT_Nezetek.Muvelet_Id is null)
'
     
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
