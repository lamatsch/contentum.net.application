IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_OBJEKTUM_ACLEK_BuildFrom_OBJEKTUMJOG_OROKLESEK]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_OBJEKTUM_ACLEK_BuildFrom_OBJEKTUMJOG_OROKLESEK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_OBJEKTUM_ACLEK_BuildFrom_OBJEKTUMJOG_OROKLESEK]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_OBJEKTUM_ACLEK_BuildFrom_OBJEKTUMJOG_OROKLESEK] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_OBJEKTUM_ACLEK_BuildFrom_OBJEKTUMJOG_OROKLESEK]
as
BEGIN
	SET NOCOUNT ON
	
	-- indexek eldobálása
	DECLARE @sqlcmd NVARCHAR(4000);
	DECLARE @indexName NVARCHAR(100);
	DECLARE cur CURSOR FOR 
		SELECT name FROM sys.indexes WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[krt_objektum_aclek]')
	SET @sqlcmd = '';
	OPEN cur;
	FETCH NEXT FROM cur INTO @indexName;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sqlcmd = @sqlcmd + N' DROP INDEX ' + @indexName + ' ON krt_objektum_aclek;';
		FETCH NEXT FROM cur INTO @indexName;
	END

	exec (@sqlcmd);
	CLOSE cur;
	DEALLOCATE cur;


	DELETE FROM KRT_OBJEKTUM_ACLEK;

	WITH objAclek AS
	(
		SELECT Obj_Id,Obj_Id_orokos,JogszintOrokolheto AS OrokolhetoJogszint FROM KRT_OBJEKTUMJOG_OROKLESEK
		UNION ALL
		SELECT objAclek.Obj_Id,
			KRT_OBJEKTUMJOG_OROKLESEK.Obj_Id_Orokos,
			CASE 
				WHEN KRT_OBJEKTUMJOG_OROKLESEK.JogszintOrokolheto = 'O' THEN CAST('O' AS CHAR(1))
				ELSE objAclek.OrokolhetoJogszint
			END AS OrokolhetoJogszint
		 FROM KRT_OBJEKTUMJOG_OROKLESEK
			JOIN objAclek ON objAclek.Obj_Id_Orokos = KRT_OBJEKTUMJOG_OROKLESEK.Obj_Id
	) INSERT INTO KRT_OBJEKTUM_ACLEK(Obj_Id, Obj_Id_Orokos, OrokolhetoJogszint)
		SELECT DISTINCT Obj_Id,Obj_Id_orokos,OrokolhetoJogszint FROM objAclek WHERE Obj_Id IS NOT NULL AND Obj_Id_orokos IS NOT null

	INSERT INTO KRT_OBJEKTUM_ACLEK(Obj_Id, Obj_Id_Orokos, OrokolhetoJogszint)
		SELECT Id,Id,'I' FROM KRT_ACL


CREATE UNIQUE CLUSTERED INDEX [IX_Obj_Id_Obj_Id_Orokos] ON [dbo].[KRT_OBJEKTUM_ACLEK] 
(
	[Obj_Id] ASC,
	[Obj_Id_Orokos] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]

--	create index IX_obj_Id on KRT_OBJEKTUM_ACLEK (
--	Obj_Id ASC
--	)
--	create index IX_Obj_Id_Orokor on KRT_OBJEKTUM_ACLEK (
--	Obj_Id_Orokos ASC
--	)

	ALTER INDEX ALL ON KRT_OBJEKTUM_ACLEK Rebuild;
END


GO
