IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_ObjTip_TargySzoGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_ObjTip_TargySzoGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_ObjTip_TargySzoGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_ObjTip_TargySzoGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_ObjTip_TargySzoGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_ObjTip_TargySzo.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_ObjTip_TargySzo.Id,
	   KRT_ObjTip_TargySzo.TargySzo_Id,
	   KRT_ObjTip_TargySzo.ObjTip_Id_AdatElem,
	   KRT_ObjTip_TargySzo.Tipus,
	   KRT_ObjTip_TargySzo.Sorszam,
	   KRT_ObjTip_TargySzo.Opcionalis,
	   KRT_ObjTip_TargySzo.Nev,
	   KRT_ObjTip_TargySzo.Ver,
	   KRT_ObjTip_TargySzo.Note,
	   KRT_ObjTip_TargySzo.Stat_id,
	   KRT_ObjTip_TargySzo.ErvKezd,
	   KRT_ObjTip_TargySzo.ErvVege,
	   KRT_ObjTip_TargySzo.Letrehozo_id,
	   KRT_ObjTip_TargySzo.LetrehozasIdo,
	   KRT_ObjTip_TargySzo.Modosito_id,
	   KRT_ObjTip_TargySzo.ModositasIdo,
	   KRT_ObjTip_TargySzo.Zarolo_id,
	   KRT_ObjTip_TargySzo.ZarolasIdo,
	   KRT_ObjTip_TargySzo.Tranz_id,
	   KRT_ObjTip_TargySzo.UIAccessLog_id  
   from 
     KRT_ObjTip_TargySzo as KRT_ObjTip_TargySzo      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
