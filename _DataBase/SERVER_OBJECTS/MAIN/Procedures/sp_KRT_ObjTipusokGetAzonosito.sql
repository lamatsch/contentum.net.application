IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_ObjTipusokGetAzonosito]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_ObjTipusokGetAzonosito]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_ObjTipusokGetAzonosito]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_ObjTipusokGetAzonosito] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_ObjTipusokGetAzonosito]
  @ExecutorUserId				uniqueidentifier,
  @ObjektumId    uniqueidentifier,
  @ObjektumTipus nvarchar(100)

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)
   
   DECLARE @fn NVARCHAR(400)
   
   SET @fn = 'dbo.fn_Get'+ @ObjektumTipus +'Azonosito'
   
   IF  EXISTS (SELECT * FROM sys.Objects WHERE object_id = OBJECT_ID(@fn) AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
   BEGIN
   		SET @sqlcmd = 'select ' + @fn + '(''' + cast(@ObjektumId as nvarchar(40))+ ''') as Azonosito'
   END
   ELSE
   BEGIN
   		SET @sqlcmd = 'Select null as Azonosito'
   END

  exec (@sqlcmd);
  
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
