IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Orgok_CloneSystemControllersForNewOrg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Orgok_CloneSystemControllersForNewOrg]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Orgok_CloneSystemControllersForNewOrg]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Orgok_CloneSystemControllersForNewOrg] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Orgok_CloneSystemControllersForNewOrg]
  @ExecutorUserId				uniqueidentifier,
  @Org uniqueidentifier,
  @Tranz_id	uniqueidentifier

as

begin

BEGIN TRY

   set nocount on;

	if @Org is null
		-- Org rendszervezérlok: Nincs megadva az Org azonosítója, a betöltés nem hajtható végre.
		RAISERROR('[65001]',16,1)
----------------------------------------------------------------------------------
-- Ellenorzes - KRT_KodTarak, KRT_Parameterek nem tartalmazza az uj Org-ot
----------------------------------------------------------------------------------
	if (exists(select 1 from KRT_KodTarak where Org=@Org) or exists(select 1 from KRT_Parameterek where Org=@Org))
		-- Org rendszervezérlok: Az Org-hoz már léteznek rendszerbejegyzések, a betöltés nem hajtható végre.
		RAISERROR('[65002]',16,1)

-- közös (szuper) rendszerparaméterek spec. Orgja
declare @SuperOrg_ID uniqueidentifier
set @SuperOrg_ID = '00000000-0000-0000-0000-000000000000'

-- Mivel a MAPPA_TIPUS es a FELADAT_PRIORITAS kulonbozo Id-kkel kerult be
-- a rendszerekbe, minden  kodcsoportot ellenorzunk Kod alapjan
declare @KODCSOPORT_ID uniqueidentifier

----------------------------------------------------------------------------------
-- KodTarak
----------------------------------------------------------------------------------

declare @STAT_ID_ALLAPOT_KELETKEZIK uniqueidentifier
set @STAT_ID_ALLAPOT_KELETKEZIK = newid()-- specialis allapot, a stat_id-t toltjuk vele

/* -------------------------------------------------------------------------------
KodCsoport: Allapot
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'Allapot' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		-- Org rendszervezérlok: Hiányzó kódcsoport, a betöltés nem hajtható végre.
		RAISERROR('[65003]',16,1)


-- Figyelem, ennél az egynél az Id-t is beszúrjuk, míg a többinél generálódik
-->
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Modosithato
			,Id
			,Kod
			,Nev
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,'0'
			,'Keletkezik (születés, tervezés, fogantatás...) alatt'
			,'10'
			);
-->

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Modosithato
			,Kod
			,Nev
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'1'
			,'Muködo (élo, érvényes, ...)'
			,'20'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Modosithato
			,Kod
			,Nev
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'9.0'
			,'Törlésre kijelölt'
			,'30'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Modosithato
			,Kod
			,Nev
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'9'
			,'Érvénytelen (megszunt, halott, nem muködo...)'
			,'40'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Modosithato
			,Kod
			,Nev
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'KodcsopTip'
			,'Kódcsoport típus'
			,'1'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Modosithato
			,Kod
			,Nev
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Status'
			,'Állapot'
			,'10'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Modosithato
			,Kod
			,Nev
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Table'
			,'Tábla'
			,'20'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Modosithato
			,Kod
			,Nev
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Other'
			,'Egyéb'
			,'30'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: KodcsoportTipus
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KodcsoportTipus' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		-- Org rendszervezérlok: Hiányzó kódcsoport, a betöltés nem hajtható végre.
		RAISERROR('[65003]',16,1)

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Modosithato
			,Kod
			,Nev
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'KodcsopTip'
			,'Kódcsoport típus'
			,'1'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Modosithato
			,Kod
			,Nev
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Status'
			,'Állapot'
			,'10'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Modosithato
			,Kod
			,Nev
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Table'
			,'Tábla'
			,'20'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Modosithato
			,Kod
			,Nev
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Other'
			,'Egyéb'
			,'30'
			);

/* -------------------------------------------------------------------------------
KodCsoport: PARTNER_TIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'PARTNER_TIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'10'
			,'Szervezet'
			,'SZ'
			,'0'
			,'10'
			); 
 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			,Note
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'14'
			,'Projekt'
			,'SZ'
			,'0'
			,'14'
			,'FPH ADAJK Projekt'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			,Note
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'15'
			,'Bizottság'
			,'SZ'
			,'0'
			,'15'
			,'FPH ADAJK Commettee '
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			,Note
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'16'
			,'Hivatal vezetés'
			,'SZ'
			,'0'
			,'16'
			,'FPH ADAJK Hivatal vezetés'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'20'
			,'Személy'
			,'T'
			,'0'
			,'20'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'30'
			,'Alkalmazás'
			,'Alkalmazások'
			,'0'
			,'30'
			); 
	 
/* -------------------------------------------------------------------------------
KodCsoport: ELSODLEGES_ADATHORDOZO
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'ELSODLEGES_ADATHORDOZO' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Papír alapú'
			,'Papír alapú'
			,'1'
			,'01'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Elektronikus'
			,''
			,'1'
			,'02'
			); 

 
/* -------------------------------------------------------------------------------
KodCsoport: TARGYSZO_FORRAS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'TARGYSZO_FORRAS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Automatikus'
			,'Metadefiníció alapján'
			,'1'
			,'01'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Listás'
			,'Adatbázisban tárolt készletbol választott'
			,'1'
			,'02'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Kézi'
			,'Egyénileg, adott objektumhoz megadott, tárgyszó azonosító nélküli'
			,'1'
			,'03'
			); 

	 
/* -------------------------------------------------------------------------------
KodCsoport: ADATHORDOZO_TIPUSA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'ADATHORDOZO_TIPUSA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Papír irat'
			,'Papír irat'
			,'1'
			,'01'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Fax'
			,'Fax'
			,'1'
			,'02'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'E-mail'
			,'E-mail'
			,'1'
			,'03'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Floppy lemez'
			,'Floppy lemez'
			,'1'
			,'04'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'CD'
			,'CD'
			,'1'
			,'05'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'DVD'
			,'DVD'
			,'1'
			,'06'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'Videó'
			,'Videó'
			,'1'
			,'07'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'08'
			,'Hang kazetta'
			,'Hang kazetta'
			,'1'
			,'08'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'09'
			,'Egyéb'
			,'Egyéb'
			,'1'
			,'09'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'10'
			,'Csomag'
			,'1'
			,'10'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'11'
			,'Pendrive'
			,'1'
			,'11'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'12'
			,'Boríték'
			,'0'
			,'12'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'13'
			,'Egyéb papír'
			,'Egyéb papír'
			,'1'
			,'13'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'14'
			,'Tértivevény'
			,'1'
			,'14'
			);

	 
/* -------------------------------------------------------------------------------
KodCsoport: ALAIRO_SZEREP
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'ALAIRO_SZEREP' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Aláíró'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Kiadmányozó'
			,'0'
			,'2'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: ALKALMAZAS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'ALKALMAZAS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'eAdmin'
			,'eAdmin'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'eRecord'
			,'eRecord'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'eMigration'
			,'eMigration'
			,'0'
			,'3'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: CEGTIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'CEGTIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Vállalkozó'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Bt'
			,'1'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Kft'
			,'1'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Rt'
			,'1'
			,'4'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Kht'
			,'1'
			,'5'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'Kkt'
			,'1'
			,'6'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'Zrt'
			,'1'
			,'41'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'08'
			,'Nyrt'
			,'1'
			,'42'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: CIM_FAJTA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'CIM_FAJTA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Székhely'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Telephely'
			,'1'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Állandó lakcím'
			,'1'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Ideiglenes lakcím'
			,'1'
			,'4'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Levelezési cím'
			,'1'
			,'5'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'Értesítési cím'
			,'1'
			,'6'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: CIM_TIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'CIM_TIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Postai cím'
			,'PC'
			,'0'
			,'01'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Postafiók'
			,'PF'
			,'0'
			,'02'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Email cím'
			,'EC'
			,'0'
			,'03'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Telefonszám'
			,'1'
			,'06'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Fax szám'
			,'Fax szám'
			,'1'
			,'04'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'WEB cím'
			,'1'
			,'05'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: CSOPORTTAGSAG_TIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'CSOPORTTAGSAG_TIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Alszervezet'
			,'0'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Dolgozó'
			,'0'
			,'03'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Vezeto'
			,'0'
			,'02'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'Beosztott vezeto'
			,'0'
			,'04'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: ELOSZTOIV_FAJTA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'ELOSZTOIV_FAJTA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Egyedi elosztóív'
			,'0'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Aláírólista'
			,'0'
			,null
			); 

/* -------------------------------------------------------------------------------
KodCsoport: CSOPORTTIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'CSOPORTTIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Szervezet'
			,'0'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Dolgozó'
			,'0'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Szerepkör'
			,'1'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'Projekt'
			,'1'
			,'4'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'Testület, Bizottság'
			,'0'
			,'5'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Note
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'6'
			,'Hivatal vezetés'
			,'0'
			,'6'
			,'FPH ADAJK activity'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'9'
			,'Org'
			,'1'
			,'O'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'M'
			,'Mindenki'
			,'0'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'R'
			,'Rendszergazdák'
			,'0'
			,'9R'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: DOKUMENTUMTIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'DOKUMENTUMTIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Számla'
			,'1'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Szerzodés'
			,'1'
			,'02'
			);  

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'11'
			,'Beszámoló'
			,'1'
			,'11'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'12'
			,'Elemzés; ellenorzés;eloterjesztés;'
			,'1'
			,'12'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'13'
			,'Feljegyzés'
			,'1'
			,'13'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'14'
			,'Jogszabály'
			,'1'
			,'14'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'15'
			,'Pályázat'
			,'1'
			,'15'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'16'
			,'Prezentáció'
			,'1'
			,'16'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'17'
			,'Tanulmány'
			,'1'
			,'17'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'18'
			,'Digitalizált dokumentum'
			,'1'
			,'18'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'19'
			,'Bontási jegyzokönyv'
			,'1'
			,'19'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'20'
			,'Átvételi elismervény'
			,'1'
			,'20'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'21'
			,'Egyéb'
			,'1'
			,'21'
			);

/* -------------------------------------------------------------------------------
KodCsoport: DOKUMENTUM_FORRAS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'DOKUMENTUM_FORRAS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'Beküldo'
			,'Beküldo'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'Belso;'
			,'Belso;'
			,'1'
			,'2'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: DOKUMENTUM_FORMATUM
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'DOKUMENTUM_FORMATUM' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'doc'
			,'Word (doc)'
			,'1'
			,'11'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'docx'
			,'Word (docx)'
			,'1'
			,'12'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'dot'
			,'Word sablon (dot)'
			,'1'
			,'121'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'dotx'
			,'Word sablon (dotx)'
			,'1'
			,'122'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'dotm'
			,'Word sablon (makróbarát)'
			,'1'
			,'123'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'docm'
			,'Word (makróbarát)'
			,'1'
			,'13'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'xls'
			,'Excel (xls)'
			,'1'
			,'21'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'xlsx'
			,'Excel (xlsx)'
			,'1'
			,'22'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'pdf'
			,'PDF (Adobe)'
			,'1'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'txt'
			,'Szöveg (txt)'
			,'1'
			,'4'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'msg'
			,'Üzenet'
			,'1'
			,'5'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'gif'
			,'Kép (gif)'
			,'1'
			,'61'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'jpg'
			,'Kép (jpg)'
			,'1'
			,'62'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'png'
			,'Kép (png)'
			,'1'
			,'63'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'bmp'
			,'Kép (bmp)'
			,'1'
			,'64'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'tiff'
			,'Kép (tiff)'
			,'1'
			,'65'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'zip'
			,'Tömörített (zip)'
			,'1'
			,'7'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'Egyeb'
			,'Egyéb'
			,'1'
			,'9'
			);

/* -------------------------------------------------------------------------------
KodCsoport: UGY_FAJTAJA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'UGY_FAJTAJA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Államigazgatási hatósági ügy'
			,'1'
			,'02'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Önkormányzati hatósági ügy'
			,'1'
			,'03'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Nem hatósági ügy'
			,'1'
			,'01'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: DONTEST_HOZTA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'DONTEST_HOZTA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Képviselo-testület'
			,'1'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Képviselo-testület bizottsága'
			,'1'
			,'02'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Részönkormányzat testülete'
			,'1'
			,'03'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'(Fo)polgármester'
			,'1'
			,'04'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'(Fo)jegyzo'
			,'1'
			,'05'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'6'
			,'(Fo)polgármesteri hivatal ügyintézoje'
			,'1'
			,'06'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: DONTES_FORMAJA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'DONTES_FORMAJA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Határozat'
			,'1'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Egyezség jóváhagyását tartalmazó határozat'
			,'1'
			,'02'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Hatósági bizonyítvány'
			,'1'
			,'03'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'Hatósági szerzodés'
			,'1'
			,'04'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'A Ket. 30. § alapján történo elutasítás'
			,'1'
			,'05'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'6'
			,'A Ket. 31. § alapján történo megszüntetése'
			,'1'
			,'06'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'7'
			,'Az elsofokú eljárásban hozott egyéb végzés'
			,'1'
			,'07'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'8'
			,'Végrehajtási eljárásban hozott végzés'
			,'1'
			,'08'
			); 

		
/* -------------------------------------------------------------------------------
KodCsoport: UGYINTEZES_IDOTARTAMA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'UGYINTEZES_IDOTARTAMA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Határidon belül'
			,'1'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Határidon túl'
			,'1'
			,'02'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: JOGORVOSLATI_ELJARAS_TIPUSA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'JOGORVOSLATI_ELJARAS_TIPUSA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Kérelem alapján indult'
			,'1'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Hivatalból indult'
			,'1'
			,'02'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: JOGORVOSLATI_DONTES_TIPUSA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'JOGORVOSLATI_DONTES_TIPUSA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Végzés'
			,'1'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Érdemi döntés'
			,'1'
			,'02'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: JOGORVOSLATI_DONTEST_HOZTA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'JOGORVOSLATI_DONTEST_HOZTA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Az elsofokú hatóság'
			,'1'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Az elsofokú hatóság újrafelvételi eljárásban'
			,'1'
			,'02'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Az elsofokú hatóság méltányossági eljárásban'
			,'1'
			,'03'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'A képviselotestület másodfokú hatáskörben'
			,'1'
			,'04'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'A közigazgatási hivatal'
			,'1'
			,'05'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'6'
			,'A dekoncentrált szerv'
			,'1'
			,'06'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'7'
			,'A bíróság'
			,'1'
			,'07'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'8'
			,'A felügyeleti szerv/felügyeleti intézkedés esetén'
			,'1'
			,'08'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: JOGORVOSLATI_DONTES_TARTALMA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'JOGORVOSLATI_DONTES_TARTALMA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Kicserélés, kiegészítés vagy kijavítás'
			,'1'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Módosítás'
			,'1'
			,'02'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Visszavonás'
			,'1'
			,'03'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'Új döntés'
			,'1'
			,'04'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'Helybenhagyás/kereset ill. kérelem, újrafelvételi vagy méltányossági kérelem elutasítása'
			,'1'
			,'05'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'6'
			,'Megváltoztatás'
			,'1'
			,'06'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'7'
			,'Megsemmisítés vagy hatályon kívül helyezés'
			,'1'
			,'07'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'8'
			,'Megsemmisítés vagy hatályon kívül helyezés és új eljárásra utasítás'
			,'1'
			,'08'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: OBJMETADEFINICIO_TIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'OBJMETADEFINICIO_TIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'A0'
			,'Nincs objektumtípus'
			,'=> KRT_DOKUMENTUMOK -ban tároljuk a meta-t (pl. számla)'
			,'0'
			,'1'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'B1'
			,'Objektum kiegészítés'
			,'Általános objektum kiegészítés (pl. partner)'
			,'0'
			,'2'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'B2'
			,'Típusos objektum kiegészítés'
			,'Általános típusos objektum kiegészítés (pl. adott típusú partner)'
			,'0'
			,'3'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'C2'
			,'Típusos kiegészítés ügyiratnyilvántartáshoz'
			,'Az ügyiratnyilvántartáshoz kapcsolódó típusos kiegészítés (van irat metadefiníció kötés)'
			,'0'
			,'4'
			); 

	 
/* -------------------------------------------------------------------------------
KodCsoport: ELINTEZESMOD
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'ELINTEZESMOD' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'9'
			,'Szerelés'
			,'1'
			,'09'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'90'
			,'Egyéb'
			,'1'
			,'90'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: ELJARASI_SZAKASZ
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'ELJARASI_SZAKASZ' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'00'
			,'Osztatlan'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Általános'
			,'1'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Speciális'
			,'1'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Közbenso'
			,'1'
			,'4'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: SZIGNALAS_TIPUSA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'SZIGNALAS_TIPUSA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Szervezetre, Ügyintézore szignálás, Iktatás'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Szervezetre szignálás, Iktatás, Ügyintézore szignálás'
			,'1'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Iktatás, Szervezetre, Ügyintézore szignálás'
			,'1'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'Nincs szervezetre szignálás'
			,'1'
			,'4'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'Automatikus szignálás ügyintézore'
			,'1'
			,'5'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'6'
			,'Szabadon szigálható'
			,'1'
			,'6'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: EMAILCSATOLMANY_TIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'EMAILCSATOLMANY_TIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Word'
			,'1'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Excel'
			,'1'
			,'02'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'JPG'
			,'1'
			,'03'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'ZIP'
			,'1'
			,'04'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'XML'
			,'1'
			,'05'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'PDF'
			,'1'
			,'06'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'99'
			,'Egyéb'
			,'1'
			,'99'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: EMAILBORITEK_FONTOSSAG
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'EMAILBORITEK_FONTOSSAG' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Átlagos'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Sürgos'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Nem sürgos'
			,'1'
			,'3'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: EMAILFORRASTIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'EMAILFORRASTIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Külso'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Belso'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Kimeno'
			,'1'
			,'3'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: EMAILBORITEK_ALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'EMAILBORITEK_ALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Tárolt'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Feldolgozott'
			,'0'
			,'2'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: EMAILCIMCIM_TIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'EMAILCIMCIM_TIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Felhasználó'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Szervezet'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Partner'
			,'1'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'Egyéb'
			,'1'
			,'4'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: KULDEMENY_BORITO_TIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KULDEMENY_BORITO_TIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Boríték'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Irat'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,' Eldobható boríték'
			,'0'
			,'3'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: FELADAT_TIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'FELADAT_TIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'F'
			,'Feladat'
			,'1'
			,'01'
			,'Kijelölt funkció végrehajtása'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'M'
			,'Megjegyzés'
			,'1'
			,'02'
			,'Feladat nélküli megjegyzés'
			); 

	 
/* -------------------------------------------------------------------------------
KodCsoport: FELADAT_ALTIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'FELADAT_ALTIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Intézkedésre'
			,'1'
			,'01'
			,''
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Véleményezésre'
			,'1'
			,'02'
			,''
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Tájékoztatásra'
			,'1'
			,'03'
			,''
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Tárgyalásra'
			,'1'
			,'04'
			,'(helyszín, ido megadása)'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'Látta'
			,'1'
			,'07'
			,''
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'08'
			,'Lássa'
			,'1'
			,'08'
			,''
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'09'
			,'Megjegyzés'
			,'1'
			,'09'
			,''
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'10'
			,'Téves szignálás miatt vissza'
			,'1'
			,'10'
			,''
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'13'
			,'Eloadói munkanapló megjegyzés'
			,'1'
			,'13'
			,'Listában nem látszik!'
			); 
			
	 
/* -------------------------------------------------------------------------------
KodCsoport: FELADAT_ALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'FELADAT_ALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Új'
			,'1'
			,'01'
			,'Újonnan felvett feladat'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Nyitott'
			,'1'
			,'02'
			,'Megtekintett feladat'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Folyamatban'
			,'1'
			,'03'
			,'Feldolgozás alatt lévo feladat'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Megoldott'
			,'1'
			,'04'
			,'Megoldott feladat'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'Kerüloúton megoldott'
			,'1'
			,'05'
			,'Kerüloúton megoldott feladat'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'Nem megoldható'
			,'1'
			,'06'
			,'Nem megoldható feladat'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'6'
			,'Sztornózott'
			,'1'
			,'07'
			,'Sztornózott feladat'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'13'
			,'Lezárt - megoldott'
			,'1'
			,'08'
			,'Lezárt megoldott feladat'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'14'
			,'Lezárt - kerüloúton megoldott'
			,'1'
			,'09'
			,'Lezárt kerüloúton megoldott feladat'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'15'
			,'Lezárt – nem megoldható'
			,'1'
			,'10'
			,'Lezárt nem megoldható feladat'
			); 

	 
/* -------------------------------------------------------------------------------
KodCsoport: FELADATBAN_ERINTETTSEG_TIPUSA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'FELADATBAN_ERINTETTSEG_TIPUSA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'00'
			,'Általános'
			,'1'
			,'01'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: FELADATTIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'FELADATTIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Feladat'
			,'1'
			,null
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: FELADAT_DEFINICIO_TIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'FELADAT_DEFINICIO_TIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Eseményfüggo'
			,'1'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Esemény- és kategóriafüggo'
			,'1'
			,'02'
			,'Obj_Metadefininicio_Id-vel definiált'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Esemény-, kategória- és állapotfüggo'
			,'1'
			,'03'
			,'Obj_Metadefinicio_Id-vel és ObjStateValue-val definiált'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'Esemény-, kategória- és folyamatfüggo'
			,'1'
			,'04'
			,'Obj_Metadefinicio_Id-vel és ObjStateValue-val definiált, workflow állapottal'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'Állapotfüggo'
			,'1'
			,'05'
			,'ütemezett állapotvizsgálat'
			); 

	 
/* -------------------------------------------------------------------------------
KodCsoport: FELADAT_IDOBAZIS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'FELADAT_IDOBAZIS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Aktuális idopont'
			,'1'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Aktuális dátum'
			,'1'
			,'02'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Dátumoszlop'
			,'1'
			,'03'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'Dátum tárgyszó'
			,'1'
			,'04'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'Helyettesíto idopont'
			,'1'
			,'05'
			,'ha az azonosító szerinti dátum nem ismert vagy üres'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'6'
			,'Helyettesíto dátum'
			,'1'
			,'06'
			,'ha az azonosító szerinti dátum nem ismert vagy üres'
			); 

	 
/* -------------------------------------------------------------------------------
KodCsoport: FELADAT_PRIORITAS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'FELADAT_PRIORITAS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'11'
			,'Alacsony'
			,'1'
			,'11'
			,'értesítés nélküli'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'21'
			,'Közepes'
			,'1'
			,'21'
			,'TÉR értesítés'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'31'
			,'Magas'
			,'1'
			,'31'
			,'E-mail értesítés'
			);
			
/* -------------------------------------------------------------------------------
KodCsoport: FELDOLGOZASI_ALLAPOT_JDV
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'FELDOLGOZASI_ALLAPOT_JDV' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'H'
			,'Hibás feldolgozás'
			,'0'
			,'H'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K'
			,'Katalogizált'
			,'0'
			,'K'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'O'
			,'Feldolgozás jó'
			,'0'
			,'O'
			);
			
/* -------------------------------------------------------------------------------
KodCsoport: FIZETESI_MOD
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'FIZETESI_MOD' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZA'
				,'Azonnali inkasszó'
				,'1'
				,'01'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZB'
				,'Bankgarancia'
				,'1'
				,'02'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZC'
				,'Csekk'
				,'1'
				,'03'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZD'
				,'Üdülési csekk'
				,'1'
				,'04'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZE'
				,'Banki átutalás engedményezéssel'
				,'1'
				,'05'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZF'
				,'Munkabérbol levonás'
				,'1'
				,'06'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZH'
				,'Határidos inkasszó'
				,'1'
				,'07'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZI'
				,'Hitelkártya'
				,'1'
				,'08'
				);
 
			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZK'
				,'Készpénz'
				,'1'
				,'09'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZL'
				,'Akkreditív'
				,'1'
				,'10'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZM'
				,'Kompenzálás'
				,'1'
				,'11'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZP'
				,'Postai utánvét/kifizetés'
				,'1'
				,'12'
				);
 
			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZR'
				,'Barter'
				,'1'
				,'13'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZS'
				,'Csoportos beszedés'
				,'1'
				,'14'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZT'
				,'Csoportos utalás'
				,'1'
				,'15'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZU'
				,'Banki átutalás'
				,'1'
				,'16'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZV'
				,'Váltó'
				,'1'
				,'17'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZX'
				,'Pénzügyileg nem rendezendo'
				,'1'
				,'18'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZY'
				,'Bankkártya'
				,'1'
				,'19'
				);

				
/* -------------------------------------------------------------------------------
KodCsoport: PIR_ALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'PIR_ALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'F'
				,'Fogadott'
				,'1'
				,'01'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'E'
				,'Ellenorzott'
				,'1'
				,'02'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'S'
				,'Sztornózott'
				,'1'
				,'03'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'R'
				,'Rontott'
				,'1'
				,'04'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'V'
				,'Véglegesített'
				,'1'
				,'05'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'U'
				,'Elutasított'
				,'1'
				,'06'
				);
				
/* -------------------------------------------------------------------------------
KodCsoport: DEVIZAKOD
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'DEVIZAKOD' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'HUF'
				,'HUF'
				,'1'
				,'01'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'EUR'
				,'EUR'
				,'1'
				,'02'
				);

			 insert into KRT_KodTarak(
				 KodCsoport_Id
				,Tranz_id
				,Stat_id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Sorrend
				) values (
				 @KODCSOPORT_ID
				,@Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'USD'
				,'USD'
				,'1'
				,'03'
				);
				
/* -------------------------------------------------------------------------------
KodCsoport: SELEJTEZESI_IDO
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'SELEJTEZESI_IDO' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'nem selejtezheto'
			,'0'
			,'9'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'2'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'5'
			,'0'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'10'
			,'0'
			,'4'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'15'
			,'0'
			,'5'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'30'
			,'0'
			,'6'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'6'
			,'75'
			,'0'
			,'7'
			); 

			
/* -------------------------------------------------------------------------------
KodCsoport: TEMPMAN_FILETIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'TEMPMAN_FILETIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Word'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Excel'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'PDF'
			,'0'
			,'3'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: IKT_SZAM_OSZTAS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'IKT_SZAM_OSZTAS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Normál'
			,'0'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Folyósorszámos'
			,'0'
			,null
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: IKTATASI_KOTELEZETTSEG
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'IKTATASI_KOTELEZETTSEG' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Nem iktatandó'
			,'Nem iktatandó'
			,'0'
			,'01'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Iktatandó'
			,'Iktatandó'
			,'0'
			,'02'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Kézzel iktatott'
			,'Kézzel iktatott'
			,'0'
			,'03'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Téves iktatás'
			,'Téves iktatás'
			,'0'
			,'04'
			); 

	 
/* -------------------------------------------------------------------------------
KodCsoport: IKTATOSZAM_KIEG
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'IKTATOSZAM_KIEG' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'00'
			,'A'
			,'1'
			,'1'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: IRAT_ALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'IRAT_ALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Megnyitott (Munkairat)'
			,'0'
			,'90'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Iktatandó (Munkairat)'
			,'0'
			,'91'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Befagyasztott (Munkairat)'
			,'0'
			,'92'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Iktatott'
			,'0'
			,'5'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'Átiktatott'
			,'0'
			,'6'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'08'
			,'Felszabadított'
			,'0'
			,'8'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'30'
			,'Kiadmányozott'
			,'0'
			,'7'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'90'
			,'Sztornózott'
			,'0'
			,'8'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: IRATPELDANY_ALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'IRATPELDANY_ALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Munkaállapotban lévo'
			,'0'
			,'95'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Iktatott'
			,'0'
			,'4'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'30'
			,'Kiadmányozott'
			,'0'
			,'30'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'31'
			,'Lezárt jegyzéken'
			,'0'
			,'31'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'32'
			,'Jegyzékre helyezett'
			,'0'
			,'32'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'40'
			,'Címzett átvette'
			,'0'
			,'41'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'45'
			,'Postázott'
			,'0'
			,'46'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'50'
			,'Továbbítás alatt'
			,'0'
			,'50'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'60'
			,'Jóváhagyás alatt'
			,'0'
			,'60'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'61'
			,'Újraküldendo'
			,'0'
			,'61'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'63'
			,'Kimeno küldeményben'
			,'0'
			,'63'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'64'
			,'Expediált'
			,'0'
			,'64'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'90'
			,'Sztornózott'
			,'0'
			,'91'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: IRAT_FAJTA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'IRAT_FAJTA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Eredeti'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Szignált irattári példány'
			,'1'
			,'9'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Másodlat'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'Másolat'
			,'0'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'Hitelesített másolat'
			,'0'
			,'4'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: IRAT_JELLEG
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'IRAT_JELLEG' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Webes'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'E-mail üzenet'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Word dokumentum'
			,'0'
			,'3'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'Egyéb'
			,'0'
			,'4'
			);
			
/* -------------------------------------------------------------------------------
KodCsoport: DOKUMENTUM_ALAIRAS_MANUALIS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'DOKUMENTUM_ALAIRAS_MANUALIS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Nincs vagy nem hiteles'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Hiteles'
			,'0'
			,'2'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: DOKUMENTUM_ALAIRAS_PKI
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'DOKUMENTUM_ALAIRAS_PKI' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Aláíratlan'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'10'
			,'Érvénytelen'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'11'
			,'KET minosített'
			,'0'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'12'
			,' Belso minosített'
			,'0'
			,'4'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'13'
			,' Szervezeti'
			,'0'
			,'5'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'14'
			,' Ellenorzés alatt'
			,'0'
			,'6'
			); 

			
/* -------------------------------------------------------------------------------
KodCsoport: IRATKATEGORIA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'IRATKATEGORIA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Magánlevél'
			,'0'
			,'9'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Beadvány'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Bejövo számla'
			,'1'
			,'10'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'Kimeno számla'
			,'1'
			,'11'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'Határozat'
			,'0'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Note
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'17'
			,'Fellebbezés'
			,'0'
			,'3'
			,'Review: Ez nem volt az OraJava-s CONTENTUM_DEMO_NEW -ban...'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'08'
			,'Eloterjesztés'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'09'
			,'Csekk'
			,'1'
			,'12'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'10'
			,'Csekk befizetés igazolás'
			,'1'
			,'13'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'11'
			,'Hivatalból'
			,'1'
			,'6'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'A'
			,'Ajánlat kérés'
			,'1'
			,'21'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'B'
			,'Ajánlat'
			,'1'
			,'22'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'C'
			,'Szerzodés'
			,'1'
			,'03'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'EU'
			,'EU dokumentum'
			,'1'
			,'EU'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'ik'
			,'Tájékoztató'
			,'1'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'LE'
			,'Utasítás'
			,'1'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'LEV'
			,'Levél'
			,'1'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Note
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'LV'
			,'Levél'
			,'1'
			,'Review: Nem ua, mint az kt.IRATKATEGORIA.LEV'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: IRATTIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'IRATTIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Magánlevél'
			,'BEJOVO'
			,'0'
			,'9'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Beadvány'
			,'BEJOVO'
			,'0'
			,'1'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'Határozat'
			,'BELSO'
			,'0'
			,'3'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			,Note
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'17'
			,'Fellebbezés'
			,'BEJOVO'
			,'0'
			,'3'
			,'Review: Ez nem volt az OraJava-s CONTENTUM_DEMO_NEW -ban...'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'08'
			,'Eloterjesztés'
			,'BELSO'
			,'0'
			,'2'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'11'
			,'Hivatalból'
			,'BELSO'
			,'0'
			,'6'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'A'
			,'Ajánlat kérés'
			,'BELSO'
			,'0'
			,'21'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'B'
			,'Ajánlat'
			,'BEJOVO'
			,'0'
			,'22'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'C'
			,'Szerzodés'
			,'BELSO'
			,'0'
			,'03'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'EU'
			,'EU dokumentum'
			,'BEJOVO'
			,'0'
			,'EU'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'ik'
			,'Tájékoztató'
			,'BELSO'
			,'0'
			,null
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'LE'
			,'Utasítás'
			,'BELSO'
			,'0'
			,null
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'LEV'
			,'Levél'
			,'BEJOVO'
			,'0'
			,null
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'KOZGY_ELOTERJ'
			,'Közgyulési eloterjesztés'
			,'BELSO'
			,'0'
			,null
			); 

	 
/* -------------------------------------------------------------------------------
KodCsoport: IRATTARI_JEL
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'IRATTARI_JEL' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'L'
			,'Levéltárba adandó'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'N'
			,'Nem selejtezheto'
			,'0'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'S'
			,'Selejtezheto'
			,'0'
			,'1'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: BARCOD_ALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'BARCOD_ALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'S'
			,'Szabad'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'F'
			,'Felhasznált'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'T'
			,'Törölt'
			,'0'
			,'3'
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: IRATTARIKIKEROALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'IRATTARIKIKEROALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Nyitott'
			,'0'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Engedélyezheto'
			,'0'
			,'02'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Engedélyezett'
			,'0'
			,'03'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Kiadott'
			,'0'
			,'04'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Visszaadott'
			,'0'
			,'05'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'Szerelt'
			,'0'
			,'06'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'Visszautasított'
			,'0'
			,'07'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'08'
			,'Sztornózott'
			,'0'
			,'08'
			);

			
/* -------------------------------------------------------------------------------
KodCsoport: KEZBESITESITETEL_ALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KEZBESITESITETEL_ALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1) 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Átadásra kijelölt'
			,'1'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Átadott'
			,'1'
			,'02'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Átvett'
			,'1'
			,'03'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'Visszaküldött'
			,'1'
			,'04'
			);
			
/* -------------------------------------------------------------------------------
KodCsoport: TargySzoFelhasznalasHelyek
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'TargySzoFelhasznalasHelyek' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,ObjTip_Id_AdatElem
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'a04417a6-54ac-4af1-9b63-5babf0203d42'
			,'U'
			,'Ügyirat'
			,'0'
			,null
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,ObjTip_Id_AdatElem
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'aa5e7bba-96a0-4c17-8709-06a6d297e107'
			,'I'
			,'Irat'
			,'0'
			,null
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,ObjTip_Id_AdatElem
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'b29081b9-3106-4df8-ab1f-d2008dced7aa'
			,'P'
			,'Irat Példány'
			,'0'
			,null
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,ObjTip_Id_AdatElem
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'cdac02ca-1763-4391-91d7-6914ee6f7be5'
			,'D'
			,'Dokumentum'
			,'0'
			,null
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,ObjTip_Id_AdatElem
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'265e5281-ef84-4ab2-bd54-5847cc4da106'
			,'K'
			,'Küldemények'
			,'0'
			,null
			); 

	 
/* -------------------------------------------------------------------------------
KodCsoport: KAPCSOLATTIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KAPCSOLATTIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'00'
			,'Egyéb'
			,'1'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Iratfordulat'
			,'1'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Iktatás példány alapján'
			,'1'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Eloirat-utóirat'
			,'1'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Ügy indító példány'
			,'1'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Megkeresés-válasz'
			,'1'
			,null
			); 
			
/* -------------------------------------------------------------------------------
KodCsoport: KEZELESI_FELJEGYZES_KULDEMENY
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KEZELESI_FELJEGYZES_KULDEMENY' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Iktatásra'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Téves szignálás miatt vissza'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Tájékoztatásra'
			,'0'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'Lezárásra'
			,'0'
			,'6'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'7'
			,'Lássa'
			,'0'
			,'7'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'8'
			,'Látta'
			,'0'
			,'7'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'9'
			,'Megjegyzés'
			,'Ezzel a típussal rögzülnek a megjegyzések!'
			,'0'
			,'9'
			); 

	 
/* -------------------------------------------------------------------------------
KodCsoport: KEZELESI_FELJEGYZESEK_TIPUSA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KEZELESI_FELJEGYZESEK_TIPUSA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Intézkedésre'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Véleményezésre'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Tájékoztatásra'
			,'0'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Megbeszélésre'
			,'0'
			,'4'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Jóváhagyásra'
			,'0'
			,'5'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'Aláírásra'
			,'0'
			,'6'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'09'
			,'Megjegyzés'
			,'0'
			,'9'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'10'
			,'Tárgyalási helyszín és ido'
			,'1'
			,'10'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'11'
			,'Irattározásra'
			,'0'
			,'11'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'12'
			,'Iktatásra'
			,'0'
			,'12'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'T'
			,'Tárgyalási helyszín'
			,'1'
			,'10'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'13'
			,'Eloadói munkanapló megjegyzés'
			,'0'
			,'13'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'14'
			,'Skontróra'
			,'0'
			,'14'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'15'
			,'Elutasításra'
			,'0'
			,'15'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Téves szignálás miatt vissza'
			,'0'
			,'16'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'Lezárásra'
			,'0'
			,'17'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'7'
			,'Lássa'
			,'0'
			,'18'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'8'
			,'Látta'
			,'0'
			,'19'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: KULDEMENY_ALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KULDEMENY_ALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'00'
			,'Átvett'
			,'Nincs bontva'
			,'0'
			,'09'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Érkeztetett'
			,'Ha bontható, fel is van bontva'
			,'0'
			,'01'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Továbbítás alatt'
			,'0'
			,'03'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Iktatott'
			,'0'
			,'04'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Szignált'
			,'0'
			,'05'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'Postázott'
			,'Postázott'
			,'0'
			,'06'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'10'
			,'Iktatás megtagadva'
			,'0'
			,'10'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'90'
			,'Sztornózott'
			,'0'
			,'07'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'51'
			,'Kimeno, összeállítás alatt'
			,'0'
			,'51'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'52'
			,'Expediált'
			,'0'
			,'52'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'99'
			,'Lezárt'
			,'Nem lehet sem szignálni, sem iktatni, sem ...'
			,'0'
			,'08'
			); 


/* -------------------------------------------------------------------------------
KodCsoport: KULDEMENY_TIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KULDEMENY_TIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Számla'
			,'0'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Szerzodés'
			,'0'
			,'02'
			); 
	 
/* -------------------------------------------------------------------------------
KodCsoport: KULDEMENY_ESEMENY_TIPUSA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KULDEMENY_ESEMENY_TIPUSA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'00'
			,'Küldemény átvétel'
			,'0'
			,'0'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Érkeztetés'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Átadás - átvétel'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Küldemény adatmódosítás'
			,'0'
			,'4'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Küldemény továbbítás'
			,'0'
			,'4'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Küldemény bontás'
			,'Küldemény bontás'
			,'0'
			,'5'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'Küldemény felszabadítása téves iktatásból'
			,'1'
			,'07'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0a'
			,'Küldemény felbontás'
			,'1'
			,'0'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'10'
			,'Iktatás'
			,'0'
			,'10'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'11'
			,'Betekintés'
			,'1'
			,'11'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'12'
			,'Betekintési kisérlet'
			,'1'
			,'12'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'13'
			,'Dokumentum betekintés'
			,'1'
			,'13'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'14'
			,'Dokumentum betekintés kisérlet'
			,'1'
			,'14'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'20'
			,'Ügyirat stornózva'
			,'1'
			,'20'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'21'
			,'Nem iktatatandóra állítás'
			,'1'
			,'21'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'22'
			,'Küldemény felbontás'
			,'1'
			,'22'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'30'
			,'Futárjegyzékre küldés'
			,'1'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'47'
			,'Futárjegyzékre helyezés'
			,'1'
			,'47'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'48'
			,'Futárjegyzékrol vissza'
			,'1'
			,'48'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'75'
			,'Csatolmány felvitel'
			,'1'
			,'75'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'76'
			,'Csatolmány törlés'
			,'1'
			,'76'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'88'
			,'Kézzel rögzített esemény'
			,'1'
			,'110'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'90'
			,'Sztornírozás'
			,'0'
			,'90'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'95'
			,'Selejtezési jegyzékre helyezés'
			,'1'
			,'95'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'96'
			,'Selejtezési jegyzékrol vissza'
			,'1'
			,'96'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'99'
			,'Lezárás'
			,'0'
			,'99'
			); 


/* -------------------------------------------------------------------------------
KodCsoport: KULDEMENY_IKTATAS_STATUSZA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KULDEMENY_IKTATAS_STATUSZA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Nem kell iktatni'
			,'0'
			,'0'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Nincs iktatva'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Iktatva'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Téves iktatás'
			,'0'
			,'3'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: KULDEMENY_KEZI_ESEMENY
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KULDEMENY_KEZI_ESEMENY' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'88'
			,'Kézzel rögzített esemény '
			,'1'
			,'01'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: KULDEMENY_KULDES_MODJA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KULDEMENY_KULDES_MODJA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'09'
			,'Kézbesíto útján'
			,'Kézbesíto útján'
			,'1'
			,'01'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Ügyfél'
			,'Ügyfél'
			,'1'
			,'02'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Postai sima'
			,'Postai sima'
			,'1'
			,'03'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Postai tértivevényes'
			,'Postai tértivevényes'
			,'1'
			,'10'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Postai ajánlott'
			,'Postai ajánlott'
			,'1'
			,'20'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Állami futárszolgálat'
			,'Állami futárszolgálat'
			,'1'
			,'40'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'Diplomáciai futárszolgálat'
			,'Diplomáciai futárszolgálat'
			,'1'
			,'60'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'08'
			,'Légiposta'
			,'Légiposta'
			,'1'
			,'70'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'10'
			,'Fax'
			,'Fax'
			,'1'
			,'90'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'11'
			,'E-mail'
			,'Elektronikus'
			,'0'
			,'92'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'13'
			,'Helyben'
			,''
			,'1'
			,'96'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'14'
			,'Postai távirat'
			,'Postai távirat'
			,'1'
			,'90'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'15'
			,'Portálon keresztül'
			,'Elektronikus'
			,'1'
			,'84'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'17'
			,'Postai elsobbségi'
			,'0'
			,'21'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'18'
			,'Futárszolgálat'
			,'1'
			,'18'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: KULDEMENYIKTATHATOSAG_SZIGNALASTIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KULDEMENYIKTATHATOSAG_SZIGNALASTIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Szignálási folyamatba még nem került be'
			,'0'
			,'00'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'S'
			,'Szervezetre szignálás szükséges'
			,'0'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'U'
			,'Szervezetre és ügyintézore szignálás is szükséges'
			,'0'
			,'02'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Iktatható'
			,'0'
			,'03'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: KIMENO_KULDEMENY_FAJTA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KIMENO_KULDEMENY_FAJTA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

		
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Szabvány levél 30 g-ig'
			,'1'
			,'01'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Levél 50 g-ig'
			,'1'
			,'02'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Levél 100 g-ig'
			,'1'
			,'03'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Levél 250 g-ig'
			,'1'
			,'04'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Levél 500 g-ig'
			,'1'
			,'05'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'Levél 750 g-ig'
			,'1'
			,'06'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'Levél 2000 g-ig'
			,'1'
			,'07'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'18'
			,'Levelezolap'
			,'1'
			,'18'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'19'
			,'Hivatalos irat'
			,'Nincs megkülönböztetve, hogy elsobbségi vagy sem'
			,'1'
			,'19'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'20'
			,'Hivatalos irat saját kézbe'
			,'1'
			,'Nincs megkülönböztetve, hogy elsobbségi vagy sem'
			,'20'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_01'
			,'Szabvány levél 20 g-ig'
			,'1'
			,'Európai'
			,'31'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_02'
			,'Levél 20 g-ig'
			,'1'
			,'Európai'
			,'32'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_03'
			,'Levél 50 g-ig'
			,'1'
			,'Európai'
			,'33'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_04'
			,'Levél 100 g-ig'
			,'1'
			,'Európai'
			,'34'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_05'
			,'Levél 250 g-ig'
			,'1'
			,'Európai'
			,'35'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_06'
			,'Levél 500 g-ig'
			,'1'
			,'Európai'
			,'36'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_07'
			,'Levél 1000 g-ig'
			,'1'
			,'Európai'
			,'37'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_08'
			,'Levél 1500 g-ig'
			,'1'
			,'Európai'
			,'38'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_09'
			,'Levél 2000 g-ig'
			,'1'
			,'Európai'
			,'39'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_10'
			,'Levelezolap'
			,'1'
			,'Európai'
			,'40'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_01'
			,'Szabvány levél 20 g-ig'
			,'1'
			,'Egyéb külföldi'
			,'51'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_02'
			,'Levél 20 g-ig'
			,'1'
			,'Egyéb külföldi'
			,'52'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_03'
			,'Levél 50 g-ig'
			,'1'
			,'Egyéb külföldi'
			,'53'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_04'
			,'Levél 100 g-ig'
			,'1'
			,'Egyéb külföldi'
			,'54'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_05'
			,'Levél 250 g-ig'
			,'1'
			,'Egyéb külföldi'
			,'55'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_06'
			,'Levél 500 g-ig'
			,'1'
			,'Egyéb külföldi'
			,'56'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_07'
			,'Levél 1000 g-ig'
			,'1'
			,'Egyéb külföldi'
			,'57'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_08'
			,'Levél 1500 g-ig'
			,'1'
			,'Egyéb külföldi'
			,'58'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_09'
			,'Levél 2000 g-ig'
			,'1'
			,'Egyéb külföldi'
			,'59'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_10'
			,'Levelezolap'
			,'1'
			,'Egyéb külföldi'
			,'60'
			);
		 
	 
/* -------------------------------------------------------------------------------
KodCsoport: KIMENO_KULDEMENY_AR_NEM_ELSOBBSEGI
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KIMENO_KULDEMENY_AR_NEM_ELSOBBSEGI' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)
		

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'80'
			,'Szabvány levél 30 g-ig'
			,'1'
			,'01'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'110'
			,'Levél 50 g-ig'
			,'1'
			,'02'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'140'
			,'Levél 100 g-ig'
			,'1'
			,'03'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'175'
			,'Levél 250 g-ig'
			,'1'
			,'04'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'315'
			,'Levél 500 g-ig'
			,'1'
			,'05'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'515'
			,'Levél 750 g-ig'
			,'1'
			,'06'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'655'
			,'Levél 2000 g-ig'
			,'1'
			,'07'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'18'
			,'80'
			,'Levelezolap'
			,'1'
			,'18'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'19'
			,'290'
			,'Hivatalos irat'
			,'1'
			,'19'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'20'
			,'355'
			,'1'
			,'Hivatalos irat saját kézbe'
			,'20'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_01'
			,'210'
			,'1'
			,'Európai Szabvány levél 20 g-ig'
			,'31'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_02'
			,'280'
			,'1'
			,'Európai Levél 20 g-ig'
			,'32'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_03'
			,'370'
			,'1'
			,'Európai Levél 50 g-ig'
			,'33'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_04'
			,'470'
			,'1'
			,'Európai Levél 100 g-ig'
			,'34'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_05'
			,'850'
			,'1'
			,'Európai Levél 250 g-ig'
			,'35'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_06'
			,'1500'
			,'1'
			,'Európai Levél 500 g-ig'
			,'36'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_07'
			,'3000'
			,'1'
			,'Európai Levél 1000 g-ig'
			,'37'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_08'
			,'4000'
			,'1'
			,'Európai Levél 1500 g-ig'
			,'38'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_09'
			,'4500'
			,'1'
			,'Európai Levél 2000 g-ig'
			,'39'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_10'
			,'210'
			,'1'
			,'Európai Levelezolap'
			,'40'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_01'
			,'240'
			,'1'
			,'Egyéb külföldi Szabvány levél 20 g-ig'
			,'51'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_02'
			,'310'
			,'1'
			,'Egyéb külföldi Levél 20 g-ig'
			,'52'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_03'
			,'440'
			,'1'
			,'Egyéb külföldi Levél 50 g-ig'
			,'53'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_04'
			,'520'
			,'1'
			,'Egyéb külföldi Levél 100 g-ig'
			,'54'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_05'
			,'1000'
			,'1'
			,'Egyéb külföldi Levél 250 g-ig'
			,'55'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_06'
			,'1700'
			,'1'
			,'Egyéb külföldi Levél 500 g-ig'
			,'56'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_07'
			,'3300'
			,'1'
			,'Egyéb külföldi Levél 1000 g-ig'
			,'57'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_08'
			,'4500'
			,'1'
			,'Egyéb külföldi Levél 1500 g-ig'
			,'58'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_09'
			,'5000'
			,'1'
			,'Egyéb külföldi Levél 2000 g-ig'
			,'59'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_10'
			,'240'
			,'1'
			,'Egyéb külföldi Levelezolap'
			,'60'
			); 		
		
/* -------------------------------------------------------------------------------
KodCsoport: KIMENO_KULDEMENY_AR_ELSOBBSEGI
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KIMENO_KULDEMENY_AR_ELSOBBSEGI' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

		
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'105'
			,'Szabvány levél 30 g-ig'
			,'1'
			,'01'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'150'
			,'Levél 50 g-ig'
			,'1'
			,'02'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'185'
			,'Levél 100 g-ig'
			,'1'
			,'03'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'240'
			,'Levél 250 g-ig'
			,'1'
			,'04'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'380'
			,'Levél 500 g-ig'
			,'1'
			,'05'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'640'
			,'Levél 750 g-ig'
			,'1'
			,'06'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'840'
			,'Levél 2000 g-ig'
			,'1'
			,'07'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'18'
			,'105'
			,'Levelezolap'
			,'1'
			,'18'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'19'
			,'290'
			,'Hivatalos irat'
			,'1'
			,'19'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'20'
			,'355'
			,'1'
			,'Hivatalos irat saját kézbe'
			,'20'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_01'
			,'230'
			,'1'
			,'Európai Szabvány levél 20 g-ig'
			,'31'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_02'
			,'300'
			,'1'
			,'Európai Levél 20 g-ig'
			,'32'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_03'
			,'390'
			,'1'
			,'Európai Levél 50 g-ig'
			,'33'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_04'
			,'490'
			,'1'
			,'Európai Levél 100 g-ig'
			,'34'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_05'
			,'1050'
			,'1'
			,'Európai Levél 250 g-ig'
			,'35'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_06'
			,'1850'
			,'1'
			,'Európai Levél 500 g-ig'
			,'36'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_07'
			,'3500'
			,'1'
			,'Európai Levél 1000 g-ig'
			,'37'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_08'
			,'4650'
			,'1'
			,'Európai Levél 1500 g-ig'
			,'38'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_09'
			,'5800'
			,'1'
			,'Európai Levél 2000 g-ig'
			,'39'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K1_10'
			,'230'
			,'1'
			,'Európai Levelezolap'
			,'40'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_01'
			,'260'
			,'1'
			,'Egyéb külföldi Szabvány levél 20 g-ig'
			,'51'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_02'
			,'330'
			,'1'
			,'Egyéb külföldi Levél 20 g-ig'
			,'52'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_03'
			,'460'
			,'1'
			,'Egyéb külföldi Levél 50 g-ig'
			,'53'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_04'
			,'540'
			,'1'
			,'Egyéb külföldi Levél 100 g-ig'
			,'54'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_05'
			,'1170'
			,'1'
			,'Egyéb külföldi Levél 250 g-ig'
			,'55'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_06'
			,'2000'
			,'1'
			,'Egyéb külföldi Levél 500 g-ig'
			,'56'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_07'
			,'3900'
			,'1'
			,'Egyéb külföldi Levél 1000 g-ig'
			,'57'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_08'
			,'5300'
			,'1'
			,'Egyéb külföldi Levél 1500 g-ig'
			,'58'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_09'
			,'6700'
			,'1'
			,'Egyéb külföldi Levél 2000 g-ig'
			,'59'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Egyeb
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K2_10'
			,'260'
			,'1'
			,'Egyéb külföldi Levelezolap'
			,'60'
			); 
	 
/* -------------------------------------------------------------------------------
KodCsoport: KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'215'
			,'Ajánlott'
			,'1'
			,'01'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'135'
			,'Tértivevény'
			,'1'
			,'02'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'200'
			,'Saját kézbe'
			,'1'
			,'03'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'55'
			,'E-értesítés'
			,'1'
			,'04'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'55'
			,'E-elorejelzés'
			,'1'
			,'05'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'220'
			,'Postai lezáró szolgálat'
			,'1'
			,'06'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K_01'
			,'750'
			,'Külföldi Ajánlott'
			,'1'
			,'11'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K_02'
			,'330'
			,'Külföldi Tértivevény'
			,'1'
			,'12'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K_03'
			,'350'
			,'Külföldi Saját kézbe'
			,'1'
			,'13'
			); 
		 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'K_06'
			,'220'
			,'Külföldi Postai lezáró szolgálat'
			,'1'
			,'16'
			); 
	 
/* -------------------------------------------------------------------------------
KodCsoport: LEZARAS_OKA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'LEZARAS_OKA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Elintézés'
			,'1'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'9'
			,'Szerelés'
			,'1'
			,null
			); 

/* -------------------------------------------------------------------------------
KodCsoport: MAPPA_TIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'MAPPA_TIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Fizikai'
			,'0'
			,'1'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Virtuális'
			,'0'
			,'2'
			); 
	

/* -------------------------------------------------------------------------------
KodCsoport: MEGYE
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'MEGYE' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Pest'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Baranya'
			,'1'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Bács-Kiskun'
			,'1'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Békés'
			,'1'
			,'4'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Borsod-Abaúj-Zemplén'
			,'1'
			,'5'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'Csongrád'
			,'1'
			,'6'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'Fejér'
			,'1'
			,'7'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'08'
			,'Gyor-Moson-Sopron'
			,'1'
			,'8'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'09'
			,'Hajdú-Bihar'
			,'1'
			,'9'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'10'
			,'Heves'
			,'1'
			,'10'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'11'
			,'Jász-Nagykun-Szolnok'
			,'1'
			,'11'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'12'
			,'Komárom-Esztergom'
			,'1'
			,'12'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'13'
			,'Nógrád'
			,'1'
			,'13'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'14'
			,'Somogy'
			,'1'
			,'14'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'15'
			,'Szabolcs-Szatmár-Bereg'
			,'1'
			,'15'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'16'
			,'Tolna'
			,'1'
			,'16'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'17'
			,'Vas'
			,'1'
			,'17'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'18'
			,'Veszprém'
			,'1'
			,'18'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'19'
			,'Zala'
			,'1'
			,'19'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: MENNYISEGI_EGYSEG
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'MENNYISEGI_EGYSEG' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'lap'
			,'1'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'darab'
			,'1'
			,null
			); 

/* -------------------------------------------------------------------------------
KodCsoport: MINOSITESI_JELOLES
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'MINOSITESI_JELOLES' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'00'
			,'Nem minosített'
			,'Nem minosített'
			,'1'
			,'00'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Nem nyilvános'
			,'Nem nyilvános'
			,'1'
			,'01'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Titkos'
			,'Titkos'
			,'1'
			,'02'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Szigorúan titkos'
			,'Szigorúan titkos'
			,'1'
			,'03'
			); 

	 
/* -------------------------------------------------------------------------------
KodCsoport: MODUL_TIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'MODUL_TIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'B'
			,'Business function'
			,'0'
			,'B'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'F'
			,'Frame'
			,'0'
			,'F'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'M'
			,'Programból indítható modul'
			,'1'
			,'EU'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: PARTNER_FORRAS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'PARTNER_FORRAS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'címtár'
			,'1'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'törzsadat'
			,'1'
			,'02'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'migrált'
			,'1'
			,'03'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'manuális'
			,'1'
			,'04'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'A'
			,'ADAJK'
			,'1'
			,'05'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'C'
			,'civil törzs'
			,'1'
			,'06'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'H'
			,'HAIR'
			,'1'
			,'07'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'I'
			,'intézmények'
			,'1'
			,'08'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'M'
			,'minosített beszállítók'
			,'1'
			,'09'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'O'
			,'önkormányzatok'
			,'1'
			,'10'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'P'
			,'KÜK'
			,'1'
			,'11'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'Q'
			,'Önkormányzat KÜK'
			,'1'
			,'12'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'R'
			,'Beszállító KÜK'
			,'1'
			,'13'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'S'
			,'Iskolák KÜK'
			,'1'
			,'14'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'T'
			,'Szociális otthon KÜK'
			,'1'
			,'15'
			);
/* -------------------------------------------------------------------------------
KodCsoport: PARTNERKAPCSOLAT_TIPUSA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'PARTNERKAPCSOLAT_TIPUSA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Szervezete, felettes szervezete'
			,'Felettese'
			,'0'
			,'1'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Felhasználó szervezete'
			,'Szervezete'
			,'1'
			,'2'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Szervezeti egység érkeztetohelye'
			,'0'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'Szervezeti egység iktatóhelye'
			,'0'
			,'4'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'Szervezeti egység irattára'
			,'1'
			,'5'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'7'
			,'Szakértoi csoportja'
			,'1'
			,'7'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'8'
			,'Szakértoi csoport vezetoje'
			,'1'
			,'8'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'9'
			,'Szakértoi csoport fogadója'
			,'1'
			,'9'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'11'
			,'Alárendelt partner'
			,'1'
			,'11'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'A'
			,'Szakértoi csoport tagja'
			,'1'
			,'A'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'B'
			,'Konszolidált partner'
			,'1'
			,'B'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'V'
			,'Szervezeti egység vezetoje'
			,'1'
			,'V'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: PMINOSITES_ALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'PMINOSITES_ALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Beadva, elbírálás alatt'
			,'1'
			,'0'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Elfogadva'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'9'
			,'Elutasítva'
			,'1'
			,'9'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: POSTAZAS_IRANYA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'POSTAZAS_IRANYA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Belso'
			,'0'
			,'0'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Bejövo'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Kimeno'
			,'0'
			,'2'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: REGIO
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'REGIO' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Közép-Magyarország'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Közép-Dunántúl'
			,'1'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Nyugat-Dunántúl'
			,'1'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Dél-Dunántúl'
			,'1'
			,'4'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Észak-Magyarország'
			,'1'
			,'5'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'Észak-Alföld'
			,'1'
			,'6'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'Dél-Alföld'
			,'1'
			,'7'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: SPEC_SZERVEK
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'SPEC_SZERVEK' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,ObjTip_Id_AdatElem
			,Obj_Id
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'fae5978e-448e-4a47-afc4-d799a7a9062a'
			,null
			,'SPEC_SZERVEK.POSTAZO'
			,'Postázó'
			,'1'
			,null
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,ObjTip_Id_AdatElem
			,Obj_Id
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'fae5978e-448e-4a47-afc4-d799a7a9062a'
			,null
			,'SPEC_SZERVEK.KOZPONTIIRATTAR'
			,'Központi irattár'
			,'1'
			,null
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,ObjTip_Id_AdatElem
			,Obj_Id
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'fae5978e-448e-4a47-afc4-d799a7a9062a'
			,null
			,'SPEC_SZERVEK.KOZPONTIIKTATO'
			,'Központi iktató'
			,'1'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,ObjTip_Id_AdatElem
			,Obj_Id
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'fae5978e-448e-4a47-afc4-d799a7a9062a'
			,null
			,'SPEC_SZERVEK.SKONTROIRATTAROS'
			,'Skontró irattáros'
			,'1'
			,null
			);
			
			insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,ObjTip_Id_AdatElem
			,Obj_Id
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'fae5978e-448e-4a47-afc4-d799a7a9062a'
			,null
			,'SPEC_SZERVEK.ELEKTRONIKUSIRATTAR'
			,'Elektronikus irattár'
			,'1'
			,null
			); 
 
			insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,ObjTip_Id_AdatElem
			,Obj_Id
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Note
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'fae5978e-448e-4a47-afc4-d799a7a9062a'
			,null
			,'SPEC_SZERVEK.EGYEBSZERVEZETIRATTAR'
			,'Egyéb szervezet irattár'
			,'1'
			,null
			,'Egyéb szervezetnek való átadás szimbolikus irattára'
			);

	 
--/* -------------------------------------------------------------------------------
--KodCsoport: Postakonyv_Vevokodok
--------------------------------------------------------------------------------- */
--
--	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'Postakonyv_Vevokodok' and getdate() between ErvKezd and ErvVege)
--	if @KODCSOPORT_ID is null
--		RAISERROR('[65003]',16,1)
--
--
--		 insert into KRT_KodTarak(
--			 KodCsoport_Id
--			,Tranz_id
--			,Stat_id
--			,Org
--			,Kod
--			,Nev
--			,Modosithato
--			,Sorrend
--			) values (
--			 @KODCSOPORT_ID
--			,@Tranz_id
--			,@STAT_ID_ALLAPOT_KELETKEZIK
--			,@Org
--			,'FPH_KOZP'
--			,'10101309'
--			,'1'
--			,null
--			);
--
--		 insert into KRT_KodTarak(
--			 KodCsoport_Id
--			,Tranz_id
--			,Stat_id
--			,Org
--			,Kod
--			,Nev
--			,Modosithato
--			,Sorrend
--			) values (
--			 @KODCSOPORT_ID
--			,@Tranz_id
--			,@STAT_ID_ALLAPOT_KELETKEZIK
--			,@Org
--			,'FPH_ADO'
--			,'10101308'
--			,'1'
--			,null
--			); 
--
--/* -------------------------------------------------------------------------------
--KodCsoport: Postakonyv_MegallapodasAzonositok
--------------------------------------------------------------------------------- */
--
--	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'Postakonyv_MegallapodasAzonositok' and getdate() between ErvKezd and ErvVege)
--	if @KODCSOPORT_ID is null
--		RAISERROR('[65003]',16,1)
--
--
--		 insert into KRT_KodTarak(
--			 KodCsoport_Id
--			,Tranz_id
--			,Stat_id
--			,Org
--			,Kod
--			,Nev
--			,Modosithato
--			,Sorrend
--			) values (
--			 @KODCSOPORT_ID
--			,@Tranz_id
--			,@STAT_ID_ALLAPOT_KELETKEZIK
--			,@Org
--			,'FPH_KOZP'
--			,'1180-1840'
--			,'1'
--			,null
--			);
--
--		 insert into KRT_KodTarak(
--			 KodCsoport_Id
--			,Tranz_id
--			,Stat_id
--			,Org
--			,Kod
--			,Nev
--			,Modosithato
--			,Sorrend
--			) values (
--			 @KODCSOPORT_ID
--			,@Tranz_id
--			,@STAT_ID_ALLAPOT_KELETKEZIK
--			,@Org
--			,'FPH_ADO'
--			,'11800269'
--			,'1'
--			,null
--			); 

/* -------------------------------------------------------------------------------
KodCsoport: SURGOSSEG
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'SURGOSSEG' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'Azonnali'
			,'Azonnali'
			,'1'
			,'01'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'IratSzerint'
			,'Irat Szerint'
			,'1'
			,'02'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'Sürgos (5 nap)'
			,'1'
			,'03'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'15'
			,'15 nap'
			,'1'
			,'04'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'30'
			,'Normál'
			,'1'
			,'05'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'60'
			,'60 nap'
			,'1'
			,'06'
			); 


/* -------------------------------------------------------------------------------
KodCsoport: TERTIVEVENY_ALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'TERTIVEVENY_ALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Elokészített'
			,'0'
			,'0'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Feltöltött'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Regisztrált'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'9'
			,'Feldolgozott'
			,'0'
			,'9'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: TERTIVEVENY_VISSZA_KOD
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'TERTIVEVENY_VISSZA_KOD' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Címzett átvette'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'10'
			,'Ismeretlen címzett'
			,'1'
			,'10'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Más átvette'
			,'1'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'9'
			,'Nem vette át'
			,'1'
			,'9'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: IRATPELDANY_POSTAZAS_ALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'IRATPELDANY_POSTAZAS_ALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Elso postázás'
			,'0'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Újrapostázás'
			,'0'
			,'02'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Elokészítés'
			,'0'
			,'03'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: TOVABBITO_SZERVEZET
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'TOVABBITO_SZERVEZET' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'ÁLLFUT'
			,'Állami futárszolgálat'
			,'0'
			,'5'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'DHL'
			,'DHL futárszolgálat'
			,'0'
			,'10'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'EGOV'
			,'eGovPortal'
			,'0'
			,'91'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'FAX'
			,'Telefax'
			,'0'
			,'15'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'MAIL'
			,'E-mail'
			,'0'
			,'18'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'NH'
			,'NEM HASZNÁLANDÓ'
			,'1'
			,null
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'POSTA'
			,'Magyar Posta'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'STB'
			,'Egyéb'
			,'0'
			,'99'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'Kezbesito'
			,'Kézbesíto'
			,'0'
			,'100'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: UGY_JELLEG
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'UGY_JELLEG' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Határozattal zárul'
			,'1'
			,'110'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Hatósági bizonyítvány'
			,'1'
			,'120'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Hatósági igazolvány'
			,'1'
			,'130'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Hatósági nyilvántartás'
			,'1'
			,'140'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Egyéb nem határozattal zár.'
			,'1'
			,'150'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'10'
			,'Jogorvoslati ügy'
			,'1'
			,'300'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'16'
			,'Közérdeku bejelentés'
			,'1'
			,'210'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'17'
			,'Közérdeku javaslat'
			,'1'
			,'220'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'18'
			,'Panasz'
			,'1'
			,'230'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'19'
			,'Egyéb nem hatósági ügy'
			,'1'
			,'240'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: UGYINTEZES_ALAPJA
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'UGYINTEZES_ALAPJA' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Hagyományos, papír'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Elektronikus (nincs papír)'
			,'0'
			,'2'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: UGYIRAT_ALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'UGYIRAT_ALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Note
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Megnyitott'
			,'0'
			,'Megnyitott (Munkaanyag)'
			,'0'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Szignált'
			,'0'
			,'02'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Iktatott'
			,'0'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'Ügyintézés alatt'
			,'0'
			,'04'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'Skontróban'
			,'0'
			,'05'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'09'
			,'Lezárt'
			,'0'
			,'11'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'10'
			,'Irattárban orzött'
			,'0'
			,'12'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'11'
			,'Irattárba küldött'
			,'0'
			,'13'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'13'
			,'Kölcsönzött'
			,'0'
			,'18'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'30'
			,'Jegyzékre helyezett'
			,'0'
			,'30'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'31'
			,'Lezárt jegyzékben lévo'
			,'0'
			,'31'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'32'
			,'Selejtezett'
			,'0'
			,'32'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'33'
			,'Levéltárba adott'
			,'0'
			,'33'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'34'
			,'Egyéb szervezetnek átadott'
			,'0'
			,'34'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'50'
			,'Továbbítás alatt'
			,'0'
			,'50'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'52'
			,'Irattározásra jóváhagyás alatt'
			,'0'
			,'52'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'54'
			,'Átmeneti irattárból kikölcsönzött'
			,'1'
			,'54'
			); 


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'55'
			,'Irattárból elkért'
			,'0'
			,'55'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'56'
			,'Engedélyezett kikéron lévo'
			,'0'
			,'55'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'57'
			,'Skontróból elkért'
			,'0'
			,'57'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'60'
			,'Szerelt'
			,'0'
			,'09'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'70'
			,'Skontróba helyezés jóváhagyása'
			,'0'
			,'70'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'71'
			,'Skontróból kikért'
			,'0'
			,'71'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'90'
			,'Sztornózott'
			,'0'
			,'99'
			);
		 
		insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'98'
			,'Elintézetté nyilvánítás jóváhagyása'
			,'0'
			,'98'
			);
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'99'
			,'Elintézett'
			,'0'
			,'08'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: UGYIRATDARAB_ALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'UGYIRATDARAB_ALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Iktatott'
			,'0'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'09'
			,'Lezárt'
			,'0'
			,'09'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'60'
			,'Szerelt'
			,'0'
			,'60'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'99'
			,'Elintézetté nyilvánított'
			,'0'
			,'99'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: UGYIRAT_ESEMENY
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'UGYIRAT_ESEMENY' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Iktatás'
			,'Iktatás'
			,'1'
			,'1'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Átadás-átvétel'
			,'Átadás-átvétel'
			,'1'
			,'2'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Szignálás'
			,'Szignálás'
			,'1'
			,'3'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Ügyintézésre átvétel'
			,'Ügyintézésre átvétel'
			,'1'
			,'4'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Selejtezés'
			,'Selejtezés'
			,'1'
			,'5'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'Ügyirat sztornó'
			,'Ügyirat sztornó'
			,'1'
			,'6'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'Skontróba (határidobe) tétel'
			,'Skontróba (határidobe) tétel'
			,'1'
			,'7'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'08'
			,'Skontróba tétel sztornó'
			,'Skontróba tétel sztornó'
			,'1'
			,'8'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'09'
			,'Skontróból (határidobol) visszavét'
			,'Skontróból (határidobol) visszavét'
			,'1'
			,'9'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'10'
			,'Átmeneti (kézi) irattárba vétel'
			,'Átmeneti (kézi) irattárba vétel'
			,'1'
			,'10'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'11'
			,'Központi irattárba vétel'
			,'Központi irattárba vétel'
			,'1'
			,'11'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'12'
			,'Ügyirat lezárás (ad acta, a/a)'
			,'Ügyirat lezárás (ad acta, a/a)'
			,'1'
			,'12'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'13'
			,'Központi irattárba küldés'
			,'1'
			,'13'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'14'
			,'Ügyirat lezárás visszavonása'
			,'1'
			,'14'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'15'
			,'Visszavét szignálásra'
			,'1'
			,'15'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'16'
			,'Átadás ügyintézésre'
			,'1'
			,'16'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'17'
			,'Dossziéba helyezés'
			,'1'
			,'17'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'20'
			,'Selejtezésre jelölés'
			,'Selejtezésre jelölés'
			,'1'
			,'20'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'21'
			,'Selejtezésre jelölés törlése'
			,'Selejtezésre jelölés törlése'
			,'1'
			,'21'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'22'
			,'Ügyirat-besorolás felülvizsgálata'
			,'1'
			,'22'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'23'
			,'Megsemmisítés'
			,'1'
			,'23'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'30'
			,'Levéltári átadásra jelölés'
			,'Levéltári átadásra jelölés'
			,'1'
			,'30'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'31'
			,'Levéltári átadásra jelölés törlése'
			,'Levéltári átadásra jelölés törlése'
			,'1'
			,'31'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'32'
			,'Levéltárba adás'
			,'0'
			,'32'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'33'
			,'Levéltárba adás visszavonása'
			,'1'
			,'33'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'34'
			,'Megsemmisítésre kijelölés'
			,'1'
			,'34'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'35'
			,'Megsemmisítésre kijelölés törlése'
			,'1'
			,'35'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'40'
			,'Jegyzékre tétel'
			,'1'
			,'40'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'41'
			,'Jegyzékrol levétel'
			,'1'
			,'41'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'55'
			,'Központi irattárból kikölcsönzés'
			,'1'
			,'55'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'56'
			,'Átmeneti irattárból kikölcsönözés'
			,'1'
			,'56'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'57'
			,'Kölcsönzésbol visszavétel'
			,'1'
			,'57'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'60'
			,'Szerelés'
			,'Szerelés-csatolás'
			,'1'
			,'60'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'61'
			,'Szerelés megszüntetése'
			,'1'
			,'61'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'62'
			,'Lezárás visszavonás iktatás miatt'
			,'1'
			,'62'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'63'
			,'Visszazárás iktatás után'
			,'1'
			,'63'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'65'
			,'Csatolás'
			,'Csatolás'
			,'1'
			,'65'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'70'
			,'Ügyirat továbbítás'
			,'1'
			,'70'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'75'
			,'Csatolmány felvitel'
			,'1'
			,'75'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'76'
			,'Csatolmány törlés'
			,'1'
			,'76'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'77'
			,'Kézbesitési tétel átadás'
			,'1'
			,'77'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'88'
			,'Kézzel rögzített esemény'
			,'1'
			,'110'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'89'
			,'Kivétel átmeneti irattárból'
			,'1'
			,'89'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'90'
			,'Elintézetté nyilvánítás'
			,'1'
			,'90'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'91'
			,'Elintézettség visszavonása'
			,'1'
			,'91'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'92'
			,'Átmeneti irattárba küldés'
			,'1'
			,'92'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'93'
			,'Átmeneti irattárba vétel'
			,'1'
			,'93'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'94'
			,'Betekintés'
			,'1'
			,'94'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'95'
			,'Betekintési kisérlet'
			,'1'
			,'95'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'96'
			,'Dokumentum betekintés'
			,'1'
			,'96'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'97'
			,'Dokumentum betekintési kisérlet'
			,'Dokumentum betekintési kisérlet'
			,'0'
			,'97'
			); 

	 
/* -------------------------------------------------------------------------------
KodCsoport: UGYIRAT_KAPCSOLATTIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'UGYIRAT_KAPCSOLATTIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Csatolt'
			,'0'
			,null
			); 

/* -------------------------------------------------------------------------------
KodCsoport: UGYIRAT_KEZI_ESEMENY
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'UGYIRAT_KEZI_ESEMENY' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'88'
			,'Kézzel rögzített esemény '
			,'1'
			,'01'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'89'
			,'Ügyirat selejtezés elotti felülvizsgálata'
			,'1'
			,'89'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: UGYTIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'UGYTIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'KOZGY_ELOTERJ'
			,'Közgyulési eloterjesztés'
			,'0'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'BIZ_ELOTERJ'
			,'Bizottsági eloterjesztés'
			,'0'
			,'2'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: VISSZAVAROLAG
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'VISSZAVAROLAG' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Tájékoztatásra'
			,'Tájékoztatásra'
			,'1'
			,'12'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Visszavárólag'
			,'Visszakérem'
			,'1'
			,'31'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Válaszadási kötelezettség'
			,'Válaszolni kell, de nem kell vissza az eredeti'
			,'0'
			,'21'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'Visszavárólag, válaszadási kötelezettség'
			,'Válaszolni kell és az eredetit is vissza kell adni'
			,'0'
			,'32'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'Postázásra'
			,'1'
			,'11'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'6'
			,'Visszavárólag, véleményezni'
			,'1'
			,'33'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'7'
			,'Megkeresés'
			,'0'
			,'13'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: ACL_DEFAULT_BINDINGS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'ACL_DEFAULT_BINDINGS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,ObjTip_Id_AdatElem
			,Obj_Id
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'ebb2a289-8ea7-46cb-89d0-c3113e416f5a'
			,'ecb902e5-fd38-4d06-9e0e-a0b2733fb2e9'
			,'EREC_eMailBoritekok-KUK'
			,'EREC_eMailBoritekok KÜK default elérés'
			,'0'
			,null
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,ObjTip_Id_AdatElem
			,Obj_Id
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06f1d5a9-57b5-4a9b-b131-7634c8750cca'
			,'ecb902e5-fd38-4d06-9e0e-a0b2733fb2e9'
			,'EREC_IraIktatokonyvek-KUK'
			,'EREC_IraIktatokonyvek KÜK default elérés'
			,'0'
			,null
			); 

	 
/* -------------------------------------------------------------------------------
KodCsoport: UGYIRAT_OBJ_KAPCSOLATTIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'UGYIRAT_OBJ_KAPCSOLATTIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Csatolt'
			,'Csatolt ügyiratok'
			,'1'
			,'1'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Iratfordulat'
			,'Irat-Irat közötti kapcsolat'
			,'1'
			,'2'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Megkeresés-válasz'
			,'Egy ügyün belüli irat és iratpéldány közötti kapcsolat'
			,'1'
			,'3'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Ügy indító példány'
			,'Amikor egy iratpéldány alapján indítanak új ügyet'
			,'1'
			,'4'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'05'
			,'Egyéb'
			,'Egyéb kapcsolatok leírására'
			,'1'
			,'5'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'06'
			,'Átiktatás'
			,'Átiktatás során a felszabadított és az új irat közti kapcsolatot jelzi'
			,'0'
			,'41'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'07'
			,'Régi ügyirat szerelése'
			,'Egy ügyirat és a bele szerelt régi (2008 elotti) ügyirat közötti kapcsolat reprezentálása'
			,'1'
			,'42'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'11'
			,'Boríték-Tartalom'
			,'Egy küldemény és az azt eredetileg tartalmazó küldemény (boríték) közötti kapcsolat reprezentálása (elozmény borítékja kapcsoltnak)'
			,'1'
			,'51'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'12'
			,'Irat-Küldemény'
			,'Iktatott irathoz kapcsolódó nem iktatott küldemény (pl. szerzodéshez tartozó számla)'
			,'1'
			,'6'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Egyeb
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'13'
			,'Ügyirat-Küldemény'
			,'Ügyirathoz kapcsolódó nem iktatott küldemény (pl. szerzodéshez tartozó számla)'
			,'1'
			,'7'
			);
	 
/* -------------------------------------------------------------------------------
KodCsoport: IRAT_MINOSITES
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'IRAT_MINOSITES' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'10'
			,'Belso használatra készült, nem nyilvános'
			,'1'
			,'10'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'20'
			,'Döntés-elokészítéssel összefüggo adatot tartalmaz, nem nyilvános'
			,'1'
			,'20'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: IDOEGYSEG
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'IDOEGYSEG' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'perc'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'60'
			,'óra'
			,'1'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1440'
			,'nap'
			,'1'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'-1440'
			,'munkanap'
			,'1'
			,'4'
			);

/* -------------------------------------------------------------------------------
KodCsoport: HELYETTESITES_MOD
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'HELYETTESITES_MOD' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Note
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Helyettesítés'
			,'1'
			,'1'
			,'teljesköru - minden szerepkör, esetleg csoporttagság alapján'
			);


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			,Note
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Megbízás'
			,'1'
			,'2'
			,'nem teljesköru - adott csoporttagsághoz tartozó kiválasztott szerepkörök'
			); 

	 
/* -------------------------------------------------------------------------------
KodCsoport: DOKUMENTUM_SZEREP
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'DOKUMENTUM_SZEREP' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Fodokumentum'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Dokumentum melléklet'
			,'1'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Segéddokumentum'
			,'1'
			,'3'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: ALAIRAS_MOD
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'ALAIRAS_MOD' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'E_EMB'
			,'Embedded elektronikus aláírás'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'E_DET'
			,'Detached elektronikus aláírás'
			,'1'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'M_ADM'
			,'Adminisztrált manuális aláírás'
			,'1'
			,'3'
			);

/* -------------------------------------------------------------------------------
KodCsoport: ALAIRAS_SZABALY_SZINT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'ALAIRAS_SZABALY_SZINT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Alapszabály'
			,'1'
			,'1'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Ügykör szabály'
			,'1'
			,'2'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Ügytípus szabály'
			,'1'
			,'3'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Eljárási szakasz szabály'
			,'1'
			,'3'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'4'
			,'Irattípus szabály'
			,'1'
			,'4'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'5'
			,'CTT szabály'
			,'1'
			,'5'
			);

/* -------------------------------------------------------------------------------
KodCsoport: ALAIRAS_SZABALY_TIPUS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'ALAIRAS_SZABALY_TIPUS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Kötelezo aláíró szabály'
			,'1'
			,'1'
			); 
 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Pótolható aláíró szabály'
			,'0'
			,'2'
			); 
 
		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Helyettesítheto aláíró szabály'
			,'0'
			,'3'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'04'
			,'Aláírás kizáró szabály'
			,'0'
			,'4'
			);

/* -------------------------------------------------------------------------------
KodCsoport: ALAIRO_SZEREP_JEL
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'ALAIRO_SZEREP_JEL' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'0'
			,'Aláírói szereptol független'
			,'0'
			,'1'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Aláírói szerephez kötött'
			,'0'
			,'2'
			); 

/* -------------------------------------------------------------------------------
KodCsoport: ALAIRT_DOKUMENTUM_ALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'ALAIRT_DOKUMENTUM_ALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Végleges'
			,'0'
			,'10'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Ismételt'
			,'0'
			,'20'
			);

/* -------------------------------------------------------------------------------
KodCsoport: IRATALAIRAS_ALLAPOT
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'IRATALAIRAS_ALLAPOT' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'1'
			,'Aláírandó'
			,'1'
			,'10'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'2'
			,'Aláírt'
			,'1'
			,'20'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'3'
			,'Visszautasított'
			,'1'
			,'30'
			);

/* -------------------------------------------------------------------------------
KodCsoport: TANUSITVANY_TAROLAS
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'TANUSITVANY_TAROLAS' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'PTAR'
			,'Központi publikus tanúsítványtár'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'BALE'
			,'Személyes eszköz'
			,'1'
			,'1'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'LTAR'
			,'Lokális tanúsítványtár'
			,'1'
			,'1'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'BTAR'
			,'Központi biztonságos tanúsítványtár'
			,'1'
			,'1'
			); 


/* -------------------------------------------------------------------------------
KodCsoport: UGYIRAT_JELLEG
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'UGYIRAT_JELLEG' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'01'
			,'Elektronikus'
			,'0'
			,'1'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'02'
			,'Papír'
			,'0'
			,'2'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'03'
			,'Vegyes'
			,'0'
			,'3'
			);

	 
/* -------------------------------------------------------------------------------
KodCsoport: CONTROLTYPE_SOURCE
------------------------------------------------------------------------------- */

	set @KODCSOPORT_ID = (select top 1 Id from KRT_KodCsoportok where Kod = 'CONTROLTYPE_SOURCE' and getdate() between ErvKezd and ErvVege)
	if @KODCSOPORT_ID is null
		RAISERROR('[65003]',16,1)	 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'CONTENTUM_DEMO_NEW.eUIControls.CustomTextBox;CONTENTUM_DEMO_NEW.eUIControls'
			,'Szövegmezo (CustomTextBox)'
			,'1'
			,'01'
			); 


		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'~/Component/RequiredTextBox.ascx'
			,'Kötelezo szövegmezo (RequiredTextBox)'
			,'1'
			,'02'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'System.Web.UI.WebControls.CheckBox;System.Web'
			,'Jelölonégyzet (CheckBox)'
			,'1'
			,'03'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'~/Component/CalendarControl.ascx'
			,'Naptár (CalendarControl)'
			,'1'
			,'04'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'~/Component/KodTarakDropDownList.ascx'
			,'Kódválasztó legördülo lista (KodTarakDropDownList)'
			,'1'
			,'05'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'~/Component/EditablePartnerTextBox.ascx'
			,'Partnerválasztó mezo, szerkesztheto (EditablePartnerTextBox)'
			,'1'
			,'06'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'CONTENTUM_DEMO_NEW.eUIControls.eDropDownList;CONTENTUM_DEMO_NEW.eUIControls'
			,'Legördülo lista (eDropDownList)'
			,'1'
			,'07'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'~/Component/RequiredNumberBox.ascx'
			,'Szám mezo (RequiredNumberBox)'
			,'1'
			,'08'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'System.Web.UI.WebControls.Label;System.Web'
			,'Címke (Label)'
			,'1'
			,'09'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'~/Component/AdoszamTextBox.ascx'
			,'Adószám mezo (AdoszamTextBox)'
			,'1'
			,'10'
			); 

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'~/Component/FelhasznaloCsoportTextBox.ascx'
			,'Felhasználócsoport-választó mezo (FelhasznaloCsoportTextBox)'
			,'1'
			,'11'
			);

		 insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			 @KODCSOPORT_ID
			,@Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'~/Component/CsoportTextBox.ascx'
			,'Csoportválasztó mezo (CsoportTextBox)'
			,'1'
			,'12'
			);

----------------------------------------------------------------------------------
-- Rendszerparameterek (Super)
----------------------------------------------------------------------------------

	if not exists (select 1 from KRT_Parameterek
		where Nev='SHORT_DATE_PATTERN'
		and Org=@SuperOrg_ID
		and getdate() between ErvKezd and ErvVege)
	begin
		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@SuperOrg_ID
			,'SHORT_DATE_PATTERN'
			,'yyyy.MM.dd.'
			,'1'
			,'A rendszerben használt dátumformátum beállítása.'
			); 
	END
	
	if not exists (select 1 from KRT_Parameterek
		where Nev='APPLICATION_VERSION'
		and Org=@SuperOrg_ID
		and getdate() between ErvKezd and ErvVege)
	begin
	insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@SuperOrg_ID
			,'APPLICATION_VERSION'
			,'v1.4.016'
			,'0'
			,'Az alkalmazás aktuális verziója (az utolsó patch verziója).'
			); 
	end		

----------------------------------------------------------------------------------
-- Rendszerparameterek (Org)
----------------------------------------------------------------------------------
		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'LOVLIST_MAX_ROW'
			,'250'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'HIBABEJELENTES_EMAIL_CIM'
			,'lamatsch.andras@axis.hu'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'RENDSZERFELUGYELET_EMAIL_CIM'
			,'rendszerfelugyelet@budapest.hu'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'EMAIL_SERVER_ADDRESS'
			,'192.168.1.220'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'IRATTARI_TETELSZAM_SORREND'
			,'0'
			,'1'
			,'A paraméter értékek lehetnek: 0 - ágazati jel / tételszám / megorzési ido; 1 - tételszám / ágazati jel / megorzési ido; 2 - tételszám / megorzési ido;'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'EMAIL_CSATOLMANY_DOKUMENTUMTARTIPUS'
			,'SharePoint'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'EMAIL_CSATOLMANY_DOKUMENTUMTARTERULET'
			,'eRecord'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'EMAIL_CSATOLMANY_DOKUMENTUMKONYVTAR'
			,'EmailCsatolmanyok'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'KULDEMENY_CSATOLMANY_DOKUMENTUMTARTIPUS'
			,'SharePoint'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'KULDEMENY_CSATOLMANY_DOKUMENTUMTARTERULET'
			,'eRecord'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'KULDEMENY_CSATOLMANY_DOKUMENTUMKONYVTAR'
			,'KuldemenyCsatolmanyok'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'UGYIRAT_CSATOLMANY_DOKUMENTUMTARTIPUS'
			,'SharePoint'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'UGYIRAT_CSATOLMANY_DOKUMENTUMTARTERULET'
			,'eRecord'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'UGYIRAT_CSATOLMANY_DOKUMENTUMKONYVTAR'
			,'UgyiratCsatolmanyok'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS'
			,'SharePoint'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'IRAT_CSATOLMANY_DOKUMENTUMTARTERULET'
			,'eRecord'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'IRAT_CSATOLMANY_DOKUMENTUMKONYVTAR'
			,'IratCsatolmanyok'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'KodCsoportokCache_RefreshPeriod_Minute'
			,'600'
			,'1'
			,'A kódcsoport-cache frissítési gyakorisága percben. (Ekkora idoközönként dobja ki a cache-bol a kódcsoportokat)'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'CsoportNevekCache_RefreshPeriod_Minute'
			,'600'
			,'1'
			,'A csoportnevek-cache frissítési gyakorisága percben. (Ekkora idoközönként dobja ki a cache-bol a csoportneveket, és kéri le újra az adatbázisból)'
			); 


		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'CsoportNevekCache_MaxRowsFromDBToCache'
			,'10000'
			,'1'
			,'A csoportnevek-cache feltöltésekor az adatbázisból maximálisan lekérheto sorok száma.'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'AllMetaCache_RefreshPeriod_Minute'
			,'600'
			,'1'
			,'Az objektumtípushoz rendelt metaadat-cache frissítési gyakorisága percben. (Ekkora idoközönként dobja ki a cache-bol a metaadatokat)'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'PKI_INTEGRACIO'
			,'Nem'
			,'1'
			,'Jelzi, hogy a PKI-szolgáltatások igénybe vehetok-e (érték: Igen) vagy sem (érték:Nem)'
			);

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'PIR_INTERFACE_ENABLED'
			,'0'
			,'1'
			,'Az Org-specifikus PirEdokInterface (számlakezelés) használatának engedélyezése vagy letiltása. Érték(1 - igen)'
			);  

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'MAX_ITEM_NUMBER'
			,'300'
			,'1'
			,'Egyszerre feldolgozható maximális tételszám'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'IRATTAROZAS_JOVAHAGYAS_ENABLED'
			,'0'
			,'1'
			,'Ezzel lehet beállítani, hogy az irattározás folyamatában be legyen-e kapcsolva a jováhagyás vagy nem. Érték(1 - igen)'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'MIGRALAS_EVE'
			,'2008'
			,'1'
			,'Ez a paraméterrel állítható be a migrálás éve. A régebbi adatok a migrált adatbázisban találahtóak.'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'VONALKOD_ELLENORZO_VEKTOR'
			,'973197319731'
			,'1'
			,'A vonalkód ellenorzo összeg számolásához használt vektor.'
			);


		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'ADOSZAM_ELLENORZO_VEKTOR'
			,'9731973'
			,'1'
			,'Az adószám ellenorzo összeg számolásához használt vektor.'
			);

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'eDocumentWebServiceUrl'
			,'http://localhost:100/eDocumentWebService'
			,'1'
			,'Az eDocument webservcie url-je (Tárolt eljárásból indított webservice híváshoz szükséges)'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'SKONTRO_JOVAHAGYAS_ENABLED'
			,'1'
			,'1'
			,'Ezzel lehet beállítani, hogy a skontróba heyezés folyamatában be legyen-e kapcsolva a jováhagyás vagy nem. Érték(1 - igen)'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'TELJESSEGELLENORZES_SKONTROELOTT_ENABLED'
			,'1'
			,'1'
			,'Beállítható, hogy ha az ügyiratra sikertelen a teljességellenorzés, akkor ne lehessen skontróba adni az ügyiratot. Érték: 1 - Igen'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'TELJESSEGELLENORZES_IRATTARBAADASELOTT_ENABLED'
			,'1'
			,'1'
			,'Beállítható, hogy ha az ügyiratra sikertelen a teljességellenorzés, akkor ne lehessen irattárba adni az ügyiratot. Érték: 1 - Igen'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
--			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'DEFAULT_SZEREPKOR'
--			,'FPH_LEKERDEZO'
			,'1'
			,'Minden újonnan létrehozott felhasználó hozzá lesz adva ehhez a szerepkörhöz!'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'REGEXP_UGYIRAT_IRATTARIHELY'
			,''
			,'1'
			,'Az ügyirat irattári helyére alkalmazható reguláris kifejezés (a "polc" leírásának formátuma)'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'HATOSAGI_STATISZTIKA_ONKORMANYZAT'
			,'Fovárosi Önkormányzat'
			,'1'
			,'A hatósági statisztikák Excel-tábláiban kiválasztható önkormányzati székhely (vagy esetleg székhelyek, pontosvesszovel elválasztva). Alapértelmezés. Ha bizonyos években ettol eltéro megnevezésre van szükség, akkor létre kell hozni egy megfelelo rendszerparamétert HATOSAGI_STATISZTIKA_ONKORMANYZATxxxx néven, ahol xxxx helyére az adott évszám kerül.'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'ITSZ_ELVALASZTO_JEL'
			,' / '
			,'1'
			,'Az irattári tételszám képzésében használt elválasztó karakterlánc (pl. '' / '' vagy ''-'': C / 271 / 0 ill. C-271-0)'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'AJAXOSReallyPartnerkereso_KellKuldemeny'
			,'Igen'
			,'1'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'CIM_SZABAD_SZOVEG_ENABLED'
			,'0'
			,'1'
			,'Az érkezteto képernyon a cím szabad formátumú (nem strukturált) bevitelét engedélyezi/tiltja. Érték: 1-Igen (tetszoleges formátumú cím beviheto) 0-Nem (a beviteli mezo tiltott állapotú: csak partner vagy címkiválasztással, ill. strukturáltan viheto fel cím)'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'BEKULDO_PARTNER_CIM_NEM_TORZSBOL_ENABLED'
			,'1'
			,'1'
			,'Az érkezteto képernyon a partner és cím szabad bevitelét engedélyezi/tiltja. Érték: 1-Igen (tetszoleges partnernév és cím beviheto) 0-Nem (beküldo partner és cím csak a törzsbol választható)'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'SCROLL_PANEL_HEIGHT'
			,'400'
			,'1'
			,'A scrollozás bekapcsolásakor a listák magassága pixelben'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'FELADATOSSZESITO_FRISSITES_MIN_PERC'
			,'10'
			,'1'
			,'Érték: egész szám. Az a minimális idotartam percben megadva, melynél gyakrabban a feladatösszesítohöz kapcsolódó adatpiac frissítését adott felhasználóra (egy adott szervezetében) nem engedélyezzük. Ha értéke 0 vagy negatív, a frissítés minden kérésre le fog futni!'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'FELADATOSSZESITO_JOB_ADATFRISSITES'
			,'EDOKFeladatok'
			,'1'
			,'Érték: karakterlánc. A feladatösszesítohöz kapcsolódó adatpiac adatfrissítését végzo job neve.'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'UI_SETUP_HIDE_DISABLED_ICONS'
			,'0'
			,'1'
			,'Érték: 1-Igen. Ha értéke 1, a listák fejlécén azon ikonok, melyek letiltott, de látható állapotban vannak, el lesznek rejtve. Ha értéke 0, a letiltott, de látható ikonok halványan megjelennek.'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'IRAT_MINOSITES_BIZALMAS'
			,'10'
			,'1'
			,'Beállíthatók azok az irat minosítés értékek, melyek bizalmasnak minosülnek, így pl. csatolmányaikat csak orzojük és annak vezetoje láthatja. Érték: IRAT_MINOSITES kódcsoport kódtárérték, vagy kódtárértékek felsorolva, vesszovel elválasztva. Pl.: 10,20'
			); 

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'SEARCH_SETUP_LEGUTOBB_KEZELT_ENABLED'
			,'0'
			,'1'
			,'Érték: 1-Igen. Az alapértelmezett szurések (Legutóbb kezelt...) használatát engedélyezi vagy tiltja. Ha értéke 1, egyes listákon (ügyiratok, iratok, iratpéldányok, küldemények) csak a legutóbbi bejelentkezést megelozo 3. napon túl módosított tételek jelennek meg. Ha értéke 0, alapállapotban az összes tétel megjelenik.'
			);

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'FELADAT_KEZELES_DISABLED'
			,'0'
			,'1'
			,'Értékek: Igen: 1, i, igen, true; Nem: minden más érték. A paraméterrel a feladatkezelés tiltható le a rendszerben.'
			);

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'DEFAULT_INTEZESI_IDO_IDOEGYSEG'
			,'-1440'
			,'1'
			,'Az alapértelmezett (törvény szerinti) ügyintézési határido számításához használt idoegység (nap, óra stb.) kódja, az IDOEGYSEG kódcsoport alapján. Értéke az idoegység  hossza percekben kifejezve. Pl. "1440" jelentése nap, 60 jelentése óra. Ha negatív, munkaidore vonatkozik. Pl. "-1440" jelentése munkanap.'
			);

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'DEFAULT_INTEZESI_IDO'
			,'22'
			,'1'
			,'Az alapértelmezett (törvény szerinti) ügyintézési határido számításához használt idoartam a DEFAULT_INTEZESI_IDO_IDOEGYSEG paraméterben megadott idoegységben (nap, óra stb.) kifejezve. Pl. ha DEFAULT_INTEZESI_IDO_IDOEGYSEG értéke -1440 (munkanap) és a DEFAULT_INTEZESI_IDO értéke 22, akkor a határido 22 munkanap az adott dátumtól kezdve.'
			);


		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'KIKERES_MAS_NEVRE_ENABLED'
			,'0'
			,'1'
			,'Értékek: Igen: 1, i, igen, true; Nem: minden más érték. Ha értéke 1, a kikérésnél mód van más személy nevének megadására is, nem beállításakor csak saját névre lehet kölcsönözni.'
			);

		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'KIKERES_ATMENETI_IRATTARBOL_BETEKINTESRE_ENABLED'
			,'0'
			,'1'
			,'Értékek: Igen: 1, i, igen, true; Nem: minden más érték. Ha értéke 1, lehet átmeneti irattárból is csak betekintésre kikérni, egyébként csak ügyintézésre.'
			);
			
			insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'KOLCSONZES_JOVAHAGYAS_BY_UGYINTEZO_DISABLED'
			,'0'
			,'1'
			,'Értékek: Igen: 1, i, igen, true; Nem: minden más érték. Ha az ügyirat ügyintézoje kérte ki az iratot, jóváhagyhatja-e a kölcsönzést.'
			); 

			insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'ATMENETI_IRATTAR_KEZELESJOG_TIPUS'
			,'1'
			,'1'
			,'Az átmeneti irattár kezelése (irattárba vétel, kiadás) 0 esetén a szervezethez rendelt bármely irattárra; 1 esetén csak a saját szervezettel megegyezo irattárra megengedett;'
			); 

			insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
			,@Org
			,'MUNKANAPLO_IRAT_UGYINTEZO_ENABLED'
			,'0'
			,'1'
			,'Értékek: Igen: 1, i, igen, true; Nem: minden más érték. Ha értéke 1, a munkanaplóban azon tételek is megjelennek, ahol a felhasználó az ügyiratnak nem, de valamely alszámának ügyintézoje. A megjelenített események ilyenkor csak az adott iratra vagy példányára vonatkozók lehetnek.'
			);

			insert into KRT_Parameterek(
				Tranz_id
				,Stat_id
				,Org
				,Nev
				,Ertek
				,Karbantarthato
				,Note
				) values (
				 @Tranz_id
				,@STAT_ID_ALLAPOT_KELETKEZIK
				,@Org
				,'FIZIKAI_UGYIRAT_KOLCSONZES_BETEKINTESRE_DISABLED'
				,'0'
				,'1'
				,'Értékek: Igen: 1, i, igen, true; Nem: minden más érték. Ha értéke igen, a fizikai ügyirat a központi irattárból, csak ügyintézésre kölcsönözheto ki.'
				);
				
			insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
		    ,@Org
			,'CSOPORT_NEV_WITH_EMAIL_DISABLED'
			,'0'
			,'1'
			,'Értékek: Igen: 1, i, igen, true. Nem: minden más érték. Ha értéke igen, akkor a felhasználóhoz létrejövo csoport nevébe nem kerül bele a felhasználó e-mail címe.'
			);
			
			insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
		    ,@Org
			,'PAGE_VIEW_ENABLED'
			,'0'
			,'1'
			,'Nézetek engedélyezése. Értékek: Igen: 1, i, igen, true; Nem: minden más érték.'
			); 

			insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
		    ,@Org
			,'VISSZAKULDES_ENABLED'
			,'1'
			,'1'
			,'Értékek: Igen: 1, i, igen, true. Nem: minden más érték. Az iratkezelési objektumok átvétel helyetti visszaküldésének engedélyezése vagy letiltása. Ha értéke 1, a Visszaküldés ikon megjelenik az iratkezelési objektumok megfelelo listáin, egyébként nem látható.'
			); 

			insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @Tranz_id
			,@STAT_ID_ALLAPOT_KELETKEZIK
		    ,@Org
			,'VISSZAKULDES_IRATTARBOL_ENABLED'
			,'1'
			,'1'
			,'Értékek: Igen: 1, i, igen, true. Nem: minden más érték. Az ügyiratok irattárba vétele helyetti visszaküldésének engedélyezése vagy letiltása. Ha értéke 1, a Visszaküldés ikon megjelenik az iratkezelési objektumok megfelelo listáin, egyébként nem látható.'
			); 			
			 
----------------------------------------------------------------------------------
-- History Log
----------------------------------------------------------------------------------
		declare @table_ColumnNames varchar(4000)
		declare @sqlCommand varchar(MAX);

/* ----------------------------------------------------------------------------------
-- KRT_KodTarak
---------------------------------------------------------------------------------- */
		set @table_ColumnNames = dbo.fn_GetTableColumnNamesConcatenated('KRT_KodTarak')
		if (@table_ColumnNames is not null)
		BEGIN
			set @sqlCommand = 'insert into KRT_KodTarakHistory (HistoryMuvelet_Id, HistoryVegrehajto_Id, HistoryVegrehajtasIdo, '
				+ @table_ColumnNames
				+ ') select 0, ''' + convert(varchar(36), @ExecutorUserId) + ''', getdate(),'
				+ @table_ColumnNames
				+ ' from KRT_KodTarak where Id in (select Id from KRT_KodTarak where Org=''' + CAST(@Org as NVarChar(40)) + ''')'
			exec (@sqlCommand)
		END

----------------------------------------------------------------------------------
-- KRT_Parameterek
----------------------------------------------------------------------------------
		set @table_ColumnNames = dbo.fn_GetTableColumnNamesConcatenated('KRT_Parameterek')
		if (@table_ColumnNames is not null)
		BEGIN
			set @sqlCommand = 'insert into KRT_ParameterekHistory (HistoryMuvelet_Id, HistoryVegrehajto_Id, HistoryVegrehajtasIdo, '
				+ @table_ColumnNames
				+ ') select 0, ''' + convert(varchar(36), @ExecutorUserId) + ''', getdate(),'
				+ @table_ColumnNames
				+ ' from KRT_Parameterek where Id in (select Id from KRT_Parameterek where Org=''' + CAST(@Org as NVarChar(40)) + ''')'
			exec (@sqlCommand)
		END

END TRY
BEGIN CATCH
	
	-- hibakezelés:
	EXEC usp_HandleException
 
END CATCH

end


GO
