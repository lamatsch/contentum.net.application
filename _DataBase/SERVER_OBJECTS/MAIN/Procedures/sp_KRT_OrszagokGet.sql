IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_OrszagokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_OrszagokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_OrszagokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_OrszagokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_OrszagokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Orszagok.Id,
	   KRT_Orszagok.Org,
	   KRT_Orszagok.Kod,
	   KRT_Orszagok.Nev,
	   KRT_Orszagok.Viszonylatkod,
	   KRT_Orszagok.Ver,
	   KRT_Orszagok.Note,
	   KRT_Orszagok.Stat_id,
	   KRT_Orszagok.ErvKezd,
	   KRT_Orszagok.ErvVege,
	   KRT_Orszagok.Letrehozo_id,
	   KRT_Orszagok.LetrehozasIdo,
	   KRT_Orszagok.Modosito_id,
	   KRT_Orszagok.ModositasIdo,
	   KRT_Orszagok.Zarolo_id,
	   KRT_Orszagok.ZarolasIdo,
	   KRT_Orszagok.Tranz_id,
	   KRT_Orszagok.UIAccessLog_id
	   from 
		 KRT_Orszagok as KRT_Orszagok 
	   where
		 KRT_Orszagok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
