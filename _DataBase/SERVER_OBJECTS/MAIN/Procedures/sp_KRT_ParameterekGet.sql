IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_ParameterekGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_ParameterekGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_ParameterekGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_ParameterekGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_ParameterekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Parameterek.Id,
	   KRT_Parameterek.Org,
	   KRT_Parameterek.Felhasznalo_id,
	   KRT_Parameterek.Nev,
	   KRT_Parameterek.Ertek,
	   KRT_Parameterek.Karbantarthato,
	   KRT_Parameterek.Ver,
	   KRT_Parameterek.Note,
	   KRT_Parameterek.Stat_id,
	   KRT_Parameterek.ErvKezd,
	   KRT_Parameterek.ErvVege,
	   KRT_Parameterek.Letrehozo_id,
	   KRT_Parameterek.LetrehozasIdo,
	   KRT_Parameterek.Modosito_id,
	   KRT_Parameterek.ModositasIdo,
	   KRT_Parameterek.Zarolo_id,
	   KRT_Parameterek.ZarolasIdo,
	   KRT_Parameterek.Tranz_id,
	   KRT_Parameterek.UIAccessLog_id
	   from 
		 KRT_Parameterek as KRT_Parameterek 
	   where
		 KRT_Parameterek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
