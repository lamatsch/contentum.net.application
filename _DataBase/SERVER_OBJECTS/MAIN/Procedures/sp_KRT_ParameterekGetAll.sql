IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_ParameterekGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_ParameterekGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_ParameterekGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_ParameterekGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_ParameterekGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Parameterek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @pageNumber		int = 0,
  @pageSize		int = -1,
  @SelectedRowId	uniqueidentifier = null,
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	if @ExecutorUserId = '00000000-0000-0000-0000-000000000000'
	begin
		-- 'szuper' (kA¶zA¶s) paramA©terek
		SET @Org = '00000000-0000-0000-0000-000000000000'
	end
	else
	begin
		SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
		if (@Org is null)
		begin
			RAISERROR('[50202]',16,1)
		end
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 	DECLARE @firstRow INT
	DECLARE @lastRow INT
   
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = 1000
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end

	/************************************************************
	* SzL±rt adatokhoz rendezA©s A©s sorszA?m A¶sszeA?llA­tA?sa			*
	************************************************************/
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   KRT_Parameterek.Id into #result
		from KRT_Parameterek as KRT_Parameterek
    Where KRT_Parameterek.Org=@Org'

   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end

	if (@SelectedRowId is not null)
	begin
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id = @SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	end

	set @sqlcmd = @sqlcmd + N' 
	if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
	BEGIN
		select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
		set @firstRow = (@pageNumber - 1) * @pageSize + 1;
		set @lastRow = @pageNumber*@pageSize;
		select @pageNumber = @pageNumber - 1
	END ;'

	/************************************************************
	* TA©nyleges select											*
	************************************************************/
	SET @sqlcmd = @sqlcmd + 
  'select 
		#result.RowNumber,
  	   KRT_Parameterek.Id,
	   KRT_Parameterek.Org,
	   KRT_Parameterek.Felhasznalo_id,
	   KRT_Parameterek.Nev,
	   KRT_Parameterek.Ertek,
	   KRT_Parameterek.Karbantarthato,
	   KRT_Parameterek.Ver,
	   KRT_Parameterek.Note,
	   KRT_Parameterek.Stat_id,
	   KRT_Parameterek.ErvKezd,
	   KRT_Parameterek.ErvVege,
	   KRT_Parameterek.Letrehozo_id,
	   KRT_Parameterek.LetrehozasIdo,
	   KRT_Parameterek.Modosito_id,
	   KRT_Parameterek.ModositasIdo,
	   KRT_Parameterek.Zarolo_id,
	   KRT_Parameterek.ZarolasIdo,
	   KRT_Parameterek.Tranz_id,
	   KRT_Parameterek.UIAccessLog_id  
   from 
     KRT_Parameterek as KRT_Parameterek      
     	inner join #result on #result.Id = KRT_Parameterek.Id
    where RowNumber between @firstRow and @lastRow
	ORDER BY #result.RowNumber;'

	-- talA?latok szA?ma A©s oldalszA?m
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';
 	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier, @Org uniqueidentifier',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId, @Org = @Org;  
  
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
