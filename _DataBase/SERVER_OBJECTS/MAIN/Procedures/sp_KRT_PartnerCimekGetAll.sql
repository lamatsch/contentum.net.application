IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerCimekGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_PartnerCimekGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerCimekGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_PartnerCimekGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_PartnerCimekGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_PartnerCimek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_PartnerCimek.Id,
	   KRT_PartnerCimek.Partner_id,
	   KRT_PartnerCimek.Cim_Id,
	   KRT_PartnerCimek.UtolsoHasznalatSiker,
	   KRT_PartnerCimek.UtolsoHasznalatiIdo,
	   KRT_PartnerCimek.eMailKuldes,
	   KRT_PartnerCimek.Fajta,
	   KRT_PartnerCimek.Ver,
	   KRT_PartnerCimek.Note,
	   KRT_PartnerCimek.Stat_id,
	   KRT_PartnerCimek.ErvKezd,
	   KRT_PartnerCimek.ErvVege,
	   KRT_PartnerCimek.Letrehozo_id,
	   KRT_PartnerCimek.LetrehozasIdo,
	   KRT_PartnerCimek.Modosito_id,
	   KRT_PartnerCimek.ModositasIdo,
	   KRT_PartnerCimek.Zarolo_id,
	   KRT_PartnerCimek.ZarolasIdo,
	   KRT_PartnerCimek.Tranz_id,
	   KRT_PartnerCimek.UIAccessLog_id  
   from 
     KRT_PartnerCimek as KRT_PartnerCimek      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
