IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerKapcsolatokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_PartnerKapcsolatokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerKapcsolatokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_PartnerKapcsolatokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_PartnerKapcsolatokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_PartnerKapcsolatok.Id,
	   KRT_PartnerKapcsolatok.Tipus,
	   KRT_PartnerKapcsolatok.Partner_id,
	   KRT_PartnerKapcsolatok.Partner_id_kapcsolt,
	   KRT_PartnerKapcsolatok.Ver,
	   KRT_PartnerKapcsolatok.Note,
	   KRT_PartnerKapcsolatok.Stat_id,
	   KRT_PartnerKapcsolatok.ErvKezd,
	   KRT_PartnerKapcsolatok.ErvVege,
	   KRT_PartnerKapcsolatok.Letrehozo_id,
	   KRT_PartnerKapcsolatok.LetrehozasIdo,
	   KRT_PartnerKapcsolatok.Modosito_id,
	   KRT_PartnerKapcsolatok.ModositasIdo,
	   KRT_PartnerKapcsolatok.Zarolo_id,
	   KRT_PartnerKapcsolatok.ZarolasIdo,
	   KRT_PartnerKapcsolatok.Tranz_id,
	   KRT_PartnerKapcsolatok.UIAccessLog_id
	   from 
		 KRT_PartnerKapcsolatok as KRT_PartnerKapcsolatok 
	   where
		 KRT_PartnerKapcsolatok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
