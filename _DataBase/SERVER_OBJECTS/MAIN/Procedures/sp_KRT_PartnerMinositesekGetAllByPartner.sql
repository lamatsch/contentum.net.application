IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerMinositesekGetAllByPartner]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_PartnerMinositesekGetAllByPartner]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerMinositesekGetAllByPartner]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_PartnerMinositesekGetAllByPartner] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_PartnerMinositesekGetAllByPartner]
  @Partner_Id UniqueIdentifier ,
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_PartnerMinositesek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_PartnerMinositesek.Id,
	   KRT_PartnerMinositesek.ALLAPOT,
	   KRT_Kodtarak1.Nev as ALLAPOT_Nev,
	   KRT_PartnerMinositesek.Partner_id,
	   KRT_PartnerMinositesek.Felhasznalo_id_kero,
       KRT_Felhasznalok1.Nev as FelhasznaloKeroNev,
	   KRT_PartnerMinositesek.KertMinosites,
       KRT_Kodtarak2.Nev as KertMinositesNev,
	   KRT_PartnerMinositesek.KertKezdDat,
	   KRT_PartnerMinositesek.KertVegeDat,
	   KRT_PartnerMinositesek.KerelemAzonosito,
	   KRT_PartnerMinositesek.KerelemBeadIdo,
	   KRT_PartnerMinositesek.KerelemDontesIdo,
	   KRT_PartnerMinositesek.Felhasznalo_id_donto,
       KRT_Felhasznalok2.Nev as FelhasznaloDontoNev,
	   KRT_PartnerMinositesek.DontesAzonosito,
	   KRT_PartnerMinositesek.Minosites,
	   KRT_PartnerMinositesek.MinositesKezdDat,
	   KRT_PartnerMinositesek.MinositesVegDat,
	   KRT_PartnerMinositesek.SztornirozasDat,
	   KRT_PartnerMinositesek.Ver,
	   KRT_PartnerMinositesek.Note,
	   KRT_PartnerMinositesek.Stat_id,
	   KRT_PartnerMinositesek.ErvKezd,
	   KRT_PartnerMinositesek.ErvVege,
	   KRT_PartnerMinositesek.Letrehozo_id,
	   KRT_PartnerMinositesek.LetrehozasIdo,
	   KRT_PartnerMinositesek.Modosito_id,
	   KRT_PartnerMinositesek.ModositasIdo,
	   KRT_PartnerMinositesek.Zarolo_id,
	   KRT_PartnerMinositesek.ZarolasIdo,
	   KRT_PartnerMinositesek.Tranz_id,
	   KRT_PartnerMinositesek.UIAccessLog_id  
   from 
     KRT_PartnerMinositesek as KRT_PartnerMinositesek
	 left join KRT_KodCsoportok as KRT_KodCsoportok1 on KRT_KodCsoportok1.Kod=''PMINOSITES_ALLAPOT''
     left join KRT_KodTarak as KRT_Kodtarak1 on KRT_PartnerMinositesek.ALLAPOT = KRT_KodTarak1.Kod and KRT_KodCsoportok1.Id = KRT_KodTarak1.KodCsoport_Id
		and KRT_KodTarak1.Org=''' + cast(@Org as NVarChar(40)) + '''
	 left join KRT_KodCsoportok as KRT_KodCsoportok2 on KRT_KodCsoportok2.Kod=''MINOSITESI_JELOLES''
     left join KRT_KodTarak as KRT_Kodtarak2 on KRT_PartnerMinositesek.KertMinosites = KRT_KodTarak2.Kod and KRT_KodCsoportok2.Id = KRT_KodTarak2.KodCsoport_Id
		and KRT_KodTarak2.Org=''' + cast(@Org as NVarChar(40)) + '''
     left join KRT_Felhasznalok as KRT_Felhasznalok1 on KRT_PartnerMinositesek.Felhasznalo_id_kero = KRT_Felhasznalok1.Id
     left join KRT_Felhasznalok as KRT_Felhasznalok2 on KRT_PartnerMinositesek.Felhasznalo_id_donto = KRT_Felhasznalok2.Id
   WHERE 
     KRT_PartnerMinositesek.Partner_id = ''' + CAST(@Partner_id as Nvarchar(40)) + '''     
   '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
