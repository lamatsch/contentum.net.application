IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerMinositesekHistoryGetRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_PartnerMinositesekHistoryGetRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerMinositesekHistoryGetRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_PartnerMinositesekHistoryGetRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_PartnerMinositesekHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin
select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and KRT_PartnerMinositesekHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ALLAPOT' as ColumnName,               
               cast(Old.ALLAPOT as nvarchar(99)) as OldValue,
               cast(New.ALLAPOT as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ALLAPOT != New.ALLAPOT 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(Old.Partner_id) as OldValue,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(New.Partner_id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Partner_id != New.Partner_id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Partnerek FTOld on FTOld.Id = Old.Partner_id and FTOld.Ver = Old.Ver
         left join KRT_Partnerek FTNew on FTNew.Id = New.Partner_id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felhasznalo_id_kero' as ColumnName,
/*FK*/           dbo.fn_GetKRT_FelhasznalokAzonosito(Old.Felhasznalo_id_kero) as OldValue,
/*FK*/           dbo.fn_GetKRT_FelhasznalokAzonosito(New.Felhasznalo_id_kero) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Felhasznalo_id_kero != New.Felhasznalo_id_kero 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Felhasznalok FTOld on FTOld.Id = Old.Felhasznalo_id_kero and FTOld.Ver = Old.Ver
         left join KRT_Felhasznalok FTNew on FTNew.Id = New.Felhasznalo_id_kero and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KertMinosites' as ColumnName,               
               cast(Old.KertMinosites as nvarchar(99)) as OldValue,
               cast(New.KertMinosites as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KertMinosites != New.KertMinosites 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KertKezdDat' as ColumnName,               
               cast(Old.KertKezdDat as nvarchar(99)) as OldValue,
               cast(New.KertKezdDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KertKezdDat != New.KertKezdDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KertVegeDat' as ColumnName,               
               cast(Old.KertVegeDat as nvarchar(99)) as OldValue,
               cast(New.KertVegeDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KertVegeDat != New.KertVegeDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KerelemAzonosito' as ColumnName,               
               cast(Old.KerelemAzonosito as nvarchar(99)) as OldValue,
               cast(New.KerelemAzonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KerelemAzonosito != New.KerelemAzonosito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KerelemBeadIdo' as ColumnName,               
               cast(Old.KerelemBeadIdo as nvarchar(99)) as OldValue,
               cast(New.KerelemBeadIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KerelemBeadIdo != New.KerelemBeadIdo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KerelemDontesIdo' as ColumnName,               
               cast(Old.KerelemDontesIdo as nvarchar(99)) as OldValue,
               cast(New.KerelemDontesIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KerelemDontesIdo != New.KerelemDontesIdo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felhasznalo_id_donto' as ColumnName,
/*FK*/           dbo.fn_GetKRT_FelhasznalokAzonosito(Old.Felhasznalo_id_donto) as OldValue,
/*FK*/           dbo.fn_GetKRT_FelhasznalokAzonosito(New.Felhasznalo_id_donto) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Felhasznalo_id_donto != New.Felhasznalo_id_donto 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Felhasznalok FTOld on FTOld.Id = Old.Felhasznalo_id_donto and FTOld.Ver = Old.Ver
         left join KRT_Felhasznalok FTNew on FTNew.Id = New.Felhasznalo_id_donto and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DontesAzonosito' as ColumnName,               
               cast(Old.DontesAzonosito as nvarchar(99)) as OldValue,
               cast(New.DontesAzonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.DontesAzonosito != New.DontesAzonosito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Minosites' as ColumnName,               
               cast(Old.Minosites as nvarchar(99)) as OldValue,
               cast(New.Minosites as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Minosites != New.Minosites 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MinositesKezdDat' as ColumnName,               
               cast(Old.MinositesKezdDat as nvarchar(99)) as OldValue,
               cast(New.MinositesKezdDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MinositesKezdDat != New.MinositesKezdDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MinositesVegDat' as ColumnName,               
               cast(Old.MinositesVegDat as nvarchar(99)) as OldValue,
               cast(New.MinositesVegDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MinositesVegDat != New.MinositesVegDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SztornirozasDat' as ColumnName,               
               cast(Old.SztornirozasDat as nvarchar(99)) as OldValue,
               cast(New.SztornirozasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_PartnerMinositesekHistory Old
         inner join KRT_PartnerMinositesekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SztornirozasDat != New.SztornirozasDat 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end


GO
