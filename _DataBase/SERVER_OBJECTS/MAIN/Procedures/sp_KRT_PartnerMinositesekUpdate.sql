IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerMinositesekUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_PartnerMinositesekUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerMinositesekUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_PartnerMinositesekUpdate] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_PartnerMinositesekUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @ALLAPOT     char(1)  = null,         
             @Partner_id     uniqueidentifier  = null,         
             @Felhasznalo_id_kero     uniqueidentifier  = null,         
             @KertMinosites     nvarchar(64)  = null,         
             @KertKezdDat     datetime  = null,         
             @KertVegeDat     datetime  = null,         
             @KerelemAzonosito     Nvarchar(100)  = null,         
             @KerelemBeadIdo     datetime  = null,         
             @KerelemDontesIdo     datetime  = null,         
             @Felhasznalo_id_donto     uniqueidentifier  = null,         
             @DontesAzonosito     Nvarchar(100)  = null,         
             @Minosites     Nvarchar(2)  = null,         
             @MinositesKezdDat     datetime  = null,         
             @MinositesVegDat     datetime  = null,         
             @SztornirozasDat     datetime  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_ALLAPOT     char(1)         
     DECLARE @Act_Partner_id     uniqueidentifier         
     DECLARE @Act_Felhasznalo_id_kero     uniqueidentifier         
     DECLARE @Act_KertMinosites     nvarchar(64)         
     DECLARE @Act_KertKezdDat     datetime         
     DECLARE @Act_KertVegeDat     datetime         
     DECLARE @Act_KerelemAzonosito     Nvarchar(100)         
     DECLARE @Act_KerelemBeadIdo     datetime         
     DECLARE @Act_KerelemDontesIdo     datetime         
     DECLARE @Act_Felhasznalo_id_donto     uniqueidentifier         
     DECLARE @Act_DontesAzonosito     Nvarchar(100)         
     DECLARE @Act_Minosites     Nvarchar(2)         
     DECLARE @Act_MinositesKezdDat     datetime         
     DECLARE @Act_MinositesVegDat     datetime         
     DECLARE @Act_SztornirozasDat     datetime         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_ALLAPOT = ALLAPOT,
     @Act_Partner_id = Partner_id,
     @Act_Felhasznalo_id_kero = Felhasznalo_id_kero,
     @Act_KertMinosites = KertMinosites,
     @Act_KertKezdDat = KertKezdDat,
     @Act_KertVegeDat = KertVegeDat,
     @Act_KerelemAzonosito = KerelemAzonosito,
     @Act_KerelemBeadIdo = KerelemBeadIdo,
     @Act_KerelemDontesIdo = KerelemDontesIdo,
     @Act_Felhasznalo_id_donto = Felhasznalo_id_donto,
     @Act_DontesAzonosito = DontesAzonosito,
     @Act_Minosites = Minosites,
     @Act_MinositesKezdDat = MinositesKezdDat,
     @Act_MinositesVegDat = MinositesVegDat,
     @Act_SztornirozasDat = SztornirozasDat,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from KRT_PartnerMinositesek
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/ALLAPOT')=1
         SET @Act_ALLAPOT = @ALLAPOT
   IF @UpdatedColumns.exist('/root/Partner_id')=1
         SET @Act_Partner_id = @Partner_id
   IF @UpdatedColumns.exist('/root/Felhasznalo_id_kero')=1
         SET @Act_Felhasznalo_id_kero = @Felhasznalo_id_kero
   IF @UpdatedColumns.exist('/root/KertMinosites')=1
         SET @Act_KertMinosites = @KertMinosites
   IF @UpdatedColumns.exist('/root/KertKezdDat')=1
         SET @Act_KertKezdDat = @KertKezdDat
   IF @UpdatedColumns.exist('/root/KertVegeDat')=1
         SET @Act_KertVegeDat = @KertVegeDat
   IF @UpdatedColumns.exist('/root/KerelemAzonosito')=1
         SET @Act_KerelemAzonosito = @KerelemAzonosito
   IF @UpdatedColumns.exist('/root/KerelemBeadIdo')=1
         SET @Act_KerelemBeadIdo = @KerelemBeadIdo
   IF @UpdatedColumns.exist('/root/KerelemDontesIdo')=1
         SET @Act_KerelemDontesIdo = @KerelemDontesIdo
   IF @UpdatedColumns.exist('/root/Felhasznalo_id_donto')=1
         SET @Act_Felhasznalo_id_donto = @Felhasznalo_id_donto
   IF @UpdatedColumns.exist('/root/DontesAzonosito')=1
         SET @Act_DontesAzonosito = @DontesAzonosito
   IF @UpdatedColumns.exist('/root/Minosites')=1
         SET @Act_Minosites = @Minosites
   IF @UpdatedColumns.exist('/root/MinositesKezdDat')=1
         SET @Act_MinositesKezdDat = @MinositesKezdDat
   IF @UpdatedColumns.exist('/root/MinositesVegDat')=1
         SET @Act_MinositesVegDat = @MinositesVegDat
   IF @UpdatedColumns.exist('/root/SztornirozasDat')=1
         SET @Act_SztornirozasDat = @SztornirozasDat
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE KRT_PartnerMinositesek
SET
     ALLAPOT = @Act_ALLAPOT,
     Partner_id = @Act_Partner_id,
     Felhasznalo_id_kero = @Act_Felhasznalo_id_kero,
     KertMinosites = @Act_KertMinosites,
     KertKezdDat = @Act_KertKezdDat,
     KertVegeDat = @Act_KertVegeDat,
     KerelemAzonosito = @Act_KerelemAzonosito,
     KerelemBeadIdo = @Act_KerelemBeadIdo,
     KerelemDontesIdo = @Act_KerelemDontesIdo,
     Felhasznalo_id_donto = @Act_Felhasznalo_id_donto,
     DontesAzonosito = @Act_DontesAzonosito,
     Minosites = @Act_Minosites,
     MinositesKezdDat = @Act_MinositesKezdDat,
     MinositesVegDat = @Act_MinositesVegDat,
     SztornirozasDat = @Act_SztornirozasDat,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'KRT_PartnerMinositesek',@Id
					,'KRT_PartnerMinositesekHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
