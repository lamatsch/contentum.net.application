IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_PartnerekGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_PartnerekGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_PartnerekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Partnerek.Id,
	   KRT_Partnerek.Org,
	   KRT_Partnerek.Orszag_Id,
	   KRT_Partnerek.Tipus,
	   KRT_Partnerek.Nev,
	   KRT_Partnerek.Hierarchia,
	   KRT_Partnerek.KulsoAzonositok,
	   KRT_Partnerek.PublikusKulcs,
	   KRT_Partnerek.Kiszolgalo,
	   KRT_Partnerek.LetrehozoSzervezet,
	   KRT_Partnerek.MaxMinosites,
	   KRT_Partnerek.MinositesKezdDat,
	   KRT_Partnerek.MinositesVegDat,
	   KRT_Partnerek.MaxMinositesSzervezet,
	   KRT_Partnerek.Belso,
	   KRT_Partnerek.Forras,
	   KRT_Partnerek.Ver,
	   KRT_Partnerek.Note,
	   KRT_Partnerek.Stat_id,
	   KRT_Partnerek.ErvKezd,
	   KRT_Partnerek.ErvVege,
	   KRT_Partnerek.Letrehozo_id,
	   KRT_Partnerek.LetrehozasIdo,
	   KRT_Partnerek.Modosito_id,
	   KRT_Partnerek.ModositasIdo,
	   KRT_Partnerek.Zarolo_id,
	   KRT_Partnerek.ZarolasIdo,
	   KRT_Partnerek.Tranz_id,
	   KRT_Partnerek.UIAccessLog_id
	   from 
		 KRT_Partnerek as KRT_Partnerek 
	   where
		 KRT_Partnerek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
