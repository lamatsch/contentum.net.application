IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_PartnerekGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_PartnerekGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_PartnerekGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Partnerek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Partnerek.Id,
	   KRT_Partnerek.Org,
	   KRT_Partnerek.Orszag_Id,
	   KRT_Partnerek.Tipus,
	   KRT_Partnerek.Nev,
	   KRT_Partnerek.Hierarchia,
	   KRT_Partnerek.KulsoAzonositok,
	   KRT_Partnerek.PublikusKulcs,
	   KRT_Partnerek.Kiszolgalo,
	   KRT_Partnerek.LetrehozoSzervezet,
	   KRT_Partnerek.MaxMinosites,
	   KRT_Partnerek.MinositesKezdDat,
	   KRT_Partnerek.MinositesVegDat,
	   KRT_Partnerek.MaxMinositesSzervezet,
	   KRT_Partnerek.Belso,
	   KRT_Partnerek.Forras,
	   KRT_Partnerek.Ver,
	   KRT_Partnerek.Note,
	   KRT_Partnerek.Stat_id,
	   KRT_Partnerek.ErvKezd,
	   KRT_Partnerek.ErvVege,
	   KRT_Partnerek.Letrehozo_id,
	   KRT_Partnerek.LetrehozasIdo,
	   KRT_Partnerek.Modosito_id,
	   KRT_Partnerek.ModositasIdo,
	   KRT_Partnerek.Zarolo_id,
	   KRT_Partnerek.ZarolasIdo,
	   KRT_Partnerek.Tranz_id,
	   KRT_Partnerek.UIAccessLog_id  
   from 
     KRT_Partnerek as KRT_Partnerek      
            Where KRT_Partnerek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
