IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekGetAllByPartner]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_PartnerekGetAllByPartner]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekGetAllByPartner]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_PartnerekGetAllByPartner] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_PartnerekGetAllByPartner]
  @Partner_Id UniqueIdentifier ,
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' KRT_Partnerek.Nev',
  @TopRow nvarchar(5) = '',  
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'select  ' + @LocalTopRow + '
  	   KRT_PartnerKapcsolatok.Id,
	   KRT_PartnerKapcsolatok.Tipus,
	   KRT_KodTarak1.Nev as TipusNev,
	   KRT_PartnerKapcsolatok.Partner_id,
	   KRT_PartnerKapcsolatok.Partner_id_kapcsolt,
	   KRT_Partnerek.Nev as Partner_kapcsolt_Nev,
	   KRT_Partnerek.Nev as Nev,	
	   KRT_KodTarak2.Nev as Partner_kapcsolt_TipusNev,	
	   KRT_PartnerKapcsolatok.Ver,
	   KRT_PartnerKapcsolatok.Note,
	   KRT_PartnerKapcsolatok.Stat_id,
	   KRT_PartnerKapcsolatok.ErvKezd,
	   KRT_PartnerKapcsolatok.ErvVege,
	   KRT_PartnerKapcsolatok.Letrehozo_id,
	   KRT_PartnerKapcsolatok.LetrehozasIdo,
	   KRT_PartnerKapcsolatok.Modosito_id,
	   KRT_PartnerKapcsolatok.ModositasIdo,
	   KRT_PartnerKapcsolatok.Zarolo_id,
	   KRT_PartnerKapcsolatok.ZarolasIdo,
	   KRT_PartnerKapcsolatok.Tranz_id,
	   KRT_PartnerKapcsolatok.UIAccessLog_id  
   from 
     KRT_PartnerKapcsolatok as KRT_PartnerKapcsolatok
	 left join KRT_KodCsoportok as KRT_KodCsoportok1 on KRT_KodCsoportok1.Kod=''PARTNERKAPCSOLAT_TIPUSA''
     left join KRT_KodTarak as KRT_Kodtarak1 on KRT_PartnerKapcsolatok.Tipus = KRT_KodTarak1.Kod and KRT_KodCsoportok1.Id = KRT_KodTarak1.KodCsoport_Id
		and KRT_KodTarak1.Org=''' + cast(@Org as NVarChar(40)) + '''
     left join KRT_Partnerek as KRT_Partnerek on KRT_PartnerKapcsolatok.Partner_id_kapcsolt = KRT_Partnerek.Id
	 left join KRT_KodCsoportok as KRT_KodCsoportok2 on KRT_KodCsoportok2.Kod=''PARTNER_TIPUS''
     left join KRT_KodTarak as KRT_KodTarak2 on KRT_Partnerek.Tipus = KRT_KodTarak2.Kod and KRT_KodCsoportok2.Id = KRT_KodTarak2.KodCsoport_Id
		and KRT_KodTarak2.Org=''' + cast(@Org as NVarChar(40)) + '''
     where 
	 KRT_PartnerKapcsolatok.Partner_id = ''' + CAST(@Partner_id as Nvarchar(40)) + '''
	and KRT_Partnerek.Org=''' + cast(@Org as NVarChar(40)) + '''
'
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end     
      
   --SET @sqlcmd = @sqlcmd + ' order by ' + @OrderBy

-- alárendenlt partnerek feluniózása, felhasználó
  SET @sqlcmd = @sqlcmd +
  ' union all select ' + @LocalTopRow + '
  	   KRT_PartnerKapcsolatok.Id,
	   KRT_PartnerKapcsolatok.Tipus,
	   TipusNev = 
	   case KRT_PartnerKapcsolatok.Tipus
	   WHEN ''1'' Then KRT_KodTarak1.Nev
	   ELSE KRT_KodTarak3.Nev 
	   END,
	   KRT_PartnerKapcsolatok.Partner_id_kapcsolt,
	   KRT_PartnerKapcsolatok.Partner_id,
	   KRT_Partnerek.Nev as Partner_kapcsolt_Nev,
	   KRT_Partnerek.Nev as Nev,
	   KRT_KodTarak2.Nev as Partner_kapcsolt_TipusNev,	
	   KRT_PartnerKapcsolatok.Ver,
	   KRT_PartnerKapcsolatok.Note,
	   KRT_PartnerKapcsolatok.Stat_id,
	   KRT_PartnerKapcsolatok.ErvKezd,
	   KRT_PartnerKapcsolatok.ErvVege,
	   KRT_PartnerKapcsolatok.Letrehozo_id,
	   KRT_PartnerKapcsolatok.LetrehozasIdo,
	   KRT_PartnerKapcsolatok.Modosito_id,
	   KRT_PartnerKapcsolatok.ModositasIdo,
	   KRT_PartnerKapcsolatok.Zarolo_id,
	   KRT_PartnerKapcsolatok.ZarolasIdo,
	   KRT_PartnerKapcsolatok.Tranz_id,
	   KRT_PartnerKapcsolatok.UIAccessLog_id  
   from 
     KRT_PartnerKapcsolatok as KRT_PartnerKapcsolatok
	 left join KRT_KodCsoportok as KRT_KodCsoportok1 on KRT_KodCsoportok1.Kod=''PARTNERKAPCSOLAT_TIPUSA''
     left join KRT_KodTarak as KRT_Kodtarak1 on ''11'' = KRT_KodTarak1.Kod and KRT_PartnerKapcsolatok.Tipus = ''1'' and KRT_KodCsoportok1.Id = KRT_KodTarak1.KodCsoport_Id
		and KRT_KodTarak1.Org=''' + cast(@Org as NVarChar(40)) + '''
     left join KRT_KodTarak as KRT_Kodtarak3 on KRT_PartnerKapcsolatok.Tipus = KRT_KodTarak3.Kod and KRT_PartnerKapcsolatok.Tipus != ''1'' and KRT_KodCsoportok1.Id = KRT_KodTarak3.KodCsoport_Id
		and KRT_KodTarak3.Org=''' + cast(@Org as NVarChar(40)) + '''
     left join KRT_Partnerek as KRT_Partnerek on KRT_PartnerKapcsolatok.Partner_id = KRT_Partnerek.Id
	 left join KRT_KodCsoportok as KRT_KodCsoportok2 on KRT_KodCsoportok2.Kod=''PARTNER_TIPUS''
     left join KRT_KodTarak as KRT_KodTarak2 on KRT_Partnerek.Tipus = KRT_KodTarak2.Kod and KRT_KodCsoportok2.Id = KRT_KodTarak2.KodCsoport_Id
		and KRT_KodTarak2.Org=''' + cast(@Org as nVarChar(40)) + '''
     where 
	 KRT_PartnerKapcsolatok.Partner_id_kapcsolt = ''' + CAST(@Partner_id as Nvarchar(40)) + '''
	 and KRT_PartnerKapcsolatok.Tipus in (''1'',''V'',''2'', ''M'')  -- BLG_346 
	and KRT_Partnerek.Org=''' + cast(@Org as NVarChar(40)) + '''
'


   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end     
      
   SET @sqlcmd = @sqlcmd + ' order by ' + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
