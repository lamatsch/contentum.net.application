IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekGetAllWithCim]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_PartnerekGetAllWithCim]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekGetAllWithCim]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_PartnerekGetAllWithCim] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_PartnerekGetAllWithCim]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Partnerek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @WithAllCim char(1) = '1',
  @CimTipus nvarchar(64) = 'all',
  @CimFajta nvarchar(64) = '05' -- Levelezesi cim

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
  
  --Cim fajtája szerinti szurés 
  DECLARE @WhereCimFajta nvarchar(100) 
  
  if @WithAllCim = '0'
   begin
      SET @WhereCimFajta = ' and KRT_PartnerCimek.Fajta = ' + @CimFajta 
   end
  else
   begin
      SET @WhereCimFajta = ''
   end

  --Cim tipus szerinti szurés
  DECLARE @WhereCimTipus nvarchar(100) 
  
  if @CimTipus != 'all'
   begin
      SET @WhereCimTipus = ' and KRT_Cimek.Tipus = ' + @CimTipus 
   end
  else
   begin
      SET @WhereCimTipus = ''
   end      

  SET @sqlcmd = 
  'select' + @LocalTopRow + '
  	   KRT_Partnerek.Id,
	   KRT_Partnerek.Org,
	   KRT_Partnerek.Orszag_Id,
	   KRT_Partnerek.Tipus,
	   KRT_Partnerek.Nev,
	   KRT_Partnerek.Hierarchia,
	   KRT_Partnerek.KulsoAzonositok,
	   KRT_Partnerek.PublikusKulcs,
	   KRT_Partnerek.Kiszolgalo,
	   KRT_Partnerek.LetrehozoSzervezet,
	   KRT_Partnerek.MaxMinosites,
	   KRT_Partnerek.MinositesKezdDat,
	   KRT_Partnerek.MinositesVegDat,
	   KRT_Partnerek.MaxMinositesSzervezet,
	   KRT_Partnerek.Belso,
	   KRT_Partnerek.Forras,
	   KRT_Partnerek.Ver,
	   KRT_Partnerek.Note,
	   KRT_Partnerek.Stat_id,
	   KRT_Partnerek.ErvKezd,
	   KRT_Partnerek.ErvVege,
	   KRT_Partnerek.Letrehozo_id,
	   KRT_Partnerek.LetrehozasIdo,
	   KRT_Partnerek.Modosito_id,
	   KRT_Partnerek.ModositasIdo,
	   KRT_Partnerek.Zarolo_id,
	   KRT_Partnerek.ZarolasIdo,
	   KRT_Partnerek.Tranz_id,
	   KRT_Partnerek.UIAccessLog_id,
       KRT_Cimek.Id as CimId,
       dbo.fn_MergeCim
          (KRT_Cimek.Tipus,
           KRT_Cimek.OrszagNev,
           KRT_Cimek.IRSZ,
           KRT_Cimek.TelepulesNev,
           KRT_Cimek.KozteruletNev,
           KRT_Cimek.KozteruletTipusNev,
           KRT_Cimek.Hazszam,
           KRT_Cimek.Hazszamig,
           KRT_Cimek.HazszamBetujel,
	       KRT_Cimek.Lepcsohaz,
	       KRT_Cimek.Szint,
	       KRT_Cimek.Ajto,
	       KRT_Cimek.AjtoBetujel,
	       KRT_Cimek.CimTobbi) as CimNev,     
	   KRT_Cimek.Kategoria,
	   KRT_Cimek.Tipus as CimTipus,
	   KRT_Cimek.KulsoAzonositok,
	   KRT_Cimek.Forras,
	   KRT_Cimek.Orszag_Id,
	   KRT_Cimek.OrszagNev,
	   KRT_Cimek.Telepules_Id,
	   KRT_Cimek.TelepulesNev,
	   KRT_Cimek.IRSZ,
	   KRT_Cimek.CimTobbi,
	   KRT_Cimek.Kozterulet_Id,
	   KRT_Cimek.KozteruletNev,
	   KRT_Cimek.KozteruletTipus_Id,
	   KRT_Cimek.KozteruletTipusNev,
	   KRT_Cimek.Hazszam,
	   KRT_Cimek.Hazszamig,
	   KRT_Cimek.HazszamBetujel,
	   KRT_Cimek.MindketOldal,
	   KRT_Cimek.HRSZ,
	   KRT_Cimek.Lepcsohaz,
	   KRT_Cimek.Szint,
	   KRT_Cimek.Ajto,
	   KRT_Cimek.AjtoBetujel,
	   KRT_Cimek.Tobbi  
   from 
     KRT_Partnerek as KRT_Partnerek
     left join KRT_PartnerCimek
     join KRT_Cimek on KRT_PartnerCimek.Cim_Id = KRT_Cimek.Id and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege ' + @WhereCimFajta + ' ' + @WhereCimTipus + '
     on KRT_Partnerek.Id = KRT_PartnerCimek.Partner_id and getdate() between KRT_PartnerCimek.ErvKezd and KRT_PartnerCimek.ErvVege 
            Where KRT_Partnerek.Org=''' + CAST(@Org as Nvarchar(40)) + '''
'
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
