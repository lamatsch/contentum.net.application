IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekGetByCsoportId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_PartnerekGetByCsoportId]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekGetByCsoportId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_PartnerekGetByCsoportId] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_KRT_PartnerekGetByCsoportId]
  ( 
		@Csoport_Id uniqueidentifier    
  )
AS
BEGIN
begin try
  
  SET NOCOUNT ON;
  
  DECLARE @csoportTipus NVARCHAR(64)
  SET @csoportTipus = (SELECT [KRT_Csoportok].[Tipus]
					   FROM [KRT_Csoportok]
					   WHERE [KRT_Csoportok].[Id] = @Csoport_Id)

  DECLARE @partnerId UNIQUEIDENTIFIER

  IF @csoporttipus = '1' --dolgozó
  BEGIN 
	SET @partnerId = (SELECT [KRT_Felhasznalok].[Partner_id]
					  FROM [KRT_Felhasznalok]
					  WHERE [KRT_Felhasznalok].[Id] = @Csoport_Id)
  END
  ELSE --IF @csoportTipus in ('0','4','5','6','R') -- Szervezet = "0"; Projekt = "4"; Testulet_Bizottsag = "5"; HivatalVezetes = "6";
  BEGIN  
	-- ugyanilyen Id-val van elvileg a krt_partnerek-ben (ellenorzés)
	SET @partnerId = @Csoport_Id	
  END
    
	SELECT 
	   KRT_Partnerek.Id,
	   KRT_Partnerek.Org,
	   KRT_Partnerek.Orszag_Id,
	   KRT_Partnerek.Tipus,
	   KRT_Partnerek.Nev,
	   KRT_Partnerek.Hierarchia,
	   KRT_Partnerek.KulsoAzonositok,
	   KRT_Partnerek.PublikusKulcs,
	   KRT_Partnerek.Kiszolgalo,
	   KRT_Partnerek.LetrehozoSzervezet,
	   KRT_Partnerek.MaxMinosites,
	   KRT_Partnerek.MinositesKezdDat,
	   KRT_Partnerek.MinositesVegDat,
	   KRT_Partnerek.MaxMinositesSzervezet,
	   KRT_Partnerek.Belso,
	   KRT_Partnerek.Forras,
	   KRT_Partnerek.Ver,
	   KRT_Partnerek.Note,
	   KRT_Partnerek.Stat_id,
	   KRT_Partnerek.ErvKezd,
	   KRT_Partnerek.ErvVege,
	   KRT_Partnerek.Letrehozo_id,
	   KRT_Partnerek.LetrehozasIdo,
	   KRT_Partnerek.Modosito_id,
	   KRT_Partnerek.ModositasIdo,
	   KRT_Partnerek.Zarolo_id,
	   KRT_Partnerek.ZarolasIdo,
	   KRT_Partnerek.Tranz_id,
	   KRT_Partnerek.UIAccessLog_id
	FROM [KRT_Partnerek]
	WHERE [KRT_Partnerek].[Id] = @partnerId
  

end try
begin catch
  DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
end catch
END


GO
