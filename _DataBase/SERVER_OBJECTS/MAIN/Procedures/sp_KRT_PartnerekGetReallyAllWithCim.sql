IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekGetReallyAllWithCim]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_PartnerekGetReallyAllWithCim]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekGetReallyAllWithCim]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_PartnerekGetReallyAllWithCim] AS' 
END
GO

ALTER procedure 
[dbo].[sp_KRT_PartnerekGetReallyAllWithCim]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by Nev ASC, CimNev',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @WithAllCim char(1) = '1',
  @WithKuldemeny char(1) = '1',
  @CimTipus nvarchar(64) = 'all',
  @CimFajta nvarchar(64) = '05' -- Levelezesi cim

as
begin

SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
BEGIN TRANSACTION;
set nocount on

BEGIN TRY
   
   DECLARE @sqlcmd           nvarchar(4000)

   DECLARE @where_Partner_Cim_delim nvarchar(25) 
   DECLARE @where_begin      nvarchar(25)
   DECLARE @where_end        nvarchar(400)
   DECLARE @whereOrig        nvarchar(400)
   DECLARE @whereCimNev      nvarchar(400)
   DECLARE @whereCimpartDelimPos INT;
   DECLARE @whereCimpartEndPos INT;
   
   DECLARE @LocalTopRow      nvarchar(10)

   DECLARE @Org              uniqueidentifier
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
   if (@Org is null)
   begin
		RAISERROR('[50202]',16,1)
   end

	--Több cím jelenjen-e meg egy partnerhez a gyorslistában, vagy csak egy
   DECLARE @TobbcimEgyPartnerhez int
   
   IF((select top 1 Ertek from KRT_Parameterek where Nev='TOBB_CIM_PARTNER_KERESOBEN'
                     and Org=@Org and getdate() between ErvKezd and ErvVege) = 1)
	  set @TobbcimEgyPartnerhez = 1
    ELSE
      set @TobbcimEgyPartnerhez = 0
   
   --Cim fajtája szerinti szűrés 
   DECLARE @WhereCimFajta    nvarchar(100) 

   SET @where_Partner_Cim_delim = '|'
   SET @whereOrig = @where
   SET @whereCimpartDelimPos = Charindex(@where_Partner_Cim_delim, @whereOrig)
   IF @whereCimpartDelimPos !=0
    --BEGIN
   	-- Print 'nincs cím rész...' 
    --END
    --ELSE
   BEGIN
   	-- Print 'van cím rész, pozíció:' 
   	-- Print @whereCimpartDelimPos
   	SET @whereCimpartEndPos = Charindex('%'')', @whereOrig, @whereCimpartDelimPos )
   	-- Print '%'') pozíció:' 
   	-- Print @whereCimpartEndPos
   	SET @whereCimNev = SUBSTRING(@whereOrig,@whereCimpartDelimPos+1,@whereCimpartEndPos - @whereCimpartDelimPos -1)
   	SET @where = SUBSTRING(@whereOrig,1, @whereCimpartDelimPos-1 ) + SUBSTRING(@whereOrig,@whereCimpartEndPos, 500)
    -- Print '@új where: ' + @where
    -- Print '@whereCimNev rész: ' + @whereCimNev
   END

   SET @where_begin = substring(@Where,1,22)
   SET @where_end   = substring(@Where,23,len(@Where))
    
   if (@TopRow = '' or @TopRow = '0')
       SET @LocalTopRow = ''
   ELSE
       SET @LocalTopRow = ' TOP ' + cast(cast((cast(@TopRow as int) * 10) as int) as nvarchar(10))
  
   if @WithAllCim = '0'
   begin
      SET @WhereCimFajta = ' and IsNull(KRT_PartnerCimek.Fajta,''' + @CimFajta + ''') = ' + @CimFajta 
   end
   else begin
      SET @WhereCimFajta = ''
   end

   --Cim tipus szerinti szűrés
   DECLARE @WhereCimTipus nvarchar(100) 
  
   if @CimTipus != 'all'
   begin
      SET @WhereCimTipus = ' and KRT_Cimek.Tipus = ' + @CimTipus 
   end
   else
   begin
      SET @WhereCimTipus = ''
   end      
   -------------------------------------------------------------------------------------------
   -- 'AJAXOSReallyPartnerkereso_Kuldemenyek' paraméter vizsgálata
   declare @kellKuldemenyHalmaz nvarchar(100)
   if (@WithKuldemeny = '0')
   begin
       set @kellKuldemenyHalmaz='Nem'
   end
   else
   begin
	   select @kellKuldemenyHalmaz=Ertek from dbo.KRT_Parameterek  
			  where Nev='AJAXOSReallyPartnerkereso_KellKuldemeny'
				and Org=@Org
	   set @kellKuldemenyHalmaz = isnull(@kellKuldemenyHalmaz,'IGEN')
	   if not @kellKuldemenyHalmaz in ('Igen','Nem') set @kellKuldemenyHalmaz='Igen'
   end


   -- Print '-----------------------'
   -- Print @kellKuldemenyHalmaz
   -- Print '-----------------------'
   ------------------------------------------------------------------------------------------


create table #t
(
  Id uniqueidentifier, 
  Nev nvarchar(400), 
  CimId uniqueidentifier,
  CimNev nvarchar(400), 
  Forras char(1) ,  
  Belso char(1)
)

IF @TobbcimEgyPartnerhez = 1
BEGIN
	SET @sqlcmd = 'insert into #t '+
	'select distinct ' + @LocalTopRow + ' KRT_Partnerek.Id as Id,
			KRT_Partnerek.Nev as Nev,
		   KRT_Cimek.Id as CimId,                       
			dbo.fn_MergeCim
			  (KRT_Cimek.Tipus,
			   KRT_Cimek.OrszagNev,
			   KRT_Cimek.IRSZ,
			   KRT_Cimek.TelepulesNev,
			   KRT_Cimek.KozteruletNev,
			   KRT_Cimek.KozteruletTipusNev,
			   KRT_Cimek.Hazszam,
			   KRT_Cimek.Hazszamig,
			   KRT_Cimek.HazszamBetujel,
			   KRT_Cimek.Lepcsohaz,
			   KRT_Cimek.Szint,
			   KRT_Cimek.Ajto,
			   KRT_Cimek.AjtoBetujel,
			   KRT_Cimek.CimTobbi) as CimNev,
			   KRT_Partnerek.Forras as Forras,
			   KRT_Partnerek.Belso as Belso
	from KRT_Partnerek left join KRT_PartnerCimek on KRT_PartnerCimek.partner_id=Krt_Partnerek.Id left join KRT_Cimek on KRT_PartnerCimek.Cim_Id = KRT_Cimek.Id
		   '+ @WhereCimFajta + ' ' + @WhereCimTipus + '
		   and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege
		   and getdate() between KRT_PartnerCimek.ErvKezd and KRT_PartnerCimek.ErvVege 
		   Where KRT_Partnerek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
END
ELSE
BEGIN
	SET @sqlcmd = 'insert into #t '+
	'select distinct ' + @LocalTopRow + ' KRT_Partnerek.Id as Id,
		   KRT_Partnerek.Nev as Nev,
		   (select top 1 KRT_Cimek.Id
			 from KRT_PartnerCimek join KRT_Cimek on KRT_PartnerCimek.Cim_Id = KRT_Cimek.Id 
				  and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege
				and getdate() between KRT_PartnerCimek.ErvKezd and KRT_PartnerCimek.ErvVege '
					+ @WhereCimFajta + ' ' + @WhereCimTipus +             
				  'and KRT_PartnerCimek.partner_id=Krt_Partnerek.Id ) as CimId,
		   (select top 1 dbo.fn_MergeCim
			  (KRT_Cimek.Tipus,
			   KRT_Cimek.OrszagNev,
			   KRT_Cimek.IRSZ,
			   KRT_Cimek.TelepulesNev,
			   KRT_Cimek.KozteruletNev,
			   KRT_Cimek.KozteruletTipusNev,
			   KRT_Cimek.Hazszam,
			   KRT_Cimek.Hazszamig,
			   KRT_Cimek.HazszamBetujel,
			   KRT_Cimek.Lepcsohaz,
			   KRT_Cimek.Szint,
			   KRT_Cimek.Ajto,
			   KRT_Cimek.AjtoBetujel,
			   KRT_Cimek.CimTobbi) as CimNev
			 from KRT_PartnerCimek join KRT_Cimek on KRT_PartnerCimek.Cim_Id = KRT_Cimek.Id 
				and getdate() between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege
				and getdate() between KRT_PartnerCimek.ErvKezd and KRT_PartnerCimek.ErvVege '
				  + @WhereCimFajta + ' ' + @WhereCimTipus + ' and KRT_PartnerCimek.partner_id=Krt_Partnerek.Id ) 
		   as CimNev,
		   KRT_Partnerek.Forras as Forras
	,	   KRT_Partnerek.Belso as Belso
	from KRT_Partnerek as KRT_Partnerek 
		   Where KRT_Partnerek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
END
		   
if @Where is not null and @Where != ''
begin 
-- Warning: az alábbiakban feltételezzük, h a nev oszlop az 1. 5 karakterben van. Mivel a tárolt eljárás
-- két helyről (küldemény beküldő, példány címzett) van meghívva, ez biztosítható
   SET @sqlcmd = @sqlcmd + ' and ' + @where
end

/* Ignorálja az @OrderBy bemenő paramétert */
set @sqlcmd = @sqlcmd + ' order by Nev ASC, CimNev'

-- -- Print @sqlcmd
exec (@sqlcmd)
----------------------------------------------------------------------------------------
if @kellKuldemenyHalmaz ='Igen'
begin
  set @sqlcmd = 'insert into #t '+ ' select distinct '  + @LocalTopRow + '
       EREC_KuldKuldemenyek.Partner_Id_Bekuldo as Id,
       EREC_KuldKuldemenyek.NevSTR_Bekuldo as Nev,
       EREC_KuldKuldemenyek.Cim_Id as CimId,
       EREC_KuldKuldemenyek.CimSTR_Bekuldo as CimNev,
       ''K'' as Forras,
       ''0'' as Belso
   FROM EREC_KuldKuldemenyek as EREC_KuldKuldemenyek
   WHERE EREC_KuldKuldemenyek.Letrehozo_Id in (select Id from KRT_Felhasznalok where Org=''' + CAST(@Org as Nvarchar(40)) + ''')
	AND (EREC_KuldKuldemenyek.Partner_Id_Bekuldo is NULL
   OR (EREC_KuldKuldemenyek.Cim_Id is NULL AND EREC_KuldKuldemenyek.CimSTR_Bekuldo != ''''))'


  if @Where is not null and @Where != ''
  begin 
    SET @sqlcmd = @sqlcmd + ' and ' + replace(@where_begin,'KRT_Partnerek.Nev','EREC_KuldKuldemenyek.NevSTR_Bekuldo ') 
                      + replace(@where_end,'KRT_Partnerek.','')

  end
  set @sqlcmd = @sqlcmd + ' order by Nev ASC, CimNev'
  -- Print @sqlcmd
  exec (@sqlcmd)
end
------------------------------------------------------------------------------------------------
   set @sqlcmd = 'select  distinct top ' + @TopRow + ' Id, Nev, CimId, CimNev, Forras , Belso
    from #t ' 
   IF  @whereCimpartDelimPos > 0
   BEGIN
   	set @sqlcmd = @sqlcmd + ' where CIMNev LIKE ''%' + @whereCimNev + '%'''
   END
    
    set @sqlcmd = @sqlcmd + @OrderBy 
 -- Print @sqlcmd
   exec (@sqlcmd)

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    

	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
ROLLBACK TRANSACTION;
end


GO
