IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_PartnerekInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_PartnerekInsert] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_PartnerekInsert]    
                @Id      uniqueidentifier = null,    
                           @Org     uniqueidentifier = null,    
                @Orszag_Id     uniqueidentifier  = null,
                @Tipus     nvarchar(64)  = null,
                @Nev     Nvarchar(400)  = null,
                @Hierarchia     Nvarchar(400)  = null,
                @KulsoAzonositok     Nvarchar(100)  = null,
                @PublikusKulcs     Nvarchar(4000)  = null,
                @Kiszolgalo     uniqueidentifier  = null,
                @LetrehozoSzervezet     uniqueidentifier  = null,
                @MaxMinosites     Nvarchar(2)  = null,
                @MinositesKezdDat     datetime  = null,
                @MinositesVegDat     datetime  = null,
                @MaxMinositesSzervezet     Nvarchar(2)  = null,
                @Belso     char(1)  = null,
                @Forras     char(1)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
 IF @ORG is NULL
 BEGIN 
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@Letrehozo_id)
   if (@Org is null)
   begin
   	RAISERROR('[50302]',16,1)
   end
END
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Org is not null
         begin
            SET @insertColumns = @insertColumns + ',Org'
            SET @insertValues = @insertValues + ',@Org'
         end 
       
         if @Orszag_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Orszag_Id'
            SET @insertValues = @insertValues + ',@Orszag_Id'
         end 
       
         if @Tipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Tipus'
            SET @insertValues = @insertValues + ',@Tipus'
         end 
       
         if @Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Nev'
            SET @insertValues = @insertValues + ',@Nev'
         end 
       
         if @Hierarchia is not null
         begin
            SET @insertColumns = @insertColumns + ',Hierarchia'
            SET @insertValues = @insertValues + ',@Hierarchia'
         end 
       
         if @KulsoAzonositok is not null
         begin
            SET @insertColumns = @insertColumns + ',KulsoAzonositok'
            SET @insertValues = @insertValues + ',@KulsoAzonositok'
         end 
       
         if @PublikusKulcs is not null
         begin
            SET @insertColumns = @insertColumns + ',PublikusKulcs'
            SET @insertValues = @insertValues + ',@PublikusKulcs'
         end 
       
         if @Kiszolgalo is not null
         begin
            SET @insertColumns = @insertColumns + ',Kiszolgalo'
            SET @insertValues = @insertValues + ',@Kiszolgalo'
         end 
       
         if @LetrehozoSzervezet is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozoSzervezet'
            SET @insertValues = @insertValues + ',@LetrehozoSzervezet'
         end 
       
         if @MaxMinosites is not null
         begin
            SET @insertColumns = @insertColumns + ',MaxMinosites'
            SET @insertValues = @insertValues + ',@MaxMinosites'
         end 
       
         if @MinositesKezdDat is not null
         begin
            SET @insertColumns = @insertColumns + ',MinositesKezdDat'
            SET @insertValues = @insertValues + ',@MinositesKezdDat'
         end 
       
         if @MinositesVegDat is not null
         begin
            SET @insertColumns = @insertColumns + ',MinositesVegDat'
            SET @insertValues = @insertValues + ',@MinositesVegDat'
         end 
       
         if @MaxMinositesSzervezet is not null
         begin
            SET @insertColumns = @insertColumns + ',MaxMinositesSzervezet'
            SET @insertValues = @insertValues + ',@MaxMinositesSzervezet'
         end 
       
         if @Belso is not null
         begin
            SET @insertColumns = @insertColumns + ',Belso'
            SET @insertValues = @insertValues + ',@Belso'
         end 
       
         if @Forras is not null
         begin
            SET @insertColumns = @insertColumns + ',Forras'
            SET @insertValues = @insertValues + ',@Forras'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into KRT_Partnerek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Org uniqueidentifier,@Orszag_Id uniqueidentifier,@Tipus nvarchar(64),@Nev Nvarchar(400),@Hierarchia Nvarchar(400),@KulsoAzonositok Nvarchar(100),@PublikusKulcs Nvarchar(4000),@Kiszolgalo uniqueidentifier,@LetrehozoSzervezet uniqueidentifier,@MaxMinosites Nvarchar(2),@MinositesKezdDat datetime,@MinositesVegDat datetime,@MaxMinositesSzervezet Nvarchar(2),@Belso char(1),@Forras char(1),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Org = @Org,@Orszag_Id = @Orszag_Id,@Tipus = @Tipus,@Nev = @Nev,@Hierarchia = @Hierarchia,@KulsoAzonositok = @KulsoAzonositok,@PublikusKulcs = @PublikusKulcs,@Kiszolgalo = @Kiszolgalo,@LetrehozoSzervezet = @LetrehozoSzervezet,@MaxMinosites = @MaxMinosites,@MinositesKezdDat = @MinositesKezdDat,@MinositesVegDat = @MinositesVegDat,@MaxMinositesSzervezet = @MaxMinositesSzervezet,@Belso = @Belso,@Forras = @Forras,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'KRT_Partnerek',@ResultUid
					,'KRT_PartnerekHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
