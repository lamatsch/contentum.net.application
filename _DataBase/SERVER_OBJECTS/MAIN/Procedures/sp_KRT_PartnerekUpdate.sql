IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_PartnerekUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_PartnerekUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_PartnerekUpdate] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_PartnerekUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Org     uniqueidentifier  = null,         
             @Orszag_Id     uniqueidentifier  = null,         
             @Tipus     nvarchar(64)  = null,         
             @Nev     Nvarchar(400)  = null,         
             @Hierarchia     Nvarchar(400)  = null,         
             @KulsoAzonositok     Nvarchar(100)  = null,         
             @PublikusKulcs     Nvarchar(4000)  = null,         
             @Kiszolgalo     uniqueidentifier  = null,         
             @LetrehozoSzervezet     uniqueidentifier  = null,         
             @MaxMinosites     Nvarchar(2)  = null,         
             @MinositesKezdDat     datetime  = null,         
             @MinositesVegDat     datetime  = null,         
             @MaxMinositesSzervezet     Nvarchar(2)  = null,         
             @Belso     char(1)  = null,         
             @Forras     char(1)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Org     uniqueidentifier         
     DECLARE @Act_Orszag_Id     uniqueidentifier         
     DECLARE @Act_Tipus     nvarchar(64)         
     DECLARE @Act_Nev     Nvarchar(400)         
     DECLARE @Act_Hierarchia     Nvarchar(400)         
     DECLARE @Act_KulsoAzonositok     Nvarchar(100)         
     DECLARE @Act_PublikusKulcs     Nvarchar(4000)         
     DECLARE @Act_Kiszolgalo     uniqueidentifier         
     DECLARE @Act_LetrehozoSzervezet     uniqueidentifier         
     DECLARE @Act_MaxMinosites     Nvarchar(2)         
     DECLARE @Act_MinositesKezdDat     datetime         
     DECLARE @Act_MinositesVegDat     datetime         
     DECLARE @Act_MaxMinositesSzervezet     Nvarchar(2)         
     DECLARE @Act_Belso     char(1)         
     DECLARE @Act_Forras     char(1)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Org = Org,
     @Act_Orszag_Id = Orszag_Id,
     @Act_Tipus = Tipus,
     @Act_Nev = Nev,
     @Act_Hierarchia = Hierarchia,
     @Act_KulsoAzonositok = KulsoAzonositok,
     @Act_PublikusKulcs = PublikusKulcs,
     @Act_Kiszolgalo = Kiszolgalo,
     @Act_LetrehozoSzervezet = LetrehozoSzervezet,
     @Act_MaxMinosites = MaxMinosites,
     @Act_MinositesKezdDat = MinositesKezdDat,
     @Act_MinositesVegDat = MinositesVegDat,
     @Act_MaxMinositesSzervezet = MaxMinositesSzervezet,
     @Act_Belso = Belso,
     @Act_Forras = Forras,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from KRT_Partnerek
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Org')=1
         SET @Act_Org = @Org
   IF @UpdatedColumns.exist('/root/Orszag_Id')=1
         SET @Act_Orszag_Id = @Orszag_Id
   IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @Act_Tipus = @Tipus
   IF @UpdatedColumns.exist('/root/Nev')=1
         SET @Act_Nev = @Nev
   IF @UpdatedColumns.exist('/root/Hierarchia')=1
         SET @Act_Hierarchia = @Hierarchia
   IF @UpdatedColumns.exist('/root/KulsoAzonositok')=1
         SET @Act_KulsoAzonositok = @KulsoAzonositok
   IF @UpdatedColumns.exist('/root/PublikusKulcs')=1
         SET @Act_PublikusKulcs = @PublikusKulcs
   IF @UpdatedColumns.exist('/root/Kiszolgalo')=1
         SET @Act_Kiszolgalo = @Kiszolgalo
   IF @UpdatedColumns.exist('/root/LetrehozoSzervezet')=1
         SET @Act_LetrehozoSzervezet = @LetrehozoSzervezet
   IF @UpdatedColumns.exist('/root/MaxMinosites')=1
         SET @Act_MaxMinosites = @MaxMinosites
   IF @UpdatedColumns.exist('/root/MinositesKezdDat')=1
         SET @Act_MinositesKezdDat = @MinositesKezdDat
   IF @UpdatedColumns.exist('/root/MinositesVegDat')=1
         SET @Act_MinositesVegDat = @MinositesVegDat
   IF @UpdatedColumns.exist('/root/MaxMinositesSzervezet')=1
         SET @Act_MaxMinositesSzervezet = @MaxMinositesSzervezet
   IF @UpdatedColumns.exist('/root/Belso')=1
         SET @Act_Belso = @Belso
   IF @UpdatedColumns.exist('/root/Forras')=1
         SET @Act_Forras = @Forras
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE KRT_Partnerek
SET
     Org = @Act_Org,
     Orszag_Id = @Act_Orszag_Id,
     Tipus = @Act_Tipus,
     Nev = @Act_Nev,
     Hierarchia = @Act_Hierarchia,
     KulsoAzonositok = @Act_KulsoAzonositok,
     PublikusKulcs = @Act_PublikusKulcs,
     Kiszolgalo = @Act_Kiszolgalo,
     LetrehozoSzervezet = @Act_LetrehozoSzervezet,
     MaxMinosites = @Act_MaxMinosites,
     MinositesKezdDat = @Act_MinositesKezdDat,
     MinositesVegDat = @Act_MinositesVegDat,
     MaxMinositesSzervezet = @Act_MaxMinositesSzervezet,
     Belso = @Act_Belso,
     Forras = @Act_Forras,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'KRT_Partnerek',@Id
					,'KRT_PartnerekHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
