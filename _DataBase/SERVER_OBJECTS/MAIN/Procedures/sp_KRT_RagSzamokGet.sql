IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_RagSzamokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_RagSzamokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_RagSzamokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_RagSzamokGet] AS' 
END
GO

ALTER procedure [dbo].[sp_KRT_RagSzamokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_RagSzamok.Id,
	   KRT_RagSzamok.Kod,
	   KRT_RagSzamok.KodNum,
	   KRT_RagSzamok.Postakonyv_Id,
	   KRT_RagSzamok.RagszamSav_Id,
	   KRT_RagSzamok.Obj_Id,
	   KRT_RagSzamok.ObjTip_Id,
	   KRT_RagSzamok.Obj_type,
	   KRT_RagSzamok.KodType,
	   KRT_RagSzamok.Allapot,
	   CASE KRT_RagSzamok.Allapot 
       		  when ''A'' then ''Allokált'' 
       		  when ''F'' then ''Felhasznált'' 
			  when ''H'' then ''Használható'' 
       		  else ''Törölt'' 
       END AllapotName,
	   KRT_RagSzamok.Ver,
	   KRT_RagSzamok.Note,
	   KRT_RagSzamok.Stat_id,
	   KRT_RagSzamok.ErvKezd,
	   KRT_RagSzamok.ErvVege,
	   KRT_RagSzamok.Letrehozo_id,
	   KRT_RagSzamok.LetrehozasIdo,
	   KRT_RagSzamok.Modosito_id,
	   KRT_RagSzamok.ModositasIdo,
	   KRT_RagSzamok.Zarolo_id,
	   KRT_RagSzamok.ZarolasIdo,
	   KRT_RagSzamok.Tranz_id,
	   KRT_RagSzamok.UIAccessLog_id
	   from 
		 KRT_RagSzamok as KRT_RagSzamok 
	   where
		 KRT_RagSzamok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end

GO
