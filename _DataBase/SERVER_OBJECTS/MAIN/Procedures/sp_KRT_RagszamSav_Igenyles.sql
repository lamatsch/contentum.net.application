IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_RagszamSav_Igenyles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_RagszamSav_Igenyles]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_RagszamSav_Igenyles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_RagszamSav_Igenyles] AS' 
END
GO

ALTER procedure [dbo].[sp_KRT_RagszamSav_Igenyles]    
			 @RagszamSav_Id         UNIQUEIDENTIFIER	=	NULL,
			 @Postakonyv_Id			UNIQUEIDENTIFIER	=	NULL,
			 @LetrehozoSzervezet_Id UNIQUEIDENTIFIER			,
			 @Letrehozo_Id			UNIQUEIDENTIFIER	=	NULL,
	         @FoglalandoDarab		INTEGER						,
			 @FoglalandoAllapota	CHAR(1)				=	'H' ,
             @ResultUid				NVARCHAR(MAX) OUTPUT
         
AS
BEGIN TRY

SET NOCOUNT ON;

		declare @teszt           int
		select  @teszt = 1
 
		declare @sajat_tranz     int  -- saját tranzakció kezelés? (0 = nem, 1 = igen)
		select  @sajat_tranz   = (CASE When @@trancount > 0 Then 0 Else 1 END)

		if @sajat_tranz = 1  
		   BEGIN TRANSACTION 
 
		select  @ResultUid = NULL

		declare @error           int
		declare @rowcount        int

		------- 
		
		IF @RagszamSav_Id IS NULL AND @Postakonyv_Id IS NULL
		     --RAISERROR('Hiányos ragszám paraméterek!',16,1)
			  RAISERROR('[64082]',16,1)
			 
		IF @RagszamSav_Id IS NOT NULL
			BEGIN
					IF NOT EXISTS(SELECT 1 FROM DBO.KRT_RagszamSavok 
								  WHERE ID = @RagszamSav_Id
					     			    AND SAVALLAPOT = 'A')
						--RAISERROR('Nem engedélyezett a ragszámsávon az igénylés!',16,1)
						 RAISERROR('[64083]',16,1)
			END

		IF @RagszamSav_Id IS NULL AND @Postakonyv_Id IS NOT NULL
			SELECT TOP 1 @RagszamSav_Id = ID 
			FROM DBO.KRT_RagszamSavok 
			WHERE Postakonyv_Id = @Postakonyv_Id
				AND SAVALLAPOT = 'A'
		
		IF @RagszamSav_Id IS NULL
		     -- RAISERROR('Nem található felhasználható ragszámsáv!',16,1)
			  RAISERROR('[64084]',16,1)

		IF (@Postakonyv_Id IS NULL)
			SELECT @Postakonyv_Id= Postakonyv_Id
			FROM KRT_RagszamSavok
			WHERE Id = @RagszamSav_Id

		--------------------------------
		-- Darab ellenőrzés
		--------------------------------
		DECLARE @MAX_RAGSZAM_COUNT	INT
		SELECT @MAX_RAGSZAM_COUNT = SavVegeNum - SavKezdNum
		FROM KRT_RagszamSavok
		WHERE ID = @RagszamSav_Id

		IF @FoglalandoDarab > @MAX_RAGSZAM_COUNT
			 --RAISERROR('A foglalni kívánt darabszám nagyobb mint ami rendelkezésre áll!',16,1)
			 RAISERROR('[64085]',16,1)
		--------------------------------
		-- Foglalás
		--------------------------------
		DECLARE @cnt INT = 0;
		DECLARE @VAN_MAR_RAGSZAM BIT = 0

		IF (EXISTS(SELECT 1 FROM KRT_RAGSZAMOK WHERE RagszamSav_Id = @RagszamSav_Id))
			SET @VAN_MAR_RAGSZAM = 1

		DECLARE @I_SAVKEZD			NVARCHAR(100)
		DECLARE @I_SAVKEZD_NEXT		NVARCHAR(100)

		DECLARE @I_SAVKEZD_NUM		FLOAT
		DECLARE @I_SAVKEZD_NUM_NEXT	FLOAT

		--INSERT PARAMS
		DECLARE @I_Id uniqueidentifier
		DECLARE @I_Kod nvarchar(100)
		DECLARE @I_KodNum float
		DECLARE @I_Postakonyv_Id uniqueidentifier
		DECLARE @I_RagszamSav_Id uniqueidentifier
		DECLARE @I_Obj_Id uniqueidentifier
		DECLARE @I_ObjTip_Id uniqueidentifier
		DECLARE @I_Obj_type nvarchar(100)
		DECLARE @I_KodType char(1)
		DECLARE @I_Allapot nvarchar(64)
		DECLARE @I_Ver int
		DECLARE @I_Note nvarchar(4000)
		DECLARE @I_Stat_id uniqueidentifier
		DECLARE @I_ErvKezd datetime
		DECLARE @I_ErvVege datetime
		DECLARE @I_Letrehozo_id uniqueidentifier
		DECLARE @I_LetrehozasIdo datetime
		DECLARE @I_Modosito_id uniqueidentifier
		DECLARE @I_ModositasIdo datetime
		DECLARE @I_Zarolo_id uniqueidentifier
		DECLARE @I_ZarolasIdo datetime
		DECLARE @I_Tranz_id uniqueidentifier
		DECLARE @I_UIAccessLog_id uniqueidentifier
		DECLARE @I_UpdatedColumns xml
		DECLARE @I_ResultUid uniqueidentifier
		--bug 2124 - checksum készítése
		DECLARE @I_SAVKEZD_NUM_SEGED FLOAT
		DECLARE @I_CDV int
		DECLARE @I_CDV_CALC int
		DECLARE @I_CDV_IND int
		DECLARE @I_SAVKEZD_NUM_NEXT_LENGTH int
		DECLARE @I_CDV_CALVULATE_CHAR char(1)
		DECLARE @I_CDV_CALVULATE_INT int
		--DECLARE @I_SAVKEZD_NUM_NEXT_CONVERTED nvarchar(100)
		--
		DECLARE @I_DATE DATETIME
		SET @I_DATE = GETDATE()
		DECLARE @ELSO BIT = 1
		WHILE @cnt < @FoglalandoDarab
		BEGIN
			IF (@VAN_MAR_RAGSZAM = 1)
				BEGIN
					SET @I_SAVKEZD			= NULL
					SET @I_SAVKEZD_NUM		= NULL
					SET @I_SAVKEZD_NEXT		= NULL
					SET @I_SAVKEZD_NUM_NEXT = NULL
			
					SELECT TOP 1 @I_SAVKEZD			= Kod, 
								 @I_SAVKEZD_NUM		= KodNum
					FROM KRT_RAGSZAMOK
					WHERE RagszamSav_Id = @RagszamSav_Id
					ORDER BY KodNum DESC
					SET @I_SAVKEZD_NUM_SEGED = FLOOR(@I_SAVKEZD_NUM/10)
					SET @I_SAVKEZD_NUM_SEGED = @I_SAVKEZD_NUM_SEGED + 1;
					--SET @I_SAVKEZD_NUM_NEXT = @I_SAVKEZD_NUM +1
					--calculate checksum
					SET @I_CDV_CALC = 0
					SET @I_CDV_IND = 1
					SET @I_SAVKEZD_NUM_NEXT_LENGTH = LEN(LTRIM(STR(@I_SAVKEZD_NUM_SEGED, 25, 0)))
					WHILE @I_CDV_IND<= @I_SAVKEZD_NUM_NEXT_LENGTH
						BEGIN
							--első 4 számjeggyel nem foglalkozunk, mert az az irányítószám
							IF @I_CDV_IND > 4
								BEGIN
									SET @I_CDV_CALVULATE_CHAR = SUBSTRING(LTRIM(STR(@I_SAVKEZD_NUM_SEGED, 25, 0)), @I_CDV_IND, 1)
									SET @I_CDV_CALVULATE_INT = CAST(@I_CDV_CALVULATE_CHAR AS INT)
									if (@I_CDV_IND % 2 = 1)
										SET @I_CDV_CALC = @I_CDV_CALC + @I_CDV_CALVULATE_INT * 3
									ELSE
										SET @I_CDV_CALC = @I_CDV_CALC + @I_CDV_CALVULATE_INT
								END
							SET @I_CDV_IND= @I_CDV_IND+ 1
						END
					SET @I_CDV = @I_CDV_CALC % 10

					SET @I_SAVKEZD_NUM_NEXT = @I_SAVKEZD_NUM_SEGED * 10 + @I_CDV

					SET @I_SAVKEZD_NEXT = REPLACE(@I_SAVKEZD,LTRIM(STR(@I_SAVKEZD_NUM, 25, 0)),LTRIM(STR(@I_SAVKEZD_NUM_NEXT, 25, 0)))

					IF (@I_SAVKEZD_NUM_NEXT IS NULL OR @I_SAVKEZD_NEXT IS NULL)
						--RAISERROR('Ragszám kód hiba!',16,1)
						RAISERROR('[64086]',16,1)

					SET @I_Id				= NEWID()
					SET @I_Kod				= @I_SAVKEZD_NEXT
					SET @I_KodNum			= @I_SAVKEZD_NUM_NEXT
					SET @I_Postakonyv_Id	= @Postakonyv_Id
					SET @I_RagszamSav_Id	= @RagszamSav_Id
					SET @I_Obj_type			= 'KRT_RagszamSavok'
					SET @I_Allapot			= @FoglalandoAllapota
					SET @I_Ver				= '1'
					SET @I_ErvKezd			= @I_DATE	
					SET @I_ErvVege			= '4700-12-31 00:00:00.000'
					SET @I_Letrehozo_id		= @Letrehozo_Id
					SET @I_LetrehozasIdo	= @I_DATE
					
					EXECUTE [dbo].[sp_KRT_RagSzamokInsert] 
							 @I_Id
							,@I_Kod
							,@I_KodNum
							,@I_Postakonyv_Id
							,@I_RagszamSav_Id
							,@I_Obj_Id
							,@I_ObjTip_Id
							,@I_Obj_type
							,@I_KodType
							,@I_Allapot
							,@I_Ver
							,@I_Note
							,@I_Stat_id
							,@I_ErvKezd
							,@I_ErvVege
							,@I_Letrehozo_id
							,@I_LetrehozasIdo
							,@I_Modosito_id
							,@I_ModositasIdo
							,@I_Zarolo_id
							,@I_ZarolasIdo
							,@I_Tranz_id
							,@I_UIAccessLog_id
							,@I_UpdatedColumns
							,@I_ResultUid OUTPUT

					IF @ELSO = 1
						BEGIN
							SET @ELSO = 0
							SET @ResultUid =  CAST(@I_Id AS NVARCHAR(MAX))
						END
					ELSE
						SET @ResultUid = @ResultUid + ',' + CAST(@I_Id AS NVARCHAR(MAX))

				END
			ELSE
				BEGIN
					SET @I_SAVKEZD		= NULL
					set @I_SAVKEZD_NUM	= NULL

					--LOAD FIRST FROM RAGSZAMSAV
					SELECT TOP 1 @I_SAVKEZD = SavKezd, 
								 @I_SAVKEZD_NUM = SavKezdNum 
					FROM KRT_RagszamSavok 
					WHERE Id = @RagszamSav_Id
						AND SAVALLAPOT = 'A'

					IF (@I_SAVKEZD IS NULL OR @I_SAVKEZD_NUM IS NULL)
						-- RAISERROR('Nem található felhasználható ragszámsáv!',16,1)
						RAISERROR('[64084]',16,1)

					SET @I_Id				= NEWID()
					SET @I_Kod				= @I_SAVKEZD
					SET @I_KodNum			= @I_SAVKEZD_NUM
					SET @I_Postakonyv_Id	= @Postakonyv_Id
					SET @I_RagszamSav_Id	= @RagszamSav_Id
					SET @I_Obj_type			= 'KRT_RagszamSavok'
					SET @I_Allapot			= @FoglalandoAllapota
					SET @I_Ver				= '1'
					SET @I_ErvKezd			= @I_DATE
					SET @I_ErvVege			= '4700-12-31 00:00:00.000'
					SET @I_Letrehozo_id		= @Letrehozo_Id
					SET @I_LetrehozasIdo	= @I_DATE	

					EXECUTE  [dbo].[sp_KRT_RagSzamokInsert] 
							 @I_Id
							,@I_Kod
							,@I_KodNum
							,@I_Postakonyv_Id
							,@I_RagszamSav_Id
							,@I_Obj_Id
							,@I_ObjTip_Id
							,@I_Obj_type
							,@I_KodType
							,@I_Allapot
							,@I_Ver
							,@I_Note
							,@I_Stat_id
							,@I_ErvKezd
							,@I_ErvVege
							,@I_Letrehozo_id
							,@I_LetrehozasIdo
							,@I_Modosito_id
							,@I_ModositasIdo
							,@I_Zarolo_id
							,@I_ZarolasIdo
							,@I_Tranz_id
							,@I_UIAccessLog_id
							,@I_UpdatedColumns
							,@I_ResultUid OUTPUT

					IF @ELSO = 1
						BEGIN
							SET @ELSO = 0
							SET @ResultUid =  CAST(@I_Id AS NVARCHAR(MAX))
						END
					ELSE
						SET @ResultUid = @ResultUid + ',' + CAST(@I_Id AS NVARCHAR(MAX))

					--SET CONDITION
					SET @VAN_MAR_RAGSZAM = 1
				END

		   SET @cnt = @cnt + 1;
		END;

		if @sajat_tranz = 1  
		   COMMIT TRANSACTION

		--SET @ResultUid = @I_Id

		if @teszt = 1
		   print '@ResultUid = '+ isnull( convert(varchar(50),@ResultUid), 'NULL')
 
END TRY
-----------
-----------
BEGIN CATCH
	   if @sajat_tranz = 1  
		  ROLLBACK TRANSACTION
   
	   DECLARE @errorSeverity INT, @errorState INT
	   DECLARE @errorCode NVARCHAR(1000)    
	   SET @errorSeverity = ERROR_SEVERITY()
	   SET @errorState = ERROR_STATE()

	   if ERROR_NUMBER()<50000	
		  SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	   else
		  SET @errorCode = ERROR_MESSAGE()
      
	   if @errorState = 0 
		  SET @errorState = 1

	   if @teszt = 1
		  print '@ResultUid = '+ isnull( convert(varchar(50),@ResultUid), 'NULL')

	   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------

GO
