IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_RagszamSav_IgenylesManual]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_RagszamSav_IgenylesManual]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_RagszamSav_IgenylesManual]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_RagszamSav_IgenylesManual] AS' 
END
GO


ALTER PROCEDURE [dbo].[sp_KRT_RagszamSav_IgenylesManual]    
			 @Ragszam		        NVARCHAR(MAX),
			 @RagszamNum	     	FLOAT,
			 @SavType		        NVARCHAR(100)		=	NULL,
			 @Postakonyv_Id			UNIQUEIDENTIFIER	=	NULL,
			 @Org_Id				UNIQUEIDENTIFIER,
			 @Letrehozo_Id			UNIQUEIDENTIFIER,
             @ResultUid				NVARCHAR(MAX) OUTPUT
AS     
BEGIN TRY

SET NOCOUNT ON;

		DECLARE @TESZT           INT
		SELECT  @TESZT = 1
 
		DECLARE @SAJAT_TRANZ     INT  -- SAJÁT TRANZAKCIÓ KEZELÉS? (0 = NEM, 1 = IGEN)
		SELECT  @SAJAT_TRANZ   = (CASE WHEN @@TRANCOUNT > 0 THEN 0 ELSE 1 END)

		IF @SAJAT_TRANZ = 1  
		   BEGIN TRANSACTION 
 
		SELECT  @RESULTUID = NULL

		DECLARE @ERROR           INT
		DECLARE @ROWCOUNT        INT

		DECLARE @RagszamSav_Id	UNIQUEIDENTIFIER
		-------------------------------------------------------------------------------

		IF EXISTS( SELECT 1 FROM KRT_RAGSZAMOK WHERE Kod = @Ragszam  )
			  --RAISERROR('A ragszám már létezik, és használatban van !',16,1)
			  RAISERROR('[64080]', 16,1)
			  

		IF NOT EXISTS(
		 		 SELECT 1
				 FROM KRT_RagszamSavok
				 WHERE @RagszamNum BETWEEN SavKezdNum and SavVegeNum
					AND 
					   SavAllapot = 'A'
				 )
			 RAISERROR('[64081]', 16,1)
			 --RAISERROR('A ragszámhoz nem tartozik megfelelő ragszámsáv !',16,1)

		 SELECT @Postakonyv_Id = Postakonyv_Id,
				@RagszamSav_Id = Id, 
				@SavType = SavType
		 FROM KRT_RagszamSavok
		 WHERE @RagszamNum BETWEEN SavKezdNum and SavVegeNum
			AND 
				SavAllapot = 'A'
		
		--------------------------------
		-- Foglalás
		--------------------------------
		--INSERT PARAMS
		DECLARE @I_Id uniqueidentifier
		DECLARE @I_Kod nvarchar(100)
		DECLARE @I_KodNum float
		DECLARE @I_Postakonyv_Id uniqueidentifier
		DECLARE @I_RagszamSav_Id uniqueidentifier
		DECLARE @I_Obj_Id uniqueidentifier
		DECLARE @I_ObjTip_Id uniqueidentifier
		DECLARE @I_Obj_type nvarchar(100)
		DECLARE @I_KodType char(1)
		DECLARE @I_Allapot nvarchar(64)
		DECLARE @I_Ver int
		DECLARE @I_Note nvarchar(4000)
		DECLARE @I_Stat_id uniqueidentifier
		DECLARE @I_ErvKezd datetime
		DECLARE @I_ErvVege datetime
		DECLARE @I_Letrehozo_id uniqueidentifier
		DECLARE @I_LetrehozasIdo datetime
		DECLARE @I_Modosito_id uniqueidentifier
		DECLARE @I_ModositasIdo datetime
		DECLARE @I_Zarolo_id uniqueidentifier
		DECLARE @I_ZarolasIdo datetime
		DECLARE @I_Tranz_id uniqueidentifier
		DECLARE @I_UIAccessLog_id uniqueidentifier
		DECLARE @I_UpdatedColumns xml
		DECLARE @I_ResultUid uniqueidentifier

		DECLARE @I_DATE DATETIME
		SET @I_DATE = GETDATE()

		SET @I_Id				= NEWID()
		SET @I_Kod				= @Ragszam
		SET @I_KodNum			= @RagszamNum
		SET @I_Postakonyv_Id	= @Postakonyv_Id
		SET @I_RagszamSav_Id	= @RagszamSav_Id
		SET @I_Obj_type			= 'KRT_RagszamSavok'
		SET @I_Allapot			= 'A'
		SET @I_Ver				= '1'
		SET @I_ErvKezd			= @I_DATE	
		SET @I_ErvVege			= '4700-12-31 00:00:00.000'
		SET @I_Letrehozo_id		= @Letrehozo_Id
		SET @I_LetrehozasIdo	= @I_DATE
					
		EXECUTE [dbo].[sp_KRT_RagSzamokInsert] 
				 @I_Id
				,@I_Kod
				,@I_KodNum
				,@I_Postakonyv_Id
				,@I_RagszamSav_Id
				,@I_Obj_Id
				,@I_ObjTip_Id
				,@I_Obj_type
				,@I_KodType
				,@I_Allapot
				,@I_Ver
				,@I_Note
				,@I_Stat_id
				,@I_ErvKezd
				,@I_ErvVege
				,@I_Letrehozo_id
				,@I_LetrehozasIdo
				,@I_Modosito_id
				,@I_ModositasIdo
				,@I_Zarolo_id
				,@I_ZarolasIdo
				,@I_Tranz_id
				,@I_UIAccessLog_id
				,@I_UpdatedColumns
				,@I_ResultUid OUTPUT

		------------------------------
		if @sajat_tranz = 1  
		   COMMIT TRANSACTION

		SET @ResultUid = @I_Id

		if @teszt = 1
		   print '@ResultUid = '+ isnull( convert(varchar(50),@ResultUid), 'NULL')
 
END TRY
-----------
BEGIN CATCH
	   if @sajat_tranz = 1  
		  ROLLBACK TRANSACTION
   
	   DECLARE @errorSeverity INT, @errorState INT
	   DECLARE @errorCode NVARCHAR(1000)    
	   SET @errorSeverity = ERROR_SEVERITY()
	   SET @errorState = ERROR_STATE()

	   if ERROR_NUMBER()<50000	
		  SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	   else
		  SET @errorCode = ERROR_MESSAGE()
      
	   if @errorState = 0 
		  SET @errorState = 1

	   if @teszt = 1
		  print '@ResultUid = '+ isnull( convert(varchar(50),@ResultUid), 'NULL')

	   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------

GO
