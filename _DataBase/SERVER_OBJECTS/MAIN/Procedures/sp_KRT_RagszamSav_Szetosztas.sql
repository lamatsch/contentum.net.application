IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_RagszamSav_Szetosztas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_RagszamSav_Szetosztas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_RagszamSav_Szetosztas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_RagszamSav_Szetosztas] AS' 
END
GO

ALTER procedure [dbo].[sp_KRT_RagszamSav_Szetosztas]    
                @RagszamSav_Id         uniqueidentifier,
                @FirstKod             nvarchar(100),
                @LastKod              nvarchar(100),
                @Szervezet_Id         uniqueidentifier,-- = NULL, -- jelenlegi tulajdonos szervezet
                @UjSzervezet_Id       uniqueidentifier,        -- új tulajdonos szervezet
                @ResultUid            uniqueidentifier OUTPUT
         
AS
---------
BEGIN TRY

SET NOCOUNT ON;


 
END TRY
-----------
-----------
BEGIN CATCH
   --if @sajat_tranz = 1  
   --   ROLLBACK TRANSACTION
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   --if @teszt = 1
   --   print '@myid = '+ isnull( convert(varchar(50),@myid), 'NULL')

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------

GO
