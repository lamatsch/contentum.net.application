IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_RagszamSavokGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_RagszamSavokGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_RagszamSavokGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_RagszamSavokGetAll] AS' 
END
GO

ALTER procedure [dbo].[sp_KRT_RagszamSavokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_RagszamSavok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('50202',16,1)
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   KRT_RagszamSavok.Id,
	   KRT_RagszamSavok.Org,
	   KRT_RagszamSavok.Csoport_Id_Felelos,
	   KRT_RagszamSavok.Postakonyv_Id,
	   KRT_RagszamSavok.SavKezd,
	   KRT_RagszamSavok.SavKezdNum,
	   KRT_RagszamSavok.SavVege,
	   KRT_RagszamSavok.SavVegeNum,
	   KRT_RagszamSavok.SavType,
	   KRT_RagszamSavok.SavAllapot,
	   KRT_RagszamSavok.Ver,
	   KRT_RagszamSavok.Note,
	   KRT_RagszamSavok.Stat_id,
	   KRT_RagszamSavok.ErvKezd,
	   KRT_RagszamSavok.ErvVege,
	   KRT_RagszamSavok.Letrehozo_id,
	   KRT_RagszamSavok.LetrehozasIdo,
	   KRT_RagszamSavok.Modosito_id,
	   KRT_RagszamSavok.ModositasIdo,
	   KRT_RagszamSavok.Zarolo_id,
	   KRT_RagszamSavok.ZarolasIdo,
	   KRT_RagszamSavok.Tranz_id,
	   KRT_RagszamSavok.UIAccessLog_id  
   from 
     KRT_RagszamSavok as KRT_RagszamSavok      
    Where KRT_RagszamSavok.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

GO
