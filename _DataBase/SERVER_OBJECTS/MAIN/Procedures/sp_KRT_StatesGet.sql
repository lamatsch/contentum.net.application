IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_StatesGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_StatesGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_StatesGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_StatesGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_StatesGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier
         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_States.Id,
	   KRT_States.Kod,
	   KRT_States.Nev,
	   KRT_States.Stat_Id_Szulo,
	   KRT_States.Ver,
	   KRT_States.Note,
	   KRT_States.Stat_id,
	   KRT_States.ErvKezd,
	   KRT_States.ErvVege,
	   KRT_States.Letrehozo_id,
	   KRT_States.LetrehozasIdo,
	   KRT_States.Modosito_id,
	   KRT_States.ModositasIdo,
	   KRT_States.Zarolo_id,
	   KRT_States.ZarolasIdo,
	   KRT_States.Tranz_id,
	   KRT_States.UIAccessLog_id
	   from 
		 KRT_States as KRT_States 
	   where
		 KRT_States.Id = @Id'

	exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
