
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_SzemelyekGetAll')
            and   type = 'P')
   drop procedure sp_KRT_SzemelyekGetAll
go

create procedure sp_KRT_SzemelyekGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Szemelyek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   KRT_Szemelyek.Id,
	   KRT_Szemelyek.Partner_Id,
	   KRT_Szemelyek.AnyjaNeve,
	   KRT_Szemelyek.AnyjaNeveCsaladiNev,
	   KRT_Szemelyek.AnyjaNeveElsoUtonev,
	   KRT_Szemelyek.AnyjaNeveTovabbiUtonev,
	   KRT_Szemelyek.ApjaNeve,
	   KRT_Szemelyek.SzuletesiNev,
	   KRT_Szemelyek.SzuletesiCsaladiNev,
	   KRT_Szemelyek.SzuletesiElsoUtonev,
	   KRT_Szemelyek.SzuletesiTovabbiUtonev,
	   KRT_Szemelyek.SzuletesiOrszag,
	   KRT_Szemelyek.SzuletesiOrszagId,
	   KRT_Szemelyek.UjTitulis,
	   KRT_Szemelyek.UjCsaladiNev,
	   KRT_Szemelyek.UjUtonev,
	   KRT_Szemelyek.UjTovabbiUtonev,
	   KRT_Szemelyek.SzuletesiHely,
	   KRT_Szemelyek.SzuletesiHely_id,
	   KRT_Szemelyek.SzuletesiIdo,
	   KRT_Szemelyek.Allampolgarsag,
	   KRT_Szemelyek.TAJSzam,
	   KRT_Szemelyek.SZIGSzam,
	   KRT_Szemelyek.Neme,
	   KRT_Szemelyek.SzemelyiAzonosito,
	   KRT_Szemelyek.Adoazonosito,
	   KRT_Szemelyek.Adoszam,
	   KRT_Szemelyek.KulfoldiAdoszamJelolo,
	   KRT_Szemelyek.Beosztas,
	   KRT_Szemelyek.MinositesiSzint,
	   KRT_Szemelyek.Ver,
	   KRT_Szemelyek.Note,
	   KRT_Szemelyek.Stat_id,
	   KRT_Szemelyek.ErvKezd,
	   KRT_Szemelyek.ErvVege,
	   KRT_Szemelyek.Letrehozo_id,
	   KRT_Szemelyek.LetrehozasIdo,
	   KRT_Szemelyek.Modosito_id,
	   KRT_Szemelyek.ModositasIdo,
	   KRT_Szemelyek.Zarolo_id,
	   KRT_Szemelyek.ZarolasIdo,
	   KRT_Szemelyek.Tranz_id,
	   KRT_Szemelyek.UIAccessLog_id  
   from 
     KRT_Szemelyek as KRT_Szemelyek      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go