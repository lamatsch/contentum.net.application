set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_SzemelyekHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_KRT_SzemelyekHistoryGetAllRecord
go

create procedure sp_KRT_SzemelyekHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from KRT_SzemelyekHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(Old.Partner_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(New.Partner_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Partner_Id != New.Partner_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Partnerek FTOld on FTOld.Id = Old.Partner_Id --and FTOld.Ver = Old.Ver
         left join KRT_Partnerek FTNew on FTNew.Id = New.Partner_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AnyjaNeve' as ColumnName,               cast(Old.AnyjaNeve as nvarchar(99)) as OldValue,
               cast(New.AnyjaNeve as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AnyjaNeve != New.AnyjaNeve 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AnyjaNeveCsaladiNev' as ColumnName,               cast(Old.AnyjaNeveCsaladiNev as nvarchar(99)) as OldValue,
               cast(New.AnyjaNeveCsaladiNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AnyjaNeveCsaladiNev != New.AnyjaNeveCsaladiNev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AnyjaNeveElsoUtonev' as ColumnName,               cast(Old.AnyjaNeveElsoUtonev as nvarchar(99)) as OldValue,
               cast(New.AnyjaNeveElsoUtonev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AnyjaNeveElsoUtonev != New.AnyjaNeveElsoUtonev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AnyjaNeveTovabbiUtonev' as ColumnName,               cast(Old.AnyjaNeveTovabbiUtonev as nvarchar(99)) as OldValue,
               cast(New.AnyjaNeveTovabbiUtonev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.AnyjaNeveTovabbiUtonev != New.AnyjaNeveTovabbiUtonev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ApjaNeve' as ColumnName,               cast(Old.ApjaNeve as nvarchar(99)) as OldValue,
               cast(New.ApjaNeve as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.ApjaNeve != New.ApjaNeve 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiNev' as ColumnName,               cast(Old.SzuletesiNev as nvarchar(99)) as OldValue,
               cast(New.SzuletesiNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SzuletesiNev != New.SzuletesiNev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiCsaladiNev' as ColumnName,               cast(Old.SzuletesiCsaladiNev as nvarchar(99)) as OldValue,
               cast(New.SzuletesiCsaladiNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SzuletesiCsaladiNev != New.SzuletesiCsaladiNev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiElsoUtonev' as ColumnName,               cast(Old.SzuletesiElsoUtonev as nvarchar(99)) as OldValue,
               cast(New.SzuletesiElsoUtonev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SzuletesiElsoUtonev != New.SzuletesiElsoUtonev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiTovabbiUtonev' as ColumnName,               cast(Old.SzuletesiTovabbiUtonev as nvarchar(99)) as OldValue,
               cast(New.SzuletesiTovabbiUtonev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SzuletesiTovabbiUtonev != New.SzuletesiTovabbiUtonev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiOrszag' as ColumnName,               cast(Old.SzuletesiOrszag as nvarchar(99)) as OldValue,
               cast(New.SzuletesiOrszag as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SzuletesiOrszag != New.SzuletesiOrszag 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiOrszagId' as ColumnName,               cast(Old.SzuletesiOrszagId as nvarchar(99)) as OldValue,
               cast(New.SzuletesiOrszagId as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SzuletesiOrszagId != New.SzuletesiOrszagId 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UjTitulis' as ColumnName,               cast(Old.UjTitulis as nvarchar(99)) as OldValue,
               cast(New.UjTitulis as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UjTitulis != New.UjTitulis 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UjCsaladiNev' as ColumnName,               cast(Old.UjCsaladiNev as nvarchar(99)) as OldValue,
               cast(New.UjCsaladiNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UjCsaladiNev != New.UjCsaladiNev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UjUtonev' as ColumnName,               cast(Old.UjUtonev as nvarchar(99)) as OldValue,
               cast(New.UjUtonev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UjUtonev != New.UjUtonev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UjTovabbiUtonev' as ColumnName,               cast(Old.UjTovabbiUtonev as nvarchar(99)) as OldValue,
               cast(New.UjTovabbiUtonev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UjTovabbiUtonev != New.UjTovabbiUtonev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiHely' as ColumnName,               cast(Old.SzuletesiHely as nvarchar(99)) as OldValue,
               cast(New.SzuletesiHely as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SzuletesiHely != New.SzuletesiHely 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiHely_id' as ColumnName,               cast(Old.SzuletesiHely_id as nvarchar(99)) as OldValue,
               cast(New.SzuletesiHely_id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SzuletesiHely_id != New.SzuletesiHely_id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzuletesiIdo' as ColumnName,               cast(Old.SzuletesiIdo as nvarchar(99)) as OldValue,
               cast(New.SzuletesiIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SzuletesiIdo != New.SzuletesiIdo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allampolgarsag' as ColumnName,               cast(Old.Allampolgarsag as nvarchar(99)) as OldValue,
               cast(New.Allampolgarsag as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Allampolgarsag != New.Allampolgarsag 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TAJSzam' as ColumnName,               cast(Old.TAJSzam as nvarchar(99)) as OldValue,
               cast(New.TAJSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.TAJSzam != New.TAJSzam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SZIGSzam' as ColumnName,               cast(Old.SZIGSzam as nvarchar(99)) as OldValue,
               cast(New.SZIGSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SZIGSzam != New.SZIGSzam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Neme' as ColumnName,               cast(Old.Neme as nvarchar(99)) as OldValue,
               cast(New.Neme as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Neme != New.Neme 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzemelyiAzonosito' as ColumnName,               cast(Old.SzemelyiAzonosito as nvarchar(99)) as OldValue,
               cast(New.SzemelyiAzonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SzemelyiAzonosito != New.SzemelyiAzonosito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Adoazonosito' as ColumnName,               cast(Old.Adoazonosito as nvarchar(99)) as OldValue,
               cast(New.Adoazonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Adoazonosito != New.Adoazonosito 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Adoszam' as ColumnName,               cast(Old.Adoszam as nvarchar(99)) as OldValue,
               cast(New.Adoszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Adoszam != New.Adoszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KulfoldiAdoszamJelolo' as ColumnName,               cast(Old.KulfoldiAdoszamJelolo as nvarchar(99)) as OldValue,
               cast(New.KulfoldiAdoszamJelolo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.KulfoldiAdoszamJelolo != New.KulfoldiAdoszamJelolo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Beosztas' as ColumnName,               cast(Old.Beosztas as nvarchar(99)) as OldValue,
               cast(New.Beosztas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Beosztas != New.Beosztas 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MinositesiSzint' as ColumnName,               cast(Old.MinositesiSzint as nvarchar(99)) as OldValue,
               cast(New.MinositesiSzint as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_SzemelyekHistory Old
         inner join KRT_SzemelyekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MinositesiSzint != New.MinositesiSzint 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go