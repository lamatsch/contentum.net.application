
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_KRT_SzemelyekInsert')
            and   type = 'P')
   drop procedure sp_KRT_SzemelyekInsert
go

create procedure sp_KRT_SzemelyekInsert    
                @Id      uniqueidentifier = null,    
               	            @Partner_Id     uniqueidentifier,
                @AnyjaNeve     Nvarchar(100)  = null,
                @AnyjaNeveCsaladiNev     Nvarchar(100)  = null,
                @AnyjaNeveElsoUtonev     Nvarchar(100)  = null,
                @AnyjaNeveTovabbiUtonev     Nvarchar(100)  = null,
                @ApjaNeve     Nvarchar(100)  = null,
                @SzuletesiNev     Nvarchar(100)  = null,
                @SzuletesiCsaladiNev     Nvarchar(100)  = null,
                @SzuletesiElsoUtonev     Nvarchar(100)  = null,
                @SzuletesiTovabbiUtonev     Nvarchar(100)  = null,
                @SzuletesiOrszag     Nvarchar(100)  = null,
                @SzuletesiOrszagId     uniqueidentifier  = null,
                @UjTitulis     Nvarchar(10)  = null,
                @UjCsaladiNev     Nvarchar(100)  = null,
                @UjUtonev     Nvarchar(100)  = null,
                @UjTovabbiUtonev     Nvarchar(100)  = null,
                @SzuletesiHely     Nvarchar(400)  = null,
                @SzuletesiHely_id     uniqueidentifier  = null,
                @SzuletesiIdo     datetime  = null,
                @Allampolgarsag     Nvarchar(100)  = null,
                @TAJSzam     Nvarchar(20)  = null,
                @SZIGSzam     Nvarchar(20)  = null,
                @Neme     char(1)  = null,
                @SzemelyiAzonosito     Nvarchar(20)  = null,
                @Adoazonosito     Nvarchar(20)  = null,
                @Adoszam     Nvarchar(20)  = null,
                @KulfoldiAdoszamJelolo     char(1)  = null,
                @Beosztas     Nvarchar(100)  = null,
                @MinositesiSzint     nvarchar(64)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Partner_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Partner_Id'
            SET @insertValues = @insertValues + ',@Partner_Id'
         end 
       
         if @AnyjaNeve is not null
         begin
            SET @insertColumns = @insertColumns + ',AnyjaNeve'
            SET @insertValues = @insertValues + ',@AnyjaNeve'
         end 
       
         if @AnyjaNeveCsaladiNev is not null
         begin
            SET @insertColumns = @insertColumns + ',AnyjaNeveCsaladiNev'
            SET @insertValues = @insertValues + ',@AnyjaNeveCsaladiNev'
         end 
       
         if @AnyjaNeveElsoUtonev is not null
         begin
            SET @insertColumns = @insertColumns + ',AnyjaNeveElsoUtonev'
            SET @insertValues = @insertValues + ',@AnyjaNeveElsoUtonev'
         end 
       
         if @AnyjaNeveTovabbiUtonev is not null
         begin
            SET @insertColumns = @insertColumns + ',AnyjaNeveTovabbiUtonev'
            SET @insertValues = @insertValues + ',@AnyjaNeveTovabbiUtonev'
         end 
       
         if @ApjaNeve is not null
         begin
            SET @insertColumns = @insertColumns + ',ApjaNeve'
            SET @insertValues = @insertValues + ',@ApjaNeve'
         end 
       
         if @SzuletesiNev is not null
         begin
            SET @insertColumns = @insertColumns + ',SzuletesiNev'
            SET @insertValues = @insertValues + ',@SzuletesiNev'
         end 
       
         if @SzuletesiCsaladiNev is not null
         begin
            SET @insertColumns = @insertColumns + ',SzuletesiCsaladiNev'
            SET @insertValues = @insertValues + ',@SzuletesiCsaladiNev'
         end 
       
         if @SzuletesiElsoUtonev is not null
         begin
            SET @insertColumns = @insertColumns + ',SzuletesiElsoUtonev'
            SET @insertValues = @insertValues + ',@SzuletesiElsoUtonev'
         end 
       
         if @SzuletesiTovabbiUtonev is not null
         begin
            SET @insertColumns = @insertColumns + ',SzuletesiTovabbiUtonev'
            SET @insertValues = @insertValues + ',@SzuletesiTovabbiUtonev'
         end 
       
         if @SzuletesiOrszag is not null
         begin
            SET @insertColumns = @insertColumns + ',SzuletesiOrszag'
            SET @insertValues = @insertValues + ',@SzuletesiOrszag'
         end 
       
         if @SzuletesiOrszagId is not null
         begin
            SET @insertColumns = @insertColumns + ',SzuletesiOrszagId'
            SET @insertValues = @insertValues + ',@SzuletesiOrszagId'
         end 
       
         if @UjTitulis is not null
         begin
            SET @insertColumns = @insertColumns + ',UjTitulis'
            SET @insertValues = @insertValues + ',@UjTitulis'
         end 
       
         if @UjCsaladiNev is not null
         begin
            SET @insertColumns = @insertColumns + ',UjCsaladiNev'
            SET @insertValues = @insertValues + ',@UjCsaladiNev'
         end 
       
         if @UjUtonev is not null
         begin
            SET @insertColumns = @insertColumns + ',UjUtonev'
            SET @insertValues = @insertValues + ',@UjUtonev'
         end 
       
         if @UjTovabbiUtonev is not null
         begin
            SET @insertColumns = @insertColumns + ',UjTovabbiUtonev'
            SET @insertValues = @insertValues + ',@UjTovabbiUtonev'
         end 
       
         if @SzuletesiHely is not null
         begin
            SET @insertColumns = @insertColumns + ',SzuletesiHely'
            SET @insertValues = @insertValues + ',@SzuletesiHely'
         end 
       
         if @SzuletesiHely_id is not null
         begin
            SET @insertColumns = @insertColumns + ',SzuletesiHely_id'
            SET @insertValues = @insertValues + ',@SzuletesiHely_id'
         end 
       
         if @SzuletesiIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',SzuletesiIdo'
            SET @insertValues = @insertValues + ',@SzuletesiIdo'
         end 
       
         if @Allampolgarsag is not null
         begin
            SET @insertColumns = @insertColumns + ',Allampolgarsag'
            SET @insertValues = @insertValues + ',@Allampolgarsag'
         end 
       
         if @TAJSzam is not null
         begin
            SET @insertColumns = @insertColumns + ',TAJSzam'
            SET @insertValues = @insertValues + ',@TAJSzam'
         end 
       
         if @SZIGSzam is not null
         begin
            SET @insertColumns = @insertColumns + ',SZIGSzam'
            SET @insertValues = @insertValues + ',@SZIGSzam'
         end 
       
         if @Neme is not null
         begin
            SET @insertColumns = @insertColumns + ',Neme'
            SET @insertValues = @insertValues + ',@Neme'
         end 
       
         if @SzemelyiAzonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',SzemelyiAzonosito'
            SET @insertValues = @insertValues + ',@SzemelyiAzonosito'
         end 
       
         if @Adoazonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Adoazonosito'
            SET @insertValues = @insertValues + ',@Adoazonosito'
         end 
       
         if @Adoszam is not null
         begin
            SET @insertColumns = @insertColumns + ',Adoszam'
            SET @insertValues = @insertValues + ',@Adoszam'
         end 
       
         if @KulfoldiAdoszamJelolo is not null
         begin
            SET @insertColumns = @insertColumns + ',KulfoldiAdoszamJelolo'
            SET @insertValues = @insertValues + ',@KulfoldiAdoszamJelolo'
         end 
       
         if @Beosztas is not null
         begin
            SET @insertColumns = @insertColumns + ',Beosztas'
            SET @insertValues = @insertValues + ',@Beosztas'
         end 
       
         if @MinositesiSzint is not null
         begin
            SET @insertColumns = @insertColumns + ',MinositesiSzint'
            SET @insertValues = @insertValues + ',@MinositesiSzint'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into KRT_Szemelyek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Partner_Id uniqueidentifier,@AnyjaNeve Nvarchar(100),@AnyjaNeveCsaladiNev Nvarchar(100),@AnyjaNeveElsoUtonev Nvarchar(100),@AnyjaNeveTovabbiUtonev Nvarchar(100),@ApjaNeve Nvarchar(100),@SzuletesiNev Nvarchar(100),@SzuletesiCsaladiNev Nvarchar(100),@SzuletesiElsoUtonev Nvarchar(100),@SzuletesiTovabbiUtonev Nvarchar(100),@SzuletesiOrszag Nvarchar(100),@SzuletesiOrszagId uniqueidentifier,@UjTitulis Nvarchar(10),@UjCsaladiNev Nvarchar(100),@UjUtonev Nvarchar(100),@UjTovabbiUtonev Nvarchar(100),@SzuletesiHely Nvarchar(400),@SzuletesiHely_id uniqueidentifier,@SzuletesiIdo datetime,@Allampolgarsag Nvarchar(100),@TAJSzam Nvarchar(20),@SZIGSzam Nvarchar(20),@Neme char(1),@SzemelyiAzonosito Nvarchar(20),@Adoazonosito Nvarchar(20),@Adoszam Nvarchar(20),@KulfoldiAdoszamJelolo char(1),@Beosztas Nvarchar(100),@MinositesiSzint nvarchar(64),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Partner_Id = @Partner_Id,@AnyjaNeve = @AnyjaNeve,@AnyjaNeveCsaladiNev = @AnyjaNeveCsaladiNev,@AnyjaNeveElsoUtonev = @AnyjaNeveElsoUtonev,@AnyjaNeveTovabbiUtonev = @AnyjaNeveTovabbiUtonev,@ApjaNeve = @ApjaNeve,@SzuletesiNev = @SzuletesiNev,@SzuletesiCsaladiNev = @SzuletesiCsaladiNev,@SzuletesiElsoUtonev = @SzuletesiElsoUtonev,@SzuletesiTovabbiUtonev = @SzuletesiTovabbiUtonev,@SzuletesiOrszag = @SzuletesiOrszag,@SzuletesiOrszagId = @SzuletesiOrszagId,@UjTitulis = @UjTitulis,@UjCsaladiNev = @UjCsaladiNev,@UjUtonev = @UjUtonev,@UjTovabbiUtonev = @UjTovabbiUtonev,@SzuletesiHely = @SzuletesiHely,@SzuletesiHely_id = @SzuletesiHely_id,@SzuletesiIdo = @SzuletesiIdo,@Allampolgarsag = @Allampolgarsag,@TAJSzam = @TAJSzam,@SZIGSzam = @SZIGSzam,@Neme = @Neme,@SzemelyiAzonosito = @SzemelyiAzonosito,@Adoazonosito = @Adoazonosito,@Adoszam = @Adoszam,@KulfoldiAdoszamJelolo = @KulfoldiAdoszamJelolo,@Beosztas = @Beosztas,@MinositesiSzint = @MinositesiSzint,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'KRT_Szemelyek',@ResultUid
					,'KRT_SzemelyekHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
go

/* haszn�lt errorcode-ok:
	50301:
	50302: 'Org' oszlop �rt�ke nem lehet NULL
*/
