IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Szerepkor_FunkcioGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Szerepkor_FunkcioGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Szerepkor_FunkcioGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Szerepkor_FunkcioGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Szerepkor_FunkcioGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Szerepkor_Funkcio.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Szerepkor_Funkcio.Id,
	   KRT_Szerepkor_Funkcio.Funkcio_Id,
	   KRT_Szerepkor_Funkcio.Szerepkor_Id,
	   KRT_Szerepkor_Funkcio.Ver,
	   KRT_Szerepkor_Funkcio.Note,
	   KRT_Szerepkor_Funkcio.Stat_id,
	   KRT_Szerepkor_Funkcio.ErvKezd,
	   KRT_Szerepkor_Funkcio.ErvVege,
	   KRT_Szerepkor_Funkcio.Letrehozo_id,
	   KRT_Szerepkor_Funkcio.LetrehozasIdo,
	   KRT_Szerepkor_Funkcio.Modosito_id,
	   KRT_Szerepkor_Funkcio.ModositasIdo,
	   KRT_Szerepkor_Funkcio.Zarolo_id,
	   KRT_Szerepkor_Funkcio.ZarolasIdo,
	   KRT_Szerepkor_Funkcio.Tranz_id,
	   KRT_Szerepkor_Funkcio.UIAccessLog_id  
   from 
     KRT_Szerepkor_Funkcio as KRT_Szerepkor_Funkcio      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
