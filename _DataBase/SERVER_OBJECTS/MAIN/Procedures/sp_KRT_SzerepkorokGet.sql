IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_SzerepkorokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_SzerepkorokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_SzerepkorokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_SzerepkorokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_SzerepkorokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Szerepkorok.Id,
	   KRT_Szerepkorok.Org,
	   KRT_Szerepkorok.Nev,
	   KRT_Szerepkorok.KulsoAzonositok,
	   KRT_Szerepkorok.Ver,
	   KRT_Szerepkorok.Note,
	   KRT_Szerepkorok.Stat_id,
	   KRT_Szerepkorok.ErvKezd,
	   KRT_Szerepkorok.ErvVege,
	   KRT_Szerepkorok.Letrehozo_id,
	   KRT_Szerepkorok.LetrehozasIdo,
	   KRT_Szerepkorok.Modosito_id,
	   KRT_Szerepkorok.ModositasIdo,
	   KRT_Szerepkorok.Zarolo_id,
	   KRT_Szerepkorok.ZarolasIdo,
	   KRT_Szerepkorok.Tranz_id,
	   KRT_Szerepkorok.UIAccessLog_id
	   from 
		 KRT_Szerepkorok as KRT_Szerepkorok 
	   where
		 KRT_Szerepkorok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
