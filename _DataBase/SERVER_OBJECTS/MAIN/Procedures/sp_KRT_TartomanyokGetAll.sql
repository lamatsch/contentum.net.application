IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_TartomanyokGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_TartomanyokGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_TartomanyokGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_TartomanyokGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_TartomanyokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Tartomanyok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Tartomanyok.Id,
	   KRT_Tartomanyok.TartomanyKezd,
	   KRT_Tartomanyok.TartomanyHossz,
	   KRT_Tartomanyok.FoglaltDarab,
	   KRT_Tartomanyok.Ver,
	   KRT_Tartomanyok.Note,
	   KRT_Tartomanyok.Stat_id,
	   KRT_Tartomanyok.ErvKezd,
	   KRT_Tartomanyok.ErvVege,
	   KRT_Tartomanyok.Letrehozo_id,
	   KRT_Tartomanyok.LetrehozasIdo,
	   KRT_Tartomanyok.Modosito_id,
	   KRT_Tartomanyok.ModositasIdo,
	   KRT_Tartomanyok.Zarolo_id,
	   KRT_Tartomanyok.ZarolasIdo,
	   KRT_Tartomanyok.Tranz_id,
	   KRT_Tartomanyok.UIAccessLog_id  
   from 
     KRT_Tartomanyok as KRT_Tartomanyok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
