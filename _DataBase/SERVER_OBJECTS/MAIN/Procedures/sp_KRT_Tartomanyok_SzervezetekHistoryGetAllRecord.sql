IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Tartomanyok_SzervezetekHistoryGetAllRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Tartomanyok_SzervezetekHistoryGetAllRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Tartomanyok_SzervezetekHistoryGetAllRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Tartomanyok_SzervezetekHistoryGetAllRecord] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Tartomanyok_SzervezetekHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from KRT_Tartomanyok_SzervezetekHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Tartomanyok_SzervezetekHistory Old
         inner join KRT_Tartomanyok_SzervezetekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tartomany_Id' as ColumnName,               cast(Old.Tartomany_Id as nvarchar(99)) as OldValue,
               cast(New.Tartomany_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Tartomanyok_SzervezetekHistory Old
         inner join KRT_Tartomanyok_SzervezetekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Tartomany_Id != New.Tartomany_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Felelos' as ColumnName,               cast(Old.Csoport_Id_Felelos as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Felelos as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Tartomanyok_SzervezetekHistory Old
         inner join KRT_Tartomanyok_SzervezetekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csoport_Id_Felelos != New.Csoport_Id_Felelos 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TartType' as ColumnName,               cast(Old.TartType as nvarchar(99)) as OldValue,
               cast(New.TartType as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_Tartomanyok_SzervezetekHistory Old
         inner join KRT_Tartomanyok_SzervezetekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.TartType != New.TartType 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end


GO
