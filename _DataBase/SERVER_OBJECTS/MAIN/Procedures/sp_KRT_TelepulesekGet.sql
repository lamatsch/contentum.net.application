IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_TelepulesekGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_TelepulesekGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_TelepulesekGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_TelepulesekGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_TelepulesekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Telepulesek.Id,
	   KRT_Telepulesek.Org,
	   KRT_Telepulesek.IRSZ,
	   KRT_Telepulesek.Telepules_Id_Fo,
	   KRT_Telepulesek.Nev,
	   KRT_Telepulesek.Orszag_Id,
	   KRT_Telepulesek.Megye,
	   KRT_Telepulesek.Regio,
	   KRT_Telepulesek.Ver,
	   KRT_Telepulesek.Note,
	   KRT_Telepulesek.Stat_id,
	   KRT_Telepulesek.ErvKezd,
	   KRT_Telepulesek.ErvVege,
	   KRT_Telepulesek.Letrehozo_id,
	   KRT_Telepulesek.LetrehozasIdo,
	   KRT_Telepulesek.Modosito_id,
	   KRT_Telepulesek.ModositasIdo,
	   KRT_Telepulesek.Zarolo_id,
	   KRT_Telepulesek.ZarolasIdo,
	   KRT_Telepulesek.Tranz_id,
	   KRT_Telepulesek.UIAccessLog_id
	   from 
		 KRT_Telepulesek as KRT_Telepulesek 
	   where
		 KRT_Telepulesek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
