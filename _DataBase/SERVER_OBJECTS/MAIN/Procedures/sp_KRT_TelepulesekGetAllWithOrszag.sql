IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_TelepulesekGetAllWithOrszag]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_TelepulesekGetAllWithOrszag]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_TelepulesekGetAllWithOrszag]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_TelepulesekGetAllWithOrszag] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_TelepulesekGetAllWithOrszag]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Telepulesek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @pageNumber		int = 0,
  @pageSize		int = -1,
  @SelectedRowId	uniqueidentifier = null,
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
          
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 	DECLARE @firstRow INT
	DECLARE @lastRow INT
   
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = 1000
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end
 
	/************************************************************
	* Szurt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   KRT_Telepulesek.Id into #result
		from KRT_Telepulesek as KRT_Telepulesek
     JOIN KRT_Orszagok as KRT_Orszagok
     ON KRT_Telepulesek.Orszag_Id = KRT_Orszagok.Id
     LEFT JOIN KRT_KodCsoportok as KRT_KodCsoportokMegye on KRT_KodCsoportokMegye.Kod=''MEGYE''
     LEFT JOIN KRT_KodTarak as KRT_KodTarakMegye on KRT_Telepulesek.Megye = KRT_KodTarakMegye.Kod and KRT_KodCsoportokMegye.Id = KRT_KodTarakMegye.KodCsoport_Id and KRT_KodTarakMegye.Org=@Org
	 LEFT JOIN KRT_KodCsoportok as KRT_KodCsoportokRegio on KRT_KodCsoportokRegio.Kod=''REGIO''
     LEFT JOIN KRT_KodTarak as KRT_KodTarakRegio on KRT_Telepulesek.Regio = KRT_KodTarakRegio.Kod and KRT_KodCsoportokRegio.Id = KRT_KodTarakRegio.KodCsoport_Id and KRT_KodTarakRegio.Org=@Org
 '
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end

	if (@SelectedRowId is not null)
	begin
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id = @SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	end

	set @sqlcmd = @sqlcmd + N' 
	if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
	BEGIN
		select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
		set @firstRow = (@pageNumber - 1) * @pageSize + 1;
		set @lastRow = @pageNumber*@pageSize;
		select @pageNumber = @pageNumber - 1
	END ;'

	/************************************************************
	* Tényleges select											*
	************************************************************/
	SET @sqlcmd = @sqlcmd + 
  'select 
		#result.RowNumber,
  	   KRT_Telepulesek.Id,
	   KRT_Telepulesek.Org,
	   KRT_Telepulesek.IRSZ,
	   KRT_Telepulesek.Telepules_Id_Fo,
	   KRT_Telepulesek.Nev,
	   KRT_Telepulesek.Orszag_Id,
       KRT_Orszagok.Nev as OrszagNev,
	   KRT_Telepulesek.Megye,
       KRT_KodTarakMegye.Nev as MegyeNev,
	   KRT_Telepulesek.Regio,
       KRT_KodTarakRegio.Nev as RegioNev,
	   KRT_Telepulesek.Ver,
	   KRT_Telepulesek.Note,
	   KRT_Telepulesek.Stat_id,
	   KRT_Telepulesek.ErvKezd,
	   KRT_Telepulesek.ErvVege,
	   KRT_Telepulesek.Letrehozo_id,
	   KRT_Telepulesek.LetrehozasIdo,
	   KRT_Telepulesek.Modosito_id,
	   KRT_Telepulesek.ModositasIdo,
	   KRT_Telepulesek.Zarolo_id,
	   KRT_Telepulesek.ZarolasIdo,
	   KRT_Telepulesek.Tranz_id,
	   KRT_Telepulesek.UIAccessLog_id
   from 
     KRT_Telepulesek as KRT_Telepulesek
     	inner join #result on #result.Id = KRT_Telepulesek.Id
     JOIN KRT_Orszagok as KRT_Orszagok
     ON KRT_Telepulesek.Orszag_Id = KRT_Orszagok.Id
     LEFT JOIN KRT_KodCsoportok as KRT_KodCsoportokMegye on KRT_KodCsoportokMegye.Kod=''MEGYE''
     LEFT JOIN KRT_KodTarak as KRT_KodTarakMegye on KRT_Telepulesek.Megye = KRT_KodTarakMegye.Kod and KRT_KodCsoportokMegye.Id = KRT_KodTarakMegye.KodCsoport_Id and KRT_KodTarakMegye.Org=@Org
	 LEFT JOIN KRT_KodCsoportok as KRT_KodCsoportokRegio on KRT_KodCsoportokRegio.Kod=''REGIO''
     LEFT JOIN KRT_KodTarak as KRT_KodTarakRegio on KRT_Telepulesek.Regio = KRT_KodTarakRegio.Kod and KRT_KodCsoportokRegio.Id = KRT_KodTarakRegio.KodCsoport_Id and KRT_KodTarakRegio.Org=@Org
    where RowNumber between @firstRow and @lastRow
	ORDER BY #result.RowNumber;  
'

	-- találatok száma és oldalszám
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';
 	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier, @Org uniqueidentifier',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId, @Org = @Org;


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
