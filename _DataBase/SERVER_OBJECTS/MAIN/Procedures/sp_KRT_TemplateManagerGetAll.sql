IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_TemplateManagerGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_TemplateManagerGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_TemplateManagerGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_TemplateManagerGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_TemplateManagerGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_TemplateManager.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_TemplateManager.Id,
	   KRT_TemplateManager.Nev,
	   KRT_TemplateManager.Tipus,
	   KRT_TemplateManager.KRT_Modul_Id,
	   KRT_TemplateManager.KRT_Dokumentum_Id,
	   KRT_TemplateManager.Ver,
	   KRT_TemplateManager.Note,
	   KRT_TemplateManager.Stat_id,
	   KRT_TemplateManager.ErvKezd,
	   KRT_TemplateManager.ErvVege,
	   KRT_TemplateManager.Letrehozo_id,
	   KRT_TemplateManager.LetrehozasIdo,
	   KRT_TemplateManager.Modosito_id,
	   KRT_TemplateManager.ModositasIdo,
	   KRT_TemplateManager.Zarolo_id,
	   KRT_TemplateManager.ZarolasIdo,
	   KRT_TemplateManager.Tranz_id,
	   KRT_TemplateManager.UIAccessLog_id  
   from 
     KRT_TemplateManager as KRT_TemplateManager      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
