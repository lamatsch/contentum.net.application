IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Tranz_ObjGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Tranz_ObjGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Tranz_ObjGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Tranz_ObjGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Tranz_ObjGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Tranz_Obj.Id,
	   KRT_Tranz_Obj.ObjTip_Id_AdatElem,
	   KRT_Tranz_Obj.Obj_Id,
	   KRT_Tranz_Obj.Ver,
	   KRT_Tranz_Obj.Note,
	   KRT_Tranz_Obj.Stat_id,
	   KRT_Tranz_Obj.ErvKezd,
	   KRT_Tranz_Obj.ErvVege,
	   KRT_Tranz_Obj.Letrehozo_id,
	   KRT_Tranz_Obj.LetrehozasIdo,
	   KRT_Tranz_Obj.Modosito_id,
	   KRT_Tranz_Obj.ModositasIdo,
	   KRT_Tranz_Obj.Zarolo_id,
	   KRT_Tranz_Obj.ZarolasIdo,
	   KRT_Tranz_Obj.Tranz_id,
	   KRT_Tranz_Obj.UIAccessLog_id
	   from 
		 KRT_Tranz_Obj as KRT_Tranz_Obj 
	   where
		 KRT_Tranz_Obj.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
