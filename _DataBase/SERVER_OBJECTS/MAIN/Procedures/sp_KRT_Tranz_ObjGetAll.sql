IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Tranz_ObjGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_Tranz_ObjGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_Tranz_ObjGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_Tranz_ObjGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_Tranz_ObjGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_Tranz_Obj.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   KRT_Tranz_Obj.Id,
	   KRT_Tranz_Obj.ObjTip_Id_AdatElem,
	   KRT_Tranz_Obj.Obj_Id,
	   KRT_Tranz_Obj.Ver,
	   KRT_Tranz_Obj.Note,
	   KRT_Tranz_Obj.Stat_id,
	   KRT_Tranz_Obj.ErvKezd,
	   KRT_Tranz_Obj.ErvVege,
	   KRT_Tranz_Obj.Letrehozo_id,
	   KRT_Tranz_Obj.LetrehozasIdo,
	   KRT_Tranz_Obj.Modosito_id,
	   KRT_Tranz_Obj.ModositasIdo,
	   KRT_Tranz_Obj.Zarolo_id,
	   KRT_Tranz_Obj.ZarolasIdo,
	   KRT_Tranz_Obj.Tranz_id,
	   KRT_Tranz_Obj.UIAccessLog_id  
   from 
     KRT_Tranz_Obj as KRT_Tranz_Obj      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
