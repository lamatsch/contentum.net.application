IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_UIMezoObjektumErtekekGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_UIMezoObjektumErtekekGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_UIMezoObjektumErtekekGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_UIMezoObjektumErtekekGetAll] AS' 
END
GO

ALTER procedure [dbo].[sp_KRT_UIMezoObjektumErtekekGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   KRT_UIMezoObjektumErtekek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   KRT_UIMezoObjektumErtekek.Id,
	   KRT_UIMezoObjektumErtekek.TemplateTipusNev,
	   KRT_UIMezoObjektumErtekek.Nev,
	   KRT_UIMezoObjektumErtekek.Alapertelmezett,
	   KRT_UIMezoObjektumErtekek.Felhasznalo_Id,
	   KRT_UIMezoObjektumErtekek.TemplateXML,
	   KRT_UIMezoObjektumErtekek.UtolsoHasznIdo,
	   KRT_UIMezoObjektumErtekek.Org_Id,
	   KRT_UIMezoObjektumErtekek.Publikus,
	   KRT_UIMezoObjektumErtekek.Szervezet_Id,
	   KRT_UIMezoObjektumErtekek.Ver,
	   KRT_UIMezoObjektumErtekek.Note,
	   KRT_UIMezoObjektumErtekek.Stat_id,
	   KRT_UIMezoObjektumErtekek.ErvKezd,
	   KRT_UIMezoObjektumErtekek.ErvVege,
	   KRT_UIMezoObjektumErtekek.Letrehozo_id,
	   KRT_UIMezoObjektumErtekek.LetrehozasIdo,
	   KRT_UIMezoObjektumErtekek.Modosito_id,
	   KRT_UIMezoObjektumErtekek.ModositasIdo,
	   KRT_UIMezoObjektumErtekek.Zarolo_id,
	   KRT_UIMezoObjektumErtekek.ZarolasIdo,
	   KRT_UIMezoObjektumErtekek.Tranz_id,
	   KRT_UIMezoObjektumErtekek.UIAccessLog_id  
   from 
     KRT_UIMezoObjektumErtekek as KRT_UIMezoObjektumErtekek      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
