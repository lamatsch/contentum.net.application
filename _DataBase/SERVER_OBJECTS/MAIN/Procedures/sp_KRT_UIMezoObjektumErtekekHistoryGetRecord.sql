IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_UIMezoObjektumErtekekHistoryGetRecord]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_UIMezoObjektumErtekekHistoryGetRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_UIMezoObjektumErtekekHistoryGetRecord]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_UIMezoObjektumErtekekHistoryGetRecord] AS' 
END
GO

ALTER procedure [dbo].[sp_KRT_UIMezoObjektumErtekekHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from KRT_UIMezoObjektumErtekekHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and KRT_UIMezoObjektumErtekekHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_UIMezoObjektumErtekekHistory Old
         inner join KRT_UIMezoObjektumErtekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TemplateTipusNev' as ColumnName,               
               cast(Old.TemplateTipusNev as nvarchar(99)) as OldValue,
               cast(New.TemplateTipusNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_UIMezoObjektumErtekekHistory Old
         inner join KRT_UIMezoObjektumErtekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.TemplateTipusNev != New.TemplateTipusNev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Nev' as ColumnName,               
               cast(Old.Nev as nvarchar(99)) as OldValue,
               cast(New.Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_UIMezoObjektumErtekekHistory Old
         inner join KRT_UIMezoObjektumErtekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Nev != New.Nev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felhasznalo_Id' as ColumnName,               
               cast(Old.Felhasznalo_Id as nvarchar(99)) as OldValue,
               cast(New.Felhasznalo_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_UIMezoObjektumErtekekHistory Old
         inner join KRT_UIMezoObjektumErtekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Felhasznalo_Id != New.Felhasznalo_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TemplateXML' as ColumnName,               
               cast(Old.TemplateXML as nvarchar(99)) as OldValue,
               cast(New.TemplateXML as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_UIMezoObjektumErtekekHistory Old
         inner join KRT_UIMezoObjektumErtekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.TemplateXML != New.TemplateXML 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UtolsoHasznIdo' as ColumnName,               
               cast(Old.UtolsoHasznIdo as nvarchar(99)) as OldValue,
               cast(New.UtolsoHasznIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_UIMezoObjektumErtekekHistory Old
         inner join KRT_UIMezoObjektumErtekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UtolsoHasznIdo != New.UtolsoHasznIdo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)
	  union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Org_Id' as ColumnName,               
               cast(Old.Org_Id as nvarchar(99)) as OldValue,
               cast(New.Org_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_UIMezoObjektumErtekekHistory Old
         inner join KRT_UIMezoObjektumErtekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Org_Id != New.Org_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)    
	  union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Publikus' as ColumnName,               
               cast(Old.Publikus as nvarchar(99)) as OldValue,
               cast(New.Publikus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from KRT_UIMezoObjektumErtekekHistory Old
         inner join KRT_UIMezoObjektumErtekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Publikus != New.Publikus 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)     
end


GO
