IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_UIMezoObjektumErtekek_MegosztasWithSzemelyOrSzervezet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_UIMezoObjektumErtekek_MegosztasWithSzemelyOrSzervezet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_UIMezoObjektumErtekek_MegosztasWithSzemelyOrSzervezet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_UIMezoObjektumErtekek_MegosztasWithSzemelyOrSzervezet] AS' 
END
GO

ALTER procedure [dbo].[sp_KRT_UIMezoObjektumErtekek_MegosztasWithSzemelyOrSzervezet]
  @Id uniqueidentifier,
  @FelhasznaloId uniqueidentifier = null,
  @Org_Id uniqueidentifier = null,
  @Publikus char(1) = '0',
  @Szervezet_Id uniqueidentifier = null,

  @Stat_id     uniqueidentifier  = null,
  @ErvKezd     datetime  = null,
  @ErvVege     datetime  = null,
  @Letrehozo_id     uniqueidentifier  = null,
  @LetrehozasIdo     datetime,
  @Modosito_id     uniqueidentifier  = null,
  @ModositasIdo     datetime  = null,
  @Tranz_id     uniqueidentifier  = null,
  @UIAccessLog_id     uniqueidentifier  = null,
  @ResultUid            uniqueidentifier OUTPUT
as

begin

BEGIN TRY
   set nocount on
   
   
   if (@Publikus = '0')
   begin
      DECLARE @InsertCommand NVARCHAR(4000)
	  DECLARE @InsertedRow TABLE (id uniqueidentifier)
	  insert into [dbo].[KRT_UIMezoObjektumErtekek] (
           [TemplateTipusNev]
           ,[Nev]
           ,[Alapertelmezett]
           ,[Felhasznalo_Id]
           ,[TemplateXML]
           ,[UtolsoHasznIdo]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id]
		   ,[Org_Id]
		   ,[Szervezet_Id]
           ) output inserted.id into @InsertedRow 
		select 
           [TemplateTipusNev]
           ,[Nev]
           ,[Alapertelmezett]
           ,@FelhasznaloId
           ,[TemplateXML]
           ,NULL
           ,'1'
           ,[Note]
           ,@Stat_id
           ,@ErvKezd
           ,@ErvVege
           ,@Letrehozo_id
           ,@LetrehozasIdo
           ,@Modosito_id
           ,@ModositasIdo
           ,@Tranz_id
           ,@UIAccessLog_id
		   ,@Org_Id
		   ,@Szervezet_Id
	  from  [dbo].[KRT_UIMezoObjektumErtekek] where id=@Id
	  select @ResultUid = id from @InsertedRow
   end
   if (@Publikus = '1')
   begin
	update [dbo].[KRT_UIMezoObjektumErtekek]
		set Publikus = @Publikus, Org_Id = @Org_Id, Szervezet_Id = @Szervezet_Id
	 where Id = @Id
	SET @ResultUid = @Id
   end
   

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
