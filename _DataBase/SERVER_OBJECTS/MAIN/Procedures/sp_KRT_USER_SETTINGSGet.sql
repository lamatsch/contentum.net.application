IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_USER_SETTINGSGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_USER_SETTINGSGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_USER_SETTINGSGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_USER_SETTINGSGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_USER_SETTINGSGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier
         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_USER_SETTINGS.Id,
	   KRT_USER_SETTINGS.Csoport_Id,
	   KRT_USER_SETTINGS.BEALLITASTIPUS,
	   KRT_USER_SETTINGS.BEALLITASOK_XML,
	   KRT_USER_SETTINGS.Ver,
	   KRT_USER_SETTINGS.Note,
	   KRT_USER_SETTINGS.Stat_id,
	   KRT_USER_SETTINGS.ErvKezd,
	   KRT_USER_SETTINGS.ErvVege,
	   KRT_USER_SETTINGS.Letrehozo_id,
	   KRT_USER_SETTINGS.LetrehozasIdo,
	   KRT_USER_SETTINGS.Modosito_id,
	   KRT_USER_SETTINGS.ModositasIdo,
	   KRT_USER_SETTINGS.Zarolo_id,
	   KRT_USER_SETTINGS.ZarolasIdo,
	   KRT_USER_SETTINGS.Tranz_id,
	   KRT_USER_SETTINGS.UIAccessLog_id
	   from 
		 KRT_USER_SETTINGS as KRT_USER_SETTINGS 
	   where
		 KRT_USER_SETTINGS.Id = @Id'

	exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
