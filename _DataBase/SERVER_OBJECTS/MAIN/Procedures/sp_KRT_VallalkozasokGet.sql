IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_VallalkozasokGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KRT_VallalkozasokGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KRT_VallalkozasokGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KRT_VallalkozasokGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KRT_VallalkozasokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   KRT_Vallalkozasok.Id,
	   KRT_Vallalkozasok.Partner_Id,
	   KRT_Vallalkozasok.Adoszam,
	   KRT_Vallalkozasok.KulfoldiAdoszamJelolo,
	   KRT_Vallalkozasok.TB_Torzsszam,
	   KRT_Vallalkozasok.Cegjegyzekszam,
	   KRT_Vallalkozasok.Tipus,
	   KRT_Vallalkozasok.Ver,
	   KRT_Vallalkozasok.Note,
	   KRT_Vallalkozasok.Stat_id,
	   KRT_Vallalkozasok.ErvKezd,
	   KRT_Vallalkozasok.ErvVege,
	   KRT_Vallalkozasok.Letrehozo_id,
	   KRT_Vallalkozasok.LetrehozasIdo,
	   KRT_Vallalkozasok.Modosito_id,
	   KRT_Vallalkozasok.ModositasIdo,
	   KRT_Vallalkozasok.Zarolo_id,
	   KRT_Vallalkozasok.ZarolasIdo,
	   KRT_Vallalkozasok.Tranz_id,
	   KRT_Vallalkozasok.UIAccessLog_id
	   from 
		 KRT_Vallalkozasok as KRT_Vallalkozasok 
	   where
		 KRT_Vallalkozasok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
