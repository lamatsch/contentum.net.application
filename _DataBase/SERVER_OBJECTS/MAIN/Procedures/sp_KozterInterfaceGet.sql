IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KozterInterfaceGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KozterInterfaceGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KozterInterfaceGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KozterInterfaceGet] AS' 
END
GO
ALTER procedure [dbo].[sp_KozterInterfaceGet]
	 @ktdok_id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
		tbl_KozterInterface.ktdok_id,
		tbl_KozterInterface.ktdok_kod,
		tbl_KozterInterface.ktdok_iktszam,
		tbl_KozterInterface.ktdok_vonalkod,
		tbl_KozterInterface.ktdok_dokid,
		tbl_KozterInterface.ktdok_url,
		tbl_KozterInterface.ktdok_date,
		tbl_KozterInterface.ktdok_siker,
		tbl_KozterInterface.ktdok_ok,
		tbl_KozterInterface.ktdok_atvet
	   from 
		 tbl_KozterInterface as tbl_KozterInterface 
	   where
		 tbl_KozterInterface.ktdok_id = ''' + cast(@ktdok_id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
