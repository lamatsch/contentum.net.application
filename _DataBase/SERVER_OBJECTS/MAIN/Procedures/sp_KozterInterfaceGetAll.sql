IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KozterInterfaceGetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KozterInterfaceGetAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KozterInterfaceGetAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KozterInterfaceGetAll] AS' 
END
GO
ALTER procedure [dbo].[sp_KozterInterfaceGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = '',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
		tbl_KozterInterface.ktdok_id,
		tbl_KozterInterface.ktdok_kod,
		tbl_KozterInterface.ktdok_iktszam,
		tbl_KozterInterface.ktdok_vonalkod,
		tbl_KozterInterface.ktdok_dokid,
		tbl_KozterInterface.ktdok_url,
		tbl_KozterInterface.ktdok_date,
		tbl_KozterInterface.ktdok_siker,
		tbl_KozterInterface.ktdok_ok,
		tbl_KozterInterface.ktdok_atvet
   from 
     tbl_KozterInterface as tbl_KozterInterface      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
