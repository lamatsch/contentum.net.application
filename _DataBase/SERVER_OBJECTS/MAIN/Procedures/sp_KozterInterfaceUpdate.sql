IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KozterInterfaceUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_KozterInterfaceUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_KozterInterfaceUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_KozterInterfaceUpdate] AS' 
END
GO
ALTER procedure [dbo].[sp_KozterInterfaceUpdate]
             @ktdok_id uniqueidentifier,		
	         @ExecutorUserId uniqueidentifier,
             @ExecutionTime datetime,
             @ktdok_kod     Nvarchar(9)  = null,
             @ktdok_iktszam     Nvarchar(14)  = null,
             @ktdok_vonalkod     Nvarchar(13)  = null,
             @ktdok_dokid     uniqueidentifier  = null,
             @ktdok_url     Nvarchar(1000)  = null,
             @ktdok_date     datetime  = null,
             @ktdok_siker     char(1)  = null,
             @ktdok_ok     Nvarchar(256)  = null,
             @ktdok_atvet     datetime  = null,
             @UpdatedColumns  xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_ktdok_kod     Nvarchar(9)
     DECLARE @Act_ktdok_iktszam     Nvarchar(14)
     DECLARE @Act_ktdok_vonalkod     Nvarchar(13)
     DECLARE @Act_ktdok_dokid     uniqueidentifier
     DECLARE @Act_ktdok_url     Nvarchar(1000)
     DECLARE @Act_ktdok_date     datetime
     DECLARE @Act_ktdok_siker     char(1)
     DECLARE @Act_ktdok_ok     Nvarchar(256)
     DECLARE @Act_ktdok_atvet     datetime
  
set nocount on

select 
     @Act_ktdok_kod = ktdok_kod,
     @Act_ktdok_iktszam = ktdok_iktszam,
     @Act_ktdok_vonalkod = ktdok_vonalkod,
     @Act_ktdok_dokid = ktdok_dokid,
     @Act_ktdok_url = ktdok_url,
     @Act_ktdok_date = ktdok_date,
     @Act_ktdok_siker = ktdok_siker,
     @Act_ktdok_ok = ktdok_ok,
     @Act_ktdok_atvet = ktdok_atvet
from tbl_KozterInterface
where ktdok_id = @ktdok_id


   IF @UpdatedColumns.exist('/root/ktdok_kod')=1
         SET @Act_ktdok_kod = @ktdok_kod
   IF @UpdatedColumns.exist('/root/ktdok_iktszam')=1
         SET @Act_ktdok_iktszam = @ktdok_iktszam
   IF @UpdatedColumns.exist('/root/ktdok_vonalkod')=1
         SET @Act_ktdok_vonalkod = @ktdok_vonalkod
   IF @UpdatedColumns.exist('/root/ktdok_dokid')=1
         SET @Act_ktdok_dokid = @ktdok_dokid
   IF @UpdatedColumns.exist('/root/ktdok_url')=1
         SET @Act_ktdok_url = @ktdok_url
   IF @UpdatedColumns.exist('/root/ktdok_date')=1
         SET @Act_ktdok_date = @ktdok_date
   IF @UpdatedColumns.exist('/root/ktdok_siker')=1
         SET @Act_ktdok_siker = @ktdok_siker
   IF @UpdatedColumns.exist('/root/ktdok_ok')=1
         SET @Act_ktdok_ok = @ktdok_ok
   IF @UpdatedColumns.exist('/root/ktdok_atvet')=1
         SET @Act_ktdok_atvet = @ktdok_atvet
   

UPDATE tbl_KozterInterface
SET
  ktdok_kod=@Act_ktdok_kod,
  ktdok_iktszam=@Act_ktdok_iktszam,
  ktdok_vonalkod=@Act_ktdok_vonalkod,
  ktdok_dokid=@Act_ktdok_dokid,
  ktdok_url=@Act_ktdok_url,
  ktdok_date=@Act_ktdok_date,
  ktdok_siker=@Act_ktdok_siker,
  ktdok_ok=@Act_ktdok_ok,
  ktdok_atvet=@Act_ktdok_atvet
where
  ktdok_id = @ktdok_id

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end

--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH


GO
