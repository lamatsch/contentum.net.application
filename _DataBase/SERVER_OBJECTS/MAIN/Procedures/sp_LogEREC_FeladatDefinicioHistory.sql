IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_FeladatDefinicioHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LogEREC_FeladatDefinicioHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_FeladatDefinicioHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_LogEREC_FeladatDefinicioHistory] AS' 
END
GO
ALTER procedure [dbo].[sp_LogEREC_FeladatDefinicioHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_FeladatDefinicio where Id = @Row_Id;
      
   insert into EREC_FeladatDefinicioHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Org     
 ,Funkcio_Id_Kivalto     
 ,Obj_Metadefinicio_Id     
 ,ObjStateValue_Id     
 ,FeladatDefinicioTipus     
 ,FeladatSorszam     
 ,MuveletDefinicio     
 ,SpecMuveletDefinicio     
 ,Funkcio_Id_Inditando     
 ,Funkcio_Id_Futtatando     
 ,BazisObjLeiro     
 ,FelelosTipus     
 ,Obj_Id_Felelos     
 ,FelelosLeiro     
 ,Idobazis     
 ,ObjTip_Id_DateCol     
 ,AtfutasiIdo     
 ,Idoegyseg     
 ,FeladatLeiras     
 ,Tipus     
 ,Prioritas     
 ,LezarasPrioritas     
 ,Allapot     
 ,ObjTip_Id     
 ,Obj_type     
 ,Obj_SzukitoFeltetel     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Org          
 ,Funkcio_Id_Kivalto          
 ,Obj_Metadefinicio_Id          
 ,ObjStateValue_Id          
 ,FeladatDefinicioTipus          
 ,FeladatSorszam          
 ,MuveletDefinicio          
 ,SpecMuveletDefinicio          
 ,Funkcio_Id_Inditando          
 ,Funkcio_Id_Futtatando          
 ,BazisObjLeiro          
 ,FelelosTipus          
 ,Obj_Id_Felelos          
 ,FelelosLeiro          
 ,Idobazis          
 ,ObjTip_Id_DateCol          
 ,AtfutasiIdo          
 ,Idoegyseg          
 ,FeladatLeiras          
 ,Tipus          
 ,Prioritas          
 ,LezarasPrioritas          
 ,Allapot          
 ,ObjTip_Id          
 ,Obj_type          
 ,Obj_SzukitoFeltetel          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
