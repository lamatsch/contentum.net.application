IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_HataridosFeladatokHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LogEREC_HataridosFeladatokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_HataridosFeladatokHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_LogEREC_HataridosFeladatokHistory] AS' 
END
GO
ALTER procedure [dbo].[sp_LogEREC_HataridosFeladatokHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_HataridosFeladatok where Id = @Row_Id;
      
   insert into EREC_HataridosFeladatokHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Esemeny_Id_Kivalto     
 ,Esemeny_Id_Lezaro     
 ,Funkcio_Id_Inditando     
 ,Csoport_Id_Felelos     
 ,Felhasznalo_Id_Felelos     
 ,Csoport_Id_Kiado     
 ,Felhasznalo_Id_Kiado     
 ,HataridosFeladat_Id     
 ,ReszFeladatSorszam     
 ,FeladatDefinicio_Id     
 ,FeladatDefinicio_Id_Lezaro     
 ,Forras     
 ,Tipus     
 ,Altipus     
 ,Memo     
 ,Leiras     
 ,Prioritas     
 ,LezarasPrioritas     
 ,KezdesiIdo     
 ,IntezkHatarido     
 ,LezarasDatuma     
 ,Azonosito     
 ,Obj_Id     
 ,ObjTip_Id     
 ,Obj_type     
 ,Allapot     
 ,Megoldas     
 ,Note     
 ,Ver     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Esemeny_Id_Kivalto          
 ,Esemeny_Id_Lezaro          
 ,Funkcio_Id_Inditando          
 ,Csoport_Id_Felelos          
 ,Felhasznalo_Id_Felelos          
 ,Csoport_Id_Kiado          
 ,Felhasznalo_Id_Kiado          
 ,HataridosFeladat_Id          
 ,ReszFeladatSorszam          
 ,FeladatDefinicio_Id          
 ,FeladatDefinicio_Id_Lezaro          
 ,Forras          
 ,Tipus          
 ,Altipus          
 ,Memo          
 ,Leiras          
 ,Prioritas          
 ,LezarasPrioritas          
 ,KezdesiIdo          
 ,IntezkHatarido          
 ,LezarasDatuma          
 ,Azonosito          
 ,Obj_Id          
 ,ObjTip_Id          
 ,Obj_type          
 ,Allapot          
 ,Megoldas          
 ,Note          
 ,Ver          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
