IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_IraIratokDokumentumokHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LogEREC_IraIratokDokumentumokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_IraIratokDokumentumokHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_LogEREC_IraIratokDokumentumokHistory] AS' 
END
GO
ALTER procedure [dbo].[sp_LogEREC_IraIratokDokumentumokHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_IraIratokDokumentumok where Id = @Row_Id;
          declare @Dokumentum_Id_Ver int;
       --select @Dokumentum_Id_Ver = max(Ver) from KRT_Dokumentumok where Id in (select Dokumentum_Id from #tempTable);       
       declare @IraIrat_Id_Ver int;
       --select @IraIrat_Id_Ver = max(Ver) from EREC_IraIratok where Id in (select IraIrat_Id from #tempTable);   
   insert into EREC_IraIratokDokumentumokHistory(HistoryId,HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Dokumentum_Id       
 ,Dokumentum_Id_Ver     
 ,IraIrat_Id       
 ,IraIrat_Id_Ver     
 ,Leiras     
 ,Lapszam     
 ,BarCode     
 ,Forras     
 ,Formatum     
 ,Vonalkodozas     
 ,DokumentumSzerep     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select newid(),@Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Dokumentum_Id            
 ,@Dokumentum_Id_Ver     
 ,IraIrat_Id            
 ,@IraIrat_Id_Ver     
 ,Leiras          
 ,Lapszam          
 ,BarCode          
 ,Forras          
 ,Formatum          
 ,Vonalkodozas          
 ,DokumentumSzerep          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
