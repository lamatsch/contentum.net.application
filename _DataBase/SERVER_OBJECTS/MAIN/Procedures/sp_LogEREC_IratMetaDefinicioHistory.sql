IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_IratMetaDefinicioHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LogEREC_IratMetaDefinicioHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_IratMetaDefinicioHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_LogEREC_IratMetaDefinicioHistory] AS' 
END
GO
ALTER procedure [dbo].[sp_LogEREC_IratMetaDefinicioHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_IratMetaDefinicio where Id = @Row_Id;
      
   insert into EREC_IratMetaDefinicioHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Org     
 ,Ugykor_Id     
 ,UgykorKod     
 ,Ugytipus     
 ,UgytipusNev     
 ,EljarasiSzakasz     
 ,Irattipus     
 ,GeneraltTargy     
 ,Rovidnev     
 ,UgyFajta     
 ,UgyiratIntezesiIdo     
 ,Idoegyseg     
 ,UgyiratIntezesiIdoKotott     
 ,UgyiratHataridoKitolas     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Org          
 ,Ugykor_Id          
 ,UgykorKod          
 ,Ugytipus          
 ,UgytipusNev          
 ,EljarasiSzakasz          
 ,Irattipus          
 ,GeneraltTargy          
 ,Rovidnev          
 ,UgyFajta          
 ,UgyiratIntezesiIdo          
 ,Idoegyseg          
 ,UgyiratIntezesiIdoKotott          
 ,UgyiratHataridoKitolas          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
