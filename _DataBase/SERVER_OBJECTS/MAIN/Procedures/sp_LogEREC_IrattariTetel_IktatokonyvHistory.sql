IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_IrattariTetel_IktatokonyvHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LogEREC_IrattariTetel_IktatokonyvHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_IrattariTetel_IktatokonyvHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_LogEREC_IrattariTetel_IktatokonyvHistory] AS' 
END
GO
ALTER procedure [dbo].[sp_LogEREC_IrattariTetel_IktatokonyvHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_IrattariTetel_Iktatokonyv where Id = @Row_Id;
          declare @IrattariTetel_Id_Ver int;
       -- select @IrattariTetel_Id_Ver = max(Ver) from EREC_IraIrattariTetelek where Id in (select IrattariTetel_Id from #tempTable);
              declare @Iktatokonyv_Id_Ver int;
       -- select @Iktatokonyv_Id_Ver = max(Ver) from EREC_IraIktatoKonyvek where Id in (select Iktatokonyv_Id from #tempTable);
          
   insert into EREC_IrattariTetel_IktatokonyvHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,IrattariTetel_Id       
 ,IrattariTetel_Id_Ver     
 ,Iktatokonyv_Id       
 ,Iktatokonyv_Id_Ver     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,IrattariTetel_Id            
 ,@IrattariTetel_Id_Ver     
 ,Iktatokonyv_Id            
 ,@Iktatokonyv_Id_Ver     
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
