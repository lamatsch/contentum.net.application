
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_LogEREC_KuldKuldemenyekHistory')
            and   type = 'P')
   drop procedure sp_LogEREC_KuldKuldemenyekHistory
go

create procedure sp_LogEREC_KuldKuldemenyekHistory
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_KuldKuldemenyek where Id = @Row_Id;
          declare @IraIktatokonyv_Id_Ver int;
       -- select @IraIktatokonyv_Id_Ver = max(Ver) from EREC_IraIktatoKonyvek where Id in (select IraIktatokonyv_Id from #tempTable);
              declare @KuldKuldemeny_Id_Szulo_Ver int;
       -- select @KuldKuldemeny_Id_Szulo_Ver = max(Ver) from EREC_KuldKuldemenyek where Id in (select KuldKuldemeny_Id_Szulo from #tempTable);
              declare @Csoport_Id_Cimzett_Ver int;
       -- select @Csoport_Id_Cimzett_Ver = max(Ver) from KRT_Csoportok where Id in (select Csoport_Id_Cimzett from #tempTable);
              declare @Csoport_Id_Felelos_Ver int;
       -- select @Csoport_Id_Felelos_Ver = max(Ver) from KRT_Csoportok where Id in (select Csoport_Id_Felelos from #tempTable);
              declare @FelhasznaloCsoport_Id_Orzo_Ver int;
       -- select @FelhasznaloCsoport_Id_Orzo_Ver = max(Ver) from KRT_Csoportok where Id in (select FelhasznaloCsoport_Id_Orzo from #tempTable);
              declare @FelhasznaloCsoport_Id_Atvevo_Ver int;
       -- select @FelhasznaloCsoport_Id_Atvevo_Ver = max(Ver) from KRT_Csoportok where Id in (select FelhasznaloCsoport_Id_Atvevo from #tempTable);
              declare @Partner_Id_Bekuldo_Ver int;
       -- select @Partner_Id_Bekuldo_Ver = max(Ver) from KRT_Partnerek where Id in (select Partner_Id_Bekuldo from #tempTable);
              declare @Cim_Id_Ver int;
       -- select @Cim_Id_Ver = max(Ver) from KRT_Cimek where Id in (select Cim_Id from #tempTable);
              declare @FelhasznaloCsoport_Id_Alairo_Ver int;
       -- select @FelhasznaloCsoport_Id_Alairo_Ver = max(Ver) from KRT_Csoportok where Id in (select FelhasznaloCsoport_Id_Alairo from #tempTable);
              declare @FelhasznaloCsoport_Id_Bonto_Ver int;
       -- select @FelhasznaloCsoport_Id_Bonto_Ver = max(Ver) from KRT_Csoportok where Id in (select FelhasznaloCsoport_Id_Bonto from #tempTable);
              declare @CsoportFelelosEloszto_Id_Ver int;
       -- select @CsoportFelelosEloszto_Id_Ver = max(Ver) from KRT_Csoportok where Id in (select CsoportFelelosEloszto_Id from #tempTable);
              declare @Csoport_Id_Felelos_Elozo_Ver int;
       -- select @Csoport_Id_Felelos_Elozo_Ver = max(Ver) from KRT_Csoportok where Id in (select Csoport_Id_Felelos_Elozo from #tempTable);
          
   insert into EREC_KuldKuldemenyekHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Allapot     
 ,BeerkezesIdeje     
 ,FelbontasDatuma     
 ,IraIktatokonyv_Id       
 ,IraIktatokonyv_Id_Ver     
 ,KuldesMod     
 ,Erkezteto_Szam     
 ,HivatkozasiSzam     
 ,Targy     
 ,Tartalom     
 ,RagSzam     
 ,Surgosseg     
 ,BelyegzoDatuma     
 ,UgyintezesModja     
 ,PostazasIranya     
 ,Tovabbito     
 ,PeldanySzam     
 ,IktatniKell     
 ,Iktathato     
 ,SztornirozasDat     
 ,KuldKuldemeny_Id_Szulo       
 ,KuldKuldemeny_Id_Szulo_Ver     
 ,Erkeztetes_Ev     
 ,Csoport_Id_Cimzett       
 ,Csoport_Id_Cimzett_Ver     
 ,Csoport_Id_Felelos       
 ,Csoport_Id_Felelos_Ver     
 ,FelhasznaloCsoport_Id_Expedial     
 ,ExpedialasIdeje     
 ,FelhasznaloCsoport_Id_Orzo       
 ,FelhasznaloCsoport_Id_Orzo_Ver     
 ,FelhasznaloCsoport_Id_Atvevo       
 ,FelhasznaloCsoport_Id_Atvevo_Ver     
 ,Partner_Id_Bekuldo       
 ,Partner_Id_Bekuldo_Ver     
 ,Cim_Id       
 ,Cim_Id_Ver     
 ,CimSTR_Bekuldo     
 ,NevSTR_Bekuldo     
 ,AdathordozoTipusa     
 ,ElsodlegesAdathordozoTipusa     
 ,FelhasznaloCsoport_Id_Alairo       
 ,FelhasznaloCsoport_Id_Alairo_Ver     
 ,BarCode     
 ,FelhasznaloCsoport_Id_Bonto       
 ,FelhasznaloCsoport_Id_Bonto_Ver     
 ,CsoportFelelosEloszto_Id       
 ,CsoportFelelosEloszto_Id_Ver     
 ,Csoport_Id_Felelos_Elozo       
 ,Csoport_Id_Felelos_Elozo_Ver     
 ,Kovetkezo_Felelos_Id     
 ,Elektronikus_Kezbesitesi_Allap     
 ,Kovetkezo_Orzo_Id     
 ,Fizikai_Kezbesitesi_Allapot     
 ,IraIratok_Id     
 ,BontasiMegjegyzes     
 ,Tipus     
 ,Minosites     
 ,MegtagadasIndoka     
 ,Megtagado_Id     
 ,MegtagadasDat     
 ,KimenoKuldemenyFajta     
 ,Elsobbsegi     
 ,Ajanlott     
 ,Tertiveveny     
 ,SajatKezbe     
 ,E_ertesites     
 ,E_elorejelzes     
 ,PostaiLezaroSzolgalat     
 ,Ar     
 ,KimenoKuld_Sorszam      
 ,Ver     
 ,TovabbitasAlattAllapot     
 ,Azonosito     
 ,BoritoTipus     
 ,MegorzesJelzo     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id     
 ,IktatastNemIgenyel     
 ,KezbesitesModja     
 ,Munkaallomas     
 ,SerultKuldemeny     
 ,TevesCimzes     
 ,TevesErkeztetes     
 ,CimzesTipusa     
 ,FutarJegyzekListaSzama     
 ,Minosito     
 ,MinositesErvenyessegiIdeje     
 ,TerjedelemMennyiseg     
 ,TerjedelemMennyisegiEgyseg     
 ,TerjedelemMegjegyzes   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Allapot          
 ,BeerkezesIdeje          
 ,FelbontasDatuma          
 ,IraIktatokonyv_Id            
 ,@IraIktatokonyv_Id_Ver     
 ,KuldesMod          
 ,Erkezteto_Szam          
 ,HivatkozasiSzam          
 ,Targy          
 ,Tartalom          
 ,RagSzam          
 ,Surgosseg          
 ,BelyegzoDatuma          
 ,UgyintezesModja          
 ,PostazasIranya          
 ,Tovabbito          
 ,PeldanySzam          
 ,IktatniKell          
 ,Iktathato          
 ,SztornirozasDat          
 ,KuldKuldemeny_Id_Szulo            
 ,@KuldKuldemeny_Id_Szulo_Ver     
 ,Erkeztetes_Ev          
 ,Csoport_Id_Cimzett            
 ,@Csoport_Id_Cimzett_Ver     
 ,Csoport_Id_Felelos            
 ,@Csoport_Id_Felelos_Ver     
 ,FelhasznaloCsoport_Id_Expedial          
 ,ExpedialasIdeje          
 ,FelhasznaloCsoport_Id_Orzo            
 ,@FelhasznaloCsoport_Id_Orzo_Ver     
 ,FelhasznaloCsoport_Id_Atvevo            
 ,@FelhasznaloCsoport_Id_Atvevo_Ver     
 ,Partner_Id_Bekuldo            
 ,@Partner_Id_Bekuldo_Ver     
 ,Cim_Id            
 ,@Cim_Id_Ver     
 ,CimSTR_Bekuldo          
 ,NevSTR_Bekuldo          
 ,AdathordozoTipusa          
 ,ElsodlegesAdathordozoTipusa          
 ,FelhasznaloCsoport_Id_Alairo            
 ,@FelhasznaloCsoport_Id_Alairo_Ver     
 ,BarCode          
 ,FelhasznaloCsoport_Id_Bonto            
 ,@FelhasznaloCsoport_Id_Bonto_Ver     
 ,CsoportFelelosEloszto_Id            
 ,@CsoportFelelosEloszto_Id_Ver     
 ,Csoport_Id_Felelos_Elozo            
 ,@Csoport_Id_Felelos_Elozo_Ver     
 ,Kovetkezo_Felelos_Id          
 ,Elektronikus_Kezbesitesi_Allap          
 ,Kovetkezo_Orzo_Id          
 ,Fizikai_Kezbesitesi_Allapot          
 ,IraIratok_Id          
 ,BontasiMegjegyzes          
 ,Tipus          
 ,Minosites          
 ,MegtagadasIndoka          
 ,Megtagado_Id          
 ,MegtagadasDat          
 ,KimenoKuldemenyFajta          
 ,Elsobbsegi          
 ,Ajanlott          
 ,Tertiveveny          
 ,SajatKezbe          
 ,E_ertesites          
 ,E_elorejelzes          
 ,PostaiLezaroSzolgalat          
 ,Ar          
 ,KimenoKuld_Sorszam           
 ,Ver          
 ,TovabbitasAlattAllapot          
 ,Azonosito          
 ,BoritoTipus          
 ,MegorzesJelzo          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id          
 ,IktatastNemIgenyel          
 ,KezbesitesModja          
 ,Munkaallomas          
 ,SerultKuldemeny          
 ,TevesCimzes          
 ,TevesErkeztetes          
 ,CimzesTipusa          
 ,FutarJegyzekListaSzama          
 ,Minosito          
 ,MinositesErvenyessegiIdeje          
 ,TerjedelemMennyiseg          
 ,TerjedelemMennyisegiEgyseg          
 ,TerjedelemMegjegyzes        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go