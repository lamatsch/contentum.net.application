IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_PldIratPeldanyokHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LogEREC_PldIratPeldanyokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_PldIratPeldanyokHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_LogEREC_PldIratPeldanyokHistory] AS' 
END
GO
ALTER procedure [dbo].[sp_LogEREC_PldIratPeldanyokHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_PldIratPeldanyok where Id = @Row_Id;
          declare @IraIrat_Id_Ver int;
       -- select @IraIrat_Id_Ver = max(Ver) from EREC_IraIratok where Id in (select IraIrat_Id from #tempTable);
          
   insert into EREC_PldIratPeldanyokHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,IraIrat_Id       
 ,IraIrat_Id_Ver     
 ,UgyUgyirat_Id_Kulso     
 ,Sorszam     
 ,SztornirozasDat     
 ,AtvetelDatuma     
 ,Eredet     
 ,KuldesMod     
 ,Ragszam     
 ,UgyintezesModja     
 ,VisszaerkezesiHatarido     
 ,Visszavarolag     
 ,VisszaerkezesDatuma     
 ,Cim_id_Cimzett     
 ,Partner_Id_Cimzett     
 ,Csoport_Id_Felelos     
 ,FelhasznaloCsoport_Id_Orzo     
 ,Csoport_Id_Felelos_Elozo     
 ,CimSTR_Cimzett     
 ,NevSTR_Cimzett     
 ,Tovabbito     
 ,IraIrat_Id_Kapcsolt     
 ,IrattariHely     
 ,Gener_Id     
 ,PostazasDatuma     
 ,BarCode     
 ,Allapot     
 ,Azonosito     
 ,Kovetkezo_Felelos_Id     
 ,Elektronikus_Kezbesitesi_Allap     
 ,Kovetkezo_Orzo_Id     
 ,Fizikai_Kezbesitesi_Allapot     
 ,TovabbitasAlattAllapot     
 ,PostazasAllapot     
 ,ValaszElektronikus     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,IraIrat_Id            
 ,@IraIrat_Id_Ver     
 ,UgyUgyirat_Id_Kulso          
 ,Sorszam          
 ,SztornirozasDat          
 ,AtvetelDatuma          
 ,Eredet          
 ,KuldesMod          
 ,Ragszam          
 ,UgyintezesModja          
 ,VisszaerkezesiHatarido          
 ,Visszavarolag          
 ,VisszaerkezesDatuma          
 ,Cim_id_Cimzett          
 ,Partner_Id_Cimzett          
 ,Csoport_Id_Felelos          
 ,FelhasznaloCsoport_Id_Orzo          
 ,Csoport_Id_Felelos_Elozo          
 ,CimSTR_Cimzett          
 ,NevSTR_Cimzett          
 ,Tovabbito          
 ,IraIrat_Id_Kapcsolt          
 ,IrattariHely          
 ,Gener_Id          
 ,PostazasDatuma          
 ,BarCode          
 ,Allapot          
 ,Azonosito          
 ,Kovetkezo_Felelos_Id          
 ,Elektronikus_Kezbesitesi_Allap          
 ,Kovetkezo_Orzo_Id          
 ,Fizikai_Kezbesitesi_Allapot          
 ,TovabbitasAlattAllapot          
 ,PostazasAllapot          
 ,ValaszElektronikus          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
