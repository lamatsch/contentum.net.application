IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_UgyUgyiratdarabokHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LogEREC_UgyUgyiratdarabokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_UgyUgyiratdarabokHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_LogEREC_UgyUgyiratdarabokHistory] AS' 
END
GO
ALTER procedure [dbo].[sp_LogEREC_UgyUgyiratdarabokHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_UgyUgyiratdarabok where Id = @Row_Id;
          declare @UgyUgyirat_Id_Ver int;
       -- select @UgyUgyirat_Id_Ver = max(Ver) from EREC_UgyUgyiratok where Id in (select UgyUgyirat_Id from #tempTable);
              declare @IraIktatokonyv_Id_Ver int;
       -- select @IraIktatokonyv_Id_Ver = max(Ver) from EREC_IraIktatoKonyvek where Id in (select IraIktatokonyv_Id from #tempTable);
          
   insert into EREC_UgyUgyiratdarabokHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,UgyUgyirat_Id       
 ,UgyUgyirat_Id_Ver     
 ,EljarasiSzakasz     
 ,Sorszam     
 ,Azonosito     
 ,Hatarido     
 ,FelhasznaloCsoport_Id_Ugyintez     
 ,ElintezesDat     
 ,LezarasDat     
 ,ElintezesMod     
 ,LezarasOka     
 ,Leiras     
 ,UgyUgyirat_Id_Elozo     
 ,IraIktatokonyv_Id       
 ,IraIktatokonyv_Id_Ver     
 ,Foszam     
 ,UtolsoAlszam     
 ,Csoport_Id_Felelos     
 ,Csoport_Id_Felelos_Elozo     
 ,IratMetadefinicio_Id     
 ,Allapot     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,UgyUgyirat_Id            
 ,@UgyUgyirat_Id_Ver     
 ,EljarasiSzakasz          
 ,Sorszam          
 ,Azonosito          
 ,Hatarido          
 ,FelhasznaloCsoport_Id_Ugyintez          
 ,ElintezesDat          
 ,LezarasDat          
 ,ElintezesMod          
 ,LezarasOka          
 ,Leiras          
 ,UgyUgyirat_Id_Elozo          
 ,IraIktatokonyv_Id            
 ,@IraIktatokonyv_Id_Ver     
 ,Foszam          
 ,UtolsoAlszam          
 ,Csoport_Id_Felelos          
 ,Csoport_Id_Felelos_Elozo          
 ,IratMetadefinicio_Id          
 ,Allapot          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
