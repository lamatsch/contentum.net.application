IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_UgyiratKapcsolatokHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LogEREC_UgyiratKapcsolatokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogEREC_UgyiratKapcsolatokHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_LogEREC_UgyiratKapcsolatokHistory] AS' 
END
GO
ALTER procedure [dbo].[sp_LogEREC_UgyiratKapcsolatokHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_UgyiratKapcsolatok where Id = @Row_Id;
          declare @Ugyirat_Ugyirat_Beepul_Ver int;
       -- select @Ugyirat_Ugyirat_Beepul_Ver = max(Ver) from EREC_UgyUgyiratok where Id in (select Ugyirat_Ugyirat_Beepul from #tempTable);
              declare @Ugyirat_Ugyirat_Felepul_Ver int;
       -- select @Ugyirat_Ugyirat_Felepul_Ver = max(Ver) from EREC_UgyUgyiratok where Id in (select Ugyirat_Ugyirat_Felepul from #tempTable);
          
   insert into EREC_UgyiratKapcsolatokHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,KapcsolatTipus     
 ,Leiras     
 ,Kezi     
 ,Ugyirat_Ugyirat_Beepul       
 ,Ugyirat_Ugyirat_Beepul_Ver     
 ,Ugyirat_Ugyirat_Felepul       
 ,Ugyirat_Ugyirat_Felepul_Ver     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,KapcsolatTipus          
 ,Leiras          
 ,Kezi          
 ,Ugyirat_Ugyirat_Beepul            
 ,@Ugyirat_Ugyirat_Beepul_Ver     
 ,Ugyirat_Ugyirat_Felepul            
 ,@Ugyirat_Ugyirat_Felepul_Ver     
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
