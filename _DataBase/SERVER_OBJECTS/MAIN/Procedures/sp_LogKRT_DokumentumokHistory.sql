IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogKRT_DokumentumokHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LogKRT_DokumentumokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogKRT_DokumentumokHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_LogKRT_DokumentumokHistory] AS' 
END
GO
ALTER procedure [dbo].[sp_LogKRT_DokumentumokHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from KRT_Dokumentumok where Id = @Row_Id;
          declare @Dokumentum_Id_Kovetkezo_Ver int;
       -- select @Dokumentum_Id_Kovetkezo_Ver = max(Ver) from KRT_Dokumentumok where Id in (select Dokumentum_Id_Kovetkezo from #tempTable);
              declare @Alkalmazas_Id_Ver int;
       -- select @Alkalmazas_Id_Ver = max(Ver) from KRT_Alkalmazasok where Id in (select Alkalmazas_Id from #tempTable);
          
   insert into KRT_DokumentumokHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Org     
 ,FajlNev     
 ,VerzioJel     
 ,Nev     
 ,Tipus     
 ,Formatum     
 ,Leiras     
 ,Meret     
 ,TartalomHash     
 ,AlairtTartalomHash     
 ,KivonatHash     
 ,Dokumentum_Id     
 ,Dokumentum_Id_Kovetkezo       
 ,Dokumentum_Id_Kovetkezo_Ver     
 ,Alkalmazas_Id       
 ,Alkalmazas_Id_Ver     
 ,Csoport_Id_Tulaj     
 ,KulsoAzonositok     
 ,External_Source     
 ,External_Link     
 ,External_Id     
 ,External_Info     
 ,CheckedOut     
 ,CheckedOutTime     
 ,BarCode     
 ,OCRPrioritas     
 ,OCRAllapot     
 ,Allapot     
 ,WorkFlowAllapot     
 ,Megnyithato     
 ,Olvashato     
 ,ElektronikusAlairas     
 ,AlairasFelulvizsgalat     
 ,Titkositas     
 ,SablonAzonosito     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_Id     
 ,ZarolasIdo     
 ,Tranz_Id     
 ,UIAccessLog_Id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Org          
 ,FajlNev          
 ,VerzioJel          
 ,Nev          
 ,Tipus          
 ,Formatum          
 ,Leiras          
 ,Meret          
 ,TartalomHash          
 ,AlairtTartalomHash          
 ,KivonatHash          
 ,Dokumentum_Id          
 ,Dokumentum_Id_Kovetkezo            
 ,@Dokumentum_Id_Kovetkezo_Ver     
 ,Alkalmazas_Id            
 ,@Alkalmazas_Id_Ver     
 ,Csoport_Id_Tulaj          
 ,KulsoAzonositok          
 ,External_Source          
 ,External_Link          
 ,External_Id          
 ,External_Info          
 ,CheckedOut          
 ,CheckedOutTime          
 ,BarCode          
 ,OCRPrioritas          
 ,OCRAllapot          
 ,Allapot          
 ,WorkFlowAllapot          
 ,Megnyithato          
 ,Olvashato          
 ,ElektronikusAlairas          
 ,AlairasFelulvizsgalat          
 ,Titkositas          
 ,SablonAzonosito          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_Id          
 ,ZarolasIdo          
 ,Tranz_Id          
 ,UIAccessLog_Id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
