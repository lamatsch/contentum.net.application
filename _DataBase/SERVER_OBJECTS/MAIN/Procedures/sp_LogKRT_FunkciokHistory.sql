IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogKRT_FunkciokHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LogKRT_FunkciokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogKRT_FunkciokHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_LogKRT_FunkciokHistory] AS' 
END
GO
ALTER procedure [dbo].[sp_LogKRT_FunkciokHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from KRT_Funkciok where Id = @Row_Id;
          declare @ObjTipus_Id_AdatElem_Ver int;
       -- select @ObjTipus_Id_AdatElem_Ver = max(Ver) from KRT_ObjTipusok where Id in (select ObjTipus_Id_AdatElem from #tempTable);
              declare @ObjStat_Id_Kezd_Ver int;
       -- select @ObjStat_Id_Kezd_Ver = max(Ver) from KRT_KodTarak where Id in (select ObjStat_Id_Kezd from #tempTable);
              declare @Alkalmazas_Id_Ver int;
       -- select @Alkalmazas_Id_Ver = max(Ver) from KRT_Alkalmazasok where Id in (select Alkalmazas_Id from #tempTable);
              declare @Muvelet_Id_Ver int;
       -- select @Muvelet_Id_Ver = max(Ver) from KRT_Muveletek where Id in (select Muvelet_Id from #tempTable);
              declare @ObjStat_Id_Veg_Ver int;
       -- select @ObjStat_Id_Veg_Ver = max(Ver) from KRT_KodTarak where Id in (select ObjStat_Id_Veg from #tempTable);
              declare @Funkcio_Id_Szulo_Ver int;
       -- select @Funkcio_Id_Szulo_Ver = max(Ver) from KRT_Funkciok where Id in (select Funkcio_Id_Szulo from #tempTable);
          
   insert into KRT_FunkciokHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Kod     
 ,Nev     
 ,ObjTipus_Id_AdatElem       
 ,ObjTipus_Id_AdatElem_Ver     
 ,ObjStat_Id_Kezd       
 ,ObjStat_Id_Kezd_Ver     
 ,Alkalmazas_Id       
 ,Alkalmazas_Id_Ver     
 ,Muvelet_Id       
 ,Muvelet_Id_Ver     
 ,ObjStat_Id_Veg       
 ,ObjStat_Id_Veg_Ver     
 ,Leiras     
 ,Funkcio_Id_Szulo       
 ,Funkcio_Id_Szulo_Ver     
 ,Csoportosito     
 ,Modosithato     
 ,Org     
 ,MunkanaploJelzo     
 ,FeladatJelzo     
 ,KeziFeladatJelzo     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Kod          
 ,Nev          
 ,ObjTipus_Id_AdatElem            
 ,@ObjTipus_Id_AdatElem_Ver     
 ,ObjStat_Id_Kezd            
 ,@ObjStat_Id_Kezd_Ver     
 ,Alkalmazas_Id            
 ,@Alkalmazas_Id_Ver     
 ,Muvelet_Id            
 ,@Muvelet_Id_Ver     
 ,ObjStat_Id_Veg            
 ,@ObjStat_Id_Veg_Ver     
 ,Leiras          
 ,Funkcio_Id_Szulo            
 ,@Funkcio_Id_Szulo_Ver     
 ,Csoportosito          
 ,Modosithato          
 ,Org          
 ,MunkanaploJelzo          
 ,FeladatJelzo          
 ,KeziFeladatJelzo          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
