IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogKRT_MenukHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LogKRT_MenukHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogKRT_MenukHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_LogKRT_MenukHistory] AS' 
END
GO
ALTER procedure [dbo].[sp_LogKRT_MenukHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from KRT_Menuk where Id = @Row_Id;
          declare @Menu_Id_Szulo_Ver int;
       -- select @Menu_Id_Szulo_Ver = max(Ver) from KRT_Menuk where Id in (select Menu_Id_Szulo from #tempTable);
              declare @Funkcio_Id_Ver int;
       -- select @Funkcio_Id_Ver = max(Ver) from KRT_Funkciok where Id in (select Funkcio_Id from #tempTable);
              declare @Modul_Id_Ver int;
       -- select @Modul_Id_Ver = max(Ver) from KRT_Modulok where Id in (select Modul_Id from #tempTable);
          
   insert into KRT_MenukHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Org     
 ,Menu_Id_Szulo       
 ,Menu_Id_Szulo_Ver     
 ,Kod     
 ,Nev     
 ,Funkcio_Id       
 ,Funkcio_Id_Ver     
 ,Modul_Id       
 ,Modul_Id_Ver     
 ,Parameter     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ImageURL     
 ,Sorrend     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Org          
 ,Menu_Id_Szulo            
 ,@Menu_Id_Szulo_Ver     
 ,Kod          
 ,Nev          
 ,Funkcio_Id            
 ,@Funkcio_Id_Ver     
 ,Modul_Id            
 ,@Modul_Id_Ver     
 ,Parameter          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ImageURL          
 ,Sorrend          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
