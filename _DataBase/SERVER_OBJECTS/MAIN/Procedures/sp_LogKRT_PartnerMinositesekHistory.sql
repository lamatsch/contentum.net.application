IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogKRT_PartnerMinositesekHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LogKRT_PartnerMinositesekHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogKRT_PartnerMinositesekHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_LogKRT_PartnerMinositesekHistory] AS' 
END
GO
ALTER procedure [dbo].[sp_LogKRT_PartnerMinositesekHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from KRT_PartnerMinositesek where Id = @Row_Id;
          declare @Partner_id_Ver int;
       -- select @Partner_id_Ver = max(Ver) from KRT_Partnerek where Id in (select Partner_id from #tempTable);
              declare @Felhasznalo_id_kero_Ver int;
       -- select @Felhasznalo_id_kero_Ver = max(Ver) from KRT_Felhasznalok where Id in (select Felhasznalo_id_kero from #tempTable);
              declare @Felhasznalo_id_donto_Ver int;
       -- select @Felhasznalo_id_donto_Ver = max(Ver) from KRT_Felhasznalok where Id in (select Felhasznalo_id_donto from #tempTable);
          
   insert into KRT_PartnerMinositesekHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,ALLAPOT     
 ,Partner_id       
 ,Partner_id_Ver     
 ,Felhasznalo_id_kero       
 ,Felhasznalo_id_kero_Ver     
 ,KertMinosites     
 ,KertKezdDat     
 ,KertVegeDat     
 ,KerelemAzonosito     
 ,KerelemBeadIdo     
 ,KerelemDontesIdo     
 ,Felhasznalo_id_donto       
 ,Felhasznalo_id_donto_Ver     
 ,DontesAzonosito     
 ,Minosites     
 ,MinositesKezdDat     
 ,MinositesVegDat     
 ,SztornirozasDat     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,ALLAPOT          
 ,Partner_id            
 ,@Partner_id_Ver     
 ,Felhasznalo_id_kero            
 ,@Felhasznalo_id_kero_Ver     
 ,KertMinosites          
 ,KertKezdDat          
 ,KertVegeDat          
 ,KerelemAzonosito          
 ,KerelemBeadIdo          
 ,KerelemDontesIdo          
 ,Felhasznalo_id_donto            
 ,@Felhasznalo_id_donto_Ver     
 ,DontesAzonosito          
 ,Minosites          
 ,MinositesKezdDat          
 ,MinositesVegDat          
 ,SztornirozasDat          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
