IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogKRT_SzemelyekHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LogKRT_SzemelyekHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogKRT_SzemelyekHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_LogKRT_SzemelyekHistory] AS' 
END
GO
ALTER procedure [dbo].[sp_LogKRT_SzemelyekHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from KRT_Szemelyek where Id = @Row_Id;
          declare @Partner_Id_Ver int;
       -- select @Partner_Id_Ver = max(Ver) from KRT_Partnerek where Id in (select Partner_Id from #tempTable);
          
   insert into KRT_SzemelyekHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Partner_Id       
 ,Partner_Id_Ver     
 ,AnyjaNeve     
 ,AnyjaNeveCsaladiNev     
 ,AnyjaNeveElsoUtonev     
 ,AnyjaNeveTovabbiUtonev     
 ,ApjaNeve     
 ,SzuletesiNev     
 ,SzuletesiCsaladiNev     
 ,SzuletesiElsoUtonev     
 ,SzuletesiTovabbiUtonev     
 ,SzuletesiOrszag     
 ,SzuletesiOrszagId     
 ,UjTitulis     
 ,UjCsaladiNev     
 ,UjUtonev     
 ,UjTovabbiUtonev     
 ,SzuletesiHely     
 ,SzuletesiHely_id     
 ,SzuletesiIdo     
 ,Allampolgarsag     
 ,TAJSzam     
 ,SZIGSzam     
 ,Neme     
 ,SzemelyiAzonosito     
 ,Adoazonosito     
 ,Adoszam     
 ,KulfoldiAdoszamJelolo     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Partner_Id            
 ,@Partner_Id_Ver     
 ,AnyjaNeve          
 ,AnyjaNeveCsaladiNev          
 ,AnyjaNeveElsoUtonev          
 ,AnyjaNeveTovabbiUtonev          
 ,ApjaNeve          
 ,SzuletesiNev          
 ,SzuletesiCsaladiNev          
 ,SzuletesiElsoUtonev          
 ,SzuletesiTovabbiUtonev          
 ,SzuletesiOrszag          
 ,SzuletesiOrszagId          
 ,UjTitulis          
 ,UjCsaladiNev          
 ,UjUtonev          
 ,UjTovabbiUtonev          
 ,SzuletesiHely          
 ,SzuletesiHely_id          
 ,SzuletesiIdo          
 ,Allampolgarsag          
 ,TAJSzam          
 ,SZIGSzam          
 ,Neme          
 ,SzemelyiAzonosito          
 ,Adoazonosito          
 ,Adoszam          
 ,KulfoldiAdoszamJelolo          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
