IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogRecordToHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LogRecordToHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LogRecordToHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_LogRecordToHistory] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_LogRecordToHistory] 
		 @TableName NVARCHAR(100)
		,@Row_Id uniqueidentifier
		,@HistoryTableName NVARCHAR(100)
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		
AS
begin
BEGIN TRY
	set nocount on
	
	declare @sqlCommand nvarchar(256);

	if (@VegrehajtasIdo is null) set @VegrehajtasIdo = getdate();
	
	set @sqlCommand = N'exec sp_Log' + @TableName + N'History @Row_Id,@Muvelet,@Vegrehajto_Id,@VegrehajtasIdo'
	
	--PRINT @sqlCommand

	exec sp_executesql @sqlCommand,N'@Row_Id UNIQUEIDENTIFIER,@Muvelet INT,@Vegrehajto_Id uniqueidentifier,@VegrehajtasIdo datetime'
	,@Row_Id=@Row_Id,@Muvelet=@Muvelet,@Vegrehajto_Id=@Vegrehajto_Id,@VegrehajtasIdo=@VegrehajtasIdo
 
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
