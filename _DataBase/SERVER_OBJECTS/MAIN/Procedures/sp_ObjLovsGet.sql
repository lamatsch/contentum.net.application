IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ObjLovsGet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ObjLovsGet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ObjLovsGet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_ObjLovsGet] AS' 
END
GO
ALTER procedure [dbo].[sp_ObjLovsGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier
         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   ObjLovs.Id,
	   ObjLovs.ObjTip_id,
	   ObjLovs.Kod,
	   ObjLovs.Nev,
	   ObjLovs.Ver,
	   ObjLovs.Note,
	   ObjLovs.Stat_id,
	   ObjLovs.ErvKezd,
	   ObjLovs.ErvVege,
	   ObjLovs.Letrehozo_id,
	   ObjLovs.LetrehozasIdo,
	   ObjLovs.Modosito_id,
	   ObjLovs.ModositasIdo,
	   ObjLovs.Zarolo_id,
	   ObjLovs.ZarolasIdo,
	   ObjLovs.Tranz_id,
	   ObjLovs.UIAccessLog_id
	   from 
		 ObjLovs as ObjLovs 
	   where
		 ObjLovs.Id = @Id'

	exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
