IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PKI_Alairas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PKI_Alairas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PKI_Alairas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_PKI_Alairas] AS' 
END
GO
ALTER procedure [dbo].[sp_PKI_Alairas]    
                 @Felhasznalo_Id      uniqueidentifier,
                 @AlkalmazasKod       nvarchar(100)    = null,
                 @AlairasSzabaly_Id   uniqueidentifier = null,
                 @AlairasKod          char(4)          = null,
                 @ObjektumTip         nvarchar(4)      = null,
                 @Objektum_Id         uniqueidentifier = null,                 
                 @UgykorKod           nvarchar(10)     = null,
                 @UgyTipusNev         nvarchar(400)    = null,                 
                 @EljarasiSzakasz     nvarchar(64)     = null,
                 @IratTipusNev        nvarchar(400)    = null,
                 @FolyamatKod         nvarchar(100)    = null,
                 @MuveletKod          nvarchar(100)    = null,
                 @AlairoSzerep        nvarchar(64)     = null,
                 @Titkositas          char(1)          = '0',
                 @HasznalatiMod       nvarchar(16), 
                 @ResultAlairasEASZkd nvarchar(100) OUTPUT,   -- EASZ azonosító
                 @ResultTanusitvanyLn nvarchar(100) OUTPUT,   -- tanúsítvány azonosító
                 @ResultAlairSzabaly  uniqueidentifier OUTPUT -- az aláírási szabály EDOK azonosítója
AS
/*
-- FELADATA:
	Az aláírás funkció futtatása általános paraméterezéssel.
    Használati módok:
    IratAlairas / MuveletAlairas / DirektAlairas / SzabalyLista / AlairasLista
*/
---------
---------
BEGIN TRY

set nocount on;

---------------------------------
-- input paraméterek feldolgozása
---------------------------------
declare @Irat_Id       uniqueidentifier,
        @Dokumentum_Id uniqueidentifier

-- aláírandó objektum típusának ellenorzése
if @ObjektumTip is not NULL AND
   @ObjektumTip not in('Irat','Dok')
   RAISERROR('Az aláírandó objektum típusa hibás (Irat/Dok)!',16,1)

-- aláírandó irat beállítása
if @ObjektumTip = 'Irat'
	set @Irat_Id = @Objektum_Id
else
	set @Irat_Id = NULL

-- aláírandó dokumentum beállítása
if @ObjektumTip = 'Dok'
	set @Dokumentum_Id = @Objektum_Id
else
	set @Dokumentum_Id = NULL

-----------------------------------
-- általános elofeldolgozó indítása
-----------------------------------
declare @ResultMetaDef       uniqueidentifier,
		@ResultMetaDefTip    nvarchar(100),
		@ResultFolyamat      uniqueidentifier,
		@ResultDokuMuvelet   uniqueidentifier,
		@ResultIratSzerepJel nvarchar(64),
		@ResultAlairoSzerep  nvarchar(64),
		@ResultOrg           uniqueidentifier,
		@ResultEDOK          char(1),
		@ResultPKIMukodes    nvarchar(100)

exec sp_PKI_ParEll
     @Felhasznalo_Id,
     @AlkalmazasKod,
     @AlairasSzabaly_Id,
     @AlairasKod,
     @Irat_Id,
     @Dokumentum_Id,
     @UgykorKod,
     @UgyTipusNev,
     @IratTipusNev,
	 @EljarasiSzakasz,
     @FolyamatKod,
     @MuveletKod,
     @AlairoSzerep,
     @Titkositas,
     @HasznalatiMod,
     @ResultMetaDef       OUTPUT,
	 @ResultMetaDefTip    OUTPUT,
     @ResultFolyamat      OUTPUT,
     @ResultDokuMuvelet   OUTPUT,
     @ResultIratSzerepJel OUTPUT,
     @ResultAlairoSzerep  OUTPUT,
     @ResultAlairSzabaly  OUTPUT,
     @ResultOrg           OUTPUT,
     @ResultEDOK          OUTPUT,
     @ResultPKIMukodes    OUTPUT

if @ResultPKIMukodes <> 'Nem' AND
   @HasznalatiMod in('DirektAlairas') AND
   @ResultAlairSzabaly is NULL
   RAISERROR('Az aláírási muvelethez nincs definiálva érvényes aláírási szabály!',16,1)

--------------------------------
-- aláírás adatok lekérdezése
--------------------------------
if  not (@HasznalatiMod like '%Alairas' AND
         @ResultAlairSzabaly is NULL)
begin

	-- aláírás adatok lekérdezése
	exec sp_PKI_ModEll
		 @Felhasznalo_Id,
		 @ResultMetaDef,       --@MetaDefinicio_Id
		 @ResultMetaDefTip,    --@MetaDefinicioTipus
		 @ResultFolyamat,      --@Folyamat_Id
		 @ResultDokuMuvelet,   --@DokumentumMuvelet_Id
		 @ResultIratSzerepJel, --@IratSzerepJel
		 @ResultAlairoSzerep,  --@AlairoSzerep
		 @Titkositas,
		 @ResultAlairSzabaly,  --@AlairasSzabaly_Id
		 @HasznalatiMod,
         @ResultPKIMukodes,
		 @ResultOrg,
		 @ResultEDOK,
		 @ResultAlairasEASZkd OUTPUT,
		 @ResultTanusitvanyLn OUTPUT,
		 @ResultAlairSzabaly  OUTPUT 

end

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------


GO
