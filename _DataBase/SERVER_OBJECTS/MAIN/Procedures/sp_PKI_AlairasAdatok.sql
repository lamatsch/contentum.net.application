IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PKI_AlairasAdatok]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PKI_AlairasAdatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PKI_AlairasAdatok]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_PKI_AlairasAdatok] AS' 
END
GO
ALTER procedure [dbo].[sp_PKI_AlairasAdatok]    
                 @Dokumentum_Id uniqueidentifier
AS
/*
-- FELADATA:
	A megadott azonosítójú aláírt dokumentumra vonatkozó aláírás adatok lekérdezése.

-- Példa:
declare @Dokumentum_Idc  uniqueidentifier
select top 1 @Dokumentum_Idc = d.Id
  from KRT_Dokumentumok d
-- where isnull(d.ElektronikusAlairas,'0') <> '0'
 order by Letrehozasido desc
exec [dbo].[sp_PKI_AlairasAdatok] 
     @Dokumentum_Idc    -- @Dokumentum_Id
*/

---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
set     @teszt = 1

declare @error           int,
        @rowcount        int

declare @ElektronikusAlairas nvarchar(64),
        @Org                 uniqueidentifier

--------------------------
-- paraméterezés vizsgálat
--------------------------
if @Dokumentum_Id is NULL
   RAISERROR('A dokumentum azonosító megadása kötelezo!',16,1)

-----------------------
-- paraméter ellenorzés
-----------------------
select @Org = d.Org -- a.Org
  from KRT_Dokumentumok d,
       KRT_Alkalmazasok a
 where d.Id = @Dokumentum_Id
   and a.Id = d.Alkalmazas_Id
   and dbo.fn_Ervenyes(d.ErvKezd,d.ErvVege)='I'
    
select @error = @@error, @rowcount = @@rowcount 
if @error <> 0
   RAISERROR('KRT_Dokumentumok lekérdezes hiba!',16,1)
if @rowcount = 0
   RAISERROR('A megadott azonosítóval nem létezik érvényes dokumentum!',16,1)

----------------------------
-- aláírás lista kimenet
----------------------------
select ds.FajlNev AlairtFajlnev,
       kd.Nev ElektronikusAlairas,
       am.Nev BelsoTipus,
       da.Idopecset,
       da.AlairasTulajdonos,
       da.AlairasRendben,
       da.AlairasVeglegRendben,
       da.LetrehozasIdo AlairasIdopont,
	   CONVERT(nvarchar(20), da.LetrehozasIdo, 120) as AlairasIdopont_convert
  from KRT_Dokumentumok do
       inner join KRT_DokumentumKapcsolatok dk
          on do.id = dk.Dokumentum_Id_Al
       inner join KRT_Dokumentumok ds
          on ds.Id = dk.Dokumentum_Id_Fo
       inner join KRT_DokumentumAlairasok da
          on ds.Id = da.Dokumentum_Id_Alairt
	   left outer join (select sz.Id, at.Nev
                          from KRT_AlairasSzabalyok sz
                         inner join KRT_AlairasTipusok at
                            on sz.AlairasTipus_Id = at.Id
                       ) am
          on da.AlairasSzabaly_Id = am.Id
       inner join (select kt.Kod, kt.Nev
                     from KRT_KodTarak kt,
                          KRT_KodCsoportok cs 
                    where cs.kod ='DOKUMENTUM_ALAIRAS_PKI'
                      and cs.Id  = kt.Kodcsoport_id
		              --and cs.org = @Org
                      and dbo.fn_Ervenyes(kt.ErvKezd,kt.ErvVege)='I'
                      and dbo.fn_Ervenyes(cs.ErvKezd,cs.ErvVege)='I'
                      and kt.org = @Org
                   ) kd
          on kd.Kod  = do.ElektronikusAlairas
 where do.Id = @Dokumentum_Id

union
  
select do.FajlNev AlairtFajlnev,
       kd.Nev  ElektronikusAlairas,
       am.Nev BelsoTipus,
       da.Idopecset,
       da.AlairasTulajdonos,
       da.AlairasRendben,
       da.AlairasVeglegRendben,
       da.LetrehozasIdo AlairasIdopont,
	   CONVERT(nvarchar(20), da.LetrehozasIdo, 120) as AlairasIdopont_convert
  from KRT_Dokumentumok do
       inner join KRT_DokumentumAlairasok da
          on do.Id = da.Dokumentum_Id_Alairt
	   left outer join (select sz.Id, at.Nev
                          from KRT_AlairasSzabalyok sz
                         inner join KRT_AlairasTipusok at
                            on sz.AlairasTipus_Id = at.Id
                       ) am
          on da.AlairasSzabaly_Id = am.Id
       inner join (select kt.Kod, kt.Nev
                     from KRT_KodTarak kt,
                          KRT_KodCsoportok cs 
                    where cs.kod ='DOKUMENTUM_ALAIRAS_PKI'
                      and cs.Id  = kt.Kodcsoport_id
		              --and cs.org = @Org
                      and dbo.fn_Ervenyes(kt.ErvKezd,kt.ErvVege)='I'
                      and dbo.fn_Ervenyes(cs.ErvKezd,cs.ErvVege)='I'
                      and kt.org = @Org
                   ) kd
          on kd.Kod  = do.ElektronikusAlairas
 where do.Id = @Dokumentum_Id

select @error = @@error, @rowcount = @@rowcount 
if @error <> 0
   RAISERROR('KRT_DokumentumAlairasok lekérdezési hiba!',16,1)

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------


GO
