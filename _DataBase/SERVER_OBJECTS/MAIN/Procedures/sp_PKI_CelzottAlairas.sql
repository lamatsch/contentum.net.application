IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PKI_CelzottAlairas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PKI_CelzottAlairas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PKI_CelzottAlairas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_PKI_CelzottAlairas] AS' 
END
GO
ALTER procedure [dbo].[sp_PKI_CelzottAlairas]    
                 @Felhasznalo_Id       uniqueidentifier,    -- az aláíró felhasználó EDOK azonosítója
                 @AlkalmazasKod        nvarchar(100),       -- a külso alkalmazás kódja (egyébként üres)
                 @ObjektumTip          nvarchar(4),         -- az aláírandó objektum típusa (Irat/Dok)
                 @Objektum_Id          uniqueidentifier,    -- az aláítandó objektum EDOK azonosítója          
                 @AlairasSzabaly_Id    uniqueidentifier,    -- az aláírás szabály EDOK azonosítója
                 @AlairasKod           char(4),             -- az aláírás típuskódja
                 @Titkositas           char(1),             -- a titkosítás jele 0/1
                 @ResultAlairasEASZkd  nvarchar(100) OUTPUT,   -- EASZ azonosító
                 @ResultTanusitvanyLn  nvarchar(100) OUTPUT,   -- Tanúsítvány azonosító
                 @ResultAlairSzabaly   uniqueidentifier OUTPUT -- az aláírási szabály EDOK azonosítója
AS
/*
-- FELADATA:
	Az EDOK-ban nyilvántartott irat vagy külso alkalmazáshoz tartozó dokumentum megadott
	típusú aláírási muveletének ellenorzése, az aláírás paramétereinek visszaadása.

-- Példa:
declare @Felhasznalo_Idc     uniqueidentifier,  @AlkalmazasKod       nvarchar(100),
        @Objektum_Idc        uniqueidentifier,  @ResultAlairasEASZkd nvarchar(100),
        @ResultTanusitvanyLn nvarchar(100),     @ResultAlairSzabaly uniqueidentifier
select top 1 @Felhasznalo_Idc = convert(uniqueidentifier, '5B7FEE68-CC7A-42D5-9D58-95794E5129F4'),
	   @Objektum_Idc = id
 from EREC_IraIratok
 where Irattipus is not null
exec [dbo].[sp_PKI_CelzottAlairas] 
     @Felhasznalo_Idc,    -- @Felhasznalo_Id
     NULL,                -- @AlkalmazasKod
     'Irat',              -- @ObjektumTip
     @Objektum_Idc,       -- @Objektum_Id
     NULL,                -- @AlairasSzabaly_Id
     'F1',                -- @AlairasKod
     '0',                 -- @Titkositas
     @ResultAlairasEASZkd output,
     @ResultTanusitvanyLn output,
     @ResultAlairSzabaly output
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
set     @teszt = 1

declare @error           int,
        @rowcount        int

declare @HasznalatiMod   nvarchar(16)

set     @HasznalatiMod = 'DirektAlairas'

set     @ResultAlairasEASZkd = NULL
set     @ResultTanusitvanyLn = NULL
set     @ResultAlairSzabaly  = NULL

--------------------------------
-- paraméterezés vizsgálat
--------------------------------
if @AlairasKod is NULL AND @AlairasSzabaly_Id is NULL
   RAISERROR('Az aláírás típusának megadása kötelezo!',16,1)

------------------------------------
-- általános aláíró eljárás indítása
------------------------------------
exec sp_PKI_Alairas
     @Felhasznalo_Id,
     @AlkalmazasKod,
     @AlairasSzabaly_Id,
     @AlairasKod,
     @ObjektumTip,
     @Objektum_Id,       
     NULL, --@UgykorKod
     NULL, --@UgyTipusNev
     NULL, --@EljarasiSzakasz
     NULL, --@IratTipusNev
     NULL, --@FolyamatKod
     NULL, --@MuveletKod
     NULL, --@AlairoSzerep
	 @Titkositas,
     @HasznalatiMod,
	 @ResultAlairasEASZkd OUTPUT,
	 @ResultTanusitvanyLn OUTPUT,
	 @ResultAlairSzabaly  OUTPUT 

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------


GO
