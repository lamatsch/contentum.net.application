IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RemoveCsoportFromJogtargyById]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RemoveCsoportFromJogtargyById]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RemoveCsoportFromJogtargyById]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_RemoveCsoportFromJogtargyById] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_RemoveCsoportFromJogtargyById]
	@Id		uniqueidentifier
AS
/**********************************************************************************************************
* Törli az összeköttetést a paraméterben megadott ACL és csoport között. (Törlés KRT_Jogosultakból).
*
* Errorcode [50403]: nem létezik a kapcsolat
**********************************************************************************************************/
BEGIN
begin try
	SET NOCOUNT ON;

	delete from KRT_Jogosultak where Id = @Id;
	
end try
begin catch
  declare @errorSeverity integer,
          @errorState integer,
          @errorCode nvarchar(1000);
  select @errorSeverity = ERROR_SEVERITY(), @errorState = ERROR_STATE();

  if ERROR_NUMBER() < 50000
    set @errorCode = '[' + convert(nvarchar(10), ERROR_NUMBER()) + '] ' + '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();
  else
    set @errorCode = '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();

  if @errorState = 0 set @errorState = 1
  raiserror(@errorCode, @errorSeverity, @errorState)
end catch
END


GO
