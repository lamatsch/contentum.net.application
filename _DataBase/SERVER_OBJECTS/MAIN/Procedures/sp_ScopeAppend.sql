IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ScopeAppend]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ScopeAppend]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ScopeAppend]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_ScopeAppend] AS' 
END
GO
ALTER procedure [dbo].[sp_ScopeAppend]
  ( @Csoport_Id uniqueidentifier,
    @Tabla_Nev nvarchar(100),          --JogtA?rgy tA?bla neve (pl.: EREC_IraIktatoKonyvek, EREC_UgyUgyiratdarabok)
    @Rekord_Id uniqueidentifier,       --JogtA?rgy sor egyedi azonosA­tAlja
    @Orokolheto char(1)                --['I' | 'N'] A–rA¶kA¶lhetL‘ legyen-e az ACL a csoporthierarchiA?ban
  )
as
begin
begin try
--  begin transaction ScopeAppendTransaction;
  set nocount on;
  --declare


  if ( 6 = 7 ) raiserror('[50404]', 16, 1); --egyik raiserror

  if ( 6 = 7 ) --mA?sik raiserror
  begin
    raiserror('[50404]', 16, 1);
    return @@error;
  end

--  commit transaction ScopeAppendTransaction;

end try
begin catch
--  if @@TRANCOUNT > 0 rollback transaction ScopeAppendTransaction;

  declare @errorSeverity int, @errorState int, @errorCode nvarchar(1000);
  select @errorSeverity = ERROR_SEVERITY(), @errorState = ERROR_STATE();

  if ERROR_NUMBER() < 50000
    set @errorCode = '[' + convert(nvarchar(10), ERROR_NUMBER()) + '] ' + ERROR_MESSAGE();
  else
    set @errorCode = ERROR_MESSAGE();
  if @errorState = 0 set @errorState = 1;

  raiserror(@errorCode, @errorSeverity, @errorState);

end catch
end --sp_ScopeAppend


GO
