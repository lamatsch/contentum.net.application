IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UgyosztalyokBekapcsolasa]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UgyosztalyokBekapcsolasa]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UgyosztalyokBekapcsolasa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_UgyosztalyokBekapcsolasa] AS' 
END
GO
ALTER procedure [dbo].[sp_UgyosztalyokBekapcsolasa]
(
	@UgyosztalyNev	NVARCHAR(400)
)
as
BEGIN
	DECLARE @SzervezetId UNIQUEIDENTIFIER;
	DECLARE @id UNIQUEIDENTIFIER
	DECLARE @ids NVARCHAR(MAX)
	DECLARE @ver INT
	DECLARE @vers NVARCHAR(MAX)
	declare @sumIds NVARCHAR(MAX)

	SELECT @SzervezetId = Id FROM KRT_Csoportok WHERE tipus = '0' AND Nev = @UgyosztalyNev

    --select * from krt_csoportok where nev like 'Központi Ügy%'

	DECLARE cur CURSOR FOR
		select Id,Ver FROM EREC_UgyUgyiratok 
		WHERE (Allapot = '06' OR Allapot = '04') 		
        AND IraIktatokonyv_Id = (select id from erec_iraIKTATOKONYVEK WHERE NEV LIKE 'Adó Ügyosztály iktatókönyve')
		AND FelhasznaloCsoport_Id_Ugyintez IN
		(
			SELECT Csoport_Id_Jogalany 
				FROM KRT_CsoportTagok 
				WHERE Csoport_Id = @SzervezetId 
					AND GETDATE() BETWEEN ErvKezd AND ErvVege
					AND Tipus IN ('2','3')
		)

	OPEN cur;
	FETCH NEXT FROM cur INTO @id,@ver
	SET @ids = '';
	SET @vers = '';
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @ids = @ids + '''' + cast(@id AS nvarchar(36)) + ''',';
		SET @vers = @vers + cast(@ver AS nvarchar(3)) + ',';
		FETCH NEXT FROM cur INTO @id,@ver
	END

	CLOSE cur;
	DEALLOCATE cur;

	SET @ids = Substring(@ids,0,len(@ids))
	SET @vers = Substring(@vers,0,len(@vers))

	EXEC sp_EREC_UgyUgyiratok_AtadasAtvetelTomeges @ids,@vers,@SzervezetId,NULL,'ECB902E5-FD38-4D06-9E0E-A0B2733FB2E9'

/*	SELECT * FROM EREC_IraKezbesitesiTetelek
		WHERE EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_LOGIN = 'ECB902E5-FD38-4D06-9E0E-A0B2733FB2E9'
			AND EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoLogin IS NULL
			AND Allapot = '1'*/

	UPDATE EREC_IraKezbesitesiTetelek
		SET Allapot = '2'
		WHERE EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_LOGIN = 'ECB902E5-FD38-4D06-9E0E-A0B2733FB2E9'
			AND EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoLogin IS NULL
			AND Allapot = '1'
			AND EREC_IraKezbesitesiTetelek.Csoport_Id_Cel = @SzervezetId;
END


GO
