IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_VIEW_IrattariHelyekGetByVonalkod]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_VIEW_IrattariHelyekGetByVonalkod]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_VIEW_IrattariHelyekGetByVonalkod]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_VIEW_IrattariHelyekGetByVonalkod] AS' 
END
GO

ALTER procedure [dbo].[sp_VIEW_IrattariHelyekGetByVonalkod]
			@Vonalkod nvarchar(100),
         @ExecutorUserId uniqueidentifier

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   VIEW_IrattariHelyLevelek.Id,
		   VIEW_IrattariHelyLevelek.Vonalkod,
		   VIEW_IrattariHelyLevelek.Ertek
	   from 
		 VIEW_IrattariHelyLevelek as VIEW_IrattariHelyLevelek 
	   where
		 VIEW_IrattariHelyLevelek.Vonalkod = ''' + cast(@Vonalkod as nvarchar(100)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


GO
