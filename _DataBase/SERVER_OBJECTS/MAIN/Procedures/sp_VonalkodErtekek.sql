IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_VonalkodErtekek]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_VonalkodErtekek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_VonalkodErtekek]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_VonalkodErtekek] AS' 
END
GO
-- tárolt eljárás létrehozása (+ paraméterezés, ha kell)
ALTER procedure [dbo].[sp_VonalkodErtekek]
 @Feltetel nvarchar(max) = ''
as

begin

BEGIN TRY -- hibafigyelo blokk eleje, csak akkor kell, ha kezeljük a hibákat

   DECLARE @sqlcmd nvarchar(MAX);

/*   SET @sqlcmd = @sqlcmd + ' select   cs.nev, barcode
			     from     erec_ugyugyiratok u
			     , krt_csoportok cs
			     where    cs.id = u.csoport_id_felelos
			     and      cs.tipus <> ''1'' '
			     + @feltetel +
			     ' order by 1, 2'

   exec sp_executesql @sqlcmd;
*/
   select   cs.nev, barcode
   from     erec_ugyugyiratok u
 	      , krt_csoportok cs
   where    cs.id = u.csoport_id_felelos
   and      cs.tipus <> '1'
   and      suser_name() in (select suser_name from krt_excelusers)
   order by 1, 2

END TRY -- hibafigyelo blokk vége

-- ha hiba lépett fel, továbbdobjuk az alkalmazás felé
BEGIN CATCH -- hibakezelési blokk eleje, csak akkor kell, ha fent volt BEGIN/END TRY blokk
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH -- hibakezelési blokk vége

end


GO
