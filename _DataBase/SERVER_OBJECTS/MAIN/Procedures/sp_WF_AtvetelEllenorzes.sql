IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WF_AtvetelEllenorzes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_WF_AtvetelEllenorzes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WF_AtvetelEllenorzes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_WF_AtvetelEllenorzes] AS' 
END
GO
ALTER procedure [dbo].[sp_WF_AtvetelEllenorzes]    

AS
/*
-- FELADATA:
	Az átadott iratkezelési objektumok megadott idon belüli átvételének ellenorzése.
    Kezelt funkció: LejartAtvetelek

-- Módosítva:
	2009.12.11 EB:
	- 30 napon túl lejárt tételekhez kapcsolódó értesítés is
	- felelos meghatározása: csoport címzett esetén a szervezettol való átvételre jogosult felhasználó(k)
		(fn_GetFelelos helyett felelos közvetlen lekérdezése - fv. használata nehézkes és hatásfok rontó
		, mert ha bemeno paraméter - Csoport_Id_Cel - van, akkor nem lehet JOIN-olni,
		ha pedig több rekordot kapunk vissza, nem lehet az allekérdezésben)
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @Funkcio_Kod_Kivalto    nvarchar(100),
		@Obj_MetaDefinicio_Id   uniqueidentifier,
		@ObjStateValue_Id       uniqueidentifier,
		@FeladatDefinicioTipus  nvarchar(64),
	    @FelelosObj_Id          uniqueidentifier,
	    @BazisObj_Id            uniqueidentifier,
        @Bazisido               datetime,
		@AtfutasiIdoMin         numeric(8),
		@Obj_Id                 uniqueidentifier,
        @Obj_Id_Kezelendo       uniqueidentifier,
        @Csoport_Id_Felelos     uniqueidentifier,
        @Csoport_Id_FelelosFelh uniqueidentifier,
        @FeladatHatarido        datetime,
		@Leiras                 nvarchar(400),
        @FeladatAllapot         nvarchar(64),
		@Feladat_Id             uniqueidentifier,
		@ObjektumSzukites       nvarchar(2000)

-- input adatok
select @Funkcio_Kod_Kivalto    = Funkcio_Kod_Kivalto,
	   @Obj_MetaDefinicio_Id   = Obj_MetaDefinicio_Id,
	   @ObjStateValue_Id       = ObjStateValue_Id,
	   @FeladatDefinicioTipus  = FeladatDefinicioTipus,
	   @FelelosObj_Id          = FelelosObj_Id,
	   @BazisObj_Id            = BazisObj_Id,
       @Bazisido               = Bazisido,
	   @AtfutasiIdoMin         = AtfutasiIdoMin,
	   @Obj_Id                 = Obj_Id,
       @Obj_Id_Kezelendo       = Obj_Id,
       @Csoport_Id_Felelos     = Csoport_Id_Felelos,
       @Csoport_Id_FelelosFelh = Csoport_Id_FelelosFelh,
       @FeladatHatarido        = FeladatHatarido,
	   @Leiras                 = Leiras,
	   @FeladatAllapot         = Allapot
  from #HataridosFeladatSpec

-------------------------------------------------
---------- speciális adatok kezelése ------------
-------------------------------------------------
--
-- értesítés határidon túli kézbesítési tételekrol (30 napos limit törölve, EB 2009.12.11)
--
;with hi as
(
	select Csoport_Id_Cel, count(*) as db
		  from EREC_IraKezbesitesiTetelek
		 where ErvVege > getdate()
		   and Allapot = '2'
		   and AtadasDat <= @FeladatHatarido
		group by Csoport_Id_Cel
)
insert into #HataridosTetelek
select hi.Csoport_Id_Cel as Csoport_Id_Felelos
	, cs.Id as Csoport_Id_Felelos_Felh
	, ' (Saját: ' + convert(nvarchar(6),hi.db)+ ' db)' as LeirasKieg
	  from KRT_Csoportok cs
		JOIN hi on cs.Id = hi.Csoport_Id_Cel
	 where cs.Tipus = '1'
	   and getdate() between cs.ErvKezd and cs.ErvVege
UNION ALL
select distinct hi.Csoport_Id_Cel as Csoport_Id_Felelos
	, cst.Csoport_Id_Jogalany as Csoport_Id_Felelos_Felh
	, ' (' + (select Nev from KRT_Csoportok where KRT_Csoportok.Id=cst.Csoport_Id) + ': ' + convert(nvarchar(6),hi.db)+ ' db)' as LeirasKieg
	  from KRT_CsoportTagok cst
		LEFT JOIN KRT_Csoportok cs on cst.Csoport_Id_Jogalany = cs.Id
		LEFT JOIN KRT_Felhasznalok felh on felh.Id = cs.Id -- csak az érvényességellenorzés miatt
		LEFT JOIN KRT_Felhasznalo_Szerepkor fsz on fsz.Felhasznalo_Id=cs.Id
		LEFT JOIN KRT_Szerepkorok sz on sz.Id=fsz.Szerepkor_Id -- csak az érvényességellenorzés miatt
		LEFT JOIN KRT_Szerepkor_Funkcio szf on sz.Id=szf.Szerepkor_Id
		LEFT JOIN KRT_Funkciok f on f.Id=szf.Funkcio_Id
		JOIN hi on cst.Csoport_Id = hi.Csoport_Id_Cel
	 where cst.Tipus in ('3','2') -- dolgozó, vezeto
	   and f.Kod = 'AtvetelSzervezettol'
	   and getdate() between cst.ErvKezd and cst.ErvVege
	   and getdate() between cs.ErvKezd and cs.ErvVege
	   and getdate() between felh.ErvKezd and felh.ErvVege
	   and getdate() between fsz.ErvKezd and fsz.ErvVege
	   and getdate() between sz.ErvKezd and sz.ErvVege
	   and getdate() between szf.ErvKezd and szf.ErvVege
	   and getdate() between f.ErvKezd and f.ErvVege


-- kimeno adatok szukítése
--set @ObjektumSzukites = 
--	' and EREC_IraKezbesitesiTetelek.AtadasDat between ' +
--  '''' + convert(varchar(25),@FeladatHatarido-30,126) + '''' + ' and ' +
--  '''' + convert(varchar(25),@FeladatHatarido,126) + ''''

set @ObjektumSzukites = 
	' and EREC_IraKezbesitesiTetelek.AtadasDat <= ''' + convert(varchar(25),@FeladatHatarido,126) + ''''

---------------------------
--------- kimenet ---------
---------------------------
--
-- értesítés adatok
--
select @FelelosObj_Id,
	   @BazisObj_Id,
	   @Bazisido,
	   @AtfutasiIdoMin,
	   @Obj_Id_Kezelendo,
	   Csoport_Id_Felelos,
	   Csoport_Id_FelelosFelh,
	   @FeladatHatarido,
	   @Leiras + LeirasKieg,
	   '0', --@FeladatAllapot
	   @Feladat_Id,
	   @ObjektumSzukites + ' and EREC_IraKezbesitesiTetelek.Csoport_Id_Cel=''' + convert(varchar(36), Csoport_Id_Felelos) + ''''
  from #HataridosTetelek

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------


GO
