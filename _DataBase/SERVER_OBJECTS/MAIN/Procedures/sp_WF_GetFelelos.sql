IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WF_GetFelelos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_WF_GetFelelos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WF_GetFelelos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_WF_GetFelelos] AS' 
END
GO

ALTER procedure [dbo].[sp_WF_GetFelelos]
                 @FelelosObj_Id   uniqueidentifier,  -- objektumazonosító a felelős megállapításához
                 @Obj_Id_Felelos  uniqueidentifier   -- felelős oszlop azonosítója
AS
/*
-- FELADATA:
	A felelős meghatározása megadott paraméterek alapján.

-- Példa:
create table #Felelos(Csoport_Id_Felelos uniqueidentifier,Csoport_Id_FelelosFelh uniqueidentifier)
--
declare @Obj_Id uniqueidentifier, @Obj_Id_Felelos uniqueidentifier
select @Obj_Id = '16720F8D-A13F-DE11-A5A5-0015AF225A5A',
	   @Obj_Id_Felelos = '9CB812A8-A4B3-4E7A-8512-54DDA039E2BF'
exec [dbo].[sp_WF_GetFelelos] 
     @Obj_Id, @Obj_Id_Felelos
select * from #Felelos
drop table #Felelos
--
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @sqlcmd                 nvarchar(1000),
		@Dtable                 nvarchar(100),
		@Dcolumn                nvarchar(100),
        @Csoport_Id             uniqueidentifier,
        @Csoport_Id_Felelos     uniqueidentifier,
        @Csoport_Id_FelelosFelh uniqueidentifier

declare @TabFelelos Table
        (Csoport_Id_Felelos uniqueidentifier)

------------------------------
-- input paraméterek kiíratása
------------------------------
if @teszt = 1
begin
   print '----- sp_WF_GetFelelos -----'
   print '@FelelosObj_Id  = '+ isnull(convert(varchar(50),@FelelosObj_Id), 'NULL')
   print '@Obj_Id_Felelos = '+ isnull(convert(varchar(50),@Obj_Id_Felelos),'NULL')
end

-------------------------------
-- oszlopérték szerinti felelős
-------------------------------
select @Dtable   = tb.Kod,
	   @Dcolumn  = tc.Kod,
	   @Csoport_Id_Felelos     = NULL,
	   @Csoport_Id_FelelosFelh = NULL
  from KRT_ObjTipusok tb,
	   KRT_ObjTipusok tc
 where tc.Id = @Obj_Id_Felelos
   and tc.Obj_Id_Szulo = tb.Id

if @@rowcount = 0
   RAISERROR('A felelős meghatározáshoz hibásan megadott oszlop!',16,1)

set @sqlcmd = 'select ' + @Dcolumn + ' from ' + @Dtable +
			  ' where Id = ' + '''' + convert(varchar(50),@FelelosObj_Id) + ''''

insert @TabFelelos
exec (@sqlcmd)

select @Csoport_Id_Felelos = Csoport_Id_Felelos
  from @TabFelelos

select @Csoport_Id_Felelos     = Csoport_Id_Felelos,
	   @Csoport_Id_FelelosFelh = Csoport_Id_FelelosFelh
  from dbo.fn_WF_GetFelelos(@Csoport_Id_Felelos,NULL)

-- kimenet
delete #Felelos
insert #Felelos (Csoport_Id_Felelos, Csoport_Id_FelelosFelh)
		values (@Csoport_Id_Felelos, @Csoport_Id_FelelosFelh)

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------


GO
