IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WF_GetHatarido]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_WF_GetHatarido]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WF_GetHatarido]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_WF_GetHatarido] AS' 
END
GO

ALTER procedure [dbo].[sp_WF_GetHatarido]    
                 @IdoBazis        nvarchar(64),     -- az időbázis kódja
                 @BazisObj_Id     uniqueidentifier, -- objektumazonosító a dátumadat szerinti számításhoz
                 @DateCol_Id      uniqueidentifier, -- dátumoszlop azonosító azonosítója
				 @BazisIdo        datetime,         -- bázisidő
				 @AtfutasiIdoMin  numeric(8)        -- átfutási idő percben
AS
/*
-- FELADATA:
	Határidő adatok számítása megadott paraméterek alapján.

-- Példa:
create table #Hatarido(Bazisido datetime, Hatarido datetime)
declare @Obj_Id uniqueidentifier, @DateCol_Id uniqueidentifier
select @Obj_Id = convert(varchar(50),NULL), @DateCol_Id = convert(varchar(50),NULL)
exec [dbo].[sp_WF_GetHatarido] 
     '2', @Obj_Id, @DateCol_Id, getdate(), 1440
--exec [dbo].[sp_WF_GetHatarido] 
-- '4', 'EBDB4E26-9E63-DD11-9DE3-000F1FFFAE55', 'AB820AFA-F28A-DD11-8549-005056C00008', NULL, 1440
select * from #Hatarido
drop table #Hatarido
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 0

declare @error           int,
        @rowcount        int

declare @sqlcmd          nvarchar(1000),
		@Dtable          nvarchar(100),
		@Dcolumn         nvarchar(100),
		@Hatarido		 datetime

declare @TabBazisido Table
        (Bazisido    datetime)

------------------------------
-- input paraméterek kiíratása
------------------------------
if @teszt = 1
begin
   print '----- sp_WF_GetHatarido -----'
   print '@IdoBazis       = '+ isnull(convert(varchar(50),@IdoBazis),      'NULL')
   print '@BazisObj_Id    = '+ isnull(convert(varchar(50),@BazisObj_Id),   'NULL')
   print '@DateCol_Id     = '+ isnull(convert(varchar(50),@DateCol_Id),    'NULL')
   print '@BazisIdo       = '+ isnull(convert(varchar(50),@BazisIdo),      'NULL')
   print '@AtfutasiIdoMin = '+ isnull(convert(varchar(50),@AtfutasiIdoMin),'NULL')
end

----------------------
-- időadatok számítása
----------------------
set @Bazisido = NULL

-- oszlopérték szerinti bázisidő
if @Idobazis = '3' -- dátumoszlop
begin

	select @Dtable   = tb.Kod,
		   @Dcolumn  = tc.Kod,
		   @Bazisido = NULL
	  from KRT_ObjTipusok tb,
		   KRT_ObjTipusok tc
	 where tc.Id = @DateCol_Id
	   and tc.Obj_Id_Szulo = tb.Id

	if @@rowcount = 0 and @Idobazis = '3'
	   RAISERROR('Az időszámításhoz hibásan megadott dátumoszlop!',16,1)

	set @sqlcmd = 'select ' + @Dcolumn + ' from ' + @Dtable +
				  ' where Id = ' + '''' + convert(varchar(50),@BazisObj_Id) + ''''

	insert @TabBazisido
	exec (@sqlcmd)

	select @Bazisido = Bazisido from @TabBazisido

end -- oszlopérték szerinti bázisidő

-- tárgyszó szerinti bázisidő
if @Idobazis = '4' and
   @BazisObj_Id is not null
begin

	select @Bazisido = Ertek
	 from EREC_ObjektumTargyszavai
	where Obj_Id            = @BazisObj_Id
	  and Obj_Metaadatai_Id = @DateCol_Id

	if @@rowcount = 0
	   RAISERROR('Az időszámításhoz megadott dátum tárgyszó nem létezik!',16,1)

end -- tárgyszó szerinti bázisidő

-- bázisidő beállítás
if @Bazisido is NULL
	set @Bazisido =
		case when @Idobazis = '1' then getdate()
			 when @Idobazis = '2' then convert(datetime,convert(char(10),getdate(),102)+' 23:59:59:990')
			 when @Idobazis in ('3','4') then @Bazisido
			 when @Idobazis = '5' and @Bazisido is NULL then getdate()
			 when @Idobazis = '6' and @Bazisido is NULL then convert(datetime,convert(char(10),getdate(),102)+' 23:59:59:990')
			 else NULL end

-- határidő beállítás
set @Hatarido =
	case when @Bazisido is not NULL and @AtfutasiIdoMin is not NULL
         then dateadd(mi,@AtfutasiIdoMin,@Bazisido)
		 else @Bazisido end

-- kimenet
delete #Hatarido
insert #Hatarido (Bazisido, Hatarido)
values (@Bazisido, @Hatarido)

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------


GO
