IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WF_HelyettesitesEllenorzes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_WF_HelyettesitesEllenorzes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WF_HelyettesitesEllenorzes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_WF_HelyettesitesEllenorzes] AS' 
END
GO
ALTER procedure [dbo].[sp_WF_HelyettesitesEllenorzes]    

AS
/*
-- FELADATA:
	Megadott idon belül lejáró helyettesitesek ellenorzése.
    Kezelt funkció: LejaroHelyettesitesek
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @Funkcio_Kod_Kivalto    nvarchar(100),
		@Obj_MetaDefinicio_Id   uniqueidentifier,
		@ObjStateValue_Id       uniqueidentifier,
		@FeladatDefinicioTipus  nvarchar(64),
	    @FelelosObj_Id          uniqueidentifier,
	    @BazisObj_Id            uniqueidentifier,
        @Bazisido               datetime,
		@AtfutasiIdoMin         numeric(8),
		@Obj_Id                 uniqueidentifier,
        @Obj_Id_Kezelendo       uniqueidentifier,
        @Csoport_Id_Felelos     uniqueidentifier,
        @Csoport_Id_FelelosFelh uniqueidentifier,
        @FeladatHatarido        datetime,
		@Leiras                 nvarchar(400),
        @FeladatAllapot         nvarchar(64),
		@Feladat_Id             uniqueidentifier,
		@ObjektumSzukites       nvarchar(2000)

-- input adatok
select @Funkcio_Kod_Kivalto    = Funkcio_Kod_Kivalto,
	   @Obj_MetaDefinicio_Id   = Obj_MetaDefinicio_Id,
	   @ObjStateValue_Id       = ObjStateValue_Id,
	   @FeladatDefinicioTipus  = FeladatDefinicioTipus,
	   @FelelosObj_Id          = FelelosObj_Id,
	   @BazisObj_Id            = BazisObj_Id,
       @Bazisido               = Bazisido,
	   @AtfutasiIdoMin         = AtfutasiIdoMin,
	   @Obj_Id                 = Obj_Id,
       @Obj_Id_Kezelendo       = Obj_Id,
       @Csoport_Id_Felelos     = Csoport_Id_Felelos,
       @Csoport_Id_FelelosFelh = Csoport_Id_FelelosFelh,
       @FeladatHatarido        = FeladatHatarido,
	   @Leiras                 = Leiras,
	   @FeladatAllapot         = Allapot
  from #HataridosFeladatSpec

-------------------------------------------------
---------- speciális adatok kezelése ------------
-------------------------------------------------
-- adott határidon belül lejáró kölcsönzés tételek
if @Funkcio_Kod_Kivalto = 'LejaroHelyettesitesek'
BEGIN
	CREATE TABLE #LejaroHelyettesitesekTable
	(
	    Csoport_Id_FelelosFelh UNIQUEIDENTIFIER,
		Csoport_Id_Felelos UNIQUEIDENTIFIER,
		Darab INT,
		Hatarido DATETIME
		
	)
	
	insert into #LejaroHelyettesitesekTable
	(
		Csoport_Id_FelelosFelh,
		Csoport_Id_Felelos,
		Darab,
		Hatarido
	)
	select Felelos.Csoport_Id_FelelosFelh,
		   Felelos.Csoport_Id_Felelos,
		   LejaroHelyettesitesek.Darab,
		   LejaroHelyettesitesek.HelyettesitesVege		     
	  from 
	  (
	          select KRT_Helyettesitesek.Felhasznalo_ID, 
	                 count(KRT_Helyettesitesek.Id) Darab,
	                 MIN(KRT_Helyettesitesek.HelyettesitesVege) HelyettesitesVege
			  FROM
			  (
				SELECT KRT_Helyettesitesek.Felhasznalo_ID_helyettesito AS Felhasznalo_ID,* FROM KRT_Helyettesitesek KRT_Helyettesitesek
				UNION
				SELECT KRT_Helyettesitesek.Felhasznalo_ID_helyettesitett AS Felhasznalo_ID,* FROM KRT_Helyettesitesek KRT_Helyettesitesek
			  ) KRT_Helyettesitesek
			  where KRT_Helyettesitesek.HelyettesitesVege BETWEEN @Bazisido AND @FeladatHatarido --Határidon belül lejár
			        AND KRT_Helyettesitesek.HelyettesitesMod = '1' --Helyettesítés
			        AND DATEDIFF(day, KRT_Helyettesitesek.HelyettesitesKezd, KRT_Helyettesitesek.HelyettesitesVege) > 1 -- 2 napnál hosszabb
			        AND GETDATE() BETWEEN KRT_Helyettesitesek.ErvKezd AND KRT_Helyettesitesek.ErvVege -- érvényes
			  group by KRT_Helyettesitesek.Felhasznalo_ID
	  ) LejaroHelyettesitesek
	  CROSS APPLY dbo.fn_WF_GetFelelos(LejaroHelyettesitesek.Felhasznalo_ID, NULL) AS Felelos

	--Leírás
	SET @Leiras = REPLACE(@Leiras,'{1}', CONVERT(INT,@AtfutasiIdoMin/60))
	---- kimeno adatok szukítése
	set @ObjektumSzukites =
		' and KRT_Helyettesitesek.HelyettesitesMod = ''1''' +
		' and DATEDIFF(day, KRT_Helyettesitesek.HelyettesitesKezd, KRT_Helyettesitesek.HelyettesitesVege) > 1' +
		' and KRT_Helyettesitesek.HelyettesitesVege between ' +
		'''' + convert(varchar(25),@Bazisido,126) + '''' + ' and ' + 
		'''' + convert(varchar(25),@FeladatHatarido,126) + ''''


	---------------------------
	--------- kimenet ---------
	---------------------------
	select @FelelosObj_Id,
		   @BazisObj_Id,
		   @Bazisido,
		   @AtfutasiIdoMin,
		   @Obj_Id_Kezelendo,
		   LejaroHelyettesitesekTable.Csoport_Id_Felelos,
		   LejaroHelyettesitesekTable.Csoport_Id_FelelosFelh,
		   LejaroHelyettesitesekTable.Hatarido,
		   REPLACE(@Leiras,'{0}', LejaroHelyettesitesekTable.Darab) Leiras,
		   '0', --@FeladatAllapot
		   @Feladat_Id,
		   @ObjektumSzukites + ' and (KRT_Helyettesitesek.Felhasznalo_ID_helyettesito =''' + convert(varchar(36), LejaroHelyettesitesekTable.Csoport_Id_FelelosFelh) + ''' or KRT_Helyettesitesek.Felhasznalo_ID_helyettesitett =''' + convert(varchar(36), LejaroHelyettesitesekTable.Csoport_Id_FelelosFelh) + ''')'
	  from #LejaroHelyettesitesekTable LejaroHelyettesitesekTable
	  
	  DROP TABLE #LejaroHelyettesitesekTable
  
END  

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------


GO
