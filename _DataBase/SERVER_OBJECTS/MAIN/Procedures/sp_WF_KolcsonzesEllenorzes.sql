IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WF_KolcsonzesEllenorzes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_WF_KolcsonzesEllenorzes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WF_KolcsonzesEllenorzes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_WF_KolcsonzesEllenorzes] AS' 
END
GO
ALTER procedure [dbo].[sp_WF_KolcsonzesEllenorzes]    

AS
/*
-- FELADATA:
	Megadott idon belül lejáró vagy már lejárt irattári kölcsönzések ellenorzése.
    Kezelt funkció: LejaroKolcsonzesek, LejartKolcsonzesek
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @Funkcio_Kod_Kivalto    nvarchar(100),
		@Obj_MetaDefinicio_Id   uniqueidentifier,
		@ObjStateValue_Id       uniqueidentifier,
		@FeladatDefinicioTipus  nvarchar(64),
	    @FelelosObj_Id          uniqueidentifier,
	    @BazisObj_Id            uniqueidentifier,
        @Bazisido               datetime,
		@AtfutasiIdoMin         numeric(8),
		@Obj_Id                 uniqueidentifier,
        @Obj_Id_Kezelendo       uniqueidentifier,
        @Csoport_Id_Felelos     uniqueidentifier,
        @Csoport_Id_FelelosFelh uniqueidentifier,
        @FeladatHatarido        datetime,
		@Leiras                 nvarchar(400),
        @FeladatAllapot         nvarchar(64),
		@Feladat_Id             uniqueidentifier,
		@ObjektumSzukites       nvarchar(2000)

-- input adatok
select @Funkcio_Kod_Kivalto    = Funkcio_Kod_Kivalto,
	   @Obj_MetaDefinicio_Id   = Obj_MetaDefinicio_Id,
	   @ObjStateValue_Id       = ObjStateValue_Id,
	   @FeladatDefinicioTipus  = FeladatDefinicioTipus,
	   @FelelosObj_Id          = FelelosObj_Id,
	   @BazisObj_Id            = BazisObj_Id,
       @Bazisido               = Bazisido,
	   @AtfutasiIdoMin         = AtfutasiIdoMin,
	   @Obj_Id                 = Obj_Id,
       @Obj_Id_Kezelendo       = Obj_Id,
       @Csoport_Id_Felelos     = Csoport_Id_Felelos,
       @Csoport_Id_FelelosFelh = Csoport_Id_FelelosFelh,
       @FeladatHatarido        = FeladatHatarido,
	   @Leiras                 = Leiras,
	   @FeladatAllapot         = Allapot
  from #HataridosFeladatSpec

-------------------------------------------------
---------- speciális adatok kezelése ------------
-------------------------------------------------
-- adott határidon belül lejáró kölcsönzés tételek
if @Funkcio_Kod_Kivalto in ('LejaroKolcsonzesek', 'LejartKolcsonzesek')
	insert into #HataridosTetelek
	select (select Csoport_Id_Felelos
			  from dbo.fn_WF_GetFelelos(hi.FelhasznaloCsoport_Id_Kikero,NULL)
		   ) as Csoport_Id_Felelos,
		   (select Csoport_Id_FelelosFelh
			  from dbo.fn_WF_GetFelelos(hi.FelhasznaloCsoport_Id_Kikero,NULL)
		   ) as Csoport_Id_FelelosFelh,
		   ' (' + convert(nvarchar(6),hi.db)+ ' db)' as LeirasKieg
	  from (select FelhasznaloCsoport_Id_Kikero, count(*) db
			  from EREC_IrattariKikero
			 where ErvVege > getdate()
			   and Allapot = '04'
			   and KikerVege >= case when @Funkcio_Kod_Kivalto = 'LejaroKolcsonzesek'
									 then @Bazisido else KikerVege end
			   and KikerVege <  case when @Funkcio_Kod_Kivalto = 'LejaroKolcsonzesek'
									 then @FeladatHatarido else @Bazisido end
			 group by FelhasznaloCsoport_Id_Kikero
		   ) hi

---- kimeno adatok szukítése
if @Funkcio_Kod_Kivalto = 'LejaroKolcsonzesek'
set @ObjektumSzukites =
	'EREC_IrattariKikero.Allapot = ''04'' ' +
	'and EREC_IrattariKikero.KikerVege between ' +
	'''' + convert(varchar(25),@Bazisido,126) + '''' + ' and ' + 
	'''' + convert(varchar(25),@FeladatHatarido,126) + ''''

if @Funkcio_Kod_Kivalto = 'LejartKolcsonzesek'
set @ObjektumSzukites =
	'EREC_IrattariKikero.Allapot = ''04'' ' +
	'and EREC_IrattariKikero.KikerVege <  ' + 
	'''' + convert(varchar(25),@Bazisido,126) + ''''

---------------------------
--------- kimenet ---------
---------------------------
select @FelelosObj_Id,
	   @BazisObj_Id,
	   @Bazisido,
	   @AtfutasiIdoMin,
       @Obj_Id_Kezelendo,
       Csoport_Id_Felelos,
	   Csoport_Id_FelelosFelh,
       @FeladatHatarido,
       @Leiras + LeirasKieg,
       '0', --@FeladatAllapot
       @Feladat_Id,
	   @ObjektumSzukites + ' and EREC_IrattariKikero.FelhasznaloCsoport_Id_Kikero=''' + convert(varchar(36), Csoport_Id_FelelosFelh) + ''''
  from #HataridosTetelek

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------


GO
