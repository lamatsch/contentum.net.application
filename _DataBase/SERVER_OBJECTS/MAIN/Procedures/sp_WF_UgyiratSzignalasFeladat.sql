IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WF_UgyiratSzignalasFeladat]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_WF_UgyiratSzignalasFeladat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WF_UgyiratSzignalasFeladat]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_WF_UgyiratSzignalasFeladat] AS' 
END
GO
ALTER procedure [dbo].[sp_WF_UgyiratSzignalasFeladat]    

AS
/*
-- FELADATA:
	EREC_UgyUgyiratok szignálásakor az ügyfelelos számára generált feladat speciális adatainak
    beállítása.
    Kezelt funkció: UgyiratSzignalas
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1

declare @error           int,
        @rowcount        int

declare @Funkcio_Kod_Kivalto    nvarchar(100),
		@Obj_MetaDefinicio_Id   uniqueidentifier,
		@ObjStateValue_Id       uniqueidentifier,
		@WorkFlow_Id            uniqueidentifier,
		@FeladatDefinicioTipus  nvarchar(64),
	    @FelelosObj_Id          uniqueidentifier,
	    @BazisObj_Id            uniqueidentifier,
        @Bazisido               datetime,
		@AtfutasiIdoMin         numeric(8),
		@Obj_Id                 uniqueidentifier,
        @Obj_Id_Kezelendo       uniqueidentifier,
        @Csoport_Id_Felelos     uniqueidentifier,
        @Csoport_Id_FelelosFelh uniqueidentifier,
        @FeladatHatarido        datetime,
		@Leiras                 nvarchar(400),
        @FeladatAllapot         nvarchar(64),
		@Feladat_Id             uniqueidentifier

-- input adatok
select @Funkcio_Kod_Kivalto    = Funkcio_Kod_Kivalto,
	   @Obj_MetaDefinicio_Id   = Obj_MetaDefinicio_Id,
	   @ObjStateValue_Id       = ObjStateValue_Id,
	   @WorkFlow_Id            = WorkFlow_Id,
	   @FeladatDefinicioTipus  = FeladatDefinicioTipus,
	   @FelelosObj_Id          = FelelosObj_Id,
	   @BazisObj_Id            = BazisObj_Id,
       @Bazisido               = Bazisido,
	   @AtfutasiIdoMin         = AtfutasiIdoMin,
	   @Obj_Id                 = Obj_Id,
       @Obj_Id_Kezelendo       = Obj_Id,
       @Csoport_Id_Felelos     = Csoport_Id_Felelos,
       @Csoport_Id_FelelosFelh = Csoport_Id_FelelosFelh,
       @FeladatHatarido        = FeladatHatarido,
	   @Leiras                 = Leiras,
	   @FeladatAllapot         = Allapot
  from #HataridosFeladatSpec

-------------------------------------------------
---------- speciális adatok kezelése ------------
-------------------------------------------------
if @Funkcio_Kod_Kivalto = 'UgyiratSzignalas'
begin

	-- átfutási ido beállítása
	if exists (select 1 
				from EREC_UgyUgyiratok
			   where Id = @Obj_Id
				 and Surgosseg in('Azonnali'))
		set @AtfutasiIdoMin = 16*60   -- 16 óra
	else
		set @AtfutasiIdoMin = 3*1440  -- 3 nap

	-- workflow-ban lévo ügyirat szignálás leírásának kiegészítése
	if @WorkFlow_Id is not NULL
		select @Leiras = @Leiras + ' ('+ ww.Nev + ' - '+ sv.ObjStateValue + ')'
		  from EREC_WorkFlow ww
			   inner join EREC_WorkFlowFazis wf on ww.Id = wf.WorkFlow_Id and wf.Tipus = '00'
			   inner join EREC_ObjStateValue sv on sv.Id = isnull(@ObjStateValue_Id, wf.ObjStateValue_Id)
		 where ww.Id = @WorkFlow_Id

end -- UgyiratSzignalas

---------------------------
--------- kimenet ---------
---------------------------
select @FelelosObj_Id, @BazisObj_Id, @Bazisido, @AtfutasiIdoMin, @Obj_Id_Kezelendo,
       @Csoport_Id_Felelos, @Csoport_Id_FelelosFelh, @FeladatHatarido, @Leiras,
       @FeladatAllapot, @Feladat_Id, NULL ObjektumSzukites

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------


GO
