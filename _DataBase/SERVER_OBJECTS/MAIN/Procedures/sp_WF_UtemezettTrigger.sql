IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WF_UtemezettTrigger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_WF_UtemezettTrigger]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WF_UtemezettTrigger]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_WF_UtemezettTrigger] AS' 
END
GO

ALTER procedure [dbo].[sp_WF_UtemezettTrigger]    
				 @FunkcioKod    nvarchar(100), -- az ellenőrzést kiváltó funkció kódja
				 @GetDefinicio  char(1) = 'N'  -- feladatdefiníció lekérdezés
AS
/*
-- FELADATA:
	Adott ellenőrzés indítása a funkciókód alapján paraméterezett sp_WF_EsemenyTrigger
    programhívással.

-- Példák:
exec [dbo].[sp_WF_UtemezettTrigger] 'LejartUgyiratHatarido'
--
--Ütemezett feladatgenerálás:
--
--LejartAtvetelek - 30 nap
--LejaroKolcsonzesek
--LejartKolcsonzesek
--LejaroUgyiratHatarido
--LejartUgyiratHatarido
--LejartSkotroHatarido
--
select * from KRT_Funkciok where Kod = 'LejartUgyiratHatarido'
update KRT_Funkciok set FeladatJelzo = '1' where Kod = 'LejartUgyiratHatarido'
*/
---------
---------
BEGIN TRY

declare @teszt       int
select  @teszt = 1

declare @Funkcio_Id   uniqueidentifier

------------------------------
-- input paraméter kiíratása
------------------------------
if @teszt = 0
begin
   print '----- sp_WF_UtemezettTrigger -----'
   print '@FunkcioKod = '+ isnull(convert(varchar(50),@FunkcioKod),'NULL')
end

---------------------------------
-- a funkcióazonosító lekérdezése
---------------------------------
select @Funkcio_Id   = Id
  from KRT_Funkciok
 where Kod = @FunkcioKod
   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'

if @@rowcount = 0
   RAISERROR('Az indítandó határidős ellenőrzéshez nincs definiálva érvényes funkció!',16,1)

-----------------------
-- feldolgozás indítása
-----------------------
exec [dbo].[sp_WF_EsemenyTrigger] 
	 NULL, NULL, @Funkcio_Id, 1, @GetDefinicio

END TRY
-----------
-----------
BEGIN CATCH
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------


GO
