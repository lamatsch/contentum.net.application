IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_eMailServiceGetAnswerEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_eMailServiceGetAnswerEmail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_eMailServiceGetAnswerEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_eMailServiceGetAnswerEmail] AS' 
END
GO
ALTER PROCEDURE [dbo].[sp_eMailServiceGetAnswerEmail]
	@KuldemenyId	uniqueidentifier = null,
	@UgyiratId	uniqueidentifier = null
AS
BEGIN
begin try
	SET NOCOUNT ON;
	if @KuldemenyId != '00000000-0000-0000-0000-000000000000' and @KuldemenyId is not null
	BEGIN
		select	EREC_KuldKuldemenyek.id,
				EREC_KuldKuldemenyek.barcode,

	N'Válasz: ' + EREC_KuldKuldemenyek.Targy as Subject,
				EREC_KuldKuldemenyek.CimSTR_Bekuldo as ToAddress,
				N'<u>Tisztelt Címzett!</u><br><br>
	Az Ön által beküldött dokumentumot rendszerünkben a mai napon nyilvántartásba vettük.<br><br>'+
	'<u>Iktatószáma:</u> <b>' + CAST(EREC_UgyUgyiratok.Foszam as nvarchar(10)) + ' - ' + cast(EREC_IraIratok.Alszam as nvarchar(10)) + ' / ' + cast(EREC_IraIktatokonyvek.Ev as nvarchar(4)) + ' / ' + EREC_IraIktatokonyvek.MegkulJelzes + '</b><br>'+
	'<u>Ügyintézoje:</u> <b>' + KRT_Csoportok.Nev + '</b><br>' +
	isnull('<u>Vonalkód:</u> <b>' + EREC_kuldKuldemenyek.BarCode + '</b><br>' , '')
	 as Body
			from EREC_KuldKuldemenyek
				inner join EREC_IraIratok on EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id
				inner join EREC_UgyUgyiratDarabok on EREC_UgyUgyiratDarabok.Id = EREC_IraIratok.UgyUgyiratDarab_Id
				inner join EREC_UgyUgyiratok on EREC_UgyUgyiratok.Id = EREC_UgyUgyiratDarabok.UgyUgyirat_Id and EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez is not null
				inner join EREC_IraIktatokonyvek on EREC_IraIktatokonyvek.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
				inner join KRT_Csoportok on KRT_Csoportok.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez
			where EREC_KuldKuldemenyek.Id = @KuldemenyId
	END
	else if @UgyiratId != '00000000-0000-0000-0000-000000000000' and @UgyiratId is not null
	BEGIN
		select	EREC_KuldKuldemenyek.Id,
				EREC_KuldKuldemenyek.barcode,
				N'Válasz: ' + EREC_KuldKuldemenyek.Targy as Subject,
				EREC_KuldKuldemenyek.CimSTR_Bekuldo as ToAddress,
				N'<u>Tisztelt Címzett!</u><br><br>
					Az Ön által beküldött dokumentumot rendszerünkben a mai napon nyilvántartásba vettük.<br><br>'+
					'<u>Iktatószáma:</u> <b>' + CAST(EREC_UgyUgyiratok.Foszam as nvarchar(10)) + ' - ' + cast(EREC_IraIratok.Alszam as nvarchar(10)) + ' / ' + cast(EREC_IraIktatokonyvek.Ev as nvarchar(4)) + ' / ' + EREC_IraIktatokonyvek.MegkulJelzes + '</b><br>'+
					'<u>Ügyintézoje:</u> <b>' + KRT_Csoportok.Nev + '</b><br>' +
					isnull('<u>Vonalkód:</u> <b>' + EREC_kuldKuldemenyek.BarCode + '</b><br>' , '')
				as Body

			from EREC_IraIratok
				inner join EREC_UgyUgyiratdarabok on EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyiratDarab_Id
				inner join EREC_UgyUgyiratok on EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				inner join KRT_Csoportok on KRT_Csoportok.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez
				inner join EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_IraIratok.KuldKuldemenyek_Id
				inner join EREC_IraIktatokonyvek on EREC_IraIktatokonyvek.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
			where EREC_IraIratok.Alszam = 1
	END
end try
begin catch
  declare @errorSeverity integer,
          @errorState integer,
          @errorCode nvarchar(1000);
  select @errorSeverity = ERROR_SEVERITY(), @errorState = ERROR_STATE();

  if ERROR_NUMBER() < 50000
    set @errorCode = '[' + convert(nvarchar(10), ERROR_NUMBER()) + '] ' + '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();
  else
    set @errorCode = '(' + ERROR_PROCEDURE() + ':' + convert(nvarchar(10), ERROR_LINE()) + ')' + ERROR_MESSAGE();

  if @errorState = 0 set @errorState = 1
  raiserror(@errorCode, @errorSeverity, @errorState)
end catch
END


GO
