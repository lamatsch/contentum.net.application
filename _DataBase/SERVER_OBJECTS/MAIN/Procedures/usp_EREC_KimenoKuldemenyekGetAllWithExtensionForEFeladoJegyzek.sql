IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_EREC_KimenoKuldemenyekGetAllWithExtensionForEFeladoJegyzek]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_EREC_KimenoKuldemenyekGetAllWithExtensionForEFeladoJegyzek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_EREC_KimenoKuldemenyekGetAllWithExtensionForEFeladoJegyzek]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_EREC_KimenoKuldemenyekGetAllWithExtensionForEFeladoJegyzek] AS' 
END
GO

ALTER procedure [dbo].[usp_EREC_KimenoKuldemenyekGetAllWithExtensionForEFeladoJegyzek]
  @ExecutorUserId uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier,
  @PostaKonyv_Id uniqueidentifier, -- nem lehet null!
  @Kikuldo_Id uniqueidentifier = null, -- Csoport_Id -  ha meg van adva, erre is szurunk
  @StartDate datetime = null,
  @EndDate datetime = null,
  @Jogosultak		char(1) = '0',
  @StartPage int = 1, -- kezdooldal sorszama
  --@PageCount int = null, -- visszaadando oldalak szama, ha null, akkor mind
  @pageNumber		int = 0,
  @pageSize		int = -1,
  --@SelectedRowId	uniqueidentifier = NULL,
  @WindowSize int = 100000, -- egy fajlba exportalhato tetelek max. szama, ha XML-t generalunk, oldalmeret egyebkent
  @ForXml char(1) = '0', -- '0' eseten tablat, '1' eseten a @WindowWith-nek megfeleloen darabolt XML_eket ad vissza
  @ForStatistics char(1) = '0', -- '1' eseten tartalmazza az id-t es szamadatokat a rekordokrol (hibas, osszes, oldalszam stb.)
							   -- egyebkent ill. '0' eseten normal lekeres, 
  @IgnoreKozonseges char(1) = '0', -- ha '1', a kozonseges kuldemenyeket nem adjuk vissza
  @IgnoreNyilvantartott char(1) = '0' -- ha '1', a nyilvantartott kuldemenyeket nem adjuk vissza
as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

/*
declare @notnull_kozonseges_tetelek table (col varchar(15))
insert into @notnull_kozonseges_tetelek
select 'sorszam'
union all select 'alapszolg'
union all select 'suly'
union all select 'darab'
union all select 'dij'

declare @notnull_nyilvantartott_tetelek table (col varchar(15))
insert into @notnull_nyilvantartott_tetelek
select 'sorszam'
union all select 'azonosito'
union all select 'alapszolg'
union all select 'cimzett_irsz'
union all select 'suly'
union all select 'cimzett_nev'
union all select 'cimzett_hely'
union all select 'dij'
*/

/*---------------------------------
Közönséges küldemények
----------------------------------*/

IF OBJECT_ID('tempdb..#tmp_kozonsegeskuldemenyek') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_kozonsegeskuldemenyek
END

CREATE TABLE #tmp_kozonsegeskuldemenyek (
	sorszam int not null,--sorszam varchar(3) not null, -- min. 1
	alapszolg varchar(9), --not null, -- min. 7
	viszonylat varchar(2), -- min. 1
	orszagkod varchar(3), -- min. 2
	suly varchar(5), --not null, -- min. 1
	darab varchar(6), --not null, -- min. 1
	kulonszolgok varchar(200),
	vakok_irasa char(1),
	meret varchar(2), -- min. 1
	dij varchar(9), --not null, -- min. 1 
	gepre_alkalmassag char(1)
)

if @IgnoreKozonseges != '1'
BEGIN
	IF OBJECT_ID('tempdb..#tmp_kozonsegeskuldemenyek_with_id') IS NOT NULL 
	BEGIN
	   DROP TABLE #tmp_kozonsegeskuldemenyek_with_id
	END

	CREATE TABLE #tmp_kozonsegeskuldemenyek_with_id (
		id uniqueidentifier,
		sorszam int, -- not null,--sorszam varchar(3) not null, -- min. 1
		alapszolg varchar(9), --not null, -- min. 7
		viszonylat varchar(2), -- min. 1
		orszagkod varchar(3), -- min. 2
		suly int,--varchar(5), --not null, -- min. 1
		darab int, --varchar(6), --not null, -- min. 1
		kulonszolgok varchar(200),
		vakok_irasa char(1),
		meret varchar(2), -- min. 1
		dij int, --varchar(9), --not null, -- min. 1 
		gepre_alkalmassag char(1)
	)

	;WITH KozonsegesKuldemenyek as
	(
	select
	 EREC_KuldKuldemenyek.Id,
	 alapszolg = case
		when KimenoKuldemenyFajta in ('01','02','03','04','05','06','07','K1_01','K1_02','K1_03','K1_04','K1_05','K1_06','K1_07','K1_08','K1_09','K2_01','K2_02','K2_03','K2_04','K2_05','K2_06','K2_07','K2_08','K2_09') then 'A_111_LEV'
		when KimenoKuldemenyFajta in ('18','K1_10','K2_10') then 'A_111_LLP'
		else null
		end
	, viszonylat = case IsNull(KRT_Orszagok.Kod, 'HU') -- Europai K1, Egyéb K2
		when 'HU' then null
		else KRT_Orszagok.Viszonylatkod
	end
	, orszagkod = case IsNull(KRT_Orszagok.Kod, 'HU') -- ha előző nem adott
		when 'HU' then null
		else case
			when KRT_Orszagok.Viszonylatkod is null then KRT_Orszagok.Kod
			else null
		end
	end
	, suly = case
		when KimenoKuldemenyFajta='01' then 30
		when KimenoKuldemenyFajta in ('02','K1_03','K2_03') then 50
		when KimenoKuldemenyFajta in ('03','K1_04','K2_04') then 100
		when KimenoKuldemenyFajta in ('04','K1_05','K2_05') then 250
		when KimenoKuldemenyFajta in ('05','K1_06','K2_06') then 500
		when KimenoKuldemenyFajta='06' then 750
		when KimenoKuldemenyFajta in ('07','K1_09','K2_09') then 2000
		when KimenoKuldemenyFajta='18' then
			case when IsNull(KRT_Orszagok.Kod, 'HU') = 'HU' then 30
			else 20
			end
		when KimenoKuldemenyFajta in ('K1_01','K2_01','K1_02','K2_02','K1_10','K2_10') then 20
		when KimenoKuldemenyFajta in ('K1_07','K2_07') then 1000
		when KimenoKuldemenyFajta in ('K1_08','K2_08') then 1500
		else null -- valódi súly, itt: hiba!!!
		end
	, PeldanySzam as darab
	, kulonszolgok = case Elsobbsegi
		when 1 then 'K_PRI' -- K_FEL
		else null
		end
	, null vakok_irasa
	, meret = case
		when KimenoKuldemenyFajta in ('01','K1_01','K2_01') then 1
		when KimenoKuldemenyFajta in ('18','K1_10','K2_10') then null
		else 0
		end
	, cast(Ar as int) dij
	, null gepre_alkalmassag
	from dbo.EREC_KuldKuldemenyek as EREC_KuldKuldemenyek
	left join dbo.EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Postakonyv on EREC_IraIktatoKonyvek_Postakonyv.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
	left join dbo.KRT_Cimek as KRT_Cimek on KRT_Cimek.Id=EREC_KuldKuldemenyek.Cim_Id
	left join dbo.KRT_Orszagok as KRT_Orszagok on KRT_Orszagok.Id=KRT_Cimek.Orszag_Id
	where
	EREC_IraIktatoKonyvek_PostaKonyv.IktatoErkezteto='P'
	and EREC_IraIktatoKonyvek_PostaKonyv.Id = @PostaKonyv_Id
	and PostazasIranya='2' -- kimenő
	and Allapot='06' -- Postázott
	--and KuldesMod in ('01', '17') -- Postai sima, Postai elsőbbségi
	and KimenoKuldemenyFajta in ('01','02','03','04','05','06','07','18','K1_01','K1_02','K1_03','K1_04','K1_05','K1_06','K1_07','K1_08','K1_09','K1_10','K2_01','K2_02','K2_03','K2_04','K2_05','K2_06','K2_07','K2_08','K2_09','K2_10')
	and Ajanlott = 0
	and Tertiveveny = 0
	and SajatKezbe = 0
	and E_ertesites = 0
	and E_elorejelzes = 0
	and PostaiLezaroSzolgalat = 0
	and EREC_KuldKuldemenyek.BelyegzoDatuma between IsNull(@StartDate, '1900-01-01') and IsNull(@EndDate, getdate())
	and (@Kikuldo_Id is null or EREC_KuldKuldemenyek.Csoport_Id_Cimzett = @Kikuldo_Id)
	)
	insert into #tmp_kozonsegeskuldemenyek_with_id
	select
	 Id,
	 (select sorszam from (
	select row_number() over (order by kulonszolgok desc, alapszolg, viszonylat, orszagkod, suly desc) sorszam
	, alapszolg
	, viszonylat
	, orszagkod
	, suly
	--, sum(darab) darab
	, kulonszolgok
	, vakok_irasa
	, meret
	--, sum(dij) dij
	, gepre_alkalmassag
	from KozonsegesKuldemenyek
	group by kulonszolgok, alapszolg, viszonylat, orszagkod, suly, meret, gepre_alkalmassag, vakok_irasa
	) tmp
	where ((tmp.kulonszolgok is null and KozonsegesKuldemenyek.kulonszolgok is null)
		or (tmp.kulonszolgok = KozonsegesKuldemenyek.kulonszolgok))
	and ((tmp.alapszolg is null and KozonsegesKuldemenyek.alapszolg is null)
		or (tmp.alapszolg = KozonsegesKuldemenyek.alapszolg))
	and ((tmp.viszonylat is null and KozonsegesKuldemenyek.viszonylat is null)
		or (tmp.viszonylat = KozonsegesKuldemenyek.viszonylat))
	and ((tmp.orszagkod is null and KozonsegesKuldemenyek.orszagkod is null)
		or (tmp.orszagkod = KozonsegesKuldemenyek.orszagkod))
	and ((tmp.suly is null and KozonsegesKuldemenyek.suly is null)
		or (tmp.suly = KozonsegesKuldemenyek.suly))
	and ((tmp.meret is null and KozonsegesKuldemenyek.meret is null)
		or (tmp.meret = KozonsegesKuldemenyek.meret))
	and ((tmp.gepre_alkalmassag is null and KozonsegesKuldemenyek.gepre_alkalmassag is null)
		or (tmp.gepre_alkalmassag = KozonsegesKuldemenyek.gepre_alkalmassag))
	and ((tmp.vakok_irasa is null and KozonsegesKuldemenyek.vakok_irasa is null)
		or (tmp.vakok_irasa = KozonsegesKuldemenyek.vakok_irasa))) sorszam
	, alapszolg
	, viszonylat
	, orszagkod
	, suly
	, darab--, sum(darab) darab
	, kulonszolgok
	, vakok_irasa
	, meret
	, dij--, sum(dij) dij
	, gepre_alkalmassag
	from KozonsegesKuldemenyek

	insert into #tmp_kozonsegeskuldemenyek
	select sorszam--row_number() over (order by kulonszolgok desc, alapszolg, viszonylat, orszagkod, suly desc) sorszam
	, alapszolg
	, viszonylat
	, orszagkod
	, suly
	, sum(darab) darab
	, kulonszolgok
	, vakok_irasa
	, meret
	, sum(dij) dij
	, gepre_alkalmassag
	from #tmp_kozonsegeskuldemenyek_with_id 
	group by sorszam, kulonszolgok, alapszolg, viszonylat, orszagkod, suly, meret, gepre_alkalmassag, vakok_irasa
	order by sorszam


END

/*---------------------------------
Nyilvántartott küldemények
----------------------------------*/

	IF OBJECT_ID('tempdb..#tmp_nyilvantartottkuldemenyek') IS NOT NULL 
	BEGIN
	   DROP TABLE #tmp_nyilvantartottkuldemenyek
	END

	CREATE TABLE #tmp_nyilvantartottkuldemenyek (
		sorszam int not null,
		azonosito varchar(16), --not null, -- min. 13
		alapszolg varchar(9), --not null, -- min. 1
		orszagkod varchar(3), -- min. 2
		suly varchar(5), --not null, -- min. 1
		kulonszolgok varchar(200),
		vakok_irasa char(1),
		uv_osszeg varchar(9), -- min. 1
		uv_lapid char(13),
		eny_osszeg varchar(9), -- min. 1
		kezelesi_mod char(1),
		meret varchar(2), -- min. 1
		cimzett_nev varchar(35), --not null, -- min. 3
		cimzett_irsz varchar(4), --not null, -- 0 v. 4	
		cimzett_hely varchar(35), --not null, -- min. 3
		-- BUG_963
		cimzett_kozelebbi_cim varchar(45),  -- bontott cimet adunk, ez üresen marad (kivéve HRSZ)
		cimzett_kozterulet_nev varchar(80),
		cimzett_kozterulet_jelleg varchar(80),
		cimzett_hazszam varchar(30),
		cimzett_epulet varchar(10),
		cimzett_lepcsohaz varchar(10),
		cimzett_emelet varchar(10),
		cimzett_ajto varchar(10),
		cimzett_postafiok varchar(10),
		--
		dij varchar(9), --not null, -- min. 1 
		potlapszam varchar(2),
		sajat_azonosito varchar(30),
		gepre_alkalmassag char(1)
	)

if @IgnoreNyilvantartott != '1'
BEGIN
	IF OBJECT_ID('tempdb..#tmp_nyilvantartottkuldemenyek_with_id') IS NOT NULL 
	BEGIN
	   DROP TABLE #tmp_nyilvantartottkuldemenyek_with_id
	END

	CREATE TABLE #tmp_nyilvantartottkuldemenyek_with_id (
		id uniqueidentifier,
		sorszam int not null,
		azonosito varchar(16), --not null, -- min. 13
		alapszolg varchar(9), --not null, -- min. 1
		orszagkod varchar(3), -- min. 2
		suly varchar(5), --not null, -- min. 1
		kulonszolgok varchar(200),
		vakok_irasa char(1),
		uv_osszeg varchar(9), -- min. 1
		uv_lapid char(13),
		eny_osszeg varchar(9), -- min. 1
		kezelesi_mod char(1),
		meret varchar(2), -- min. 1
		cimzett_nev varchar(35), --not null, -- min. 3
		cimzett_irsz varchar(4), --not null, -- 0 v. 4	
		cimzett_hely varchar(35), --not null, -- min. 3
		-- BUG_963
		cimzett_kozelebbi_cim varchar(45),  -- bontott cimet adunk, ez üresen marad (kivéve HRSZ)
		cimzett_kozterulet_nev varchar(80),
		cimzett_kozterulet_jelleg varchar(80),
		cimzett_hazszam varchar(30),
		cimzett_epulet varchar(10),
		cimzett_lepcsohaz varchar(10),
		cimzett_emelet varchar(10),
		cimzett_ajto varchar(10),
		cimzett_postafiok varchar(10),
		--
		dij varchar(9), --not null, -- min. 1 
		potlapszam varchar(2),
		sajat_azonosito varchar(30),
		gepre_alkalmassag char(1)
	)

	insert into #tmp_nyilvantartottkuldemenyek_with_id
	select
	 EREC_KuldKuldemenyek.Id,
	 (IsNull((select Max(cast(sorszam as int)) from #tmp_kozonsegeskuldemenyek), 0)+ row_number() over (order by case SUBSTRING(Ragszam, 1, 2)
		when 'EL' then 101 -- 1. Tételesen kezelt küldemények
		when 'RB' then 102 -- 1. Tételesen kezelt küldemények
		when 'VV' then 103 -- 1. Tételesen kezelt küldemények
		when 'RL' then 201 -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
		when 'TRL' then 202 -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
		when 'KR' then 203 -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
		when 'RR' then 204 -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
		else null end, RagSzam)) as sorszam, -- sorszam
	cast(RagSzam as varchar(16)) azonosito
	--sorrendkod = case SUBSTRING(Ragszam, 1, 2)  -- segédmező a rendezéshez, nem adjuk vissza
	--	when 'EL' then 101 -- 1. Tételesen kezelt küldemények
	--	when 'RB' then 102 -- 1. Tételesen kezelt küldemények
	--	when 'VV' then 103 -- 1. Tételesen kezelt küldemények
	--	when 'RL' then 201 -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
	--	when 'TRL' then 202 -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
	--	when 'KR' then 203 -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
	--	when 'RR' then 204 -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
	--	else null
	--end
	, alapszolg = case
		when KimenoKuldemenyFajta in ('01','02','03','04','05','06','07','K1_01','K1_02','K1_03','K1_04','K1_05','K1_06','K1_07','K1_08','K1_09','K2_01','K2_02','K2_03','K2_04','K2_05','K2_06','K2_07','K2_08','K2_09') then 'A_111_LEV'
		when KimenoKuldemenyFajta in ('18','K1_10','K2_10') then 'A_111_LLP'
		when KimenoKuldemenyFajta in ('19','20') then 'A_15_HIV'
		else null
		end
	, orszagkod = case IsNull(KRT_Orszagok.Kod, 'HU')
		when 'HU' then null
		else KRT_Orszagok.Kod
	end
	, suly = case
		when KimenoKuldemenyFajta='01' then 30
		when KimenoKuldemenyFajta in ('02','K1_03','K2_03') then 50
		when KimenoKuldemenyFajta in ('03','K1_04','K2_04') then 100
		when KimenoKuldemenyFajta in ('04','K1_05','K2_05') then 250
		when KimenoKuldemenyFajta in ('05','K1_06','K2_06') then 500
		when KimenoKuldemenyFajta='06' then 750
		when KimenoKuldemenyFajta in ('07','K1_09','K2_09') then 2000
		when KimenoKuldemenyFajta='18' then
			case when IsNull(KRT_Orszagok.Kod, 'HU') = 'HU' then 30
			else 20
			end
		when KimenoKuldemenyFajta='19' then 30 -- valódi súly?
		when KimenoKuldemenyFajta='20' then 30 -- valódi súly
		when KimenoKuldemenyFajta in ('K1_01','K2_01','K1_02','K2_02','K1_10','K2_10') then 20
		when KimenoKuldemenyFajta in ('K1_07','K2_07') then 1000
		when KimenoKuldemenyFajta in ('K1_08','K2_08') then 1500
		else null -- valódi súly, itt: hiba!!!
		end
	, kulonszolgok = stuff((select
	 case Elsobbsegi
		when 1 then ',K_PRI' -- K_FEL
		else ''
		end
	+
	case 
		when Ajanlott=1 then ',K_AJN'
		when KimenoKuldemenyFajta in ('19', '20') then  ',K_AJN' -- Hivatalos irat
		else ''
		end
	+
	case 
		when Tertiveveny=1 then ',K_TEV'
		when KimenoKuldemenyFajta in ('19', '20') then  ',K_TEV' -- Hivatalos irat
		else ''
		end
	+
	case PostaiLezaroSzolgalat
		when 1 then ',K_LEZ'
		else ''
		end
	+
	case 
		when SajatKezbe=1 then ',K_SKZ'
		when KimenoKuldemenyFajta = '20' then ',K_SKZ' -- Hivatalos irat saját kézbe
		else ''
		end
	+
	case E_ertesites
		when 1 then ',K_EFF'
		else ''
		end
	+
	case E_elorejelzes
		when 1 then ',K_EFC'
		else ''
		end
	for xml PATH('')),1,1,'' )
	, null vakok_irasa
	, null uv_osszeg
	, null uv_lapid
	, null eny_osszeg
	, null kezelesi_mod -- = case Erteknyilvantartott when 1 then '3' else '2' end
	, meret = case
		when KimenoKuldemenyFajta in ('01','K1_01','K2_01') then 1
		when KimenoKuldemenyFajta in ('18','K1_10','K2_10') then null
		when KimenoKuldemenyFajta in ('19','20') then null -- A_15_HIV mellett nem szerepelhet
		else 0
		end
	, cast(NevSTR_Bekuldo as varchar(35)) cimzett_nev
	, cimzett_irsz = case IsNull(KRT_Orszagok.Kod, 'HU')
		when 'HU' then cast(KRT_Cimek.IRSZ as varchar(4))
		else null --'' -- kötelező
	end
	, cast(ISNULL(KRT_Cimek.TelepulesNev,'  ') as varchar(35)) cimzett_hely  -- min 2 karakter
	--, cast((KRT_Cimek.KozteruletNev + ' ' + KRT_Cimek.KozteruletTipusNev + ' ' + KRT_Cimek.Hazszam) as varchar(45)) cimzett_kozelebbi_cim
	-- BUG_963
	--,cast(dbo.fn_MergeCim(KRT_Cimek.Tipus,
	--	   null,
	--	   null,
	--	   KRT_Cimek.TelepulesNev,
	--	   KRT_Cimek.KozteruletNev,
	--	   KRT_Cimek.KozteruletTipusNev,
	--	   KRT_Cimek.Hazszam,
	--	   KRT_Cimek.Hazszamig,
	--	   KRT_Cimek.HazszamBetujel,
	--	   KRT_Cimek.Lepcsohaz,
	--	   KRT_Cimek.Szint,
	--	   KRT_Cimek.Ajto,
	--	   KRT_Cimek.AjtoBetujel,
	--	   KRT_Cimek.CimTobbi) as varchar(45)) cimzett_kozelebbi_cim
	, Cast(CASE WHEN KRT_Cimek.HRSZ is NULL OR KRT_Cimek.HRSZ='' THEN '' ELSE 
		   CASE WHEN KRT_Cimek.TelepulesNev is NULL OR KRT_Cimek.TelepulesNev = '' THEN '' ELSE KRT_Cimek.TelepulesNev + ' HRSZ.' + KRT_Cimek.HRSZ END 
		   END as varchar(45)) cimzett_kozelebbi_cim 
	, Cast(KRT_Cimek.KozteruletNev as varchar(80)) cimzett_kozterulet_nev
	, Cast(KRT_Cimek.KozteruletTipusNev as varchar( 80)) cimzett_kozterulet_jelleg
	, Cast(CASE WHEN KRT_Cimek.Tipus = '02' THEN NULL ELSE KRT_Cimek.Hazszam + CASE WHEN  KRT_Cimek.Hazszamig is NULL OR  KRT_Cimek.Hazszamig = '' THEN '' ELSE ' - ' +  KRT_Cimek.Hazszamig END END as varchar(30)) cimzett_hazszam
	, Cast(KRT_Cimek.HazszamBetujel as varchar(10)) cimzett_epulet
	, Cast(KRT_Cimek.Lepcsohaz as varchar(10)) cimzett_lepcsohaz
	, Cast(KRT_Cimek.Szint as varchar(10)) cimzett_emelet
	, Cast(KRT_Cimek.Ajto + CASE WHEN KRT_Cimek.AjtoBetujel is NULL OR KRT_Cimek.AjtoBetujel = '' THEN '' ELSE ' / ' + KRT_Cimek.AjtoBetujel END as varchar(10)) cimzett_ajto
	, CAST(CASE WHEN KRT_Cimek.Tipus = '02' THEN KRT_Cimek.Hazszam ELSE NULL END as varchar(10)) cimzett_postafiok
	--
   , cast(Ar as int) dij
	, cast(null as varchar(2)) potlapszam -- K_POT
	, cast(null as varchar(30)) sajat_azonosito -- case K_ETV különszolg when 1 then  ...  else null
	, cast(null as char(1)) gepre_alkalmassag -- case K_FEL különszolg  when 1 then case ... 'E' v. 'G' else null
	from dbo.EREC_KuldKuldemenyek as EREC_KuldKuldemenyek
	left join dbo.EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Postakonyv on EREC_IraIktatoKonyvek_Postakonyv.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
	left join dbo.KRT_Cimek as KRT_Cimek on KRT_Cimek.Id=EREC_KuldKuldemenyek.Cim_Id
	left join dbo.KRT_Orszagok as KRT_Orszagok on KRT_Orszagok.Id=KRT_Cimek.Orszag_Id
	where
	EREC_IraIktatoKonyvek_PostaKonyv.IktatoErkezteto='P'
	and EREC_IraIktatoKonyvek_PostaKonyv.Id = @PostaKonyv_Id
	and PostazasIranya='2' -- kimenő
	and Allapot='06' -- Postázott
	--and KuldesMod in ('01', '17') -- Postai sima, Postai elsőbbségi
	and KimenoKuldemenyFajta in ('01','02','03','04','05','06','07','18','19','20','K1_01','K1_02','K1_03','K1_04','K1_05','K1_06','K1_07','K1_08','K1_09','K1_10','K2_01','K2_02','K2_03','K2_04','K2_05','K2_06','K2_07','K2_08','K2_09','K2_10')
	and 1 = case
		when KimenoKuldemenyFajta in ('19', '20') then 1
		when IsNull(Ajanlott, 0) + IsNull(Tertiveveny, 0) + IsNull(SajatKezbe, 0) + IsNull(E_ertesites, 0) + IsNull(E_elorejelzes, 0) + IsNull(PostaiLezaroSzolgalat, 0) > 0 then 1
		else 0
	end
	and EREC_KuldKuldemenyek.BelyegzoDatuma between IsNull(@StartDate, '1900-01-01') and IsNull(@EndDate, getdate())
	and (@Kikuldo_Id is null or EREC_KuldKuldemenyek.Csoport_Id_Cimzett = @Kikuldo_Id)

	insert into #tmp_nyilvantartottkuldemenyek
	select sorszam--IsNull((select Max(cast(sorszam as int)) from #tmp_kozonsegeskuldemenyek), 0)+ row_number() over (order by sorrendkod, azonosito) sorszam
	, azonosito --azonosito - kötelező
	, alapszolg
	, orszagkod
	, suly
	, kulonszolgok
	, vakok_irasa
	, uv_osszeg
	, uv_lapid
	, eny_osszeg
	, kezelesi_mod
	, meret
	, cimzett_nev -- kotelezo
	, cimzett_irsz --cimzett_irsz
	, cimzett_hely --cimzett_hely -- kotelezo
	-- BUG_963
	, cimzett_kozelebbi_cim
	, cimzett_kozterulet_nev
	, cimzett_kozterulet_jelleg
	, cimzett_hazszam
	, cimzett_epulet
	, cimzett_lepcsohaz
	, cimzett_emelet
	, cimzett_ajto
	, cimzett_postafiok
	--
	, dij -- kotelezo
	, potlapszam
	, sajat_azonosito
	, gepre_alkalmassag
	from #tmp_nyilvantartottkuldemenyek_with_id--Nyilvantartottkuldemenyek
	--order by sorszam
END

/*-----------------------------------------------------------------------
EREDMENYEK VISSZAADASA
-------------------------------------------------------------------------*/
if IsNull(@WindowSize, 0) <= 0
begin
	set @WindowSize = 100000
end

if IsNull(@StartPage, 0) <= 0
begin
	set @StartPage = 1
end


declare @cnt_kozonseges int
declare @cnt_nyilvantartott int
declare @cnt_all int
set @cnt_kozonseges = (select count(*) from #tmp_kozonsegeskuldemenyek)
set @cnt_nyilvantartott = (select count(*) from #tmp_nyilvantartottkuldemenyek)
set @cnt_all = @cnt_kozonseges + @cnt_nyilvantartott

declare @mutato_start int
declare @mutato_end int
set @mutato_start = (@StartPage - 1) * @WindowSize + 1
if @StartPage > 1
begin
	set @cnt_all = @cnt_all - @mutato_start + 1
end

if @ForXml = '0'
begin

	-- mindent visszadunk es nem modositjuk a sorszamot
	set @mutato_end = @mutato_start - 1 + @WindowSize

	-- lapozas
	declare @firstRow int
	declare @lastRow int

	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = @pageNumber * @pageSize
		set @lastRow = @firstRow + @pageSize - 1

		if (@mutato_start + @lastRow < @mutato_end)
		begin
			set @mutato_end = @mutato_start + @lastRow
		end
		set @mutato_start = @mutato_start + @firstRow
	end
--
--	select @WindowSize as WindowWidth
--	, @cnt_kozonseges as cnt_kozonseges
--	, @cnt_nyilvantartott as cnt_nyilvantartott
--	, @cnt_all as cnt_all
--	, @mutato_start as mutato_start
--	, @mutato_end as mutato_end
--	, @pageSize as pageSize
--	, @pageNumber as pageNumber
----	, @pagecountall as pagecountall
----	--, @pagecounter as pagecounter
----	, @PageCount as PageCount

	-- lapozas info
	select @cnt_all as RecordNumber, @pageNumber as PageNumber;

	if @IgnoreKozonseges != '1'
	BEGIN
		-- kozonseges
		select #tmp_kozonsegeskuldemenyek.*, tmp.ids from #tmp_kozonsegeskuldemenyek
			cross apply (select STUFF((SELECT ';' + convert(varchar(36), Id) from #tmp_kozonsegeskuldemenyek_with_id
				where #tmp_kozonsegeskuldemenyek.sorszam=#tmp_kozonsegeskuldemenyek_with_id.sorszam
				FOR XML PATH('')),1, 1, '') ids) tmp
			where #tmp_kozonsegeskuldemenyek.sorszam between @mutato_start and @mutato_end
			order by #tmp_kozonsegeskuldemenyek.sorszam
	--
	--	select * from #tmp_kozonsegeskuldemenyek
	--		where sorszam between @mutato_start and @mutato_end
	--		order by sorszam
	END
--	ELSE
--	BEGIN
--		-- kozonseges
--		select #tmp_kozonsegeskuldemenyek.*, null as ids from #tmp_kozonsegeskuldemenyek
--	END

	if @IgnoreNyilvantartott != '1'
	BEGIN
		-- nyilvantartott
		select * from #tmp_nyilvantartottkuldemenyek
			cross apply (select STUFF((SELECT ';' + convert(varchar(36), Id) from #tmp_nyilvantartottkuldemenyek_with_id
				where #tmp_nyilvantartottkuldemenyek.sorszam=#tmp_nyilvantartottkuldemenyek_with_id.sorszam
				FOR XML PATH('')),1, 1, '') ids) tmp
			where #tmp_nyilvantartottkuldemenyek.sorszam between @mutato_start and @mutato_end
			order by #tmp_nyilvantartottkuldemenyek.sorszam

	--	select * from #tmp_nyilvantartottkuldemenyek
	--		where sorszam between @mutato_start and @mutato_end
	--		order by sorszam
	END
--	ELSE
--	BEGIN
--		-- nyilvantartott
--		select #tmp_nyilvantartottkuldemenyek.*, null as ids from #tmp_nyilvantartottkuldemenyek
--	END
end
else
begin

	declare @pagecounter int
	set @pagecounter = 1

	set @mutato_end = @mutato_start - 1 
		+ case
			when @cnt_all < @WindowSize
				then @cnt_all
			else @WindowSize
		end

--		select @WindowSize as WindowWidth
--		, @cnt_kozonseges as cnt_kozonseges
--		, @cnt_nyilvantartott as cnt_nyilvantartott
--		, @cnt_all as cnt_all
--		, @mutato_start as mutato_start
--		, @mutato_end as mutato_end
--		, @pagecountall as pagecountall
--		, @pagecounter as pagecounter
--		, @PageCount as PageCount

	-- lapozas info
	select @cnt_all as RecordNumber, @pageNumber as PageNumber;

		select cast(
		(
			case when (@cnt_kozonseges >= @mutato_start) then (select cast(
				(select (sorszam - @mutato_start + 1) as sorszam
		, alapszolg
		, viszonylat
		, orszagkod
		, suly
		, darab
		, kulonszolgok
		, vakok_irasa
		, meret
		, dij
		, gepre_alkalmassag
		 from #tmp_kozonsegeskuldemenyek where sorszam between @mutato_start and @mutato_end order by sorszam for xml PATH('kozonseges_tetel'))
			as xml) for xml PATH('kozonseges_tetelek'))
			else '' end
			+
			case when (@cnt_kozonseges < @mutato_end) then (select cast(
				(select (sorszam - @mutato_start + 1) as sorszam
		, azonosito
		, alapszolg
		, orszagkod
		, suly
		, kulonszolgok
		, vakok_irasa
		, uv_osszeg
		, uv_lapid
		, eny_osszeg
		, kezelesi_mod
		, meret
		, cimzett_nev
		, cimzett_irsz
		, cimzett_hely
		-- BUG_963
		, cimzett_kozelebbi_cim
		, cimzett_kozterulet_nev
		, cimzett_kozterulet_jelleg
		, cimzett_hazszam
		, cimzett_epulet
		, cimzett_lepcsohaz
		, cimzett_emelet
		, cimzett_ajto
		, cimzett_postafiok
		--
		, dij
		, potlapszam
		, sajat_azonosito
		, gepre_alkalmassag
		from #tmp_nyilvantartottkuldemenyek where sorszam between @mutato_start and @mutato_end order by sorszam for xml PATH('nyilvantartott_tetel'))
			as xml) for xml PATH('nyilvantartott_tetelek'))
			else '' end
		) as xml) for xml PATH('tomeges_adatok')

end

IF OBJECT_ID('tempdb..#tmp_kozonsegeskuldemenyek') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_kozonsegeskuldemenyek
END

IF OBJECT_ID('tempdb..#tmp_kozonsegeskuldemenyek_with_id') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_kozonsegeskuldemenyek_with_id
END

IF OBJECT_ID('tempdb..#tmp_nyilvantartottkuldemenyek') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_nyilvantartottkuldemenyek
END

IF OBJECT_ID('tempdb..#tmp_nyilvantartottkuldemenyek_with_id') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_nyilvantartottkuldemenyek_with_id
END

/*
'KULDEMENY_KULDES_MODJA'
01	Postai sima	Postai sima
02	Postai tértivevényes	Postai tértivevényes
03	Postai ajánlott	Postai ajánlott
04	Ügyfél	Ügyfél
05	Állami futárszolgálat	Állami futárszolgálat
07	Diplomáciai futárszolgálat	Diplomáciai futárszolgálat
08	Légiposta	Légiposta
09	Kézbesítő útján	Kézbesítő útján
10	Fax	Fax
11	E-mail	Elektronikus
13	Helyben	
14	Postai távirat	Postai távirat
15	Portálon keresztül	Elektronikus
17	Postai elsőbbségi	NULL
18	Futárszolgálat	NULL

'KIMENO_KULDEMENY_FAJTA'
01	Szabvány levél 30 g-ig	NULL
02	Levél 50 g-ig	NULL
03	Levél 100 g-ig	NULL
04	Levél 250 g-ig	NULL
05	Levél 500 g-ig	NULL
06	Levél 750 g-ig	NULL
07	Levél 2000 g-ig	NULL
18	Levelezőlap	NULL
19	Hivatalos irat	Nincs megkülönböztetve, hogy elsőbbségi vagy sem
20	Hivatalos irat saját kézbe	Nincs megkülönböztetve, hogy elsőbbségi vagy sem

*/


/*
select * from EREC_KuldKuldemenyek kk
left join KRT_KodTarak k1 on kk.KuldesMod collate Hungarian_CS_AS = k1.Kod and k1.KodCsoport_Id=(select Id from KRT_KodCsoportok where Kod='KULDEMENY_KULDES_MODJA')
where PostazasIranya='2' and Allapot='06'

select * from EREC_KuldKuldemenyek kk
left join KRT_KodTarak k1 on kk.KimenoKuldemenyFajta collate Hungarian_CS_AS = k1.Kod and k1.KodCsoport_Id=(select Id from KRT_KodCsoportok where Kod='KIMENO_KULDEMENY_FAJTA')
where PostazasIranya='2' and Allapot='06'
and Kod not in '19' -- Hivatalos irat

select Kod, Nev, Egyeb, Note from KRT_KodTarak where KodCsoport_Id=(select Id from KRT_KodCsoportok where Kod='KIMENO_KULDEMENY_FAJTA')
group by Org, Kod, Nev, Egyeb, Note


KIMENO_KULDEMENY_AR_ELSOBBSEGI
KIMENO_KULDEMENY_AR_NEM_ELSOBBSEGI
KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS
*/
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO
