using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using SqlClrUCMFullTextSearch;
using System.Collections;

public partial class UserDefinedFunctions
{
    [SqlFunction(DataAccess = DataAccessKind.None,
           TableDefinition = @"
                Title nvarchar(400), 
                Path nvarchar(4000)"
           , FillRowMethodName = "UCMFullTextSearch_FillRow")]
    public static IEnumerable UCMFullTextSearch(string eDocumentWebServiceUrl, string felhasznaloId, string szoveg)
    {
        if (!String.IsNullOrEmpty(eDocumentWebServiceUrl) && !eDocumentWebServiceUrl.EndsWith("/"))
        {
            eDocumentWebServiceUrl += "/";
        }

        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = felhasznaloId;

        UCMDocumentService ucmDocumentService = new UCMDocumentService();

        ucmDocumentService.Url = eDocumentWebServiceUrl + "UCMDocumentService.asmx";
        ucmDocumentService.PreAuthenticate = true;
        ucmDocumentService.Credentials = System.Net.CredentialCache.DefaultCredentials;

        Result result = ucmDocumentService.FullTextSearch(execParam, szoveg);

        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            // hiba:
            throw new Exception("ErrorCode: " + result.ErrorCode + "; ErrorMessage: " + result.ErrorMessage);
        }

        return result.Ds.Tables[0].Rows;
    }

    static void UCMFullTextSearch_FillRow(
            object o,
            out string Title,
            out string Path)
    {
        DataRow row = (DataRow)o;

        Title = row["Title"].ToString();
        Path = row["Path"].ToString();
    }
}
