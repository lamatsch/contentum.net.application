IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_AGA_AGAZATIJE_EREC_AGA]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_AgazatiJelek]'))
ALTER TABLE [dbo].[EREC_AgazatiJelek] DROP CONSTRAINT [FK_EREC_AGA_AGAZATIJE_EREC_AGA]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_AgazatiJelek]') AND name = N'Index_FK')
DROP INDEX [Index_FK] ON [dbo].[EREC_AgazatiJelek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_AgazatiJelek]') AND name = N'AgazatiJel_UK')
DROP INDEX [AgazatiJel_UK] ON [dbo].[EREC_AgazatiJelek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_AgazatiJelek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_AgazatiJelek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_AgazatiJelek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_AgazatiJelek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Agazati__Id__13F1F5EB]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Agazat__Org__14E61A24]  DEFAULT ('00000000-0000-0000-0000-000000000000'),
	[Kod] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[AgazatiJel_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_Agazat__Ver__15DA3E5D]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Agaz__ErvKe__16CE6296]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_AgazatiJelek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Agaz__Letre__18B6AB08]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_AGAZATIJELEK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_AgazatiJelek]') AND name = N'AgazatiJel_UK')
CREATE NONCLUSTERED INDEX [AgazatiJel_UK] ON [dbo].[EREC_AgazatiJelek]
(
	[Kod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_AgazatiJelek]') AND name = N'Index_FK')
CREATE NONCLUSTERED INDEX [Index_FK] ON [dbo].[EREC_AgazatiJelek]
(
	[AgazatiJel_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_AGA_AGAZATIJE_EREC_AGA]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_AgazatiJelek]'))
ALTER TABLE [dbo].[EREC_AgazatiJelek]  WITH CHECK ADD  CONSTRAINT [FK_EREC_AGA_AGAZATIJE_EREC_AGA] FOREIGN KEY([AgazatiJel_Id])
REFERENCES [dbo].[EREC_AgazatiJelek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_AGA_AGAZATIJE_EREC_AGA]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_AgazatiJelek]'))
ALTER TABLE [dbo].[EREC_AgazatiJelek] CHECK CONSTRAINT [FK_EREC_AGA_AGAZATIJE_EREC_AGA]
GO
