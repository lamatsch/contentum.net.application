/****** Object:  Table [dbo].[EREC_Alairas_Folyamat_Tetelek]    Script Date: 2017.10.09. 11:25:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EREC_Alairas_Folyamat_Tetelek](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid()),
	[AlairasFolyamat_Id] [uniqueidentifier] NOT NULL,
	[IraIrat_Id] [uniqueidentifier] NOT NULL,
	[Csatolmany_Id] [uniqueidentifier] NOT NULL,
	[AlairasStatus] [nvarchar](64) NULL,
	[Hiba] [nvarchar](400) NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_ALAIRAS_FOLYAMAT_TETEL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamat_Tetelek] ADD  DEFAULT ((1)) FOR [Ver]
GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamat_Tetelek] ADD  DEFAULT (getdate()) FOR [ErvKezd]
GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamat_Tetelek] ADD  DEFAULT (getdate()) FOR [LetrehozasIdo]
GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamat_Tetelek]  WITH CHECK ADD  CONSTRAINT [FK_EREC_ALA_REFERENCE_EREC_ALA] FOREIGN KEY([AlairasFolyamat_Id])
REFERENCES [dbo].[EREC_Alairas_Folyamatok] ([Id])
GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamat_Tetelek] CHECK CONSTRAINT [FK_EREC_ALA_REFERENCE_EREC_ALA]
GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamat_Tetelek]  WITH CHECK ADD  CONSTRAINT [FK_EREC_ALA_REFERENCE_EREC_CSA] FOREIGN KEY([Csatolmany_Id])
REFERENCES [dbo].[EREC_Csatolmanyok] ([Id])
GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamat_Tetelek] CHECK CONSTRAINT [FK_EREC_ALA_REFERENCE_EREC_CSA]
GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamat_Tetelek]  WITH CHECK ADD  CONSTRAINT [FK_EREC_ALA_REFERENCE_EREC_IRA] FOREIGN KEY([IraIrat_Id])
REFERENCES [dbo].[EREC_IraIratok] ([Id])
GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamat_Tetelek] CHECK CONSTRAINT [FK_EREC_ALA_REFERENCE_EREC_IRA]
GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamat_Tetelek]  WITH NOCHECK ADD  CONSTRAINT [CK_EREC_Alairas_Folyamat_Tetelek] CHECK  (((0)=[dbo].[fn_EREC_Alairas_Folyamat_TetelekCheckUK]([Id],[ErvKezd],[ErvVege])))
GO

ALTER TABLE [dbo].[EREC_Alairas_Folyamat_Tetelek] CHECK CONSTRAINT [CK_EREC_Alairas_Folyamat_Tetelek]
GO


