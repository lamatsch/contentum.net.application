IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KULDEMENY_CSATOLMANY_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Csatolmanyok]'))
ALTER TABLE [dbo].[EREC_Csatolmanyok] DROP CONSTRAINT [EREC_KULDEMENY_CSATOLMANY_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IRAT_CSATOLMANY_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Csatolmanyok]'))
ALTER TABLE [dbo].[EREC_Csatolmanyok] DROP CONSTRAINT [EREC_IRAT_CSATOLMANY_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Csatolmanyok]') AND name = N'IX_Csatolmanyok_KuldId')
DROP INDEX [IX_Csatolmanyok_KuldId] ON [dbo].[EREC_Csatolmanyok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Csatolmanyok]') AND name = N'IX_Csatolmanyok_IratId')
DROP INDEX [IX_Csatolmanyok_IratId] ON [dbo].[EREC_Csatolmanyok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Csatolmanyok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_Csatolmanyok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Csatolmanyok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_Csatolmanyok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Csatolm__Id__1B9317B3]  DEFAULT (newsequentialid()),
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[IraIrat_Id] [uniqueidentifier] NULL,
	[Dokumentum_Id] [uniqueidentifier] NOT NULL,
	[Leiras] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Lapszam] [int] NULL,
	[KapcsolatJelleg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[DokumentumSzerep] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[IratAlairoJel] [char](1) COLLATE Hungarian_CI_AS NULL,
	[CustomId] NVARCHAR(4000) NULL,  
	[Ver] [int] NULL CONSTRAINT [DF__EREC_Csatol__Ver__1C873BEC]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Csat__ErvKe__1D7B6025]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_Csatolmanyok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Csat__Letre__1F63A897]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_CSATOLMANYOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Csatolmanyok]') AND name = N'IX_Csatolmanyok_IratId')
CREATE NONCLUSTERED INDEX [IX_Csatolmanyok_IratId] ON [dbo].[EREC_Csatolmanyok]
(
	[IraIrat_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Csatolmanyok]') AND name = N'IX_Csatolmanyok_KuldId')
CREATE NONCLUSTERED INDEX [IX_Csatolmanyok_KuldId] ON [dbo].[EREC_Csatolmanyok]
(
	[KuldKuldemeny_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IRAT_CSATOLMANY_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Csatolmanyok]'))
ALTER TABLE [dbo].[EREC_Csatolmanyok]  WITH CHECK ADD  CONSTRAINT [EREC_IRAT_CSATOLMANY_FK] FOREIGN KEY([IraIrat_Id])
REFERENCES [dbo].[EREC_IraIratok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IRAT_CSATOLMANY_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Csatolmanyok]'))
ALTER TABLE [dbo].[EREC_Csatolmanyok] CHECK CONSTRAINT [EREC_IRAT_CSATOLMANY_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KULDEMENY_CSATOLMANY_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Csatolmanyok]'))
ALTER TABLE [dbo].[EREC_Csatolmanyok]  WITH CHECK ADD  CONSTRAINT [EREC_KULDEMENY_CSATOLMANY_FK] FOREIGN KEY([KuldKuldemeny_Id])
REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KULDEMENY_CSATOLMANY_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Csatolmanyok]'))
ALTER TABLE [dbo].[EREC_Csatolmanyok] CHECK CONSTRAINT [EREC_KULDEMENY_CSATOLMANY_FK]
GO
