IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_FeladatDefinicio]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_FeladatDefinicio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_FeladatDefinicio]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_FeladatDefinicio](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Feladat__Id__39237A9A]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Felada__Org__3A179ED3]  DEFAULT ('00000000-0000-0000-0000-000000000000'),
	[Funkcio_Id_Kivalto] [uniqueidentifier] NULL,
	[Obj_Metadefinicio_Id] [uniqueidentifier] NULL,
	[ObjStateValue_Id] [uniqueidentifier] NULL,
	[FeladatDefinicioTipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[FeladatSorszam] [int] NULL,
	[MuveletDefinicio] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[SpecMuveletDefinicio] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Funkcio_Id_Inditando] [uniqueidentifier] NULL,
	[Funkcio_Id_Futtatando] [uniqueidentifier] NULL,
	[BazisObjLeiro] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[FelelosTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Obj_Id_Felelos] [uniqueidentifier] NULL,
	[FelelosLeiro] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Idobazis] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[ObjTip_Id_DateCol] [uniqueidentifier] NULL,
	[AtfutasiIdo] [int] NULL,
	[Idoegyseg] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[FeladatLeiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Prioritas] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[LezarasPrioritas] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Obj_SzukitoFeltetel] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_Felada__Ver__3B0BC30C]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Fela__ErvKe__3BFFE745]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_FeladatDefinicio_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Fela__Letre__3DE82FB7]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_FELADATDEFINICIO] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
