IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Hata__Letre__5F94079C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Hatarid_Objektumok] DROP CONSTRAINT [DF__EREC_Hata__Letre__5F94079C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_Hatarid_Objektumok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Hatarid_Objektumok] DROP CONSTRAINT [DF_EREC_Hatarid_Objektumok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Hata__ErvKe__5DABBF2A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Hatarid_Objektumok] DROP CONSTRAINT [DF__EREC_Hata__ErvKe__5DABBF2A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Hatari__Ver__5CB79AF1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Hatarid_Objektumok] DROP CONSTRAINT [DF__EREC_Hatari__Ver__5CB79AF1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Hatarid__Id__5BC376B8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Hatarid_Objektumok] DROP CONSTRAINT [DF__EREC_Hatarid__Id__5BC376B8]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Hatarid_Objektumok]') AND name = N'FK_HataridosFeladat_Id')
DROP INDEX [FK_HataridosFeladat_Id] ON [dbo].[EREC_Hatarid_Objektumok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Hatarid_Objektumok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_Hatarid_Objektumok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Hatarid_Objektumok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_Hatarid_Objektumok](
	[Id] [uniqueidentifier] NOT NULL,
	[HataridosFeladat_Id] [uniqueidentifier] NOT NULL,
	[Obj_Id] [uniqueidentifier] NOT NULL,
	[ObjTip_Id] [uniqueidentifier] NOT NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_HATARID_OBJEKTUMOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Hatarid_Objektumok]') AND name = N'FK_HataridosFeladat_Id')
CREATE NONCLUSTERED INDEX [FK_HataridosFeladat_Id] ON [dbo].[EREC_Hatarid_Objektumok]
(
	[HataridosFeladat_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Hatarid__Id__5BC376B8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Hatarid_Objektumok] ADD  CONSTRAINT [DF__EREC_Hatarid__Id__5BC376B8]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Hatari__Ver__5CB79AF1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Hatarid_Objektumok] ADD  CONSTRAINT [DF__EREC_Hatari__Ver__5CB79AF1]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Hata__ErvKe__5DABBF2A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Hatarid_Objektumok] ADD  CONSTRAINT [DF__EREC_Hata__ErvKe__5DABBF2A]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_Hatarid_Objektumok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Hatarid_Objektumok] ADD  CONSTRAINT [DF_EREC_Hatarid_Objektumok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Hata__Letre__5F94079C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Hatarid_Objektumok] ADD  CONSTRAINT [DF__EREC_Hata__Letre__5F94079C]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
