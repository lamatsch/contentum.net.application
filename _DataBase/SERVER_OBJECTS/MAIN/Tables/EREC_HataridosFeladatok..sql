IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Erec_Hataridos_Erec_Hataridos_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatok]'))
ALTER TABLE [dbo].[EREC_HataridosFeladatok] DROP CONSTRAINT [Erec_Hataridos_Erec_Hataridos_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatok]') AND name = N'IX_Obj_Id')
DROP INDEX [IX_Obj_Id] ON [dbo].[EREC_HataridosFeladatok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatok]') AND name = N'FK_HataridosFeladat_Id')
DROP INDEX [FK_HataridosFeladat_Id] ON [dbo].[EREC_HataridosFeladatok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatok]') AND name = N'EREC_HataridosFeladat_UK')
DROP INDEX [EREC_HataridosFeladat_UK] ON [dbo].[EREC_HataridosFeladatok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_HataridosFeladatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_HataridosFeladatok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Hatarid__Id__42ACE4D4]  DEFAULT (newsequentialid()),
	[Esemeny_Id_Kivalto] [uniqueidentifier] NULL,
	[Esemeny_Id_Lezaro] [uniqueidentifier] NULL,
	[Funkcio_Id_Inditando] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[Felhasznalo_Id_Felelos] [uniqueidentifier] NULL,
	[Csoport_Id_Kiado] [uniqueidentifier] NULL,
	[Felhasznalo_Id_Kiado] [uniqueidentifier] NULL,
	[HataridosFeladat_Id] [uniqueidentifier] NULL,
	[ReszFeladatSorszam] [int] NULL,
	[FeladatDefinicio_Id] [uniqueidentifier] NULL,
	[FeladatDefinicio_Id_Lezaro] [uniqueidentifier] NULL,
	[Forras] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Altipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Memo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Prioritas] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[LezarasPrioritas] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[KezdesiIdo] [datetime] NULL,
	[IntezkHatarido] [datetime] NULL,
	[LezarasDatuma] [datetime] NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Megoldas] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_Hatari__Ver__43A1090D]  DEFAULT ((1)),
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Hata__ErvKe__44952D46]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_HataridosFeladatok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Hata__Letre__467D75B8]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_HATARIDOSFELADATOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatok]') AND name = N'EREC_HataridosFeladat_UK')
CREATE NONCLUSTERED INDEX [EREC_HataridosFeladat_UK] ON [dbo].[EREC_HataridosFeladatok]
(
	[Csoport_Id_Felelos] ASC,
	[Felhasznalo_Id_Kiado] ASC,
	[HataridosFeladat_Id] ASC,
	[IntezkHatarido] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatok]') AND name = N'FK_HataridosFeladat_Id')
CREATE NONCLUSTERED INDEX [FK_HataridosFeladat_Id] ON [dbo].[EREC_HataridosFeladatok]
(
	[HataridosFeladat_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatok]') AND name = N'IX_Obj_Id')
CREATE NONCLUSTERED INDEX [IX_Obj_Id] ON [dbo].[EREC_HataridosFeladatok]
(
	[Obj_Id] ASC
)
INCLUDE ( 	[ErvKezd],
	[ErvVege]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Erec_Hataridos_Erec_Hataridos_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatok]'))
ALTER TABLE [dbo].[EREC_HataridosFeladatok]  WITH CHECK ADD  CONSTRAINT [Erec_Hataridos_Erec_Hataridos_FK] FOREIGN KEY([HataridosFeladat_Id])
REFERENCES [dbo].[EREC_HataridosFeladatok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Erec_Hataridos_Erec_Hataridos_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatok]'))
ALTER TABLE [dbo].[EREC_HataridosFeladatok] CHECK CONSTRAINT [Erec_Hataridos_Erec_Hataridos_FK]
GO
