IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_IRA_IRA_EIVTE_KRT_PART]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelek]'))
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek] DROP CONSTRAINT [FK_EREC_IRA_IRA_EIVTE_KRT_PART]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EIVTetel_EIV_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelek]'))
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek] DROP CONSTRAINT [EIVTetel_EIV_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraE__Letre__54CB950F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek] DROP CONSTRAINT [DF__EREC_IraE__Letre__54CB950F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraElosztoivTetelek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek] DROP CONSTRAINT [DF_EREC_IraElosztoivTetelek_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraE__ErvKe__52E34C9D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek] DROP CONSTRAINT [DF__EREC_IraE__ErvKe__52E34C9D]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraElo__Ver__51EF2864]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek] DROP CONSTRAINT [DF__EREC_IraElo__Ver__51EF2864]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraElos__Id__50FB042B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek] DROP CONSTRAINT [DF__EREC_IraElos__Id__50FB042B]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelek]') AND name = N'FK_Partner_Id')
DROP INDEX [FK_Partner_Id] ON [dbo].[EREC_IraElosztoivTetelek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelek]') AND name = N'FK_ElosztoIv_Id')
DROP INDEX [FK_ElosztoIv_Id] ON [dbo].[EREC_IraElosztoivTetelek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraElosztoivTetelek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraElosztoivTetelek](
	[Id] [uniqueidentifier] NOT NULL,
	[ElosztoIv_Id] [uniqueidentifier] NOT NULL,
	[Sorszam] [int] NOT NULL,
	[Partner_Id] [uniqueidentifier] NOT NULL,
	[Cim_Id] [uniqueidentifier] NULL,
	[CimSTR] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[NevSTR] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Kuldesmod] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Visszavarolag] [char](1) COLLATE Hungarian_CI_AS NULL,
	[VisszavarasiIdo] [datetime] NULL,
	[Vissza_Nap_Mulva] [int] NULL,
	[AlairoSzerep] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [ELIT_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelek]') AND name = N'FK_ElosztoIv_Id')
CREATE NONCLUSTERED INDEX [FK_ElosztoIv_Id] ON [dbo].[EREC_IraElosztoivTetelek]
(
	[ElosztoIv_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelek]') AND name = N'FK_Partner_Id')
CREATE NONCLUSTERED INDEX [FK_Partner_Id] ON [dbo].[EREC_IraElosztoivTetelek]
(
	[Partner_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraElos__Id__50FB042B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek] ADD  CONSTRAINT [DF__EREC_IraElos__Id__50FB042B]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraElo__Ver__51EF2864]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek] ADD  CONSTRAINT [DF__EREC_IraElo__Ver__51EF2864]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraE__ErvKe__52E34C9D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek] ADD  CONSTRAINT [DF__EREC_IraE__ErvKe__52E34C9D]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraElosztoivTetelek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek] ADD  CONSTRAINT [DF_EREC_IraElosztoivTetelek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraE__Letre__54CB950F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek] ADD  CONSTRAINT [DF__EREC_IraE__Letre__54CB950F]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EIVTetel_EIV_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelek]'))
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek]  WITH CHECK ADD  CONSTRAINT [EIVTetel_EIV_FK] FOREIGN KEY([ElosztoIv_Id])
REFERENCES [dbo].[EREC_IraElosztoivek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EIVTetel_EIV_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelek]'))
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek] CHECK CONSTRAINT [EIVTetel_EIV_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_IRA_IRA_EIVTE_KRT_PART]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelek]'))
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek]  WITH CHECK ADD  CONSTRAINT [FK_EREC_IRA_IRA_EIVTE_KRT_PART] FOREIGN KEY([Partner_Id])
REFERENCES [dbo].[KRT_Partnerek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_IRA_IRA_EIVTE_KRT_PART]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivTetelek]'))
ALTER TABLE [dbo].[EREC_IraElosztoivTetelek] CHECK CONSTRAINT [FK_EREC_IRA_IRA_EIVTE_KRT_PART]
GO
