IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraE__Letre__4E1E9780]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivek] DROP CONSTRAINT [DF__EREC_IraE__Letre__4E1E9780]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraElosztoivek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivek] DROP CONSTRAINT [DF_EREC_IraElosztoivek_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraE__ErvKe__4C364F0E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivek] DROP CONSTRAINT [DF__EREC_IraE__ErvKe__4C364F0E]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraElo__Ver__4B422AD5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivek] DROP CONSTRAINT [DF__EREC_IraElo__Ver__4B422AD5]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraElo__Org__4A4E069C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivek] DROP CONSTRAINT [DF__EREC_IraElo__Org__4A4E069C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraElos__Id__4959E263]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivek] DROP CONSTRAINT [DF__EREC_IraElos__Id__4959E263]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivek]') AND name = N'FK_Csoport_Id_Tulaj')
DROP INDEX [FK_Csoport_Id_Tulaj] ON [dbo].[EREC_IraElosztoivek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraElosztoivek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraElosztoivek](
	[Id] [uniqueidentifier] NOT NULL,
	[Org] [uniqueidentifier] NOT NULL,
	[Ev] [int] NULL,
	[Fajta] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Kod] [nvarchar](10) COLLATE Hungarian_CI_AS NOT NULL,
	[NEV] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Hasznalat] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NOT NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [ELIV_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraElosztoivek]') AND name = N'FK_Csoport_Id_Tulaj')
CREATE NONCLUSTERED INDEX [FK_Csoport_Id_Tulaj] ON [dbo].[EREC_IraElosztoivek]
(
	[Csoport_Id_Tulaj] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraElos__Id__4959E263]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivek] ADD  CONSTRAINT [DF__EREC_IraElos__Id__4959E263]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraElo__Org__4A4E069C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivek] ADD  CONSTRAINT [DF__EREC_IraElo__Org__4A4E069C]  DEFAULT ('00000000-0000-0000-0000-000000000000') FOR [Org]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraElo__Ver__4B422AD5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivek] ADD  CONSTRAINT [DF__EREC_IraElo__Ver__4B422AD5]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraE__ErvKe__4C364F0E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivek] ADD  CONSTRAINT [DF__EREC_IraE__ErvKe__4C364F0E]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraElosztoivek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivek] ADD  CONSTRAINT [DF_EREC_IraElosztoivek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraE__Letre__4E1E9780]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraElosztoivek] ADD  CONSTRAINT [DF__EREC_IraE__Letre__4E1E9780]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
