IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIktatoKonyvek]') AND name = N'Index_UK')
DROP INDEX [Index_UK] ON [dbo].[EREC_IraIktatoKonyvek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIktatoKonyvek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraIktatoKonyvek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIktatoKonyvek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraIktatoKonyvek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_IraIkta__Id__57A801BA]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NOT NULL,
	[Ev] [int] NOT NULL,
	[Azonosito] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[DefaultIrattariTetelszam] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[MegkulJelzes] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Iktatohely] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KozpontiIktatasJelzo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[UtolsoFoszam] [int] NOT NULL,
	[UtolsoSorszam] [int] NULL,
	[Csoport_Id_Olvaso] [uniqueidentifier] NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NULL,
	[IktSzamOsztas] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[FormatumKod] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Titkos] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__EREC_IraI__Titko__589C25F3]  DEFAULT ('N'),
	[IktatoErkezteto] [char](1) COLLATE Hungarian_CI_AS NULL,
	[LezarasDatuma] [datetime] NULL,
	[PostakonyvVevokod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[PostakonyvMegallapodasAzon] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[EFeladoJegyzekUgyfelAdatok] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_IraIkt__Ver__59904A2C]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_IraI__ErvKe__5A846E65]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_IraIktatoKonyvek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_IraI__Letre__5C6CB6D7]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[Statusz] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__EREC_IraI__Statu__5D60DB10]  DEFAULT ('1'),
	[Terjedelem] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[SelejtezesDatuma] [datetime] NULL,
	[KezelesTipusa] [char](1) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [IKTK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIktatoKonyvek]') AND name = N'Index_UK')
CREATE NONCLUSTERED INDEX [Index_UK] ON [dbo].[EREC_IraIktatoKonyvek]
(
	[Org] ASC,
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
