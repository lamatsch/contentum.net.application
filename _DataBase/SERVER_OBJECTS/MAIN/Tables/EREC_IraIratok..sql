IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Irat_Kuldemeny_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]'))
ALTER TABLE [dbo].[EREC_IraIratok] DROP CONSTRAINT [Irat_Kuldemeny_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IraIrat_UgyiratDarab_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]'))
ALTER TABLE [dbo].[EREC_IraIratok] DROP CONSTRAINT [IraIrat_UgyiratDarab_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IRAT_UGY_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]'))
ALTER TABLE [dbo].[EREC_IraIratok] DROP CONSTRAINT [EREC_IRAT_UGY_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'LETREHOZO_IND')
DROP INDEX [LETREHOZO_IND] ON [dbo].[EREC_IraIratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'IX_ModositasIdo_Irat')
DROP INDEX [IX_ModositasIdo_Irat] ON [dbo].[EREC_IraIratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'IX_Irat_Ugyirat_Id')
DROP INDEX [IX_Irat_Ugyirat_Id] ON [dbo].[EREC_IraIratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'IX_Irat_Minosites')
DROP INDEX [IX_Irat_Minosites] ON [dbo].[EREC_IraIratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'IRA_PRT_FK_I')
DROP INDEX [IRA_PRT_FK_I] ON [dbo].[EREC_IraIratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'IRA_IRAU_UK')
DROP INDEX [IRA_IRAU_UK] ON [dbo].[EREC_IraIratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'IND_UDB_ID')
DROP INDEX [IND_UDB_ID] ON [dbo].[EREC_IraIratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'I_IKTATAS_DATUM')
DROP INDEX [I_IKTATAS_DATUM] ON [dbo].[EREC_IraIratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'FK_KuldKuldemenyek_Id')
DROP INDEX [FK_KuldKuldemenyek_Id] ON [dbo].[EREC_IraIratok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraIratok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraIratok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_IraIrat__Id__603D47BB]  DEFAULT (newsequentialid()),
	[PostazasIranya] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Alszam] [int] NULL,
	[Sorszam] [int] NULL,
	[UtolsoSorszam] [int] NULL,
	[UgyUgyIratDarab_Id] [uniqueidentifier] NULL,
	[Kategoria] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[HivatkozasiSzam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[IktatasDatuma] [datetime] NULL,
	[ExpedialasDatuma] [datetime] NULL,
	[ExpedialasModja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FelhasznaloCsoport_Id_Expedial] [uniqueidentifier] NULL,
	[Targy] [nvarchar](4000) COLLATE Hungarian_CI_AS NOT NULL,
	[Jelleg] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[FelhasznaloCsoport_Id_Iktato] [uniqueidentifier] NOT NULL,
	[FelhasznaloCsoport_Id_Kiadmany] [uniqueidentifier] NULL,
	[UgyintezesAlapja] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[AdathordozoTipusa] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[Csoport_Id_Ugyfelelos] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Orzo] [uniqueidentifier] NULL,
	[KiadmanyozniKell] [char](1) COLLATE Hungarian_CI_AS NULL,
	[FelhasznaloCsoport_Id_Ugyintez] [uniqueidentifier] NULL,
	[Hatarido] [datetime] NULL,
	[MegorzesiIdo] [datetime] NULL,
	[IratFajta] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Irattipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[KuldKuldemenyek_Id] [uniqueidentifier] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[GeneraltTargy] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[IratMetaDef_Id] [uniqueidentifier] NULL,
	[IrattarbaKuldDatuma] [datetime] NULL,
	[IrattarbaVetelDat] [datetime] NULL,
	[Ugyirat_Id] [uniqueidentifier] NULL,
	[Minosites] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_IraIra__Ver__61316BF4]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_IraI__ErvKe__6225902D]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_IraIratok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_IraI__Letre__640DD89F]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[Munkaallomas] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UgyintezesModja] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[IntezesIdopontja] [datetime] NULL,
	[Elintezett] [char](1) COLLATE Hungarian_CI_AS NULL,
	[AKTIV]  AS (case when [ALLAPOT]='90' then (0) else (1) end) PERSISTED NOT NULL,
 CONSTRAINT [IRA_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'FK_KuldKuldemenyek_Id')
CREATE NONCLUSTERED INDEX [FK_KuldKuldemenyek_Id] ON [dbo].[EREC_IraIratok]
(
	[KuldKuldemenyek_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'I_IKTATAS_DATUM')
CREATE NONCLUSTERED INDEX [I_IKTATAS_DATUM] ON [dbo].[EREC_IraIratok]
(
	[IktatasDatuma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'IND_UDB_ID')
CREATE NONCLUSTERED INDEX [IND_UDB_ID] ON [dbo].[EREC_IraIratok]
(
	[UgyUgyIratDarab_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'IRA_IRAU_UK')
CREATE NONCLUSTERED INDEX [IRA_IRAU_UK] ON [dbo].[EREC_IraIratok]
(
	[Alszam] ASC,
	[UgyUgyIratDarab_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'IRA_PRT_FK_I')
CREATE NONCLUSTERED INDEX [IRA_PRT_FK_I] ON [dbo].[EREC_IraIratok]
(
	[FelhasznaloCsoport_Id_Iktato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'IX_Irat_Minosites')
CREATE NONCLUSTERED INDEX [IX_Irat_Minosites] ON [dbo].[EREC_IraIratok]
(
	[Minosites] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'IX_Irat_Ugyirat_Id')
CREATE NONCLUSTERED INDEX [IX_Irat_Ugyirat_Id] ON [dbo].[EREC_IraIratok]
(
	[Ugyirat_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'IX_ModositasIdo_Irat')
CREATE NONCLUSTERED INDEX [IX_ModositasIdo_Irat] ON [dbo].[EREC_IraIratok]
(
	[ModositasIdo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]') AND name = N'LETREHOZO_IND')
CREATE NONCLUSTERED INDEX [LETREHOZO_IND] ON [dbo].[EREC_IraIratok]
(
	[Letrehozo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IRAT_UGY_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]'))
ALTER TABLE [dbo].[EREC_IraIratok]  WITH CHECK ADD  CONSTRAINT [EREC_IRAT_UGY_FK] FOREIGN KEY([Ugyirat_Id])
REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IRAT_UGY_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]'))
ALTER TABLE [dbo].[EREC_IraIratok] CHECK CONSTRAINT [EREC_IRAT_UGY_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IraIrat_UgyiratDarab_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]'))
ALTER TABLE [dbo].[EREC_IraIratok]  WITH CHECK ADD  CONSTRAINT [IraIrat_UgyiratDarab_FK] FOREIGN KEY([UgyUgyIratDarab_Id])
REFERENCES [dbo].[EREC_UgyUgyiratdarabok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IraIrat_UgyiratDarab_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]'))
ALTER TABLE [dbo].[EREC_IraIratok] CHECK CONSTRAINT [IraIrat_UgyiratDarab_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Irat_Kuldemeny_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]'))
ALTER TABLE [dbo].[EREC_IraIratok]  WITH CHECK ADD  CONSTRAINT [Irat_Kuldemeny_FK] FOREIGN KEY([KuldKuldemenyek_Id])
REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Irat_Kuldemeny_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraIratok]'))
ALTER TABLE [dbo].[EREC_IraIratok] CHECK CONSTRAINT [Irat_Kuldemeny_FK]
GO
