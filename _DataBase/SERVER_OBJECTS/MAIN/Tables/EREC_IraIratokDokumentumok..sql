IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraI__Letre__63649880]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraIratokDokumentumok] DROP CONSTRAINT [DF__EREC_IraI__Letre__63649880]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraIratokDokumentumok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraIratokDokumentumok] DROP CONSTRAINT [DF_EREC_IraIratokDokumentumok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraI__ErvKe__617C500E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraIratokDokumentumok] DROP CONSTRAINT [DF__EREC_IraI__ErvKe__617C500E]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraIra__Ver__60882BD5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraIratokDokumentumok] DROP CONSTRAINT [DF__EREC_IraIra__Ver__60882BD5]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokDokumentumok]') AND name = N'I_Barkod')
DROP INDEX [I_Barkod] ON [dbo].[EREC_IraIratokDokumentumok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokDokumentumok]') AND name = N'FK_IraIrat_Id')
DROP INDEX [FK_IraIrat_Id] ON [dbo].[EREC_IraIratokDokumentumok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokDokumentumok]') AND name = N'FK_Dokumentum_Id')
DROP INDEX [FK_Dokumentum_Id] ON [dbo].[EREC_IraIratokDokumentumok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokDokumentumok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraIratokDokumentumok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokDokumentumok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraIratokDokumentumok](
	[Id] [uniqueidentifier] NOT NULL,
	[Dokumentum_Id] [uniqueidentifier] NOT NULL,
	[IraIrat_Id] [uniqueidentifier] NOT NULL,
	[Leiras] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Lapszam] [int] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Forras] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Formatum] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Vonalkodozas] [char](1) COLLATE Hungarian_CI_AS NULL,
	[DokumentumSzerep] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [IratDoku_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokDokumentumok]') AND name = N'FK_Dokumentum_Id')
CREATE NONCLUSTERED INDEX [FK_Dokumentum_Id] ON [dbo].[EREC_IraIratokDokumentumok]
(
	[Dokumentum_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokDokumentumok]') AND name = N'FK_IraIrat_Id')
CREATE NONCLUSTERED INDEX [FK_IraIrat_Id] ON [dbo].[EREC_IraIratokDokumentumok]
(
	[IraIrat_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIratokDokumentumok]') AND name = N'I_Barkod')
CREATE NONCLUSTERED INDEX [I_Barkod] ON [dbo].[EREC_IraIratokDokumentumok]
(
	[BarCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraIra__Ver__60882BD5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraIratokDokumentumok] ADD  CONSTRAINT [DF__EREC_IraIra__Ver__60882BD5]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraI__ErvKe__617C500E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraIratokDokumentumok] ADD  CONSTRAINT [DF__EREC_IraI__ErvKe__617C500E]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraIratokDokumentumok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraIratokDokumentumok] ADD  CONSTRAINT [DF_EREC_IraIratokDokumentumok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraI__Letre__63649880]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraIratokDokumentumok] ADD  CONSTRAINT [DF__EREC_IraI__Letre__63649880]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
