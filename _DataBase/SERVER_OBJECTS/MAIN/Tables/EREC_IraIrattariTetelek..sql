IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IrattariTetel_AgazatiJel_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraIrattariTetelek]'))
ALTER TABLE [dbo].[EREC_IraIrattariTetelek] DROP CONSTRAINT [IrattariTetel_AgazatiJel_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIrattariTetelek]') AND name = N'KIRL_IRA_IRATTARI_TETELEK_I_N')
DROP INDEX [KIRL_IRA_IRATTARI_TETELEK_I_N] ON [dbo].[EREC_IraIrattariTetelek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIrattariTetelek]') AND name = N'IraIrattariTetelek_UK')
DROP INDEX [IraIrattariTetelek_UK] ON [dbo].[EREC_IraIrattariTetelek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIrattariTetelek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraIrattariTetelek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIrattariTetelek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraIrattariTetelek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_IraIrat__Id__68D28DBC]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NULL,
	[IrattariTetelszam] [nvarchar](20) COLLATE Hungarian_CI_AS NOT NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NOT NULL,
	[IrattariJel] [nvarchar](2) COLLATE Hungarian_CS_AS NOT NULL,
	[MegorzesiIdo] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Idoegyseg] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Folyo_CM] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[AgazatiJel_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_IraIra__Ver__69C6B1F5]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_IraI__ErvKe__6ABAD62E]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_IraIrattariTetelek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_IraI__Letre__6CA31EA0]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[EvenTuliIktatas] [char](1) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [ITSZ_PK] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIrattariTetelek]') AND name = N'IraIrattariTetelek_UK')
CREATE NONCLUSTERED INDEX [IraIrattariTetelek_UK] ON [dbo].[EREC_IraIrattariTetelek]
(
	[Org] ASC,
	[IrattariTetelszam] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraIrattariTetelek]') AND name = N'KIRL_IRA_IRATTARI_TETELEK_I_N')
CREATE NONCLUSTERED INDEX [KIRL_IRA_IRATTARI_TETELEK_I_N] ON [dbo].[EREC_IraIrattariTetelek]
(
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IrattariTetel_AgazatiJel_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraIrattariTetelek]'))
ALTER TABLE [dbo].[EREC_IraIrattariTetelek]  WITH CHECK ADD  CONSTRAINT [IrattariTetel_AgazatiJel_FK] FOREIGN KEY([AgazatiJel_Id])
REFERENCES [dbo].[EREC_AgazatiJelek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IrattariTetel_AgazatiJel_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraIrattariTetelek]'))
ALTER TABLE [dbo].[EREC_IraIrattariTetelek] CHECK CONSTRAINT [IrattariTetel_AgazatiJel_FK]
GO
