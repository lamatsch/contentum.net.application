IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KIRL_IRA_JEGYZ_TETELEK_JGY_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekTetelek]'))
ALTER TABLE [dbo].[EREC_IraJegyzekTetelek] DROP CONSTRAINT [KIRL_IRA_JEGYZ_TETELEK_JGY_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJ__Letre__7CD98669]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekTetelek] DROP CONSTRAINT [DF__EREC_IraJ__Letre__7CD98669]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraJegyzekTetelek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekTetelek] DROP CONSTRAINT [DF_EREC_IraJegyzekTetelek_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJ__ErvKe__7AF13DF7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekTetelek] DROP CONSTRAINT [DF__EREC_IraJ__ErvKe__7AF13DF7]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJeg__Ver__79FD19BE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekTetelek] DROP CONSTRAINT [DF__EREC_IraJeg__Ver__79FD19BE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJegy__Id__7908F585]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekTetelek] DROP CONSTRAINT [DF__EREC_IraJegy__Id__7908F585]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekTetelek]') AND name = N'FK_Obj_Id')
DROP INDEX [FK_Obj_Id] ON [dbo].[EREC_IraJegyzekTetelek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekTetelek]') AND name = N'FK_Jegyzek_Id')
DROP INDEX [FK_Jegyzek_Id] ON [dbo].[EREC_IraJegyzekTetelek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekTetelek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraJegyzekTetelek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekTetelek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraJegyzekTetelek](
	[Id] [uniqueidentifier] NOT NULL,
	[Jegyzek_Id] [uniqueidentifier] NULL,
	[Sorszam] [int] NULL,
	[Obj_Id] [uniqueidentifier] NOT NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SztornozasDat] [datetime] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [eRec_IraJegyzekTetelek_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekTetelek]') AND name = N'FK_Jegyzek_Id')
CREATE NONCLUSTERED INDEX [FK_Jegyzek_Id] ON [dbo].[EREC_IraJegyzekTetelek]
(
	[Jegyzek_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekTetelek]') AND name = N'FK_Obj_Id')
CREATE NONCLUSTERED INDEX [FK_Obj_Id] ON [dbo].[EREC_IraJegyzekTetelek]
(
	[Obj_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJegy__Id__7908F585]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekTetelek] ADD  CONSTRAINT [DF__EREC_IraJegy__Id__7908F585]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJeg__Ver__79FD19BE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekTetelek] ADD  CONSTRAINT [DF__EREC_IraJeg__Ver__79FD19BE]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJ__ErvKe__7AF13DF7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekTetelek] ADD  CONSTRAINT [DF__EREC_IraJ__ErvKe__7AF13DF7]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraJegyzekTetelek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekTetelek] ADD  CONSTRAINT [DF_EREC_IraJegyzekTetelek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJ__Letre__7CD98669]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekTetelek] ADD  CONSTRAINT [DF__EREC_IraJ__Letre__7CD98669]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KIRL_IRA_JEGYZ_TETELEK_JGY_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekTetelek]'))
ALTER TABLE [dbo].[EREC_IraJegyzekTetelek]  WITH CHECK ADD  CONSTRAINT [KIRL_IRA_JEGYZ_TETELEK_JGY_FK] FOREIGN KEY([Jegyzek_Id])
REFERENCES [dbo].[EREC_IraJegyzekek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KIRL_IRA_JEGYZ_TETELEK_JGY_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekTetelek]'))
ALTER TABLE [dbo].[EREC_IraJegyzekTetelek] CHECK CONSTRAINT [KIRL_IRA_JEGYZ_TETELEK_JGY_FK]
GO
