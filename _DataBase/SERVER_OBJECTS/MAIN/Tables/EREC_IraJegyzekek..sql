IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJ__Letre__762C88DA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] DROP CONSTRAINT [DF__EREC_IraJ__Letre__762C88DA]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraJegyzekek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] DROP CONSTRAINT [DF_EREC_IraJegyzekek_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJ__ErvKe__74444068]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] DROP CONSTRAINT [DF__EREC_IraJ__ErvKe__74444068]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJeg__Ver__73501C2F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] DROP CONSTRAINT [DF__EREC_IraJeg__Ver__73501C2F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJ__Vegre__725BF7F6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] DROP CONSTRAINT [DF__EREC_IraJ__Vegre__725BF7F6]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJ__Lezar__7167D3BD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] DROP CONSTRAINT [DF__EREC_IraJ__Lezar__7167D3BD]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJeg__Org__7073AF84]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] DROP CONSTRAINT [DF__EREC_IraJeg__Org__7073AF84]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJegy__Id__6F7F8B4B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] DROP CONSTRAINT [DF__EREC_IraJegy__Id__6F7F8B4B]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekek]') AND name = N'EREC_IraJegyzek_UK')
DROP INDEX [EREC_IraJegyzek_UK] ON [dbo].[EREC_IraJegyzekek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraJegyzekek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraJegyzekek](
	[Id] [uniqueidentifier] NOT NULL,
	[Org] [uniqueidentifier] NOT NULL,
	[Tipus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Minosites] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id_felelos] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Vegrehaj] [uniqueidentifier] NULL,
	[Partner_Id_LeveltariAtvevo] [uniqueidentifier] NULL,
	[LezarasDatuma] [datetime] NULL,
	[SztornirozasDat] [datetime] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[VegrehajtasDatuma] [datetime] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KIRL_IRA_JEGYZEKEK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekek]') AND name = N'EREC_IraJegyzek_UK')
CREATE NONCLUSTERED INDEX [EREC_IraJegyzek_UK] ON [dbo].[EREC_IraJegyzekek]
(
	[Tipus] ASC,
	[Minosites] ASC,
	[Csoport_Id_felelos] ASC,
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJegy__Id__6F7F8B4B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] ADD  CONSTRAINT [DF__EREC_IraJegy__Id__6F7F8B4B]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJeg__Org__7073AF84]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] ADD  CONSTRAINT [DF__EREC_IraJeg__Org__7073AF84]  DEFAULT ('00000000-0000-0000-0000-000000000000') FOR [Org]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJ__Lezar__7167D3BD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] ADD  CONSTRAINT [DF__EREC_IraJ__Lezar__7167D3BD]  DEFAULT (NULL) FOR [LezarasDatuma]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJ__Vegre__725BF7F6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] ADD  CONSTRAINT [DF__EREC_IraJ__Vegre__725BF7F6]  DEFAULT (NULL) FOR [VegrehajtasDatuma]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJeg__Ver__73501C2F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] ADD  CONSTRAINT [DF__EREC_IraJeg__Ver__73501C2F]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJ__ErvKe__74444068]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] ADD  CONSTRAINT [DF__EREC_IraJ__ErvKe__74444068]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraJegyzekek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] ADD  CONSTRAINT [DF_EREC_IraJegyzekek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraJ__Letre__762C88DA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraJegyzekek] ADD  CONSTRAINT [DF__EREC_IraJ__Letre__762C88DA]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
