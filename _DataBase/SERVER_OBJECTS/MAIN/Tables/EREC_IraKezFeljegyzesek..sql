IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IraKezFeljegyzes_IratPld_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraKezFeljegyzesek]'))
ALTER TABLE [dbo].[EREC_IraKezFeljegyzesek] DROP CONSTRAINT [IraKezFeljegyzes_IratPld_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraK__Letre__10E07F16]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraKezFeljegyzesek] DROP CONSTRAINT [DF__EREC_IraK__Letre__10E07F16]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraKezFeljegyzesek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraKezFeljegyzesek] DROP CONSTRAINT [DF_EREC_IraKezFeljegyzesek_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraK__ErvKe__0EF836A4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraKezFeljegyzesek] DROP CONSTRAINT [DF__EREC_IraK__ErvKe__0EF836A4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraKez__Ver__0E04126B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraKezFeljegyzesek] DROP CONSTRAINT [DF__EREC_IraKez__Ver__0E04126B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraKezF__Id__0D0FEE32]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraKezFeljegyzesek] DROP CONSTRAINT [DF__EREC_IraKezF__Id__0D0FEE32]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezFeljegyzesek]') AND name = N'FK_IraIrat_Id')
DROP INDEX [FK_IraIrat_Id] ON [dbo].[EREC_IraKezFeljegyzesek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezFeljegyzesek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraKezFeljegyzesek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezFeljegyzesek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraKezFeljegyzesek](
	[Id] [uniqueidentifier] NOT NULL,
	[IraIrat_Id] [uniqueidentifier] NOT NULL,
	[KezelesTipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KIRL_IRA_KEZ_FELJEGYZESEK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezFeljegyzesek]') AND name = N'FK_IraIrat_Id')
CREATE NONCLUSTERED INDEX [FK_IraIrat_Id] ON [dbo].[EREC_IraKezFeljegyzesek]
(
	[IraIrat_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraKezF__Id__0D0FEE32]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraKezFeljegyzesek] ADD  CONSTRAINT [DF__EREC_IraKezF__Id__0D0FEE32]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraKez__Ver__0E04126B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraKezFeljegyzesek] ADD  CONSTRAINT [DF__EREC_IraKez__Ver__0E04126B]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraK__ErvKe__0EF836A4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraKezFeljegyzesek] ADD  CONSTRAINT [DF__EREC_IraK__ErvKe__0EF836A4]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IraKezFeljegyzesek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraKezFeljegyzesek] ADD  CONSTRAINT [DF_EREC_IraKezFeljegyzesek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IraK__Letre__10E07F16]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IraKezFeljegyzesek] ADD  CONSTRAINT [DF__EREC_IraK__Letre__10E07F16]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IraKezFeljegyzes_IratPld_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraKezFeljegyzesek]'))
ALTER TABLE [dbo].[EREC_IraKezFeljegyzesek]  WITH CHECK ADD  CONSTRAINT [IraKezFeljegyzes_IratPld_FK] FOREIGN KEY([IraIrat_Id])
REFERENCES [dbo].[EREC_PldIratPeldanyok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IraKezFeljegyzes_IratPld_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraKezFeljegyzesek]'))
ALTER TABLE [dbo].[EREC_IraKezFeljegyzesek] CHECK CONSTRAINT [IraKezFeljegyzes_IratPld_FK]
GO
