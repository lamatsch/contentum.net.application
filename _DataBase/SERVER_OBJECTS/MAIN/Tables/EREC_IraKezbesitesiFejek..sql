IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiFejek]') AND name = N'Index_1')
DROP INDEX [Index_1] ON [dbo].[EREC_IraKezbesitesiFejek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiFejek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraKezbesitesiFejek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiFejek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraKezbesitesiFejek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_IraKezb__Id__7FB5F314]  DEFAULT (newsequentialid()),
	[CsomagAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [char](1) COLLATE Hungarian_CI_AS NOT NULL,
	[UtolsoNyomtatasIdo] [datetime] NULL,
	[UtolsoNyomtatas_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_IraKez__Ver__00AA174D]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_IraK__ErvKe__019E3B86]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_IraKezbesitesiFejek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_IraK__Letre__038683F8]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IRAKEZBESITESIFEJEK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiFejek]') AND name = N'Index_1')
CREATE NONCLUSTERED INDEX [Index_1] ON [dbo].[EREC_IraKezbesitesiFejek]
(
	[Tipus] ASC,
	[CsomagAzonosito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
