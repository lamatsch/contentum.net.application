IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KezbesitesiTelel_AtveteliFej_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]'))
ALTER TABLE [dbo].[EREC_IraKezbesitesiTetelek] DROP CONSTRAINT [KezbesitesiTelel_AtveteliFej_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KezbesitesFek_KezbesitesTetelek_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]'))
ALTER TABLE [dbo].[EREC_IraKezbesitesiTetelek] DROP CONSTRAINT [KezbesitesFek_KezbesitesTetelek_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'KZT_UTSO_PRINT_ID')
DROP INDEX [KZT_UTSO_PRINT_ID] ON [dbo].[EREC_IraKezbesitesiTetelek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'KZT_UTSO_PRINT_DATUM')
DROP INDEX [KZT_UTSO_PRINT_DATUM] ON [dbo].[EREC_IraKezbesitesiTetelek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'KZT_PRT_ID_CEL')
DROP INDEX [KZT_PRT_ID_CEL] ON [dbo].[EREC_IraKezbesitesiTetelek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'KZT_PRT_ID_ATVEVO_USER')
DROP INDEX [KZT_PRT_ID_ATVEVO_USER] ON [dbo].[EREC_IraKezbesitesiTetelek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'KZT_PRT_ID_ATADO_USER')
DROP INDEX [KZT_PRT_ID_ATADO_USER] ON [dbo].[EREC_IraKezbesitesiTetelek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'KZT_ATVETEL_DATUMA')
DROP INDEX [KZT_ATVETEL_DATUMA] ON [dbo].[EREC_IraKezbesitesiTetelek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'IX_Objektum')
DROP INDEX [IX_Objektum] ON [dbo].[EREC_IraKezbesitesiTetelek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'IX_Atvetel')
DROP INDEX [IX_Atvetel] ON [dbo].[EREC_IraKezbesitesiTetelek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'IX_AtadasDat')
DROP INDEX [IX_AtadasDat] ON [dbo].[EREC_IraKezbesitesiTetelek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'IX_Atadas')
DROP INDEX [IX_Atadas] ON [dbo].[EREC_IraKezbesitesiTetelek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraKezbesitesiTetelek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraKezbesitesiTetelek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_IraKezb__Id__0662F0A3]  DEFAULT (newsequentialid()),
	[KezbesitesFej_Id] [uniqueidentifier] NULL,
	[AtveteliFej_Id] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AtadasDat] [datetime] NULL,
	[AtvetelDat] [datetime] NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[UtolsoNyomtatas_Id] [uniqueidentifier] NULL,
	[UtolsoNyomtatasIdo] [datetime] NULL,
	[Felhasznalo_Id_Atado_USER] [uniqueidentifier] NULL,
	[Felhasznalo_Id_Atado_LOGIN] [uniqueidentifier] NULL,
	[Csoport_Id_Cel] [uniqueidentifier] NULL,
	[Felhasznalo_Id_AtvevoUser] [uniqueidentifier] NULL,
	[Felhasznalo_Id_AtvevoLogin] [uniqueidentifier] NULL,
	[Allapot] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Azonosito_szoveges] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UgyintezesModja] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_IraKez__Ver__075714DC]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_IraK__ErvKe__084B3915]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_IraKezbesitesiTetelek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_IraK__Letre__0A338187]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KZT_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'IX_Atadas')
CREATE NONCLUSTERED INDEX [IX_Atadas] ON [dbo].[EREC_IraKezbesitesiTetelek]
(
	[KezbesitesFej_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'IX_AtadasDat')
CREATE NONCLUSTERED INDEX [IX_AtadasDat] ON [dbo].[EREC_IraKezbesitesiTetelek]
(
	[AtadasDat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'IX_Atvetel')
CREATE NONCLUSTERED INDEX [IX_Atvetel] ON [dbo].[EREC_IraKezbesitesiTetelek]
(
	[AtveteliFej_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'IX_Objektum')
CREATE NONCLUSTERED INDEX [IX_Objektum] ON [dbo].[EREC_IraKezbesitesiTetelek]
(
	[Obj_Id] ASC,
	[Obj_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'KZT_ATVETEL_DATUMA')
CREATE NONCLUSTERED INDEX [KZT_ATVETEL_DATUMA] ON [dbo].[EREC_IraKezbesitesiTetelek]
(
	[AtvetelDat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'KZT_PRT_ID_ATADO_USER')
CREATE NONCLUSTERED INDEX [KZT_PRT_ID_ATADO_USER] ON [dbo].[EREC_IraKezbesitesiTetelek]
(
	[Felhasznalo_Id_Atado_USER] ASC,
	[Allapot] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC
)
INCLUDE ( 	[Obj_type]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'KZT_PRT_ID_ATVEVO_USER')
CREATE NONCLUSTERED INDEX [KZT_PRT_ID_ATVEVO_USER] ON [dbo].[EREC_IraKezbesitesiTetelek]
(
	[Felhasznalo_Id_AtvevoUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'KZT_PRT_ID_CEL')
CREATE NONCLUSTERED INDEX [KZT_PRT_ID_CEL] ON [dbo].[EREC_IraKezbesitesiTetelek]
(
	[Csoport_Id_Cel] ASC,
	[Allapot] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC
)
INCLUDE ( 	[Obj_Id],
	[Obj_type]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'KZT_UTSO_PRINT_DATUM')
CREATE NONCLUSTERED INDEX [KZT_UTSO_PRINT_DATUM] ON [dbo].[EREC_IraKezbesitesiTetelek]
(
	[UtolsoNyomtatasIdo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]') AND name = N'KZT_UTSO_PRINT_ID')
CREATE NONCLUSTERED INDEX [KZT_UTSO_PRINT_ID] ON [dbo].[EREC_IraKezbesitesiTetelek]
(
	[UtolsoNyomtatas_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KezbesitesFek_KezbesitesTetelek_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]'))
ALTER TABLE [dbo].[EREC_IraKezbesitesiTetelek]  WITH CHECK ADD  CONSTRAINT [KezbesitesFek_KezbesitesTetelek_FK] FOREIGN KEY([KezbesitesFej_Id])
REFERENCES [dbo].[EREC_IraKezbesitesiFejek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KezbesitesFek_KezbesitesTetelek_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]'))
ALTER TABLE [dbo].[EREC_IraKezbesitesiTetelek] CHECK CONSTRAINT [KezbesitesFek_KezbesitesTetelek_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KezbesitesiTelel_AtveteliFej_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]'))
ALTER TABLE [dbo].[EREC_IraKezbesitesiTetelek]  WITH CHECK ADD  CONSTRAINT [KezbesitesiTelel_AtveteliFej_FK] FOREIGN KEY([AtveteliFej_Id])
REFERENCES [dbo].[EREC_IraKezbesitesiFejek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KezbesitesiTelel_AtveteliFej_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraKezbesitesiTetelek]'))
ALTER TABLE [dbo].[EREC_IraKezbesitesiTetelek] CHECK CONSTRAINT [KezbesitesiTelel_AtveteliFej_FK]
GO
