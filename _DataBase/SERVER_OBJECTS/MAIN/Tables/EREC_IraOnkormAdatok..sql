IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_IRA_FK_EREC_I_EREC_IRA]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraOnkormAdatok]'))
ALTER TABLE [dbo].[EREC_IraOnkormAdatok] DROP CONSTRAINT [FK_EREC_IRA_FK_EREC_I_EREC_IRA]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraOnkormAdatok]') AND name = N'FK_Ugy_Id')
DROP INDEX [FK_Ugy_Id] ON [dbo].[EREC_IraOnkormAdatok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraOnkormAdatok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IraOnkormAdatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraOnkormAdatok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IraOnkormAdatok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_IraOnko__Id__13BCEBC1]  DEFAULT (newsequentialid()),
	[IraIratok_Id] [uniqueidentifier] NULL,
	[UgyFajtaja] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[DontestHozta] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[DontesFormaja] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[UgyintezesHataridore] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[HataridoTullepes] [int] NULL,
	[JogorvoslatiEljarasTipusa] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[JogorvoslatiDontesTipusa] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[JogorvoslatiDontestHozta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[JogorvoslatiDontesTartalma] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[JogorvoslatiDontes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[HatosagiEllenorzes] [char](1) COLLATE Hungarian_CI_AS NULL,
	[MunkaorakSzama] [float] NULL,
	[EljarasiKoltseg] [int] NULL,
	[KozigazgatasiBirsagMerteke] [int] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_IraOnk__Ver__14B10FFA]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_IraO__ErvKe__15A53433]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_IraOnkormAdatok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_IraO__Letre__178D7CA5]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[SommasEljDontes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[NyolcNapBelulNemSommas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoHatalyuHatarozat] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoHatHatalybaLepes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoHatalyuVegzes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoVegzesHatalyba] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[HatAltalVisszafizOsszeg] [int] NULL,
	[HatTerheloEljKtsg] [int] NULL,
	[FelfuggHatarozat] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [ONK_ID_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraOnkormAdatok]') AND name = N'FK_Ugy_Id')
CREATE NONCLUSTERED INDEX [FK_Ugy_Id] ON [dbo].[EREC_IraOnkormAdatok]
(
	[IraIratok_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_IRA_FK_EREC_I_EREC_IRA]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraOnkormAdatok]'))
ALTER TABLE [dbo].[EREC_IraOnkormAdatok]  WITH CHECK ADD  CONSTRAINT [FK_EREC_IRA_FK_EREC_I_EREC_IRA] FOREIGN KEY([IraIratok_Id])
REFERENCES [dbo].[EREC_IraIratok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_IRA_FK_EREC_I_EREC_IRA]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IraOnkormAdatok]'))
ALTER TABLE [dbo].[EREC_IraOnkormAdatok] CHECK CONSTRAINT [FK_EREC_IRA_FK_EREC_I_EREC_IRA]
GO
