IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_IRATALAIRO_ALAIRSZABALY_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratAlairok]'))
ALTER TABLE [dbo].[EREC_IratAlairok] DROP CONSTRAINT [KRT_IRATALAIRO_ALAIRSZABALY_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IratAlairo_PldIratPeldany_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratAlairok]'))
ALTER TABLE [dbo].[EREC_IratAlairok] DROP CONSTRAINT [IratAlairo_PldIratPeldany_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratAlairok]') AND name = N'FK_IraIrat_Id')
DROP INDEX [FK_IraIrat_Id] ON [dbo].[EREC_IratAlairok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratAlairok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IratAlairok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratAlairok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IratAlairok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_IratAla__Id__27C3E46E]  DEFAULT (newsequentialid()),
	[PldIratPeldany_Id] [uniqueidentifier] NULL,
	[Objtip_Id] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[AlairasDatuma] [datetime] NULL,
	[Leiras] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[AlairoSzerep] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[AlairasSorrend] [int] NULL CONSTRAINT [DF__EREC_Irat__Alair__28B808A7]  DEFAULT ((1)),
	[FelhasznaloCsoport_Id_Alairo] [uniqueidentifier] NOT NULL,
	[FelhaszCsoport_Id_Helyettesito] [uniqueidentifier] NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AlairasMod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__EREC_Irat__Alair__29AC2CE0]  DEFAULT ('0'),
	[AlairasSzabaly_Id] [uniqueidentifier] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_IratAl__Ver__2AA05119]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Irat__ErvKe__2B947552]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_IratAlairok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Irat__Letre__2D7CBDC4]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [IRA_JH_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratAlairok]') AND name = N'FK_IraIrat_Id')
CREATE NONCLUSTERED INDEX [FK_IraIrat_Id] ON [dbo].[EREC_IratAlairok]
(
	[Obj_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IratAlairo_PldIratPeldany_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratAlairok]'))
ALTER TABLE [dbo].[EREC_IratAlairok]  WITH CHECK ADD  CONSTRAINT [IratAlairo_PldIratPeldany_FK] FOREIGN KEY([PldIratPeldany_Id])
REFERENCES [dbo].[EREC_PldIratPeldanyok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IratAlairo_PldIratPeldany_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratAlairok]'))
ALTER TABLE [dbo].[EREC_IratAlairok] CHECK CONSTRAINT [IratAlairo_PldIratPeldany_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_IRATALAIRO_ALAIRSZABALY_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratAlairok]'))
ALTER TABLE [dbo].[EREC_IratAlairok]  WITH CHECK ADD  CONSTRAINT [KRT_IRATALAIRO_ALAIRSZABALY_FK] FOREIGN KEY([AlairasSzabaly_Id])
REFERENCES [dbo].[KRT_AlairasSzabalyok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_IRATALAIRO_ALAIRSZABALY_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratAlairok]'))
ALTER TABLE [dbo].[EREC_IratAlairok] CHECK CONSTRAINT [KRT_IRATALAIRO_ALAIRSZABALY_FK]
GO
