IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Irat_Irat_felepules]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatok]'))
ALTER TABLE [dbo].[EREC_IratKapcsolatok] DROP CONSTRAINT [Irat_Irat_felepules]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Irat_Irat_Beepules_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatok]'))
ALTER TABLE [dbo].[EREC_IratKapcsolatok] DROP CONSTRAINT [Irat_Irat_Beepules_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__Letre__68294D9D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratKapcsolatok] DROP CONSTRAINT [DF__EREC_Irat__Letre__68294D9D]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IratKapcsolatok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratKapcsolatok] DROP CONSTRAINT [DF_EREC_IratKapcsolatok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__ErvKe__6641052B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratKapcsolatok] DROP CONSTRAINT [DF__EREC_Irat__ErvKe__6641052B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IratKa__Ver__654CE0F2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratKapcsolatok] DROP CONSTRAINT [DF__EREC_IratKa__Ver__654CE0F2]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IratKap__Id__6458BCB9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratKapcsolatok] DROP CONSTRAINT [DF__EREC_IratKap__Id__6458BCB9]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatok]') AND name = N'Felepul_ind')
DROP INDEX [Felepul_ind] ON [dbo].[EREC_IratKapcsolatok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatok]') AND name = N'Beepul_ind')
DROP INDEX [Beepul_ind] ON [dbo].[EREC_IratKapcsolatok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IratKapcsolatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IratKapcsolatok](
	[Id] [uniqueidentifier] NOT NULL,
	[KapcsolatTipus] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[Leiras] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Kezi] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Irat_Irat_Beepul] [uniqueidentifier] NULL,
	[Irat_Irat_Felepul] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KIRL_IRATKAPCSOLATOK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatok]') AND name = N'Beepul_ind')
CREATE NONCLUSTERED INDEX [Beepul_ind] ON [dbo].[EREC_IratKapcsolatok]
(
	[Irat_Irat_Beepul] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatok]') AND name = N'Felepul_ind')
CREATE NONCLUSTERED INDEX [Felepul_ind] ON [dbo].[EREC_IratKapcsolatok]
(
	[Irat_Irat_Felepul] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IratKap__Id__6458BCB9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratKapcsolatok] ADD  CONSTRAINT [DF__EREC_IratKap__Id__6458BCB9]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IratKa__Ver__654CE0F2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratKapcsolatok] ADD  CONSTRAINT [DF__EREC_IratKa__Ver__654CE0F2]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__ErvKe__6641052B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratKapcsolatok] ADD  CONSTRAINT [DF__EREC_Irat__ErvKe__6641052B]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IratKapcsolatok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratKapcsolatok] ADD  CONSTRAINT [DF_EREC_IratKapcsolatok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__Letre__68294D9D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratKapcsolatok] ADD  CONSTRAINT [DF__EREC_Irat__Letre__68294D9D]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Irat_Irat_Beepules_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatok]'))
ALTER TABLE [dbo].[EREC_IratKapcsolatok]  WITH CHECK ADD  CONSTRAINT [Irat_Irat_Beepules_FK] FOREIGN KEY([Irat_Irat_Beepul])
REFERENCES [dbo].[EREC_IraIratok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Irat_Irat_Beepules_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatok]'))
ALTER TABLE [dbo].[EREC_IratKapcsolatok] CHECK CONSTRAINT [Irat_Irat_Beepules_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Irat_Irat_felepules]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatok]'))
ALTER TABLE [dbo].[EREC_IratKapcsolatok]  WITH CHECK ADD  CONSTRAINT [Irat_Irat_felepules] FOREIGN KEY([Irat_Irat_Felepul])
REFERENCES [dbo].[EREC_IraIratok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Irat_Irat_felepules]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratKapcsolatok]'))
ALTER TABLE [dbo].[EREC_IratKapcsolatok] CHECK CONSTRAINT [Irat_Irat_felepules]
GO
