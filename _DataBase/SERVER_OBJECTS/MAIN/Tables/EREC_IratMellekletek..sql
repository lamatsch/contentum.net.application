IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__Letre__3BCADD1B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratMellekletek] DROP CONSTRAINT [DF__EREC_Irat__Letre__3BCADD1B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IratMellekletek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratMellekletek] DROP CONSTRAINT [DF_EREC_IratMellekletek_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__ErvKe__39E294A9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratMellekletek] DROP CONSTRAINT [DF__EREC_Irat__ErvKe__39E294A9]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IratMe__Ver__38EE7070]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratMellekletek] DROP CONSTRAINT [DF__EREC_IratMe__Ver__38EE7070]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMellekletek]') AND name = N'MEL_IRA_FK_I')
DROP INDEX [MEL_IRA_FK_I] ON [dbo].[EREC_IratMellekletek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMellekletek]') AND name = N'I_Barkod')
DROP INDEX [I_Barkod] ON [dbo].[EREC_IratMellekletek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMellekletek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IratMellekletek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMellekletek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IratMellekletek](
	[Id] [uniqueidentifier] NOT NULL,
	[IraIrat_Id] [uniqueidentifier] NOT NULL,
	[KuldMellekletek_Id] [uniqueidentifier] NULL,
	[AdathordozoTipus] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[MennyisegiEgyseg] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Mennyiseg] [int] NOT NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [MEL_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMellekletek]') AND name = N'I_Barkod')
CREATE NONCLUSTERED INDEX [I_Barkod] ON [dbo].[EREC_IratMellekletek]
(
	[BarCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMellekletek]') AND name = N'MEL_IRA_FK_I')
CREATE NONCLUSTERED INDEX [MEL_IRA_FK_I] ON [dbo].[EREC_IratMellekletek]
(
	[IraIrat_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_IratMe__Ver__38EE7070]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratMellekletek] ADD  CONSTRAINT [DF__EREC_IratMe__Ver__38EE7070]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__ErvKe__39E294A9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratMellekletek] ADD  CONSTRAINT [DF__EREC_Irat__ErvKe__39E294A9]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IratMellekletek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratMellekletek] ADD  CONSTRAINT [DF_EREC_IratMellekletek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__Letre__3BCADD1B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratMellekletek] ADD  CONSTRAINT [DF__EREC_Irat__Letre__3BCADD1B]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
