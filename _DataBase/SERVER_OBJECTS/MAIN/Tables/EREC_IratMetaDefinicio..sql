IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IratMeta_IrattariTetel_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratMetaDefinicio]'))
ALTER TABLE [dbo].[EREC_IratMetaDefinicio] DROP CONSTRAINT [IratMeta_IrattariTetel_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMetaDefinicio]') AND name = N'EREC_IratMetaDefinicio_UK')
DROP INDEX [EREC_IratMetaDefinicio_UK] ON [dbo].[EREC_IratMetaDefinicio]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMetaDefinicio]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IratMetaDefinicio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMetaDefinicio]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IratMetaDefinicio](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_IratMet__Id__3EA749C6]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_IratMe__Org__3F9B6DFF]  DEFAULT ('00000000-0000-0000-0000-000000000000'),
	[Ugykor_Id] [uniqueidentifier] NULL,
	[UgykorKod] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Ugytipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[UgytipusNev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[EljarasiSzakasz] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Irattipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[GeneraltTargy] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Rovidnev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UgyFajta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[UgyiratIntezesiIdo] [int] NULL,
	[Idoegyseg] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[UgyiratIntezesiIdoKotott] [char](1) COLLATE Hungarian_CI_AS NULL,
	[UgyiratHataridoKitolas] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__EREC_Irat__Ugyir__408F9238]  DEFAULT ('1'),
	[Ver] [int] NULL CONSTRAINT [DF__EREC_IratMe__Ver__4183B671]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Irat__ErvKe__4277DAAA]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_IratMetaDefinicio_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Irat__Letre__4460231C]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IRATMETADEFINICIO] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratMetaDefinicio]') AND name = N'EREC_IratMetaDefinicio_UK')
CREATE NONCLUSTERED INDEX [EREC_IratMetaDefinicio_UK] ON [dbo].[EREC_IratMetaDefinicio]
(
	[Org] ASC,
	[UgykorKod] ASC,
	[Ugytipus] ASC,
	[EljarasiSzakasz] ASC,
	[Irattipus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IratMeta_IrattariTetel_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratMetaDefinicio]'))
ALTER TABLE [dbo].[EREC_IratMetaDefinicio]  WITH CHECK ADD  CONSTRAINT [IratMeta_IrattariTetel_FK] FOREIGN KEY([Ugykor_Id])
REFERENCES [dbo].[EREC_IraIrattariTetelek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IratMeta_IrattariTetel_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratMetaDefinicio]'))
ALTER TABLE [dbo].[EREC_IratMetaDefinicio] CHECK CONSTRAINT [IratMeta_IrattariTetel_FK]
GO
