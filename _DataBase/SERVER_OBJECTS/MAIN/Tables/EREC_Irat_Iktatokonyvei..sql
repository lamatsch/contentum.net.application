IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KII_IKTK_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Irat_Iktatokonyvei]'))
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei] DROP CONSTRAINT [KII_IKTK_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IratIktatoKonyv_Csoport_Iktatohely_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Irat_Iktatokonyvei]'))
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei] DROP CONSTRAINT [IratIktatoKonyv_Csoport_Iktatohely_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Irat_Iktatokonyvei]') AND name = N'EREC_Irat_Iktatokonyv_UK')
DROP INDEX [EREC_Irat_Iktatokonyv_UK] ON [dbo].[EREC_Irat_Iktatokonyvei]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Irat_Iktatokonyvei]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_Irat_Iktatokonyvei]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Irat_Iktatokonyvei]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_Irat_Iktatokonyvei](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Irat_Ik__Id__1A69E950]  DEFAULT (newsequentialid()),
	[IraIktatokonyv_Id] [uniqueidentifier] NOT NULL,
	[Csoport_Id_Iktathat] [uniqueidentifier] NOT NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_Irat_I__Ver__1B5E0D89]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Irat__ErvKe__1C5231C2]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_Irat_Iktatokonyvei_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Irat__Letre__1E3A7A34]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KII_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Irat_Iktatokonyvei]') AND name = N'EREC_Irat_Iktatokonyv_UK')
CREATE NONCLUSTERED INDEX [EREC_Irat_Iktatokonyv_UK] ON [dbo].[EREC_Irat_Iktatokonyvei]
(
	[IraIktatokonyv_Id] ASC,
	[Csoport_Id_Iktathat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IratIktatoKonyv_Csoport_Iktatohely_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Irat_Iktatokonyvei]'))
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei]  WITH CHECK ADD  CONSTRAINT [IratIktatoKonyv_Csoport_Iktatohely_FK] FOREIGN KEY([Csoport_Id_Iktathat])
REFERENCES [dbo].[KRT_Csoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IratIktatoKonyv_Csoport_Iktatohely_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Irat_Iktatokonyvei]'))
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei] CHECK CONSTRAINT [IratIktatoKonyv_Csoport_Iktatohely_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KII_IKTK_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Irat_Iktatokonyvei]'))
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei]  WITH CHECK ADD  CONSTRAINT [KII_IKTK_FK] FOREIGN KEY([IraIktatokonyv_Id])
REFERENCES [dbo].[EREC_IraIktatoKonyvek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KII_IKTK_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Irat_Iktatokonyvei]'))
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei] CHECK CONSTRAINT [KII_IKTK_FK]
GO
