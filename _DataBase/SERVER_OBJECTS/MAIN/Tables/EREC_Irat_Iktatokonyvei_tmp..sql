IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__Letre__24E777C3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei_tmp] DROP CONSTRAINT [DF__EREC_Irat__Letre__24E777C3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_Irat_Iktatokonyvei_tmp_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei_tmp] DROP CONSTRAINT [DF_EREC_Irat_Iktatokonyvei_tmp_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__ErvKe__22FF2F51]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei_tmp] DROP CONSTRAINT [DF__EREC_Irat__ErvKe__22FF2F51]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat_I__Ver__220B0B18]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei_tmp] DROP CONSTRAINT [DF__EREC_Irat_I__Ver__220B0B18]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat_Ik__Id__2116E6DF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei_tmp] DROP CONSTRAINT [DF__EREC_Irat_Ik__Id__2116E6DF]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Irat_Iktatokonyvei_tmp]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_Irat_Iktatokonyvei_tmp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Irat_Iktatokonyvei_tmp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_Irat_Iktatokonyvei_tmp](
	[Id] [uniqueidentifier] NOT NULL,
	[IraIktatokonyv_Id] [uniqueidentifier] NOT NULL,
	[Csoport_Id_Iktathat] [uniqueidentifier] NOT NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KII_PK_tmp] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat_Ik__Id__2116E6DF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei_tmp] ADD  CONSTRAINT [DF__EREC_Irat_Ik__Id__2116E6DF]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat_I__Ver__220B0B18]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei_tmp] ADD  CONSTRAINT [DF__EREC_Irat_I__Ver__220B0B18]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__ErvKe__22FF2F51]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei_tmp] ADD  CONSTRAINT [DF__EREC_Irat__ErvKe__22FF2F51]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_Irat_Iktatokonyvei_tmp_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei_tmp] ADD  CONSTRAINT [DF_EREC_Irat_Iktatokonyvei_tmp_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__Letre__24E777C3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Irat_Iktatokonyvei_tmp] ADD  CONSTRAINT [DF__EREC_Irat__Letre__24E777C3]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
