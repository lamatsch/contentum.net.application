IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_MELLEKLET_ELEMKAPCS_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratelemKapcsolatok]'))
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok] DROP CONSTRAINT [EREC_MELLEKLET_ELEMKAPCS_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_CSATOLMANY_ELEMKAPCS_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratelemKapcsolatok]'))
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok] DROP CONSTRAINT [EREC_CSATOLMANY_ELEMKAPCS_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__Letre__3429BB53]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok] DROP CONSTRAINT [DF__EREC_Irat__Letre__3429BB53]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IratelemKapcsolatok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok] DROP CONSTRAINT [DF_EREC_IratelemKapcsolatok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__ErvKe__324172E1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok] DROP CONSTRAINT [DF__EREC_Irat__ErvKe__324172E1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Iratel__Ver__314D4EA8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok] DROP CONSTRAINT [DF__EREC_Iratel__Ver__314D4EA8]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Iratele__Id__30592A6F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok] DROP CONSTRAINT [DF__EREC_Iratele__Id__30592A6F]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratelemKapcsolatok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IratelemKapcsolatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IratelemKapcsolatok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IratelemKapcsolatok](
	[Id] [uniqueidentifier] NOT NULL,
	[Melleklet_Id] [uniqueidentifier] NULL,
	[Csatolmany_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IRATELEMKAPCSOLATOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Iratele__Id__30592A6F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok] ADD  CONSTRAINT [DF__EREC_Iratele__Id__30592A6F]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Iratel__Ver__314D4EA8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok] ADD  CONSTRAINT [DF__EREC_Iratel__Ver__314D4EA8]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__ErvKe__324172E1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok] ADD  CONSTRAINT [DF__EREC_Irat__ErvKe__324172E1]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IratelemKapcsolatok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok] ADD  CONSTRAINT [DF_EREC_IratelemKapcsolatok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__Letre__3429BB53]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok] ADD  CONSTRAINT [DF__EREC_Irat__Letre__3429BB53]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_CSATOLMANY_ELEMKAPCS_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratelemKapcsolatok]'))
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok]  WITH CHECK ADD  CONSTRAINT [EREC_CSATOLMANY_ELEMKAPCS_FK] FOREIGN KEY([Csatolmany_Id])
REFERENCES [dbo].[EREC_Csatolmanyok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_CSATOLMANY_ELEMKAPCS_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratelemKapcsolatok]'))
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok] CHECK CONSTRAINT [EREC_CSATOLMANY_ELEMKAPCS_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_MELLEKLET_ELEMKAPCS_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratelemKapcsolatok]'))
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok]  WITH CHECK ADD  CONSTRAINT [EREC_MELLEKLET_ELEMKAPCS_FK] FOREIGN KEY([Melleklet_Id])
REFERENCES [dbo].[EREC_Mellekletek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_MELLEKLET_ELEMKAPCS_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IratelemKapcsolatok]'))
ALTER TABLE [dbo].[EREC_IratelemKapcsolatok] CHECK CONSTRAINT [EREC_MELLEKLET_ELEMKAPCS_FK]
GO
