IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_IRA_FK_IRATTA_EREC_IRA]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IrattariHelyek]'))
ALTER TABLE [dbo].[EREC_IrattariHelyek] DROP CONSTRAINT [FK_EREC_IRA_FK_IRATTA_EREC_IRA]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__Letre__08EB22EA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariHelyek] DROP CONSTRAINT [DF__EREC_Irat__Letre__08EB22EA]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IrattariHelyek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariHelyek] DROP CONSTRAINT [DF_EREC_IrattariHelyek_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__ErvKe__0702DA78]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariHelyek] DROP CONSTRAINT [DF__EREC_Irat__ErvKe__0702DA78]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Iratta__Ver__060EB63F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariHelyek] DROP CONSTRAINT [DF__EREC_Iratta__Ver__060EB63F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irattar__Id__051A9206]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariHelyek] DROP CONSTRAINT [DF__EREC_Irattar__Id__051A9206]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariHelyek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IrattariHelyek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariHelyek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IrattariHelyek](
	[Id] [uniqueidentifier] NOT NULL,
	[Ertek] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Vonalkod] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[SzuloId] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[Felelos_Csoport_Id] [uniqueidentifier] NULL,
	[IrattarTipus] [char](1) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_IRATTARIHELYEK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irattar__Id__051A9206]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariHelyek] ADD  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Iratta__Ver__060EB63F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariHelyek] ADD  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__ErvKe__0702DA78]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariHelyek] ADD  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IrattariHelyek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariHelyek] ADD  CONSTRAINT [DF_EREC_IrattariHelyek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__Letre__08EB22EA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariHelyek] ADD  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_IRA_FK_IRATTA_EREC_IRA]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IrattariHelyek]'))
ALTER TABLE [dbo].[EREC_IrattariHelyek]  WITH CHECK ADD  CONSTRAINT [FK_EREC_IRA_FK_IRATTA_EREC_IRA] FOREIGN KEY([SzuloId])
REFERENCES [dbo].[EREC_IrattariHelyek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_IRA_FK_IRATTA_EREC_IRA]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IrattariHelyek]'))
ALTER TABLE [dbo].[EREC_IrattariHelyek] CHECK CONSTRAINT [FK_EREC_IRA_FK_IRATTA_EREC_IRA]
GO
