IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IratKikero_Ugyirat_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IrattariKikero]'))
ALTER TABLE [dbo].[EREC_IrattariKikero] DROP CONSTRAINT [IratKikero_Ugyirat_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__Letre__4B0D20AB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariKikero] DROP CONSTRAINT [DF__EREC_Irat__Letre__4B0D20AB]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IrattariKikero_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariKikero] DROP CONSTRAINT [DF_EREC_IrattariKikero_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__ErvKe__4924D839]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariKikero] DROP CONSTRAINT [DF__EREC_Irat__ErvKe__4924D839]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Iratta__Ver__4830B400]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariKikero] DROP CONSTRAINT [DF__EREC_Iratta__Ver__4830B400]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irattar__Id__473C8FC7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariKikero] DROP CONSTRAINT [DF__EREC_Irattar__Id__473C8FC7]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariKikero]') AND name = N'I_Barkod')
DROP INDEX [I_Barkod] ON [dbo].[EREC_IrattariKikero]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariKikero]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IrattariKikero]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariKikero]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IrattariKikero](
	[Id] [uniqueidentifier] NOT NULL,
	[Keres_note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[FelhasznalasiCel] [char](1) COLLATE Hungarian_CI_AS NULL,
	[DokumentumTipus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Indoklas_note] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[FelhasznaloCsoport_Id_Kikero] [uniqueidentifier] NULL,
	[KeresDatuma] [datetime] NULL,
	[KulsoAzonositok] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UgyUgyirat_Id] [uniqueidentifier] NULL,
	[Tertiveveny_Id] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[KikerKezd] [datetime] NULL,
	[KikerVege] [datetime] NULL,
	[FelhasznaloCsoport_Id_Jovahagy] [uniqueidentifier] NULL,
	[JovagyasDatuma] [datetime] NULL,
	[FelhasznaloCsoport_Id_Kiado] [uniqueidentifier] NULL,
	[KiadasDatuma] [datetime] NULL,
	[FelhasznaloCsoport_Id_Visszaad] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Visszave] [uniqueidentifier] NULL,
	[VisszaadasDatuma] [datetime] NULL,
	[SztornirozasDat] [datetime] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Irattar_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IRATTARIKIKERO] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariKikero]') AND name = N'I_Barkod')
CREATE NONCLUSTERED INDEX [I_Barkod] ON [dbo].[EREC_IrattariKikero]
(
	[BarCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irattar__Id__473C8FC7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariKikero] ADD  CONSTRAINT [DF__EREC_Irattar__Id__473C8FC7]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Iratta__Ver__4830B400]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariKikero] ADD  CONSTRAINT [DF__EREC_Iratta__Ver__4830B400]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__ErvKe__4924D839]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariKikero] ADD  CONSTRAINT [DF__EREC_Irat__ErvKe__4924D839]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_IrattariKikero_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariKikero] ADD  CONSTRAINT [DF_EREC_IrattariKikero_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Irat__Letre__4B0D20AB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_IrattariKikero] ADD  CONSTRAINT [DF__EREC_Irat__Letre__4B0D20AB]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IratKikero_Ugyirat_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IrattariKikero]'))
ALTER TABLE [dbo].[EREC_IrattariKikero]  WITH CHECK ADD  CONSTRAINT [IratKikero_Ugyirat_FK] FOREIGN KEY([UgyUgyirat_Id])
REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IratKikero_Ugyirat_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IrattariKikero]'))
ALTER TABLE [dbo].[EREC_IrattariKikero] CHECK CONSTRAINT [IratKikero_Ugyirat_FK]
GO
