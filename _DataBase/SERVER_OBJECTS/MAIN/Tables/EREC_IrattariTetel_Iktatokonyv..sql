IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kapcs_IrattariTetel_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_Iktatokonyv]'))
ALTER TABLE [dbo].[EREC_IrattariTetel_Iktatokonyv] DROP CONSTRAINT [Kapcs_IrattariTetel_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kapcs_Iktatokonyv_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_Iktatokonyv]'))
ALTER TABLE [dbo].[EREC_IrattariTetel_Iktatokonyv] DROP CONSTRAINT [Kapcs_Iktatokonyv_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_Iktatokonyv]') AND name = N'Index_2')
DROP INDEX [Index_2] ON [dbo].[EREC_IrattariTetel_Iktatokonyv]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_Iktatokonyv]') AND name = N'Index_1')
DROP INDEX [Index_1] ON [dbo].[EREC_IrattariTetel_Iktatokonyv]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_Iktatokonyv]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_IrattariTetel_Iktatokonyv]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_Iktatokonyv]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_IrattariTetel_Iktatokonyv](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Irattar__Id__4DE98D56]  DEFAULT (newsequentialid()),
	[IrattariTetel_Id] [uniqueidentifier] NULL,
	[Iktatokonyv_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_Iratta__Ver__4EDDB18F]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Irat__ErvKe__4FD1D5C8]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_IrattariTetel_Iktatokonyv_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Irat__Letre__51BA1E3A]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_IRATTARITETEL_IKTATOKO] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_Iktatokonyv]') AND name = N'Index_1')
CREATE NONCLUSTERED INDEX [Index_1] ON [dbo].[EREC_IrattariTetel_Iktatokonyv]
(
	[Iktatokonyv_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_Iktatokonyv]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[EREC_IrattariTetel_Iktatokonyv]
(
	[IrattariTetel_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kapcs_Iktatokonyv_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_Iktatokonyv]'))
ALTER TABLE [dbo].[EREC_IrattariTetel_Iktatokonyv]  WITH CHECK ADD  CONSTRAINT [Kapcs_Iktatokonyv_FK] FOREIGN KEY([Iktatokonyv_Id])
REFERENCES [dbo].[EREC_IraIktatoKonyvek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kapcs_Iktatokonyv_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_Iktatokonyv]'))
ALTER TABLE [dbo].[EREC_IrattariTetel_Iktatokonyv] CHECK CONSTRAINT [Kapcs_Iktatokonyv_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kapcs_IrattariTetel_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_Iktatokonyv]'))
ALTER TABLE [dbo].[EREC_IrattariTetel_Iktatokonyv]  WITH CHECK ADD  CONSTRAINT [Kapcs_IrattariTetel_FK] FOREIGN KEY([IrattariTetel_Id])
REFERENCES [dbo].[EREC_IraIrattariTetelek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kapcs_IrattariTetel_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_Iktatokonyv]'))
ALTER TABLE [dbo].[EREC_IrattariTetel_Iktatokonyv] CHECK CONSTRAINT [Kapcs_IrattariTetel_FK]
GO
