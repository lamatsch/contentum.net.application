IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KuldBekuldo_PartnerCim_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]'))
ALTER TABLE [dbo].[EREC_KuldBekuldok] DROP CONSTRAINT [KuldBekuldo_PartnerCim_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KuldBekuldo_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]'))
ALTER TABLE [dbo].[EREC_KuldBekuldok] DROP CONSTRAINT [KuldBekuldo_Partner_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KuldBekuldo_kuldemeny_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]'))
ALTER TABLE [dbo].[EREC_KuldBekuldok] DROP CONSTRAINT [KuldBekuldo_kuldemeny_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__Letre__6CEE02BA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldBekuldok] DROP CONSTRAINT [DF__EREC_Kuld__Letre__6CEE02BA]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldBekuldok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldBekuldok] DROP CONSTRAINT [DF_EREC_KuldBekuldok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__ErvKe__6B05BA48]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldBekuldok] DROP CONSTRAINT [DF__EREC_Kuld__ErvKe__6B05BA48]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldBe__Ver__6A11960F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldBekuldok] DROP CONSTRAINT [DF__EREC_KuldBe__Ver__6A11960F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldBek__Id__691D71D6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldBekuldok] DROP CONSTRAINT [DF__EREC_KuldBek__Id__691D71D6]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]') AND name = N'FK_PartnerCim_Id_Bekuldo')
DROP INDEX [FK_PartnerCim_Id_Bekuldo] ON [dbo].[EREC_KuldBekuldok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]') AND name = N'FK_Partner_Id_Bekuldo')
DROP INDEX [FK_Partner_Id_Bekuldo] ON [dbo].[EREC_KuldBekuldok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]') AND name = N'FK_KuldKuldemeny_Id')
DROP INDEX [FK_KuldKuldemeny_Id] ON [dbo].[EREC_KuldBekuldok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_KuldBekuldok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_KuldBekuldok](
	[Id] [uniqueidentifier] NOT NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NOT NULL,
	[Partner_Id_Bekuldo] [uniqueidentifier] NULL,
	[PartnerCim_Id_Bekuldo] [uniqueidentifier] NULL,
	[NevSTR] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[CimSTR] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KIRL_KULD_BEKULDOK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]') AND name = N'FK_KuldKuldemeny_Id')
CREATE NONCLUSTERED INDEX [FK_KuldKuldemeny_Id] ON [dbo].[EREC_KuldBekuldok]
(
	[KuldKuldemeny_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]') AND name = N'FK_Partner_Id_Bekuldo')
CREATE NONCLUSTERED INDEX [FK_Partner_Id_Bekuldo] ON [dbo].[EREC_KuldBekuldok]
(
	[Partner_Id_Bekuldo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]') AND name = N'FK_PartnerCim_Id_Bekuldo')
CREATE NONCLUSTERED INDEX [FK_PartnerCim_Id_Bekuldo] ON [dbo].[EREC_KuldBekuldok]
(
	[PartnerCim_Id_Bekuldo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldBek__Id__691D71D6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldBekuldok] ADD  CONSTRAINT [DF__EREC_KuldBek__Id__691D71D6]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldBe__Ver__6A11960F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldBekuldok] ADD  CONSTRAINT [DF__EREC_KuldBe__Ver__6A11960F]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__ErvKe__6B05BA48]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldBekuldok] ADD  CONSTRAINT [DF__EREC_Kuld__ErvKe__6B05BA48]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldBekuldok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldBekuldok] ADD  CONSTRAINT [DF_EREC_KuldBekuldok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__Letre__6CEE02BA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldBekuldok] ADD  CONSTRAINT [DF__EREC_Kuld__Letre__6CEE02BA]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KuldBekuldo_kuldemeny_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]'))
ALTER TABLE [dbo].[EREC_KuldBekuldok]  WITH CHECK ADD  CONSTRAINT [KuldBekuldo_kuldemeny_FK] FOREIGN KEY([KuldKuldemeny_Id])
REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KuldBekuldo_kuldemeny_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]'))
ALTER TABLE [dbo].[EREC_KuldBekuldok] CHECK CONSTRAINT [KuldBekuldo_kuldemeny_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KuldBekuldo_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]'))
ALTER TABLE [dbo].[EREC_KuldBekuldok]  WITH CHECK ADD  CONSTRAINT [KuldBekuldo_Partner_FK] FOREIGN KEY([Partner_Id_Bekuldo])
REFERENCES [dbo].[KRT_Partnerek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KuldBekuldo_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]'))
ALTER TABLE [dbo].[EREC_KuldBekuldok] CHECK CONSTRAINT [KuldBekuldo_Partner_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KuldBekuldo_PartnerCim_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]'))
ALTER TABLE [dbo].[EREC_KuldBekuldok]  WITH CHECK ADD  CONSTRAINT [KuldBekuldo_PartnerCim_FK] FOREIGN KEY([PartnerCim_Id_Bekuldo])
REFERENCES [dbo].[KRT_Cimek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KuldBekuldo_PartnerCim_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldBekuldok]'))
ALTER TABLE [dbo].[EREC_KuldBekuldok] CHECK CONSTRAINT [KuldBekuldo_PartnerCim_FK]
GO
