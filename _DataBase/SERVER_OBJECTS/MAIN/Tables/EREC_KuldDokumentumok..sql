IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__Letre__595B4002]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldDokumentumok] DROP CONSTRAINT [DF__EREC_Kuld__Letre__595B4002]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldDokumentumok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldDokumentumok] DROP CONSTRAINT [DF_EREC_KuldDokumentumok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__ErvKe__5772F790]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldDokumentumok] DROP CONSTRAINT [DF__EREC_Kuld__ErvKe__5772F790]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldDo__Ver__567ED357]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldDokumentumok] DROP CONSTRAINT [DF__EREC_KuldDo__Ver__567ED357]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldDokumentumok]') AND name = N'I_Barcode')
DROP INDEX [I_Barcode] ON [dbo].[EREC_KuldDokumentumok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldDokumentumok]') AND name = N'FK_KulKuldemeny_Id')
DROP INDEX [FK_KulKuldemeny_Id] ON [dbo].[EREC_KuldDokumentumok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldDokumentumok]') AND name = N'FK_Dokumentum_Id')
DROP INDEX [FK_Dokumentum_Id] ON [dbo].[EREC_KuldDokumentumok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldDokumentumok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_KuldDokumentumok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldDokumentumok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_KuldDokumentumok](
	[Id] [uniqueidentifier] NOT NULL,
	[Dokumentum_Id] [uniqueidentifier] NOT NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NOT NULL,
	[Leiras] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Lapszam] [int] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Forras] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Formatum] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Vonalkodozas] [char](1) COLLATE Hungarian_CI_AS NULL,
	[DokumentumSzerep] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KullDoku_PK] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldDokumentumok]') AND name = N'FK_Dokumentum_Id')
CREATE NONCLUSTERED INDEX [FK_Dokumentum_Id] ON [dbo].[EREC_KuldDokumentumok]
(
	[Dokumentum_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldDokumentumok]') AND name = N'FK_KulKuldemeny_Id')
CREATE NONCLUSTERED INDEX [FK_KulKuldemeny_Id] ON [dbo].[EREC_KuldDokumentumok]
(
	[KuldKuldemeny_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldDokumentumok]') AND name = N'I_Barcode')
CREATE NONCLUSTERED INDEX [I_Barcode] ON [dbo].[EREC_KuldDokumentumok]
(
	[BarCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldDo__Ver__567ED357]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldDokumentumok] ADD  CONSTRAINT [DF__EREC_KuldDo__Ver__567ED357]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__ErvKe__5772F790]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldDokumentumok] ADD  CONSTRAINT [DF__EREC_Kuld__ErvKe__5772F790]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldDokumentumok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldDokumentumok] ADD  CONSTRAINT [DF_EREC_KuldDokumentumok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__Letre__595B4002]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldDokumentumok] ADD  CONSTRAINT [DF__EREC_Kuld__Letre__595B4002]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
