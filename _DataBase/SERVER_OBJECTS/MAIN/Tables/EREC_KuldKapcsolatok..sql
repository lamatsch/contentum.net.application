IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kuldemeny_Kuldemeny_Felep_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatok]'))
ALTER TABLE [dbo].[EREC_KuldKapcsolatok] DROP CONSTRAINT [Kuldemeny_Kuldemeny_Felep_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kuldemeny_Kuldemeny_Beep_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatok]'))
ALTER TABLE [dbo].[EREC_KuldKapcsolatok] DROP CONSTRAINT [Kuldemeny_Kuldemeny_Beep_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__Letre__71B2B7D7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKapcsolatok] DROP CONSTRAINT [DF__EREC_Kuld__Letre__71B2B7D7]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldKapcsolatok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKapcsolatok] DROP CONSTRAINT [DF_EREC_KuldKapcsolatok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__ErvKe__6FCA6F65]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKapcsolatok] DROP CONSTRAINT [DF__EREC_Kuld__ErvKe__6FCA6F65]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldKa__Ver__6ED64B2C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKapcsolatok] DROP CONSTRAINT [DF__EREC_KuldKa__Ver__6ED64B2C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldKap__Id__6DE226F3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKapcsolatok] DROP CONSTRAINT [DF__EREC_KuldKap__Id__6DE226F3]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatok]') AND name = N'Felepul_ind')
DROP INDEX [Felepul_ind] ON [dbo].[EREC_KuldKapcsolatok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatok]') AND name = N'Beepul_ind')
DROP INDEX [Beepul_ind] ON [dbo].[EREC_KuldKapcsolatok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_KuldKapcsolatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_KuldKapcsolatok](
	[Id] [uniqueidentifier] NOT NULL,
	[KapcsolatTipus] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[Leiras] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Kezi] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Kuld_Kuld_Beepul] [uniqueidentifier] NULL,
	[Kuld_Kuld_Felepul] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KIRL_KULDKAPCSOLATOK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatok]') AND name = N'Beepul_ind')
CREATE NONCLUSTERED INDEX [Beepul_ind] ON [dbo].[EREC_KuldKapcsolatok]
(
	[Kuld_Kuld_Beepul] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatok]') AND name = N'Felepul_ind')
CREATE NONCLUSTERED INDEX [Felepul_ind] ON [dbo].[EREC_KuldKapcsolatok]
(
	[Kuld_Kuld_Felepul] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldKap__Id__6DE226F3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKapcsolatok] ADD  CONSTRAINT [DF__EREC_KuldKap__Id__6DE226F3]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldKa__Ver__6ED64B2C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKapcsolatok] ADD  CONSTRAINT [DF__EREC_KuldKa__Ver__6ED64B2C]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__ErvKe__6FCA6F65]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKapcsolatok] ADD  CONSTRAINT [DF__EREC_Kuld__ErvKe__6FCA6F65]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldKapcsolatok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKapcsolatok] ADD  CONSTRAINT [DF_EREC_KuldKapcsolatok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__Letre__71B2B7D7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKapcsolatok] ADD  CONSTRAINT [DF__EREC_Kuld__Letre__71B2B7D7]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kuldemeny_Kuldemeny_Beep_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatok]'))
ALTER TABLE [dbo].[EREC_KuldKapcsolatok]  WITH CHECK ADD  CONSTRAINT [Kuldemeny_Kuldemeny_Beep_FK] FOREIGN KEY([Kuld_Kuld_Beepul])
REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kuldemeny_Kuldemeny_Beep_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatok]'))
ALTER TABLE [dbo].[EREC_KuldKapcsolatok] CHECK CONSTRAINT [Kuldemeny_Kuldemeny_Beep_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kuldemeny_Kuldemeny_Felep_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatok]'))
ALTER TABLE [dbo].[EREC_KuldKapcsolatok]  WITH CHECK ADD  CONSTRAINT [Kuldemeny_Kuldemeny_Felep_FK] FOREIGN KEY([Kuld_Kuld_Felepul])
REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kuldemeny_Kuldemeny_Felep_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldKapcsolatok]'))
ALTER TABLE [dbo].[EREC_KuldKapcsolatok] CHECK CONSTRAINT [Kuldemeny_Kuldemeny_Felep_FK]
GO
