IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KKKF_KUL_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldKezFeljegyzesek]'))
ALTER TABLE [dbo].[EREC_KuldKezFeljegyzesek] DROP CONSTRAINT [KKKF_KUL_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__Letre__689D8392]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKezFeljegyzesek] DROP CONSTRAINT [DF__EREC_Kuld__Letre__689D8392]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldKezFeljegyzesek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKezFeljegyzesek] DROP CONSTRAINT [DF_EREC_KuldKezFeljegyzesek_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__ErvKe__66B53B20]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKezFeljegyzesek] DROP CONSTRAINT [DF__EREC_Kuld__ErvKe__66B53B20]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldKe__Ver__65C116E7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKezFeljegyzesek] DROP CONSTRAINT [DF__EREC_KuldKe__Ver__65C116E7]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldKez__Id__64CCF2AE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKezFeljegyzesek] DROP CONSTRAINT [DF__EREC_KuldKez__Id__64CCF2AE]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKezFeljegyzesek]') AND name = N'KEZELESI_TIPUS_I')
DROP INDEX [KEZELESI_TIPUS_I] ON [dbo].[EREC_KuldKezFeljegyzesek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKezFeljegyzesek]') AND name = N'FK_KulKuldemeny_Id')
DROP INDEX [FK_KulKuldemeny_Id] ON [dbo].[EREC_KuldKezFeljegyzesek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKezFeljegyzesek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_KuldKezFeljegyzesek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKezFeljegyzesek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_KuldKezFeljegyzesek](
	[Id] [uniqueidentifier] NOT NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NOT NULL,
	[KezelesTipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KKF_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKezFeljegyzesek]') AND name = N'FK_KulKuldemeny_Id')
CREATE NONCLUSTERED INDEX [FK_KulKuldemeny_Id] ON [dbo].[EREC_KuldKezFeljegyzesek]
(
	[KuldKuldemeny_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKezFeljegyzesek]') AND name = N'KEZELESI_TIPUS_I')
CREATE NONCLUSTERED INDEX [KEZELESI_TIPUS_I] ON [dbo].[EREC_KuldKezFeljegyzesek]
(
	[KezelesTipus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldKez__Id__64CCF2AE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKezFeljegyzesek] ADD  CONSTRAINT [DF__EREC_KuldKez__Id__64CCF2AE]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldKe__Ver__65C116E7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKezFeljegyzesek] ADD  CONSTRAINT [DF__EREC_KuldKe__Ver__65C116E7]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__ErvKe__66B53B20]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKezFeljegyzesek] ADD  CONSTRAINT [DF__EREC_Kuld__ErvKe__66B53B20]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldKezFeljegyzesek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKezFeljegyzesek] ADD  CONSTRAINT [DF_EREC_KuldKezFeljegyzesek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__Letre__689D8392]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldKezFeljegyzesek] ADD  CONSTRAINT [DF__EREC_Kuld__Letre__689D8392]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KKKF_KUL_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldKezFeljegyzesek]'))
ALTER TABLE [dbo].[EREC_KuldKezFeljegyzesek]  WITH CHECK ADD  CONSTRAINT [KKKF_KUL_FK] FOREIGN KEY([KuldKuldemeny_Id])
REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KKKF_KUL_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldKezFeljegyzesek]'))
ALTER TABLE [dbo].[EREC_KuldKezFeljegyzesek] CHECK CONSTRAINT [KKKF_KUL_FK]
GO
