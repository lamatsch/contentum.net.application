IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kuldemeny_Iktatokonyv_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]'))
ALTER TABLE [dbo].[EREC_KuldKuldemenyek] DROP CONSTRAINT [Kuldemeny_Iktatokonyv_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KUL_SZULO_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]'))
ALTER TABLE [dbo].[EREC_KuldKuldemenyek] DROP CONSTRAINT [KUL_SZULO_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'TKUL_PRT_ID_ALAIRO')
DROP INDEX [TKUL_PRT_ID_ALAIRO] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'TKUL_BELYEGZO_DATUM')
DROP INDEX [TKUL_BELYEGZO_DATUM] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'TEST_KULD_PRT_ID_ORZO')
DROP INDEX [TEST_KULD_PRT_ID_ORZO] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'TEST_ELSO_ATVETIDO_I')
DROP INDEX [TEST_ELSO_ATVETIDO_I] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'KUL_TOVABBITO_SZERV_I')
DROP INDEX [KUL_TOVABBITO_SZERV_I] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'KUL_SZULO_FK_I')
DROP INDEX [KUL_SZULO_FK_I] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_Ragszam')
DROP INDEX [IX_Ragszam] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_Postazas_iranya')
DROP INDEX [IX_Postazas_iranya] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_NevSTR')
DROP INDEX [IX_NevSTR] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_ModositasIdo_Kuldemeny')
DROP INDEX [IX_ModositasIdo_Kuldemeny] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_Letrehozo')
DROP INDEX [IX_Letrehozo] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_Kuldemeny_Partner_Id_Bekuldo')
DROP INDEX [IX_Kuldemeny_Partner_Id_Bekuldo] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_Hivatkozsai_szam')
DROP INDEX [IX_Hivatkozsai_szam] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_Erkeztetoszam_UK')
DROP INDEX [IX_Erkeztetoszam_UK] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_Allapot_MP')
DROP INDEX [IX_Allapot_MP] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'I_PRT_ID_FELELOS')
DROP INDEX [I_PRT_ID_FELELOS] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'I_PRT_ID_CIMZETT')
DROP INDEX [I_PRT_ID_CIMZETT] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'I_IratId')
DROP INDEX [I_IratId] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'I_Barcode')
DROP INDEX [I_Barcode] ON [dbo].[EREC_KuldKuldemenyek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_KuldKuldemenyek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_KuldKuldemenyek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_KuldKul__Id__6B79F03D]  DEFAULT (newsequentialid()),
	[Allapot] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[BeerkezesIdeje] [datetime] NOT NULL,
	[FelbontasDatuma] [datetime] NULL,
	[IraIktatokonyv_Id] [uniqueidentifier] NULL,
	[KuldesMod] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[Erkezteto_Szam] [int] NULL,
	[HivatkozasiSzam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Targy] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Tartalom] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[RagSzam] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Surgosseg] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[BelyegzoDatuma] [datetime] NULL,
	[UgyintezesModja] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[PostazasIranya] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[Tovabbito] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[PeldanySzam] [int] NOT NULL,
	[IktatniKell] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Iktathato] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[KuldKuldemeny_Id_Szulo] [uniqueidentifier] NULL,
	[Erkeztetes_Ev] [int] NULL,
	[Csoport_Id_Cimzett] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Expedial] [uniqueidentifier] NULL,
	[ExpedialasIdeje] [datetime] NULL,
	[FelhasznaloCsoport_Id_Orzo] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Atvevo] [uniqueidentifier] NULL,
	[Partner_Id_Bekuldo] [uniqueidentifier] NULL,
	[Cim_Id] [uniqueidentifier] NULL,
	[CimSTR_Bekuldo] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[NevSTR_Bekuldo] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[AdathordozoTipusa] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[ElsodlegesAdathordozoTipusa] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FelhasznaloCsoport_Id_Alairo] [uniqueidentifier] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[FelhasznaloCsoport_Id_Bonto] [uniqueidentifier] NULL,
	[CsoportFelelosEloszto_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos_Elozo] [uniqueidentifier] NULL,
	[Kovetkezo_Felelos_Id] [uniqueidentifier] NULL,
	[Elektronikus_Kezbesitesi_Allap] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Kovetkezo_Orzo_Id] [uniqueidentifier] NULL,
	[Fizikai_Kezbesitesi_Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[IraIratok_Id] [uniqueidentifier] NULL,
	[BontasiMegjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Minosites] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[MegtagadasIndoka] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Megtagado_Id] [uniqueidentifier] NULL,
	[MegtagadasDat] [datetime] NULL,
	[KimenoKuldemenyFajta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Elsobbsegi] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ajanlott] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Tertiveveny] [char](1) COLLATE Hungarian_CI_AS NULL,
	[SajatKezbe] [char](1) COLLATE Hungarian_CI_AS NULL,
	[E_ertesites] [char](1) COLLATE Hungarian_CI_AS NULL,
	[E_elorejelzes] [char](1) COLLATE Hungarian_CI_AS NULL,
	[PostaiLezaroSzolgalat] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ar] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KimenoKuld_Sorszam] [int] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_KuldKu__Ver__6C6E1476]  DEFAULT ((1)),
	[TovabbitasAlattAllapot] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[BoritoTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[MegorzesJelzo] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__EREC_Kuld__Megor__6D6238AF]  DEFAULT ('1'),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Kuld__ErvKe__6E565CE8]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_KuldKuldemenyek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Kuld__Letre__703EA55A]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[IktatastNemIgenyel] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__EREC_Kuld__Iktat__7132C993]  DEFAULT ('0'),
	[KezbesitesModja] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Munkaallomas] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SerultKuldemeny] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__EREC_Kuld__Serul__7226EDCC]  DEFAULT ('0'),
	[TevesCimzes] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__EREC_Kuld__Teves__731B1205]  DEFAULT ('0'),
	[TevesErkeztetes] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__EREC_Kuld__Teves__740F363E]  DEFAULT ('0'),
	[CimzesTipusa] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FutarJegyzekListaSzama] [Nvarchar](100) COLLATE Hungarian_CI_AS NULL,
    [Minosito] [uniqueidentifier] NULL,
    [MinositesErvenyessegiIdeje] [datetime] NULL,
    [TerjedelemMennyiseg]  [int] NULL,
    [TerjedelemMennyisegiEgyseg] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
    [TerjedelemMegjegyzes] [Nvarchar](400) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [KuldKuldemenyek_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'I_Barcode')
CREATE NONCLUSTERED INDEX [I_Barcode] ON [dbo].[EREC_KuldKuldemenyek]
(
	[BarCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'I_IratId')
CREATE NONCLUSTERED INDEX [I_IratId] ON [dbo].[EREC_KuldKuldemenyek]
(
	[IraIratok_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'I_PRT_ID_CIMZETT')
CREATE NONCLUSTERED INDEX [I_PRT_ID_CIMZETT] ON [dbo].[EREC_KuldKuldemenyek]
(
	[Csoport_Id_Cimzett] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'I_PRT_ID_FELELOS')
CREATE NONCLUSTERED INDEX [I_PRT_ID_FELELOS] ON [dbo].[EREC_KuldKuldemenyek]
(
	[Csoport_Id_Felelos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_Allapot_MP')
CREATE NONCLUSTERED INDEX [IX_Allapot_MP] ON [dbo].[EREC_KuldKuldemenyek]
(
	[Allapot] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_Erkeztetoszam_UK')
CREATE NONCLUSTERED INDEX [IX_Erkeztetoszam_UK] ON [dbo].[EREC_KuldKuldemenyek]
(
	[IraIktatokonyv_Id] ASC,
	[Erkezteto_Szam] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_Hivatkozsai_szam')
CREATE NONCLUSTERED INDEX [IX_Hivatkozsai_szam] ON [dbo].[EREC_KuldKuldemenyek]
(
	[HivatkozasiSzam] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_Kuldemeny_Partner_Id_Bekuldo')
CREATE NONCLUSTERED INDEX [IX_Kuldemeny_Partner_Id_Bekuldo] ON [dbo].[EREC_KuldKuldemenyek]
(
	[Partner_Id_Bekuldo] ASC,
	[NevSTR_Bekuldo] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC
)
INCLUDE ( 	[Cim_Id],
	[CimSTR_Bekuldo]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_Letrehozo')
CREATE NONCLUSTERED INDEX [IX_Letrehozo] ON [dbo].[EREC_KuldKuldemenyek]
(
	[Letrehozo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_ModositasIdo_Kuldemeny')
CREATE NONCLUSTERED INDEX [IX_ModositasIdo_Kuldemeny] ON [dbo].[EREC_KuldKuldemenyek]
(
	[ModositasIdo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_NevSTR')
CREATE NONCLUSTERED INDEX [IX_NevSTR] ON [dbo].[EREC_KuldKuldemenyek]
(
	[NevSTR_Bekuldo] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC
)
INCLUDE ( 	[Partner_Id_Bekuldo],
	[Cim_Id],
	[CimSTR_Bekuldo]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_Postazas_iranya')
CREATE NONCLUSTERED INDEX [IX_Postazas_iranya] ON [dbo].[EREC_KuldKuldemenyek]
(
	[PostazasIranya] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'IX_Ragszam')
CREATE NONCLUSTERED INDEX [IX_Ragszam] ON [dbo].[EREC_KuldKuldemenyek]
(
	[RagSzam] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'KUL_SZULO_FK_I')
CREATE NONCLUSTERED INDEX [KUL_SZULO_FK_I] ON [dbo].[EREC_KuldKuldemenyek]
(
	[KuldKuldemeny_Id_Szulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'KUL_TOVABBITO_SZERV_I')
CREATE NONCLUSTERED INDEX [KUL_TOVABBITO_SZERV_I] ON [dbo].[EREC_KuldKuldemenyek]
(
	[Tovabbito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'TEST_ELSO_ATVETIDO_I')
CREATE NONCLUSTERED INDEX [TEST_ELSO_ATVETIDO_I] ON [dbo].[EREC_KuldKuldemenyek]
(
	[BeerkezesIdeje] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'TEST_KULD_PRT_ID_ORZO')
CREATE NONCLUSTERED INDEX [TEST_KULD_PRT_ID_ORZO] ON [dbo].[EREC_KuldKuldemenyek]
(
	[FelhasznaloCsoport_Id_Orzo] ASC,
	[Allapot] ASC,
	[PostazasIranya] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'TKUL_BELYEGZO_DATUM')
CREATE NONCLUSTERED INDEX [TKUL_BELYEGZO_DATUM] ON [dbo].[EREC_KuldKuldemenyek]
(
	[BelyegzoDatuma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]') AND name = N'TKUL_PRT_ID_ALAIRO')
CREATE NONCLUSTERED INDEX [TKUL_PRT_ID_ALAIRO] ON [dbo].[EREC_KuldKuldemenyek]
(
	[FelhasznaloCsoport_Id_Alairo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KUL_SZULO_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]'))
ALTER TABLE [dbo].[EREC_KuldKuldemenyek]  WITH CHECK ADD  CONSTRAINT [KUL_SZULO_FK] FOREIGN KEY([KuldKuldemeny_Id_Szulo])
REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KUL_SZULO_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]'))
ALTER TABLE [dbo].[EREC_KuldKuldemenyek] CHECK CONSTRAINT [KUL_SZULO_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kuldemeny_Iktatokonyv_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]'))
ALTER TABLE [dbo].[EREC_KuldKuldemenyek]  WITH CHECK ADD  CONSTRAINT [Kuldemeny_Iktatokonyv_FK] FOREIGN KEY([IraIktatokonyv_Id])
REFERENCES [dbo].[EREC_IraIktatoKonyvek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kuldemeny_Iktatokonyv_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldKuldemenyek]'))
ALTER TABLE [dbo].[EREC_KuldKuldemenyek] CHECK CONSTRAINT [Kuldemeny_Iktatokonyv_FK]
GO
