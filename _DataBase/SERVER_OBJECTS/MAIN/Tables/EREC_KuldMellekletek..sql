IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__Letre__79C80F94]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldMellekletek] DROP CONSTRAINT [DF__EREC_Kuld__Letre__79C80F94]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldMellekletek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldMellekletek] DROP CONSTRAINT [DF_EREC_KuldMellekletek_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__ErvKe__77DFC722]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldMellekletek] DROP CONSTRAINT [DF__EREC_Kuld__ErvKe__77DFC722]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldMe__Ver__76EBA2E9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldMellekletek] DROP CONSTRAINT [DF__EREC_KuldMe__Ver__76EBA2E9]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldMellekletek]') AND name = N'KUM_KULD_FK_I')
DROP INDEX [KUM_KULD_FK_I] ON [dbo].[EREC_KuldMellekletek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldMellekletek]') AND name = N'I_Barcode')
DROP INDEX [I_Barcode] ON [dbo].[EREC_KuldMellekletek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldMellekletek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_KuldMellekletek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldMellekletek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_KuldMellekletek](
	[Id] [uniqueidentifier] NOT NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NOT NULL,
	[Mennyiseg] [int] NOT NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[AdathordozoTipus] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[MennyisegiEgyseg] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KUM_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldMellekletek]') AND name = N'I_Barcode')
CREATE NONCLUSTERED INDEX [I_Barcode] ON [dbo].[EREC_KuldMellekletek]
(
	[BarCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldMellekletek]') AND name = N'KUM_KULD_FK_I')
CREATE NONCLUSTERED INDEX [KUM_KULD_FK_I] ON [dbo].[EREC_KuldMellekletek]
(
	[KuldKuldemeny_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldMe__Ver__76EBA2E9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldMellekletek] ADD  CONSTRAINT [DF__EREC_KuldMe__Ver__76EBA2E9]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__ErvKe__77DFC722]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldMellekletek] ADD  CONSTRAINT [DF__EREC_Kuld__ErvKe__77DFC722]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldMellekletek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldMellekletek] ADD  CONSTRAINT [DF_EREC_KuldMellekletek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__Letre__79C80F94]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldMellekletek] ADD  CONSTRAINT [DF__EREC_Kuld__Letre__79C80F94]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
