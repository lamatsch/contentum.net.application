IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Tertiveveny_Kuldemeny_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldTertivevenyek]'))
ALTER TABLE [dbo].[EREC_KuldTertivevenyek] DROP CONSTRAINT [Tertiveveny_Kuldemeny_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__KezbV__0169315C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldTertivevenyek] DROP CONSTRAINT [DF__EREC_Kuld__KezbV__0169315C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__Letre__00750D23]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldTertivevenyek] DROP CONSTRAINT [DF__EREC_Kuld__Letre__00750D23]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldTertivevenyek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldTertivevenyek] DROP CONSTRAINT [DF_EREC_KuldTertivevenyek_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__ErvKe__7E8CC4B1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldTertivevenyek] DROP CONSTRAINT [DF__EREC_Kuld__ErvKe__7E8CC4B1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldTe__Ver__7D98A078]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldTertivevenyek] DROP CONSTRAINT [DF__EREC_KuldTe__Ver__7D98A078]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldTer__Id__7CA47C3F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldTertivevenyek] DROP CONSTRAINT [DF__EREC_KuldTer__Id__7CA47C3F]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldTertivevenyek]') AND name = N'I_Kuldemeny')
DROP INDEX [I_Kuldemeny] ON [dbo].[EREC_KuldTertivevenyek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldTertivevenyek]') AND name = N'I_Barcode')
DROP INDEX [I_Barcode] ON [dbo].[EREC_KuldTertivevenyek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldTertivevenyek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_KuldTertivevenyek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldTertivevenyek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_KuldTertivevenyek](
	[Id] [uniqueidentifier] NOT NULL,
	[Kuldemeny_Id] [uniqueidentifier] NULL,
	[Ragszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[TertivisszaKod] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[TertivisszaDat] [datetime] NULL,
	[AtvevoSzemely] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[AtvetelDat] [datetime] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[KezbVelelemBeallta] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KezbVelelemDatuma] [datetime] NULL,
 CONSTRAINT [KKT_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldTertivevenyek]') AND name = N'I_Barcode')
CREATE NONCLUSTERED INDEX [I_Barcode] ON [dbo].[EREC_KuldTertivevenyek]
(
	[BarCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldTertivevenyek]') AND name = N'I_Kuldemeny')
CREATE NONCLUSTERED INDEX [I_Kuldemeny] ON [dbo].[EREC_KuldTertivevenyek]
(
	[Kuldemeny_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldTer__Id__7CA47C3F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldTertivevenyek] ADD  CONSTRAINT [DF__EREC_KuldTer__Id__7CA47C3F]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_KuldTe__Ver__7D98A078]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldTertivevenyek] ADD  CONSTRAINT [DF__EREC_KuldTe__Ver__7D98A078]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__ErvKe__7E8CC4B1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldTertivevenyek] ADD  CONSTRAINT [DF__EREC_Kuld__ErvKe__7E8CC4B1]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_KuldTertivevenyek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldTertivevenyek] ADD  CONSTRAINT [DF_EREC_KuldTertivevenyek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__Letre__00750D23]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldTertivevenyek] ADD  CONSTRAINT [DF__EREC_Kuld__Letre__00750D23]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Kuld__KezbV__0169315C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_KuldTertivevenyek] ADD  CONSTRAINT [DF__EREC_Kuld__KezbV__0169315C]  DEFAULT ('0') FOR [KezbVelelemBeallta]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Tertiveveny_Kuldemeny_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldTertivevenyek]'))
ALTER TABLE [dbo].[EREC_KuldTertivevenyek]  WITH CHECK ADD  CONSTRAINT [Tertiveveny_Kuldemeny_FK] FOREIGN KEY([Kuldemeny_Id])
REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Tertiveveny_Kuldemeny_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_KuldTertivevenyek]'))
ALTER TABLE [dbo].[EREC_KuldTertivevenyek] CHECK CONSTRAINT [Tertiveveny_Kuldemeny_FK]
GO
