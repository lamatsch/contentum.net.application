IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KuldemenyIratPeldany_Kuldemeny_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyai]'))
ALTER TABLE [dbo].[EREC_Kuldemeny_IratPeldanyai] DROP CONSTRAINT [KuldemenyIratPeldany_Kuldemeny_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KuldemenyIratPeldany_IratPeldany_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyai]'))
ALTER TABLE [dbo].[EREC_Kuldemeny_IratPeldanyai] DROP CONSTRAINT [KuldemenyIratPeldany_IratPeldany_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyai]') AND name = N'FK_Peldany_Id')
DROP INDEX [FK_Peldany_Id] ON [dbo].[EREC_Kuldemeny_IratPeldanyai]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyai]') AND name = N'FK_KuldKuldemeny_Id')
DROP INDEX [FK_KuldKuldemeny_Id] ON [dbo].[EREC_Kuldemeny_IratPeldanyai]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyai]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_Kuldemeny_IratPeldanyai]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyai]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_Kuldemeny_IratPeldanyai](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Kuldeme__Id__5C37ACAD]  DEFAULT (newsequentialid()),
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[Peldany_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_Kuldem__Ver__5D2BD0E6]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Kuld__ErvKe__5E1FF51F]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_Kuldemeny_IratPeldanyai_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Kuld__Letre__60083D91]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_KULDEMENY_IRATPELDANYA] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyai]') AND name = N'FK_KuldKuldemeny_Id')
CREATE NONCLUSTERED INDEX [FK_KuldKuldemeny_Id] ON [dbo].[EREC_Kuldemeny_IratPeldanyai]
(
	[KuldKuldemeny_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyai]') AND name = N'FK_Peldany_Id')
CREATE NONCLUSTERED INDEX [FK_Peldany_Id] ON [dbo].[EREC_Kuldemeny_IratPeldanyai]
(
	[Peldany_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KuldemenyIratPeldany_IratPeldany_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyai]'))
ALTER TABLE [dbo].[EREC_Kuldemeny_IratPeldanyai]  WITH CHECK ADD  CONSTRAINT [KuldemenyIratPeldany_IratPeldany_FK] FOREIGN KEY([Peldany_Id])
REFERENCES [dbo].[EREC_PldIratPeldanyok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KuldemenyIratPeldany_IratPeldany_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyai]'))
ALTER TABLE [dbo].[EREC_Kuldemeny_IratPeldanyai] CHECK CONSTRAINT [KuldemenyIratPeldany_IratPeldany_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KuldemenyIratPeldany_Kuldemeny_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyai]'))
ALTER TABLE [dbo].[EREC_Kuldemeny_IratPeldanyai]  WITH CHECK ADD  CONSTRAINT [KuldemenyIratPeldany_Kuldemeny_FK] FOREIGN KEY([KuldKuldemeny_Id])
REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KuldemenyIratPeldany_Kuldemeny_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Kuldemeny_IratPeldanyai]'))
ALTER TABLE [dbo].[EREC_Kuldemeny_IratPeldanyai] CHECK CONSTRAINT [KuldemenyIratPeldany_Kuldemeny_FK]
GO
