IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KULDEMENY_MELLEKLET_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Mellekletek]'))
ALTER TABLE [dbo].[EREC_Mellekletek] DROP CONSTRAINT [EREC_KULDEMENY_MELLEKLET_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IRAT_MELLEKLET_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Mellekletek]'))
ALTER TABLE [dbo].[EREC_Mellekletek] DROP CONSTRAINT [EREC_IRAT_MELLEKLET_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Mellekletek]') AND name = N'IX_Mellekletek_Irat_Id')
DROP INDEX [IX_Mellekletek_Irat_Id] ON [dbo].[EREC_Mellekletek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Mellekletek]') AND name = N'IX_Kuldemeny_Id')
DROP INDEX [IX_Kuldemeny_Id] ON [dbo].[EREC_Mellekletek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Mellekletek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_Mellekletek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Mellekletek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_Mellekletek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Mellekl__Id__04459E07]  DEFAULT (newsequentialid()),
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[IraIrat_Id] [uniqueidentifier] NULL,
	[AdathordozoTipus] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[MennyisegiEgyseg] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Mennyiseg] [int] NOT NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_Mellek__Ver__0539C240]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Mell__ErvKe__062DE679]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_Mellekletek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Mell__Letre__08162EEB]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[MellekletAdathordozoTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_MELLEKLETEK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Mellekletek]') AND name = N'IX_Kuldemeny_Id')
CREATE NONCLUSTERED INDEX [IX_Kuldemeny_Id] ON [dbo].[EREC_Mellekletek]
(
	[KuldKuldemeny_Id] ASC,
	[AdathordozoTipus] ASC,
	[BarCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Mellekletek]') AND name = N'IX_Mellekletek_Irat_Id')
CREATE NONCLUSTERED INDEX [IX_Mellekletek_Irat_Id] ON [dbo].[EREC_Mellekletek]
(
	[IraIrat_Id] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IRAT_MELLEKLET_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Mellekletek]'))
ALTER TABLE [dbo].[EREC_Mellekletek]  WITH CHECK ADD  CONSTRAINT [EREC_IRAT_MELLEKLET_FK] FOREIGN KEY([IraIrat_Id])
REFERENCES [dbo].[EREC_IraIratok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IRAT_MELLEKLET_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Mellekletek]'))
ALTER TABLE [dbo].[EREC_Mellekletek] CHECK CONSTRAINT [EREC_IRAT_MELLEKLET_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KULDEMENY_MELLEKLET_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Mellekletek]'))
ALTER TABLE [dbo].[EREC_Mellekletek]  WITH CHECK ADD  CONSTRAINT [EREC_KULDEMENY_MELLEKLET_FK] FOREIGN KEY([KuldKuldemeny_Id])
REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KULDEMENY_MELLEKLET_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Mellekletek]'))
ALTER TABLE [dbo].[EREC_Mellekletek] CHECK CONSTRAINT [EREC_KULDEMENY_MELLEKLET_FK]
GO
