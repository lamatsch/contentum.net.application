IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[StateMetaDef_ObjStateVal_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_ObjStateValue]'))
ALTER TABLE [dbo].[EREC_ObjStateValue] DROP CONSTRAINT [StateMetaDef_ObjStateVal_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_ObjS__Letre__758348BB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_ObjStateValue] DROP CONSTRAINT [DF__EREC_ObjS__Letre__758348BB]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_ObjS__ErvKe__748F2482]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_ObjStateValue] DROP CONSTRAINT [DF__EREC_ObjS__ErvKe__748F2482]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_ObjSta__Ver__739B0049]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_ObjStateValue] DROP CONSTRAINT [DF__EREC_ObjSta__Ver__739B0049]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_ObjStat__Id__72A6DC10]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_ObjStateValue] DROP CONSTRAINT [DF__EREC_ObjStat__Id__72A6DC10]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_ObjStateValue]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_ObjStateValue]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_ObjStateValue]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_ObjStateValue](
	[Id] [uniqueidentifier] NOT NULL,
	[StateMetaDefinicio_Id] [uniqueidentifier] NULL,
	[ObjStateValue] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_OBJSTATEVALUE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_ObjStat__Id__72A6DC10]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_ObjStateValue] ADD  CONSTRAINT [DF__EREC_ObjStat__Id__72A6DC10]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_ObjSta__Ver__739B0049]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_ObjStateValue] ADD  CONSTRAINT [DF__EREC_ObjSta__Ver__739B0049]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_ObjS__ErvKe__748F2482]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_ObjStateValue] ADD  CONSTRAINT [DF__EREC_ObjS__ErvKe__748F2482]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_ObjS__Letre__758348BB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_ObjStateValue] ADD  CONSTRAINT [DF__EREC_ObjS__Letre__758348BB]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[StateMetaDef_ObjStateVal_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_ObjStateValue]'))
ALTER TABLE [dbo].[EREC_ObjStateValue]  WITH CHECK ADD  CONSTRAINT [StateMetaDef_ObjStateVal_FK] FOREIGN KEY([StateMetaDefinicio_Id])
REFERENCES [dbo].[EREC_StateMetaDefinicio] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[StateMetaDef_ObjStateVal_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_ObjStateValue]'))
ALTER TABLE [dbo].[EREC_ObjStateValue] CHECK CONSTRAINT [StateMetaDef_ObjStateVal_FK]
GO
