IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Obj_MetaAdatai_Targyszavak_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdatai]'))
ALTER TABLE [dbo].[EREC_Obj_MetaAdatai] DROP CONSTRAINT [Obj_MetaAdatai_Targyszavak_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_OBJTARGYSZO_OBJMETADEF_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdatai]'))
ALTER TABLE [dbo].[EREC_Obj_MetaAdatai] DROP CONSTRAINT [EREC_OBJTARGYSZO_OBJMETADEF_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdatai]') AND name = N'IX_Targyszavak')
DROP INDEX [IX_Targyszavak] ON [dbo].[EREC_Obj_MetaAdatai]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdatai]') AND name = N'IX_ObjMetadefinicio')
DROP INDEX [IX_ObjMetadefinicio] ON [dbo].[EREC_Obj_MetaAdatai]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdatai]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_Obj_MetaAdatai]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdatai]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_Obj_MetaAdatai](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Obj_Met__Id__0AF29B96]  DEFAULT (newsequentialid()),
	[Obj_MetaDefinicio_Id] [uniqueidentifier] NULL,
	[Targyszavak_Id] [uniqueidentifier] NULL,
	[AlapertelmezettErtek] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Sorszam] [int] NOT NULL,
	[Opcionalis] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ismetlodo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Funkcio] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[ControlTypeSource] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[ControlTypeDataSource] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[SPSSzinkronizalt] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__EREC_Obj___SPSSz__0BE6BFCF]  DEFAULT ('0'),
	[SPS_Field_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_Obj_Me__Ver__0CDAE408]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Obj___ErvKe__0DCF0841]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_Obj_MetaAdatai_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_Id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Obj___Letre__0FB750B3]  DEFAULT (getdate()),
	[Modosito_Id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_Id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_Id] [uniqueidentifier] NULL,
	[UIAccessLog_Id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KIRL_UGY_TARGYSZAVAI] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdatai]') AND name = N'IX_ObjMetadefinicio')
CREATE NONCLUSTERED INDEX [IX_ObjMetadefinicio] ON [dbo].[EREC_Obj_MetaAdatai]
(
	[Obj_MetaDefinicio_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdatai]') AND name = N'IX_Targyszavak')
CREATE NONCLUSTERED INDEX [IX_Targyszavak] ON [dbo].[EREC_Obj_MetaAdatai]
(
	[Targyszavak_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_OBJTARGYSZO_OBJMETADEF_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdatai]'))
ALTER TABLE [dbo].[EREC_Obj_MetaAdatai]  WITH CHECK ADD  CONSTRAINT [EREC_OBJTARGYSZO_OBJMETADEF_FK] FOREIGN KEY([Obj_MetaDefinicio_Id])
REFERENCES [dbo].[EREC_Obj_MetaDefinicio] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_OBJTARGYSZO_OBJMETADEF_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdatai]'))
ALTER TABLE [dbo].[EREC_Obj_MetaAdatai] CHECK CONSTRAINT [EREC_OBJTARGYSZO_OBJMETADEF_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Obj_MetaAdatai_Targyszavak_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdatai]'))
ALTER TABLE [dbo].[EREC_Obj_MetaAdatai]  WITH CHECK ADD  CONSTRAINT [Obj_MetaAdatai_Targyszavak_FK] FOREIGN KEY([Targyszavak_Id])
REFERENCES [dbo].[EREC_TargySzavak] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Obj_MetaAdatai_Targyszavak_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaAdatai]'))
ALTER TABLE [dbo].[EREC_Obj_MetaAdatai] CHECK CONSTRAINT [Obj_MetaAdatai_Targyszavak_FK]
GO
