IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_OBJMETA_OBJTIPCOL_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]'))
ALTER TABLE [dbo].[EREC_Obj_MetaDefinicio] DROP CONSTRAINT [EREC_OBJMETA_OBJTIPCOL_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_OBJMETA_OBJTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]'))
ALTER TABLE [dbo].[EREC_Obj_MetaDefinicio] DROP CONSTRAINT [EREC_OBJMETA_OBJTIP_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_OBJMETA_FELETTESMETA_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]'))
ALTER TABLE [dbo].[EREC_Obj_MetaDefinicio] DROP CONSTRAINT [EREC_OBJMETA_FELETTESMETA_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]') AND name = N'IX_ObjTipCol_FK')
DROP INDEX [IX_ObjTipCol_FK] ON [dbo].[EREC_Obj_MetaDefinicio]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]') AND name = N'IX_Objtip_FK')
DROP INDEX [IX_Objtip_FK] ON [dbo].[EREC_Obj_MetaDefinicio]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]') AND name = N'IX_FelettesMeta_FK')
DROP INDEX [IX_FelettesMeta_FK] ON [dbo].[EREC_Obj_MetaDefinicio]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_Obj_MetaDefinicio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_Obj_MetaDefinicio](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Obj_Met__Id__1387E197]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Obj_Me__Org__147C05D0]  DEFAULT ('00000000-0000-0000-0000-000000000000'),
	[Objtip_Id] [uniqueidentifier] NULL,
	[Objtip_Id_Column] [uniqueidentifier] NULL,
	[ColumnValue] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[DefinicioTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Felettes_Obj_Meta] [uniqueidentifier] NULL,
	[MetaXSD] [xml] NULL,
	[ImportXML] [xml] NULL,
	[EgyebXML] [xml] NULL,
	[ContentType] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SPSSzinkronizalt] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__EREC_Obj___SPSSz__15702A09]  DEFAULT ('0'),
	[SPS_CTT_Id] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[WorkFlowVezerles] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__EREC_Obj___WorkF__16644E42]  DEFAULT ('0'),
	[Ver] [int] NULL CONSTRAINT [DF__EREC_Obj_Me__Ver__1758727B]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Obj___ErvKe__184C96B4]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_Obj_MetaDefinicio_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Obj___Letre__1A34DF26]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_OBJ_METADEFINICIO] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY],
 CONSTRAINT [AK_OBJMETADEFINICIO] UNIQUE NONCLUSTERED 
(
	[Org] ASC,
	[ContentType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]') AND name = N'IX_FelettesMeta_FK')
CREATE NONCLUSTERED INDEX [IX_FelettesMeta_FK] ON [dbo].[EREC_Obj_MetaDefinicio]
(
	[Felettes_Obj_Meta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]') AND name = N'IX_Objtip_FK')
CREATE NONCLUSTERED INDEX [IX_Objtip_FK] ON [dbo].[EREC_Obj_MetaDefinicio]
(
	[Objtip_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]') AND name = N'IX_ObjTipCol_FK')
CREATE NONCLUSTERED INDEX [IX_ObjTipCol_FK] ON [dbo].[EREC_Obj_MetaDefinicio]
(
	[Objtip_Id_Column] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_OBJMETA_FELETTESMETA_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]'))
ALTER TABLE [dbo].[EREC_Obj_MetaDefinicio]  WITH CHECK ADD  CONSTRAINT [EREC_OBJMETA_FELETTESMETA_FK] FOREIGN KEY([Felettes_Obj_Meta])
REFERENCES [dbo].[EREC_Obj_MetaDefinicio] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_OBJMETA_FELETTESMETA_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]'))
ALTER TABLE [dbo].[EREC_Obj_MetaDefinicio] CHECK CONSTRAINT [EREC_OBJMETA_FELETTESMETA_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_OBJMETA_OBJTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]'))
ALTER TABLE [dbo].[EREC_Obj_MetaDefinicio]  WITH CHECK ADD  CONSTRAINT [EREC_OBJMETA_OBJTIP_FK] FOREIGN KEY([Objtip_Id])
REFERENCES [dbo].[KRT_ObjTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_OBJMETA_OBJTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]'))
ALTER TABLE [dbo].[EREC_Obj_MetaDefinicio] CHECK CONSTRAINT [EREC_OBJMETA_OBJTIP_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_OBJMETA_OBJTIPCOL_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]'))
ALTER TABLE [dbo].[EREC_Obj_MetaDefinicio]  WITH CHECK ADD  CONSTRAINT [EREC_OBJMETA_OBJTIPCOL_FK] FOREIGN KEY([Objtip_Id_Column])
REFERENCES [dbo].[KRT_ObjTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_OBJMETA_OBJTIPCOL_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicio]'))
ALTER TABLE [dbo].[EREC_Obj_MetaDefinicio] CHECK CONSTRAINT [EREC_OBJMETA_OBJTIPCOL_FK]
GO
