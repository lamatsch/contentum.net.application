IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ObjektumTargyszo_Targyszo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_ObjektumTargyszavai]'))
ALTER TABLE [dbo].[EREC_ObjektumTargyszavai] DROP CONSTRAINT [ObjektumTargyszo_Targyszo_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_OBJTARGYSZO_OBJMETA_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_ObjektumTargyszavai]'))
ALTER TABLE [dbo].[EREC_ObjektumTargyszavai] DROP CONSTRAINT [EREC_OBJTARGYSZO_OBJMETA_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_ObjektumTargyszavai]') AND name = N'IX_Obj_Objtip')
DROP INDEX [IX_Obj_Objtip] ON [dbo].[EREC_ObjektumTargyszavai]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_ObjektumTargyszavai]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_ObjektumTargyszavai]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_ObjektumTargyszavai]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_ObjektumTargyszavai](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Objektu__Id__1D114BD1]  DEFAULT (newsequentialid()),
	[Targyszo_Id] [uniqueidentifier] NULL,
	[Obj_Metaadatai_Id] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NOT NULL,
	[ObjTip_Id] [uniqueidentifier] NOT NULL,
	[Targyszo] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Forras] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[SPSSzinkronizalt] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__EREC_Obje__SPSSz__1E05700A]  DEFAULT ('0'),
	[Ertek] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Sorszam] [int] NULL,
	[Targyszo_XML] [xml] NULL,
	[Targyszo_XML_Ervenyes] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_Objekt__Ver__1EF99443]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Obje__ErvKe__1FEDB87C]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_ObjektumTargyszavai_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Obje__Letre__21D600EE]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_OBJEKTUMTARGYSZAVAI] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_ObjektumTargyszavai]') AND name = N'IX_Obj_Objtip')
CREATE NONCLUSTERED INDEX [IX_Obj_Objtip] ON [dbo].[EREC_ObjektumTargyszavai]
(
	[Obj_Id] ASC,
	[ObjTip_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_OBJTARGYSZO_OBJMETA_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_ObjektumTargyszavai]'))
ALTER TABLE [dbo].[EREC_ObjektumTargyszavai]  WITH CHECK ADD  CONSTRAINT [EREC_OBJTARGYSZO_OBJMETA_FK] FOREIGN KEY([Obj_Metaadatai_Id])
REFERENCES [dbo].[EREC_Obj_MetaAdatai] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_OBJTARGYSZO_OBJMETA_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_ObjektumTargyszavai]'))
ALTER TABLE [dbo].[EREC_ObjektumTargyszavai] CHECK CONSTRAINT [EREC_OBJTARGYSZO_OBJMETA_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ObjektumTargyszo_Targyszo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_ObjektumTargyszavai]'))
ALTER TABLE [dbo].[EREC_ObjektumTargyszavai]  WITH CHECK ADD  CONSTRAINT [ObjektumTargyszo_Targyszo_FK] FOREIGN KEY([Targyszo_Id])
REFERENCES [dbo].[EREC_TargySzavak] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ObjektumTargyszo_Targyszo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_ObjektumTargyszavai]'))
ALTER TABLE [dbo].[EREC_ObjektumTargyszavai] CHECK CONSTRAINT [ObjektumTargyszo_Targyszo_FK]
GO
