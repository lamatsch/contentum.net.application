IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IRP_IRA_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]'))
ALTER TABLE [dbo].[EREC_PldIratPeldanyok] DROP CONSTRAINT [IRP_IRA_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratpeldanyok_Ugyiratok_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]'))
ALTER TABLE [dbo].[EREC_PldIratPeldanyok] DROP CONSTRAINT [EREC_PldIratpeldanyok_Ugyiratok_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'TIRP_ALLAPOT')
DROP INDEX [TIRP_ALLAPOT] ON [dbo].[EREC_PldIratPeldanyok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'KULDESMOD_I')
DROP INDEX [KULDESMOD_I] ON [dbo].[EREC_PldIratPeldanyok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'IX_ModositasIdo')
DROP INDEX [IX_ModositasIdo] ON [dbo].[EREC_PldIratPeldanyok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'IX_Iratpld_Sorszam')
DROP INDEX [IX_Iratpld_Sorszam] ON [dbo].[EREC_PldIratPeldanyok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'IX_Iratpld_Kulso_ID')
DROP INDEX [IX_Iratpld_Kulso_ID] ON [dbo].[EREC_PldIratPeldanyok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'IX_FelhasznaloCsoport_Id_Orzo')
DROP INDEX [IX_FelhasznaloCsoport_Id_Orzo] ON [dbo].[EREC_PldIratPeldanyok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'IX_ervkezd_ervvege')
DROP INDEX [IX_ervkezd_ervvege] ON [dbo].[EREC_PldIratPeldanyok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'IRP_PRT_ID_CIMZETT_I')
DROP INDEX [IRP_PRT_ID_CIMZETT_I] ON [dbo].[EREC_PldIratPeldanyok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'Iratpeldany_Letrehozo_I')
DROP INDEX [Iratpeldany_Letrehozo_I] ON [dbo].[EREC_PldIratPeldanyok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'Iratpeldany_LetrehozasIdo_I')
DROP INDEX [Iratpeldany_LetrehozasIdo_I] ON [dbo].[EREC_PldIratPeldanyok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'IRA_KAPCSOLT_IND')
DROP INDEX [IRA_KAPCSOLT_IND] ON [dbo].[EREC_PldIratPeldanyok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'I_PRT_ID_FEL')
DROP INDEX [I_PRT_ID_FEL] ON [dbo].[EREC_PldIratPeldanyok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'I_barcode')
DROP INDEX [I_barcode] ON [dbo].[EREC_PldIratPeldanyok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'EREC_PldIratPeldany_UK')
DROP INDEX [EREC_PldIratPeldany_UK] ON [dbo].[EREC_PldIratPeldanyok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_PldIratPeldanyok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_PldIratPeldanyok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_PldIrat__Id__269AB60B]  DEFAULT (newsequentialid()),
	[IraIrat_Id] [uniqueidentifier] NOT NULL,
	[UgyUgyirat_Id_Kulso] [uniqueidentifier] NULL,
	[Sorszam] [int] NOT NULL,
	[SztornirozasDat] [datetime] NULL,
	[AtvetelDatuma] [datetime] NULL,
	[Eredet] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[KuldesMod] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Ragszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UgyintezesModja] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[VisszaerkezesiHatarido] [datetime] NULL,
	[Visszavarolag] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[VisszaerkezesDatuma] [datetime] NULL,
	[Cim_id_Cimzett] [uniqueidentifier] NULL,
	[Partner_Id_Cimzett] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Orzo] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos_Elozo] [uniqueidentifier] NULL,
	[CimSTR_Cimzett] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[NevSTR_Cimzett] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Tovabbito] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[IraIrat_Id_Kapcsolt] [uniqueidentifier] NULL,
	[IrattariHely] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Gener_Id] [uniqueidentifier] NULL,
	[PostazasDatuma] [datetime] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Kovetkezo_Felelos_Id] [uniqueidentifier] NULL,
	[Elektronikus_Kezbesitesi_Allap] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Kovetkezo_Orzo_Id] [uniqueidentifier] NULL,
	[Fizikai_Kezbesitesi_Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[TovabbitasAlattAllapot] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[PostazasAllapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[ValaszElektronikus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_PldIra__Ver__278EDA44]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_PldI__ErvKe__2882FE7D]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_PldIratPeldanyok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_PldI__Letre__2A6B46EF]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[AKTIV]  AS (case when [ALLAPOT]='90' then (0) else (1) end) PERSISTED NOT NULL,
 CONSTRAINT [PldIratpeldanyok__IratokSsz_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'EREC_PldIratPeldany_UK')
CREATE NONCLUSTERED INDEX [EREC_PldIratPeldany_UK] ON [dbo].[EREC_PldIratPeldanyok]
(
	[IraIrat_Id] ASC,
	[Sorszam] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'I_barcode')
CREATE NONCLUSTERED INDEX [I_barcode] ON [dbo].[EREC_PldIratPeldanyok]
(
	[BarCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'I_PRT_ID_FEL')
CREATE NONCLUSTERED INDEX [I_PRT_ID_FEL] ON [dbo].[EREC_PldIratPeldanyok]
(
	[Csoport_Id_Felelos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'IRA_KAPCSOLT_IND')
CREATE NONCLUSTERED INDEX [IRA_KAPCSOLT_IND] ON [dbo].[EREC_PldIratPeldanyok]
(
	[IraIrat_Id_Kapcsolt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'Iratpeldany_LetrehozasIdo_I')
CREATE NONCLUSTERED INDEX [Iratpeldany_LetrehozasIdo_I] ON [dbo].[EREC_PldIratPeldanyok]
(
	[LetrehozasIdo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'Iratpeldany_Letrehozo_I')
CREATE NONCLUSTERED INDEX [Iratpeldany_Letrehozo_I] ON [dbo].[EREC_PldIratPeldanyok]
(
	[Letrehozo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'IRP_PRT_ID_CIMZETT_I')
CREATE NONCLUSTERED INDEX [IRP_PRT_ID_CIMZETT_I] ON [dbo].[EREC_PldIratPeldanyok]
(
	[Partner_Id_Cimzett] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'IX_ervkezd_ervvege')
CREATE NONCLUSTERED INDEX [IX_ervkezd_ervvege] ON [dbo].[EREC_PldIratPeldanyok]
(
	[ErvKezd] ASC,
	[ErvVege] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'IX_FelhasznaloCsoport_Id_Orzo')
CREATE NONCLUSTERED INDEX [IX_FelhasznaloCsoport_Id_Orzo] ON [dbo].[EREC_PldIratPeldanyok]
(
	[FelhasznaloCsoport_Id_Orzo] ASC
)
INCLUDE ( 	[IraIrat_Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'IX_Iratpld_Kulso_ID')
CREATE NONCLUSTERED INDEX [IX_Iratpld_Kulso_ID] ON [dbo].[EREC_PldIratPeldanyok]
(
	[UgyUgyirat_Id_Kulso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'IX_Iratpld_Sorszam')
CREATE NONCLUSTERED INDEX [IX_Iratpld_Sorszam] ON [dbo].[EREC_PldIratPeldanyok]
(
	[Sorszam] ASC,
	[FelhasznaloCsoport_Id_Orzo] ASC
)
INCLUDE ( 	[Id],
	[IraIrat_Id],
	[Allapot]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'IX_ModositasIdo')
CREATE NONCLUSTERED INDEX [IX_ModositasIdo] ON [dbo].[EREC_PldIratPeldanyok]
(
	[ModositasIdo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'KULDESMOD_I')
CREATE NONCLUSTERED INDEX [KULDESMOD_I] ON [dbo].[EREC_PldIratPeldanyok]
(
	[KuldesMod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]') AND name = N'TIRP_ALLAPOT')
CREATE NONCLUSTERED INDEX [TIRP_ALLAPOT] ON [dbo].[EREC_PldIratPeldanyok]
(
	[Allapot] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratpeldanyok_Ugyiratok_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]'))
ALTER TABLE [dbo].[EREC_PldIratPeldanyok]  WITH CHECK ADD  CONSTRAINT [EREC_PldIratpeldanyok_Ugyiratok_FK] FOREIGN KEY([UgyUgyirat_Id_Kulso])
REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldIratpeldanyok_Ugyiratok_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]'))
ALTER TABLE [dbo].[EREC_PldIratPeldanyok] CHECK CONSTRAINT [EREC_PldIratpeldanyok_Ugyiratok_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IRP_IRA_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]'))
ALTER TABLE [dbo].[EREC_PldIratPeldanyok]  WITH CHECK ADD  CONSTRAINT [IRP_IRA_FK] FOREIGN KEY([IraIrat_Id])
REFERENCES [dbo].[EREC_IraIratok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[IRP_IRA_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_PldIratPeldanyok]'))
ALTER TABLE [dbo].[EREC_PldIratPeldanyok] CHECK CONSTRAINT [IRP_IRA_FK]
GO
