IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KAPJAKMEG_PLD_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_PldKapjakMeg]'))
ALTER TABLE [dbo].[EREC_PldKapjakMeg] DROP CONSTRAINT [EREC_KAPJAKMEG_PLD_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KAPJAKMEG_CSOPORT_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_PldKapjakMeg]'))
ALTER TABLE [dbo].[EREC_PldKapjakMeg] DROP CONSTRAINT [EREC_KAPJAKMEG_CSOPORT_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_PldK__Letre__3118447E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_PldKapjakMeg] DROP CONSTRAINT [DF__EREC_PldK__Letre__3118447E]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_PldKapjakMeg_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_PldKapjakMeg] DROP CONSTRAINT [DF_EREC_PldKapjakMeg_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_PldK__ErvKe__2F2FFC0C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_PldKapjakMeg] DROP CONSTRAINT [DF__EREC_PldK__ErvKe__2F2FFC0C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_PldKap__Ver__2E3BD7D3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_PldKapjakMeg] DROP CONSTRAINT [DF__EREC_PldKap__Ver__2E3BD7D3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_PldKapj__Id__2D47B39A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_PldKapjakMeg] DROP CONSTRAINT [DF__EREC_PldKapj__Id__2D47B39A]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldKapjakMeg]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_PldKapjakMeg]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_PldKapjakMeg]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_PldKapjakMeg](
	[Id] [uniqueidentifier] NOT NULL,
	[PldIratPeldany_Id] [uniqueidentifier] NOT NULL,
	[Csoport_Id] [uniqueidentifier] NOT NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_PLDKAPJAKMEG] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_PldKapj__Id__2D47B39A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_PldKapjakMeg] ADD  CONSTRAINT [DF__EREC_PldKapj__Id__2D47B39A]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_PldKap__Ver__2E3BD7D3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_PldKapjakMeg] ADD  CONSTRAINT [DF__EREC_PldKap__Ver__2E3BD7D3]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_PldK__ErvKe__2F2FFC0C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_PldKapjakMeg] ADD  CONSTRAINT [DF__EREC_PldK__ErvKe__2F2FFC0C]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_PldKapjakMeg_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_PldKapjakMeg] ADD  CONSTRAINT [DF_EREC_PldKapjakMeg_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_PldK__Letre__3118447E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_PldKapjakMeg] ADD  CONSTRAINT [DF__EREC_PldK__Letre__3118447E]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KAPJAKMEG_CSOPORT_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_PldKapjakMeg]'))
ALTER TABLE [dbo].[EREC_PldKapjakMeg]  WITH CHECK ADD  CONSTRAINT [EREC_KAPJAKMEG_CSOPORT_FK] FOREIGN KEY([Csoport_Id])
REFERENCES [dbo].[KRT_Csoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KAPJAKMEG_CSOPORT_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_PldKapjakMeg]'))
ALTER TABLE [dbo].[EREC_PldKapjakMeg] CHECK CONSTRAINT [EREC_KAPJAKMEG_CSOPORT_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KAPJAKMEG_PLD_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_PldKapjakMeg]'))
ALTER TABLE [dbo].[EREC_PldKapjakMeg]  WITH CHECK ADD  CONSTRAINT [EREC_KAPJAKMEG_PLD_FK] FOREIGN KEY([PldIratPeldany_Id])
REFERENCES [dbo].[EREC_PldIratPeldanyok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KAPJAKMEG_PLD_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_PldKapjakMeg]'))
ALTER TABLE [dbo].[EREC_PldKapjakMeg] CHECK CONSTRAINT [EREC_KAPJAKMEG_PLD_FK]
GO
