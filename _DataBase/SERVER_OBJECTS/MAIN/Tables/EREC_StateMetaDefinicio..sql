IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_STATEMETA_OBJMETADEF_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_StateMetaDefinicio]'))
ALTER TABLE [dbo].[EREC_StateMetaDefinicio] DROP CONSTRAINT [EREC_STATEMETA_OBJMETADEF_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Stat__Letre__7953D99F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_StateMetaDefinicio] DROP CONSTRAINT [DF__EREC_Stat__Letre__7953D99F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Stat__ErvKe__785FB566]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_StateMetaDefinicio] DROP CONSTRAINT [DF__EREC_Stat__ErvKe__785FB566]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_StateM__Ver__776B912D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_StateMetaDefinicio] DROP CONSTRAINT [DF__EREC_StateM__Ver__776B912D]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_StateMe__Id__76776CF4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_StateMetaDefinicio] DROP CONSTRAINT [DF__EREC_StateMe__Id__76776CF4]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_StateMetaDefinicio]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_StateMetaDefinicio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_StateMetaDefinicio]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_StateMetaDefinicio](
	[Id] [uniqueidentifier] NOT NULL,
	[Obj_Metadefinicio_Id] [uniqueidentifier] NULL,
	[Obj_Id_StateColumn] [uniqueidentifier] NULL,
	[Obj_StateColumnType] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_STATEMETADEFINICIO] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_StateMe__Id__76776CF4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_StateMetaDefinicio] ADD  CONSTRAINT [DF__EREC_StateMe__Id__76776CF4]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_StateM__Ver__776B912D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_StateMetaDefinicio] ADD  CONSTRAINT [DF__EREC_StateM__Ver__776B912D]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Stat__ErvKe__785FB566]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_StateMetaDefinicio] ADD  CONSTRAINT [DF__EREC_Stat__ErvKe__785FB566]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Stat__Letre__7953D99F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_StateMetaDefinicio] ADD  CONSTRAINT [DF__EREC_Stat__Letre__7953D99F]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_STATEMETA_OBJMETADEF_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_StateMetaDefinicio]'))
ALTER TABLE [dbo].[EREC_StateMetaDefinicio]  WITH CHECK ADD  CONSTRAINT [EREC_STATEMETA_OBJMETADEF_FK] FOREIGN KEY([Obj_Metadefinicio_Id])
REFERENCES [dbo].[EREC_Obj_MetaDefinicio] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_STATEMETA_OBJMETADEF_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_StateMetaDefinicio]'))
ALTER TABLE [dbo].[EREC_StateMetaDefinicio] CHECK CONSTRAINT [EREC_STATEMETA_OBJMETADEF_FK]
GO
