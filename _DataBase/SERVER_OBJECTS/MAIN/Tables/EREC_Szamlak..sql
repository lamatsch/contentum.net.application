IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_SZAMLAK_KRT_PARTNEREK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]'))
ALTER TABLE [dbo].[EREC_Szamlak] DROP CONSTRAINT [FK_EREC_SZAMLAK_KRT_PARTNEREK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_SZAMLAK_KRT_DOKUMENTUMOK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]'))
ALTER TABLE [dbo].[EREC_Szamlak] DROP CONSTRAINT [FK_EREC_SZAMLAK_KRT_DOKUMENTUMOK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_SZAMLAK_KRT_CIMEK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]'))
ALTER TABLE [dbo].[EREC_Szamlak] DROP CONSTRAINT [FK_EREC_SZAMLAK_KRT_CIMEK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_SZAMLAK_KRT_BANKSZAMLASZAMOK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]'))
ALTER TABLE [dbo].[EREC_Szamlak] DROP CONSTRAINT [FK_EREC_SZAMLAK_KRT_BANKSZAMLASZAMOK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_SZAMLAK_EREC_KULDKULDEMENYEK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]'))
ALTER TABLE [dbo].[EREC_Szamlak] DROP CONSTRAINT [FK_EREC_SZAMLAK_EREC_KULDKULDEMENYEK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Szam__Letre__3AA1AEB8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Szamlak] DROP CONSTRAINT [DF__EREC_Szam__Letre__3AA1AEB8]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_Szamlak_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Szamlak] DROP CONSTRAINT [DF_EREC_Szamlak_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Szam__ErvKe__38B96646]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Szamlak] DROP CONSTRAINT [DF__EREC_Szam__ErvKe__38B96646]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Szamla__Ver__37C5420D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Szamlak] DROP CONSTRAINT [DF__EREC_Szamla__Ver__37C5420D]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Szam__Deviz__36D11DD4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Szamlak] DROP CONSTRAINT [DF__EREC_Szam__Deviz__36D11DD4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Szamlak__Id__35DCF99B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Szamlak] DROP CONSTRAINT [DF__EREC_Szamlak__Id__35DCF99B]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]') AND name = N'IX_Szamlak_KuldemenyId')
DROP INDEX [IX_Szamlak_KuldemenyId] ON [dbo].[EREC_Szamlak]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]') AND name = N'IX_Szamlak_DokumentumId')
DROP INDEX [IX_Szamlak_DokumentumId] ON [dbo].[EREC_Szamlak]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_Szamlak]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_Szamlak](
	[Id] [uniqueidentifier] NOT NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[Dokumentum_Id] [uniqueidentifier] NULL,
	[Partner_Id_Szallito] [uniqueidentifier] NOT NULL,
	[Cim_Id_Szallito] [uniqueidentifier] NULL,
	[Bankszamlaszam_Id] [uniqueidentifier] NULL,
	[SzamlaSorszam] [varchar](16) COLLATE Hungarian_CI_AS NOT NULL,
	[FizetesiMod] [varchar](4) COLLATE Hungarian_CI_AS NOT NULL,
	[TeljesitesDatuma] [smalldatetime] NULL,
	[BizonylatDatuma] [smalldatetime] NULL,
	[FizetesiHatarido] [smalldatetime] NULL,
	[DevizaKod] [varchar](3) COLLATE Hungarian_CI_AS NULL,
	[VisszakuldesDatuma] [smalldatetime] NULL,
	[EllenorzoOsszeg] [float] NOT NULL,
	[PIRAllapot] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[VevoBesorolas] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_EREC_SZAMLAK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]') AND name = N'IX_Szamlak_DokumentumId')
CREATE NONCLUSTERED INDEX [IX_Szamlak_DokumentumId] ON [dbo].[EREC_Szamlak]
(
	[Dokumentum_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]') AND name = N'IX_Szamlak_KuldemenyId')
CREATE NONCLUSTERED INDEX [IX_Szamlak_KuldemenyId] ON [dbo].[EREC_Szamlak]
(
	[KuldKuldemeny_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Szamlak__Id__35DCF99B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Szamlak] ADD  CONSTRAINT [DF__EREC_Szamlak__Id__35DCF99B]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Szam__Deviz__36D11DD4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Szamlak] ADD  CONSTRAINT [DF__EREC_Szam__Deviz__36D11DD4]  DEFAULT ('HUF') FOR [DevizaKod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Szamla__Ver__37C5420D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Szamlak] ADD  CONSTRAINT [DF__EREC_Szamla__Ver__37C5420D]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Szam__ErvKe__38B96646]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Szamlak] ADD  CONSTRAINT [DF__EREC_Szam__ErvKe__38B96646]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_Szamlak_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Szamlak] ADD  CONSTRAINT [DF_EREC_Szamlak_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Szam__Letre__3AA1AEB8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_Szamlak] ADD  CONSTRAINT [DF__EREC_Szam__Letre__3AA1AEB8]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_SZAMLAK_EREC_KULDKULDEMENYEK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]'))
ALTER TABLE [dbo].[EREC_Szamlak]  WITH CHECK ADD  CONSTRAINT [FK_EREC_SZAMLAK_EREC_KULDKULDEMENYEK] FOREIGN KEY([KuldKuldemeny_Id])
REFERENCES [dbo].[EREC_KuldKuldemenyek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_SZAMLAK_EREC_KULDKULDEMENYEK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]'))
ALTER TABLE [dbo].[EREC_Szamlak] CHECK CONSTRAINT [FK_EREC_SZAMLAK_EREC_KULDKULDEMENYEK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_SZAMLAK_KRT_BANKSZAMLASZAMOK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]'))
ALTER TABLE [dbo].[EREC_Szamlak]  WITH CHECK ADD  CONSTRAINT [FK_EREC_SZAMLAK_KRT_BANKSZAMLASZAMOK] FOREIGN KEY([Bankszamlaszam_Id])
REFERENCES [dbo].[KRT_Bankszamlaszamok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_SZAMLAK_KRT_BANKSZAMLASZAMOK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]'))
ALTER TABLE [dbo].[EREC_Szamlak] CHECK CONSTRAINT [FK_EREC_SZAMLAK_KRT_BANKSZAMLASZAMOK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_SZAMLAK_KRT_CIMEK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]'))
ALTER TABLE [dbo].[EREC_Szamlak]  WITH CHECK ADD  CONSTRAINT [FK_EREC_SZAMLAK_KRT_CIMEK] FOREIGN KEY([Cim_Id_Szallito])
REFERENCES [dbo].[KRT_Cimek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_SZAMLAK_KRT_CIMEK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]'))
ALTER TABLE [dbo].[EREC_Szamlak] CHECK CONSTRAINT [FK_EREC_SZAMLAK_KRT_CIMEK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_SZAMLAK_KRT_DOKUMENTUMOK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]'))
ALTER TABLE [dbo].[EREC_Szamlak]  WITH CHECK ADD  CONSTRAINT [FK_EREC_SZAMLAK_KRT_DOKUMENTUMOK] FOREIGN KEY([Dokumentum_Id])
REFERENCES [dbo].[KRT_Dokumentumok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_SZAMLAK_KRT_DOKUMENTUMOK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]'))
ALTER TABLE [dbo].[EREC_Szamlak] CHECK CONSTRAINT [FK_EREC_SZAMLAK_KRT_DOKUMENTUMOK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_SZAMLAK_KRT_PARTNEREK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]'))
ALTER TABLE [dbo].[EREC_Szamlak]  WITH CHECK ADD  CONSTRAINT [FK_EREC_SZAMLAK_KRT_PARTNEREK] FOREIGN KEY([Partner_Id_Szallito])
REFERENCES [dbo].[KRT_Partnerek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_EREC_SZAMLAK_KRT_PARTNEREK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_Szamlak]'))
ALTER TABLE [dbo].[EREC_Szamlak] CHECK CONSTRAINT [FK_EREC_SZAMLAK_KRT_PARTNEREK]
GO
