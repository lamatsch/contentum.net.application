IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Szignalas_Iratmeta_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekek]'))
ALTER TABLE [dbo].[EREC_SzignalasiJegyzekek] DROP CONSTRAINT [Szignalas_Iratmeta_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Szagnalas_Csoport_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekek]'))
ALTER TABLE [dbo].[EREC_SzignalasiJegyzekek] DROP CONSTRAINT [Szagnalas_Csoport_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekek]') AND name = N'Index_2')
DROP INDEX [Index_2] ON [dbo].[EREC_SzignalasiJegyzekek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekek]') AND name = N'Index_1')
DROP INDEX [Index_1] ON [dbo].[EREC_SzignalasiJegyzekek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_SzignalasiJegyzekek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_SzignalasiJegyzekek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Szignal__Id__3D7E1B63]  DEFAULT (newsequentialid()),
	[UgykorTargykor_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[SzignalasTipusa] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[SzignalasiKotelezettseg] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_Szigna__Ver__3E723F9C]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Szig__ErvKe__3F6663D5]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_SzignalasiJegyzekek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Szig__Letre__414EAC47]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_SZIGNALASIJEGYZEKEK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekek]') AND name = N'Index_1')
CREATE NONCLUSTERED INDEX [Index_1] ON [dbo].[EREC_SzignalasiJegyzekek]
(
	[Csoport_Id_Felelos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekek]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[EREC_SzignalasiJegyzekek]
(
	[UgykorTargykor_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Szagnalas_Csoport_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekek]'))
ALTER TABLE [dbo].[EREC_SzignalasiJegyzekek]  WITH CHECK ADD  CONSTRAINT [Szagnalas_Csoport_FK] FOREIGN KEY([Csoport_Id_Felelos])
REFERENCES [dbo].[KRT_Csoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Szagnalas_Csoport_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekek]'))
ALTER TABLE [dbo].[EREC_SzignalasiJegyzekek] CHECK CONSTRAINT [Szagnalas_Csoport_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Szignalas_Iratmeta_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekek]'))
ALTER TABLE [dbo].[EREC_SzignalasiJegyzekek]  WITH CHECK ADD  CONSTRAINT [Szignalas_Iratmeta_FK] FOREIGN KEY([UgykorTargykor_Id])
REFERENCES [dbo].[EREC_IratMetaDefinicio] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Szignalas_Iratmeta_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_SzignalasiJegyzekek]'))
ALTER TABLE [dbo].[EREC_SzignalasiJegyzekek] CHECK CONSTRAINT [Szignalas_Iratmeta_FK]
GO
