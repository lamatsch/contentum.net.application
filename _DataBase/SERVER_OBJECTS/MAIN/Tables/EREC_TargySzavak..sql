IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Targyszavak_Csoport_Tulaj_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TargySzavak]'))
ALTER TABLE [dbo].[EREC_TargySzavak] DROP CONSTRAINT [Targyszavak_Csoport_Tulaj_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TargySzavak]') AND name = N'TRSZ_PRT_FK_I')
DROP INDEX [TRSZ_PRT_FK_I] ON [dbo].[EREC_TargySzavak]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TargySzavak]') AND name = N'EREC_TargySzavak_UK')
DROP INDEX [EREC_TargySzavak_UK] ON [dbo].[EREC_TargySzavak]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TargySzavak]') AND name = N'EREC_BelsoAzonosito_UK')
DROP INDEX [EREC_BelsoAzonosito_UK] ON [dbo].[EREC_TargySzavak]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TargySzavak]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_TargySzavak]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TargySzavak]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_TargySzavak](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_TargySz__Id__442B18F2]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NULL,
	[Tipus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[TargySzavak] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[AlapertelmezettErtek] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[BelsoAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SPSSzinkronizalt] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__EREC_Targ__SPSSz__451F3D2B]  DEFAULT ('0'),
	[SPS_Field_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NULL,
	[RegExp] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ToolTip] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[ControlTypeSource] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[ControlTypeDataSource] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[XSD] [xml] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_TargyS__Ver__46136164]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Targ__ErvKe__4707859D]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_TargySzavak_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Targ__Letre__48EFCE0F]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [TRSZ_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TargySzavak]') AND name = N'EREC_BelsoAzonosito_UK')
CREATE NONCLUSTERED INDEX [EREC_BelsoAzonosito_UK] ON [dbo].[EREC_TargySzavak]
(
	[Org] ASC,
	[BelsoAzonosito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TargySzavak]') AND name = N'EREC_TargySzavak_UK')
CREATE NONCLUSTERED INDEX [EREC_TargySzavak_UK] ON [dbo].[EREC_TargySzavak]
(
	[Org] ASC,
	[TargySzavak] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TargySzavak]') AND name = N'TRSZ_PRT_FK_I')
CREATE NONCLUSTERED INDEX [TRSZ_PRT_FK_I] ON [dbo].[EREC_TargySzavak]
(
	[Csoport_Id_Tulaj] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Targyszavak_Csoport_Tulaj_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TargySzavak]'))
ALTER TABLE [dbo].[EREC_TargySzavak]  WITH CHECK ADD  CONSTRAINT [Targyszavak_Csoport_Tulaj_FK] FOREIGN KEY([Csoport_Id_Tulaj])
REFERENCES [dbo].[KRT_Csoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Targyszavak_Csoport_Tulaj_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TargySzavak]'))
ALTER TABLE [dbo].[EREC_TargySzavak] CHECK CONSTRAINT [Targyszavak_Csoport_Tulaj_FK]
GO
