IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_Ugytipus]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] DROP CONSTRAINT [fk_Tomeges_Ugytipus]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_Irattipus]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] DROP CONSTRAINT [fk_Tomeges_Irattipus]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_IrattariTetelek]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] DROP CONSTRAINT [fk_Tomeges_IrattariTetelek]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_HelyettesCsoport]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] DROP CONSTRAINT [fk_Tomeges_HelyettesCsoport]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_FelelosCsoport]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] DROP CONSTRAINT [fk_Tomeges_FelelosCsoport]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_AlairoCsoport]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] DROP CONSTRAINT [fk_Tomeges_AlairoCsoport]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_AlairasSzabaly]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] DROP CONSTRAINT [fk_Tomeges_AlairasSzabaly]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_AlairasMod]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] DROP CONSTRAINT [fk_Tomeges_AlairasMod]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_Agazat]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] DROP CONSTRAINT [fk_Tomeges_Agazat]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_TomegesIktatas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_TomegesIktatas](
	[Id] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid()),
	[ForrasTipusNev] [nvarchar](4000) COLLATE Hungarian_CI_AS NOT NULL,
	[FelelosCsoport_Id] [uniqueidentifier] NOT NULL,
	[AgazatiJelek_Id] [uniqueidentifier] NOT NULL,
	[IraIrattariTetelek_Id] [uniqueidentifier] NOT NULL,
	[Ugytipus_Id] [uniqueidentifier] NOT NULL,
	[Irattipus_Id] [uniqueidentifier] NOT NULL,
	[TargyPrefix] [nvarchar](4000) COLLATE Hungarian_CI_AS NOT NULL,
	[AlairasKell] [char](1) COLLATE Hungarian_CI_AS NULL DEFAULT ((0)),
	[AlairasMod] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Alairo] [uniqueidentifier] NULL,
	[FelhaszCsoport_Id_Helyettesito] [uniqueidentifier] NULL,
	[AlairasSzabaly_Id] [uniqueidentifier] NULL,
	[HatosagiAdatlapKell] [bit] NULL DEFAULT ((0)),
	[UgyFajtaja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[DontestHozta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[DontesFormaja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[UgyintezesHataridore] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[HataridoTullepes] [int] NULL,
	[HatosagiEllenorzes] [char](1) COLLATE Hungarian_CI_AS NULL,
	[MunkaorakSzama] [float] NULL,
	[EljarasiKoltseg] [int] NULL,
	[KozigazgatasiBirsagMerteke] [int] NULL,
	[SommasEljDontes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[NyolcNapBelulNemSommas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoHatalyuHatarozat] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoHatalyuVegzes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[HatAltalVisszafizOsszeg] [int] NULL,
	[HatTerheloEljKtsg] [int] NULL,
	[FelfuggHatarozat] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_Tomeges_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [Tomeges_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_Agazat]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas]  WITH CHECK ADD  CONSTRAINT [fk_Tomeges_Agazat] FOREIGN KEY([AgazatiJelek_Id])
REFERENCES [dbo].[EREC_AgazatiJelek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_Agazat]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] CHECK CONSTRAINT [fk_Tomeges_Agazat]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_AlairasMod]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas]  WITH CHECK ADD  CONSTRAINT [fk_Tomeges_AlairasMod] FOREIGN KEY([AlairasMod])
REFERENCES [dbo].[KRT_AlairasTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_AlairasMod]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] CHECK CONSTRAINT [fk_Tomeges_AlairasMod]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_AlairasSzabaly]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas]  WITH CHECK ADD  CONSTRAINT [fk_Tomeges_AlairasSzabaly] FOREIGN KEY([AlairasSzabaly_Id])
REFERENCES [dbo].[KRT_AlairasSzabalyok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_AlairasSzabaly]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] CHECK CONSTRAINT [fk_Tomeges_AlairasSzabaly]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_AlairoCsoport]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas]  WITH CHECK ADD  CONSTRAINT [fk_Tomeges_AlairoCsoport] FOREIGN KEY([FelhasznaloCsoport_Id_Alairo])
REFERENCES [dbo].[KRT_Csoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_AlairoCsoport]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] CHECK CONSTRAINT [fk_Tomeges_AlairoCsoport]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_FelelosCsoport]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas]  WITH CHECK ADD  CONSTRAINT [fk_Tomeges_FelelosCsoport] FOREIGN KEY([FelelosCsoport_Id])
REFERENCES [dbo].[KRT_Csoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_FelelosCsoport]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] CHECK CONSTRAINT [fk_Tomeges_FelelosCsoport]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_HelyettesCsoport]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas]  WITH CHECK ADD  CONSTRAINT [fk_Tomeges_HelyettesCsoport] FOREIGN KEY([FelhaszCsoport_Id_Helyettesito])
REFERENCES [dbo].[KRT_Csoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_HelyettesCsoport]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] CHECK CONSTRAINT [fk_Tomeges_HelyettesCsoport]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_IrattariTetelek]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas]  WITH CHECK ADD  CONSTRAINT [fk_Tomeges_IrattariTetelek] FOREIGN KEY([IraIrattariTetelek_Id])
REFERENCES [dbo].[EREC_IraIrattariTetelek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_IrattariTetelek]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] CHECK CONSTRAINT [fk_Tomeges_IrattariTetelek]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_Irattipus]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas]  WITH CHECK ADD  CONSTRAINT [fk_Tomeges_Irattipus] FOREIGN KEY([Irattipus_Id])
REFERENCES [dbo].[KRT_KodTarak] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_Irattipus]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] CHECK CONSTRAINT [fk_Tomeges_Irattipus]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_Ugytipus]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas]  WITH CHECK ADD  CONSTRAINT [fk_Tomeges_Ugytipus] FOREIGN KEY([Ugytipus_Id])
REFERENCES [dbo].[EREC_IratMetaDefinicio] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_Tomeges_Ugytipus]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatas]'))
ALTER TABLE [dbo].[EREC_TomegesIktatas] CHECK CONSTRAINT [fk_Tomeges_Ugytipus]
GO
