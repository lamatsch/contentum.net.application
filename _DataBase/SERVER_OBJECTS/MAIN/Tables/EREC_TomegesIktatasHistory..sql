IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatasHistory]') AND name = N'IX_EREC_TomegesIktatasHistory_ID_VER')
DROP INDEX [IX_EREC_TomegesIktatasHistory_ID_VER] ON [dbo].[EREC_TomegesIktatasHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatasHistory]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_TomegesIktatasHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatasHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_TomegesIktatasHistory](
	[HistoryId] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid()),
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[ForrasTipusNev] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[FelelosCsoport_Id] [uniqueidentifier] NULL,
	[AgazatiJelek_Id] [uniqueidentifier] NULL,
	[IraIrattariTetelek_Id] [uniqueidentifier] NULL,
	[Ugytipus_Id] [uniqueidentifier] NULL,
	[Irattipus_Id] [uniqueidentifier] NULL,
	[TargyPrefix] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[AlairasKell] [char](1) COLLATE Hungarian_CI_AS NULL,
	[AlairasMod] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Alairo] [uniqueidentifier] NULL,
	[FelhaszCsoport_Id_Helyettesito] [uniqueidentifier] NULL,
	[AlairasSzabaly_Id] [uniqueidentifier] NULL,
	[HatosagiAdatlapKell] [char](1) COLLATE Hungarian_CI_AS NULL,
	[UgyFajtaja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[DontestHozta] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[DontesFormaja] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[UgyintezesHataridore] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[HataridoTullepes] [int] NULL,
	[HatosagiEllenorzes] [char](1) COLLATE Hungarian_CI_AS NULL,
	[MunkaorakSzama] [float] NULL,
	[EljarasiKoltseg] [int] NULL,
	[KozigazgatasiBirsagMerteke] [int] NULL,
	[SommasEljDontes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[NyolcNapBelulNemSommas] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoHatalyuHatarozat] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[FuggoHatalyuVegzes] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[HatAltalVisszafizOsszeg] [int] NULL,
	[HatTerheloEljKtsg] [int] NULL,
	[FelfuggHatarozat] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_TomegesIktatasHistory]') AND name = N'IX_EREC_TomegesIktatasHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_EREC_TomegesIktatasHistory_ID_VER] ON [dbo].[EREC_TomegesIktatasHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
