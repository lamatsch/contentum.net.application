IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KUF_EIV_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyKezFeljegyzesek]'))
ALTER TABLE [dbo].[EREC_UgyKezFeljegyzesek] DROP CONSTRAINT [KUF_EIV_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_UgyK__Letre__5832119F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyKezFeljegyzesek] DROP CONSTRAINT [DF__EREC_UgyK__Letre__5832119F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_UgyKezFeljegyzesek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyKezFeljegyzesek] DROP CONSTRAINT [DF_EREC_UgyKezFeljegyzesek_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_UgyK__ErvKe__5649C92D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyKezFeljegyzesek] DROP CONSTRAINT [DF__EREC_UgyK__ErvKe__5649C92D]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_UgyKez__Ver__5555A4F4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyKezFeljegyzesek] DROP CONSTRAINT [DF__EREC_UgyKez__Ver__5555A4F4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_UgyKezF__Id__546180BB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyKezFeljegyzesek] DROP CONSTRAINT [DF__EREC_UgyKezF__Id__546180BB]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyKezFeljegyzesek]') AND name = N'TKUF_KEZELESI_TIPUS_I')
DROP INDEX [TKUF_KEZELESI_TIPUS_I] ON [dbo].[EREC_UgyKezFeljegyzesek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyKezFeljegyzesek]') AND name = N'Letrehozo_I')
DROP INDEX [Letrehozo_I] ON [dbo].[EREC_UgyKezFeljegyzesek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyKezFeljegyzesek]') AND name = N'FK_UgyUgyirat_Id')
DROP INDEX [FK_UgyUgyirat_Id] ON [dbo].[EREC_UgyKezFeljegyzesek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyKezFeljegyzesek]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_UgyKezFeljegyzesek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyKezFeljegyzesek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_UgyKezFeljegyzesek](
	[Id] [uniqueidentifier] NOT NULL,
	[UgyUgyirat_Id] [uniqueidentifier] NOT NULL,
	[KezelesTipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KUF_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyKezFeljegyzesek]') AND name = N'FK_UgyUgyirat_Id')
CREATE NONCLUSTERED INDEX [FK_UgyUgyirat_Id] ON [dbo].[EREC_UgyKezFeljegyzesek]
(
	[UgyUgyirat_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyKezFeljegyzesek]') AND name = N'Letrehozo_I')
CREATE NONCLUSTERED INDEX [Letrehozo_I] ON [dbo].[EREC_UgyKezFeljegyzesek]
(
	[Letrehozo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyKezFeljegyzesek]') AND name = N'TKUF_KEZELESI_TIPUS_I')
CREATE NONCLUSTERED INDEX [TKUF_KEZELESI_TIPUS_I] ON [dbo].[EREC_UgyKezFeljegyzesek]
(
	[KezelesTipus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_UgyKezF__Id__546180BB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyKezFeljegyzesek] ADD  CONSTRAINT [DF__EREC_UgyKezF__Id__546180BB]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_UgyKez__Ver__5555A4F4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyKezFeljegyzesek] ADD  CONSTRAINT [DF__EREC_UgyKez__Ver__5555A4F4]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_UgyK__ErvKe__5649C92D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyKezFeljegyzesek] ADD  CONSTRAINT [DF__EREC_UgyK__ErvKe__5649C92D]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_UgyKezFeljegyzesek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyKezFeljegyzesek] ADD  CONSTRAINT [DF_EREC_UgyKezFeljegyzesek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_UgyK__Letre__5832119F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyKezFeljegyzesek] ADD  CONSTRAINT [DF__EREC_UgyK__Letre__5832119F]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KUF_EIV_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyKezFeljegyzesek]'))
ALTER TABLE [dbo].[EREC_UgyKezFeljegyzesek]  WITH CHECK ADD  CONSTRAINT [KUF_EIV_FK] FOREIGN KEY([UgyUgyirat_Id])
REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KUF_EIV_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyKezFeljegyzesek]'))
ALTER TABLE [dbo].[EREC_UgyKezFeljegyzesek] CHECK CONSTRAINT [KUF_EIV_FK]
GO
