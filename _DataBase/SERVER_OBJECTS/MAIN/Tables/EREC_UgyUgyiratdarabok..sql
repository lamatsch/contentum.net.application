IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[UgyUgyiratDarab_IraIktatokonyv_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratdarabok] DROP CONSTRAINT [UgyUgyiratDarab_IraIktatokonyv_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[UgyiratDarab_Ugy_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratdarabok] DROP CONSTRAINT [UgyiratDarab_Ugy_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabok]') AND name = N'IND_UDB_UGY_ID')
DROP INDEX [IND_UDB_UGY_ID] ON [dbo].[EREC_UgyUgyiratdarabok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabok]') AND name = N'I_EljarasiSzakasz_UK')
DROP INDEX [I_EljarasiSzakasz_UK] ON [dbo].[EREC_UgyUgyiratdarabok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabok]') AND name = N'FK_IktatokonyvId')
DROP INDEX [FK_IktatokonyvId] ON [dbo].[EREC_UgyUgyiratdarabok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_UgyUgyiratdarabok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_UgyUgyiratdarabok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_UgyUgyi__Id__5B0E7E4A]  DEFAULT (newsequentialid()),
	[UgyUgyirat_Id] [uniqueidentifier] NOT NULL,
	[EljarasiSzakasz] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Sorszam] [int] NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Hatarido] [datetime] NULL,
	[FelhasznaloCsoport_Id_Ugyintez] [uniqueidentifier] NULL,
	[ElintezesDat] [datetime] NULL,
	[LezarasDat] [datetime] NULL,
	[ElintezesMod] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[LezarasOka] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Leiras] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[UgyUgyirat_Id_Elozo] [uniqueidentifier] NULL,
	[IraIktatokonyv_Id] [uniqueidentifier] NULL,
	[Foszam] [int] NULL,
	[UtolsoAlszam] [int] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos_Elozo] [uniqueidentifier] NULL,
	[IratMetadefinicio_Id] [uniqueidentifier] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_UgyUgy__Ver__5C02A283]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_UgyU__ErvKe__5CF6C6BC]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_UgyUgyiratdarabok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_UgyU__Letre__5EDF0F2E]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KIRL_UGY_UGYIRATDARABOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabok]') AND name = N'FK_IktatokonyvId')
CREATE NONCLUSTERED INDEX [FK_IktatokonyvId] ON [dbo].[EREC_UgyUgyiratdarabok]
(
	[IraIktatokonyv_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabok]') AND name = N'I_EljarasiSzakasz_UK')
CREATE NONCLUSTERED INDEX [I_EljarasiSzakasz_UK] ON [dbo].[EREC_UgyUgyiratdarabok]
(
	[UgyUgyirat_Id] ASC,
	[LetrehozasIdo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabok]') AND name = N'IND_UDB_UGY_ID')
CREATE NONCLUSTERED INDEX [IND_UDB_UGY_ID] ON [dbo].[EREC_UgyUgyiratdarabok]
(
	[UgyUgyirat_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[UgyiratDarab_Ugy_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratdarabok]  WITH CHECK ADD  CONSTRAINT [UgyiratDarab_Ugy_FK] FOREIGN KEY([UgyUgyirat_Id])
REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[UgyiratDarab_Ugy_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratdarabok] CHECK CONSTRAINT [UgyiratDarab_Ugy_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[UgyUgyiratDarab_IraIktatokonyv_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratdarabok]  WITH CHECK ADD  CONSTRAINT [UgyUgyiratDarab_IraIktatokonyv_FK] FOREIGN KEY([IraIktatokonyv_Id])
REFERENCES [dbo].[EREC_IraIktatoKonyvek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[UgyUgyiratDarab_IraIktatokonyv_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratdarabok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratdarabok] CHECK CONSTRAINT [UgyUgyiratDarab_IraIktatokonyv_FK]
GO
