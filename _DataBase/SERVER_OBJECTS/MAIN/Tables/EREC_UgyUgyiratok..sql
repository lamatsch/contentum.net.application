IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[UgyUgyirat_IraIktatokonyv_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratok] DROP CONSTRAINT [UgyUgyirat_IraIktatokonyv_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Ugy_Csoport_Felelos_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratok] DROP CONSTRAINT [Ugy_Csoport_Felelos_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Ugyirat_UgyiratKulso_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratok] DROP CONSTRAINT [EREC_Ugyirat_UgyiratKulso_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EIV_EIV_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratok] DROP CONSTRAINT [EIV_EIV_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'UgyUgyirat_LetrehozasIdo_I')
DROP INDEX [UgyUgyirat_LetrehozasIdo_I] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'TEIV_SURGOSSEG_I')
DROP INDEX [TEIV_SURGOSSEG_I] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'TEIV_SKONTROBADATUM_I')
DROP INDEX [TEIV_SKONTROBADATUM_I] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'TEIV_PRT_ID_ORZO_I')
DROP INDEX [TEIV_PRT_ID_ORZO_I] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'TEIV_KATEGORIA')
DROP INDEX [TEIV_KATEGORIA] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'TEIV_IRATTARI_HELY')
DROP INDEX [TEIV_IRATTARI_HELY] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'TEIV_HATARIDO_I')
DROP INDEX [TEIV_HATARIDO_I] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'IX_Ugyirat_Kulso_ID')
DROP INDEX [IX_Ugyirat_Kulso_ID] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'IX_Ugyirat_CimSTR_Ugyindito')
DROP INDEX [IX_Ugyirat_CimSTR_Ugyindito] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'IX_Ugyirat_Cim_Id_Ugyindito')
DROP INDEX [IX_Ugyirat_Cim_Id_Ugyindito] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'IX_Ugyinteto_Id')
DROP INDEX [IX_Ugyinteto_Id] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'IX_ModositasIdo_Ugyirat')
DROP INDEX [IX_ModositasIdo_Ugyirat] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'IX_Foszam')
DROP INDEX [IX_Foszam] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'I_IRATTARI_TETELSZAM')
DROP INDEX [I_IRATTARI_TETELSZAM] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'I_barkod')
DROP INDEX [I_barkod] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'FK_UgyUgyirat_Id_Szulo')
DROP INDEX [FK_UgyUgyirat_Id_Szulo] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'FK_Csoport_Id_Felelos')
DROP INDEX [FK_Csoport_Id_Felelos] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'EIV_FOSZAM_UK')
DROP INDEX [EIV_FOSZAM_UK] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'ALLAPOT_I')
DROP INDEX [ALLAPOT_I] ON [dbo].[EREC_UgyUgyiratok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_UgyUgyiratok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_UgyUgyiratok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_UgyUgyi__Id__61BB7BD9]  DEFAULT (newsequentialid()),
	[Foszam] [int] NULL,
	[Sorszam] [int] NULL,
	[Ugyazonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Alkalmazas_Id] [uniqueidentifier] NULL,
	[UgyintezesModja] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Hatarido] [datetime] NULL,
	[SkontrobaDat] [datetime] NULL,
	[LezarasDat] [datetime] NULL,
	[IrattarbaKuldDatuma] [datetime] NULL,
	[IrattarbaVetelDat] [datetime] NULL,
	[FelhCsoport_Id_IrattariAtvevo] [uniqueidentifier] NULL,
	[SelejtezesDat] [datetime] NULL,
	[FelhCsoport_Id_Selejtezo] [uniqueidentifier] NULL,
	[LeveltariAtvevoNeve] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[FelhCsoport_Id_Felulvizsgalo] [uniqueidentifier] NULL,
	[FelulvizsgalatDat] [datetime] NULL,
	[IktatoszamKieg] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[Targy] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[UgyUgyirat_Id_Szulo] [uniqueidentifier] NULL,
	[UgyUgyirat_Id_Kulso] [uniqueidentifier] NULL,
	[UgyTipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[IrattariHely] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SztornirozasDat] [datetime] NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[FelhasznaloCsoport_Id_Orzo] [uniqueidentifier] NULL,
	[Csoport_Id_Cimzett] [uniqueidentifier] NULL,
	[Jelleg] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[IraIrattariTetel_Id] [uniqueidentifier] NULL,
	[IraIktatokonyv_Id] [uniqueidentifier] NULL,
	[SkontroOka] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SkontroVege] [datetime] NULL,
	[Surgosseg] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[SkontrobanOsszesen] [int] NULL,
	[MegorzesiIdoVege] [datetime] NULL CONSTRAINT [DF_EREC_UgyUgyiratok_MegorzesiIdoVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Partner_Id_Ugyindito] [uniqueidentifier] NULL,
	[NevSTR_Ugyindito] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[ElintezesDat] [datetime] NULL,
	[FelhasznaloCsoport_Id_Ugyintez] [uniqueidentifier] NULL,
	[Csoport_Id_Felelos_Elozo] [uniqueidentifier] NULL,
	[KolcsonKikerDat] [datetime] NULL CONSTRAINT [DF__EREC_UgyU__Kolcs__63A3C44B]  DEFAULT (NULL),
	[KolcsonKiadDat] [datetime] NULL CONSTRAINT [DF__EREC_UgyU__Kolcs__6497E884]  DEFAULT (NULL),
	[Kolcsonhatarido] [datetime] NULL CONSTRAINT [DF__EREC_UgyU__Kolcs__658C0CBD]  DEFAULT (NULL),
	[BARCODE] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[IratMetadefinicio_Id] [uniqueidentifier] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[TovabbitasAlattAllapot] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Azonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Fizikai_Kezbesitesi_Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Kovetkezo_Orzo_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Ugyfelelos] [uniqueidentifier] NULL,
	[Elektronikus_Kezbesitesi_Allap] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Kovetkezo_Felelos_Id] [uniqueidentifier] NULL,
	[UtolsoAlszam] [int] NULL,
	[UtolsoSorszam] [int] NULL,
	[IratSzam] [int] NULL,
	[ElintezesMod] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[RegirendszerIktatoszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[GeneraltTargy] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Cim_Id_Ugyindito] [uniqueidentifier] NULL,
	[CimSTR_Ugyindito] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ugy_Fajtaja] nvarchar(64) NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_UgyUgy__Ver__668030F6]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_UgyU__ErvKe__6774552F]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_UgyUgyiratok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_UgyU__Letre__695C9DA1]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[UjOrzesiIdo] [int] NULL,
	[IrattarId] [uniqueidentifier] NULL,
	[LezarasOka] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[SkontroOka_Kod] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[AKTIV]  AS (case when [ALLAPOT]='90' then (0) else (1) end) PERSISTED NOT NULL,
 CONSTRAINT [EIV_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'ALLAPOT_I')
CREATE NONCLUSTERED INDEX [ALLAPOT_I] ON [dbo].[EREC_UgyUgyiratok]
(
	[Allapot] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC,
	[ModositasIdo] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'EIV_FOSZAM_UK')
CREATE NONCLUSTERED INDEX [EIV_FOSZAM_UK] ON [dbo].[EREC_UgyUgyiratok]
(
	[IraIktatokonyv_Id] ASC,
	[Foszam] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'FK_Csoport_Id_Felelos')
CREATE NONCLUSTERED INDEX [FK_Csoport_Id_Felelos] ON [dbo].[EREC_UgyUgyiratok]
(
	[Csoport_Id_Felelos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'FK_UgyUgyirat_Id_Szulo')
CREATE NONCLUSTERED INDEX [FK_UgyUgyirat_Id_Szulo] ON [dbo].[EREC_UgyUgyiratok]
(
	[UgyUgyirat_Id_Szulo] ASC
)
INCLUDE ( 	[Id],
	[Foszam],
	[Ugyazonosito],
	[Alkalmazas_Id],
	[UgyintezesModja],
	[Hatarido],
	[SkontrobaDat],
	[LezarasDat],
	[IrattarbaKuldDatuma],
	[IrattarbaVetelDat],
	[FelhCsoport_Id_IrattariAtvevo],
	[SelejtezesDat],
	[FelhCsoport_Id_Selejtezo],
	[LeveltariAtvevoNeve],
	[FelhCsoport_Id_Felulvizsgalo],
	[FelulvizsgalatDat],
	[IktatoszamKieg],
	[Targy],
	[UgyUgyirat_Id_Kulso],
	[UgyTipus],
	[IrattariHely],
	[SztornirozasDat],
	[Csoport_Id_Felelos],
	[FelhasznaloCsoport_Id_Orzo],
	[Csoport_Id_Cimzett],
	[Jelleg],
	[IraIrattariTetel_Id],
	[IraIktatokonyv_Id],
	[SkontroOka],
	[SkontroVege],
	[Surgosseg],
	[SkontrobanOsszesen],
	[MegorzesiIdoVege],
	[Partner_Id_Ugyindito],
	[NevSTR_Ugyindito],
	[ElintezesDat],
	[FelhasznaloCsoport_Id_Ugyintez],
	[Csoport_Id_Felelos_Elozo],
	[KolcsonKikerDat],
	[KolcsonKiadDat],
	[Kolcsonhatarido],
	[BARCODE],
	[IratMetadefinicio_Id],
	[Allapot],
	[TovabbitasAlattAllapot],
	[Megjegyzes],
	[Azonosito],
	[Fizikai_Kezbesitesi_Allapot],
	[Kovetkezo_Orzo_Id],
	[Csoport_Id_Ugyfelelos],
	[Elektronikus_Kezbesitesi_Allap],
	[Kovetkezo_Felelos_Id],
	[UtolsoAlszam],
	[IratSzam],
	[ElintezesMod],
	[RegirendszerIktatoszam],
	[GeneraltTargy],
	[Cim_Id_Ugyindito],
	[CimSTR_Ugyindito],
	[Ver],
	[Note],
	[Stat_id],
	[ErvKezd],
	[ErvVege],
	[Letrehozo_id],
	[LetrehozasIdo],
	[Modosito_id],
	[ModositasIdo],
	[Zarolo_id],
	[ZarolasIdo],
	[Tranz_id],
	[UIAccessLog_id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'I_barkod')
CREATE NONCLUSTERED INDEX [I_barkod] ON [dbo].[EREC_UgyUgyiratok]
(
	[BARCODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'I_IRATTARI_TETELSZAM')
CREATE NONCLUSTERED INDEX [I_IRATTARI_TETELSZAM] ON [dbo].[EREC_UgyUgyiratok]
(
	[IraIrattariTetel_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'IX_Foszam')
CREATE NONCLUSTERED INDEX [IX_Foszam] ON [dbo].[EREC_UgyUgyiratok]
(
	[Foszam] ASC,
	[Allapot] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'IX_ModositasIdo_Ugyirat')
CREATE NONCLUSTERED INDEX [IX_ModositasIdo_Ugyirat] ON [dbo].[EREC_UgyUgyiratok]
(
	[ModositasIdo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'IX_Ugyinteto_Id')
CREATE NONCLUSTERED INDEX [IX_Ugyinteto_Id] ON [dbo].[EREC_UgyUgyiratok]
(
	[FelhasznaloCsoport_Id_Ugyintez] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'IX_Ugyirat_Cim_Id_Ugyindito')
CREATE NONCLUSTERED INDEX [IX_Ugyirat_Cim_Id_Ugyindito] ON [dbo].[EREC_UgyUgyiratok]
(
	[Cim_Id_Ugyindito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'IX_Ugyirat_CimSTR_Ugyindito')
CREATE NONCLUSTERED INDEX [IX_Ugyirat_CimSTR_Ugyindito] ON [dbo].[EREC_UgyUgyiratok]
(
	[CimSTR_Ugyindito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'IX_Ugyirat_Kulso_ID')
CREATE NONCLUSTERED INDEX [IX_Ugyirat_Kulso_ID] ON [dbo].[EREC_UgyUgyiratok]
(
	[UgyUgyirat_Id_Kulso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'TEIV_HATARIDO_I')
CREATE NONCLUSTERED INDEX [TEIV_HATARIDO_I] ON [dbo].[EREC_UgyUgyiratok]
(
	[Hatarido] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'TEIV_IRATTARI_HELY')
CREATE NONCLUSTERED INDEX [TEIV_IRATTARI_HELY] ON [dbo].[EREC_UgyUgyiratok]
(
	[IrattariHely] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'TEIV_KATEGORIA')
CREATE NONCLUSTERED INDEX [TEIV_KATEGORIA] ON [dbo].[EREC_UgyUgyiratok]
(
	[UgyTipus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'TEIV_PRT_ID_ORZO_I')
CREATE NONCLUSTERED INDEX [TEIV_PRT_ID_ORZO_I] ON [dbo].[EREC_UgyUgyiratok]
(
	[FelhasznaloCsoport_Id_Orzo] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC
)
INCLUDE ( 	[Id],
	[Allapot],
	[TovabbitasAlattAllapot]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'TEIV_SKONTROBADATUM_I')
CREATE NONCLUSTERED INDEX [TEIV_SKONTROBADATUM_I] ON [dbo].[EREC_UgyUgyiratok]
(
	[SkontrobaDat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'TEIV_SURGOSSEG_I')
CREATE NONCLUSTERED INDEX [TEIV_SURGOSSEG_I] ON [dbo].[EREC_UgyUgyiratok]
(
	[Surgosseg] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]') AND name = N'UgyUgyirat_LetrehozasIdo_I')
CREATE NONCLUSTERED INDEX [UgyUgyirat_LetrehozasIdo_I] ON [dbo].[EREC_UgyUgyiratok]
(
	[LetrehozasIdo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EIV_EIV_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratok]  WITH CHECK ADD  CONSTRAINT [EIV_EIV_FK] FOREIGN KEY([UgyUgyirat_Id_Szulo])
REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EIV_EIV_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratok] CHECK CONSTRAINT [EIV_EIV_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Ugyirat_UgyiratKulso_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratok]  WITH CHECK ADD  CONSTRAINT [EREC_Ugyirat_UgyiratKulso_FK] FOREIGN KEY([UgyUgyirat_Id_Kulso])
REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Ugyirat_UgyiratKulso_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratok] CHECK CONSTRAINT [EREC_Ugyirat_UgyiratKulso_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Ugy_Csoport_Felelos_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratok]  WITH CHECK ADD  CONSTRAINT [Ugy_Csoport_Felelos_FK] FOREIGN KEY([Csoport_Id_Felelos])
REFERENCES [dbo].[KRT_Csoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Ugy_Csoport_Felelos_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratok] CHECK CONSTRAINT [Ugy_Csoport_Felelos_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[UgyUgyirat_IraIktatokonyv_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratok]  WITH CHECK ADD  CONSTRAINT [UgyUgyirat_IraIktatokonyv_FK] FOREIGN KEY([IraIktatokonyv_Id])
REFERENCES [dbo].[EREC_IraIktatoKonyvek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[UgyUgyirat_IraIktatokonyv_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyUgyiratok]'))
ALTER TABLE [dbo].[EREC_UgyUgyiratok] CHECK CONSTRAINT [UgyUgyirat_IraIktatokonyv_FK]
GO
