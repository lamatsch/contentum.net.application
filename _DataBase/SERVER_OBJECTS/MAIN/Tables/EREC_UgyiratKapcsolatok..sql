IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[UgyiratKapcs_Ugyirat_Felep_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatok]'))
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok] DROP CONSTRAINT [UgyiratKapcs_Ugyirat_Felep_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[UgyiratKapcs_Ugyirat_Beep_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatok]'))
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok] DROP CONSTRAINT [UgyiratKapcs_Ugyirat_Beep_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Ugyi__Letre__7E188EBC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok] DROP CONSTRAINT [DF__EREC_Ugyi__Letre__7E188EBC]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_UgyiratKapcsolatok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok] DROP CONSTRAINT [DF_EREC_UgyiratKapcsolatok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Ugyi__ErvKe__7C30464A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok] DROP CONSTRAINT [DF__EREC_Ugyi__ErvKe__7C30464A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Ugyira__Ver__7B3C2211]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok] DROP CONSTRAINT [DF__EREC_Ugyira__Ver__7B3C2211]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Ugyirat__Id__7A47FDD8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok] DROP CONSTRAINT [DF__EREC_Ugyirat__Id__7A47FDD8]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatok]') AND name = N'Felep_ind')
DROP INDEX [Felep_ind] ON [dbo].[EREC_UgyiratKapcsolatok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatok]') AND name = N'Dup_UK')
DROP INDEX [Dup_UK] ON [dbo].[EREC_UgyiratKapcsolatok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatok]') AND name = N'Beep_ind')
DROP INDEX [Beep_ind] ON [dbo].[EREC_UgyiratKapcsolatok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_UgyiratKapcsolatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_UgyiratKapcsolatok](
	[Id] [uniqueidentifier] NOT NULL,
	[KapcsolatTipus] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[Leiras] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Kezi] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ugyirat_Ugyirat_Beepul] [uniqueidentifier] NULL,
	[Ugyirat_Ugyirat_Felepul] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KIRL_UGYIRATKAPCS_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatok]') AND name = N'Beep_ind')
CREATE NONCLUSTERED INDEX [Beep_ind] ON [dbo].[EREC_UgyiratKapcsolatok]
(
	[Ugyirat_Ugyirat_Beepul] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatok]') AND name = N'Dup_UK')
CREATE NONCLUSTERED INDEX [Dup_UK] ON [dbo].[EREC_UgyiratKapcsolatok]
(
	[Ugyirat_Ugyirat_Beepul] ASC,
	[Ugyirat_Ugyirat_Felepul] ASC,
	[KapcsolatTipus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatok]') AND name = N'Felep_ind')
CREATE NONCLUSTERED INDEX [Felep_ind] ON [dbo].[EREC_UgyiratKapcsolatok]
(
	[Ugyirat_Ugyirat_Felepul] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Ugyirat__Id__7A47FDD8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok] ADD  CONSTRAINT [DF__EREC_Ugyirat__Id__7A47FDD8]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Ugyira__Ver__7B3C2211]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok] ADD  CONSTRAINT [DF__EREC_Ugyira__Ver__7B3C2211]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Ugyi__ErvKe__7C30464A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok] ADD  CONSTRAINT [DF__EREC_Ugyi__ErvKe__7C30464A]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_UgyiratKapcsolatok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok] ADD  CONSTRAINT [DF_EREC_UgyiratKapcsolatok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Ugyi__Letre__7E188EBC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok] ADD  CONSTRAINT [DF__EREC_Ugyi__Letre__7E188EBC]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[UgyiratKapcs_Ugyirat_Beep_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatok]'))
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok]  WITH CHECK ADD  CONSTRAINT [UgyiratKapcs_Ugyirat_Beep_FK] FOREIGN KEY([Ugyirat_Ugyirat_Beepul])
REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[UgyiratKapcs_Ugyirat_Beep_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatok]'))
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok] CHECK CONSTRAINT [UgyiratKapcs_Ugyirat_Beep_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[UgyiratKapcs_Ugyirat_Felep_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatok]'))
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok]  WITH CHECK ADD  CONSTRAINT [UgyiratKapcs_Ugyirat_Felep_FK] FOREIGN KEY([Ugyirat_Ugyirat_Felepul])
REFERENCES [dbo].[EREC_UgyUgyiratok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[UgyiratKapcs_Ugyirat_Felep_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratKapcsolatok]'))
ALTER TABLE [dbo].[EREC_UgyiratKapcsolatok] CHECK CONSTRAINT [UgyiratKapcs_Ugyirat_Felep_FK]
GO
