IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratObjKapcsolatok]') AND name = N'IX_Obj_Id_Kapcsolt')
DROP INDEX [IX_Obj_Id_Kapcsolt] ON [dbo].[EREC_UgyiratObjKapcsolatok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratObjKapcsolatok]') AND name = N'IX_Obj_Id_Elozmeny')
DROP INDEX [IX_Obj_Id_Elozmeny] ON [dbo].[EREC_UgyiratObjKapcsolatok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratObjKapcsolatok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_UgyiratObjKapcsolatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratObjKapcsolatok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_UgyiratObjKapcsolatok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_Ugyirat__Id__4DB4832C]  DEFAULT (newsequentialid()),
	[KapcsolatTipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Leiras] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Kezi] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Obj_Id_Elozmeny] [uniqueidentifier] NOT NULL,
	[Obj_Tip_Id_Elozmeny] [uniqueidentifier] NULL,
	[Obj_Type_Elozmeny] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Obj_Id_Kapcsolt] [uniqueidentifier] NOT NULL,
	[Obj_Tip_Id_Kapcsolt] [uniqueidentifier] NULL,
	[Obj_Type_Kapcsolt] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_Ugyira__Ver__4EA8A765]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_Ugyi__ErvKe__4F9CCB9E]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_UgyiratObjKapcsolatok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_Ugyi__Letre__51851410]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [AK_PK_UGYIRATOBJKAPCS_EREC_UGY] UNIQUE NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratObjKapcsolatok]') AND name = N'IX_Obj_Id_Elozmeny')
CREATE NONCLUSTERED INDEX [IX_Obj_Id_Elozmeny] ON [dbo].[EREC_UgyiratObjKapcsolatok]
(
	[Obj_Id_Elozmeny] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_UgyiratObjKapcsolatok]') AND name = N'IX_Obj_Id_Kapcsolt')
CREATE NONCLUSTERED INDEX [IX_Obj_Id_Kapcsolt] ON [dbo].[EREC_UgyiratObjKapcsolatok]
(
	[Obj_Id_Kapcsolt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
