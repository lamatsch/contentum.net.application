IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__Letre__01E91FA0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlow] DROP CONSTRAINT [DF__EREC_Work__Letre__01E91FA0]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__ErvKe__00F4FB67]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlow] DROP CONSTRAINT [DF__EREC_Work__ErvKe__00F4FB67]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_WorkFl__Ver__0000D72E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlow] DROP CONSTRAINT [DF__EREC_WorkFl__Ver__0000D72E]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_WorkFlo__Id__7F0CB2F5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlow] DROP CONSTRAINT [DF__EREC_WorkFlo__Id__7F0CB2F5]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_WorkFlow]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_WorkFlow]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_WorkFlow]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_WorkFlow](
	[Id] [uniqueidentifier] NOT NULL,
	[Obj_MetaDefinicio_Id] [uniqueidentifier] NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Org] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_WORKFLOW] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_WorkFlo__Id__7F0CB2F5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlow] ADD  CONSTRAINT [DF__EREC_WorkFlo__Id__7F0CB2F5]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_WorkFl__Ver__0000D72E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlow] ADD  CONSTRAINT [DF__EREC_WorkFl__Ver__0000D72E]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__ErvKe__00F4FB67]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlow] ADD  CONSTRAINT [DF__EREC_Work__ErvKe__00F4FB67]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__Letre__01E91FA0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlow] ADD  CONSTRAINT [DF__EREC_Work__Letre__01E91FA0]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
