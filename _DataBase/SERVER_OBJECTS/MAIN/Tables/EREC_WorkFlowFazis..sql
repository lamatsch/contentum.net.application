IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__Letre__06ADD4BD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazis] DROP CONSTRAINT [DF__EREC_Work__Letre__06ADD4BD]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__ErvKe__05B9B084]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazis] DROP CONSTRAINT [DF__EREC_Work__ErvKe__05B9B084]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_WorkFl__Ver__04C58C4B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazis] DROP CONSTRAINT [DF__EREC_WorkFl__Ver__04C58C4B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__Atfut__03D16812]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazis] DROP CONSTRAINT [DF__EREC_Work__Atfut__03D16812]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_WorkFlo__Id__02DD43D9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazis] DROP CONSTRAINT [DF__EREC_WorkFlo__Id__02DD43D9]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_WorkFlowFazis]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_WorkFlowFazis]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_WorkFlowFazis]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_WorkFlowFazis](
	[Id] [uniqueidentifier] NOT NULL,
	[WorkFlow_Id] [uniqueidentifier] NOT NULL,
	[Fazis_Id] [uniqueidentifier] NULL,
	[ObjStateValue_Id] [uniqueidentifier] NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Kod] [char](4) COLLATE Hungarian_CI_AS NULL,
	[Ertek] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[KezdIdobazis] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[KezdObjTip_Id_DateCol] [uniqueidentifier] NULL,
	[Idobazis] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[ObjTip_Id_DateCol] [uniqueidentifier] NULL,
	[AtfutasiIdo] [numeric](8, 0) NULL,
	[Idoegyseg] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_WORKFLOWFAZIS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_WorkFlo__Id__02DD43D9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazis] ADD  CONSTRAINT [DF__EREC_WorkFlo__Id__02DD43D9]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__Atfut__03D16812]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazis] ADD  CONSTRAINT [DF__EREC_Work__Atfut__03D16812]  DEFAULT ((0)) FOR [AtfutasiIdo]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_WorkFl__Ver__04C58C4B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazis] ADD  CONSTRAINT [DF__EREC_WorkFl__Ver__04C58C4B]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__ErvKe__05B9B084]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazis] ADD  CONSTRAINT [DF__EREC_Work__ErvKe__05B9B084]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__Letre__06ADD4BD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazis] ADD  CONSTRAINT [DF__EREC_Work__Letre__06ADD4BD]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
