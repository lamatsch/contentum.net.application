IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__Letre__0B7289DA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazisAtmenet] DROP CONSTRAINT [DF__EREC_Work__Letre__0B7289DA]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__ErvKe__0A7E65A1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazisAtmenet] DROP CONSTRAINT [DF__EREC_Work__ErvKe__0A7E65A1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_WorkFl__Ver__098A4168]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazisAtmenet] DROP CONSTRAINT [DF__EREC_WorkFl__Ver__098A4168]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__IdoKo__08961D2F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazisAtmenet] DROP CONSTRAINT [DF__EREC_Work__IdoKo__08961D2F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_WorkFlo__Id__07A1F8F6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazisAtmenet] DROP CONSTRAINT [DF__EREC_WorkFlo__Id__07A1F8F6]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_WorkFlowFazisAtmenet]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_WorkFlowFazisAtmenet]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_WorkFlowFazisAtmenet]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_WorkFlowFazisAtmenet](
	[Id] [uniqueidentifier] NOT NULL,
	[BealltFazis_Id] [uniqueidentifier] NULL,
	[BealloFazis_Id] [uniqueidentifier] NULL,
	[FazisKimenet] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[IdoKorrekcio] [numeric](8, 0) NULL,
	[Idoegyseg] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_WORKFLOWFAZISATMENET] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_WorkFlo__Id__07A1F8F6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazisAtmenet] ADD  CONSTRAINT [DF__EREC_WorkFlo__Id__07A1F8F6]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__IdoKo__08961D2F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazisAtmenet] ADD  CONSTRAINT [DF__EREC_Work__IdoKo__08961D2F]  DEFAULT ((0)) FOR [IdoKorrekcio]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_WorkFl__Ver__098A4168]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazisAtmenet] ADD  CONSTRAINT [DF__EREC_WorkFl__Ver__098A4168]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__ErvKe__0A7E65A1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazisAtmenet] ADD  CONSTRAINT [DF__EREC_Work__ErvKe__0A7E65A1]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_Work__Letre__0B7289DA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_WorkFlowFazisAtmenet] ADD  CONSTRAINT [DF__EREC_Work__Letre__0B7289DA]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
