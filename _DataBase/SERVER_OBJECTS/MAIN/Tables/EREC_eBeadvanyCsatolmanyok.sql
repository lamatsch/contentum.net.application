if exists (select 1
            from  sysobjects
           where  id = object_id('EREC_eBeadvanyCsatolmanyok')
            and   type = 'U')
   drop table EREC_eBeadvanyCsatolmanyok
go

/*==============================================================*/
/* Table: EREC_eBeadvanyCsatolmanyok                            */
/*==============================================================*/
create table EREC_eBeadvanyCsatolmanyok (
   Id                   uniqueidentifier     not null default newsequentialid(),
   eBeadvany_Id         uniqueidentifier     null,
   Dokumentum_Id        uniqueidentifier     null,
   Nev                  Nvarchar(400)        null,
   Ver                  int                  null default 1,
   Note                 Nvarchar(4000)       null,
   Stat_id              uniqueidentifier     null,
   ErvKezd              datetime             null default Getdate(),
   ErvVege              datetime             null,
   Letrehozo_id         uniqueidentifier     null,
   LetrehozasIdo        datetime             not null default Getdate(),
   Modosito_id          uniqueidentifier     null,
   ModositasIdo         datetime             null,
   Zarolo_id            uniqueidentifier     null,
   ZarolasIdo           datetime             null,
   Tranz_id             uniqueidentifier     null,
   UIAccessLog_id       uniqueidentifier     null,
   constraint PK_EREC_EBEADVANYCSATOLMANYOK primary key (Id)
)
go

alter table EREC_eBeadvanyCsatolmanyok 
  ADD CONSTRAINT
	DF_EREC_eBeadvanyCsatolmanyok_ErvVege DEFAULT CONVERT(datetime, '4700-12-31', 102) FOR ErvVege;
go

alter table EREC_eBeadvanyCsatolmanyok
   add constraint EREC_eBeadvanyCsatolmanyok_Dokumentum_FK foreign key (Dokumentum_Id)
      references KRT_Dokumentumok (Id)
go

alter table EREC_eBeadvanyCsatolmanyok
   add constraint EREC_eBeadvanyCsatolmanyok_eBeadvany_FK foreign key (eBeadvany_Id)
      references EREC_eBeadvanyok (Id)
go
