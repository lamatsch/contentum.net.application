if exists (select 1
            from  sysobjects
           where  id = object_id('EREC_eBeadvanyok')
            and   type = 'U')
   drop table EREC_eBeadvanyok
go

/*==============================================================*/
/* Table: EREC_eBeadvanyok                                      */
/*==============================================================*/
create table EREC_eBeadvanyok (
   Id                   uniqueidentifier     not null default newsequentialid(),
   Irany                char(1)              null,
   Allapot              nvarchar(64)         COLLATE Hungarian_CS_AS NULL,
   KuldoRendszer        Nvarchar(400)        null,
   UzenetTipusa         nvarchar(64)         COLLATE Hungarian_CS_AS NULL,
   FeladoTipusa         int                  null,
   PartnerKapcsolatiKod Nvarchar(400)        null,
   PartnerNev           Nvarchar(400)        null,
   PartnerEmail         Nvarchar(400)        null,
   PartnerRovidNev      Nvarchar(400)        null,
   PartnerMAKKod        Nvarchar(400)        null,
   PartnerKRID          Nvarchar(400)        null,
   Partner_Id           uniqueidentifier     null,
   Cim_Id               uniqueidentifier     null,
   KR_HivatkozasiSzam   Nvarchar(400)        null,
   KR_ErkeztetesiSzam   Nvarchar(400)        null,
   Contentum_HivatkozasiSzam uniqueidentifier     null,
   PR_HivatkozasiSzam   Nvarchar(400)        null,
   PR_ErkeztetesiSzam   Nvarchar(400)        null,
   KR_DokTipusHivatal   Nvarchar(400)        null,
   KR_DokTipusAzonosito Nvarchar(400)        null,
   KR_DokTipusLeiras    Nvarchar(400)        null,
   KR_Megjegyzes        Nvarchar(4000)       null,
   KR_ErvenyessegiDatum datetime             null,
   KR_ErkeztetesiDatum  datetime             null,
   KR_FileNev           Nvarchar(400)        null,
   KR_Kezbesitettseg    int                  null,
   KR_Idopecset         Nvarchar(4000)       null,
   KR_Valasztitkositas  char(1)              null,
   KR_Valaszutvonal     int                  null,
   KR_Rendszeruzenet    char(1)              null,
   KR_Tarterulet        int                  null,
   KR_ETertiveveny      char(1)              null,
   KR_Lenyomat          Nvarchar(Max)        null,
   KuldKuldemeny_Id     uniqueidentifier     null,
   IraIrat_Id           uniqueidentifier     null,
   IratPeldany_Id       uniqueidentifier     null,
   Cel                  Nvarchar(400)        null,
   Ver                  int                  null default 1,
   Note                 Nvarchar(4000)       null,
   Stat_id              uniqueidentifier     null,
   ErvKezd              datetime             null default Getdate(),
   ErvVege              datetime             null,
   Letrehozo_id         uniqueidentifier     null,
   LetrehozasIdo        datetime             not null default Getdate(),
   Modosito_id          uniqueidentifier     null,
   ModositasIdo         datetime             null,
   Zarolo_id            uniqueidentifier     null,
   ZarolasIdo           datetime             null,
   Tranz_id             uniqueidentifier     null,
   UIAccessLog_id       uniqueidentifier     null,
   constraint PK_EREC_EBEADVANYOK primary key (Id)
)
go

alter table EREC_eBeadvanyok 
  ADD CONSTRAINT
	DF_EREC_eBeadvanyok_ErvVege DEFAULT CONVERT(datetime, '4700-12-31', 102) FOR ErvVege;
go

alter table EREC_eBeadvanyok
   add constraint EREC_eBeadvanyok_Cim_FK foreign key (Cim_Id)
      references KRT_Cimek (Id)
go

alter table EREC_eBeadvanyok
   add constraint EREC_eBeadvanyok_IratPeldany_FK foreign key (IratPeldany_Id)
      references EREC_PldIratPeldanyok (Id)
go

alter table EREC_eBeadvanyok
   add constraint EREC_eBeadvanyok_Irat_FK foreign key (IraIrat_Id)
      references EREC_IraIratok (Id)
go

alter table EREC_eBeadvanyok
   add constraint EREC_eBeadvanyok_Kuldemeny_FK foreign key (KuldKuldemeny_Id)
      references EREC_KuldKuldemenyek (Id)
go

alter table EREC_eBeadvanyok
   add constraint EREC_eBeadvanyok_Partner_FK foreign key (Partner_Id)
      references KRT_Partnerek (Id)
go
