IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[XBC_POP3_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimei]'))
ALTER TABLE [dbo].[EREC_eMailBoritekCimei] DROP CONSTRAINT [XBC_POP3_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[BoritekCim_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimei]'))
ALTER TABLE [dbo].[EREC_eMailBoritekCimei] DROP CONSTRAINT [BoritekCim_Partner_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimei]') AND name = N'FK_PartnerId')
DROP INDEX [FK_PartnerId] ON [dbo].[EREC_eMailBoritekCimei]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimei]') AND name = N'FK_eMailBoritek_Id')
DROP INDEX [FK_eMailBoritek_Id] ON [dbo].[EREC_eMailBoritekCimei]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimei]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_eMailBoritekCimei]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimei]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_eMailBoritekCimei](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_eMailBo__Id__22401542]  DEFAULT (newsequentialid()),
	[eMailBoritek_Id] [uniqueidentifier] NOT NULL,
	[Sorszam] [int] NOT NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NOT NULL,
	[MailCim] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Partner_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_eMailB__Ver__2334397B]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_eMai__ErvKe__24285DB4]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_eMailBoritekCimei_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_eMai__Letre__2610A626]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [XBC_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimei]') AND name = N'FK_eMailBoritek_Id')
CREATE NONCLUSTERED INDEX [FK_eMailBoritek_Id] ON [dbo].[EREC_eMailBoritekCimei]
(
	[eMailBoritek_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimei]') AND name = N'FK_PartnerId')
CREATE NONCLUSTERED INDEX [FK_PartnerId] ON [dbo].[EREC_eMailBoritekCimei]
(
	[Partner_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[BoritekCim_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimei]'))
ALTER TABLE [dbo].[EREC_eMailBoritekCimei]  WITH CHECK ADD  CONSTRAINT [BoritekCim_Partner_FK] FOREIGN KEY([Partner_Id])
REFERENCES [dbo].[KRT_Partnerek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[BoritekCim_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimei]'))
ALTER TABLE [dbo].[EREC_eMailBoritekCimei] CHECK CONSTRAINT [BoritekCim_Partner_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[XBC_POP3_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimei]'))
ALTER TABLE [dbo].[EREC_eMailBoritekCimei]  WITH CHECK ADD  CONSTRAINT [XBC_POP3_FK] FOREIGN KEY([eMailBoritek_Id])
REFERENCES [dbo].[EREC_eMailBoritekok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[XBC_POP3_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCimei]'))
ALTER TABLE [dbo].[EREC_eMailBoritekCimei] CHECK CONSTRAINT [XBC_POP3_FK]
GO
