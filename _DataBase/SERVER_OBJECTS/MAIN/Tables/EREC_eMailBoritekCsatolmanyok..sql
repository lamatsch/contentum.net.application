IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_EMAILCSAT_EMAILBOR_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCsatolmanyok]'))
ALTER TABLE [dbo].[EREC_eMailBoritekCsatolmanyok] DROP CONSTRAINT [EREC_EMAILCSAT_EMAILBOR_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[eMailBoritekCsatol_Dokumentum_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCsatolmanyok]'))
ALTER TABLE [dbo].[EREC_eMailBoritekCsatolmanyok] DROP CONSTRAINT [eMailBoritekCsatol_Dokumentum_FK]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCsatolmanyok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_eMailBoritekCsatolmanyok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCsatolmanyok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_eMailBoritekCsatolmanyok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_eMailBo__Id__28ED12D1]  DEFAULT (newsequentialid()),
	[eMailBoritek_Id] [uniqueidentifier] NOT NULL,
	[Dokumentum_Id] [uniqueidentifier] NOT NULL,
	[Nev] [nvarchar](4000) COLLATE Hungarian_CI_AS NOT NULL,
	[Tomoritve] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_eMailB__Ver__29E1370A]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_eMai__ErvKe__2AD55B43]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_eMailBoritekCsatolmanyok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_eMai__Letre__2CBDA3B5]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_EREC_EMAILBORITEKCSATOLMANY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[eMailBoritekCsatol_Dokumentum_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCsatolmanyok]'))
ALTER TABLE [dbo].[EREC_eMailBoritekCsatolmanyok]  WITH CHECK ADD  CONSTRAINT [eMailBoritekCsatol_Dokumentum_FK] FOREIGN KEY([Dokumentum_Id])
REFERENCES [dbo].[KRT_Dokumentumok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[eMailBoritekCsatol_Dokumentum_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCsatolmanyok]'))
ALTER TABLE [dbo].[EREC_eMailBoritekCsatolmanyok] CHECK CONSTRAINT [eMailBoritekCsatol_Dokumentum_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_EMAILCSAT_EMAILBOR_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCsatolmanyok]'))
ALTER TABLE [dbo].[EREC_eMailBoritekCsatolmanyok]  WITH CHECK ADD  CONSTRAINT [EREC_EMAILCSAT_EMAILBOR_FK] FOREIGN KEY([eMailBoritek_Id])
REFERENCES [dbo].[EREC_eMailBoritekok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[EREC_EMAILCSAT_EMAILBOR_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCsatolmanyok]'))
ALTER TABLE [dbo].[EREC_eMailBoritekCsatolmanyok] CHECK CONSTRAINT [EREC_EMAILCSAT_EMAILBOR_FK]
GO
