IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_eMailBoritekok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_eMailBoritekok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__EREC_eMailBo__Id__2F9A1060]  DEFAULT (newsequentialid()),
	[Felado] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Cimzett] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[CC] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Targy] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[FeladasDatuma] [datetime] NULL CONSTRAINT [DF__EREC_eMai__Felad__308E3499]  DEFAULT (NULL),
	[ErkezesDatuma] [datetime] NULL,
	[Fontossag] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[IraIrat_Id] [uniqueidentifier] NULL,
	[DigitalisAlairas] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Uzenet] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[EmailForras] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[EmailGuid] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[FeldolgozasIdo] [datetime] NULL,
	[ForrasTipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__EREC_eMailB__Ver__318258D2]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__EREC_eMai__ErvKe__32767D0B]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_EREC_eMailBoritekok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__EREC_eMai__Letre__345EC57D]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [POP3_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
