IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_eMai__Letre__5ACF527F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_eMailFiokok] DROP CONSTRAINT [DF__EREC_eMai__Letre__5ACF527F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_eMailFiokok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_eMailFiokok] DROP CONSTRAINT [DF_EREC_eMailFiokok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_eMai__ErvKe__58E70A0D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_eMailFiokok] DROP CONSTRAINT [DF__EREC_eMai__ErvKe__58E70A0D]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_eMailF__Ver__57F2E5D4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_eMailFiokok] DROP CONSTRAINT [DF__EREC_eMailF__Ver__57F2E5D4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_eMailFi__Id__56FEC19B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_eMailFiokok] DROP CONSTRAINT [DF__EREC_eMailFi__Id__56FEC19B]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailFiokok]') AND name = N'EREC_eMailFiok_UK')
DROP INDEX [EREC_eMailFiok_UK] ON [dbo].[EREC_eMailFiokok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailFiokok]') AND type in (N'U'))
DROP TABLE [dbo].[EREC_eMailFiokok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailFiokok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EREC_eMailFiokok](
	[Id] [uniqueidentifier] NOT NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UserNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Csoportok_Id] [uniqueidentifier] NULL,
	[Jelszo] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[NapiMax] [int] NULL,
	[NapiTeny] [int] NULL,
	[EmailCim] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Modul_Id] [uniqueidentifier] NULL,
	[MappaNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KIRL_POP3_FIOKOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailFiokok]') AND name = N'EREC_eMailFiok_UK')
CREATE NONCLUSTERED INDEX [EREC_eMailFiok_UK] ON [dbo].[EREC_eMailFiokok]
(
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_eMailFi__Id__56FEC19B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_eMailFiokok] ADD  CONSTRAINT [DF__EREC_eMailFi__Id__56FEC19B]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_eMailF__Ver__57F2E5D4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_eMailFiokok] ADD  CONSTRAINT [DF__EREC_eMailF__Ver__57F2E5D4]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_eMai__ErvKe__58E70A0D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_eMailFiokok] ADD  CONSTRAINT [DF__EREC_eMai__ErvKe__58E70A0D]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_EREC_eMailFiokok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_eMailFiokok] ADD  CONSTRAINT [DF_EREC_eMailFiokok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__EREC_eMai__Letre__5ACF527F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[EREC_eMailFiokok] ADD  CONSTRAINT [DF__EREC_eMai__Letre__5ACF527F]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
