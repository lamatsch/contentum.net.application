IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[HKP_DokumentumAdatok_Dokumentum_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[HKP_DokumentumAdatok]'))
ALTER TABLE [dbo].[HKP_DokumentumAdatok] DROP CONSTRAINT [HKP_DokumentumAdatok_Dokumentum_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__HKP_Dokum__Letre__61A66D40]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HKP_DokumentumAdatok] DROP CONSTRAINT [DF__HKP_Dokum__Letre__61A66D40]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_HKP_DokumentumAdatok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HKP_DokumentumAdatok] DROP CONSTRAINT [DF_HKP_DokumentumAdatok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__HKP_Dokum__ErvKe__5FBE24CE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HKP_DokumentumAdatok] DROP CONSTRAINT [DF__HKP_Dokum__ErvKe__5FBE24CE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__HKP_Dokumen__Ver__5ECA0095]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HKP_DokumentumAdatok] DROP CONSTRAINT [DF__HKP_Dokumen__Ver__5ECA0095]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__HKP_Dokument__Id__5DD5DC5C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HKP_DokumentumAdatok] DROP CONSTRAINT [DF__HKP_Dokument__Id__5DD5DC5C]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HKP_DokumentumAdatok]') AND type in (N'U'))
DROP TABLE [dbo].[HKP_DokumentumAdatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HKP_DokumentumAdatok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HKP_DokumentumAdatok](
	[Id] [uniqueidentifier] NOT NULL,
	[Irany] [int] NULL,
	[Allapot] [int] NULL,
	[FeladoTipusa] [int] NULL,
	[KapcsolatiKod] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Email] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[RovidNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[MAKKod] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[KRID] [int] NULL,
	[ErkeztetesiSzam] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[HivatkozasiSzam] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[DokTipusHivatal] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[DokTipusAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[DokTipusLeiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Megjegyzes] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[FileNev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[ErvenyessegiDatum] [datetime] NULL,
	[ErkeztetesiDatum] [datetime] NULL,
	[Kezbesitettseg] [int] NULL,
	[Idopecset] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ValaszTitkositas] [char](1) COLLATE Hungarian_CI_AS NULL,
	[ValaszUtvonal] [int] NULL,
	[Rendszeruzenet] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Tarterulet] [int] NULL,
	[ETertiveveny] [int] NULL,
	[Lenyomat] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Dokumentum_Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[IraIrat_Id] [uniqueidentifier] NULL,
	[IratPeldany_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_HKP_DOKUMENTUMADATOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__HKP_Dokument__Id__5DD5DC5C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HKP_DokumentumAdatok] ADD  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__HKP_Dokumen__Ver__5ECA0095]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HKP_DokumentumAdatok] ADD  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__HKP_Dokum__ErvKe__5FBE24CE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HKP_DokumentumAdatok] ADD  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_HKP_DokumentumAdatok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HKP_DokumentumAdatok] ADD  CONSTRAINT [DF_HKP_DokumentumAdatok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__HKP_Dokum__Letre__61A66D40]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HKP_DokumentumAdatok] ADD  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[HKP_DokumentumAdatok_Dokumentum_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[HKP_DokumentumAdatok]'))
ALTER TABLE [dbo].[HKP_DokumentumAdatok]  WITH CHECK ADD  CONSTRAINT [HKP_DokumentumAdatok_Dokumentum_FK] FOREIGN KEY([Dokumentum_Id])
REFERENCES [dbo].[KRT_Dokumentumok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[HKP_DokumentumAdatok_Dokumentum_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[HKP_DokumentumAdatok]'))
ALTER TABLE [dbo].[HKP_DokumentumAdatok] CHECK CONSTRAINT [HKP_DokumentumAdatok_Dokumentum_FK]
GO
