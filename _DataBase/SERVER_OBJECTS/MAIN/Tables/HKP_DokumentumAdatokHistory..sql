IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__HKP_Dokum__Histo__629A9179]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HKP_DokumentumAdatokHistory] DROP CONSTRAINT [DF__HKP_Dokum__Histo__629A9179]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[HKP_DokumentumAdatokHistory]') AND name = N'IX_HKP_DokumentumAdatokHistory_ID_VER')
DROP INDEX [IX_HKP_DokumentumAdatokHistory_ID_VER] ON [dbo].[HKP_DokumentumAdatokHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HKP_DokumentumAdatokHistory]') AND type in (N'U'))
DROP TABLE [dbo].[HKP_DokumentumAdatokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HKP_DokumentumAdatokHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HKP_DokumentumAdatokHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[Irany] [int] NULL,
	[Allapot] [int] NULL,
	[FeladoTipusa] [int] NULL,
	[KapcsolatiKod] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Email] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[RovidNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[MAKKod] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[KRID] [int] NULL,
	[ErkeztetesiSzam] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[HivatkozasiSzam] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[DokTipusHivatal] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[DokTipusAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[DokTipusLeiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Megjegyzes] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[FileNev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[ErvenyessegiDatum] [datetime] NULL,
	[ErkeztetesiDatum] [datetime] NULL,
	[Kezbesitettseg] [int] NULL,
	[Idopecset] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ValaszTitkositas] [char](1) COLLATE Hungarian_CI_AS NULL,
	[ValaszUtvonal] [int] NULL,
	[Rendszeruzenet] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Tarterulet] [int] NULL,
	[ETertiveveny] [int] NULL,
	[Lenyomat] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Dokumentum_Id] [uniqueidentifier] NULL,
	[KuldKuldemeny_Id] [uniqueidentifier] NULL,
	[IraIrat_Id] [uniqueidentifier] NULL,
	[IratPeldany_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[HKP_DokumentumAdatokHistory]') AND name = N'IX_HKP_DokumentumAdatokHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_HKP_DokumentumAdatokHistory_ID_VER] ON [dbo].[HKP_DokumentumAdatokHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__HKP_Dokum__Histo__629A9179]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HKP_DokumentumAdatokHistory] ADD  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
