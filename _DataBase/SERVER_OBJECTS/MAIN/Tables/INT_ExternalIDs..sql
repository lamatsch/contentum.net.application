IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[INTExternalIDs_Modul_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[INT_ExternalIDs]'))
ALTER TABLE [dbo].[INT_ExternalIDs] DROP CONSTRAINT [INTExternalIDs_Modul_FK]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[INT_ExternalIDs]') AND type in (N'U'))
DROP TABLE [dbo].[INT_ExternalIDs]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[INT_ExternalIDs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[INT_ExternalIDs](
	[Id] [uniqueidentifier] NOT NULL,
	[Modul_Id] [uniqueidentifier] NOT NULL,
	[Edok_Type] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Edok_Id] [uniqueidentifier] NULL,
	[Edok_ExpiredDate] [datetime] NULL,
	[External_Group] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[External_Id] [uniqueidentifier] NULL,
	[Deleted] [char](1) COLLATE Hungarian_CI_AS NULL,
	[LastSync] [datetime] NULL,
	[State] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_INT_EXTERNALIDS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[INTExternalIDs_Modul_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[INT_ExternalIDs]'))
ALTER TABLE [dbo].[INT_ExternalIDs]  WITH CHECK ADD  CONSTRAINT [INTExternalIDs_Modul_FK] FOREIGN KEY([Modul_Id])
REFERENCES [dbo].[INT_Modulok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[INTExternalIDs_Modul_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[INT_ExternalIDs]'))
ALTER TABLE [dbo].[INT_ExternalIDs] CHECK CONSTRAINT [INTExternalIDs_Modul_FK]
GO
