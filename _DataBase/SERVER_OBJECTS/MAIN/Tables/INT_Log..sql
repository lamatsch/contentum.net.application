IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[INTLog_Modul_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[INT_Log]'))
ALTER TABLE [dbo].[INT_Log] DROP CONSTRAINT [INTLog_Modul_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Log__Letreho__0D5AD24C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Log] DROP CONSTRAINT [DF__INT_Log__Letreho__0D5AD24C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Log__Id__0C66AE13]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Log] DROP CONSTRAINT [DF__INT_Log__Id__0C66AE13]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[INT_Log]') AND type in (N'U'))
DROP TABLE [dbo].[INT_Log]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[INT_Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[INT_Log](
	[Id] [uniqueidentifier] NOT NULL,
	[Org] [uniqueidentifier] NULL,
	[Modul_id] [uniqueidentifier] NOT NULL,
	[Sync_StartDate] [datetime] NULL,
	[Parancs] [nvarchar](400) COLLATE Hungarian_CI_AS NOT NULL,
	[Machine] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Sync_EndDate] [datetime] NULL,
	[HibaKod] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[HibaUzenet] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
 CONSTRAINT [LOG_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Log__Id__0C66AE13]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Log] ADD  CONSTRAINT [DF__INT_Log__Id__0C66AE13]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Log__Letreho__0D5AD24C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Log] ADD  CONSTRAINT [DF__INT_Log__Letreho__0D5AD24C]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[INTLog_Modul_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[INT_Log]'))
ALTER TABLE [dbo].[INT_Log]  WITH CHECK ADD  CONSTRAINT [INTLog_Modul_FK] FOREIGN KEY([Modul_id])
REFERENCES [dbo].[INT_Modulok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[INTLog_Modul_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[INT_Log]'))
ALTER TABLE [dbo].[INT_Log] CHECK CONSTRAINT [INTLog_Modul_FK]
GO
