if exists (select 1
            from  sysobjects
           where  id = object_id('INT_ManagedEmailAddress')
            and   type = 'U')
   drop table INT_ManagedEmailAddress
go

/*==============================================================*/
/* Table: INT_ManagedEmailAddress                               */
/*==============================================================*/
create table INT_ManagedEmailAddress (
  Id                   uniqueidentifier     not null constraint DF__INT_ManagedEmailAddress__Id__66D10805 default 'newsequentialid()',
   Org                  uniqueidentifier     null,
   Felhasznalo_id       uniqueidentifier     null,
   Nev                  Nvarchar(400)        collate Hungarian_CI_AS not null,
   Ertek                Nvarchar(max)        collate Hungarian_CI_AS null,
   Karbantarthato       char(1)              collate Hungarian_CI_AS not null,
   Ver                  int                  null constraint DF__INT_ManagedEmailAddress__Ver__67C52C3E default (1),
   Note                 Nvarchar(4000)       collate Hungarian_CI_AS null,
   Stat_id              uniqueidentifier     null,
   ErvKezd              datetime             null constraint DF__INT_ManagedEmailAddress__ErvKe__68B95077 default getdate(),
   ErvVege              datetime             null constraint DF_INT_ManagedEmail_ErvVege default CONVERT([datetime],'4700-12-31',(102)),
   Letrehozo_id         uniqueidentifier     null,
   LetrehozasIdo        datetime             not null constraint DF__INT_ManagedEmailAddress__Letre__6AA198E9 default getdate(),
   Modosito_id          uniqueidentifier     null,
   ModositasIdo         datetime             null,
   Zarolo_id            uniqueidentifier     null,
   ZarolasIdo           datetime             null,
   Tranz_id             uniqueidentifier     null,
   UIAccessLog_id       uniqueidentifier     null,
   constraint PK_INT_MANAGEDEMAILADDRESS primary key (Id)

)
go
