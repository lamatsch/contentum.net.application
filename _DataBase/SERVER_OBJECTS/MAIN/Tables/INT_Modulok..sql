IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Modul__Letre__0504B816]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Modulok] DROP CONSTRAINT [DF__INT_Modul__Letre__0504B816]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_INT_Modulok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Modulok] DROP CONSTRAINT [DF_INT_Modulok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Modul__ErvKe__031C6FA4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Modulok] DROP CONSTRAINT [DF__INT_Modul__ErvKe__031C6FA4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Modulok__Ver__02284B6B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Modulok] DROP CONSTRAINT [DF__INT_Modulok__Ver__02284B6B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Modulok__Id__01342732]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Modulok] DROP CONSTRAINT [DF__INT_Modulok__Id__01342732]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[INT_Modulok]') AND type in (N'U'))
DROP TABLE [dbo].[INT_Modulok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[INT_Modulok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[INT_Modulok](
	[Id] [uniqueidentifier] NOT NULL,
	[Org] [uniqueidentifier] NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NOT NULL,
	[Statusz] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Parancs] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [MOD_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Modulok__Id__01342732]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Modulok] ADD  CONSTRAINT [DF__INT_Modulok__Id__01342732]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Modulok__Ver__02284B6B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Modulok] ADD  CONSTRAINT [DF__INT_Modulok__Ver__02284B6B]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Modul__ErvKe__031C6FA4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Modulok] ADD  CONSTRAINT [DF__INT_Modul__ErvKe__031C6FA4]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_INT_Modulok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Modulok] ADD  CONSTRAINT [DF_INT_Modulok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Modul__Letre__0504B816]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Modulok] ADD  CONSTRAINT [DF__INT_Modul__Letre__0504B816]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
