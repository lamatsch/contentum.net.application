IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[INTParameter_Modul_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[INT_Parameterek]'))
ALTER TABLE [dbo].[INT_Parameterek] DROP CONSTRAINT [INTParameter_Modul_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[INTParameter_Felhasznalo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[INT_Parameterek]'))
ALTER TABLE [dbo].[INT_Parameterek] DROP CONSTRAINT [INTParameter_Felhasznalo_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Param__Letre__0BB1B5A5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Parameterek] DROP CONSTRAINT [DF__INT_Param__Letre__0BB1B5A5]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_INT_Parameterek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Parameterek] DROP CONSTRAINT [DF_INT_Parameterek_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Param__ErvKe__09C96D33]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Parameterek] DROP CONSTRAINT [DF__INT_Param__ErvKe__09C96D33]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Paramet__Ver__08D548FA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Parameterek] DROP CONSTRAINT [DF__INT_Paramet__Ver__08D548FA]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Paramete__Id__07E124C1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Parameterek] DROP CONSTRAINT [DF__INT_Paramete__Id__07E124C1]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[INT_Parameterek]') AND type in (N'U'))
DROP TABLE [dbo].[INT_Parameterek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[INT_Parameterek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[INT_Parameterek](
	[Id] [uniqueidentifier] NOT NULL,
	[Org] [uniqueidentifier] NULL,
	[Modul_id] [uniqueidentifier] NOT NULL,
	[Felhasznalo_id] [uniqueidentifier] NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NOT NULL,
	[Ertek] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Karbantarthato] [char](1) COLLATE Hungarian_CI_AS NOT NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [INT_PAR_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Paramete__Id__07E124C1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Parameterek] ADD  CONSTRAINT [DF__INT_Paramete__Id__07E124C1]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Paramet__Ver__08D548FA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Parameterek] ADD  CONSTRAINT [DF__INT_Paramet__Ver__08D548FA]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Param__ErvKe__09C96D33]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Parameterek] ADD  CONSTRAINT [DF__INT_Param__ErvKe__09C96D33]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_INT_Parameterek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Parameterek] ADD  CONSTRAINT [DF_INT_Parameterek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__INT_Param__Letre__0BB1B5A5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[INT_Parameterek] ADD  CONSTRAINT [DF__INT_Param__Letre__0BB1B5A5]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[INTParameter_Felhasznalo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[INT_Parameterek]'))
ALTER TABLE [dbo].[INT_Parameterek]  WITH CHECK ADD  CONSTRAINT [INTParameter_Felhasznalo_FK] FOREIGN KEY([Felhasznalo_id])
REFERENCES [dbo].[KRT_Felhasznalok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[INTParameter_Felhasznalo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[INT_Parameterek]'))
ALTER TABLE [dbo].[INT_Parameterek] CHECK CONSTRAINT [INTParameter_Felhasznalo_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[INTParameter_Modul_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[INT_Parameterek]'))
ALTER TABLE [dbo].[INT_Parameterek]  WITH CHECK ADD  CONSTRAINT [INTParameter_Modul_FK] FOREIGN KEY([Modul_id])
REFERENCES [dbo].[INT_Modulok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[INTParameter_Modul_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[INT_Parameterek]'))
ALTER TABLE [dbo].[INT_Parameterek] CHECK CONSTRAINT [INTParameter_Modul_FK]
GO
