IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IrattariTetelTemp]') AND type in (N'U'))
DROP TABLE [dbo].[IrattariTetelTemp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IrattariTetelTemp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[IrattariTetelTemp](
	[Ágazat betujele] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[Ágazat megnevezése] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[Tetelszam] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[Tétel megnevezése] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[Selejtezési ido (év)] [float] NULL,
	[Lt#] [float] NULL,
	[Ügytípus kód] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[UgytipusKod] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[Ügytípus neve] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[Ügyfajta] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[Ügyint# ido] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[Ügyint# ido mérték-egysége] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[TetelMegnev] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[megnev] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[Tetel] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[irattaritetelGUID] [uniqueidentifier] NULL,
	[iratmetaGUID] [uniqueidentifier] NULL,
	[Szamkeret] [nvarchar](4) COLLATE Hungarian_CI_AS NULL
) ON [PRIMARY]
END
GO
