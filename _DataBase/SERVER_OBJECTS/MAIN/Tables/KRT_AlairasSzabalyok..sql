IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_IRATALAIR_DOKUMUV_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_AlairasSzabalyok]'))
ALTER TABLE [dbo].[KRT_AlairasSzabalyok] DROP CONSTRAINT [KRT_IRATALAIR_DOKUMUV_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_IRATALAIR_ALAIRASTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_AlairasSzabalyok]'))
ALTER TABLE [dbo].[KRT_AlairasSzabalyok] DROP CONSTRAINT [KRT_IRATALAIR_ALAIRASTIP_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_AlairasSzabalyok]') AND name = N'KRT_AlairasSzabaly_UK')
DROP INDEX [KRT_AlairasSzabaly_UK] ON [dbo].[KRT_AlairasSzabalyok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_AlairasSzabalyok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_AlairasSzabalyok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_AlairasSzabalyok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_AlairasSzabalyok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_AlairasS__Id__162F4418]  DEFAULT (newsequentialid()),
	[DokumentumMuvelet_Id] [uniqueidentifier] NOT NULL,
	[MetaDefinicio_Id] [uniqueidentifier] NULL,
	[MetaDefinicioTipus] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AlairoSzerep] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[SzabalySzint] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AlairasTipus_Id] [uniqueidentifier] NULL,
	[SzabalyTipus] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[Titkositas] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Sorrend] [int] NULL CONSTRAINT [DF__KRT_Alair__Sorre__17236851]  DEFAULT ((1)),
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Alairas__Ver__18178C8A]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Alair__ErvKe__190BB0C3]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Alair__Letre__19FFD4FC]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_ALAIRASSZABALYOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_AlairasSzabalyok]') AND name = N'KRT_AlairasSzabaly_UK')
CREATE NONCLUSTERED INDEX [KRT_AlairasSzabaly_UK] ON [dbo].[KRT_AlairasSzabalyok]
(
	[DokumentumMuvelet_Id] ASC,
	[AlairoSzerep] ASC,
	[AlairasTipus_Id] ASC,
	[Titkositas] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_IRATALAIR_ALAIRASTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_AlairasSzabalyok]'))
ALTER TABLE [dbo].[KRT_AlairasSzabalyok]  WITH CHECK ADD  CONSTRAINT [KRT_IRATALAIR_ALAIRASTIP_FK] FOREIGN KEY([AlairasTipus_Id])
REFERENCES [dbo].[KRT_AlairasTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_IRATALAIR_ALAIRASTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_AlairasSzabalyok]'))
ALTER TABLE [dbo].[KRT_AlairasSzabalyok] CHECK CONSTRAINT [KRT_IRATALAIR_ALAIRASTIP_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_IRATALAIR_DOKUMUV_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_AlairasSzabalyok]'))
ALTER TABLE [dbo].[KRT_AlairasSzabalyok]  WITH CHECK ADD  CONSTRAINT [KRT_IRATALAIR_DOKUMUV_FK] FOREIGN KEY([DokumentumMuvelet_Id])
REFERENCES [dbo].[KRT_DokumentumMuveletek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_IRATALAIR_DOKUMUV_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_AlairasSzabalyok]'))
ALTER TABLE [dbo].[KRT_AlairasSzabalyok] CHECK CONSTRAINT [KRT_IRATALAIR_DOKUMUV_FK]
GO
