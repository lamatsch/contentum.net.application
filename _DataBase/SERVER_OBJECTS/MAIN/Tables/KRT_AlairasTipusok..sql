IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_AlairasTipusok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_AlairasTipusok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_AlairasTipusok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_AlairasTipusok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_AlairasT__Id__10766AC2]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[AlairasKod] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[AlairasMod] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[AlairasSzint] [int] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Alairas__Ver__116A8EFB]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Alair__ErvKe__125EB334]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Alair__Letre__1352D76D]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_ALAIRASTIPUSOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
