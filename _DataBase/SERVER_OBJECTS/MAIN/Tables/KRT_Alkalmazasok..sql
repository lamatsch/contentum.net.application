IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_ALKA_ALKALMAZA_KRT_ALKA]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Alkalmazasok]'))
ALTER TABLE [dbo].[KRT_Alkalmazasok] DROP CONSTRAINT [FK_KRT_ALKA_ALKALMAZA_KRT_ALKA]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Alkalmazasok]') AND name = N'KRT_Alkalmazas_UK')
DROP INDEX [KRT_Alkalmazas_UK] ON [dbo].[KRT_Alkalmazasok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Alkalmazasok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Alkalmazasok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Alkalmazasok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Alkalmazasok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Alkalmaz__Id__1CDC41A7]  DEFAULT (newsequentialid()),
	[KRT_Id] [uniqueidentifier] NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Kulso] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Org] [uniqueidentifier] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Alkalma__Ver__1DD065E0]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Alkal__ErvKe__1EC48A19]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Alkalmazasok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Alkal__Letre__20ACD28B]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_ALKALMAZASOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Alkalmazasok]') AND name = N'KRT_Alkalmazas_UK')
CREATE NONCLUSTERED INDEX [KRT_Alkalmazas_UK] ON [dbo].[KRT_Alkalmazasok]
(
	[Org] ASC,
	[Kod] ASC,
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_ALKA_ALKALMAZA_KRT_ALKA]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Alkalmazasok]'))
ALTER TABLE [dbo].[KRT_Alkalmazasok]  WITH CHECK ADD  CONSTRAINT [FK_KRT_ALKA_ALKALMAZA_KRT_ALKA] FOREIGN KEY([KRT_Id])
REFERENCES [dbo].[KRT_Alkalmazasok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_ALKA_ALKALMAZA_KRT_ALKA]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Alkalmazasok]'))
ALTER TABLE [dbo].[KRT_Alkalmazasok] CHECK CONSTRAINT [FK_KRT_ALKA_ALKALMAZA_KRT_ALKA]
GO
