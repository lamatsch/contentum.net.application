IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_BANKSZAMLASZAMOK_KRT_PARTNEREK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Bankszamlaszamok]'))
ALTER TABLE [dbo].[KRT_Bankszamlaszamok] DROP CONSTRAINT [FK_KRT_BANKSZAMLASZAMOK_KRT_PARTNEREK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_BANKSZAMLASZAMOK_BANK_KRT_PARTNEREK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Bankszamlaszamok]'))
ALTER TABLE [dbo].[KRT_Bankszamlaszamok] DROP CONSTRAINT [FK_KRT_BANKSZAMLASZAMOK_BANK_KRT_PARTNEREK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Banks__Letre__2759D01A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Bankszamlaszamok] DROP CONSTRAINT [DF__KRT_Banks__Letre__2759D01A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Bankszamlaszamok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Bankszamlaszamok] DROP CONSTRAINT [DF_KRT_Bankszamlaszamok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Banks__ErvKe__257187A8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Bankszamlaszamok] DROP CONSTRAINT [DF__KRT_Banks__ErvKe__257187A8]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Banksza__Ver__247D636F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Bankszamlaszamok] DROP CONSTRAINT [DF__KRT_Banksza__Ver__247D636F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Bankszam__Id__23893F36]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Bankszamlaszamok] DROP CONSTRAINT [DF__KRT_Bankszam__Id__23893F36]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Bankszamlaszamok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Bankszamlaszamok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Bankszamlaszamok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Bankszamlaszamok](
	[Id] [uniqueidentifier] NOT NULL,
	[Partner_Id] [uniqueidentifier] NULL,
	[Bankszamlaszam] [varchar](34) COLLATE Hungarian_CI_AS NOT NULL,
	[Partner_Id_Bank] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_BANKSZAMLASZAMOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Bankszam__Id__23893F36]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Bankszamlaszamok] ADD  CONSTRAINT [DF__KRT_Bankszam__Id__23893F36]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Banksza__Ver__247D636F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Bankszamlaszamok] ADD  CONSTRAINT [DF__KRT_Banksza__Ver__247D636F]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Banks__ErvKe__257187A8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Bankszamlaszamok] ADD  CONSTRAINT [DF__KRT_Banks__ErvKe__257187A8]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Bankszamlaszamok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Bankszamlaszamok] ADD  CONSTRAINT [DF_KRT_Bankszamlaszamok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Banks__Letre__2759D01A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Bankszamlaszamok] ADD  CONSTRAINT [DF__KRT_Banks__Letre__2759D01A]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_BANKSZAMLASZAMOK_BANK_KRT_PARTNEREK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Bankszamlaszamok]'))
ALTER TABLE [dbo].[KRT_Bankszamlaszamok]  WITH CHECK ADD  CONSTRAINT [FK_KRT_BANKSZAMLASZAMOK_BANK_KRT_PARTNEREK] FOREIGN KEY([Partner_Id_Bank])
REFERENCES [dbo].[KRT_Partnerek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_BANKSZAMLASZAMOK_BANK_KRT_PARTNEREK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Bankszamlaszamok]'))
ALTER TABLE [dbo].[KRT_Bankszamlaszamok] CHECK CONSTRAINT [FK_KRT_BANKSZAMLASZAMOK_BANK_KRT_PARTNEREK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_BANKSZAMLASZAMOK_KRT_PARTNEREK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Bankszamlaszamok]'))
ALTER TABLE [dbo].[KRT_Bankszamlaszamok]  WITH CHECK ADD  CONSTRAINT [FK_KRT_BANKSZAMLASZAMOK_KRT_PARTNEREK] FOREIGN KEY([Partner_Id])
REFERENCES [dbo].[KRT_Partnerek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_BANKSZAMLASZAMOK_KRT_PARTNEREK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Bankszamlaszamok]'))
ALTER TABLE [dbo].[KRT_Bankszamlaszamok] CHECK CONSTRAINT [FK_KRT_BANKSZAMLASZAMOK_KRT_PARTNEREK]
GO
