IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodSavok]') AND name = N'Sav_Vege_I')
DROP INDEX [Sav_Vege_I] ON [dbo].[KRT_BarkodSavok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodSavok]') AND name = N'Sav_Kezd_I')
DROP INDEX [Sav_Kezd_I] ON [dbo].[KRT_BarkodSavok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodSavok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_BarkodSavok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodSavok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_BarkodSavok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_BarkodSa__Id__34B3CB38]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_BarkodS__Org__35A7EF71]  DEFAULT ('00000000-0000-0000-0000-000000000000'),
	[Csoport_Id_Felelos] [uniqueidentifier] NOT NULL,
	[SavKezd] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[SavVege] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[SavType] [char](1) COLLATE Hungarian_CI_AS NULL,
	[SavAllapot] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_BarkodS__Ver__369C13AA]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Barko__ErvKe__379037E3]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_BarkodSavok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Barko__Letre__39788055]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_BARKODSAVOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodSavok]') AND name = N'Sav_Kezd_I')
CREATE NONCLUSTERED INDEX [Sav_Kezd_I] ON [dbo].[KRT_BarkodSavok]
(
	[SavKezd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodSavok]') AND name = N'Sav_Vege_I')
CREATE NONCLUSTERED INDEX [Sav_Vege_I] ON [dbo].[KRT_BarkodSavok]
(
	[SavVege] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
