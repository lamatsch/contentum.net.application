IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Barkodok]') AND name = N'Barkod_UK')
DROP INDEX [Barkod_UK] ON [dbo].[KRT_Barkodok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Barkodok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Barkodok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Barkodok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Barkodok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Barkodok__Id__2A363CC5]  DEFAULT (newsequentialid()),
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KodType] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Barkodo__Ver__2B2A60FE]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Barko__ErvKe__2C1E8537]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Barkodok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Barko__Letre__2E06CDA9]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_BARKODOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Barkodok]') AND name = N'Barkod_UK')
CREATE NONCLUSTERED INDEX [Barkod_UK] ON [dbo].[KRT_Barkodok]
(
	[Kod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
