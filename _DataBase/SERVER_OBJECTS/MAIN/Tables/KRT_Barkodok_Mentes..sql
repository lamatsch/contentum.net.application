IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Barko__Letre__31D75E8D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Barkodok_Mentes] DROP CONSTRAINT [DF__KRT_Barko__Letre__31D75E8D]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Barko__ErvKe__30E33A54]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Barkodok_Mentes] DROP CONSTRAINT [DF__KRT_Barko__ErvKe__30E33A54]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Barkodo__Ver__2FEF161B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Barkodok_Mentes] DROP CONSTRAINT [DF__KRT_Barkodo__Ver__2FEF161B]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Barkodok_Mentes]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Barkodok_Mentes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Barkodok_Mentes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Barkodok_Mentes](
	[Id] [uniqueidentifier] NOT NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KodType] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Barkodo__Ver__2FEF161B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Barkodok_Mentes] ADD  CONSTRAINT [DF__KRT_Barkodo__Ver__2FEF161B]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Barko__ErvKe__30E33A54]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Barkodok_Mentes] ADD  CONSTRAINT [DF__KRT_Barko__ErvKe__30E33A54]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Barko__Letre__31D75E8D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Barkodok_Mentes] ADD  CONSTRAINT [DF__KRT_Barko__Letre__31D75E8D]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
