IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_CIME_CIM_KOZTE_KRT_KOZT]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]'))
ALTER TABLE [dbo].[KRT_Cimek] DROP CONSTRAINT [FK_KRT_CIME_CIM_KOZTE_KRT_KOZT]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Cim_telepules_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]'))
ALTER TABLE [dbo].[KRT_Cimek] DROP CONSTRAINT [Cim_telepules_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Cim_Orszag_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]'))
ALTER TABLE [dbo].[KRT_Cimek] DROP CONSTRAINT [Cim_Orszag_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Cim_Kozterulet_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]'))
ALTER TABLE [dbo].[KRT_Cimek] DROP CONSTRAINT [Cim_Kozterulet_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]') AND name = N'KRT_Cimek')
DROP INDEX [KRT_Cimek] ON [dbo].[KRT_Cimek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]') AND name = N'Cimek_TelepKozter')
DROP INDEX [Cimek_TelepKozter] ON [dbo].[KRT_Cimek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]') AND name = N'Cimek_LetrehozasIdo')
DROP INDEX [Cimek_LetrehozasIdo] ON [dbo].[KRT_Cimek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]') AND name = N'Cimek_ErvKezdVege')
DROP INDEX [Cimek_ErvKezdVege] ON [dbo].[KRT_Cimek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]') AND name = N'_dta_index_KRT_Cimek_5_1550016653__K7')
DROP INDEX [_dta_index_KRT_Cimek_5_1550016653__K7] ON [dbo].[KRT_Cimek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Cimek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Cimek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Cimek__Id__3C54ED00]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NULL,
	[Kategoria] [char](1) COLLATE Hungarian_CI_AS NOT NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KulsoAzonositok] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Forras] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Orszag_Id] [uniqueidentifier] NULL,
	[OrszagNev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Telepules_Id] [uniqueidentifier] NULL,
	[TelepulesNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[IRSZ] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[CimTobbi] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Kozterulet_Id] [uniqueidentifier] NULL,
	[KozteruletNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KozteruletTipus_Id] [uniqueidentifier] NULL,
	[KozteruletTipusNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Hazszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Hazszamig] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[HazszamBetujel] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[MindketOldal] [char](1) COLLATE Hungarian_CI_AS NULL,
	[HRSZ] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Lepcsohaz] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Szint] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Ajto] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[AjtoBetujel] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Tobbi] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Cimek__Ver__3D491139]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Cimek__ErvKe__3E3D3572]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Cimek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Cimek__Letre__40257DE4]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [CIM_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]') AND name = N'_dta_index_KRT_Cimek_5_1550016653__K7')
CREATE NONCLUSTERED INDEX [_dta_index_KRT_Cimek_5_1550016653__K7] ON [dbo].[KRT_Cimek]
(
	[Forras] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]') AND name = N'Cimek_ErvKezdVege')
CREATE NONCLUSTERED INDEX [Cimek_ErvKezdVege] ON [dbo].[KRT_Cimek]
(
	[ErvKezd] ASC,
	[ErvVege] ASC,
	[LetrehozasIdo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]') AND name = N'Cimek_LetrehozasIdo')
CREATE NONCLUSTERED INDEX [Cimek_LetrehozasIdo] ON [dbo].[KRT_Cimek]
(
	[LetrehozasIdo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]') AND name = N'Cimek_TelepKozter')
CREATE NONCLUSTERED INDEX [Cimek_TelepKozter] ON [dbo].[KRT_Cimek]
(
	[TelepulesNev] ASC,
	[KozteruletNev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]') AND name = N'KRT_Cimek')
CREATE NONCLUSTERED INDEX [KRT_Cimek] ON [dbo].[KRT_Cimek]
(
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Cim_Kozterulet_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]'))
ALTER TABLE [dbo].[KRT_Cimek]  WITH CHECK ADD  CONSTRAINT [Cim_Kozterulet_FK] FOREIGN KEY([Kozterulet_Id])
REFERENCES [dbo].[KRT_Kozteruletek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Cim_Kozterulet_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]'))
ALTER TABLE [dbo].[KRT_Cimek] CHECK CONSTRAINT [Cim_Kozterulet_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Cim_Orszag_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]'))
ALTER TABLE [dbo].[KRT_Cimek]  WITH CHECK ADD  CONSTRAINT [Cim_Orszag_FK] FOREIGN KEY([Orszag_Id])
REFERENCES [dbo].[KRT_Orszagok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Cim_Orszag_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]'))
ALTER TABLE [dbo].[KRT_Cimek] CHECK CONSTRAINT [Cim_Orszag_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Cim_telepules_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]'))
ALTER TABLE [dbo].[KRT_Cimek]  WITH CHECK ADD  CONSTRAINT [Cim_telepules_FK] FOREIGN KEY([Telepules_Id])
REFERENCES [dbo].[KRT_Telepulesek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Cim_telepules_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]'))
ALTER TABLE [dbo].[KRT_Cimek] CHECK CONSTRAINT [Cim_telepules_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_CIME_CIM_KOZTE_KRT_KOZT]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]'))
ALTER TABLE [dbo].[KRT_Cimek]  WITH CHECK ADD  CONSTRAINT [FK_KRT_CIME_CIM_KOZTE_KRT_KOZT] FOREIGN KEY([KozteruletTipus_Id])
REFERENCES [dbo].[KRT_KozteruletTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_CIME_CIM_KOZTE_KRT_KOZT]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Cimek]'))
ALTER TABLE [dbo].[KRT_Cimek] CHECK CONSTRAINT [FK_KRT_CIME_CIM_KOZTE_KRT_KOZT]
GO
