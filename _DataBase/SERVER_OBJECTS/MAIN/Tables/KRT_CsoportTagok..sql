IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[CsoportTag_Csoport_Jogalany_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagok]'))
ALTER TABLE [dbo].[KRT_CsoportTagok] DROP CONSTRAINT [CsoportTag_Csoport_Jogalany_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Csoporttag_Csoport_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagok]'))
ALTER TABLE [dbo].[KRT_CsoportTagok] DROP CONSTRAINT [Csoporttag_Csoport_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagok]') AND name = N'KRT_CSOPORTTAGOK_ID_JOGALANY')
DROP INDEX [KRT_CSOPORTTAGOK_ID_JOGALANY] ON [dbo].[KRT_CsoportTagok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagok]') AND name = N'KRT_CSOPORTOK_FK_I')
DROP INDEX [KRT_CSOPORTOK_FK_I] ON [dbo].[KRT_CsoportTagok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_CsoportTagok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_CsoportTagok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_CsoportT__Id__49AEE81E]  DEFAULT (newsequentialid()),
	[Tipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Csoport_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Jogalany] [uniqueidentifier] NULL,
	[ObjTip_Id_Jogalany] [uniqueidentifier] NULL,
	[ErtesitesMailCim] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[ErtesitesKell] [char](1) COLLATE Hungarian_CI_AS NULL,
	[System] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Orokolheto] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__KRT_Csopo__Oroko__4AA30C57]  DEFAULT ('0'),
	[ObjektumTulajdonos] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Csoport__Ver__4B973090]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Csopo__ErvKe__4C8B54C9]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_CsoportTagok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Csopo__Letre__4E739D3B]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_CSOPORTTAGOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagok]') AND name = N'KRT_CSOPORTOK_FK_I')
CREATE NONCLUSTERED INDEX [KRT_CSOPORTOK_FK_I] ON [dbo].[KRT_CsoportTagok]
(
	[Csoport_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagok]') AND name = N'KRT_CSOPORTTAGOK_ID_JOGALANY')
CREATE NONCLUSTERED INDEX [KRT_CSOPORTTAGOK_ID_JOGALANY] ON [dbo].[KRT_CsoportTagok]
(
	[Csoport_Id] ASC,
	[Csoport_Id_Jogalany] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Csoporttag_Csoport_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagok]'))
ALTER TABLE [dbo].[KRT_CsoportTagok]  WITH CHECK ADD  CONSTRAINT [Csoporttag_Csoport_FK] FOREIGN KEY([Csoport_Id])
REFERENCES [dbo].[KRT_Csoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Csoporttag_Csoport_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagok]'))
ALTER TABLE [dbo].[KRT_CsoportTagok] CHECK CONSTRAINT [Csoporttag_Csoport_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[CsoportTag_Csoport_Jogalany_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagok]'))
ALTER TABLE [dbo].[KRT_CsoportTagok]  WITH CHECK ADD  CONSTRAINT [CsoportTag_Csoport_Jogalany_FK] FOREIGN KEY([Csoport_Id_Jogalany])
REFERENCES [dbo].[KRT_Csoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[CsoportTag_Csoport_Jogalany_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_CsoportTagok]'))
ALTER TABLE [dbo].[KRT_CsoportTagok] CHECK CONSTRAINT [CsoportTag_Csoport_Jogalany_FK]
GO
