IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Csoport_ObjTipus_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Csoportok]'))
ALTER TABLE [dbo].[KRT_Csoportok] DROP CONSTRAINT [Csoport_ObjTipus_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Csoportok]') AND name = N'TMP_KRTCSOP_TIPUS')
DROP INDEX [TMP_KRTCSOP_TIPUS] ON [dbo].[KRT_Csoportok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Csoportok]') AND name = N'TMP_KRTCSOP_ERVVDAT')
DROP INDEX [TMP_KRTCSOP_ERVVDAT] ON [dbo].[KRT_Csoportok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Csoportok]') AND name = N'TMP_KRTCSOP_ERVKDAT')
DROP INDEX [TMP_KRTCSOP_ERVKDAT] ON [dbo].[KRT_Csoportok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Csoportok]') AND name = N'IX_Csoportok_Jogalany')
DROP INDEX [IX_Csoportok_Jogalany] ON [dbo].[KRT_Csoportok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Csoportok]') AND name = N'Csoportok_UK')
DROP INDEX [Csoportok_UK] ON [dbo].[KRT_Csoportok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Csoportok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Csoportok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Csoportok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Csoportok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Csoporto__Id__4301EA8F]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NOT NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Jogalany] [char](1) COLLATE Hungarian_CI_AS NULL,
	[ErtesitesEmail] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[System] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Adatforras] [char](1) COLLATE Hungarian_CI_AS NULL,
	[ObjTipus_Id_Szulo] [uniqueidentifier] NULL,
	[Kiszolgalhato] [char](1) COLLATE Hungarian_CI_AS NULL,
	[JogosultsagOroklesMod] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Csoport__Ver__43F60EC8]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Csopo__ErvKe__44EA3301]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Csoportok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Csopo__Letre__46D27B73]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_CSOPORTOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Csoportok]') AND name = N'Csoportok_UK')
CREATE NONCLUSTERED INDEX [Csoportok_UK] ON [dbo].[KRT_Csoportok]
(
	[Org] ASC,
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Csoportok]') AND name = N'IX_Csoportok_Jogalany')
CREATE NONCLUSTERED INDEX [IX_Csoportok_Jogalany] ON [dbo].[KRT_Csoportok]
(
	[Jogalany] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Csoportok]') AND name = N'TMP_KRTCSOP_ERVKDAT')
CREATE NONCLUSTERED INDEX [TMP_KRTCSOP_ERVKDAT] ON [dbo].[KRT_Csoportok]
(
	[ErvKezd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Csoportok]') AND name = N'TMP_KRTCSOP_ERVVDAT')
CREATE NONCLUSTERED INDEX [TMP_KRTCSOP_ERVVDAT] ON [dbo].[KRT_Csoportok]
(
	[ErvVege] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Csoportok]') AND name = N'TMP_KRTCSOP_TIPUS')
CREATE NONCLUSTERED INDEX [TMP_KRTCSOP_TIPUS] ON [dbo].[KRT_Csoportok]
(
	[Tipus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Csoport_ObjTipus_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Csoportok]'))
ALTER TABLE [dbo].[KRT_Csoportok]  WITH CHECK ADD  CONSTRAINT [Csoport_ObjTipus_FK] FOREIGN KEY([ObjTipus_Id_Szulo])
REFERENCES [dbo].[KRT_ObjTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Csoport_ObjTipus_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Csoportok]'))
ALTER TABLE [dbo].[KRT_Csoportok] CHECK CONSTRAINT [Csoport_ObjTipus_FK]
GO
