IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[DokuAlairasok_Dokumentum_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumAlairasok]'))
ALTER TABLE [dbo].[KRT_DokumentumAlairasok] DROP CONSTRAINT [DokuAlairasok_Dokumentum_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[DokuAlairas_AlairasSzabaly_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumAlairasok]'))
ALTER TABLE [dbo].[KRT_DokumentumAlairasok] DROP CONSTRAINT [DokuAlairas_AlairasSzabaly_FK]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumAlairasok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_DokumentumAlairasok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumAlairasok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_DokumentumAlairasok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Dokument__Id__0E4EF685]  DEFAULT (newsequentialid()),
	[Dokumentum_Id_Alairt] [uniqueidentifier] NULL,
	[AlairasMod] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[AlairasSzabaly_Id] [uniqueidentifier] NULL,
	[AlairasRendben] [char](1) COLLATE Hungarian_CI_AS NOT NULL,
	[KivarasiIdoVege] [datetime] NOT NULL CONSTRAINT [DF__KRT_Dokum__Kivar__0F431ABE]  DEFAULT (getdate()),
	[AlairasVeglegRendben] [char](1) COLLATE Hungarian_CI_AS NOT NULL,
	[Idopecset] [datetime] NULL,
	[Csoport_Id_Alairo] [uniqueidentifier] NULL,
	[AlairoSzemely] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AlairasTulajdonos] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Tanusitvany_Id] [uniqueidentifier] NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Dokumen__Ver__10373EF7]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Dokum__ErvKe__112B6330]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Dokum__Letre__121F8769]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_DOKUMENTUMALAIRASOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[DokuAlairas_AlairasSzabaly_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumAlairasok]'))
ALTER TABLE [dbo].[KRT_DokumentumAlairasok]  WITH CHECK ADD  CONSTRAINT [DokuAlairas_AlairasSzabaly_FK] FOREIGN KEY([AlairasSzabaly_Id])
REFERENCES [dbo].[KRT_AlairasSzabalyok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[DokuAlairas_AlairasSzabaly_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumAlairasok]'))
ALTER TABLE [dbo].[KRT_DokumentumAlairasok] CHECK CONSTRAINT [DokuAlairas_AlairasSzabaly_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[DokuAlairasok_Dokumentum_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumAlairasok]'))
ALTER TABLE [dbo].[KRT_DokumentumAlairasok]  WITH CHECK ADD  CONSTRAINT [DokuAlairasok_Dokumentum_FK] FOREIGN KEY([Dokumentum_Id_Alairt])
REFERENCES [dbo].[KRT_Dokumentumok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[DokuAlairasok_Dokumentum_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumAlairasok]'))
ALTER TABLE [dbo].[KRT_DokumentumAlairasok] CHECK CONSTRAINT [DokuAlairasok_Dokumentum_FK]
GO
