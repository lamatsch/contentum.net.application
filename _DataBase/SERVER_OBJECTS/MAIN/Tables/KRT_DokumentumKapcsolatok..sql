IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DOK_KAPCSOLATOK_FO_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumKapcsolatok]'))
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] DROP CONSTRAINT [KRT_DOK_KAPCSOLATOK_FO_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DOK_KAPCSOLATOK_AL_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumKapcsolatok]'))
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] DROP CONSTRAINT [KRT_DOK_KAPCSOLATOK_AL_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokum__Letre__17D860BF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] DROP CONSTRAINT [DF__KRT_Dokum__Letre__17D860BF]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_DokumentumKapcsolatok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] DROP CONSTRAINT [DF_KRT_DokumentumKapcsolatok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokum__ErvKe__15F0184D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] DROP CONSTRAINT [DF__KRT_Dokum__ErvKe__15F0184D]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokumen__Ver__14FBF414]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] DROP CONSTRAINT [DF__KRT_Dokumen__Ver__14FBF414]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokum__DokuK__1407CFDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] DROP CONSTRAINT [DF__KRT_Dokum__DokuK__1407CFDB]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokument__Id__1313ABA2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] DROP CONSTRAINT [DF__KRT_Dokument__Id__1313ABA2]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumKapcsolatok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_DokumentumKapcsolatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumKapcsolatok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_DokumentumKapcsolatok](
	[Id] [uniqueidentifier] NOT NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[Dokumentum_Id_Fo] [uniqueidentifier] NULL,
	[Dokumentum_Id_Al] [uniqueidentifier] NULL,
	[DokumentumKapcsolatJelleg] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[DokuKapcsolatSorrend] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KRT_DOK_KAPCSOLATOK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokument__Id__1313ABA2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] ADD  CONSTRAINT [DF__KRT_Dokument__Id__1313ABA2]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokum__DokuK__1407CFDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] ADD  CONSTRAINT [DF__KRT_Dokum__DokuK__1407CFDB]  DEFAULT ((1)) FOR [DokuKapcsolatSorrend]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokumen__Ver__14FBF414]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] ADD  CONSTRAINT [DF__KRT_Dokumen__Ver__14FBF414]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokum__ErvKe__15F0184D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] ADD  CONSTRAINT [DF__KRT_Dokum__ErvKe__15F0184D]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_DokumentumKapcsolatok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] ADD  CONSTRAINT [DF_KRT_DokumentumKapcsolatok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokum__Letre__17D860BF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] ADD  CONSTRAINT [DF__KRT_Dokum__Letre__17D860BF]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DOK_KAPCSOLATOK_AL_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumKapcsolatok]'))
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok]  WITH CHECK ADD  CONSTRAINT [KRT_DOK_KAPCSOLATOK_AL_FK] FOREIGN KEY([Dokumentum_Id_Al])
REFERENCES [dbo].[KRT_Dokumentumok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DOK_KAPCSOLATOK_AL_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumKapcsolatok]'))
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] CHECK CONSTRAINT [KRT_DOK_KAPCSOLATOK_AL_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DOK_KAPCSOLATOK_FO_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumKapcsolatok]'))
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok]  WITH CHECK ADD  CONSTRAINT [KRT_DOK_KAPCSOLATOK_FO_FK] FOREIGN KEY([Dokumentum_Id_Fo])
REFERENCES [dbo].[KRT_Dokumentumok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DOK_KAPCSOLATOK_FO_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumKapcsolatok]'))
ALTER TABLE [dbo].[KRT_DokumentumKapcsolatok] CHECK CONSTRAINT [KRT_DOK_KAPCSOLATOK_FO_FK]
GO
