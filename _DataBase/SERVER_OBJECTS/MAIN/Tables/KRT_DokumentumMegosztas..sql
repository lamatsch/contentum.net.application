IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ALAIRMEGOSZT_CSOPORTOK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumMegosztas]'))
ALTER TABLE [dbo].[KRT_DokumentumMegosztas] DROP CONSTRAINT [KRT_ALAIRMEGOSZT_CSOPORTOK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[DokuMegosztas_Dokumentum_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumMegosztas]'))
ALTER TABLE [dbo].[KRT_DokumentumMegosztas] DROP CONSTRAINT [DokuMegosztas_Dokumentum_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokum__Letre__1BA8F1A3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumMegosztas] DROP CONSTRAINT [DF__KRT_Dokum__Letre__1BA8F1A3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokum__ErvKe__1AB4CD6A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumMegosztas] DROP CONSTRAINT [DF__KRT_Dokum__ErvKe__1AB4CD6A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokumen__Ver__19C0A931]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumMegosztas] DROP CONSTRAINT [DF__KRT_Dokumen__Ver__19C0A931]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokument__Id__18CC84F8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumMegosztas] DROP CONSTRAINT [DF__KRT_Dokument__Id__18CC84F8]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumMegosztas]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_DokumentumMegosztas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumMegosztas]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_DokumentumMegosztas](
	[Id] [uniqueidentifier] NOT NULL,
	[Dokumentum_Id_Titkos] [uniqueidentifier] NULL,
	[Csoport_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_DOKUMENTUMMEGOSZTAS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokument__Id__18CC84F8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumMegosztas] ADD  CONSTRAINT [DF__KRT_Dokument__Id__18CC84F8]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokumen__Ver__19C0A931]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumMegosztas] ADD  CONSTRAINT [DF__KRT_Dokumen__Ver__19C0A931]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokum__ErvKe__1AB4CD6A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumMegosztas] ADD  CONSTRAINT [DF__KRT_Dokum__ErvKe__1AB4CD6A]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Dokum__Letre__1BA8F1A3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_DokumentumMegosztas] ADD  CONSTRAINT [DF__KRT_Dokum__Letre__1BA8F1A3]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[DokuMegosztas_Dokumentum_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumMegosztas]'))
ALTER TABLE [dbo].[KRT_DokumentumMegosztas]  WITH CHECK ADD  CONSTRAINT [DokuMegosztas_Dokumentum_FK] FOREIGN KEY([Dokumentum_Id_Titkos])
REFERENCES [dbo].[KRT_Dokumentumok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[DokuMegosztas_Dokumentum_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumMegosztas]'))
ALTER TABLE [dbo].[KRT_DokumentumMegosztas] CHECK CONSTRAINT [DokuMegosztas_Dokumentum_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ALAIRMEGOSZT_CSOPORTOK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumMegosztas]'))
ALTER TABLE [dbo].[KRT_DokumentumMegosztas]  WITH CHECK ADD  CONSTRAINT [KRT_ALAIRMEGOSZT_CSOPORTOK] FOREIGN KEY([Csoport_Id])
REFERENCES [dbo].[KRT_Csoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ALAIRMEGOSZT_CSOPORTOK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumMegosztas]'))
ALTER TABLE [dbo].[KRT_DokumentumMegosztas] CHECK CONSTRAINT [KRT_ALAIRMEGOSZT_CSOPORTOK]
GO
