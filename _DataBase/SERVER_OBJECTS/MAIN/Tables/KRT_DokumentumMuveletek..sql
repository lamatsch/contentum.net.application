IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DOKUMUV_FOLYAMAT_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumMuveletek]'))
ALTER TABLE [dbo].[KRT_DokumentumMuveletek] DROP CONSTRAINT [KRT_DOKUMUV_FOLYAMAT_FK]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumMuveletek]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_DokumentumMuveletek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumMuveletek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_DokumentumMuveletek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Dokument__Id__5708E33C]  DEFAULT (newsequentialid()),
	[Folyamat_Id] [uniqueidentifier] NULL,
	[MuveletKod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[AlairoSzerepJel] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Dokumen__Ver__57FD0775]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Dokum__ErvKe__58F12BAE]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Dokum__Letre__59E54FE7]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_DOKUMENTUMMUVELETEK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DOKUMUV_FOLYAMAT_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumMuveletek]'))
ALTER TABLE [dbo].[KRT_DokumentumMuveletek]  WITH CHECK ADD  CONSTRAINT [KRT_DOKUMUV_FOLYAMAT_FK] FOREIGN KEY([Folyamat_Id])
REFERENCES [dbo].[KRT_Folyamatok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DOKUMUV_FOLYAMAT_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumMuveletek]'))
ALTER TABLE [dbo].[KRT_DokumentumMuveletek] CHECK CONSTRAINT [KRT_DOKUMUV_FOLYAMAT_FK]
GO
