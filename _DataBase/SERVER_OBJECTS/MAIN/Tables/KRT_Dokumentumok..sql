IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DOKUMENTUMOK_OSDOKU_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]'))
ALTER TABLE [dbo].[KRT_Dokumentumok] DROP CONSTRAINT [KRT_DOKUMENTUMOK_OSDOKU_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DOKUMENTUMOK_KOVETKEZO_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]'))
ALTER TABLE [dbo].[KRT_Dokumentumok] DROP CONSTRAINT [KRT_DOKUMENTUMOK_KOVETKEZO_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ALKALMAZAS_DOKU_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]'))
ALTER TABLE [dbo].[KRT_Dokumentumok] DROP CONSTRAINT [KRT_ALKALMAZAS_DOKU_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]') AND name = N'IX_Dokumentumok_KivHash')
DROP INDEX [IX_Dokumentumok_KivHash] ON [dbo].[KRT_Dokumentumok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]') AND name = N'IX_Dokumentumok_FajlNev')
DROP INDEX [IX_Dokumentumok_FajlNev] ON [dbo].[KRT_Dokumentumok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]') AND name = N'IX_Dokumentumok_ExtLink')
DROP INDEX [IX_Dokumentumok_ExtLink] ON [dbo].[KRT_Dokumentumok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Dokumentumok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Dokumentumok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Dokument__Id__5CC1BC92]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Dokumen__Org__5DB5E0CB]  DEFAULT ('00000000-0000-0000-0000-000000000000'),
	[FajlNev] [nvarchar](400) COLLATE Hungarian_CI_AS NOT NULL,
	[VerzioJel] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Formatum] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Meret] [int] NULL,
	[TartalomHash] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AlairtTartalomHash] [varbinary](4000) NULL,
	[KivonatHash] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Dokumentum_Id] [uniqueidentifier] NULL,
	[Dokumentum_Id_Kovetkezo] [uniqueidentifier] NULL,
	[Alkalmazas_Id] [uniqueidentifier] NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NOT NULL,
	[KulsoAzonositok] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[External_Source] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[External_Link] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[External_Id] [uniqueidentifier] NULL,
	[External_Info] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[CheckedOut] [uniqueidentifier] NULL,
	[CheckedOutTime] [datetime] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[OCRPrioritas] [int] NULL,
	[OCRAllapot] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[WorkFlowAllapot] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Megnyithato] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Olvashato] [char](1) COLLATE Hungarian_CI_AS NULL,
	[ElektronikusAlairas] [nvarchar](64) COLLATE Hungarian_CS_AS NULL CONSTRAINT [DF__KRT_Dokum__Elekt__5EAA0504]  DEFAULT ('0'),
	[AlairasFelulvizsgalat] [datetime] NULL,
	[Titkositas] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__KRT_Dokum__Titko__5F9E293D]  DEFAULT ('0'),
	[SablonAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Dokumen__Ver__60924D76]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Dokum__ErvKe__618671AF]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Dokumentumok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Dokum__Letre__636EBA21]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_Id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_Id] [uniqueidentifier] NULL,
	[UIAccessLog_Id] [uniqueidentifier] NULL,
 CONSTRAINT [KRT_DOKUMENTUMOK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]') AND name = N'IX_Dokumentumok_ExtLink')
CREATE NONCLUSTERED INDEX [IX_Dokumentumok_ExtLink] ON [dbo].[KRT_Dokumentumok]
(
	[External_Link] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]') AND name = N'IX_Dokumentumok_FajlNev')
CREATE NONCLUSTERED INDEX [IX_Dokumentumok_FajlNev] ON [dbo].[KRT_Dokumentumok]
(
	[FajlNev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]') AND name = N'IX_Dokumentumok_KivHash')
CREATE NONCLUSTERED INDEX [IX_Dokumentumok_KivHash] ON [dbo].[KRT_Dokumentumok]
(
	[KivonatHash] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ALKALMAZAS_DOKU_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]'))
ALTER TABLE [dbo].[KRT_Dokumentumok]  WITH CHECK ADD  CONSTRAINT [KRT_ALKALMAZAS_DOKU_FK] FOREIGN KEY([Alkalmazas_Id])
REFERENCES [dbo].[KRT_Alkalmazasok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ALKALMAZAS_DOKU_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]'))
ALTER TABLE [dbo].[KRT_Dokumentumok] CHECK CONSTRAINT [KRT_ALKALMAZAS_DOKU_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DOKUMENTUMOK_KOVETKEZO_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]'))
ALTER TABLE [dbo].[KRT_Dokumentumok]  WITH CHECK ADD  CONSTRAINT [KRT_DOKUMENTUMOK_KOVETKEZO_FK] FOREIGN KEY([Dokumentum_Id_Kovetkezo])
REFERENCES [dbo].[KRT_Dokumentumok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DOKUMENTUMOK_KOVETKEZO_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]'))
ALTER TABLE [dbo].[KRT_Dokumentumok] CHECK CONSTRAINT [KRT_DOKUMENTUMOK_KOVETKEZO_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DOKUMENTUMOK_OSDOKU_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]'))
ALTER TABLE [dbo].[KRT_Dokumentumok]  WITH CHECK ADD  CONSTRAINT [KRT_DOKUMENTUMOK_OSDOKU_FK] FOREIGN KEY([Dokumentum_Id])
REFERENCES [dbo].[KRT_Dokumentumok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DOKUMENTUMOK_OSDOKU_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Dokumentumok]'))
ALTER TABLE [dbo].[KRT_Dokumentumok] CHECK CONSTRAINT [KRT_DOKUMENTUMOK_OSDOKU_FK]
GO
