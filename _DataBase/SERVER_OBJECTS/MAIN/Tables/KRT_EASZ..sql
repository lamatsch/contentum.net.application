IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZ_EASZTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_EASZ]'))
ALTER TABLE [dbo].[KRT_EASZ] DROP CONSTRAINT [KRT_EASZ_EASZTIP_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZ__Letreh__1F798287]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZ] DROP CONSTRAINT [DF__KRT_EASZ__Letreh__1F798287]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZ__ErvKez__1E855E4E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZ] DROP CONSTRAINT [DF__KRT_EASZ__ErvKez__1E855E4E]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZ__Ver__1D913A15]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZ] DROP CONSTRAINT [DF__KRT_EASZ__Ver__1D913A15]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZ__Id__1C9D15DC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZ] DROP CONSTRAINT [DF__KRT_EASZ__Id__1C9D15DC]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZ]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_EASZ]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZ]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_EASZ](
	[Id] [uniqueidentifier] NOT NULL,
	[EASZKod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[EASZTipus_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_EASZ] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZ__Id__1C9D15DC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZ] ADD  CONSTRAINT [DF__KRT_EASZ__Id__1C9D15DC]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZ__Ver__1D913A15]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZ] ADD  CONSTRAINT [DF__KRT_EASZ__Ver__1D913A15]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZ__ErvKez__1E855E4E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZ] ADD  CONSTRAINT [DF__KRT_EASZ__ErvKez__1E855E4E]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZ__Letreh__1F798287]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZ] ADD  CONSTRAINT [DF__KRT_EASZ__Letreh__1F798287]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZ_EASZTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_EASZ]'))
ALTER TABLE [dbo].[KRT_EASZ]  WITH CHECK ADD  CONSTRAINT [KRT_EASZ_EASZTIP_FK] FOREIGN KEY([EASZTipus_Id])
REFERENCES [dbo].[KRT_EASZTipus] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZ_EASZTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_EASZ]'))
ALTER TABLE [dbo].[KRT_EASZ] CHECK CONSTRAINT [KRT_EASZ_EASZTIP_FK]
GO
