IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZTAN_TANUSITV_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_EASZTanusitvanyok]'))
ALTER TABLE [dbo].[KRT_EASZTanusitvanyok] DROP CONSTRAINT [KRT_EASZTAN_TANUSITV_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZTAN_EASZ_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_EASZTanusitvanyok]'))
ALTER TABLE [dbo].[KRT_EASZTanusitvanyok] DROP CONSTRAINT [KRT_EASZTAN_EASZ_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZT__Letre__6B0FDBE9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTanusitvanyok] DROP CONSTRAINT [DF__KRT_EASZT__Letre__6B0FDBE9]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZT__ErvKe__6A1BB7B0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTanusitvanyok] DROP CONSTRAINT [DF__KRT_EASZT__ErvKe__6A1BB7B0]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZTan__Ver__69279377]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTanusitvanyok] DROP CONSTRAINT [DF__KRT_EASZTan__Ver__69279377]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZTanu__Id__68336F3E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTanusitvanyok] DROP CONSTRAINT [DF__KRT_EASZTanu__Id__68336F3E]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZTanusitvanyok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_EASZTanusitvanyok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZTanusitvanyok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_EASZTanusitvanyok](
	[Id] [uniqueidentifier] NOT NULL,
	[EASZ_Id] [uniqueidentifier] NULL,
	[Tanusitvany_Id] [uniqueidentifier] NULL,
	[KonfigTetel] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_EASZTANUSITVANYOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZTanu__Id__68336F3E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTanusitvanyok] ADD  CONSTRAINT [DF__KRT_EASZTanu__Id__68336F3E]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZTan__Ver__69279377]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTanusitvanyok] ADD  CONSTRAINT [DF__KRT_EASZTan__Ver__69279377]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZT__ErvKe__6A1BB7B0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTanusitvanyok] ADD  CONSTRAINT [DF__KRT_EASZT__ErvKe__6A1BB7B0]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZT__Letre__6B0FDBE9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTanusitvanyok] ADD  CONSTRAINT [DF__KRT_EASZT__Letre__6B0FDBE9]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZTAN_EASZ_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_EASZTanusitvanyok]'))
ALTER TABLE [dbo].[KRT_EASZTanusitvanyok]  WITH CHECK ADD  CONSTRAINT [KRT_EASZTAN_EASZ_FK] FOREIGN KEY([EASZ_Id])
REFERENCES [dbo].[KRT_EASZTipus] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZTAN_EASZ_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_EASZTanusitvanyok]'))
ALTER TABLE [dbo].[KRT_EASZTanusitvanyok] CHECK CONSTRAINT [KRT_EASZTAN_EASZ_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZTAN_TANUSITV_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_EASZTanusitvanyok]'))
ALTER TABLE [dbo].[KRT_EASZTanusitvanyok]  WITH CHECK ADD  CONSTRAINT [KRT_EASZTAN_TANUSITV_FK] FOREIGN KEY([Tanusitvany_Id])
REFERENCES [dbo].[KRT_Tanusitvanyok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZTAN_TANUSITV_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_EASZTanusitvanyok]'))
ALTER TABLE [dbo].[KRT_EASZTanusitvanyok] CHECK CONSTRAINT [KRT_EASZTAN_TANUSITV_FK]
GO
