IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZTIP_ALAIRASTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_EASZTipus]'))
ALTER TABLE [dbo].[KRT_EASZTipus] DROP CONSTRAINT [KRT_EASZTIP_ALAIRASTIP_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZT__Letre__70C8B53F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTipus] DROP CONSTRAINT [DF__KRT_EASZT__Letre__70C8B53F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZT__ErvKe__6FD49106]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTipus] DROP CONSTRAINT [DF__KRT_EASZT__ErvKe__6FD49106]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZTip__Ver__6EE06CCD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTipus] DROP CONSTRAINT [DF__KRT_EASZTip__Ver__6EE06CCD]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZTipu__Id__6DEC4894]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTipus] DROP CONSTRAINT [DF__KRT_EASZTipu__Id__6DEC4894]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZTipus]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_EASZTipus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZTipus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_EASZTipus](
	[Id] [uniqueidentifier] NOT NULL,
	[Org] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[AlairasTipus_Id] [uniqueidentifier] NULL,
	[EASZTipusKod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KivarasiIdo] [int] NULL,
	[AktualisEASZVerzio] [int] NULL,
	[URL] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_EASZTIPUS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZTipu__Id__6DEC4894]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTipus] ADD  CONSTRAINT [DF__KRT_EASZTipu__Id__6DEC4894]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZTip__Ver__6EE06CCD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTipus] ADD  CONSTRAINT [DF__KRT_EASZTip__Ver__6EE06CCD]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZT__ErvKe__6FD49106]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTipus] ADD  CONSTRAINT [DF__KRT_EASZT__ErvKe__6FD49106]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_EASZT__Letre__70C8B53F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_EASZTipus] ADD  CONSTRAINT [DF__KRT_EASZT__Letre__70C8B53F]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZTIP_ALAIRASTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_EASZTipus]'))
ALTER TABLE [dbo].[KRT_EASZTipus]  WITH CHECK ADD  CONSTRAINT [KRT_EASZTIP_ALAIRASTIP_FK] FOREIGN KEY([AlairasTipus_Id])
REFERENCES [dbo].[KRT_AlairasTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_EASZTIP_ALAIRASTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_EASZTipus]'))
ALTER TABLE [dbo].[KRT_EASZTipus] CHECK CONSTRAINT [KRT_EASZTIP_ALAIRASTIP_FK]
GO
