IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ErintettTabla_Tranzakcio_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Erintett_Tablak]'))
ALTER TABLE [dbo].[KRT_Erintett_Tablak] DROP CONSTRAINT [ErintettTabla_Tranzakcio_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Erint__Letre__7775B2CE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Erintett_Tablak] DROP CONSTRAINT [DF__KRT_Erint__Letre__7775B2CE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Erintett_Tablak_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Erintett_Tablak] DROP CONSTRAINT [DF_KRT_Erintett_Tablak_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Erint__ErvKe__758D6A5C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Erintett_Tablak] DROP CONSTRAINT [DF__KRT_Erint__ErvKe__758D6A5C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Erintet__Ver__74994623]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Erintett_Tablak] DROP CONSTRAINT [DF__KRT_Erintet__Ver__74994623]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Erintett__Id__73A521EA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Erintett_Tablak] DROP CONSTRAINT [DF__KRT_Erintett__Id__73A521EA]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Erintett_Tablak]') AND name = N'KRT_Erintett_Tabla_Ind01')
DROP INDEX [KRT_Erintett_Tabla_Ind01] ON [dbo].[KRT_Erintett_Tablak]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Erintett_Tablak]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Erintett_Tablak]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Erintett_Tablak]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Erintett_Tablak](
	[Id] [uniqueidentifier] NOT NULL,
	[Tranzakci_id] [uniqueidentifier] NOT NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_ERINTETT_TABLAK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Erintett_Tablak]') AND name = N'KRT_Erintett_Tabla_Ind01')
CREATE NONCLUSTERED INDEX [KRT_Erintett_Tabla_Ind01] ON [dbo].[KRT_Erintett_Tablak]
(
	[ObjTip_Id] ASC,
	[Tranzakci_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Erintett__Id__73A521EA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Erintett_Tablak] ADD  CONSTRAINT [DF__KRT_Erintett__Id__73A521EA]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Erintet__Ver__74994623]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Erintett_Tablak] ADD  CONSTRAINT [DF__KRT_Erintet__Ver__74994623]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Erint__ErvKe__758D6A5C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Erintett_Tablak] ADD  CONSTRAINT [DF__KRT_Erint__ErvKe__758D6A5C]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Erintett_Tablak_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Erintett_Tablak] ADD  CONSTRAINT [DF_KRT_Erintett_Tablak_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Erint__Letre__7775B2CE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Erintett_Tablak] ADD  CONSTRAINT [DF__KRT_Erint__Letre__7775B2CE]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ErintettTabla_Tranzakcio_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Erintett_Tablak]'))
ALTER TABLE [dbo].[KRT_Erintett_Tablak]  WITH CHECK ADD  CONSTRAINT [ErintettTabla_Tranzakcio_FK] FOREIGN KEY([Tranzakci_id])
REFERENCES [dbo].[KRT_Tranzakciok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ErintettTabla_Tranzakcio_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Erintett_Tablak]'))
ALTER TABLE [dbo].[KRT_Erintett_Tablak] CHECK CONSTRAINT [ErintettTabla_Tranzakcio_FK]
GO
