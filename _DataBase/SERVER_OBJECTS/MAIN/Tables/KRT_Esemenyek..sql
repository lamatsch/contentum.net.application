IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Esemeny_Funkcio_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Esemenyek]'))
ALTER TABLE [dbo].[KRT_Esemenyek] DROP CONSTRAINT [Esemeny_Funkcio_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Esemenyek]') AND name = N'IX_Esemenyek_Obj_Id')
DROP INDEX [IX_Esemenyek_Obj_Id] ON [dbo].[KRT_Esemenyek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Esemenyek]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Esemenyek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Esemenyek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Esemenyek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Esemenye__Id__7A521F79]  DEFAULT (newsequentialid()),
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Azonositoja] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Felhasznalo_Id_User] [uniqueidentifier] NULL,
	[Csoport_Id_FelelosUserSzerveze] [uniqueidentifier] NULL,
	[Felhasznalo_Id_Login] [uniqueidentifier] NULL,
	[Csoport_Id_Cel] [uniqueidentifier] NULL,
	[Helyettesites_Id] [uniqueidentifier] NULL,
	[Tranzakcio_Id] [uniqueidentifier] NULL,
	[Funkcio_Id] [uniqueidentifier] NULL,
	[CsoportHierarchia] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[KeresesiFeltetel] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[TalalatokSzama] [int] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Esemeny__Ver__7B4643B2]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Eseme__ErvKe__7C3A67EB]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Esemenyek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Eseme__Letre__7E22B05D]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[Munkaallomas] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[MuveletKimenete] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_KIRL_ESEMENYEK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Esemenyek]') AND name = N'IX_Esemenyek_Obj_Id')
CREATE NONCLUSTERED INDEX [IX_Esemenyek_Obj_Id] ON [dbo].[KRT_Esemenyek]
(
	[Obj_Id] ASC,
	[Id] ASC
)
INCLUDE ( 	[Csoport_Id_FelelosUserSzerveze],
	[Felhasznalo_Id_User],
	[Funkcio_Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Esemeny_Funkcio_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Esemenyek]'))
ALTER TABLE [dbo].[KRT_Esemenyek]  WITH CHECK ADD  CONSTRAINT [Esemeny_Funkcio_FK] FOREIGN KEY([Funkcio_Id])
REFERENCES [dbo].[KRT_Funkciok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Esemeny_Funkcio_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Esemenyek]'))
ALTER TABLE [dbo].[KRT_Esemenyek] CHECK CONSTRAINT [Esemeny_Funkcio_FK]
GO
