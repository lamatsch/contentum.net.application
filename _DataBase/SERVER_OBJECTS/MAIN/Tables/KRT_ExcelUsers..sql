IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ExcelUsers]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_ExcelUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ExcelUsers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_ExcelUsers](
	[id] [uniqueidentifier] NOT NULL,
	[suser_name] [varchar](250) COLLATE Hungarian_CI_AS NULL,
	[user_name] [varchar](50) COLLATE Hungarian_CI_AS NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
