IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Extra__Letre__05C3D225]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Extra_Napok] DROP CONSTRAINT [DF__KRT_Extra__Letre__05C3D225]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Extra_Napok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Extra_Napok] DROP CONSTRAINT [DF_KRT_Extra_Napok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Extra__ErvKe__03DB89B3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Extra_Napok] DROP CONSTRAINT [DF__KRT_Extra__ErvKe__03DB89B3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Extra_N__Ver__02E7657A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Extra_Napok] DROP CONSTRAINT [DF__KRT_Extra_N__Ver__02E7657A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Extra_Na__Id__01F34141]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Extra_Napok] DROP CONSTRAINT [DF__KRT_Extra_Na__Id__01F34141]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Extra_Napok]') AND name = N'UI_KRT_EXTRA_NAPOK')
DROP INDEX [UI_KRT_EXTRA_NAPOK] ON [dbo].[KRT_Extra_Napok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Extra_Napok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Extra_Napok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Extra_Napok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Extra_Napok](
	[Id] [uniqueidentifier] NOT NULL,
	[Datum] [datetime] NULL,
	[Jelzo] [int] NULL,
	[Megjegyzes] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_EXTRA_NAPOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Extra_Napok]') AND name = N'UI_KRT_EXTRA_NAPOK')
CREATE UNIQUE NONCLUSTERED INDEX [UI_KRT_EXTRA_NAPOK] ON [dbo].[KRT_Extra_Napok]
(
	[Datum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Extra_Na__Id__01F34141]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Extra_Napok] ADD  CONSTRAINT [DF__KRT_Extra_Na__Id__01F34141]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Extra_N__Ver__02E7657A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Extra_Napok] ADD  CONSTRAINT [DF__KRT_Extra_N__Ver__02E7657A]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Extra__ErvKe__03DB89B3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Extra_Napok] ADD  CONSTRAINT [DF__KRT_Extra__ErvKe__03DB89B3]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Extra_Napok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Extra_Napok] ADD  CONSTRAINT [DF_KRT_Extra_Napok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Extra__Letre__05C3D225]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Extra_Napok] ADD  CONSTRAINT [DF__KRT_Extra__Letre__05C3D225]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
