IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FELHASZNALO_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloProfilok]'))
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok] DROP CONSTRAINT [KRT_FELHASZNALO_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FelhasznaloProfil_ObjTip_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloProfilok]'))
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok] DROP CONSTRAINT [FelhasznaloProfil_ObjTip_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felha__Letre__243E37A4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok] DROP CONSTRAINT [DF__KRT_Felha__Letre__243E37A4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_FelhasznaloProfilok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok] DROP CONSTRAINT [DF_KRT_FelhasznaloProfilok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felha__ErvKe__2255EF32]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok] DROP CONSTRAINT [DF__KRT_Felha__ErvKe__2255EF32]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felhasz__Ver__2161CAF9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok] DROP CONSTRAINT [DF__KRT_Felhasz__Ver__2161CAF9]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felhaszn__Id__206DA6C0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok] DROP CONSTRAINT [DF__KRT_Felhaszn__Id__206DA6C0]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloProfilok]') AND name = N'KRT_FelhasznaloProfil_Ind01')
DROP INDEX [KRT_FelhasznaloProfil_Ind01] ON [dbo].[KRT_FelhasznaloProfilok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloProfilok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_FelhasznaloProfilok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloProfilok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_FelhasznaloProfilok](
	[Id] [uniqueidentifier] NOT NULL,
	[Org] [uniqueidentifier] NOT NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Felhasznalo_id] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[XML] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_FELHASZNALO_PROFILOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloProfilok]') AND name = N'KRT_FelhasznaloProfil_Ind01')
CREATE NONCLUSTERED INDEX [KRT_FelhasznaloProfil_Ind01] ON [dbo].[KRT_FelhasznaloProfilok]
(
	[Org] ASC,
	[Felhasznalo_id] ASC,
	[Obj_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felhaszn__Id__206DA6C0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok] ADD  CONSTRAINT [DF__KRT_Felhaszn__Id__206DA6C0]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felhasz__Ver__2161CAF9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok] ADD  CONSTRAINT [DF__KRT_Felhasz__Ver__2161CAF9]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felha__ErvKe__2255EF32]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok] ADD  CONSTRAINT [DF__KRT_Felha__ErvKe__2255EF32]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_FelhasznaloProfilok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok] ADD  CONSTRAINT [DF_KRT_FelhasznaloProfilok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felha__Letre__243E37A4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok] ADD  CONSTRAINT [DF__KRT_Felha__Letre__243E37A4]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FelhasznaloProfil_ObjTip_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloProfilok]'))
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok]  WITH CHECK ADD  CONSTRAINT [FelhasznaloProfil_ObjTip_FK] FOREIGN KEY([Obj_Id])
REFERENCES [dbo].[KRT_ObjTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FelhasznaloProfil_ObjTip_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloProfilok]'))
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok] CHECK CONSTRAINT [FelhasznaloProfil_ObjTip_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FELHASZNALO_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloProfilok]'))
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok]  WITH CHECK ADD  CONSTRAINT [KRT_FELHASZNALO_FK] FOREIGN KEY([Felhasznalo_id])
REFERENCES [dbo].[KRT_Felhasznalok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FELHASZNALO_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloProfilok]'))
ALTER TABLE [dbo].[KRT_FelhasznaloProfilok] CHECK CONSTRAINT [KRT_FELHASZNALO_FK]
GO
