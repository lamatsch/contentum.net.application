IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FelhasznSpecMappai_Mappa_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloSpecialisMappai]'))
ALTER TABLE [dbo].[KRT_FelhasznaloSpecialisMappai] DROP CONSTRAINT [FelhasznSpecMappai_Mappa_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felha__Letre__2902ECC1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloSpecialisMappai] DROP CONSTRAINT [DF__KRT_Felha__Letre__2902ECC1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_FelhasznaloSpecialisMappai_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloSpecialisMappai] DROP CONSTRAINT [DF_KRT_FelhasznaloSpecialisMappai_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felha__ErvKe__271AA44F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloSpecialisMappai] DROP CONSTRAINT [DF__KRT_Felha__ErvKe__271AA44F]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felhasz__Ver__26268016]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloSpecialisMappai] DROP CONSTRAINT [DF__KRT_Felhasz__Ver__26268016]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felhaszn__Id__25325BDD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloSpecialisMappai] DROP CONSTRAINT [DF__KRT_Felhaszn__Id__25325BDD]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloSpecialisMappai]') AND name = N'FK_Mappa_Id')
DROP INDEX [FK_Mappa_Id] ON [dbo].[KRT_FelhasznaloSpecialisMappai]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloSpecialisMappai]') AND name = N'FK_Felhasznalo_Id')
DROP INDEX [FK_Felhasznalo_Id] ON [dbo].[KRT_FelhasznaloSpecialisMappai]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloSpecialisMappai]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_FelhasznaloSpecialisMappai]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloSpecialisMappai]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_FelhasznaloSpecialisMappai](
	[Id] [uniqueidentifier] NOT NULL,
	[Felhasznalo_Id] [uniqueidentifier] NOT NULL,
	[Mappa_Id] [uniqueidentifier] NOT NULL,
	[Sorrend] [int] NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KIRL_EDOC_FELH_SPEC_MAPPAI_PK] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloSpecialisMappai]') AND name = N'FK_Felhasznalo_Id')
CREATE NONCLUSTERED INDEX [FK_Felhasznalo_Id] ON [dbo].[KRT_FelhasznaloSpecialisMappai]
(
	[Felhasznalo_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloSpecialisMappai]') AND name = N'FK_Mappa_Id')
CREATE NONCLUSTERED INDEX [FK_Mappa_Id] ON [dbo].[KRT_FelhasznaloSpecialisMappai]
(
	[Mappa_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felhaszn__Id__25325BDD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloSpecialisMappai] ADD  CONSTRAINT [DF__KRT_Felhaszn__Id__25325BDD]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felhasz__Ver__26268016]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloSpecialisMappai] ADD  CONSTRAINT [DF__KRT_Felhasz__Ver__26268016]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felha__ErvKe__271AA44F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloSpecialisMappai] ADD  CONSTRAINT [DF__KRT_Felha__ErvKe__271AA44F]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_FelhasznaloSpecialisMappai_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloSpecialisMappai] ADD  CONSTRAINT [DF_KRT_FelhasznaloSpecialisMappai_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felha__Letre__2902ECC1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FelhasznaloSpecialisMappai] ADD  CONSTRAINT [DF__KRT_Felha__Letre__2902ECC1]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FelhasznSpecMappai_Mappa_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloSpecialisMappai]'))
ALTER TABLE [dbo].[KRT_FelhasznaloSpecialisMappai]  WITH CHECK ADD  CONSTRAINT [FelhasznSpecMappai_Mappa_FK] FOREIGN KEY([Mappa_Id])
REFERENCES [dbo].[KRT_Mappak] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FelhasznSpecMappai_Mappa_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FelhasznaloSpecialisMappai]'))
ALTER TABLE [dbo].[KRT_FelhasznaloSpecialisMappai] CHECK CONSTRAINT [FelhasznSpecMappai_Mappa_FK]
GO
