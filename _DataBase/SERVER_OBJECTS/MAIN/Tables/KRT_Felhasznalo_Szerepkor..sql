IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Szerepkor_csoport_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]'))
ALTER TABLE [dbo].[KRT_Felhasznalo_Szerepkor] DROP CONSTRAINT [Szerepkor_csoport_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FELHASZNSZEREP_HELYETTES_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]'))
ALTER TABLE [dbo].[KRT_Felhasznalo_Szerepkor] DROP CONSTRAINT [KRT_FELHASZNSZEREP_HELYETTES_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FES_FEL_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]'))
ALTER TABLE [dbo].[KRT_Felhasznalo_Szerepkor] DROP CONSTRAINT [FES_FEL_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Felhasznalo_Szerepkor_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]'))
ALTER TABLE [dbo].[KRT_Felhasznalo_Szerepkor] DROP CONSTRAINT [Felhasznalo_Szerepkor_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Csoporttag_Szerepkor_Fk]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]'))
ALTER TABLE [dbo].[KRT_Felhasznalo_Szerepkor] DROP CONSTRAINT [Csoporttag_Szerepkor_Fk]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]') AND name = N'FES_SRK_FK_I')
DROP INDEX [FES_SRK_FK_I] ON [dbo].[KRT_Felhasznalo_Szerepkor]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]') AND name = N'FES_FEL_FK_I')
DROP INDEX [FES_FEL_FK_I] ON [dbo].[KRT_Felhasznalo_Szerepkor]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Felhasznalo_Szerepkor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Felhasznalo_Szerepkor](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Felhaszn__Id__08A03ED0]  DEFAULT (newsequentialid()),
	[CsoportTag_Id] [uniqueidentifier] NULL,
	[Csoport_Id] [uniqueidentifier] NULL,
	[Felhasznalo_Id] [uniqueidentifier] NOT NULL,
	[Szerepkor_Id] [uniqueidentifier] NOT NULL,
	[Helyettesites_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Felhasz__Ver__09946309]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Felha__ErvKe__0A888742]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Felhasznalo_Szerepkor_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Felha__Letre__0C70CFB4]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [FES_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]') AND name = N'FES_FEL_FK_I')
CREATE NONCLUSTERED INDEX [FES_FEL_FK_I] ON [dbo].[KRT_Felhasznalo_Szerepkor]
(
	[Felhasznalo_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]') AND name = N'FES_SRK_FK_I')
CREATE NONCLUSTERED INDEX [FES_SRK_FK_I] ON [dbo].[KRT_Felhasznalo_Szerepkor]
(
	[Szerepkor_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Csoporttag_Szerepkor_Fk]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]'))
ALTER TABLE [dbo].[KRT_Felhasznalo_Szerepkor]  WITH CHECK ADD  CONSTRAINT [Csoporttag_Szerepkor_Fk] FOREIGN KEY([CsoportTag_Id])
REFERENCES [dbo].[KRT_CsoportTagok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Csoporttag_Szerepkor_Fk]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]'))
ALTER TABLE [dbo].[KRT_Felhasznalo_Szerepkor] CHECK CONSTRAINT [Csoporttag_Szerepkor_Fk]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Felhasznalo_Szerepkor_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]'))
ALTER TABLE [dbo].[KRT_Felhasznalo_Szerepkor]  WITH CHECK ADD  CONSTRAINT [Felhasznalo_Szerepkor_FK] FOREIGN KEY([Szerepkor_Id])
REFERENCES [dbo].[KRT_Szerepkorok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Felhasznalo_Szerepkor_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]'))
ALTER TABLE [dbo].[KRT_Felhasznalo_Szerepkor] CHECK CONSTRAINT [Felhasznalo_Szerepkor_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FES_FEL_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]'))
ALTER TABLE [dbo].[KRT_Felhasznalo_Szerepkor]  WITH CHECK ADD  CONSTRAINT [FES_FEL_FK] FOREIGN KEY([Felhasznalo_Id])
REFERENCES [dbo].[KRT_Felhasznalok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FES_FEL_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]'))
ALTER TABLE [dbo].[KRT_Felhasznalo_Szerepkor] CHECK CONSTRAINT [FES_FEL_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FELHASZNSZEREP_HELYETTES_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]'))
ALTER TABLE [dbo].[KRT_Felhasznalo_Szerepkor]  WITH CHECK ADD  CONSTRAINT [KRT_FELHASZNSZEREP_HELYETTES_FK] FOREIGN KEY([Helyettesites_Id])
REFERENCES [dbo].[KRT_Helyettesitesek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FELHASZNSZEREP_HELYETTES_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]'))
ALTER TABLE [dbo].[KRT_Felhasznalo_Szerepkor] CHECK CONSTRAINT [KRT_FELHASZNSZEREP_HELYETTES_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Szerepkor_csoport_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]'))
ALTER TABLE [dbo].[KRT_Felhasznalo_Szerepkor]  WITH CHECK ADD  CONSTRAINT [Szerepkor_csoport_FK] FOREIGN KEY([Csoport_Id])
REFERENCES [dbo].[KRT_Csoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Szerepkor_csoport_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalo_Szerepkor]'))
ALTER TABLE [dbo].[KRT_Felhasznalo_Szerepkor] CHECK CONSTRAINT [Szerepkor_csoport_FK]
GO
