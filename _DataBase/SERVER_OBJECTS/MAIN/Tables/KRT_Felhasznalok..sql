IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Felhasznalo_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok]'))
ALTER TABLE [dbo].[KRT_Felhasznalok] DROP CONSTRAINT [Felhasznalo_Partner_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Felhasznalo_Partner__munkahely_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok]'))
ALTER TABLE [dbo].[KRT_Felhasznalok] DROP CONSTRAINT [Felhasznalo_Partner__munkahely_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok]') AND name = N'KRT_Felhasznalo_ErvKezd_I')
DROP INDEX [KRT_Felhasznalo_ErvKezd_I] ON [dbo].[KRT_Felhasznalok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok]') AND name = N'KRT_FELH_USERN_UK')
DROP INDEX [KRT_FELH_USERN_UK] ON [dbo].[KRT_Felhasznalok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok]') AND name = N'FEL_PRT_FK_I')
DROP INDEX [FEL_PRT_FK_I] ON [dbo].[KRT_Felhasznalok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Felhasznalok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Felhasznalok](
	[Id] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [DF__KRT_Felhaszn__Id__0F4D3C5F]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NULL,
	[Partner_id] [uniqueidentifier] NULL,
	[Tipus] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[UserNev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Jelszo] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[JelszoLejaratIdo] [datetime] NOT NULL CONSTRAINT [DF_KRT_Felhasznalok_JelszoLejaratIdo]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[System] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Kiszolgalo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[DefaultPrivatKulcs] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Partner_Id_Munkahely] [uniqueidentifier] NULL,
	[MaxMinosites] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[EMail] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Engedelyezett] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__KRT_Felha__Enged__113584D1]  DEFAULT ('1'),
	[Telefonszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Beosztas] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Felhasz__Ver__1229A90A]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Felha__ErvKe__131DCD43]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Felhasznalok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Felha__Letre__150615B5]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [FEL_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok]') AND name = N'FEL_PRT_FK_I')
CREATE NONCLUSTERED INDEX [FEL_PRT_FK_I] ON [dbo].[KRT_Felhasznalok]
(
	[Partner_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok]') AND name = N'KRT_FELH_USERN_UK')
CREATE NONCLUSTERED INDEX [KRT_FELH_USERN_UK] ON [dbo].[KRT_Felhasznalok]
(
	[Org] ASC,
	[UserNev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok]') AND name = N'KRT_Felhasznalo_ErvKezd_I')
CREATE NONCLUSTERED INDEX [KRT_Felhasznalo_ErvKezd_I] ON [dbo].[KRT_Felhasznalok]
(
	[ErvKezd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Felhasznalo_Partner__munkahely_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok]'))
ALTER TABLE [dbo].[KRT_Felhasznalok]  WITH CHECK ADD  CONSTRAINT [Felhasznalo_Partner__munkahely_FK] FOREIGN KEY([Partner_Id_Munkahely])
REFERENCES [dbo].[KRT_Partnerek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Felhasznalo_Partner__munkahely_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok]'))
ALTER TABLE [dbo].[KRT_Felhasznalok] CHECK CONSTRAINT [Felhasznalo_Partner__munkahely_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Felhasznalo_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok]'))
ALTER TABLE [dbo].[KRT_Felhasznalok]  WITH CHECK ADD  CONSTRAINT [Felhasznalo_Partner_FK] FOREIGN KEY([Partner_id])
REFERENCES [dbo].[KRT_Partnerek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Felhasznalo_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok]'))
ALTER TABLE [dbo].[KRT_Felhasznalok] CHECK CONSTRAINT [Felhasznalo_Partner_FK]
GO
