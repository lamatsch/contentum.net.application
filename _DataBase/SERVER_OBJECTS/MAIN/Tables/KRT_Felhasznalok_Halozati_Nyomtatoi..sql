IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Felhasznalok_Halozati_Nyomtatoi_Nyomtatok_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi]'))
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi] DROP CONSTRAINT [Felhasznalok_Halozati_Nyomtatoi_Nyomtatok_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Felhasznalok_Halozati_Nyomtatoi_Felhasznalok_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi]'))
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi] DROP CONSTRAINT [Felhasznalok_Halozati_Nyomtatoi_Felhasznalok_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felha__Letre__6522C3C0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi] DROP CONSTRAINT [DF__KRT_Felha__Letre__6522C3C0]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Felhasznalok_Halozati_Nyomtatoi_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi] DROP CONSTRAINT [DF_KRT_Felhasznalok_Halozati_Nyomtatoi_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felha__ErvKe__633A7B4E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi] DROP CONSTRAINT [DF__KRT_Felha__ErvKe__633A7B4E]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felhasz__Ver__62465715]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi] DROP CONSTRAINT [DF__KRT_Felhasz__Ver__62465715]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felhaszn__Id__615232DC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi] DROP CONSTRAINT [DF__KRT_Felhaszn__Id__615232DC]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi](
	[Id] [uniqueidentifier] NOT NULL,
	[Felhasznalo_Id] [uniqueidentifier] NOT NULL,
	[Halozati_Nyomtato_Id] [uniqueidentifier] NOT NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_Felhasznalok_Halozati_Nyomtatoi] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felhaszn__Id__615232DC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi] ADD  CONSTRAINT [DF__KRT_Felhaszn__Id__615232DC]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felhasz__Ver__62465715]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi] ADD  CONSTRAINT [DF__KRT_Felhasz__Ver__62465715]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felha__ErvKe__633A7B4E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi] ADD  CONSTRAINT [DF__KRT_Felha__ErvKe__633A7B4E]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Felhasznalok_Halozati_Nyomtatoi_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi] ADD  CONSTRAINT [DF_KRT_Felhasznalok_Halozati_Nyomtatoi_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Felha__Letre__6522C3C0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi] ADD  CONSTRAINT [DF__KRT_Felha__Letre__6522C3C0]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Felhasznalok_Halozati_Nyomtatoi_Felhasznalok_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi]'))
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi]  WITH CHECK ADD  CONSTRAINT [Felhasznalok_Halozati_Nyomtatoi_Felhasznalok_FK] FOREIGN KEY([Felhasznalo_Id])
REFERENCES [dbo].[KRT_Felhasznalok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Felhasznalok_Halozati_Nyomtatoi_Felhasznalok_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi]'))
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi] CHECK CONSTRAINT [Felhasznalok_Halozati_Nyomtatoi_Felhasznalok_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Felhasznalok_Halozati_Nyomtatoi_Nyomtatok_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi]'))
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi]  WITH CHECK ADD  CONSTRAINT [Felhasznalok_Halozati_Nyomtatoi_Nyomtatok_FK] FOREIGN KEY([Halozati_Nyomtato_Id])
REFERENCES [dbo].[KRT_Halozati_Nyomtatok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Felhasznalok_Halozati_Nyomtatoi_Nyomtatok_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi]'))
ALTER TABLE [dbo].[KRT_Felhasznalok_Halozati_Nyomtatoi] CHECK CONSTRAINT [Felhasznalok_Halozati_Nyomtatoi_Nyomtatok_FK]
GO
