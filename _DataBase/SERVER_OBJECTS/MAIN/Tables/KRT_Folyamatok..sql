IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FOLYAMAT_ALKALMAZAS_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Folyamatok]'))
ALTER TABLE [dbo].[KRT_Folyamatok] DROP CONSTRAINT [KRT_FOLYAMAT_ALKALMAZAS_FK]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Folyamatok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Folyamatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Folyamatok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Folyamatok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Folyamat__Id__1BB31344]  DEFAULT (newsequentialid()),
	[FolyamatCsoportKod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[FolyamatKod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[FolyamatSorszam] [int] NULL,
	[Alkalmazas_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Folyama__Ver__1CA7377D]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Folya__ErvKe__1D9B5BB6]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Folya__Letre__1E8F7FEF]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_FOLYAMATOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FOLYAMAT_ALKALMAZAS_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Folyamatok]'))
ALTER TABLE [dbo].[KRT_Folyamatok]  WITH CHECK ADD  CONSTRAINT [KRT_FOLYAMAT_ALKALMAZAS_FK] FOREIGN KEY([Alkalmazas_Id])
REFERENCES [dbo].[KRT_Alkalmazasok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FOLYAMAT_ALKALMAZAS_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Folyamatok]'))
ALTER TABLE [dbo].[KRT_Folyamatok] CHECK CONSTRAINT [KRT_FOLYAMAT_ALKALMAZAS_FK]
GO
