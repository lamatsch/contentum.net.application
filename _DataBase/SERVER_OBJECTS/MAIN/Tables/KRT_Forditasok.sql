if exists (select 1
            from  sysobjects
           where  id = object_id('KRT_Forditasok')
            and   type = 'U')
   drop table KRT_Forditasok
go

/*==============================================================*/
/* Table: KRT_Forditasok                                        */
/*==============================================================*/
create table KRT_Forditasok (
   Id                   uniqueidentifier     not null default newsequentialid(),
   NyelvKod             Nvarchar(10)         null,
   OrgKod               Nvarchar(100)        null,
   Modul                Nvarchar(400)        null,
   Komponens            Nvarchar(400)        null,
   ObjAzonosito         Nvarchar(400)        null,
   Forditas             Nvarchar(4000)       null,
   Ver                  int                  null default 1,
   Note                 Nvarchar(4000)       null,
   Stat_id              uniqueidentifier     null,
   ErvKezd              datetime             null default Getdate(),
   ErvVege              datetime             null,
   Letrehozo_id         uniqueidentifier     null,
   LetrehozasIdo        datetime             not null default Getdate(),
   Modosito_id          uniqueidentifier     null,
   ModositasIdo         datetime             null,
   Zarolo_id            uniqueidentifier     null,
   ZarolasIdo           datetime             null,
   Tranz_id             uniqueidentifier     null,
   UIAccessLog_id       uniqueidentifier     null
)
go

alter table KRT_Forditasok 
  ADD CONSTRAINT
	DF_KRT_Forditasok_ErvVege DEFAULT CONVERT(datetime, '4700-12-31', 102) FOR ErvVege;
go
