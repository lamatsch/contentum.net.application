IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkciok_FunkcioInditas_Fk]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioInditas]'))
ALTER TABLE [dbo].[KRT_FunkcioInditas] DROP CONSTRAINT [Funkciok_FunkcioInditas_Fk]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Funkc__Letre__2CD37DA5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioInditas] DROP CONSTRAINT [DF__KRT_Funkc__Letre__2CD37DA5]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Funkc__ErvKe__2BDF596C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioInditas] DROP CONSTRAINT [DF__KRT_Funkc__ErvKe__2BDF596C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Funkcio__Ver__2AEB3533]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioInditas] DROP CONSTRAINT [DF__KRT_Funkcio__Ver__2AEB3533]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_FunkcioI__Id__29F710FA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioInditas] DROP CONSTRAINT [DF__KRT_FunkcioI__Id__29F710FA]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioInditas]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_FunkcioInditas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioInditas]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_FunkcioInditas](
	[Id] [uniqueidentifier] NOT NULL,
	[Funkcio_Id] [uniqueidentifier] NOT NULL,
	[AlapertelmezettFunkcio] [char](1) COLLATE Hungarian_CI_AS NOT NULL,
	[URLDefinicio] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[URLMegnyitas] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_FUNKCIOINDITAS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_FunkcioI__Id__29F710FA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioInditas] ADD  CONSTRAINT [DF__KRT_FunkcioI__Id__29F710FA]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Funkcio__Ver__2AEB3533]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioInditas] ADD  CONSTRAINT [DF__KRT_Funkcio__Ver__2AEB3533]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Funkc__ErvKe__2BDF596C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioInditas] ADD  CONSTRAINT [DF__KRT_Funkc__ErvKe__2BDF596C]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Funkc__Letre__2CD37DA5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioInditas] ADD  CONSTRAINT [DF__KRT_Funkc__Letre__2CD37DA5]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkciok_FunkcioInditas_Fk]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioInditas]'))
ALTER TABLE [dbo].[KRT_FunkcioInditas]  WITH CHECK ADD  CONSTRAINT [Funkciok_FunkcioInditas_Fk] FOREIGN KEY([Funkcio_Id])
REFERENCES [dbo].[KRT_Funkciok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkciok_FunkcioInditas_Fk]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioInditas]'))
ALTER TABLE [dbo].[KRT_FunkcioInditas] CHECK CONSTRAINT [Funkciok_FunkcioInditas_Fk]
GO
