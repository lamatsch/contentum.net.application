IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkciolista_Funkcio_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioLista]'))
ALTER TABLE [dbo].[KRT_FunkcioLista] DROP CONSTRAINT [Funkciolista_Funkcio_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_FUNK_FUNK_ALFU_KRT_FUNK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioLista]'))
ALTER TABLE [dbo].[KRT_FunkcioLista] DROP CONSTRAINT [FK_KRT_FUNK_FUNK_ALFU_KRT_FUNK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Funkc__Letre__319832C2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioLista] DROP CONSTRAINT [DF__KRT_Funkc__Letre__319832C2]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_FunkcioLista_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioLista] DROP CONSTRAINT [DF_KRT_FunkcioLista_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Funkc__ErvKe__2FAFEA50]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioLista] DROP CONSTRAINT [DF__KRT_Funkc__ErvKe__2FAFEA50]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Funkcio__Ver__2EBBC617]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioLista] DROP CONSTRAINT [DF__KRT_Funkcio__Ver__2EBBC617]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_FunkcioL__Id__2DC7A1DE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioLista] DROP CONSTRAINT [DF__KRT_FunkcioL__Id__2DC7A1DE]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioLista]') AND name = N'KRT_FunkcioLista_Ind01')
DROP INDEX [KRT_FunkcioLista_Ind01] ON [dbo].[KRT_FunkcioLista]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioLista]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_FunkcioLista]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioLista]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_FunkcioLista](
	[Id] [uniqueidentifier] NOT NULL,
	[Funkcio_Id] [uniqueidentifier] NULL,
	[Funkcio_Id_Hivott] [uniqueidentifier] NULL,
	[FutasiSorrend] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_FUNKCIOLISTA] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioLista]') AND name = N'KRT_FunkcioLista_Ind01')
CREATE NONCLUSTERED INDEX [KRT_FunkcioLista_Ind01] ON [dbo].[KRT_FunkcioLista]
(
	[Funkcio_Id] ASC,
	[Funkcio_Id_Hivott] ASC,
	[FutasiSorrend] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_FunkcioL__Id__2DC7A1DE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioLista] ADD  CONSTRAINT [DF__KRT_FunkcioL__Id__2DC7A1DE]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Funkcio__Ver__2EBBC617]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioLista] ADD  CONSTRAINT [DF__KRT_Funkcio__Ver__2EBBC617]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Funkc__ErvKe__2FAFEA50]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioLista] ADD  CONSTRAINT [DF__KRT_Funkc__ErvKe__2FAFEA50]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_FunkcioLista_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioLista] ADD  CONSTRAINT [DF_KRT_FunkcioLista_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Funkc__Letre__319832C2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_FunkcioLista] ADD  CONSTRAINT [DF__KRT_Funkc__Letre__319832C2]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_FUNK_FUNK_ALFU_KRT_FUNK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioLista]'))
ALTER TABLE [dbo].[KRT_FunkcioLista]  WITH CHECK ADD  CONSTRAINT [FK_KRT_FUNK_FUNK_ALFU_KRT_FUNK] FOREIGN KEY([Funkcio_Id])
REFERENCES [dbo].[KRT_Funkciok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_FUNK_FUNK_ALFU_KRT_FUNK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioLista]'))
ALTER TABLE [dbo].[KRT_FunkcioLista] CHECK CONSTRAINT [FK_KRT_FUNK_FUNK_ALFU_KRT_FUNK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkciolista_Funkcio_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioLista]'))
ALTER TABLE [dbo].[KRT_FunkcioLista]  WITH CHECK ADD  CONSTRAINT [Funkciolista_Funkcio_FK] FOREIGN KEY([Funkcio_Id_Hivott])
REFERENCES [dbo].[KRT_Funkciok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkciolista_Funkcio_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_FunkcioLista]'))
ALTER TABLE [dbo].[KRT_FunkcioLista] CHECK CONSTRAINT [Funkciolista_Funkcio_FK]
GO
