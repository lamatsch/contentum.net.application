IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkcio_ObjTip_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok] DROP CONSTRAINT [Funkcio_ObjTip_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkcio_ObjStat_Veg_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok] DROP CONSTRAINT [Funkcio_ObjStat_Veg_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkcio_ObjStat_Kezd_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok] DROP CONSTRAINT [Funkcio_ObjStat_Kezd_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkcio_Muvelet_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok] DROP CONSTRAINT [Funkcio_Muvelet_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkcio_funkcio_szulo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok] DROP CONSTRAINT [Funkcio_funkcio_szulo_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_FUNK_TRANZ_FUN_KRT_TRAN]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok] DROP CONSTRAINT [FK_KRT_FUNK_TRANZ_FUN_KRT_TRAN]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_FUNK_ALKALMAZA_KRT_ALKA]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok] DROP CONSTRAINT [FK_KRT_FUNK_ALKALMAZA_KRT_ALKA]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]') AND name = N'KRT_Funkcio_Kod_Ervkezd_Ervvege')
DROP INDEX [KRT_Funkcio_Kod_Ervkezd_Ervvege] ON [dbo].[KRT_Funkciok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]') AND name = N'KRT_Funkcio_Ind01')
DROP INDEX [KRT_Funkcio_Ind01] ON [dbo].[KRT_Funkciok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Funkciok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Funkciok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Funkciok__Id__2354350C]  DEFAULT (newsequentialid()),
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[ObjTipus_Id_AdatElem] [uniqueidentifier] NULL,
	[ObjStat_Id_Kezd] [uniqueidentifier] NULL,
	[Alkalmazas_Id] [uniqueidentifier] NULL,
	[Muvelet_Id] [uniqueidentifier] NULL,
	[ObjStat_Id_Veg] [uniqueidentifier] NULL,
	[Leiras] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Funkcio_Id_Szulo] [uniqueidentifier] NULL,
	[Csoportosito] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Modosithato] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Org] [uniqueidentifier] NULL,
	[MunkanaploJelzo] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__KRT_Funkc__Munka__24485945]  DEFAULT ('0'),
	[FeladatJelzo] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__KRT_Funkc__Felad__253C7D7E]  DEFAULT ('0'),
	[KeziFeladatJelzo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Funkcio__Ver__2630A1B7]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Funkc__ErvKe__2724C5F0]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Funkciok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Funkc__Letre__290D0E62]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [FUN_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]') AND name = N'KRT_Funkcio_Ind01')
CREATE NONCLUSTERED INDEX [KRT_Funkcio_Ind01] ON [dbo].[KRT_Funkciok]
(
	[Alkalmazas_Id] ASC,
	[Muvelet_Id] ASC,
	[Kod] ASC,
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]') AND name = N'KRT_Funkcio_Kod_Ervkezd_Ervvege')
CREATE NONCLUSTERED INDEX [KRT_Funkcio_Kod_Ervkezd_Ervvege] ON [dbo].[KRT_Funkciok]
(
	[Kod] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_FUNK_ALKALMAZA_KRT_ALKA]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok]  WITH CHECK ADD  CONSTRAINT [FK_KRT_FUNK_ALKALMAZA_KRT_ALKA] FOREIGN KEY([Alkalmazas_Id])
REFERENCES [dbo].[KRT_Alkalmazasok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_FUNK_ALKALMAZA_KRT_ALKA]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok] CHECK CONSTRAINT [FK_KRT_FUNK_ALKALMAZA_KRT_ALKA]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_FUNK_TRANZ_FUN_KRT_TRAN]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok]  WITH CHECK ADD  CONSTRAINT [FK_KRT_FUNK_TRANZ_FUN_KRT_TRAN] FOREIGN KEY([Tranz_id])
REFERENCES [dbo].[KRT_Tranzakciok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_FUNK_TRANZ_FUN_KRT_TRAN]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok] CHECK CONSTRAINT [FK_KRT_FUNK_TRANZ_FUN_KRT_TRAN]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkcio_funkcio_szulo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok]  WITH CHECK ADD  CONSTRAINT [Funkcio_funkcio_szulo_FK] FOREIGN KEY([Funkcio_Id_Szulo])
REFERENCES [dbo].[KRT_Funkciok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkcio_funkcio_szulo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok] CHECK CONSTRAINT [Funkcio_funkcio_szulo_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkcio_Muvelet_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok]  WITH CHECK ADD  CONSTRAINT [Funkcio_Muvelet_FK] FOREIGN KEY([Muvelet_Id])
REFERENCES [dbo].[KRT_Muveletek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkcio_Muvelet_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok] CHECK CONSTRAINT [Funkcio_Muvelet_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkcio_ObjStat_Kezd_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok]  WITH CHECK ADD  CONSTRAINT [Funkcio_ObjStat_Kezd_FK] FOREIGN KEY([ObjStat_Id_Kezd])
REFERENCES [dbo].[KRT_KodTarak] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkcio_ObjStat_Kezd_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok] CHECK CONSTRAINT [Funkcio_ObjStat_Kezd_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkcio_ObjStat_Veg_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok]  WITH CHECK ADD  CONSTRAINT [Funkcio_ObjStat_Veg_FK] FOREIGN KEY([ObjStat_Id_Veg])
REFERENCES [dbo].[KRT_KodTarak] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkcio_ObjStat_Veg_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok] CHECK CONSTRAINT [Funkcio_ObjStat_Veg_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkcio_ObjTip_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok]  WITH CHECK ADD  CONSTRAINT [Funkcio_ObjTip_FK] FOREIGN KEY([ObjTipus_Id_AdatElem])
REFERENCES [dbo].[KRT_ObjTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Funkcio_ObjTip_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Funkciok]'))
ALTER TABLE [dbo].[KRT_Funkciok] CHECK CONSTRAINT [Funkcio_ObjTip_FK]
GO
