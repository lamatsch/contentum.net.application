IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Haloz__Letre__5E75C631]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Halozati_Nyomtatok] DROP CONSTRAINT [DF__KRT_Haloz__Letre__5E75C631]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Halozati_Nyomtatok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Halozati_Nyomtatok] DROP CONSTRAINT [DF_KRT_Halozati_Nyomtatok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Haloz__ErvKe__5C8D7DBF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Halozati_Nyomtatok] DROP CONSTRAINT [DF__KRT_Haloz__ErvKe__5C8D7DBF]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Halozat__Ver__5B995986]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Halozati_Nyomtatok] DROP CONSTRAINT [DF__KRT_Halozat__Ver__5B995986]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Halozati__Id__5AA5354D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Halozati_Nyomtatok] DROP CONSTRAINT [DF__KRT_Halozati__Id__5AA5354D]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Halozati_Nyomtatok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Halozati_Nyomtatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Halozati_Nyomtatok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Halozati_Nyomtatok](
	[Id] [uniqueidentifier] NOT NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Cim] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_Halozati_Nyomtatok] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Halozati__Id__5AA5354D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Halozati_Nyomtatok] ADD  CONSTRAINT [DF__KRT_Halozati__Id__5AA5354D]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Halozat__Ver__5B995986]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Halozati_Nyomtatok] ADD  CONSTRAINT [DF__KRT_Halozat__Ver__5B995986]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Haloz__ErvKe__5C8D7DBF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Halozati_Nyomtatok] ADD  CONSTRAINT [DF__KRT_Haloz__ErvKe__5C8D7DBF]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Halozati_Nyomtatok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Halozati_Nyomtatok] ADD  CONSTRAINT [DF_KRT_Halozati_Nyomtatok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Haloz__Letre__5E75C631]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Halozati_Nyomtatok] ADD  CONSTRAINT [DF__KRT_Haloz__Letre__5E75C631]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
