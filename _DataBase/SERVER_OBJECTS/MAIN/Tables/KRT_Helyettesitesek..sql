IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_HELYETTESITES_CSOPORTTAG_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Helyettesitesek]'))
ALTER TABLE [dbo].[KRT_Helyettesitesek] DROP CONSTRAINT [KRT_HELYETTESITES_CSOPORTTAG_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[HEL_HELYETTESITO_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Helyettesitesek]'))
ALTER TABLE [dbo].[KRT_Helyettesitesek] DROP CONSTRAINT [HEL_HELYETTESITO_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[HEL_HELYETTESITETT_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Helyettesitesek]'))
ALTER TABLE [dbo].[KRT_Helyettesitesek] DROP CONSTRAINT [HEL_HELYETTESITETT_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Helyettesitesek]') AND name = N'HEL_HELYETTESITO_FK_I')
DROP INDEX [HEL_HELYETTESITO_FK_I] ON [dbo].[KRT_Helyettesitesek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Helyettesitesek]') AND name = N'HEL_HELYETTESITETT_FK_I')
DROP INDEX [HEL_HELYETTESITETT_FK_I] ON [dbo].[KRT_Helyettesitesek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Helyettesitesek]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Helyettesitesek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Helyettesitesek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Helyettesitesek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Helyette__Id__2DD1C37F]  DEFAULT (newsequentialid()),
	[Felhasznalo_ID_helyettesitett] [uniqueidentifier] NOT NULL,
	[CsoportTag_ID_helyettesitett] [uniqueidentifier] NULL,
	[Felhasznalo_ID_helyettesito] [uniqueidentifier] NOT NULL,
	[HelyettesitesMod] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[HelyettesitesKezd] [datetime] NULL,
	[HelyettesitesVege] [datetime] NULL,
	[Megjegyzes] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Helyett__Ver__2EC5E7B8]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Helye__ErvKe__2FBA0BF1]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Helyettesitesek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Helye__Letre__31A25463]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [HEL_PK_I] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Helyettesitesek]') AND name = N'HEL_HELYETTESITETT_FK_I')
CREATE NONCLUSTERED INDEX [HEL_HELYETTESITETT_FK_I] ON [dbo].[KRT_Helyettesitesek]
(
	[Felhasznalo_ID_helyettesitett] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Helyettesitesek]') AND name = N'HEL_HELYETTESITO_FK_I')
CREATE NONCLUSTERED INDEX [HEL_HELYETTESITO_FK_I] ON [dbo].[KRT_Helyettesitesek]
(
	[Felhasznalo_ID_helyettesito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[HEL_HELYETTESITETT_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Helyettesitesek]'))
ALTER TABLE [dbo].[KRT_Helyettesitesek]  WITH CHECK ADD  CONSTRAINT [HEL_HELYETTESITETT_FK] FOREIGN KEY([Felhasznalo_ID_helyettesitett])
REFERENCES [dbo].[KRT_Felhasznalok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[HEL_HELYETTESITETT_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Helyettesitesek]'))
ALTER TABLE [dbo].[KRT_Helyettesitesek] CHECK CONSTRAINT [HEL_HELYETTESITETT_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[HEL_HELYETTESITO_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Helyettesitesek]'))
ALTER TABLE [dbo].[KRT_Helyettesitesek]  WITH CHECK ADD  CONSTRAINT [HEL_HELYETTESITO_FK] FOREIGN KEY([Felhasznalo_ID_helyettesito])
REFERENCES [dbo].[KRT_Felhasznalok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[HEL_HELYETTESITO_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Helyettesitesek]'))
ALTER TABLE [dbo].[KRT_Helyettesitesek] CHECK CONSTRAINT [HEL_HELYETTESITO_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_HELYETTESITES_CSOPORTTAG_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Helyettesitesek]'))
ALTER TABLE [dbo].[KRT_Helyettesitesek]  WITH CHECK ADD  CONSTRAINT [KRT_HELYETTESITES_CSOPORTTAG_FK] FOREIGN KEY([CsoportTag_ID_helyettesitett])
REFERENCES [dbo].[KRT_CsoportTagok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_HELYETTESITES_CSOPORTTAG_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Helyettesitesek]'))
ALTER TABLE [dbo].[KRT_Helyettesitesek] CHECK CONSTRAINT [KRT_HELYETTESITES_CSOPORTTAG_FK]
GO
