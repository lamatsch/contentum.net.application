IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_JOGOSULTAK_CSOP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Jogosultak]'))
ALTER TABLE [dbo].[KRT_Jogosultak] DROP CONSTRAINT [KRT_JOGOSULTAK_CSOP_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Jogosultak]') AND name = N'KRT_JOGOSULTAK_CSOP_I')
DROP INDEX [KRT_JOGOSULTAK_CSOP_I] ON [dbo].[KRT_Jogosultak]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Jogosultak]') AND name = N'KRT_JOGOSULTAK_ACL_I')
DROP INDEX [KRT_JOGOSULTAK_ACL_I] ON [dbo].[KRT_Jogosultak]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Jogosultak]') AND name = N'IX_Obj_Id_Csoport_Id_Jogalany')
DROP INDEX [IX_Obj_Id_Csoport_Id_Jogalany] ON [dbo].[KRT_Jogosultak]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Jogosultak]') AND name = N'IX_Jogosultak_Csoport_Id_Jogalany')
DROP INDEX [IX_Jogosultak_Csoport_Id_Jogalany] ON [dbo].[KRT_Jogosultak]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Jogosultak]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Jogosultak]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Jogosultak]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Jogosultak](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Jogosult__Id__347EC10E]  DEFAULT (newsequentialid()),
	[Csoport_Id_Jogalany] [uniqueidentifier] NOT NULL,
	[Jogszint] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[Obj_Id] [uniqueidentifier] NOT NULL,
	[Orokolheto] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__KRT_Jogos__Oroko__3572E547]  DEFAULT ('0'),
	[Kezi] [char](1) COLLATE Hungarian_CI_AS NOT NULL,
	[Tipus] [char](1) COLLATE Hungarian_CI_AS NULL CONSTRAINT [DF__KRT_Jogos__Tipus__36670980]  DEFAULT ('0'),
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Jogosul__Ver__375B2DB9]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Jogos__ErvKe__384F51F2]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Jogosultak_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Jogos__Letre__3A379A64]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KRT_JOGOSULTAK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Jogosultak]') AND name = N'IX_Jogosultak_Csoport_Id_Jogalany')
CREATE NONCLUSTERED INDEX [IX_Jogosultak_Csoport_Id_Jogalany] ON [dbo].[KRT_Jogosultak]
(
	[Csoport_Id_Jogalany] ASC,
	[Obj_Id] ASC
)
INCLUDE ( 	[Tipus]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Jogosultak]') AND name = N'IX_Obj_Id_Csoport_Id_Jogalany')
CREATE NONCLUSTERED INDEX [IX_Obj_Id_Csoport_Id_Jogalany] ON [dbo].[KRT_Jogosultak]
(
	[Obj_Id] ASC
)
INCLUDE ( 	[Csoport_Id_Jogalany]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Jogosultak]') AND name = N'KRT_JOGOSULTAK_ACL_I')
CREATE NONCLUSTERED INDEX [KRT_JOGOSULTAK_ACL_I] ON [dbo].[KRT_Jogosultak]
(
	[Tipus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Jogosultak]') AND name = N'KRT_JOGOSULTAK_CSOP_I')
CREATE NONCLUSTERED INDEX [KRT_JOGOSULTAK_CSOP_I] ON [dbo].[KRT_Jogosultak]
(
	[Csoport_Id_Jogalany] ASC,
	[Obj_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_JOGOSULTAK_CSOP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Jogosultak]'))
ALTER TABLE [dbo].[KRT_Jogosultak]  WITH CHECK ADD  CONSTRAINT [KRT_JOGOSULTAK_CSOP_FK] FOREIGN KEY([Csoport_Id_Jogalany])
REFERENCES [dbo].[KRT_Csoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_JOGOSULTAK_CSOP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Jogosultak]'))
ALTER TABLE [dbo].[KRT_Jogosultak] CHECK CONSTRAINT [KRT_JOGOSULTAK_CSOP_FK]
GO
