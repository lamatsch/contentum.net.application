IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodCsoportok]') AND name = N'KRT_KodCsoport_UK')
DROP INDEX [KRT_KodCsoport_UK] ON [dbo].[KRT_KodCsoportok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodCsoportok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_KodCsoportok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodCsoportok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_KodCsoportok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_KodCsopo__Id__3D14070F]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NULL,
	[KodTarak_Id_KodcsoportTipus] [uniqueidentifier] NULL,
	[Kod] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NOT NULL,
	[Modosithato] [char](1) COLLATE Hungarian_CI_AS NOT NULL,
	[Hossz] [int] NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NULL,
	[Leiras] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[BesorolasiSema] [char](1) COLLATE Hungarian_CI_AS NOT NULL,
	[KiegAdat] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KiegMezo] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[KiegAdattipus] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KiegSzotar] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_KodCsop__Ver__3E082B48]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_KodCs__ErvKe__3EFC4F81]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_KodCsoportok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_KodCs__Letre__40E497F3]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KCS_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodCsoportok]') AND name = N'KRT_KodCsoport_UK')
CREATE NONCLUSTERED INDEX [KRT_KodCsoport_UK] ON [dbo].[KRT_KodCsoportok]
(
	[Org] ASC,
	[Kod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
