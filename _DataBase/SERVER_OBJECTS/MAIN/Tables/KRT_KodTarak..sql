IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kodtar_ObjTipus_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]'))
ALTER TABLE [dbo].[KRT_KodTarak] DROP CONSTRAINT [Kodtar_ObjTipus_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KodTar_Kodcsoport_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]'))
ALTER TABLE [dbo].[KRT_KodTarak] DROP CONSTRAINT [KodTar_Kodcsoport_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]') AND name = N'KTR_KCS_FK_I')
DROP INDEX [KTR_KCS_FK_I] ON [dbo].[KRT_KodTarak]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]') AND name = N'IX_Objektum')
DROP INDEX [IX_Objektum] ON [dbo].[KRT_KodTarak]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]') AND name = N'IX_kodok_UK')
DROP INDEX [IX_kodok_UK] ON [dbo].[KRT_KodTarak]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]') AND name = N'IX_Kod')
DROP INDEX [IX_Kod] ON [dbo].[KRT_KodTarak]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_KodTarak]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_KodTarak](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_KodTarak__Id__43C1049E]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NULL,
	[KodCsoport_Id] [uniqueidentifier] NOT NULL,
	[ObjTip_Id_AdatElem] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[Kod] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NOT NULL,
	[RovidNev] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Egyeb] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Modosithato] [char](1) COLLATE Hungarian_CI_AS NOT NULL,
	[Sorrend] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_KodTara__Ver__44B528D7]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_KodTa__ErvKe__45A94D10]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_KodTarak_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_KodTa__Letre__47919582]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KRT_KODTARAK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]') AND name = N'IX_Kod')
CREATE NONCLUSTERED INDEX [IX_Kod] ON [dbo].[KRT_KodTarak]
(
	[Org] ASC,
	[Kod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]') AND name = N'IX_kodok_UK')
CREATE NONCLUSTERED INDEX [IX_kodok_UK] ON [dbo].[KRT_KodTarak]
(
	[Org] ASC,
	[KodCsoport_Id] ASC,
	[Kod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]') AND name = N'IX_Objektum')
CREATE NONCLUSTERED INDEX [IX_Objektum] ON [dbo].[KRT_KodTarak]
(
	[Obj_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]') AND name = N'KTR_KCS_FK_I')
CREATE NONCLUSTERED INDEX [KTR_KCS_FK_I] ON [dbo].[KRT_KodTarak]
(
	[KodCsoport_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KodTar_Kodcsoport_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]'))
ALTER TABLE [dbo].[KRT_KodTarak]  WITH CHECK ADD  CONSTRAINT [KodTar_Kodcsoport_FK] FOREIGN KEY([KodCsoport_Id])
REFERENCES [dbo].[KRT_KodCsoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KodTar_Kodcsoport_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]'))
ALTER TABLE [dbo].[KRT_KodTarak] CHECK CONSTRAINT [KodTar_Kodcsoport_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kodtar_ObjTipus_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]'))
ALTER TABLE [dbo].[KRT_KodTarak]  WITH CHECK ADD  CONSTRAINT [Kodtar_ObjTipus_FK] FOREIGN KEY([ObjTip_Id_AdatElem])
REFERENCES [dbo].[KRT_ObjTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Kodtar_ObjTipus_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_KodTarak]'))
ALTER TABLE [dbo].[KRT_KodTarak] CHECK CONSTRAINT [Kodtar_ObjTipus_FK]
GO
