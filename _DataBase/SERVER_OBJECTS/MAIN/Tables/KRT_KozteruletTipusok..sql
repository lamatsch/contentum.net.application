IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KozteruletTipusNev_KozteruletTipusNev_Szinonima_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_KozteruletTipusok]'))
ALTER TABLE [dbo].[KRT_KozteruletTipusok] DROP CONSTRAINT [KozteruletTipusNev_KozteruletTipusNev_Szinonima_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KozteruletTipusok]') AND name = N'KozteruletTipusNev_Nev_I')
DROP INDEX [KozteruletTipusNev_Nev_I] ON [dbo].[KRT_KozteruletTipusok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KozteruletTipusok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_KozteruletTipusok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KozteruletTipusok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_KozteruletTipusok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Kozterul__Id__511AFFBC]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NULL,
	[KozterTipNev_Id_Szinonima] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Kozteru__Ver__520F23F5]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Kozte__ErvKe__5303482E]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_KozteruletTipusok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Kozte__Letre__54EB90A0]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KozteruletTipusNevek] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_KozteruletTipusok]') AND name = N'KozteruletTipusNev_Nev_I')
CREATE NONCLUSTERED INDEX [KozteruletTipusNev_Nev_I] ON [dbo].[KRT_KozteruletTipusok]
(
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KozteruletTipusNev_KozteruletTipusNev_Szinonima_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_KozteruletTipusok]'))
ALTER TABLE [dbo].[KRT_KozteruletTipusok]  WITH CHECK ADD  CONSTRAINT [KozteruletTipusNev_KozteruletTipusNev_Szinonima_FK] FOREIGN KEY([KozterTipNev_Id_Szinonima])
REFERENCES [dbo].[KRT_KozteruletTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KozteruletTipusNev_KozteruletTipusNev_Szinonima_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_KozteruletTipusok]'))
ALTER TABLE [dbo].[KRT_KozteruletTipusok] CHECK CONSTRAINT [KozteruletTipusNev_KozteruletTipusNev_Szinonima_FK]
GO
