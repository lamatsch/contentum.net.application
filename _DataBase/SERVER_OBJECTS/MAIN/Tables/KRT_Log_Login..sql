IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Log_Login]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Log_Login]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Log_Login]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Log_Login](
	[Date] [datetime] NOT NULL,
	[Status] [char](1) COLLATE Hungarian_CI_AS NULL,
	[StartDate] [datetime] NULL,
	[Felhasznalo_Id] [uniqueidentifier] NULL,
	[Felhasznalo_Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[CsoportTag_Id] [uniqueidentifier] NULL,
	[Helyettesito_Id] [uniqueidentifier] NULL,
	[Helyettesito_Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Helyettesites_Id] [uniqueidentifier] NULL,
	[Helyettesites_Mod] [char](2) COLLATE Hungarian_CI_AS NULL,
	[LoginType] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UserHostAddress] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Level] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[Message] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[HibaKod] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[HibaUzenet] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
