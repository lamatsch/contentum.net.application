IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Mapatartalom_Mappa_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_MappaTartalmak]'))
ALTER TABLE [dbo].[KRT_MappaTartalmak] DROP CONSTRAINT [KRT_Mapatartalom_Mappa_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappa__Letre__66161CA2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaTartalmak] DROP CONSTRAINT [DF__KRT_Mappa__Letre__66161CA2]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_MappaTartalmak_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaTartalmak] DROP CONSTRAINT [DF_KRT_MappaTartalmak_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappa__ErvKe__642DD430]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaTartalmak] DROP CONSTRAINT [DF__KRT_Mappa__ErvKe__642DD430]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_MappaTa__Ver__6339AFF7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaTartalmak] DROP CONSTRAINT [DF__KRT_MappaTa__Ver__6339AFF7]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_MappaTar__Id__62458BBE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaTartalmak] DROP CONSTRAINT [DF__KRT_MappaTar__Id__62458BBE]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaTartalmak]') AND name = N'Index_UK')
DROP INDEX [Index_UK] ON [dbo].[KRT_MappaTartalmak]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaTartalmak]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_MappaTartalmak]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaTartalmak]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_MappaTartalmak](
	[Id] [uniqueidentifier] NOT NULL,
	[Mappa_Id] [uniqueidentifier] NOT NULL,
	[Obj_Id] [uniqueidentifier] NOT NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Leiras] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KRT_MAPPATARTALMAK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaTartalmak]') AND name = N'Index_UK')
CREATE NONCLUSTERED INDEX [Index_UK] ON [dbo].[KRT_MappaTartalmak]
(
	[Mappa_Id] ASC,
	[Obj_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_MappaTar__Id__62458BBE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaTartalmak] ADD  CONSTRAINT [DF__KRT_MappaTar__Id__62458BBE]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_MappaTa__Ver__6339AFF7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaTartalmak] ADD  CONSTRAINT [DF__KRT_MappaTa__Ver__6339AFF7]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappa__ErvKe__642DD430]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaTartalmak] ADD  CONSTRAINT [DF__KRT_Mappa__ErvKe__642DD430]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_MappaTartalmak_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaTartalmak] ADD  CONSTRAINT [DF_KRT_MappaTartalmak_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappa__Letre__66161CA2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaTartalmak] ADD  CONSTRAINT [DF__KRT_Mappa__Letre__66161CA2]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Mapatartalom_Mappa_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_MappaTartalmak]'))
ALTER TABLE [dbo].[KRT_MappaTartalmak]  WITH CHECK ADD  CONSTRAINT [KRT_Mapatartalom_Mappa_FK] FOREIGN KEY([Mappa_Id])
REFERENCES [dbo].[KRT_Mappak] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Mapatartalom_Mappa_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_MappaTartalmak]'))
ALTER TABLE [dbo].[KRT_MappaTartalmak] CHECK CONSTRAINT [KRT_Mapatartalom_Mappa_FK]
GO
