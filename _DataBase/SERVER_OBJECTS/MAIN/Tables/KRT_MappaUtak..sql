IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappa__Letre__365CE7DF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaUtak] DROP CONSTRAINT [DF__KRT_Mappa__Letre__365CE7DF]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_MappaUtak_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaUtak] DROP CONSTRAINT [DF_KRT_MappaUtak_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappa__ErvKe__34749F6D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaUtak] DROP CONSTRAINT [DF__KRT_Mappa__ErvKe__34749F6D]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_MappaUt__Ver__33807B34]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaUtak] DROP CONSTRAINT [DF__KRT_MappaUt__Ver__33807B34]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_MappaUta__Id__328C56FB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaUtak] DROP CONSTRAINT [DF__KRT_MappaUta__Id__328C56FB]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaUtak]') AND name = N'IX_Mappa_Id')
DROP INDEX [IX_Mappa_Id] ON [dbo].[KRT_MappaUtak]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaUtak]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_MappaUtak]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaUtak]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_MappaUtak](
	[Id] [uniqueidentifier] NOT NULL,
	[Mappa_Id] [uniqueidentifier] NOT NULL,
	[PathFromORG] [nvarchar](400) COLLATE Hungarian_CI_AS NOT NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KRT_MAPPA_UTAK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_MappaUtak]') AND name = N'IX_Mappa_Id')
CREATE NONCLUSTERED INDEX [IX_Mappa_Id] ON [dbo].[KRT_MappaUtak]
(
	[Mappa_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_MappaUta__Id__328C56FB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaUtak] ADD  CONSTRAINT [DF__KRT_MappaUta__Id__328C56FB]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_MappaUt__Ver__33807B34]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaUtak] ADD  CONSTRAINT [DF__KRT_MappaUt__Ver__33807B34]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappa__ErvKe__34749F6D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaUtak] ADD  CONSTRAINT [DF__KRT_Mappa__ErvKe__34749F6D]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_MappaUtak_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaUtak] ADD  CONSTRAINT [DF_KRT_MappaUtak_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappa__Letre__365CE7DF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_MappaUtak] ADD  CONSTRAINT [DF__KRT_Mappa__Letre__365CE7DF]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
