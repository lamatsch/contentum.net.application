IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappa__Letre__5F691F13]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mappak] DROP CONSTRAINT [DF__KRT_Mappa__Letre__5F691F13]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Mappak_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mappak] DROP CONSTRAINT [DF_KRT_Mappak_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappa__ErvKe__5D80D6A1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mappak] DROP CONSTRAINT [DF__KRT_Mappa__ErvKe__5D80D6A1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappak__Ver__5C8CB268]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mappak] DROP CONSTRAINT [DF__KRT_Mappak__Ver__5C8CB268]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappak__Id__5B988E2F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mappak] DROP CONSTRAINT [DF__KRT_Mappak__Id__5B988E2F]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Mappak]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Mappak]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Mappak]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Mappak](
	[Id] [uniqueidentifier] NOT NULL,
	[SzuloMappa_Id] [uniqueidentifier] NULL,
	[BarCode] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NOT NULL,
	[Leiras] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NOT NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KRT_MAPPAK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappak__Id__5B988E2F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mappak] ADD  CONSTRAINT [DF__KRT_Mappak__Id__5B988E2F]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappak__Ver__5C8CB268]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mappak] ADD  CONSTRAINT [DF__KRT_Mappak__Ver__5C8CB268]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappa__ErvKe__5D80D6A1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mappak] ADD  CONSTRAINT [DF__KRT_Mappa__ErvKe__5D80D6A1]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Mappak_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mappak] ADD  CONSTRAINT [DF_KRT_Mappak_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mappa__Letre__5F691F13]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mappak] ADD  CONSTRAINT [DF__KRT_Mappa__Letre__5F691F13]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
