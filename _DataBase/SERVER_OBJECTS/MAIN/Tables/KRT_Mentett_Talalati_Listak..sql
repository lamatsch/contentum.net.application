IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mente__Letre__3A2D78C3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mentett_Talalati_Listak] DROP CONSTRAINT [DF__KRT_Mente__Letre__3A2D78C3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mente__ErvKe__3939548A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mentett_Talalati_Listak] DROP CONSTRAINT [DF__KRT_Mente__ErvKe__3939548A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mentett__Ver__38453051]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mentett_Talalati_Listak] DROP CONSTRAINT [DF__KRT_Mentett__Ver__38453051]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mentett___Id__37510C18]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mentett_Talalati_Listak] DROP CONSTRAINT [DF__KRT_Mentett___Id__37510C18]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Mentett_Talalati_Listak]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Mentett_Talalati_Listak]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Mentett_Talalati_Listak]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Mentett_Talalati_Listak](
	[Id] [uniqueidentifier] NOT NULL,
	[ListName] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SaveDate] [datetime] NULL,
	[Searched_Table] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Requestor] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[HeaderXML] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[BodyXML] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[WorkStation] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_MENTETT_TALALATI_LISTAK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mentett___Id__37510C18]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mentett_Talalati_Listak] ADD  CONSTRAINT [DF__KRT_Mentett___Id__37510C18]  DEFAULT ('newsequentialid()') FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mentett__Ver__38453051]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mentett_Talalati_Listak] ADD  CONSTRAINT [DF__KRT_Mentett__Ver__38453051]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mente__ErvKe__3939548A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mentett_Talalati_Listak] ADD  CONSTRAINT [DF__KRT_Mente__ErvKe__3939548A]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mente__Letre__3A2D78C3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mentett_Talalati_Listak] ADD  CONSTRAINT [DF__KRT_Mente__Letre__3A2D78C3]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
