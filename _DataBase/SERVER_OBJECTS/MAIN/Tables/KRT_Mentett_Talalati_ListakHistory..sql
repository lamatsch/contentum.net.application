IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mente__Histo__3B219CFC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mentett_Talalati_ListakHistory] DROP CONSTRAINT [DF__KRT_Mente__Histo__3B219CFC]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Mentett_Talalati_ListakHistory]') AND name = N'IX_KRT_Mentett_Talalati_ListakHistory_ID_VER')
DROP INDEX [IX_KRT_Mentett_Talalati_ListakHistory_ID_VER] ON [dbo].[KRT_Mentett_Talalati_ListakHistory]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Mentett_Talalati_ListakHistory]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Mentett_Talalati_ListakHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Mentett_Talalati_ListakHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Mentett_Talalati_ListakHistory](
	[HistoryId] [uniqueidentifier] NOT NULL,
	[HistoryMuvelet_Id] [int] NULL,
	[HistoryVegrehajto_Id] [uniqueidentifier] NULL,
	[HistoryVegrehajtasIdo] [datetime] NULL,
	[Id] [uniqueidentifier] NULL,
	[ListName] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SaveDate] [datetime] NULL,
	[Searched_Table] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Requestor] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[HeaderXML] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[BodyXML] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[WorkStation] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK__KRT_Ment__4D7B4ABD6CC31A31] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Mentett_Talalati_ListakHistory]') AND name = N'IX_KRT_Mentett_Talalati_ListakHistory_ID_VER')
CREATE NONCLUSTERED INDEX [IX_KRT_Mentett_Talalati_ListakHistory_ID_VER] ON [dbo].[KRT_Mentett_Talalati_ListakHistory]
(
	[Id] ASC,
	[Ver] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Mente__Histo__3B219CFC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Mentett_Talalati_ListakHistory] ADD  CONSTRAINT [DF__KRT_Mente__Histo__3B219CFC]  DEFAULT (newsequentialid()) FOR [HistoryId]
END

GO
