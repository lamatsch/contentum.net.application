IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Menu_id_szulo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Menuk]'))
ALTER TABLE [dbo].[KRT_Menuk] DROP CONSTRAINT [Menu_id_szulo_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Menu_Funkcio_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Menuk]'))
ALTER TABLE [dbo].[KRT_Menuk] DROP CONSTRAINT [Menu_Funkcio_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_MENU_MODUL_MEN_KRT_MODU]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Menuk]'))
ALTER TABLE [dbo].[KRT_Menuk] DROP CONSTRAINT [FK_KRT_MENU_MODUL_MEN_KRT_MODU]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Menuk]') AND name = N'MNU_MDL_FK_I')
DROP INDEX [MNU_MDL_FK_I] ON [dbo].[KRT_Menuk]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Menuk]') AND name = N'Menu_UK')
DROP INDEX [Menu_UK] ON [dbo].[KRT_Menuk]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Menuk]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Menuk]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Menuk]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Menuk](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Menuk__Id__7093AB15]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NULL,
	[Menu_Id_Szulo] [uniqueidentifier] NULL,
	[Kod] [nvarchar](20) COLLATE Hungarian_CI_AS NOT NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Funkcio_Id] [uniqueidentifier] NULL,
	[Modul_Id] [uniqueidentifier] NULL,
	[Parameter] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Menuk__Ver__7187CF4E]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ImageURL] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Sorrend] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Menuk__ErvKe__727BF387]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Menuk_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Menuk__Letre__74643BF9]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [MNU_1_UK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Menuk]') AND name = N'Menu_UK')
CREATE UNIQUE NONCLUSTERED INDEX [Menu_UK] ON [dbo].[KRT_Menuk]
(
	[Org] ASC,
	[Menu_Id_Szulo] ASC,
	[Kod] ASC,
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Menuk]') AND name = N'MNU_MDL_FK_I')
CREATE NONCLUSTERED INDEX [MNU_MDL_FK_I] ON [dbo].[KRT_Menuk]
(
	[Funkcio_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_MENU_MODUL_MEN_KRT_MODU]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Menuk]'))
ALTER TABLE [dbo].[KRT_Menuk]  WITH CHECK ADD  CONSTRAINT [FK_KRT_MENU_MODUL_MEN_KRT_MODU] FOREIGN KEY([Modul_Id])
REFERENCES [dbo].[KRT_Modulok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_MENU_MODUL_MEN_KRT_MODU]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Menuk]'))
ALTER TABLE [dbo].[KRT_Menuk] CHECK CONSTRAINT [FK_KRT_MENU_MODUL_MEN_KRT_MODU]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Menu_Funkcio_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Menuk]'))
ALTER TABLE [dbo].[KRT_Menuk]  WITH CHECK ADD  CONSTRAINT [Menu_Funkcio_FK] FOREIGN KEY([Funkcio_Id])
REFERENCES [dbo].[KRT_Funkciok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Menu_Funkcio_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Menuk]'))
ALTER TABLE [dbo].[KRT_Menuk] CHECK CONSTRAINT [Menu_Funkcio_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Menu_id_szulo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Menuk]'))
ALTER TABLE [dbo].[KRT_Menuk]  WITH CHECK ADD  CONSTRAINT [Menu_id_szulo_FK] FOREIGN KEY([Menu_Id_Szulo])
REFERENCES [dbo].[KRT_Menuk] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Menu_id_szulo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Menuk]'))
ALTER TABLE [dbo].[KRT_Menuk] CHECK CONSTRAINT [Menu_id_szulo_FK]
GO
