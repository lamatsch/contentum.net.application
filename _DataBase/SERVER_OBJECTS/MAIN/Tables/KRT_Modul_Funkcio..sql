IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Modul_Funkcio__Modul_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Modul_Funkcio]'))
ALTER TABLE [dbo].[KRT_Modul_Funkcio] DROP CONSTRAINT [Modul_Funkcio__Modul_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Modul_Funkcio__Funkcio_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Modul_Funkcio]'))
ALTER TABLE [dbo].[KRT_Modul_Funkcio] DROP CONSTRAINT [Modul_Funkcio__Funkcio_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Modul_Funkcio]') AND name = N'Funkcio_Modul_UK')
DROP INDEX [Funkcio_Modul_UK] ON [dbo].[KRT_Modul_Funkcio]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Modul_Funkcio]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Modul_Funkcio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Modul_Funkcio]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Modul_Funkcio](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Modul_Fu__Id__7740A8A4]  DEFAULT (newsequentialid()),
	[Funkcio_Id] [uniqueidentifier] NOT NULL,
	[Modul_Id] [uniqueidentifier] NOT NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Modul_F__Ver__7834CCDD]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Modul__ErvKe__7928F116]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Modul_Funkcio_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Modul__Letre__7B113988]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_MODUL_FUNKCIO] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Modul_Funkcio]') AND name = N'Funkcio_Modul_UK')
CREATE UNIQUE NONCLUSTERED INDEX [Funkcio_Modul_UK] ON [dbo].[KRT_Modul_Funkcio]
(
	[Funkcio_Id] ASC,
	[Modul_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Modul_Funkcio__Funkcio_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Modul_Funkcio]'))
ALTER TABLE [dbo].[KRT_Modul_Funkcio]  WITH CHECK ADD  CONSTRAINT [Modul_Funkcio__Funkcio_FK] FOREIGN KEY([Funkcio_Id])
REFERENCES [dbo].[KRT_Funkciok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Modul_Funkcio__Funkcio_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Modul_Funkcio]'))
ALTER TABLE [dbo].[KRT_Modul_Funkcio] CHECK CONSTRAINT [Modul_Funkcio__Funkcio_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Modul_Funkcio__Modul_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Modul_Funkcio]'))
ALTER TABLE [dbo].[KRT_Modul_Funkcio]  WITH CHECK ADD  CONSTRAINT [Modul_Funkcio__Modul_FK] FOREIGN KEY([Modul_Id])
REFERENCES [dbo].[KRT_Modulok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Modul_Funkcio__Modul_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Modul_Funkcio]'))
ALTER TABLE [dbo].[KRT_Modul_Funkcio] CHECK CONSTRAINT [Modul_Funkcio__Modul_FK]
GO
