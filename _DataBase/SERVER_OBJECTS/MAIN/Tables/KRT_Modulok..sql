IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Modulok]') AND name = N'KRT_Modul_Ind01')
DROP INDEX [KRT_Modul_Ind01] ON [dbo].[KRT_Modulok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Modulok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Modulok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Modulok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Modulok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Modulok__Id__7DEDA633]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NULL,
	[Tipus] [nvarchar](10) COLLATE Hungarian_CS_AS NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Leiras] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[SelectString] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id_Tulaj] [uniqueidentifier] NULL,
	[Parameterek] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ObjTip_Id_Adatelem] [uniqueidentifier] NULL,
	[ObjTip_Adatelem] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Modulok__Ver__7EE1CA6C]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Modul__ErvKe__7FD5EEA5]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Modulok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Modul__Letre__01BE3717]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_ASPXLapok] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Modulok]') AND name = N'KRT_Modul_Ind01')
CREATE NONCLUSTERED INDEX [KRT_Modul_Ind01] ON [dbo].[KRT_Modulok]
(
	[Org] ASC,
	[Kod] ASC,
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
