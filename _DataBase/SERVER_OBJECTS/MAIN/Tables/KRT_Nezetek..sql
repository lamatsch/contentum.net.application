IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Nezet_Muvelet_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Nezetek]'))
ALTER TABLE [dbo].[KRT_Nezetek] DROP CONSTRAINT [Nezet_Muvelet_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Nezet_Lap_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Nezetek]'))
ALTER TABLE [dbo].[KRT_Nezetek] DROP CONSTRAINT [Nezet_Lap_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Nezet__Letre__3FE65219]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Nezetek] DROP CONSTRAINT [DF__KRT_Nezet__Letre__3FE65219]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Nezetek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Nezetek] DROP CONSTRAINT [DF_KRT_Nezetek_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Nezet__ErvKe__3DFE09A7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Nezetek] DROP CONSTRAINT [DF__KRT_Nezet__ErvKe__3DFE09A7]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Nezetek__Ver__3D09E56E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Nezetek] DROP CONSTRAINT [DF__KRT_Nezetek__Ver__3D09E56E]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Nezetek__Id__3C15C135]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Nezetek] DROP CONSTRAINT [DF__KRT_Nezetek__Id__3C15C135]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Nezetek]') AND name = N'KRT_Nezet_Ind01')
DROP INDEX [KRT_Nezet_Ind01] ON [dbo].[KRT_Nezetek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Nezetek]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Nezetek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Nezetek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Nezetek](
	[Id] [uniqueidentifier] NOT NULL,
	[Org] [uniqueidentifier] NOT NULL,
	[Form_Id] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Leiras] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Csoport_Id] [uniqueidentifier] NULL,
	[Muvelet_Id] [uniqueidentifier] NULL,
	[DisableControls] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[InvisibleControls] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ReadOnlyControls] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Prioritas] [int] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_NEZETEK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Nezetek]') AND name = N'KRT_Nezet_Ind01')
CREATE NONCLUSTERED INDEX [KRT_Nezet_Ind01] ON [dbo].[KRT_Nezetek]
(
	[Org] ASC,
	[Form_Id] ASC,
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Nezetek__Id__3C15C135]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Nezetek] ADD  CONSTRAINT [DF__KRT_Nezetek__Id__3C15C135]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Nezetek__Ver__3D09E56E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Nezetek] ADD  CONSTRAINT [DF__KRT_Nezetek__Ver__3D09E56E]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Nezet__ErvKe__3DFE09A7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Nezetek] ADD  CONSTRAINT [DF__KRT_Nezet__ErvKe__3DFE09A7]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Nezetek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Nezetek] ADD  CONSTRAINT [DF_KRT_Nezetek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Nezet__Letre__3FE65219]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Nezetek] ADD  CONSTRAINT [DF__KRT_Nezet__Letre__3FE65219]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Nezet_Lap_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Nezetek]'))
ALTER TABLE [dbo].[KRT_Nezetek]  WITH CHECK ADD  CONSTRAINT [Nezet_Lap_FK] FOREIGN KEY([Form_Id])
REFERENCES [dbo].[KRT_Modulok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Nezet_Lap_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Nezetek]'))
ALTER TABLE [dbo].[KRT_Nezetek] CHECK CONSTRAINT [Nezet_Lap_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Nezet_Muvelet_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Nezetek]'))
ALTER TABLE [dbo].[KRT_Nezetek]  WITH CHECK ADD  CONSTRAINT [Nezet_Muvelet_FK] FOREIGN KEY([Muvelet_Id])
REFERENCES [dbo].[KRT_Muveletek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Nezet_Muvelet_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Nezetek]'))
ALTER TABLE [dbo].[KRT_Nezetek] CHECK CONSTRAINT [Nezet_Muvelet_FK]
GO
