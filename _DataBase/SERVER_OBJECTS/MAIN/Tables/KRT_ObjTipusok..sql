IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ObjTipus_KodCsoport_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]'))
ALTER TABLE [dbo].[KRT_ObjTipusok] DROP CONSTRAINT [ObjTipus_KodCsoport_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTip_ObjTip_Tipus_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]'))
ALTER TABLE [dbo].[KRT_ObjTipusok] DROP CONSTRAINT [KRT_ObjTip_ObjTip_Tipus_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTip_ObjTip_Szulo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]'))
ALTER TABLE [dbo].[KRT_ObjTipusok] DROP CONSTRAINT [KRT_ObjTip_ObjTip_Szulo_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]') AND name = N'NonClusteredIndex-20160309-175338')
DROP INDEX [NonClusteredIndex-20160309-175338] ON [dbo].[KRT_ObjTipusok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]') AND name = N'NonClusteredIndex-20160309-175102')
DROP INDEX [NonClusteredIndex-20160309-175102] ON [dbo].[KRT_ObjTipusok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]') AND name = N'KRT_ObjTipusok_UK')
DROP INDEX [KRT_ObjTipusok_UK] ON [dbo].[KRT_ObjTipusok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]') AND name = N'KRT_ObjTipusok_kod')
DROP INDEX [KRT_ObjTipusok_kod] ON [dbo].[KRT_ObjTipusok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_ObjTipusok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_ObjTipusok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_ObjTipus__Id__0D2FE9C3]  DEFAULT (newsequentialid()),
	[ObjTipus_Id_Tipus] [uniqueidentifier] NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Obj_Id_Szulo] [uniqueidentifier] NULL,
	[KodCsoport_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_ObjTipu__Ver__0E240DFC]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_ObjTi__ErvKe__0F183235]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_ObjTipusok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_ObjTi__Letre__11007AA7]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [KRT_OBJEKTUMTIPUSOK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]') AND name = N'KRT_ObjTipusok_kod')
CREATE NONCLUSTERED INDEX [KRT_ObjTipusok_kod] ON [dbo].[KRT_ObjTipusok]
(
	[Kod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]') AND name = N'KRT_ObjTipusok_UK')
CREATE NONCLUSTERED INDEX [KRT_ObjTipusok_UK] ON [dbo].[KRT_ObjTipusok]
(
	[Obj_Id_Szulo] ASC,
	[Kod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]') AND name = N'NonClusteredIndex-20160309-175102')
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20160309-175102] ON [dbo].[KRT_ObjTipusok]
(
	[ObjTipus_Id_Tipus] ASC,
	[Kod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]') AND name = N'NonClusteredIndex-20160309-175338')
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20160309-175338] ON [dbo].[KRT_ObjTipusok]
(
	[ObjTipus_Id_Tipus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTip_ObjTip_Szulo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]'))
ALTER TABLE [dbo].[KRT_ObjTipusok]  WITH CHECK ADD  CONSTRAINT [KRT_ObjTip_ObjTip_Szulo_FK] FOREIGN KEY([Obj_Id_Szulo])
REFERENCES [dbo].[KRT_ObjTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTip_ObjTip_Szulo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]'))
ALTER TABLE [dbo].[KRT_ObjTipusok] CHECK CONSTRAINT [KRT_ObjTip_ObjTip_Szulo_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTip_ObjTip_Tipus_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]'))
ALTER TABLE [dbo].[KRT_ObjTipusok]  WITH CHECK ADD  CONSTRAINT [KRT_ObjTip_ObjTip_Tipus_FK] FOREIGN KEY([ObjTipus_Id_Tipus])
REFERENCES [dbo].[KRT_ObjTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ObjTip_ObjTip_Tipus_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]'))
ALTER TABLE [dbo].[KRT_ObjTipusok] CHECK CONSTRAINT [KRT_ObjTip_ObjTip_Tipus_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ObjTipus_KodCsoport_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]'))
ALTER TABLE [dbo].[KRT_ObjTipusok]  WITH CHECK ADD  CONSTRAINT [ObjTipus_KodCsoport_FK] FOREIGN KEY([KodCsoport_Id])
REFERENCES [dbo].[KRT_KodCsoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[ObjTipus_KodCsoport_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_ObjTipusok]'))
ALTER TABLE [dbo].[KRT_ObjTipusok] CHECK CONSTRAINT [ObjTipus_KodCsoport_FK]
GO
