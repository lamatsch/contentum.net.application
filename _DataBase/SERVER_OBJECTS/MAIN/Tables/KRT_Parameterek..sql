IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Parameter_Felhasznalo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Parameterek]'))
ALTER TABLE [dbo].[KRT_Parameterek] DROP CONSTRAINT [Parameter_Felhasznalo_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Parameterek]') AND name = N'KRT_PAR_PAR_NEV_UK')
DROP INDEX [KRT_PAR_PAR_NEV_UK] ON [dbo].[KRT_Parameterek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Parameterek]') AND name = N'FK_Felhasznalo_id')
DROP INDEX [FK_Felhasznalo_id] ON [dbo].[KRT_Parameterek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Parameterek]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Parameterek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Parameterek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Parameterek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Paramete__Id__2136E270]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NULL,
	[Felhasznalo_id] [uniqueidentifier] NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NOT NULL,
	[Ertek] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Karbantarthato] [char](1) COLLATE Hungarian_CI_AS NOT NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Paramet__Ver__222B06A9]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Param__ErvKe__231F2AE2]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Parameterek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Param__Letre__25077354]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PAR_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Parameterek]') AND name = N'FK_Felhasznalo_id')
CREATE NONCLUSTERED INDEX [FK_Felhasznalo_id] ON [dbo].[KRT_Parameterek]
(
	[Felhasznalo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Parameterek]') AND name = N'KRT_PAR_PAR_NEV_UK')
CREATE NONCLUSTERED INDEX [KRT_PAR_PAR_NEV_UK] ON [dbo].[KRT_Parameterek]
(
	[Org] ASC,
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Parameter_Felhasznalo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Parameterek]'))
ALTER TABLE [dbo].[KRT_Parameterek]  WITH CHECK ADD  CONSTRAINT [Parameter_Felhasznalo_FK] FOREIGN KEY([Felhasznalo_id])
REFERENCES [dbo].[KRT_Felhasznalok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Parameter_Felhasznalo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Parameterek]'))
ALTER TABLE [dbo].[KRT_Parameterek] CHECK CONSTRAINT [Parameter_Felhasznalo_FK]
GO
