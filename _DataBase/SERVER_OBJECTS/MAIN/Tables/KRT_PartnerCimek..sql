IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PartnerCimek_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerCimek]'))
ALTER TABLE [dbo].[KRT_PartnerCimek] DROP CONSTRAINT [PartnerCimek_Partner_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerCimek]') AND name = N'PTC_PRT_FK_UK')
DROP INDEX [PTC_PRT_FK_UK] ON [dbo].[KRT_PartnerCimek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerCimek]') AND name = N'FK_Cim_Id')
DROP INDEX [FK_Cim_Id] ON [dbo].[KRT_PartnerCimek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerCimek]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_PartnerCimek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerCimek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_PartnerCimek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_PartnerC__Id__27E3DFFF]  DEFAULT (newsequentialid()),
	[Partner_id] [uniqueidentifier] NULL,
	[Cim_Id] [uniqueidentifier] NOT NULL,
	[UtolsoHasznalatSiker] [char](1) COLLATE Hungarian_CI_AS NULL,
	[UtolsoHasznalatiIdo] [datetime] NULL,
	[eMailKuldes] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Fajta] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Partner__Ver__28D80438]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Partn__ErvKe__29CC2871]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_PartnerCimek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Partn__Letre__2BB470E3]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PTC_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerCimek]') AND name = N'FK_Cim_Id')
CREATE NONCLUSTERED INDEX [FK_Cim_Id] ON [dbo].[KRT_PartnerCimek]
(
	[Cim_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerCimek]') AND name = N'PTC_PRT_FK_UK')
CREATE NONCLUSTERED INDEX [PTC_PRT_FK_UK] ON [dbo].[KRT_PartnerCimek]
(
	[Partner_id] ASC,
	[Cim_Id] ASC,
	[Fajta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PartnerCimek_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerCimek]'))
ALTER TABLE [dbo].[KRT_PartnerCimek]  WITH CHECK ADD  CONSTRAINT [PartnerCimek_Partner_FK] FOREIGN KEY([Partner_id])
REFERENCES [dbo].[KRT_Partnerek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PartnerCimek_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerCimek]'))
ALTER TABLE [dbo].[KRT_PartnerCimek] CHECK CONSTRAINT [PartnerCimek_Partner_FK]
GO
