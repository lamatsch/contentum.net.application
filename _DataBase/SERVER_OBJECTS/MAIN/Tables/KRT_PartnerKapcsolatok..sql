IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PTK_PRT_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatok]'))
ALTER TABLE [dbo].[KRT_PartnerKapcsolatok] DROP CONSTRAINT [PTK_PRT_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_PART_PTK_PRT_P_KRT_PART]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatok]'))
ALTER TABLE [dbo].[KRT_PartnerKapcsolatok] DROP CONSTRAINT [FK_KRT_PART_PTK_PRT_P_KRT_PART]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatok]') AND name = N'KRT_Partner_KapcsoltPartner_UK')
DROP INDEX [KRT_Partner_KapcsoltPartner_UK] ON [dbo].[KRT_PartnerKapcsolatok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatok]') AND name = N'KRT_Partner_KapcsoltPartner_Tipus_I2')
DROP INDEX [KRT_Partner_KapcsoltPartner_Tipus_I2] ON [dbo].[KRT_PartnerKapcsolatok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatok]') AND name = N'FK_Partner_id_kapcsolt')
DROP INDEX [FK_Partner_id_kapcsolt] ON [dbo].[KRT_PartnerKapcsolatok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_PartnerKapcsolatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_PartnerKapcsolatok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_PartnerK__Id__3726238F]  DEFAULT (newsequentialid()),
	[Tipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Partner_id] [uniqueidentifier] NULL,
	[Partner_id_kapcsolt] [uniqueidentifier] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Partner__Ver__381A47C8]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Partn__ErvKe__390E6C01]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_PartnerKapcsolatok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Partn__Letre__3AF6B473]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PTK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatok]') AND name = N'FK_Partner_id_kapcsolt')
CREATE NONCLUSTERED INDEX [FK_Partner_id_kapcsolt] ON [dbo].[KRT_PartnerKapcsolatok]
(
	[Partner_id_kapcsolt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatok]') AND name = N'KRT_Partner_KapcsoltPartner_Tipus_I2')
CREATE NONCLUSTERED INDEX [KRT_Partner_KapcsoltPartner_Tipus_I2] ON [dbo].[KRT_PartnerKapcsolatok]
(
	[Partner_id] ASC,
	[Tipus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatok]') AND name = N'KRT_Partner_KapcsoltPartner_UK')
CREATE NONCLUSTERED INDEX [KRT_Partner_KapcsoltPartner_UK] ON [dbo].[KRT_PartnerKapcsolatok]
(
	[Partner_id] ASC,
	[Partner_id_kapcsolt] ASC,
	[Tipus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_PART_PTK_PRT_P_KRT_PART]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatok]'))
ALTER TABLE [dbo].[KRT_PartnerKapcsolatok]  WITH CHECK ADD  CONSTRAINT [FK_KRT_PART_PTK_PRT_P_KRT_PART] FOREIGN KEY([Partner_id_kapcsolt])
REFERENCES [dbo].[KRT_Partnerek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_PART_PTK_PRT_P_KRT_PART]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatok]'))
ALTER TABLE [dbo].[KRT_PartnerKapcsolatok] CHECK CONSTRAINT [FK_KRT_PART_PTK_PRT_P_KRT_PART]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PTK_PRT_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatok]'))
ALTER TABLE [dbo].[KRT_PartnerKapcsolatok]  WITH CHECK ADD  CONSTRAINT [PTK_PRT_FK] FOREIGN KEY([Partner_id])
REFERENCES [dbo].[KRT_Partnerek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PTK_PRT_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatok]'))
ALTER TABLE [dbo].[KRT_PartnerKapcsolatok] CHECK CONSTRAINT [PTK_PRT_FK]
GO
