IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PartnerMinositesek_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesek]'))
ALTER TABLE [dbo].[KRT_PartnerMinositesek] DROP CONSTRAINT [PartnerMinositesek_Partner_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PartnerMinositesek_KeroFelhasznalo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesek]'))
ALTER TABLE [dbo].[KRT_PartnerMinositesek] DROP CONSTRAINT [PartnerMinositesek_KeroFelhasznalo_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PartnerMinositesek_Felhasznalo_DontoFK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesek]'))
ALTER TABLE [dbo].[KRT_PartnerMinositesek] DROP CONSTRAINT [PartnerMinositesek_Felhasznalo_DontoFK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Partn__Letre__46934FA8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesek] DROP CONSTRAINT [DF__KRT_Partn__Letre__46934FA8]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_PartnerMinositesek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesek] DROP CONSTRAINT [DF_KRT_PartnerMinositesek_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Partn__ErvKe__44AB0736]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesek] DROP CONSTRAINT [DF__KRT_Partn__ErvKe__44AB0736]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Partner__Ver__43B6E2FD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesek] DROP CONSTRAINT [DF__KRT_Partner__Ver__43B6E2FD]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_PartnerMinositesek_KerelemDontesIdo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesek] DROP CONSTRAINT [DF_KRT_PartnerMinositesek_KerelemDontesIdo]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Partn__Kerel__41CE9A8B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesek] DROP CONSTRAINT [DF__KRT_Partn__Kerel__41CE9A8B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_PartnerM__Id__40DA7652]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesek] DROP CONSTRAINT [DF__KRT_PartnerM__Id__40DA7652]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesek]') AND name = N'TPSEC_MINOSITES_I')
DROP INDEX [TPSEC_MINOSITES_I] ON [dbo].[KRT_PartnerMinositesek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesek]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_PartnerMinositesek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_PartnerMinositesek](
	[Id] [uniqueidentifier] NOT NULL,
	[ALLAPOT] [char](1) COLLATE Hungarian_CS_AS NOT NULL,
	[Partner_id] [uniqueidentifier] NOT NULL,
	[Felhasznalo_id_kero] [uniqueidentifier] NOT NULL,
	[KertMinosites] [nvarchar](64) COLLATE Hungarian_CS_AS NOT NULL,
	[KertKezdDat] [datetime] NULL,
	[KertVegeDat] [datetime] NULL,
	[KerelemAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KerelemBeadIdo] [datetime] NOT NULL,
	[KerelemDontesIdo] [datetime] NOT NULL,
	[Felhasznalo_id_donto] [uniqueidentifier] NOT NULL,
	[DontesAzonosito] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Minosites] [nvarchar](2) COLLATE Hungarian_CS_AS NULL,
	[MinositesKezdDat] [datetime] NULL,
	[MinositesVegDat] [datetime] NULL,
	[SztornirozasDat] [datetime] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PSEC_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesek]') AND name = N'TPSEC_MINOSITES_I')
CREATE NONCLUSTERED INDEX [TPSEC_MINOSITES_I] ON [dbo].[KRT_PartnerMinositesek]
(
	[Partner_id] ASC,
	[Felhasznalo_id_kero] ASC,
	[Felhasznalo_id_donto] ASC,
	[ALLAPOT] ASC,
	[Minosites] ASC,
	[KertMinosites] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_PartnerM__Id__40DA7652]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesek] ADD  CONSTRAINT [DF__KRT_PartnerM__Id__40DA7652]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Partn__Kerel__41CE9A8B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesek] ADD  CONSTRAINT [DF__KRT_Partn__Kerel__41CE9A8B]  DEFAULT (getdate()) FOR [KerelemBeadIdo]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_PartnerMinositesek_KerelemDontesIdo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesek] ADD  CONSTRAINT [DF_KRT_PartnerMinositesek_KerelemDontesIdo]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [KerelemDontesIdo]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Partner__Ver__43B6E2FD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesek] ADD  CONSTRAINT [DF__KRT_Partner__Ver__43B6E2FD]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Partn__ErvKe__44AB0736]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesek] ADD  CONSTRAINT [DF__KRT_Partn__ErvKe__44AB0736]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_PartnerMinositesek_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesek] ADD  CONSTRAINT [DF_KRT_PartnerMinositesek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Partn__Letre__46934FA8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_PartnerMinositesek] ADD  CONSTRAINT [DF__KRT_Partn__Letre__46934FA8]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PartnerMinositesek_Felhasznalo_DontoFK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesek]'))
ALTER TABLE [dbo].[KRT_PartnerMinositesek]  WITH CHECK ADD  CONSTRAINT [PartnerMinositesek_Felhasznalo_DontoFK] FOREIGN KEY([Felhasznalo_id_donto])
REFERENCES [dbo].[KRT_Felhasznalok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PartnerMinositesek_Felhasznalo_DontoFK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesek]'))
ALTER TABLE [dbo].[KRT_PartnerMinositesek] CHECK CONSTRAINT [PartnerMinositesek_Felhasznalo_DontoFK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PartnerMinositesek_KeroFelhasznalo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesek]'))
ALTER TABLE [dbo].[KRT_PartnerMinositesek]  WITH CHECK ADD  CONSTRAINT [PartnerMinositesek_KeroFelhasznalo_FK] FOREIGN KEY([Felhasznalo_id_kero])
REFERENCES [dbo].[KRT_Felhasznalok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PartnerMinositesek_KeroFelhasznalo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesek]'))
ALTER TABLE [dbo].[KRT_PartnerMinositesek] CHECK CONSTRAINT [PartnerMinositesek_KeroFelhasznalo_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PartnerMinositesek_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesek]'))
ALTER TABLE [dbo].[KRT_PartnerMinositesek]  WITH CHECK ADD  CONSTRAINT [PartnerMinositesek_Partner_FK] FOREIGN KEY([Partner_id])
REFERENCES [dbo].[KRT_Partnerek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PartnerMinositesek_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesek]'))
ALTER TABLE [dbo].[KRT_PartnerMinositesek] CHECK CONSTRAINT [PartnerMinositesek_Partner_FK]
GO
