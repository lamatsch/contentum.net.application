IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Partnerek]') AND name = N'Partner_ErvVege_I')
DROP INDEX [Partner_ErvVege_I] ON [dbo].[KRT_Partnerek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Partnerek]') AND name = N'Partner_ErvKezd_I')
DROP INDEX [Partner_ErvKezd_I] ON [dbo].[KRT_Partnerek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Partnerek]') AND name = N'KRT_PRT_MEGNEVEZES')
DROP INDEX [KRT_PRT_MEGNEVEZES] ON [dbo].[KRT_Partnerek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Partnerek]') AND name = N'KRT_Partner_Hier_I')
DROP INDEX [KRT_Partner_Hier_I] ON [dbo].[KRT_Partnerek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Partnerek]') AND name = N'_dta_index_KRT_Partnerek_5_1600112841__K16')
DROP INDEX [_dta_index_KRT_Partnerek_5_1600112841__K16] ON [dbo].[KRT_Partnerek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Partnerek]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Partnerek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Partnerek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Partnerek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Partnere__Id__2E90DD8E]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NULL,
	[Orszag_Id] [uniqueidentifier] NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Nev] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[Hierarchia] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[KulsoAzonositok] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[PublikusKulcs] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Kiszolgalo] [uniqueidentifier] NULL,
	[LetrehozoSzervezet] [uniqueidentifier] NULL,
	[MaxMinosites] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[MinositesKezdDat] [datetime] NULL CONSTRAINT [DF__KRT_Partn__Minos__2F8501C7]  DEFAULT (getdate()),
	[MinositesVegDat] [datetime] NULL CONSTRAINT [DF_KRT_Partnerek_MinositesVegDat]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[MaxMinositesSzervezet] [nvarchar](2) COLLATE Hungarian_CI_AS NULL,
	[Belso] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Forras] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Partner__Ver__316D4A39]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Partn__ErvKe__32616E72]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Partnerek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Partn__Letre__3449B6E4]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PRT_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Partnerek]') AND name = N'_dta_index_KRT_Partnerek_5_1600112841__K16')
CREATE NONCLUSTERED INDEX [_dta_index_KRT_Partnerek_5_1600112841__K16] ON [dbo].[KRT_Partnerek]
(
	[Forras] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Partnerek]') AND name = N'KRT_Partner_Hier_I')
CREATE NONCLUSTERED INDEX [KRT_Partner_Hier_I] ON [dbo].[KRT_Partnerek]
(
	[Hierarchia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Partnerek]') AND name = N'KRT_PRT_MEGNEVEZES')
CREATE NONCLUSTERED INDEX [KRT_PRT_MEGNEVEZES] ON [dbo].[KRT_Partnerek]
(
	[Org] ASC,
	[Nev] ASC,
	[Orszag_Id] ASC,
	[ErvKezd] ASC,
	[ErvVege] ASC
)
INCLUDE ( 	[Id],
	[Belso],
	[Forras]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Partnerek]') AND name = N'Partner_ErvKezd_I')
CREATE NONCLUSTERED INDEX [Partner_ErvKezd_I] ON [dbo].[KRT_Partnerek]
(
	[ErvKezd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Partnerek]') AND name = N'Partner_ErvVege_I')
CREATE NONCLUSTERED INDEX [Partner_ErvVege_I] ON [dbo].[KRT_Partnerek]
(
	[ErvVege] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
