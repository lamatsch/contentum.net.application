IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_RAGS_KRT_RAGSZ_EREC_IRA]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_RagSzamok]'))
ALTER TABLE [dbo].[KRT_RagSzamok] DROP CONSTRAINT [FK_KRT_RAGS_KRT_RAGSZ_EREC_IRA]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_RagSz__Letre__2240E099]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagSzamok] DROP CONSTRAINT [DF__KRT_RagSz__Letre__2240E099]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_RagSzamok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagSzamok] DROP CONSTRAINT [DF_KRT_RagSzamok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_RagSz__ErvKe__214CBC60]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagSzamok] DROP CONSTRAINT [DF__KRT_RagSz__ErvKe__214CBC60]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_RagSzam__Ver__20589827]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagSzamok] DROP CONSTRAINT [DF__KRT_RagSzam__Ver__20589827]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_RagSzamo__Id__1F6473EE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagSzamok] DROP CONSTRAINT [DF__KRT_RagSzamo__Id__1F6473EE]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagSzamok]') AND name = N'Ragszam_UK')
DROP INDEX [Ragszam_UK] ON [dbo].[KRT_RagSzamok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagSzamok]') AND name = N'Index_2')
DROP INDEX [Index_2] ON [dbo].[KRT_RagSzamok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagSzamok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_RagSzamok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagSzamok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_RagSzamok](
	[Id] [uniqueidentifier] NOT NULL,
	[Kod] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[KodNum] [float] NULL,
	[Postakonyv_Id] [uniqueidentifier] NULL,
	[RagszamSav_Id] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[ObjTip_Id] [uniqueidentifier] NULL,
	[Obj_type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[KodType] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_RAGSZAMOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagSzamok]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[KRT_RagSzamok]
(
	[RagszamSav_Id] ASC,
	[KodNum] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagSzamok]') AND name = N'Ragszam_UK')
CREATE NONCLUSTERED INDEX [Ragszam_UK] ON [dbo].[KRT_RagSzamok]
(
	[Kod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_RagSzamo__Id__1F6473EE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagSzamok] ADD  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_RagSzam__Ver__20589827]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagSzamok] ADD  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_RagSz__ErvKe__214CBC60]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagSzamok] ADD  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_RagSzamok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagSzamok] ADD  CONSTRAINT [DF_KRT_RagSzamok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_RagSz__Letre__2240E099]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagSzamok] ADD  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_RAGS_KRT_RAGSZ_EREC_IRA]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_RagSzamok]'))
ALTER TABLE [dbo].[KRT_RagSzamok]  WITH CHECK ADD  CONSTRAINT [FK_KRT_RAGS_KRT_RAGSZ_EREC_IRA] FOREIGN KEY([Postakonyv_Id])
REFERENCES [dbo].[EREC_IraIktatoKonyvek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_RAGS_KRT_RAGSZ_EREC_IRA]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_RagSzamok]'))
ALTER TABLE [dbo].[KRT_RagSzamok] CHECK CONSTRAINT [FK_KRT_RAGS_KRT_RAGSZ_EREC_IRA]
GO
