IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_RAGS_KRT_CSOP]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavok]'))
ALTER TABLE [dbo].[KRT_RagszamSavok] DROP CONSTRAINT [FK_KRT_RAGS_KRT_CSOP]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_RAGS_EREC_IRA_IKTKONYV]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavok]'))
ALTER TABLE [dbo].[KRT_RagszamSavok] DROP CONSTRAINT [FK_KRT_RAGS_EREC_IRA_IKTKONYV]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Ragsz__Letre__2CBE6F0C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagszamSavok] DROP CONSTRAINT [DF__KRT_Ragsz__Letre__2CBE6F0C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_RagszamSavok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagszamSavok] DROP CONSTRAINT [DF_KRT_RagszamSavok_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Ragsz__ErvKe__2BCA4AD3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagszamSavok] DROP CONSTRAINT [DF__KRT_Ragsz__ErvKe__2BCA4AD3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Ragszam__Ver__2AD6269A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagszamSavok] DROP CONSTRAINT [DF__KRT_Ragszam__Ver__2AD6269A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Ragszam__Org__29E20261]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagszamSavok] DROP CONSTRAINT [DF__KRT_Ragszam__Org__29E20261]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_RagszamS__Id__28EDDE28]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagszamSavok] DROP CONSTRAINT [DF__KRT_RagszamS__Id__28EDDE28]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavok]') AND name = N'Sav_Vege_I')
DROP INDEX [Sav_Vege_I] ON [dbo].[KRT_RagszamSavok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavok]') AND name = N'Sav_Kezd_I')
DROP INDEX [Sav_Kezd_I] ON [dbo].[KRT_RagszamSavok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_RagszamSavok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_RagszamSavok](
	[Id] [uniqueidentifier] NOT NULL,
	[Org] [uniqueidentifier] NOT NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NOT NULL,
	[Postakonyv_Id] [uniqueidentifier] NULL,
	[SavKezd] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[SavKezdNum] [float] NULL,
	[SavVege] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[SavVegeNum] [float] NULL,
	[SavType] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SavAllapot] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_RAGSZAMSAVOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavok]') AND name = N'Sav_Kezd_I')
CREATE NONCLUSTERED INDEX [Sav_Kezd_I] ON [dbo].[KRT_RagszamSavok]
(
	[SavKezd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavok]') AND name = N'Sav_Vege_I')
CREATE NONCLUSTERED INDEX [Sav_Vege_I] ON [dbo].[KRT_RagszamSavok]
(
	[SavVege] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_RagszamS__Id__28EDDE28]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagszamSavok] ADD  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Ragszam__Org__29E20261]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagszamSavok] ADD  DEFAULT ('00000000-0000-0000-0000-000000000000') FOR [Org]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Ragszam__Ver__2AD6269A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagszamSavok] ADD  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Ragsz__ErvKe__2BCA4AD3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagszamSavok] ADD  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_RagszamSavok_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagszamSavok] ADD  CONSTRAINT [DF_KRT_RagszamSavok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Ragsz__Letre__2CBE6F0C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_RagszamSavok] ADD  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_RAGS_EREC_IRA_IKTKONYV]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavok]'))
ALTER TABLE [dbo].[KRT_RagszamSavok]  WITH CHECK ADD  CONSTRAINT [FK_KRT_RAGS_EREC_IRA_IKTKONYV] FOREIGN KEY([Postakonyv_Id])
REFERENCES [dbo].[EREC_IraIktatoKonyvek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_RAGS_EREC_IRA_IKTKONYV]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavok]'))
ALTER TABLE [dbo].[KRT_RagszamSavok] CHECK CONSTRAINT [FK_KRT_RAGS_EREC_IRA_IKTKONYV]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_RAGS_KRT_CSOP]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavok]'))
ALTER TABLE [dbo].[KRT_RagszamSavok]  WITH CHECK ADD  CONSTRAINT [FK_KRT_RAGS_KRT_CSOP] FOREIGN KEY([Csoport_Id_Felelos])
REFERENCES [dbo].[KRT_Csoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KRT_RAGS_KRT_CSOP]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_RagszamSavok]'))
ALTER TABLE [dbo].[KRT_RagszamSavok] CHECK CONSTRAINT [FK_KRT_RAGS_KRT_CSOP]
GO
