IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Szemely_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Szemelyek]'))
ALTER TABLE [dbo].[KRT_Szemelyek] DROP CONSTRAINT [Szemely_Partner_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szemelyek]') AND name = N'FK_Partner_Id')
DROP INDEX [FK_Partner_Id] ON [dbo].[KRT_Szemelyek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szemelyek]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Szemelyek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szemelyek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Szemelyek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Szemelye__Id__40AF8DC9]  DEFAULT (newsequentialid()),
	[Partner_Id] [uniqueidentifier] NOT NULL,
	[AnyjaNeve] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AnyjaNeveCsaladiNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AnyjaNeveElsoUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AnyjaNeveTovabbiUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[ApjaNeve] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiCsaladiNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiElsoUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiTovabbiUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiOrszag] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiOrszagId] [uniqueidentifier] NULL,
	[UjTitulis] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[UjCsaladiNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UjUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[UjTovabbiUtonev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiHely] [nvarchar](400) COLLATE Hungarian_CI_AS NULL,
	[SzuletesiHely_id] [uniqueidentifier] NULL,
	[SzuletesiIdo] [datetime] NULL,
	[Allampolgarsag] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[TAJSzam] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[SZIGSzam] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Neme] [char](1) COLLATE Hungarian_CI_AS NULL,
	[SzemelyiAzonosito] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Adoazonosito] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Adoszam] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[KulfoldiAdoszamJelolo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Szemely__Ver__41A3B202]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Szeme__ErvKe__4297D63B]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Szemelyek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Szeme__Letre__44801EAD]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_SZEMELYEK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szemelyek]') AND name = N'FK_Partner_Id')
CREATE NONCLUSTERED INDEX [FK_Partner_Id] ON [dbo].[KRT_Szemelyek]
(
	[Partner_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Szemely_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Szemelyek]'))
ALTER TABLE [dbo].[KRT_Szemelyek]  WITH CHECK ADD  CONSTRAINT [Szemely_Partner_FK] FOREIGN KEY([Partner_Id])
REFERENCES [dbo].[KRT_Partnerek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Szemely_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Szemelyek]'))
ALTER TABLE [dbo].[KRT_Szemelyek] CHECK CONSTRAINT [Szemely_Partner_FK]
GO
