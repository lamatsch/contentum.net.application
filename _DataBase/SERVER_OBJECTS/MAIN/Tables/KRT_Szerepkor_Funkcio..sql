IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[SzerepkorFunkcio_Szerepkor_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_Funkcio]'))
ALTER TABLE [dbo].[KRT_Szerepkor_Funkcio] DROP CONSTRAINT [SzerepkorFunkcio_Szerepkor_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[SKF_FUN_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_Funkcio]'))
ALTER TABLE [dbo].[KRT_Szerepkor_Funkcio] DROP CONSTRAINT [SKF_FUN_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_Funkcio]') AND name = N'Szerepkor_Funkcio_UK')
DROP INDEX [Szerepkor_Funkcio_UK] ON [dbo].[KRT_Szerepkor_Funkcio]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_Funkcio]') AND name = N'FK_Funkcio_Id')
DROP INDEX [FK_Funkcio_Id] ON [dbo].[KRT_Szerepkor_Funkcio]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_Funkcio]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Szerepkor_Funkcio]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_Funkcio]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Szerepkor_Funkcio](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Szerepko__Id__475C8B58]  DEFAULT (newsequentialid()),
	[Funkcio_Id] [uniqueidentifier] NOT NULL,
	[Szerepkor_Id] [uniqueidentifier] NOT NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Szerepk__Ver__4850AF91]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Szere__ErvKe__4944D3CA]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Szerepkor_Funkcio_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Szere__Letre__4B2D1C3C]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [SKF_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_Funkcio]') AND name = N'FK_Funkcio_Id')
CREATE NONCLUSTERED INDEX [FK_Funkcio_Id] ON [dbo].[KRT_Szerepkor_Funkcio]
(
	[Funkcio_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_Funkcio]') AND name = N'Szerepkor_Funkcio_UK')
CREATE NONCLUSTERED INDEX [Szerepkor_Funkcio_UK] ON [dbo].[KRT_Szerepkor_Funkcio]
(
	[Szerepkor_Id] ASC,
	[Funkcio_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[SKF_FUN_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_Funkcio]'))
ALTER TABLE [dbo].[KRT_Szerepkor_Funkcio]  WITH CHECK ADD  CONSTRAINT [SKF_FUN_FK] FOREIGN KEY([Funkcio_Id])
REFERENCES [dbo].[KRT_Funkciok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[SKF_FUN_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_Funkcio]'))
ALTER TABLE [dbo].[KRT_Szerepkor_Funkcio] CHECK CONSTRAINT [SKF_FUN_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[SzerepkorFunkcio_Szerepkor_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_Funkcio]'))
ALTER TABLE [dbo].[KRT_Szerepkor_Funkcio]  WITH CHECK ADD  CONSTRAINT [SzerepkorFunkcio_Szerepkor_FK] FOREIGN KEY([Szerepkor_Id])
REFERENCES [dbo].[KRT_Szerepkorok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[SzerepkorFunkcio_Szerepkor_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkor_Funkcio]'))
ALTER TABLE [dbo].[KRT_Szerepkor_Funkcio] CHECK CONSTRAINT [SzerepkorFunkcio_Szerepkor_FK]
GO
