IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkorok]') AND name = N'KRT_Szerepkor_UK')
DROP INDEX [KRT_Szerepkor_UK] ON [dbo].[KRT_Szerepkorok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkorok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Szerepkorok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkorok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Szerepkorok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Szerepko__Id__4E0988E7]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[KulsoAzonositok] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Szerepk__Ver__4EFDAD20]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Szere__ErvKe__4FF1D159]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Szerepkorok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Szere__Letre__51DA19CB]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [SRK_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Szerepkorok]') AND name = N'KRT_Szerepkor_UK')
CREATE NONCLUSTERED INDEX [KRT_Szerepkor_UK] ON [dbo].[KRT_Szerepkorok]
(
	[Org] ASC,
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
