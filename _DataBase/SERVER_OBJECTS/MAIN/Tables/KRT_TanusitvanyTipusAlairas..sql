IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TANALAIRAS_ALAIRASTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_TanusitvanyTipusAlairas]'))
ALTER TABLE [dbo].[KRT_TanusitvanyTipusAlairas] DROP CONSTRAINT [KRT_TANALAIRAS_ALAIRASTIP_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanus__Letre__5D4BCC77]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusAlairas] DROP CONSTRAINT [DF__KRT_Tanus__Letre__5D4BCC77]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanus__ErvKe__5C57A83E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusAlairas] DROP CONSTRAINT [DF__KRT_Tanus__ErvKe__5C57A83E]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanusit__Ver__5B638405]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusAlairas] DROP CONSTRAINT [DF__KRT_Tanusit__Ver__5B638405]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanusitv__Id__5A6F5FCC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusAlairas] DROP CONSTRAINT [DF__KRT_Tanusitv__Id__5A6F5FCC]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TanusitvanyTipusAlairas]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_TanusitvanyTipusAlairas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TanusitvanyTipusAlairas]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_TanusitvanyTipusAlairas](
	[Id] [uniqueidentifier] NOT NULL,
	[AlairasTipus_Id] [uniqueidentifier] NULL,
	[TanusitvanyTipus_Id] [uniqueidentifier] NOT NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_TANUSITVANYTIPUSALAIRAS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanusitv__Id__5A6F5FCC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusAlairas] ADD  CONSTRAINT [DF__KRT_Tanusitv__Id__5A6F5FCC]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanusit__Ver__5B638405]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusAlairas] ADD  CONSTRAINT [DF__KRT_Tanusit__Ver__5B638405]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanus__ErvKe__5C57A83E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusAlairas] ADD  CONSTRAINT [DF__KRT_Tanus__ErvKe__5C57A83E]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanus__Letre__5D4BCC77]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusAlairas] ADD  CONSTRAINT [DF__KRT_Tanus__Letre__5D4BCC77]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TANALAIRAS_ALAIRASTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_TanusitvanyTipusAlairas]'))
ALTER TABLE [dbo].[KRT_TanusitvanyTipusAlairas]  WITH CHECK ADD  CONSTRAINT [KRT_TANALAIRAS_ALAIRASTIP_FK] FOREIGN KEY([AlairasTipus_Id])
REFERENCES [dbo].[KRT_AlairasTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TANALAIRAS_ALAIRASTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_TanusitvanyTipusAlairas]'))
ALTER TABLE [dbo].[KRT_TanusitvanyTipusAlairas] CHECK CONSTRAINT [KRT_TANALAIRAS_ALAIRASTIP_FK]
GO
