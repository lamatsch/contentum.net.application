IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanus__Letre__6304A5CD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusok] DROP CONSTRAINT [DF__KRT_Tanus__Letre__6304A5CD]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanus__ErvKe__62108194]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusok] DROP CONSTRAINT [DF__KRT_Tanus__ErvKe__62108194]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanusit__Ver__611C5D5B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusok] DROP CONSTRAINT [DF__KRT_Tanusit__Ver__611C5D5B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanusitv__Id__60283922]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusok] DROP CONSTRAINT [DF__KRT_Tanusitv__Id__60283922]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TanusitvanyTipusok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_TanusitvanyTipusok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TanusitvanyTipusok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_TanusitvanyTipusok](
	[Id] [uniqueidentifier] NOT NULL,
	[Org] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[TanusitvanyTipus] [nvarchar](8) COLLATE Hungarian_CI_AS NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_TANUSITVANYTIPUSOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanusitv__Id__60283922]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusok] ADD  CONSTRAINT [DF__KRT_Tanusitv__Id__60283922]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanusit__Ver__611C5D5B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusok] ADD  CONSTRAINT [DF__KRT_Tanusit__Ver__611C5D5B]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanus__ErvKe__62108194]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusok] ADD  CONSTRAINT [DF__KRT_Tanus__ErvKe__62108194]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanus__Letre__6304A5CD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TanusitvanyTipusok] ADD  CONSTRAINT [DF__KRT_Tanus__Letre__6304A5CD]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
