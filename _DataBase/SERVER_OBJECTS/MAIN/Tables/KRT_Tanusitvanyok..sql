IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TANUSITV_TANUSTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Tanusitvanyok]'))
ALTER TABLE [dbo].[KRT_Tanusitvanyok] DROP CONSTRAINT [KRT_TANUSITV_TANUSTIP_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TANUSITV_CSOPORT_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Tanusitvanyok]'))
ALTER TABLE [dbo].[KRT_Tanusitvanyok] DROP CONSTRAINT [KRT_TANUSITV_CSOPORT_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanus__Letre__5792F321]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tanusitvanyok] DROP CONSTRAINT [DF__KRT_Tanus__Letre__5792F321]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanus__ErvKe__569ECEE8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tanusitvanyok] DROP CONSTRAINT [DF__KRT_Tanus__ErvKe__569ECEE8]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanusit__Ver__55AAAAAF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tanusitvanyok] DROP CONSTRAINT [DF__KRT_Tanusit__Ver__55AAAAAF]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanusitv__Id__54B68676]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tanusitvanyok] DROP CONSTRAINT [DF__KRT_Tanusitv__Id__54B68676]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tanusitvanyok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Tanusitvanyok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tanusitvanyok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Tanusitvanyok](
	[Id] [uniqueidentifier] NOT NULL,
	[Csoport_Id] [uniqueidentifier] NULL,
	[TanusitvanyTipus_Id] [uniqueidentifier] NULL,
	[Tulajdonos] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Ujjlenyomat] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Tarolas] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Ervenytelenites] [datetime] NULL,
	[Visszavonas] [datetime] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_TANUSITVANYOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanusitv__Id__54B68676]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tanusitvanyok] ADD  CONSTRAINT [DF__KRT_Tanusitv__Id__54B68676]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanusit__Ver__55AAAAAF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tanusitvanyok] ADD  CONSTRAINT [DF__KRT_Tanusit__Ver__55AAAAAF]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanus__ErvKe__569ECEE8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tanusitvanyok] ADD  CONSTRAINT [DF__KRT_Tanus__ErvKe__569ECEE8]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tanus__Letre__5792F321]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tanusitvanyok] ADD  CONSTRAINT [DF__KRT_Tanus__Letre__5792F321]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TANUSITV_CSOPORT_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Tanusitvanyok]'))
ALTER TABLE [dbo].[KRT_Tanusitvanyok]  WITH CHECK ADD  CONSTRAINT [KRT_TANUSITV_CSOPORT_FK] FOREIGN KEY([Csoport_Id])
REFERENCES [dbo].[KRT_Csoportok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TANUSITV_CSOPORT_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Tanusitvanyok]'))
ALTER TABLE [dbo].[KRT_Tanusitvanyok] CHECK CONSTRAINT [KRT_TANUSITV_CSOPORT_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TANUSITV_TANUSTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Tanusitvanyok]'))
ALTER TABLE [dbo].[KRT_Tanusitvanyok]  WITH CHECK ADD  CONSTRAINT [KRT_TANUSITV_TANUSTIP_FK] FOREIGN KEY([TanusitvanyTipus_Id])
REFERENCES [dbo].[KRT_TanusitvanyTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TANUSITV_TANUSTIP_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Tanusitvanyok]'))
ALTER TABLE [dbo].[KRT_Tanusitvanyok] CHECK CONSTRAINT [KRT_TANUSITV_TANUSTIP_FK]
GO
