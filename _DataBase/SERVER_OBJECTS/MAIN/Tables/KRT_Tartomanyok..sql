IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tartomanyok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Tartomanyok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tartomanyok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Tartomanyok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Tartoman__Id__65E11278]  DEFAULT (newsequentialid()),
	[TartomanyKezd] [numeric](12, 0) NULL,
	[TartomanyHossz] [numeric](12, 0) NULL,
	[FoglaltDarab] [numeric](12, 0) NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Tartoma__Ver__66D536B1]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Tarto__ErvKe__67C95AEA]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Tartomanyok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Tarto__Letre__69B1A35C]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_TARTOMANYOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
