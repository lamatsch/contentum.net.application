IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TARTSZERV_BARKODTART_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Tartomanyok_Szervezetek]'))
ALTER TABLE [dbo].[KRT_Tartomanyok_Szervezetek] DROP CONSTRAINT [KRT_TARTSZERV_BARKODTART_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tartomanyok_Szervezetek]') AND name = N'IX_Tartomany_Szervezet')
DROP INDEX [IX_Tartomany_Szervezet] ON [dbo].[KRT_Tartomanyok_Szervezetek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tartomanyok_Szervezetek]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Tartomanyok_Szervezetek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tartomanyok_Szervezetek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Tartomanyok_Szervezetek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Tartoman__Id__6C8E1007]  DEFAULT (newsequentialid()),
	[Tartomany_Id] [uniqueidentifier] NOT NULL,
	[Csoport_Id_Felelos] [uniqueidentifier] NULL,
	[TartType] [char](1) COLLATE Hungarian_CI_AS NOT NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Tartoma__Ver__6D823440]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Tarto__ErvKe__6E765879]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Tartomanyok_Szervezetek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Tarto__Letre__705EA0EB]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_TARTOMANYOK_SZERVEZETEK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tartomanyok_Szervezetek]') AND name = N'IX_Tartomany_Szervezet')
CREATE UNIQUE NONCLUSTERED INDEX [IX_Tartomany_Szervezet] ON [dbo].[KRT_Tartomanyok_Szervezetek]
(
	[TartType] ASC,
	[Csoport_Id_Felelos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TARTSZERV_BARKODTART_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Tartomanyok_Szervezetek]'))
ALTER TABLE [dbo].[KRT_Tartomanyok_Szervezetek]  WITH CHECK ADD  CONSTRAINT [KRT_TARTSZERV_BARKODTART_FK] FOREIGN KEY([Tartomany_Id])
REFERENCES [dbo].[KRT_Tartomanyok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TARTSZERV_BARKODTART_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Tartomanyok_Szervezetek]'))
ALTER TABLE [dbo].[KRT_Tartomanyok_Szervezetek] CHECK CONSTRAINT [KRT_TARTSZERV_BARKODTART_FK]
GO
