IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Telepules_telepules_fo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]'))
ALTER TABLE [dbo].[KRT_Telepulesek] DROP CONSTRAINT [Telepules_telepules_fo_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Telepules_Orszag_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]'))
ALTER TABLE [dbo].[KRT_Telepulesek] DROP CONSTRAINT [Telepules_Orszag_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]') AND name = N'IND_MEGYE_MEGN_TLP')
DROP INDEX [IND_MEGYE_MEGN_TLP] ON [dbo].[KRT_Telepulesek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]') AND name = N'IND_MEGN_IRSZ_TLP_UK')
DROP INDEX [IND_MEGN_IRSZ_TLP_UK] ON [dbo].[KRT_Telepulesek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]') AND name = N'FK_Telepules_Id_Fo')
DROP INDEX [FK_Telepules_Id_Fo] ON [dbo].[KRT_Telepulesek]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]') AND name = N'FK_Orszag_Id')
DROP INDEX [FK_Orszag_Id] ON [dbo].[KRT_Telepulesek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Telepulesek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Telepulesek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Telepule__Id__733B0D96]  DEFAULT (newsequentialid()),
	[Org] [uniqueidentifier] NULL,
	[IRSZ] [nvarchar](10) COLLATE Hungarian_CI_AS NOT NULL,
	[Telepules_Id_Fo] [uniqueidentifier] NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Orszag_Id] [uniqueidentifier] NULL,
	[Megye] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Regio] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Telepul__Ver__742F31CF]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Telep__ErvKe__75235608]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Telepulesek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Telep__Letre__770B9E7A]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_Telepulesek] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]') AND name = N'FK_Orszag_Id')
CREATE NONCLUSTERED INDEX [FK_Orszag_Id] ON [dbo].[KRT_Telepulesek]
(
	[Orszag_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]') AND name = N'FK_Telepules_Id_Fo')
CREATE NONCLUSTERED INDEX [FK_Telepules_Id_Fo] ON [dbo].[KRT_Telepulesek]
(
	[Telepules_Id_Fo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]') AND name = N'IND_MEGN_IRSZ_TLP_UK')
CREATE NONCLUSTERED INDEX [IND_MEGN_IRSZ_TLP_UK] ON [dbo].[KRT_Telepulesek]
(
	[Org] ASC,
	[IRSZ] ASC,
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]') AND name = N'IND_MEGYE_MEGN_TLP')
CREATE NONCLUSTERED INDEX [IND_MEGYE_MEGN_TLP] ON [dbo].[KRT_Telepulesek]
(
	[Megye] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Telepules_Orszag_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]'))
ALTER TABLE [dbo].[KRT_Telepulesek]  WITH CHECK ADD  CONSTRAINT [Telepules_Orszag_FK] FOREIGN KEY([Orszag_Id])
REFERENCES [dbo].[KRT_Orszagok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Telepules_Orszag_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]'))
ALTER TABLE [dbo].[KRT_Telepulesek] CHECK CONSTRAINT [Telepules_Orszag_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Telepules_telepules_fo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]'))
ALTER TABLE [dbo].[KRT_Telepulesek]  WITH CHECK ADD  CONSTRAINT [Telepules_telepules_fo_FK] FOREIGN KEY([Telepules_Id_Fo])
REFERENCES [dbo].[KRT_Telepulesek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Telepules_telepules_fo_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Telepulesek]'))
ALTER TABLE [dbo].[KRT_Telepulesek] CHECK CONSTRAINT [Telepules_telepules_fo_FK]
GO
