IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager_KRT_Modulok_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager]'))
ALTER TABLE [dbo].[KRT_TemplateManager] DROP CONSTRAINT [KRT_TemplateManager_KRT_Modulok_FK]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager_KRT_Dokumentum_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager]'))
ALTER TABLE [dbo].[KRT_TemplateManager] DROP CONSTRAINT [KRT_TemplateManager_KRT_Dokumentum_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Templ__Letre__4A63E08C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TemplateManager] DROP CONSTRAINT [DF__KRT_Templ__Letre__4A63E08C]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Templ__ErvKe__496FBC53]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TemplateManager] DROP CONSTRAINT [DF__KRT_Templ__ErvKe__496FBC53]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Templat__Ver__487B981A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TemplateManager] DROP CONSTRAINT [DF__KRT_Templat__Ver__487B981A]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Template__Id__478773E1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TemplateManager] DROP CONSTRAINT [DF__KRT_Template__Id__478773E1]
END

GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager]') AND name = N'Index_2')
DROP INDEX [Index_2] ON [dbo].[KRT_TemplateManager]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager]') AND name = N'Index_1')
DROP INDEX [Index_1] ON [dbo].[KRT_TemplateManager]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_TemplateManager]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_TemplateManager](
	[Id] [uniqueidentifier] NOT NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[KRT_Modul_Id] [uniqueidentifier] NULL,
	[KRT_Dokumentum_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_TEMPLATEMANAGER] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager]') AND name = N'Index_1')
CREATE NONCLUSTERED INDEX [Index_1] ON [dbo].[KRT_TemplateManager]
(
	[KRT_Dokumentum_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager]') AND name = N'Index_2')
CREATE NONCLUSTERED INDEX [Index_2] ON [dbo].[KRT_TemplateManager]
(
	[KRT_Modul_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Template__Id__478773E1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TemplateManager] ADD  CONSTRAINT [DF__KRT_Template__Id__478773E1]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Templat__Ver__487B981A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TemplateManager] ADD  CONSTRAINT [DF__KRT_Templat__Ver__487B981A]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Templ__ErvKe__496FBC53]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TemplateManager] ADD  CONSTRAINT [DF__KRT_Templ__ErvKe__496FBC53]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Templ__Letre__4A63E08C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_TemplateManager] ADD  CONSTRAINT [DF__KRT_Templ__Letre__4A63E08C]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager_KRT_Dokumentum_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager]'))
ALTER TABLE [dbo].[KRT_TemplateManager]  WITH CHECK ADD  CONSTRAINT [KRT_TemplateManager_KRT_Dokumentum_FK] FOREIGN KEY([KRT_Dokumentum_Id])
REFERENCES [dbo].[KRT_Dokumentumok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager_KRT_Dokumentum_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager]'))
ALTER TABLE [dbo].[KRT_TemplateManager] CHECK CONSTRAINT [KRT_TemplateManager_KRT_Dokumentum_FK]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager_KRT_Modulok_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager]'))
ALTER TABLE [dbo].[KRT_TemplateManager]  WITH CHECK ADD  CONSTRAINT [KRT_TemplateManager_KRT_Modulok_FK] FOREIGN KEY([KRT_Modul_Id])
REFERENCES [dbo].[KRT_Modulok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager_KRT_Modulok_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_TemplateManager]'))
ALTER TABLE [dbo].[KRT_TemplateManager] CHECK CONSTRAINT [KRT_TemplateManager_KRT_Modulok_FK]
GO
