IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Tranz_Obj_ObjTip_Adatelem_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Tranz_Obj]'))
ALTER TABLE [dbo].[KRT_Tranz_Obj] DROP CONSTRAINT [Tranz_Obj_ObjTip_Adatelem_FK]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tranz__Letre__4F2895A9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tranz_Obj] DROP CONSTRAINT [DF__KRT_Tranz__Letre__4F2895A9]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Tranz_Obj_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tranz_Obj] DROP CONSTRAINT [DF_KRT_Tranz_Obj_ErvVege]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tranz__ErvKe__4D404D37]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tranz_Obj] DROP CONSTRAINT [DF__KRT_Tranz__ErvKe__4D404D37]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tranz_O__Ver__4C4C28FE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tranz_Obj] DROP CONSTRAINT [DF__KRT_Tranz_O__Ver__4C4C28FE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tranz_Ob__Id__4B5804C5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tranz_Obj] DROP CONSTRAINT [DF__KRT_Tranz_Ob__Id__4B5804C5]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tranz_Obj]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Tranz_Obj]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tranz_Obj]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Tranz_Obj](
	[Id] [uniqueidentifier] NOT NULL,
	[ObjTip_Id_AdatElem] [uniqueidentifier] NOT NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL,
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL,
	[ErvVege] [datetime] NULL,
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL,
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_TRANZ_OBJ] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tranz_Ob__Id__4B5804C5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tranz_Obj] ADD  CONSTRAINT [DF__KRT_Tranz_Ob__Id__4B5804C5]  DEFAULT (newsequentialid()) FOR [Id]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tranz_O__Ver__4C4C28FE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tranz_Obj] ADD  CONSTRAINT [DF__KRT_Tranz_O__Ver__4C4C28FE]  DEFAULT ((1)) FOR [Ver]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tranz__ErvKe__4D404D37]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tranz_Obj] ADD  CONSTRAINT [DF__KRT_Tranz__ErvKe__4D404D37]  DEFAULT (getdate()) FOR [ErvKezd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_KRT_Tranz_Obj_ErvVege]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tranz_Obj] ADD  CONSTRAINT [DF_KRT_Tranz_Obj_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))) FOR [ErvVege]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__KRT_Tranz__Letre__4F2895A9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[KRT_Tranz_Obj] ADD  CONSTRAINT [DF__KRT_Tranz__Letre__4F2895A9]  DEFAULT (getdate()) FOR [LetrehozasIdo]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Tranz_Obj_ObjTip_Adatelem_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Tranz_Obj]'))
ALTER TABLE [dbo].[KRT_Tranz_Obj]  WITH CHECK ADD  CONSTRAINT [Tranz_Obj_ObjTip_Adatelem_FK] FOREIGN KEY([ObjTip_Id_AdatElem])
REFERENCES [dbo].[KRT_ObjTipusok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Tranz_Obj_ObjTip_Adatelem_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Tranz_Obj]'))
ALTER TABLE [dbo].[KRT_Tranz_Obj] CHECK CONSTRAINT [Tranz_Obj_ObjTip_Adatelem_FK]
GO
