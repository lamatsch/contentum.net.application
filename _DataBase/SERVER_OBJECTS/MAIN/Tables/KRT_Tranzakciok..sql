IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[TRANZ_ALKALM_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Tranzakciok]'))
ALTER TABLE [dbo].[KRT_Tranzakciok] DROP CONSTRAINT [TRANZ_ALKALM_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tranzakciok]') AND name = N'FK_Alkalmazas_Id')
DROP INDEX [FK_Alkalmazas_Id] ON [dbo].[KRT_Tranzakciok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tranzakciok]') AND name = N'FK_Felhasznalo_Id')
DROP INDEX [FK_Felhasznalo_Id] ON [dbo].[KRT_Tranzakciok] WITH ( ONLINE = OFF )
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tranzakciok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Tranzakciok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tranzakciok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Tranzakciok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Tranzakc__Id__7DB89C09]  DEFAULT (newsequentialid()),
	[Alkalmazas_Id] [uniqueidentifier] NULL,
	[Felhasznalo_Id] [uniqueidentifier] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Tranzak__Ver__7EACC042]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Tranz__ErvKe__7FA0E47B]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Tranzakciok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Tranz__Letre__01892CED]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_TRANZAKCIOK] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tranzakciok]') AND name = N'FK_Felhasznalo_Id')
CREATE UNIQUE CLUSTERED INDEX [FK_Felhasznalo_Id] ON [dbo].[KRT_Tranzakciok]
(
	[Felhasznalo_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tranzakciok]') AND name = N'FK_Alkalmazas_Id')
CREATE NONCLUSTERED INDEX [FK_Alkalmazas_Id] ON [dbo].[KRT_Tranzakciok]
(
	[Alkalmazas_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[TRANZ_ALKALM_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Tranzakciok]'))
ALTER TABLE [dbo].[KRT_Tranzakciok]  WITH CHECK ADD  CONSTRAINT [TRANZ_ALKALM_FK] FOREIGN KEY([Alkalmazas_Id])
REFERENCES [dbo].[KRT_Alkalmazasok] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[TRANZ_ALKALM_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Tranzakciok]'))
ALTER TABLE [dbo].[KRT_Tranzakciok] CHECK CONSTRAINT [TRANZ_ALKALM_FK]
GO
