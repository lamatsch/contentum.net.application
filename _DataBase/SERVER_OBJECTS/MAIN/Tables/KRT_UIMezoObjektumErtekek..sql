IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_UIMezoObjektumErtekek]') AND name = N'KRT_UIMezoObjektumErtekek_TemplatTip_Nev_FelhId_UK')
DROP INDEX [KRT_UIMezoObjektumErtekek_TemplatTip_Nev_FelhId_UK] ON [dbo].[KRT_UIMezoObjektumErtekek]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_UIMezoObjektumErtekek]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_UIMezoObjektumErtekek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_UIMezoObjektumErtekek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_UIMezoObjektumErtekek](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_UIMezoOb__Id__04659998]  DEFAULT (newsequentialid()),
	[TemplateTipusNev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Nev] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[Alapertelmezett] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Felhasznalo_Id] [uniqueidentifier] NOT NULL,
	[TemplateXML] [xml] NULL,
	[UtolsoHasznIdo] [datetime] NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_UIMezoO__Ver__0559BDD1]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_UIMez__ErvKe__064DE20A]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_UIMezoObjektumErtekek_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_UIMez__Letre__08362A7C]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
	[Org_Id] [uniqueidentifier] NULL,
	[Publikus] [char](1) COLLATE Hungarian_CI_AS NULL DEFAULT ((0)),
	[Szervezet_Id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_UIMEZOOBJEKTUMERTEKEK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_UIMezoObjektumErtekek]') AND name = N'KRT_UIMezoObjektumErtekek_TemplatTip_Nev_FelhId_UK')
CREATE NONCLUSTERED INDEX [KRT_UIMezoObjektumErtekek_TemplatTip_Nev_FelhId_UK] ON [dbo].[KRT_UIMezoObjektumErtekek]
(
	[TemplateTipusNev] ASC,
	[Felhasznalo_Id] ASC,
	[Nev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
