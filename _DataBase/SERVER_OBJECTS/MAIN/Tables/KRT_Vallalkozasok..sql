IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Vallalkozas_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Vallalkozasok]'))
ALTER TABLE [dbo].[KRT_Vallalkozasok] DROP CONSTRAINT [Vallalkozas_Partner_FK]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Vallalkozasok]') AND name = N'FK_Partner_Id')
DROP INDEX [FK_Partner_Id] ON [dbo].[KRT_Vallalkozasok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Vallalkozasok]') AND type in (N'U'))
DROP TABLE [dbo].[KRT_Vallalkozasok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Vallalkozasok]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KRT_Vallalkozasok](
	[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__KRT_Vallalko__Id__0B129727]  DEFAULT (newsequentialid()),
	[Partner_Id] [uniqueidentifier] NOT NULL,
	[Adoszam] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[KulfoldiAdoszamJelolo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[TB_Torzsszam] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[Cegjegyzekszam] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Tipus] [nvarchar](64) COLLATE Hungarian_CS_AS NULL,
	[Ver] [int] NULL CONSTRAINT [DF__KRT_Vallalk__Ver__0C06BB60]  DEFAULT ((1)),
	[Note] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[Stat_id] [uniqueidentifier] NULL,
	[ErvKezd] [datetime] NULL CONSTRAINT [DF__KRT_Valla__ErvKe__0CFADF99]  DEFAULT (getdate()),
	[ErvVege] [datetime] NULL CONSTRAINT [DF_KRT_Vallalkozasok_ErvVege]  DEFAULT (CONVERT([datetime],'4700-12-31',(102))),
	[Letrehozo_id] [uniqueidentifier] NULL,
	[LetrehozasIdo] [datetime] NOT NULL CONSTRAINT [DF__KRT_Valla__Letre__0EE3280B]  DEFAULT (getdate()),
	[Modosito_id] [uniqueidentifier] NULL,
	[ModositasIdo] [datetime] NULL,
	[Zarolo_id] [uniqueidentifier] NULL,
	[ZarolasIdo] [datetime] NULL,
	[Tranz_id] [uniqueidentifier] NULL,
	[UIAccessLog_id] [uniqueidentifier] NULL,
 CONSTRAINT [PK_KRT_VALLALKOZASOK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Vallalkozasok]') AND name = N'FK_Partner_Id')
CREATE NONCLUSTERED INDEX [FK_Partner_Id] ON [dbo].[KRT_Vallalkozasok]
(
	[Partner_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 85) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Vallalkozas_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Vallalkozasok]'))
ALTER TABLE [dbo].[KRT_Vallalkozasok]  WITH CHECK ADD  CONSTRAINT [Vallalkozas_Partner_FK] FOREIGN KEY([Partner_Id])
REFERENCES [dbo].[KRT_Partnerek] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[Vallalkozas_Partner_FK]') AND parent_object_id = OBJECT_ID(N'[dbo].[KRT_Vallalkozasok]'))
ALTER TABLE [dbo].[KRT_Vallalkozasok] CHECK CONSTRAINT [Vallalkozas_Partner_FK]
GO
