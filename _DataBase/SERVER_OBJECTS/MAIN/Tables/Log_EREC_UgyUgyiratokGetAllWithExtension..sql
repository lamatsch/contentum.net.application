IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Log_EREC___Statu__11BF94B6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Log_EREC_UgyUgyiratokGetAllWithExtension] DROP CONSTRAINT [DF__Log_EREC___Statu__11BF94B6]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Log_EREC_U__Date__10CB707D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Log_EREC_UgyUgyiratokGetAllWithExtension] DROP CONSTRAINT [DF__Log_EREC_U__Date__10CB707D]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Log_EREC_UgyUgyiratokGetAllWithExtension]') AND type in (N'U'))
DROP TABLE [dbo].[Log_EREC_UgyUgyiratokGetAllWithExtension]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Log_EREC_UgyUgyiratokGetAllWithExtension]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Log_EREC_UgyUgyiratokGetAllWithExtension](
	[Date] [datetime] NOT NULL,
	[Status] [char](1) COLLATE Hungarian_CI_AS NOT NULL,
	[StartDate] [datetime] NULL,
	[WHERE] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[Where_KuldKuldemenyek] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[Where_UgyUgyiratdarabok] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[Where_IraIratok] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[Where_IraIktatokonyvek] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[Where_Dosszie] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[ObjektumTargyszavai_ObjIdFilter] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[Altalanos_FTS] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[OrderBy] [nvarchar](200) COLLATE Hungarian_CI_AS NULL,
	[TopRow] [nvarchar](5) COLLATE Hungarian_CI_AS NULL,
	[ExecutorUserId] [uniqueidentifier] NULL,
	[FelhasznaloSzervezet_Id] [uniqueidentifier] NULL,
	[Jogosultak] [char](1) COLLATE Hungarian_CI_AS NULL,
	[ForMunkanaplo] [char](1) COLLATE Hungarian_CI_AS NULL,
	[pageNumber] [int] NULL,
	[pageSize] [int] NULL,
	[SelectedRowId] [uniqueidentifier] NULL,
	[AlSzervezetId] [uniqueidentifier] NULL,
	[MESSAGE] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[HibaUzenet] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Log_EREC_U__Date__10CB707D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Log_EREC_UgyUgyiratokGetAllWithExtension] ADD  CONSTRAINT [DF__Log_EREC_U__Date__10CB707D]  DEFAULT (getdate()) FOR [Date]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Log_EREC___Statu__11BF94B6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Log_EREC_UgyUgyiratokGetAllWithExtension] ADD  CONSTRAINT [DF__Log_EREC___Statu__11BF94B6]  DEFAULT ('0') FOR [Status]
END

GO
