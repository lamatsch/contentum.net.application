IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MIGR_partnerek]') AND type in (N'U'))
DROP TABLE [dbo].[MIGR_partnerek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MIGR_partnerek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MIGR_partnerek](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nev] [varchar](255) COLLATE Hungarian_CI_AS NULL,
	[titul] [varchar](10) COLLATE Hungarian_CI_AS NULL,
	[csaladinev] [varchar](30) COLLATE Hungarian_CI_AS NULL,
	[utonev1] [varchar](30) COLLATE Hungarian_CI_AS NULL,
	[utonev2] [varchar](30) COLLATE Hungarian_CI_AS NULL,
	[anyanev] [varchar](50) COLLATE Hungarian_CI_AS NULL,
	[szulnev] [varchar](50) COLLATE Hungarian_CI_AS NULL,
	[szulido] [datetime] NULL,
	[szulhely] [varchar](30) COLLATE Hungarian_CI_AS NULL,
	[szemszam] [varchar](11) COLLATE Hungarian_CI_AS NULL,
	[tajszam] [varchar](9) COLLATE Hungarian_CI_AS NULL,
	[szigszam] [varchar](8) COLLATE Hungarian_CI_AS NULL,
	[adoazjel] [varchar](10) COLLATE Hungarian_CI_AS NULL,
	[nem] [varchar](5) COLLATE Hungarian_CI_AS NULL,
	[adoszam] [varchar](11) COLLATE Hungarian_CI_AS NULL,
	[tbtorzsszam] [varchar](15) COLLATE Hungarian_CI_AS NULL,
	[cegjegyzekszam] [varchar](20) COLLATE Hungarian_CI_AS NULL,
	[cegtipuskod] [varchar](2) COLLATE Hungarian_CI_AS NULL,
	[orszag] [varchar](20) COLLATE Hungarian_CI_AS NULL,
	[irszam] [varchar](4) COLLATE Hungarian_CI_AS NULL,
	[telepules] [varchar](30) COLLATE Hungarian_CI_AS NULL,
	[helyrajziszam] [varchar](10) COLLATE Hungarian_CI_AS NULL,
	[kozternev] [varchar](30) COLLATE Hungarian_CI_AS NULL,
	[koztertipuskod] [varchar](3) COLLATE Hungarian_CI_AS NULL,
	[hazszam] [varchar](20) COLLATE Hungarian_CI_AS NULL,
	[epulet] [varchar](10) COLLATE Hungarian_CI_AS NULL,
	[lepcsohaz] [varchar](10) COLLATE Hungarian_CI_AS NULL,
	[emelet] [varchar](10) COLLATE Hungarian_CI_AS NULL,
	[ajto] [varchar](10) COLLATE Hungarian_CI_AS NULL,
	[adatforras] [varchar](10) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK__MIG_part__3213E83F023E255B] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
