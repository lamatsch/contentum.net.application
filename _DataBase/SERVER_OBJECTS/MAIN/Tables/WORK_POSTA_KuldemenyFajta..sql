IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WORK_POSTA_KuldemenyFajta]') AND type in (N'U'))
DROP TABLE [dbo].[WORK_POSTA_KuldemenyFajta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WORK_POSTA_KuldemenyFajta]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WORK_POSTA_KuldemenyFajta](
	[CreatedByName] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[CreatedByYomiName] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[CreatedOnBehalfByName] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[CreatedOnBehalfByYomiName] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ModifiedByName] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ModifiedByYomiName] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ModifiedOnBehalfByName] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ModifiedOnBehalfByYomiName] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[TransactionCurrencyIdName] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[OwnerId] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[OwnerIdName] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[OwnerIdYomiName] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[OwnerIdDsc] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[OwnerIdType] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[OwningUser] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[OwningTeam] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_kuldemenyfajtaId] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[CreatedOn] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[CreatedBy] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ModifiedOn] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ModifiedBy] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[CreatedOnBehalfBy] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ModifiedOnBehalfBy] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[OwningBusinessUnit] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[statecode] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[statuscode] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[VersionNumber] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ImportSequenceNumber] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[OverriddenCreatedOn] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[TimeZoneRuleVersionNumber] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[UTCConversionTimeZoneCode] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_name] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_Ar] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[TransactionCurrencyId] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ExchangeRate] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_ar_Base] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_Egyeb] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_Postairovidites] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_Sorrend] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL
) ON [PRIMARY]
END
GO
