IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WORK_POSTA_TertivevenyVisszaerkeztetes]') AND type in (N'U'))
DROP TABLE [dbo].[WORK_POSTA_TertivevenyVisszaerkeztetes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WORK_POSTA_TertivevenyVisszaerkeztetes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WORK_POSTA_TertivevenyVisszaerkeztetes](
	[createdby] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[createdbyname] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[createdbyyominame] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[createdon] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[createdonutc] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[createdonbehalfby] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[createdonbehalfbyname] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[createdonbehalfbyyominame] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[importsequencenumber] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedby] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedbyname] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedbyyominame] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedon] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedonutc] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedonbehalfby] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedonbehalfbyname] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedonbehalfbyyominame] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[overriddencreatedon] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[overriddencreatedonutc] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ownerid] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[owneriddsc] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[owneridname] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[owneridtype] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[owneridyominame] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[owningbusinessunit] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[owningteam] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[owninguser] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_atveteldatuma] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_atveteldatumautc] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_atveteljogcime] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_atvevoszemely] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_ismertcimzettkuldemeny] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_ismertcimzettkuldemenyname] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_kezbesiteseredmenye] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_kezbesiteseredmenyename] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_tertivevenyvisszaerkeztetesid] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_tertivevenyvonalkodja] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[statecode] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[statecodename] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[statuscode] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[statuscodename] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[timezoneruleversionnumber] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[utcconversiontimezonecode] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[versionnumber] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL
) ON [PRIMARY]
END
GO
