IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WORK_POSTA_TomegesKuldemeny]') AND type in (N'U'))
DROP TABLE [dbo].[WORK_POSTA_TomegesKuldemeny]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WORK_POSTA_TomegesKuldemeny]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WORK_POSTA_TomegesKuldemeny](
	[createdby] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[createdbyname] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[createdbyyominame] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[createdon] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[createdonutc] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[createdonbehalfby] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[createdonbehalfbyname] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[createdonbehalfbyyominame] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[exchangerate] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[importsequencenumber] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedby] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedbyname] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedbyyominame] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedon] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedonutc] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedonbehalfby] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedonbehalfbyname] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[modifiedonbehalfbyyominame] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[overriddencreatedon] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[overriddencreatedonutc] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[ownerid] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[owneriddsc] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[owneridname] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[owneridtype] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[owneridyominame] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[owningbusinessunit] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[owningteam] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[owninguser] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_darab] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_kuldemenyar] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_kuldemenyar_base] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_kuldemenyfajta] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_kuldemenyfajtaname] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_megjegyzes] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_name] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_tomegeskuldemeny] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_tomegeskuldemenyid] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_tomegeskuldemenyname] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_viszonylat] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[posta_viszonylatname] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[statecode] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[statecodename] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[statuscode] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[statuscodename] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[timezoneruleversionnumber] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[transactioncurrencyid] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[transactioncurrencyidname] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[utcconversiontimezonecode] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[versionnumber] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[crm_moneyformatstring] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[crm_priceformatstring] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL
) ON [PRIMARY]
END
GO
