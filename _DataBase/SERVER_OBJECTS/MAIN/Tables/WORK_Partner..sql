IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WORK_Partner]') AND type in (N'U'))
DROP TABLE [dbo].[WORK_Partner]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WORK_Partner]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WORK_Partner](
	[Nev] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[Iranyitoszam] [int] NULL,
	[Telepules] [nvarchar](50) COLLATE Hungarian_CI_AS NULL,
	[Cim] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Forras] [nvarchar](25) COLLATE Hungarian_CI_AS NULL
) ON [PRIMARY]
END
GO
