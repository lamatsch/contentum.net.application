IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FeladatFelelos]') AND name = N'ix_szervezet_szervezet_id')
DROP INDEX [ix_szervezet_szervezet_id] ON [eAdatpiac].[Dim_FeladatFelelos]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FeladatFelelos]') AND name = N'ix_szervezet_nev')
DROP INDEX [ix_szervezet_nev] ON [eAdatpiac].[Dim_FeladatFelelos]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FeladatFelelos]') AND name = N'ix_szervezet_felhnev')
DROP INDEX [ix_szervezet_felhnev] ON [eAdatpiac].[Dim_FeladatFelelos]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FeladatFelelos]') AND name = N'ix_szervezet_felhaszn_id')
DROP INDEX [ix_szervezet_felhaszn_id] ON [eAdatpiac].[Dim_FeladatFelelos]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FeladatFelelos]') AND type in (N'U'))
DROP TABLE [eAdatpiac].[Dim_FeladatFelelos]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FeladatFelelos]') AND type in (N'U'))
BEGIN
CREATE TABLE [eAdatpiac].[Dim_FeladatFelelos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FelelosTipusKod] [char](1) COLLATE Hungarian_CI_AS NULL,
	[FelelosTipusNev] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Szervezet_Id] [uniqueidentifier] NULL,
	[SzervezetNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Felhasznalo_Id] [uniqueidentifier] NULL,
	[FelhasznaloNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[FrissitesIdo] [datetime] NULL,
	[Modositas] [datetime] NULL,
	[ETL_Load_Id] [int] NULL,
 CONSTRAINT [PK_DIM_FELADATFELELOS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FeladatFelelos]') AND name = N'ix_szervezet_felhaszn_id')
CREATE NONCLUSTERED INDEX [ix_szervezet_felhaszn_id] ON [eAdatpiac].[Dim_FeladatFelelos]
(
	[Felhasznalo_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FeladatFelelos]') AND name = N'ix_szervezet_felhnev')
CREATE NONCLUSTERED INDEX [ix_szervezet_felhnev] ON [eAdatpiac].[Dim_FeladatFelelos]
(
	[FelhasznaloNev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FeladatFelelos]') AND name = N'ix_szervezet_nev')
CREATE NONCLUSTERED INDEX [ix_szervezet_nev] ON [eAdatpiac].[Dim_FeladatFelelos]
(
	[SzervezetNev] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FeladatFelelos]') AND name = N'ix_szervezet_szervezet_id')
CREATE NONCLUSTERED INDEX [ix_szervezet_szervezet_id] ON [eAdatpiac].[Dim_FeladatFelelos]
(
	[Szervezet_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
