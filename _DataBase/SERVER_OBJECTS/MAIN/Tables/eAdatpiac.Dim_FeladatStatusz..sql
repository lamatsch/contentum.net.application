IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FeladatStatusz]') AND type in (N'U'))
DROP TABLE [eAdatpiac].[Dim_FeladatStatusz]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FeladatStatusz]') AND type in (N'U'))
BEGIN
CREATE TABLE [eAdatpiac].[Dim_FeladatStatusz](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FeladatJel] [char](1) COLLATE Hungarian_CI_AS NULL,
	[FeladatJelNev] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[KeletkezesKod] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KeletkezesNev] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[EredetKod] [char](1) COLLATE Hungarian_CI_AS NULL,
	[EredetNev] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
	[KezelesSzintKod] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KezelesSzintNev] [nvarchar](20) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_DIM_FELADATSTATUSZ] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
