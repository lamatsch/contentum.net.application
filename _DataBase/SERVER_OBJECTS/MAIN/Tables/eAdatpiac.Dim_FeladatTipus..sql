IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FeladatTipus]') AND type in (N'U'))
DROP TABLE [eAdatpiac].[Dim_FeladatTipus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FeladatTipus]') AND type in (N'U'))
BEGIN
CREATE TABLE [eAdatpiac].[Dim_FeladatTipus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FeladatKod] [char](4) COLLATE Hungarian_CI_AS NULL,
	[Kategoria] [nvarchar](100) COLLATE Hungarian_CI_AS NOT NULL,
	[KategoriaKibontas] [int] NULL,
	[Altipus1] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Altipus1Kibontas] [int] NULL,
	[Altipus2] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Altipus2Kibontas] [int] NULL,
	[Altipus3] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[Altipus3Kibontas] [int] NULL,
	[Obj_Type] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
	[AlapPrioritas] [int] NULL,
	[Funkcio_Id] [uniqueidentifier] NULL,
	[SzervezetJel] [int] NULL,
	[URLJel] [int] NULL,
	[URLGlobDefinicio] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
	[URLTetDefinicio] [nvarchar](4000) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_DIM_FELADATTIPUS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
