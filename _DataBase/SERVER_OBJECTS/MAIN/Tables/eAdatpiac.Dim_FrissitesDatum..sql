IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FrissitesDatum]') AND name = N'ix_ido_evhonap')
DROP INDEX [ix_ido_evhonap] ON [eAdatpiac].[Dim_FrissitesDatum]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FrissitesDatum]') AND type in (N'U'))
DROP TABLE [eAdatpiac].[Dim_FrissitesDatum]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FrissitesDatum]') AND type in (N'U'))
BEGIN
CREATE TABLE [eAdatpiac].[Dim_FrissitesDatum](
	[Datum_Id] [int] IDENTITY(1,1) NOT NULL,
	[Datum] [datetime] NULL,
	[Ev] [int] NULL,
	[Ho] [int] NULL,
	[Het] [int] NULL,
	[Nap] [int] NULL,
	[Negyedev] [int] NULL,
	[Felev] [int] NULL,
	[Evnap] [int] NULL,
	[Hetnap] [int] NULL,
	[NegyedRovid] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[NegyedHosszu] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[HoRovid] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[HoHosszu] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[NapHosszu] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[NapRovid] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_DIM_FRISSITESDATUM] PRIMARY KEY CLUSTERED 
(
	[Datum_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_FrissitesDatum]') AND name = N'ix_ido_evhonap')
CREATE NONCLUSTERED INDEX [ix_ido_evhonap] ON [eAdatpiac].[Dim_FrissitesDatum]
(
	[Ev] ASC,
	[Ho] ASC,
	[Nap] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
