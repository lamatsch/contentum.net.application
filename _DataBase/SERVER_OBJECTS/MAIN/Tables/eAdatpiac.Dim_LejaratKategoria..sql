IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_LejaratKategoria]') AND type in (N'U'))
DROP TABLE [eAdatpiac].[Dim_LejaratKategoria]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_LejaratKategoria]') AND type in (N'U'))
BEGIN
CREATE TABLE [eAdatpiac].[Dim_LejaratKategoria](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Sorrend] [int] NULL,
	[TetelJel] [int] NULL,
	[LejaratKod] [char](1) COLLATE Hungarian_CI_AS NULL,
	[LejaratNev] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[ElteresErtek] [int] NULL,
	[ElteresNev] [nvarchar](100) COLLATE Hungarian_CI_AS NULL,
 CONSTRAINT [PK_DIM_LEJARATKATEGORIA] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
