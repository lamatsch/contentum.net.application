IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_Prioritas]') AND type in (N'U'))
DROP TABLE [eAdatpiac].[Dim_Prioritas]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Dim_Prioritas]') AND type in (N'U'))
BEGIN
CREATE TABLE [eAdatpiac].[Dim_Prioritas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PrioritasSzint] [int] NULL,
	[PrioritasSzintNev] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[Prioritas] [int] NULL,
	[PrioritasNev] [nvarchar](10) COLLATE Hungarian_CI_AS NULL,
	[ETL_Load_Id] [int] NULL,
 CONSTRAINT [PK_DIM_PRIORITAS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
