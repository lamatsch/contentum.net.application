IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Fact_Feladatok]') AND name = N'ix_fact_statusz')
DROP INDEX [ix_fact_statusz] ON [eAdatpiac].[Fact_Feladatok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Fact_Feladatok]') AND name = N'ix_fact_objid')
DROP INDEX [ix_fact_objid] ON [eAdatpiac].[Fact_Feladatok]
GO
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Fact_Feladatok]') AND name = N'ix_fact_felelos')
DROP INDEX [ix_fact_felelos] ON [eAdatpiac].[Fact_Feladatok]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Fact_Feladatok]') AND type in (N'U'))
DROP TABLE [eAdatpiac].[Fact_Feladatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Fact_Feladatok]') AND type in (N'U'))
BEGIN
CREATE TABLE [eAdatpiac].[Fact_Feladatok](
	[FeladatFelelos_Id] [int] NULL,
	[FrissitesDatum_Id] [int] NULL,
	[FeladatTipus_Id] [int] NULL,
	[FeladatStatusz_Id] [int] NULL,
	[LejaratKategoria_Id] [int] NULL,
	[Prioritas_Id] [int] NULL,
	[Telelszam] [int] NULL,
	[Obj_Id] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[ETL_Load_Id] [int] NULL,
	[LetrehozasIdo] [datetime] NULL
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Fact_Feladatok]') AND name = N'ix_fact_felelos')
CREATE NONCLUSTERED INDEX [ix_fact_felelos] ON [eAdatpiac].[Fact_Feladatok]
(
	[FrissitesDatum_Id] ASC,
	[FeladatFelelos_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Fact_Feladatok]') AND name = N'ix_fact_objid')
CREATE NONCLUSTERED INDEX [ix_fact_objid] ON [eAdatpiac].[Fact_Feladatok]
(
	[Obj_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[Fact_Feladatok]') AND name = N'ix_fact_statusz')
CREATE NONCLUSTERED INDEX [ix_fact_statusz] ON [eAdatpiac].[Fact_Feladatok]
(
	[FeladatStatusz_Id] ASC,
	[LejaratKategoria_Id] ASC,
	[FeladatTipus_Id] ASC,
	[Prioritas_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
