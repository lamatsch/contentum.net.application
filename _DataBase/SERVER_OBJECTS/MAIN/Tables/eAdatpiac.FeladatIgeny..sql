IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[FeladatIgeny]') AND name = N'IX_OsszesitesIgeny')
DROP INDEX [IX_OsszesitesIgeny] ON [eAdatpiac].[FeladatIgeny]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[FeladatIgeny]') AND type in (N'U'))
DROP TABLE [eAdatpiac].[FeladatIgeny]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[FeladatIgeny]') AND type in (N'U'))
BEGIN
CREATE TABLE [eAdatpiac].[FeladatIgeny](
	[Keres_id] [int] IDENTITY(1,1) NOT NULL,
	[Indito_Id] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Szervezet_id] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Felhasznalo_id] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[KeresDatuma] [datetime] NULL,
	[KeresEvHoNap] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[KeresIndito] [nvarchar](64) COLLATE Hungarian_CI_AS NULL,
	[Allapot] [int] NULL,
	[KezdesIdo] [datetime] NULL,
	[VegeIdo] [datetime] NULL,
	[Osszesre] [bit] NULL,
 CONSTRAINT [PK_FELADATIGENY] PRIMARY KEY CLUSTERED 
(
	[Keres_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON

GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[FeladatIgeny]') AND name = N'IX_OsszesitesIgeny')
CREATE NONCLUSTERED INDEX [IX_OsszesitesIgeny] ON [eAdatpiac].[FeladatIgeny]
(
	[Allapot] ASC,
	[Szervezet_id] ASC,
	[Felhasznalo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
