IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[FeladatLoad]') AND name = N'IX_SzervezetFelhasznalo')
DROP INDEX [IX_SzervezetFelhasznalo] ON [eAdatpiac].[FeladatLoad]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[FeladatLoad]') AND type in (N'U'))
DROP TABLE [eAdatpiac].[FeladatLoad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[eAdatpiac].[FeladatLoad]') AND type in (N'U'))
BEGIN
CREATE TABLE [eAdatpiac].[FeladatLoad](
	[FrissitesIdo] [datetime] NULL,
	[FeladatKod] [varchar](4) COLLATE Hungarian_CI_AS NULL,
	[FelelosSzervezet_Id] [uniqueidentifier] NULL,
	[FelelosFelhasznalo_Id] [uniqueidentifier] NULL,
	[Prioritas] [smallint] NULL,
	[LejaratDatum] [datetime] NULL,
	[FeladatJel] [char](1) COLLATE Hungarian_CI_AS NULL,
	[EredetKod] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KeletkezesKod] [char](1) COLLATE Hungarian_CI_AS NULL,
	[KezelesSzintKod] [char](1) COLLATE Hungarian_CI_AS NULL,
	[Funkcio_Id_Inditando] [uniqueidentifier] NULL,
	[Obj_Id] [uniqueidentifier] NULL,
	[Obj_Type] [varchar](100) COLLATE Hungarian_CI_AS NULL,
	[Kiado_Id] [uniqueidentifier] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[eAdatpiac].[FeladatLoad]') AND name = N'IX_SzervezetFelhasznalo')
CREATE NONCLUSTERED INDEX [IX_SzervezetFelhasznalo] ON [eAdatpiac].[FeladatLoad]
(
	[FelelosSzervezet_Id] ASC,
	[FelelosFelhasznalo_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
