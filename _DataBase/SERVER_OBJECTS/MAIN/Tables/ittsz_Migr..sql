IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ittsz_Migr]') AND type in (N'U'))
DROP TABLE [dbo].[ittsz_Migr]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ittsz_Migr]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ittsz_Migr](
	[Ugy_id] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[Old_itsz                           ] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[Old_Itsz_ID] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[New_itsz] [nvarchar](255) COLLATE Hungarian_CI_AS NULL,
	[New_itsz_ID] [nvarchar](255) COLLATE Hungarian_CI_AS NULL
) ON [PRIMARY]
END
GO
