IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[krt_partnertorzs_temp]') AND type in (N'U'))
DROP TABLE [dbo].[krt_partnertorzs_temp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[krt_partnertorzs_temp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[krt_partnertorzs_temp](
	[Partner_nev] [nchar](200) COLLATE Hungarian_CI_AS NULL,
	[Partner_belso_azonositoja] [nchar](40) COLLATE Hungarian_CI_AS NULL
) ON [PRIMARY]
END
GO
