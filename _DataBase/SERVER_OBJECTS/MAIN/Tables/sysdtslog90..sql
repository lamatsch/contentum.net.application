IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sysdtslog90]') AND type in (N'U'))
DROP TABLE [dbo].[sysdtslog90]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sysdtslog90]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sysdtslog90](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[event] [sysname] COLLATE Hungarian_CI_AS NOT NULL,
	[computer] [nvarchar](128) COLLATE Hungarian_CI_AS NOT NULL,
	[operator] [nvarchar](128) COLLATE Hungarian_CI_AS NOT NULL,
	[source] [nvarchar](1024) COLLATE Hungarian_CI_AS NOT NULL,
	[sourceid] [uniqueidentifier] NOT NULL,
	[executionid] [uniqueidentifier] NOT NULL,
	[starttime] [datetime] NOT NULL,
	[endtime] [datetime] NOT NULL,
	[datacode] [int] NOT NULL,
	[databytes] [image] NULL,
	[message] [nvarchar](2048) COLLATE Hungarian_CI_AS NOT NULL,
 CONSTRAINT [PK__sysdtslo__3213E83F1590259A] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
