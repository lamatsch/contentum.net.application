IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_KozterInterface]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_KozterInterface]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_KozterInterface]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_KozterInterface](
	[ktdok_id] [uniqueidentifier] NOT NULL,
	[ktdok_kod] [nvarchar](9) COLLATE Hungarian_CI_AS NULL,
	[ktdok_iktszam] [nvarchar](14) COLLATE Hungarian_CI_AS NULL,
	[ktdok_vonalkod] [nvarchar](13) COLLATE Hungarian_CI_AS NULL,
	[ktdok_dokid] [uniqueidentifier] NULL,
	[ktdok_url] [nvarchar](max) COLLATE Hungarian_CI_AS NULL,
	[ktdok_date] [datetime] NULL,
	[ktdok_siker] [char](1) COLLATE Hungarian_CI_AS NULL,
	[ktdok_ok] [nvarchar](256) COLLATE Hungarian_CI_AS NULL,
	[ktdok_atvet] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
