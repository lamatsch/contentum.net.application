IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_UgyiratMegorzIdoMod]') AND type in (N'U'))
DROP TABLE [dbo].[tmp_UgyiratMegorzIdoMod]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_UgyiratMegorzIdoMod]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tmp_UgyiratMegorzIdoMod](
	[id] [uniqueidentifier] NOT NULL,
	[megorzIdoVege_uj] [datetime] NOT NULL,
	[megorzIdo_ev] [int] NULL,
	[lezarasDat] [datetime] NULL,
	[megorzIdoVege_eredeti] [datetime] NOT NULL,
	[ModositoId_eredeti] [uniqueidentifier] NULL,
	[ModositasIdo_eredeti] [datetime] NULL,
	[Ver_eredeti] [int] NULL
) ON [PRIMARY]
END
GO
