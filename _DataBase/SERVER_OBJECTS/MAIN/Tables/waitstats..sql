IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__waitstats__now__3717A178]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[waitstats] DROP CONSTRAINT [DF__waitstats__now__3717A178]
END

GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[waitstats]') AND type in (N'U'))
DROP TABLE [dbo].[waitstats]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[waitstats]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[waitstats](
	[wait_type] [nvarchar](60) COLLATE Hungarian_CI_AS NOT NULL,
	[waiting_tasks_count] [bigint] NOT NULL,
	[wait_time_ms] [bigint] NOT NULL,
	[max_wait_time_ms] [bigint] NOT NULL,
	[signal_wait_time_ms] [bigint] NOT NULL,
	[now] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__waitstats__now__3717A178]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[waitstats] ADD  CONSTRAINT [DF__waitstats__now__3717A178]  DEFAULT (getdate()) FOR [now]
END

GO
