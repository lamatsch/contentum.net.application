IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_AgazatiJelekHistory]'))
DROP VIEW [dbo].[EREC_AgazatiJelekHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_AgazatiJelekHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[EREC_AgazatiJelekHistory] as select * from CONTENTUM_NMHH_History.dbo.EREC_AgazatiJelekHistory;

' 
GO
