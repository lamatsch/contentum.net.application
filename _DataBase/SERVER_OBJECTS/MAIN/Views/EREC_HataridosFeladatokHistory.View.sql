IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatokHistory]'))
DROP VIEW [dbo].[EREC_HataridosFeladatokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_HataridosFeladatokHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[EREC_HataridosFeladatokHistory] as select * from CONTENTUM_NMHH_History.dbo.EREC_HataridosFeladatokHistory;

' 
GO
