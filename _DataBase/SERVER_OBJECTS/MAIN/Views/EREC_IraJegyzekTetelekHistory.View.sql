IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekTetelekHistory]'))
DROP VIEW [dbo].[EREC_IraJegyzekTetelekHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IraJegyzekTetelekHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[EREC_IraJegyzekTetelekHistory] as select * from CONTENTUM_NMHH_History.dbo.EREC_IraJegyzekTetelekHistory;

' 
GO
