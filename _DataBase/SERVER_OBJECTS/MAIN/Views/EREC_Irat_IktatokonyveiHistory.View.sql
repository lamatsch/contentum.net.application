IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Irat_IktatokonyveiHistory]'))
DROP VIEW [dbo].[EREC_Irat_IktatokonyveiHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Irat_IktatokonyveiHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[EREC_Irat_IktatokonyveiHistory] as select * from CONTENTUM_NMHH_History.dbo.EREC_Irat_IktatokonyveiHistory;

' 
GO
