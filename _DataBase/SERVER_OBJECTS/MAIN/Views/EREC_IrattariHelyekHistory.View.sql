IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariHelyekHistory]'))
DROP VIEW [dbo].[EREC_IrattariHelyekHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariHelyekHistory]'))
EXEC dbo.sp_executesql @statement = N'
create view [dbo].[EREC_IrattariHelyekHistory] as select * from contentum_nmhh_history.dbo.EREC_IrattariHelyekHistory;


' 
GO
