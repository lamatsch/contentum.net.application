IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_IktatokonyvHistory]'))
DROP VIEW [dbo].[EREC_IrattariTetel_IktatokonyvHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_IrattariTetel_IktatokonyvHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[EREC_IrattariTetel_IktatokonyvHistory] as select * from CONTENTUM_NMHH_History.dbo.EREC_IrattariTetel_IktatokonyvHistory;

' 
GO
