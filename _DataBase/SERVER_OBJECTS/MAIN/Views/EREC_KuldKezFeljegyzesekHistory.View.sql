IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKezFeljegyzesekHistory]'))
DROP VIEW [dbo].[EREC_KuldKezFeljegyzesekHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_KuldKezFeljegyzesekHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[EREC_KuldKezFeljegyzesekHistory] as select * from CONTENTUM_NMHH_History.dbo.EREC_KuldKezFeljegyzesekHistory;

' 
GO
