IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_MellekletekHistory]'))
DROP VIEW [dbo].[EREC_MellekletekHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_MellekletekHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[EREC_MellekletekHistory] as select * from CONTENTUM_NMHH_History.dbo.EREC_MellekletekHistory;

' 
GO
