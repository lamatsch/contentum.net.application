IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicioHistory]'))
DROP VIEW [dbo].[EREC_Obj_MetaDefinicioHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_Obj_MetaDefinicioHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[EREC_Obj_MetaDefinicioHistory] as select * from CONTENTUM_NMHH_History.dbo.EREC_Obj_MetaDefinicioHistory;

' 
GO
