/****** Object:  View [dbo].[EREC_eBeadvanyCsatolmanyokHistory]    Script Date: 2017. 10. 19. 16:27:00 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eBeadvanyCsatolmanyokHistory]'))
	DROP VIEW [dbo].[EREC_eBeadvanyCsatolmanyokHistory]
GO

/****** Object:  View [dbo].[EREC_eBeadvanyCsatolmanyokHistory]    Script Date: 2017. 10. 19. 16:27:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[EREC_eBeadvanyCsatolmanyokHistory] AS SELECT * FROM $(history).dbo.EREC_eBeadvanyCsatolmanyokHistory;

GO


