/****** Object:  View [dbo].[EREC_eBeadvanyokHistory]    Script Date: 2017. 10. 19. 16:27:00 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eBeadvanyokHistory]'))
	DROP VIEW [dbo].[EREC_eBeadvanyokHistory]
GO

/****** Object:  View [dbo].[EREC_eBeadvanyokHistory]    Script Date: 2017. 10. 19. 16:27:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[EREC_eBeadvanyokHistory] AS SELECT * FROM $(history).dbo.EREC_eBeadvanyokHistory;

GO


