IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCsatolmanyokHistory]'))
DROP VIEW [dbo].[EREC_eMailBoritekCsatolmanyokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekCsatolmanyokHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[EREC_eMailBoritekCsatolmanyokHistory] as select * from CONTENTUM_NMHH_History.dbo.EREC_eMailBoritekCsatolmanyokHistory;

' 
GO
