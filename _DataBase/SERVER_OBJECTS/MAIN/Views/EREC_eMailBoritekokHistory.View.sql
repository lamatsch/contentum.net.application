IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekokHistory]'))
DROP VIEW [dbo].[EREC_eMailBoritekokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[EREC_eMailBoritekokHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[EREC_eMailBoritekokHistory] as select * from CONTENTUM_NMHH_History.dbo.EREC_eMailBoritekokHistory;

' 
GO
