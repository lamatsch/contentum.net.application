IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodSavokHistory]'))
DROP VIEW [dbo].[KRT_BarkodSavokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodSavokHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[KRT_BarkodSavokHistory] as select * from CONTENTUM_NMHH_History.dbo.KRT_BarkodSavokHistory;

' 
GO
