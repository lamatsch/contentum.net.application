IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodokHistory]'))
DROP VIEW [dbo].[KRT_BarkodokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_BarkodokHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[KRT_BarkodokHistory] as select * from CONTENTUM_NMHH_History.dbo.KRT_BarkodokHistory;

' 
GO
