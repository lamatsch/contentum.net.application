IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumAlairasokHistory]'))
DROP VIEW [dbo].[KRT_DokumentumAlairasokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumAlairasokHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[KRT_DokumentumAlairasokHistory] as select * from CONTENTUM_NMHH_History.dbo.KRT_DokumentumAlairasokHistory;

' 
GO
