IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumokHistory]'))
DROP VIEW [dbo].[KRT_DokumentumokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_DokumentumokHistory]'))
EXEC dbo.sp_executesql @statement = N'
create view [dbo].[KRT_DokumentumokHistory] as select * from CONTENTUM_NMHH_History.dbo.KRT_DokumentumokHistory;


' 
GO
