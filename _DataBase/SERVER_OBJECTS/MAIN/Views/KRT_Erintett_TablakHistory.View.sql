IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Erintett_TablakHistory]'))
DROP VIEW [dbo].[KRT_Erintett_TablakHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Erintett_TablakHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[KRT_Erintett_TablakHistory] as select * from CONTENTUM_NMHH_History.dbo.KRT_Erintett_TablakHistory;

' 
GO
