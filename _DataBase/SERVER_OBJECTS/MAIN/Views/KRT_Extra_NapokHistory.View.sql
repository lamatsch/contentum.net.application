IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Extra_NapokHistory]'))
DROP VIEW [dbo].[KRT_Extra_NapokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Extra_NapokHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[KRT_Extra_NapokHistory] as select * from CONTENTUM_NMHH_History.dbo.KRT_Extra_NapokHistory;

' 
GO
