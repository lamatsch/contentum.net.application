IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory]'))
DROP VIEW [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory] as 

select * from CONTENTUM_NMHH_History.[dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory];

' 
GO
