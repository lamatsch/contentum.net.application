IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ForditasokHistory]'))
DROP VIEW [dbo].[KRT_ForditasokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_ForditasokHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[KRT_ForditasokHistory] as select * from $(history).dbo.KRT_ForditasokHistory;

' 
GO
