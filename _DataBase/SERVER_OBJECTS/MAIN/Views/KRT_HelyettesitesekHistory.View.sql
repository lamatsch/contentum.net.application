IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_HelyettesitesekHistory]'))
DROP VIEW [dbo].[KRT_HelyettesitesekHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_HelyettesitesekHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[KRT_HelyettesitesekHistory] as select * from CONTENTUM_NMHH_History.dbo.KRT_HelyettesitesekHistory;

' 
GO
