IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_OrszagokHistory]'))
DROP VIEW [dbo].[KRT_OrszagokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_OrszagokHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[KRT_OrszagokHistory] as select * from CONTENTUM_NMHH_History.dbo.KRT_OrszagokHistory;

' 
GO
