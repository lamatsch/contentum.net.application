IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatokHistory]'))
DROP VIEW [dbo].[KRT_PartnerKapcsolatokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerKapcsolatokHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[KRT_PartnerKapcsolatokHistory] as select * from CONTENTUM_NMHH_History.dbo.KRT_PartnerKapcsolatokHistory;

' 
GO
