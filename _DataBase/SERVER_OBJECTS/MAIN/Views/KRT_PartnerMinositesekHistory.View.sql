IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesekHistory]'))
DROP VIEW [dbo].[KRT_PartnerMinositesekHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_PartnerMinositesekHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[KRT_PartnerMinositesekHistory] as select * from CONTENTUM_NMHH_History.dbo.KRT_PartnerMinositesekHistory;

' 
GO
