IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tranz_ObjHistory]'))
DROP VIEW [dbo].[KRT_Tranz_ObjHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Tranz_ObjHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[KRT_Tranz_ObjHistory] as select * from CONTENTUM_NMHH_History.dbo.KRT_Tranz_ObjHistory;

' 
GO
