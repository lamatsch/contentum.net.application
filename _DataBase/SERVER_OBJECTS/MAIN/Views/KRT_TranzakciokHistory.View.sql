IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TranzakciokHistory]'))
DROP VIEW [dbo].[KRT_TranzakciokHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_TranzakciokHistory]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[KRT_TranzakciokHistory] as select * from CONTENTUM_NMHH_History.dbo.KRT_TranzakciokHistory;

' 
GO
