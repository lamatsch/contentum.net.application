IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VIEW_POSTA_FeladoJegyzek]'))
DROP VIEW [dbo].[VIEW_POSTA_FeladoJegyzek]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VIEW_POSTA_FeladoJegyzek]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[VIEW_POSTA_FeladoJegyzek] as select
convert(uniqueidentifier,createdby) as createdby,
createdbyname as createdbyname,
createdbyyominame as createdbyyominame,
convert(datetime,createdon) as createdon,
convert(datetime,createdonutc) as createdonutc,
convert(uniqueidentifier,createdonbehalfby) as createdonbehalfby,
createdonbehalfbyname as createdonbehalfbyname,
createdonbehalfbyyominame as createdonbehalfbyyominame,
importsequencenumber as importsequencenumber,
convert(uniqueidentifier,modifiedby) as modifiedby,
modifiedbyname as modifiedbyname,
modifiedbyyominame as modifiedbyyominame,
convert(datetime,modifiedon) as modifiedon,
convert(datetime,modifiedonutc) as modifiedonutc,
convert(uniqueidentifier,modifiedonbehalfby) as modifiedonbehalfby,
modifiedonbehalfbyname as modifiedonbehalfbyname,
modifiedonbehalfbyyominame as modifiedonbehalfbyyominame,
convert(datetime,overriddencreatedon) as overriddencreatedon,
convert(datetime,overriddencreatedonutc) as overriddencreatedonutc,
convert(uniqueidentifier,ownerid) as ownerid,
convert(integer,owneriddsc) as owneriddsc,
owneridname as owneridname,
convert(integer,owneridtype) as owneridtype,
owneridyominame as owneridyominame,
convert(uniqueidentifier,owningbusinessunit) as owningbusinessunit,
convert(uniqueidentifier,owningteam) as owningteam,
convert(uniqueidentifier,owninguser) as owninguser,
convert(datetime,posta_datum) as posta_datum,
convert(datetime,posta_datumutc) as posta_datumutc,
posta_felado as posta_felado,
convert(uniqueidentifier,posta_feladojegyzekid) as posta_feladojegyzekid,
posta_feladoname as posta_feladoname,
posta_megallapodasazonosito as posta_megallapodasazonosito,
posta_name as posta_name,
posta_vevokod as posta_vevokod,
convert(integer,statecode) as statecode,
statecodename as statecodename,
convert(integer,statuscode) as statuscode,
statuscodename as statuscodename,
timezoneruleversionnumber as timezoneruleversionnumber,
utcconversiontimezonecode as utcconversiontimezonecode,
versionnumber as versionnumber,
char(32) as dummy 
from WORK_POSTA_FeladoJegyzek;

' 
GO
