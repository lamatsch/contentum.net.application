IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VIEW_POSTA_IsmertCimzett]'))
DROP VIEW [dbo].[VIEW_POSTA_IsmertCimzett]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VIEW_POSTA_IsmertCimzett]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[VIEW_POSTA_IsmertCimzett] as select
convert(uniqueidentifier,createdby) as createdby,
createdbyname as createdbyname,
createdbyyominame as createdbyyominame,
convert(datetime,createdon) as createdon,
convert(datetime,createdonutc) as createdonutc,
convert(uniqueidentifier,createdonbehalfby) as createdonbehalfby,
createdonbehalfbyname as createdonbehalfbyname,
createdonbehalfbyyominame as createdonbehalfbyyominame,
convert(numeric(12,5),replace(exchangerate,'','',''.'') )as exchangerate, -- mod AA 2016.02.03 -- tizedesvesszo cserélése tizedespontra kézzel ...
importsequencenumber as importsequencenumber,
convert(uniqueidentifier,modifiedby) as modifiedby,
modifiedbyname as modifiedbyname,
modifiedbyyominame as modifiedbyyominame,
convert(datetime,modifiedon) as modifiedon,
convert(datetime,modifiedonutc) as modifiedonutc,
convert(uniqueidentifier,modifiedonbehalfby) as modifiedonbehalfby,
modifiedonbehalfbyname as modifiedonbehalfbyname,
modifiedonbehalfbyyominame as modifiedonbehalfbyyominame,
convert(datetime,overriddencreatedon) as overriddencreatedon,
convert(datetime,overriddencreatedonutc) as overriddencreatedonutc,
convert(uniqueidentifier,ownerid) as ownerid,
convert(integer,owneriddsc) as owneriddsc,
owneridname as owneridname,
convert(integer,owneridtype) as owneridtype,
owneridyominame as owneridyominame,
convert(uniqueidentifier,owningbusinessunit) as owningbusinessunit,
convert(uniqueidentifier,owningteam) as owningteam,
convert(uniqueidentifier,owninguser) as owninguser,
posta_ajanlott as posta_ajanlott,
posta_ajanlottname as posta_ajanlottname,
convert(uniqueidentifier,posta_cimzett) as posta_cimzett,
posta_cimzettcim as posta_cimzettcim,
posta_cimzettname as posta_cimzettname,
posta_iktatoszam as posta_iktatoszam,
convert(uniqueidentifier,posta_ismertcimzett) as posta_ismertcimzett,
convert(uniqueidentifier,posta_ismertcimzettid) as posta_ismertcimzettid,
posta_ismertcimzettname as posta_ismertcimzettname,
convert(uniqueidentifier,posta_kuldemanyfajta) as posta_kuldemanyfajta,
posta_kuldemanyfajtaname as posta_kuldemanyfajtaname,
convert(integer,posta_kuldemenyar) as posta_kuldemenyar,
convert(integer,posta_kuldemenyar_base) as posta_kuldemenyar_base,
convert(uniqueidentifier,posta_kuldo) as posta_kuldo,
posta_kuldoname as posta_kuldoname,
posta_name as posta_name,
posta_ragszam as posta_ragszam,
posta_sajatkezbe as posta_sajatkezbe,
posta_sajatkezbename as posta_sajatkezbename,
posta_tertiveveny as posta_tertiveveny,
posta_tertivevenyname as posta_tertivevenyname,
substring(posta_tertivevenyvonalkodja,1,50) as posta_tertivevenyvonalkodja, -- mod AA 2016.02.15 -- indexet teszünk rá, ezért boven elég az 50-hossz ...
posta_tomeg_g as posta_tomeg_g,
posta_vonalkod as posta_vonalkod,
convert(integer,statecode) as statecode,
statecodename as statecodename,
convert(integer,statuscode) as statuscode,
statuscodename as statuscodename,
timezoneruleversionnumber as timezoneruleversionnumber,
convert(uniqueidentifier,transactioncurrencyid) as transactioncurrencyid,
transactioncurrencyidname as transactioncurrencyidname,
utcconversiontimezonecode as utcconversiontimezonecode,
versionnumber as versionnumber,
char(32) as dummy 
from WORK_POSTA_IsmertCimzett;

' 
GO
