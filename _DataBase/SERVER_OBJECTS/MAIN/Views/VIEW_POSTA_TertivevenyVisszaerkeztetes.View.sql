IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VIEW_POSTA_TertivevenyVisszaerkeztetes]'))
DROP VIEW [dbo].[VIEW_POSTA_TertivevenyVisszaerkeztetes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VIEW_POSTA_TertivevenyVisszaerkeztetes]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[VIEW_POSTA_TertivevenyVisszaerkeztetes] as select
convert(uniqueidentifier,createdby) as createdby,
createdbyname as createdbyname,
createdbyyominame as createdbyyominame,
convert(datetime,createdon) as createdon,
convert(datetime,createdonutc) as createdonutc,
convert(uniqueidentifier,createdonbehalfby) as createdonbehalfby,
createdonbehalfbyname as createdonbehalfbyname,
createdonbehalfbyyominame as createdonbehalfbyyominame,
importsequencenumber as importsequencenumber,
convert(uniqueidentifier,modifiedby) as modifiedby,
modifiedbyname as modifiedbyname,
modifiedbyyominame as modifiedbyyominame,
convert(datetime,modifiedon) as modifiedon,
convert(datetime,modifiedonutc) as modifiedonutc,
convert(uniqueidentifier,modifiedonbehalfby) as modifiedonbehalfby,
modifiedonbehalfbyname as modifiedonbehalfbyname,
modifiedonbehalfbyyominame as modifiedonbehalfbyyominame,
convert(datetime,overriddencreatedon) as overriddencreatedon,
convert(datetime,overriddencreatedonutc) as overriddencreatedonutc,
convert(uniqueidentifier,ownerid) as ownerid,
convert(integer,owneriddsc) as owneriddsc,
owneridname as owneridname,
convert(integer,owneridtype) as owneridtype,
owneridyominame as owneridyominame,
convert(uniqueidentifier,owningbusinessunit) as owningbusinessunit,
convert(uniqueidentifier,owningteam) as owningteam,
convert(uniqueidentifier,owninguser) as owninguser,
convert(datetime,posta_atveteldatuma) as posta_atveteldatuma,
convert(datetime,posta_atveteldatumautc) as posta_atveteldatumautc,
posta_atveteljogcime as posta_atveteljogcime,
posta_atvevoszemely as posta_atvevoszemely,
convert(uniqueidentifier,posta_ismertcimzettkuldemeny) as posta_ismertcimzettkuldemeny,
posta_ismertcimzettkuldemenyname as posta_ismertcimzettkuldemenyname,
posta_kezbesiteseredmenye as posta_kezbesiteseredmenye,
posta_kezbesiteseredmenyename as posta_kezbesiteseredmenyename,
convert(uniqueidentifier,posta_tertivevenyvisszaerkeztetesid) as posta_tertivevenyvisszaerkeztetesid,
substring(posta_tertivevenyvonalkodja,1,50) as posta_tertivevenyvonalkodja, -- mod AA 2016.02.15 -- indexet teszünk rá, ezért boven elég az 50-hossz ...
convert(integer,statecode) as statecode,
statecodename as statecodename,
convert(integer,statuscode) as statuscode,
statuscodename as statuscodename,
timezoneruleversionnumber as timezoneruleversionnumber,
utcconversiontimezonecode as utcconversiontimezonecode,
versionnumber as versionnumber,
char(32) as dummy 
from WORK_POSTA_TertivevenyVisszaerkeztetes;

' 
GO
