IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[eDokAdatok]'))
DROP VIEW [dbo].[eDokAdatok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[eDokAdatok]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[eDokAdatok] as
select 
    -- cast(irat.id as nvarchar(36))+cast(edocs.id as nvarchar(36)) as irat_dok_id
     edocs.id as dokumentum_id
   , irat.id as irat_id
   , pld.barcode as barcode  --???!!!!
   , ugy.foszam as foszam
   , irat.alszam as alszam
   , ikt.azonosito as ikt_azonosito
   , ikt.ev as ikt_ev
   , ikt.iktatohely as ikt_hely
   , ikt.megkuljelzes as ikt_megkuljelzes
   , agjelek.nev as agjel_nev
   , agjelek.kod as agjel_kod
   , imd.ugykorkod as ugykorkod
   , imd.ugytipus as ugytipus
   , imd.ugytipusnev as ugytipusnev
   , imd.eljarasiszakasz as eljarasiszakasz
   , imd.generalttargy as generalt_targy
   , irat.irattipus as irattipus
   , irat.targy as irattargy
   , irat.allapot as iratallapot
   , pld.partner_id_cimzett as cimzett_neve_id
   , pld.nevstr_cimzett as cimzett_neve
   , pld.cim_id_cimzett as cimzett_cime_id
   , pld.cimstr_cimzett as cimzett_cime
   , alairok.felhasznalocsoport_id_alairo as alairo_neve_id
   , csoportok.nev as alairo_neve
from 
     dbo.KRT_Dokumentumok as edocs
   , dbo.erec_IraIratokDokumentumok as iradocs
   , dbo.erec_irairatok as irat
   , dbo.EREC_UgyUgyiratdarabok as darab
   , dbo.erec_ugyugyiratok as ugy
   , dbo.EREC_IraIktatoKonyvek as ikt
   , dbo.EREC_IratAlairok as alairok
   , dbo.EREC_PldIratPeldanyok as pld
   , dbo.KRT_Csoportok as csoportok
   , dbo.EREC_IraIrattariTetelek as itaritetelek
   , dbo.EREC_IratMetaDefinicio as imd
   , dbo.EREC_AgazatiJelek as agjelek
where  edocs.id = iradocs.Dokumentum_Id
  and iradocs.IraIrat_Id = irat.id
  and irat.ugyugyiratdarab_id = darab.id
  and darab.ugyugyirat_id = ugy.id
  and ugy.iraiktatokonyv_id = ikt.id
  and alairok.obj_id = irat.id
  and alairok.felhasznalocsoport_id_alairo = csoportok.id
  and pld.irairat_id = irat.id
  and ugy.irairattaritetel_id = itaritetelek.id
  and imd.ugykor_id = itaritetelek.id
  and imd.ugytipus = ugy.ugytipus
  and imd.eljarasiszakasz = darab.eljarasiszakasz
  --and imd.irattipus = irat.irattipus
  and itaritetelek.agazatijel_id = agjelek.id

' 
GO
