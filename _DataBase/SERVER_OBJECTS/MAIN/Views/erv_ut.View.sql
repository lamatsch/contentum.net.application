IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[erv_ut]'))
DROP VIEW [dbo].[erv_ut]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[erv_ut]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[erv_ut]
as 
(
select ugykor_id,ugykorkod,ugytipus,ervkezd, ervvege from erec_iratmetadefinicio 
where ervkezd >= ''2013-01-01 00:00:00.000'' and ervvege >= ''2013-01-25 00:00:00.000''  
)

' 
GO
