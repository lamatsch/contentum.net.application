
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_FoszamSzereles')
            and   type = 'P')
   drop procedure sp_MIG_FoszamSzereles
go

create procedure sp_MIG_FoszamSzereles
             @Sav nvarchar(20),
             @Year int,
             @Num int,					
             @Edok_Utoirat_Id UNIQUEIDENTIFIER = NULL,
             @Edok_Utoirat_Azon NVARCHAR(100) = NULL,
             @Visszavonas INT = 0,
             @ExecutorUserId uniqueidentifier,
			 @ExecutionTime DATETIME
as

/*
--              Hibaüzenetek helyett hibakódok, az alkalmazás oldja fel
--                  [56001] 'Az ügyirat (alszám) megadása kötelező!'
--                  [56002] 'MIG_Alszam select hiba!'
--                  [56003] 'A megadott ügyirat nem létezik!'
--                  [56004] 'Az ügyirat új helyének megadása kötelező!'
--                  [56005] 'Az ügyirat új helyének megadása hibás!'
--                  [56006] 'Az ügyirat már eleve a kért helyen van!'
--                  [56007] 'Irattárból skontróba való áthelyezés nem engedélyezett!'
--                  [56008] 'Skontróba helyezésnél kötelező a skontró végének megadása!'
--                  [56009] 'A skontró végének az aktuális napnál későbbinek kell lennie!'
--                  [56010] 'MIG_Alszam update hiba!'
--                  [56011] 'Az ügyirat közben módosult!'
--                  [56012] 'Utóirat id-jának megadása kötelező!'
--                  [56013] 'Utóirat azonosítójának megadása kötelező!'
--                  [56014] 'MIG_Foszam update hiba!'
--                  [56015] 'MIG_Foszam select hiba!'
--                  [56016] 'A megadott előzmény ügyirat nem létezik!'
--                  [56017] 'A megadott előzmény ügyirat már szerelve van!'
--
--                  [56301] 'Az ügyirat (főszám) megadása kötelező!'

-- példa:  -----
begin tran 

select Edok_Utoirat_Id,Edok_Utoirat_Azon  -- előtte
  from MIG_Foszam
 where ui_sav=02 AND ui_year=1997 AND ui_num=210

exec [dbo].[sp_MIG_FoszamSzereles]  02,1997,210,'1D8FE7E7-3767-4121-951E-DCC922BB6F66','FPH01/610/2008/01'

select Edok_Utoirat_Id,Edok_Utoirat_Azon  -- utánna
from MIG_Foszam
where ui_sav=02 AND ui_year=1997 AND ui_num=210

rollback tran 
GO

*/
/*
Módosította:
-- 2008.07.14. Boda Eszter:
   A MIG_Alszam táblából az UGYHOL (valamint a SCONTRO, IRATTARBA) mezők átkerültek
   a MIG_Foszam táblába
   Ha a szerelendő (régi) ügyirat nincs osztályon, és nem visszavonás történik,
   automatikusan kivesszük teljes hierarchiájával együtt,
   mivel másként nem lehetne együtt mozgatni a szülő EDOK-os ügyirattal
       -> sp_MIG_FoszamAthelyezes hívása
-- 2009.05.14. Boda Eszter:
   Ha a szerelendő (régi) ügyirat állapota Irattárból elkért ('I') vagy Skontróból elkért ('E'),
   nem helyezzük át automatikusan, hanem aktuális állapotában megtartjuk
*/

---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 0
 
declare @sajat_tranz     int  -- saját tranzakció kezelés? (0 = nem, 1 = igen)
select  @sajat_tranz   = (CASE When @@trancount > 0 Then 0 Else 1 END)

if @sajat_tranz = 1  
   BEGIN TRANSACTION 
   
   
--
DECLARE @Id UNIQUEIDENTIFIER
DECLARE @Act_Edok_Utoirat_Id UNIQUEIDENTIFIER
DECLARE @Act_Edok_Utoirat_Azon NVARCHAR(100)

DECLARE @Act_Ugyhol char(1)
DECLARE @Act_Ver int
--
declare @error           int
declare @rowcount        int

--------------------------------
-- input paraméterek ellenőrzése
--------------------------------
-- input paraméterek kiiratása
if @teszt = 1
   print '@Sav = '+ isnull(convert(nvarchar(50), @Sav), 'NULL') + ', ' +
         '@Year = '+ isnull(convert(nvarchar(50),@Year), 'NULL') + ', ' +
         '@Num = '+ isnull(convert(nvarchar(50),@Num), 'NULL') + ', ' +
         '@Edok_Utoirat_Id  = '+ isnull(convert(varchar(50),@Edok_Utoirat_Id) , 'NULL') + ', ' +
         '@Edok_Utoirat_Azon = '+ isnull(@Edok_Utoirat_Azon, 'NULL') + ', ' +
         '@Visszavonas = ' + isnull(convert(nvarchar(50),@Visszavonas), 'NULL')

--
if @Sav is NULL
   --RAISERROR('Az ügyirat (főszám) megadása kötelező!',16,1)
   RAISERROR('[56301]',16,1)
   
if @Year is NULL
   --RAISERROR('Az ügyirat (főszám) megadása kötelező!',16,1)
   RAISERROR('[56301]',16,1)
   
if @Num is NULL
   --RAISERROR('Az ügyirat (főszám) megadása kötelező!',16,1)
   RAISERROR('[56301]',16,1)

IF @Visszavonas = 1
BEGIN
	SET @Edok_Utoirat_Id = NULL
	SET @Edok_Utoirat_Azon = NULL
END
ELSE
BEGIN
	if @Edok_Utoirat_Id is NULL
	   --RAISERROR('Utóirat id-jának megadása kötelező!,16,1)
	   RAISERROR('[56012]',16,1)
	   
	if @Edok_Utoirat_Azon is NULL
	   --RAISERROR('Utóirat azonosítójának megadása kötelező!,16,1)
	   RAISERROR('[56013]',16,1)
END   
   
-- a régi dokumentum olvasása
select @Id = MIG_Foszam.Id,
       @Act_Edok_Utoirat_Id  = MIG_Foszam.Edok_Utoirat_Id,
       @Act_Edok_Utoirat_Azon  = MIG_Foszam.Edok_Utoirat_Azon,
       @Act_Ugyhol = MIG_foszam.UGYHOL,
       @Act_Ver = Ver
  from MIG_Foszam
--  inner join MIG_Sav on MIG_Sav.Id = MIG_Foszam.MIG_Sav_Id
 where  
--	 SUBSTRING(MIG_Sav.NAME,2, CHARINDEX(')', MIG_Sav.NAME) - 2 ) = @Sav
-- AND CHARINDEX('(', MIG_Sav.NAME) = 1 AND CHARINDEX(')', MIG_Sav.NAME) > 2
	EdokSav = @Sav
 AND UI_YEAR = @Year
 AND UI_NUM = @Num

select  @error = @@error, @rowcount = @@rowcount

if @error <> 0
   --RAISERROR('MIG_Foszam select hiba!',16,1)
   RAISERROR('[56015]',16,1)

if @rowcount = 0
   --RAISERROR('A megadott előzmény ügyirat nem létezik!',16,1)
   RAISERROR('[56016]',16,1) 

IF @Visszavonas != 1
BEGIN   
	IF @Act_Edok_Utoirat_Id IS NOT NULL
	-- RAISERROR('A megadott előzmény ügyirat már szerelve van!',16,1)
	   RAISERROR('[56017]',16,1)             
END

-- Ha a szerelendo ugyirat nincs osztályon, kivesszuk, hierarchiajaval egyutt
-- kivéve, ha Irattűrból elért ('I') vagy Skontróból elkért ('E')
IF @Act_Ugyhol not in ('I','E')
BEGIN
	IF @Visszavonas != 1 and @Act_Ugyhol != '3'
	BEGIN
		IF @Act_Ugyhol = '2' --Irattárban
		BEGIN
			EXEC sp_MIG_FoszamAthelyezes
					 @Id = @Id,
					 @Ugyhova = 'I', -- Irattárból elkért
					 @MoveHierarchy = '1',
					 @ExecutorUserId = @ExecutorUserId,
					 @ExecutionTime = @ExecutionTime 			
		END
		ELSE IF @Act_Ugyhol = '1' --Skontróban
		BEGIN
			EXEC sp_MIG_FoszamAthelyezes
					 @Id = @Id,
					 @Ugyhova = 'E', -- Skontróból elkért
					 @MoveHierarchy = '1',
					 @ExecutorUserId = @ExecutorUserId,
					 @ExecutionTime = @ExecutionTime 			
		END
		ELSE
		BEGIN
			EXEC sp_MIG_FoszamAthelyezes
					 @Id = @Id,
					 @Ugyhova = '3', -- Osztályon
					 @MoveHierarchy = '1',
					 @ExecutorUserId = @ExecutorUserId,
					 @ExecutionTime = @ExecutionTime 
		END          
	END
END

--Visszavonás esetén, ha a szerelendő ügyirat irattárból elkért vagy skontróbol elkért, akkor vissza tesszük irattárba vagy skontróba
IF @Visszavonas = 1
BEGIN
	IF @Act_Ugyhol = 'I' -- Irattárból elkért
	BEGIN
		EXEC sp_MIG_FoszamAthelyezes
					 @Id = @Id,
					 @Ugyhova = '2', -- Irattárban
					 @MoveHierarchy = '1',
					 @ExecutorUserId = @ExecutorUserId,
					 @ExecutionTime = @ExecutionTime 
	END
	ELSE IF @Act_Ugyhol = 'E' -- Skontróból elkért
	BEGIN
		EXEC sp_MIG_FoszamAthelyezes
					 @Id = @Id,
					 @Ugyhova = '1', -- Skontróban
					 @MoveHierarchy = '1',
					 @ExecutorUserId = @ExecutorUserId,
					 @ExecutionTime = @ExecutionTime 
	END
END

---------------------------------------------------------
-- akkor jöhet a tényleges módosítás (update)            
---------------------------------------------------------
update dbo.MIG_Foszam
   set Edok_Utoirat_Id = @Edok_Utoirat_Id,
       Edok_Utoirat_Azon = @Edok_Utoirat_Azon,
       Ver = ISNULL(Ver,1) + 1
 where Id = @Id

select  @error = @@error, @rowcount = @@rowcount

if @error <> 0
   --RAISERROR('MIG_Foszam update hiba!',16,1)
   RAISERROR('[56014]',16,1)

if @rowcount = 0
    --RAISERROR('Az ügyirat közben módosult!',16,1)
   RAISERROR('[56011]',16,1)
   
/* History Log */   
exec sp_LogRecordToHistory 'MIG_Foszam',@Id
					,'MIG_FoszamHistory',1,@ExecutorUserId,@ExecutionTime   

if @sajat_tranz = 1  
   COMMIT TRANSACTION

END TRY
-----------
-----------
BEGIN CATCH
   if @sajat_tranz = 1  
      ROLLBACK TRANSACTION
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------
go

