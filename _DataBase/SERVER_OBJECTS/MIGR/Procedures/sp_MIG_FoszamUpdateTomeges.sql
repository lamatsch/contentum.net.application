set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_FoszamUpdateTomeges')
            and   type = 'P')
   drop procedure sp_MIG_FoszamUpdateTomeges
go

create procedure sp_MIG_FoszamUpdateTomeges
             @Where nvarchar(MAX)  = '', -- Tömeges update szűrési feltételeknek megfelelő tételekre
--             @ExtendedMIG_SavWhere nvarchar(MAX) = '',
             @ExtendedMIG_AlszamWhere nvarchar(MAX) = '',
             @ExtendedMIG_EloadoWhere nvarchar(MAX) = '',
             @ExtendedMIG_JegyzekTetelekWhere nvarchar(MAX) = '',
             @ExecutorUserId uniqueidentifier,
			 @ExecutionTime DATETIME,
             @UI_SAV int = NULL,
			 @UI_YEAR int = NULL,
			 @UI_NUM int = NULL,
			 @UI_NAME nvarchar(30) = NULL,
			 @UI_IRSZ nvarchar(4) = NULL,
			 @UI_UTCA nvarchar(25) = NULL,
			 @UI_HSZ nvarchar(12) = NULL,
			 @UI_HRSZ nvarchar(20) = NULL,
			 @UI_TYPE nvarchar(4) = NULL,
			 @UI_IRJ nvarchar(6) = NULL,
			 @UI_PERS nvarchar(6) = NULL,
			 @UI_OT_ID nvarchar(20) = NULL,
			 @MEMO nvarchar(70) = NULL,
			 @IRSZ_PLUSS INT = NULL,
			 @IRJ2000 nvarchar(7) = NULL,
			 @Conc nvarchar(255) = NULL,
			 @Selejtezve INT = NULL,
			 @Selejtezes_Datuma DATETIME = NULL,
			 @Csatolva_Rendszer INT = NULL,
			 @Csatolva_Id UNIQUEIDENTIFIER = NULL,
			 @MIG_Sav_Id UNIQUEIDENTIFIER = NULL,
			 @MIG_Varos_Id UNIQUEIDENTIFIER = NULL,
			 @MIG_IktatasTipus_Id UNIQUEIDENTIFIER = NULL,
			 @MIG_Eloado_Id UNIQUEIDENTIFIER = NULL,
			 @Edok_Ugyintezo_Csoport_Id UNIQUEIDENTIFIER = NULL,
			 @Edok_Ugyintezo_Nev nvarchar(100) = NULL,
			 @Edok_Utoirat_Id UNIQUEIDENTIFIER = NULL,
			 @Edok_Utoirat_Azon nvarchar(100) = NULL,
			 @UGYHOL nvarchar(1) = NULL,
			 @IRATTARBA DATETIME = NULL,
			 @SCONTRO DATETIME = NULL,
			 @Ver INT = NULL,
			 @EdokSav nvarchar(3)  = null, -- "ReadOnly" mező
			 @MegorzesiIdo DATETIME = null,
             @UpdatedColumns              XML,
             @UpdateHierarchy      char(1) = '0'

             
AS

BEGIN TRY

set nocount on;

DECLARE @updateColumnsAndValues NVARCHAR(4000)
SET @updateColumnsAndValues = ''
       
       
         IF @UpdatedColumns.exist('/root/UI_SAV')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + 'UI_SAV = @UI_SAV
            '
         END
         IF @UpdatedColumns.exist('/root/UI_YEAR')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_YEAR = @UI_YEAR
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_NUM')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_NUM = @UI_NUM
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_NAME')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_NAME = @UI_NAME
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_IRSZ')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_IRSZ = @UI_IRSZ
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_UTCA')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_UTCA = @UI_UTCA
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_HSZ')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_HSZ = @UI_HSZ
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_HRSZ')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_HRSZ = @UI_HRSZ
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_TYPE')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_TYPE = @UI_TYPE
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_IRJ')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_IRJ = @UI_IRJ
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_PERS')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_PERS = @UI_PERS
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_OT_ID')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_OT_ID = @UI_OT_ID
            '
         end 
         IF @UpdatedColumns.exist('/root/MEMO')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',MEMO = @MEMO
            '
         end 
         IF @UpdatedColumns.exist('/root/IRSZ_PLUSS')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',IRSZ_PLUSS = @IRSZ_PLUSS
            '
         end 
         IF @UpdatedColumns.exist('/root/IRJ2000')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',IRJ2000 = @IRJ2000
            '
         end 
         IF @UpdatedColumns.exist('/root/Conc')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Conc = @Conc
            '
         end 
         IF @UpdatedColumns.exist('/root/Selejtezve')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Selejtezve = @Selejtezve
            '
         end 
         IF @UpdatedColumns.exist('/root/Selejtezes_Datuma')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Selejtezes_Datuma = @Selejtezes_Datuma
            '
         end 
         IF @UpdatedColumns.exist('/root/Csatolva_Rendszer')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Csatolva_Rendszer = @Csatolva_Rendszer
            '
         end 
         IF @UpdatedColumns.exist('/root/Csatolva_Id')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Csatolva_Id = @Csatolva_Id
            '
         end 
         IF @UpdatedColumns.exist('/root/MIG_Sav_Id')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',MIG_Sav_Id = @MIG_Sav_Id
            '
         end 
         IF @UpdatedColumns.exist('/root/MIG_Varos_Id')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',MIG_Varos_Id = @MIG_Varos_Id
            '
         end 
         IF @UpdatedColumns.exist('/root/MIG_IktatasTipus_Id')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',MIG_IktatasTipus_Id = @MIG_IktatasTipus_Id
            '
         end 
         IF @UpdatedColumns.exist('/root/MIG_Eloado_Id')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',MIG_Eloado_Id = @MIG_Eloado_Id
            '
         end
         IF @UpdatedColumns.exist('/root/Edok_Ugyintezo_Csoport_Id')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Edok_Ugyintezo_Csoport_Id = @Edok_Ugyintezo_Csoport_Id
            '
         end 
         IF @UpdatedColumns.exist('/root/Edok_Ugyintezo_Nev')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Edok_Ugyintezo_Nev = @Edok_Ugyintezo_Nev
            '
         end  
         IF @UpdatedColumns.exist('/root/Edok_Utoirat_Id')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Edok_Utoirat_Id = @Edok_Utoirat_Id
            '
         end
         IF @UpdatedColumns.exist('/root/Edok_Utoirat_Azon')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Edok_Utoirat_Azon = @Edok_Utoirat_Azon
            '
         end
         IF @UpdatedColumns.exist('/root/UGYHOL')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UGYHOL = @UGYHOL
            '
         end 
         IF @UpdatedColumns.exist('/root/IRATTARBA')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',IRATTARBA = @IRATTARBA
            '
         end 
         IF @UpdatedColumns.exist('/root/SCONTRO')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',SCONTRO = @SCONTRO
            '
         END
		 IF @UpdatedColumns.exist('/root/MegorzesiIdo')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',MegorzesiIdo = @MegorzesiIdo
            '
         END
         
          SET @updateColumnsAndValues = @updateColumnsAndValues + ',Ver=ISNULL(Ver,1)+1
          '    
   
IF CHARINDEX(',', @updateColumnsAndValues) = 1
   SET @updateColumnsAndValues = SUBSTRING(@updateColumnsAndValues, 2, LEN(@updateColumnsAndValues) -1 )
   
DECLARE @UpdatetCommand NVARCHAR(4000)

SET @UpdatetCommand = 'update MIG_Foszam SET ' + @updateColumnsAndValues + ' where Id in (select Id from #filter)'
--SET @UpdatetCommand = @UpdatetCommand + '
--					  select @@ROWCOUNT as RecordNumber'

PRINT @UpdatetCommand

create table #filter(Id UNIQUEIDENTIFIER)

INSERT INTO [#filter] (
	[Id]
) EXEC [sp_MIG_FoszamGetAll_Id]
	@Where = @Where,
--	@ExtendedMIG_SavWhere = @ExtendedMIG_SavWhere,
	@ExtendedMIG_AlszamWhere = @ExtendedMIG_AlszamWhere,
	@ExtendedMIG_EloadoWhere = @ExtendedMIG_EloadoWhere,
	@ExtendedMIG_JegyzekTetelekWhere = @ExtendedMIG_JegyzekTetelekWhere
	
SELECT COUNT(Id) AS RecordNumber FROM [#filter]
	
IF @UpdateHierarchy = '1'
BEGIN
    DECLARE @ValueTable table (Id UNIQUEIDENTIFIER);
    DECLARE @BaseTable table (Id UNIQUEIDENTIFIER, Csatolva_Id UNIQUEIDENTIFIER);
    INSERT INTO @BaseTable(Id, Csatolva_Id)
    SELECT Id, Csatolva_Id from MIG_Foszam
    WHERE Id in (Select Id from [#filter]);
	WITH FoszamHierarchy  AS
	(
	   -- Base case
	   SELECT Id, Csatolva_Id, 0 as HierarchyLevel
	   FROM @BaseTable

	   UNION ALL

	   -- Recursive step
	   SELECT f.Id, f.Csatolva_Id, fh.HierarchyLevel - 1 AS HierarchyLevel
	   FROM MIG_Foszam as f
		  INNER JOIN  FoszamHierarchy as fh ON
			 f.Csatolva_Id = fh.Id
	)
	
	INSERT INTO @ValueTable
	    SELECT Id FROM FoszamHierarchy;
-- 2. szülők
	WITH FoszamHierarchy  AS
	(
	   -- Base case
	   SELECT Id, Csatolva_Id, 0 as HierarchyLevel
	   FROM @BaseTable

	   UNION ALL

	   -- Recursive step
	   SELECT f.Id, f.Csatolva_Id, fh.HierarchyLevel + 1 AS HierarchyLevel
	   FROM MIG_Foszam as f
		  INNER JOIN  FoszamHierarchy as fh ON
			 f.Id = fh.Csatolva_Id
	)
	
	INSERT INTO @ValueTable
	    SELECT Id FROM FoszamHierarchy WHERE HierarchyLevel > 0
	    
	DELETE [#filter]
	INSERT INTO [#filter] (
		[Id]
	) SELECT Id FROM @ValueTable    
END	


exec sp_executesql @UpdatetCommand, 
           N'@UI_SAV int,
			 @UI_YEAR int,
			 @UI_NUM int,
			 @UI_NAME nvarchar(30),
			 @UI_IRSZ nvarchar(4),
			 @UI_UTCA nvarchar(25),
			 @UI_HSZ nvarchar(12),
			 @UI_HRSZ nvarchar(20),
			 @UI_TYPE nvarchar(4),
			 @UI_IRJ nvarchar(6),
			 @UI_PERS nvarchar(6),
			 @UI_OT_ID nvarchar(20),
			 @MEMO nvarchar(70),
			 @IRSZ_PLUSS INT,
			 @IRJ2000 nvarchar(7),
			 @Conc nvarchar(255),
			 @Selejtezve INT,
			 @Selejtezes_Datuma DATETIME,
			 @Csatolva_Rendszer INT,
			 @Csatolva_Id UNIQUEIDENTIFIER,
			 @MIG_Sav_Id UNIQUEIDENTIFIER,
			 @MIG_Varos_Id UNIQUEIDENTIFIER,
			 @MIG_IktatasTipus_Id UNIQUEIDENTIFIER,
			 @MIG_Eloado_Id UNIQUEIDENTIFIER,
			 @Edok_Ugyintezo_Csoport_Id UNIQUEIDENTIFIER,
			 @Edok_Ugyintezo_Nev nvarchar(100),
			 @Edok_Utoirat_Id UNIQUEIDENTIFIER,
			 @Edok_Utoirat_Azon nvarchar(100),
			 @UGYHOL nvarchar(1),
			 @IRATTARBA DATETIME,
			 @SCONTRO DATETIME,
			 @MegorzesiIdo DATETIME',
			 @UI_SAV = @UI_SAV,
			 @UI_YEAR = @UI_YEAR,
			 @UI_NUM = @UI_NUM,
			 @UI_NAME = @UI_NAME,
			 @UI_IRSZ = @UI_IRSZ,
			 @UI_UTCA = @UI_UTCA,
			 @UI_HSZ = @UI_HSZ,
			 @UI_HRSZ = @UI_HRSZ,
			 @UI_TYPE = @UI_TYPE,
			 @UI_IRJ = @UI_IRJ,
			 @UI_PERS = @UI_PERS,
			 @UI_OT_ID = @UI_OT_ID,
			 @MEMO = @MEMO,
			 @IRSZ_PLUSS = @IRSZ_PLUSS,
			 @IRJ2000 = @IRJ2000,
			 @Conc = @Conc,
			 @Selejtezve = @Selejtezve,
			 @Selejtezes_Datuma = @Selejtezes_Datuma,
			 @Csatolva_Rendszer = @Csatolva_Rendszer,
			 @Csatolva_Id = @Csatolva_Id,
			 @MIG_Sav_Id = @MIG_Sav_Id,
			 @MIG_Varos_Id = @MIG_Varos_Id,
			 @MIG_IktatasTipus_Id = @MIG_IktatasTipus_Id,
			 @MIG_Eloado_Id = @MIG_Eloado_Id,
			 @Edok_Ugyintezo_Csoport_Id = @Edok_Ugyintezo_Csoport_Id,
			 @Edok_Ugyintezo_Nev = @Edok_Ugyintezo_Nev,
			 @Edok_Utoirat_Id = @Edok_Utoirat_Id,
			 @Edok_Utoirat_Azon =@Edok_Utoirat_Azon,
			 @UGYHOL = @UGYHOL,
			 @IRATTARBA = @IRATTARBA,
			 @SCONTRO = @SCONTRO,
			 @MegorzesiIdo = @MegorzesiIdo
			 
/* History Log */
exec sp_LogMIG_FoszamHistoryTomeges 1,@ExecutorUserId,@ExecutionTime 			 

Drop table #filter
END TRY

BEGIN CATCH
    
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH

go

