
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_META_ADATInsert')
            and   type = 'P')
   drop procedure sp_MIG_META_ADATInsert
go

create procedure sp_MIG_META_ADATInsert    
                @Id      uniqueidentifier = null,    
               	            @OBJ_ID     uniqueidentifier,
	            @META_ID     uniqueidentifier,
                @ERTEK     nvarchar(Max)  = null,
	            @Ver     int,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @OBJ_ID is not null
         begin
            SET @insertColumns = @insertColumns + ',OBJ_ID'
            SET @insertValues = @insertValues + ',@OBJ_ID'
         end 
       
         if @META_ID is not null
         begin
            SET @insertColumns = @insertColumns + ',META_ID'
            SET @insertValues = @insertValues + ',@META_ID'
         end 
       
         if @ERTEK is not null
         begin
            SET @insertColumns = @insertColumns + ',ERTEK'
            SET @insertValues = @insertValues + ',@ERTEK'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'                  
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into MIG_META_ADAT ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@OBJ_ID uniqueidentifier,@META_ID uniqueidentifier,@ERTEK nvarchar(Max),@Ver int,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@OBJ_ID = @OBJ_ID,@META_ID = @META_ID,@ERTEK = @ERTEK,@Ver = @Ver ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
	declare @Letrehozo_id uniqueidentifier
	declare @LetrehozasIdo datetime

	set @Letrehozo_id = null
	set @LetrehozasIdo = GETDATE()
   /* History Log */
   exec sp_LogRecordToHistory 'MIG_META_ADAT',@ResultUid
					,'MIG_META_ADATHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
go

/* használt errorcode-ok:
	50301:
	50302: 'Org' oszlop értéke nem lehet NULL
*/
