

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_META_ADATUpdate')
            and   type = 'P')
   drop procedure sp_MIG_META_ADATUpdate
go

create procedure sp_MIG_META_ADATUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @OBJ_ID     uniqueidentifier  = null,         
             @META_ID     uniqueidentifier  = null,         
             @ERTEK     nvarchar(Max)  = null,         
             @Ver     int  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_OBJ_ID     uniqueidentifier         
     DECLARE @Act_META_ID     uniqueidentifier         
     DECLARE @Act_ERTEK     nvarchar(Max)         
     DECLARE @Act_Ver     int           
  
set nocount on

select 
     @Act_OBJ_ID = OBJ_ID,
     @Act_META_ID = META_ID,
     @Act_ERTEK = ERTEK,
     @Act_Ver = Ver
from MIG_META_ADAT
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  

	   IF @UpdatedColumns.exist('/root/OBJ_ID')=1
         SET @Act_OBJ_ID = @OBJ_ID
   IF @UpdatedColumns.exist('/root/META_ID')=1
         SET @Act_META_ID = @META_ID
   IF @UpdatedColumns.exist('/root/ERTEK')=1
         SET @Act_ERTEK = @ERTEK   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE MIG_META_ADAT
SET
     OBJ_ID = @Act_OBJ_ID,
     META_ID = @Act_META_ID,
     ERTEK = @Act_ERTEK,
     Ver = @Act_Ver
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'MIG_META_ADAT',@Id
					,'MIG_META_ADATHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

go

/* használt errorcode-ok:
	50401:
	50402: 'Ver' oszlop értéke nem egyezik a meglévő rekordéval
   50403: érvényesség már lejárt a rekordra
	50499: rekord zárolva
*/
