set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_META_TIPUSHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_MIG_META_TIPUSHistoryGetAllRecord
go

create procedure sp_MIG_META_TIPUSHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'L�trehoz�s' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from MIG_META_TIPUSHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_META_TIPUSHistory Old
         inner join MIG_META_TIPUSHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'TIPUS' as ColumnName,               cast(Old.TIPUS as nvarchar(99)) as OldValue,
               cast(New.TIPUS as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_META_TIPUSHistory Old
         inner join MIG_META_TIPUSHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.TIPUS != New.TIPUS 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'M�dos�t�s'
                  when 2 then '�rv�nytelen�t�s'
               end as Operation, 
               'CIMKE' as ColumnName,               cast(Old.CIMKE as nvarchar(99)) as OldValue,
               cast(New.CIMKE as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_META_TIPUSHistory Old
         inner join MIG_META_TIPUSHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.CIMKE != New.CIMKE 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go