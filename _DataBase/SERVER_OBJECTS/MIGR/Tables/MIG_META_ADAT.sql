if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('MIG_META_ADAT') and o.name = 'MIG_META_ADAT_META_TIPUS_FK')
alter table MIG_META_ADAT
   drop constraint MIG_META_ADAT_META_TIPUS_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('MIG_META_ADAT')
            and   name  = 'MIG_META_ADAT_UI'
            and   indid > 0
            and   indid < 255)
   drop index MIG_META_ADAT.MIG_META_ADAT_UI
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('MIG_META_ADAT')
            and   name  = 'MIG_META_ADAT_IX_Obj_ID'
            and   indid > 0
            and   indid < 255)
   drop index MIG_META_ADAT.MIG_META_ADAT_IX_Obj_ID
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MIG_META_ADAT')
            and   type = 'U')
   drop table MIG_META_ADAT
go

/*==============================================================*/
/* Table: MIG_META_ADAT                                         */
/*==============================================================*/
create table MIG_META_ADAT (
   Id                   uniqueidentifier     not null default newsequentialid(),
   OBJ_ID               uniqueidentifier     not null,
   META_ID              uniqueidentifier     not null,
   ERTEK                nvarchar(Max)        null,
   Ver                  int                  not null,
   constraint PK_MIG_META_ADAT primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('MIG_META_ADAT') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'MIG_META_ADAT' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '..., hogy a csak egyes �gyfelekn�l megjelen�  m�sodlagos/le�r� META adatokat tudjuk t�rolni ...', 
   'user', @CurrentUser, 'table', 'MIG_META_ADAT'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('MIG_META_ADAT')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'OBJ_ID')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'MIG_META_ADAT', 'column', 'OBJ_ID'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'MIG_Alszam.ID, ha META_ID -> TIPUS=''A''-ra mutat; MIG_Foszam.ID, ha META_ID -> TIPUS=''F'' -ra mutat',
   'user', @CurrentUser, 'table', 'MIG_META_ADAT', 'column', 'OBJ_ID'
go

/*==============================================================*/
/* Index: MIG_META_ADAT_IX_Obj_ID                               */
/*==============================================================*/
create index MIG_META_ADAT_IX_Obj_ID on MIG_META_ADAT (
OBJ_ID ASC
)
go

/*==============================================================*/
/* Index: MIG_META_ADAT_UI                                      */
/*==============================================================*/
create unique index MIG_META_ADAT_UI on MIG_META_ADAT (
OBJ_ID ASC,
META_ID ASC
)
go

alter table MIG_META_ADAT
   add constraint MIG_META_ADAT_META_TIPUS_FK foreign key (META_ID)
      references MIG_META_TIPUS (Id)
go
