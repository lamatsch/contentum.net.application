-- =========================================
-- Create History table for MIG_META_ADAT
-- =========================================
 
IF OBJECT_ID('MIG_META_ADATHistory', 'U') IS NOT NULL
  DROP TABLE MIG_META_ADATHistory
GO
 
CREATE TABLE MIG_META_ADATHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 OBJ_ID uniqueidentifier,  
 META_ID uniqueidentifier,  
 ERTEK nvarchar(Max),  
 Ver int,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_MIG_META_ADATHistory_ID_VER ON MIG_META_ADATHistory
(
	Id,Ver ASC
)
GO