if exists (select 1
            from  sysobjects
           where  id = object_id('MIG_META_TIPUS')
            and   type = 'U')
   drop table MIG_META_TIPUS
go

/*==============================================================*/
/* Table: MIG_META_TIPUS                                        */
/*==============================================================*/
create table MIG_META_TIPUS (
   Id                   uniqueidentifier     not null default newsequentialid(),
   TIPUS                nvarchar(10)         not null
      constraint CKC_TIPUS_MIG_META check (TIPUS in ('A','F') and TIPUS = upper(TIPUS)),
   CIMKE                nvarchar(255)        not null,
   Ver                  int                  not null,
   constraint PK_MIG_META_TIPUS primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('MIG_META_TIPUS') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'MIG_META_TIPUS' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   'm�sodlagos META adat t�pusa', 
   'user', @CurrentUser, 'table', 'MIG_META_TIPUS'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('MIG_META_TIPUS')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'TIPUS')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'MIG_META_TIPUS', 'column', 'TIPUS'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'A - alsz�m; F - f�sz�m',
   'user', @CurrentUser, 'table', 'MIG_META_TIPUS', 'column', 'TIPUS'
go
