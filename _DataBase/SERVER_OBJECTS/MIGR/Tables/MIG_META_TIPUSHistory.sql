-- =========================================
-- Create History table for MIG_META_TIPUS
-- =========================================
 
IF OBJECT_ID('MIG_META_TIPUSHistory', 'U') IS NOT NULL
  DROP TABLE MIG_META_TIPUSHistory
GO
 
CREATE TABLE MIG_META_TIPUSHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 TIPUS nvarchar(10),  
 CIMKE nvarchar(255),  
 Ver int,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_MIG_META_TIPUSHistory_ID_VER ON MIG_META_TIPUSHistory
(
	Id,Ver ASC
)
GO