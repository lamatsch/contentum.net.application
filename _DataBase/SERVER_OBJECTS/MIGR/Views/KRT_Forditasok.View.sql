IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Forditasok]'))
DROP VIEW [dbo].[KRT_Forditasok]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[KRT_Forditasok]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[KRT_Forditasok] as select * from [$(DatabaseNMHH)].dbo.KRT_Forditasok;' 
GO
