﻿/*
--------------------------------------------------------------------------------------				
--------------------------------------------------------------------------------------
SCHEMA Update Script							
--------------------------------------------------------------------------------------				
--------------------------------------------------------------------------------------
*/

PRINT 'BLG 3530'

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Ugy_Fajtaja' AND Object_ID = Object_ID(N'dbo.EREC_IraIratokHistory'))
	ALTER TABLE [dbo].[EREC_IraIratokHistory] 
	ADD [Ugy_Fajtaja] [nvarchar] (64)
GO
--------------------------------------------------------------------------------------


PRINT 'BLG 5177'
 
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'ElteltIdoUtolsoModositas' AND Object_ID = Object_ID(N'EREC_UgyUgyiratokHistory'))
		ALTER TABLE [dbo].[EREC_UgyUgyiratokHistory] 
		ADD [ElteltIdoUtolsoModositas] [datetime]
GO
--------------------------------------------------------------------------------------

PRINT 'BLG 4731'

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'NevSTR_Szallito' AND Object_ID = Object_ID(N'dbo.EREC_SzamlakHistory'))
	ALTER TABLE [dbo].[EREC_SzamlakHistory] 
	ADD NevSTR_Szallito [nvarchar] (400) null
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'CimSTR_Szallito' AND Object_ID = Object_ID(N'dbo.EREC_SzamlakHistory'))
	ALTER TABLE [dbo].[EREC_SzamlakHistory] 
	ADD CimSTR_Szallito [nvarchar] (400) null
GO

ALTER TABLE [dbo].[EREC_SzamlakHistory] 
	ALTER COLUMN Partner_Id_Szallito UNIQUEIDENTIFIER NULL
GO
--------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Aktiv' AND Object_ID = Object_ID(N'EREC_IraIratokHistory'))
BEGIN
		PRINT 'ADD COLUMN EREC_IraIratokHistory.Aktiv'
		ALTER TABLE [dbo].[EREC_IraIratokHistory] 
		ADD [Aktiv] [bit]
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'MinositoSzervezet' AND Object_ID = Object_ID(N'dbo.EREC_KuldKuldemenyekHistory'))
BEGIN

	PRINT 'Add Column EREC_KuldKuldemenyekHistory.MinositoSzervezet'

	ALTER TABLE [dbo].[EREC_KuldKuldemenyekHistory] 
	ADD [MinositoSzervezet] UNIQUEIDENTIFIER NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Partner_Id_BekuldoKapcsolt' AND Object_ID = Object_ID(N'EREC_KuldKuldemenyekHistory'))
BEGIN
		PRINT 'ADD COLUMN EREC_KuldKuldemenyekHistory.Partner_Id_BekuldoKapcsolt'
		ALTER TABLE [dbo].[EREC_KuldKuldemenyekHistory] 
		ADD [Partner_Id_BekuldoKapcsolt] UNIQUEIDENTIFIER NULL
END
GO

--------------------------------------------------------------------------------------
PRINT 'BLG 5991'

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Erteknyilvanitas' AND Object_ID = Object_ID(N'dbo.EREC_KuldKuldemenyekHistory'))
BEGIN

	PRINT 'Add Column EREC_KuldKuldemenyekHistory.Erteknyilvanitas'

	ALTER TABLE [dbo].[EREC_KuldKuldemenyekHistory] 
	ADD [Erteknyilvanitas] CHAR(1) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'ErteknyilvanitasOsszege' AND Object_ID = Object_ID(N'EREC_KuldKuldemenyekHistory'))
BEGIN
		PRINT 'ADD COLUMN EREC_KuldKuldemenyekHistory.ErteknyilvanitasOsszege'
		ALTER TABLE [dbo].[EREC_KuldKuldemenyekHistory] 
		ADD [ErteknyilvanitasOsszege] INT NULL
END
GO
GO

---------------------------------------BLG_7246---------------------------------------

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'MinositesFelulvizsgalatBizTag' AND Object_ID = Object_ID(N'dbo.EREC_IraIratokHistory'))
BEGIN
	PRINT 'ADD Column EREC_IraIratokHistory.MinositesFelulvizsgalatBizTag'
	ALTER TABLE [dbo].[EREC_IraIratokHistory] 
	ADD [MinositesFelulvizsgalatBizTag] [nvarchar] (4000)
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'MinositesFelulvizsgalatBizTag' AND Object_ID = Object_ID(N'dbo.EREC_KuldKuldemenyekHistory'))
BEGIN
	PRINT 'ADD Column EREC_KuldKuldemenyekHistory.MinositesFelulvizsgalatBizTag'
	ALTER TABLE [dbo].[EREC_KuldKuldemenyekHistory] 
	ADD [MinositesFelulvizsgalatBizTag] [nvarchar] (4000)
END
GO

--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Kapacitas' AND Object_ID = Object_ID(N'EREC_IrattariHelyekHistory'))
BEGIN
		PRINT 'ADD COLUMN EREC_IrattariHelyekHistory.Kapacitas'
		ALTER TABLE [dbo].[EREC_IrattariHelyekHistory] 
		ADD [Kapacitas] FLOAT NULL
END
GO
----------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'IrattarHelyfoglalas' AND Object_ID = Object_ID(N'EREC_UgyUgyiratokHistory'))
BEGIN
		PRINT 'ADD COLUMN EREC_UgyUgyiratokHistory.IrattarHelyfoglalas'
		ALTER TABLE [dbo].[EREC_UgyUgyiratokHistory] 
		ADD [IrattarHelyfoglalas] FLOAT NULL
END
GO
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Allapot' AND Object_ID = Object_ID(N'EREC_TomegesIktatasFolyamatHistory'))
BEGIN
		PRINT 'ADD COLUMN EREC_TomegesIktatasFolyamatHistory.Allapot'
		ALTER TABLE [dbo].[EREC_TomegesIktatasFolyamatHistory] 
		ADD [Allapot] NVARCHAR(64) NULL
END
GO
----------------------------------------------------------------------------------------



IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'IrattarId' AND Object_ID = Object_ID(N'dbo.EREC_KuldKuldemenyekHistory'))
	ALTER TABLE [dbo].[EREC_KuldKuldemenyekHistory] 
	ADD [IrattarId] [uniqueidentifier] NULL
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'IrattariHely' AND Object_ID = Object_ID(N'dbo.EREC_KuldKuldemenyekHistory'))
	ALTER TABLE [dbo].[EREC_KuldKuldemenyekHistory] 
	ADD [IrattariHely] [nvarchar](100) NULL
GO

----------------------------------------------------------------------------------------


PRINT 'BLG 9124 -  EREC_KuldKuldemenyekHistory.KulsoAzonosito'

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'KulsoAzonosito' AND Object_ID = Object_ID(N'dbo.EREC_KuldKuldemenyekHistory'))
	ALTER TABLE [dbo].[EREC_KuldKuldemenyekHistory] 
	ADD [KulsoAzonosito] [NVARCHAR] (400) NULL
GO
-----------------------------------------------------------------------------

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'IrattarId' AND Object_ID = Object_ID(N'EREC_PldIratPeldanyokHistory'))
BEGIN
		PRINT 'ADD COLUMN EREC_PldIratPeldanyokHistory.IrattarId'
		ALTER TABLE [dbo].[EREC_PldIratPeldanyokHistory] 
		ADD [IrattarId] UNIQUEIDENTIFIER NULL
END
GO
-----------------------------------------------------------------------------

--------------------------------------BUG_6058------------------------------------------

PRINT 'BUG 6058'

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Expedialas' AND Object_ID = Object_ID(N'dbo.EREC_TomegesIktatasTetelekHistory'))
BEGIN

	PRINT 'Add Column EREC_TomegesIktatasTetelekHistory.Expedialas'

	ALTER TABLE [dbo].[EREC_TomegesIktatasTetelekHistory] 
	ADD [Expedialas] INT NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'TobbIratEgyKuldemenybe' AND Object_ID = Object_ID(N'dbo.EREC_TomegesIktatasTetelekHistory'))
BEGIN

	PRINT 'Add Column EREC_TomegesIktatasTetelekHistory.TobbIratEgyKuldemenybe'

	ALTER TABLE [dbo].[EREC_TomegesIktatasTetelekHistory] 
	ADD [TobbIratEgyKuldemenybe] INT NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'KuldemenyAtadas' AND Object_ID = Object_ID(N'dbo.EREC_TomegesIktatasTetelekHistory'))
BEGIN

	PRINT 'Add Column EREC_TomegesIktatasTetelekHistory.KuldemenyAtadas'

	ALTER TABLE [dbo].[EREC_TomegesIktatasTetelekHistory] 
	ADD [KuldemenyAtadas] UNIQUEIDENTIFIER NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'HatosagiStatAdat' AND Object_ID = Object_ID(N'dbo.EREC_TomegesIktatasTetelekHistory'))
BEGIN

	PRINT 'Add Column EREC_TomegesIktatasTetelekHistory.HatosagiStatAdat'

	ALTER TABLE [dbo].[EREC_TomegesIktatasTetelekHistory] 
	ADD [HatosagiStatAdat] Nvarchar(Max) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'EREC_IratAlairok' AND Object_ID = Object_ID(N'dbo.EREC_TomegesIktatasTetelekHistory'))
BEGIN

	PRINT 'Add Column EREC_TomegesIktatasTetelekHistory.EREC_IratAlairok'

	ALTER TABLE [dbo].[EREC_TomegesIktatasTetelekHistory] 
	ADD [EREC_IratAlairok] Nvarchar(Max) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Lezarhato' AND Object_ID = Object_ID(N'dbo.EREC_TomegesIktatasTetelekHistory'))
BEGIN

	PRINT 'Add Column EREC_TomegesIktatasTetelekHistory.Lezarhato'

	ALTER TABLE [dbo].[EREC_TomegesIktatasTetelekHistory] 
	ADD [Lezarhato] INT NULL
END
GO

-----------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'FeldolgozasStatusz' AND Object_ID = Object_ID(N'EREC_eBeadvanyokHistory'))
BEGIN
		PRINT 'ADD COLUMN EREC_eBeadvanyokHistory.FeldolgozasStatusz'
		ALTER TABLE [dbo].[EREC_eBeadvanyokHistory] 
		ADD [FeldolgozasStatusz] INT NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'FeldolgozasiHiba' AND Object_ID = Object_ID(N'EREC_eBeadvanyokHistory'))
BEGIN
		PRINT 'ADD COLUMN EREC_eBeadvanyokHistory.FeldolgozasiHiba'
		ALTER TABLE [dbo].[EREC_eBeadvanyokHistory] 
		ADD [FeldolgozasiHiba] NVARCHAR(4000) NULL
END
-----------------------------------------------------------------------------


IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'CustomId' AND Object_ID = Object_ID(N'EREC_CsatolmanyokHistory'))
BEGIN
PRINT 'CustomId mező  hozzáadása a EREC_CsatolmanyokHistory'
		ALTER TABLE [dbo].[EREC_CsatolmanyokHistory] add [CustomId]  char(1)  default '0'
END
GO
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'AspAdoTorolve' AND Object_ID = Object_ID(N'KRT_PartnerekHistory'))
BEGIN
PRINT 'AspAdoTorolve mező  hozzáadása a KRT_PartnerekHistory'
		ALTER TABLE [dbo].[KRT_PartnerekHistory] add [AspAdoTorolve]  char(1)  default '0'
END
GO

-----------------------------------------------------------------------------






















GO
--------------------------------------------------------------------------------------
-- Frissiti az osszes view-t chema alapjan.
--------------------------------------------------------------------------------------
SET NOCOUNT ON

DECLARE @CURRENTVIEW VARCHAR(255)

DECLARE AllView CURSOR FAST_FORWARD
FOR
SELECT
	DISTINCT s.name + '.' + o.name AS ViewName
	FROM sys.objects o JOIN sys.schemas s ON o.schema_id = s.schema_id 
	WHERE	o.[type] = 'V'
		AND OBJECTPROPERTY(o.[object_id], 'IsSchemaBound') <> 1
		AND OBJECTPROPERTY(o.[object_id], 'IsMsShipped') <> 1

OPEN AllView

FETCH NEXT FROM AllView 
INTO @CurrentView

WHILE @@FETCH_STATUS = 0
BEGIN

	--PRINT @CurrentView
	EXEC sp_refreshview @CurrentView
	
	FETCH NEXT FROM AllView
	INTO @CurrentView
	
END
CLOSE AllView
DEALLOCATE AllView
GO
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

----  BUG_13213  Partner_Id_CimzettKapcsolt mező hozzáadása az EREC_PldIratpeldanyokHistory táblához ---------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Partner_Id_CimzettKapcsolt' AND Object_ID = Object_ID(N'EREC_PldIratPeldanyokHistory'))
BEGIN
PRINT 'BUG_13213  Partner_Id_CimzettKapcsolt mező hozzáadása az EREC_PldIratpeldanyokHistory táblához'
		ALTER TABLE [dbo].[EREC_PldIratPeldanyokHistory] add [Partner_Id_CimzettKapcsolt] UNIQUEIDENTIFIER NULL 
END
GO
----------------------------------------------------------------------------
