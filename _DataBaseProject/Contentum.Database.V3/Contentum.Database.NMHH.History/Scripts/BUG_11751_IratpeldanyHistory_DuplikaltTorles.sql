-- BUG_11751 EREC_PldIratPeldanyokHistory táblából duplikált sorok törlése
-- Minden felhasználónál futtatandó HISTORY adatbázison

 DELETE FROM [EREC_PldIratPeldanyokHistory]
  FROM [EREC_PldIratPeldanyokHistory] pldh
  inner join (SELECT id, ver from EREC_PldIratPeldanyokHistory
								group by id, azonosito, ver 
								having count(id)>1) dupla on dupla.id= pldh.id and dupla.ver=pldh.ver
	and aktiv is null