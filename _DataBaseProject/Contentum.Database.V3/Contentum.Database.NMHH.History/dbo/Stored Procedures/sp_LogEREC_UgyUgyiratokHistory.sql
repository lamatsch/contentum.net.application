﻿create procedure sp_LogEREC_UgyUgyiratokHistory
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from EREC_UgyUgyiratok where Id = @Row_Id;
          declare @UgyUgyirat_Id_Szulo_Ver int;
       -- select @UgyUgyirat_Id_Szulo_Ver = max(Ver) from EREC_UgyUgyiratok where Id in (select UgyUgyirat_Id_Szulo from #tempTable);
              declare @Csoport_Id_Felelos_Ver int;
       -- select @Csoport_Id_Felelos_Ver = max(Ver) from KRT_Csoportok where Id in (select Csoport_Id_Felelos from #tempTable);
              declare @FelhasznaloCsoport_Id_Orzo_Ver int;
       -- select @FelhasznaloCsoport_Id_Orzo_Ver = max(Ver) from KRT_Csoportok where Id in (select FelhasznaloCsoport_Id_Orzo from #tempTable);
              declare @IraIrattariTetel_Id_Ver int;
       -- select @IraIrattariTetel_Id_Ver = max(Ver) from EREC_IraIrattariTetelek where Id in (select IraIrattariTetel_Id from #tempTable);
              declare @IraIktatokonyv_Id_Ver int;
       -- select @IraIktatokonyv_Id_Ver = max(Ver) from EREC_IraIktatoKonyvek where Id in (select IraIktatokonyv_Id from #tempTable);
              declare @Partner_Id_Ugyindito_Ver int;
       -- select @Partner_Id_Ugyindito_Ver = max(Ver) from KRT_Partnerek where Id in (select Partner_Id_Ugyindito from #tempTable);
              declare @FelhasznaloCsoport_Id_Ugyintez_Ver int;
       -- select @FelhasznaloCsoport_Id_Ugyintez_Ver = max(Ver) from KRT_Csoportok where Id in (select FelhasznaloCsoport_Id_Ugyintez from #tempTable);
              declare @Csoport_Id_Felelos_Elozo_Ver int;
       -- select @Csoport_Id_Felelos_Elozo_Ver = max(Ver) from KRT_Csoportok where Id in (select Csoport_Id_Felelos_Elozo from #tempTable);
          
   insert into EREC_UgyUgyiratokHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Foszam     
 ,Sorszam     
 ,Ugyazonosito      
 ,Alkalmazas_Id     
 ,UgyintezesModja     
 ,Hatarido     
 ,SkontrobaDat     
 ,LezarasDat     
 ,IrattarbaKuldDatuma      
 ,IrattarbaVetelDat     
 ,FelhCsoport_Id_IrattariAtvevo     
 ,SelejtezesDat     
 ,FelhCsoport_Id_Selejtezo     
 ,LeveltariAtvevoNeve     
 ,FelhCsoport_Id_Felulvizsgalo     
 ,FelulvizsgalatDat     
 ,IktatoszamKieg     
 ,Targy     
 ,UgyUgyirat_Id_Szulo       
 ,UgyUgyirat_Id_Szulo_Ver     
 ,UgyUgyirat_Id_Kulso     
 ,UgyTipus     
 ,IrattariHely     
 ,SztornirozasDat     
 ,Csoport_Id_Felelos       
 ,Csoport_Id_Felelos_Ver     
 ,FelhasznaloCsoport_Id_Orzo       
 ,FelhasznaloCsoport_Id_Orzo_Ver     
 ,Csoport_Id_Cimzett     
 ,Jelleg     
 ,IraIrattariTetel_Id       
 ,IraIrattariTetel_Id_Ver     
 ,IraIktatokonyv_Id       
 ,IraIktatokonyv_Id_Ver     
 ,SkontroOka     
 ,SkontroVege     
 ,Surgosseg     
 ,SkontrobanOsszesen     
 ,MegorzesiIdoVege     
 ,Partner_Id_Ugyindito       
 ,Partner_Id_Ugyindito_Ver     
 ,NevSTR_Ugyindito     
 ,ElintezesDat     
 ,FelhasznaloCsoport_Id_Ugyintez       
 ,FelhasznaloCsoport_Id_Ugyintez_Ver     
 ,Csoport_Id_Felelos_Elozo       
 ,Csoport_Id_Felelos_Elozo_Ver     
 ,KolcsonKikerDat     
 ,KolcsonKiadDat     
 ,Kolcsonhatarido     
 ,BARCODE     
 ,IratMetadefinicio_Id     
 ,Allapot     
 ,TovabbitasAlattAllapot     
 ,Megjegyzes     
 ,Azonosito     
 ,Fizikai_Kezbesitesi_Allapot     
 ,Kovetkezo_Orzo_Id     
 ,Csoport_Id_Ugyfelelos     
 ,Elektronikus_Kezbesitesi_Allap     
 ,Kovetkezo_Felelos_Id     
 ,UtolsoAlszam     
 ,UtolsoSorszam     
 ,IratSzam     
 ,ElintezesMod     
 ,RegirendszerIktatoszam     
 ,GeneraltTargy     
 ,Cim_Id_Ugyindito      
 ,CimSTR_Ugyindito      
 ,LezarasOka     
 ,FelfuggesztesOka     
 ,IrattarId     
 ,SkontroOka_Kod     
 ,UjOrzesiIdo     
 ,UgyintezesKezdete     
 ,FelfuggesztettNapokSzama     
 ,IntezesiIdo     
 ,IntezesiIdoegyseg     
 ,Ugy_Fajtaja     
 ,SzignaloId     
 ,SzignalasIdeje     
 ,Ver     
 ,Note     
 ,Stat_id     
 ,ErvKezd     
 ,ErvVege     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Zarolo_id     
 ,ZarolasIdo     
 ,Tranz_id     
 ,UIAccessLog_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Foszam          
 ,Sorszam          
 ,Ugyazonosito           
 ,Alkalmazas_Id          
 ,UgyintezesModja          
 ,Hatarido          
 ,SkontrobaDat          
 ,LezarasDat          
 ,IrattarbaKuldDatuma           
 ,IrattarbaVetelDat          
 ,FelhCsoport_Id_IrattariAtvevo          
 ,SelejtezesDat          
 ,FelhCsoport_Id_Selejtezo          
 ,LeveltariAtvevoNeve          
 ,FelhCsoport_Id_Felulvizsgalo          
 ,FelulvizsgalatDat          
 ,IktatoszamKieg          
 ,Targy          
 ,UgyUgyirat_Id_Szulo            
 ,@UgyUgyirat_Id_Szulo_Ver     
 ,UgyUgyirat_Id_Kulso          
 ,UgyTipus          
 ,IrattariHely          
 ,SztornirozasDat          
 ,Csoport_Id_Felelos            
 ,@Csoport_Id_Felelos_Ver     
 ,FelhasznaloCsoport_Id_Orzo            
 ,@FelhasznaloCsoport_Id_Orzo_Ver     
 ,Csoport_Id_Cimzett          
 ,Jelleg          
 ,IraIrattariTetel_Id            
 ,@IraIrattariTetel_Id_Ver     
 ,IraIktatokonyv_Id            
 ,@IraIktatokonyv_Id_Ver     
 ,SkontroOka          
 ,SkontroVege          
 ,Surgosseg          
 ,SkontrobanOsszesen          
 ,MegorzesiIdoVege          
 ,Partner_Id_Ugyindito            
 ,@Partner_Id_Ugyindito_Ver     
 ,NevSTR_Ugyindito          
 ,ElintezesDat          
 ,FelhasznaloCsoport_Id_Ugyintez            
 ,@FelhasznaloCsoport_Id_Ugyintez_Ver     
 ,Csoport_Id_Felelos_Elozo            
 ,@Csoport_Id_Felelos_Elozo_Ver     
 ,KolcsonKikerDat          
 ,KolcsonKiadDat          
 ,Kolcsonhatarido          
 ,BARCODE          
 ,IratMetadefinicio_Id          
 ,Allapot          
 ,TovabbitasAlattAllapot          
 ,Megjegyzes          
 ,Azonosito          
 ,Fizikai_Kezbesitesi_Allapot          
 ,Kovetkezo_Orzo_Id          
 ,Csoport_Id_Ugyfelelos          
 ,Elektronikus_Kezbesitesi_Allap          
 ,Kovetkezo_Felelos_Id          
 ,UtolsoAlszam          
 ,UtolsoSorszam          
 ,IratSzam          
 ,ElintezesMod          
 ,RegirendszerIktatoszam          
 ,GeneraltTargy          
 ,Cim_Id_Ugyindito           
 ,CimSTR_Ugyindito           
 ,LezarasOka          
 ,FelfuggesztesOka          
 ,IrattarId          
 ,SkontroOka_Kod          
 ,UjOrzesiIdo          
 ,UgyintezesKezdete          
 ,FelfuggesztettNapokSzama          
 ,IntezesiIdo          
 ,IntezesiIdoegyseg          
 ,Ugy_Fajtaja          
 ,SzignaloId          
 ,SzignalasIdeje          
 ,Ver          
 ,Note          
 ,Stat_id          
 ,ErvKezd          
 ,ErvVege          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Zarolo_id          
 ,ZarolasIdo          
 ,Tranz_id          
 ,UIAccessLog_id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go