﻿CREATE TABLE [dbo].[EREC_Alairas_Folyamat_TetelekHistory] (
    [HistoryId]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [AlairasFolyamat_Id]    UNIQUEIDENTIFIER NULL,
    [IraIrat_Id]            UNIQUEIDENTIFIER NULL,
    [Csatolmany_Id]         UNIQUEIDENTIFIER NULL,
    [AlairasStatus]         NVARCHAR (64)    NULL,
    [Hiba]                  NVARCHAR (400)   NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_Alairas_Folyamat_TetelekHistory_ID_VER]
    ON [dbo].[EREC_Alairas_Folyamat_TetelekHistory]([Id] ASC, [Ver] ASC);

