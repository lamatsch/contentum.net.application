﻿CREATE TABLE [dbo].[EREC_CsatolmanyokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_CsatolmanyokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [KuldKuldemeny_Id]      UNIQUEIDENTIFIER NULL,
    [IraIrat_Id]            UNIQUEIDENTIFIER NULL,
    [Dokumentum_Id]         UNIQUEIDENTIFIER NULL,
    [Leiras]                NVARCHAR (100)   NULL,
    [Lapszam]               INT              NULL,
    [KapcsolatJelleg]       NVARCHAR (64)    NULL,
    [DokumentumSzerep]      NVARCHAR (64)    NULL,
    [IratAlairoJel]         CHAR (1)         NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_CsatolmanyokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_CsatolmanyokHistory_ID_VER]
    ON [dbo].[EREC_CsatolmanyokHistory]([Id] ASC, [Ver] ASC);

