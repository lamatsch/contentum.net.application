-- =========================================
-- Create History table for EREC_FeladatErtesites
-- =========================================
 /*
IF OBJECT_ID('EREC_FeladatErtesitesHistory', 'U') IS NOT NULL
  DROP TABLE EREC_FeladatErtesitesHistory
GO
 */
CREATE TABLE EREC_FeladatErtesitesHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 FeladatDefinicio_id uniqueidentifier,  
 Felhasznalo_Id uniqueidentifier,  
 Ertesites char(1),  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_EREC_FeladatErtesitesHistory_ID_VER ON EREC_FeladatErtesitesHistory
(
	Id,Ver ASC
)
GO
