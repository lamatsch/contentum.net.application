﻿CREATE TABLE [dbo].[EREC_Hatarid_ObjektumokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_Hatarid_ObjektumokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [HataridosFeladat_Id]   UNIQUEIDENTIFIER NULL,
    [Obj_Id]                UNIQUEIDENTIFIER NULL,
    [ObjTip_Id]             UNIQUEIDENTIFIER NULL,
    [Obj_type]              NVARCHAR (100)   NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_Hatarid_ObjektumokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_Hatarid_ObjektumokHistory_ID_VER]
    ON [dbo].[EREC_Hatarid_ObjektumokHistory]([Id] ASC, [Ver] ASC);

