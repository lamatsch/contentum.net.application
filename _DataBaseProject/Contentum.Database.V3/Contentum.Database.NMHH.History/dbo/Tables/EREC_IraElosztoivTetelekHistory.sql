﻿CREATE TABLE [dbo].[EREC_IraElosztoivTetelekHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_IraElosztoivTetelekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [ElosztoIv_Id]          UNIQUEIDENTIFIER NULL,
    [ElosztoIv_Id_Ver]      INT              NULL,
    [Sorszam]               INT              NULL,
    [Partner_Id]            UNIQUEIDENTIFIER NULL,
    [Partner_Id_Ver]        INT              NULL,
    [Cim_Id]                UNIQUEIDENTIFIER NULL,
    [CimSTR]                NVARCHAR (400)   NULL,
    [NevSTR]                NVARCHAR (400)   NULL,
    [Kuldesmod]             NVARCHAR (64)    NULL,
    [Visszavarolag]         CHAR (1)         NULL,
    [VisszavarasiIdo]       DATETIME         NULL,
    [Vissza_Nap_Mulva]      INT              NULL,
    [AlairoSzerep]          NVARCHAR (64)    NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_IraElosztoivTetelekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_IraElosztoivTetelekHistory_ID_VER]
    ON [dbo].[EREC_IraElosztoivTetelekHistory]([Id] ASC, [Ver] ASC);

