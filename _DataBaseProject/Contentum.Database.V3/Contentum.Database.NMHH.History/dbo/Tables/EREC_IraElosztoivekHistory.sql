﻿CREATE TABLE [dbo].[EREC_IraElosztoivekHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_IraElosztoivekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Org]                   UNIQUEIDENTIFIER NULL,
    [Ev]                    INT              NULL,
    [Fajta]                 NVARCHAR (64)    NULL,
    [Kod]                   NVARCHAR (10)    NULL,
    [NEV]                   NVARCHAR (100)   NULL,
    [Hasznalat]             NVARCHAR (64)    NULL,
    [Csoport_Id_Tulaj]      UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Tulaj_Ver]  INT              NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_IraElosztoivekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_IraElosztoivekHistory_ID_VER]
    ON [dbo].[EREC_IraElosztoivekHistory]([Id] ASC, [Ver] ASC);

