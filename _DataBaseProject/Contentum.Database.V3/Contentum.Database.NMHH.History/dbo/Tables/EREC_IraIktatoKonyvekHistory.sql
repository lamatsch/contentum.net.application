-- =========================================
-- Create History table for EREC_IraIktatoKonyvek
-- =========================================
 /*
IF OBJECT_ID('EREC_IraIktatoKonyvekHistory', 'U') IS NOT NULL
  DROP TABLE EREC_IraIktatoKonyvekHistory
GO
 */
CREATE TABLE EREC_IraIktatoKonyvekHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 Org uniqueidentifier,  
 Ev int,  
 Azonosito Nvarchar(20),  
 DefaultIrattariTetelszam uniqueidentifier,  
 Nev Nvarchar(100),  
 MegkulJelzes Nvarchar(20),  
 Iktatohely Nvarchar(100),  
 KozpontiIktatasJelzo char(1),  
 UtolsoFoszam int,  
 UtolsoSorszam int,  
 Csoport_Id_Olvaso uniqueidentifier,  
 Csoport_Id_Tulaj uniqueidentifier,  
 IktSzamOsztas nvarchar(64),  
 FormatumKod char(1),  
 Titkos char(1),  
 IktatoErkezteto char(1),  
 LezarasDatuma datetime,  
 PostakonyvVevokod Nvarchar(100),  
 PostakonyvMegallapodasAzon Nvarchar(100),  
 EFeladoJegyzekUgyfelAdatok Nvarchar(4000),  
 HitelesExport_Dok_ID uniqueidentifier,  
 Ujranyitando char(1),  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 Statusz char(1),  
 Terjedelem Nvarchar(20),  
 SelejtezesDatuma datetime,  
 KezelesTipusa char(1),  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_EREC_IraIktatoKonyvekHistory_ID_VER ON EREC_IraIktatoKonyvekHistory
(
	Id,Ver ASC
)
GO