﻿CREATE TABLE [dbo].[EREC_IraIrattariTetelekHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_IraIrattariTetelekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Org]                   UNIQUEIDENTIFIER NULL,
    [IrattariTetelszam]     NVARCHAR (20)    NULL,
    [Nev]                   NVARCHAR (400)   NULL,
    [IrattariJel]           NVARCHAR (2)     NULL,
    [MegorzesiIdo]          NVARCHAR (64)    NULL,
    [Idoegyseg]             nvarchar(64)     NULL,
    [Folyo_CM]              NVARCHAR (10)    NULL,
    [AgazatiJel_Id]         UNIQUEIDENTIFIER NULL,
    [AgazatiJel_Id_Ver]     INT              NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    [EvenTuliIktatas]       CHAR (1)         NULL,
    CONSTRAINT [PK_EREC_IraIrattariTetelekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_IraIrattariTetelekHistory_ID_VER]
    ON [dbo].[EREC_IraIrattariTetelekHistory]([Id] ASC, [Ver] ASC);

