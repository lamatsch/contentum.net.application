﻿CREATE TABLE [dbo].[EREC_IraJegyzekTetelekHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_IraJegyzekTetelekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Jegyzek_Id]            UNIQUEIDENTIFIER NULL,
    [Jegyzek_Id_Ver]        INT              NULL,
    [Sorszam]               INT              NULL,
    [Obj_Id]                UNIQUEIDENTIFIER NULL,
    [ObjTip_Id]             UNIQUEIDENTIFIER NULL,
    [Obj_type]              NVARCHAR (100)   NULL,
    [Allapot]               NVARCHAR (64)    NULL,
    [Megjegyzes]            NVARCHAR (400)   NULL,
    [Azonosito]             NVARCHAR (100)   NULL,
    [SztornozasDat]         DATETIME         NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    [AtadasDatuma]          DATETIME         NULL,
    CONSTRAINT [PK_EREC_IraJegyzekTetelekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC) WITH (FILLFACTOR = 85)
);




GO
CREATE NONCLUSTERED INDEX [IX_EREC_IraJegyzekTetelekHistory_ID_VER]
    ON [dbo].[EREC_IraJegyzekTetelekHistory]([Id] ASC, [Ver] ASC);

