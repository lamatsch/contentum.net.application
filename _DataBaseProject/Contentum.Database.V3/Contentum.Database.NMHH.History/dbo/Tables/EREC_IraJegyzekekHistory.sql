﻿CREATE TABLE [dbo].[EREC_IraJegyzekekHistory] (
    [HistoryId]                      UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_IraJegyzekekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]              INT              NULL,
    [HistoryVegrehajto_Id]           UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]          DATETIME         NULL,
    [Id]                             UNIQUEIDENTIFIER NULL,
    [Org]                            UNIQUEIDENTIFIER NULL,
    [Tipus]                          CHAR (1)         NULL,
    [Minosites]                      NVARCHAR (2)     NULL,
    [Csoport_Id_felelos]             UNIQUEIDENTIFIER NULL,
    [FelhasznaloCsoport_Id_Vegrehaj] UNIQUEIDENTIFIER NULL,
    [Partner_Id_LeveltariAtvevo]     UNIQUEIDENTIFIER NULL,
    [LezarasDatuma]                  DATETIME         NULL,
    [SztornirozasDat]                DATETIME         NULL,
    [Nev]                            NVARCHAR (100)   NULL,
    [VegrehajtasDatuma]              DATETIME         NULL,
	Foszam               int                  null,
    Azonosito            Nvarchar(100)        null,
    MegsemmisitesDatuma datetime              null,
	Csoport_Id_FelelosSzervezet uniqueidentifier     null,
	IraIktatokonyv_Id uniqueidentifier     null,
	Allapot							 nvarchar(64)     collate Hungarian_CS_AS null,
    VegrehajtasKezdoDatuma			 datetime         null,
    [Ver]                            INT              NULL,
    [Note]                           NVARCHAR (4000)  NULL,
    [Stat_id]                        UNIQUEIDENTIFIER NULL,
    [ErvKezd]                        DATETIME         NULL,
    [ErvVege]                        DATETIME         NULL,
    [Letrehozo_id]                   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                  DATETIME         NULL,
    [Modosito_id]                    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                   DATETIME         NULL,
    [Zarolo_id]                      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                     DATETIME         NULL,
    [Tranz_id]                       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                 UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_IraJegyzekekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_IraJegyzekekHistory_ID_VER]
    ON [dbo].[EREC_IraJegyzekekHistory]([Id] ASC, [Ver] ASC);

