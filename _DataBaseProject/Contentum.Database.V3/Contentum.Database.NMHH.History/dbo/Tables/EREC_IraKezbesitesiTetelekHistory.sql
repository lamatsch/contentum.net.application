﻿CREATE TABLE [dbo].[EREC_IraKezbesitesiTetelekHistory] (
    [HistoryId]                  UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_IraKezbesitesiTetelekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]          INT              NULL,
    [HistoryVegrehajto_Id]       UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]      DATETIME         NULL,
    [Id]                         UNIQUEIDENTIFIER NULL,
    [KezbesitesFej_Id]           UNIQUEIDENTIFIER NULL,
    [AtveteliFej_Id]             UNIQUEIDENTIFIER NULL,
    [Obj_Id]                     UNIQUEIDENTIFIER NULL,
    [ObjTip_Id]                  UNIQUEIDENTIFIER NULL,
    [Obj_type]                   NVARCHAR (100)   NULL,
    [AtadasDat]                  DATETIME         NULL,
    [AtvetelDat]                 DATETIME         NULL,
    [Megjegyzes]                 NVARCHAR (400)   NULL,
    [SztornirozasDat]            DATETIME         NULL,
    [UtolsoNyomtatas_Id]         UNIQUEIDENTIFIER NULL,
    [UtolsoNyomtatasIdo]         DATETIME         NULL,
    [Felhasznalo_Id_Atado_USER]  UNIQUEIDENTIFIER NULL,
    [Felhasznalo_Id_Atado_LOGIN] UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Cel]             UNIQUEIDENTIFIER NULL,
    [Felhasznalo_Id_AtvevoUser]  UNIQUEIDENTIFIER NULL,
    [Felhasznalo_Id_AtvevoLogin] UNIQUEIDENTIFIER NULL,
    [Allapot]                    CHAR (1)         NULL,
    [Azonosito_szoveges]         NVARCHAR (100)   NULL,
    [UgyintezesModja]            NVARCHAR (64)    NULL,
    [Ver]                        INT              NULL,
    [Note]                       NVARCHAR (4000)  NULL,
    [Stat_id]                    UNIQUEIDENTIFIER NULL,
    [ErvKezd]                    DATETIME         NULL,
    [ErvVege]                    DATETIME         NULL,
    [Letrehozo_id]               UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]              DATETIME         NULL,
    [Modosito_id]                UNIQUEIDENTIFIER NULL,
    [ModositasIdo]               DATETIME         NULL,
    [Zarolo_id]                  UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                 DATETIME         NULL,
    [Tranz_id]                   UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]             UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_IraKezbesitesiTetelekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_IraKezbesitesiTetelekHistory_ID_VER]
    ON [dbo].[EREC_IraKezbesitesiTetelekHistory]([Id] ASC, [Ver] ASC);

