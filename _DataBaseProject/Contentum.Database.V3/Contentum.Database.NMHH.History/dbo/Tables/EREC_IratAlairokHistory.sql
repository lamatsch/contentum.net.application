﻿CREATE TABLE [dbo].[EREC_IratAlairokHistory] (
    [HistoryId]                      UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_IratAlairokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]              INT              NULL,
    [HistoryVegrehajto_Id]           UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]          DATETIME         NULL,
    [Id]                             UNIQUEIDENTIFIER NULL,
    [PldIratPeldany_Id]              UNIQUEIDENTIFIER NULL,
    [Objtip_Id]                      UNIQUEIDENTIFIER NULL,
    [Obj_Id]                         UNIQUEIDENTIFIER NULL,
    [AlairasDatuma]                  DATETIME         NULL,
    [Leiras]                         NVARCHAR (4000)  NULL,
    [AlairoSzerep]                   NVARCHAR (64)    NULL,
    [AlairasSorrend]                 INT              NULL,
    [FelhasznaloCsoport_Id_Alairo]   UNIQUEIDENTIFIER NULL,
    [FelhaszCsoport_Id_Helyettesito] UNIQUEIDENTIFIER NULL,
    [Azonosito]                      NVARCHAR (100)   NULL,
    [AlairasMod]                     NVARCHAR (64)    NULL,
    [AlairasSzabaly_Id]              UNIQUEIDENTIFIER NULL,
    [Allapot]                        NVARCHAR (64)    NULL,
    [Ver]                            INT              NULL,
    [Note]                           NVARCHAR (4000)  NULL,
    [Stat_id]                        UNIQUEIDENTIFIER NULL,
    [ErvKezd]                        DATETIME         NULL,
    [ErvVege]                        DATETIME         NULL,
    [Letrehozo_id]                   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                  DATETIME         NULL,
    [Modosito_id]                    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                   DATETIME         NULL,
    [Zarolo_id]                      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                     DATETIME         NULL,
    [Tranz_id]                       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                 UNIQUEIDENTIFIER NULL,
    [FelhaszCsop_Id_HelyettesAlairo] UNIQUEIDENTIFIER NULL, 
    CONSTRAINT [PK_EREC_IratAlairokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_IratAlairokHistory_ID_VER]
    ON [dbo].[EREC_IratAlairokHistory]([Id] ASC, [Ver] ASC);

