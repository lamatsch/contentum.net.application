﻿CREATE TABLE [dbo].[EREC_IratMetaDefinicioHistory] (
    [HistoryId]                UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_IratMetaDefinicioHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]        INT              NULL,
    [HistoryVegrehajto_Id]     UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]    DATETIME         NULL,
    [Id]                       UNIQUEIDENTIFIER NULL,
    [Org]                      UNIQUEIDENTIFIER NULL,
    [Ugykor_Id]                UNIQUEIDENTIFIER NULL,
    [UgykorKod]                NVARCHAR (10)    NULL,
    [Ugytipus]                 NVARCHAR (64)    NULL,
    [UgytipusNev]              NVARCHAR (400)   NULL,
    [EljarasiSzakasz]          NVARCHAR (64)    NULL,
    [Irattipus]                NVARCHAR (64)    NULL,
    [GeneraltTargy]            NVARCHAR (400)   NULL,
    [Rovidnev]                 NVARCHAR (100)   NULL,
    [UgyFajta]                 NVARCHAR (64)    NULL,
    [UgyiratIntezesiIdo]       INT              NULL,
    [Idoegyseg]                NVARCHAR (64)    NULL,
    [UgyiratIntezesiIdoKotott] CHAR (1)         NULL,
    [UgyiratHataridoKitolas]   CHAR (1)         NULL,
    [Ver]                      INT              NULL,
    [Note]                     NVARCHAR (4000)  NULL,
    [Stat_id]                  UNIQUEIDENTIFIER NULL,
    [ErvKezd]                  DATETIME         NULL,
    [ErvVege]                  DATETIME         NULL,
    [Letrehozo_id]             UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]            DATETIME         NULL,
    [Modosito_id]              UNIQUEIDENTIFIER NULL,
    [ModositasIdo]             DATETIME         NULL,
    [Zarolo_id]                UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]               DATETIME         NULL,
    [Tranz_id]                 UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]           UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_IratMetaDefinicioHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_IratMetaDefinicioHistory_ID_VER]
    ON [dbo].[EREC_IratMetaDefinicioHistory]([Id] ASC, [Ver] ASC);

