﻿CREATE TABLE [dbo].[EREC_Irat_IktatokonyveiHistory] (
    [HistoryId]               UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_Irat_IktatokonyveiHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]       INT              NULL,
    [HistoryVegrehajto_Id]    UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]   DATETIME         NULL,
    [Id]                      UNIQUEIDENTIFIER NULL,
    [IraIktatokonyv_Id]       UNIQUEIDENTIFIER NULL,
    [IraIktatokonyv_Id_Ver]   INT              NULL,
    [Csoport_Id_Iktathat]     UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Iktathat_Ver] INT              NULL,
    [Ver]                     INT              NULL,
    [Note]                    NVARCHAR (4000)  NULL,
    [Stat_id]                 UNIQUEIDENTIFIER NULL,
    [ErvKezd]                 DATETIME         NULL,
    [ErvVege]                 DATETIME         NULL,
    [Letrehozo_id]            UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]           DATETIME         NULL,
    [Modosito_id]             UNIQUEIDENTIFIER NULL,
    [ModositasIdo]            DATETIME         NULL,
    [Zarolo_id]               UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]              DATETIME         NULL,
    [Tranz_id]                UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]          UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_Irat_IktatokonyveiHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_Irat_IktatokonyveiHistory_ID_VER]
    ON [dbo].[EREC_Irat_IktatokonyveiHistory]([Id] ASC, [Ver] ASC);

