﻿CREATE TABLE [dbo].[EREC_KuldBekuldokHistory] (
    [HistoryId]                 UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_KuldBekuldokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]         INT              NULL,
    [HistoryVegrehajto_Id]      UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]     DATETIME         NULL,
    [Id]                        UNIQUEIDENTIFIER NULL,
    [KuldKuldemeny_Id]          UNIQUEIDENTIFIER NULL,
    [KuldKuldemeny_Id_Ver]      INT              NULL,
    [Partner_Id_Bekuldo]        UNIQUEIDENTIFIER NULL,
    [Partner_Id_Bekuldo_Ver]    INT              NULL,
    [PartnerCim_Id_Bekuldo]     UNIQUEIDENTIFIER NULL,
    [PartnerCim_Id_Bekuldo_Ver] INT              NULL,
    [NevSTR]                    NVARCHAR (400)   NULL,
    [CimSTR]                    NVARCHAR (400)   NULL,
    [Ver]                       INT              NULL,
    [Note]                      NVARCHAR (4000)  NULL,
    [Stat_id]                   UNIQUEIDENTIFIER NULL,
    [ErvKezd]                   DATETIME         NULL,
    [ErvVege]                   DATETIME         NULL,
    [Letrehozo_id]              UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]             DATETIME         NULL,
    [Modosito_id]               UNIQUEIDENTIFIER NULL,
    [ModositasIdo]              DATETIME         NULL,
    [Zarolo_id]                 UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                DATETIME         NULL,
    [Tranz_id]                  UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]            UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_KuldBekuldokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_KuldBekuldokHistory_ID_VER]
    ON [dbo].[EREC_KuldBekuldokHistory]([Id] ASC, [Ver] ASC);

