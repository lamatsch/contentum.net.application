﻿CREATE TABLE [dbo].[EREC_KuldDokumentumokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_KuldDokumentumokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Dokumentum_Id]         UNIQUEIDENTIFIER NULL,
    [KuldKuldemeny_Id]      UNIQUEIDENTIFIER NULL,
    [KuldKuldemeny_Id_Ver]  INT              NULL,
    [Leiras]                NVARCHAR (100)   NULL,
    [Lapszam]               INT              NULL,
    [BarCode]               NVARCHAR (100)   NULL,
    [Forras]                NVARCHAR (64)    NULL,
    [Formatum]              NVARCHAR (64)    NULL,
    [Vonalkodozas]          CHAR (1)         NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    [DokumentumSzerep]      NVARCHAR (64)    NULL,
    CONSTRAINT [PK_EREC_KuldDokumentumokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_KuldDokumentumokHistory_ID_VER]
    ON [dbo].[EREC_KuldDokumentumokHistory]([Id] ASC, [Ver] ASC);

