﻿CREATE TABLE [dbo].[EREC_KuldKapcsolatokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_KuldKapcsolatokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [KapcsolatTipus]        NVARCHAR (64)    NULL,
    [Leiras]                NVARCHAR (100)   NULL,
    [Kezi]                  CHAR (1)         NULL,
    [Kuld_Kuld_Beepul]      UNIQUEIDENTIFIER NULL,
    [Kuld_Kuld_Beepul_Ver]  INT              NULL,
    [Kuld_Kuld_Felepul]     UNIQUEIDENTIFIER NULL,
    [Kuld_Kuld_Felepul_Ver] INT              NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_KuldKapcsolatokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_KuldKapcsolatokHistory_ID_VER]
    ON [dbo].[EREC_KuldKapcsolatokHistory]([Id] ASC, [Ver] ASC);

