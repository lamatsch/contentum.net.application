﻿CREATE TABLE [dbo].[EREC_KuldKezFeljegyzesekHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_KuldKezFeljegyzesekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [KuldKuldemeny_Id]      UNIQUEIDENTIFIER NULL,
    [KuldKuldemeny_Id_Ver]  INT              NULL,
    [KezelesTipus]          NVARCHAR (64)    NULL,
    [Leiras]                NVARCHAR (400)   NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_KuldKezFeljegyzesekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_KuldKezFeljegyzesekHistory_ID_VER]
    ON [dbo].[EREC_KuldKezFeljegyzesekHistory]([Id] ASC, [Ver] ASC);

