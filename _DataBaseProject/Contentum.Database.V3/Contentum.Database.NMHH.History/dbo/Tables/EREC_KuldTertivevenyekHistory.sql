﻿CREATE TABLE [dbo].[EREC_KuldTertivevenyekHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_KuldTertivevenyekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Kuldemeny_Id]          UNIQUEIDENTIFIER NULL,
    [Kuldemeny_Id_Ver]      INT              NULL,
    [Ragszam]               NVARCHAR (400)   NULL,
    [TertivisszaKod]        NVARCHAR (64)    NULL,
    [TertivisszaDat]        DATETIME         NULL,
    [AtvevoSzemely]         NVARCHAR (400)   NULL,
    [BarCode]               NVARCHAR (100)   NULL,
    [Allapot]               NVARCHAR (64)    NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    [AtvetelDat]            DATETIME         NULL,
    [KezbVelelemBeallta]    CHAR (1)         NULL,
    [KezbVelelemDatuma]     DATETIME         NULL,
    CONSTRAINT [PK_EREC_KuldTertivevenyekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_KuldTertivevenyekHistory_ID_VER]
    ON [dbo].[EREC_KuldTertivevenyekHistory]([Id] ASC, [Ver] ASC);

