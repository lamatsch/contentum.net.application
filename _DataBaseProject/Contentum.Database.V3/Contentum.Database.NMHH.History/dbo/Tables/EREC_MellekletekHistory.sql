﻿CREATE TABLE [dbo].[EREC_MellekletekHistory] (
    [HistoryId]                 UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_MellekletekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]         INT              NULL,
    [HistoryVegrehajto_Id]      UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]     DATETIME         NULL,
    [Id]                        UNIQUEIDENTIFIER NULL,
    [KuldKuldemeny_Id]          UNIQUEIDENTIFIER NULL,
    [IraIrat_Id]                UNIQUEIDENTIFIER NULL,
    [AdathordozoTipus]          NVARCHAR (64)    NULL,
    [Megjegyzes]                NVARCHAR (400)   NULL,
    [SztornirozasDat]           DATETIME         NULL,
    [MennyisegiEgyseg]          NVARCHAR (64)    NULL,
    [Mennyiseg]                 INT              NULL,
    [BarCode]                   NVARCHAR (100)   NULL,
    [Ver]                       INT              NULL,
    [Note]                      NVARCHAR (4000)  NULL,
    [Stat_id]                   UNIQUEIDENTIFIER NULL,
    [ErvKezd]                   DATETIME         NULL,
    [ErvVege]                   DATETIME         NULL,
    [Letrehozo_id]              UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]             DATETIME         NULL,
    [Modosito_id]               UNIQUEIDENTIFIER NULL,
    [ModositasIdo]              DATETIME         NULL,
    [Zarolo_id]                 UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                DATETIME         NULL,
    [Tranz_id]                  UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]            UNIQUEIDENTIFIER NULL,
    [MellekletAdathordozoTipus] NVARCHAR (64)    NULL,
    CONSTRAINT [PK_EREC_MellekletekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_MellekletekHistory_ID_VER]
    ON [dbo].[EREC_MellekletekHistory]([Id] ASC, [Ver] ASC);

