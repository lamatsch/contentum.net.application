﻿CREATE TABLE [dbo].[EREC_Obj_MetaAdataiHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_Obj_MetaAdataiHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Obj_MetaDefinicio_Id]  UNIQUEIDENTIFIER NULL,
    [Targyszavak_Id]        UNIQUEIDENTIFIER NULL,
    [AlapertelmezettErtek]  NVARCHAR (100)   NULL,
    [Sorszam]               INT              NULL,
    [Opcionalis]            CHAR (1)         NULL,
    [Ismetlodo]             CHAR (1)         NULL,
    [Funkcio]               NVARCHAR (100)   NULL,
    [SPSSzinkronizalt]      CHAR (1)         NULL,
    [SPS_Field_Id]          UNIQUEIDENTIFIER NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_Id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_Id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_Id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_Id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_Id]        UNIQUEIDENTIFIER NULL,
    [ControlTypeSource]     NVARCHAR (64)    NULL,
    [ControlTypeDataSource] NVARCHAR (4000)  NULL,
    CONSTRAINT [PK_EREC_Obj_MetaAdataiHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_Obj_MetaAdataiHistory_ID_VER]
    ON [dbo].[EREC_Obj_MetaAdataiHistory]([Id] ASC, [Ver] ASC);

