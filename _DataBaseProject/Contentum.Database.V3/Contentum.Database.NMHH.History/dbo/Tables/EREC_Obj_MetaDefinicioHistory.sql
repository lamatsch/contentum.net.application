﻿CREATE TABLE [dbo].[EREC_Obj_MetaDefinicioHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_Obj_MetaDefinicioHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Org]                   UNIQUEIDENTIFIER NULL,
    [Objtip_Id]             UNIQUEIDENTIFIER NULL,
    [Objtip_Id_Column]      UNIQUEIDENTIFIER NULL,
    [ColumnValue]           NVARCHAR (100)   NULL,
    [DefinicioTipus]        NVARCHAR (64)    NULL,
    [Felettes_Obj_Meta]     UNIQUEIDENTIFIER NULL,
    [MetaXSD]               XML              NULL,
    [ImportXML]             XML              NULL,
    [EgyebXML]              XML              NULL,
    [ContentType]           NVARCHAR (100)   NULL,
    [SPSSzinkronizalt]      CHAR (1)         NULL,
    [SPS_CTT_Id]            NVARCHAR (400)   NULL,
    [WorkFlowVezerles]      CHAR (1)         NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_Obj_MetaDefinicioHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_Obj_MetaDefinicioHistory_ID_VER]
    ON [dbo].[EREC_Obj_MetaDefinicioHistory]([Id] ASC, [Ver] ASC);

