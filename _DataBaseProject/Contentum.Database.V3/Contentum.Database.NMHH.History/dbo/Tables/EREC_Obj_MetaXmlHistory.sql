﻿CREATE TABLE [dbo].[EREC_Obj_MetaXmlHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_Obj_MetaXmlHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [ObjTip_Id]             UNIQUEIDENTIFIER NULL,
    [Kodertek]              NVARCHAR (64)    NULL,
    [IratMetadefinicio_Id]  UNIQUEIDENTIFIER NULL,
    [Meta_XML]              XML              NULL,
    [Meta_XML_Ervenyes]     CHAR (1)         NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_Id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_Id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_Id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_Id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_Id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_Obj_MetaXmlHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_Obj_MetaXmlHistory_ID_VER]
    ON [dbo].[EREC_Obj_MetaXmlHistory]([Id] ASC, [Ver] ASC);

