﻿CREATE TABLE [dbo].[EREC_ObjektumTargyszavaiHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_ObjektumTargyszavaiHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Targyszo_Id]           UNIQUEIDENTIFIER NULL,
    [Obj_Metaadatai_Id]     UNIQUEIDENTIFIER NULL,
    [Obj_Id]                UNIQUEIDENTIFIER NULL,
    [ObjTip_Id]             UNIQUEIDENTIFIER NULL,
    [Targyszo]              NVARCHAR (100)   NULL,
    [Forras]                NVARCHAR (64)    NULL,
    [SPSSzinkronizalt]      CHAR (1)         NULL,
    [Ertek]                 NVARCHAR (MAX)   NULL,
    [Sorszam]               INT              NULL,
    [Targyszo_XML]          XML              NULL,
    [Targyszo_XML_Ervenyes] CHAR (1)         NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_ObjektumTargyszavaiHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_ObjektumTargyszavaiHistory_ID_VER]
    ON [dbo].[EREC_ObjektumTargyszavaiHistory]([Id] ASC, [Ver] ASC);

