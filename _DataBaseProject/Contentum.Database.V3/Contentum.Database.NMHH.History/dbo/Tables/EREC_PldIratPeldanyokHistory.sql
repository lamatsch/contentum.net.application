﻿CREATE TABLE [dbo].[EREC_PldIratPeldanyokHistory] (
    [HistoryId]                      UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_PldIratPeldanyokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]              INT              NULL,
    [HistoryVegrehajto_Id]           UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]          DATETIME         NULL,
    [Id]                             UNIQUEIDENTIFIER NULL,
    [IraIrat_Id]                     UNIQUEIDENTIFIER NULL,
    [IraIrat_Id_Ver]                 INT              NULL,
    [Sorszam]                        INT              NULL,
    [SztornirozasDat]                DATETIME         NULL,
    [AtvetelDatuma]                  DATETIME         NULL,
    [Eredet]                         NVARCHAR (64)    NULL,
    [KuldesMod]                      NVARCHAR (64)    NULL,
    [Ragszam]                        NVARCHAR (100)   NULL,
    [UgyintezesModja]                NVARCHAR (64)    NULL,
    [VisszaerkezesiHatarido]         DATETIME         NULL,
    [Visszavarolag]                  NVARCHAR (64)    NULL,
    [VisszaerkezesDatuma]            DATETIME         NULL,
    [Cim_id_Cimzett]                 UNIQUEIDENTIFIER NULL,
    [Partner_Id_Cimzett]             UNIQUEIDENTIFIER NULL,
	[Partner_Id_CimzettKapcsolt]     UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Felelos]             UNIQUEIDENTIFIER NULL,
    [FelhasznaloCsoport_Id_Orzo]     UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Felelos_Elozo]       UNIQUEIDENTIFIER NULL,
    [CimSTR_Cimzett]                 NVARCHAR (400)   NULL,
    [NevSTR_Cimzett]                 NVARCHAR (400)   NULL,
    [Tovabbito]                      NVARCHAR (64)    NULL,
    [IraIrat_Id_Kapcsolt]            UNIQUEIDENTIFIER NULL,
    [IrattariHely]                   NVARCHAR (100)   NULL,
    [Gener_Id]                       UNIQUEIDENTIFIER NULL,
    [PostazasDatuma]                 DATETIME         NULL,
    [BarCode]                        NVARCHAR (100)   NULL,
    [Allapot]                        NVARCHAR (64)    NULL,
    [Kovetkezo_Felelos_Id]           UNIQUEIDENTIFIER NULL,
    [Elektronikus_Kezbesitesi_Allap] NVARCHAR (64)    NULL,
    [Kovetkezo_Orzo_Id]              UNIQUEIDENTIFIER NULL,
    [Fizikai_Kezbesitesi_Allapot]    NVARCHAR (64)    NULL,
    [TovabbitasAlattAllapot]         NVARCHAR (64)    NULL,
    [PostazasAllapot]                NVARCHAR (64)    NULL,
    [ValaszElektronikus]             CHAR (1)         NULL,
	SelejtezesDat        datetime             null,
   FelhCsoport_Id_Selejtezo uniqueidentifier     null,
   LeveltariAtvevoNeve  Nvarchar(100)        null,
    [Ver]                            INT              NULL,
    [Note]                           NVARCHAR (4000)  NULL,
    [Stat_id]                        UNIQUEIDENTIFIER NULL,
    [ErvKezd]                        DATETIME         NULL,
    [ErvVege]                        DATETIME         NULL,
    [Letrehozo_id]                   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                  DATETIME         NULL,
    [Modosito_id]                    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                   DATETIME         NULL,
    [Zarolo_id]                      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                     DATETIME         NULL,
    [Tranz_id]                       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                 UNIQUEIDENTIFIER NULL,
    [UgyUgyirat_Id_Kulso]            UNIQUEIDENTIFIER NULL,
    [Azonosito]                      NVARCHAR (100)   NULL,
    [AKTIV]                          INT              NULL,
	[IratPeldanyMegsemmisitve] CHAR NULL, 
    [IratPeldanyMegsemmisitesDatuma] DATETIME NULL, 
	IrattarId uniqueidentifier,  
    CONSTRAINT [PK_EREC_PldIratPeldanyokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_PldIratPeldanyokHistory_ID_VER]
    ON [dbo].[EREC_PldIratPeldanyokHistory]([Id] ASC, [Ver] ASC);

