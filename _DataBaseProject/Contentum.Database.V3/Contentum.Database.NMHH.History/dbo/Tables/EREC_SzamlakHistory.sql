﻿CREATE TABLE [dbo].[EREC_SzamlakHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_SzamlakHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [KuldKuldemeny_Id]      UNIQUEIDENTIFIER NULL,
    [Dokumentum_Id]         UNIQUEIDENTIFIER NULL,
    [Partner_Id_Szallito]   UNIQUEIDENTIFIER NULL,
    [Cim_Id_Szallito]       UNIQUEIDENTIFIER NULL,
	NevSTR_Szallito Nvarchar(400),  
    CimSTR_Szallito Nvarchar(400),
    [Bankszamlaszam_Id]     UNIQUEIDENTIFIER NULL,
    [SzamlaSorszam]         VARCHAR (50)     NULL,
    [FizetesiMod]           VARCHAR (4)      NULL,
    [TeljesitesDatuma]      SMALLDATETIME    NULL,
    [BizonylatDatuma]       SMALLDATETIME    NULL,
    [FizetesiHatarido]      SMALLDATETIME    NULL,
    [DevizaKod]             VARCHAR (3)      NULL,
    [VisszakuldesDatuma]    SMALLDATETIME    NULL,
    [EllenorzoOsszeg]       FLOAT (53)       NULL,
    [PIRAllapot]            CHAR (1)         NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    [VevoBesorolas]         NVARCHAR (20)    NULL,
    CONSTRAINT [PK_EREC_SzamlakHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_SzamlakHistory_ID_VER]
    ON [dbo].[EREC_SzamlakHistory]([Id] ASC, [Ver] ASC);

