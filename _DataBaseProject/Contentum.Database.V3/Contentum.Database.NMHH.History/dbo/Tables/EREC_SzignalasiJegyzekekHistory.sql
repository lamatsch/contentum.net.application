﻿CREATE TABLE [dbo].[EREC_SzignalasiJegyzekekHistory] (
    [HistoryId]               UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_SzignalasiJegyzekekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]       INT              NULL,
    [HistoryVegrehajto_Id]    UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]   DATETIME         NULL,
    [Id]                      UNIQUEIDENTIFIER NULL,
    [UgykorTargykor_Id]       UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Felelos]      UNIQUEIDENTIFIER NULL,
    [SzignalasTipusa]         NVARCHAR (64)    NULL,
    [SzignalasiKotelezettseg] NVARCHAR (64)    NULL,
    [Ver]                     INT              NULL,
    [Note]                    NVARCHAR (4000)  NULL,
    [Stat_id]                 UNIQUEIDENTIFIER NULL,
    [ErvKezd]                 DATETIME         NULL,
    [ErvVege]                 DATETIME         NULL,
    [Letrehozo_id]            UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]           DATETIME         NULL,
    [Modosito_id]             UNIQUEIDENTIFIER NULL,
    [ModositasIdo]            DATETIME         NULL,
    [Zarolo_id]               UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]              DATETIME         NULL,
    [Tranz_id]                UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]          UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_SzignalasiJegyzekekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_SzignalasiJegyzekekHistory_ID_VER]
    ON [dbo].[EREC_SzignalasiJegyzekekHistory]([Id] ASC, [Ver] ASC);

