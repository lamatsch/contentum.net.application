﻿CREATE TABLE [dbo].[EREC_TargySzavakHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_TargySzavakHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Org]                   UNIQUEIDENTIFIER NULL,
    [Tipus]                 CHAR (1)         NULL,
    [TargySzavak]           NVARCHAR (100)   NULL,
    [AlapertelmezettErtek]  NVARCHAR (100)   NULL,
    [BelsoAzonosito]        NVARCHAR (100)   NULL,
    [SPSSzinkronizalt]      CHAR (1)         NULL,
    [SPS_Field_Id]          UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Tulaj]      UNIQUEIDENTIFIER NULL,
    [RegExp]                NVARCHAR (4000)  NULL,
    [ToolTip]               NVARCHAR (400)   NULL,
    [XSD]                   XML              NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    [ControlTypeSource]     NVARCHAR (64)    NULL,
    [ControlTypeDataSource] NVARCHAR (4000)  NULL,
	[Targyszo_Id_Parent]    UNIQUEIDENTIFIER NULL
    CONSTRAINT [PK_EREC_TargySzavakHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_TargySzavakHistory_ID_VER]
    ON [dbo].[EREC_TargySzavakHistory]([Id] ASC, [Ver] ASC);

