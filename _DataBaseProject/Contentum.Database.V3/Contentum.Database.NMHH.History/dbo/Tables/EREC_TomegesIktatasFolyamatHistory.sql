-- =========================================
-- Create History table for EREC_TomegesIktatasFolyamat
-- =========================================
 /*
IF OBJECT_ID('EREC_TomegesIktatasFolyamatHistory', 'U') IS NOT NULL
  DROP TABLE EREC_TomegesIktatasFolyamatHistory
GO
 */
CREATE TABLE EREC_TomegesIktatasFolyamatHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 Forras Nvarchar(100),  
 Forras_Azonosito Nvarchar(100),  
 Prioritas nvarchar(64),  
 RQ_Dokumentum_Id uniqueidentifier,  
 RS_Dokumentum_Id uniqueidentifier,
 Allapot nvarchar(64),  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_EREC_TomegesIktatasFolyamatHistory_ID_VER ON EREC_TomegesIktatasFolyamatHistory
(
	Id,Ver ASC
)
GO