-- =========================================
-- Create History table for EREC_TomegesIktatasTetelek
-- =========================================
 /*
IF OBJECT_ID('EREC_TomegesIktatasTetelekHistory', 'U') IS NOT NULL
  DROP TABLE EREC_TomegesIktatasTetelekHistory
GO
 */
CREATE TABLE EREC_TomegesIktatasTetelekHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 Folyamat_Id uniqueidentifier,  
 Forras_Azonosito Nvarchar(100),  
 IktatasTipus int,  
 Alszamra int,  
 Iktatokonyv_Id uniqueidentifier,  
 Ugyirat_Id uniqueidentifier,  
 Kuldemeny_Id uniqueidentifier,  
 ExecParam Nvarchar(Max),  
 EREC_UgyUgyiratok Nvarchar(Max),  
 EREC_UgyUgyiratdarabok Nvarchar(Max),  
 EREC_IraIratok Nvarchar(Max),  
 EREC_PldIratPeldanyok Nvarchar(Max),  
 EREC_HataridosFeladatok Nvarchar(Max),  
 EREC_KuldKuldemenyek Nvarchar(Max),  
 IktatasiParameterek Nvarchar(Max),  
 ErkeztetesParameterek Nvarchar(Max),  
 Dokumentumok Nvarchar(Max),  
 Azonosito Nvarchar(100),  
 Allapot nvarchar(64),  
 Irat_Id uniqueidentifier,  
 Hiba Nvarchar(4000),  
 Expedialas int,  
 TobbIratEgyKuldemenybe int,  
 KuldemenyAtadas uniqueidentifier,  
 HatosagiStatAdat Nvarchar(Max),  
 EREC_IratAlairok Nvarchar(Max),  
 Lezarhato int,  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_EREC_TomegesIktatasTetelekHistory_ID_VER ON EREC_TomegesIktatasTetelekHistory
(
	Id,Ver ASC
)
GO