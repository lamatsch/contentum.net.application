﻿CREATE TABLE [dbo].[EREC_UgyUgyiratdarabokHistory] (
    [HistoryId]                      UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_UgyUgyiratdarabokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]              INT              NULL,
    [HistoryVegrehajto_Id]           UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]          DATETIME         NULL,
    [Id]                             UNIQUEIDENTIFIER NULL,
    [UgyUgyirat_Id]                  UNIQUEIDENTIFIER NULL,
    [UgyUgyirat_Id_Ver]              INT              NULL,
    [EljarasiSzakasz]                NVARCHAR (64)    NULL,
    [Sorszam]                        INT              NULL,
    [Azonosito]                      NVARCHAR (100)   NULL,
    [Hatarido]                       DATETIME         NULL,
    [FelhasznaloCsoport_Id_Ugyintez] UNIQUEIDENTIFIER NULL,
    [ElintezesDat]                   DATETIME         NULL,
    [LezarasDat]                     DATETIME         NULL,
    [ElintezesMod]                   NVARCHAR (64)    NULL,
    [LezarasOka]                     NVARCHAR (64)    NULL,
    [Leiras]                         NVARCHAR (4000)  NULL,
    [UgyUgyirat_Id_Elozo]            UNIQUEIDENTIFIER NULL,
    [IraIktatokonyv_Id]              UNIQUEIDENTIFIER NULL,
    [IraIktatokonyv_Id_Ver]          INT              NULL,
    [Foszam]                         INT              NULL,
    [UtolsoAlszam]                   INT              NULL,
    [Csoport_Id_Felelos]             UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Felelos_Elozo]       UNIQUEIDENTIFIER NULL,
    [IratMetadefinicio_Id]           UNIQUEIDENTIFIER NULL,
    [Allapot]                        NVARCHAR (64)    NULL,
    [Ver]                            INT              NULL,
    [Note]                           NVARCHAR (4000)  NULL,
    [Stat_id]                        UNIQUEIDENTIFIER NULL,
    [ErvKezd]                        DATETIME         NULL,
    [ErvVege]                        DATETIME         NULL,
    [Letrehozo_id]                   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                  DATETIME         NULL,
    [Modosito_id]                    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                   DATETIME         NULL,
    [Zarolo_id]                      UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                     DATETIME         NULL,
    [Tranz_id]                       UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                 UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_UgyUgyiratdarabokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_UgyUgyiratdarabokHistory_ID_VER]
    ON [dbo].[EREC_UgyUgyiratdarabokHistory]([Id] ASC, [Ver] ASC);

