/*
-- =========================================
-- Create History table for EREC_UgyUgyiratok
-- =========================================
 
IF OBJECT_ID('EREC_UgyUgyiratokHistory', 'U') IS NOT NULL
  DROP TABLE EREC_UgyUgyiratokHistory
GO
 */
CREATE TABLE EREC_UgyUgyiratokHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 Foszam int,  
 Sorszam int,  
 Ugyazonosito  Nvarchar(100),  
 Alkalmazas_Id uniqueidentifier,  
 UgyintezesModja nvarchar(64),  
 Hatarido datetime,  
 SkontrobaDat datetime,  
 LezarasDat datetime,  
 IrattarbaKuldDatuma  datetime,  
 IrattarbaVetelDat datetime,  
 FelhCsoport_Id_IrattariAtvevo uniqueidentifier,  
 SelejtezesDat datetime,  
 FelhCsoport_Id_Selejtezo uniqueidentifier,  
 LeveltariAtvevoNeve Nvarchar(100),  
 FelhCsoport_Id_Felulvizsgalo uniqueidentifier,  
 FelulvizsgalatDat datetime,  
 IktatoszamKieg Nvarchar(2),  
 Targy Nvarchar(4000),  
  UgyUgyirat_Id_Szulo uniqueidentifier,
  
  UgyUgyirat_Id_Szulo_Ver int,  
 UgyUgyirat_Id_Kulso uniqueidentifier,  
 UgyTipus nvarchar(64),  
 IrattariHely Nvarchar(100),  
 SztornirozasDat datetime,  
  Csoport_Id_Felelos uniqueidentifier,
  
  Csoport_Id_Felelos_Ver int,  
  FelhasznaloCsoport_Id_Orzo uniqueidentifier,
  
  FelhasznaloCsoport_Id_Orzo_Ver int,  
 Csoport_Id_Cimzett uniqueidentifier,  
 Jelleg nvarchar(64),  
  IraIrattariTetel_Id uniqueidentifier,
  
  IraIrattariTetel_Id_Ver int,  
  IraIktatokonyv_Id uniqueidentifier,
  
  IraIktatokonyv_Id_Ver int,  
 SkontroOka Nvarchar(400),  
 SkontroVege datetime,  
 Surgosseg nvarchar(64),  
 SkontrobanOsszesen int,  
 MegorzesiIdoVege datetime,  
  Partner_Id_Ugyindito uniqueidentifier,
  
  Partner_Id_Ugyindito_Ver int,  
 NevSTR_Ugyindito Nvarchar(400),  
 ElintezesDat datetime,  
  FelhasznaloCsoport_Id_Ugyintez uniqueidentifier,
  
  FelhasznaloCsoport_Id_Ugyintez_Ver int,  
  Csoport_Id_Felelos_Elozo uniqueidentifier,
  
  Csoport_Id_Felelos_Elozo_Ver int,  
 KolcsonKikerDat datetime,  
 KolcsonKiadDat datetime,  
 Kolcsonhatarido datetime,  
 BARCODE Nvarchar(20),  
 IratMetadefinicio_Id uniqueidentifier,  
 Allapot nvarchar(64),  
 TovabbitasAlattAllapot nvarchar(64),  
 Megjegyzes Nvarchar(400),  
 Azonosito Nvarchar(100),  
 Fizikai_Kezbesitesi_Allapot nvarchar(64),  
 Kovetkezo_Orzo_Id uniqueidentifier,  
 Csoport_Id_Ugyfelelos uniqueidentifier,  
 Elektronikus_Kezbesitesi_Allap nvarchar(64),  
 Kovetkezo_Felelos_Id uniqueidentifier,  
 UtolsoAlszam int,  
 UtolsoSorszam int,  
 IratSzam int,  
 ElintezesMod nvarchar(64),  
 RegirendszerIktatoszam Nvarchar(100),  
 GeneraltTargy Nvarchar(400),  
 Cim_Id_Ugyindito  uniqueidentifier,  
 CimSTR_Ugyindito  Nvarchar(400),  
 LezarasOka nvarchar(64),  
 FelfuggesztesOka Nvarchar(400),  
 IrattarId uniqueidentifier,  
 SkontroOka_Kod nvarchar(64),  
 UjOrzesiIdo int,  
 UjOrzesiIdoIdoegyseg nvarchar(64),  
 UgyintezesKezdete datetime,  
 FelfuggesztettNapokSzama int,  
 IntezesiIdo nvarchar(64),  
 IntezesiIdoegyseg nvarchar(64),  
 Ugy_Fajtaja nvarchar(64),  
 SzignaloId uniqueidentifier,  
 SzignalasIdeje datetime,  
 ElteltIdo nvarchar(64),  
 ElteltIdoIdoEgyseg nvarchar(64),  
 ElteltidoAllapot int,  
 SakkoraAllapot nvarchar(64),  
 HatralevoNapok int,  
 HatralevoMunkaNapok int,  
 ElozoAllapot nvarchar(64),   
 Aktiv int,  
 IrattarHelyfoglalas float,  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 [ElteltIdoUtolsoModositas] DATETIME NULL, 
    PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_EREC_UgyUgyiratokHistory_ID_VER ON EREC_UgyUgyiratokHistory
(
	Id,Ver ASC
)
GO