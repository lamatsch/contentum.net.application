﻿CREATE TABLE [dbo].[EREC_UgyiratObjKapcsolatokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_UgyiratObjKapcsolatokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [KapcsolatTipus]        NVARCHAR (64)    NULL,
    [Leiras]                NVARCHAR (100)   NULL,
    [Kezi]                  CHAR (1)         NULL,
    [Obj_Id_Elozmeny]       UNIQUEIDENTIFIER NULL,
    [Obj_Tip_Id_Elozmeny]   UNIQUEIDENTIFIER NULL,
    [Obj_Type_Elozmeny]     NVARCHAR (100)   NULL,
    [Obj_Id_Kapcsolt]       UNIQUEIDENTIFIER NULL,
    [Obj_Tip_Id_Kapcsolt]   UNIQUEIDENTIFIER NULL,
    [Obj_Type_Kapcsolt]     NVARCHAR (100)   NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_UgyiratObjKapcsolatokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_UgyiratObjKapcsolatokHistory_ID_VER]
    ON [dbo].[EREC_UgyiratObjKapcsolatokHistory]([Id] ASC, [Ver] ASC);

