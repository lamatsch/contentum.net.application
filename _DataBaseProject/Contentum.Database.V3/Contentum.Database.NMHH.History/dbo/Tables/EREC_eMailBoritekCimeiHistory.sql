﻿CREATE TABLE [dbo].[EREC_eMailBoritekCimeiHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_eMailBoritekCimeiHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [eMailBoritek_Id]       UNIQUEIDENTIFIER NULL,
    [eMailBoritek_Id_Ver]   INT              NULL,
    [Sorszam]               INT              NULL,
    [Tipus]                 NVARCHAR (64)    NULL,
    [MailCim]               NVARCHAR (100)   NULL,
    [Partner_Id]            UNIQUEIDENTIFIER NULL,
    [Partner_Id_Ver]        INT              NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_eMailBoritekCimeiHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_eMailBoritekCimeiHistory_ID_VER]
    ON [dbo].[EREC_eMailBoritekCimeiHistory]([Id] ASC, [Ver] ASC);

