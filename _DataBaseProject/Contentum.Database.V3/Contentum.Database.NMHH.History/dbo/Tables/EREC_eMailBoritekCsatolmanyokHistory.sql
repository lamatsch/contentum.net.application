﻿CREATE TABLE [dbo].[EREC_eMailBoritekCsatolmanyokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_eMailBoritekCsatolmanyokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [eMailBoritek_Id]       UNIQUEIDENTIFIER NULL,
    [eMailBoritek_Id_Ver]   INT              NULL,
    [Dokumentum_Id]         UNIQUEIDENTIFIER NULL,
    [Dokumentum_Id_Ver]     INT              NULL,
    [Nev]                   NVARCHAR (4000)  NULL,
    [Tomoritve]             CHAR (1)         NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_eMailBoritekCsatolmanyokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_eMailBoritekCsatolmanyokHistory_ID_VER]
    ON [dbo].[EREC_eMailBoritekCsatolmanyokHistory]([Id] ASC, [Ver] ASC);

