﻿CREATE TABLE [dbo].[EREC_eMailBoritekokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_eMailBoritekokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Felado]                NVARCHAR (4000)  NULL,
    [Cimzett]               NVARCHAR (4000)  NULL,
    [Targy]                 NVARCHAR (4000)  NULL,
    [FeladasDatuma]         DATETIME         NULL,
    [ErkezesDatuma]         DATETIME         NULL,
    [Fontossag]             NVARCHAR (64)    NULL,
    [KuldKuldemeny_Id]      UNIQUEIDENTIFIER NULL,
    [IraIrat_Id]            UNIQUEIDENTIFIER NULL,
    [DigitalisAlairas]      NVARCHAR (4000)  NULL,
    [Uzenet]                NVARCHAR (MAX)   NULL,
    [EmailForras]           NVARCHAR (MAX)   NULL,
    [EmailGuid]             NVARCHAR (4000)  NULL,
    [FeldolgozasIdo]        DATETIME         NULL,
    [ForrasTipus]           NVARCHAR (64)    NULL,
    [Allapot]               NVARCHAR (64)    NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    [CC]                    NVARCHAR (4000)  NULL,
    CONSTRAINT [PK_EREC_eMailBoritekokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_eMailBoritekokHistory_ID_VER]
    ON [dbo].[EREC_eMailBoritekokHistory]([Id] ASC, [Ver] ASC);

