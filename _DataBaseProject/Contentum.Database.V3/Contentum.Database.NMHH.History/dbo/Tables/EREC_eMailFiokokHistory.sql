﻿CREATE TABLE [dbo].[EREC_eMailFiokokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_EREC_eMailFiokokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Nev]                   NVARCHAR (100)   NULL,
    [UserNev]               NVARCHAR (100)   NULL,
    [Csoportok_Id]          UNIQUEIDENTIFIER NULL,
    [Jelszo]                NVARCHAR (20)    NULL,
    [Tipus]                 CHAR (1)         NULL,
    [NapiMax]               INT              NULL,
    [NapiTeny]              INT              NULL,
    [EmailCim]              NVARCHAR (100)   NULL,
    [Modul_Id]              UNIQUEIDENTIFIER NULL,
    [MappaNev]              NVARCHAR (100)   NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_EREC_eMailFiokokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_EREC_eMailFiokokHistory_ID_VER]
    ON [dbo].[EREC_eMailFiokokHistory]([Id] ASC, [Ver] ASC);

