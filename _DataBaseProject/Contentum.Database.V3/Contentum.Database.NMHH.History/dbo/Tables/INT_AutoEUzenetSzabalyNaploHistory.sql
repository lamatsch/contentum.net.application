﻿CREATE TABLE [dbo].[INT_AutoEUzenetSzabalyNaploHistory] (
    [HistoryId]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [ESzabalyId]            UNIQUEIDENTIFIER NULL,
    [EUzenetId]             UNIQUEIDENTIFIER NULL,
    [EUzenetTipusId]        NVARCHAR (100)   NULL,
    [Megjegyzes]            NVARCHAR (MAX)   NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_INT_AutoEUzenetSzabalyNaploHistory_ID_VER]
    ON [dbo].[INT_AutoEUzenetSzabalyNaploHistory]([Id] ASC, [Ver] ASC);

