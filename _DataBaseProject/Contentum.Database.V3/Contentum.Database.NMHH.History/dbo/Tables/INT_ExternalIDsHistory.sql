-- =========================================
-- Create History table for INT_ExternalIDs
-- =========================================
 /*
IF OBJECT_ID('INT_ExternalIDsHistory', 'U') IS NOT NULL
  DROP TABLE INT_ExternalIDsHistory
GO
 */
CREATE TABLE INT_ExternalIDsHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 Modul_Id uniqueidentifier,  
 Edok_Type Nvarchar(20),  
 Edok_Id uniqueidentifier,  
 Edok_ExpiredDate datetime,  
 External_Group Nvarchar(20),  
 External_Id uniqueidentifier,  
 Deleted char(1),  
 LastSync datetime,  
 State Nvarchar(10),  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_INT_ExternalIDsHistory_ID_VER ON INT_ExternalIDsHistory
(
	Id,Ver ASC
)
GO