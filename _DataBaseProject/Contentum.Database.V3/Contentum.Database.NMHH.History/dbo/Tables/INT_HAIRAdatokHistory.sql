-- =========================================
-- Create History table for INT_HAIRAdatok
-- =========================================
 /*
IF OBJECT_ID('INT_HAIRAdatokHistory', 'U') IS NOT NULL
  DROP TABLE INT_HAIRAdatokHistory
GO
 */
CREATE TABLE INT_HAIRAdatokHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 Obj_Id uniqueidentifier,  
 ObjTip_Id uniqueidentifier,  
 Tipus Nvarchar(100),  
 Ertek Nvarchar(4000),  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_INT_HAIRAdatokHistory_ID_VER ON INT_HAIRAdatokHistory
(
	Id,Ver ASC
)
GO