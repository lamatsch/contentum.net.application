-- =========================================
-- Create History table for INT_Log
-- =========================================
 /*
IF OBJECT_ID('INT_LogHistory', 'U') IS NOT NULL
  DROP TABLE INT_LogHistory
GO
 */
CREATE TABLE INT_LogHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 Org uniqueidentifier,  
 Modul_id uniqueidentifier,  
 Sync_StartDate datetime,  
 Parancs Nvarchar(400),  
 Machine Nvarchar(100),  
 Sync_EndDate datetime,  
 HibaKod Nvarchar(4000),  
 HibaUzenet Nvarchar(4000),  
 ExternalId Nvarchar(4000),  
 Status Nvarchar(100),  
 Dokumentumok_Id_Sent uniqueidentifier,  
 Dokumentumok_Id_Error uniqueidentifier,  
 PackageHash Nvarchar(100),  
 PackageVer int,  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_INT_LogHistory_ID_VER ON INT_LogHistory
(
	Id,Ver ASC
)
GO