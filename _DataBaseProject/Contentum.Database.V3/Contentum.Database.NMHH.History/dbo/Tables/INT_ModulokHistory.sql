-- =========================================
-- Create History table for INT_Modulok
-- =========================================
 /*
IF OBJECT_ID('INT_ModulokHistory', 'U') IS NOT NULL
  DROP TABLE INT_ModulokHistory
GO
 */
CREATE TABLE INT_ModulokHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 Org uniqueidentifier,  
 Nev Nvarchar(400),  
 Statusz char(1),  
 Leiras Nvarchar(400),  
 Parancs Nvarchar(400),  
 Gyakorisag int,  
 GyakorisagMertekegyseg Nvarchar(20),  
 UtolsoFutas datetime,  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_INT_ModulokHistory_ID_VER ON INT_ModulokHistory
(
	Id,Ver ASC
)
GO