-- =========================================
-- Create History table for INT_Parameterek
-- =========================================
 /*
IF OBJECT_ID('INT_ParameterekHistory', 'U') IS NOT NULL
  DROP TABLE INT_ParameterekHistory
GO
 */
CREATE TABLE INT_ParameterekHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
 Org uniqueidentifier,  
 Modul_id uniqueidentifier,  
 Felhasznalo_id uniqueidentifier,  
 Nev Nvarchar(400),  
 Ertek Nvarchar(400),  
 Karbantarthato char(1),  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_INT_ParameterekHistory_ID_VER ON INT_ParameterekHistory
(
	Id,Ver ASC
)
GO