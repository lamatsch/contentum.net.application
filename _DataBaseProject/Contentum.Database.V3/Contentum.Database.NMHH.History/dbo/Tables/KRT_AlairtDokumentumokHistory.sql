﻿CREATE TABLE [dbo].[KRT_AlairtDokumentumokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_AlairtDokumentumokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [AlairasMod]            NVARCHAR (64)    NULL,
    [AlairtFajlnev]         NVARCHAR (400)   NULL,
    [AlairtTartalomHash]    VARBINARY (4000) NULL,
    [IratAlairasSzabaly_Id] UNIQUEIDENTIFIER NULL,
    [AlairasRendben]        CHAR (1)         NULL,
    [KivarasiIdoVege]       DATETIME         NULL,
    [AlairasVeglegRendben]  CHAR (1)         NULL,
    [Idopecset]             DATETIME         NULL,
    [Csoport_Id_Alairo]     UNIQUEIDENTIFIER NULL,
    [AlairasTulajdonos]     NVARCHAR (100)   NULL,
    [Tanusitvany_Id]        UNIQUEIDENTIFIER NULL,
    [External_Link]         NVARCHAR (400)   NULL,
    [External_Id]           UNIQUEIDENTIFIER NULL,
    [External_Source]       NVARCHAR (100)   NULL,
    [External_Info]         NVARCHAR (400)   NULL,
    [Allapot]               NVARCHAR (64)    NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_AlairtDokumentumokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_AlairtDokumentumokHistory_ID_VER]
    ON [dbo].[KRT_AlairtDokumentumokHistory]([Id] ASC, [Ver] ASC);

