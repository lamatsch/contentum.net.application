﻿CREATE TABLE [dbo].[KRT_CsoportTagokHistory] (
    [HistoryId]               UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_CsoportTagokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]       INT              NULL,
    [HistoryVegrehajto_Id]    UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]   DATETIME         NULL,
    [Id]                      UNIQUEIDENTIFIER NULL,
    [Tipus]                   NVARCHAR (64)    NULL,
    [Csoport_Id]              UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Ver]          INT              NULL,
    [Csoport_Id_Jogalany]     UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Jogalany_Ver] INT              NULL,
    [ObjTip_Id_Jogalany]      UNIQUEIDENTIFIER NULL,
    [ErtesitesMailCim]        NVARCHAR (100)   NULL,
    [ErtesitesKell]           CHAR (1)         NULL,
    [System]                  CHAR (1)         NULL,
    [Orokolheto]              CHAR (1)         NULL,
    [Ver]                     INT              NULL,
    [Note]                    NVARCHAR (4000)  NULL,
    [Stat_id]                 UNIQUEIDENTIFIER NULL,
    [ErvKezd]                 DATETIME         NULL,
    [ErvVege]                 DATETIME         NULL,
    [Letrehozo_id]            UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]           DATETIME         NULL,
    [Modosito_id]             UNIQUEIDENTIFIER NULL,
    [ModositasIdo]            DATETIME         NULL,
    [Zarolo_id]               UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]              DATETIME         NULL,
    [Tranz_id]                UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]          UNIQUEIDENTIFIER NULL,
    [ObjektumTulajdonos]      CHAR (1)         NULL,
    CONSTRAINT [PK_KRT_CsoportTagokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_CsoportTagokHistory_ID_VER]
    ON [dbo].[KRT_CsoportTagokHistory]([Id] ASC, [Ver] ASC);

