﻿CREATE TABLE [dbo].[KRT_CsoportokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_CsoportokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Org]                   UNIQUEIDENTIFIER NULL,
    [Kod]                   NVARCHAR (100)   NULL,
    [Nev]                   NVARCHAR (400)   NULL,
    [Tipus]                 NVARCHAR (64)    NULL,
    [Jogalany]              CHAR (1)         NULL,
    [ErtesitesEmail]        NVARCHAR (100)   NULL,
    [System]                CHAR (1)         NULL,
    [Adatforras]            CHAR (1)         NULL,
    [ObjTipus_Id_Szulo]     UNIQUEIDENTIFIER NULL,
    [ObjTipus_Id_Szulo_Ver] INT              NULL,
    [Kiszolgalhato]         CHAR (1)         NULL,
    [JogosultsagOroklesMod] CHAR (1)         NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_CsoportokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_CsoportokHistory_ID_VER]
    ON [dbo].[KRT_CsoportokHistory]([Id] ASC, [Ver] ASC);

