﻿CREATE TABLE [dbo].[KRT_DokumentumAlairasokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_DokumentumAlairasokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Dokumentum_Id_Alairt]  UNIQUEIDENTIFIER NULL,
    [AlairasMod]            NVARCHAR (64)    NULL,
    [AlairasSzabaly_Id]     UNIQUEIDENTIFIER NULL,
    [AlairasRendben]        CHAR (1)         NULL,
    [KivarasiIdoVege]       DATETIME         NULL,
    [AlairasVeglegRendben]  CHAR (1)         NULL,
    [Idopecset]             DATETIME         NULL,
    [Csoport_Id_Alairo]     UNIQUEIDENTIFIER NULL,
    [AlairasTulajdonos]     NVARCHAR (100)   NULL,
    [Tanusitvany_Id]        UNIQUEIDENTIFIER NULL,
    [Allapot]               NVARCHAR (64)    NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    [AlairoSzemely]         NVARCHAR (100)   NULL,
    CONSTRAINT [PK_KRT_DokumentumAlairasokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_DokumentumAlairasokHistory_ID_VER]
    ON [dbo].[KRT_DokumentumAlairasokHistory]([Id] ASC, [Ver] ASC);

