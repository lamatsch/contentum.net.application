﻿CREATE TABLE [dbo].[KRT_DokumentumKapcsolatokHistory] (
    [HistoryId]                 UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_DokumentumKapcsolatokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]         INT              NULL,
    [HistoryVegrehajto_Id]      UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]     DATETIME         NULL,
    [Id]                        UNIQUEIDENTIFIER NULL,
    [Tipus]                     NVARCHAR (64)    NULL,
    [Dokumentum_Id_Fo]          UNIQUEIDENTIFIER NULL,
    [Dokumentum_Id_Al]          UNIQUEIDENTIFIER NULL,
    [DokumentumKapcsolatJelleg] NVARCHAR (400)   NULL,
    [DokuKapcsolatSorrend]      INT              NULL,
    [Ver]                       INT              NULL,
    [Note]                      NVARCHAR (4000)  NULL,
    [Stat_id]                   UNIQUEIDENTIFIER NULL,
    [ErvKezd]                   DATETIME         NULL,
    [ErvVege]                   DATETIME         NULL,
    [Letrehozo_id]              UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]             DATETIME         NULL,
    [Modosito_id]               UNIQUEIDENTIFIER NULL,
    [ModositasIdo]              DATETIME         NULL,
    [Zarolo_id]                 UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                DATETIME         NULL,
    [Tranz_id]                  UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]            UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_DokumentumKapcsolatokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_DokumentumKapcsolatokHistory_ID_VER]
    ON [dbo].[KRT_DokumentumKapcsolatokHistory]([Id] ASC, [Ver] ASC);

