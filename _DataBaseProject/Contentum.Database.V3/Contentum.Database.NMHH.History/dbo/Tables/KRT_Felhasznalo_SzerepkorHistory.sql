﻿CREATE TABLE [dbo].[KRT_Felhasznalo_SzerepkorHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_Felhasznalo_SzerepkorHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [CsoportTag_Id]         UNIQUEIDENTIFIER NULL,
    [Csoport_Id]            UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Ver]        INT              NULL,
    [Felhasznalo_Id]        UNIQUEIDENTIFIER NULL,
    [Felhasznalo_Id_Ver]    INT              NULL,
    [Szerepkor_Id]          UNIQUEIDENTIFIER NULL,
    [Szerepkor_Id_Ver]      INT              NULL,
    [Helyettesites_Id]      UNIQUEIDENTIFIER NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_Felhasznalo_SzerepkorHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_Felhasznalo_SzerepkorHistory_ID_VER]
    ON [dbo].[KRT_Felhasznalo_SzerepkorHistory]([Id] ASC, [Ver] ASC);

