﻿CREATE TABLE [dbo].[KRT_FelhasznalokHistory] (
    [HistoryId]                UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_FelhasznalokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]        INT              NULL,
    [HistoryVegrehajto_Id]     UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]    DATETIME         NULL,
    [Id]                       UNIQUEIDENTIFIER NULL,
    [Org]                      UNIQUEIDENTIFIER NULL,
    [Partner_id]               UNIQUEIDENTIFIER NULL,
    [Partner_id_Ver]           INT              NULL,
    [Tipus]                    NVARCHAR (10)    NULL,
    [UserNev]                  NVARCHAR (100)   NULL,
    [Nev]                      NVARCHAR (100)   NULL,
    [Jelszo]                   NVARCHAR (100)   NULL,
    [JelszoLejaratIdo]         DATETIME         NULL,
    [System]                   CHAR (1)         NULL,
    [Kiszolgalo]               CHAR (1)         NULL,
    [DefaultPrivatKulcs]       NVARCHAR (4000)  NULL,
    [Partner_Id_Munkahely]     UNIQUEIDENTIFIER NULL,
    [Partner_Id_Munkahely_Ver] INT              NULL,
    [MaxMinosites]             NVARCHAR (2)     NULL,
    [EMail]                    NVARCHAR (100)   NULL,
    [Engedelyezett]            CHAR (1)         NULL,
    [Telefonszam]              NVARCHAR (100)   NULL,
    [Beosztas]                 NVARCHAR (100)   NULL,
    [Ver]                      INT              NULL,
    [Note]                     NVARCHAR (4000)  NULL,
    [Stat_id]                  UNIQUEIDENTIFIER NULL,
    [ErvKezd]                  DATETIME         NULL,
    [ErvVege]                  DATETIME         NULL,
    [Letrehozo_id]             UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]            DATETIME         NULL,
    [Modosito_id]              UNIQUEIDENTIFIER NULL,
    [ModositasIdo]             DATETIME         NULL,
    [ZarolasIdo]               DATETIME         NULL,
    [Zarolo_id]                UNIQUEIDENTIFIER NULL,
    [Tranz_id]                 UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]           UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_FelhasznalokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_FelhasznalokHistory_ID_VER]
    ON [dbo].[KRT_FelhasznalokHistory]([Id] ASC, [Ver] ASC);

