﻿CREATE TABLE [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_Felhasznalok_Halozati_NyomtatoiHistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NOT NULL,
    [Felhasznalo_Id]        UNIQUEIDENTIFIER NOT NULL,
    [Halozati_Nyomtato_Id]  UNIQUEIDENTIFIER NOT NULL,
    [Ver]                   INT              DEFAULT ((1)) NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NOT NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_Felhasznalok_Halozati_NyomtatoiHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);

