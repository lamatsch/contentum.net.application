﻿CREATE TABLE [dbo].[KRT_ForditasokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [NyelvKod]              NVARCHAR (10)    NULL,
    [OrgKod]                NVARCHAR (100)   NULL,
    [Modul]                 NVARCHAR (400)   NULL,
    [Komponens]             NVARCHAR (400)   NULL,
    [ObjAzonosito]          NVARCHAR (400)   NULL,
    [Forditas]              NVARCHAR (4000)  NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_ForditasokHistory_ID_VER]
    ON [dbo].[KRT_ForditasokHistory]([Id] ASC, [Ver] ASC);

