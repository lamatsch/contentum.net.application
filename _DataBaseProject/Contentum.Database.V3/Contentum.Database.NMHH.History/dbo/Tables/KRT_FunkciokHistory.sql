﻿CREATE TABLE [dbo].[KRT_FunkciokHistory] (
    [HistoryId]                UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_FunkciokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]        INT              NULL,
    [HistoryVegrehajto_Id]     UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]    DATETIME         NULL,
    [Id]                       UNIQUEIDENTIFIER NULL,
    [Kod]                      NVARCHAR (100)   NULL,
    [Nev]                      NVARCHAR (100)   NULL,
    [ObjTipus_Id_AdatElem]     UNIQUEIDENTIFIER NULL,
    [ObjTipus_Id_AdatElem_Ver] INT              NULL,
    [ObjStat_Id_Kezd]          UNIQUEIDENTIFIER NULL,
    [ObjStat_Id_Kezd_Ver]      INT              NULL,
    [Alkalmazas_Id]            UNIQUEIDENTIFIER NULL,
    [Alkalmazas_Id_Ver]        INT              NULL,
    [Muvelet_Id]               UNIQUEIDENTIFIER NULL,
    [Muvelet_Id_Ver]           INT              NULL,
    [ObjStat_Id_Veg]           UNIQUEIDENTIFIER NULL,
    [ObjStat_Id_Veg_Ver]       INT              NULL,
    [Leiras]                   NVARCHAR (4000)  NULL,
    [Funkcio_Id_Szulo]         UNIQUEIDENTIFIER NULL,
    [Funkcio_Id_Szulo_Ver]     INT              NULL,
    [Csoportosito]             CHAR (1)         NULL,
    [Modosithato]              CHAR (1)         NULL,
    [Org]                      UNIQUEIDENTIFIER NULL,
    [Ver]                      INT              NULL,
    [Note]                     NVARCHAR (4000)  NULL,
    [Stat_id]                  UNIQUEIDENTIFIER NULL,
    [ErvKezd]                  DATETIME         NULL,
    [ErvVege]                  DATETIME         NULL,
    [Letrehozo_id]             UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]            DATETIME         NULL,
    [Modosito_id]              UNIQUEIDENTIFIER NULL,
    [ModositasIdo]             DATETIME         NULL,
    [Zarolo_id]                UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]               DATETIME         NULL,
    [Tranz_id]                 UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]           UNIQUEIDENTIFIER NULL,
    [MunkanaploJelzo]          CHAR (1)         NULL,
    [FeladatJelzo]             CHAR (1)         NULL,
    [KeziFeladatJelzo]         CHAR (1)         NULL,
    CONSTRAINT [PK_KRT_FunkciokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_FunkciokHistory_ID_VER]
    ON [dbo].[KRT_FunkciokHistory]([Id] ASC, [Ver] ASC);

