﻿CREATE TABLE [dbo].[KRT_HelyettesitesekHistory] (
    [HistoryId]                         UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_HelyettesitesekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]                 INT              NULL,
    [HistoryVegrehajto_Id]              UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]             DATETIME         NULL,
    [Id]                                UNIQUEIDENTIFIER NULL,
    [Felhasznalo_ID_helyettesitett]     UNIQUEIDENTIFIER NULL,
    [Felhasznalo_ID_helyettesitett_Ver] INT              NULL,
    [Felhasznalo_ID_helyettesito]       UNIQUEIDENTIFIER NULL,
    [Felhasznalo_ID_helyettesito_Ver]   INT              NULL,
    [HelyettesitesKezd]                 DATETIME         NULL,
    [HelyettesitesVege]                 DATETIME         NULL,
    [Megjegyzes]                        NVARCHAR (4000)  NULL,
    [Ver]                               INT              NULL,
    [Note]                              NVARCHAR (4000)  NULL,
    [Stat_id]                           UNIQUEIDENTIFIER NULL,
    [ErvKezd]                           DATETIME         NULL,
    [ErvVege]                           DATETIME         NULL,
    [Letrehozo_id]                      UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                     DATETIME         NULL,
    [Modosito_id]                       UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                      DATETIME         NULL,
    [Zarolo_id]                         UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                        DATETIME         NULL,
    [Tranz_id]                          UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                    UNIQUEIDENTIFIER NULL,
    [HelyettesitesMod]                  NVARCHAR (64)    NULL,
    [CsoportTag_ID_helyettesitett]      UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_HelyettesitesekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_HelyettesitesekHistory_ID_VER]
    ON [dbo].[KRT_HelyettesitesekHistory]([Id] ASC, [Ver] ASC);

