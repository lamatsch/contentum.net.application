﻿CREATE TABLE [dbo].[KRT_KodCsoportokHistory] (
    [HistoryId]                       UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_KodCsoportokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]               INT              NULL,
    [HistoryVegrehajto_Id]            UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]           DATETIME         NULL,
    [Id]                              UNIQUEIDENTIFIER NULL,
    [Org]                             UNIQUEIDENTIFIER NULL,
    [KodTarak_Id_KodcsoportTipus]     UNIQUEIDENTIFIER NULL,
    [KodTarak_Id_KodcsoportTipus_Ver] INT              NULL,
    [Kod]                             NVARCHAR (64)    NULL,
    [Nev]                             NVARCHAR (400)   NULL,
    [Modosithato]                     CHAR (1)         NULL,
    [Hossz]                           INT              NULL,
    [Csoport_Id_Tulaj]                UNIQUEIDENTIFIER NULL,
    [Leiras]                          NVARCHAR (4000)  NULL,
    [BesorolasiSema]                  CHAR (1)         NULL,
    [KiegAdat]                        CHAR (1)         NULL,
    [KiegMezo]                        NVARCHAR (400)   NULL,
    [KiegAdattipus]                   CHAR (1)         NULL,
    [KiegSzotar]                      NVARCHAR (100)   NULL,
    [Ver]                             INT              NULL,
    [Note]                            NVARCHAR (4000)  NULL,
    [Stat_id]                         UNIQUEIDENTIFIER NULL,
    [ErvKezd]                         DATETIME         NULL,
    [ErvVege]                         DATETIME         NULL,
    [Letrehozo_id]                    UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                   DATETIME         NULL,
    [Modosito_id]                     UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                    DATETIME         NULL,
    [Zarolo_id]                       UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                      DATETIME         NULL,
    [Tranz_id]                        UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]                  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_KodCsoportokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_KodCsoportokHistory_ID_VER]
    ON [dbo].[KRT_KodCsoportokHistory]([Id] ASC, [Ver] ASC);

