﻿CREATE TABLE [dbo].[KRT_KodTarakHistory] (
    [HistoryId]              UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_KodTarakHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]      INT              NULL,
    [HistoryVegrehajto_Id]   UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]  DATETIME         NULL,
    [Id]                     UNIQUEIDENTIFIER NULL,
    [Org]                    UNIQUEIDENTIFIER NULL,
    [KodCsoport_Id]          UNIQUEIDENTIFIER NULL,
    [KodCsoport_Id_Ver]      INT              NULL,
    [ObjTip_Id_AdatElem]     UNIQUEIDENTIFIER NULL,
    [ObjTip_Id_AdatElem_Ver] INT              NULL,
    [Obj_Id]                 UNIQUEIDENTIFIER NULL,
    [Kod]                    NVARCHAR (64)    NULL,
    [Nev]                    NVARCHAR (400)   NULL,
    [RovidNev]               NVARCHAR (10)    NULL,
    [Egyeb]                  NVARCHAR (4000)  NULL,
    [Modosithato]            CHAR (1)         NULL,
    [Sorrend]                NVARCHAR (100)   NULL,
    [Ver]                    INT              NULL,
    [Note]                   NVARCHAR (4000)  NULL,
    [Stat_id]                UNIQUEIDENTIFIER NULL,
    [ErvKezd]                DATETIME         NULL,
    [ErvVege]                DATETIME         NULL,
    [Letrehozo_id]           UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]          DATETIME         NULL,
    [Modosito_id]            UNIQUEIDENTIFIER NULL,
    [ModositasIdo]           DATETIME         NULL,
    [Zarolo_id]              UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]             DATETIME         NULL,
    [Tranz_id]               UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]         UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_KodTarakHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_KodTarakHistory_ID_VER]
    ON [dbo].[KRT_KodTarakHistory]([Id] ASC, [Ver] ASC);

