﻿CREATE TABLE [dbo].[KRT_KodtarFuggosegHistory] (
    [HistoryId]                 UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]         INT              NULL,
    [HistoryVegrehajto_Id]      UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]     DATETIME         NULL,
    [Id]                        UNIQUEIDENTIFIER NULL,
    [Org]                       UNIQUEIDENTIFIER NULL,
    [Vezerlo_KodCsoport_Id]     UNIQUEIDENTIFIER NULL,
    [Vezerlo_KodCsoport_Id_Ver] INT              NULL,
    [Fuggo_KodCsoport_Id]       UNIQUEIDENTIFIER NULL,
    [Fuggo_KodCsoport_Id_Ver]   INT              NULL,
    [Adat]                      NVARCHAR (MAX)   NULL,
    [Aktiv]                     CHAR (1)         NULL,
    [Ver]                       INT              NULL,
    [Note]                      NVARCHAR (4000)  NULL,
    [Stat_id]                   UNIQUEIDENTIFIER NULL,
    [ErvKezd]                   DATETIME         NULL,
    [ErvVege]                   DATETIME         NULL,
    [Letrehozo_id]              UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]             DATETIME         NULL,
    [Modosito_id]               UNIQUEIDENTIFIER NULL,
    [ModositasIdo]              DATETIME         NULL,
    [Zarolo_id]                 UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]                DATETIME         NULL,
    [Tranz_id]                  UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]            UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_KodtarFuggosegHistory_ID_VER]
    ON [dbo].[KRT_KodtarFuggosegHistory]([Id] ASC, [Ver] ASC);

