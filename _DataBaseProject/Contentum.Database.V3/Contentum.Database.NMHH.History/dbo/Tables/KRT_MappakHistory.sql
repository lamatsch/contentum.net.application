﻿CREATE TABLE [dbo].[KRT_MappakHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_MappakHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [SzuloMappa_Id]         UNIQUEIDENTIFIER NULL,
    [BarCode]               NVARCHAR (100)   NULL,
    [Tipus]                 NVARCHAR (64)    NULL,
    [Nev]                   NVARCHAR (400)   NULL,
    [Leiras]                NVARCHAR (4000)  NULL,
    [Csoport_Id_Tulaj]      UNIQUEIDENTIFIER NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_MappakHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_MappakHistory_ID_VER]
    ON [dbo].[KRT_MappakHistory]([Id] ASC, [Ver] ASC);

