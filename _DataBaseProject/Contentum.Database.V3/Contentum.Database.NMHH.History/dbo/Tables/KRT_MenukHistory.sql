﻿CREATE TABLE [dbo].[KRT_MenukHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_MenukHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Org]                   UNIQUEIDENTIFIER NULL,
    [Menu_Id_Szulo]         UNIQUEIDENTIFIER NULL,
    [Menu_Id_Szulo_Ver]     INT              NULL,
    [Kod]                   NVARCHAR (20)    NULL,
    [Nev]                   NVARCHAR (100)   NULL,
    [Funkcio_Id]            UNIQUEIDENTIFIER NULL,
    [Funkcio_Id_Ver]        INT              NULL,
    [Modul_Id]              UNIQUEIDENTIFIER NULL,
    [Modul_Id_Ver]          INT              NULL,
    [Parameter]             NVARCHAR (400)   NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ImageURL]              NVARCHAR (400)   NULL,
    [Sorrend]               NVARCHAR (100)   NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_MenukHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_MenukHistory_ID_VER]
    ON [dbo].[KRT_MenukHistory]([Id] ASC, [Ver] ASC);

