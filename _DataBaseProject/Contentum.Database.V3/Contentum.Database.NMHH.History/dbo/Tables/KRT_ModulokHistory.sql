﻿CREATE TABLE [dbo].[KRT_ModulokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_ModulokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Org]                   UNIQUEIDENTIFIER NULL,
    [Tipus]                 NVARCHAR (10)    NULL,
    [Kod]                   NVARCHAR (100)   NULL,
    [Nev]                   NVARCHAR (100)   NULL,
    [Leiras]                NVARCHAR (4000)  NULL,
    [SelectString]          NVARCHAR (4000)  NULL,
    [Csoport_Id_Tulaj]      UNIQUEIDENTIFIER NULL,
    [Parameterek]           NVARCHAR (4000)  NULL,
    [ObjTip_Id_Adatelem]    UNIQUEIDENTIFIER NULL,
    [ObjTip_Adatelem]       NVARCHAR (100)   NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_ModulokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_ModulokHistory_ID_VER]
    ON [dbo].[KRT_ModulokHistory]([Id] ASC, [Ver] ASC);

