﻿CREATE TABLE [dbo].[KRT_NezetekHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_NezetekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Org]                   UNIQUEIDENTIFIER NULL,
    [Form_Id]               UNIQUEIDENTIFIER NULL,
    [Form_Id_Ver]           INT              NULL,
    [Nev]                   NVARCHAR (100)   NULL,
    [Leiras]                NVARCHAR (4000)  NULL,
    [Csoport_Id]            UNIQUEIDENTIFIER NULL,
    [Muvelet_Id]            UNIQUEIDENTIFIER NULL,
    [Muvelet_Id_Ver]        INT              NULL,
    [DisableControls]       NVARCHAR (4000)  NULL,
    [ReadOnlyControls]      NVARCHAR (4000)  NULL,
    [Prioritas]             INT              NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    [InvisibleControls]     NVARCHAR (4000)  NULL,
    [TabIndexControls]      NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_KRT_NezetekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_KRT_NezetekHistory_ID_VER]
    ON [dbo].[KRT_NezetekHistory]([Id] ASC, [Ver] ASC);

