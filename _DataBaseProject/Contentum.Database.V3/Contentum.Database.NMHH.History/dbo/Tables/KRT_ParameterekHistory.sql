﻿CREATE TABLE [dbo].[KRT_ParameterekHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_ParameterekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Org]                   UNIQUEIDENTIFIER NULL,
    [Felhasznalo_id]        UNIQUEIDENTIFIER NULL,
    [Felhasznalo_id_Ver]    INT              NULL,
    [Nev]                   NVARCHAR (400)   NULL,
    [Ertek]                 NVARCHAR (400)   NULL,
    [Karbantarthato]        CHAR (1)         NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_ParameterekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_ParameterekHistory_ID_VER]
    ON [dbo].[KRT_ParameterekHistory]([Id] ASC, [Ver] ASC);

