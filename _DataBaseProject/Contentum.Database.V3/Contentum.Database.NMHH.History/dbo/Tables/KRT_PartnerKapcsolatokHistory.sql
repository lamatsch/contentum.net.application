﻿CREATE TABLE [dbo].[KRT_PartnerKapcsolatokHistory] (
    [HistoryId]               UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_PartnerKapcsolatokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]       INT              NULL,
    [HistoryVegrehajto_Id]    UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]   DATETIME         NULL,
    [Id]                      UNIQUEIDENTIFIER NULL,
    [Tipus]                   NVARCHAR (64)    NULL,
    [Partner_id]              UNIQUEIDENTIFIER NULL,
    [Partner_id_Ver]          INT              NULL,
    [Partner_id_kapcsolt]     UNIQUEIDENTIFIER NULL,
    [Partner_id_kapcsolt_Ver] INT              NULL,
    [Ver]                     INT              NULL,
    [Note]                    NVARCHAR (4000)  NULL,
    [Stat_id]                 UNIQUEIDENTIFIER NULL,
    [ErvKezd]                 DATETIME         NULL,
    [ErvVege]                 DATETIME         NULL,
    [Letrehozo_id]            UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]           DATETIME         NULL,
    [Modosito_id]             UNIQUEIDENTIFIER NULL,
    [ModositasIdo]            DATETIME         NULL,
    [Zarolo_id]               UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]              DATETIME         NULL,
    [Tranz_id]                UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]          UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_PartnerKapcsolatokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_PartnerKapcsolatokHistory_ID_VER]
    ON [dbo].[KRT_PartnerKapcsolatokHistory]([Id] ASC, [Ver] ASC);

