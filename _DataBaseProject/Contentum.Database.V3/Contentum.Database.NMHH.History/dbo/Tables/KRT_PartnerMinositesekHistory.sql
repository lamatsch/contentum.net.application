﻿CREATE TABLE [dbo].[KRT_PartnerMinositesekHistory] (
    [HistoryId]                UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_PartnerMinositesekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]        INT              NULL,
    [HistoryVegrehajto_Id]     UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]    DATETIME         NULL,
    [Id]                       UNIQUEIDENTIFIER NULL,
    [ALLAPOT]                  CHAR (1)         NULL,
    [Partner_id]               UNIQUEIDENTIFIER NULL,
    [Partner_id_Ver]           INT              NULL,
    [Felhasznalo_id_kero]      UNIQUEIDENTIFIER NULL,
    [Felhasznalo_id_kero_Ver]  INT              NULL,
    [KertMinosites]            NVARCHAR (64)    NULL,
    [KertKezdDat]              DATETIME         NULL,
    [KertVegeDat]              DATETIME         NULL,
    [KerelemAzonosito]         NVARCHAR (100)   NULL,
    [KerelemBeadIdo]           DATETIME         NULL,
    [KerelemDontesIdo]         DATETIME         NULL,
    [Felhasznalo_id_donto]     UNIQUEIDENTIFIER NULL,
    [Felhasznalo_id_donto_Ver] INT              NULL,
    [DontesAzonosito]          NVARCHAR (100)   NULL,
    [Minosites]                NVARCHAR (2)     NULL,
    [MinositesKezdDat]         DATETIME         NULL,
    [MinositesVegDat]          DATETIME         NULL,
    [SztornirozasDat]          DATETIME         NULL,
    [Ver]                      INT              NULL,
    [Note]                     NVARCHAR (4000)  NULL,
    [Stat_id]                  UNIQUEIDENTIFIER NULL,
    [ErvKezd]                  DATETIME         NULL,
    [ErvVege]                  DATETIME         NULL,
    [Letrehozo_id]             UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]            DATETIME         NULL,
    [Modosito_id]              UNIQUEIDENTIFIER NULL,
    [ModositasIdo]             DATETIME         NULL,
    [Zarolo_id]                UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]               DATETIME         NULL,
    [Tranz_id]                 UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]           UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_PartnerMinositesekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_PartnerMinositesekHistory_ID_VER]
    ON [dbo].[KRT_PartnerMinositesekHistory]([Id] ASC, [Ver] ASC);

