-- =========================================
-- Create History table for KRT_Partner_Dokumentumok
-- =========================================
 
 
CREATE TABLE KRT_Partner_DokumentumokHistory
(
  HistoryId uniqueidentifier not null default newsequentialid(),
  HistoryMuvelet_Id int,
  HistoryVegrehajto_Id uniqueidentifier,
  HistoryVegrehajtasIdo datetime,  
 Id uniqueidentifier,  
  Partner_id uniqueidentifier,
  
  Partner_id_Ver int,  
  Dokumentum_Id uniqueidentifier,
  
  Dokumentum_Id_Ver int,  
 Ver int,  
 Note Nvarchar(4000),  
 Stat_id uniqueidentifier,  
 ErvKezd datetime,  
 ErvVege datetime,  
 Letrehozo_id uniqueidentifier,  
 LetrehozasIdo datetime,  
 Modosito_id uniqueidentifier,  
 ModositasIdo datetime,  
 Zarolo_id uniqueidentifier,  
 ZarolasIdo datetime,  
 Tranz_id uniqueidentifier,  
 UIAccessLog_id uniqueidentifier,  
 PRIMARY KEY (HistoryId)
)
GO

CREATE NONCLUSTERED INDEX IX_KRT_Partner_DokumentumokHistory_ID_VER ON KRT_Partner_DokumentumokHistory
(
	Id,Ver ASC
)
GO