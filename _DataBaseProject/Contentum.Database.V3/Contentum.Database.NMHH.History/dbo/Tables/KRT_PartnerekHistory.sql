﻿CREATE TABLE [dbo].[KRT_PartnerekHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_PartnerekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Id_Ver]                INT              NULL,
    [Org]                   UNIQUEIDENTIFIER NULL,
    [Orszag_Id]             UNIQUEIDENTIFIER NULL,
    [Tipus]                 NVARCHAR (64)    NULL,
    [Nev]                   NVARCHAR (400)   NULL,
    [Hierarchia]            NVARCHAR (400)   NULL,
    [KulsoAzonositok]       NVARCHAR (100)   NULL,
    [PublikusKulcs]         NVARCHAR (4000)  NULL,
    [Kiszolgalo]            UNIQUEIDENTIFIER NULL,
    [LetrehozoSzervezet]    UNIQUEIDENTIFIER NULL,
    [MaxMinosites]          NVARCHAR (2)     NULL,
    [MinositesKezdDat]      DATETIME         NULL,
    [MinositesVegDat]       DATETIME         NULL,
    [MaxMinositesSzervezet] NVARCHAR (2)     NULL,
    [Belso]                 CHAR (1)         NULL,
    [Forras]                CHAR (1)         NULL,
	 Allapot              Nvarchar(100)        null,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_PartnerekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_PartnerekHistory_ID_VER]
    ON [dbo].[KRT_PartnerekHistory]([Id] ASC, [Ver] ASC);

