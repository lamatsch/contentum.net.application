﻿CREATE TABLE [dbo].[KRT_RagSzamokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Kod]                   NVARCHAR (100)   NULL,
    [KodNum]                FLOAT (53)       NULL,
    [Postakonyv_Id]         UNIQUEIDENTIFIER NULL,
    [RagszamSav_Id]         UNIQUEIDENTIFIER NULL,
    [Obj_Id]                UNIQUEIDENTIFIER NULL,
    [ObjTip_Id]             UNIQUEIDENTIFIER NULL,
    [Obj_type]              NVARCHAR (100)   NULL,
    [KodType]               CHAR (1)         NULL,
    [Allapot]               NVARCHAR (64)    NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_RagSzamokHistory_ID_VER]
    ON [dbo].[KRT_RagSzamokHistory]([Id] ASC, [Ver] ASC);

