﻿CREATE TABLE [dbo].[KRT_RagszamSavokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Org]                   UNIQUEIDENTIFIER NULL,
    [Csoport_Id_Felelos]    UNIQUEIDENTIFIER NULL,
    [Postakonyv_Id]         UNIQUEIDENTIFIER NULL,
    [SavKezd]               NVARCHAR (100)   NULL,
    [SavKezdNum]            FLOAT (53)       NULL,
    [SavVege]               NVARCHAR (100)   NULL,
    [SavVegeNum]            FLOAT (53)       NULL,
	[IgenyeltDarabszam]		FLOAT (53)		 NULL,
    [SavType]               NVARCHAR (100)   NULL,
    [SavAllapot]            CHAR (1)         NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_RagszamSavokHistory_ID_VER]
    ON [dbo].[KRT_RagszamSavokHistory]([Id] ASC, [Ver] ASC);

