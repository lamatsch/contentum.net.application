﻿CREATE TABLE [dbo].[KRT_SoapMessageLogHistory] (
    [HistoryId]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    [ForrasRendszer]        NVARCHAR (200)   NULL,
    [Url]                   NVARCHAR (1500)  NULL,
    [CelRendszer]           NVARCHAR (200)   NULL,
    [MetodusNeve]           NVARCHAR (1000)  NULL,
    [KeresFogadas]          DATETIME         NULL,
    [KeresKuldes]           DATETIME         NULL,
    [RequestData]           NVARCHAR (MAX)   NULL,
    [ResponseData]          NVARCHAR (MAX)   NULL,
    [FuttatoFelhasznalo]    NVARCHAR (500)   NULL,
    [Local]                 CHAR (1)         NULL,
    [Hiba]                  NVARCHAR (MAX)   NULL,
    [Sikeres]               CHAR (1)         NULL,
    PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_SoapMessageLogHistory_ID_VER]
    ON [dbo].[KRT_SoapMessageLogHistory]([Id] ASC, [Ver] ASC);

