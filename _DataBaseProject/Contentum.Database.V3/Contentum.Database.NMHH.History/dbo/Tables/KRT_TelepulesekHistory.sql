﻿CREATE TABLE [dbo].[KRT_TelepulesekHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_TelepulesekHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Org]                   UNIQUEIDENTIFIER NULL,
    [IRSZ]                  NVARCHAR (10)    NULL,
    [Telepules_Id_Fo]       UNIQUEIDENTIFIER NULL,
    [Telepules_Id_Fo_Ver]   INT              NULL,
    [Nev]                   NVARCHAR (100)   NULL,
    [Orszag_Id]             UNIQUEIDENTIFIER NULL,
    [Orszag_Id_Ver]         INT              NULL,
    [Megye]                 NVARCHAR (64)    NULL,
    [Regio]                 NVARCHAR (64)    NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_TelepulesekHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_TelepulesekHistory_ID_VER]
    ON [dbo].[KRT_TelepulesekHistory]([Id] ASC, [Ver] ASC);

