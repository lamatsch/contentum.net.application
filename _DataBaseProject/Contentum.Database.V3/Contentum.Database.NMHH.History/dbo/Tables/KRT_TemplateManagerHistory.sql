﻿CREATE TABLE [dbo].[KRT_TemplateManagerHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_TemplateManagerHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Nev]                   NVARCHAR (100)   NULL,
    [Tipus]                 NVARCHAR (64)    NULL,
    [KRT_Modul_Id]          UNIQUEIDENTIFIER NULL,
    [KRT_Modul_Id_Ver]      INT              NULL,
    [KRT_Dokumentum_Id]     UNIQUEIDENTIFIER NULL,
    [KRT_Dokumentum_Id_Ver] INT              NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_KRT_TemplateManagerHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_TemplateManagerHistory_ID_VER]
    ON [dbo].[KRT_TemplateManagerHistory]([Id] ASC, [Ver] ASC);

