﻿CREATE TABLE [dbo].[KRT_VallalkozasokHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF_KRT_VallalkozasokHistory_HistoryId] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Partner_Id]            UNIQUEIDENTIFIER NULL,
    [Partner_Id_Ver]        INT              NULL,
    [Adoszam]               NVARCHAR (20)    NULL,
    [TB_Torzsszam]          NVARCHAR (20)    NULL,
    [Cegjegyzekszam]        NVARCHAR (100)   NULL,
    [Tipus]                 NVARCHAR (64)    NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (4000)  NULL,
    [Stat_id]               UNIQUEIDENTIFIER NULL,
    [ErvKezd]               DATETIME         NULL,
    [ErvVege]               DATETIME         NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Zarolo_id]             UNIQUEIDENTIFIER NULL,
    [ZarolasIdo]            DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    [UIAccessLog_id]        UNIQUEIDENTIFIER NULL,
    [KulfoldiAdoszamJelolo] CHAR (1)         NULL,
    CONSTRAINT [PK_KRT_VallalkozasokHistory] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_KRT_VallalkozasokHistory_ID_VER]
    ON [dbo].[KRT_VallalkozasokHistory]([Id] ASC, [Ver] ASC);

