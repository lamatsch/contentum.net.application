if not exists (select 1 FROM sys.indexes WHERE name='MIG_Foszam_IX_EdokSav_UI_YEAR_UI_NUM' AND object_id = OBJECT_ID('MIG_Foszam'))
BEGIN
	CREATE NONCLUSTERED INDEX [MIG_Foszam_IX_EdokSav_UI_YEAR_UI_NUM] ON [dbo].[MIG_Foszam]
	(
		[EdokSav] ASC,
		[UI_YEAR] DESC,
		[UI_NUM] DESC,
		[Selejtezve] ASC
	)
END
GO

if exists (select 1 FROM sys.indexes WHERE name='MIG_Foszam_UI' AND object_id = OBJECT_ID('MIG_Foszam'))
BEGIN
	DROP INDEX [MIG_Foszam_UI] ON [dbo].[MIG_Foszam]
END

GO

if not exists (select 1 FROM sys.indexes WHERE name='MIG_Foszam_IX_Edok_Ugyintezo_Csoport_Id' AND object_id = OBJECT_ID('MIG_Foszam'))
BEGIN
	CREATE NONCLUSTERED INDEX [MIG_Foszam_IX_Edok_Ugyintezo_Csoport_Id] ON [dbo].[MIG_Foszam]
	(
		[Edok_Ugyintezo_Csoport_Id] ASC
	)
	INCLUDE ([Edok_Ugyintezo_Nev],[MIG_Eloado_Id])
	END
GO


