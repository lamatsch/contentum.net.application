﻿/*
--------------------------------------------------------------------------------------				
--------------------------------------------------------------------------------------
SCHEMA Update Script							
--------------------------------------------------------------------------------------				
--------------------------------------------------------------------------------------
*/

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'MIG_Forras_id' AND Object_ID = Object_ID(N'dbo.MIG_Alszam'))
BEGIN

	PRINT 'Add Column MIG_Alszam.MIG_Forras_id'

	ALTER TABLE [dbo].[MIG_Alszam] 
	ADD [MIG_Forras_id] [int] NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Adathordozo_tipusa' AND Object_ID = Object_ID(N'dbo.MIG_Alszam'))
BEGIN

	PRINT 'Add Column MIG_Alszam.Adathordozo_tipusa'

	ALTER TABLE [dbo].[MIG_Alszam] 
	ADD [Adathordozo_tipusa] [nvarchar](64) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Prioritas' AND Object_ID = Object_ID(N'dbo.MIG_Alszam'))
BEGIN

	PRINT 'Add Column MIG_Alszam.Prioritas'

	ALTER TABLE [dbo].[MIG_Alszam] 
	ADD [Prioritas] [nvarchar](10) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Irattipus' AND Object_ID = Object_ID(N'dbo.MIG_Alszam'))
BEGIN

	PRINT 'Add Column MIG_Alszam.Irattipus'

	ALTER TABLE [dbo].[MIG_Alszam] 
	ADD [Irattipus] [nvarchar](64) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Feladat' AND Object_ID = Object_ID(N'dbo.MIG_Alszam'))
BEGIN

	PRINT 'Add Column MIG_Alszam.Feladat'

	ALTER TABLE [dbo].[MIG_Alszam] 
	ADD [Feladat] [nvarchar](255) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Szervezet' AND Object_ID = Object_ID(N'dbo.MIG_Alszam'))
BEGIN

	PRINT 'Add Column MIG_Alszam.Szervezet'

	ALTER TABLE [dbo].[MIG_Alszam] 
	ADD [Szervezet] [nvarchar](100) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Dokumentum' AND Object_ID = Object_ID(N'dbo.MIG_Alszam'))
BEGIN

	PRINT 'Add Column MIG_Alszam.Dokumentum'

	ALTER TABLE [dbo].[MIG_Alszam] 
	ADD [Dokumentum] [nvarchar](1000) NULL
END
GO

IF col_length('MIG_Alszam','UGYHOL') != 2*3
BEGIN
	print 'ALTER COLUMN MIG_Alszam.UGYHOL'
	ALTER TABLE MIG_Alszam
	ALTER COLUMN UGYHOL nvarchar(3)
END

IF col_length('MIG_Alszam','Feladat') != 2*255
BEGIN
	print 'ALTER COLUMN MIG_Alszam.Feladat'
	ALTER TABLE MIG_Alszam
	ALTER COLUMN Feladat nvarchar(255)
END


IF col_length('MIG_AlszamHistory','UGYHOL') != 2*3
BEGIN
	print 'ALTER COLUMN MIG_AlszamHistory.UGYHOL'
	ALTER TABLE MIG_AlszamHistory
	ALTER COLUMN UGYHOL nvarchar(3)
END
--------------------------------------------------------------------------------------

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'MEGORZESI_IDO_ERTEK' AND Object_ID = Object_ID(N'dbo.CTXX_DBF'))
BEGIN

	PRINT 'Add Column CTXX_DBF.MEGORZESI_IDO_ERTEK'

	ALTER TABLE [dbo].[CTXX_DBF] 
	ADD [MEGORZESI_IDO_ERTEK] INT NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'MEGORZESI_IDOEGYSEG' AND Object_ID = Object_ID(N'dbo.CTXX_DBF'))
BEGIN

	PRINT 'Add Column CTXX_DBF.MEGORZESI_IDOEGYSEG'

	ALTER TABLE [dbo].[CTXX_DBF] 
	ADD [MEGORZESI_IDOEGYSEG] NVARCHAR (64) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'MEGORZESI_MOD' AND Object_ID = Object_ID(N'dbo.CTXX_DBF'))
BEGIN

	PRINT 'Add Column CTXX_DBF.MEGORZESI_MOD'

	ALTER TABLE [dbo].[CTXX_DBF] 
	ADD [MEGORZESI_MOD] NVARCHAR (2) NULL
END
GO

--------------- MIG_Jegyzekek--------------------------------------------------

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Allapot' AND Object_ID = Object_ID(N'dbo.MIG_Jegyzekek'))
BEGIN

	PRINT 'Add Column MIG_Jegyzekek.Allapot'

	ALTER TABLE [dbo].[MIG_Jegyzekek] 
	ADD [Allapot] NVARCHAR (64) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Allapot_Nev' AND Object_ID = Object_ID(N'dbo.MIG_Jegyzekek'))
BEGIN

	PRINT 'Add Column MIG_Jegyzekek.Allapot_Nev'

	ALTER TABLE [dbo].[MIG_Jegyzekek] 
	ADD [Allapot_Nev] NVARCHAR (400) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'VegrehajtasKezdoDatuma' AND Object_ID = Object_ID(N'dbo.MIG_Jegyzekek'))
BEGIN

	PRINT 'Add Column MIG_Jegyzekek.VegrehajtasKezdoDatuma'

	ALTER TABLE [dbo].[MIG_Jegyzekek] 
	ADD [VegrehajtasKezdoDatuma] DATETIME NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'MegsemmisitesDatuma' AND Object_ID = Object_ID(N'dbo.MIG_Jegyzekek'))
BEGIN

	PRINT 'Add Column MIG_Jegyzekek.MegsemmisitesDatuma'

	ALTER TABLE [dbo].[MIG_Jegyzekek] 
	ADD [MegsemmisitesDatuma] DATETIME NULL
END
GO

--------------- MIG_JegyzekekHistory--------------------------------------------------

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Allapot' AND Object_ID = Object_ID(N'dbo.MIG_JegyzekekHistory'))
BEGIN

	PRINT 'Add Column MIG_JegyzekekHistory.Allapot'

	ALTER TABLE [dbo].[MIG_JegyzekekHistory] 
	ADD [Allapot] NVARCHAR (64) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Allapot_Nev' AND Object_ID = Object_ID(N'dbo.MIG_JegyzekekHistory'))
BEGIN

	PRINT 'Add Column MIG_JegyzekekHistory.Allapot_Nev'

	ALTER TABLE [dbo].[MIG_JegyzekekHistory] 
	ADD [Allapot_Nev] NVARCHAR (400) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'VegrehajtasKezdoDatuma' AND Object_ID = Object_ID(N'dbo.MIG_JegyzekekHistory'))
BEGIN

	PRINT 'Add Column MIG_JegyzekekHistory.VegrehajtasKezdoDatuma'

	ALTER TABLE [dbo].[MIG_JegyzekekHistory] 
	ADD [VegrehajtasKezdoDatuma] DATETIME NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'MegsemmisitesDatuma' AND Object_ID = Object_ID(N'dbo.MIG_JegyzekekHistory'))
BEGIN

	PRINT 'Add Column MIG_JegyzekekHistory.MegsemmisitesDatuma'

	ALTER TABLE [dbo].[MIG_JegyzekekHistory] 
	ADD [MegsemmisitesDatuma] DATETIME NULL
END
GO

--------------- MIG_JegyzekTetelek--------------------------------------------------

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'AtadasDatuma' AND Object_ID = Object_ID(N'dbo.MIG_JegyzekTetelek'))
BEGIN

	PRINT 'Add Column MIG_JegyzekTetelek.AtadasDatuma'

	ALTER TABLE [dbo].[MIG_JegyzekTetelek] 
	ADD [AtadasDatuma] DATETIME NULL
END
GO

--------------- MIG_JegyzekTetelekHistory--------------------------------------------------

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'AtadasDatuma' AND Object_ID = Object_ID(N'dbo.MIG_JegyzekTetelekHistory'))
BEGIN

	PRINT 'Add Column MIG_JegyzekTetelekHistory.AtadasDatuma'

	ALTER TABLE [dbo].[MIG_JegyzekTetelekHistory] 
	ADD [AtadasDatuma] DATETIME NULL
END
GO

--------------- MIG_Dokumentum--------------------------------------------------

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'ErvVege' AND Object_ID = Object_ID(N'dbo.MIG_Dokumentum'))
BEGIN

	PRINT 'Add Column MIG_Dokumentum.ErvVege'

	ALTER TABLE [dbo].[MIG_Dokumentum] 
	ADD [ErvVege] DATETIME NULL
END
GO

--IF NOT EXISTS(SELECT * FROM SYS.OBJECTS WHERE TYPE = 'D' AND NAME = 'DF_MIG_Dokumentum_ErvVege')
--BEGIN
--  PRINT 'ADD CONSTRAINT DF_MIG_Dokumentum_ErvVege'

--  ALTER Table MIG_Dokumentum 
--  ADD CONSTRAINT DF_MIG_Dokumentum_ErvVege DEFAULT CONVERT(datetime, '4700-12-31', 102) FOR ErvVege;
--END

if NOT EXISTS (select 1 FROM sys.indexes WHERE NAME ='IX_MIG_Dokumentumok_ErvVege' 
AND object_id = OBJECT_ID('MIG_Dokumentum'))
BEGIN
	PRINT 'CREATE INDEX IX_MIG_Dokumentumok_ErvVege'

	CREATE INDEX IX_MIG_Dokumentumok_ErvVege on MIG_Dokumentum (
	ErvVege ASC
	)
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'ErvVege' AND Object_ID = Object_ID(N'dbo.MIG_DokumentumHistory'))
BEGIN

	PRINT 'Add Column MIG_DokumentumHistory.ErvVege'

	ALTER TABLE [dbo].[MIG_DokumentumHistory] 
	ADD [ErvVege] DATETIME NULL
END
GO