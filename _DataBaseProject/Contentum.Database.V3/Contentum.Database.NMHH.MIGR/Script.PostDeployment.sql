﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
---------------------------------------------------------------------------------------------------------------------
------------------- BUG_10802 - eMigration modulban Irat típusa mező érték javítása ---------------------------------
UPDATE [dbo].[MIG_Foszam]
SET Ugyirat_tipus = 'papír alapú'
WHERE Ugyirat_tipus = 'papĂr alapĂş' OR
Ugyirat_tipus = 'papĂ­r alapĂş' OR -- nem ugyanaz mint a felső (valójában ez volt a váglapon: [papĂ­-r alapĂş], de így jeleníti meg, és így kell a BOPMH-ban is legyen)
Ugyirat_tipus = 'papĂ­-r alapĂş' --így is beteszem

---------------------------------------------------------------------------------------------------------------------
------------------- BUG_10802 - eMigration modulban Irat típusa mező érték javítása ---------------------------------
---------------------------------------- * VÉGE * -------------------------------------------------------------------