﻿PRINT 'Executing: BUG_13233_BOPMH_IRATTARITERV_FIX'


declare @MegorzesiIdoErtekek table(MegorzesiIdo nvarchar(100), Ertek int)
INSERT INTO @MegorzesiIdoErtekek
VALUES 
('0', 0),
('1', 2),
('2', 5),
('3', 10),
('4', 15),
('5', 30),
('6', 75)

declare @idoegysegEv nvarchar(64) = '365'
declare @idoegysegNap nvarchar(64) = '1440'

--EV < 2010, SELEJTEZHETO:
UPDATE CTXX_DBF
SET MEGORZESI_IDO_ERTEK = (SELECT ertekek.Ertek FROM @MegorzesiIdoErtekek ertekek WHERE ertekek.MegorzesiIdo = MEGORZESI_IDO),
    MEGORZESI_IDOEGYSEG = @idoegysegEv,
	MEGORZESI_MOD = 'S'
WHERE
EV < 2010
AND isnull(KOD,'') = ''
AND MEGORZESI_IDO IS NOT NULL

--EV < 2010, LEVELTARI:
UPDATE CTXX_DBF
SET MEGORZESI_IDO_ERTEK = KOD,
    MEGORZESI_IDOEGYSEG = @idoegysegEv,
	MEGORZESI_MOD = 'L'
WHERE
EV < 2010
AND  isnull(KOD,'') != ''
AND  isnull(KOD,'') != 'HN'
AND TRY_CAST(KOD as INT) IS NOT NULL


UPDATE CTXX_DBF
SET MEGORZESI_IDO_ERTEK = 90,
    MEGORZESI_IDOEGYSEG = @idoegysegNap,
	MEGORZESI_MOD = 'L'
WHERE
EV < 2010
AND  isnull(KOD,'') = '90 nap'

--EV < 2010, HN:
UPDATE CTXX_DBF
SET MEGORZESI_MOD = 'N'
WHERE
EV < 2010
AND  isnull(KOD,'') = 'HN'

--EV >= 2010, SELEJTEZHETO:
UPDATE CTXX_DBF
SET MEGORZESI_IDO_ERTEK = MEGORZESI_IDO,
    MEGORZESI_IDOEGYSEG = @idoegysegEv,
	MEGORZESI_MOD = 'S'
WHERE
EV >= 2010 AND EV < 2016
AND isnull(KOD,'') = ''
AND TRY_CAST(MEGORZESI_IDO AS int) IS NOT NULL

--EV >= 2010, LEVELTARI:
UPDATE CTXX_DBF
SET MEGORZESI_IDO_ERTEK = KOD,
    MEGORZESI_IDOEGYSEG = @idoegysegEv,
	MEGORZESI_MOD = 'L'
WHERE
EV >= 2010 AND EV < 2016
AND isnull(KOD,'') != ''
AND  isnull(KOD,'') != 'HN'
AND TRY_CAST(KOD AS int) IS NOT NULL

UPDATE CTXX_DBF
SET MEGORZESI_IDO_ERTEK = 90,
    MEGORZESI_IDOEGYSEG = @idoegysegNap,
	MEGORZESI_MOD = 'L'
WHERE
EV >= 2010 AND EV < 2016
AND isnull(KOD,'') = '90 nap'

--EV >= 2010, HN:
UPDATE CTXX_DBF
SET MEGORZESI_MOD = 'N'
WHERE
EV >= 2010 AND EV < 2016
AND  isnull(KOD,'') = 'HN'

--EV >= 2016, SELEJTEZHETO:
UPDATE CTXX_DBF
SET MEGORZESI_IDO_ERTEK = KOD,
    MEGORZESI_IDOEGYSEG = @idoegysegEv,
	MEGORZESI_MOD = 'S'
WHERE
EV >= 2016
AND isnull(KOD,'') != 'NS'
AND TRY_CAST(KOD as int) IS NOT NULL

--EV >= 2016, LEVELTARI:
UPDATE CTXX_DBF
SET MEGORZESI_IDO_ERTEK = HATOS,
    MEGORZESI_IDOEGYSEG = @idoegysegEv,
	MEGORZESI_MOD = 'L'
WHERE
EV >= 2016
AND isnull(KOD,'') = 'NS'
AND ISNULL(HATOS, '') != 'HN'
AND TRY_CAST(HATOS as int) IS NOT NULL

UPDATE CTXX_DBF
SET MEGORZESI_IDO_ERTEK = 90,
    MEGORZESI_IDOEGYSEG = @idoegysegNap,
	MEGORZESI_MOD = 'L'
WHERE
EV >= 2016
AND isnull(KOD,'') = 'NS'
AND ISNULL(HATOS, '') = '#90 nap'

--EV >= 2016, HN:
UPDATE CTXX_DBF
SET MEGORZESI_MOD = 'N'
WHERE
EV >= 2016
AND isnull(KOD,'') = 'NS'
AND ISNULL(HATOS, '') = 'HN'

--Egyedi esetek kezelese
UPDATE CTXX_DBF
SET MEGORZESI_IDO_ERTEK = 50,
    MEGORZESI_IDOEGYSEG = @idoegysegEv,
	MEGORZESI_MOD = 'S'
WHERE
MEGORZESI_IDO = '50 (a jogviszony megszűnésétől)'
OR KOD = '50 (a jogviszony megszűnésétől)'
OR MEGORZESI_IDO = '50 '

UPDATE CTXX_DBF
SET MEGORZESI_IDO_ERTEK = 15,
    MEGORZESI_IDOEGYSEG = @idoegysegEv,
	MEGORZESI_MOD = 'S'
WHERE
MEGORZESI_IDO = '15 (visszavonását követően)'
OR KOD = '15 (visszavonását követően)'

UPDATE CTXX_DBF
SET MEGORZESI_IDO_ERTEK = 0,
    MEGORZESI_IDOEGYSEG = @idoegysegEv,
	MEGORZESI_MOD = 'S'
WHERE
MEGORZESI_IDO = 'visszavonását követően haladéktalanul'
OR MEGORZESI_IDO = 'lejárat után'
OR KOD = 'visszavonását követően haladéktalanul'
OR KOD = 'lejárat után'

UPDATE CTXX_DBF
SET MEGORZESI_IDO_ERTEK = 90,
    MEGORZESI_IDOEGYSEG = @idoegysegNap,
	MEGORZESI_MOD = 'L'
WHERE
KOD = '#90 nap'

UPDATE CTXX_DBF
	SET MEGORZESI_MOD = 'N'
WHERE (IRJEL = '352E0'
OR IRJEL = 'U611-NS'
OR IRJEL='36H0')
AND MEGORZESI_MOD IS NULL

UPDATE CTXX_DBF
SET MEGORZESI_MOD = 'N'
WHERE
MEGORZESI_IDO = '0'
AND MEGORZESI_MOD IS NULL

