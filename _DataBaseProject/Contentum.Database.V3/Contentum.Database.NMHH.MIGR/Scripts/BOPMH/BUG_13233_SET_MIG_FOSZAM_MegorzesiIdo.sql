﻿PRINT 'Executing: BUG_13233_SET_MIG_FOSZAM_MegorzesiIdo.sql'

create table #updated(FoszamId uniqueidentifier) 

UPDATE MIG_Foszam
	SET MegorzesiIdo = m.MegorzesiIdoVege,
		Ver=ISNULL(Ver,1)+1
OUTPUT deleted.Id INTO #updated
FROM MIG_Foszam
CROSS APPLY dbo.fn_GetMegorzesiIdoVege(UI_YEAR, UI_IRJ, IRJ2000, IRATTARBA) m
WHERE IRATTARBA IS NOT NULL

-- History

declare @Muvelet int = 1
declare @VegrehajtasIdo datetime = getdate()
declare @Vegrehajto_Id uniqueidentifier = '54E861A5-36ED-44CA-BAA7-C287D125B309'

ALTER INDEX MIG_FoszamHistory_IX_HistoryVegrehajtasIdo ON [MIG_FoszamHistory] DISABLE;

insert into MIG_FoszamHistory with(TABLOCKX) (HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,UI_SAV     
 ,UI_YEAR     
 ,UI_NUM     
 ,UI_NAME     
 ,UI_IRSZ     
 ,UI_UTCA     
 ,UI_HSZ     
 ,UI_HRSZ     
 ,UI_TYPE     
 ,UI_IRJ     
 ,UI_PERS     
 ,UI_OT_ID     
 ,MEMO     
 ,IRSZ_PLUSS     
 ,IRJ2000     
 ,Conc     
 ,Selejtezve     
 ,Selejtezes_Datuma     
 ,Csatolva_Rendszer     
 ,Csatolva_Id     
 ,MIG_Sav_Id     
 ,MIG_Varos_Id     
 ,MIG_IktatasTipus_Id     
 ,MIG_Eloado_Id     
 ,Edok_Utoirat_Id     
 ,Edok_Utoirat_Azon     
 ,UGYHOL     
 ,IRATTARBA     
 ,SCONTRO     
 ,Edok_Ugyintezo_Csoport_Id     
 ,Edok_Ugyintezo_Nev
 ,Ver
 ,MegorzesiIdo
 ,ugyirat_tipus
 ,IrattarId
 ,IrattariHely
  )
  SELECT @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
	,Id          
	,UI_SAV          
	,UI_YEAR          
	,UI_NUM          
	,UI_NAME          
	,UI_IRSZ          
	,UI_UTCA          
	,UI_HSZ          
	,UI_HRSZ          
	,UI_TYPE          
	,UI_IRJ          
	,UI_PERS          
	,UI_OT_ID          
	,MEMO          
	,IRSZ_PLUSS          
	,IRJ2000          
	,Conc          
	,Selejtezve          
	,Selejtezes_Datuma          
	,Csatolva_Rendszer          
	,Csatolva_Id          
	,MIG_Sav_Id          
	,MIG_Varos_Id          
	,MIG_IktatasTipus_Id          
	,MIG_Eloado_Id          
	,Edok_Utoirat_Id          
	,Edok_Utoirat_Azon          
	,UGYHOL          
	,IRATTARBA          
	,SCONTRO          
	,Edok_Ugyintezo_Csoport_Id          
	,Edok_Ugyintezo_Nev
	,Ver
	,MegorzesiIdo
	,Ugyirat_tipus
	,IrattarId
	,IrattariHely
 FROM MIG_Foszam
 JOIN #updated on MIG_Foszam.Id = #updated.FoszamId

 ALTER INDEX MIG_FoszamHistory_IX_HistoryVegrehajtasIdo ON [MIG_FoszamHistory] REBUILD;

 drop table #updated