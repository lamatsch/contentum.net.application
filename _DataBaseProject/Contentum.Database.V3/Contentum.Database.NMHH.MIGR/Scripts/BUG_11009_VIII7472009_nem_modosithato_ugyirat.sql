IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = 'MIG_Dokumentum_IX_MIG_Alszam_Id' AND object_id = OBJECT_ID('[MIG_Dokumentum]'))
BEGIN
PRINT 'Creating nonclustered index on MIG_Dokumentum.Mig_Alszam_id'
	CREATE NONCLUSTERED INDEX   [MIG_Dokumentum_IX_MIG_Alszam_Id] ON [dbo].[MIG_Dokumentum] 
	(
		[MIG_Alszam_Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
END
GO
