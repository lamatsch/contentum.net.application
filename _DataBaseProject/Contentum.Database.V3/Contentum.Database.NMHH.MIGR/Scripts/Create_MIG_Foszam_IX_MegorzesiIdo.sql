
IF NOT EXISTS (SELECT 1 FROM sys.indexes 
WHERE name='MIG_Foszam_IX_MegorzesiIdo' AND object_id = OBJECT_ID('dbo.MIG_Foszam'))
BEGIN
	PRINT 'CREATE INDEX MIG_Foszam_IX_MegorzesiIdo'
	CREATE NONCLUSTERED INDEX MIG_Foszam_IX_MegorzesiIdo
	ON [dbo].[MIG_Foszam] ([MegorzesiIdo])
	INCLUDE ([Id],[UI_YEAR],[UI_IRJ],[IRJ2000])
END
GO

