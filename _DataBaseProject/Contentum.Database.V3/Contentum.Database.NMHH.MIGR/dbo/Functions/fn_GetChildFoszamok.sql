﻿CREATE FUNCTION [dbo].[fn_GetChildFoszamok]
(	
	@FoszamId UNIQUEIDENTIFIER
	
)
RETURNS @ValueTable table (Id UNIQUEIDENTIFIER)
AS
BEGIN
 WITH FoszamHierarchy  AS
	(
	   -- Base case
	   SELECT *,
			  0 as HierarchyLevel
	   FROM MIG_Foszam
	   WHERE Id = @FoszamId

	   UNION ALL

	   -- Recursive step
	   SELECT f.*,
		  fh.HierarchyLevel + 1 AS HierarchyLevel
	   FROM MIG_Foszam as f
		  INNER JOIN  FoszamHierarchy as fh ON
			 f.Id = fh.Csatolva_Id
	)
	
	INSERT INTO @ValueTable SELECT Id From FoszamHierarchy
RETURN
END