﻿/****** Object:  UserDefinedFunction [dbo].[fn_GetFieldrowsFromTable]    Script Date: 2017.10.24. 12:39:10 ******/
/*
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
*/
CREATE FUNCTION [dbo].[fn_GetFieldrowsFromTable]
(	
	@FieldName nvarchar(100),
	@TableName nvarchar(100)
	
)
RETURNS @ValueTable TABLE (ERTEK nvarchar(255))
AS
BEGIN
  declare @sqlcmd nvarchar(max);

  SET @sqlcmd = N'insert into @ValueTable select ' + @FieldName + ' from ' + @TableName + ';';
  execute sp_executesql @sqlcmd, N'@FieldName nvarchar(100), @TableName nvarchar(100)'
                               , @FieldName = @FieldName, @TableName = @TableName;

RETURN
END;