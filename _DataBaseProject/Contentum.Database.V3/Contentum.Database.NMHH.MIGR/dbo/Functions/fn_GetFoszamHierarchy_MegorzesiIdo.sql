﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetFoszamHierarchy_MegorzesiIdo]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fn_GetFoszamHierarchy_MegorzesiIdo]
GO

CREATE FUNCTION [dbo].[fn_GetFoszamHierarchy_MegorzesiIdo] 
(
	@FoszamId uniqueidentifier
)

RETURNS TABLE
AS
RETURN
(
	WITH FoszamChilds  AS
	(
	   -- Base case
	   SELECT Id, Csatolva_Id, MegorzesiIdo, 0 as HierarchyLevel
	   FROM MIG_Foszam
	   WHERE Id = @FoszamId

	   UNION ALL

	   -- Recursive step
	   SELECT f.Id, f.Csatolva_Id, f.MegorzesiIdo, child.HierarchyLevel - 1 AS HierarchyLevel
	   FROM MIG_Foszam as f
		  INNER JOIN FoszamChilds as child ON
			 f.Csatolva_Id = child.Id
	),
	FoszamParents as
	(
	   -- Base case
	   SELECT Id, Csatolva_Id, MegorzesiIdo, 0 as HierarchyLevel
	   FROM MIG_Foszam
	   WHERE Id = @FoszamId

	   UNION ALL

	   -- Recursive step
	   SELECT f.Id, f.Csatolva_Id, f.MegorzesiIdo, parent.HierarchyLevel + 1 AS HierarchyLevel
	   FROM MIG_Foszam as f
		  INNER JOIN  FoszamParents as parent ON
			 f.Id = parent.Csatolva_Id
	),
	FoszamHierarchy as
	(
		select * from FoszamChilds
		union all
		select * from FoszamParents
		where HierarchyLevel > 0
	)


	SELECT MAX(ISNULL(fh.MegorzesiIdo, '4700-12-31')) MaxMegorzesiIdo
	FROM FoszamHierarchy fh

)



