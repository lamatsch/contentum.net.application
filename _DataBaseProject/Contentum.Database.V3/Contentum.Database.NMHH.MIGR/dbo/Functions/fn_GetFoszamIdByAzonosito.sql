﻿CREATE FUNCTION [dbo].[fn_GetFoszamIdByAzonosito]
(	
	@Azonosito NVARCHAR(1000)
	
)
RETURNS UNIQUEIDENTIFIER 
AS
BEGIN

DECLARE @ret UNIQUEIDENTIFIER

DECLARE @Index1 INT

SET @Index1 = CHARINDEX('/', @Azonosito)

IF @Index1 = 0
BEGIN
	RETURN NULL
END

DECLARE @Index2 INT

SET @Index2 = CHARINDEX('/', @Azonosito, @Index1 + 1)

IF @Index2 = 0
BEGIN
	RETURN NULL
END

DECLARE @Sav NVARCHAR(100)

SET @Sav = SUBSTRING(@Azonosito , 0 ,@Index1)

IF CHARINDEX('(', @Sav) = 0
BEGIN
	RETURN NULL
END

IF CHARINDEX(')', @Sav) = 0
BEGIN
	RETURN NULL
END

IF CHARINDEX('(', @Sav) >= CHARINDEX(')', @Sav)
BEGIN
	RETURN NULL
END

SET @Sav = SUBSTRING(@Sav ,CHARINDEX('(', @Sav) + 1, CHARINDEX(')',@Sav) - CHARINDEX('(', @Sav) -1)

DECLARE @Foszam NVARCHAR(100)

SET @Foszam = SUBSTRING(@Azonosito , @Index1 + 1 ,@Index2 - @Index1 - 1)

DECLARE @Ev NVARCHAR(100)

SET @Ev = SUBSTRING(@Azonosito , @Index2 + 1 ,LEN(@Azonosito) - @Index2)

SET @ret = (SELECT [MIG_Foszam].[Id] FROM [MIG_Foszam]
--INNER JOIN [MIG_Sav] ON [MIG_Sav].[Id] = [MIG_Foszam].[MIG_Sav_Id]
--WHERE SUBSTRING(MIG_Sav.NAME,2, CHARINDEX(')', MIG_Sav.NAME) - 2 ) = @Sav
WHERE MIG_Foszam.EdokSav = @Sav
AND [MIG_Foszam].[UI_NUM] = @Foszam
AND [MIG_Foszam].[UI_YEAR] = @Ev 
--AND CHARINDEX('(', MIG_Sav.NAME) = 1 AND CHARINDEX(')', MIG_Sav.NAME) > 2
)
            
			  
RETURN @ret

END