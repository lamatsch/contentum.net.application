﻿CREATE FUNCTION [dbo].[fn_GetFoszamokHierarchy]
(	
	@FoszamId UNIQUEIDENTIFIER
	
)
RETURNS @ValueTable table (Id UNIQUEIDENTIFIER)
AS
BEGIN
-- MIG hierarchia lekérése
-- 1. leszármazottak
	WITH FoszamHierarchy  AS
	(
	   -- Base case
	   SELECT Id, Csatolva_Id, 0 as HierarchyLevel
	   FROM MIG_Foszam
	   WHERE Id = @FoszamId

	   UNION ALL

	   -- Recursive step
	   SELECT f.Id, f.Csatolva_Id, fh.HierarchyLevel - 1 AS HierarchyLevel
	   FROM MIG_Foszam as f
		  INNER JOIN  FoszamHierarchy as fh ON
			 f.Csatolva_Id = fh.Id
	)
	
	INSERT INTO @ValueTable
	    SELECT Id FROM FoszamHierarchy;
-- 2. szülők
	WITH FoszamHierarchy  AS
	(
	   -- Base case
	   SELECT Id, Csatolva_Id, 0 as HierarchyLevel
	   FROM MIG_Foszam
	   WHERE Id = @FoszamId

	   UNION ALL

	   -- Recursive step
	   SELECT f.Id, f.Csatolva_Id, fh.HierarchyLevel + 1 AS HierarchyLevel
	   FROM MIG_Foszam as f
		  INNER JOIN  FoszamHierarchy as fh ON
			 f.Id = fh.Csatolva_Id
	)
	
	INSERT INTO @ValueTable
	    SELECT Id FROM FoszamHierarchy WHERE HierarchyLevel > 0

RETURN
END