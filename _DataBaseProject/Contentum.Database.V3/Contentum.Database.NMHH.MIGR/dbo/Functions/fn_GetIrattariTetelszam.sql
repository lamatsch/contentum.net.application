﻿CREATE FUNCTION [dbo].[fn_GetIrattariTetelszam]
(	
	@UI_YEAR INT,
	@UI_TYPE NVARCHAR(4)
	
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400)

IF @UI_YEAR >= 2000
BEGIN
	SET @ret = substring(convert(nvarchar, @UI_TYPE), 1 , 3)
END
ELSE
BEGIN
	RETURN NULL
END	  

RETURN @ret

END