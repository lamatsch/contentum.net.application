﻿CREATE FUNCTION [dbo].[fn_GetLastAlszam]
(	
	@Foszam_Id UNIQUEIDENTIFIER
	
)
RETURNS TABLE
AS
RETURN
(
  
	SELECT TOP 1 *
		from Mig_Alszam
		WHERE [MIG_Foszam_Id] = @Foszam_Id
		ORDER BY [ALNO] DESC  

);