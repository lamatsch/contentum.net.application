﻿CREATE FUNCTION [dbo].[fn_GetMIGFoszam]
(	
	@MIG_Foszam_Id UNIQUEIDENTIFIER
	
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400)

DECLARE @SavName NVARCHAR(60)
DECLARE @Foszam INT
DECLARE @Ev INT

SELECT @SavName = [MIG_Sav].[NAME],
	   @Foszam = [MIG_Foszam].[UI_NUM],
	   @Ev = [MIG_Foszam].[UI_YEAR]
 FROM [MIG_Foszam]
 INNER JOIN [MIG_Sav] ON [MIG_Sav].[Id] = [MIG_Foszam].[MIG_Sav_Id]
 WHERE [MIG_Foszam].[Id] = @MIG_Foszam_Id

SET @ret = dbo.fn_MergeFoszam(@SavName,@Foszam,@Ev) 			  
RETURN @ret

END