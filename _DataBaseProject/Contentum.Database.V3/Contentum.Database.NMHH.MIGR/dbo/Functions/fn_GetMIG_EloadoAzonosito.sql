﻿CREATE FUNCTION [dbo].[fn_GetMIG_EloadoAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = NAME from MIG_Eloado where Id = @Obj_Id;
   
   RETURN @Return;

END