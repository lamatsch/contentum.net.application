﻿CREATE FUNCTION [dbo].[fn_GetMIG_FoszamAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = dbo.fn_MergeFoszam(MIG_Sav.Name, MIG_Foszam.UI_NUM, MIG_Foszam.UI_YEAR)
		from MIG_Foszam
		left join MIG_Sav on MIG_Sav.Id = MIG_Foszam.MIG_Sav_Id
	where MIG_Foszam.Id = @Obj_Id;
   
   RETURN @Return;

END