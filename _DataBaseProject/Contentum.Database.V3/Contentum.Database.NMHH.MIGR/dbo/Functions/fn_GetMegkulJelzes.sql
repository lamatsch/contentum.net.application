﻿CREATE FUNCTION [dbo].[fn_GetMegkulJelzes]
(	
	@SavName NVARCHAR(60)
	
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400)

IF CHARINDEX('(', @SavName) = 1 AND CHARINDEX(')', @SavName) > 2
BEGIN
	SET @ret = substring(@SavName, 2, CHARINDEX(')', @SavName) - 2)
END
ELSE
BEGIN
	RETURN NULL
END 

RETURN @ret

END