set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetMegorzesiMod]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fn_GetMegorzesiMod]
GO

CREATE FUNCTION [dbo].[fn_GetMegorzesiMod]
(	
	@UI_YEAR int,
	@UI_IRJ nvarchar(6),
	@IRJ2000 nvarchar(20)
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		MEGORZESI_MOD MegorzesiMod
	FROM dbo.CTXX_DBF
	WHERE EV = CAST(@UI_YEAR as nvarchar(4))
	AND IRJEL = (CASE WHEN @UI_YEAR < 2000 THEN @UI_IRJ ELSE @IRJ2000 END)
	AND MEGORZESI_MOD IS NOT NULL
)
