﻿CREATE FUNCTION [dbo].[fn_MergeFoszam]
(	
	@SavName NVARCHAR(60),
	@Foszam INT,
	@Ev INT
	
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400)

IF (@SavName IS NOT NULL) AND (@Foszam IS NOT NULL) AND (@Ev IS NOT NULL)
BEGIN
	IF(CHARINDEX('(', @SavName) = 1 AND CHARINDEX(')', @SavName) > 2)
	BEGIN
		SET @SavName =  SUBSTRING(@SavName,2, CHARINDEX(')', @SavName) - 2 )
	END
	SET @ret = '(' + @SavName + ')' + ' /' + CAST(@Foszam AS NVARCHAR) + '/' + CAST(@Ev AS NVARCHAR)
	RETURN @ret
END

RETURN NULL

END