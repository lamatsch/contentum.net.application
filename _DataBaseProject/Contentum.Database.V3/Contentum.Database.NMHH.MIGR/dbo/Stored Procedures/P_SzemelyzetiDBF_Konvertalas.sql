﻿create procedure [dbo].[P_SzemelyzetiDBF_Konvertalas]
as
begin
---------------------------------------------------------
---------------------------------------------------------
-- Személyzeti adatok konvertálása a közös DBF táblákba
-- az Általános adatok betöltésük után
-- ( DBF1 -ből --> DBF-be konvertálás)
---------------------------------------------------------

---------------------------------------------------------
-- I. 
-- ÁLTALÁNOS adatok "kozmetikázása"
---------------------------------------------------------

-- Az Általános adatok között esetlegesen lévő
-- 53-as sávra vonatkozó adatok hibássá nyilvánítása
-- (Err_kod := 10 beállításával)
-- ui: az 53-as sáv majd a személyzeti lesz!!!

------
update FSXX_DBF
   set err_kod = 10
 where ui_sav  = '53'

print 'FSXX_DBF: updating '+convert(varchar,@@rowcount)+' rows'
 
------
update ALXX_DBF
   set err_kod = 10
 where uisav  = '53'

print 'ALXX_DBF: updating '+convert(varchar,@@rowcount)+' rows'

------
update TSXX_DBF
   set err_kod = 10
 where ui_sav  = '53'

print 'TSXX_DBF: updating '+convert(varchar,@@rowcount)+' rows'

------
update CSXX_DBF
   set err_kod = 10
 where ssav  = '53'
    or tsav  = '53'

print 'CSXX_DBF: updating '+convert(varchar,@@rowcount)+' rows'

---------------------------------------------------------
-- II. 
-- SZEMÉLYZETI adatok konvertált átmásolása 
-- az ÁLTALÁNOS adatok közé (DBF1 -ből DBF -be)
---------------------------------------------------------

-- FSXX_DBF 24 oszlop
set identity_insert FSXX_DBF on

insert into FSXX_DBF
      (UI_SAV,
       UI_YEAR,UI_NUM,UI_NAME,UI_IRSZ,UI_UTCA,UI_HSZ,UI_HRSZ,UI_CIMID,UI_TYPE,UI_IRJ,UI_PERS,
       MEMO,UI_PREV,UI_NEXT,UI_ALNO,UI_TSNO,UI_KCSAT,UI_OT_ID,IRSZ_PLUSS,CSOMAG,IRJ2000,
       IND,
       ERR_KOD)
select (CASE When ui_year <= '1997' Then  '32' Else '53' END) as UI_SAV,
       UI_YEAR,UI_NUM,UI_NAME,UI_IRSZ,UI_UTCA,UI_HSZ,UI_HRSZ,UI_CIMID,UI_TYPE,UI_IRJ,UI_PERS,
       MEMO,UI_PREV,UI_NEXT,UI_ALNO,UI_TSNO,UI_KCSAT,UI_OT_ID,IRSZ_PLUSS,CSOMAG,IRJ2000,
       IND+100000000 as IND, -- 100 millióval nagyobb IND-et veszünk, ezzel jelezzük, hogy személyzeti adat volt!
       ERR_KOD
  from FSXX_DBF1

set identity_insert FSXX_DBF off

---------------------------------------------------------
-- ALXX_DBF 28 oszlop
set identity_insert ALXX_DBF on

insert into ALXX_DBF 
      (UISAV,
       UIYEAR,UINUM,ALNO,ERK,IKTAT,BK,BKNEV,EIKOD,ELINT,UGYHOL,IRATTARBA,SCONTRO,ELOAD,
       ROGZIT,LHAT,KIADM,MEMO,MJ,IDNUM,ATDATE,ATADO,CIMZETT,JELL,ATVEVO,ATVEVE,
       IND,
       ERR_KOD)
select (CASE When uiyear <= '1997' Then  '32' Else '53' END) as UISAV,
       UIYEAR,UINUM,ALNO,ERK,IKTAT,BK,BKNEV,EIKOD,ELINT,UGYHOL,IRATTARBA,SCONTRO,ELOAD,
       ROGZIT,LHAT,KIADM,MEMO,MJ,IDNUM,ATDATE,ATADO,CIMZETT,JELL,ATVEVO,ATVEVE,
       IND+100000000 as IND, -- 100 millióval nagyobb IND-et veszünk, ezzel jelezzük, hogy személyzeti adat volt!
       ERR_KOD
  from ALXX_DBF1

set identity_insert ALXX_DBF off

---------------------------------------------------------
-- TSXX_DBF 12 oszlop
set identity_insert TSXX_DBF on

insert into TSXX_DBF
      (UI_SAV,
       UI_YEAR,UI_NAME,ALNO,NEV,IRSZ,UTCA,HSZ,CIMID,IRSZ_PLUSS,
       IND,
       ERR_KOD)
select (CASE When ui_year <= '1997' Then  '32' Else '53' END) as UI_SAV,
       UI_YEAR,UI_NAME,ALNO,NEV,IRSZ,UTCA,HSZ,CIMID,IRSZ_PLUSS,
       IND+100000000 as IND, -- 100 millióval nagyobb IND-et veszünk, ezzel jelezzük, hogy személyzeti adat volt!
       ERR_KOD
  from TSXX_DBF1

set identity_insert TSXX_DBF off

---------------------------------------------------------
-- CSXX_DBF 12 oszlop
set identity_insert CSXX_DBF on

insert into CSXX_DBF 
      (SSAV,
       SEV,SNUM,SF_G,SHEAD,
       TSAV,
       TEV,TNUM,TF_G,THEAD,
       IND,
       ERR_KOD)
select (CASE When sev <= '1997' Then  '32' Else '53' END) as SSAV,
       SEV,SNUM,SF_G,SHEAD,
       (CASE When tev <= '1997' Then  '32' Else '53' END) as TSAV,
       TEV,TNUM,TF_G,THEAD,
       IND+100000000 as IND, -- 100 millióval nagyobb IND-et veszünk, ezzel jelezzük, hogy személyzeti adat volt!
       ERR_KOD
  from CSXX_DBF1

set identity_insert CSXX_DBF off

---------------------------------------------------------
-- VAROS_DBF 6 oszlop
-- mj: nem kell áthozni semmit a személyzeti VAROS_DBF-ből,
--     mert teljesen megegyezik az általános VAROS_DBF-fel!!
set identity_insert VAROS_DBF on

insert into VAROS_DBF 
      (NAME,KOD,SKOD,MEGYEKOD,
       IND,
       ERR_KOD)
select NAME,KOD,SKOD,MEGYEKOD,
       IND+100000000 as IND, -- 100 millióval nagyobb IND-et veszünk, ezzel jelezzük, hogy személyzeti adat volt!
       ERR_KOD
  from VAROS_DBF1
 where 1=2 

set identity_insert VAROS_DBF off

---------------------------------------------------------
-- SIXX_DBF 7 oszlop
-- mj: csak 2 sort hozunk át, azt is módosítva 
--     az 1-es a 32-es sávra, a személyzeti SIXX_DBF-ből,
--     hogy az 1992-1993-as időszakra is legyen külön személyzeti ügyosztály sáv!!
set identity_insert SIXX_DBF on

insert into SIXX_DBF 
      (EV,
       NAME,
       S_ID,
       D_TOL,D_IG,
       IND,
       ERR_KOD)
select EV,
       '(16) Személyzeti ÜO' as NAME,
       '0032' as S_ID,
       D_TOL,D_IG,
       IND+100000000 as IND, -- 100 millióval nagyobb IND-et veszünk, ezzel jelezzük, hogy személyzeti adat volt!
       ERR_KOD
  from SIXX_DBF1
 where s_id = '0001'
   and ev in ('1992','1993')

set identity_insert SIXX_DBF off

---------------------------------------------------------
-- CIXX_DBF 8 oszlop
-- mj: átvesszük az összes sort a személyzeti CIXX_DBF-ből,
--     mivel a kod -ok különböző hosszúak bennük ...
--    (lehetne próbálkozni a személyzeti kod balról '0'-val való kiegészítésével,
--     de csak bonyolódna a dolog ...)
--
set identity_insert CIXX_DBF on

insert into CIXX_DBF 
      (EV,NAME,KOD,VALID,SZERVEZET,WT,
       IND,
       ERR_KOD)
select EV,NAME,KOD,VALID,SZERVEZET,WT,
       IND+100000000 as IND, -- 100 millióval nagyobb IND-et veszünk, ezzel jelezzük, hogy személyzeti adat volt!
       ERR_KOD
  from CIXX_DBF1

set identity_insert CIXX_DBF off

---------------------------------------------------------
-- CTXX_DBF 9 oszlop
-- mj: csak az 1995-1997 évek összes sorát vesszük át a személyzeti CTXX_DBF-ből,
--     mivel itt azonos (ev,kod) mellett teljesen eltérőek a (name,irjel) adatok!
--     természetesen az (ev,kod) egyediség megtartása miatt az átvett kod-ot transzformálni kell,
--     s ez legegyszerűbben talán a 9000 hozzáadásával történhet,
--     azaz a '0000'-'0131' kódokból '9000'-'9131' lesz ...
-- mj: a 2002-2007 években is vannak azonos (ev,kod) mellett eltérő irjel -ek,
--     de ott a name azonos, tehát itt nem fogjunk már átvenni személyzeti CTXX_DBF szótár adatokat,
--     hanem inkább a személyzeti FSXX_DBF -ben módosítjuk az irj2000 hivatkozást!!!
--
set identity_insert CTXX_DBF on

insert into CTXX_DBF 
      (EV,NAME,
       KOD,
       IRJEL,UI,HATOS,KIADM,
       IND,
       ERR_KOD)
select EV,NAME,
       '9'+substring(KOD,2,3) as KOD,
       IRJEL,UI,HATOS,KIADM,
       IND+100000000 as IND, -- 100 millióval nagyobb IND-et veszünk, ezzel jelezzük, hogy személyzeti adat volt!
       ERR_KOD
  from CTXX_DBF1
 where ev in ('1995','1996','1997')

set identity_insert CTXX_DBF off


update FSXX_DBF
   set irj2000 = CASE When irj2000='136U2'  Then '136U3'
                      When irj2000='032U2-' Then '032U3-'
                      When irj2000='100U1-' Then '100U2-'
                      When irj2000='108U2-' Then '108U1-'
                      When irj2000='112U1-' Then '112U2-'
                      When irj2000='116U1'  Then '116U1-'
                                            Else irj2000
                 END
 where ind > 100000000 -- ezek a személyzetiek
   and (1=2
    or (ui_year='2002' and irj2000='136U2')
    or (ui_year='2003' and irj2000='136U2')
    or (ui_year='2007' and irj2000 in ('032U2-','100U1-','108U2-','112U1-','116U1'))
       ) 

---------------------------------------------------------
end