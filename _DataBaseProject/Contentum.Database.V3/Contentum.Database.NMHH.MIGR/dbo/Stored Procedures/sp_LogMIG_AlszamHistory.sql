﻿create procedure [dbo].[sp_LogMIG_AlszamHistory]
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from MIG_Alszam where Id = @Row_Id;
      
   insert into MIG_AlszamHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,UI_SAV     
 ,UI_YEAR     
 ,UI_NUM     
 ,ALNO     
 ,ERK     
 ,IKTAT     
 ,BKNEV     
 ,ELINT     
 ,UGYHOL     
 ,IRATTARBA     
 ,SCONTRO     
 ,ELOAD     
 ,ROGZIT     
 ,MJ     
 ,IDNUM     
 ,MIG_Foszam_Id     
 ,MIG_Eloado_Id     
 ,MIG_Sav_Id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,UI_SAV          
 ,UI_YEAR          
 ,UI_NUM          
 ,ALNO          
 ,ERK          
 ,IKTAT          
 ,BKNEV          
 ,ELINT          
 ,UGYHOL          
 ,IRATTARBA          
 ,SCONTRO          
 ,ELOAD          
 ,ROGZIT          
 ,MJ          
 ,IDNUM          
 ,MIG_Foszam_Id          
 ,MIG_Eloado_Id          
 ,MIG_Sav_Id        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end