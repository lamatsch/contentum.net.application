
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_LogMIG_DokumentumHistory')
            and   type = 'P')
   drop procedure sp_LogMIG_DokumentumHistory
go

create procedure sp_LogMIG_DokumentumHistory
		 @Row_Id uniqueidentifier
		,@Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from MIG_Dokumentum where Id = @Row_Id;
      
   insert into MIG_DokumentumHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,FajlNev     
 ,Tipus     
 ,Formatum     
 ,Leiras     
 ,External_Source     
 ,External_Link     
 ,BarCode     
 ,Allapot     
 ,ElektronikusAlairas     
 ,AlairasFelulvizsgalat     
 ,MIG_Alszam_Id     
 ,Ver     
 ,ErvVege   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,FajlNev          
 ,Tipus          
 ,Formatum          
 ,Leiras          
 ,External_Source          
 ,External_Link          
 ,BarCode          
 ,Allapot          
 ,ElektronikusAlairas          
 ,AlairasFelulvizsgalat          
 ,MIG_Alszam_Id          
 ,Ver          
 ,ErvVege        from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go