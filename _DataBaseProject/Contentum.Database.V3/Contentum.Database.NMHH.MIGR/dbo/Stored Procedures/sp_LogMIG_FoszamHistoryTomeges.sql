﻿
create procedure sp_LogMIG_FoszamHistoryTomeges
		 @Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   select * into #tempTable from MIG_Foszam where Id IN (SELECT Id FROM #filter);
      
   insert into MIG_FoszamHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,UI_SAV     
 ,UI_YEAR     
 ,UI_NUM     
 ,UI_NAME     
 ,UI_IRSZ     
 ,UI_UTCA     
 ,UI_HSZ     
 ,UI_HRSZ     
 ,UI_TYPE     
 ,UI_IRJ     
 ,UI_PERS     
 ,UI_OT_ID     
 ,MEMO     
 ,IRSZ_PLUSS     
 ,IRJ2000     
 ,Conc     
 ,Selejtezve     
 ,Selejtezes_Datuma     
 ,Csatolva_Rendszer     
 ,Csatolva_Id     
 ,MIG_Sav_Id     
 ,MIG_Varos_Id     
 ,MIG_IktatasTipus_Id     
 ,MIG_Eloado_Id     
 ,Edok_Utoirat_Id     
 ,Edok_Utoirat_Azon     
 ,UGYHOL     
 ,IRATTARBA     
 ,SCONTRO     
 ,Edok_Ugyintezo_Csoport_Id     
 ,Edok_Ugyintezo_Nev
 ,Ver
 ,MegorzesiIdo
 ,IrattarId
 ,IrattariHely)  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,UI_SAV          
 ,UI_YEAR          
 ,UI_NUM          
 ,UI_NAME          
 ,UI_IRSZ          
 ,UI_UTCA          
 ,UI_HSZ          
 ,UI_HRSZ          
 ,UI_TYPE          
 ,UI_IRJ          
 ,UI_PERS          
 ,UI_OT_ID          
 ,MEMO          
 ,IRSZ_PLUSS          
 ,IRJ2000          
 ,Conc          
 ,Selejtezve          
 ,Selejtezes_Datuma          
 ,Csatolva_Rendszer          
 ,Csatolva_Id          
 ,MIG_Sav_Id          
 ,MIG_Varos_Id          
 ,MIG_IktatasTipus_Id          
 ,MIG_Eloado_Id          
 ,Edok_Utoirat_Id          
 ,Edok_Utoirat_Azon          
 ,UGYHOL          
 ,IRATTARBA          
 ,SCONTRO          
 ,Edok_Ugyintezo_Csoport_Id          
 ,Edok_Ugyintezo_Nev
 ,Ver
 ,MegorzesiIdo
 ,IrattarId
 ,IrattariHely from #tempTable t;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end