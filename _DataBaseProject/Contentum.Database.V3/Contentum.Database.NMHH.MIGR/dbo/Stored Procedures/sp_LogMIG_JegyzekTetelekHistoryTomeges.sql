﻿create procedure [dbo].[sp_LogMIG_JegyzekTetelekHistoryTomeges]
		 @Muvelet int
		,@Vegrehajto_Id uniqueidentifier
		,@VegrehajtasIdo datetime
		

as
begin
BEGIN TRY

   set nocount on
   
   insert into MIG_JegyzekTetelekHistory(HistoryMuvelet_Id,HistoryVegrehajto_Id,HistoryVegrehajtasIdo     
 ,Id     
 ,Jegyzek_Id     
 ,MIG_Foszam_Id     
 ,UGYHOL     
 ,AtvevoIktatoszama     
 ,Megjegyzes     
 ,Azonosito     
 ,SztornozasDat     
 ,Ver     
 ,Note     
 ,Letrehozo_id     
 ,LetrehozasIdo     
 ,Modosito_id     
 ,ModositasIdo     
 ,Tranz_id   )  
   select @Muvelet,@Vegrehajto_Id,@VegrehajtasIdo     
 ,Id          
 ,Jegyzek_Id          
 ,MIG_Foszam_Id          
 ,UGYHOL          
 ,AtvevoIktatoszama          
 ,Megjegyzes          
 ,Azonosito          
 ,SztornozasDat          
 ,Ver          
 ,Note          
 ,Letrehozo_id          
 ,LetrehozasIdo          
 ,Modosito_id          
 ,ModositasIdo          
 ,Tranz_id        from MIG_JegyzekTetelek where Id IN (SELECT Id FROM #result);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end