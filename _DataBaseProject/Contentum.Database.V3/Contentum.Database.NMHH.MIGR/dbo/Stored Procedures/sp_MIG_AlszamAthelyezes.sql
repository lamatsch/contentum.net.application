﻿create procedure [dbo].[sp_MIG_AlszamAthelyezes]
             @Id                 uniqueidentifier,
             @Ugyhova            nvarchar(1),      -- '0','1',...,'9'
             @SkontroVege        datetime = null
as

/*
--
-- KĂ‰SZĂŤTETTE:   ĂdĂˇm AndrĂˇs       2007.12.21
-- MĂ“DOSĂŤTOTTA: Boda Eszter        2008.04.04
--              1. SkontrĂł vĂ©ge az alkalmazĂˇs felĹ‘l jĂ¶n, mint Ăşj paramĂ©ter
--              2. HibaĂĽzenetek helyett hibakĂłdok, az alkalmazĂˇs oldja fel
--                  [56001] 'Az ĂĽgyirat (alszĂˇm) megadĂˇsa kĂ¶telezĹ‘!'
--                  [56002] 'MIG_Alszam select hiba!'
--                  [56003] 'A megadott ĂĽgyirat nem lĂ©tezik!'
--                  [56004] 'Az ĂĽgyirat Ăşj helyĂ©nek megadĂˇsa kĂ¶telezĹ‘!'
--                  [56005] 'Az ĂĽgyirat Ăşj helyĂ©nek megadĂˇsa hibĂˇs!'
--                  [56006] 'Az ĂĽgyirat mĂˇr eleve a kĂ©rt helyen van!'
--                  [56007] 'IrattĂˇrbĂłl skontrĂłba valĂł ĂˇthelyezĂ©s nem engedĂ©lyezett!'
--                  [56008] 'SkontrĂłba helyezĂ©snĂ©l kĂ¶telezĹ‘ a skontrĂł vĂ©gĂ©nek megadĂˇsa!'
--                  [56009] 'A skontrĂł vĂ©gĂ©nek az aktuĂˇlis napnĂˇl kĂ©sĹ‘bbinek kell lennie!'
--                  [56010] 'MIG_Alszam update hiba!'
--                  [56011] 'Az ĂĽgyirat kĂ¶zben mĂłdosult!'
--

Ăśgyirat elhelyezĂ©s/ĂˇthelyezĂ©s valahovĂˇ.
A lehetsĂ©ges esetek az alĂˇbbiak:

5 - BĂ­rĂłsĂˇg
9 - FPH
2 - IrattĂˇr
8 - KĂ¶zjegyzĹ‘
4 - KĂ¶zt.Hiv
3 - OsztĂˇlyon
1 - Scontro
6 - ĂśgyĂ©szsĂ©g
0 - ĂśSZI irattĂˇr
7 - VezetĹ‘sĂ©g

ĂltalĂˇnosan igaz, hogy ha ugyanoda akarjuk Ăˇthelyezni ahol eleve van,
akkor az nem megengedett.

Ha viszont engedĂ©lyezett az ĂˇthelyezĂ©s, 
akkor a vĂ©grehajtĂłdik az alĂˇbbi update:

   MIG_Alszam.Ugyhol := @Ugyhova


Plussz ellenĹ‘rzĂ©sek, mĹ±veletek:
a. ha @Ugyhova = 1 (Scontro) -- Scontro-ba helyezĂ©s
   - IrattĂˇr-bĂłl Scontro-ba valĂł ĂˇthelyezĂ©s nem engedĂ©lyezett!
   - Ă©s jĂ¶n az alĂˇbbi update
     Scontro   := aktuĂˇlis hĂłnap utolsĂł napja  -- ha today <= az aktuĂˇlis hĂłnap 15. napja
               := kĂ¶vetkezĹ‘ hĂłnap 15. napja    -- ha today  > az aktuĂˇlis hĂłnap 15. napja

b. ha @Ugyhova = 2 (IrattĂˇr) -- IrattĂˇr-ba helyezĂ©s
   - jĂ¶n az alĂˇbbi update
     Irattarba := today, ha Irattarba = NULL
   - Ă©s ha Scontro-bĂłl helyeztĂĽnk Ăˇt, akkor a kĂ¶vetkezĹ‘ update is jĂ¶n
     Scontro   := NULL
     
c. ha @Ugyhova <> 1,2 (Scontro,IrattĂˇr) -- EgyĂ©b helyre behelyezĂ©s
   - ha Scontro-bĂłl helyeztĂĽnk Ăˇt, akkor a kĂ¶vetkezĹ‘ update jĂ¶n
     Scontro   := NULL

-- pĂ©lda:  -----
begin tran 

declare @ID uniqueidentifier

select @ID = Id 
  from MIG_Alszam
 where ui_sav=13 AND ui_year=1997 AND ui_num=1154 AND alno=1
 
select Id,ugyhol,irattarba,scontro  -- elĹ‘tte
  from MIG_Alszam
 where Id=@ID

exec [dbo].[sp_MIG_AlszamAthelyezes]  @ID,'1'

select Id,ugyhol,irattarba,scontro  -- utĂˇna
  from MIG_Alszam
 where Id=@ID

rollback tran 
go

*/

---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 0
 
declare @sajat_tranz     int  -- sajĂˇt tranzakciĂł kezelĂ©s? (0 = nem, 1 = igen)
select  @sajat_tranz   = (CASE When @@trancount > 0 Then 0 Else 1 END)

if @sajat_tranz = 1  
   BEGIN TRANSACTION 
 
--
declare @error           int
declare @rowcount        int

--
declare @ugyhol          nvarchar(1)
declare @irattarba       datetime
declare @scontro         datetime

declare @aktdate         datetime


select  @aktdate = convert(datetime, convert(varchar,getdate(),102) )

--------------------------------
-- input paramĂ©terek ellenĹ‘rzĂ©se
--------------------------------
-- input paramĂ©terek kiiratĂˇsa
if @teszt = 1
   print '@Id = '+ isnull( convert(varchar(50),@Id), 'NULL') + ', ' +
         '@Ugyhova = '+ isnull(@Ugyhova, 'NULL') + ', ' +
         '@SkontroVege = '+ isnull(@SkontroVege, 'NULL')

--
if @Id is NULL
   --RAISERROR('Az ĂĽgyirat (alszĂˇm) megadĂˇsa kĂ¶telezĹ‘!',16,1)
   RAISERROR('[56001]',16,1)

-- a rĂ©gi dokumentum olvasĂˇsa
select @ugyhol                    = Ugyhol,
       @irattarba                 = Irattarba,
       @scontro                   = Scontro
  from dbo.MIG_Alszam
 where Id = @Id

select  @error = @@error, @rowcount = @@rowcount

if @error <> 0
   --RAISERROR('MIG_Alszam select hiba!',16,1)
   RAISERROR('[56002]',16,1)

if @rowcount = 0
   --RAISERROR('A megadott ĂĽgyirat nem lĂ©tezik!',16,1)
   RAISERROR('[56003]',16,1)

--
if @Ugyhova is NULL
   --RAISERROR('Az ĂĽgyirat Ăşj helyĂ©nek megadĂˇsa kĂ¶telezĹ‘!',16,1)
   RAISERROR('[56004]',16,1)

if @Ugyhova not in ('0','1','2','3','4','5','6','7','8','9')
   --RAISERROR('Az ĂĽgyirat Ăşj helyĂ©nek megadĂˇsa hibĂˇs!',16,1)
   RAISERROR('[56005]',16,1)

if isnull(@ugyhol,'X') = @Ugyhova
   --RAISERROR('Az ĂĽgyirat mĂˇr eleve a kĂ©rt helyen van!',16,1)
   RAISERROR('[56006]',16,1)

if @ugyhol = '2' and @Ugyhova = '1'
   --RAISERROR('IrattĂˇrbĂłl Scontroba valĂł ĂˇthelyezĂ©s nem engedĂ©lyezett!',16,1)
   RAISERROR('[56007]',16,1)

--
if @Ugyhova = 1 -- Scontro-ba helyezĂ©s
begin
     if @SkontroVege is NULL
     BEGIN
         --RAISERROR('[56008]',16,1) -- ha skontrĂłba teszĂĽnk, kĂ¶telezĹ‘ a skontrĂł vĂ©gĂ©nek megadĂˇsa
		 if DATEDIFF (d, getdate(), @SkontroVege) < 1
			 RAISERROR ('[56009]',16,1) -- skontrĂł vĂ©ge legalĂˇbb egy nappal az aktuĂˇlis dĂˇtum utĂˇn legyen
     END    

     set @scontro = @SkontroVege
--   if datepart(dd,@aktdate) <= 15
--   begin -- az akt.hĂł vĂ©ge
--     select @scontro = convert(datetime, convert(varchar(8),@aktdate,102)+'01') -- akt.hĂł 1.napja
--     select @scontro = dateadd(mm, 1,@scontro)                                  -- kĂ¶v.hĂł 1.napja
--     select @scontro = dateadd(dd,-1,@scontro)                                  -- akt.hĂł utolsĂł napja
--   end
--   else
--   begin -- a kĂ¶v.hĂł 15.napja
--     select @scontro = convert(datetime, convert(varchar(8),@aktdate,102)+'01') -- akt.hĂł 1.napja
--     select @scontro = dateadd(mm, 1,@scontro)                                  -- kĂ¶v.hĂł 1.napja
--     select @scontro = dateadd(dd,14,@scontro)                                  -- kĂ¶v.hĂł 15.napja
--   end
end

if @ugyhol = 1 -- Scontro-bĂłl kivĂ©tel
begin
   select @scontro = NULL
   select @irattarba = NULL
end

if @Ugyhova = 2 -- IrattĂˇr-ba helyezĂ©s
   select @irattarba = isnull(@irattarba, @aktdate) 
else
   select @irattarba = null

---------------------------------------------------------
-- akkor jĂ¶het a tĂ©nyleges mĂłdosĂ­tĂˇs (update)            
---------------------------------------------------------
update dbo.MIG_Alszam
   set Ugyhol            = @Ugyhova,
       Scontro           = @scontro,
       Irattarba         = @irattarba
  from dbo.MIG_Alszam
 where Id = @Id

select  @error = @@error, @rowcount = @@rowcount

if @error <> 0
   --RAISERROR('MIG_Alszam update hiba!',16,1)
   RAISERROR('[56010]',16,1)

if @rowcount = 0
   --RAISERROR('Az ĂĽgyirat kĂ¶zben mĂłdosult!',16,1)
   RAISERROR('[56011]',16,1)
   
-- ennek akkor lenne Ă©rtelme, ha az eredetileg felolvasott Ă©rtĂ©keket is hasznĂˇlnĂˇm a "where"-ben ...


if @sajat_tranz = 1  
   COMMIT TRANSACTION

END TRY
-----------
-----------
BEGIN CATCH
   if @sajat_tranz = 1  
      ROLLBACK TRANSACTION
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------