﻿-- set ANSI_NULLS ON
-- set QUOTED_IDENTIFIER ON
-- go

-- if exists (select 1
            -- from  sysobjects
           -- where  id = object_id('sp_MIG_AlszamGet')
            -- and   type = 'P')
   -- drop procedure sp_MIG_AlszamGet
-- go

create procedure [dbo].[sp_MIG_AlszamGet]
       @Id uniqueidentifier,
       @ExecutorUserId uniqueidentifier = NULL  -- ezt egyelőre nem használjuk!!!

/*
Használata például:

exec sp_MIG_AlszamGet
     @Id      = '14543938-3344-4CE8-9FCF-2772F1049DD8'
*/

as

begin

BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

  SET @sqlcmd = 
  'select ' + 
  '
       MIG_Alszam.Id,
       MIG_Alszam.UI_SAV,
       MIG_Alszam.UI_YEAR,
       MIG_Alszam.UI_NUM,
       MIG_Alszam.ALNO,
       MIG_Alszam.ERK,
       MIG_Alszam.IKTAT,
       MIG_Alszam.BKNEV,
       MIG_Alszam.ELINT,
       MIG_Alszam.ELOAD,
       MIG_Alszam.ROGZIT,
       MIG_Alszam.MJ,
       MIG_Alszam.IDNUM,
       MIG_Alszam.MIG_Foszam_Id,
       MIG_Alszam.MIG_Eloado_Id,
       MIG_Alszam.MIG_Sav_Id,
	   MIG_Alszam.UGYHOL,
	   MIG_Alszam.IRATTARBA,
	   MIG_Alszam.Adathordozo_tipusa,
	   MIG_Alszam.Irattipus,
	   MIG_Alszam.Feladat
  from MIG_Alszam as MIG_Alszam
 where MIG_Alszam.Id = @Id'

	print @sqlcmd  -- teszt kiirás

	exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY


BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1
		RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH

end