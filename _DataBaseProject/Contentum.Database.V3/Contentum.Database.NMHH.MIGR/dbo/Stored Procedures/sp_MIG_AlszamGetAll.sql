﻿-- set ANSI_NULLS ON
-- set QUOTED_IDENTIFIER ON
-- go

-- if exists (select 1
            -- from  sysobjects
           -- where  id = object_id('sp_MIG_AlszamGetAll')
            -- and   type = 'P')
   -- drop procedure sp_MIG_AlszamGetAll
-- go

CREATE procedure [dbo].[sp_MIG_AlszamGetAll]
       @Where nvarchar(4000)  = '',
       @OrderBy nvarchar(200) = 'order by ui_sav, ui_year, ui_num',
       @TopRow nvarchar(5)    = '0',
       @ExecutorUserId uniqueidentifier = NULL  -- ezt egyelőre nem használjuk!!!

/*
Használata például:

exec sp_MIG_AlszamGetAll 
     @Where   = '1=1',
     @OrderBy = 'order by ui_sav, ui_year, ui_num',
     @TopRow  = '1000'
*/

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)
   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   else
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

   DECLARE @Org uniqueidentifier

/*
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
*/


  SET @sqlcmd = 
  'select' + @LocalTopRow + 
  '(SELECT COUNT(MIG_Dokumentum.Id) 
		FROM MIG_Dokumentum 
		WHERE MIG_Dokumentum.MIG_Alszam_Id = MIG_Alszam.Id) as Csatolmany_Count,
				
		(SELECT TOP 1 MIG_Dokumentum.Id
		FROM MIG_Dokumentum 
		WHERE MIG_Dokumentum.MIG_Alszam_Id = MIG_Alszam.Id) as MIG_Dokumentum_Id,
       MIG_Alszam.Id,
       MIG_Alszam.UI_SAV,
       MIG_Alszam.UI_YEAR,
       MIG_Alszam.UI_NUM,
       MIG_Alszam.ALNO,
       MIG_Alszam.ERK,
       MIG_Alszam.IKTAT,
       MIG_Alszam.BKNEV,
       MIG_Alszam.ELINT,
       MIG_Alszam.ELOAD,
       MIG_Alszam.ROGZIT,
       MIG_Alszam.MJ,
       MIG_Alszam.IDNUM,
       MIG_Alszam.MIG_Foszam_Id,
       MIG_Alszam.MIG_Eloado_Id,
       (SELECT Name FROM MIG_Eloado WHERE MIG_Eloado.Id = MIG_Alszam.MIG_Eloado_Id) as EloadoNev,
       MIG_Alszam.MIG_Sav_Id,
	   MIG_Alszam.UGYHOL,
	   MIG_Alszam.IRATTARBA,
	   MIG_Alszam.Adathordozo_tipusa,
	   MIG_Alszam.Irattipus,
	   MIG_Alszam.Feladat
  from MIG_Alszam as MIG_Alszam
 where 1 = 1'
   
   if @Where is not null and @Where != ''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end

   SET @sqlcmd = @sqlcmd + ' ' + @OrderBy

   print @sqlcmd  -- teszt kiirás

   exec (@sqlcmd);

END TRY


BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER() < 50000
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1
		RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH

end

