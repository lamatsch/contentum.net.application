﻿/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_AlszamInsert')
            and   type = 'P')
   drop procedure sp_MIG_AlszamInsert
go
*/
create procedure sp_MIG_AlszamInsert    
                @Id      uniqueidentifier = null,    
                               @UI_SAV     int  = null,
                @UI_YEAR     int  = null,
                @UI_NUM     int  = null,
                @ALNO     int  = null,
                @ERK     datetime  = null,
                @IKTAT     datetime  = null,
                @BKNEV     nvarchar(1000)  = null,
                @ELINT     datetime  = null,
                @IRATTARBA     datetime  = null,
                @ELOAD     nvarchar(6)  = null,
                @ROGZIT     nvarchar(8)  = null,
                @MJ     nvarchar(200)  = null,
                @IDNUM     nvarchar(20)  = null,
                @MIG_Foszam_Id     uniqueidentifier  = null,
                @MIG_Eloado_Id     uniqueidentifier  = null,
                @MIG_Forras_Id     int  = null,
                @Adathordozo_tipusa     nvarchar(4)  = null,
                @Prioritas     nvarchar(4)  = null,
                @Irattipus     nvarchar(64)  = null,
                @Feladat     nvarchar(64)  = null,
                @Szervezet     nvarchar(64)  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @UI_SAV is not null
         begin
            SET @insertColumns = @insertColumns + ',UI_SAV'
            SET @insertValues = @insertValues + ',@UI_SAV'
         end 
       
         if @UI_YEAR is not null
         begin
            SET @insertColumns = @insertColumns + ',UI_YEAR'
            SET @insertValues = @insertValues + ',@UI_YEAR'
         end 
       
         if @UI_NUM is not null
         begin
            SET @insertColumns = @insertColumns + ',UI_NUM'
            SET @insertValues = @insertValues + ',@UI_NUM'
         end 
       
         if @ALNO is not null
         begin
            SET @insertColumns = @insertColumns + ',ALNO'
            SET @insertValues = @insertValues + ',@ALNO'
         end 
       
         if @ERK is not null
         begin
            SET @insertColumns = @insertColumns + ',ERK'
            SET @insertValues = @insertValues + ',@ERK'
         end 
       
         if @IKTAT is not null
         begin
            SET @insertColumns = @insertColumns + ',IKTAT'
            SET @insertValues = @insertValues + ',@IKTAT'
         end 
       
         if @BKNEV is not null
         begin
            SET @insertColumns = @insertColumns + ',BKNEV'
            SET @insertValues = @insertValues + ',@BKNEV'
         end 
       
         if @ELINT is not null
         begin
            SET @insertColumns = @insertColumns + ',ELINT'
            SET @insertValues = @insertValues + ',@ELINT'
         end 
       
         if @IRATTARBA is not null
         begin
            SET @insertColumns = @insertColumns + ',IRATTARBA'
            SET @insertValues = @insertValues + ',@IRATTARBA'
         end 
       
         if @ELOAD is not null
         begin
            SET @insertColumns = @insertColumns + ',ELOAD'
            SET @insertValues = @insertValues + ',@ELOAD'
         end 
       
         if @ROGZIT is not null
         begin
            SET @insertColumns = @insertColumns + ',ROGZIT'
            SET @insertValues = @insertValues + ',@ROGZIT'
         end 
       
         if @MJ is not null
         begin
            SET @insertColumns = @insertColumns + ',MJ'
            SET @insertValues = @insertValues + ',@MJ'
         end 
       
         if @IDNUM is not null
         begin
            SET @insertColumns = @insertColumns + ',IDNUM'
            SET @insertValues = @insertValues + ',@IDNUM'
         end 
       
         if @MIG_Foszam_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',MIG_Foszam_Id'
            SET @insertValues = @insertValues + ',@MIG_Foszam_Id'
         end 
       
         if @MIG_Eloado_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',MIG_Eloado_Id'
            SET @insertValues = @insertValues + ',@MIG_Eloado_Id'
         end 
       
         if @MIG_Forras_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',MIG_Forras_Id'
            SET @insertValues = @insertValues + ',@MIG_Forras_Id'
         end 
       
         if @Adathordozo_tipusa is not null
         begin
            SET @insertColumns = @insertColumns + ',Adathordozo_tipusa'
            SET @insertValues = @insertValues + ',@Adathordozo_tipusa'
         end 
       
         if @Prioritas is not null
         begin
            SET @insertColumns = @insertColumns + ',Prioritas'
            SET @insertValues = @insertValues + ',@Prioritas'
         end 
       
         if @Irattipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Irattipus'
            SET @insertValues = @insertValues + ',@Irattipus'
         end 
       
         if @Feladat is not null
         begin
            SET @insertColumns = @insertColumns + ',Feladat'
            SET @insertValues = @insertValues + ',@Feladat'
         end 
       
         if @Szervezet is not null
         begin
            SET @insertColumns = @insertColumns + ',Szervezet'
            SET @insertValues = @insertValues + ',@Szervezet'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into MIG_Alszam ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@UI_SAV int,@UI_YEAR int,@UI_NUM int,@ALNO int,@ERK datetime,@IKTAT datetime,@BKNEV nvarchar(1000),@ELINT datetime,@IRATTARBA datetime,@ELOAD nvarchar(6),@ROGZIT nvarchar(8),@MJ nvarchar(200),@IDNUM nvarchar(20),@MIG_Foszam_Id uniqueidentifier,@MIG_Eloado_Id uniqueidentifier,@MIG_Forras_Id int,@Adathordozo_tipusa nvarchar(4),@Prioritas nvarchar(4),@Irattipus nvarchar(64),@Feladat nvarchar(64),@Szervezet nvarchar(64),@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@UI_SAV = @UI_SAV,@UI_YEAR = @UI_YEAR,@UI_NUM = @UI_NUM,@ALNO = @ALNO,@ERK = @ERK,@IKTAT = @IKTAT,@BKNEV = @BKNEV,@ELINT = @ELINT,@IRATTARBA = @IRATTARBA,@ELOAD = @ELOAD,@ROGZIT = @ROGZIT,@MJ = @MJ,@IDNUM = @IDNUM,@MIG_Foszam_Id = @MIG_Foszam_Id,@MIG_Eloado_Id = @MIG_Eloado_Id,@MIG_Forras_Id = @MIG_Forras_Id,@Adathordozo_tipusa = @Adathordozo_tipusa,@Prioritas = @Prioritas,@Irattipus = @Irattipus,@Feladat = @Feladat,@Szervezet = @Szervezet ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'MIG_Alszam',@ResultUid
					,'MIG_AlszamHistory',0,NULL,NULL
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
go

/* használt errorcode-ok:
	50301:
	50302: 'Org' oszlop értéke nem lehet NULL
*/
