﻿CREATE procedure [dbo].[sp_MIG_AlszamUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @UI_SAV     int  = null,         
             @UI_YEAR     int  = null,         
             @UI_NUM     int  = null,         
             @ALNO     int  = null,         
             @ERK     datetime  = null,         
             @IKTAT     datetime  = null,         
             @BKNEV     nvarchar(1000)  = null,         
             @ELINT     datetime  = null,         
			 @UGYHOL     nvarchar(3)  = null,      
             @IRATTARBA     datetime  = null,         
             @ELOAD     nvarchar(6)  = null,         
             @ROGZIT     nvarchar(8)  = null,         
             @MJ     nvarchar(200)  = null,         
             @IDNUM     nvarchar(20)  = null,         
             @MIG_Foszam_Id     uniqueidentifier  = null,         
             @MIG_Eloado_Id     uniqueidentifier  = null,         
             @MIG_Forras_Id     int  = null,         
             @Adathordozo_tipusa     nvarchar(4)  = null,         
             @Prioritas     nvarchar(4)  = null,         
             @Irattipus     nvarchar(64)  = null,         
             @Feladat     nvarchar(64)  = null,         
             @Szervezet     nvarchar(64)  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_UI_SAV     int         
     DECLARE @Act_UI_YEAR     int         
     DECLARE @Act_UI_NUM     int         
     DECLARE @Act_ALNO     int         
     DECLARE @Act_ERK     datetime         
     DECLARE @Act_IKTAT     datetime         
     DECLARE @Act_BKNEV     nvarchar(1000)         
     DECLARE @Act_ELINT     datetime       
	 DECLARE @Act_UGYHOL     nvarchar(3)       
     DECLARE @Act_IRATTARBA     datetime         
     DECLARE @Act_ELOAD     nvarchar(6)         
     DECLARE @Act_ROGZIT     nvarchar(8)         
     DECLARE @Act_MJ     nvarchar(200)         
     DECLARE @Act_IDNUM     nvarchar(20)         
     DECLARE @Act_MIG_Foszam_Id     uniqueidentifier         
     DECLARE @Act_MIG_Eloado_Id     uniqueidentifier         
     DECLARE @Act_MIG_Forras_Id     int         
     DECLARE @Act_Adathordozo_tipusa     nvarchar(4)         
     DECLARE @Act_Prioritas     nvarchar(4)         
     DECLARE @Act_Irattipus     nvarchar(64)         
     DECLARE @Act_Feladat     nvarchar(64)         
     DECLARE @Act_Szervezet     nvarchar(64)           
  
set nocount on

select 
     @Act_UI_SAV = UI_SAV,
     @Act_UI_YEAR = UI_YEAR,
     @Act_UI_NUM = UI_NUM,
     @Act_ALNO = ALNO,
     @Act_ERK = ERK,
     @Act_IKTAT = IKTAT,
     @Act_BKNEV = BKNEV,
     @Act_ELINT = ELINT,
	 @Act_UGYHOL = UGYHOL,
     @Act_IRATTARBA = IRATTARBA,
     @Act_ELOAD = ELOAD,
     @Act_ROGZIT = ROGZIT,
     @Act_MJ = MJ,
     @Act_IDNUM = IDNUM,
     @Act_MIG_Foszam_Id = MIG_Foszam_Id,
     @Act_MIG_Eloado_Id = MIG_Eloado_Id,
     @Act_MIG_Forras_Id = MIG_Forras_Id,
     @Act_Adathordozo_tipusa = Adathordozo_tipusa,
     @Act_Prioritas = Prioritas,
     @Act_Irattipus = Irattipus,
     @Act_Feladat = Feladat,
     @Act_Szervezet = Szervezet
from MIG_Alszam
where Id = @Id


  

	   IF @UpdatedColumns.exist('/root/UI_SAV')=1
         SET @Act_UI_SAV = @UI_SAV
   IF @UpdatedColumns.exist('/root/UI_YEAR')=1
         SET @Act_UI_YEAR = @UI_YEAR
   IF @UpdatedColumns.exist('/root/UI_NUM')=1
         SET @Act_UI_NUM = @UI_NUM
   IF @UpdatedColumns.exist('/root/ALNO')=1
         SET @Act_ALNO = @ALNO
   IF @UpdatedColumns.exist('/root/ERK')=1
         SET @Act_ERK = @ERK
   IF @UpdatedColumns.exist('/root/IKTAT')=1
         SET @Act_IKTAT = @IKTAT
   IF @UpdatedColumns.exist('/root/BKNEV')=1
         SET @Act_BKNEV = @BKNEV
   IF @UpdatedColumns.exist('/root/ELINT')=1
         SET @Act_ELINT = @ELINT
   IF @UpdatedColumns.exist('/root/UGYHOL')=1
         SET @Act_UGYHOL = @UGYHOL
   IF @UpdatedColumns.exist('/root/IRATTARBA')=1
         SET @Act_IRATTARBA = @IRATTARBA
   IF @UpdatedColumns.exist('/root/ELOAD')=1
         SET @Act_ELOAD = @ELOAD
   IF @UpdatedColumns.exist('/root/ROGZIT')=1
         SET @Act_ROGZIT = @ROGZIT
   IF @UpdatedColumns.exist('/root/MJ')=1
         SET @Act_MJ = @MJ
   IF @UpdatedColumns.exist('/root/IDNUM')=1
         SET @Act_IDNUM = @IDNUM
   IF @UpdatedColumns.exist('/root/MIG_Foszam_Id')=1
         SET @Act_MIG_Foszam_Id = @MIG_Foszam_Id
   IF @UpdatedColumns.exist('/root/MIG_Eloado_Id')=1
         SET @Act_MIG_Eloado_Id = @MIG_Eloado_Id
   IF @UpdatedColumns.exist('/root/MIG_Forras_Id')=1
         SET @Act_MIG_Forras_Id = @MIG_Forras_Id
   IF @UpdatedColumns.exist('/root/Adathordozo_tipusa')=1
         SET @Act_Adathordozo_tipusa = @Adathordozo_tipusa
   IF @UpdatedColumns.exist('/root/Prioritas')=1
         SET @Act_Prioritas = @Prioritas
   IF @UpdatedColumns.exist('/root/Irattipus')=1
         SET @Act_Irattipus = @Irattipus
   IF @UpdatedColumns.exist('/root/Feladat')=1
         SET @Act_Feladat = @Feladat
   IF @UpdatedColumns.exist('/root/Szervezet')=1
         SET @Act_Szervezet = @Szervezet   
 
UPDATE MIG_Alszam
SET
     UI_SAV = @Act_UI_SAV,
     UI_YEAR = @Act_UI_YEAR,
     UI_NUM = @Act_UI_NUM,
     ALNO = @Act_ALNO,
     ERK = @Act_ERK,
     IKTAT = @Act_IKTAT,
     BKNEV = @Act_BKNEV,
     ELINT = @Act_ELINT,
	 UGYHOL = @Act_UGYHOL,
     IRATTARBA = @Act_IRATTARBA,
     ELOAD = @Act_ELOAD,
     ROGZIT = @Act_ROGZIT,
     MJ = @Act_MJ,
     IDNUM = @Act_IDNUM,
     MIG_Foszam_Id = @Act_MIG_Foszam_Id,
     MIG_Eloado_Id = @Act_MIG_Eloado_Id,
     MIG_Forras_Id = @Act_MIG_Forras_Id,
     Adathordozo_tipusa = @Act_Adathordozo_tipusa,
     Prioritas = @Act_Prioritas,
     Irattipus = @Act_Irattipus,
     Feladat = @Act_Feladat,
     Szervezet = @Act_Szervezet
where
  Id = @Id
 

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'MIG_Alszam',@Id
					,'MIG_AlszamHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

