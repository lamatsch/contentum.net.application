

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_DokumentumGet')
            and   type = 'P')
   drop procedure sp_MIG_DokumentumGet
go

create procedure sp_MIG_DokumentumGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   MIG_Dokumentum.Id,
	   MIG_Dokumentum.FajlNev,
	   MIG_Dokumentum.Tipus,
	   MIG_Dokumentum.Formatum,
	   MIG_Dokumentum.Leiras,
	   MIG_Dokumentum.External_Source,
	   MIG_Dokumentum.External_Link,
	   MIG_Dokumentum.BarCode,
	   MIG_Dokumentum.Allapot,
	   MIG_Dokumentum.ElektronikusAlairas,
	   MIG_Dokumentum.AlairasFelulvizsgalat,
	   MIG_Dokumentum.MIG_Alszam_Id,
	   MIG_Dokumentum.Ver,
	   MIG_Dokumentum.ErvVege
	   from 
		 MIG_Dokumentum as MIG_Dokumentum 
	   where
		 MIG_Dokumentum.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
