set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_DokumentumHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_MIG_DokumentumHistoryGetAllRecord
go

create procedure sp_MIG_DokumentumHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from MIG_DokumentumHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_DokumentumHistory Old
         inner join MIG_DokumentumHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FajlNev' as ColumnName,               cast(Old.FajlNev as nvarchar(99)) as OldValue,
               cast(New.FajlNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_DokumentumHistory Old
         inner join MIG_DokumentumHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FajlNev as nvarchar(max)),'') != ISNULL(CAST(New.FajlNev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tipus' as ColumnName,               cast(Old.Tipus as nvarchar(99)) as OldValue,
               cast(New.Tipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_DokumentumHistory Old
         inner join MIG_DokumentumHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tipus as nvarchar(max)),'') != ISNULL(CAST(New.Tipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Formatum' as ColumnName,               cast(Old.Formatum as nvarchar(99)) as OldValue,
               cast(New.Formatum as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_DokumentumHistory Old
         inner join MIG_DokumentumHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Formatum as nvarchar(max)),'') != ISNULL(CAST(New.Formatum as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Leiras' as ColumnName,               cast(Old.Leiras as nvarchar(99)) as OldValue,
               cast(New.Leiras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_DokumentumHistory Old
         inner join MIG_DokumentumHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Leiras as nvarchar(max)),'') != ISNULL(CAST(New.Leiras as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'External_Source' as ColumnName,               cast(Old.External_Source as nvarchar(99)) as OldValue,
               cast(New.External_Source as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_DokumentumHistory Old
         inner join MIG_DokumentumHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.External_Source as nvarchar(max)),'') != ISNULL(CAST(New.External_Source as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'External_Link' as ColumnName,               cast(Old.External_Link as nvarchar(99)) as OldValue,
               cast(New.External_Link as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_DokumentumHistory Old
         inner join MIG_DokumentumHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.External_Link as nvarchar(max)),'') != ISNULL(CAST(New.External_Link as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BarCode' as ColumnName,               cast(Old.BarCode as nvarchar(99)) as OldValue,
               cast(New.BarCode as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_DokumentumHistory Old
         inner join MIG_DokumentumHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.BarCode as nvarchar(max)),'') != ISNULL(CAST(New.BarCode as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               cast(Old.Allapot as nvarchar(99)) as OldValue,
               cast(New.Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_DokumentumHistory Old
         inner join MIG_DokumentumHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Allapot as nvarchar(max)),'') != ISNULL(CAST(New.Allapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ElektronikusAlairas' as ColumnName,               cast(Old.ElektronikusAlairas as nvarchar(99)) as OldValue,
               cast(New.ElektronikusAlairas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_DokumentumHistory Old
         inner join MIG_DokumentumHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ElektronikusAlairas as nvarchar(max)),'') != ISNULL(CAST(New.ElektronikusAlairas as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasFelulvizsgalat' as ColumnName,               cast(Old.AlairasFelulvizsgalat as nvarchar(99)) as OldValue,
               cast(New.AlairasFelulvizsgalat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_DokumentumHistory Old
         inner join MIG_DokumentumHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AlairasFelulvizsgalat as nvarchar(max)),'') != ISNULL(CAST(New.AlairasFelulvizsgalat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MIG_Alszam_Id' as ColumnName,               cast(Old.MIG_Alszam_Id as nvarchar(99)) as OldValue,
               cast(New.MIG_Alszam_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_DokumentumHistory Old
         inner join MIG_DokumentumHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MIG_Alszam_Id as nvarchar(max)),'') != ISNULL(CAST(New.MIG_Alszam_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go