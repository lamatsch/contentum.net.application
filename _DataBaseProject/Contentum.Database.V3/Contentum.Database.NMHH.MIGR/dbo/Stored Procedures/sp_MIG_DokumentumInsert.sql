
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_DokumentumInsert')
            and   type = 'P')
   drop procedure sp_MIG_DokumentumInsert
go

create procedure sp_MIG_DokumentumInsert    
                @Id      uniqueidentifier = null,    
               	            @FajlNev     nvarchar(400),
	            @Tipus     nvarchar(100),
	            @Formatum     nvarchar(64),
                @Leiras     nvarchar(400)  = null,
	            @External_Source     nvarchar(100),
	            @External_Link     nvarchar(400),
                @BarCode     nvarchar(100)  = null,
                @Allapot     nvarchar(64)  = null,
                @ElektronikusAlairas     nvarchar(64)  = null,
                @AlairasFelulvizsgalat     datetime  = null,
	            @MIG_Alszam_Id     uniqueidentifier,
                @Ver     int  = null,
                @ErvVege     datetime  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @FajlNev is not null
         begin
            SET @insertColumns = @insertColumns + ',FajlNev'
            SET @insertValues = @insertValues + ',@FajlNev'
         end 
       
         if @Tipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Tipus'
            SET @insertValues = @insertValues + ',@Tipus'
         end 
       
         if @Formatum is not null
         begin
            SET @insertColumns = @insertColumns + ',Formatum'
            SET @insertValues = @insertValues + ',@Formatum'
         end 
       
         if @Leiras is not null
         begin
            SET @insertColumns = @insertColumns + ',Leiras'
            SET @insertValues = @insertValues + ',@Leiras'
         end 
       
         if @External_Source is not null
         begin
            SET @insertColumns = @insertColumns + ',External_Source'
            SET @insertValues = @insertValues + ',@External_Source'
         end 
       
         if @External_Link is not null
         begin
            SET @insertColumns = @insertColumns + ',External_Link'
            SET @insertValues = @insertValues + ',@External_Link'
         end 
       
         if @BarCode is not null
         begin
            SET @insertColumns = @insertColumns + ',BarCode'
            SET @insertValues = @insertValues + ',@BarCode'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @ElektronikusAlairas is not null
         begin
            SET @insertColumns = @insertColumns + ',ElektronikusAlairas'
            SET @insertValues = @insertValues + ',@ElektronikusAlairas'
         end 
       
         if @AlairasFelulvizsgalat is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairasFelulvizsgalat'
            SET @insertValues = @insertValues + ',@AlairasFelulvizsgalat'
         end 
       
         if @MIG_Alszam_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',MIG_Alszam_Id'
            SET @insertValues = @insertValues + ',@MIG_Alszam_Id'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into MIG_Dokumentum ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@FajlNev nvarchar(400),@Tipus nvarchar(100),@Formatum nvarchar(64),@Leiras nvarchar(400),@External_Source nvarchar(100),@External_Link nvarchar(400),@BarCode nvarchar(100),@Allapot nvarchar(64),@ElektronikusAlairas nvarchar(64),@AlairasFelulvizsgalat datetime,@MIG_Alszam_Id uniqueidentifier,@Ver int,@ErvVege datetime,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@FajlNev = @FajlNev,@Tipus = @Tipus,@Formatum = @Formatum,@Leiras = @Leiras,@External_Source = @External_Source,@External_Link = @External_Link,@BarCode = @BarCode,@Allapot = @Allapot,@ElektronikusAlairas = @ElektronikusAlairas,@AlairasFelulvizsgalat = @AlairasFelulvizsgalat,@MIG_Alszam_Id = @MIG_Alszam_Id,@Ver = @Ver,@ErvVege = @ErvVege ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
/*ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'MIG_Dokumentum',@ResultUid
					,'MIG_DokumentumHistory',0,@Letrehozo_id,@LetrehozasIdo
END  */          
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
go

/* használt errorcode-ok:
	50301:
	50302: 'Org' oszlop értéke nem lehet NULL
*/
