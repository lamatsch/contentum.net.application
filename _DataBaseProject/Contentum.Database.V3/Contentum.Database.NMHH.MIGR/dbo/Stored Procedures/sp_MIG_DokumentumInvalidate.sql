
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_DokumentumInvalidate')
            and   type = 'P')
   drop procedure sp_MIG_DokumentumInvalidate
go

create procedure sp_MIG_DokumentumInvalidate
        @Id							uniqueidentifier,
		@ExecutorUserId				uniqueidentifier,
		@ExecutionTime					datetime			

as

BEGIN TRY
--BEGIN TRANSACTION InvalidateTransaction

  
	set nocount on
   
    DECLARE @Act_ErvVege datetime
    DECLARE @Act_Ver int
      
    select
        @Act_ErvVege = ErvVege,
        @Act_Ver = Ver
    from MIG_Dokumentum
    where Id = @Id
      
    if @@rowcount = 0
	begin
	    RAISERROR('[50602]',16,1)
	end
      
    if @Act_ErvVege <= @ExecutionTime
    begin
        -- már érvénytelenítve van
        RAISERROR('[50603]',16,1)
    end
      
    IF @Act_Ver is null
	SET @Act_Ver = 2	
    ELSE
	SET @Act_Ver = @Act_Ver+1
      

   
	UPDATE MIG_Dokumentum
	SET		 					
		   	ErvVege = @ExecutionTime,
        Ver     = @Act_Ver
	WHERE
		    Id = @Id
        

	if @@rowcount != 1
	begin
		RAISERROR('[50601]',16,1)
	end        
	else
	begin
           
		/* History Log */
		exec sp_LogRecordToHistory 'MIG_Dokumentum',@Id
				,'MIG_DokumentumHistory',2,@ExecutorUserId,@ExecutionTime
                   
	end	
--COMMIT TRANSACTION InvalidateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InvalidateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

go


/* használt errorcode-ok:
	50601: UPDATE sikertelen (update-elt sorok száma 0)
   50602: nincs ilyen rekord
   50603: Már érvénytelenítve van a rekord
	50699: rekord zárolva
*/