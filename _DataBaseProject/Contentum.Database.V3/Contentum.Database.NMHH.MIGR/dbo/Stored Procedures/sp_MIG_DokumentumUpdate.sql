

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_DokumentumUpdate')
            and   type = 'P')
   drop procedure sp_MIG_DokumentumUpdate
go

create procedure sp_MIG_DokumentumUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @FajlNev     nvarchar(400)  = null,         
             @Tipus     nvarchar(100)  = null,         
             @Formatum     nvarchar(64)  = null,         
             @Leiras     nvarchar(400)  = null,         
             @External_Source     nvarchar(100)  = null,         
             @External_Link     nvarchar(400)  = null,         
             @BarCode     nvarchar(100)  = null,         
             @Allapot     nvarchar(64)  = null,         
             @ElektronikusAlairas     nvarchar(64)  = null,         
             @AlairasFelulvizsgalat     datetime  = null,         
             @MIG_Alszam_Id     uniqueidentifier  = null,         
             @Ver     int  = null,         
             @ErvVege     datetime  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_FajlNev     nvarchar(400)         
     DECLARE @Act_Tipus     nvarchar(100)         
     DECLARE @Act_Formatum     nvarchar(64)         
     DECLARE @Act_Leiras     nvarchar(400)         
     DECLARE @Act_External_Source     nvarchar(100)         
     DECLARE @Act_External_Link     nvarchar(400)         
     DECLARE @Act_BarCode     nvarchar(100)         
     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_ElektronikusAlairas     nvarchar(64)         
     DECLARE @Act_AlairasFelulvizsgalat     datetime         
     DECLARE @Act_MIG_Alszam_Id     uniqueidentifier         
     DECLARE @Act_Ver     int         
     DECLARE @Act_ErvVege     datetime           
  
set nocount on

select 
     @Act_FajlNev = FajlNev,
     @Act_Tipus = Tipus,
     @Act_Formatum = Formatum,
     @Act_Leiras = Leiras,
     @Act_External_Source = External_Source,
     @Act_External_Link = External_Link,
     @Act_BarCode = BarCode,
     @Act_Allapot = Allapot,
     @Act_ElektronikusAlairas = ElektronikusAlairas,
     @Act_AlairasFelulvizsgalat = AlairasFelulvizsgalat,
     @Act_MIG_Alszam_Id = MIG_Alszam_Id,
     @Act_Ver = Ver,
     @Act_ErvVege = ErvVege
from MIG_Dokumentum
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
 

	   IF @UpdatedColumns.exist('/root/FajlNev')=1
         SET @Act_FajlNev = @FajlNev
   IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @Act_Tipus = @Tipus
   IF @UpdatedColumns.exist('/root/Formatum')=1
         SET @Act_Formatum = @Formatum
   IF @UpdatedColumns.exist('/root/Leiras')=1
         SET @Act_Leiras = @Leiras
   IF @UpdatedColumns.exist('/root/External_Source')=1
         SET @Act_External_Source = @External_Source
   IF @UpdatedColumns.exist('/root/External_Link')=1
         SET @Act_External_Link = @External_Link
   IF @UpdatedColumns.exist('/root/BarCode')=1
         SET @Act_BarCode = @BarCode
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/ElektronikusAlairas')=1
         SET @Act_ElektronikusAlairas = @ElektronikusAlairas
   IF @UpdatedColumns.exist('/root/AlairasFelulvizsgalat')=1
         SET @Act_AlairasFelulvizsgalat = @AlairasFelulvizsgalat
   IF @UpdatedColumns.exist('/root/MIG_Alszam_Id')=1
         SET @Act_MIG_Alszam_Id = @MIG_Alszam_Id
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE MIG_Dokumentum
SET
     FajlNev = @Act_FajlNev,
     Tipus = @Act_Tipus,
     Formatum = @Act_Formatum,
     Leiras = @Act_Leiras,
     External_Source = @Act_External_Source,
     External_Link = @Act_External_Link,
     BarCode = @Act_BarCode,
     Allapot = @Act_Allapot,
     ElektronikusAlairas = @Act_ElektronikusAlairas,
     AlairasFelulvizsgalat = @Act_AlairasFelulvizsgalat,
     MIG_Alszam_Id = @Act_MIG_Alszam_Id,
     Ver = @Act_Ver,
     ErvVege = @Act_ErvVege
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'MIG_Dokumentum',@Id
					,'MIG_DokumentumHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

go

/* használt errorcode-ok:
	50401:
	50402: 'Ver' oszlop értéke nem egyezik a meglévő rekordéval
   50403: érvényesség már lejárt a rekordra
	50499: rekord zárolva
*/
