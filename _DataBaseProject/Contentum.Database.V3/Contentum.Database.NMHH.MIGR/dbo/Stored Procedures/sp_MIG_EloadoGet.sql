﻿create procedure [dbo].[sp_MIG_EloadoGet]
       @Id uniqueidentifier,
       @ExecutorUserId uniqueidentifier = NULL  -- ezt egyelőre nem használjuk!!!

/*
Használata például:

exec sp_MIG_EloadoGet
     @Id      = '14543938-3344-4CE8-9FCF-2772F1049DD8'
*/

as

begin

BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

  SET @sqlcmd = 
  'select ' + 
  '
       MIG_Eloado.Id,
       MIG_Eloado.EV,
       MIG_Eloado.KOD,
       MIG_Eloado.NAME
  from MIG_Eloado as MIG_Eloado
 where MIG_Eloado.Id = @Id'

	print @sqlcmd  -- teszt kiirás

	exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY


BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1
		RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH

end