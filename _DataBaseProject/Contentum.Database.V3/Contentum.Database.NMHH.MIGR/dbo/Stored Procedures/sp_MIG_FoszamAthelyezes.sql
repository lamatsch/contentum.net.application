﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1 from  sysobjects
           where  id = object_id('sp_MIG_FoszamAthelyezes')
             and  type in ('P'))
   drop procedure sp_MIG_FoszamAthelyezes
go

create procedure sp_MIG_FoszamAthelyezes
             @Id                 uniqueidentifier,
             @ExecutorUserId	 uniqueidentifier,
			 @ExecutionTime       DATETIME,
             @Ugyhova            nvarchar(1),      -- '0','1',...,'9'
             @SkontroVege        datetime = null,
             @MoveHierarchy      char(1) = '0',     -- ha '1', a teljes foszamhierarchia mozog, ha '0', csak a konkret rekord
             @IRJ2000 NVARCHAR(7) = NULL,
             @UI_IRJ NVARCHAR(6) = NULL,
             @IrattarbolKikeroNev nvarchar(100) = null
as

/*
--
-- KĂ‰SZĂŤTETTE:   ĂdĂˇm AndrĂˇs       2007.12.21
-- MĂ“DOSĂŤTOTTA: Boda Eszter        2008.04.04
--              1. SkontrĂł vĂ©ge az alkalmazĂˇs felĹ‘l jĂ¶n, mint Ăşj paramĂ©ter
--              2. HibaĂĽzenetek helyett hibakĂłdok, az alkalmazĂˇs oldja fel
--                  [56001] 'Az ĂĽgyirat (alszĂˇm) megadĂˇsa kĂ¶telezĹ‘!'
--                  [56002] 'MIG_Alszam select hiba!'
--                  [56003] 'A megadott ĂĽgyirat nem lĂ©tezik!'
--                  [56004] 'Az ĂĽgyirat Ăşj helyĂ©nek megadĂˇsa kĂ¶telezĹ‘!'
--                  [56005] 'Az ĂĽgyirat Ăşj helyĂ©nek megadĂˇsa hibĂˇs!'
--                  [56006] 'Az ĂĽgyirat mĂˇr eleve a kĂ©rt helyen van!'
--                  [56007] 'IrattĂˇrbĂłl skontrĂłba valĂł ĂˇthelyezĂ©s nem engedĂ©lyezett!'
--                  [56008] 'SkontrĂłba helyezĂ©snĂ©l kĂ¶telezĹ‘ a skontrĂł vĂ©gĂ©nek megadĂˇsa!'
--                  [56009] 'A skontrĂł vĂ©gĂ©nek az aktuĂˇlis napnĂˇl kĂ©sĹ‘bbinek kell lennie!'
--                  [56010] 'MIG_Alszam update hiba!'
--                  [56011] 'Az ĂĽgyirat kĂ¶zben mĂłdosult!'
--                  [56014] 'MIG_Foszam select hiba!'
--                  [56015] 'MIG_Foszam update hiba!'
--
--                  [56301] 'Az ĂĽgyirat (fĹ‘szĂˇm) megadĂˇsa kĂ¶telezĹ‘!'


--				Boda Eszter		2009.05.13
--				Ăšj Ăˇllapot: SkontrĂłbĂłl elkĂ©rt ('E')
--				[56018] 'Az ĂĽgyirat nem kĂ©rhetĹ‘ ki skontrĂłbĂłl, mert nincs skontrĂłban!'

Ăśgyirat elhelyezĂ©s/ĂˇthelyezĂ©s valahovĂˇ.
A lehetsĂ©ges esetek az alĂˇbbiak:

5 - BĂ­rĂłsĂˇg
9 - FPH
2 - IrattĂˇr
8 - KĂ¶zjegyzĹ‘
4 - KĂ¶zt.Hiv
3 - OsztĂˇlyon
1 - Scontro
6 - ĂśgyĂ©szsĂ©g
0 - ĂśSZI irattĂˇr
7 - VezetĹ‘sĂ©g
S - Selejtezett
V - SelejtezĂ©sre vĂˇr
I - IrattĂˇrbĂłl elkĂ©rt
E - SkontrĂłbĂłl elkĂ©rt
L - LomtĂˇr

T - LevĂ©ltĂˇrban
J - JegyzĂ©ken
Z - LezĂˇrt jegyzĂ©ken
A - egyĂ©b szervezetnek Ăˇtadott

ĂltalĂˇnosan igaz, hogy ha ugyanoda akarjuk Ăˇthelyezni ahol eleve van,
akkor az nem megengedett.

Ha viszont engedĂ©lyezett az ĂˇthelyezĂ©s, 
akkor a vĂ©grehajtĂłdik az alĂˇbbi update:

   MIG_Foszam.Ugyhol := @Ugyhova


Plussz ellenĹ‘rzĂ©sek, mĹ±veletek:
a. ha @Ugyhova = 1 (Scontro) -- Scontro-ba helyezĂ©s
   - IrattĂˇr-bĂłl Scontro-ba valĂł ĂˇthelyezĂ©s nem engedĂ©lyezett!
   - Ă©s jĂ¶n az alĂˇbbi update
     Scontro   := aktuĂˇlis hĂłnap utolsĂł napja  -- ha today <= az aktuĂˇlis hĂłnap 15. napja
               := kĂ¶vetkezĹ‘ hĂłnap 15. napja    -- ha today  > az aktuĂˇlis hĂłnap 15. napja

b. ha @Ugyhova = 2 (IrattĂˇr) -- IrattĂˇr-ba helyezĂ©s
   - jĂ¶n az alĂˇbbi update
     Irattarba := today, ha Irattarba = NULL
   - Ă©s ha Scontro-bĂłl helyeztĂĽnk Ăˇt, akkor a kĂ¶vetkezĹ‘ update is jĂ¶n
     Scontro   := NULL
     
c. ha @Ugyhova <> 1,2 (Scontro,IrattĂˇr) -- EgyĂ©b helyre behelyezĂ©s
   - ha Scontro-bĂłl helyeztĂĽnk Ăˇt, akkor a kĂ¶vetkezĹ‘ update jĂ¶n
     Scontro   := NULL

-- pĂ©lda:  -----
begin tran 

declare @ID uniqueidentifier

select @ID = Id 
  from MIG_Foszam
 where ui_sav=13 AND ui_year=1997 AND ui_num=1154 AND alno=1
 
select Id,ugyhol,irattarba,scontro  -- elĹ‘tte
  from MIG_Foszam
 where Id=@ID

exec [dbo].[sp_MIG_FoszamAthelyezes]  @ID,'1'

select Id,ugyhol,irattarba,scontro  -- utĂˇna
  from MIG_Foszam
 where Id=@ID

rollback tran 
go

*/

---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 0
 
declare @sajat_tranz     int  -- sajĂˇt tranzakciĂł kezelĂ©s? (0 = nem, 1 = igen)
select  @sajat_tranz   = (CASE When @@trancount > 0 Then 0 Else 1 END)

if @sajat_tranz = 1  
   BEGIN TRANSACTION 
 
--
declare @error           int
declare @rowcount        int

--
declare @ugyhol          nvarchar(1)
declare @irattarba       datetime
declare @scontro         DATETIME
DECLARE @Selejtezes_Datuma Datetime
DECLARE @Act_IRJ2000 NVARCHAR(7)
DECLARE @Act_UI_IRJ NVARCHAR(6)
DECLARE @Act_Ver     INT
declare @New_IrattarbolKikeroNev nvarchar(100)
DECLARE @IrattarbolKikeres_Datuma Datetime

declare @aktdate         datetime


select  @aktdate = convert(datetime, convert(varchar,getdate(),102) )

--------------------------------
-- input paramĂ©terek ellenĹ‘rzĂ©se
--------------------------------
-- input paramĂ©terek kiiratĂˇsa
if @teszt = 1
   print '@Id = '+ isnull( convert(varchar(50),@Id), 'NULL') + ', ' +
         '@Ugyhova = '+ isnull(@Ugyhova, 'NULL') + ', ' +
         '@SkontroVege = '+ isnull( convert(nvarchar,@SkontroVege), 'NULL')

--
if @Id is NULL
   --RAISERROR('Az ĂĽgyirat (fĹ‘szĂˇm) megadĂˇsa kĂ¶telezĹ‘!',16,1)
   RAISERROR('[56301]',16,1)

-- a rĂ©gi dokumentum olvasĂˇsa
select @ugyhol                    = Ugyhol,
       @Selejtezes_Datuma		  = Selejtezes_Datuma,
       @irattarba                 = Irattarba,
       @scontro                   = Scontro,
       @Act_IRJ2000 = IRJ2000,
       @Act_UI_IRJ = UI_IRJ,
       @Act_Ver = Ver,
       @New_IrattarbolKikeroNev = IrattarbolKikeroNev,
       @IrattarbolKikeres_Datuma = IrattarbolKikeres_Datuma
  from dbo.MIG_Foszam
 where Id = @Id

select  @error = @@error, @rowcount = @@rowcount

if @error <> 0
   --RAISERROR('MIG_Foszam select hiba!',16,1)
   RAISERROR('[56014]',16,1)

if @rowcount = 0
   --RAISERROR('A megadott ĂĽgyirat nem lĂ©tezik!',16,1)
   RAISERROR('[56003]',16,1)

--
if @Ugyhova is NULL
   --RAISERROR('Az ĂĽgyirat Ăşj helyĂ©nek megadĂˇsa kĂ¶telezĹ‘!',16,1)
   RAISERROR('[56004]',16,1)

if @Ugyhova not in ('0','1','2','3','4','5','6','7','8','9','V','S','I','E','L','T','J','Z','A')
   --RAISERROR('Az ĂĽgyirat Ăşj helyĂ©nek megadĂˇsa hibĂˇs!',16,1)
   RAISERROR('[56005]',16,1)

-- if isnull(@ugyhol,'X') = @Ugyhova
-- --RAISERROR('Az ĂĽgyirat mĂˇr eleve a kĂ©rt helyen van!',16,1)
-- RAISERROR('[56006]',16,1)

if @ugyhol = '2' and @Ugyhova = '1'
   --RAISERROR('IrattĂˇrbĂłl Scontroba valĂł ĂˇthelyezĂ©s nem engedĂ©lyezett!',16,1)
   RAISERROR('[56007]',16,1)

if @Ugyhova = 'E' and @ugyhol not in ('1', 'J') -- jegyzĂ©k sztornĂłnĂˇl visszaĂˇllĂ­thatĂł --<> '1'
	--RAISERROR('Az ĂĽgyirat nem kĂ©rhetĹ‘ ki skontrĂłbĂłl, mert nincs skontrĂłban!')
	RAISERROR('[56018]',16,1)

--Ha nincs tĂ©nyleges fĹ‘szĂˇmĂˇthelyezĂ©s nem kellenek a vizsgĂˇlatok
IF(ISNULL(@ugyhol,'NULL') != ISNULL(@Ugyhova,'NULL'))
BEGIN
	if @Ugyhova = '1' -- Scontro-ba helyezĂ©s
	begin
		 if @SkontroVege is NULL
		 BEGIN
			 --RAISERROR('[56008]',16,1) -- ha skontrĂłba teszĂĽnk, kĂ¶telezĹ‘ a skontrĂł vĂ©gĂ©nek megadĂˇsa
			 if DATEDIFF (d, getdate(), @SkontroVege) < 1
				 RAISERROR ('[56009]',16,1) -- skontrĂł vĂ©ge legalĂˇbb egy nappal az aktuĂˇlis dĂˇtum utĂˇn legyen
		 END    

		 set @scontro = @SkontroVege
	--   if datepart(dd,@aktdate) <= 15
	--   begin -- az akt.hĂł vĂ©ge
	--     select @scontro = convert(datetime, convert(varchar(8),@aktdate,102)+'01') -- akt.hĂł 1.napja
	--     select @scontro = dateadd(mm, 1,@scontro)                                  -- kĂ¶v.hĂł 1.napja
	--     select @scontro = dateadd(dd,-1,@scontro)                                  -- akt.hĂł utolsĂł napja
	--   end
	--   else
	--   begin -- a kĂ¶v.hĂł 15.napja
	--     select @scontro = convert(datetime, convert(varchar(8),@aktdate,102)+'01') -- akt.hĂł 1.napja
	--     select @scontro = dateadd(mm, 1,@scontro)                                  -- kĂ¶v.hĂł 1.napja
	--     select @scontro = dateadd(dd,14,@scontro)                                  -- kĂ¶v.hĂł 15.napja
	--   end
	end

	if @ugyhol = '1' -- Scontro-bĂłl kivĂ©tel
	begin
		if @Ugyhova <> 'E' -- ha skontrĂłbĂłl kikĂ©rt, mĂ©g megĹ‘rizzĂĽk a skontrĂł vĂ©gĂ©t
		begin
			select @scontro = NULL
		end
	   select @irattarba = NULL
	end

	if @ugyhol = 'E' -- Scontro-bĂłl elkĂ©rt kivĂ©tele
	begin
		if @Ugyhova <> '1' -- ha skontrĂłbĂłl kikĂ©rt, Ă©s nem skontrĂłba helyezzĂĽk vissza, tĂ¶rĂ¶ljĂĽk a skontrĂł vĂ©gĂ©t
		begin
			select @scontro = NULL
		end
	end

	if @Ugyhova = '2' -- IrattĂˇr-ba helyezĂ©s
	BEGIN
	   select @irattarba = isnull(@irattarba, @aktdate)
	   -- kikĂ©rĂ©s tĂ¶rlĂ©se
	   set @New_IrattarbolKikeroNev = null
	   set @IrattarbolKikeres_Datuma = null
	END 
	else
	   select @irattarba = NULL
	   
	 IF @Ugyhova = 'S' -- SelejtezĂ©s
	 BEGIN
		SET @Selejtezes_Datuma = @aktdate
	 END
	 
	 IF @Ugyhova = 'I' -- IrattĂˇrbĂłl elkĂ©rt
	 BEGIN
		SET @New_IrattarbolKikeroNev = @IrattarbolKikeroNev
		set @IrattarbolKikeres_Datuma = GETDATE()
	 END
 END  
   
 IF(@IRJ2000 IS NULL)
	SET @IRJ2000 = @Act_IRJ2000
	
IF (@UI_IRJ IS NULL)
	SET @UI_IRJ = @Act_UI_IRJ

---------------------------------------------------------
-- akkor jĂ¶het a tĂ©nyleges mĂłdosĂ­tĂˇs (update)            
---------------------------------------------------------
if (@MoveHierarchy = '0')
begin
    update dbo.MIG_Foszam
       set Ugyhol            = @Ugyhova,
           Selejtezes_Datuma = @Selejtezes_Datuma,
           Scontro           = @scontro,
           Irattarba         = @irattarba,
           IRJ2000 = @IRJ2000,
           UI_IRJ = @UI_IRJ,
           Ver = ISNULL(Ver, 1) + 1,
           IrattarbolKikeroNev = @New_IrattarbolKikeroNev,
           IrattarbolKikeres_Datuma = @IrattarbolKikeres_Datuma
      from dbo.MIG_Foszam
    where Id = @Id
end
else
begin
    update dbo.MIG_Foszam
       set Ugyhol            = @Ugyhova,
           Selejtezes_Datuma = @Selejtezes_Datuma,
           Scontro           = @scontro,
           Irattarba         = @irattarba,
           IRJ2000 = @IRJ2000,
           UI_IRJ = @UI_IRJ,
           Ver = ISNULL(Ver, 1) + 1,
           IrattarbolKikeroNev = @New_IrattarbolKikeroNev,
           IrattarbolKikeres_Datuma = @IrattarbolKikeres_Datuma
      from dbo.MIG_Foszam
    where Id in (select Id from dbo.fn_GetFoszamokHierarchy(@Id))
end

select  @error = @@error, @rowcount = @@rowcount

if @error <> 0
   --RAISERROR('MIG_Foszam update hiba!',16,1)
   RAISERROR('[56015]',16,1)

if @rowcount = 0
   --RAISERROR('Az ĂĽgyirat kĂ¶zben mĂłdosult!',16,1)
   RAISERROR('[56011]',16,1)

-- Irattárba helyezés esetén megőrzési idő számítása
if @Ugyhova = '2'
BEGIN
	if (@MoveHierarchy = '0')
	begin
		UPDATE MIG_Foszam
			SET MegorzesiIdo = m.MegorzesiIdoVege
		FROM MIG_Foszam
		CROSS APPLY dbo.fn_GetMegorzesiIdoVege(UI_YEAR, UI_IRJ, IRJ2000, IRATTARBA) m
		WHERE MIG_Foszam.Id = @Id
	end
	else
	begin
		UPDATE MIG_Foszam
			SET MegorzesiIdo = m.MegorzesiIdoVege
		FROM MIG_Foszam
		CROSS APPLY dbo.fn_GetMegorzesiIdoVege(UI_YEAR, UI_IRJ, IRJ2000, IRATTARBA) m
		WHERE MIG_Foszam.Id in (select Id from dbo.fn_GetFoszamokHierarchy(@Id))
	end
END
   
/* History Log */
if (@MoveHierarchy = '0')
begin   
exec sp_LogRecordToHistory 'MIG_Foszam',@Id
					,'MIG_FoszamHistory',1,@ExecutorUserId,@ExecutionTime
END
ELSE
BEGIN
	create table #filter(Id UNIQUEIDENTIFIER)
	INSERT INTO [#filter] (
	[Id]
	) select Id from dbo.fn_GetFoszamokHierarchy(@Id)
	exec sp_LogMIG_FoszamHistoryTomeges 1,@ExecutorUserId,@ExecutionTime 
END					   

   
-- ennek akkor lenne Ă©rtelme, ha az eredetileg felolvasott Ă©rtĂ©keket is hasznĂˇlnĂˇm a "where"-ben ...


if @sajat_tranz = 1  
   COMMIT TRANSACTION

END TRY
-----------
-----------
BEGIN CATCH
   if @sajat_tranz = 1  
      ROLLBACK TRANSACTION
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------