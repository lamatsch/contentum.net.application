﻿
create procedure sp_MIG_FoszamAthelyezesTomeges
             @FoszamIdList     NVARCHAR(MAX), -- Foszam Id lista
             @Delimeter NVARCHAR(1) = ',',
             @ExecutorUserId	 uniqueidentifier,
			 @ExecutionTime       DATETIME,
             @Ugyhova            nvarchar(1),      -- '0','1',...,'9'
             @SkontroVege        datetime = null,
             @MoveHierarchy      char(1) = '0',     -- ha '1', a teljes foszamhierarchia mozog, ha '0', csak a konkret rekord
             @IrattarbolKikeroNev nvarchar(100) = null
as

/*
--              1. SkontrĂł vĂ©ge az alkalmazĂˇs felĹ‘l jĂ¶n, mint Ăşj paramĂ©ter
--              2. HibaĂĽzenetek helyett hibakĂłdok, az alkalmazĂˇs oldja fel
--                  [56004] 'Az ĂĽgyirat Ăşj helyĂ©nek megadĂˇsa kĂ¶telezĹ‘!'
--                  [56005] 'Az ĂĽgyirat Ăşj helyĂ©nek megadĂˇsa hibĂˇs!'

--				Boda Eszter		2009.05.13
--				Ăšj Ăˇllapot: SkontrĂłbĂłl elkĂ©rt ('E')
--				[56018] 'Az ĂĽgyirat nem kĂ©rhetĹ‘ ki skontrĂłbĂłl, mert nincs skontrĂłban!'


Ăśgyirat(Foszam) elhelyezĂ©s/ĂˇthelyezĂ©s valahovĂˇ. A Foszam ĂˇllapotĂˇt a legutolsĂł alszĂˇm record-ban tĂˇroljuk.
A lehetsĂ©ges esetek az alĂˇbbiak:

5 - BĂ­rĂłsĂˇg
9 - FPH
2 - IrattĂˇr
8 - KĂ¶zjegyzĹ‘
4 - KĂ¶zt.Hiv
3 - OsztĂˇlyon
1 - Scontro
6 - ĂśgyĂ©szsĂ©g
0 - ĂśSZI irattĂˇr
7 - VezetĹ‘sĂ©g
V - SelejtezĂ©sre vĂˇr
S - Selejtezve
I - IrattĂˇrbĂłl elkĂ©rt
E - SkontrĂłbĂłl elkĂ©rt
L - LomtĂˇr

T - LevĂ©ltĂˇrban
J - JegyzĂ©ken
Z - LezĂˇrt jegyzĂ©ken
A - egyĂ©b szervezetnek Ăˇtadott

-- pĂ©lda:  -----
begin tran 

Declare @Id1 uniqueidentifier
set @Id1 = dbo.fn_GetFoszamIdByAzonosito('(01) /26097/2007')

Declare @Id2 uniqueidentifier
set @Id2 = dbo.fn_GetFoszamIdByAzonosito('(01) /24482/2007')
  
select Id,Alno,ugyhol,irattarba,scontro  -- elĹ‘tte
  from MIG_Alszam
 where MIG_Foszam_Id = @Id1 or MIG_Foszam_Id = @Id2
 
declare @FoszamIdList nvarchar(max)
set @FoszamIdList = cast(@Id1 as nvarchar(40))
set @FoszamIdList = @FoszamIdList + ','
set @FoszamIdList = @FoszamIdList + cast(@Id2 as nvarchar(40))

exec [dbo].[sp_MIG_FoszamAthelyezesTomeges]  @FoszamIdList,default,'2'

select Id,Alno,ugyhol,irattarba,scontro  -- utĂˇna
  from MIG_Alszam
where MIG_Foszam_Id = @Id1 or MIG_Foszam_Id = @Id2

rollback tran 
go

*/
/*
MĂłdosĂ­tĂˇs:
2008.07.11. Boda Eszter:
Az UGYHOL, SCONTRO, IRATTARBA mezĹ‘k a MIG_Alszam tĂˇblĂˇbĂłl
a MIG_Foszam tĂˇblĂˇba kerĂĽltek Ăˇt, ezĂ©rt nem az utolsĂł alszĂˇmot,
hanem kĂ¶zvetlenĂĽl a fĹ‘szĂˇm rekordot kell mĂłdosĂ­tani
Az ĂĽgyiratok egy szerelĂ©si lĂˇncban csak egyĂĽtt mozoghatnak,
a FoszamAthelyezes elvĂ©gzi a hierarchia meghatĂˇrozĂˇsĂˇt,
kivĂ©ve az EDOK felĂ© valĂł szerelĂ©st(!)
*/
---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 0
 
declare @sajat_tranz     int  -- sajĂˇt tranzakciĂł kezelĂ©s? (0 = nem, 1 = igen)
select  @sajat_tranz   = (CASE When @@trancount > 0 Then 0 Else 1 END)

if @sajat_tranz = 1  
   BEGIN TRANSACTION 
 
--
declare @error           int
declare @rowcount        int
DECLARE @curExist INT
SET @curExist = 0
--------------------------------
-- input paramĂ©terek ellenĹ‘rzĂ©se
--------------------------------
-- input paramĂ©terek kiiratĂˇsa
if @teszt = 1
   print '@FoszamIdList = '+ isnull(@FoszamIdList, 'NULL') + ', ' +
         '@Delimeter = '+ isnull(@Delimeter, 'NULL') + ', ' +
         '@Ugyhova = '+ isnull(@Ugyhova, 'NULL') + ', ' +
         '@SkontroVege = '+ isnull(convert(varchar(50),@SkontroVege), 'NULL')

if @Ugyhova is NULL
   --RAISERROR('Az ĂĽgyirat Ăşj helyĂ©nek megadĂˇsa kĂ¶telezĹ‘!',16,1)
   RAISERROR('[56004]',16,1)

if @Ugyhova not in ('0','1','2','3','4','5','6','7','8','9','V','S','I','E','L','T','J','Z','A')
   --RAISERROR('Az ĂĽgyirat Ăşj helyĂ©nek megadĂˇsa hibĂˇs!',16,1)
   RAISERROR('[56005]',16,1)
 
 
DECLARE @FoszamId UNIQUEIDENTIFIER

DECLARE @Ids table
(
	Id uniqueidentifier
)

INSERT INTO @Ids
	SELECT Value Id FROM dbo.fn_Split(@FoszamIdList,@Delimeter)
	WHERE Value != ''
	

-- MeghatĂˇrozzuk az esetleges szĂĽlĹ‘t Ă©s megnĂ©zzĂĽk, szerepel-e az Ăˇtadott Id-k kĂ¶zĂ¶tt
-- ha igen, nem kell mozgatni, mert majd a szĂĽlĹ‘t mozgatjuk, Ă©s vele egyĂĽtt a gyerekeit is
DELETE FROM @Ids
    WHERE Id in (SELECT MIG_Foszam.Csatolva_Id FROM MIG_Foszam WHERE MIG_Foszam.Id IN (SELECT tb.Id FROM @Ids tb))

DECLARE cur CURSOR local FAST_FORWARD READ_ONLY FOR
	SELECT Id FROM @Ids

OPEN cur

SET @curExist = 1

FETCH NEXT FROM cur INTO @FoszamId

WHILE @@FETCH_STATUS = 0
BEGIN
IF (@teszt = 1) PRINT 'Kovetkezo foszam athelyezese: ' + isnull( convert(varchar(50),@FoszamId), 'NULL')

IF @FoszamId IS NOT NULL
BEGIN
	--EXEC [sp_MIG_FoszamAthelyezes] @Id = @FoszamId, @Ugyhova = @Ugyhova, @SkontroVege = @SkontroVege
    EXEC [sp_MIG_FoszamAthelyezes] @Id = @FoszamId
								 , @ExecutorUserId = @ExecutorUserId
								 , @ExecutionTime = @ExecutionTime
                                 , @Ugyhova = @Ugyhova
                                 , @SkontroVege = @SkontroVege
                                 , @MoveHierarchy = @MoveHierarchy
                                 , @IrattarbolKikeroNev = @IrattarbolKikeroNev

END

FETCH NEXT FROM cur INTO @FoszamId

END

CLOSE cur
DEALLOCATE cur  

if @sajat_tranz = 1  
   COMMIT TRANSACTION

END TRY
-----------
-----------
BEGIN CATCH
   if @sajat_tranz = 1  
      ROLLBACK TRANSACTION
   
   IF @curExist = 1
   BEGIN
	   CLOSE cur
	   DEALLOCATE cur
   END  
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------