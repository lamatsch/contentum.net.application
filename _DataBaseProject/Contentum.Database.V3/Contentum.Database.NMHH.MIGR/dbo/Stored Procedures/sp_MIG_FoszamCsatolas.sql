﻿create procedure [dbo].[sp_MIG_FoszamCsatolas]
             @Id                 uniqueidentifier,
             @Muvelet            char(1)          = 'C',   -- C/S = CsatolĂˇs/SelejtezĂ©s     
             @Csatolva_Id        uniqueidentifier = null
as

/*
--
-- KĂ‰SZĂŤTETTE:   ĂdĂˇm AndrĂˇs       2007.11.26
-- MĂ“DOSĂŤTOTTA:

RĂ©gi rendszerbeli, migrĂˇlt adatokon 
elvileg csak 2-fĂ©le adatmĂłdosĂ­tĂł mĹ±veletet vĂ©gezhetĂĽnk:

-- SelejtezzĂĽk, mert mĂˇr nincs rĂˇ szĂĽksĂ©g.
   A selejtezĂ©s csak akkor vĂ©gezhetĹ‘ el, 
   ha a rĂ©gi dokumentum mĂ©g nem volt leselejzezve
   (azaz Selejtezes = NULL).

-- MegjelĂ¶ljĂĽk valamely Ăşj rendszerbeli dokumentum elĹ‘zmĂ©nyekĂ©nt
   (azaz az Ăşj dokumentumhoz hozzĂˇcsatoljuk).
   A csatolĂˇs csak akkor vĂ©gezhetĹ‘ el,
   ha a rĂ©gi dokumentum mĂ©g nem volt hozzĂˇcsatolva semmihez 
   (Csatolva_Rendszer = NULL),
   tovĂˇbbĂˇ ha nem volt mĂ©g leselejtezve (Selejtezve = NULL).

     
A procedura a fenti 2-mĹ±velet valamelyikĂ©nek vĂ©grehajtĂˇsĂˇra szolgĂˇl.

A paramĂ©terezĂ©s szempontjĂˇbĂłl,

CSATOLĂS-rĂłl akkor van szĂł, ha @Muvelet = 'C' !
Ekkor MIG_Foszam tĂˇblĂˇban Csatolva_Rendszer := 1, Csatolva_Id := Csatolva_Id lesz

SELEJTEZĂ‰S-rĹ‘l pedig akkor van szĂł, ha @Muvelet = 'S' !
Ekkor MIG_Foszam tĂˇblĂˇban Selejtezve := 1, Selejtezes_Datuma := getdate() lesz

-- pl: CSATOLĂS -----
begin tran 

declare @ID uniqueidentifier, @NEW_DOC_ID uniqueidentifier
select  @ID = 'FFEA58C1-D812-4487-A246-00000278F60D'
select  @NEW_DOC_ID = '0D5FBB5C-2C91-4DE0-8D43-64F612DE55F7'

exec [dbo].[sp_MIG_FoszamCsatolas] 
     @ID,                                    -- @Id
     'C',                                    -- @Muvelet
     @NEW_DOC_ID                             -- @Csatolva_Id

--select * from dbo.MIG_Foszam where Id = @ID 
select Id,Selejtezve,Selejtezes_Datuma,Csatolva_Rendszer,Csatolva_Id  
  from dbo.MIG_Foszam 
 where Id = @ID 

rollback tran 
go

-- pl: SELEJTEZĂ‰S -----
begin tran 

declare @ID uniqueidentifier
select  @ID = 'FFEA58C1-D812-4487-A246-00000278F60D'

exec [dbo].[sp_MIG_FoszamCsatolas] 
     @ID,                                    -- @Id
     'S',                                    -- @Muvelet
     null                                    -- @Csatolva_Id

--select * from dbo.MIG_Foszam where Id = @ID 
select Id,Selejtezve,Selejtezes_Datuma,Csatolva_Rendszer,Csatolva_Id  
  from dbo.MIG_Foszam 
 where Id = @ID 

rollback tran 
go

*/

---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 1
 
declare @sajat_tranz     int  -- sajĂˇt tranzakciĂł kezelĂ©s? (0 = nem, 1 = igen)
select  @sajat_tranz   = (CASE When @@trancount > 0 Then 0 Else 1 END)

if @sajat_tranz = 1  
   BEGIN TRANSACTION 
 
--
declare @error           int
declare @rowcount        int

------- 
/*
declare @Selejtezve_Column         integer
declare @Selejtezes_Datuma_Column  datetime
declare @Csatolva_Rendszer_Column  integer
declare @Csatolva_Id_Column        uniqueidentifier
*/

declare @Selejtezve_Column         integer
declare @Selejtezes_Datuma_Column  datetime
declare @Csatolva_Rendszer_Column  integer
declare @Csatolva_Id_Column        uniqueidentifier

--------------------------------
-- input paramĂ©terek ellenĹ‘rzĂ©se
--------------------------------
-- input paramĂ©terek kiiratĂˇsa
if @teszt = 1
   print '@Id = '+ isnull( convert(varchar(50),@Id), 'NULL') + ', ' +
         '@Muvelet = '+ isnull(@Muvelet, 'NULL') + ', ' +
         '@Csatolva_Id = '+ isnull( convert(varchar(50),@Csatolva_Id), 'NULL')

--
if @Id is NULL
   RAISERROR('A rĂ©gi dokumentum megadĂˇsa kĂ¶telezĹ‘!',16,1)

-- a rĂ©gi dokumentum olvasĂˇsa
select @Selejtezve_Column         = Selejtezve,
       @Selejtezes_Datuma_Column  = Selejtezes_Datuma,
       @Csatolva_Rendszer_Column  = Csatolva_Rendszer,
       @Csatolva_Id_Column        = Csatolva_Id
  from dbo.MIG_Foszam
 where Id = @Id

select  @error = @@error, @rowcount = @@rowcount

if @error <> 0
   RAISERROR('MIG_Foszam select hiba!',16,1)

if @rowcount = 0
   RAISERROR('A megadott rĂ©gi dokumentum nem lĂ©tezik!',16,1)

--
if @Muvelet not in ('C','S')
   RAISERROR('A mĹ±velet C[satolĂˇs]/S[elejtezĂ©s] megadĂˇsa kĂ¶telezĹ‘!',16,1)

-- CSATOLĂS-rĂłl van szĂł?
if @Muvelet = 'C'
begin

   if @Csatolva_Id is NULL
      RAISERROR('Az Ăşj dokumentum ("utĂłzmĂˇny") megadĂˇsa kĂ¶telezĹ‘!',16,1)
   
   if @Selejtezve_Column is not NULL
      RAISERROR('A megadott rĂ©gi dokumentum mĂˇr selejtezett!',16,1)

   if @Csatolva_Rendszer_Column is not NULL
      RAISERROR('A megadott rĂ©gi dokumentum mĂˇr csatolt!',16,1)

   -- MJ: Ă©rdemes lenne mĂ©g itt megnĂ©zni, hogy az Ăşj dokumnetum
   --     1. egyĂˇltalĂˇn lĂ©tezik-e?
   --     2. nincs-e mĂˇr valami (mĂˇs) hozzĂˇcsatolva?
   --     3. nincs-e mĂˇr selejtezve?
   --     MAJD KĂ‰SĹBB !!!!!
end

---------------------------------------------------------
-- akkor jĂ¶het a tĂ©nyleges mĂłdosĂ­tĂˇs (update)            
---------------------------------------------------------

if @Muvelet = 'S'
begin

   update dbo.MIG_Foszam
      set Selejtezve        = 1,
          Selejtezes_Datuma = getdate()
     from dbo.MIG_Foszam
   where Id = @Id

end
else
begin

   update dbo.MIG_Foszam
      set Csatolva_Rendszer = 1,
          Csatolva_Id       = @Csatolva_Id
     from dbo.MIG_Foszam
   where Id = @Id

end

select  @error = @@error, @rowcount = @@rowcount

if @error <> 0
   RAISERROR('MIG_Foszam update hiba!',16,1)

if @rowcount = 0
   RAISERROR('A megadott rĂ©gi dokumentum kĂ¶zben mĂłdosult!',16,1)
-- ennek akkor lenne Ă©rtelme, ha az eredetileg felolvasott Ă©rtĂ©keket is hasznĂˇlnĂˇm a "where"-ben ...


if @sajat_tranz = 1  
   COMMIT TRANSACTION

END TRY
-----------
-----------
BEGIN CATCH
   if @sajat_tranz = 1  
      ROLLBACK TRANSACTION
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------