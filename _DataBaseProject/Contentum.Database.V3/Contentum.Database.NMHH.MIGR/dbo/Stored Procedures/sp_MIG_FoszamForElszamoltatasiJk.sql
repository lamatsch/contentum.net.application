﻿CREATE procedure [dbo].[sp_MIG_FoszamForElszamoltatasiJk]
  @Where			nvarchar(MAX) = '',
  @UgyintezoNev	nvarchar(100) = null,
  @UgyintezoId	uniqueidentifier = null,
  @OrderBy			nvarchar(400) = ' order by MIG_Foszam.EdokSav,MIG_Foszam.UI_YEAR,MIG_Foszam.UI_NUM,MIG_Alszam.ALNO',
  @ExecutorUserId	uniqueidentifier,
  @TopRow NVARCHAR(5) = ''

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
--
--  SET @sqlcmd = 
--  '
--		select(select dbo.fn_MergeFoszam(MIG_Sav.Name, MIG_Foszam.UI_NUM, MIG_Foszam.UI_YEAR) as Foszam_Merge,
--			   MIG_Foszam.UI_YEAR as LetrehozasIdo_Rovid,
--			   MIG_Foszam.MEMO as Targy,
--			   Ugyirat_helye = 
--			   CASE MIG_Foszam.UGYHOL
--					WHEN ''3''
--					THEN ''Ügyintézőnél''
--					ELSE ''Skontróban''
--			   END,
--			   MIG_Alszam.ALNO as Alszam,
--			   dbo.fn_MergeIktatoszam(MIG_Sav.Name, MIG_Foszam.UI_NUM, MIG_Alszam.ALNO, MIG_Foszam.UI_YEAR) as IktatoSzam_Merge,
--			   CONVERT(nvarchar(10), MIG_Alszam.ERK, 102) as Erkezett
--		from MIG_Foszam
--			inner join MIG_Sav on MIG_Sav.Id = MIG_Foszam.MIG_Sav_Id and CHARINDEX(''('', MIG_Sav.NAME) = 1 AND CHARINDEX('')'', MIG_Sav.NAME) > 2
--			left join MIG_Eloado on MIG_Eloado.Id = MIG_Foszam.MIG_Eloado_Id 
--			inner join MIG_Alszam on MIG_Alszam.MIG_Foszam_Id = MIG_Foszam.Id 
--		where MIG_Foszam.Edok_Utoirat_Id IS NULL 
--			and MIG_Foszam.Csatolva_Id IS NULL '
--   if @Where is not null and @Where!=''
--	begin 
--		SET @sqlcmd = @sqlcmd + ' and ' + @Where
--	end
--	SET @sqlcmd = @sqlcmd + @OrderBy + '
--
-- FOR XML AUTO, ELEMENTS, ROOT(''NewDataSet'')) as string_xml 
-- '

	DECLARE @sqlcmdDistinct nvarchar(10)
	IF @UgyintezoId is null or @UgyintezoNev is null or @UgyintezoNev = ''
	BEGIN
		SET @sqlcmdDistinct = ' distinct '
	END
	ELSE
	BEGIN
		SET @sqlcmdDistinct = ''
	END

	DECLARE @sqlcmdFoszam nvarchar(MAX)
	set @sqlcmdFoszam =   ' select ' + @sqlcmdDistinct + ' dbo.fn_MergeFoszam(MIG_Foszam.EdokSav, MIG_Foszam.UI_NUM, MIG_Foszam.UI_YEAR) as Foszam_Merge,
		MIG_Foszam.EdokSav,
		MIG_Foszam.UI_YEAR, -- as LetrehozasIdo_Rovid,
		MIG_Foszam.MEMO, --as Targy,
		Ugyirat_helye = 
			CASE MIG_Foszam.UGYHOL
				WHEN ''3''
				THEN ''Ügyintézőnél''
				ELSE ''Skontróban''
			END,
MIG_Foszam.UI_SAV,
MIG_Foszam.UI_NUM,
MIG_Foszam.Id
		from MIG_Foszam
			left join MIG_Eloado on MIG_Eloado.Id = MIG_Foszam.MIG_Eloado_Id
			inner join MIG_Alszam on MIG_Alszam.MIG_Foszam_Id = MIG_Foszam.Id 
		where MIG_Foszam.Edok_Utoirat_Id IS NULL 
			and MIG_Foszam.Csatolva_Id IS NULL
			and MIG_Foszam.MIG_Sav_Id IS NOT NULL
'

   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmdFoszam = @sqlcmdFoszam + ' and ' + @Where
	end


	DECLARE @sqlcmdAlszam nvarchar(MAX)
	set @sqlcmdAlszam =   ' select ' + @sqlcmdDistinct + ' MIG_Alszam.ALNO,
			dbo.fn_MergeIktatoszam(MIG_Foszam.EdokSav, MIG_Foszam.UI_NUM, MIG_Alszam.ALNO, MIG_Foszam.UI_YEAR) as IktatoSzam_Merge,
			CONVERT(nvarchar(10), MIG_Alszam.ERK, 102) as Erkezett,
MIG_Alszam.MIG_Foszam_Id
		from MIG_Alszam
			inner join MIG_Foszam on MIG_Alszam.MIG_Foszam_Id = MIG_Foszam.Id 
			left join MIG_Eloado on MIG_Eloado.Id = MIG_Foszam.MIG_Eloado_Id
		where MIG_Foszam.Edok_Utoirat_Id IS NULL 
			and MIG_Foszam.Csatolva_Id IS NULL
			and MIG_Foszam.MIG_Sav_Id IS NOT NULL
'

   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmdAlszam = @sqlcmdAlszam + ' and ' + @Where
	end

  SET @sqlcmd = 'select(
select ' + @LocalTopRow + '
MIG_Foszam.Foszam_Merge,
MIG_Foszam.UI_YEAR as LetrehozasIdo_Rovid,
MIG_Foszam.MEMO as Targy,
MIG_Foszam.Ugyirat_helye,
MIG_Alszam.ALNO as Alszam,
MIG_Alszam.IktatoSzam_Merge,
MIG_Alszam.Erkezett
from (
' + @sqlcmdFoszam


   if @UgyintezoId is not null
	begin 
		SET @sqlcmd = @sqlcmd + ' and MIG_Foszam.EDOK_Ugyintezo_Csoport_Id=@UgyintezoId'

		if @UgyintezoNev is not null and @UgyintezoNev <> ''
		begin
			SET @sqlcmd = @sqlcmd + '
UNION
' + @sqlcmdFoszam

		end
	end

if @UgyintezoNev is not null and @UgyintezoNev <> ''
begin

   SET @sqlcmd = @sqlcmd + ' and (MIG_Foszam.Edok_Ugyintezo_Csoport_Id is null and MIG_Eloado.NAME=@UgyintezoNev)'

end -- @UgyintezoNev is not null

	SET @sqlcmd = @sqlcmd + ') as MIG_Foszam
INNER JOIN (
' + @sqlcmdAlszam

   if @UgyintezoId is not null
	begin 
		SET @sqlcmd = @sqlcmd + ' and MIG_Foszam.EDOK_Ugyintezo_Csoport_Id=@UgyintezoId'

		if @UgyintezoNev is not null and @UgyintezoNev <> ''
		begin
			SET @sqlcmd = @sqlcmd + '
UNION
' + @sqlcmdAlszam

		end
	end

if @UgyintezoNev is not null and @UgyintezoNev <> ''
begin

   SET @sqlcmd = @sqlcmd + ' and (MIG_Foszam.Edok_Ugyintezo_Csoport_Id is null and MIG_Eloado.NAME=@UgyintezoNev)'

end -- @UgyintezoNev is not null

	SET @sqlcmd = @sqlcmd + ') as MIG_Alszam
on MIG_Foszam.Id=MIG_Alszam.MIG_Foszam_Id
'

		SET @sqlcmd = @sqlcmd + @OrderBy
		SET @sqlcmd = @sqlcmd + '
 FOR XML AUTO, ELEMENTS, ROOT(''NewDataSet'')) as string_xml 
'

--   exec(@sqlcmd)
	execute sp_executesql @sqlcmd,N'@UgyintezoId uniqueidentifier, @UgyintezoNev nvarchar(100)'
		, @UgyintezoId=@UgyintezoId, @UgyintezoNev=@UgyintezoNev;


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end