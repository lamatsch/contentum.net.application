﻿CREATE procedure [dbo].[sp_MIG_FoszamForElszamoltatasiJk_old]
  @Where			nvarchar(MAX) = '',
  @OrderBy			nvarchar(400) = ' order by Foszam_Merge',
  @ExecutorUserId	uniqueidentifier,
  @TopRow NVARCHAR(5) = ''

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  '
		select(select dbo.fn_MergeFoszam(MIG_Sav.Name, MIG_Foszam.UI_NUM, MIG_Foszam.UI_YEAR) as Foszam_Merge,
			   MIG_Foszam.UI_YEAR as LetrehozasIdo_Rovid,
			   MIG_Foszam.MEMO as Targy,
			   Ugyirat_helye = 
			   CASE MIG_Foszam.UGYHOL
					WHEN ''3''
					THEN ''Ügyintézőnél''
					ELSE ''Skontróban''
			   END,
			   MIG_Alszam.ALNO as Alszam,
			   dbo.fn_MergeIktatoszam(MIG_Sav.Name, MIG_Foszam.UI_NUM, MIG_Alszam.ALNO, MIG_Foszam.UI_YEAR) as IktatoSzam_Merge,
			   CONVERT(nvarchar(10), MIG_Alszam.ERK, 102) as Erkezett
		from MIG_Foszam
			inner join MIG_Sav on MIG_Sav.Id = MIG_Foszam.MIG_Sav_Id and CHARINDEX(''('', MIG_Sav.NAME) = 1 AND CHARINDEX('')'', MIG_Sav.NAME) > 2
			left join MIG_Eloado on MIG_Eloado.Id = MIG_Foszam.MIG_Eloado_Id 
			inner join MIG_Alszam on MIG_Alszam.MIG_Foszam_Id = MIG_Foszam.Id '
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
	SET @sqlcmd = @sqlcmd + @OrderBy + '

 FOR XML AUTO, ELEMENTS, ROOT(''NewDataSet'')) as string_xml 
 '

   exec(@sqlcmd)


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end