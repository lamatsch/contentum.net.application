﻿CREATE procedure [dbo].[sp_MIG_FoszamForXml]
  @Where			nvarchar(MAX) = '',
  @OrderBy			nvarchar(400) = ' order by Foszam_Merge',
  @ExecutorUserId	uniqueidentifier,
  @TopRow NVARCHAR(5) = ''

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

--  SET @sqlcmd = 
--  '
--		--select(
--'
  SET @sqlcmd = 
  'select ' + @LocalTopRow + ' dbo.fn_MergeFoszam(MIG_Foszam.EdokSav, MIG_Foszam.UI_NUM, MIG_Foszam.UI_YEAR) as Foszam_Merge,
			   MIG_Foszam.UI_YEAR as LetrehozasIdo_Rovid,
			   MIG_Foszam.UI_NAME as NevSTR_Ugyindito,
			   MIG_Foszam.MEMO as Targy,
			   KRT_Csoportok_Orzo_Nev = case
					when MIG_Foszam.EDOK_Ugyintezo_Csoport_Id is not null then MIG_Foszam.EDOK_Ugyintezo_Nev
					else MIG_Eloado.NAME
				end,
			   null as PotlistaOrzoNev,
			   ktUGYHOL.Nev Ugyirat_helye
		from MIG_Foszam
			left join MIG_Eloado on (EDOK_Ugyintezo_Csoport_Id is null and MIG_Eloado.Id = MIG_Foszam.MIG_Eloado_Id)'

set @sqlcmd = @sqlcmd + '
left join (select ''1'' Kod, ''Skontróban'' Nev
UNION ALL
select ''2'' Kod, ''Irattár'' Nev
UNION ALL
select ''3'' Kod, ''Osztályon'' Nev
UNION ALL
select ''4'' Kod, ''Közt.Hiv'' Nev
UNION ALL
select ''5'' Kod, ''Bíróság'' Nev
UNION ALL
select ''6'' Kod, ''Ügyészség'' Nev
UNION ALL
select ''7'' Kod, ''Vezetőség'' Nev
UNION ALL
select ''8'' Kod, ''Közjegyző'' Nev
UNION ALL
select ''9'' Kod, ''FPH'' Nev
UNION ALL
select ''S'' Kod, ''Selejtezett'' Nev
UNION ALL
select ''V'' Kod, ''Selejtezésre vár'' Nev
UNION ALL
select ''I'' Kod, ''Irattárból elkért'' Nev
UNION ALL
select ''E'' Kod, ''Skontróból elkért'' Nev
UNION ALL
select ''L'' Kod, ''Lomtár'' Nev
) ktUGYHOL on ktUGYHOL.Kod=MIG_Foszam.UGYHOL
where MIG_Foszam.EdokSav is not null
'

   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
	SET @sqlcmd = @sqlcmd + @OrderBy

--SET @sqlcmd = @sqlcmd + '
-- --FOR XML AUTO, ELEMENTS, ROOT(''NewDataSet'')) as string_xml 
-- '
   exec(@sqlcmd)


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end