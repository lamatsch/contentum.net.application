﻿CREATE procedure [dbo].[sp_MIG_FoszamForXmlNincsUgyintezo]
  @Where			nvarchar(MAX) = '',
  @OrderBy			nvarchar(400) = ' order by KRT_Felhasznalok.NAME,EREC_UgyUgyiratok.UI_NUM',
  @ExecutorUserId	uniqueidentifier,
  @TopRow NVARCHAR(5) = ''

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  '
		select(
select' + @LocalTopRow + ' KRT_Felhasznalok.NAME as Nev,
		EREC_UgyUgyiratok.MEMO as Targy,
		(select ktUGYHOL.Nev) Ugyirat_helye,
		EREC_UgyUgyiratok.UI_NAME as NevSTR_Ugyindito,
		dbo.fn_MergeFoszam(EREC_UgyUgyiratok.EdokSav, EREC_UgyUgyiratok.UI_NUM, EREC_UgyUgyiratok.UI_YEAR) as Foszam_Merge,
			   EREC_UgyUgyiratok.UI_YEAR as LetrehozasIdo_Rovid   		   
		from MIG_Eloado as KRT_Felhasznalok
			inner join MIG_Foszam as EREC_UgyUgyiratok on EREC_UgyUgyiratok.MIG_Eloado_Id = KRT_Felhasznalok.Id
'
set @sqlcmd = @sqlcmd + '
left join (select ''1'' Kod, ''Skontróban'' Nev
UNION ALL
select ''2'' Kod, ''Irattár'' Nev
UNION ALL
select ''3'' Kod, ''Osztályon'' Nev
UNION ALL
select ''4'' Kod, ''Közt.Hiv'' Nev
UNION ALL
select ''5'' Kod, ''Bíróság'' Nev
UNION ALL
select ''6'' Kod, ''Ügyészség'' Nev
UNION ALL
select ''7'' Kod, ''Vezetőség'' Nev
UNION ALL
select ''8'' Kod, ''Közjegyző'' Nev
UNION ALL
select ''9'' Kod, ''FPH'' Nev
UNION ALL
select ''S'' Kod, ''Selejtezett'' Nev
UNION ALL
select ''V'' Kod, ''Selejtezésre vár'' Nev
UNION ALL
select ''I'' Kod, ''Irattárból elkért'' Nev
UNION ALL
select ''E'' Kod, ''Skontróból elkért'' Nev
UNION ALL
select ''L'' Kod, ''Lomtár'' Nev
) ktUGYHOL on ktUGYHOL.Kod=EREC_UgyUgyiratok.UGYHOL
where MIG_Sav_Id is not null
'

   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
	SET @sqlcmd = @sqlcmd + @OrderBy + '

 FOR XML AUTO, ELEMENTS, ROOT(''NewDataSet'')) as string_xml 
 '

   exec(@sqlcmd)


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end