﻿CREATE procedure [dbo].[sp_MIG_FoszamGet]
       @Id uniqueidentifier,
       @ExecutorUserId uniqueidentifier = NULL  -- ezt egyelőre nem használjuk!!!

/*
Használata például:

exec sp_MIG_FoszamGet
     @Id      = '14543938-3344-4CE8-9FCF-2772F1049DD8'
*/

as

begin

BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

  SET @sqlcmd = 
  'select ' + 
  '
		MIG_Foszam.Id,
		MIG_Foszam.EdokSav as UI_SAV,
		MIG_Foszam.UI_SAV as OLD_UI_SAV,
		MIG_Foszam.UI_YEAR,
		MIG_Foszam.UI_NUM,
		MIG_Foszam.UI_NAME,
		MIG_Foszam.UI_IRSZ,
		MIG_Foszam.UI_UTCA,
		MIG_Foszam.UI_HSZ,
		MIG_Foszam.UI_HRSZ,
		MIG_Foszam.UI_TYPE,
		MIG_Foszam.UI_IRJ,
		MIG_Foszam.UI_PERS,
		MIG_Foszam.UI_OT_ID,
		MIG_Foszam.MEMO,
		MIG_Foszam.IRSZ_PLUSS,
		MIG_Foszam.IRJ2000,
		MIG_Foszam.Conc,
		MIG_Foszam.Selejtezve,
		MIG_Foszam.Selejtezes_Datuma,
		MIG_Foszam.Csatolva_Rendszer,
		MIG_Foszam.Csatolva_Id,
		MIG_Foszam.MIG_Sav_Id,
		MIG_Foszam.MIG_Varos_Id,
		MIG_Foszam.MIG_IktatasTipus_Id,
		MIG_Foszam.MIG_Eloado_Id,
		MIG_Foszam.Edok_Ugyintezo_Csoport_Id,
		MIG_Foszam.Edok_Ugyintezo_Nev,
		MIG_Foszam.Edok_Utoirat_Id,
		MIG_Foszam.Edok_Utoirat_Azon,
		MIG_Foszam.UGYHOL,
		MIG_Foszam.Ugy_kezdete,
		convert(varchar, MIG_Foszam.Hatarido, 102) as Hatarido,
		convert(varchar, MIG_Foszam.IRATTARBA, 102) as IRATTARBA,
		convert(varchar, MIG_Foszam.SCONTRO, 102) as SCONTRO,
		MIG_Foszam.Ver,
		MIG_Foszam.EdokSav,
		MIG_Foszam.Feladat,
		MIG_Foszam.Szervezet,
		MIG_Foszam.IrattarbolKikeroNev,
		MIG_Foszam.Ugyirat_tipus,
		MIG_Foszam.IrattarbolKikeres_Datuma,
		MIG_Foszam.MegorzesiIdo,
		MIG_Foszam.IrattarId,
		MIG_Foszam.IrattariHely
  from MIG_Foszam as MIG_Foszam
 where MIG_Foszam.Id = @Id'

	print @sqlcmd  -- teszt kiirás

	exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY


BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1
		RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH

end
