﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1 from  sysobjects
           where  id = object_id('sp_MIG_FoszamGetAll')
             and  type in ('P'))
   drop procedure sp_MIG_FoszamGetAll
go

CREATE PROCEDURE [dbo].[sp_MIG_FoszamGetAll]
       @Where nvarchar(MAX)  = '',
--	   @ExtendedMIG_SavWhere nvarchar(MAX) = '',
       @ExtendedMIG_AlszamWhere nvarchar(MAX) = '',
       @ExtendedMIG_EloadoWhere nvarchar(MAX) = '',
       @OrderBy nvarchar(200) = 'order by MIG_Foszam.EdokSav, MIG_Foszam.UI_YEAR DESC, MIG_Foszam.UI_NUM DESC', --'order by UI_SAV, UI_YEAR DESC, UI_NUM DESC',
       @TopRow nvarchar(5)    = '0',
       @ExecutorUserId uniqueidentifier = NULL,  -- ezt egyelőre nem használjuk!!!
       @pageNumber		int = 0,
       @pageSize			int = -1,
       @SelectedRowId	uniqueidentifier = null,
	   @FilterFelulvizsgalat nvarchar(2) = null

/*
Használata például:

exec sp_MIG_FoszamGetAll 
     @Where   = 'contains( *, ''"önk"'' )',
     @OrderBy = 'order by ui_sav, ui_year, ui_num',
     @TopRow  = '1000'
*/

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)
   DECLARE @LocalTopRow nvarchar(10)
   declare @firstRow int
   declare @lastRow int

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
		if (@pageSize > @TopRow)
			SET @pageSize = @TopRow;
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
                  
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = null
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end
	set @sqlcmd = '';

   DECLARE @Org uniqueidentifier

/*
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
*/
	/************************************************************
	* Szűrési feltételek sorrendjének összeállítása				*
	************************************************************/
	create table #filterOrder (Filter nvarchar(50), RowNumber bigint);

    -- ha előadó és alszám szűrés is meg van adva, a következő filter úgyis leszűri
	IF @ExtendedMIG_AlszamWhere is not null and @ExtendedMIG_AlszamWhere != ''
       and (@ExtendedMIG_EloadoWhere is null or @ExtendedMIG_EloadoWhere = '')
	BEGIN
		SET @sqlcmd = @sqlcmd + N' insert into #filterOrder select ''ExtendedMIG_AlszamWhere'',
				count(MIG_Foszam.Id)
					from MIG_Foszam
						inner join MIG_Alszam on MIG_Foszam.Id = MIG_Alszam.MIG_Foszam_Id and (' + @ExtendedMIG_AlszamWhere + ') ';
	END
	
	IF @ExtendedMIG_EloadoWhere is not null and @ExtendedMIG_EloadoWhere != ''
	BEGIN
        SET @sqlcmd = @sqlcmd + N' insert into #filterOrder select ''ExtendedMIG_EloadoWhere'',
				count(MIG_Foszam.Id)
					from MIG_Foszam
						inner join MIG_Alszam on MIG_Foszam.Id = MIG_Alszam.MIG_Foszam_Id
                        inner join MIG_Eloado on MIG_Eloado.Id = MIG_Alszam.MIG_Eloado_Id and (' + @ExtendedMIG_EloadoWhere + ') ';
        IF @ExtendedMIG_AlszamWhere is not null and @ExtendedMIG_AlszamWhere != ''
	    BEGIN
            SET @sqlcmd = @sqlcmd + N' and (' + @ExtendedMIG_AlszamWhere + ') '
        END
	END

	exec sp_executesql @sqlcmd;

	/************************************************************
	* Szűrési tábla összeállítása								*
	************************************************************/
	declare @whereFilter nvarchar(50);
	declare cur cursor local fast_forward read_only for
		select Filter from #filterOrder order by RowNumber DESC;

	SET @sqlcmd = 'select MIG_Foszam.Id into #filter from MIG_Foszam
'
--inner join MIG_Sav on MIG_Sav.Id = MIG_Foszam.MIG_Sav_Id
--	 '  
--   if @ExtendedMIG_SavWhere is not null AND @ExtendedMIG_SavWhere != ''
--   begin
--		SET @sqlcmd = @sqlcmd + ' AND ' + @ExtendedMIG_SavWhere
--   end

--   SET @sqlcmd = @sqlcmd + ' 
--	LEFT JOIN MIG_Eloado ON MIG_Eloado.Id = MIG_Foszam.MIG_Eloado_Id
--	where CHARINDEX(''('', MIG_Sav.NAME) = 1 AND CHARINDEX('')'', MIG_Sav.NAME) > 2 '

   SET @sqlcmd = @sqlcmd + ' 
	LEFT JOIN MIG_Eloado ON MIG_Eloado.Id = MIG_Foszam.MIG_Eloado_Id
	where MIG_Foszam.EdokSav is not null '

   if @Where is not null and @Where != ''
	BEGIN 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	END

	open cur;
	fetch next from cur into @whereFilter;
	while @@FETCH_STATUS = 0
	BEGIN
		IF @whereFilter = 'ExtendedMIG_AlszamWhere'
			set @sqlcmd = @sqlcmd + ' delete from #filter where Id not in
(select MIG_Foszam.Id
    from MIG_Foszam
	inner join MIG_Alszam on MIG_Foszam.Id = MIG_Alszam.MIG_Foszam_Id and (' + @ExtendedMIG_AlszamWhere + ')) ';
	
		IF @whereFilter = 'ExtendedMIG_EloadoWhere'
        BEGIN
			set @sqlcmd = @sqlcmd + ' delete from #filter where Id not in
(select MIG_Foszam.Id
    from MIG_Foszam
    inner join MIG_Alszam on MIG_Foszam.Id = MIG_Alszam.MIG_Foszam_Id
    inner join MIG_Eloado on MIG_Eloado.Id = MIG_Alszam.MIG_Eloado_Id and (' + @ExtendedMIG_EloadoWhere + ') ';
            IF @ExtendedMIG_AlszamWhere is not null and @ExtendedMIG_AlszamWhere != ''
	        BEGIN
                SET @sqlcmd = @sqlcmd + N' and ' + @ExtendedMIG_AlszamWhere
            END
	        SET @sqlcmd = @sqlcmd + ') ';
        END
		fetch next from cur into @whereFilter;
	END
	close cur;
	deallocate cur;

	IF @FilterFelulvizsgalat IS NOT NULL
	BEGIN
		SET @sqlcmd = @sqlcmd + N'
		delete from #filter where Id not in
		(
			SELECT foszam.Id
			FROM MIG_Foszam foszam
			CROSS APPLY dbo.fn_GetMegorzesiMod(foszam.UI_YEAR, foszam.UI_IRJ, foszam.IRJ2000) MegorzesiModTable
		    CROSS APPLY dbo.fn_GetFoszamHierarchy_MegorzesiIdo(foszam.Id) MegorzesiIdoTable
			WHERE
			MegorzesiModTable.MegorzesiMod = @FilterFelulvizsgalat
			AND foszam.MegorzesiIdo < @CurrentDate
			AND MegorzesiIdoTable.MaxMegorzesiIdo < @CurrentDate
		)
		'
	END

 	/************************************************************
	* Szűrt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/
-- a több helyről összefésült Eloado_Nev mezőt már itt lekérjük az esetleges rendezés miatt
/* --- BogI, 2019.09.27.
		SET @sqlcmd = @sqlcmd + N'
select row_number() over('+@OrderBy+') as RowNumber,
    MIG_Foszam.Id, tmp.Eloado_Nev as Eloado_Nev
 into #result
    from
(select MIG_Foszam.Id as Id, 
       case when MIG_Foszam.Edok_Ugyintezo_Csoport_Id is null then (select MIG_Eloado.NAME from MIG_Eloado where MIG_Eloado.Id = MIG_Foszam.MIG_Eloado_Id)
       else (select MIG_Foszam.Edok_Ugyintezo_Nev) end as Eloado_Nev
    from MIG_Foszam
where MIG_Foszam.Id in (select Id from #filter)) tmp
inner join MIG_Foszam on tmp.Id=MIG_Foszam.Id
'
--- */
/* --- BogI, 2019.09.27. */
		SET @sqlcmd = @sqlcmd + N'
select row_number() over('+@OrderBy+') as RowNumber,
       #filter.Id, 
	   case when MIG_Foszam.Edok_Ugyintezo_Csoport_Id is Null then MIG_Eloado.Name
	        else MIG_Foszam.Edok_Ugyintezo_Nev
	   end as Eloado_Nev
  into #result
  from #filter
 inner join MIG_Foszam on MIG_Foszam.Id = #filter.Id
 left join MIG_Eloado on MIG_Eloado.Id = MIG_Foszam.MIG_Eloado_Id 
'
/* BogI --- */

--inner join MIG_Sav on MIG_Foszam.MIG_Sav_Id=MIG_Sav.Id
--where MIG_Foszam.Id in (select Id from #filter)
-- '

		if (@SelectedRowId is not null)
			set @sqlcmd = @sqlcmd + N'
			if exists (select 1 from #result where Id = @SelectedRowId)
			BEGIN
				select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
				set @firstRow = (@pageNumber - 1) * @pageSize + 1;
				set @lastRow = @pageNumber*@pageSize;
				select @pageNumber = @pageNumber - 1
			END
			ELSE'
		--ELSE
			set @sqlcmd = @sqlcmd + N' 
				if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
				BEGIN
					select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
					set @firstRow = (@pageNumber - 1) * @pageSize + 1;
					set @lastRow = @pageNumber*@pageSize;
					select @pageNumber = @pageNumber - 1
				END ;'

	/************************************************************
	* Tényleges select											*
	************************************************************/
  SET @sqlcmd = @sqlcmd + N'
	select	 
	(SELECT COUNT(MIG_Dokumentum.Id) 
FROM MIG_Foszam 
JOIN MIG_Alszam ON MIG_Foszam.Id = MIG_Alszam.MIG_Foszam_Id
JOIN MIG_Dokumentum ON MIG_Alszam.Id = MIG_Dokumentum.MIG_Alszam_Id
WHERE MIG_Foszam.Id = #result.Id) as Csatolmany_Count,
		MIG_Foszam.Id,
		MIG_Foszam.UI_SAV as OLD_UI_SAV,
		MIG_Foszam.EdokSav as UI_SAV,
		MIG_Foszam.EdokSav, -- eredeti nevén is kell
	   dbo.fn_MergeFoszam(MIG_Foszam.EdokSav, MIG_Foszam.UI_NUM, MIG_Foszam.UI_YEAR) as Foszam_Merge,
		MIG_Foszam.UI_YEAR,
		MIG_Foszam.UI_NUM,
		MIG_Foszam.UI_NAME,
		MIG_Foszam.UI_IRSZ,
		MIG_Foszam.UI_UTCA,
		MIG_Foszam.UI_HSZ,
		MIG_Foszam.UI_HRSZ,
		MIG_Foszam.UI_TYPE,
		dbo.fn_GetIrattariTetelszam(MIG_Foszam.UI_YEAR, MIG_Foszam.UI_TYPE) as IrattariTetelszam, 
		MIG_Foszam.UI_IRJ,
		MIG_Foszam.UI_PERS,
		MIG_Foszam.UI_OT_ID,
		MIG_Foszam.MEMO,
		MIG_Foszam.IRSZ_PLUSS,
		MIG_Foszam.IRJ2000,
		MIG_Foszam.Conc,
		MIG_Foszam.Ugyirat_tipus,
		MIG_Foszam.Eltelt_napok,
		MIG_Foszam.Ugy_kezdete,
		MIG_Foszam.Ugykezeles_modja,
		MIG_Foszam.Egyeb_adat2,
		MIG_Foszam.Munkanapos,
		MIG_Foszam.Hatarido,
		MIG_Foszam.Feladat,
		MIG_Foszam.Targyszavak,
		MIG_Foszam.Kulso_eloirat,
		MIG_Foszam.Felfuggesztve,
		MIG_Foszam.Szervezet,
		MIG_Foszam.Selejtezve,
		MIG_Foszam.Selejtezes_Datuma,
		MIG_Foszam.Csatolva_Rendszer,
		MIG_Foszam.Csatolva_Id,
      dbo.fn_GetMIGFoszam(MIG_Foszam.Csatolva_Id) AS MergeFoszam_Csatolt,
      (SELECT STUFF((SELECT CAST(''|||'' + dbo.fn_GetMIGFoszam(MIG_Foszam_Eloirat.Id) + ''***'' + convert(NVARCHAR(40), MIG_Foszam_Eloirat.Id) AS VARCHAR(MAX))
		 FROM [MIG_Foszam] AS MIG_Foszam_Eloirat
		 WHERE [MIG_Foszam_Eloirat].Csatolva_Id = MIG_Foszam.Id FOR XML PATH ('''')), 1, 3, '''')) AS Eloirat_Azon_Id,
(SELECT STUFF((SELECT CAST(''|||'' + dbo.fn_GetMIGFoszam(MIG_Foszam_Eloirat.Id) AS VARCHAR(MAX)) FROM [MIG_Foszam] AS MIG_Foszam_Eloirat WHERE [MIG_Foszam_Eloirat].Csatolva_Id = MIG_Foszam.Id FOR XML PATH ('''')), 1, 3, '''')) as Eloirat_Azon,
		MIG_Foszam.UGYHOL,
Utoirat_Azon = 
CASE WHEN MIG_Foszam.Csatolva_Id IS NULL 
	 THEN MIG_Foszam.Edok_Utoirat_Azon
	 ELSE dbo.fn_GetMIGFoszam(MIG_Foszam.Csatolva_Id)
END,
		convert(varchar, MIG_Foszam.IRATTARBA, 102) as IRATTARBA,
		convert(varchar, MIG_Foszam.SCONTRO, 102) as SCONTRO,
		MIG_Foszam.MIG_Sav_Id,
		--MIG_Sav.NAME as Sav_Nev,
		--dbo.fn_GetMegkulJelzes(MIG_Sav.NAME) as MegkulJelzes,
		MIG_Foszam.EdokSav as MegkulJelzes,
		MIG_Foszam.MIG_Varos_Id,
		MIG_Foszam.MIG_IktatasTipus_Id,
		MIG_Foszam.MIG_Eloado_Id,
	   --MIG_Eloado.NAME AS Eloado_Nev,
		MIG_Foszam.Edok_Ugyintezo_Csoport_Id,
		MIG_Foszam.Edok_Ugyintezo_Nev,
--		case when MIG_Foszam.Edok_Ugyintezo_Csoport_Id is null then (select MIG_Eloado.NAME from MIG_Eloado where MIG_Eloado.Id = MIG_Foszam.MIG_Eloado_Id)
--		else (select MIG_Foszam.Edok_Ugyintezo_Nev) end as Eloado_Nev,
	#result.Eloado_Nev as Eloado_Nev,
		case when MIG_Foszam.Edok_Ugyintezo_Csoport_Id is null then MIG_Foszam.MIG_Eloado_Id
		else MIG_Foszam.Edok_Ugyintezo_Csoport_Id end as Eloado_Id,
	   MIG_Foszam.Edok_Utoirat_Id,
		MIG_Foszam.Edok_Utoirat_Azon,
		MIG_Foszam.Ver,
		MIG_Alszam.ALNO,
		MIG_Foszam.IrattarbolKikeroNev,
		MIG_Foszam.IrattarbolKikeres_Datuma,
	    convert(varchar, MIG_Foszam.MegorzesiIdo, 102) as MegorzesiIdo,
		MIG_Foszam.IrattarId,
		MIG_Foszam.IrattariHely
  from MIG_Foszam as MIG_Foszam
  inner join #result on #result.Id = MIG_Foszam.Id 
  '
/* --- BogI, 2019.09.27.
   SET @sqlcmd = @sqlcmd + 'left join MIG_Eloado ON MIG_Eloado.Id = MIG_Foszam.MIG_Eloado_Id
'
BogI --- */

/* --- BogI, 2019.09.27.
	SET @sqlcmd = @sqlcmd + 'left join MIG_Alszam 
on MIG_Foszam.Id = MIG_Alszam.MIG_Foszam_Id
and MIG_Alszam.ALNO = (SELECT max(mi.ALNO) FROM MIG_Alszam mi where mi.MIG_Foszam_Id = MIG_Foszam.Id)
'
BogI --- */
/* --- BogI, 2019.09.27. */
	SET @sqlcmd = @sqlcmd + 'left join ( select MIG_Alszam.MIG_Foszam_ID, max(MIG_Alszam.ALNO) as ALNO from MIG_Alszam group by MIG_Alszam.MIG_Foszam_ID ) as MIG_Alszam
	                                on MIG_Alszam.MIG_Foszam_ID = MIG_Foszam.ID
'
/* BogI --- */

   IF  @lastRow is not null
       SET @sqlcmd = @sqlcmd + 'where RowNumber between @firstRow and @lastRow
ORDER BY #result.RowNumber;'
  ELSE
       SET @sqlcmd = @sqlcmd + 'where RowNumber >= @firstRow ORDER BY #result.RowNumber;'

--   --print @sqlcmd  -- teszt kiirás
--   EXEC (@sqlcmd);

    -- találatok száma és oldalszám
    set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter;';
    
	declare @CurrentDate datetime = getdate()

    execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int,@SelectedRowId uniqueidentifier, @FilterFelulvizsgalat nvarchar(2), @CurrentDate datetime'
		,@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId, @FilterFelulvizsgalat = @FilterFelulvizsgalat, @CurrentDate = @CurrentDate


END TRY


BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER() < 50000
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1
		RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH

end
