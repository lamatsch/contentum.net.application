﻿CREATE PROCEDURE [dbo].[sp_MIG_FoszamGetAllDistinct]
       @Column nvarchar(64),
	   @Where nvarchar(MAX)  = ''

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX) = 'SELECT DISTINCT ' + @Column + ' FROM MIG_Foszam '

   if @Where is not null and @Where != ''
	BEGIN 
		SET @sqlcmd = @sqlcmd + ' WHERE ' + @Where
	END

	exec sp_executesql @sqlcmd;

    -- találatok száma és oldalszám
    --set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter;';
    
    --execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int,@SelectedRowId uniqueidentifier'
	--	,@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId;
END TRY


BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER() < 50000
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1
		RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH

end
