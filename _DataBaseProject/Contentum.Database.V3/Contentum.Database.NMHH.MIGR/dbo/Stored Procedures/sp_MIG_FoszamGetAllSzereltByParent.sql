﻿CREATE procedure [dbo].[sp_MIG_FoszamGetAllSzereltByParent]
		@ParentIdList	NVARCHAR(MAX), -- Id lista
		@Delimeter NVARCHAR(1) = ','

as

begin

BEGIN TRY

    set nocount on;
    
    -- (Azokat kell hozni, ahol a Csatolva_Id vagy Edok_Utoirat_Id ki van töltve)
    -- egyelőre csak Csatolva_Id
	WITH FoszamHierarchy  AS
	(
	   -- Base case	   
	   SELECT *,
			  1 as HierarchyLevel
	   FROM MIG_Foszam
	   WHERE Csatolva_Id IN 
	   (SELECT Value FROM dbo.fn_Split(@ParentIdList, @Delimeter) WHERE Value != '')
--			or
--	   Edok_Utoirat_Id IN 
--	   (SELECT Value FROM dbo.fn_Split(@ParentIdList, @Delimeter) WHERE Value != '')

	   UNION ALL

	   -- Recursive step
	   SELECT u.*,
		  uh.HierarchyLevel + 1 AS HierarchyLevel
	   FROM MIG_Foszam as u
		  INNER JOIN FoszamHierarchy as uh ON
			 u.Csatolva_Id = uh.Id
	)

	SELECT *
	FROM FoszamHierarchy
  

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end