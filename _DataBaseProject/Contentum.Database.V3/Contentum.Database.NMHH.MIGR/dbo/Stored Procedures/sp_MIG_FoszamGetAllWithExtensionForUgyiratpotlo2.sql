



CREATE procedure [dbo].[sp_MIG_FoszamGetAllWithExtensionForUgyiratpotlo2]
  @Where			nvarchar(MAX) = '',
  @OrderBy			nvarchar(200) = '',
  @TopRow			nvarchar(5) = '',
  @ExecutorUserId	uniqueidentifier,
  @id				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	--DECLARE @Org uniqueidentifier
	--SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	--if (@Org is null)
	--begin
	--	RAISERROR('[50202]',16,1)
	--end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
	 
  SET @sqlcmd = '
SELECT ' + @LocalTopRow + '
	   MIG_Foszam.ID,
	   CAST(MIG_Foszam.EdokSav AS NVARCHAR) +''/'' + CAST(MIG_Foszam.UI_NUM AS NVARCHAR) +''/'' + CAST(MIG_Foszam.UI_YEAR AS NVARCHAR)  as Foszam_Merge,
	   MIG_Foszam.UI_NAME              as Targy,
	   MIG_Foszam.UGYHOL			   as Irattar_Id,
	   MIG_Foszam.IrattariHely  as Irattar_neve,
	   CAST(NULL AS NVARCHAR)  as KikerKezd,
	   MIG_Foszam.IrattarbolKikeres_datuma as KiadasDatuma,
	   MIG_Foszam.Feladat      as FelhasznalasiCelNev,
	   MIG_Foszam.IrattarbolKikeroNev  as Kikero_IdNev,
	   dbo.[fn_FoszamUtolsoVegrehajtoId] (MIG_Foszam.ID) as Kapta_Id,
	   CAST(NULL AS NVARCHAR) as Kapta_IdNev,
	   CAST(NULL AS NVARCHAR) as Kiado_IdNev,
	   MIG_Foszam.hatarido         as KikerVege,
	   MIG_Foszam.IRJ2000             as IrattariTetelszam,
	   CAST(MIG_Foszam.EdokSav AS NVARCHAR) +''/'' + CAST(MIG_Foszam.UI_NUM AS NVARCHAR) +''/'' + CAST(MIG_Foszam.UI_YEAR AS NVARCHAR) as BARCODE
FROM 
		MIG_Foszam as MIG_Foszam
WHERE MIG_Foszam.Id = @id
'

if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end

   SET @sqlcmd = @sqlcmd + @OrderBy

  print @sqlcmd

  execute sp_executesql @sqlcmd, N'@ExecutorUserId uniqueidentifier, @id uniqueidentifier, @Where nvarchar(MAX), @OrderBy nvarchar(200), @TopRow nvarchar(5)'
      ,@ExecutorUserId = @ExecutorUserId, @id = @id, @Where = @Where, @OrderBy = @OrderBy, @TopRow = @TopRow

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
GO


