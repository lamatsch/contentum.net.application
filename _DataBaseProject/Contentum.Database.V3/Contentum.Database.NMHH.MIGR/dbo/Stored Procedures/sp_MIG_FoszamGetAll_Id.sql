﻿create procedure [dbo].[sp_MIG_FoszamGetAll_Id]
       @Where nvarchar(MAX)  = '',
--	   @ExtendedMIG_SavWhere nvarchar(MAX) = '',
       @ExtendedMIG_AlszamWhere nvarchar(MAX) = '',
       @ExtendedMIG_EloadoWhere nvarchar(MAX) = '',
       @ExtendedMIG_JegyzekTetelekWhere nvarchar(MAX) = ''

/*
Használata például:

exec sp_MIG_FoszamGetAll 
     @Where   = 'contains( *, ''"önk"'' )',
     @OrderBy = 'order by ui_sav, ui_year, ui_num',
     @TopRow  = '1000'
*/

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)
set @sqlcmd = '';
	/************************************************************
	* Szűrési feltételek sorrendjének összeállítása				*
	************************************************************/
	create table #filterOrder (Filter nvarchar(50), RowNumber bigint);

    -- ha előadó és alszám szűrés is meg van adva, a következő filter úgyis leszűri
	IF @ExtendedMIG_AlszamWhere is not null and @ExtendedMIG_AlszamWhere != ''
       and (@ExtendedMIG_EloadoWhere is null or @ExtendedMIG_EloadoWhere != '')
	BEGIN
		SET @sqlcmd = @sqlcmd + N' insert into #filterOrder select ''ExtendedMIG_AlszamWhere'',
				count(MIG_Foszam.Id)
					from MIG_Foszam
						inner join MIG_Alszam on MIG_Foszam.Id = MIG_Alszam.MIG_Foszam_Id and (' + @ExtendedMIG_AlszamWhere + ') ';
	END
	
	IF @ExtendedMIG_EloadoWhere is not null and @ExtendedMIG_EloadoWhere != ''
	BEGIN
        SET @sqlcmd = @sqlcmd + N' insert into #filterOrder select ''ExtendedMIG_EloadoWhere'',
				count(MIG_Foszam.Id)
					from MIG_Foszam
						inner join MIG_Alszam on MIG_Foszam.Id = MIG_Alszam.MIG_Foszam_Id
                        inner join MIG_Eloado on MIG_Eloado.Id = MIG_Alszam.MIG_Eloado_Id and (' + @ExtendedMIG_EloadoWhere + ') ';
        IF @ExtendedMIG_AlszamWhere is not null and @ExtendedMIG_AlszamWhere != ''
	    BEGIN
            SET @sqlcmd = @sqlcmd + N' and (' + @ExtendedMIG_AlszamWhere + ') '
        END
	END
	
	exec sp_executesql @sqlcmd;

	/************************************************************
	* Szűrési tábla összeállítása								*
	************************************************************/
	declare @whereFilter nvarchar(50);
	declare cur cursor local fast_forward read_only for
		select Filter from #filterOrder order by RowNumber DESC;

	SET @sqlcmd = 'select MIG_Foszam.Id into #filter from MIG_Foszam
'

   SET @sqlcmd = @sqlcmd + ' 
	LEFT JOIN MIG_Eloado ON MIG_Eloado.Id = MIG_Foszam.MIG_Eloado_Id
	where MIG_Foszam.EdokSav is not null'

   if @Where is not null and @Where != ''
	BEGIN 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	END
	
	IF @ExtendedMIG_JegyzekTetelekWhere is not null and @ExtendedMIG_JegyzekTetelekWhere != ''
	BEGIN
        SET @sqlcmd = @sqlcmd + N' and MIG_Foszam.Id in (
					select MIG_JegyzekTetelek.MIG_Foszam_Id
					from MIG_JegyzekTetelek
                    where (' + @ExtendedMIG_JegyzekTetelekWhere + ')) ';
	END

	open cur;
	fetch next from cur into @whereFilter;
	while @@FETCH_STATUS = 0
	BEGIN
		IF @whereFilter = 'ExtendedMIG_AlszamWhere'
			set @sqlcmd = @sqlcmd + ' delete from #filter where Id not in
(select MIG_Foszam.Id
    from MIG_Foszam
	inner join MIG_Alszam on MIG_Foszam.Id = MIG_Alszam.MIG_Foszam_Id and (' + @ExtendedMIG_AlszamWhere + ')) ';
	
		IF @whereFilter = 'ExtendedMIG_EloadoWhere'
        BEGIN
			set @sqlcmd = @sqlcmd + ' delete from #filter where Id not in
(select MIG_Foszam.Id
    from MIG_Foszam
    inner join MIG_Alszam on MIG_Foszam.Id = MIG_Alszam.MIG_Foszam_Id
    inner join MIG_Eloado on MIG_Eloado.Id = MIG_Alszam.MIG_Eloado_Id and (' + @ExtendedMIG_EloadoWhere + ') ';
            IF @ExtendedMIG_AlszamWhere is not null and @ExtendedMIG_AlszamWhere != ''
	        BEGIN
                SET @sqlcmd = @sqlcmd + N' and ' + @ExtendedMIG_AlszamWhere
            END
	        SET @sqlcmd = @sqlcmd + ') ';
        END
	
		fetch next from cur into @whereFilter;
	END
	close cur;
	deallocate cur;


	/************************************************************
	* Tényleges select											*
	************************************************************/
  SET @sqlcmd = @sqlcmd + N'
  select Id from #filter'
    
    execute sp_executesql @sqlcmd


END TRY


BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER() < 50000
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1
		RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH

end