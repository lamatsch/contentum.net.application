﻿
CREATE procedure [dbo].[sp_MIG_FoszamGetSzuloHiearchy]
		@Id	uniqueidentifier,
		@ExecutorUserId	UNIQUEIDENTIFIER = NULL

as

begin

BEGIN TRY

    set nocount on;
   
	WITH FoszamHierarchy  AS
	(
	   -- Base case
	   SELECT *,
			  0 as HierarchyLevel
	   FROM MIG_Foszam
	   WHERE Id = @Id

	   UNION ALL

	   -- Recursive step
	   SELECT f.*,
		  fh.HierarchyLevel + 1 AS HierarchyLevel
	   FROM MIG_Foszam as f
		  INNER JOIN  FoszamHierarchy as fh ON
			 f.Id = fh.Csatolva_Id
	)
	
	SELECT * INTO #temp
	FROM FoszamHierarchy
	where FoszamHierarchy.MIG_Sav_Id is not null
	
	-- BUG_10622 (BUG_8097 javítás)
	SELECT --#temp.* ,
	#temp.[Id], 
	#temp.UI_SAV as OLD_UI_SAV,
	#temp.EdokSav as UI_SAV,
	#temp.[UI_YEAR], #temp.[UI_NUM], #temp.[UI_NAME], #temp.[UI_IRSZ], #temp.[UI_UTCA], 
	#temp.[UI_HSZ], #temp.[UI_HRSZ], #temp.[UI_TYPE], #temp.[UI_IRJ], #temp.[UI_PERS], 
	#temp.[UI_OT_ID], #temp.[MEMO], #temp.[IRSZ_PLUSS], #temp.[IRJ2000], #temp.[Conc], 
	#temp.[Selejtezve], #temp.[Selejtezes_Datuma], #temp.[Csatolva_Rendszer], #temp.[Csatolva_Id], 
	#temp.[MIG_Sav_Id], #temp.[MIG_Varos_Id], #temp.[MIG_IktatasTipus_Id], #temp.[MIG_Eloado_Id], 
	#temp.[Edok_Utoirat_Id], #temp.[Edok_Utoirat_Azon], #temp.[UGYHOL], #temp.[IRATTARBA], 
	#temp.[SCONTRO], #temp.[Edok_Ugyintezo_Csoport_Id], #temp.[Edok_Ugyintezo_Nev], #temp.[Ver], 
	#temp.[EdokSav], #temp.[MIG_Forras_id], #temp.[Ugyirat_tipus], #temp.[Eltelt_napok], 
	#temp.[Ugy_kezdete], #temp.[Ugykezeles_modja], #temp.[Egyeb_adat2], #temp.[Munkanapos], 
	#temp.[Hatarido], #temp.[Feladat], #temp.[Targyszavak], #temp.[Kulso_eloirat], #temp.[Felfuggesztve], 
	#temp.[Szervezet], #temp.[Sztorno_datum], #temp.[IrattarbolKikeroNev], -- BUG_12668 #temp.[szervezet2], 
	#temp.[IrattarbolKikeres_Datuma], #temp.[MegorzesiIdo], #temp.[IrattarId], #temp.[IrattariHely],
	
	dbo.fn_MergeFoszam(#temp.EdokSav, #temp.UI_NUM, #temp.UI_YEAR) as Foszam_Merge,
	dbo.fn_GetIrattariTetelszam(#temp.UI_YEAR, #temp.UI_TYPE) as IrattariTetelszam,
--	dbo.fn_GetMegkulJelzes(MIG_Sav.NAME) as MegkulJelzes,
	#temp.EdokSav as MegkulJelzes,
--    mi.ALNO, mi.UGYHOL,
--    convert(varchar, mi.SCONTRO, 102) as SCONTRO,
--    convert(varchar, mi.IRATTARBA, 102) as IRATTARBA
    convert(varchar, #temp.SCONTRO, 102) as SCONTRO,
    convert(varchar, #temp.IRATTARBA, 102) as IRATTARBA,
    mi.ALNO
	FROM #temp
--	inner join MIG_Sav on MIG_Sav.Id = #temp.MIG_Sav_Id
    left join MIG_Alszam AS mi
    on #temp.Id = mi.MIG_Foszam_Id
	WHERE mi.ALNO = (SELECT max(ALNO) FROM MIG_Alszam WHERE MIG_Foszam_Id = mi.MIG_Foszam_Id)
	ORDER BY #temp.HierarchyLevel ASC	

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end