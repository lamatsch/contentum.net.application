﻿create procedure [dbo].[sp_MIG_FoszamHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from MIG_FoszamHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_SAV' as ColumnName,               cast(Old.UI_SAV as nvarchar(99)) as OldValue,
               cast(New.UI_SAV as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UI_SAV != New.UI_SAV 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_YEAR' as ColumnName,               cast(Old.UI_YEAR as nvarchar(99)) as OldValue,
               cast(New.UI_YEAR as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UI_YEAR != New.UI_YEAR 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_NUM' as ColumnName,               cast(Old.UI_NUM as nvarchar(99)) as OldValue,
               cast(New.UI_NUM as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UI_NUM != New.UI_NUM 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_NAME' as ColumnName,               cast(Old.UI_NAME as nvarchar(99)) as OldValue,
               cast(New.UI_NAME as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UI_NAME != New.UI_NAME 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_IRSZ' as ColumnName,               cast(Old.UI_IRSZ as nvarchar(99)) as OldValue,
               cast(New.UI_IRSZ as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UI_IRSZ != New.UI_IRSZ 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_UTCA' as ColumnName,               cast(Old.UI_UTCA as nvarchar(99)) as OldValue,
               cast(New.UI_UTCA as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UI_UTCA != New.UI_UTCA 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_HSZ' as ColumnName,               cast(Old.UI_HSZ as nvarchar(99)) as OldValue,
               cast(New.UI_HSZ as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UI_HSZ != New.UI_HSZ 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_HRSZ' as ColumnName,               cast(Old.UI_HRSZ as nvarchar(99)) as OldValue,
               cast(New.UI_HRSZ as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UI_HRSZ != New.UI_HRSZ 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_TYPE' as ColumnName,               cast(Old.UI_TYPE as nvarchar(99)) as OldValue,
               cast(New.UI_TYPE as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UI_TYPE != New.UI_TYPE 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_IRJ' as ColumnName,               cast(Old.UI_IRJ as nvarchar(99)) as OldValue,
               cast(New.UI_IRJ as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UI_IRJ != New.UI_IRJ 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_PERS' as ColumnName,               cast(Old.UI_PERS as nvarchar(99)) as OldValue,
               cast(New.UI_PERS as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UI_PERS != New.UI_PERS 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_OT_ID' as ColumnName,               cast(Old.UI_OT_ID as nvarchar(99)) as OldValue,
               cast(New.UI_OT_ID as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UI_OT_ID != New.UI_OT_ID 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MEMO' as ColumnName,               cast(Old.MEMO as nvarchar(99)) as OldValue,
               cast(New.MEMO as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MEMO != New.MEMO 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IRSZ_PLUSS' as ColumnName,               cast(Old.IRSZ_PLUSS as nvarchar(99)) as OldValue,
               cast(New.IRSZ_PLUSS as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IRSZ_PLUSS != New.IRSZ_PLUSS 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IRJ2000' as ColumnName,               cast(Old.IRJ2000 as nvarchar(99)) as OldValue,
               cast(New.IRJ2000 as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IRJ2000 != New.IRJ2000 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Conc' as ColumnName,               cast(Old.Conc as nvarchar(99)) as OldValue,
               cast(New.Conc as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Conc != New.Conc 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Selejtezve' as ColumnName,               cast(Old.Selejtezve as nvarchar(99)) as OldValue,
               cast(New.Selejtezve as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Selejtezve != New.Selejtezve 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Selejtezes_Datuma' as ColumnName,               cast(Old.Selejtezes_Datuma as nvarchar(99)) as OldValue,
               cast(New.Selejtezes_Datuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Selejtezes_Datuma != New.Selejtezes_Datuma 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csatolva_Rendszer' as ColumnName,               cast(Old.Csatolva_Rendszer as nvarchar(99)) as OldValue,
               cast(New.Csatolva_Rendszer as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csatolva_Rendszer != New.Csatolva_Rendszer 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csatolva_Id' as ColumnName,               cast(Old.Csatolva_Id as nvarchar(99)) as OldValue,
               cast(New.Csatolva_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Csatolva_Id != New.Csatolva_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MIG_Sav_Id' as ColumnName,               cast(Old.MIG_Sav_Id as nvarchar(99)) as OldValue,
               cast(New.MIG_Sav_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MIG_Sav_Id != New.MIG_Sav_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MIG_Varos_Id' as ColumnName,               cast(Old.MIG_Varos_Id as nvarchar(99)) as OldValue,
               cast(New.MIG_Varos_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MIG_Varos_Id != New.MIG_Varos_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MIG_IktatasTipus_Id' as ColumnName,               cast(Old.MIG_IktatasTipus_Id as nvarchar(99)) as OldValue,
               cast(New.MIG_IktatasTipus_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MIG_IktatasTipus_Id != New.MIG_IktatasTipus_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MIG_Eloado_Id' as ColumnName,               cast(Old.MIG_Eloado_Id as nvarchar(99)) as OldValue,
               cast(New.MIG_Eloado_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.MIG_Eloado_Id != New.MIG_Eloado_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Edok_Utoirat_Id' as ColumnName,               cast(Old.Edok_Utoirat_Id as nvarchar(99)) as OldValue,
               cast(New.Edok_Utoirat_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Edok_Utoirat_Id != New.Edok_Utoirat_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Edok_Utoirat_Azon' as ColumnName,               cast(Old.Edok_Utoirat_Azon as nvarchar(99)) as OldValue,
               cast(New.Edok_Utoirat_Azon as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Edok_Utoirat_Azon != New.Edok_Utoirat_Azon 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UGYHOL' as ColumnName,               cast(Old.UGYHOL as nvarchar(99)) as OldValue,
               cast(New.UGYHOL as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.UGYHOL != New.UGYHOL 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IRATTARBA' as ColumnName,               cast(Old.IRATTARBA as nvarchar(99)) as OldValue,
               cast(New.IRATTARBA as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IRATTARBA != New.IRATTARBA 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SCONTRO' as ColumnName,               cast(Old.SCONTRO as nvarchar(99)) as OldValue,
               cast(New.SCONTRO as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.SCONTRO != New.SCONTRO 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Edok_Ugyintezo_Csoport_Id' as ColumnName,               cast(Old.Edok_Ugyintezo_Csoport_Id as nvarchar(99)) as OldValue,
               cast(New.Edok_Ugyintezo_Csoport_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Edok_Ugyintezo_Csoport_Id != New.Edok_Ugyintezo_Csoport_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Edok_Ugyintezo_Nev' as ColumnName,               cast(Old.Edok_Ugyintezo_Nev as nvarchar(99)) as OldValue,
               cast(New.Edok_Ugyintezo_Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.Edok_Ugyintezo_Nev != New.Edok_Ugyintezo_Nev 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
  union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarId' as ColumnName,              
			   cast(Old.IrattarId as nvarchar(99)) as OldValue,
               cast(New.IrattarId as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and Old.IrattarId != New.IrattarId 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id  
		 )
		 union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattariHely' as ColumnName,               cast(Old.IrattariHely as nvarchar(99)) as OldValue,
               cast(New.IrattariHely as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_FoszamHistory Old
         inner join MIG_FoszamHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IrattariHely as nvarchar(max)),'') != ISNULL(CAST(New.IrattariHely as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)

end