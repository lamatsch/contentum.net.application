﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1 from  sysobjects
           where  id = object_id('sp_MIG_FoszamHistoryGetRecord')
             and  type in ('P'))
   drop procedure sp_MIG_FoszamHistoryGetRecord
go

create procedure [dbo].[sp_MIG_FoszamHistoryGetRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

   SELECT * INTO #cache from MIG_FoszamHistory WHERE Id = @RecordId
   
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, dbo.fn_GetMIG_EloadoAzonosito(HistoryVegrehajto_Id) as Executor, HistoryVegrehajto_Id AS ExecutorId, HistoryVegrehajtasIdo as ExecutionTime
      from #cache 
      where HistoryMuvelet_Id = 0       
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_SAV' as ColumnName,               
               cast(Old.UI_SAV as nvarchar(99)) as OldValue,
               cast(New.UI_SAV as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.UI_SAV != New.UI_SAV
            or (Old.UI_SAV IS NULL and New.UI_SAV IS NOT NULL)
            or (Old.UI_SAV IS NOT NULL and New.UI_SAV IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_YEAR' as ColumnName,               
               cast(Old.UI_YEAR as nvarchar(99)) as OldValue,
               cast(New.UI_YEAR as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.UI_YEAR != New.UI_YEAR
            or (Old.UI_YEAR IS NULL and New.UI_YEAR IS NOT NULL)
            or (Old.UI_YEAR IS NOT NULL and New.UI_YEAR IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_NUM' as ColumnName,               
               cast(Old.UI_NUM as nvarchar(99)) as OldValue,
               cast(New.UI_NUM as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.UI_NUM != New.UI_NUM
            or (Old.UI_NUM IS NULL and New.UI_NUM IS NOT NULL)
            or (Old.UI_NUM IS NOT NULL and New.UI_NUM IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_NAME' as ColumnName,               
               cast(Old.UI_NAME as nvarchar(99)) as OldValue,
               cast(New.UI_NAME as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.UI_NAME != New.UI_NAME
            or (Old.UI_NAME IS NULL and New.UI_NAME IS NOT NULL)
            or (Old.UI_NAME IS NOT NULL and New.UI_NAME IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_IRSZ' as ColumnName,               
               cast(Old.UI_IRSZ as nvarchar(99)) as OldValue,
               cast(New.UI_IRSZ as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.UI_IRSZ != New.UI_IRSZ
            or (Old.UI_IRSZ IS NULL and New.UI_IRSZ IS NOT NULL)
            or (Old.UI_IRSZ IS NOT NULL and New.UI_IRSZ IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_UTCA' as ColumnName,               
               cast(Old.UI_UTCA as nvarchar(99)) as OldValue,
               cast(New.UI_UTCA as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.UI_UTCA != New.UI_UTCA
            or (Old.UI_UTCA IS NULL and New.UI_UTCA IS NOT NULL)
            or (Old.UI_UTCA IS NOT NULL and New.UI_UTCA IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_HSZ' as ColumnName,               
               cast(Old.UI_HSZ as nvarchar(99)) as OldValue,
               cast(New.UI_HSZ as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.UI_HSZ != New.UI_HSZ
            or (Old.UI_HSZ IS NULL and New.UI_HSZ IS NOT NULL)
            or (Old.UI_HSZ IS NOT NULL and New.UI_HSZ IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_HRSZ' as ColumnName,               
               cast(Old.UI_HRSZ as nvarchar(99)) as OldValue,
               cast(New.UI_HRSZ as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.UI_HRSZ != New.UI_HRSZ
            or (Old.UI_HRSZ IS NULL and New.UI_HRSZ IS NOT NULL)
            or (Old.UI_HRSZ IS NOT NULL and New.UI_HRSZ IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_TYPE' as ColumnName,               
               cast(Old.UI_TYPE as nvarchar(99)) as OldValue,
               cast(New.UI_TYPE as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.UI_TYPE != New.UI_TYPE
            or (Old.UI_TYPE IS NULL and New.UI_TYPE IS NOT NULL)
            or (Old.UI_TYPE IS NOT NULL and New.UI_TYPE IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_IRJ' as ColumnName,               
               cast(Old.UI_IRJ as nvarchar(99)) as OldValue,
               cast(New.UI_IRJ as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.UI_IRJ != New.UI_IRJ
            or (Old.UI_IRJ IS NULL and New.UI_IRJ IS NOT NULL)
            or (Old.UI_IRJ IS NOT NULL and New.UI_IRJ IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_PERS' as ColumnName,               
               cast(Old.UI_PERS as nvarchar(99)) as OldValue,
               cast(New.UI_PERS as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.UI_PERS != New.UI_PERS
            or (Old.UI_PERS IS NULL and New.UI_PERS IS NOT NULL)
            or (Old.UI_PERS IS NOT NULL and New.UI_PERS IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UI_OT_ID' as ColumnName,               
               cast(Old.UI_OT_ID as nvarchar(99)) as OldValue,
               cast(New.UI_OT_ID as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.UI_OT_ID != New.UI_OT_ID
            or (Old.UI_OT_ID IS NULL and New.UI_OT_ID IS NOT NULL)
            or (Old.UI_OT_ID IS NOT NULL and New.UI_OT_ID IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MEMO' as ColumnName,               
               cast(Old.MEMO as nvarchar(99)) as OldValue,
               cast(New.MEMO as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.MEMO != New.MEMO
            or (Old.MEMO IS NULL and New.MEMO IS NOT NULL)
            or (Old.MEMO IS NOT NULL and New.MEMO IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IRSZ_PLUSS' as ColumnName,               
               cast(Old.IRSZ_PLUSS as nvarchar(99)) as OldValue,
               cast(New.IRSZ_PLUSS as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.IRSZ_PLUSS != New.IRSZ_PLUSS
            or (Old.IRSZ_PLUSS IS NULL and New.IRSZ_PLUSS IS NOT NULL)
            or (Old.IRSZ_PLUSS IS NOT NULL and New.IRSZ_PLUSS IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IRJ2000' as ColumnName,               
               cast(Old.IRJ2000 as nvarchar(99)) as OldValue,
               cast(New.IRJ2000 as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.IRJ2000 != New.IRJ2000
            or (Old.IRJ2000 IS NULL and New.IRJ2000 IS NOT NULL)
            or (Old.IRJ2000 IS NOT NULL and New.IRJ2000 IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Conc' as ColumnName,               
               cast(Old.Conc as nvarchar(99)) as OldValue,
               cast(New.Conc as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.Conc != New.Conc
            or (Old.Conc IS NULL and New.Conc IS NOT NULL)
            or (Old.Conc IS NOT NULL and New.Conc IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Selejtezve' as ColumnName,               
               cast(Old.Selejtezve as nvarchar(99)) as OldValue,
               cast(New.Selejtezve as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.Selejtezve != New.Selejtezve
            or (Old.Selejtezve IS NULL and New.Selejtezve IS NOT NULL)
            or (Old.Selejtezve IS NOT NULL and New.Selejtezve IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Selejtezes_Datuma' as ColumnName,               
               convert(NVARCHAR,Old.Selejtezes_Datuma,120) as OldValue,
               convert(NVARCHAR,New.Selejtezes_Datuma,120) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.Selejtezes_Datuma != New.Selejtezes_Datuma
            or (Old.Selejtezes_Datuma IS NULL and New.Selejtezes_Datuma IS NOT NULL)
            or (Old.Selejtezes_Datuma IS NOT NULL and New.Selejtezes_Datuma IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csatolva_Rendszer' as ColumnName,               
               cast(Old.Csatolva_Rendszer as nvarchar(99)) as OldValue,
               cast(New.Csatolva_Rendszer as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.Csatolva_Rendszer != New.Csatolva_Rendszer
            or (Old.Csatolva_Rendszer IS NULL and New.Csatolva_Rendszer IS NOT NULL)
            or (Old.Csatolva_Rendszer IS NOT NULL and New.Csatolva_Rendszer IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csatolva_Id' as ColumnName,               
               cast(Old.Csatolva_Id as nvarchar(99)) as OldValue,
               cast(New.Csatolva_Id as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.Csatolva_Id != New.Csatolva_Id
            or (Old.Csatolva_Id IS NULL and New.Csatolva_Id IS NOT NULL)
            or (Old.Csatolva_Id IS NOT NULL and New.Csatolva_Id IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MIG_Sav_Id' as ColumnName,               
               cast(Old.MIG_Sav_Id as nvarchar(99)) as OldValue,
               cast(New.MIG_Sav_Id as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.MIG_Sav_Id != New.MIG_Sav_Id
            or (Old.MIG_Sav_Id IS NULL and New.MIG_Sav_Id IS NOT NULL)
            or (Old.MIG_Sav_Id IS NOT NULL and New.MIG_Sav_Id IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MIG_Varos_Id' as ColumnName,               
               cast(Old.MIG_Varos_Id as nvarchar(99)) as OldValue,
               cast(New.MIG_Varos_Id as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.MIG_Varos_Id != New.MIG_Varos_Id
            or (Old.MIG_Varos_Id IS NULL and New.MIG_Varos_Id IS NOT NULL)
            or (Old.MIG_Varos_Id IS NOT NULL and New.MIG_Varos_Id IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MIG_IktatasTipus_Id' as ColumnName,               
               cast(Old.MIG_IktatasTipus_Id as nvarchar(99)) as OldValue,
               cast(New.MIG_IktatasTipus_Id as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.MIG_IktatasTipus_Id != New.MIG_IktatasTipus_Id
            or (Old.MIG_IktatasTipus_Id IS NULL and New.MIG_IktatasTipus_Id IS NOT NULL)
            or (Old.MIG_IktatasTipus_Id IS NOT NULL and New.MIG_IktatasTipus_Id IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MIG_Eloado_Id' as ColumnName,               
               dbo.fn_GetMIG_EloadoAzonosito(Old.MIG_Eloado_Id) as OldValue,
               dbo.fn_GetMIG_EloadoAzonosito(New.MIG_Eloado_Id) as NewValue, '' as Executor,              
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.MIG_Eloado_Id != New.MIG_Eloado_Id
            or (Old.MIG_Eloado_Id IS NULL and New.MIG_Eloado_Id IS NOT NULL)
            or (Old.MIG_Eloado_Id IS NOT NULL and New.MIG_Eloado_Id IS NULL))
            and Old.Id = New.Id      
      )      
--      union all 
--      (select  New.HistoryId as RowId, 
--               New.Ver as Ver, 
--               case New.HistoryMuvelet_Id 
--                  when 1 then 'Módosítás'
--                  when 2 then 'Érvénytelenítés'
--               end as Operation, 
--               'Edok_Utoirat_Id' as ColumnName,               
--               cast(Old.Edok_Utoirat_Id as nvarchar(99)) as OldValue,
--               cast(New.Edok_Utoirat_Id as nvarchar(99)) as NewValue, '' as Executor,               
--               CAST(New.HistoryVegrehajto_Id as NVARCHAR) as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
--      from #cache Old
--         inner join #cache New on Old.Ver = New.Ver-1
--            and Old.Id = New.Id
--            and (Old.Edok_Utoirat_Id != New.Edok_Utoirat_Id
--            or (Old.Edok_Utoirat_Id IS NULL and New.Edok_Utoirat_Id IS NOT NULL)
--            or (Old.Edok_Utoirat_Id IS NOT NULL and New.Edok_Utoirat_Id IS NULL))
--            and Old.Id = New.Id      
--      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Edok_Utoirat_Azon' as ColumnName,               
               cast(Old.Edok_Utoirat_Azon as nvarchar(99)) as OldValue,
               cast(New.Edok_Utoirat_Azon as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.Edok_Utoirat_Azon != New.Edok_Utoirat_Azon
            or (Old.Edok_Utoirat_Azon IS NULL and New.Edok_Utoirat_Azon IS NOT NULL)
            or (Old.Edok_Utoirat_Azon IS NOT NULL and New.Edok_Utoirat_Azon IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UGYHOL' as ColumnName,               
               cast(Old.UGYHOL as nvarchar(99)) as OldValue,
               cast(New.UGYHOL as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.UGYHOL != New.UGYHOL
            or (Old.UGYHOL IS NULL and New.UGYHOL IS NOT NULL)
            or (Old.UGYHOL IS NOT NULL and New.UGYHOL IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IRATTARBA' as ColumnName,               
               convert(NVARCHAR,Old.IRATTARBA, 120) as OldValue,
               convert(NVARCHAR,New.IRATTARBA, 120) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.IRATTARBA != New.IRATTARBA
            or (Old.IRATTARBA IS NULL and New.IRATTARBA IS NOT NULL)
            or (Old.IRATTARBA IS NOT NULL and New.IRATTARBA IS NULL))
            and Old.Id = New.Id      
      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SCONTRO' as ColumnName,               
               convert(NVARCHAR,Old.SCONTRO,120) as OldValue,
               convert(NVARCHAR,New.SCONTRO,120) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.SCONTRO != New.SCONTRO
            or (Old.SCONTRO IS NULL and New.SCONTRO IS NOT NULL)
            or (Old.SCONTRO IS NOT NULL and New.SCONTRO IS NULL))
            and Old.Id = New.Id      
      )      
--      union all 
--      (select  New.HistoryId as RowId, 
--               New.Ver as Ver, 
--               case New.HistoryMuvelet_Id 
--                  when 1 then 'Módosítás'
--                  when 2 then 'Érvénytelenítés'
--               end as Operation, 
--               'Edok_Ugyintezo_Csoport_Id' as ColumnName,               
--               cast(Old.Edok_Ugyintezo_Csoport_Id as nvarchar(99)) as OldValue,
--               cast(New.Edok_Ugyintezo_Csoport_Id as nvarchar(99)) as NewValue, '' as Executor,               
--               CAST(New.HistoryVegrehajto_Id as NVARCHAR) as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
--      from #cache Old
--         inner join #cache New on Old.Ver = New.Ver-1
--            and Old.Id = New.Id
--            and (Old.Edok_Ugyintezo_Csoport_Id != New.Edok_Ugyintezo_Csoport_Id
--            or (Old.Edok_Ugyintezo_Csoport_Id IS NULL and New.Edok_Ugyintezo_Csoport_Id IS NOT NULL)
--            or (Old.Edok_Ugyintezo_Csoport_Id IS NOT NULL and New.Edok_Ugyintezo_Csoport_Id IS NULL))
--            and Old.Id = New.Id      
--      )      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Edok_Ugyintezo_Nev' as ColumnName,               
               cast(Old.Edok_Ugyintezo_Nev as nvarchar(99)) as OldValue,
               cast(New.Edok_Ugyintezo_Nev as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.Edok_Ugyintezo_Nev != New.Edok_Ugyintezo_Nev
            or (Old.Edok_Ugyintezo_Nev IS NULL and New.Edok_Ugyintezo_Nev IS NOT NULL)
            or (Old.Edok_Ugyintezo_Nev IS NOT NULL and New.Edok_Ugyintezo_Nev IS NULL))
            and Old.Id = New.Id      
      )
	   union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarId' as ColumnName,               
               cast(Old.IrattarId as nvarchar(99)) as OldValue,
               cast(New.IrattarId as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.IrattarId != New.IrattarId
            or (Old.IrattarId IS NULL and New.IrattarId IS NOT NULL)
            or (Old.IrattarId IS NOT NULL and New.IrattarId IS NULL))
            and Old.Id = New.Id      
      )
	 union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattariHely' as ColumnName,               
               cast(Old.IrattariHely as nvarchar(99)) as OldValue,
               cast(New.IrattariHely as nvarchar(99)) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.IrattariHely != New.IrattariHely
            or (Old.IrattariHely IS NULL and New.IrattariHely IS NOT NULL)
            or (Old.IrattariHely IS NOT NULL and New.IrattariHely IS NULL))
            and Old.Id = New.Id      
      )
	  union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MegorzesiIdo' as ColumnName,               
               convert(NVARCHAR,Old.MegorzesiIdo, 120) as OldValue,
               convert(NVARCHAR,New.MegorzesiIdo, 120) as NewValue, '' as Executor,               
               New.HistoryVegrehajto_Id as ExecutorId, New.HistoryVegrehajtasIdo as ExecutionTime
      from #cache Old
         inner join #cache New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and (Old.MegorzesiIdo != New.MegorzesiIdo
            or (Old.MegorzesiIdo IS NULL and New.MegorzesiIdo IS NOT NULL)
            or (Old.MegorzesiIdo IS NOT NULL and New.MegorzesiIdo IS NULL))
            and Old.Id = New.Id      
      )
      ORDER BY ExecutionTime
end