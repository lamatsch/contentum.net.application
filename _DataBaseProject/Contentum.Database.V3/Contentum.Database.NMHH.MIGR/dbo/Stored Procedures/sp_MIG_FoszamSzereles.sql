﻿
create procedure sp_MIG_FoszamSzereles
             @Sav nvarchar(20),
             @Year int,
             @Num int,					
             @Edok_Utoirat_Id UNIQUEIDENTIFIER = NULL,
             @Edok_Utoirat_Azon NVARCHAR(100) = NULL,
             @Visszavonas INT = 0,
             @ExecutorUserId uniqueidentifier,
			 @ExecutionTime DATETIME
as

/*
--              HibaĂĽzenetek helyett hibakĂłdok, az alkalmazĂˇs oldja fel
--                  [56001] 'Az ĂĽgyirat (alszĂˇm) megadĂˇsa kĂ¶telezĹ‘!'
--                  [56002] 'MIG_Alszam select hiba!'
--                  [56003] 'A megadott ĂĽgyirat nem lĂ©tezik!'
--                  [56004] 'Az ĂĽgyirat Ăşj helyĂ©nek megadĂˇsa kĂ¶telezĹ‘!'
--                  [56005] 'Az ĂĽgyirat Ăşj helyĂ©nek megadĂˇsa hibĂˇs!'
--                  [56006] 'Az ĂĽgyirat mĂˇr eleve a kĂ©rt helyen van!'
--                  [56007] 'IrattĂˇrbĂłl skontrĂłba valĂł ĂˇthelyezĂ©s nem engedĂ©lyezett!'
--                  [56008] 'SkontrĂłba helyezĂ©snĂ©l kĂ¶telezĹ‘ a skontrĂł vĂ©gĂ©nek megadĂˇsa!'
--                  [56009] 'A skontrĂł vĂ©gĂ©nek az aktuĂˇlis napnĂˇl kĂ©sĹ‘bbinek kell lennie!'
--                  [56010] 'MIG_Alszam update hiba!'
--                  [56011] 'Az ĂĽgyirat kĂ¶zben mĂłdosult!'
--                  [56012] 'UtĂłirat id-jĂˇnak megadĂˇsa kĂ¶telezĹ‘!'
--                  [56013] 'UtĂłirat azonosĂ­tĂłjĂˇnak megadĂˇsa kĂ¶telezĹ‘!'
--                  [56014] 'MIG_Foszam update hiba!'
--                  [56015] 'MIG_Foszam select hiba!'
--                  [56016] 'A megadott elĹ‘zmĂ©ny ĂĽgyirat nem lĂ©tezik!'
--                  [56017] 'A megadott elĹ‘zmĂ©ny ĂĽgyirat mĂˇr szerelve van!'
--
--                  [56301] 'Az ĂĽgyirat (fĹ‘szĂˇm) megadĂˇsa kĂ¶telezĹ‘!'

-- pĂ©lda:  -----
begin tran 

select Edok_Utoirat_Id,Edok_Utoirat_Azon  -- elĹ‘tte
  from MIG_Foszam
 where ui_sav=02 AND ui_year=1997 AND ui_num=210

exec [dbo].[sp_MIG_FoszamSzereles]  02,1997,210,'1D8FE7E7-3767-4121-951E-DCC922BB6F66','FPH01/610/2008/01'

select Edok_Utoirat_Id,Edok_Utoirat_Azon  -- utĂˇnna
from MIG_Foszam
where ui_sav=02 AND ui_year=1997 AND ui_num=210

rollback tran 
GO

*/
/*
MĂłdosĂ­totta:
-- 2008.07.14. Boda Eszter:
   A MIG_Alszam tĂˇblĂˇbĂłl az UGYHOL (valamint a SCONTRO, IRATTARBA) mezĹ‘k ĂˇtkerĂĽltek
   a MIG_Foszam tĂˇblĂˇba
   Ha a szerelendĹ‘ (rĂ©gi) ĂĽgyirat nincs osztĂˇlyon, Ă©s nem visszavonĂˇs tĂ¶rtĂ©nik,
   automatikusan kivesszĂĽk teljes hierarchiĂˇjĂˇval egyĂĽtt,
   mivel mĂˇskĂ©nt nem lehetne egyĂĽtt mozgatni a szĂĽlĹ‘ EDOK-os ĂĽgyirattal
       -> sp_MIG_FoszamAthelyezes hĂ­vĂˇsa
-- 2009.05.14. Boda Eszter:
   Ha a szerelendĹ‘ (rĂ©gi) ĂĽgyirat Ăˇllapota IrattĂˇrbĂłl elkĂ©rt ('I') vagy SkontrĂłbĂłl elkĂ©rt ('E'),
   nem helyezzĂĽk Ăˇt automatikusan, hanem aktuĂˇlis ĂˇllapotĂˇban megtartjuk
*/

---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 0
 
declare @sajat_tranz     int  -- sajĂˇt tranzakciĂł kezelĂ©s? (0 = nem, 1 = igen)
select  @sajat_tranz   = (CASE When @@trancount > 0 Then 0 Else 1 END)

if @sajat_tranz = 1  
   BEGIN TRANSACTION 
   
   
--
DECLARE @Id UNIQUEIDENTIFIER
DECLARE @Act_Edok_Utoirat_Id UNIQUEIDENTIFIER
DECLARE @Act_Edok_Utoirat_Azon NVARCHAR(100)

DECLARE @Act_Ugyhol char(1)
DECLARE @Act_Ver int
--
declare @error           int
declare @rowcount        int

--------------------------------
-- input paramĂ©terek ellenĹ‘rzĂ©se
--------------------------------
-- input paramĂ©terek kiiratĂˇsa
if @teszt = 1
   print '@Sav = '+ isnull(convert(nvarchar(50), @Sav), 'NULL') + ', ' +
         '@Year = '+ isnull(convert(nvarchar(50),@Year), 'NULL') + ', ' +
         '@Num = '+ isnull(convert(nvarchar(50),@Num), 'NULL') + ', ' +
         '@Edok_Utoirat_Id  = '+ isnull(convert(varchar(50),@Edok_Utoirat_Id) , 'NULL') + ', ' +
         '@Edok_Utoirat_Azon = '+ isnull(@Edok_Utoirat_Azon, 'NULL') + ', ' +
         '@Visszavonas = ' + isnull(convert(nvarchar(50),@Visszavonas), 'NULL')

--
if @Sav is NULL
   --RAISERROR('Az ĂĽgyirat (fĹ‘szĂˇm) megadĂˇsa kĂ¶telezĹ‘!',16,1)
   RAISERROR('[56301]',16,1)
   
if @Year is NULL
   --RAISERROR('Az ĂĽgyirat (fĹ‘szĂˇm) megadĂˇsa kĂ¶telezĹ‘!',16,1)
   RAISERROR('[56301]',16,1)
   
if @Num is NULL
   --RAISERROR('Az ĂĽgyirat (fĹ‘szĂˇm) megadĂˇsa kĂ¶telezĹ‘!',16,1)
   RAISERROR('[56301]',16,1)

IF @Visszavonas = 1
BEGIN
	SET @Edok_Utoirat_Id = NULL
	SET @Edok_Utoirat_Azon = NULL
END
ELSE
BEGIN
	if @Edok_Utoirat_Id is NULL
	   --RAISERROR('UtĂłirat id-jĂˇnak megadĂˇsa kĂ¶telezĹ‘!,16,1)
	   RAISERROR('[56012]',16,1)
	   
	if @Edok_Utoirat_Azon is NULL
	   --RAISERROR('UtĂłirat azonosĂ­tĂłjĂˇnak megadĂˇsa kĂ¶telezĹ‘!,16,1)
	   RAISERROR('[56013]',16,1)
END   
   
-- a rĂ©gi dokumentum olvasĂˇsa
select @Id = MIG_Foszam.Id,
       @Act_Edok_Utoirat_Id  = MIG_Foszam.Edok_Utoirat_Id,
       @Act_Edok_Utoirat_Azon  = MIG_Foszam.Edok_Utoirat_Azon,
       @Act_Ugyhol = MIG_foszam.UGYHOL,
       @Act_Ver = Ver
  from MIG_Foszam
--  inner join MIG_Sav on MIG_Sav.Id = MIG_Foszam.MIG_Sav_Id
 where  
--	 SUBSTRING(MIG_Sav.NAME,2, CHARINDEX(')', MIG_Sav.NAME) - 2 ) = @Sav
-- AND CHARINDEX('(', MIG_Sav.NAME) = 1 AND CHARINDEX(')', MIG_Sav.NAME) > 2
	EdokSav = @Sav
 AND UI_YEAR = @Year
 AND UI_NUM = @Num

select  @error = @@error, @rowcount = @@rowcount

if @error <> 0
   --RAISERROR('MIG_Foszam select hiba!',16,1)
   RAISERROR('[56015]',16,1)

if @rowcount = 0
   --RAISERROR('A megadott elĹ‘zmĂ©ny ĂĽgyirat nem lĂ©tezik!',16,1)
   RAISERROR('[56016]',16,1) 

IF @Visszavonas != 1
BEGIN   
	IF @Act_Edok_Utoirat_Id IS NOT NULL
	-- RAISERROR('A megadott elĹ‘zmĂ©ny ĂĽgyirat mĂˇr szerelve van!',16,1)
	   RAISERROR('[56017]',16,1)             
END

-- Ha a szerelendo ugyirat nincs osztĂˇlyon, kivesszuk, hierarchiajaval egyutt
-- kivĂ©ve, ha IrattĹ±rbĂłl elĂ©rt ('I') vagy SkontrĂłbĂłl elkĂ©rt ('E')
IF @Act_Ugyhol not in ('I','E')
BEGIN
	IF @Visszavonas != 1 and @Act_Ugyhol != '3'
	BEGIN
		IF @Act_Ugyhol = '2' --IrattĂˇrban
		BEGIN
			EXEC sp_MIG_FoszamAthelyezes
					 @Id = @Id,
					 @Ugyhova = 'I', -- IrattĂˇrbĂłl elkĂ©rt
					 @MoveHierarchy = '1',
					 @ExecutorUserId = @ExecutorUserId,
					 @ExecutionTime = @ExecutionTime 			
		END
		ELSE IF @Act_Ugyhol = '1' --SkontrĂłban
		BEGIN
			EXEC sp_MIG_FoszamAthelyezes
					 @Id = @Id,
					 @Ugyhova = 'E', -- SkontrĂłbĂłl elkĂ©rt
					 @MoveHierarchy = '1',
					 @ExecutorUserId = @ExecutorUserId,
					 @ExecutionTime = @ExecutionTime 			
		END
		ELSE
		BEGIN
			EXEC sp_MIG_FoszamAthelyezes
					 @Id = @Id,
					 @Ugyhova = '3', -- OsztĂˇlyon
					 @MoveHierarchy = '1',
					 @ExecutorUserId = @ExecutorUserId,
					 @ExecutionTime = @ExecutionTime 
		END          
	END
END

--VisszavonĂˇs esetĂ©n, ha a szerelendĹ‘ ĂĽgyirat irattĂˇrbĂłl elkĂ©rt vagy skontrĂłbol elkĂ©rt, akkor vissza tesszĂĽk irattĂˇrba vagy skontrĂłba
IF @Visszavonas = 1
BEGIN
	IF @Act_Ugyhol = 'I' -- IrattĂˇrbĂłl elkĂ©rt
	BEGIN
		EXEC sp_MIG_FoszamAthelyezes
					 @Id = @Id,
					 @Ugyhova = '2', -- IrattĂˇrban
					 @MoveHierarchy = '1',
					 @ExecutorUserId = @ExecutorUserId,
					 @ExecutionTime = @ExecutionTime 
	END
	ELSE IF @Act_Ugyhol = 'E' -- SkontrĂłbĂłl elkĂ©rt
	BEGIN
		EXEC sp_MIG_FoszamAthelyezes
					 @Id = @Id,
					 @Ugyhova = '1', -- SkontrĂłban
					 @MoveHierarchy = '1',
					 @ExecutorUserId = @ExecutorUserId,
					 @ExecutionTime = @ExecutionTime 
	END
END

---------------------------------------------------------
-- akkor jĂ¶het a tĂ©nyleges mĂłdosĂ­tĂˇs (update)            
---------------------------------------------------------
update dbo.MIG_Foszam
   set Edok_Utoirat_Id = @Edok_Utoirat_Id,
       Edok_Utoirat_Azon = @Edok_Utoirat_Azon,
       Ver = ISNULL(Ver,1) + 1
 where Id = @Id

select  @error = @@error, @rowcount = @@rowcount

if @error <> 0
   --RAISERROR('MIG_Foszam update hiba!',16,1)
   RAISERROR('[56014]',16,1)

if @rowcount = 0
    --RAISERROR('Az ĂĽgyirat kĂ¶zben mĂłdosult!',16,1)
   RAISERROR('[56011]',16,1)
   
/* History Log */   
exec sp_LogRecordToHistory 'MIG_Foszam',@Id
					,'MIG_FoszamHistory',1,@ExecutorUserId,@ExecutionTime   

if @sajat_tranz = 1  
   COMMIT TRANSACTION

END TRY
-----------
-----------
BEGIN CATCH
   if @sajat_tranz = 1  
      ROLLBACK TRANSACTION
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------