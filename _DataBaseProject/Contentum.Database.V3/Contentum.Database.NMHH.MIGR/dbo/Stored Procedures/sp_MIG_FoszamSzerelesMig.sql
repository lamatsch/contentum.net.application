﻿alter procedure [dbo].[sp_MIG_FoszamSzerelesMig]
             @Szerelendo_Id UNIQUEIDENTIFIER = NULL,
             @Csatolva_Id UNIQUEIDENTIFIER = NULL,
             @Csatolva_Rendszer INT = NULL,
             @Visszavonas INT = 0,
             @ExecutorUserId uniqueidentifier,
			 @ExecutionTime DATETIME
as

/*
--              Hibaüzenetek helyett hibakódok, az alkalmazás oldja fel
--                  [56001] 'Az ügyirat (alszám) megadása kötelezõ!'
--                  [56002] 'MIG_Alszam select hiba!'
--                  [56003] 'A megadott ügyirat nem létezik!'
--                  [56004] 'Az ügyirat új helyének megadása kötelezõ!'
--                  [56005] 'Az ügyirat új helyének megadása hibás!'
--                  [56006] 'Az ügyirat már eleve a kért helyen van!'
--                  [56007] 'Irattárból skontróba való áthelyezés nem engedélyezett!'
--                  [56008] 'Skontróba helyezésnél kötelezõ a skontró végének megadása!'
--                  [56009] 'A skontró végének az aktuális napnál késõbbinek kell lennie!'
--                  [56010] 'MIG_Alszam update hiba!'
--                  [56011] 'Az ügyirat közben módosult!'
--                  [56012] 'Utóirat id-jának megadása kötelezõ!'
--                  [56013] 'Utóirat azonosítójának megadása kötelezõ!'
--                  [56014] 'MIG_Foszam update hiba!'
--                  [56015] 'MIG_Foszam select hiba!'
--                  [56016] 'A megadott elõzmény ügyirat nem létezik!'
--                  [56017] 'A megadott elõzmény ügyirat már szerelve van!'
--
--                  [56301] 'Az ügyirat (fõszám) megadása kötelezõ!'
*/
/*
Módosította:
-- 2008.07.14. Boda Eszter:
   A MIG_Alszam táblából az UGYHOL (valamint a SCONTRO, IRATTARBA) mezõk átkerültek
   a MIG_Foszam táblába
   Ha a szerelendõ (régi) ügyirat nincs osztályon, és nem visszavonás történik,
   automatikusan kivesszük teljes hierarchiájával együtt,
   mivel másként nem lehetne együtt mozgatni a szülõ EDOK-os ügyirattal
       -> sp_MIG_FoszamAthelyezes hívása
-- 2009.05.14. Boda Eszter:
   Ha a szerelendõ (régi) ügyirat állapota Irattárból elkért ('I') vagy Skontróból elkért ('E'),
   nem helyezzük át automatikusan, hanem aktuális állapotában megtartjuk
 -- 2017.01.26. Nagy Csaba:
	t alakítva a rendes szereléshez Migrationben az SP_MIG_FoszamSzereles alapján.	
*/

---------
---------
BEGIN TRY

set nocount on;

declare @teszt           int
select  @teszt = 0
 
declare @sajat_tranz     int  -- saját tranzakció kezelés? (0 = nem, 1 = igen)
select  @sajat_tranz   = (CASE When @@trancount > 0 Then 0 Else 1 END)

if @sajat_tranz = 1  
   BEGIN TRANSACTION 
   
   
--
DECLARE @Id UNIQUEIDENTIFIER
DECLARE @Act_Csatolva_Id UNIQUEIDENTIFIER
DECLARE @Act_Csatolva_Rendszer INT

DECLARE @Act_Ugyhol char(1)
DECLARE @Act_Ver int
DECLARE @Act_Ugyirat_tipus NVARCHAR(20)
--
declare @error           int
declare @rowcount        int

--------------------------------
-- input paraméterek ellenõrzése
--------------------------------
-- input paraméterek kiiratása
if @teszt = 1
   print '@Szerelendo_Id  = '+ isnull(convert(varchar(50),@Csatolva_Id) , 'NULL') + ', ' +
         '@Csatolva_Id  = '+ isnull(convert(varchar(50),@Csatolva_Id) , 'NULL') + ', ' +
         '@Csatolva_Rendszer = '+ isnull(@Csatolva_Rendszer, 'NULL') + ', ' +
         '@Visszavonas = ' + isnull(convert(nvarchar(50),@Visszavonas), 'NULL')

--

IF @Visszavonas = 1
BEGIN
	SET @Csatolva_Id = NULL
	SET @Csatolva_Rendszer = NULL
END
ELSE
BEGIN
	if @Csatolva_Id is NULL
	   --RAISERROR('Utóirat id-jának megadása kötelezõ!,16,1)
	   RAISERROR('[56012]',16,1)
END   
   
-- a régi dokumentum olvasása
select @Id = MIG_Foszam.Id,
       @Act_Csatolva_Id  = MIG_Foszam.Csatolva_Id,
       @Act_Csatolva_Rendszer  = MIG_Foszam.Csatolva_Rendszer,
       @Act_Ugyhol = MIG_foszam.UGYHOL,
       @Act_Ver = Ver,
	   @Act_Ugyirat_tipus = MIG_foszam.Ugyirat_tipus
  from MIG_Foszam
--  inner join MIG_Sav on MIG_Sav.Id = MIG_Foszam.MIG_Sav_Id
 where  
--	 SUBSTRING(MIG_Sav.NAME,2, CHARINDEX(')', MIG_Sav.NAME) - 2 ) = @Sav
-- AND CHARINDEX('(', MIG_Sav.NAME) = 1 AND CHARINDEX(')', MIG_Sav.NAME) > 2
	Id= @Szerelendo_Id

select  @error = @@error, @rowcount = @@rowcount

if @error <> 0
   --RAISERROR('MIG_Foszam select hiba!',16,1)
   RAISERROR('[56015]',16,1)

if @rowcount = 0
   --RAISERROR('A megadott elõzmény ügyirat nem létezik!',16,1)
   RAISERROR('[56016]',16,1) 

IF @Visszavonas != 1
BEGIN   
	IF @Act_Csatolva_Id IS NOT NULL
	-- RAISERROR('A megadott elõzmény ügyirat már szerelve van!',16,1)
	   RAISERROR('[56017]',16,1)             
END

-- Ha a szerelendo ugyirat nincs osztályon, kivesszuk, hierarchiajaval egyutt
-- kivéve, ha Irattûrból elért ('I') vagy Skontróból elkért ('E')
IF @Act_Ugyhol not in ('I','E')
BEGIN
	IF @Visszavonas != 1 and @Act_Ugyhol != '3'
	BEGIN
		IF @Act_Ugyhol = '2' --Irattárban
		BEGIN
			EXEC sp_MIG_FoszamAthelyezes
					 @Id = @Id,
					 @Ugyhova = 'I', -- Irattárból elkért
					 @MoveHierarchy = '1',
					 @ExecutorUserId = @ExecutorUserId,
					 @ExecutionTime = @ExecutionTime 			
		END
		ELSE IF @Act_Ugyhol = '1' --Skontróban
		BEGIN
			EXEC sp_MIG_FoszamAthelyezes
					 @Id = @Id,
					 @Ugyhova = 'E', -- Skontróból elkért
					 @MoveHierarchy = '1',
					 @ExecutorUserId = @ExecutorUserId,
					 @ExecutionTime = @ExecutionTime 			
		END
		ELSE
		BEGIN
			EXEC sp_MIG_FoszamAthelyezes
					 @Id = @Id,
					 @Ugyhova = '3', -- Osztályon
					 @MoveHierarchy = '1',
					 @ExecutorUserId = @ExecutorUserId,
					 @ExecutionTime = @ExecutionTime 
		END          
	END
END
---------------------------------------------------------
-- Ugyirat tipus ?        
---------------------------------------------------------
--BUG 75
DECLARE @New_Ugyirat_tipus NVARCHAR(20)
DECLARE @Ugyirat_tipus_V nvarchar(20)
DECLARE @Ugyirat_tipus_E nvarchar(20)
DECLARE @Ugyirat_tipus_P nvarchar(20)

SET @Ugyirat_tipus_P = 'papír alapú'
SET @Ugyirat_tipus_E = 'elektronikus'
SET @Ugyirat_tipus_V = 'vegyes'

SELECT @New_Ugyirat_tipus = Ugyirat_tipus
FROM dbo.MIG_Foszam
WHERE Id = @Csatolva_Id

--------
DECLARE @Ugyirat_tipus_kod INT
--IF (@New_Ugyirat_tipus <> @ACT_Ugyirat_tipus)
IF (@New_Ugyirat_tipus = @Ugyirat_tipus_E OR 
	@New_Ugyirat_tipus = @Ugyirat_tipus_V OR
	@ACT_Ugyirat_tipus = @Ugyirat_tipus_E OR
	@ACT_Ugyirat_tipus = @Ugyirat_tipus_V)
	SET @Ugyirat_tipus_kod = 1
ELSE
	SET @Ugyirat_tipus_kod = 0

---SAVE TO LOG
IF @Visszavonas = 0
BEGIN
	IF  @Ugyirat_tipus_kod <> 0
	BEGIN
		PRINT 'Add to history'
		EXECUTE [dbo].[sp_LogMIG_FoszamHistory] 
		   @Csatolva_Id
		  ,10--@Muvelet
		  ,@ExecutorUserId--@Vegrehajto_Id
		  ,@ExecutionTime--@VegrehajtasIdo

		PRINT 'Update ugyirat tipus'
		UPDATE dbo.MIG_Foszam
		SET  Ugyirat_tipus = CASE @Ugyirat_tipus_kod
							WHEN 1 THEN @Ugyirat_tipus_V
							ELSE Ugyirat_tipus
						END
		WHERE Id= @Csatolva_Id
	END
END
ELSE IF @Visszavonas = 1
BEGIN
		PRINT 'Visszavonas - Ugyirat_tipus visszaallitas'
		DECLARE @CSAT_ID			UNIQUEIDENTIFIER
		DECLARE @UgyiratTipusPrev	NVARCHAR(20)

		SELECT @CSAT_ID = Csatolva_Id 
		FROM dbo.MIG_Foszam
		WHERE ID = @ID		

		UPDATE dbo.MIG_Foszam
		SET Edok_Utoirat_Id = NULL,
			   Edok_Utoirat_Azon = NULL			   
		WHERE Id = @Id

		IF @CSAT_ID IS NOT NULL
		BEGIN
			SELECT TOP 1 @UgyiratTipusPrev = Ugyirat_tipus 
			FROM  MIG_FoszamHistory
			WHERE ID = @CSAT_ID
				AND HistoryMuvelet_Id = 10
			ORDER BY HistoryVegrehajtasIdo DESC

			EXECUTE [dbo].[sp_LogMIG_FoszamHistory] 
			   @CSAT_ID
			  ,10--@Muvelet
			  ,@ExecutorUserId--@Vegrehajto_Id
			  ,@ExecutionTime--@VegrehajtasIdo

			PRINT 'Update ugyirat tipus'
			UPDATE dbo.MIG_Foszam
				SET
				  Ugyirat_tipus = @UgyiratTipusPrev				 
			WHERE Id= @CSAT_ID
		END
END


---------------------------------------------------------
-- akkor jöhet a tényleges módosítás (update)            
---------------------------------------------------------
update dbo.MIG_Foszam
   set Csatolva_Id = @Csatolva_Id,
       Csatolva_Rendszer = @Csatolva_Rendszer,
       Ver = ISNULL(Ver,1) + 1
 where Id = @Id

select  @error = @@error, @rowcount = @@rowcount

if @error <> 0
   --RAISERROR('MIG_Foszam update hiba!',16,1)
   RAISERROR('[56014]',16,1)

if @rowcount = 0
    --RAISERROR('Az ügyirat közben módosult!',16,1)
   RAISERROR('[56011]',16,1)
   
/* History Log */   
exec sp_LogRecordToHistory 'MIG_Foszam',@Id
					,'MIG_FoszamHistory',1,@ExecutorUserId,@ExecutionTime   

if @sajat_tranz = 1  
   COMMIT TRANSACTION

END TRY
-----------
-----------
BEGIN CATCH
   if @sajat_tranz = 1  
      ROLLBACK TRANSACTION
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(4000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
---------
---------