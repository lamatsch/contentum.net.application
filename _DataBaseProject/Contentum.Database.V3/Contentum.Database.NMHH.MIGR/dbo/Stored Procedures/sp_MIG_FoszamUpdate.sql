﻿CREATE procedure [dbo].[sp_MIG_FoszamUpdate]
             @Id uniqueidentifier,		
             @ExecutorUserId uniqueidentifier,
             @ExecutionTime datetime,
             @UI_SAV int = NULL,
			 @UI_YEAR int = NULL,
			 @UI_NUM int = NULL,
			 @UI_NAME nvarchar(30) = NULL,
			 @UI_IRSZ nvarchar(4) = NULL,
			 @UI_UTCA nvarchar(25) = NULL,
			 @UI_HSZ nvarchar(12) = NULL,
			 @UI_HRSZ nvarchar(20) = NULL,
			 @UI_TYPE nvarchar(4) = NULL,
			 @UI_IRJ nvarchar(6) = NULL,
			 @UI_PERS nvarchar(6) = NULL,
			 @UI_OT_ID nvarchar(20) = NULL,
			 @MEMO nvarchar(1000) = NULL,
			 @IRSZ_PLUSS INT = NULL,
			 @IRJ2000 nvarchar(7) = NULL,
			 @Conc nvarchar(255) = NULL,
			 @Selejtezve INT = NULL,
			 @Selejtezes_Datuma DATETIME = NULL,
			 @Csatolva_Rendszer INT = NULL,
			 @Csatolva_Id UNIQUEIDENTIFIER = NULL,
			 @MIG_Sav_Id UNIQUEIDENTIFIER = NULL,
			 @MIG_Varos_Id UNIQUEIDENTIFIER = NULL,
			 @MIG_IktatasTipus_Id UNIQUEIDENTIFIER = NULL,
			 @MIG_Eloado_Id UNIQUEIDENTIFIER = NULL,
			 @Ugyirat_tipus nvarchar(20) = null,
			 @Edok_Ugyintezo_Csoport_Id UNIQUEIDENTIFIER = NULL,
			 @Edok_Ugyintezo_Nev nvarchar(100) = NULL,
			 @Edok_Utoirat_Id UNIQUEIDENTIFIER = NULL,
			 @Edok_Utoirat_Azon nvarchar(100) = NULL,
			 @UGYHOL nvarchar(1) = NULL,
			 @Hatarido DATETIME = NULL,
			 @IRATTARBA DATETIME = NULL,
			 @SCONTRO DATETIME = NULL,
			 @Ver int  = null,
			 @EdokSav nvarchar(3)  = null, -- "ReadOnly" mező
			 @MegorzesiIdo DATETIME = null,
			 @IrattarId UNIQUEIDENTIFIER = NULL,
			 @IrattariHely nvarchar(100) = NULL,
             @UpdatedColumns XML
             
AS

BEGIN TRY

set nocount on;

DECLARE @updateColumnsAndValues NVARCHAR(4000)
SET @updateColumnsAndValues = ''
       
       
         IF @UpdatedColumns.exist('/root/UI_SAV')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + 'UI_SAV = @UI_SAV
            '
         END
         IF @UpdatedColumns.exist('/root/UI_YEAR')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_YEAR = @UI_YEAR
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_NUM')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_NUM = @UI_NUM
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_NAME')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_NAME = @UI_NAME
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_IRSZ')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_IRSZ = @UI_IRSZ
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_UTCA')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_UTCA = @UI_UTCA
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_HSZ')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_HSZ = @UI_HSZ
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_HRSZ')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_HRSZ = @UI_HRSZ
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_TYPE')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_TYPE = @UI_TYPE
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_IRJ')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_IRJ = @UI_IRJ
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_PERS')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_PERS = @UI_PERS
            '
         end 
         IF @UpdatedColumns.exist('/root/UI_OT_ID')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UI_OT_ID = @UI_OT_ID
            '
         end 
         IF @UpdatedColumns.exist('/root/MEMO')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',MEMO = @MEMO
            '
         end 
         IF @UpdatedColumns.exist('/root/IRSZ_PLUSS')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',IRSZ_PLUSS = @IRSZ_PLUSS
            '
         end 
         IF @UpdatedColumns.exist('/root/IRJ2000')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',IRJ2000 = @IRJ2000
            '
         end 
         IF @UpdatedColumns.exist('/root/Conc')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Conc = @Conc
            '
         end 
         IF @UpdatedColumns.exist('/root/Selejtezve')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Selejtezve = @Selejtezve
            '
         end 
         IF @UpdatedColumns.exist('/root/Selejtezes_Datuma')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Selejtezes_Datuma = @Selejtezes_Datuma
            '
         end 
         IF @UpdatedColumns.exist('/root/Csatolva_Rendszer')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Csatolva_Rendszer = @Csatolva_Rendszer
            '
         end 
         IF @UpdatedColumns.exist('/root/Csatolva_Id')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Csatolva_Id = @Csatolva_Id
            '
         end 
         IF @UpdatedColumns.exist('/root/MIG_Sav_Id')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',MIG_Sav_Id = @MIG_Sav_Id
            '
         end 
         IF @UpdatedColumns.exist('/root/MIG_Varos_Id')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',MIG_Varos_Id = @MIG_Varos_Id
            '
         end 
         IF @UpdatedColumns.exist('/root/MIG_IktatasTipus_Id')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',MIG_IktatasTipus_Id = @MIG_IktatasTipus_Id
            '
         end 
         IF @UpdatedColumns.exist('/root/MIG_Eloado_Id')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',MIG_Eloado_Id = @MIG_Eloado_Id
            '
         end 
         IF @UpdatedColumns.exist('/root/Edok_Utoirat_Id')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Edok_Utoirat_Id = @Edok_Utoirat_Id
            '
         end 
         IF @UpdatedColumns.exist('/root/Edok_Utoirat_Azon')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Edok_Utoirat_Azon = @Edok_Utoirat_Azon
            '
         end
         IF @UpdatedColumns.exist('/root/Edok_Ugyintezo_Csoport_Id')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Edok_Ugyintezo_Csoport_Id = @Edok_Ugyintezo_Csoport_Id
            '
         end 
         IF @UpdatedColumns.exist('/root/Edok_Ugyintezo_Nev')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Edok_Ugyintezo_Nev = @Edok_Ugyintezo_Nev
            '
         end  
         IF @UpdatedColumns.exist('/root/UGYHOL')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',UGYHOL = @UGYHOL
            '
         end 
         IF @UpdatedColumns.exist('/root/IRATTARBA')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',IRATTARBA = @IRATTARBA
            '
         end 
		 IF @UpdatedColumns.exist('/root/Hatarido')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Hatarido = @Hatarido
            '
         end 
		 IF @UpdatedColumns.exist('/root/Ugyirat_tipus')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',Ugyirat_tipus = @Ugyirat_tipus
            '
         end          
         IF @UpdatedColumns.exist('/root/SCONTRO')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',SCONTRO = @SCONTRO
            '
         END
		 IF @UpdatedColumns.exist('/root/MegorzesiIdo')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',MegorzesiIdo = @MegorzesiIdo
            '
         END
         IF @UpdatedColumns.exist('/root/IrattarId')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',IrattarId = @IrattarId
            '
         END
		IF @UpdatedColumns.exist('/root/IrattariHely')=1
         begin
            SET @updateColumnsAndValues = @updateColumnsAndValues + ',IrattariHely = @IrattariHely
            '
         END
         SET @updateColumnsAndValues = @updateColumnsAndValues +  ',Ver=ISNULL(Ver,1)+1
         '  
   
IF CHARINDEX(',', @updateColumnsAndValues) = 1
   SET @updateColumnsAndValues = SUBSTRING(@updateColumnsAndValues, 2, LEN(@updateColumnsAndValues) -1 )
   
DECLARE @UpdateCommand NVARCHAR(4000)

SET @UpdateCommand = 'update MIG_Foszam SET ' + @updateColumnsAndValues + ' where Id = ''' + convert(nvarchar(36), @Id) + ''''


PRINT @UpdateCommand

exec sp_executesql @UpdateCommand, 
           N'@UI_SAV int,
			 @UI_YEAR int,
			 @UI_NUM int,
			 @UI_NAME nvarchar(30),
			 @UI_IRSZ nvarchar(4),
			 @UI_UTCA nvarchar(25),
			 @UI_HSZ nvarchar(12),
			 @UI_HRSZ nvarchar(20),
			 @UI_TYPE nvarchar(4),
			 @UI_IRJ nvarchar(6),
			 @UI_PERS nvarchar(6),
			 @UI_OT_ID nvarchar(20),
			 @MEMO nvarchar(1000),
			 @IRSZ_PLUSS INT,
			 @IRJ2000 nvarchar(7),
			 @Conc nvarchar(255),
			 @Selejtezve INT,
			 @Selejtezes_Datuma DATETIME,
			 @Csatolva_Rendszer INT,
			 @Csatolva_Id UNIQUEIDENTIFIER,
			 @MIG_Sav_Id UNIQUEIDENTIFIER,
			 @MIG_Varos_Id UNIQUEIDENTIFIER,
			 @MIG_IktatasTipus_Id UNIQUEIDENTIFIER,
			 @MIG_Eloado_Id UNIQUEIDENTIFIER,
			 @Edok_Ugyintezo_Csoport_Id UNIQUEIDENTIFIER,
			 @Edok_Ugyintezo_Nev nvarchar(100),
			 @Edok_Utoirat_Id UNIQUEIDENTIFIER,
			 @Edok_Utoirat_Azon nvarchar(100),
			 @UGYHOL nvarchar(1),			 
			 @IRATTARBA DATETIME,
			 @SCONTRO DATETIME,
			 @Hatarido DATETIME,
			 @Ugyirat_tipus NVARCHAR(20),
			 @MegorzesiIdo DATETIME,
			 @IrattarId UNIQUEIDENTIFIER,
			 @IrattariHely NVARCHAR(100)',
			 @UI_SAV = @UI_SAV,
			 @UI_YEAR = @UI_YEAR,
			 @UI_NUM = @UI_NUM,
			 @UI_NAME = @UI_NAME,
			 @UI_IRSZ = @UI_IRSZ,
			 @UI_UTCA = @UI_UTCA,
			 @UI_HSZ = @UI_HSZ,
			 @UI_HRSZ = @UI_HRSZ,
			 @UI_TYPE = @UI_TYPE,
			 @UI_IRJ = @UI_IRJ,
			 @UI_PERS = @UI_PERS,
			 @UI_OT_ID = @UI_OT_ID,
			 @MEMO = @MEMO,
			 @IRSZ_PLUSS = @IRSZ_PLUSS,
			 @IRJ2000 = @IRJ2000,
			 @Conc = @Conc,
			 @Selejtezve = @Selejtezve,
			 @Selejtezes_Datuma = @Selejtezes_Datuma,
			 @Csatolva_Rendszer = @Csatolva_Rendszer,
			 @Csatolva_Id = @Csatolva_Id,
			 @MIG_Sav_Id = @MIG_Sav_Id,
			 @MIG_Varos_Id = @MIG_Varos_Id,
			 @MIG_IktatasTipus_Id = @MIG_IktatasTipus_Id,
			 @MIG_Eloado_Id = @MIG_Eloado_Id,
			 @Edok_Ugyintezo_Csoport_Id = @Edok_Ugyintezo_Csoport_Id,
			 @Edok_Ugyintezo_Nev = @Edok_Ugyintezo_Nev,
			 @Edok_Utoirat_Id = @Edok_Utoirat_Id,
			 @Edok_Utoirat_Azon =@Edok_Utoirat_Azon,
			 @UGYHOL = @UGYHOL,
			 @IRATTARBA = @IRATTARBA,
			 @SCONTRO = @SCONTRO,
			 @Hatarido = @Hatarido,
			 @Ugyirat_tipus = @Ugyirat_tipus,
			 @MegorzesiIdo = @MegorzesiIdo,
			 @IrattarId = @IrattarId,
			 @IrattariHely = @IrattariHely

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
END
ELSE
BEGIN
	/* History Log */   
	exec sp_LogRecordToHistory 'MIG_Foszam',@Id
					,'MIG_FoszamHistory',1,@ExecutorUserId,@ExecutionTime 
END

END TRY

BEGIN CATCH
    
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH
