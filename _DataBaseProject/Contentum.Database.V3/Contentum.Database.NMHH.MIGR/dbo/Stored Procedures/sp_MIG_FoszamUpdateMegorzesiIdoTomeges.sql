﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1 from  sysobjects
           where  id = object_id('sp_MIG_FoszamUpdateMegorzesiIdoTomeges')
             and  type in ('P'))
   drop procedure sp_MIG_FoszamUpdateMegorzesiIdoTomeges
go

create procedure sp_MIG_FoszamUpdateMegorzesiIdoTomeges
             @Where nvarchar(MAX)  = '',
             @ExecutorUserId uniqueidentifier,
			 @ExecutionTime DATETIME           
AS

BEGIN TRY

set nocount on;
   
DECLARE @UpdatetCommand NVARCHAR(MAX)

SET @UpdatetCommand = 'UPDATE MIG_Foszam
							SET MegorzesiIdo = m.MegorzesiIdoVege
						FROM MIG_Foszam
						CROSS APPLY dbo.fn_GetMegorzesiIdoVege(UI_YEAR, UI_IRJ, IRJ2000, IRATTARBA) m
						WHERE IRATTARBA IS NOT NULL'

SET @UpdatetCommand = @UpdatetCommand + ' and ' + @Where


exec sp_executesql @UpdatetCommand
           
END TRY

BEGIN CATCH
    
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000	
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 
      SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH