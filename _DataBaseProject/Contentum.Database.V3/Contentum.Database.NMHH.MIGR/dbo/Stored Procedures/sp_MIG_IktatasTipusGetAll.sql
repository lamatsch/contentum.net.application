﻿create procedure [dbo].[sp_MIG_IktatasTipusGetAll]
       @Where nvarchar(4000)  = '',
       @OrderBy nvarchar(200) = 'order by ui_sav, ui_year, ui_num',
       @TopRow nvarchar(5)    = '0',
       @ExecutorUserId uniqueidentifier = NULL  -- ezt egyelőre nem használjuk!!!

/*
Használata például:

exec sp_MIG_IktatasTipusGetAll 
     @Where   = 'contains( *, ''"önk"'' )',
     @OrderBy = 'order by ui_sav, ui_year, ui_num',
     @TopRow  = '1000'
*/

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)
   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   else
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

   DECLARE @Org uniqueidentifier

/*
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
*/


  SET @sqlcmd = 
  'select' + @LocalTopRow + 
  '
       MIG_IktatasTipus.Id,
       MIG_IktatasTipus.EV,
       MIG_IktatasTipus.KOD,
       MIG_IktatasTipus.NAME,
	   MIG_IktatasTipus.IRJEL
  from MIG_IktatasTipus as MIG_IktatasTipus
 where 1 = 1'
   
   if @Where is not null and @Where != ''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end

   SET @sqlcmd = @sqlcmd + ' ' + @OrderBy

   print @sqlcmd  -- teszt kiirás

   exec (@sqlcmd);

END TRY


BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER() < 50000
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1
		RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH

end