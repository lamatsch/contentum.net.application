﻿create procedure [dbo].[sp_MIG_IrattarRendezes]
         @UgyiratIds						NVARCHAR(MAX)    ,
		@ExecutorUserId				uniqueidentifier,
		@ExecutionTime					datetime ,
		@IrattarId				uniqueidentifier,
		@IrattariHelyErtek     Nvarchar(100)
as

BEGIN TRY

    create table #filter(Id UNIQUEIDENTIFIER);

	WITH FoszamHierarchy  AS
	(
	   -- Base case
	   SELECT Id, Csatolva_Id, 0 as HierarchyLevel
	   FROM MIG_Foszam
	   WHERE Id IN (SELECT convert(uniqueidentifier, Value) from dbo.fn_Split(@UgyiratIds,','))

	   UNION ALL

	   -- Recursive step
	   SELECT f.Id, f.Csatolva_Id, fh.HierarchyLevel - 1 AS HierarchyLevel
	   FROM MIG_Foszam as f
		  INNER JOIN  FoszamHierarchy as fh ON
			 f.Csatolva_Id = fh.Id
	)

	
		 
	INSERT INTO [#filter] (
		[Id]
	)SELECT id from  MIG_Foszam	where MIG_Foszam.Id IN (SELECT Id From FoszamHierarchy)
	

	update MIG_Foszam
	set	MIG_Foszam.IrattarId = @IrattarId,
	MIG_Foszam.IrattariHely = @IrattariHelyErtek,
    MIG_Foszam.Ver = ISNULL(MIG_Foszam.Ver,0)+1
	where MIG_Foszam.Id IN (SELECT Id From [#filter])


--SELECT COUNT(Id) AS RecordNumber FROM [#filter]
--select * from #filter

/* History Log */
exec sp_LogMIG_FoszamHistoryTomeges 1,@ExecutorUserId,@ExecutionTime 		

Drop table #filter

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InvalidateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
