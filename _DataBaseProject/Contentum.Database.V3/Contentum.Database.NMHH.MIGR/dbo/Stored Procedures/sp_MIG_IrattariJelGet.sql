﻿create procedure [dbo].[sp_MIG_IrattariJelGet]
			@Id int,
            @ExecutorUserId uniqueidentifier = null,
            @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	  select 
       CTXX_DBF.EV,
	   CTXX_DBF.NAME,
	   CTXX_DBF.KOD,
	   CTXX_DBF.IRJEL,
	   CTXX_DBF.UI,
	   CTXX_DBF.HATOS,
	   CTXX_DBF.KIADM,
	   CTXX_DBF.IND,
	   CTXX_DBF.ERR_KOD
	   from 
		 CTXX_DBF as CTXX_DBF 
	   where
		 CTXX_DBF.IND = @Id
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end