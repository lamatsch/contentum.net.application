﻿create procedure [dbo].[sp_MIG_IrattariJelGetAll]
       @Where nvarchar(4000)  = '',
       @OrderBy nvarchar(200) = 'order by CTXX_DBF.IRJEL ASC',
       @TopRow nvarchar(5)    = '0',
       @ExecutorUserId uniqueidentifier = NULL  -- ezt egyelőre nem használjuk!!!

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)
   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   else
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

   DECLARE @Org uniqueidentifier



  SET @sqlcmd = 
  'select' + @LocalTopRow + 
  '
       CTXX_DBF.EV,
       CTXX_DBF.NAME,
       CTXX_DBF.KOD,
       CTXX_DBF.IRJEL,
       CTXX_DBF.UI,
       CTXX_DBF.HATOS,
       CTXX_DBF.KIADM,
       CTXX_DBF.IND,
       CTXX_DBF.ERR_KOD
  from CTXX_DBF as CTXX_DBF
  where CTXX_DBF.IRJEL is not null'
   
   if @Where is not null and @Where != ''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end

   SET @sqlcmd = @sqlcmd + ' ' + @OrderBy
   
   PRINT @sqlcmd

   exec (@sqlcmd);

END TRY


BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER() < 50000
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1
		RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH

end