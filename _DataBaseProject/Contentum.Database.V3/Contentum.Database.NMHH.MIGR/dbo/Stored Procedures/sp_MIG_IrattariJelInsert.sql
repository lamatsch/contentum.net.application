﻿create procedure [dbo].[sp_MIG_IrattariJelInsert]      
                @EV     nvarchar(4)  = null,
                @NAME     nvarchar(50)  = null,
                @KOD     nvarchar(4)  = null,
                @IRJEL     nvarchar(7)  = null,
                @UI     nvarchar(4)  = null,
                @HATOS     nvarchar(10)  = null,
                @KIADM     nvarchar(1)  = null,
                @ERR_KOD     int  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            int OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @EV is not null
         begin
            SET @insertColumns = @insertColumns + ',EV'
            SET @insertValues = @insertValues + ',@EV'
         end 
       
         if @NAME is not null
         begin
            SET @insertColumns = @insertColumns + ',NAME'
            SET @insertValues = @insertValues + ',@NAME'
         end 
       
         if @KOD is not null
         begin
            SET @insertColumns = @insertColumns + ',KOD'
            SET @insertValues = @insertValues + ',@KOD'
         end 
       
         if @IRJEL is not null
         begin
            SET @insertColumns = @insertColumns + ',IRJEL'
            SET @insertValues = @insertValues + ',@IRJEL'
         end 
       
         if @UI is not null
         begin
            SET @insertColumns = @insertColumns + ',UI'
            SET @insertValues = @insertValues + ',@UI'
         end 
       
         if @HATOS is not null
         begin
            SET @insertColumns = @insertColumns + ',HATOS'
            SET @insertValues = @insertValues + ',@HATOS'
         end 
       
         if @KIADM is not null
         begin
            SET @insertColumns = @insertColumns + ',KIADM'
            SET @insertValues = @insertValues + ',@KIADM'
         end 
       
       
         if @ERR_KOD is not null
         begin
            SET @insertColumns = @insertColumns + ',ERR_KOD'
            SET @insertValues = @insertValues + ',@ERR_KOD'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id int)
'
SET @InsertCommand = @InsertCommand + 'insert into CTXX_DBF ('+@insertColumns+') output inserted.IND into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@EV nvarchar(4),@NAME nvarchar(50),@KOD nvarchar(4),@IRJEL nvarchar(7),@UI nvarchar(4),@HATOS nvarchar(10),@KIADM nvarchar(1),@ERR_KOD int,@ResultUid int OUTPUT'
,@EV = @EV,@NAME = @NAME,@KOD = @KOD,@IRJEL = @IRJEL,@UI = @UI,@HATOS = @HATOS,@KIADM = @KIADM,@ERR_KOD = @ERR_KOD ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END       
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH