﻿create procedure [dbo].[sp_MIG_IrattariJelUpdate]
        @Id							int    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @EV     nvarchar(4)  = null,         
             @NAME     nvarchar(50)  = null,         
             @KOD     nvarchar(4)  = null,         
             @IRJEL     nvarchar(7)  = null,         
             @UI     nvarchar(4)  = null,         
             @HATOS     nvarchar(10)  = null,         
             @KIADM     nvarchar(1)  = null,               
             @ERR_KOD     int  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_EV     nvarchar(4)         
     DECLARE @Act_NAME     nvarchar(50)         
     DECLARE @Act_KOD     nvarchar(4)         
     DECLARE @Act_IRJEL     nvarchar(7)         
     DECLARE @Act_UI     nvarchar(4)         
     DECLARE @Act_HATOS     nvarchar(10)         
     DECLARE @Act_KIADM     nvarchar(1)              
     DECLARE @Act_ERR_KOD     int           
  
set nocount on

select 
     @Act_EV = EV,
     @Act_NAME = NAME,
     @Act_KOD = KOD,
     @Act_IRJEL = IRJEL,
     @Act_UI = UI,
     @Act_HATOS = HATOS,
     @Act_KIADM = KIADM,
     @Act_ERR_KOD = ERR_KOD
from CTXX_DBF
where IND = @Id



  


	   IF @UpdatedColumns.exist('/root/EV')=1
         SET @Act_EV = @EV
   IF @UpdatedColumns.exist('/root/NAME')=1
         SET @Act_NAME = @NAME
   IF @UpdatedColumns.exist('/root/KOD')=1
         SET @Act_KOD = @KOD
   IF @UpdatedColumns.exist('/root/IRJEL')=1
         SET @Act_IRJEL = @IRJEL
   IF @UpdatedColumns.exist('/root/UI')=1
         SET @Act_UI = @UI
   IF @UpdatedColumns.exist('/root/HATOS')=1
         SET @Act_HATOS = @HATOS
   IF @UpdatedColumns.exist('/root/KIADM')=1
         SET @Act_KIADM = @KIADM
   IF @UpdatedColumns.exist('/root/ERR_KOD')=1
         SET @Act_ERR_KOD = @ERR_KOD   

UPDATE CTXX_DBF
SET
     EV = @Act_EV,
     NAME = @Act_NAME,
     KOD = @Act_KOD,
     IRJEL = @Act_IRJEL,
     UI = @Act_UI,
     HATOS = @Act_HATOS,
     KIADM = @Act_KIADM,
     ERR_KOD = @Act_ERR_KOD
where
  IND = @Id

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH