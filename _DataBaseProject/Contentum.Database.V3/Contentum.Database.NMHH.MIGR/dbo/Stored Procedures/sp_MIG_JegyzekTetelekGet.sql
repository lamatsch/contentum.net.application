

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_JegyzekTetelekGet')
            and   type = 'P')
   drop procedure sp_MIG_JegyzekTetelekGet
go

create procedure sp_MIG_JegyzekTetelekGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   MIG_JegyzekTetelek.Id,
	   MIG_JegyzekTetelek.Jegyzek_Id,
	   MIG_JegyzekTetelek.MIG_Foszam_Id,
	   MIG_JegyzekTetelek.UGYHOL,
	   MIG_JegyzekTetelek.AtvevoIktatoszama,
	   MIG_JegyzekTetelek.Megjegyzes,
	   MIG_JegyzekTetelek.Azonosito,
	   MIG_JegyzekTetelek.SztornozasDat,
	   MIG_JegyzekTetelek.AtadasDatuma,
	   MIG_JegyzekTetelek.Ver,
	   MIG_JegyzekTetelek.Note,
	   MIG_JegyzekTetelek.Letrehozo_id,
	   MIG_JegyzekTetelek.LetrehozasIdo,
	   MIG_JegyzekTetelek.Modosito_id,
	   MIG_JegyzekTetelek.ModositasIdo,
	   MIG_JegyzekTetelek.Tranz_id
	   from 
		 MIG_JegyzekTetelek as MIG_JegyzekTetelek 
	   where
		 MIG_JegyzekTetelek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
