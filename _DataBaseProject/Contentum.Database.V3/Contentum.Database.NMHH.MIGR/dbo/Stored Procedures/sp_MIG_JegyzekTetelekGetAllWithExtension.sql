﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_JegyzekTetelekGetAllWithExtension')
            and   type = 'P')
   drop procedure sp_MIG_JegyzekTetelekGetAllWithExtension
go

create procedure [dbo].[sp_MIG_JegyzekTetelekGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   MIG_JegyzekTetelek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   MIG_JegyzekTetelek.Id,
	   MIG_JegyzekTetelek.Jegyzek_Id,
	   MIG_JegyzekTetelek.MIG_Foszam_Id,
MIG_Foszam.Csatolva_Id,
	   MIG_JegyzekTetelek.UGYHOL,
	   MIG_JegyzekTetelek.AtvevoIktatoszama,
	   MIG_JegyzekTetelek.Megjegyzes,
	   MIG_JegyzekTetelek.Azonosito,
	   MIG_JegyzekTetelek.SztornozasDat,
	   MIG_JegyzekTetelek.AtadasDatuma,
	   MIG_JegyzekTetelek.Ver,
	   MIG_JegyzekTetelek.Note,
	   MIG_JegyzekTetelek.Letrehozo_id,
	   MIG_JegyzekTetelek.LetrehozasIdo,
	   MIG_JegyzekTetelek.Modosito_id,
	   MIG_JegyzekTetelek.ModositasIdo,
	   MIG_JegyzekTetelek.Tranz_id  
   from 
     dbo.MIG_JegyzekTetelek as MIG_JegyzekTetelek
inner JOIN dbo.MIG_Foszam MIG_Foszam on MIG_JegyzekTetelek.MIG_Foszam_Id=MIG_Foszam.Id
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end