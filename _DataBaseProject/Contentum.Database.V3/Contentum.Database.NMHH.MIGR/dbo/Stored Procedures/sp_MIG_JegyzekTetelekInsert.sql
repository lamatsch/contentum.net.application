
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_JegyzekTetelekInsert')
            and   type = 'P')
   drop procedure sp_MIG_JegyzekTetelekInsert
go

create procedure sp_MIG_JegyzekTetelekInsert    
                @Id      uniqueidentifier = null,    
                               @Jegyzek_Id     uniqueidentifier  = null,
	            @MIG_Foszam_Id     uniqueidentifier,
                @UGYHOL     nvarchar(1)  = null,
                @AtvevoIktatoszama     nvarchar(100)  = null,
                @Megjegyzes     Nvarchar(400)  = null,
                @Azonosito     Nvarchar(100)  = null,
                @SztornozasDat     datetime  = null,
                @AtadasDatuma     datetime  = null,
                @Ver     int  = null,
                @Note     Nvarchar(400)  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Jegyzek_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Jegyzek_Id'
            SET @insertValues = @insertValues + ',@Jegyzek_Id'
         end 
       
         if @MIG_Foszam_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',MIG_Foszam_Id'
            SET @insertValues = @insertValues + ',@MIG_Foszam_Id'
         end 
       
         if @UGYHOL is not null
         begin
            SET @insertColumns = @insertColumns + ',UGYHOL'
            SET @insertValues = @insertValues + ',@UGYHOL'
         end 
       
         if @AtvevoIktatoszama is not null
         begin
            SET @insertColumns = @insertColumns + ',AtvevoIktatoszama'
            SET @insertValues = @insertValues + ',@AtvevoIktatoszama'
         end 
       
         if @Megjegyzes is not null
         begin
            SET @insertColumns = @insertColumns + ',Megjegyzes'
            SET @insertValues = @insertValues + ',@Megjegyzes'
         end 
       
         if @Azonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Azonosito'
            SET @insertValues = @insertValues + ',@Azonosito'
         end 
       
         if @SztornozasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SztornozasDat'
            SET @insertValues = @insertValues + ',@SztornozasDat'
         end 
       
         if @AtadasDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',AtadasDatuma'
            SET @insertValues = @insertValues + ',@AtadasDatuma'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into MIG_JegyzekTetelek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Jegyzek_Id uniqueidentifier,@MIG_Foszam_Id uniqueidentifier,@UGYHOL nvarchar(1),@AtvevoIktatoszama nvarchar(100),@Megjegyzes Nvarchar(400),@Azonosito Nvarchar(100),@SztornozasDat datetime,@AtadasDatuma datetime,@Ver int,@Note Nvarchar(400),@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Tranz_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Jegyzek_Id = @Jegyzek_Id,@MIG_Foszam_Id = @MIG_Foszam_Id,@UGYHOL = @UGYHOL,@AtvevoIktatoszama = @AtvevoIktatoszama,@Megjegyzes = @Megjegyzes,@Azonosito = @Azonosito,@SztornozasDat = @SztornozasDat,@AtadasDatuma = @AtadasDatuma,@Ver = @Ver,@Note = @Note,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Tranz_id = @Tranz_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'MIG_JegyzekTetelek',@ResultUid
					,'MIG_JegyzekTetelekHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
go

/* használt errorcode-ok:
	50301:
	50302: 'Org' oszlop értéke nem lehet NULL
*/
