﻿create procedure [dbo].[sp_MIG_JegyzekTetelekInsertTomeges]
				@MIG_FoszamWhere nvarchar(MAX), -- TĂ¶meges update szĹ±rĂ©si feltĂ©teleknek megfelelĹ‘ tĂ©telekre
                @ExtendedMIG_AlszamWhere nvarchar(MAX) = '',
                @ExtendedMIG_EloadoWhere nvarchar(MAX) = '',
                @InsertFoszamHierarchy  char(1) = '0',    
                @Id      uniqueidentifier = null,    
                @Jegyzek_Id     uniqueidentifier  = null,
	            @MIG_Foszam_Id     uniqueidentifier = null,
                @UGYHOL     nvarchar(1)  = null,
                @AtvevoIktatoszama     nvarchar(100)  = null,
                @Megjegyzes     Nvarchar(400)  = null,
                @Azonosito     Nvarchar(100)  = null,
                @SztornozasDat     datetime  = null,
                @Ver     int  = null,
                @Note     Nvarchar(400)  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UpdatedColumns              xml = null
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;

create table #filter(Id UNIQUEIDENTIFIER)

INSERT INTO [#filter] (
	[Id]
) EXEC [sp_MIG_FoszamGetAll_Id]
	@Where = @MIG_FoszamWhere,
	@ExtendedMIG_AlszamWhere = @ExtendedMIG_AlszamWhere,
	@ExtendedMIG_EloadoWhere = @ExtendedMIG_EloadoWhere
	
IF @InsertFoszamHierarchy = '1'
BEGIN
    DECLARE @ValueTable table (Id UNIQUEIDENTIFIER);
	WITH FoszamHierarchy  AS
	(
	   -- Base case
	   SELECT Id, 0 as HierarchyLevel
	   FROM #filter

	   UNION ALL

	   -- Recursive step
	   SELECT f.Id, fh.HierarchyLevel - 1 AS HierarchyLevel
	   FROM MIG_Foszam as f
		  INNER JOIN  FoszamHierarchy as fh ON
			 f.Csatolva_Id = fh.Id
	)
	
	INSERT INTO @ValueTable
	    SELECT Id FROM FoszamHierarchy;

	    
	DELETE [#filter]
	INSERT INTO [#filter] (
		[Id]
	) SELECT Id FROM @ValueTable    
END
	
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Jegyzek_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Jegyzek_Id'
            SET @insertValues = @insertValues + ',@Jegyzek_Id'
         end 
       
         --if @MIG_Foszam_Id is not null
         --begin
            SET @insertColumns = @insertColumns + ',MIG_Foszam_Id'
            SET @insertValues = @insertValues + ',MIG_Foszam.Id'
         --end 
       
         --if @UGYHOL is not null
         --begin
            SET @insertColumns = @insertColumns + ',UGYHOL'
            SET @insertValues = @insertValues + ',MIG_Foszam.UGYHOL'
         --end 
       
         if @AtvevoIktatoszama is not null
         begin
            SET @insertColumns = @insertColumns + ',AtvevoIktatoszama'
            SET @insertValues = @insertValues + ',@AtvevoIktatoszama'
         end 
       
         if @Megjegyzes is not null
         begin
            SET @insertColumns = @insertColumns + ',Megjegyzes'
            SET @insertValues = @insertValues + ',@Megjegyzes'
         end 
       
         --if @Azonosito is not null
         --begin
            SET @insertColumns = @insertColumns + ',Azonosito'
            SET @insertValues = @insertValues + ',dbo.fn_MergeFoszam(MIG_Foszam.EdokSav, MIG_Foszam.UI_NUM, MIG_Foszam.UI_YEAR)'
         --end 
       
         if @SztornozasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SztornozasDat'
            SET @insertValues = @insertValues + ',@SztornozasDat'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

CREATE TABLE #result (id uniqueidentifier)

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = '
'
SET @InsertCommand = @InsertCommand + 'insert into MIG_JegyzekTetelek ('+@insertColumns+') output inserted.id into #result select '+@insertValues+'
'
SET @InsertCommand = @InsertCommand + ' from MIG_Foszam where Id in (select Id from #filter)
'
--SET @InsertCommand = @InsertCommand + 'select id from #result'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Jegyzek_Id uniqueidentifier,@MIG_Foszam_Id uniqueidentifier,@UGYHOL nvarchar(1),@AtvevoIktatoszama nvarchar(100),@Megjegyzes Nvarchar(400),@Azonosito Nvarchar(100),@SztornozasDat datetime,@Ver int,@Note Nvarchar(400),@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Tranz_id uniqueidentifier'
,@Id = @Id,@Jegyzek_Id = @Jegyzek_Id,@MIG_Foszam_Id = @MIG_Foszam_Id,@UGYHOL = @UGYHOL,@AtvevoIktatoszama = @AtvevoIktatoszama,@Megjegyzes = @Megjegyzes,@Azonosito = @Azonosito,@SztornozasDat = @SztornozasDat,@Ver = @Ver,@Note = @Note,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Tranz_id = @Tranz_id


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogMIG_JegyzekTetelekHistoryTomeges 0,@Letrehozo_id,@LetrehozasIdo
END

drop table #filter
drop table #result
            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH