set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_JegyzekTetelekUpdate')
            and   type = 'P')
   drop procedure sp_MIG_JegyzekTetelekUpdate
go

create procedure sp_MIG_JegyzekTetelekUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Jegyzek_Id     uniqueidentifier  = null,         
             @MIG_Foszam_Id     uniqueidentifier  = null,         
             @UGYHOL     nvarchar(1)  = null,         
             @AtvevoIktatoszama     nvarchar(100)  = null,         
             @Megjegyzes     Nvarchar(400)  = null,         
             @Azonosito     Nvarchar(100)  = null,         
             @SztornozasDat     datetime  = null,         
             @AtadasDatuma     datetime  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(400)  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Jegyzek_Id     uniqueidentifier         
     DECLARE @Act_MIG_Foszam_Id     uniqueidentifier         
     DECLARE @Act_UGYHOL     nvarchar(1)         
     DECLARE @Act_AtvevoIktatoszama     nvarchar(100)         
     DECLARE @Act_Megjegyzes     Nvarchar(400)         
     DECLARE @Act_Azonosito     Nvarchar(100)         
     DECLARE @Act_SztornozasDat     datetime         
     DECLARE @Act_AtadasDatuma     datetime         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(400)         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Jegyzek_Id = Jegyzek_Id,
     @Act_MIG_Foszam_Id = MIG_Foszam_Id,
     @Act_UGYHOL = UGYHOL,
     @Act_AtvevoIktatoszama = AtvevoIktatoszama,
     @Act_Megjegyzes = Megjegyzes,
     @Act_Azonosito = Azonosito,
     @Act_SztornozasDat = SztornozasDat,
     @Act_AtadasDatuma = AtadasDatuma,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Tranz_id = Tranz_id
from MIG_JegyzekTetelek
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   

	   IF @UpdatedColumns.exist('/root/Jegyzek_Id')=1
         SET @Act_Jegyzek_Id = @Jegyzek_Id
   IF @UpdatedColumns.exist('/root/MIG_Foszam_Id')=1
         SET @Act_MIG_Foszam_Id = @MIG_Foszam_Id
   IF @UpdatedColumns.exist('/root/UGYHOL')=1
         SET @Act_UGYHOL = @UGYHOL
   IF @UpdatedColumns.exist('/root/AtvevoIktatoszama')=1
         SET @Act_AtvevoIktatoszama = @AtvevoIktatoszama
   IF @UpdatedColumns.exist('/root/Megjegyzes')=1
         SET @Act_Megjegyzes = @Megjegyzes
   IF @UpdatedColumns.exist('/root/Azonosito')=1
         SET @Act_Azonosito = @Azonosito
   IF @UpdatedColumns.exist('/root/SztornozasDat')=1
         SET @Act_SztornozasDat = @SztornozasDat
   IF @UpdatedColumns.exist('/root/AtadasDatuma')=1
         SET @Act_AtadasDatuma = @AtadasDatuma
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE MIG_JegyzekTetelek
SET
     Jegyzek_Id = @Act_Jegyzek_Id,
     MIG_Foszam_Id = @Act_MIG_Foszam_Id,
     UGYHOL = @Act_UGYHOL,
     AtvevoIktatoszama = @Act_AtvevoIktatoszama,
     Megjegyzes = @Act_Megjegyzes,
     Azonosito = @Act_Azonosito,
     SztornozasDat = @Act_SztornozasDat,
     AtadasDatuma = @Act_AtadasDatuma,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Tranz_id = @Act_Tranz_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'MIG_JegyzekTetelek',@Id
					,'MIG_JegyzekTetelekHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

go

/* használt errorcode-ok:
	50401:
	50402: 'Ver' oszlop értéke nem egyezik a meglévő rekordéval
   50403: érvényesség már lejárt a rekordra
	50499: rekord zárolva
*/
