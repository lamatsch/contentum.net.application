﻿create procedure [dbo].[sp_MIG_JegyzekTetelekUpdateTomeges]
        @Where  nvarchar(MAX), -- TĂ¶meges update szĹ±rĂ©si feltĂ©teleknek megfelelĹ‘ tĂ©telekre
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Jegyzek_Id     uniqueidentifier  = null,         
             @MIG_Foszam_Id     uniqueidentifier  = null,         
             @UGYHOL     nvarchar(1)  = null,         
             @AtvevoIktatoszama     nvarchar(100)  = null,         
             @Megjegyzes     Nvarchar(400)  = null,         
             @Azonosito     Nvarchar(100)  = null,         
             @SztornozasDat     datetime  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(400)  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction
     
  
set nocount on

create table #filter(Id UNIQUEIDENTIFIER)

DECLARE @sqlcmd nvarchar(MAX)

SET @sqlcmd = '
INSERT INTO [#filter] (
	[Id]
) select Id
from MIG_JegyzekTetelek where ' + @Where

exec (@sqlcmd);


DECLARE @updateColumnsAndValues NVARCHAR(4000)
SET @updateColumnsAndValues = ''


	   IF @UpdatedColumns.exist('/root/Jegyzek_Id')=1
         SET  @updateColumnsAndValues = @updateColumnsAndValues + 'Jegyzek_Id = @Jegyzek_Id
         '
   IF @UpdatedColumns.exist('/root/MIG_Foszam_Id')=1
         SET @updateColumnsAndValues = @updateColumnsAndValues + ',MIG_Foszam_Id = @MIG_Foszam_Id
         '
   IF @UpdatedColumns.exist('/root/UGYHOL')=1
         SET @updateColumnsAndValues = @updateColumnsAndValues + ',UGYHOL = @UGYHOL
         '
   IF @UpdatedColumns.exist('/root/AtvevoIktatoszama')=1
         SET @updateColumnsAndValues = @updateColumnsAndValues + ',AtvevoIktatoszama = @AtvevoIktatoszama
         '
   IF @UpdatedColumns.exist('/root/Megjegyzes')=1
         SET @updateColumnsAndValues = @updateColumnsAndValues + ',Megjegyzes = @Megjegyzes
         '
   IF @UpdatedColumns.exist('/root/Azonosito')=1
         SET @updateColumnsAndValues = @updateColumnsAndValues + ',Azonosito = @Azonosito
         '
   IF @UpdatedColumns.exist('/root/SztornozasDat')=1
         SET @updateColumnsAndValues = @updateColumnsAndValues + ',SztornozasDat = @SztornozasDat
         '
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @updateColumnsAndValues = @updateColumnsAndValues + ',Note = @Note
         '
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @updateColumnsAndValues = @updateColumnsAndValues + ',Letrehozo_id = @Letrehozo_id
         '
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @updateColumnsAndValues = @updateColumnsAndValues + ',LetrehozasIdo = @LetrehozasIdo
         '
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @updateColumnsAndValues = @updateColumnsAndValues + ',Modosito_id = @Modosito_id
         '
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @updateColumnsAndValues = @updateColumnsAndValues + ',ModositasIdo = @ModositasIdo
         '
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @updateColumnsAndValues = @updateColumnsAndValues + ',Tranz_id = @Tranz_id
         '
   
   SET @updateColumnsAndValues = @updateColumnsAndValues + ',Ver=ISNULL(Ver,1)+1
          '
          
   IF CHARINDEX(',', @updateColumnsAndValues) = 1
   SET @updateColumnsAndValues = SUBSTRING(@updateColumnsAndValues, 2, LEN(@updateColumnsAndValues) -1 )
   
   DECLARE @UpdatetCommand NVARCHAR(4000)

   create table #result(Id UNIQUEIDENTIFIER)
   
   SET @UpdatetCommand = 'update MIG_JegyzekTetelek SET ' + @updateColumnsAndValues + ' output inserted.id into #result 
   where MIG_JegyzekTetelek.Id in (select Id from #filter)'
   
   exec sp_executesql @UpdatetCommand, 
                             N'@Jegyzek_Id uniqueidentifier,@MIG_Foszam_Id uniqueidentifier,@UGYHOL nvarchar(1),@AtvevoIktatoszama nvarchar(100),@Megjegyzes Nvarchar(400),@Azonosito Nvarchar(100),@SztornozasDat datetime,@Ver int,@Note Nvarchar(400),@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Tranz_id uniqueidentifier'
,@Jegyzek_Id = @Jegyzek_Id,@MIG_Foszam_Id = @MIG_Foszam_Id,@UGYHOL = @UGYHOL,@AtvevoIktatoszama = @AtvevoIktatoszama,@Megjegyzes = @Megjegyzes,@Azonosito = @Azonosito,@SztornozasDat = @SztornozasDat,@Ver = @Ver,@Note = @Note,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Tranz_id = @Tranz_id
   

if @@error != 0
BEGIN
   RAISERROR('[50341]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogMIG_JegyzekTetelekHistoryTomeges 1,@ExecutorUserId,@ExecutionTime 
END

drop table #filter
drop table #result


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH