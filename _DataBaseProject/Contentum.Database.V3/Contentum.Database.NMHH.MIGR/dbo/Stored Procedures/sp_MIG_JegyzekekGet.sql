

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_JegyzekekGet')
            and   type = 'P')
   drop procedure sp_MIG_JegyzekekGet
go

create procedure sp_MIG_JegyzekekGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   MIG_Jegyzekek.Id,
	   MIG_Jegyzekek.Tipus,
	   MIG_Jegyzekek.Csoport_Id_Felelos,
	   MIG_Jegyzekek.Felelos_Nev,
	   MIG_Jegyzekek.FelhasznaloCsoport_Id_Vegrehaj,
	   MIG_Jegyzekek.Vegrehajto_Nev,
	   MIG_Jegyzekek.Atvevo_Nev,
	   MIG_Jegyzekek.LezarasDatuma,
	   MIG_Jegyzekek.SztornozasDat,
	   MIG_Jegyzekek.Nev,
	   MIG_Jegyzekek.VegrehajtasDatuma,
	   MIG_Jegyzekek.Allapot,
	   MIG_Jegyzekek.Allapot_Nev,
	   MIG_Jegyzekek.VegrehajtasKezdoDatuma,
	   MIG_Jegyzekek.MegsemmisitesDatuma,
	   MIG_Jegyzekek.Ver,
	   MIG_Jegyzekek.Note,
	   MIG_Jegyzekek.Letrehozo_id,
	   MIG_Jegyzekek.LetrehozasIdo,
	   MIG_Jegyzekek.Modosito_id,
	   MIG_Jegyzekek.ModositasIdo,
	   MIG_Jegyzekek.Tranz_id
	   from 
		 MIG_Jegyzekek as MIG_Jegyzekek 
	   where
		 MIG_Jegyzekek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
