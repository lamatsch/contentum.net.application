
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_JegyzekekGetAll')
            and   type = 'P')
   drop procedure sp_MIG_JegyzekekGetAll
go

create procedure sp_MIG_JegyzekekGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   MIG_Jegyzekek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   MIG_Jegyzekek.Id,
	   MIG_Jegyzekek.Tipus,
	   MIG_Jegyzekek.Csoport_Id_Felelos,
	   MIG_Jegyzekek.Felelos_Nev,
	   MIG_Jegyzekek.FelhasznaloCsoport_Id_Vegrehaj,
	   MIG_Jegyzekek.Vegrehajto_Nev,
	   MIG_Jegyzekek.Atvevo_Nev,
	   MIG_Jegyzekek.LezarasDatuma,
	   MIG_Jegyzekek.SztornozasDat,
	   MIG_Jegyzekek.Nev,
	   MIG_Jegyzekek.VegrehajtasDatuma,
	   MIG_Jegyzekek.Allapot,
	   MIG_Jegyzekek.Allapot_Nev,
	   MIG_Jegyzekek.VegrehajtasKezdoDatuma,
	   MIG_Jegyzekek.MegsemmisitesDatuma,
	   MIG_Jegyzekek.Ver,
	   MIG_Jegyzekek.Note,
	   MIG_Jegyzekek.Letrehozo_id,
	   MIG_Jegyzekek.LetrehozasIdo,
	   MIG_Jegyzekek.Modosito_id,
	   MIG_Jegyzekek.ModositasIdo,
	   MIG_Jegyzekek.Tranz_id  
   from 
     MIG_Jegyzekek as MIG_Jegyzekek      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go