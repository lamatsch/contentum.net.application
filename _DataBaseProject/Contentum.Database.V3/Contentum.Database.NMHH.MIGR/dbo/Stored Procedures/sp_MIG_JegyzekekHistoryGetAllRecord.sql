set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_JegyzekekHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_MIG_JegyzekekHistoryGetAllRecord
go

create procedure sp_MIG_JegyzekekHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from MIG_JegyzekekHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_JegyzekekHistory Old
         inner join MIG_JegyzekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tipus' as ColumnName,               cast(Old.Tipus as nvarchar(99)) as OldValue,
               cast(New.Tipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_JegyzekekHistory Old
         inner join MIG_JegyzekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tipus as nvarchar(max)),'') != ISNULL(CAST(New.Tipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Felelos' as ColumnName,               cast(Old.Csoport_Id_Felelos as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Felelos as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_JegyzekekHistory Old
         inner join MIG_JegyzekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id_Felelos as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id_Felelos as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felelos_Nev' as ColumnName,               cast(Old.Felelos_Nev as nvarchar(99)) as OldValue,
               cast(New.Felelos_Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_JegyzekekHistory Old
         inner join MIG_JegyzekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Felelos_Nev as nvarchar(max)),'') != ISNULL(CAST(New.Felelos_Nev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Vegrehaj' as ColumnName,               cast(Old.FelhasznaloCsoport_Id_Vegrehaj as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Vegrehaj as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_JegyzekekHistory Old
         inner join MIG_JegyzekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Vegrehaj as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Vegrehaj as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Vegrehajto_Nev' as ColumnName,               cast(Old.Vegrehajto_Nev as nvarchar(99)) as OldValue,
               cast(New.Vegrehajto_Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_JegyzekekHistory Old
         inner join MIG_JegyzekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Vegrehajto_Nev as nvarchar(max)),'') != ISNULL(CAST(New.Vegrehajto_Nev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Atvevo_Nev' as ColumnName,               cast(Old.Atvevo_Nev as nvarchar(99)) as OldValue,
               cast(New.Atvevo_Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_JegyzekekHistory Old
         inner join MIG_JegyzekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Atvevo_Nev as nvarchar(max)),'') != ISNULL(CAST(New.Atvevo_Nev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'LezarasDatuma' as ColumnName,               cast(Old.LezarasDatuma as nvarchar(99)) as OldValue,
               cast(New.LezarasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_JegyzekekHistory Old
         inner join MIG_JegyzekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.LezarasDatuma as nvarchar(max)),'') != ISNULL(CAST(New.LezarasDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SztornozasDat' as ColumnName,               cast(Old.SztornozasDat as nvarchar(99)) as OldValue,
               cast(New.SztornozasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_JegyzekekHistory Old
         inner join MIG_JegyzekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SztornozasDat as nvarchar(max)),'') != ISNULL(CAST(New.SztornozasDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Nev' as ColumnName,               cast(Old.Nev as nvarchar(99)) as OldValue,
               cast(New.Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_JegyzekekHistory Old
         inner join MIG_JegyzekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Nev as nvarchar(max)),'') != ISNULL(CAST(New.Nev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VegrehajtasDatuma' as ColumnName,               cast(Old.VegrehajtasDatuma as nvarchar(99)) as OldValue,
               cast(New.VegrehajtasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_JegyzekekHistory Old
         inner join MIG_JegyzekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.VegrehajtasDatuma as nvarchar(max)),'') != ISNULL(CAST(New.VegrehajtasDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               cast(Old.Allapot as nvarchar(99)) as OldValue,
               cast(New.Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_JegyzekekHistory Old
         inner join MIG_JegyzekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Allapot as nvarchar(max)),'') != ISNULL(CAST(New.Allapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot_Nev' as ColumnName,               cast(Old.Allapot_Nev as nvarchar(99)) as OldValue,
               cast(New.Allapot_Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_JegyzekekHistory Old
         inner join MIG_JegyzekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Allapot_Nev as nvarchar(max)),'') != ISNULL(CAST(New.Allapot_Nev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VegrehajtasKezdoDatuma' as ColumnName,               cast(Old.VegrehajtasKezdoDatuma as nvarchar(99)) as OldValue,
               cast(New.VegrehajtasKezdoDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_JegyzekekHistory Old
         inner join MIG_JegyzekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.VegrehajtasKezdoDatuma as nvarchar(max)),'') != ISNULL(CAST(New.VegrehajtasKezdoDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MegsemmisitesDatuma' as ColumnName,               cast(Old.MegsemmisitesDatuma as nvarchar(99)) as OldValue,
               cast(New.MegsemmisitesDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from MIG_JegyzekekHistory Old
         inner join MIG_JegyzekekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MegsemmisitesDatuma as nvarchar(max)),'') != ISNULL(CAST(New.MegsemmisitesDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go