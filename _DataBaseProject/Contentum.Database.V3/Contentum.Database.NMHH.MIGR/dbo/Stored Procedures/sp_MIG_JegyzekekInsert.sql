
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_JegyzekekInsert')
            and   type = 'P')
   drop procedure sp_MIG_JegyzekekInsert
go

create procedure sp_MIG_JegyzekekInsert    
                @Id      uniqueidentifier = null,    
                               @Tipus     char(1)  = null,
                @Csoport_Id_Felelos     uniqueidentifier  = null,
                @Felelos_Nev     nvarchar(400)  = null,
                @FelhasznaloCsoport_Id_Vegrehaj     uniqueidentifier  = null,
                @Vegrehajto_Nev     nvarchar(400)  = null,
                @Atvevo_Nev     nvarchar(400)  = null,
                @LezarasDatuma     datetime  = null,
                @SztornozasDat     datetime  = null,
                @Nev     Nvarchar(100)  = null,
                @VegrehajtasDatuma     datetime  = null,
                @Allapot     nvarchar(64)  = null,
                @Allapot_Nev     nvarchar(400)  = null,
                @VegrehajtasKezdoDatuma     datetime  = null,
                @MegsemmisitesDatuma     datetime  = null,
                @Ver     int  = null,
                @Note     nvarchar(400)  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Tipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Tipus'
            SET @insertValues = @insertValues + ',@Tipus'
         end 
       
         if @Csoport_Id_Felelos is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Felelos'
            SET @insertValues = @insertValues + ',@Csoport_Id_Felelos'
         end 
       
         if @Felelos_Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Felelos_Nev'
            SET @insertValues = @insertValues + ',@Felelos_Nev'
         end 
       
         if @FelhasznaloCsoport_Id_Vegrehaj is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Vegrehaj'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Vegrehaj'
         end 
       
         if @Vegrehajto_Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Vegrehajto_Nev'
            SET @insertValues = @insertValues + ',@Vegrehajto_Nev'
         end 
       
         if @Atvevo_Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Atvevo_Nev'
            SET @insertValues = @insertValues + ',@Atvevo_Nev'
         end 
       
         if @LezarasDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',LezarasDatuma'
            SET @insertValues = @insertValues + ',@LezarasDatuma'
         end 
       
         if @SztornozasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SztornozasDat'
            SET @insertValues = @insertValues + ',@SztornozasDat'
         end 
       
         if @Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Nev'
            SET @insertValues = @insertValues + ',@Nev'
         end 
       
         if @VegrehajtasDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',VegrehajtasDatuma'
            SET @insertValues = @insertValues + ',@VegrehajtasDatuma'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @Allapot_Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot_Nev'
            SET @insertValues = @insertValues + ',@Allapot_Nev'
         end 
       
         if @VegrehajtasKezdoDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',VegrehajtasKezdoDatuma'
            SET @insertValues = @insertValues + ',@VegrehajtasKezdoDatuma'
         end 
       
         if @MegsemmisitesDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',MegsemmisitesDatuma'
            SET @insertValues = @insertValues + ',@MegsemmisitesDatuma'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into MIG_Jegyzekek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Tipus char(1),@Csoport_Id_Felelos uniqueidentifier,@Felelos_Nev nvarchar(400),@FelhasznaloCsoport_Id_Vegrehaj uniqueidentifier,@Vegrehajto_Nev nvarchar(400),@Atvevo_Nev nvarchar(400),@LezarasDatuma datetime,@SztornozasDat datetime,@Nev Nvarchar(100),@VegrehajtasDatuma datetime,@Allapot nvarchar(64),@Allapot_Nev nvarchar(400),@VegrehajtasKezdoDatuma datetime,@MegsemmisitesDatuma datetime,@Ver int,@Note nvarchar(400),@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Tranz_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Tipus = @Tipus,@Csoport_Id_Felelos = @Csoport_Id_Felelos,@Felelos_Nev = @Felelos_Nev,@FelhasznaloCsoport_Id_Vegrehaj = @FelhasznaloCsoport_Id_Vegrehaj,@Vegrehajto_Nev = @Vegrehajto_Nev,@Atvevo_Nev = @Atvevo_Nev,@LezarasDatuma = @LezarasDatuma,@SztornozasDat = @SztornozasDat,@Nev = @Nev,@VegrehajtasDatuma = @VegrehajtasDatuma,@Allapot = @Allapot,@Allapot_Nev = @Allapot_Nev,@VegrehajtasKezdoDatuma = @VegrehajtasKezdoDatuma,@MegsemmisitesDatuma = @MegsemmisitesDatuma,@Ver = @Ver,@Note = @Note,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Tranz_id = @Tranz_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'MIG_Jegyzekek',@ResultUid
					,'MIG_JegyzekekHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
go

/* használt errorcode-ok:
	50301:
	50302: 'Org' oszlop értéke nem lehet NULL
*/
