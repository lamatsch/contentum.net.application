

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_MIG_JegyzekekUpdate')
            and   type = 'P')
   drop procedure sp_MIG_JegyzekekUpdate
go

create procedure sp_MIG_JegyzekekUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Tipus     char(1)  = null,         
             @Csoport_Id_Felelos     uniqueidentifier  = null,         
             @Felelos_Nev     nvarchar(400)  = null,         
             @FelhasznaloCsoport_Id_Vegrehaj     uniqueidentifier  = null,         
             @Vegrehajto_Nev     nvarchar(400)  = null,         
             @Atvevo_Nev     nvarchar(400)  = null,         
             @LezarasDatuma     datetime  = null,         
             @SztornozasDat     datetime  = null,         
             @Nev     Nvarchar(100)  = null,         
             @VegrehajtasDatuma     datetime  = null,         
             @Allapot     nvarchar(64)  = null,         
             @Allapot_Nev     nvarchar(400)  = null,         
             @VegrehajtasKezdoDatuma     datetime  = null,         
             @MegsemmisitesDatuma     datetime  = null,         
             @Ver     int  = null,         
             @Note     nvarchar(400)  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Tipus     char(1)         
     DECLARE @Act_Csoport_Id_Felelos     uniqueidentifier         
     DECLARE @Act_Felelos_Nev     nvarchar(400)         
     DECLARE @Act_FelhasznaloCsoport_Id_Vegrehaj     uniqueidentifier         
     DECLARE @Act_Vegrehajto_Nev     nvarchar(400)         
     DECLARE @Act_Atvevo_Nev     nvarchar(400)         
     DECLARE @Act_LezarasDatuma     datetime         
     DECLARE @Act_SztornozasDat     datetime         
     DECLARE @Act_Nev     Nvarchar(100)         
     DECLARE @Act_VegrehajtasDatuma     datetime         
     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_Allapot_Nev     nvarchar(400)         
     DECLARE @Act_VegrehajtasKezdoDatuma     datetime         
     DECLARE @Act_MegsemmisitesDatuma     datetime         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     nvarchar(400)         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Tipus = Tipus,
     @Act_Csoport_Id_Felelos = Csoport_Id_Felelos,
     @Act_Felelos_Nev = Felelos_Nev,
     @Act_FelhasznaloCsoport_Id_Vegrehaj = FelhasznaloCsoport_Id_Vegrehaj,
     @Act_Vegrehajto_Nev = Vegrehajto_Nev,
     @Act_Atvevo_Nev = Atvevo_Nev,
     @Act_LezarasDatuma = LezarasDatuma,
     @Act_SztornozasDat = SztornozasDat,
     @Act_Nev = Nev,
     @Act_VegrehajtasDatuma = VegrehajtasDatuma,
     @Act_Allapot = Allapot,
     @Act_Allapot_Nev = Allapot_Nev,
     @Act_VegrehajtasKezdoDatuma = VegrehajtasKezdoDatuma,
     @Act_MegsemmisitesDatuma = MegsemmisitesDatuma,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Tranz_id = Tranz_id
from MIG_Jegyzekek
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  

	   IF @UpdatedColumns.exist('/root/Tipus')=1
         SET @Act_Tipus = @Tipus
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1
         SET @Act_Csoport_Id_Felelos = @Csoport_Id_Felelos
   IF @UpdatedColumns.exist('/root/Felelos_Nev')=1
         SET @Act_Felelos_Nev = @Felelos_Nev
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Vegrehaj')=1
         SET @Act_FelhasznaloCsoport_Id_Vegrehaj = @FelhasznaloCsoport_Id_Vegrehaj
   IF @UpdatedColumns.exist('/root/Vegrehajto_Nev')=1
         SET @Act_Vegrehajto_Nev = @Vegrehajto_Nev
   IF @UpdatedColumns.exist('/root/Atvevo_Nev')=1
         SET @Act_Atvevo_Nev = @Atvevo_Nev
   IF @UpdatedColumns.exist('/root/LezarasDatuma')=1
         SET @Act_LezarasDatuma = @LezarasDatuma
   IF @UpdatedColumns.exist('/root/SztornozasDat')=1
         SET @Act_SztornozasDat = @SztornozasDat
   IF @UpdatedColumns.exist('/root/Nev')=1
         SET @Act_Nev = @Nev
   IF @UpdatedColumns.exist('/root/VegrehajtasDatuma')=1
         SET @Act_VegrehajtasDatuma = @VegrehajtasDatuma
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/Allapot_Nev')=1
         SET @Act_Allapot_Nev = @Allapot_Nev
   IF @UpdatedColumns.exist('/root/VegrehajtasKezdoDatuma')=1
         SET @Act_VegrehajtasKezdoDatuma = @VegrehajtasKezdoDatuma
   IF @UpdatedColumns.exist('/root/MegsemmisitesDatuma')=1
         SET @Act_MegsemmisitesDatuma = @MegsemmisitesDatuma
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE MIG_Jegyzekek
SET
     Tipus = @Act_Tipus,
     Csoport_Id_Felelos = @Act_Csoport_Id_Felelos,
     Felelos_Nev = @Act_Felelos_Nev,
     FelhasznaloCsoport_Id_Vegrehaj = @Act_FelhasznaloCsoport_Id_Vegrehaj,
     Vegrehajto_Nev = @Act_Vegrehajto_Nev,
     Atvevo_Nev = @Act_Atvevo_Nev,
     LezarasDatuma = @Act_LezarasDatuma,
     SztornozasDat = @Act_SztornozasDat,
     Nev = @Act_Nev,
     VegrehajtasDatuma = @Act_VegrehajtasDatuma,
     Allapot = @Act_Allapot,
     Allapot_Nev = @Act_Allapot_Nev,
     VegrehajtasKezdoDatuma = @Act_VegrehajtasKezdoDatuma,
     MegsemmisitesDatuma = @Act_MegsemmisitesDatuma,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Tranz_id = @Act_Tranz_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'MIG_Jegyzekek',@Id
					,'MIG_JegyzekekHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

go

/* használt errorcode-ok:
	50401:
	50402: 'Ver' oszlop értéke nem egyezik a meglévő rekordéval
   50403: érvényesség már lejárt a rekordra
	50499: rekord zárolva
*/
