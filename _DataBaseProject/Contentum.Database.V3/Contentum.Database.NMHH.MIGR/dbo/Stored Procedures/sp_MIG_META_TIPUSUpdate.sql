﻿
create procedure sp_MIG_META_TIPUSUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @TIPUS     nvarchar(10)  = null,         
             @CIMKE     nvarchar(255)  = null,         
             @Ver     int  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_TIPUS     nvarchar(10)         
     DECLARE @Act_CIMKE     nvarchar(255)         
     DECLARE @Act_Ver     int           
  
set nocount on

select 
     @Act_TIPUS = TIPUS,
     @Act_CIMKE = CIMKE,
     @Act_Ver = Ver
from MIG_META_TIPUS
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  

	   IF @UpdatedColumns.exist('/root/TIPUS')=1
         SET @Act_TIPUS = @TIPUS
   IF @UpdatedColumns.exist('/root/CIMKE')=1
         SET @Act_CIMKE = @CIMKE   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE MIG_META_TIPUS
SET
     TIPUS = @Act_TIPUS,
     CIMKE = @Act_CIMKE,
     Ver = @Act_Ver
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end



--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH