﻿CREATE procedure [dbo].[sp_MIG_TobbesGet]
       @Id uniqueidentifier,
       @ExecutorUserId uniqueidentifier = NULL  -- ezt egyelőre nem használjuk!!!

/*
Használata például:

exec sp_MIG_TobbesGet
     @Id      = '14543938-3344-4CE8-9FCF-2772F1049DD8'
*/

as

begin

BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

  SET @sqlcmd = 
  'select ' + 
  '
       MIG_Tobbes.Id,
       MIG_Tobbes.UI_SAV,
       MIG_Tobbes.UI_YEAR,
       MIG_Tobbes.UI_NUM,
       MIG_Tobbes.ALNO,
       MIG_Tobbes.NEV,
       MIG_Tobbes.IRSZ,
       MIG_Tobbes.UTCA,
       MIG_Tobbes.HSZ,
       MIG_Tobbes.IRSZ_PLUSS,
       MIG_Tobbes.MIG_Alszam_Id,
       MIG_Tobbes.MIG_Varos_Id,
       MIG_Tobbes.MIG_Foszam_Id,
       MIG_Tobbes.MIG_Sav_Id
  from MIG_Tobbes as MIG_Tobbes
 where MIG_Tobbes.Id = @Id'

	print @sqlcmd  -- teszt kiirás

	exec sp_executesql @sqlcmd, N'@Id uniqueidentifier',@Id = @Id
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY


BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1
		RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH

end