﻿create procedure [dbo].[sp_MIG_UgyUgyiratokForElszamoltatasiJkSSRS]

  @Where			nvarchar(MAX) = '',
  @OrderBy			nvarchar(400) = ' order by Foszam_Merge',
  @ExecutorUserId	nvarchar(400),
  @TopRow NVARCHAR(5) = '',
  @Alszam bit = 0

as

begin

BEGIN TRY

DECLARE @sqlcmd nvarchar(MAX)

SET @sqlcmd = 'select 
					dbo.fn_GetMIGFoszam(MIG_Foszam.Id) as Foszam_Merge,
					SubString (isnull(try_convert(varchar,MIG_Foszam.Ugy_kezdete,126),''''), 1, 10) as LetrehozasIdo_Rovid,
					'''' as NevSTR_Ugyindito,
					'''' as CimSTR_Ugyindito,
					MIG_Foszam.Memo as Targy,
					'''' as KRT_Csoportok_Orzo_Nev,
					'''' as PotlistaOrzoNev,
					'''' as UgyTipus_Nev,
					Ugyirat_helye = 
					case MIG_Foszam.UGYHOL
						WHEN ''1'' THEN ''Skontró''
						WHEN ''2'' THEN ''Irattárban''
						WHEN ''3'' THEN ''Osztályon''
						WHEN ''4'' THEN ''Köztisztviselő''
						WHEN ''5'' THEN ''Bíróság''
						WHEN ''6'' THEN ''Ügyészség''
						WHEN ''7'' THEN ''Vezetőség''
						WHEN ''8'' THEN ''Közjegyzőnél''
						WHEN ''9'' THEN ''Főpolgármesteri hivatal''
						WHEN ''B'' THEN ''Sztornózott''
						WHEN ''V'' THEN ''Selejtezésre vár''
						WHEN ''S'' THEN ''Selejtezett''
						WHEN ''I'' THEN ''Irattárból elkért''
						WHEN ''E'' THEN ''Skontróból elkért''
						WHEN ''L'' THEN ''Lomtár''
						WHEN ''0'' THEN ''Irattárba küldött''
						WHEN ''T'' THEN ''Levéltár''
						WHEN ''J'' THEN ''Jegyzék''
						WHEN ''Z'' THEN ''Lezárt jegyzék''
						WHEN ''A'' THEN ''Egyéb szervezetnek átadott''
						WHEN ''C'' THEN ''Szerelt''	
					END,
                    '''' as Nev  
				from MIG_Foszam 
				join MIG_Eloado on MIG_Foszam.MIG_Eloado_Id = MIG_Eloado.Id
				'

SET @sqlcmd = @sqlcmd + 'where ' + @Where  

 PRINT @sqlcmd

exec(@sqlcmd)


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
END

