﻿CREATE TABLE [dbo].[ALXX_DBF1] (
    [UISAV]     NVARCHAR (3)   NULL,
    [UIYEAR]    NVARCHAR (4)   NULL,
    [UINUM]     NVARCHAR (6)   NULL,
    [ALNO]      NVARCHAR (4)   NULL,
    [ERK]       NVARCHAR (10)  NULL,
    [IKTAT]     NVARCHAR (10)  NULL,
    [BK]        NVARCHAR (2)   NULL,
    [BKNEV]     NVARCHAR (25)  NULL,
    [EIKOD]     NVARCHAR (1)   NULL,
    [ELINT]     NVARCHAR (10)  NULL,
    [UGYHOL]    NVARCHAR (1)   NULL,
    [IRATTARBA] NVARCHAR (10)  NULL,
    [SCONTRO]   NVARCHAR (10)  NULL,
    [ELOAD]     NVARCHAR (6)   NULL,
    [ROGZIT]    NVARCHAR (8)   NULL,
    [LHAT]      NVARCHAR (10)  NULL,
    [KIADM]     NVARCHAR (10)  NULL,
    [MEMO]      NVARCHAR (100) NULL,
    [MJ]        NVARCHAR (30)  NULL,
    [IDNUM]     NVARCHAR (20)  NULL,
    [ATDATE]    NVARCHAR (10)  NULL,
    [ATADO]     NVARCHAR (4)   NULL,
    [CIMZETT]   NVARCHAR (5)   NULL,
    [JELL]      NVARCHAR (1)   NULL,
    [ATVEVO]    NVARCHAR (5)   NULL,
    [ATVEVE]    NVARCHAR (10)  NULL,
    [IND]       INT            IDENTITY (1, 1) NOT NULL,
    [ERR_KOD]   INT            NULL
);


GO
CREATE NONCLUSTERED INDEX [ALXX_DBF1_IX]
    ON [dbo].[ALXX_DBF1]([UISAV] ASC, [UIYEAR] ASC, [UINUM] ASC, [ALNO] ASC);

