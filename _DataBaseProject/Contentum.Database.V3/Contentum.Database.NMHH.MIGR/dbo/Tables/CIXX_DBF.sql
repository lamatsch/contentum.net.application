﻿CREATE TABLE [dbo].[CIXX_DBF] (
    [EV]        NVARCHAR (4)  NULL,
    [NAME]      NVARCHAR (50) NULL,
    [KOD]       NVARCHAR (6)  NULL,
    [VALID]     NVARCHAR (1)  NULL,
    [SZERVEZET] NVARCHAR (4)  NULL,
    [WT]        NVARCHAR (5)  NULL,
    [IND]       INT           IDENTITY (1, 1) NOT NULL,
    [ERR_KOD]   INT           NULL
);


GO
CREATE NONCLUSTERED INDEX [CIXX_DBF_IX]
    ON [dbo].[CIXX_DBF]([EV] ASC, [KOD] ASC);

