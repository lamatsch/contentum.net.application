﻿CREATE TABLE [dbo].[CTXX_DBF] (
    [EV]              NVARCHAR (4)    NULL,
    [NAME]            NVARCHAR (1000) NULL,
    [KOD]             NVARCHAR (4)    NULL,
    [IRJEL]           NVARCHAR (7)    NULL,
    [UI]              NVARCHAR (4)    NULL,
    [HATOS]           NVARCHAR (10)   NULL,
    [KIADM]           NVARCHAR (1)    NULL,
    [IND]             INT             IDENTITY (1, 1) NOT NULL,
    [ERR_KOD]         INT             NULL,
    [UGYKORSZAM]      NVARCHAR (100)  NULL,
    [AGAZATI_BETUJEL] NVARCHAR (100)  NULL,
    [AGAZAT_LEIRAS]   NVARCHAR (100)  NULL,
    [MEGORZESI_IDO]   NVARCHAR (100)  NULL,
	[MEGORZESI_IDO_ERTEK]	INT				NULL,
    [MEGORZESI_IDOEGYSEG]	NVARCHAR (64)	NULL,
    [MEGORZESI_MOD]			NVARCHAR (2)	NULL
);


GO
CREATE NONCLUSTERED INDEX [CTXX_DBF_IX]
    ON [dbo].[CTXX_DBF]([EV] ASC, [KOD] ASC);

