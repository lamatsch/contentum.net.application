﻿CREATE TABLE [dbo].[MIG_Alszam] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [UI_SAV]             INT              NOT NULL,
    [UI_YEAR]            INT              NOT NULL,
    [UI_NUM]             INT              NOT NULL,
    [ALNO]               INT              NOT NULL,
    [ERK]                DATETIME         NULL,
    [IKTAT]              DATETIME         NULL,
    [BKNEV]              NVARCHAR (400)   NULL,
    [ELINT]              DATETIME         NULL,
    [UGYHOL]             NVARCHAR (3)     NULL,
    [IRATTARBA]          DATETIME         NULL,
    [SCONTRO]            DATETIME         NULL,
    [ELOAD]              NVARCHAR (6)     NULL,
    [ROGZIT]             NVARCHAR (8)     NULL,
    [MJ]                 NVARCHAR (200)   NULL,
    [IDNUM]              NVARCHAR (20)    NULL,
    [MIG_Foszam_Id]      UNIQUEIDENTIFIER NULL,
    [MIG_Eloado_Id]      UNIQUEIDENTIFIER NULL,
    [MIG_Sav_Id]         UNIQUEIDENTIFIER NULL,
    [MIG_Forras_id]      INT              NULL,
    [Adathordozo_tipusa] NVARCHAR (64)    NULL,
    [Prioritas]          NVARCHAR (10)    NULL,
    [Irattipus]          NVARCHAR (64)    NULL,
    [Feladat]            NVARCHAR (255)    NULL,
    [Szervezet]          NVARCHAR (100)   NULL,
    [Dokumentum]         NVARCHAR (1000)  NULL,
    CONSTRAINT [MIG_Alszam_PK] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [MIG_Alszam_UI]
    ON [dbo].[MIG_Alszam]([UI_SAV] ASC, [UI_YEAR] ASC, [UI_NUM] ASC, [ALNO] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Alszam_IX_MIG_Sav_Id]
    ON [dbo].[MIG_Alszam]([MIG_Sav_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Alszam_IX_MIG_Foszam_Id]
    ON [dbo].[MIG_Alszam]([MIG_Foszam_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Alszam_IX_MIG_Eloado_Id]
    ON [dbo].[MIG_Alszam]([MIG_Eloado_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Alszam_IX_IDNUM]
    ON [dbo].[MIG_Alszam]([IDNUM] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Alszam_IX_BKNEV]
    ON [dbo].[MIG_Alszam]([BKNEV] ASC);

