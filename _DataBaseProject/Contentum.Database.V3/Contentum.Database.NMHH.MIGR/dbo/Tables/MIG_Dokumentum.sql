﻿CREATE TABLE [dbo].[MIG_Dokumentum] (
    [Id]                    UNIQUEIDENTIFIER NOT NULL,
    [FajlNev]               NVARCHAR (400)   NOT NULL,
    [Tipus]                 NVARCHAR (100)   NOT NULL,
    [Formatum]              NVARCHAR (64)    NOT NULL,
    [Leiras]                NVARCHAR (400)   NULL,
    [External_Source]       NVARCHAR (100)   NOT NULL,
    [External_Link]         NVARCHAR (400)   NOT NULL,
    [BarCode]               NVARCHAR (100)   NULL,
    [Allapot]               NVARCHAR (64)    NULL,
    [ElektronikusAlairas]   NVARCHAR (64)    NULL,
    [AlairasFelulvizsgalat] DATETIME         NULL,
    [MIG_Alszam_Id]         UNIQUEIDENTIFIER NOT NULL,
    [Ver]                   INT              NULL,
	[ErvVege]				DATETIME		 NULL,
    CONSTRAINT [PK_MIG_DOKUMENTUM] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MIG_DOKU_REFERENCE_MIG_ALSZ] FOREIGN KEY ([MIG_Alszam_Id]) REFERENCES [dbo].[MIG_Alszam] ([Id])
);


GO

CREATE INDEX IX_MIG_Dokumentumok_ErvVege on MIG_Dokumentum (
	ErvVege ASC
	)

EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Az alszámokhoz migrált dokumentumok', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MIG_Dokumentum';

