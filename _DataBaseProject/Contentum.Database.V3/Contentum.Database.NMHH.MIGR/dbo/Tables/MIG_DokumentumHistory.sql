﻿CREATE TABLE [dbo].[MIG_DokumentumHistory] (
    [HistoryId]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [FajlNev]               NVARCHAR (400)   NULL,
    [Tipus]                 NVARCHAR (100)   NULL,
    [Formatum]              NVARCHAR (64)    NULL,
    [Leiras]                NVARCHAR (400)   NULL,
    [External_Source]       NVARCHAR (100)   NULL,
    [External_Link]         NVARCHAR (400)   NULL,
    [BarCode]               NVARCHAR (100)   NULL,
    [Allapot]               NVARCHAR (64)    NULL,
    [ElektronikusAlairas]   NVARCHAR (64)    NULL,
    [AlairasFelulvizsgalat] DATETIME         NULL,
    [MIG_Alszam_Id]         UNIQUEIDENTIFIER NULL,
    [Ver]                   INT              NULL,
	[ErvVege]				DATETIME		 NULL,
    PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MIG_DokumentumHistory_ID_VER]
    ON [dbo].[MIG_DokumentumHistory]([Id] ASC, [Ver] ASC);

