﻿CREATE TABLE [dbo].[MIG_Eloado] (
    [Id]   UNIQUEIDENTIFIER NOT NULL,
    [EV]   INT              NOT NULL,
    [KOD]  NVARCHAR (6)     NOT NULL,
    [NAME] NVARCHAR (50)    NOT NULL,
    CONSTRAINT [MIG_Eloado_PK] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [MIG_Eloado_UI]
    ON [dbo].[MIG_Eloado]([EV] ASC, [KOD] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Eloado_IX_NAME]
    ON [dbo].[MIG_Eloado]([NAME] ASC);

