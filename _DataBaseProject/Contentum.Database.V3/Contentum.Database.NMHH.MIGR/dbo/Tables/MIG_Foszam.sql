﻿CREATE TABLE [dbo].[MIG_Foszam] (
    [Id]                        UNIQUEIDENTIFIER NOT NULL,
    [UI_SAV]                    INT              NOT NULL,
    [UI_YEAR]                   INT              NOT NULL,
    [UI_NUM]                    INT              NOT NULL,
    [UI_NAME]                   NVARCHAR (1000)  NULL,
    [UI_IRSZ]                   NVARCHAR (10)    NULL,
    [UI_UTCA]                   NVARCHAR (25)    NULL,
    [UI_HSZ]                    NVARCHAR (12)    NULL,
    [UI_HRSZ]                   NVARCHAR (20)    NULL,
    [UI_TYPE]                   NVARCHAR (4)     NULL,
    [UI_IRJ]                    NVARCHAR (6)     NULL,
    [UI_PERS]                   NVARCHAR (6)     NULL,
    [UI_OT_ID]                  NVARCHAR (64)    NULL,
    [MEMO]                      NVARCHAR (1000)  NULL,
    [IRSZ_PLUSS]                INT              NULL,
    [IRJ2000]                   NVARCHAR (20)    NULL,
    [Conc]                      NVARCHAR (255)   NULL,
    [Selejtezve]                INT              NULL,
    [Selejtezes_Datuma]         DATETIME         NULL,
    [Csatolva_Rendszer]         INT              NULL,
    [Csatolva_Id]               UNIQUEIDENTIFIER NULL,
    [MIG_Sav_Id]                UNIQUEIDENTIFIER NULL,
    [MIG_Varos_Id]              UNIQUEIDENTIFIER NULL,
    [MIG_IktatasTipus_Id]       UNIQUEIDENTIFIER NULL,
    [MIG_Eloado_Id]             UNIQUEIDENTIFIER NULL,
    [Edok_Utoirat_Id]           UNIQUEIDENTIFIER NULL,
    [Edok_Utoirat_Azon]         NVARCHAR (100)   NULL,
    [UGYHOL]                    NVARCHAR (1)     NULL,
    [IRATTARBA]                 DATETIME         NULL,
    [SCONTRO]                   DATETIME         NULL,
    [Edok_Ugyintezo_Csoport_Id] UNIQUEIDENTIFIER NULL,
    [Edok_Ugyintezo_Nev]        NVARCHAR (100)   NULL,
    [Ver]                       INT              CONSTRAINT [DF__MIG_Foszam__Ver__1B0907CE] DEFAULT ((1)) NULL,
    [EdokSav]                   NVARCHAR (20)    NULL,
    [MIG_Forras_id]             INT              NULL,
    [Ugyirat_tipus]             NVARCHAR (20)    NULL,
    [Eltelt_napok]              INT              NULL,
    [Ugy_kezdete]               DATETIME         NULL,
    [Ugykezeles_modja]          NVARCHAR (64)    NULL,
    [Egyeb_adat2]               NVARCHAR (100)   NULL,
    [Munkanapos]                NVARCHAR (1)     NULL,
    [Hatarido]                  DATETIME         NULL,
    [Feladat]                   NVARCHAR (64)    NULL,
    [Targyszavak]               NVARCHAR (1000)  NULL,
    [Kulso_eloirat]             NVARCHAR (125)   NULL,
    [Felfuggesztve]             NVARCHAR (1)     NULL,
    [Szervezet]                 NVARCHAR (100)   NULL,
    [Sztorno_datum]             DATETIME         NULL,
    [IrattarbolKikeroNev]       NVARCHAR (100)   NULL,
    [IrattarbolKikeres_Datuma]  DATETIME         NULL,
    [MegorzesiIdo]              DATETIME         NULL,
    [IrattarId] UNIQUEIDENTIFIER NULL,
	IrattariHely nvarchar(100) NULL,
    CONSTRAINT [MIG_Foszam_PK] PRIMARY KEY CLUSTERED ([Id] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [MIG_Foszam_UI]
    ON [dbo].[MIG_Foszam]([UI_SAV] ASC, [UI_YEAR] ASC, [UI_NUM] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Foszam_IX_UI_NAME]
    ON [dbo].[MIG_Foszam]([UI_NAME] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Foszam_IX_UGYHOL_SCONTRO_IRATTARBA]
    ON [dbo].[MIG_Foszam]([UGYHOL] ASC, [SCONTRO] ASC, [IRATTARBA] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Foszam_IX_MIG_Varos_Id]
    ON [dbo].[MIG_Foszam]([MIG_Varos_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Foszam_IX_MIG_Sav_Id]
    ON [dbo].[MIG_Foszam]([MIG_Sav_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Foszam_IX_MIG_IktatasTipus_Id]
    ON [dbo].[MIG_Foszam]([MIG_IktatasTipus_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Foszam_IX_MIG_Eloado_Id]
    ON [dbo].[MIG_Foszam]([MIG_Eloado_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Foszam_IX_Csatolva_Id]
    ON [dbo].[MIG_Foszam]([Csatolva_Id] ASC);

