﻿CREATE TABLE [dbo].[MIG_IktatasTipus] (
    [Id]    UNIQUEIDENTIFIER NOT NULL,
    [EV]    INT              NOT NULL,
    [KOD]   NVARCHAR (4)     NOT NULL,
    [NAME]  NVARCHAR (50)    NOT NULL,
    [IRJEL] NVARCHAR (7)     NULL,
    CONSTRAINT [MIG_IktatasTipus_PK] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [MIG_IktatasTipus_UI]
    ON [dbo].[MIG_IktatasTipus]([EV] ASC, [KOD] ASC);

