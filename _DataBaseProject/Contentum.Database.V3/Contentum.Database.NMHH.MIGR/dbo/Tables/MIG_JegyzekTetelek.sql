﻿CREATE TABLE [dbo].[MIG_JegyzekTetelek] (
    [Id]                UNIQUEIDENTIFIER CONSTRAINT [DF__MIG_JegyzekT__Id__49C3F6B7] DEFAULT (newsequentialid()) NOT NULL,
    [Jegyzek_Id]        UNIQUEIDENTIFIER NULL,
    [MIG_Foszam_Id]     UNIQUEIDENTIFIER NOT NULL,
    [UGYHOL]            NVARCHAR (1)     NULL,
    [AtvevoIktatoszama] NVARCHAR (100)   NULL,
    [Megjegyzes]        NVARCHAR (400)   NULL,
    [Azonosito]         NVARCHAR (100)   NULL,
    [SztornozasDat]     DATETIME         NULL,
	[AtadasDatuma]		DATETIME		 NULL,
    [Ver]               INT              CONSTRAINT [DF__MIG_Jegyzek__Ver__4AB81AF0] DEFAULT ((1)) NULL,
    [Note]              NVARCHAR (400)   NULL,
    [Letrehozo_id]      UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]     DATETIME         CONSTRAINT [DF__MIG_Jegyz__Letre__4BAC3F29] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]       UNIQUEIDENTIFIER NULL,
    [ModositasIdo]      DATETIME         NULL,
    [Tranz_id]          UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MIG_JEGYZEKTETELEK] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MIG_JEGY_MIG_FOSZA_MIG_FOSZ] FOREIGN KEY ([MIG_Foszam_Id]) REFERENCES [dbo].[MIG_Foszam] ([Id]),
    CONSTRAINT [FK_MIG_JEGY_MIG_JEGYZ_MIG_JEGY] FOREIGN KEY ([Jegyzek_Id]) REFERENCES [dbo].[MIG_Jegyzekek] ([Id])
);

