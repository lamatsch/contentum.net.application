﻿CREATE TABLE [dbo].[MIG_JegyzekTetelekHistory] (
    [HistoryId]             UNIQUEIDENTIFIER CONSTRAINT [DF__MIG_Jegyz__Histo__4CA06362] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [Jegyzek_Id]            UNIQUEIDENTIFIER NULL,
    [MIG_Foszam_Id]         UNIQUEIDENTIFIER NULL,
    [UGYHOL]                NVARCHAR (1)     NULL,
    [AtvevoIktatoszama]     NVARCHAR (100)   NULL,
    [Megjegyzes]            NVARCHAR (400)   NULL,
    [Azonosito]             NVARCHAR (100)   NULL,
    [SztornozasDat]         DATETIME         NULL,
	[AtadasDatuma]			DATETIME		 NULL,
    [Ver]                   INT              NULL,
    [Note]                  NVARCHAR (400)   NULL,
    [Letrehozo_id]          UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]         DATETIME         NULL,
    [Modosito_id]           UNIQUEIDENTIFIER NULL,
    [ModositasIdo]          DATETIME         NULL,
    [Tranz_id]              UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK__MIG_Jegy__4D7B4ABD2C3393D0] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MIG_JegyzekTetelekHistory_ID_VER]
    ON [dbo].[MIG_JegyzekTetelekHistory]([Id] ASC, [Ver] ASC);

