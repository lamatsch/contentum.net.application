﻿CREATE TABLE [dbo].[MIG_Jegyzekek] (
    [Id]                             UNIQUEIDENTIFIER CONSTRAINT [DF__MIG_Jegyzeke__Id__440B1D61] DEFAULT (newsequentialid()) NOT NULL,
    [Tipus]                          CHAR (1)         NULL,
    [Csoport_Id_Felelos]             UNIQUEIDENTIFIER NULL,
    [Felelos_Nev]                    NVARCHAR (400)   NULL,
    [FelhasznaloCsoport_Id_Vegrehaj] UNIQUEIDENTIFIER NULL,
    [Vegrehajto_Nev]                 NVARCHAR (400)   NULL,
    [Atvevo_Nev]                     NVARCHAR (400)   NULL,
    [LezarasDatuma]                  DATETIME         CONSTRAINT [DF__MIG_Jegyz__Lezar__44FF419A] DEFAULT (NULL) NULL,
    [SztornozasDat]                  DATETIME         NULL,
    [Nev]                            NVARCHAR (100)   NULL,
    [VegrehajtasDatuma]              DATETIME         CONSTRAINT [DF__MIG_Jegyz__Vegre__45F365D3] DEFAULT (NULL) NULL,
    [Allapot]						 NVARCHAR (64)	  NULL,
	[Allapot_Nev]					 NVARCHAR (400)	  NULL,
	[VegrehajtasKezdoDatuma]		 DATETIME		  NULL,
	[MegsemmisitesDatuma]  			 DATETIME		  NULL,
	[Ver]                            INT              CONSTRAINT [DF__MIG_Jegyzek__Ver__46E78A0C] DEFAULT ((1)) NULL,
    [Note]                           NVARCHAR (400)   NULL,
    [Letrehozo_id]                   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                  DATETIME         CONSTRAINT [DF__MIG_Jegyz__Letre__47DBAE45] DEFAULT (getdate()) NOT NULL,
    [Modosito_id]                    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                   DATETIME         NULL,
    [Tranz_id]                       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MIG_JEGYZEKEK] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [MIG_Jegyzekek_UK]
    ON [dbo].[MIG_Jegyzekek]([Tipus] ASC, [Csoport_Id_Felelos] ASC);

