﻿CREATE TABLE [dbo].[MIG_JegyzekekHistory] (
    [HistoryId]                      UNIQUEIDENTIFIER CONSTRAINT [DF__MIG_Jegyz__Histo__48CFD27E] DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]              INT              NULL,
    [HistoryVegrehajto_Id]           UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo]          DATETIME         NULL,
    [Id]                             UNIQUEIDENTIFIER NULL,
    [Tipus]                          CHAR (1)         NULL,
    [Csoport_Id_Felelos]             UNIQUEIDENTIFIER NULL,
    [Felelos_Nev]                    NVARCHAR (400)   NULL,
    [FelhasznaloCsoport_Id_Vegrehaj] UNIQUEIDENTIFIER NULL,
    [Vegrehajto_Nev]                 NVARCHAR (400)   NULL,
    [Atvevo_Nev]                     NVARCHAR (400)   NULL,
    [LezarasDatuma]                  DATETIME         NULL,
    [SztornozasDat]                  DATETIME         NULL,
    [Nev]                            NVARCHAR (100)   NULL,
    [VegrehajtasDatuma]              DATETIME         NULL,
	[Allapot]						 NVARCHAR (64)	  NULL,
	[Allapot_Nev]					 NVARCHAR (400)	  NULL,
	[VegrehajtasKezdoDatuma]		 DATETIME		  NULL,
	[MegsemmisitesDatuma]  			 DATETIME		  NULL,
    [Ver]                            INT              NULL,
    [Note]                           NVARCHAR (400)   NULL,
    [Letrehozo_id]                   UNIQUEIDENTIFIER NULL,
    [LetrehozasIdo]                  DATETIME         NULL,
    [Modosito_id]                    UNIQUEIDENTIFIER NULL,
    [ModositasIdo]                   DATETIME         NULL,
    [Tranz_id]                       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK__MIG_Jegy__4D7B4ABD267ABA7A] PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MIG_JegyzekekHistory_ID_VER]
    ON [dbo].[MIG_JegyzekekHistory]([Id] ASC, [Ver] ASC);

