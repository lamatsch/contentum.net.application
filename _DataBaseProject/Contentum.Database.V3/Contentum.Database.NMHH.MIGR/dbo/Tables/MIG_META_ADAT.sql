﻿CREATE TABLE [dbo].[MIG_META_ADAT] (
    [ID]      UNIQUEIDENTIFIER NOT NULL,
    [OBJ_ID]  UNIQUEIDENTIFIER NOT NULL,
    [META_ID] UNIQUEIDENTIFIER NOT NULL,
    [ERTEK]   NVARCHAR (MAX)   NULL,
    [VER]     INT              NOT NULL,
    CONSTRAINT [PK_MIG_META_ADAT] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [MIG_META_ADAT_META_TIPUS_FK] FOREIGN KEY ([META_ID]) REFERENCES [dbo].[MIG_META_TIPUS] ([Id])
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [MIG_META_ADAT_UI]
    ON [dbo].[MIG_META_ADAT]([OBJ_ID] ASC);




GO
CREATE NONCLUSTERED INDEX [MIG_META_ADAT_IX_Obj_ID]
    ON [dbo].[MIG_META_ADAT]([OBJ_ID] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'MIG_Alszam.ID, ha META_ID -> TIPUS=''A''-ra mutat; MIG_Foszam.ID, ha META_ID -> TIPUS=''F'' -ra mutat', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MIG_META_ADAT', @level2type = N'COLUMN', @level2name = N'OBJ_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = '..., hogy a csak egyes ügyfeleknél megjelenő  másodlagos/leíró META adatokat tudjuk tárolni ...', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MIG_META_ADAT';

