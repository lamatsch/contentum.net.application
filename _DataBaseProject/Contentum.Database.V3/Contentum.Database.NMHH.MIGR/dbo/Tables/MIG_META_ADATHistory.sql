﻿CREATE TABLE [dbo].[MIG_META_ADATHistory] (
    [HistoryId]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [OBJ_ID]                UNIQUEIDENTIFIER NULL,
    [META_ID]               UNIQUEIDENTIFIER NULL,
    [ERTEK]                 NVARCHAR (MAX)   NULL,
    [Ver]                   INT              NULL,
    PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MIG_META_ADATHistory_ID_VER]
    ON [dbo].[MIG_META_ADATHistory]([Id] ASC, [Ver] ASC);

