﻿CREATE TABLE [dbo].[MIG_META_TIPUS] (
    [Id]    UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [TIPUS] NVARCHAR (10)    NOT NULL,
    [CIMKE] NVARCHAR (255)   NOT NULL,
    [Ver]   INT              NOT NULL,
    [SORSZAM] INT				 NULL, 
    CONSTRAINT [PK_MIG_META_TIPUS] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [CKC_TIPUS_MIG_META] CHECK (([TIPUS]='F' OR [TIPUS]='A') AND [TIPUS]=upper([TIPUS]))
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'A - alszám; F - főszám', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MIG_META_TIPUS', @level2type = N'COLUMN', @level2name = N'TIPUS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'másodlagos META adat típusa', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MIG_META_TIPUS';

