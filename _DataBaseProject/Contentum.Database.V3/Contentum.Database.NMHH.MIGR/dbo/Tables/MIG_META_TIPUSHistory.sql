﻿CREATE TABLE [dbo].[MIG_META_TIPUSHistory] (
    [HistoryId]             UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [HistoryMuvelet_Id]     INT              NULL,
    [HistoryVegrehajto_Id]  UNIQUEIDENTIFIER NULL,
    [HistoryVegrehajtasIdo] DATETIME         NULL,
    [Id]                    UNIQUEIDENTIFIER NULL,
    [TIPUS]                 NVARCHAR (10)    NULL,
    [CIMKE]                 NVARCHAR (255)   NULL,
    [Ver]                   INT              NULL,
    PRIMARY KEY CLUSTERED ([HistoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MIG_META_TIPUSHistory_ID_VER]
    ON [dbo].[MIG_META_TIPUSHistory]([Id] ASC, [Ver] ASC);

