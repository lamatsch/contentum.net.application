﻿CREATE TABLE [dbo].[MIG_MigralasiHiba] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [ERR_KOD]  INT              NOT NULL,
    [ERR_NAME] NVARCHAR (120)   NULL,
    CONSTRAINT [MIG_MigralasiHiba_PK] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [MIG_MigralasiHiba_UI]
    ON [dbo].[MIG_MigralasiHiba]([ERR_KOD] ASC);

