﻿CREATE TABLE [dbo].[MIG_Tobbes] (
    [Id]            UNIQUEIDENTIFIER NOT NULL,
    [UI_SAV]        INT              NOT NULL,
    [UI_YEAR]       INT              NOT NULL,
    [UI_NUM]        INT              NOT NULL,
    [ALNO]          INT              NOT NULL,
    [NEV]           NVARCHAR (30)    NULL,
    [IRSZ]          NVARCHAR (4)     NULL,
    [UTCA]          NVARCHAR (25)    NULL,
    [HSZ]           NVARCHAR (12)    NULL,
    [IRSZ_PLUSS]    INT              NULL,
    [MIG_Alszam_Id] UNIQUEIDENTIFIER NULL,
    [MIG_Varos_Id]  UNIQUEIDENTIFIER NULL,
    [MIG_Foszam_Id] UNIQUEIDENTIFIER NULL,
    [MIG_Sav_Id]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [MIG_Tobbes_PK] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [MIG_Tobbes_UI]
    ON [dbo].[MIG_Tobbes]([UI_SAV] ASC, [UI_YEAR] ASC, [UI_NUM] ASC, [ALNO] ASC, [NEV] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Tobbes_IX_MIG_Varos_Id]
    ON [dbo].[MIG_Tobbes]([MIG_Varos_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Tobbes_IX_MIG_Sav_Id]
    ON [dbo].[MIG_Tobbes]([MIG_Sav_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Tobbes_IX_MIG_Foszam_Id]
    ON [dbo].[MIG_Tobbes]([MIG_Foszam_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [MIG_Tobbes_IX_MIG_Alszam_Id]
    ON [dbo].[MIG_Tobbes]([MIG_Alszam_Id] ASC);

