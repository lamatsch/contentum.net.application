SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_Felhasznalok')
	DROP VIEW KRT_Felhasznalok

GO

create view [dbo].[KRT_Felhasznalok] as select * from [$(MAINDB)].dbo.KRT_Felhasznalok;
GO


