[Console]::OutputEncoding = [Text.UTF8Encoding]::UTF8

$FolderPath = ".\KRTParameterekScripts\"
New-Item -ItemType Directory -Force -Path  $FolderPath

# RENDSZERPARAMETER NEVE
$KrtParamNev = Read-Host -Prompt 'Rendszerparameter neve'
if ($KrtParamNev -eq $null -OR $KrtParamNev -eq "" )
	{
		Write-Error "Rendszerparameter nev megadasa kotelezo !"
		return
	}
$KrtParamNev = $KrtParamNev.ToUpper()

# RENDSZERPARAMETER KARBANTARTHATO
$KrtParamKarbantarthato = Read-Host -Prompt 'Rendszerparameter karbantarthato? 1-igen 0-Nem'
if ($KrtParamKarbantarthato -eq $null -OR $KrtParamKarbantarthato -eq "" )
	{
		$KrtParamKarbantarthato = "1"
	}

# RENDSZERPARAMETER LEIRAS
$KrtParamMegjegyzes = Read-Host -Prompt 'Rendszerparameter leirasa'

# RENDSZERPARAMETER ERTEKEK ORG ALAPJAN
$OrgFilter = Read-Host -Prompt 'Rendszerparameter erteke minden ugyfelnel megegyezik? 1-igen, 0-Nem'
if($OrgFilter -eq "1"){
	$KrtParamErtek_Org = Read-Host -Prompt 'Rendszerparameter erteke:'
	$KrtParamErtek_Org_FPH   = $KrtParamErtek_Org
	$KrtParamErtek_Org_BOPMH = $KrtParamErtek_Org
	$KrtParamErtek_Org_CSPH  = $KrtParamErtek_Org
	$KrtParamErtek_Org_NMHH  = $KrtParamErtek_Org

}else{
    $KrtParamErtek_Org_FPH   = Read-Host -Prompt 'Rendszerparameter erteke (FPH)'
	$KrtParamErtek_Org_BOPMH = Read-Host -Prompt 'Rendszerparameter erteke (BOPMH)'
	$KrtParamErtek_Org_CSPH  = Read-Host -Prompt 'Rendszerparameter erteke (CSPH)'
	$KrtParamErtek_Org_NMHH  = Read-Host -Prompt 'Rendszerparameter erteke (NMHH)'
}

# RENDSZERPARAMETER SQL TEMPLATE
$ScriptTemplate = 	
"
	---------------------------------------------------------------------
	-- RENDSZERPARAMETER = " + $KrtParamNev +"
	---------------------------------------------------------------------	
	GO

    DECLARE @ORGID				UNIQUEIDENTIFIER
	DECLARE @ID					UNIQUEIDENTIFIER
	DECLARE @NEV				NVARCHAR(400)
	DECLARE @KARBANTARTHATO		CHAR(1)
	DECLARE @NOTE				NVARCHAR(4000)

	DECLARE @ERTEK_FPH				NVARCHAR(400)
	DECLARE @ERTEK_CSPH				NVARCHAR(400)
	DECLARE @ERTEK_NMHH				NVARCHAR(400)
	DECLARE @ERTEK_BOPMH			NVARCHAR(400)

	SET @ORGID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
	SET @ID = 	 '" + (New-Guid) +"'
	SET @NEV =   '" + $KrtParamNev +"'
	SET @KARBANTARTHATO = '" + $KrtParamKarbantarthato +"'
	SET @NOTE = '" + $KrtParamMegjegyzes +"'

	SET @ERTEK_FPH = '" + $KrtParamErtek_Org_FPH +"'
	SET @ERTEK_CSPH = '" + $KrtParamErtek_Org_CSPH +"'
	SET @ERTEK_NMHH = '" + $KrtParamErtek_Org_NMHH +"'
	SET @ERTEK_BOPMH = '" + $KrtParamErtek_Org_BOPMH +"'

	IF NOT EXISTS(SELECT 1 FROM KRT_Parameterek WHERE NEV=@NEV OR ID=@ID) 
	BEGIN
		IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='FPH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_FPH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='BOPMH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_BOPMH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='NMHH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_NMHH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='CSPH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_CSPH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
	END
	GO
	---------------------------------------------------------------------
"
 
$ScriptFullPath = $FolderPath + "\"+  $KrtParamNev + ".sql"

Set-Content -Encoding UTF8 -Path $ScriptFullPath -Value $ScriptTemplate

Invoke-Item $ScriptFullPath
