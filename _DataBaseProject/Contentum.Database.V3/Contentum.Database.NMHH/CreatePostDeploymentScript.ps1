[Console]::OutputEncoding = [Text.UTF8Encoding]::UTF8
$GitBranch = (git branch --show-current)

if($GitBranch.length -eq 0){
	$ScriptName = Read-Host -Prompt 'Enter the BUG/BLG name with a short summary e.g.: BUG_1112_Uj_felhasznalok_felvetele'
}else{
	$GitBranch = $GitBranch.replace("feature/","")
	$Question = "Would you like to use the current branch name. Which is: '" + $GitBranch + "'"
	Write-host $Question  
		$Readhost = Read-Host " ( y / n ) " 
		Switch ($ReadHost) 
		 { 
		   Y {$ScriptName = $GitBranch } 
		   N {$ScriptName = Read-Host -Prompt 'Enter the BUG/BLG name with a short summary e.g.: BUG_1112_Uj_felhasznalok_felvetele'} 
		  } 
}

$FolderName = get-date -Format yyyy-M
$FolderPath = ".\PostDeploymentScripts\" + $FolderName
New-Item -ItemType Directory -Force -Path  $FolderPath


$OrgFilter = Read-Host -Prompt 'If you want this script to work only one org. Please enter one of the following All,NMHH,BOPHM,FPH,CSPH'
$ScriptTemplate = "\\n GO"

 
if($OrgFilter -eq "All"){
$ScriptTemplate = "PRINT 'Executing: " + $ScriptName + 
 "'
	--WRITE YOUR SCRIPT HERE. PREPARE TO BE RUN MULTIPLE TIMES!
 GO"
}else{
$ScriptTemplate = 	"DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD ='" + $OrgFilter +"')
	BEGIN 
	PRINT 'Executing: " + $ScriptName + 
	"'
	--WRITE YOUR SCRIPT HERE. PREPARE TO BE RUN MULTIPLE TIMES!
	END
GO"
}
 
 
$ScriptFullPath = $FolderPath + "\"+  $ScriptName + ".sql"

 
Set-Content -Encoding UTF8 -Path $ScriptFullPath -Value $ScriptTemplate

#Needed when using with database project
#$ScriptReference = ":r"+  $ScriptFullPath
#Add-Content -Path  ".\PostDeployment.sql" -Value $ScriptReference

Invoke-Item $ScriptFullPath
