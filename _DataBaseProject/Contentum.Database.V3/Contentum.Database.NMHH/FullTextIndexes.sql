﻿CREATE FULLTEXT INDEX ON [dbo].[EREC_UgyUgyiratok]
    ([Targy] LANGUAGE 1033, [NevSTR_Ugyindito] LANGUAGE 1033, [Note]LANGUAGE 1033)
    KEY INDEX [EIV_PK]
    ON [Edoc_FT];

GO
CREATE FULLTEXT INDEX ON [dbo].[EREC_ObjektumTargyszavai]
    ([Targyszo] LANGUAGE 1033, [Ertek] LANGUAGE 1033, [Note] LANGUAGE 1033)
    KEY INDEX [PK_EREC_OBJEKTUMTARGYSZAVAI]
    ON [Edoc_FT];


GO
CREATE FULLTEXT INDEX ON [dbo].[EREC_KuldKuldemenyek]
    ([Targy] LANGUAGE 1033, [NevSTR_Bekuldo] LANGUAGE 1033)
    KEY INDEX [KuldKuldemenyek_PK]
    ON [Edoc_FT];


GO
CREATE FULLTEXT INDEX ON [dbo].[EREC_IraIratok]
    ([Targy] LANGUAGE 1033)
    KEY INDEX [IRA_PK]
    ON [Edoc_FT];



