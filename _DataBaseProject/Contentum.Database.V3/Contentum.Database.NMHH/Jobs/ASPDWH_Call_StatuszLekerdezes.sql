USE [msdb]
GO

IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'ASPDWH_Call_StatuszLekerdezes')
EXEC msdb.dbo.sp_delete_job @job_name=N'ASPDWH_Call_StatuszLekerdezes', @delete_unused_schedule=1
GO


/****** Object:  Job [ASPDWH_Call_StatuszLekerdezes_$(DataBaseName)]    Script Date: 04/05/2019 11:54:44 ******/
IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'ASPDWH_Call_StatuszLekerdezes_$(DataBaseName)')
EXEC msdb.dbo.sp_delete_job @job_name=N'ASPDWH_Call_StatuszLekerdezes_$(DataBaseName)', @delete_unused_schedule=1
GO

USE [msdb]
GO

/****** Object:  Job [ASPDWH_Call_StatuszLekerdezes_$(DataBaseName)]    Script Date: 04/05/2019 11:54:44 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 04/05/2019 11:54:45 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'ASPDWH_Call_StatuszLekerdezes_$(DataBaseName)', 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Call service]    Script Date: 04/05/2019 11:54:45 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Call service', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE	@return_value int

EXEC	@return_value = [sp_ASPDWH_CallWebService]
		@url = N''$(eIntegratorWebServiceUrl)''
', 
		@database_name=N'$(DataBaseName)', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Reschecdule]    Script Date: 04/05/2019 11:54:45 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Reschecdule', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'declare @utolsoStatus as nvarchar(4000)
declare @utolsoFutas as datetime
declare @yesterday as datetime
declare @tomorrow as datetime
declare @today as datetime
declare @NewDate as int
declare @NewTime as integer
declare @dtNewTime as datetime

select top 1  @utolsoStatus=m.Status from dbo.INT_Log m
where m.Modul_id=''2FA6B0F7-7C8F-40A9-ABFD-33F327E01F0E''
order by m.Sync_StartDate desc, m.LetrehozasIdo desc

select top 1  @utolsoFutas=CONVERT(date,m.UtolsoFutas) from dbo.INT_Modulok m
where m.Id=''2FA6B0F7-7C8F-40A9-ABFD-33F327E01F0E''

SELECT @yesterday = dateadd(day,datediff(day,1,GETDATE()),0)
SELECT @tomorrow = CONVERT(date, dateadd(day,1, GETDATE()))
SELECT @today = CONVERT(date, GETDATE())

if (@utolsoStatus<>''1'' OR @utolsoFutas<>@today)
BEGIN
--� wait for 10 minutes
set @dtNewTime = dateadd(mi,10,getdate())
select @NewDate = convert(integer,(replace(convert(char(10),@dtNewTime,121),''-'','''')))
, @NewTime = convert(integer,substring(replace(convert( char(25),@dtNewTime,121),'':'',''''),12,4)+''00'')
END
ELSE
BEGIN
set @dtNewTime = dateadd(mi,10,@tomorrow)
select @NewDate = convert(integer,(replace(convert(char(10),@dtNewTime,121),''-'','''')))
END

if(@utolsoStatus<>''0'')
BEGIN
	EXECUTE msdb.dbo.sp_update_jobschedule @job_name = N''ASPDWH_Call_SyncUgyirat_$(DataBaseName)'', @name = N''Syncschedule'', @enabled = 1, @freq_type = 1, @active_start_date = @NewDate,@active_start_time = @NewTime
END', 
		@database_name=N'$(DataBaseName)', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Esti', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=8, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20190403, 
		@active_end_date=99991231, 
		@active_start_time=180000, 
		@active_end_time=60000, 
		@schedule_uid=N'd03aa2f7-96f0-4936-92b5-192df6adbcb4'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


