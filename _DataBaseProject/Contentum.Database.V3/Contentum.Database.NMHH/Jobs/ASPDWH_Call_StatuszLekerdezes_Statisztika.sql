USE [msdb]
GO

IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'ASPDWH_Call_StatuszLekerdezes_Statisztika')
EXEC msdb.dbo.sp_delete_job @job_name=N'ASPDWH_Call_StatuszLekerdezes_Statisztika', @delete_unused_schedule=1
GO

/****** Object:  Job [ASPDWH_Call_StatuszLekerdezes]    Script Date: 04/05/2019 11:54:44 ******/
IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'ASPDWH_Call_StatuszLekerdezes_Statisztika_$(DataBaseName)')
EXEC msdb.dbo.sp_delete_job @job_name=N'ASPDWH_Call_StatuszLekerdezes_Statisztika_$(DataBaseName)', @delete_unused_schedule=1
GO

USE [msdb]
GO

/****** Object:  Job [ASPDWH_Call_StatuszLekerdezes]    Script Date: 04/05/2019 11:54:44 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 04/05/2019 11:54:45 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'ASPDWH_Call_StatuszLekerdezes_Statisztika_$(DataBaseName)', 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Call service]    Script Date: 04/05/2019 11:54:45 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Call service', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE	@return_value int

EXEC	@return_value = [sp_ASPDWH_CallWebService]
		@url = N''$(eIntegratorWebServiceUrl)''
', 
		@database_name=N'$(DataBaseName)', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Reschecdule]    Script Date: 04/05/2019 11:54:45 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Reschecdule', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'declare @utolsoStatus as nvarchar(4000)
declare @utolsoFutas as datetime
declare @NewDate as int
declare @NewTime as integer
declare @dtNewTime as datetime
DECLARE @lastDayOfPreviousMont DATETIME
DECLARE @firstDayOfNextMont DATETIME

select top 1  @utolsoStatus=m.Status from INT_Log m
where m.Modul_id=''73F2AC45-7127-43F4-B48D-782D3C489F9D''
order by m.Sync_StartDate desc, m.LetrehozasIdo desc

select top 1  @utolsoFutas=CONVERT(date,m.UtolsoFutas) from INT_Modulok m
where m.Id=''73F2AC45-7127-43F4-B48D-782D3C489F9D''


SET @lastDayOfPreviousMont = CONVERT(date, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1))

SET @firstDayOfNextMont = CONVERT(date, DATEADD(m, DATEDIFF(m, -1, current_timestamp), 0))

if (@utolsoStatus<>''1'' OR @utolsoFutas<@lastDayOfPreviousMont)
BEGIN
--� wait for 10 minutes
set @dtNewTime = dateadd(mi,10,getdate())
select @NewDate = convert(integer,(replace(convert(char(10),@dtNewTime,121),''-'','''')))
, @NewTime = convert(integer,substring(replace(convert( char(25),@dtNewTime,121),'':'',''''),12,4)+''00'')
END
ELSE
BEGIN
set @dtNewTime = dateadd(mi,10,@firstDayOfNextMont)
select @NewDate = convert(integer,(replace(convert(char(10),@dtNewTime,121),''-'','''')))
END

if(@utolsoStatus<>''0'')
BEGIN
	EXECUTE msdb.dbo.sp_update_jobschedule @job_name = N''ASPDWH_Call_SyncStatisztika_$(DataBaseName)'', @name = N''Syncschedule'', @enabled = 1, @freq_type = 1, @active_start_date = @NewDate,@active_start_time = @NewTime
END
', 
		@database_name=N'$(DataBaseName)', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Esti', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=8, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20190403, 
		@active_end_date=99991231, 
		@active_start_time=180000, 
		@active_end_time=60000, 
		@schedule_uid=N'41ecb56e-a364-47f9-8bf9-46e2995659ff'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


