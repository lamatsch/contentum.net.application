@echo off

set sqlcmd="sqlcmd.exe"

if "%1"=="" goto :help
if "%2"=="" goto :help
if "%3"=="" goto :help

@echo on

%sqlcmd% -S %1 -v DataBaseName="%2" eIntegratorWebServiceUrl="%3/eIntegratorWebService/INT_ASPDWH_FileService.asmx/StatuszLekerdezesUgyirat" -i ASPDWH_Call_StatuszLekerdezes.sql

@echo off

goto :EOF

:help
echo. Hasznalat:
echo.  1. parameter: szerver nev
echo.  2. parameter: adatbazis neve
echo.  3. parameter: app server url
echo.
echo. Pelda:
echo. build.bat INTCSQL2T EDOK http://edokteszt
echo. build.bat INCSQL2 EDOK http://edok

:EOF