@echo off

set sqlcmd="sqlcmd.exe"

if "%1"=="" goto :help
if "%2"=="" goto :help
if "%3"=="" goto :help

@echo on
%sqlcmd% -S %1 -v DataBase="%2" eRecordWebServiceUrl="%3" -i Job_ErkeztetoKonyvekAutomatikusLezaras.sql

@echo off

goto :EOF

:help
echo. Hasznalat:
echo.  1. parameter: szerver nev
echo.  2. parameter: adatbazis neve
echo.  3. parameter: eRecordWebService url
echo.
echo. Pelda:
echo. build.bat INTCSQL2T EDOK http://edok/eRecordWebService/
echo. build.bat INCSQL2 EDOK http://edokteszt/eRecordWebService/

:EOF