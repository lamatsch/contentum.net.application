USE [msdb]
GO

/****** Object:  Job [Job_HibasHivataliKapuVevenyekFeldolgozasa]    Script Date: 2020. 01. 15. 14:43:17 ******/
BEGIN TRANSACTION

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from $(DataBaseName).dbo.KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9')

IF @org_kod != 'BOPMH' and @org_kod != 'CSPH' and @org_kod != 'FPH'
	GOTO QuitWithRollback

DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 2020. 01. 15. 14:43:17 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)

SELECT @jobId = job_id FROM msdb.dbo.sysjobs WHERE (name = N'Job_HibasHivataliKapuVevenyekFeldolgozasa_$(DataBaseName)')
IF (@jobId IS NOT NULL)
	GOTO QuitWithRollback

PRINT 'Create job: Job_HibasHivataliKapuVevenyekFeldolgozasa_$(DataBaseName)'

EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Job_HibasHivataliKapuVevenyekFeldolgozasa_$(DataBaseName)', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'A sikertelen�l feldogozott hivatali kapur�l �rkezett vev�nyeket megpr�b�lja �jra feldoglozni.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [PostProcessFailedVevenyek]    Script Date: 2020. 01. 15. 14:43:17 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'PostProcessFailedVevenyek', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec sp_CallWebService ''$(eRecordWebServiceUrl)/EREC_eBeadvanyokService.asmx/PostProcessFailedVevenyek_Job''', 
		@database_name=N'$(DataBaseName)', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'PostProcessFailedVevenyek_Schedule', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20200115, 
		@active_end_date=99991231, 
		@active_start_time=23700, 
		@active_end_time=235959, 
		@schedule_uid=N'3c1f7313-00b4-4024-bf61-0840de58a31c'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


