﻿/****** Object:  Job [Job_IktatoKonyvekAutomatikusLezaras]    Script Date: 2020. 02. 18. 13:59:54 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 2020. 02. 18. 13:59:54 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Job_IktatoKonyvekAutomatikusLezaras', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'SQL job, amely minden nap 0:00-kor lefut, és ellenőrzi, hogy az EREC_IraIktatkonyvek között van-e olyan, ahol a LezarasDatuma kisebb egyenlő az adott nap, az Ujranyitando mező értéke 1, de nincs még olyan tétel, ahol az Iktatohely megegyezik az Ev pedig eggyel nagyobb. Ha van, akkor az adott tételre meg kell futtatni az új iktatókönyv létrehozását azonos adatokkal (eRecordWebService/EREC_IraIktatoKonyvekService.amsx/MasolKovetkezoEvre).', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [MasolasKovetkezoEvre_Lezaras]    Script Date: 2020. 02. 18. 13:59:55 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'MasolasKovetkezoEvre_Lezaras', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @REQUESTTEXT VARCHAR(8000)
	DECLARE @RESPONSETEXT VARCHAR(8000)
	SET @REQUESTTEXT=
	''<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <IraIktatoKonyvekProxyService xmlns="Contentum.eRecord.WebService">
      <FelhasznaloId>54E861A5-36ED-44CA-BAA7-C287D125B309</FelhasznaloId>
      <FelhasznaloSzervezetId>BD00C8D0-CF99-4DFC-8792-33220B7BFCC6</FelhasznaloSzervezetId>
      <IktatoErkezteto>I</IktatoErkezteto>
	  <Auto>true</Auto>
    </IraIktatoKonyvekProxyService>
  </soap:Body>
</soap:Envelope>''

	exec SP_HTTPREQUEST 
		''http://ax-vfphtst03/NMHH_R2T/eRecordWebservice/EREC_IraIktatoKonyvekService.asmx'',
		''POST'', 
		@REQUESTTEXT,
		''Contentum.eRecord.WebService/IraIktatoKonyvekProxyService'',
		'''', 
		'''', 
		@RESPONSETEXT out', 
		@database_name=N'CONTENTUM_NMHH_R2T', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Schedule automatikus lezaras', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20181210, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'd723b3b5-a567-4092-9208-bf596aaf9c5e'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


