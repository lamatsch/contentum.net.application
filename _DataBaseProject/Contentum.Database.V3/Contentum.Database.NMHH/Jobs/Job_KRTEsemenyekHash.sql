PRINT 'START KRT_ESEMENYEK_HASH'
PRINT ''
 
DECLARE @INPUT_OWNER_LOGIN_NAME NVARCHAR(100)
SET @INPUT_OWNER_LOGIN_NAME ='sa'

DECLARE @jobIdExist binary(16)

SELECT @jobIdExist = job_id FROM msdb.dbo.sysjobs WHERE (name = N'$(DataBaseName)_KRT_EsemenyekHash_job')
IF (@jobIdExist IS NULL)
BEGIN
    BEGIN TRANSACTION
	
	DECLARE @ReturnCode INT
	SELECT @ReturnCode = 0

	IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
	BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	END

	DECLARE @jobId BINARY(16)
	EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'$(DataBaseName)_KRT_EsemenyekHash_job', 
			@enabled=1, 
			@notify_level_eventlog=0, 
			@notify_level_email=0, 
			@notify_level_netsend=0, 
			@notify_level_page=0, 
			@delete_level=0, 
			@description=N'KRT_Esemenyek_Hash', 
			@category_name=N'[Uncategorized (Local)]', 
			@owner_login_name=@INPUT_OWNER_LOGIN_NAME, 
			@job_id = @jobId OUTPUT
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'SetIntegrityCheckSumSimple', 
			@step_id=1, 
			@cmdexec_success_code=0, 
			@on_success_action=1, 
			@on_success_step_id=0, 
			@on_fail_action=2, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, @subsystem=N'TSQL', 
			@command=
	N'DECLARE @REQUESTTEXT VARCHAR(8000)
	DECLARE @RESPONSETEXT VARCHAR(8000)
	SET @REQUESTTEXT=
	''<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	  <soap:Body>
		<SetIntegrityCheckSumSimple xmlns="Contentum.eAdmin.WebService">
		  <felhasznaloId>54E861A5-36ED-44CA-BAA7-C287D125B309</felhasznaloId>
		  <FelhasznaloSzervezetId>BD00C8D0-CF99-4DFC-8792-33220B7BFCC6</FelhasznaloSzervezetId>
		  <itemsNumber>1000</itemsNumber>
		</SetIntegrityCheckSumSimple>
	  </soap:Body>
	</soap:Envelope>''

	exec SP_HTTPREQUEST 
		''$(eadminwebservice_url)/KRT_EsemenyekService.asmx'',
		''POST'', 
		@REQUESTTEXT,
		''Contentum.eAdmin.WebService/SetIntegrityCheckSumSimple'',
		'''', 
		'''', 
		@RESPONSETEXT out', 
			@database_name=N'$(DataBaseName)', 
			@flags=0
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Utemezes', 
			@enabled=1, 
			@freq_type=4, 
			@freq_interval=1, 
			@freq_subday_type=4, 
			@freq_subday_interval=5, 
			@freq_relative_interval=0, 
			@freq_recurrence_factor=0, 
			@active_start_date=20160503, 
			@active_end_date=99991231, 
			@active_start_time=0, 
			@active_end_time=235959,
			@schedule_uid=N'500e91b3-3499-414d-bfb4-2566a22d3587'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	COMMIT TRANSACTION
	GOTO EndSave
	QuitWithRollback:
		IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
	EndSave:

END
PRINT 'STOP KRT_ESEMENYEK_HASH'
PRINT ''

GO