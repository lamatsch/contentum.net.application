USE [msdb]
GO 

/****** Object:  Job [Job_KRT_LOG_Delete]    Script Date: 2019. 03. 27. 12:06:37 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 2019. 03. 27. 12:06:37 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Job_KRT_LOG_Delete', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [KRT_Log_Login_Delete]    Script Date: 2019. 03. 27. 12:06:38 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'KRT_Log_Login_Delete', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'
WHILE ( CONVERT(INT,(SELECT COUNT(*) FROM KRT_Log_Login WHERE [Date] <= DATEADD(month, -13, GETDATE()))) > 0)
BEGIN
	BEGIN TRY
	    BEGIN TRANSACTION
		        DELETE TOP (1000) 
				FROM [KRT_Log_Login]
				WHERE[Date] <= DATEADD(month, -13, GETDATE()) 

				COMMIT
				
				Print N'' 1000 records deleted.''
				WAITFOR DELAY ''00:00:01''
	END TRY
BEGIN CATCH
    ROLLBACK
END CATCH
END;
', 
		@database_name=N'CONTENTUM_NMHH_R2T', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [KRT_Log_Page_Delete]    Script Date: 2019. 03. 27. 12:06:38 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'KRT_Log_Page_Delete', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'
WHILE ( CONVERT(INT,(SELECT COUNT(*) FROM KRT_Log_Page WHERE [Date] <= DATEADD(month, -13, GETDATE()))) > 0)
BEGIN
	BEGIN TRY
	    BEGIN TRANSACTION
		        DELETE TOP (1000) 
				FROM [KRT_Log_Page]
				WHERE[Date] <= DATEADD(month, -13, GETDATE()) 

				COMMIT
				
				Print N'' 1000 records deleted.''
				WAITFOR DELAY ''00:00:01''
	END TRY
BEGIN CATCH
    ROLLBACK
END CATCH
END;
', 
		@database_name=N'CONTENTUM_NMHH_R2T', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [KRT_Log_StoredProcedures_Delete]    Script Date: 2019. 03. 27. 12:06:38 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'KRT_Log_StoredProcedures_Delete', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'
WHILE ( CONVERT(INT,(SELECT COUNT(*) FROM KRT_Log_StoredProcedure WHERE [Date] <= DATEADD(month, -13, GETDATE()))) > 0)
BEGIN
	BEGIN TRY
	    BEGIN TRANSACTION
		        DELETE TOP (1000) 
				FROM [KRT_Log_StoredProcedure]
				WHERE[Date] <= DATEADD(month, -13, GETDATE()) 

				COMMIT
				
				Print N'' 1000 records deleted.''
				WAITFOR DELAY ''00:00:01''
	END TRY
BEGIN CATCH
    ROLLBACK
END CATCH
END;
', 
		@database_name=N'CONTENTUM_NMHH_R2T', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [KRT_Log_WebService]    Script Date: 2019. 03. 27. 12:06:38 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'KRT_Log_WebService', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'
WHILE ( CONVERT(INT,(SELECT COUNT(*) FROM KRT_Log_WebService WHERE [Date] <= DATEADD(month, -13, GETDATE()))) > 0)
BEGIN
	BEGIN TRY
	    BEGIN TRANSACTION
		        DELETE TOP (1000) 
				FROM [KRT_Log_WebService]
				WHERE[Date] <= DATEADD(month, -13, GETDATE()) 

				COMMIT
				
				Print N'' 1000 records deleted.''
				WAITFOR DELAY ''00:00:01''
	END TRY
BEGIN CATCH
    ROLLBACK
END CATCH
END;
', 
		@database_name=N'CONTENTUM_NMHH_R2T', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'First day of every month', 
		@enabled=1, 
		@freq_type=16, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20190326, 
		@active_end_date=99991231, 
		@active_start_time=30000, 
		@active_end_time=235959, 
		@schedule_uid=N'0213e30d-98f9-4542-b335-569b95ee7b66'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


