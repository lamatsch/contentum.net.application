PRINT 'START LejartMegorzesiIdo' 
PRINT ''

--BE�LL�TAND�: 
--  LOGIN N�V
--  SZERVIZ URL

DECLARE @jobIdExist binary(16)

SELECT @jobIdExist = job_id FROM msdb.dbo.sysjobs WHERE (name = N'Job_LejartMegorzesiIdo_$(DataBaseName)')
IF (@jobIdExist IS NULL)
BEGIN
    BEGIN TRANSACTION
	
	DECLARE @ReturnCode INT
	SELECT @ReturnCode = 0

	IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
	BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	END

	DECLARE @jobId BINARY(16)
	EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Job_LejartMegorzesiIdo_$(DataBaseName)', 
			@enabled=1, 
			@notify_level_eventlog=0, 
			@notify_level_email=0, 
			@notify_level_netsend=0, 
			@notify_level_page=0, 
			@delete_level=0, 
			@description=N'LejartMegorzesiIdo', 
			@category_name=N'[Uncategorized (Local)]', 
			@owner_login_name='sa', 
			@job_id = @jobId OUTPUT
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'LeveltariAtadas', 
			@step_id=1, 
			@cmdexec_success_code=0, 
			@on_success_action=1, 
			@on_success_step_id=0, 
			@on_fail_action=2, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, @subsystem=N'TSQL', 
			@command=
	N'DECLARE @REQUESTTEXT VARCHAR(8000)
	DECLARE @RESPONSETEXT VARCHAR(8000)
	SET @REQUESTTEXT=
	''<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	  <soap:Body>
		<Ertesites_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas xmlns="Contentum.eRecord.WebService">
		  <felhasznaloId>54E861A5-36ED-44CA-BAA7-C287D125B309</felhasznaloId>
		  <FelhasznaloSzervezetId>BD00C8D0-CF99-4DFC-8792-33220B7BFCC6</FelhasznaloSzervezetId>
		</Ertesites_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas>
	  </soap:Body>
	</soap:Envelope>''

	exec SP_HTTPREQUEST 
		''$(eRecordWebServiceUrl)'',
		''POST'', 
		@REQUESTTEXT,
		''Contentum.eRecord.WebService/Ertesites_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas'',
		'''', 
		'''', 
		@RESPONSETEXT out', 
			@database_name=N'$(DataBaseName)', 
			@flags=0
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Selejtezes', 
			@step_id=2, 
			@cmdexec_success_code=0, 
			@on_success_action=1, 
			@on_success_step_id=0, 
			@on_fail_action=2, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, @subsystem=N'TSQL', 
			@command=
	N'DECLARE @REQUESTTEXT VARCHAR(8000)
	DECLARE @RESPONSETEXT VARCHAR(8000)
	SET @REQUESTTEXT=
	''<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	  <soap:Body>
		<Ertesites_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes xmlns="Contentum.eRecord.WebService">
		  <felhasznaloId>54E861A5-36ED-44CA-BAA7-C287D125B309</felhasznaloId>
		  <FelhasznaloSzervezetId>BD00C8D0-CF99-4DFC-8792-33220B7BFCC6</FelhasznaloSzervezetId>
		</Ertesites_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes>
	  </soap:Body>
	</soap:Envelope>''

	exec SP_HTTPREQUEST 
		''$(eRecordWebServiceUrl)'',
		''POST'', 
		@REQUESTTEXT,
		''Contentum.eRecord.WebService/Ertesites_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes'',
		'''', 
		'''', 
		@RESPONSETEXT out', 
			@database_name=N'$(DataBaseName)', 
			@flags=0
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	
	EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'JAN_01', 
		@enabled=1, 
		@freq_type=16, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=12, 
		@active_start_date=20190101, 
		@active_end_date=99991231, 
		@active_start_time=1, 
		@active_end_time=235959, 
		@schedule_uid=N'd5e1365e-8136-47bf-aa9a-7d2ab792ead0'

	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	COMMIT TRANSACTION
	GOTO EndSave
	QuitWithRollback:
		IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
	EndSave:

END
PRINT 'STOP LejartMegorzesiIdo'
PRINT ''

GO