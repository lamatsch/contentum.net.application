USE [msdb]
GO

IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'Job_MIGR_FolyamatbanLevoJegyzekVegrehajtasa_$(DataBase)')
	EXEC msdb.dbo.sp_delete_job @job_name=N'Job_MIGR_FolyamatbanLevoJegyzekVegrehajtasa_$(DataBase)', @delete_unused_schedule=1
GO

DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from $(DataBase).dbo.KRT_Orgok where id=@ORG_ID) 

--Cseplen nincs migr�lt db
IF @ORG_KOD = 'CSPH'
	GOTO EndSave

/****** Object:  Job [Job_MIGR_FolyamatbanLevoJegyzekVegrehajtasa]    Script Date: 2018. 07. 05. 12:27:30 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 2018. 07. 05. 12:27:30 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Job_MIGR_FolyamatbanLevoJegyzekVegrehajtasa_$(DataBase)', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [FolyamatbanLevoJegyzekekVegrehajtasa]    Script Date: 2018. 07. 05. 12:27:30 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'FolyamatbanLevoJegyzekekVegrehajtasa', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'

DECLARE @SelejtezesJegyzekreHelyezessel nvarchar(400) = (SELECT Ertek FROM KRT_Parameterek WHERE Nev = ''EMIGRATIONBEN_SELEJTEZES_JEGYZEKRE_HELYEZESSEL''
AND Org = ''450B510A-7CAA-46B0-83E3-18445C0C53A9'' AND GETDATE() BETWEEN ErvKezd AND ErvVege)

IF @SelejtezesJegyzekreHelyezessel = ''1''
BEGIN

	declare @xmlOut varchar(8000)
	declare @RequestText as nvarchar(max)

	set @RequestText =
	''<?xml version="1.0" encoding="utf-8"?>
	<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	  <soap:Body>
		<FolyamatbanLevoJegyzekekVegrehajtasa xmlns="Contentum.eMigration.WebService" />
	  </soap:Body>
	</soap:Envelope>''

	exec dbo.sp_HTTPRequest 
		''$(eMigrationWebServiceUrl)/MIG_JegyzekekService.asmx'', ''POST'', @RequestText, 
		''Contentum.eMigration.WebService/FolyamatbanLevoJegyzekekVegrehajtasa'', '''', '''', @xmlOut out

	select @xmlOut

END', 
		@database_name=N'$(DataBase)', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'FolyamatbanLevoJegyzekekVegrehajtasa_Schedule', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20180705, 
		@active_end_date=99991231, 
		@active_start_time=183000, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


