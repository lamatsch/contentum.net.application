@echo off

set sqlcmd="sqlcmd.exe"

if "%1"=="" goto :help
if "%2"=="" goto :help
if "%3"=="" goto :help

@echo on
%sqlcmd% -S %1 -v DataBaseName="%2" ScanServiceUrl="%3" -i Job_SzkenneltDokumentumokFeltoltese.sql

@echo off

goto :EOF

:help
echo. Hasznalat:
echo.  1. parameter: szerver nev
echo.  2. parameter: adatbazis neve
echo.  3. parameter: scan service url
echo.
echo. Pelda:
echo. build.bat INTCSQL2T EDOK http://edok/eRecordWebService/ScanService.asmx
echo. build.bat INCSQL2 EDOK http://edokteszt/eRecordWebService/ScanService.asmx

:EOF