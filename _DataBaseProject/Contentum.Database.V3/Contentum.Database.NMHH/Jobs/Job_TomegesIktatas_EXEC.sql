USE [msdb]
GO

/****** Object:  Job [Job_TomegesIktatas_EXEC]    Script Date: 2019. 07. 18. 18:56:01 ******/
BEGIN TRANSACTION

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from $(DataBaseName).dbo.KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9')

IF @org_kod != 'BOPMH' and @org_kod != 'CSPH' and @org_kod != 'NMHH'
	GOTO QuitWithRollback

DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 2019. 07. 18. 18:56:01 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)

SELECT @jobId = job_id FROM msdb.dbo.sysjobs WHERE (name = N'Job_TomegesIktatas_EXEC_$(DataBaseName)')
IF (@jobId IS NOT NULL)
	GOTO QuitWithRollback

PRINT 'Create job Job_TomegesIktatas_EXEC_$(DataBaseName)'

EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Job_TomegesIktatas_EXEC_$(DataBaseName)', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'T�meges iktat�si folyamatok aszinkron v�grehajt�sa.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [TomegesIktatas_EXEC]    Script Date: 2019. 07. 18. 18:56:01 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'TomegesIktatas_EXEC', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec sp_CallWebService ''$(eRecordWebServiceUrl)/EREC_IraIratokService.asmx/TomegesIktatas_EXEC_Job''', 
		@database_name=N'$(DataBaseName)', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'TomegesIktatas_EXEC_Schedule', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=10, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20190718, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'4ac51a17-4ecc-4ef9-b7f3-2cd01e00c2cf'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO

