@echo off

set sqlcmd="sqlcmd.exe"

if "%1"=="" goto :help
if "%2"=="" goto :help
if "%3"=="" goto :help

@echo on

%sqlcmd% -S %1 -d %2 -i Job_ScheduledOCR.sql -v DataBaseName="%2" erecordwebservice_url="%3"

@echo off

goto :EOF

:help
echo. Hasznalat:
echo.  1. parameter: szerver nev
echo.  2. parameter: adatbazis neve
echo.  3. parameter: eintegrator web service url
echo.
echo. Pelda:
echo. build.bat INTCSQL2T EDOK http://localhost:100/eRecordWebService

:EOF