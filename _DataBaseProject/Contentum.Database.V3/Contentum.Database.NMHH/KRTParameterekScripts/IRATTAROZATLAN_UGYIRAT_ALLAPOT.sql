﻿
	---------------------------------------------------------------------
	-- RENDSZERPARAMETER = IRATTAROZATLAN_UGYIRAT_ALLAPOT
	---------------------------------------------------------------------	
	GO

    DECLARE @ORGID				UNIQUEIDENTIFIER
	DECLARE @ID					UNIQUEIDENTIFIER
	DECLARE @NEV				NVARCHAR(400)
	DECLARE @KARBANTARTHATO		CHAR(1)
	DECLARE @NOTE				NVARCHAR(4000)

	DECLARE @ERTEK_FPH				NVARCHAR(400)
	DECLARE @ERTEK_CSPH				NVARCHAR(400)
	DECLARE @ERTEK_NMHH				NVARCHAR(400)
	DECLARE @ERTEK_BOPMH			NVARCHAR(400)

	SET @ORGID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
	SET @ID = 	 NEWID()
	SET @NEV =   'IRATTAROZATLAN_UGYIRAT_ALLAPOT'
	SET @KARBANTARTHATO = '1'
	SET @NOTE = 'A megadott állapottal rendelkező ügyiratok szerepeljenek az Irattározatlan ügyiratok jegyzékén, ","-vel elválasztott felsorolás'

	SET @ERTEK_FPH = '0,03,04,06,07,09,11,13,50,52,70,98'
	SET @ERTEK_CSPH = '0,03,04,06,07,09,11,13,50,52,70,98'
	SET @ERTEK_NMHH = '0,03,04,06,07,09,11,13,50,52,70,98'
	SET @ERTEK_BOPMH = '0,03,04,06,07,09,11,13,50,52,70,98'

	IF NOT EXISTS(SELECT 1 FROM KRT_Parameterek WHERE NEV=@NEV OR ID=@ID) 
	BEGIN
		IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='FPH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_FPH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='BOPMH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_BOPMH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='NMHH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_NMHH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='CSPH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_CSPH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
	END
	ELSE
	BEGIN
		IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='FPH')
			BEGIN
				UPDATE KRT_Parameterek SET Ertek = @ERTEK_FPH, Note = @NOTE WHERE (NEV=@NEV OR ID=@ID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='BOPMH')
			BEGIN
				UPDATE KRT_Parameterek SET Ertek = @ERTEK_BOPMH, Note = @NOTE WHERE (NEV=@NEV OR ID=@ID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='NMHH')
			BEGIN
				UPDATE KRT_Parameterek SET Ertek = @ERTEK_NMHH, Note = @NOTE WHERE (NEV=@NEV OR ID=@ID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='CSPH')
			BEGIN
				UPDATE KRT_Parameterek SET Ertek = @ERTEK_CSPH, Note = @NOTE WHERE (NEV=@NEV OR ID=@ID)
			END	
	END
	GO
	---------------------------------------------------------------------

