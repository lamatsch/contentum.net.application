﻿
	---------------------------------------------------------------------
	-- RENDSZERPARAMETER = MIGRACIOS_FOSZAM_HELY_VALTOZAS_TELJES_LANC
	---------------------------------------------------------------------	
	GO

    DECLARE @ORGID				UNIQUEIDENTIFIER
	DECLARE @ID					UNIQUEIDENTIFIER
	DECLARE @NEV				NVARCHAR(400)
	DECLARE @KARBANTARTHATO		CHAR(1)
	DECLARE @NOTE				NVARCHAR(4000)

	DECLARE @ERTEK_FPH				NVARCHAR(400)
	DECLARE @ERTEK_CSPH				NVARCHAR(400)
	DECLARE @ERTEK_NMHH				NVARCHAR(400)
	DECLARE @ERTEK_BOPMH			NVARCHAR(400)

	SET @ORGID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
	SET @ID = 	 'b5cbe007-9bec-4fbe-a4c0-8a1e6c280e80'
	SET @NEV =   'MIGRACIOS_FOSZAM_HELY_VALTOZAS_TELJES_LANC'
	SET @KARBANTARTHATO = '1'
	SET @NOTE = 'eMigration modulban, a Szerelt ügyiratoknál az ITSZ adott iktatási évben történő módosítását követően a Szerelt ügyirat többi részénél (a többi iktatási évben lévő ügyiratnál) is módosítja az ITSZ-t a program'

	SET @ERTEK_FPH = '1'
	SET @ERTEK_CSPH = '1'
	SET @ERTEK_NMHH = '1'
	SET @ERTEK_BOPMH = '0'

	IF NOT EXISTS(SELECT 1 FROM KRT_Parameterek WHERE NEV=@NEV OR ID=@ID) 
	BEGIN
		IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='FPH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_FPH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='BOPMH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_BOPMH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='NMHH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_NMHH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='CSPH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_CSPH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
	END
	GO
	---------------------------------------------------------------------

