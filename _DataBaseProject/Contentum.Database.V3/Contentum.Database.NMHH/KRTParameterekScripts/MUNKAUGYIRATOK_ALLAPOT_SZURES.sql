﻿
	---------------------------------------------------------------------
	-- RENDSZERPARAMETER = MUNKAUGYIRATOK_ALLAPOT_SZURES
	---------------------------------------------------------------------	
	GO

    DECLARE @ORGID				UNIQUEIDENTIFIER
	DECLARE @ID					UNIQUEIDENTIFIER
	DECLARE @NEV				NVARCHAR(400)
	DECLARE @KARBANTARTHATO		CHAR(1)
	DECLARE @NOTE				NVARCHAR(4000)

	DECLARE @ERTEK_FPH				NVARCHAR(400)
	DECLARE @ERTEK_CSPH				NVARCHAR(400)
	DECLARE @ERTEK_NMHH				NVARCHAR(400)
	DECLARE @ERTEK_BOPMH			NVARCHAR(400)

	SET @ORGID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
	SET @ID = 	 'DD6CF501-2856-4279-947F-CD18EDDD9DFA'
	SET @NEV =   'MUNKAUGYIRATOK_ALLAPOT_SZURES'
	SET @KARBANTARTHATO = '1'
	SET @NOTE = 'Meghatározza, hogy a munkaügyiratok listáján alapállapotban milyen állapotú ügyiratok jelenjenek meg.'

	SET @ERTEK_FPH = '0'
	SET @ERTEK_CSPH = ''
	SET @ERTEK_NMHH = ''
	SET @ERTEK_BOPMH = ''

	IF NOT EXISTS(SELECT 1 FROM KRT_Parameterek WHERE NEV=@NEV OR ID=@ID) 
	BEGIN
		IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='FPH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_FPH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='BOPMH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_BOPMH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='NMHH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_NMHH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='CSPH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_CSPH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
	END
	GO
	---------------------------------------------------------------------

