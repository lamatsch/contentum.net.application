﻿
	---------------------------------------------------------------------
	-- RENDSZERPARAMETER = UGYIRATI_PELDANY_AUTO_ATADAS
	---------------------------------------------------------------------	
	GO

    DECLARE @ORGID				UNIQUEIDENTIFIER
	DECLARE @ID					UNIQUEIDENTIFIER
	DECLARE @NEV				NVARCHAR(400)
	DECLARE @KARBANTARTHATO		CHAR(1)
	DECLARE @NOTE				NVARCHAR(4000)

	DECLARE @ERTEK_FPH				NVARCHAR(400)
	DECLARE @ERTEK_CSPH				NVARCHAR(400)
	DECLARE @ERTEK_NMHH				NVARCHAR(400)
	DECLARE @ERTEK_BOPMH			NVARCHAR(400)

	SET @ORGID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
	SET @ID = 	 '38300006-D85E-47CC-9A99-793629BD038C'
	SET @NEV =   'UGYIRATI_PELDANY_AUTO_ATADAS'
	SET @KARBANTARTHATO = '0'
	SET @NOTE = 'Ha 1 az értéke, akkor az irattári példány induljon el az ügyirat irat helye felé, ha 0 akkor az irattári példány helye legyen az iktatást végző felhasználó'

	SET @ERTEK_FPH = '1'
	SET @ERTEK_CSPH = '1'
	SET @ERTEK_NMHH = '0'
	SET @ERTEK_BOPMH = '1'

	IF NOT EXISTS(SELECT 1 FROM KRT_Parameterek WHERE NEV=@NEV OR ID=@ID) 
	BEGIN
		IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='FPH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_FPH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='BOPMH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_BOPMH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='NMHH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_NMHH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
		ELSE IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='CSPH')
			BEGIN
				INSERT INTO KRT_Parameterek (Id, Nev, Ertek, Karbantarthato, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_CSPH, @KARBANTARTHATO, @NOTE, @ORGID)
			END
	END
	GO
	---------------------------------------------------------------------

