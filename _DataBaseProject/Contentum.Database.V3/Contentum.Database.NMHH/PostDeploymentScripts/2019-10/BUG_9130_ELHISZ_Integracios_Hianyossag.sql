DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD ='NMHH')
	BEGIN 
	PRINT 'Executing: BUG_9130_ELHISZ_Intergracios_Hianyossag'
		DECLARE @LINK_ID uniqueidentifier
		
		SET @LINK_ID = 'FE17ADA5-7981-47CA-8995-4FAAC656EFD6'
		IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @LINK_ID)
		BEGIN	
			insert into KRT_Forditasok (Id,NyelvKod,OrgKod,Modul,Komponens,ObjAzonosito,Forditas) values(@LINK_ID,'hu-HU','NMHH','*','*','LabelPartnerCim*','KRID')
		END

		SET @LINK_ID = '20559BBE-5147-4598-89C1-ED224727F55E'
		IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @LINK_ID)
		BEGIN	
			insert into KRT_Forditasok (Id,NyelvKod,OrgKod,Modul,Komponens,ObjAzonosito,Forditas) values(@LINK_ID,'hu-HU','NMHH','*','*','LabelDokumentumHivatal*','Dokumentum t�pus hivatal')
		END

		SET @LINK_ID = 'D61E365C-0F89-4407-BC45-159AD9E0B861'
		IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @LINK_ID)
		BEGIN	
			insert into KRT_Forditasok (Id,NyelvKod,OrgKod,Modul,Komponens,ObjAzonosito,Forditas) values(@LINK_ID,'hu-HU','NMHH','*','*','LabelDokumentumLeiras*','Dokumentum t�pus le�r�s')
		END

		SET @LINK_ID = '2750F738-0F00-4E8F-BC8D-1ADB6A584823'
		IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @LINK_ID)
		BEGIN	
			insert into KRT_Forditasok (Id,NyelvKod,OrgKod,Modul,Komponens,ObjAzonosito,Forditas) values(@LINK_ID,'hu-HU','NMHH','*','*','LabelDokumentumAzonosito*','Dokumentum t�pus azonos�t�')
		END
   END
GO