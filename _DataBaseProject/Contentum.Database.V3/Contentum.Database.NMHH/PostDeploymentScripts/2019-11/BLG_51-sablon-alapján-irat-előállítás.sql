﻿PRINT 'Executing: BLG_51-sablon-alapján-irat-előállítás'

 ----------------------- BLG 51 - sablon alapján irat előállítás ---------------------
--Létre kell hozni egy új kódcsoportot Sablonok néven, ide kerülnek a Sablon dokumentumok, 
--de a kódtár kezelés nem ad elégséges funkciót a kódtár értékekhez sablon dokumentumok rendelésére
IF NOT EXISTS (SELECT 1 FROM KRT_KodCsoportok WHERE Id ='5B9EDE81-895B-40A6-AB75-1B6A7DD766D4')
INSERT INTO [dbo].[KRT_KodCsoportok]
           ([Id]
           ,[Org]
           ,[KodTarak_Id_KodcsoportTipus]
           ,[Kod]
           ,[Nev]
           ,[Modosithato]
           ,[Hossz]
           ,[Csoport_Id_Tulaj]
           ,[Leiras]
           ,[BesorolasiSema]
           ,[KiegAdat]
           ,[KiegMezo]
           ,[KiegAdattipus]
           ,[KiegSzotar]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('5B9EDE81-895B-40A6-AB75-1B6A7DD766D4',
           NULL,
           'A54AF681-ADA2-420F-860F-826257E95932',
           'SABLONOK',
           'Sablonok dokumentumok kezelése',
           1,
           0,
            NULL,
           'Sablonok dokumentumok kezelése',
           0,
		   NULL,
           NULL,
           NULL,
           NULL,
           1,
           'Sablonok dokumentumok kezelése',
           NULL,
           '2019-11-18',
           '4700-12-31',
            NULL,
           '2019-11-18',
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
			)
GO
----------------------- BLG 51 - sablon alapján irat előállítás ---------------------
------------------------------------- VÉGE ------------------------------------------

----------------------- BLG 51 - sablon alapján irat előállítás ---------------------
-----------------------------Kódtárak hozzáadása-------------------------------------
IF NOT EXISTS (SELECT 1 FROM KRT_KODTARAK WHERE ID ='FECAF4EF-A221-476A-B260-57EF9D59F37D')
INSERT INTO [dbo].[KRT_KodTarak]
           ([Id]
           ,[Org]
           ,[KodCsoport_Id]
           ,[ObjTip_Id_AdatElem]
           ,[Obj_Id]
           ,[Kod]
           ,[Nev]
           ,[RovidNev]
           ,[Egyeb]
           ,[Modosithato]
           ,[Sorrend]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('FECAF4EF-A221-476A-B260-57EF9D59F37D',
           '450B510A-7CAA-46B0-83E3-18445C0C53A9',
           '5B9EDE81-895B-40A6-AB75-1B6A7DD766D4',
           NULL,
           NULL,
          'Hatarozat',
           'Határozat',
           NULL,
           NULL,
           1,
           1,
           1,
           NULL,
           NULL,
            '2019-11-18',
           '4700-12-31',
            NULL,
           '2019-11-18',
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
			)
GO
----------------------- BLG 51 - sablon alapján irat előállítás ---------------------
-----------------------------Kódtárak hozzáadása-------------------------------------
-------------------------------------VÉGE--------------------------------------------

----------------------- BLG 51 - sablon alapján irat előállítás ---------------------
-------------------------------Modul hozzáadása--------------------------------------
IF NOT EXISTS (SELECT 1 FROM KRT_Modulok WHERE Id ='6FA44798-43D1-4FF8-A059-1DC232356664')
INSERT INTO [dbo].[KRT_Modulok]
           ([Id]
           ,[Org]
           ,[Tipus]
           ,[Kod]
           ,[Nev]
           ,[Leiras]
           ,[SelectString]
           ,[Csoport_Id_Tulaj]
           ,[Parameterek]
           ,[ObjTip_Id_Adatelem]
           ,[ObjTip_Adatelem]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('6FA44798-43D1-4FF8-A059-1DC232356664'
           ,NULL
           ,'F'
           ,'SablonDokumentumokKezelesList.aspx'
           ,'Sablondokumentumok kezelése lista'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,1
           ,NULL
           ,NULL
           ,'2019-11-18 00:00:00.000'
           ,'4700-12-31 00:00:00.000'
           ,NULL
           ,GETDATE()
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL)
GO
----------------------- BLG 51 - sablon alapján irat előállítás ---------------------
-------------------------------Modul hozzáadása--------------------------------------
-------------------------------------VÉGE--------------------------------------------

----------------------- BLG 51 - sablon alapján irat előállítás ---------------------
-------------------------------Funkciók hozzáadása-----------------------------------
--SablonokList
IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE Id ='E46263C5-438C-43EA-BD7D-0A50F4373E59')
INSERT INTO [dbo].[KRT_Funkciok]
           ([Id]
           ,[Kod]
           ,[Nev]
           ,[ObjTipus_Id_AdatElem]
           ,[ObjStat_Id_Kezd]
           ,[Alkalmazas_Id]
           ,[Muvelet_Id]
           ,[ObjStat_Id_Veg]
           ,[Leiras]
           ,[Funkcio_Id_Szulo]
           ,[Csoportosito]
           ,[Modosithato]
           ,[Org]
           ,[MunkanaploJelzo]
           ,[FeladatJelzo]
           ,[KeziFeladatJelzo]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('E46263C5-438C-43EA-BD7D-0A50F4373E59'
           ,'SablonokList'
           ,'Sablonok listázása'
           ,'D56B16FD-0364-4A7D-A138-432A3B7D4C3B'
           ,NULL
           ,'20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
           ,'9508908E-AEFA-41C6-B6D7-807E3AE3A3AB'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,0
           ,NULL
           ,0
           ,0
           ,NULL
           ,1
           ,NULL
           ,NULL
           ,'2019-11-19 00:00:00.000'
           ,'4700-12-31 00:00:00.000'
           ,'54E861A5-36ED-44CA-BAA7-C287D125B309'
           ,'2019-11-19 00:00:00.000'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL)
GO
--SablonokNew
IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE Id ='D68EF4C7-53B9-430D-9B9E-41BE15BAEED9')
INSERT INTO [dbo].[KRT_Funkciok]
           ([Id]
           ,[Kod]
           ,[Nev]
           ,[ObjTipus_Id_AdatElem]
           ,[ObjStat_Id_Kezd]
           ,[Alkalmazas_Id]
           ,[Muvelet_Id]
           ,[ObjStat_Id_Veg]
           ,[Leiras]
           ,[Funkcio_Id_Szulo]
           ,[Csoportosito]
           ,[Modosithato]
           ,[Org]
           ,[MunkanaploJelzo]
           ,[FeladatJelzo]
           ,[KeziFeladatJelzo]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('D68EF4C7-53B9-430D-9B9E-41BE15BAEED9'
           ,'SablonokNew'
           ,'Sablon létrehozása'
           ,'D56B16FD-0364-4A7D-A138-432A3B7D4C3B'
           ,NULL
           ,'20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
           ,'01B026C0-25D4-4AE3-B6E2-10CC9A97D679'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,0
           ,NULL
           ,0
           ,0
           ,NULL
           ,1
           ,NULL
           ,NULL
           ,'2019-11-19 00:00:00.000'
           ,'4700-12-31 00:00:00.000'
           ,'54E861A5-36ED-44CA-BAA7-C287D125B309'
           ,'2019-11-19 00:00:00.000'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL)
GO

--SablonokView
IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE Id ='184CC875-3F78-43B1-9360-CD62BBDEFD46')
INSERT INTO [dbo].[KRT_Funkciok]
           ([Id]
           ,[Kod]
           ,[Nev]
           ,[ObjTipus_Id_AdatElem]
           ,[ObjStat_Id_Kezd]
           ,[Alkalmazas_Id]
           ,[Muvelet_Id]
           ,[ObjStat_Id_Veg]
           ,[Leiras]
           ,[Funkcio_Id_Szulo]
           ,[Csoportosito]
           ,[Modosithato]
           ,[Org]
           ,[MunkanaploJelzo]
           ,[FeladatJelzo]
           ,[KeziFeladatJelzo]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('184CC875-3F78-43B1-9360-CD62BBDEFD46'
           ,'SablonokView'
           ,'Sablon megtekintése'
           ,'D56B16FD-0364-4A7D-A138-432A3B7D4C3B'
           ,NULL
           ,'20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
           ,'78F0D5A2-E048-4A1F-9F11-5D57AB78B985'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,0
           ,NULL
           ,0
           ,0
           ,NULL
           ,1
           ,NULL
           ,NULL
           ,'2019-11-19 00:00:00.000'
           ,'4700-12-31 00:00:00.000'
           ,'54E861A5-36ED-44CA-BAA7-C287D125B309'
           ,'2019-11-19 00:00:00.000'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL)
GO

--SablonokModify
IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE Id ='31EB614E-4817-4A47-892A-1E3BB558E4E3')
INSERT INTO [dbo].[KRT_Funkciok]
           ([Id]
           ,[Kod]
           ,[Nev]
           ,[ObjTipus_Id_AdatElem]
           ,[ObjStat_Id_Kezd]
           ,[Alkalmazas_Id]
           ,[Muvelet_Id]
           ,[ObjStat_Id_Veg]
           ,[Leiras]
           ,[Funkcio_Id_Szulo]
           ,[Csoportosito]
           ,[Modosithato]
           ,[Org]
           ,[MunkanaploJelzo]
           ,[FeladatJelzo]
           ,[KeziFeladatJelzo]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('31EB614E-4817-4A47-892A-1E3BB558E4E3'
           ,'SablonokModify'
           ,'Sablon módosítása'
           ,'D56B16FD-0364-4A7D-A138-432A3B7D4C3B'
           ,NULL
           ,'20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
           ,'5B9AC7B3-879C-48A7-BEBA-EF73A49AF893'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,0
           ,NULL
           ,0
           ,0
           ,NULL
           ,1
           ,NULL
           ,NULL
           ,'2019-11-19 00:00:00.000'
           ,'4700-12-31 00:00:00.000'
           ,'54E861A5-36ED-44CA-BAA7-C287D125B309'
           ,'2019-11-19 00:00:00.000'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL)
GO

--SablonokInvalidate
IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE Id ='89984D9B-747D-44D3-9295-4CD758372BEF')
INSERT INTO [dbo].[KRT_Funkciok]
           ([Id]
           ,[Kod]
           ,[Nev]
           ,[ObjTipus_Id_AdatElem]
           ,[ObjStat_Id_Kezd]
           ,[Alkalmazas_Id]
           ,[Muvelet_Id]
           ,[ObjStat_Id_Veg]
           ,[Leiras]
           ,[Funkcio_Id_Szulo]
           ,[Csoportosito]
           ,[Modosithato]
           ,[Org]
           ,[MunkanaploJelzo]
           ,[FeladatJelzo]
           ,[KeziFeladatJelzo]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('89984D9B-747D-44D3-9295-4CD758372BEF'
           ,'SablonokInvalidate'
           ,'Sablon érvénytelenítése'
           ,'D56B16FD-0364-4A7D-A138-432A3B7D4C3B'
           ,NULL
           ,'20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
           ,'805C7634-F205-4AE7-8580-DB3C73D8F43B'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,0
           ,NULL
           ,0
           ,0
           ,NULL
           ,1
           ,NULL
           ,NULL
           ,'2019-11-19 00:00:00.000'
           ,'4700-12-31 00:00:00.000'
           ,'54E861A5-36ED-44CA-BAA7-C287D125B309'
           ,'2019-11-19 00:00:00.000'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL)
GO

--SablonokRevalidate
IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE Id ='3952805C-90CD-411E-9DA2-71215D042CCD')
INSERT INTO [dbo].[KRT_Funkciok]
           ([Id]
           ,[Kod]
           ,[Nev]
           ,[ObjTipus_Id_AdatElem]
           ,[ObjStat_Id_Kezd]
           ,[Alkalmazas_Id]
           ,[Muvelet_Id]
           ,[ObjStat_Id_Veg]
           ,[Leiras]
           ,[Funkcio_Id_Szulo]
           ,[Csoportosito]
           ,[Modosithato]
           ,[Org]
           ,[MunkanaploJelzo]
           ,[FeladatJelzo]
           ,[KeziFeladatJelzo]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('3952805C-90CD-411E-9DA2-71215D042CCD'
           ,'SablonokRevalidate'
           ,'Sablon érvényesítés'
           ,'D56B16FD-0364-4A7D-A138-432A3B7D4C3B'
           ,NULL
           ,'20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
           ,'F4B80FA8-9E45-4913-B15A-6573B246B101'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,0
           ,NULL
           ,0
           ,0
           ,NULL
           ,1
           ,NULL
           ,NULL
           ,'2019-11-19 00:00:00.000'
           ,'4700-12-31 00:00:00.000'
           ,'54E861A5-36ED-44CA-BAA7-C287D125B309'
           ,'2019-11-19 00:00:00.000'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL)
GO

--SablonokViewHistory
IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE Id ='91B57BF0-FA85-4623-8A18-4BD42D52F77C')
INSERT INTO [dbo].[KRT_Funkciok]
           ([Id]
           ,[Kod]
           ,[Nev]
           ,[ObjTipus_Id_AdatElem]
           ,[ObjStat_Id_Kezd]
           ,[Alkalmazas_Id]
           ,[Muvelet_Id]
           ,[ObjStat_Id_Veg]
           ,[Leiras]
           ,[Funkcio_Id_Szulo]
           ,[Csoportosito]
           ,[Modosithato]
           ,[Org]
           ,[MunkanaploJelzo]
           ,[FeladatJelzo]
           ,[KeziFeladatJelzo]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('91B57BF0-FA85-4623-8A18-4BD42D52F77C'
           ,'SablonokViewHistory'
           ,'Sablon history'
           ,'D56B16FD-0364-4A7D-A138-432A3B7D4C3B'
           ,NULL
           ,'20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
           ,'89806484-BF5A-4396-87B2-0066513A008D'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,0
           ,NULL
           ,0
           ,0
           ,NULL
           ,1
           ,NULL
           ,NULL
           ,'2019-11-19 00:00:00.000'
           ,'4700-12-31 00:00:00.000'
           ,'54E861A5-36ED-44CA-BAA7-C287D125B309'
           ,'2019-11-19 00:00:00.000'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL)
GO

declare @fejlesztoSzerepkor uniqueidentifier
set @fejlesztoSzerepkor = 'e855f681-36ed-41b6-8413-576fcb5d1542'

IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE Id = 'CD0E20FC-740C-EA11-80E0-00155D017B06')
BEGIN
  PRINT 'INSERT KRT_Szerepkor_Funkcio: FEJLESZTO - SablonokInvalidate'
  INSERT INTO KRT_Szerepkor_Funkcio
  (Id,
  Szerepkor_Id,
  Funkcio_Id)
  VALUES
  ('CD0E20FC-740C-EA11-80E0-00155D017B06',
  @fejlesztoSzerepkor,
  '89984D9B-747D-44D3-9295-4CD758372BEF'
  )
END


IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE Id = 'E94FEA1D-750C-EA11-80E0-00155D017B06')
BEGIN
  PRINT 'INSERT KRT_Szerepkor_Funkcio: FEJLESZTO - SablonokList'
  INSERT INTO KRT_Szerepkor_Funkcio
  (Id,
  Szerepkor_Id,
  Funkcio_Id)
  VALUES
  ('E94FEA1D-750C-EA11-80E0-00155D017B06',
  @fejlesztoSzerepkor,
  'E46263C5-438C-43EA-BD7D-0A50F4373E59'
  )
END


IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE Id = 'E64FEA1D-750C-EA11-80E0-00155D017B06')
BEGIN
  PRINT 'INSERT KRT_Szerepkor_Funkcio: FEJLESZTO - SablonokModify'
  INSERT INTO KRT_Szerepkor_Funkcio
  (Id,
  Szerepkor_Id,
  Funkcio_Id)
  VALUES
  ('E64FEA1D-750C-EA11-80E0-00155D017B06',
  @fejlesztoSzerepkor,
  '31EB614E-4817-4A47-892A-1E3BB558E4E3'
  )
END


IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE Id = '38BC1409-750C-EA11-80E0-00155D017B06')
BEGIN
  PRINT 'INSERT KRT_Szerepkor_Funkcio: FEJLESZTO - SablonokNew'
  INSERT INTO KRT_Szerepkor_Funkcio
  (Id,
  Szerepkor_Id,
  Funkcio_Id)
  VALUES
  ('38BC1409-750C-EA11-80E0-00155D017B06',
  @fejlesztoSzerepkor,
  'D68EF4C7-53B9-430D-9B9E-41BE15BAEED9'
  )
END


IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE Id = 'CA0E20FC-740C-EA11-80E0-00155D017B06')
BEGIN
  PRINT 'INSERT KRT_Szerepkor_Funkcio: FEJLESZTO - SablonokRevalidate'
  INSERT INTO KRT_Szerepkor_Funkcio
  (Id,
  Szerepkor_Id,
  Funkcio_Id)
  VALUES
  ('CA0E20FC-740C-EA11-80E0-00155D017B06',
  @fejlesztoSzerepkor,
  '3952805C-90CD-411E-9DA2-71215D042CCD'
  )
END


IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE Id = '3D9F9412-750C-EA11-80E0-00155D017B06')
BEGIN
  PRINT 'INSERT KRT_Szerepkor_Funkcio: FEJLESZTO - SablonokView'
  INSERT INTO KRT_Szerepkor_Funkcio
  (Id,
  Szerepkor_Id,
  Funkcio_Id)
  VALUES
  ('3D9F9412-750C-EA11-80E0-00155D017B06',
  @fejlesztoSzerepkor,
  '184CC875-3F78-43B1-9360-CD62BBDEFD46'
  )
END


IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE Id = '35BC1409-750C-EA11-80E0-00155D017B06')
BEGIN
  PRINT 'INSERT KRT_Szerepkor_Funkcio: FEJLESZTO - SablonokViewHistory'
  INSERT INTO KRT_Szerepkor_Funkcio
  (Id,
  Szerepkor_Id,
  Funkcio_Id)
  VALUES
  ('35BC1409-750C-EA11-80E0-00155D017B06',
  @fejlesztoSzerepkor,
  '91B57BF0-FA85-4623-8A18-4BD42D52F77C'
  )
END

GO

----------------------- BLG 51 - sablon alapján irat előállítás ---------------------
-------------------------------Funkciók hozzáadása------------------------------------
-------------------------------------VÉGE--------------------------------------------

----------------------- BLG 51 - sablon alapján irat előállítás ---------------------
-----------------------------Menüpont hozzáadása-------------------------------------
IF NOT EXISTS (SELECT 1 FROM KRT_Menuk WHERE Id ='C8678950-5DB5-4E1D-8641-57856D2BF1E2')
INSERT INTO [dbo].[KRT_Menuk]
           ([Id]
           ,[Org]
           ,[Menu_Id_Szulo]
           ,[Kod]
           ,[Nev]
           ,[Funkcio_Id]
           ,[Modul_Id]
           ,[Parameter]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ImageURL]
           ,[Sorrend]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           (
		   'C8678950-5DB5-4E1D-8641-57856D2BF1E2'
           ,NULL
           ,'19B72B61-0FF3-4239-8D0F-6FB20792D79C'
           ,'eRecord'
           ,'Irat sablon kezelés'
           ,'E46263C5-438C-43EA-BD7D-0A50F4373E59'
           ,'6FA44798-43D1-4FF8-A059-1DC232356664'
           ,NULL
           ,1
           ,NULL
           ,NULL
           ,NULL
           ,904
           ,'2019-11-18 00:00:00.000'
           ,'4700-12-31 00:00:00.000'
           ,NULL
           ,GETDATE()
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
		   )
GO
----------------------- BLG 51 - sablon alapján irat előállítás ---------------------
-----------------------------Menüpont hozzáadása-------------------------------------
-------------------------------------VÉGE--------------------------------------------

	
declare @record_id uniqueidentifier
set @record_id = 'E8C8A9CA-8369-4004-8A3F-ED47121609BD'

declare @adat nvarchar(max)
SET @adat = ''

IF NOT EXISTS (SELECT 1 FROM KRT_KodtarFuggoseg WHERE Id = @record_id)
BEGIN
	PRINT 'INSERT - KRT_KodtarFuggoseg - IRATTIPUS - SABLONOK'
	INSERT INTO KRT_KodtarFuggoseg
	(Id
	,Org
	,Vezerlo_KodCsoport_Id
	,Fuggo_KodCsoport_Id
	,Adat
	,Aktiv)
	VALUES
	(@record_id
	,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
	,'272DC3FD-4A8D-4EA2-88ED-C795A55D0373' -- IRATTIPUS
	,'5B9EDE81-895B-40A6-AB75-1B6A7DD766D4' -- SABLONOK
	,@adat
	,'1')
END

GO
