﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

 
DECLARE @IS_TUK char(1) 
SET @IS_TUK = (select TOP 1 ertek from KRT_Parameterek	where nev='TUK')

IF((@ORG_KOD ='NMHH') and (@IS_TUK ='1'))
	BEGIN 
		PRINT 'Executing: BUG_11500_NMHH_TUK_eseteben_nincs_szukseg_irat_intezkedes_ala_vetelere'
		update KRT_Parameterek set Ertek='0' where Nev='IRAT_INTEZKEDESREATVETEL_ELERHETO'
	END
GO