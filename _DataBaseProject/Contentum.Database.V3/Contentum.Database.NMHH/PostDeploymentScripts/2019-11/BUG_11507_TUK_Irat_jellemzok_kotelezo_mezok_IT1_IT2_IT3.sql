﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

DECLARE @IS_TUK char(1) 
SET @IS_TUK = (select TOP 1 ertek from KRT_Parameterek	where nev='TUK')

IF((@ORG_KOD ='NMHH') and (@IS_TUK ='1'))
	BEGIN 
	PRINT 'Executing: BUG_11507_TUK_Irat_jellemzok_kotelezo_mezok_IT1_IT2_IT3'
	update EREC_Obj_MetaAdatai set Opcionalis='1' where Targyszavak_Id in (select Id from EREC_TargySzavak where TargySzavak in ('IT1','IT2','IT3'))
	END
GO
