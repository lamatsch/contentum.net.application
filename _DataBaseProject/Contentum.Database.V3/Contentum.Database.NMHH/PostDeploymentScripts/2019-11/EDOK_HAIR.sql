﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD ='FPH')
BEGIN 
	PRINT 'Executing: EDOK_HAIR'
		
	declare @modulId uniqueidentifier
	set @modulId  = '4C9C4A03-2A06-EA11-80E0-00155D017B06'

	IF NOT EXISTS (Select 1 FROM INT_Modulok where Id = @modulId)
	BEGIN
		INSERT INTO INT_Modulok
		(Id, Org, Nev,Parancs,Gyakorisag,GyakorisagMertekegyseg,UtolsoFutas)
		VALUES
		(@modulId,'450b510a-7caa-46b0-83e3-18445c0c53a9', 'HAIR', 'Iktatas', NULL, NULL, NULL)
	END

declare @recordId uniqueidentifier
set @recordId = '6B5BE138-2A06-EA11-80E0-00155D017B06'
 
IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @recordId)
BEGIN
	INSERT INTO INT_Parameterek
	(Id, Org, Modul_id, Nev, Ertek, Karbantarthato)
	VALUES
	(@recordId, '450b510a-7caa-46b0-83e3-18445c0c53a9', @modulId, 'TECHNIKAI_IKTATOKONYV_IKTATOHELY', 'FPH901','0')
END

	set @recordId = '900FA44A-2A06-EA11-80E0-00155D017B06'

	IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @recordId)
	BEGIN
		INSERT INTO INT_Parameterek
		(Id, Org, Modul_id, Nev, Ertek, Karbantarthato)
		VALUES
		(@recordId, '450b510a-7caa-46b0-83e3-18445c0c53a9', @modulId, 'TECHNIKAI_USER', '54E861A5-36ED-44CA-BAA7-C287D125B309','0')
	END

	set @recordId = 'C5DD285C-2A06-EA11-80E0-00155D017B06'

	IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @recordId)
	BEGIN
		INSERT INTO INT_Parameterek
		(Id, Org, Modul_id, Nev, Ertek, Karbantarthato)
		VALUES
		(@recordId, '450b510a-7caa-46b0-83e3-18445c0c53a9', @modulId, 'TECHNIKAI_USER_SZERVEZET', 'BD00C8D0-CF99-4DFC-8792-33220B7BFCC6','0')
	END

	set @recordId = '2098D76E-2A06-EA11-80E0-00155D017B06'

	IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @recordId)
	BEGIN
		INSERT INTO INT_Parameterek
		(Id, Org, Modul_id, Nev, Ertek, Karbantarthato)
		VALUES
		(@recordId, '450b510a-7caa-46b0-83e3-18445c0c53a9', @modulId, 'HIBA_EMAIL', 'contentum@4ig.hu','0')
	END

set @recordId = 'A13504BD-8C0F-EA11-80E0-00155D017B06'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @recordId)
BEGIN
	INSERT INTO INT_Parameterek
	(Id, Org, Modul_id, Nev, Ertek, Karbantarthato)
	VALUES
	(@recordId, '450b510a-7caa-46b0-83e3-18445c0c53a9', @modulId, 'TECHNIKAI_ERKEZTETOKONYV_MEGKULJEL', 'FPH_HAIR','0')
END

DECLARE @KodCsoport_Id uniqueidentifier
SET @KodCsoport_Id = '899D51D9-C70B-EA11-80E0-00155D017B06'

	IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = @KodCsoport_Id)
		BEGIN 		
			insert into KRT_KodCsoportok(
			 BesorolasiSema
			,Id
			,Kod
			,Nev
			,Modosithato
			,Note
			) values (
			'0'
			,@KodCsoport_Id
			,'HAIR_ADAT_TIPUSOK'
			,'HAIR adattípusok'
			,'0'
			,NULL
			); 
		END


	declare @kodcsoportId uniqueidentifier
	set @kodcsoportId = '899D51D9-C70B-EA11-80E0-00155D017B06'

	set @recordId = '794DE5F2-C70B-EA11-80E0-00155D017B06'
	declare @Org uniqueidentifier
	set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

	IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
	BEGIN 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		) values (
		 @recordId
		,@Org
		,@kodcsoportId
		,'01'
		,'Kézbesítés értesítés'
		,'0'
		,NULL
		); 
	END 
	 
	set @recordId = '00E4850B-C80B-EA11-80E0-00155D017B06'
	IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
	BEGIN 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		) values (
		 @recordId
		,@Org
		,@kodcsoportId
		,'02'
		,'Partner adat szinkronizáció'
		,'0'
		,NULL
		); 
	END 	
END
GO

 
