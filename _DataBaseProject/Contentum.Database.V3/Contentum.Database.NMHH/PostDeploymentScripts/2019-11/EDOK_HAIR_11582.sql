
DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD ='FPH')
	BEGIN 
		PRINT 'HAIR csatolmány feltöltéshez szükséges paraméterek létrehozása'

		declare @RECORD_ID uniqueidentifier
	
		set @RECORD_ID = 'C785E1CF-A3B1-4C20-A1C0-5E15A4480735'
		IF NOT EXISTS(SELECT 1 FROM KRT_Parameterek where Id=@RECORD_ID)
			BEGIN
			INSERT INTO KRT_Parameterek (Id,Org,Karbantarthato,Nev,Ertek) values (@RECORD_ID,@ORG_ID,'0','HAIR.CSATOLMANY.URL','https://webadoteszt.budapest.hu/web_hair/hair/ords/b/tics/')
		END
		IF EXISTS(SELECT 1 FROM KRT_Parameterek where Id=@RECORD_ID)
			BEGIN
			UPDATE  KRT_Parameterek set Nev='HAIR.CSATOLMANY.URL',Ertek='https://webadoteszt.budapest.hu/web_hair/hair/ords/b/tics/' where Id=@RECORD_ID
			END
		END

		set @RECORD_ID = '80825F3B-3841-4C04-8967-C41620541403'
		IF NOT EXISTS(SELECT 1 FROM KRT_Parameterek where Id=@RECORD_ID)
			BEGIN
			INSERT INTO KRT_Parameterek (Id,Org,Karbantarthato,Nev,Ertek) values (@RECORD_ID,@ORG_ID,'0','HAIR.CSATOLMANY.USER','_EDOK_HAIR_usr')
			END
		IF EXISTS(SELECT 1 FROM KRT_Parameterek where Id=@RECORD_ID)
			BEGIN
			UPDATE  KRT_Parameterek  set Nev='HAIR.CSATOLMANY.USER',Ertek='_EDOK_HAIR_usr' where Id=@RECORD_ID
		END	
		
		set @RECORD_ID = '380C349E-E8F8-4D2B-A861-1CD6E3310A66'
		IF NOT EXISTS(SELECT 1 FROM KRT_Parameterek where Id=@RECORD_ID)
			BEGIN
			INSERT INTO KRT_Parameterek (Id,Org,Karbantarthato,Nev,Ertek) values (@RECORD_ID,@ORG_ID,'0','HAIR.CSATOLMANY.PWD','Ed0kHa1r2019')		
		END	
		IF EXISTS(SELECT 1 FROM KRT_Parameterek where Id=@RECORD_ID)
			BEGIN
			UPDATE  KRT_Parameterek set Nev='HAIR.CSATOLMANY.PWD',Ertek='Ed0kHa1r2019' where Id=@RECORD_ID		
		END		
GO



