DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 

IF @isTUK = '1'
BEGIN
	PRINT 'UPDATE IKTATOSZAM_FORMATUM'
	UPDATE KRT_Parameterek
	SET Ertek = '*U*$K.MegkulJelzes$-#U#*U*$K.Azonosito$-#U#*U*$U.Foszam$#U#*I*-$I.Alszam$#I#*U*/$K.Ev$#U#*P* ($P.Sorszam$)#P#'
	WHERE Id ='224E5D1E-96D6-E711-80C7-00155D027E9B'
END
