﻿PRINT 'Executing: BLG_11160_Rejtett_elektronikus_uzenetek_megjelenitese'
declare @FUNKCIO_KOD varchar(100)
set @FUNKCIO_KOD = 'eBeadvanyokOsszesCelRendszer'

IF NOT EXISTS (SELECT 1 from KRT_Funkciok WHERE Kod = @FUNKCIO_KOD)
	BEGIN	 
		INSERT INTO KRT_Funkciok (Kod,Nev) values(@FUNKCIO_KOD,'Rejtett hivatali kapus üzenetek megjelenítése')
	
		declare @FUNKCIO_ID uniqueidentifier
		set @FUNKCIO_ID = (select Id  from KRT_Funkciok where Kod = @FUNKCIO_KOD)
	
		declare @FEJLESZTO_SZEREPKOR_ID uniqueidentifier
		set @FEJLESZTO_SZEREPKOR_ID = (select Id from KRT_Szerepkorok where Nev = 'FEJLESZTO')
	
		INSERT INTO KRT_Szerepkor_Funkcio (Funkcio_id,Szerepkor_id) values(@FUNKCIO_ID,@FEJLESZTO_SZEREPKOR_ID)	
END
GO
