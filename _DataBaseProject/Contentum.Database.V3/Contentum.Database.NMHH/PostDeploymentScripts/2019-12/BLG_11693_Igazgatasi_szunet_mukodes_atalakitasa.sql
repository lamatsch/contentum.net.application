﻿PRINT 'Executing: BLG_11693_Igazgatasi_szunet_mukodes_atalakitasa'

declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @eUzenetDoktipusAzon_Id uniqueidentifier
set @eUzenetDoktipusAzon_Id = '12568469-706A-417A-A60B-EDADD4A862F5'

IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @eUzenetDoktipusAzon_Id)
BEGIN
	print 'Add KRT_KodCsoportok - EUZENET_DOKTIPAZON_LETOLT_IGSZUNETBEN'

	insert into KRT_KodCsoportok
	(
	     Id
		,Kod
		,Nev
		,Modosithato
		,Note
		,BesorolasiSema
	) 
	values
	(
		@eUzenetDoktipusAzon_Id
		,'EUZENET_DOKTIPAZON_LETOLT_IGSZUNETBEN'
		,'Igazgatási szünetben letöltendő üzenetek dokumentum típus azonosítói'
		,'1'
		,'Igazgatási szünet alatt is letöltendő üzenetek dokumentum típus azonosítói. A kódcsoport alá felvett kódtárelemek kódjaival megegyező dokumentum azonosítójú üzentek kerülnek letöltésre.'
		,'0'
	)

END

declare @eUzenetFelado_Id uniqueidentifier
set @eUzenetFelado_Id = '748987A1-3651-45E0-954B-0D8A023552AE5'

IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @eUzenetFelado_Id)
BEGIN
	print 'Add KRT_KodCsoportok - EUZENET_FELADO_LETOLT_IGSZUNETBEN'

	insert into KRT_KodCsoportok
	(
	     Id
		,Kod
		,Nev
		,Modosithato
		,Note
		,BesorolasiSema
	) 
	values
	(
		@eUzenetFelado_Id
		,'EUZENET_FELADO_LETOLT_IGSZUNETBEN'
		,'Igazgatási szünetben letöltendő üzenetek feladói'
		,'1'
		,'Igazgatási szünet alatt is letöltendő üzenetek feladói. A kódcsoport alá felvett kódtárelemek kódjaiban kell megadni a feladó hivatal KRID-ját.'
		,'0'
	)

END

declare @kodtarFuggosegId uniqueidentifier
set @kodtarFuggosegId = '56C0FD1B-85D9-476F-BA29-6652B058F3EA'

IF NOT EXISTS (SELECT 1 FROM KRT_KodtarFuggoseg WHERE Id = @kodtarFuggosegId)
BEGIN 
  Print 'INSERT KRT_KodtarFuggosegek - EUZENET_DOKTIPAZON_LETOLT_IGSZUNETBEN - EUZENET_FELADO_LETOLT_IGSZUNETBEN' 

  insert into KRT_KodtarFuggoseg
  (
    Id,
    Org,
    Vezerlo_KodCsoport_Id,
    Fuggo_KodCsoport_Id,
    Adat,
    Aktiv
  ) 
  values (
    @kodtarFuggosegId,
    @org,
    @eUzenetDoktipusAzon_Id,
    @eUzenetFelado_Id,
    '',
    '1'
  ); 
   
END

GO
