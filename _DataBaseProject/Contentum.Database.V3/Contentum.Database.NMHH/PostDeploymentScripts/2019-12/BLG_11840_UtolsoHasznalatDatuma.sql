﻿CREATE FUNCTION [dbo].[fn_GetPartnerUtolsoHasznalat]
(
	@PartnerID uniqueidentifier
)
RETURNS datetime
AS
BEGIN
	
	return (select max(LetrehozasIdo) from EREC_KuldKuldemenyek where Partner_Id_Bekuldo = @PartnerID)

END
GO