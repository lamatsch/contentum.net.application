﻿
DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD = 'NMHH')
BEGIN
	PRINT 'BUG_11233: Minden technikai post�z�hoz hozz� kell rendelni a post�z�s elv�gz�s�hez sz�ks�ges funkcionalit�sokkal rendelkez� szerepk�rt.' 
	DECLARE @POSTAZO_SZEREPKOR_NEV nvarchar(100) 
	SET @POSTAZO_SZEREPKOR_NEV = 'POSTAZO'
	DECLARE @POSTAZO_SZEREPKOR_ID uniqueidentifier
	SET @POSTAZO_SZEREPKOR_ID = 'D966D634-9C0E-456B-BFB2-AF00573905A3'

	IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkorok WHERE Id = @POSTAZO_SZEREPKOR_ID)
	BEGIN
		PRINT 'Creating POSTAZO in SzerepKorok table' 
		INSERT INTO KRT_Szerepkorok (Id,Nev,ErvKezd,Org) VALUES(@POSTAZO_SZEREPKOR_ID,@POSTAZO_SZEREPKOR_NEV,GETDATE(),@ORG_ID)
	END	 

	DECLARE @FUNKCIO_KOD nvarchar(100) 
	PRINT 'Starting checking interested Szerepkor-s to Funkcio-s' 	

	SET @FUNKCIO_KOD = 'KimenoKuldemenySztorno'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('98617706-68F7-E611-9444-00155D1098F2',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END
	
	SET @FUNKCIO_KOD = 'IratPeldanyAtadas'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('88D112C1-EAEC-488E-ADF3-0D8C3F963510',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KuldemenyView'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('99D0F132-8B27-439A-A68B-A211F9A52E81',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'IratPeldanyAtvetel'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('330956F6-836F-40AB-BC0A-6CBD44B4B003',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'HelyettesitesekList'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('80F7A41C-1019-DE11-BD91-001EC9E754BC',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KimenoKuldemenyekList'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('42467DFB-3576-4805-8045-E1EC991C5F76',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'MegbizasokList'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('14537356-1019-DE11-BD91-001EC9E754BC',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'Atadandok'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD )
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('B14F9F01-4812-4CE8-AB64-2BDF711DA481',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD and KRT_Funkciok.Id ='B8C2330E-CD9B-423C-A72C-3140313C65DF'),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'Atveendok'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('97393C3F-095D-4FBC-ADAD-48198138EA4E',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'Expedialas'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('F22011A2-53D2-441F-9EE0-3817710ECCDD',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KimenoKuldemenyFelvitel'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('0CBB50E7-F9ED-4DC5-A1A3-4EBA749CB11F',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KuldTertivevenyekList'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('D3FC8CC6-BB32-E011-889B-001EC9E754BC',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'Postazas'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('C9F0D84C-C65C-40BC-8654-3F20DCFA36BC',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KuldTertivevenyErkeztetes'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('30726F4D-BB32-E011-889B-001EC9E754BC',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KRT_RagszamSavokInvalidate'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('45754A9A-FFDF-4B5B-9302-40F4666B8617',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END
 
 	SET @FUNKCIO_KOD = 'KRT_RagszamSavokKezeles'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('A19015A6-DCEE-4F29-B765-4919AFEDFB67',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KRT_RagszamSavokNew'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('D0791846-A962-4AC3-B92B-3626B38F4F0D',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KRT_RagszamSavokList'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('371E8D42-B693-4FF0-8009-84879B9CB867',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KRT_RagszamSavokView'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('0FB73FD4-DA89-4808-9757-EBC273ADDB0B',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KRT_RagszamSavokModify'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('86C01C9E-71E4-42ED-8806-96A9BB248989',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'ePdfTertivevenyKezeles'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('6E3C9DCC-B391-4A6E-82D2-8AED4C3460EC',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END


	PRINT 'Linking users to POSTAZO szerepkor.'
	DECLARE @LINK_ID uniqueidentifier
	DECLARE @NAME nvarchar(100)
	DECLARE @TIPUS nvarchar(64)
	
	SET @TIPUS = '2'
	
	SET @LINK_ID = 'D2499E62-7B6D-45C7-B18D-E3FEB3669197'
	SET @NAME = 'Debrecen Technikai Post�z�'
	
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN		 		
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	 

	SET @LINK_ID = 'F25E0759-980D-4417-A1EC-5700B81F10B0'
	SET @NAME = 'Miskolc Technikai Post�z�'
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN	
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	

	SET @LINK_ID = 'AAA59999-73F2-44AD-BAD8-94FDB755C105'
	SET @NAME = 'Ostrom Technikai Post�z�'
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN		
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	

	SET @LINK_ID = 'DFD830FA-DF15-4132-80B2-54FA4D4C2FDC'
	SET @NAME = 'P�cs Technikai Post�z�'
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN		
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	

	SET @LINK_ID = '51B0671E-2CDA-437F-99AE-3DC78B751BE3'
	SET @NAME = 'Reviczky Technikai Post�z�'
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN		
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	

	SET @LINK_ID = 'F3ADF785-8CDA-4C01-9892-E82DF25BA753'
	SET @NAME = 'Sopron Technikai Post�z�'
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN	
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	

	SET @LINK_ID = '58308079-95D3-450E-9A81-37C9F7F9BF5F'
	SET @NAME = 'Szeged Technikai Post�z�'
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN		
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	

	SET @LINK_ID = '3BDD6509-F694-418E-8400-36734D4AE053'
	SET @NAME = 'Visegr�di Technikai Post�z�'
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN		
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	
END
GO
