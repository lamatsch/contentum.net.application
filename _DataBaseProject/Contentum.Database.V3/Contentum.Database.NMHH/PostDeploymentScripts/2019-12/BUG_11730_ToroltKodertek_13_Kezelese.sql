﻿PRINT 'Executing: BUG_11730_ToroltKodertek_13_Kezelese'
-- BUG_11730 13-as törölt kódérték javítás

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

-- EXPEDIALAS_MODJA_DEFAULT állítása 290-re (Helyben marad) NMHH-ban és Csepelen

DECLARE @record_id uniqueidentifier

if (@org_kod = 'NMHH') or (@org_kod = 'CSPH')
BEGIN
DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
	if (@isTUK='1')
	BEGIN
		print 'EXPEDIALAS_MODJA_DEFAULT állítása 110-re (TUK Helyben maradó) NMHH-ban'	
		Print @org_kod 
		Print 'isTUK:' +@isTUK	
		SET @record_id = null
		SET @record_id = (select Id from KRT_Parameterek
				where nev='EXPEDIALAS_MODJA_DEFAULT') 
		Print '@record_id='+convert(NVARCHAR(100),@record_id) 

		if @record_id IS NOT NULL 
		BEGIN 
			 Print 'UPDATE 110-re'
			 UPDATE KRT_Parameterek
			 SET Ertek='110'
			 WHERE Id=@record_id 
		 END
	END
	ELSE
	BEGIN
		print 'EXPEDIALAS_MODJA_DEFAULT állítása 290-re (Helyben marad) NMHH-ban és Csepelen'	
		Print @org_kod 	
		SET @record_id = null
		 SET @record_id = (select Id from KRT_Parameterek
				where nev='EXPEDIALAS_MODJA_DEFAULT') 
		Print '@record_id='+convert(NVARCHAR(100),@record_id) 

		if @record_id IS NOT NULL 
		BEGIN 
			 Print 'UPDATE 290-re'
			 UPDATE KRT_Parameterek
			 SET Ertek='290'
			 WHERE Id=@record_id 
		 END
	END
END
-- KULDEMENY_KULDES_MODJA kódcsoport 13-as,45-ös kód kezelése BOPMH-ban és FPH-n

if (@org_kod = 'FPH') or (@org_kod = 'BOPMH')
BEGIN
	print 'KULDEMENY_KULDES_MODJA kódcsoport 13-as kód visszaérvényesítése BOPMH-ban és FPH-n'	
	Print @org_kod 	
	SET @record_id = null 
	SET @record_id = (select kt.id from KRT_KodTarak as kt
						inner join KRT_KodCsoportok as kcs on kt.KodCsoport_Id=kcs.id 
						where  kcs.Kod='KULDEMENY_KULDES_MODJA'
						and kt.id='F9B80F0D-FE46-4921-8B66-359B0E384AEF' and kt.ervvege <'4700-12-31 00:00:00.000')
	
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 

	if @record_id IS NOT NULL 
	BEGIN 
		 Print 'UPDATE 13-as visszaérvényesítés'
		 UPDATE KRT_KodTarak
		 SET Ervvege='4700-12-31 00:00:00.000'
		 WHERE Id=@record_id 
	END
	
	if (@org_kod = 'FPH')
	BEGIN
		print 'KULDEMENY_KULDES_MODJA kódcsoport 45-ös kód érvényesítelenítése FPH-n'	
		SET @record_id = null 
		SET @record_id = (select kt.id from KRT_KodTarak as kt
							inner join KRT_KodCsoportok as kcs on kt.KodCsoport_Id=kcs.id 
							where  kcs.Kod='KULDEMENY_KULDES_MODJA'
							and kt.id='586C4882-681A-E911-9482-00155DF11484' and kt.ervvege ='4700-12-31 00:00:00.000') 
		
		Print '@record_id='+convert(NVARCHAR(100),@record_id) 

		if @record_id IS NOT NULL 
		BEGIN 
			 Print 'UPDATE érvénytelenítés (45)'
			 UPDATE KRT_KodTarak
			 SET Ervvege='2019-12-01 00:00:00.000'
			 WHERE Id=@record_id 
		END
		
		print 'Iratpéldány UPDATE Kuldesmod 45-ről 13-ra'	
		update EREC_PldIratPeldanyok
		set KuldesMod = '13'
		where KuldesMod='45'

	END
END


 GO
