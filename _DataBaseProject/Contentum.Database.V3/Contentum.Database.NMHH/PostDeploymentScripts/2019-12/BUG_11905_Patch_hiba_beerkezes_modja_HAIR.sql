﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD  != 'FPH')
	BEGIN 
	PRINT 'Executing: BUG_11905_Patch_hiba_beerkezes_modja_HAIR'
	DECLARE @KCS_ID_KULDEMENY_KULDES_MODJA UNIQUEIDENTIFIER
	SELECT @KCS_ID_KULDEMENY_KULDES_MODJA = Id FROM KRT_KODCSOPORTOK WHERE KOD ='KULDEMENY_KULDES_MODJA'

	IF @KCS_ID_KULDEMENY_KULDES_MODJA IS NOT NULL
		BEGIN
			DECLARE @KODTAR_ID_KULDKUDLMOD_HAIR UNIQUEIDENTIFIER
			SET @KODTAR_ID_KULDKUDLMOD_HAIR = '62BC4E4D-6A16-4078-B7D4-D27D7F3B2DDA'

			DECLARE @ORG UNIQUEIDENTIFIER
			SET @ORG = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

			IF EXISTS (SELECT 1 FROM KRT_KodTarak WHERE Id = @KODTAR_ID_KULDKUDLMOD_HAIR)
				BEGIN  	
					DELETE FROM KRT_KODTARAK WHERE Id = @KODTAR_ID_KULDKUDLMOD_HAIR				
				END 
		END
	END
GO
