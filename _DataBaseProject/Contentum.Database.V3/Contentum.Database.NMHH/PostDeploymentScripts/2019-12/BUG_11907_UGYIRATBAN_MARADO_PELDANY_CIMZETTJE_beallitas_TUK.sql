﻿PRINT 'Executing: BUG_11907_UGYIRATBAN_MARADO_PELDANY_CIMZETTJE_beallitas_TUK'
	-- BUG_11907 - UGYIRATBAN_MARADO_PELDANY_CIMZETTJE Rendszerparaméter állítása TÜK-ben
-- Elég ha egyszer lefut és csak TUK-on kell
-- Paraméter Új értéke: TÜK Irattár

DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
if  (@isTUK = '1')
BEGIN

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Nev='UGYIRATBAN_MARADO_PELDANY_CIMZETTJE') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NOT NULL 
	BEGIN 
		 Print 'UPDATE UGYIRATBAN_MARADO_PELDANY_CIMZETTJE = TÜK Irattár'
		 UPDATE KRT_Parameterek
		 SET Ertek='TÜK Irattár'
		 WHERE Id=@record_id 
	 END
END

--------------------------------------------------------------------------------
------------------------------ BUG_11907 vége ----------------------------------
 GO
