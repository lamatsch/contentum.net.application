﻿PRINT 'Executing: BUG_11930_ElektronikusIrat_Sorrend'
--WRITE YOUR SCRIPT HERE. PREPARE TO BE RUN MULTIPLE TIMES!
-- BUG_11930: ELSODLEGES_ADATHORDOZO kódcsoportba  09 - elektronikus irat megjelenitesi sorrend állítás
--            Adathordozó típusa kódcsoportok átnevezése

Print 'ELSODLEGES_ADATHORDOZO kódcsoport'

 
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_KodCsoportok
			where Id='B833BDF6-C802-40E7-B9EA-46D9B49F579F') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'ERROR: Nincs ELSODLEGES_ADATHORDOZO kódcsoport vagy nem jó az ID-ja' 

END 
ELSE 
BEGIN 
    Print '09 - elektronikus irat kódtárérték' 
	SET @record_id= null
    SET @record_id = (select Id from KRT_KodTarak
		where  Id='4B8C063D-1692-E711-80C2-00155D020D7E') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
		
		Print 'INSERT' 
		insert into KRT_KodTarak(
			KodCsoport_Id
		,Tranz_id
		,Stat_id
		,Org
		,Id
		,Kod
		,Nev
		,Sorrend
		,Modosithato
		,Ervkezd
		) values (
		'B833BDF6-C802-40E7-B9EA-46D9B49F579F'
		,null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'4B8C063D-1692-E711-80C2-00155D020D7E'
		,'09'
		,'elektronikus irat'
		,'03'
		,'0'
		,'2017-09-05'
		); 
	END 
	ELSE
	BEGIN
		Print 'UPDATE'
		UPDATE KRT_KodTarak
			SET Sorrend ='03'
			WHERE Id=@record_id 
	END

END
 GO
