﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 
declare @recordId uniqueidentifier

IF(@ORG_KOD ='FPH')
	BEGIN 
	PRINT 'Executing: EDOK_HAIR_11520'
	PRINT 'Creating GFO_KOD Kodcsoport'
	set @recordId = '35C41BAB-1BF4-4199-B6BE-17682BBB1A6E'
	IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @recordId)
	BEGIN 	
		insert into KRT_KodCsoportok(
		 Id	 
		,Kod
		,Nev
		,Modosithato
		,BesorolasiSema
		) values (
		 @recordId
		,'GFO_KOD'
		,'HAIR Gazdálkodási forma'		 
		,'0'	
		,'0'
		); 

		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','1','Jogi személyiségű vállalkozás','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','2','Jogi személyiség nélküli vállalkozás','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','3','Költségvetési szervek és költségvetési rend szerint gazdálkodó szervek','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','5','Jogi személyiségű nonprofit szervezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','6','Jogi személyiség nélküli nonprofit szervezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','7','Átmeneti és megszűnt gazdálkodási formák','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','9','Technikai kód','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','11','Gazdasági társaság','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','12','Szövetkezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','13','Egyéb jogi személyiségű vállalkozás','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','14','Jogi személyiségű európai gazdasági vállalkozások','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','15','Szövetkezet (folytatás)','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','21','Jogi személyiség nélküli gazdálkodó szervezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','22','Gazdasági tevékenységet végző jogalany','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','23','Önálló vállalkozó','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','31','Központi költségvetési irányító és költségvetési szervek','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','32','Helyi önkormányzati költségvetési irányító és költségvetési szervek','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','34','Köztestületi költségvetési irányító és költségvetési szervek','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','35','Országos nemzetiségi önkormányzati költségvetési irányító és költségvetési szervek','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','36','Területfejlesztési tanácsok','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','37','Helyi nemzetiségi önkormányzati költségvetési irányító és költségvetési szervek','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','38','Költségvetési rend szerint gazdálkodó egyéb szervek','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','39','Költségvetési technikai kódok','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','51','Párt, érdekképviselet, szövetség, az egyesület jogi személyiséggel rendelkező szervezeti egysége','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','52','Az egyesület egyéb formái','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','54','Köztestület','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','55','Egyházi jogi személy','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','56','Alapítvány, jogi személyiségű intézménye és szervezeti egysége','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','57','Nonprofit gazdasági társaság','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','58','Pénztárszervezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','59','Egyéb, jogi személyiségű nonprofit szervezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','61','Jogi személyiség nélküli nonprofit szervezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','69','Egyéb, jogi személyiség nélküli nonprofit szervezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','91','Alap','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','92','Munkavállalói résztulajdonosi program szervezete','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','93','Egyéb adóalany','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','94','Külföldi diplomáciai, konzuli testület, egyéb területen kívüli szervezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','96','Háztartás','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','113','Korlátolt felelősségű társaság','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','114','Részvénytársaság','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','116','Közkereseti társaság','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','117','Betéti társaság','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','121','Szociális szövetkezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','122','Takarék- és hitelszövetkezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','123','Iskola szövetkezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','124','Agrárgazdasági szövetkezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','126','Biztosító és viszontbiztosító szövetkezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','129','Egyéb szövetkezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','131','Ügyvédi iroda','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','132','Szabadalmi ügyvivő iroda','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','133','Vízitársulat','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','135','Erdőbirtokossági társulat','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','136','Végrehajtó iroda','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','137','Közjegyzői iroda','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','141','Európai részvénytársaság (SE)','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','142','Európai szövetkezet (SCE)','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','143','Magyarországi székhelyű európai gazdasági egyesülés','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','144','Európai területi együttműködési csoportosulás (EGTC)','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','151','Közérdekű nyugdíjas szövetkezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','214','Víziközmű-társulat','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','226','Külföldi vállalkozás magyarországi fióktelepe','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','227','Külföldi székhelyű európai gazdasági egyesülés magyarországi telephelye','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','228','Egyéni cég','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','231','Egyéni vállalkozó','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','232','Egyéb önálló vállalkozó','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','233','Adószámmal rendelkező magánszemély','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','311','Központi költségvetési irányító szerv','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','312','Központi költségvetési szerv','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','321','Helyi önkormányzat','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','322','Helyi önkormányzati költségvetési szerv','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','325','Önkormányzati hivatal (költségvetési szerv)','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','327','Helyi önkormányzatok társulása','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','328','Területfejlesztési önkormányzati társulás','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','341','Köztestületi költségvetési irányító szerv','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','342','Köztestületi költségvetési szerv','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','351','Országos nemzetiségi önkormányzat','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','352','Országos nemzetiségi önkormányzati költségvetési szerv','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','353','Országos nemzetiségi önkormányzatok társulása','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','362','Térségi fejlesztési tanács','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','371','Helyi nemzetiségi önkormányzat','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','372','Helyi nemzetiségi önkormányzati költségvetési szerv','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','373','Helyi nemzetiségi önkormányzatok társulása','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','381','Költségvetési rend szerint gazdálkodó, központi költségvetési körbe tartozó szerv','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','382','Költségvetési rend szerint gazdálkodó, önkormányzati költségvetési körbe tartozó szerv','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','391','Központi kezelésű előirányzat','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','392','Fejezeti kezelésű előirányzat','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','393','Elkülönített állami pénzalap','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','394','Nyugdíjbiztosítási Alap','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','395','Egészségbiztosítási Alap','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','396','Nemzeti Együttműködési Alap','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','397','Járási hivatalok','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','511','Párt','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','512','Szakszervezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','513','Egyéb munkavállalói érdekképviselet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','514','Munkáltatói, tulajdonosi érdekképviselet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','515','Országos sportági szakszövetség','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','516','Egyéb sportszövetség','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','517','Egyéb szövetség','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','519','Egyesület jogi személyiséggel rendelkező szervezeti egysége','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','521','Sportegyesület','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','524','Kölcsönös biztosító egyesület','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','525','Vallási egyesület','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','526','Polgárőr egyesület','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','528','Nemzetiségi egyesület','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','529','Egyéb egyesület','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','541','Kamara','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','549','Egyéb köztestület','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','551','Bevett egyház','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','552','Bevett egyház elsődlegesen közfeladatot ellátó belső egyházi jogi személye  ','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','555','Bevett egyház elsődlegesen vallási tevékenységet végző belső egyházi jogi személye','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','556','Bejegyzett egyház','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','557','Nyilvántartásba vett egyház','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','558','Bejegyzett egyház és Nyilvántartásba vett egyház belső egyházi jogi személye','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','559','Egyéb egyházi szervezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','561','Közalapítvány','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','562','Közalapítvány önálló intézménye','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','563','Egyéb alapítvány önálló intézménye','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','564','Pártalapítvány','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','565','Alapítvány jogi személyiséggel rendelkező szervezeti egysége','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','566','Kormány által létrehozott alapítvány','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','567','Vagyonkezelő alapítvány','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','568','Közérdekű vagyonkezelő alapítvány','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','569','Egyéb alapítvány','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','572','Nonprofit korlátolt felelősségű társaság','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','573','Nonprofit részvénytársaság','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','574','Európai kutatási infrastruktúráért felelős konzorcium (ERIC)','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','575','Nonprofit közkereseti társaság','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','576','Nonprofit betéti társaság','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','581','Önkéntes kölcsönös biztosító pénztár','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','582','Magánnyugdíjpénztár','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','583','Vegyes nyugdíjpénztár','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','591','Egyesülés','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','593','Lakásszövetkezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','595','Nemzeti otthonteremtési közösség','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','599','Egyéb, jogi személyiségű nonprofit szervezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','611','Külföldiek magyarországi közvetlen kereskedelmi képviselete','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','692','Társasház','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','693','Építőközösség','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','699','Egyéb, jogi személyiség nélküli nonprofit szervezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','731','Gazdasági munkaközösség','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','732','Közös vállalat','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','734','Polgári jogi társaság','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','735','Művészeti alkotóközösség','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','736','Közhasznú társaság','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','737','Egyes jogi személyek vállalata','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','738','Egyéb, máshova nem sorolt vállalat','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','739','Egyéb megszűnt gazdálkodási forma','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','741','Foglalkoztatási szövetkezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','742','Állami vállalat','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','915','Befektetési alap','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','916','Országos Betétbiztosítási Alap','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','918','Pénztárak garanciaalapja','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','919','Egyéb alap','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','921','Munkavállalói résztulajdonosi program szervezete','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','931','Egyéb adóalany','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','932','Adószámmal rendelkező külföldi vállalkozás','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','941','Külföldi diplomáciai, konzuli testület','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','942','Egyéb területen kívüli szervezet','0')
		insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values (newid(),'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E','450B510A-7CAA-46B0-83E3-18445C0C53A9','961','Háztartás','0')
	END 
END


IF(@ORG_KOD ='FPH')
	BEGIN 
		PRINT 'HAIR partner modositashoz szükséges paraméterek létrehozása'
		declare @RECORD_ID uniqueidentifier
		
		set @RECORD_ID = '09EB60E4-32CA-4291-BCE0-6094BAB161BD'
		IF NOT EXISTS(SELECT 1 FROM KRT_Parameterek where Id=@RECORD_ID)
			BEGIN
			INSERT INTO KRT_Parameterek (Id,Org,Karbantarthato,Nev,Ertek) values (@RECORD_ID,@ORG_ID,'0','HAIR.PARTNER.URL','https://webadoteszt.budapest.hu/web_hair/hair/ords/b/tado/')
			END

		set @RECORD_ID = '99CB0204-29AF-461E-9364-1891EBA2C639'
		IF NOT EXISTS(SELECT 1 FROM KRT_Parameterek where Id=@RECORD_ID)
			BEGIN
			INSERT INTO KRT_Parameterek (Id,Org,Karbantarthato,Nev,Ertek) values (@RECORD_ID,@ORG_ID,'0','HAIR.PARTNER.USER','_EDOK_HAIR_usr')
			END
		
		set @RECORD_ID = '09B75E0E-BDAA-4C9A-81A4-625120DB90F4'
		IF NOT EXISTS(SELECT 1 FROM KRT_Parameterek where Id=@RECORD_ID)
			BEGIN
			INSERT INTO KRT_Parameterek (Id,Org,Karbantarthato,Nev,Ertek) values (@RECORD_ID,@ORG_ID,'0','HAIR.PARTNER.PWD','Ed0kHa1r2019')
			END
	END
GO
