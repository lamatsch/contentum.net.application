DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 
declare @recordId uniqueidentifier

IF(@ORG_KOD ='FPH')
	BEGIN 
	IF NOT EXISTS (SELECT 1 FROM KRT_KODTARFUGGOSEG WHERE ID='3729E789-3820-40EA-8DB2-6169A31ACF12')
		BEGIN
			PRINT 'Kódtár Függőség összerendelés CÉGTIPUS és HAIR Gazdálkodási forma között'
			INSERT [dbo].[KRT_KodtarFuggoseg] ([Id], [Org], [Vezerlo_KodCsoport_Id], [Fuggo_KodCsoport_Id], [Adat], [Aktiv]) 
			VALUES					(N'3729E789-3820-40EA-8DB2-6169A31ACF12', N'450B510A-7CAA-46B0-83E3-18445C0C53A9', N'35C41BAB-1BF4-4199-B6BE-17682BBB1A6E', N'195E7763-932B-4E2D-9672-CCE1ABEFE657', N'{}', N'1')
	END
END