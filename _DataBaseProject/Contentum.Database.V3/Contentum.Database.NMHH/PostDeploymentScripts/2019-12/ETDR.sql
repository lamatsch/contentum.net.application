﻿PRINT 'Executing: ETDR'

DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @modulId uniqueidentifier
set @modulId = '6198C759-B1FD-49E7-ABA8-FCD995544779'

IF NOT EXISTS (Select 1 FROM INT_Modulok where Id = @modulId)
BEGIN
	PRINT 'INSERT INTO INT_Modulok: ETDR' 
	INSERT INTO INT_Modulok
	(Id, Nev)
	VALUES
	(@modulId,'ETDR')
END

declare @parameterId uniqueidentifier

set @parameterId = '8AD1360D-D016-41FC-8461-9C8DE1842556'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @parameterId)
BEGIN
  PRINT 'INSERT INTO INT_Parameterek: ETDR_LoginName'
  INSERT INTO INT_Parameterek
  (Id, Modul_id, Nev, Ertek, Karbantarthato, Note)
  VALUES
  (@parameterId, @modulId, 'ETDR_LoginName', '', '1', '')
END


set @parameterId = 'BB97E183-419D-4046-90E9-620E333151E8'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @parameterId)
BEGIN
  PRINT 'INSERT INTO INT_Parameterek: ETDR_LoginPassword'
  INSERT INTO INT_Parameterek
  (Id, Modul_id, Nev, Ertek, Karbantarthato, Note)
  VALUES
  (@parameterId, @modulId, 'ETDR_LoginPassword', '', '1', '')
END


set @parameterId = '2FAB061F-550D-4B83-98A7-5F94874C19E3'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @parameterId)
BEGIN
  PRINT 'INSERT INTO INT_Parameterek: ETDR_ID'
  INSERT INTO INT_Parameterek
  (Id, Modul_id, Nev, Ertek, Karbantarthato, Note)
  VALUES
  (@parameterId, @modulId, 'ETDR_ID', '', '1', '')
END


set @parameterId = 'B8D9580A-37CD-4FB9-88F3-3A7FF7C44988'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @parameterId)
BEGIN
  PRINT 'INSERT INTO INT_Parameterek: ETDR_LoginIDExpiryTimeInSeconds'
  INSERT INTO INT_Parameterek
  (Id, Modul_id, Nev, Ertek, Karbantarthato, Note)
  VALUES
  (@parameterId, @modulId, 'ETDR_LoginIDExpiryTimeInSeconds', 12*3600, '1', '')
END


set @parameterId = '630B340C-3C7D-4C7F-B01A-7978679D65E6'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @parameterId)
BEGIN
  PRINT 'INSERT INTO INT_Parameterek: ETDR_ErkeztetoKonyvMegKulJel'
  INSERT INTO INT_Parameterek
  (Id, Modul_id, Nev, Ertek, Karbantarthato, Note)
  VALUES
  (@parameterId, @modulId, 'ETDR_ErkeztetoKonyvMegKulJel', '', '1', 'Ez alapján kerül meghatározásra a tárgyévi érkeztető könyv (EREC_IraIktatoKonyvek: Ev = Year(getDate()), MegkulJelzes = a megadott paraméter, IktatoErkezteto = ''E''), ami bekerül érkeztetéskor az IraIktatokonyv_Id mezőbe')
END


set @parameterId = '4740070C-5872-4398-80FC-4D0D5CA576B3'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @parameterId)
BEGIN
  PRINT 'INSERT INTO INT_Parameterek: ETDR_IktatoKonyvIktatohely'
  INSERT INTO INT_Parameterek
  (Id, Modul_id, Nev, Ertek, Karbantarthato, Note)
  VALUES
  (@parameterId, @modulId, 'ETDR_IktatoKonyvIktatohely', '', '1', 'Ez alapján kerül meghatározásra a tárgyévi iktató könyv (EREC_IraIktatoKonyvek: Ev = Year(getDate()), Iktatohely = a megadott paraméter, IktatoErkezteto = ''I''), ami bekerül iktatáskor (Add_Iktatas_By_Ugyf) a _EREC_IraIktatoKonyvek_Id paraméterbe')
END


set @parameterId = '394F8130-AFDF-421D-8B23-1305C3482275'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @parameterId)
BEGIN
  PRINT 'INSERT INTO INT_Parameterek: ETDR_ITSZ'
  INSERT INTO INT_Parameterek
  (Id, Modul_id, Nev, Ertek, Karbantarthato, Note)
  VALUES
  (@parameterId, @modulId, 'ETDR_ITSZ', '', '1', 'Ez alapján kerül meghatározásra az irattári tételszám (EREC_IraIrattartiTetelek.Ugykor valamely értéke)')
END


set @parameterId = 'EF4D04AD-82E6-46B9-8D04-44D7022FC901'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @parameterId)
BEGIN
  PRINT 'INSERT INTO INT_Parameterek: ETDR_Ugytipus'
  INSERT INTO INT_Parameterek
  (Id, Modul_id, Nev, Ertek, Karbantarthato, Note)
  VALUES
  (@parameterId, @modulId, 'ETDR_Ugytipus', '', '1', 'Ez alapján kerül meghatározásra az irattári tételszámhoz tartozó ügytípusok közül a beállítandó')
END


--Admin
set @parameterId = '7DC65A5F-644D-4E41-931C-127C28DBA814'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @parameterId)
BEGIN
  PRINT 'INSERT INTO INT_Parameterek: ETDR_CONTENTUM_TECHNIKAI_USER'
  INSERT INTO INT_Parameterek
  (Id, Modul_id, Nev, Ertek, Karbantarthato, Note)
  VALUES
  (@parameterId, @modulId, 'ETDR_CONTENTUM_TECHNIKAI_USER', '54E861A5-36ED-44CA-BAA7-C287D125B309', '1', 'ExecParam-ban használt technikai felhasználó.')
END


set @parameterId = '3BA5ED25-AC8A-4DFD-A654-E9F4800E993E'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @parameterId)
BEGIN
  PRINT 'INSERT INTO INT_Parameterek: ETDR_CONTENTUM_TECHNIKAI_USER_SZERVEZET'
  INSERT INTO INT_Parameterek
  (Id, Modul_id, Nev, Ertek, Karbantarthato, Note)
  VALUES
  (@parameterId, @modulId, 'ETDR_CONTENTUM_TECHNIKAI_USER_SZERVEZET', '', '1', 'ExecParam-ban használt technikai felhasználó szervezete.')
END


GO

--------------------------------------------------------------------------------------------------------------------------------------

DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @ETDR_IKTTIP_Id uniqueidentifier
set @ETDR_IKTTIP_Id = '929A92DA-BB1D-4CAB-9186-EFA2E6C8C74C'

IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @ETDR_IKTTIP_Id)
BEGIN
	print 'INSERT INTO KRT_KodCsoportok: ETDR_IKTTIP'

	insert into KRT_KodCsoportok
	(
	     Id
		,Kod
		,Nev
		,Modosithato
		,Note
		,BesorolasiSema
	) 
	values
	(
		@ETDR_IKTTIP_Id
		,'ETDR_IKTTIP'
		,'ÉTDR iktatás típus'
		,'1'
		,''
		,'0'
	)

END

declare @IRATTIPUS_Id uniqueidentifier
set @IRATTIPUS_Id = (select Id from KRT_KodCsoportok Where Kod='IRATTIPUS' and GETDATE() between ErvKezd and ErvVege)

declare @kodtarFuggosegId uniqueidentifier
set @kodtarFuggosegId = 'FEC3C03B-502C-4EB7-BC71-0B0123486512'

IF NOT EXISTS (SELECT 1 FROM KRT_KodtarFuggoseg WHERE Id = @kodtarFuggosegId)
BEGIN 
  Print 'INSERT INTO KRT_KodtarFuggosegek: ETDR_IKTTIP - IRATTIPUS' 

  insert into KRT_KodtarFuggoseg
  (
    Id,
    Org,
    Vezerlo_KodCsoport_Id,
    Fuggo_KodCsoport_Id,
    Adat,
    Aktiv
  ) 
  values (
    @kodtarFuggosegId,
    @ORG_ID,
    @ETDR_IKTTIP_Id,
    @IRATTIPUS_Id,
    '',
    '1'
  ); 
   
END

GO

--------------------------------------------------------------------------------------------------------------------------------------

DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @ETDR_KULDMOD_Id uniqueidentifier
set @ETDR_KULDMOD_Id = 'CAAE354C-5045-47D0-B47F-94C67050C6C2'

IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @ETDR_KULDMOD_Id)
BEGIN
	print 'INSERT INTO KRT_KodCsoportok: ETDR_KULDMOD'

	insert into KRT_KodCsoportok
	(
	     Id
		,Kod
		,Nev
		,Modosithato
		,Note
		,BesorolasiSema
	) 
	values
	(
		@ETDR_KULDMOD_Id
		,'ETDR_KULDMOD'
		,'ÉTDR Küldés Módja'
		,'1'
		,''
		,'0'
	)

END

declare @KULDEMENY_KULDES_MODJA_Id uniqueidentifier
set @KULDEMENY_KULDES_MODJA_Id = (select Id from KRT_KodCsoportok Where Kod='KULDEMENY_KULDES_MODJA' and GETDATE() between ErvKezd and ErvVege)

declare @kodtarFuggosegId uniqueidentifier
set @kodtarFuggosegId = '7422A641-B477-415C-B38D-43872D7A8D4E'

IF NOT EXISTS (SELECT 1 FROM KRT_KodtarFuggoseg WHERE Id = @kodtarFuggosegId)
BEGIN 
  Print 'INSERT INTO KRT_KodtarFuggosegek: ETDR_KULDMOD - KULDEMENY_KULDES_MODJA' 

  insert into KRT_KodtarFuggoseg
  (
    Id,
    Org,
    Vezerlo_KodCsoport_Id,
    Fuggo_KodCsoport_Id,
    Adat,
    Aktiv
  ) 
  values (
    @kodtarFuggosegId,
    @ORG_ID,
    @ETDR_KULDMOD_Id,
    @KULDEMENY_KULDES_MODJA_Id,
    '',
    '1'
  ); 
   
END

GO

declare @kcsPARTNER_FORRAS uniqueidentifier = '552F33EC-0BAC-400E-86F2-8AB624959DA4'
declare @record_id uniqueidentifier = '60F4A465-48C8-443A-86D4-6404964FC758'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @record_id)
BEGIN 
  Print 'INSERT INTO KRT_Kodtarak: PARTNER_FORRAS - ETDR' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @record_id,
    '450b510a-7caa-46b0-83e3-18445c0c53a9',
    @kcsPARTNER_FORRAS,
    'É',
    'ETDR',
    '19',
    '1'
  ); 
   
END