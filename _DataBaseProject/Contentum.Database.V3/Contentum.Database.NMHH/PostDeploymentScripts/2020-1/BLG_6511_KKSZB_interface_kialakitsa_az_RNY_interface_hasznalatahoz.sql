﻿PRINT 'Executing: BLG_6511_KKSZB_interface_kialakitsa_az_RNY_interface_hasznalatahoz'

DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

declare @modul_id uniqueidentifier = 'AFCDEA82-5F3B-4D9C-AC5C-759DF63E7D4C'

if not exists (select 1 from INT_Modulok where Id = @modul_id)
begin
    print 'INSERT INT_Modulok - NAP'

	insert into INT_Modulok(Id, Nev)
	values( @modul_id, 'NAP')
end

declare @parameter_id uniqueidentifier = '22238E02-03F7-4987-89F7-799CBAC02CDD'
declare @parameter_ertek nvarchar(400)

if not exists (select 1 from INT_Parameterek where Id = @parameter_id)
begin

	IF @ORG_KOD = 'BOPMH'
		set @parameter_ertek = 'BUDAORS'
	ELSE
		set @parameter_ertek = 'NULL'
	
    print 'INSERT INT_Parameterek - NAP.Lekerdezes.SzervKod - ' + isnull(@parameter_ertek, 'NULL')

	insert into INT_Parameterek(Id, Org, Modul_id, Nev, Ertek, Karbantarthato)
	values(@parameter_id, @ORG_ID, @modul_id, 'NAP.Lekerdezes.SzervKod', @parameter_ertek, '1')
end

set @parameter_id = '8DFDF0A4-FE3A-4E73-A265-F15DF69F5E54'

if not exists (select 1 from INT_Parameterek where Id = @parameter_id)
begin

	set @parameter_ertek = 'JKAP23'
	
    print 'INSERT INT_Parameterek - NAP.Lekerdezes.Jogalap - ' + isnull(@parameter_ertek, 'NULL')

	insert into INT_Parameterek(Id, Org, Modul_id, Nev, Ertek, Karbantarthato)
	values(@parameter_id, @ORG_ID, @modul_id, 'NAP.Lekerdezes.Jogalap', @parameter_ertek, '1')
end

set @parameter_id = 'BBFC20A5-6F5F-4A3F-8A02-6880DF6193DB'

if not exists (select 1 from INT_Parameterek where Id = @parameter_id)
begin

	IF @ORG_KOD = 'BOPMH'
		set @parameter_ertek = 'BUDAORS'
	ELSE
		set @parameter_ertek = 'NULL'
	
    print 'INSERT INT_Parameterek - NAP.Lekerdezes.Adatigenylonev - ' + isnull(@parameter_ertek, 'NULL')

	insert into INT_Parameterek(Id, Org, Modul_id, Nev, Ertek, Karbantarthato)
	values(@parameter_id, @ORG_ID, @modul_id, 'NAP.Lekerdezes.Adatigenylonev', @parameter_ertek, '1')
end

set @parameter_id = '598E2CC5-0824-4930-921E-0DBD3F5EA72D'

if not exists (select 1 from INT_Parameterek where Id = @parameter_id)
begin

	IF @ORG_KOD = 'BOPMH'
		set @parameter_ertek = '39a3bbb27bd0cd249062efd0c68cebdb'
	ELSE
		set @parameter_ertek = 'NULL'
	
    print 'INSERT INT_Parameterek - NAP.Lekerdezes.SzakrendszerHash - ' + isnull(@parameter_ertek, 'NULL')

	insert into INT_Parameterek(Id, Org, Modul_id, Nev, Ertek, Karbantarthato)
	values(@parameter_id, @ORG_ID, @modul_id, 'NAP.Lekerdezes.SzakrendszerHash', @parameter_ertek, '1')
end

GO
