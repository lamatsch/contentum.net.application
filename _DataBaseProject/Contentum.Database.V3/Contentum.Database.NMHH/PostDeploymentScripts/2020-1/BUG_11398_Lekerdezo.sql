﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD ='NMHH')
	BEGIN 
	PRINT 'Executing: BUG_11398_Lekerdezo'
	
	if not exists (select 1 from KRT_Szerepkorok where Nev = 'LEKERDEZO')
		begin
			print 'Nem található lekérdező szerepkör !!!'
			return
		end
	declare @szerepkor_id uniqueidentifier
	select @szerepkor_id = Id from KRT_Szerepkorok where Nev = 'LEKERDEZO'
	insert into KRT_Felhasznalo_Szerepkor 
		(
		Id, 
		Felhasznalo_Id, 
		Szerepkor_Id, 
		Ver, 
		ErvKezd, 
		ErvVege, 
		Letrehozo_id, 
		LetrehozasIdo
		)
	select 
		NEWID() as id,
		f.id as  felhasznalo_id,
		@szerepkor_id as szerepkor_id,
		1 as ver,
		GETDATE() as ervkezd,
		'4700-12-31' as ervvege,
		'54E861A5-36ED-44CA-BAA7-C287D125B309' as letrehozo_id,
		GETDATE() as letrehozasido
	from KRT_Felhasznalok f
	where f.Id not in (select felhasznalo_id from KRT_Felhasznalo_Szerepkor where Szerepkor_Id = @szerepkor_id)
	and f.ErvVege > GETDATE()
	
	END
GO
