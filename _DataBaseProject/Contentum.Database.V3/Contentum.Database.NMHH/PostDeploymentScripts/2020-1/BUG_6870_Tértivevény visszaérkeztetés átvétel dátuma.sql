﻿PRINT 'Executing: BUG_6870_Tértivevény visszaérkeztetés átvétel dátuma'
	--WRITE YOUR SCRIPT HERE. PREPARE TO BE RUN MULTIPLE TIMES!

	--------- BUG_6870 - Tértivevény visszaérkeztetés, átvétel dátuma -------------------------
-------------------------------------------------------------------------------------------

--A kódcsoport ('TERTIVEVENY_VISSZA_KOD') karbantarthatóságát kikapcsolni
UPDATE KRT_KodCsoportok 
SET Modosithato = 0
WHERE Kod = 'TERTIVEVENY_VISSZA_KOD'

--A 'TERTIVEVENY_VISSZA_KOD' kódcsoporthoz tartozó kódtár értékek karbantarthatóságát kikapcsolni
UPDATE KRT_KodTarak
SET Modosithato = 0
WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD')

--------- BUG_6870 - Tértivevény visszaérkeztetés, átvétel dátuma -------------------------
-------------------------------------------------------------------------------------------

--A kódcsoport ('TERTIVEVENY_VISSZA_KOD') karbantarthatóságát kikapcsolni
UPDATE KRT_KodCsoportok 
SET Modosithato = 0
WHERE Kod = 'TERTIVEVENY_VISSZA_KOD'

--A 'TERTIVEVENY_VISSZA_KOD' kódcsoporthoz tartozó kódtár értékek karbantarthatóságát kikapcsolni
UPDATE KRT_KodTarak
SET Modosithato = 0
WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD')

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

------------------------------------ BOPMH -------------------------------------------------------
--BOPMH-ban javítani a kódcsoportot (ott az 5-ös szám kimaradt, azért van elcsúszva, ha jól emlékszem)
IF @org_kod = 'BOPMH'
BEGIN
	PRINT @org_kod

	IF ( SELECT MAX( CONVERT(int,Kod)) FROM [dbo].[KRT_KodTarak] WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD')) = 12
	BEGIN
	UPDATE KRT_KodTarak
	SET Kod = '5',
		Sorrend = '05'
	WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '6'

	UPDATE KRT_KodTarak
	SET Kod = '6',
		Sorrend = '06'
	WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '7'

	UPDATE KRT_KodTarak
	SET Kod = '7',
		Sorrend = '07'
	WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '8'

	UPDATE KRT_KodTarak
	SET Kod = '8',
		Sorrend = '08'
	WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '9'

	UPDATE KRT_KodTarak
	SET Kod = '9',
		Sorrend = '09'
	WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '10'

	UPDATE KRT_KodTarak
	SET Kod = '10',
		Sorrend = '10'
	WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '11'

	UPDATE KRT_KodTarak
	SET Kod = '11',
		Sorrend = '11'
	WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '12'
	END
END
------------------------------------ BOPMH -------------------------------------------------------
--------------------------------------VÉGE--------------------------------------------------------

------------------------------------ FPH,CSEPEL,NMHH -------------------------------------------------------
--------------------------------------------------------------------------------------------------
IF @org_kod != 'BOPMH'
BEGIN
	PRINT @org_kod

	IF NOT EXISTS (Select 1 FROM KRT_KodTarak where KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '1')
	BEGIN
	
	INSERT INTO [dbo].[KRT_KodTarak]
			   ([Id]
			   ,[Org]
			   ,[KodCsoport_Id]
			   ,[ObjTip_Id_AdatElem]
			   ,[Obj_Id]
			   ,[Kod]
			   ,[Nev]
			   ,[RovidNev]
			   ,[Egyeb]
			   ,[Modosithato]
			   ,[Sorrend]
			   ,[Ver]
			   ,[Note]
			   ,[Stat_id]
			   ,[ErvKezd]
			   ,[ErvVege]
			   ,[Letrehozo_id]
			   ,[LetrehozasIdo]
			   ,[Modosito_id]
			   ,[ModositasIdo]
			   ,[Zarolo_id]
			   ,[ZarolasIdo]
			   ,[Tranz_id]
			   ,[UIAccessLog_id])
		 VALUES
			   ('408E96CA-B0B9-4428-B513-4CC367E48903'
			   ,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
			   ,(SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD')
			   ,null
			   ,null
			   ,'1'
			   ,'Kézbesítve: Címzettnek'
			   ,null
			   ,null
			   ,0
			   ,'01'
			   ,1
			   ,null
			   ,null
			   ,GETDATE()
			   ,'4700-12-31 00:00:00.000'
			   ,null
			   ,GETDATE()
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null)
	END
	ELSE
		BEGIN
			UPDATE KRT_KodTarak
			SET Kod = '1',
				Sorrend = '01',
				Modosithato = 0,
				Nev = 'Kézbesítve: Címzettnek'
			WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '1'
		END

	IF NOT EXISTS (Select 1 FROM KRT_KodTarak where KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '2')
	BEGIN
	
	INSERT INTO [dbo].[KRT_KodTarak]
			   ([Id]
			   ,[Org]
			   ,[KodCsoport_Id]
			   ,[ObjTip_Id_AdatElem]
			   ,[Obj_Id]
			   ,[Kod]
			   ,[Nev]
			   ,[RovidNev]
			   ,[Egyeb]
			   ,[Modosithato]
			   ,[Sorrend]
			   ,[Ver]
			   ,[Note]
			   ,[Stat_id]
			   ,[ErvKezd]
			   ,[ErvVege]
			   ,[Letrehozo_id]
			   ,[LetrehozasIdo]
			   ,[Modosito_id]
			   ,[ModositasIdo]
			   ,[Zarolo_id]
			   ,[ZarolasIdo]
			   ,[Tranz_id]
			   ,[UIAccessLog_id])
		 VALUES
			   ('7D0D3BA7-36D9-45BF-9EA2-AF1008C49AAE'
			   ,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
			   ,(SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD')
			   ,null
			   ,null
			   ,'2'
			   ,'Kézbesítve: közvetett kézbesítőnek'
			   ,null
			   ,null
			   ,0
			   ,'02'
			   ,1
			   ,null
			   ,null
			   ,GETDATE()
			   ,'4700-12-31 00:00:00.000'
			   ,null
			   ,GETDATE()
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null)
	END
	ELSE
		BEGIN
			UPDATE KRT_KodTarak
			SET Kod = '2',
				Sorrend = '02',
				Modosithato = 0,
				Nev = 'Kézbesítve: közvetett kézbesítőnek'
			WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '2'
		END

	IF NOT EXISTS (Select 1 FROM KRT_KodTarak where KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '3')
	BEGIN
	
	INSERT INTO [dbo].[KRT_KodTarak]
			   ([Id]
			   ,[Org]
			   ,[KodCsoport_Id]
			   ,[ObjTip_Id_AdatElem]
			   ,[Obj_Id]
			   ,[Kod]
			   ,[Nev]
			   ,[RovidNev]
			   ,[Egyeb]
			   ,[Modosithato]
			   ,[Sorrend]
			   ,[Ver]
			   ,[Note]
			   ,[Stat_id]
			   ,[ErvKezd]
			   ,[ErvVege]
			   ,[Letrehozo_id]
			   ,[LetrehozasIdo]
			   ,[Modosito_id]
			   ,[ModositasIdo]
			   ,[Zarolo_id]
			   ,[ZarolasIdo]
			   ,[Tranz_id]
			   ,[UIAccessLog_id])
		 VALUES
			   ('5B385CAF-4357-4F48-8F94-6CC0AE2455AB'
			   ,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
			   ,(SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD')
			   ,null
			   ,null
			   ,'3'
			   ,'Kézbesítve: meghatalmazottnak'
			   ,null
			   ,null
			   ,0
			   ,'03'
			   ,1
			   ,null
			   ,null
			   ,GETDATE()
			   ,'4700-12-31 00:00:00.000'
			   ,null
			   ,GETDATE()
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null)
	END
	ELSE
		BEGIN
			UPDATE KRT_KodTarak
			SET Kod = '3',
				Sorrend = '03',
				Modosithato = 0,
				Nev = 'Kézbesítve: meghatalmazottnak'
			WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '3'
		END

	IF NOT EXISTS (Select 1 FROM KRT_KodTarak where KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '4')
	BEGIN
	
	INSERT INTO [dbo].[KRT_KodTarak]
			   ([Id]
			   ,[Org]
			   ,[KodCsoport_Id]
			   ,[ObjTip_Id_AdatElem]
			   ,[Obj_Id]
			   ,[Kod]
			   ,[Nev]
			   ,[RovidNev]
			   ,[Egyeb]
			   ,[Modosithato]
			   ,[Sorrend]
			   ,[Ver]
			   ,[Note]
			   ,[Stat_id]
			   ,[ErvKezd]
			   ,[ErvVege]
			   ,[Letrehozo_id]
			   ,[LetrehozasIdo]
			   ,[Modosito_id]
			   ,[ModositasIdo]
			   ,[Zarolo_id]
			   ,[ZarolasIdo]
			   ,[Tranz_id]
			   ,[UIAccessLog_id])
		 VALUES
			   ('A21B9536-C410-4875-9186-D7FD68E5F04E'
			   ,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
			   ,(SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD')
			   ,null
			   ,null
			   ,'4'
			   ,'Kézbesítve: helyettes átvevőnek'
			   ,null
			   ,null
			   ,0
			   ,'04'
			   ,1
			   ,null
			   ,null
			   ,GETDATE()
			   ,'4700-12-31 00:00:00.000'
			   ,null
			   ,GETDATE()
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null)
	END
	ELSE
		BEGIN
			UPDATE KRT_KodTarak
			SET Kod = '4',
				Sorrend = '04',
				Modosithato = 0,
				Nev = 'Kézbesítve: helyettes átvevőnek'
			WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '4'
		END

	IF NOT EXISTS (Select 1 FROM KRT_KodTarak where KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '5')
	BEGIN
	
	INSERT INTO [dbo].[KRT_KodTarak]
			   ([Id]
			   ,[Org]
			   ,[KodCsoport_Id]
			   ,[ObjTip_Id_AdatElem]
			   ,[Obj_Id]
			   ,[Kod]
			   ,[Nev]
			   ,[RovidNev]
			   ,[Egyeb]
			   ,[Modosithato]
			   ,[Sorrend]
			   ,[Ver]
			   ,[Note]
			   ,[Stat_id]
			   ,[ErvKezd]
			   ,[ErvVege]
			   ,[Letrehozo_id]
			   ,[LetrehozasIdo]
			   ,[Modosito_id]
			   ,[ModositasIdo]
			   ,[Zarolo_id]
			   ,[ZarolasIdo]
			   ,[Tranz_id]
			   ,[UIAccessLog_id])
		 VALUES
			   ('B3BD5F0D-C2E2-4D62-BD37-E32AA3C965C0'
			   ,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
			   ,(SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD')
			   ,null
			   ,null
			   ,'5'
			   ,'Visszaküldés oka: cím nem azonosítható'
			   ,null
			   ,null
			   ,0
			   ,'05'
			   ,1
			   ,null
			   ,null
			   ,GETDATE()
			   ,'4700-12-31 00:00:00.000'
			   ,null
			   ,GETDATE()
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null)
	END
	ELSE
		BEGIN
			UPDATE KRT_KodTarak
			SET Kod = '5',
				Sorrend = '05',
				Modosithato = 0,
				Nev = 'Visszaküldés oka: cím nem azonosítható'
			WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '5'
		END

	IF NOT EXISTS (Select 1 FROM KRT_KodTarak where KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '6')
	BEGIN
	
	INSERT INTO [dbo].[KRT_KodTarak]
			   ([Id]
			   ,[Org]
			   ,[KodCsoport_Id]
			   ,[ObjTip_Id_AdatElem]
			   ,[Obj_Id]
			   ,[Kod]
			   ,[Nev]
			   ,[RovidNev]
			   ,[Egyeb]
			   ,[Modosithato]
			   ,[Sorrend]
			   ,[Ver]
			   ,[Note]
			   ,[Stat_id]
			   ,[ErvKezd]
			   ,[ErvVege]
			   ,[Letrehozo_id]
			   ,[LetrehozasIdo]
			   ,[Modosito_id]
			   ,[ModositasIdo]
			   ,[Zarolo_id]
			   ,[ZarolasIdo]
			   ,[Tranz_id]
			   ,[UIAccessLog_id])
		 VALUES
			   ('F0A03258-C4AF-45B2-B129-F9BADD60A057'
			   ,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
			   ,(SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD')
			   ,null
			   ,null
			   ,'6'
			   ,'Visszaküldés oka: nem kereste'
			   ,null
			   ,null
			   ,0
			   ,'06'
			   ,1
			   ,null
			   ,null
			   ,GETDATE()
			   ,'4700-12-31 00:00:00.000'
			   ,null
			   ,GETDATE()
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null)
	END
	ELSE
		BEGIN
			UPDATE KRT_KodTarak
			SET Kod = '6',
				Sorrend = '06',
				Modosithato = 0,
				Nev = 'Visszaküldés oka: nem kereste'
			WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '6'
		END

	IF NOT EXISTS (Select 1 FROM KRT_KodTarak where KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '7')
	BEGIN
	
	INSERT INTO [dbo].[KRT_KodTarak]
			   ([Id]
			   ,[Org]
			   ,[KodCsoport_Id]
			   ,[ObjTip_Id_AdatElem]
			   ,[Obj_Id]
			   ,[Kod]
			   ,[Nev]
			   ,[RovidNev]
			   ,[Egyeb]
			   ,[Modosithato]
			   ,[Sorrend]
			   ,[Ver]
			   ,[Note]
			   ,[Stat_id]
			   ,[ErvKezd]
			   ,[ErvVege]
			   ,[Letrehozo_id]
			   ,[LetrehozasIdo]
			   ,[Modosito_id]
			   ,[ModositasIdo]
			   ,[Zarolo_id]
			   ,[ZarolasIdo]
			   ,[Tranz_id]
			   ,[UIAccessLog_id])
		 VALUES
			   ('24DB3336-29AD-4EA9-890F-3D0A9E3CC072'
			   ,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
			   ,(SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD')
			   ,null
			   ,null
			   ,'7'
			   ,'Visszaküldés oka: elköltözött'
			   ,null
			   ,null
			   ,0
			   ,'07'
			   ,1
			   ,null
			   ,null
			   ,GETDATE()
			   ,'4700-12-31 00:00:00.000'
			   ,null
			   ,GETDATE()
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null)
	END
	ELSE
		BEGIN
			UPDATE KRT_KodTarak
			SET Kod = '7',
				Sorrend = '07',
				Modosithato = 0,
				Nev = 'Visszaküldés oka: elköltözött'
			WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '7'
		END

	IF NOT EXISTS (Select 1 FROM KRT_KodTarak where KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '8')
	BEGIN
	
	INSERT INTO [dbo].[KRT_KodTarak]
			   ([Id]
			   ,[Org]
			   ,[KodCsoport_Id]
			   ,[ObjTip_Id_AdatElem]
			   ,[Obj_Id]
			   ,[Kod]
			   ,[Nev]
			   ,[RovidNev]
			   ,[Egyeb]
			   ,[Modosithato]
			   ,[Sorrend]
			   ,[Ver]
			   ,[Note]
			   ,[Stat_id]
			   ,[ErvKezd]
			   ,[ErvVege]
			   ,[Letrehozo_id]
			   ,[LetrehozasIdo]
			   ,[Modosito_id]
			   ,[ModositasIdo]
			   ,[Zarolo_id]
			   ,[ZarolasIdo]
			   ,[Tranz_id]
			   ,[UIAccessLog_id])
		 VALUES
			   ('DC8C34D0-F595-4292-848C-C38B30AC580B'
			   ,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
			   ,(SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD')
			   ,null
			   ,null
			   ,'8'
			   ,'Visszaküldés oka: bejelentve: meghalt, megszűnt'
			   ,null
			   ,null
			   ,0
			   ,'08'
			   ,1
			   ,null
			   ,null
			   ,GETDATE()
			   ,'4700-12-31 00:00:00.000'
			   ,null
			   ,GETDATE()
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null)
	END
	ELSE
		BEGIN
			UPDATE KRT_KodTarak
			SET Kod = '8',
				Sorrend = '08',
				Modosithato = 0,
				Nev = 'Visszaküldés oka: bejelentve: meghalt, megszűnt'
			WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '8'
		END
	
	IF NOT EXISTS (Select 1 FROM KRT_KodTarak where KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '9')
	BEGIN
	
	INSERT INTO [dbo].[KRT_KodTarak]
			   ([Id]
			   ,[Org]
			   ,[KodCsoport_Id]
			   ,[ObjTip_Id_AdatElem]
			   ,[Obj_Id]
			   ,[Kod]
			   ,[Nev]
			   ,[RovidNev]
			   ,[Egyeb]
			   ,[Modosithato]
			   ,[Sorrend]
			   ,[Ver]
			   ,[Note]
			   ,[Stat_id]
			   ,[ErvKezd]
			   ,[ErvVege]
			   ,[Letrehozo_id]
			   ,[LetrehozasIdo]
			   ,[Modosito_id]
			   ,[ModositasIdo]
			   ,[Zarolo_id]
			   ,[ZarolasIdo]
			   ,[Tranz_id]
			   ,[UIAccessLog_id])
		 VALUES
			   ('E228068E-32B0-45FE-942D-0D7146307344'
			   ,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
			   ,(SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD')
			   ,null
			   ,null
			   ,'9'
			   ,'Visszaküldés oka: címzett ismeretlen'
			   ,null
			   ,null
			   ,0
			   ,'09'
			   ,1
			   ,null
			   ,null
			   ,GETDATE()
			   ,'4700-12-31 00:00:00.000'
			   ,null
			   ,GETDATE()
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null)
	END
	ELSE
		BEGIN
			UPDATE KRT_KodTarak
			SET Kod = '9',
				Sorrend = '09',
				Modosithato = 0,
				Nev = 'Visszaküldés oka: címzett ismeretlen'
			WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '9'
		END

	IF NOT EXISTS (Select 1 FROM KRT_KodTarak where KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '10')
	BEGIN
	
	INSERT INTO [dbo].[KRT_KodTarak]
			   ([Id]
			   ,[Org]
			   ,[KodCsoport_Id]
			   ,[ObjTip_Id_AdatElem]
			   ,[Obj_Id]
			   ,[Kod]
			   ,[Nev]
			   ,[RovidNev]
			   ,[Egyeb]
			   ,[Modosithato]
			   ,[Sorrend]
			   ,[Ver]
			   ,[Note]
			   ,[Stat_id]
			   ,[ErvKezd]
			   ,[ErvVege]
			   ,[Letrehozo_id]
			   ,[LetrehozasIdo]
			   ,[Modosito_id]
			   ,[ModositasIdo]
			   ,[Zarolo_id]
			   ,[ZarolasIdo]
			   ,[Tranz_id]
			   ,[UIAccessLog_id])
		 VALUES
			   ('5152DF31-9116-4F8A-A947-B40CFA08714B'
			   ,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
			   ,(SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD')
			   ,null
			   ,null
			   ,'10'
			   ,'Visszaküldés oka: átvételt megtagadta'
			   ,null
			   ,null
			   ,0
			   ,'10'
			   ,1
			   ,null
			   ,null
			   ,GETDATE()
			   ,'4700-12-31 00:00:00.000'
			   ,null
			   ,GETDATE()
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null)
	END
	ELSE
		BEGIN
			UPDATE KRT_KodTarak
			SET Kod = '10',
				Sorrend = '10',
				Modosithato = 0,
				Nev = 'Visszaküldés oka: átvételt megtagadta'
			WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '10'
		END

	IF NOT EXISTS (Select 1 FROM KRT_KodTarak where KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '11')
	BEGIN	
	INSERT INTO [dbo].[KRT_KodTarak]
			   ([Id]
			   ,[Org]
			   ,[KodCsoport_Id]
			   ,[ObjTip_Id_AdatElem]
			   ,[Obj_Id]
			   ,[Kod]
			   ,[Nev]
			   ,[RovidNev]
			   ,[Egyeb]
			   ,[Modosithato]
			   ,[Sorrend]
			   ,[Ver]
			   ,[Note]
			   ,[Stat_id]
			   ,[ErvKezd]
			   ,[ErvVege]
			   ,[Letrehozo_id]
			   ,[LetrehozasIdo]
			   ,[Modosito_id]
			   ,[ModositasIdo]
			   ,[Zarolo_id]
			   ,[ZarolasIdo]
			   ,[Tranz_id]
			   ,[UIAccessLog_id])
		 VALUES
			   ('F570B0ED-E852-44CD-85D1-FA791E0C0230'
			   ,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
			   ,(SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD')
			   ,null
			   ,null
			   ,'11'
			   ,'Visszaküldés oka: kézbesítés akadályozott'
			   ,null
			   ,null
			   ,0
			   ,'11'
			   ,1
			   ,null
			   ,null
			   ,GETDATE()
			   ,'4700-12-31 00:00:00.000'
			   ,null
			   ,GETDATE()
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null)
	END
	ELSE
		BEGIN
			UPDATE KRT_KodTarak
			SET Kod = '11',
				Sorrend = '11',
				Modosithato = 0,
				Nev = 'Visszaküldés oka: kézbesítés akadályozott'
			WHERE KodCsoport_Id = (SELECT Id FROM KRT_KodCsoportok WHERE Kod = 'TERTIVEVENY_VISSZA_KOD') and Kod = '11'
		END
END

------------------------------------ FPH,CSEPEL,NMHH -------------------------------------------------------
--------------------------------------VÉGE--------------------------------------------------------

--------- BUG_6870 - Tértivevény visszaérkeztetés, átvétel dátuma -------------------------
-------------------------------------------------------------------------------------------
-----------------------------------VÉGE----------------------------------------------------
 