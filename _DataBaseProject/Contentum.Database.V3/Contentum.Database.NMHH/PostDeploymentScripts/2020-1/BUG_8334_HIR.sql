﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD ='NMHH')
	BEGIN 
	PRINT 'Executing: BUG_8334_HIR'
	
	UPDATE [dbo].[KRT_KodTarak]
	SET Nev = 'elektronikus úton'
	WHERE Id = (
	SELECT TOP 1 KT.Id
	  FROM [dbo].[KRT_KodTarak] as KT
	  INNER JOIN [dbo].[KRT_KodCsoportok] as KCS on KT.KodCsoport_Id = KCS.Id
	  WHERE KCS.Kod = 'KULDEMENY_KULDES_MODJA' And KT.kod = '190')

	END
GO
