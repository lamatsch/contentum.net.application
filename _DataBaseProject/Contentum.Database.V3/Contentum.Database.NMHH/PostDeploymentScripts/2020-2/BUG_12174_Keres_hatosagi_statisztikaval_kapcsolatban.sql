﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD ='CSPH')
	BEGIN 
	PRINT 'Executing: BUG_12174_Keres_hatosagi_statisztikaval_kapcsolatban'
	update EREC_Obj_MetaAdatai set Opcionalis = 0 where TargySzavak_Id = (select Id from EREC_TargySzavak where BelsoAzonosito = 'Dontest_hozta')
	update EREC_Obj_MetaAdatai set Opcionalis = 0 where TargySzavak_Id = (select Id from EREC_TargySzavak where BelsoAzonosito = 'Dontest_hoza_allamigazgatasi')
	update EREC_Obj_MetaAdatai set Opcionalis = 0 where TargySzavak_Id = (select Id from EREC_TargySzavak where BelsoAzonosito = 'Dontes_formaja_onkormanyzati')
	update EREC_Obj_MetaAdatai set Opcionalis = 0 where TargySzavak_Id = (select Id from EREC_TargySzavak where BelsoAzonosito = 'Dontes_formaja_allamigazgatasi')
	update EREC_Obj_MetaAdatai set Opcionalis = 0 where TargySzavak_Id = (select Id from EREC_TargySzavak where BelsoAzonosito = 'Ugyintezes_idotartama')
	update EREC_Obj_MetaAdatai set Opcionalis = 0 where TargySzavak_Id = (select Id from EREC_TargySzavak where BelsoAzonosito = 'Hatarido_tullepes_napokban')
	END
GO
