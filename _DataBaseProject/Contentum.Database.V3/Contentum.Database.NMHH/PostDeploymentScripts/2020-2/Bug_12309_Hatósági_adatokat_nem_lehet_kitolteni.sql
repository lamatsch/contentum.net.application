declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @orgKod nvarchar(400)
set @orgKod = (select Kod from KRT_Orgok where Id = @org)

IF @orgKod = 'CSPH'
BEGIN

	declare @kcs_UgyFajtaja uniqueidentifier = '72554FDB-8323-4110-B7B2-65576AE3204A'

	-- Nem hat�s�gi �gy
	IF NOT EXISTS (select 1 from KRT_KodTarak where KodCsoport_Id = @kcs_UgyFajtaja
	and Kod = '0' and GETDATE() between ErvKezd and ErvVege)
	BEGIN
			PRINT 'INSERT INTO KRT_KodTarak - UGY_FAJTAJA.0'
			INSERT INTO KRT_KodTarak
			(
				Org,
				KodCsoport_Id,
				Kod,
				Nev,
				RovidNev,
				Egyeb,
				Modosithato,
				Sorrend
			)
			VALUES
			(
				@org,
				@kcs_UgyFajtaja,
				'0',
				'Nem hat�s�gi �gy',
				'0',
				'NEM_HATOSAGI',
				'1',
				'01'
			)
	END


	PRINT 'INVALIDATE KRT_KodtTarak - UGY_FAJTAJA'
	UPDATE KRT_KodTarak
		Set ErvVege = GETDATE()
	where KodCsoport_Id = @kcs_UgyFajtaja
	and Kod not in ('0','1','2')
	and ErvVege >= GETDATE()

END