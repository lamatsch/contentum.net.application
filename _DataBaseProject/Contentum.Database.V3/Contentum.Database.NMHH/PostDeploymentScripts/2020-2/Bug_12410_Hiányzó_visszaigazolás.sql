﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD ='FPH' or @ORG_KOD = 'CSPH')
	BEGIN 
	PRINT 'Executing: Bug_12410_Hiányzó_visszaigazolás'
	begin tran

		update EREC_eBeadvanyok
			set ErvVege = '4700-12-31'
			,FeldolgozasStatusz = '-1' --Hibás
			,FeldolgozasiHiba = 'Bug 12410:Hiányzó visszaigazolás'
		where Id in
		(
			select eb.Id from EREC_KuldTertivevenyek tert
			join EREC_eBeadvanyok eb on tert.Ragszam = eb.KR_HivatkozasiSzam and eb.ErvVege < GETDATE()
			and eb.KR_DokTipusAzonosito in ('MeghiusulasiIgazolas','LetoltesiIgazolas')
			where tert.Allapot = '0' -- Előkészített
			and eb.LetrehozasIdo > '2020-01-01'
		)

	commit

	END
GO
