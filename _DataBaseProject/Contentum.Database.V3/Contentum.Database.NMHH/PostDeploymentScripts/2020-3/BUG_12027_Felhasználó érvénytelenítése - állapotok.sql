﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id=@ORG_ID) 

-------------------------------------------------------------------------------
-- BLG 12072 - Felhasználó érvénytelenítése - állapotok
-- Az elszámoltatási jegyzőkönyv szerint releváns Iratpéldány állapotok listája. (Vesszővel elválasztva)
-- ELSZAMOLTATAS_RELEVANS_IRATPELDANY_ALLAPOT  rendszerparameter	 
-- ertek: 00, 04, 30, 32, 50, 60, 61, 63, 64
-- FPH,NMHH,BOPMH,CSPH-ban

DECLARE @Id nvarchar(100)
DECLARE @record_id uniqueidentifier

SET @Id = '2DAE4909-D729-42F0-95D9-2FC8ABB20868'

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 


SET @record_id = (select Id from KRT_Parameterek
where Id=@Id) 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,@Id
		,'ELSZAMOLTATAS_RELEVANS_IRATPELDANY_ALLAPOT'
		,'00,04,30,32,50,60,61,63,64'
		,'1'
		,'Az elszámoltatási jegyzőkönyv szerint releváns Iratpéldány állapotok listája. (Vesszővel elválasztva)'
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
		Org='450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,Id=@record_id
		,Nev='ELSZAMOLTATAS_RELEVANS_IRATPELDANY_ALLAPOT'
		,Ertek='00,04,30,32,50,60,61,63,64'
		,Karbantarthato='1'
		,Note='Az elszámoltatási jegyzőkönyv szerint releváns Iratpéldány állapotok listája. (Vesszővel elválasztva)'
	 WHERE Id=@record_id 
 END
 ---------------------------------------------------------------------------------------------------------------
 -------------------- BLG 12072 - Felhasználó érvénytelenítése - állapotok--------------------------------------
 --------------------------------------------------------vége --------------------------------------------------
 
 -------------------------------------------------------------------------------
-- BLG 12072 - Felhasználó érvénytelenítése - állapotok
-- Az elszámoltatási jegyzőkönyv szerint releváns Küldemény állapotok listája. (Vesszővel elválasztva)
-- ELSZAMOLTATAS_RELEVANS_KULDEMENY_ALLAPOT  rendszerparameter	 
-- ertek: üres
-- FPH,NMHH,BOPMH,CSPH-ban


SET @Id = '89FC5C45-A6FA-46AC-8887-AD254F911BA8'

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

 SET @record_id = (select Id from KRT_Parameterek
where Id=@Id) 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,@Id
		,'ELSZAMOLTATAS_RELEVANS_KULDEMENY_ALLAPOT'
		,''
		,'1'
		,'Az elszámoltatási jegyzőkönyv szerint releváns Küldemény állapotok listája. (Vesszővel elválasztva)'
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
		Org='450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,Id=@record_id
		,Nev='ELSZAMOLTATAS_RELEVANS_KULDEMENY_ALLAPOT'
		,Ertek=''
		,Karbantarthato='1'
		,Note='Az elszámoltatási jegyzőkönyv szerint releváns Küldemény állapotok listája. (Vesszővel elválasztva)'
	 WHERE Id=@record_id 
 END
 ---------------------------------------------------------------------------------------------------------------
 -------------------- BLG 12072 - Felhasználó érvénytelenítése - állapotok--------------------------------------
 --------------------------------------------------------vége --------------------------------------------------


IF(@org_kod ='NMHH')
	BEGIN 
	PRINT 'Executing: BUG_12027_Felhasználó érvénytelenítése - állapotok'
	-------------------------------------------------------------------------------
-- BLG 12072 - Felhasználó érvénytelenítése - állapotok
-- Az elszámoltatási jegyzőkönyv szerint releváns Ügyirat állapotok listája. (Vesszővel elválasztva)
-- ELSZAMOLTATAS_RELEVANS_UGYIRAT_ALLAPOT  rendszerparameter	 
-- ertek: 00, 03, 04, 06, 07, 11, 13, 54, 56, 57, 70
-- NMHH-ban

SET @Id = '509A7DE0-BB0D-44F5-AE41-0CB1705F95D7'

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 


SET @record_id = (select Id from KRT_Parameterek
where Id=@Id) 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,@Id
		,'ELSZAMOLTATAS_RELEVANS_UGYIRAT_ALLAPOT'
		,'00,03,04,06,07,11,13,54,56,57,70'
		,'1'
		,'Az elszámoltatási jegyzőkönyv szerint releváns Ügyirat állapotok listája. (Vesszővel elválasztva)'
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
		Org='450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,Id=@record_id
		,Nev='ELSZAMOLTATAS_RELEVANS_UGYIRAT_ALLAPOT'
		,Ertek='00,03,04,06,07,11,13,54,56,57,70'
		,Karbantarthato='1'
		,Note='Az elszámoltatási jegyzőkönyv szerint releváns Ügyirat állapotok listája. (Vesszővel elválasztva)'
	 WHERE Id=@record_id 
 END
 ---------------------------------------------------------------------------------------------------------------
 -------------------- BLG 12072 - Felhasználó érvénytelenítése - állapotok--------------------------------------
 --------------------------------------------------------vége --------------------------------------------------

END

IF(@org_kod !='NMHH')
	BEGIN 
	PRINT 'Executing: BUG_12027_Felhasználó érvénytelenítése - állapotok'
	-------------------------------------------------------------------------------
-- BLG 12072 - Felhasználó érvénytelenítése - állapotok
-- Az elszámoltatási jegyzőkönyv szerint releváns Ügyirat állapotok listája. (Vesszővel elválasztva)
-- ELSZAMOLTATAS_RELEVANS_UGYIRAT_ALLAPOT  rendszerparameter	 
-- ertek: 00, 03, 04, 06, 07, 09, 11, 13, 52, 54, 55, 56, 57, 70
-- FPH,BOPMH,CSPH-ban

SET @Id = '509A7DE0-BB0D-44F5-AE41-0CB1705F95D7'

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 


SET @record_id = (select Id from KRT_Parameterek
where Id=@Id) 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,@Id
		,'ELSZAMOLTATAS_RELEVANS_UGYIRAT_ALLAPOT'
		,'00,03,04,06,07,09,11,13,52,54,55,56,57,70'
		,'1'
		,'Az elszámoltatási jegyzőkönyv szerint releváns Ügyirat állapotok listája. (Vesszővel elválasztva)'
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
		Org='450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,Id=@record_id
		,Nev='ELSZAMOLTATAS_RELEVANS_UGYIRAT_ALLAPOT'
		,Ertek='00,03,04,06,07,09,11,13,52,54,55,56,57,70'
		,Karbantarthato='1'
		,Note='Az elszámoltatási jegyzőkönyv szerint releváns Ügyirat állapotok listája. (Vesszővel elválasztva)'
	 WHERE Id=@record_id 
 END
 ---------------------------------------------------------------------------------------------------------------
 -------------------- BLG 12072 - Felhasználó érvénytelenítése - állapotok--------------------------------------
 --------------------------------------------------------vége --------------------------------------------------

	END
GO
