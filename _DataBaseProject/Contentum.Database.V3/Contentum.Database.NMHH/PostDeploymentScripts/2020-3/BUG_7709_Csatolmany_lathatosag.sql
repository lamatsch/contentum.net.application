﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'


DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 
print @ORG_KOD

DECLARE @RECORD_ID uniqueidentifier
SET @RECORD_ID = 'A4227BE2-E4B0-4788-9C55-8B5EBA982D08'

IF(@ORG_KOD ='BOPMH')
	BEGIN 
	PRINT 'Executing: BUG_7709_Csatolmany_lathatosag'
	IF(not exists (select 1 from KRT_Parameterek where Id = @RECORD_ID))
		BEGIN
			 INSERT INTO KRT_Parameterek(
									 Org
									,Id
									,Nev
									,Ertek
									,Karbantarthato
									,Note
									) values (
									 @ORG_ID
									,@RECORD_ID
									,'CSATOLMANY_LATHATOSAG_IRATPELDANYTOL'
									,'1'
									,'1'
									,'Ha 1 es az értéke akkor, az irat csatolmányát csak az láthatja, aki az iratpéldánynak az irat helye mezőn szerepel.'
									); 
		END
		UPDATE KRT_Parameterek SET Ertek = '1' where Id = @RECORD_ID
	END
ELSE
	BEGIN
		IF(not exists (select 1 from KRT_Parameterek where Id = @RECORD_ID))
		BEGIN
			 INSERT INTO KRT_Parameterek(
									 Org
									,Id
									,Nev
									,Ertek
									,Karbantarthato
									,Note
									) values (
									 @ORG_ID
									,@RECORD_ID
									,'CSATOLMANY_LATHATOSAG_IRATPELDANYTOL'
									,'0'
									,'1'
									,'Ha 1 es az értéke akkor, az irat csatolmányát csak az láthatja, aki az iratpéldánynak az irat helye mezőn szerepel.'
									); 
		END
		UPDATE KRT_Parameterek SET Ertek = '0' where Id = @RECORD_ID
	END
GO
