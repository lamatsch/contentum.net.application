﻿
	---------------------------------------------------------------------
	-- RENDSZERPARAMETER = EPOSTAI_TERTIVEVENY.SFTP.DAYS_TO_PROCESS
	---------------------------------------------------------------------	
	GO

    DECLARE @ORGID				UNIQUEIDENTIFIER
	DECLARE @ID					UNIQUEIDENTIFIER
	DECLARE @NEV				NVARCHAR(400)
	DECLARE @KARBANTARTHATO		CHAR(1)
	DECLARE @NOTE				NVARCHAR(4000)
	
	DECLARE @ERTEK_BOPMH			NVARCHAR(400)

	SET @ORGID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
	SET @ID = 	 NEWID()
	SET @NEV =   'EPOSTAI_TERTIVEVENY.SFTP_DAYS_TO_PROCESS'
	SET @KARBANTARTHATO = '1'
	SET @NOTE = 'Meghatátorozza, hogy hány napra visszamenőleg kell a tértivevényeket letölteni a posta szerveréről.'
	
	SET @ERTEK_BOPMH = '1'

	IF NOT EXISTS(SELECT 1 FROM INT_Parameterek WHERE NEV=@NEV OR ID=@ID) 
	BEGIN
		IF EXISTS (SELECT 1 FROM KRT_Orgok WHERE Kod='BOPMH')
			BEGIN
				INSERT INTO INT_Parameterek (Id, Nev, Ertek, Karbantarthato,Modul_id, Note, Org)
				VALUES (@ID, @NEV, @ERTEK_BOPMH, @KARBANTARTHATO,'10B9D981-63D3-4211-A804-745E85B0B74D', @NOTE, @ORGID)
			END		
	END
	GO
	---------------------------------------------------------------------

