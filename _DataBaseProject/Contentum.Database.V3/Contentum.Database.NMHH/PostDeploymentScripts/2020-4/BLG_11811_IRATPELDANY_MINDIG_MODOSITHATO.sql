﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 
PRINT 'Executing: BLG_11811_IRATPELDANY_MINDIG_MODOSITHATO'
	
DECLARE @ERTEK nvarchar(100)

IF(@ORG_KOD ='BOPMH')
	BEGIN 
		SET @ERTEK = '1'
	END

IF(@ORG_KOD ='CSPH')
	BEGIN 
		SET @ERTEK = '0'
	END

IF(@ORG_KOD ='FPH')
	BEGIN 
		SET @ERTEK = '0'
	END


IF NOT EXISTS(SELECT 1 FROM KRT_Parameterek WHERE NEV='IRATPELDANY_MINDIG_MODOSITHATO') 
		BEGIN
			INSERT INTO KRT_Parameterek
				(Id,Org,  Nev, Ertek, Karbantarthato,Note)
				VALUES
				('ACD47ADA-B5D6-4152-9D72-D62D252A0EB7',@ORG_ID,'IRATPELDANY_MINDIG_MODOSITHATO',@ERTEK,'1','Ha 1-es az értéke akkor az iratpéldányt akkor is lehet módosítani ha az sztornózott állapoton kívül bármi másban van')
		END
ELSE
		BEGIN
			DECLARE @RECORD_ID uniqueidentifier
			SET @RECORD_ID =  (SELECT ID FROM KRT_Parameterek WHERE NEV='IRATPELDANY_MINDIG_MODOSITHATO')
			UPDATE KRT_Parameterek SET Ertek = @ERTEK where Id = @RECORD_ID
		END
GO

select *from krt_parameterek where nev = 'IRATPELDANY_MINDIG_MODOSITHATO'