﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD ='BOPMH')
	BEGIN 
	PRINT 'Executing: BUG_12602_Hatosagi_adatlapkitolto_feluleten_hibas_adattartalom'
		UPDATE [KRT_Kodtarak]
		SET ErvVege = GETDATE()
		WHERE Id = '309558B1-2CE1-E611-A154-0050569A6FBA'

		SELECT Id, Nev, Kod, ErvVege FROM [KRT_Kodtarak] WHERE Id = '309558B1-2CE1-E611-A154-0050569A6FBA'
	END
GO
