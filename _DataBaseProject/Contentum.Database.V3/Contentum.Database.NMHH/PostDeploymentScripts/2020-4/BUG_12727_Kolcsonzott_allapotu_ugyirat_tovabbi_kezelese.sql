﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD ='BOPMH')
	BEGIN 
	PRINT 'Executing: BUG_12727_Kolcsonzott_allapotu_ugyirat_tovabbi_kezelese'
	
	UPDATE EREC_UgyUgyiratok
	SET Allapot = '06'
	WHERE Id = '52b9366f-58e0-e911-bc3c-0050569a6fba'

	END
GO
