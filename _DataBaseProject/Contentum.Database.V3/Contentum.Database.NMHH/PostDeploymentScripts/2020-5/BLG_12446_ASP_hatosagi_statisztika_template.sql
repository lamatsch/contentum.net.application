﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD ='BOPMH')
	BEGIN 
	PRINT 'Executing: BLG_12446_ASP_hatosagi_statisztika_template'
	declare @POSTAI_ID uniqueidentifier = 'C427258A-A1AB-49D0-80F1-3AC6E2971E40' 
	declare @NEM_POSTAI_ID uniqueidentifier = '8094C421-8E6A-484F-BA52-B0345D01F5FC' 	
	 	
	IF NOT EXISTS (select 1 from KRT_UIMezoObjektumErtekek where Id = @POSTAI_ID)	
		BEGIN			
				INSERT INTO KRT_UIMezoObjektumErtekek
				(
					Id,
					TemplateTipusNev,
					Nev,
					Alapertelmezett,
					Felhasznalo_Id,
					TemplateXML,
					Org_Id,
					Publikus
				)
				VALUES
				(
					@POSTAI_ID,
					'HatosagiStatisztikaTargyszavakTemplate',
					'ASPADO_vegzes_postai',
					0,
					'A1806670-DC77-E911-80D3-00155D020D17',
					'<HatosagiStatisztikaTargyszavakTemplate><Dontest_hoza_allamigazgatasi>5</Dontest_hoza_allamigazgatasi><Ugyintezes_idotartama>1</Ugyintezes_idotartama><Hatarido_tullepes_napokban>0</Hatarido_tullepes_napokban><Jogorvoslati_eljaras_tipusa /><Jogorv_eljarasban_dontes_tipusa /><Jogorv_eljarasban_dontest_hozta /><Jogorv_eljarasban_dontes_tartalma /><Jogorv_eljarasban_dontes_valtozasa /><Hatosagi_ellenorzes_tortent>0</Hatosagi_ellenorzes_tortent><Dontes_munkaorak_szama>0,5</Dontes_munkaorak_szama><Megallapitott_eljarasi_koltseg>400</Megallapitott_eljarasi_koltseg><Kiszabott_kozigazgatasi_birsag>0</Kiszabott_kozigazgatasi_birsag><Sommas_eljarasban_hozott_dontes /><Nyolc_napon_belul_lezart_nem_sommas /><Fuggo_hatalyu_hatarozat /><Hatarozat_hatalyba_lepese /><Fuggo_hatalyu_vegzes /><Vegzes_hatalyba_lepese>2</Vegzes_hatalyba_lepese><Hatosag_altal_visszafizetett_osszeg>0</Hatosag_altal_visszafizetett_osszeg><Hatosagot_terhelo_eljarasi_koltseg>0</Hatosagot_terhelo_eljarasi_koltseg><Felfuggesztett_hatarozat>0</Felfuggesztett_hatarozat><Dontes_formaja_allamigazgatasi>8</Dontes_formaja_allamigazgatasi></HatosagiStatisztikaTargyszavakTemplate>',
					'A1806670-DC77-E911-80D3-00155D020D17',
					1
				)
		END


	IF NOT EXISTS (select 1 from KRT_UIMezoObjektumErtekek where Id = @NEM_POSTAI_ID)	
		BEGIN			
				INSERT INTO KRT_UIMezoObjektumErtekek
				(
					Id,
					TemplateTipusNev,
					Nev,
					Alapertelmezett,
					Felhasznalo_Id,
					TemplateXML,
					Org_Id,
					Publikus
				)
				VALUES
				(
					@NEM_POSTAI_ID,
					'HatosagiStatisztikaTargyszavakTemplate',
					'ASPADO_vegzes_nempostai',
					0,
					'A1806670-DC77-E911-80D3-00155D020D17',
					'<HatosagiStatisztikaTargyszavakTemplate><Dontest_hoza_allamigazgatasi>5</Dontest_hoza_allamigazgatasi><Ugyintezes_idotartama>1</Ugyintezes_idotartama><Hatarido_tullepes_napokban>0</Hatarido_tullepes_napokban><Jogorvoslati_eljaras_tipusa /><Jogorv_eljarasban_dontes_tipusa /><Jogorv_eljarasban_dontest_hozta /><Jogorv_eljarasban_dontes_tartalma /><Jogorv_eljarasban_dontes_valtozasa /><Hatosagi_ellenorzes_tortent>0</Hatosagi_ellenorzes_tortent><Dontes_munkaorak_szama>0,5</Dontes_munkaorak_szama><Megallapitott_eljarasi_koltseg>0</Megallapitott_eljarasi_koltseg><Kiszabott_kozigazgatasi_birsag>0</Kiszabott_kozigazgatasi_birsag><Sommas_eljarasban_hozott_dontes>0</Sommas_eljarasban_hozott_dontes><Nyolc_napon_belul_lezart_nem_sommas>0</Nyolc_napon_belul_lezart_nem_sommas><Fuggo_hatalyu_hatarozat>0</Fuggo_hatalyu_hatarozat><Hatarozat_hatalyba_lepese /><Fuggo_hatalyu_vegzes /><Vegzes_hatalyba_lepese>2</Vegzes_hatalyba_lepese><Hatosag_altal_visszafizetett_osszeg>0</Hatosag_altal_visszafizetett_osszeg><Hatosagot_terhelo_eljarasi_koltseg>0</Hatosagot_terhelo_eljarasi_koltseg><Felfuggesztett_hatarozat>0</Felfuggesztett_hatarozat><Dontes_formaja_allamigazgatasi>8</Dontes_formaja_allamigazgatasi></HatosagiStatisztikaTargyszavakTemplate>',
					'A1806670-DC77-E911-80D3-00155D020D17',
					1
				)
		END
END
GO
