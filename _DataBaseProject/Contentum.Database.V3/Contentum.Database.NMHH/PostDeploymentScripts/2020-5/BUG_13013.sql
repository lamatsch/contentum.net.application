﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

PRINT 'Executing: BUG_13013'
UPDATE KRT_Esemenyek
SET MuveletKimenete = 'SIKERES'
WHERE MuveletKimenete IS NULL
AND
KRT_Esemenyek.Funkcio_Id = '38B849F7-E7C1-458A-BE76-E39CE4E2DC4A'

GO
