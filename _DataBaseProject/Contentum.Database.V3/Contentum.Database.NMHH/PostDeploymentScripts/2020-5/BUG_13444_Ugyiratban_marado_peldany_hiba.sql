﻿PRINT 'Executing: BUG_13444_Ugyiratban_marado_peldany_hiba'
	-------------------------------------------------------------------------------
-- BUG 13444 - Ügyiratban maradó példány hiba
-- KULDEMENY_KULDES_MODJA_HELYBEN amely tartalmazza a helyben maradó küldés értékét, és erre állítsuk be a küldés módját. 
-- Az alapértelmezett értéknek vegyük fel a 13-at ("helyben"), az egyes ügyfeleknél a kívánt értéket telepítés előtt egyeztetni szükséges.
-- KULDEMENY_KULDES_MODJA_HELYBEN rendszerparameter	 
-- ertek: 13
-------------------------------------------------------------------------------

DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
where Id='F21FE013-FDEE-42D8-B6D7-D3EAC2863926') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'F21FE013-FDEE-42D8-B6D7-D3EAC2863926'
		,'KULDEMENY_KULDES_MODJA_HELYBEN'
		,'13'
		,'1'
		,'KULDEMENY_KULDES_MODJA_HELYBEN amely tartalmazza a helyben maradó küldés értékét, és erre állítsuk be a küldés módját.'
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		,Id='F21FE013-FDEE-42D8-B6D7-D3EAC2863926'
		,Nev='KULDEMENY_KULDES_MODJA_HELYBEN'
		,Ertek='13'
		,Karbantarthato='1'
		,Note='KULDEMENY_KULDES_MODJA_HELYBEN amely tartalmazza a helyben maradó küldés értékét, és erre állítsuk be a küldés módját.'
	 WHERE Id=@record_id 
 END
 ---------------------------------------------------------------------------------------------------------------
 ------------------------------- BUG 13444 - Ügyiratban maradó példány hiba  -----------------------------------

 GO
