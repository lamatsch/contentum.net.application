﻿PRINT 'Executing: BUG_13474_Ugyiratok_tomeges_update'
	
-- BUG_13474
-- UGYIRAT_TOMEGES_UPDATE_LIMIT rendszerparameter	 
-- Ügyrat tömeges update átadott id-k limitálásának értéke
-- BOPMH érték: 5000

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='ECC71ACA-C534-4827-85CD-89C9A1566FEA') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 


print 'Ügyrat tömeges update átadott id-k limitálásának értéke'
SET @record_id = null 
SET @record_id = (select Id from KRT_Parameterek
			where Id='ECC71ACA-C534-4827-85CD-89C9A1566FEA') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'ECC71ACA-C534-4827-85CD-89C9A1566FEA'
		,'UGYIRAT_TOMEGES_UPDATE_LIMIT'
		,'5000'
		,'1'
		,'Ügyrat tömeges update átadott id-k limitálásának értéke'
		); 
 END 
 ELSE 
 BEGIN 
	Print 'UPDATE'
	UPDATE KRT_Parameterek
	SET 
		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		,Id='ECC71ACA-C534-4827-85CD-89C9A1566FEA'
		,Nev='UGYIRAT_TOMEGES_UPDATE_LIMIT'
		,Ertek='5000'
		,Karbantarthato='1'
		,Note='Ügyrat tömeges update átadott id-k limitálásának értéke'
	 WHERE Id=@record_id 
 END

GO
 GO
