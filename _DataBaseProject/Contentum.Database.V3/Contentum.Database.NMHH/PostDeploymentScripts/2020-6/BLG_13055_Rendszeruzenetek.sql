﻿

DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 
PRINT 'Executing: BLG_13055_Rendszeruzenetek'
DECLARE @ERTEK nvarchar(100)
SET @ERTEK = '2'

IF(@ORG_KOD ='CSPH')
	BEGIN
		SET @ERTEK =  '0'
END

			IF NOT EXISTS(SELECT 2 FROM KRT_Parameterek WHERE NEV='EUZENET_RENDSZERUZENETEK_SZURES') 
				BEGIN
					INSERT INTO KRT_Parameterek
						(Id,Org,Karbantarthato,  NEV, Ertek, Note)
						VALUES
						('8A8FC997-82C1-47C6-865B-CCE7481418C8',@ORG_ID,'1','EUZENET_RENDSZERUZENETEK_SZURES',@ERTEK,'0 - Nem (Ne kerüljenek be a lekérdezett listába a rendszerüzenetek)
																													  1 - Igen (Kerüljenek be a lekérdezett listába a rendszerüzenetek)
																													  2 - Összes (Ne vegye figyelembe a lekérdezés)')
				END
		ELSE
				BEGIN
					DECLARE @RECORD_ID uniqueidentifier
					SET @RECORD_ID =  (SELECT ID FROM KRT_Parameterek WHERE NEV='EUZENET_RENDSZERUZENETEK_SZURES')
					UPDATE KRT_Parameterek SET Ertek = @ERTEK where Id = @RECORD_ID
				END
GO
