﻿PRINT 'Executing: BLG_13824_EFJ_kiegeszites_eTerti_visszakuldesi_moddal'
	declare @KOD_CSOPORT_ID uniqueidentifier
	set @KOD_CSOPORT_ID = '5C342129-2425-4D80-9089-891FD62AE029'

	declare @ORG_ID uniqueidentifier
	set @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

		IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @KOD_CSOPORT_ID)
		BEGIN
			insert into KRT_KodCsoportok
			(
				 Id
				,Org
				,Kod
				,Nev
				,Modosithato
				,Note
				,BesorolasiSema
			) 
			values
			(
				@KOD_CSOPORT_ID
				,@ORG_ID
				,'POSTA_HIV_ERTESITO_TIPUS'
				,'POSTA_HIV_ERTESITO_TIPUS'
				,'1'
				,'Az EFJ-be kerülő hiv_ertesito lehetséges értékeit tartalmazza.'
				,'0'
			)
		END

		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='609CFFC8-B6C0-45A6-82BA-EFDA13FC14CD')
		BEGIN
			insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values ('609CFFC8-B6C0-45A6-82BA-EFDA13FC14CD',@KOD_CSOPORT_ID,@ORG_ID,'A/1','A/1','0')
		END
		
		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='76B212E6-F609-41CF-A75F-3B5264422544')
		BEGIN
			insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values ('76B212E6-F609-41CF-A75F-3B5264422544',@KOD_CSOPORT_ID,@ORG_ID,'A/1/SK','A/1/SK','0')
		END

		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='CEEEB21B-0927-445B-981A-58A26452D7F7')
		BEGIN
			insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values ('CEEEB21B-0927-445B-981A-58A26452D7F7',@KOD_CSOPORT_ID,@ORG_ID,'A/2','A/2','0')
		END

		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='AA95B5DC-CA01-4595-B74A-9AC2D936BC5A')
		BEGIN
			insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values ('AA95B5DC-CA01-4595-B74A-9AC2D936BC5A',@KOD_CSOPORT_ID,@ORG_ID,'A/3','A/3','0')
		END

		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='CD5A2B3F-93F4-45B4-B70D-35E21F83BE6C')
		BEGIN
			insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values ('CD5A2B3F-93F4-45B4-B70D-35E21F83BE6C',@KOD_CSOPORT_ID,@ORG_ID,'A/4','A/4','0')
		END

		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='CD5A2B3F-93F4-45B4-B70D-35E21F83BE6C')
		BEGIN
			insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values ('CD5A2B3F-93F4-45B4-B70D-35E21F83BE6C',@KOD_CSOPORT_ID,@ORG_ID,'A/5','A/5','0')
		END

		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='85E3C256-39FB-4842-A6E1-B9B93051E491')
		BEGIN
			insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values ('85E3C256-39FB-4842-A6E1-B9B93051E491',@KOD_CSOPORT_ID,@ORG_ID,'A/6','A/6','0')
		END

		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='D5887487-068D-49A0-B90B-BAF7ED80E780')
		BEGIN
			insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values ('D5887487-068D-49A0-B90B-BAF7ED80E780',@KOD_CSOPORT_ID,@ORG_ID,'A/7','A/7','0')
		END

		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='213E26E5-04BF-404A-A787-526604DBAA65')
		BEGIN
			insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values ('213E26E5-04BF-404A-A787-526604DBAA65',@KOD_CSOPORT_ID,@ORG_ID,'A/7/2','A/7/2','0')
		END

		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='24AB124F-6138-4122-985B-84C4CAE4D943')
		BEGIN
			insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values ('24AB124F-6138-4122-985B-84C4CAE4D943',@KOD_CSOPORT_ID,@ORG_ID,'A/8',' A/8','0')
		END

		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='9DEC6726-56B1-4E3E-9F0C-1023F40BA23F')
		BEGIN
			insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values ('9DEC6726-56B1-4E3E-9F0C-1023F40BA23F',@KOD_CSOPORT_ID,@ORG_ID,'A/9','A/9','0')
		END

		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='7906E67A-3FE7-4F21-8C63-46BD8BBD6B88')
		BEGIN
			insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values ('7906E67A-3FE7-4F21-8C63-46BD8BBD6B88',@KOD_CSOPORT_ID,@ORG_ID,'A/10','A/10','0')
		END

		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='C183E695-3DBC-47E4-9EE6-9F550DE0F3C3')
		BEGIN
			insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values ('C183E695-3DBC-47E4-9EE6-9F550DE0F3C3',@KOD_CSOPORT_ID,@ORG_ID,'A/10/SK','A/10/SK','0')
		END
		
		
		IF NOT EXISTS (SELECT 1 FROM KRT_KODTARFUGGOSEG WHERE ID='0021581B-1E23-423A-8F06-B829DA5700E0')
		BEGIN
			INSERT [dbo].[KRT_KodtarFuggoseg] ([Id], [Org], [Vezerlo_KodCsoport_Id], [Fuggo_KodCsoport_Id], [Adat], [Aktiv]) 
			VALUES					(N'0021581B-1E23-423A-8F06-B829DA5700E0', @ORG_ID, N'11F9DC99-3708-49BE-A5DA-F7B6DB343784', @KOD_CSOPORT_ID, N'{}', N'1')
		END		
 GO