﻿PRINT 'Executing: BUG_13539_emigration_tarolt_hianyzik'

DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9' 
PRINT 'Executing: BUG_13539_emigration_tarolt_hianyzik'
	
DECLARE @ERTEK nvarchar(100)
set @ERTEK = '98,04,11,52,13,09,70,07,03,50,06'

IF NOT EXISTS(SELECT 1 FROM KRT_Parameterek WHERE NEV='IRATTAROZATLAN_UGYIRAT_ALLAPOT') 
		BEGIN
			INSERT INTO KRT_Parameterek
				(Id,Org,  Nev, Ertek, Karbantarthato,Note)
				VALUES
				('97562BAE-26D1-44FE-A8B3-43E47F60CC20',@ORG_ID,'IRATTAROZATLAN_UGYIRAT_ALLAPOT',@ERTEK,'1','A megadott állapottal rendelkező ügyiratok szerepeljenek az Irattározatlan ügyiratok jegyzékén')
		END
ELSE
		BEGIN
			DECLARE @RECORD_ID uniqueidentifier
			SET @RECORD_ID =  (SELECT ID FROM KRT_Parameterek WHERE NEV='IRATTAROZATLAN_UGYIRAT_ALLAPOT')
			UPDATE KRT_Parameterek SET Ertek = @ERTEK where Id = @RECORD_ID
		END
GO

GO
