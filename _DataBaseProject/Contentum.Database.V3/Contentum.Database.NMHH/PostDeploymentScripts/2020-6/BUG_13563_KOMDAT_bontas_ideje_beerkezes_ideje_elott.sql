﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD ='BOPMH')
	BEGIN 
	PRINT 'Executing: BUG_13563_KOMDAT_bontas_ideje_beerkezes_ideje_elott'
	
Update EREC_KuldKuldemenyek
   Set BeerkezesIdeje = DATEADD(HOUR, -1, BeerkezesIdeje) 
   where FelbontasDatuma < BeerkezesIdeje 

Update EREC_KuldKuldemenyek
   Set BeerkezesIdeje = FelbontasDatuma
   where FelbontasDatuma < BeerkezesIdeje
	END
GO
