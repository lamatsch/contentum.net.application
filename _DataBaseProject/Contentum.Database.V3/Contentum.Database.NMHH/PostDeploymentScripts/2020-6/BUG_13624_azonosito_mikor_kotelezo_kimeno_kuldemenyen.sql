﻿PRINT 'Executing: BUG_13624_azonosito_mikor_kotelezo_kimeno_kuldemenyen'
		IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = 'B2D6C8BD-F217-4D41-A865-9CB4D0A8EFE3')
		BEGIN
			insert into KRT_KodCsoportok
			(
				 Id
				,Kod
				,Nev
				,Modosithato
				,Note
				,BesorolasiSema
			) 
			values
			(
				'B2D6C8BD-F217-4D41-A865-9CB4D0A8EFE3'
				,'HIVATALOS_LEVEL'
				,'A hivatalos levélnek minősülő kimenő küldeménytípusokat tartalmazza.'
				,'1'
				,'A hivatalos levélnek minősülő kimenő küldeménytípusokat tartalmazza.'
				,'0'
			)
		END
		
		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='7E1E417F-84DB-4CF8-B3C8-6459894B4E11')
		BEGIN
			insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values ('7E1E417F-84DB-4CF8-B3C8-6459894B4E11','B2D6C8BD-F217-4D41-A865-9CB4D0A8EFE3','450B510A-7CAA-46B0-83E3-18445C0C53A9','1','Igen','0')
		END

		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='934BCD8F-94B5-452D-919B-49FA388AA24C')
		BEGIN
			insert into KRT_KodTarak(Id,Kodcsoport_id,Org,Kod,Nev,Modosithato) values ('934BCD8F-94B5-452D-919B-49FA388AA24C','B2D6C8BD-F217-4D41-A865-9CB4D0A8EFE3','450B510A-7CAA-46B0-83E3-18445C0C53A9','0','Nem','0')
		END

		IF NOT EXISTS (SELECT 1 FROM KRT_KODTARFUGGOSEG WHERE ID='5F886947-713D-429E-990A-8E840E3DCE0E')
		BEGIN
			INSERT [dbo].[KRT_KodtarFuggoseg] ([Id], [Org], [Vezerlo_KodCsoport_Id], [Fuggo_KodCsoport_Id], [Adat], [Aktiv]) 
			VALUES					(N'5F886947-713D-429E-990A-8E840E3DCE0E', N'450B510A-7CAA-46B0-83E3-18445C0C53A9', N'11F9DC99-3708-49BE-A5DA-F7B6DB343784', N'B2D6C8BD-F217-4D41-A865-9CB4D0A8EFE3', N'{}', N'1')
		END
 GO