﻿PRINT 'Executing: BUG_13684_bejovo_elektronikus_uzenetek_automatikus_feldolgozasa'

	if exists(SELECT * FROM sys.indexes WHERE name='IX_EREC_eBeadvanyok_Irany_KuldKuldemeny_Id_Cel_ErvKezd_ErvVege')
		BEGIN
			drop index IX_EREC_eBeadvanyok_Irany_KuldKuldemeny_Id_Cel_ErvKezd_ErvVege ON [dbo].[EREC_eBeadvanyok]
		END
	if not exists(SELECT * FROM sys.indexes WHERE name='IX_EREC_eBeadvanyok_Irany_KuldKuldemeny_Id__IraIrat_Id_Cel_ErvKezd_ErvVege') 	
		BEGIN 
			CREATE NONCLUSTERED INDEX [IX_EREC_eBeadvanyok_Irany_KuldKuldemeny_Id__IraIrat_Id_Cel_ErvKezd_ErvVege] ON [dbo].[EREC_eBeadvanyok]
		(
			[Irany] ASC,
			[KuldKuldemeny_Id] ASC,
			[IraIrat_id] ASC,
			[Cel] ASC,
			[ErvKezd] ASC,
			[ErvVege] ASC
		)
		INCLUDE([Id],[KR_ErkeztetesiDatum]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]		
		END
 GO
