﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD ='BOPMH')
	BEGIN 
	PRINT 'Executing: BUG_13527_XVIII_96_2017 ügyirat ügyintézőhöz visszaküldése'
	 UPDATE EREC_PldIratPeldanyok
	 SET Allapot = '45',
	     Csoport_Id_Felelos = '5E6B77B4-4994-EA11-80C2-0050569A6FBA',
         FelhasznaloCsoport_Id_Orzo = '5E6B77B4-4994-EA11-80C2-0050569A6FBA'
	 WHERE Id='f5e463b8-89d2-e611-90f7-0050569a6fba'
	END
GO
