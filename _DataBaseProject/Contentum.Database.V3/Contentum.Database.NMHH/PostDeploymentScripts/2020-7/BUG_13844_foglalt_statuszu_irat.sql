﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD ='CSPH')
	BEGIN 
	PRINT 'Executing: BUG_13844_Foglalt_státutuszú_irat'
	 UPDATE EREC_IraIratok
     SET Allapot = '04'
	 WHERE Id = '44B17695-D357-EA11-93FF-00155D144604'
	END
GO
