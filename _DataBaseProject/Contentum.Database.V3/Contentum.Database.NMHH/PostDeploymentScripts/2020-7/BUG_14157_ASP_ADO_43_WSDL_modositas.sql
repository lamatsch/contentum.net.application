﻿PRINT 'Executing: BUG_14157_ASP_ADO_43_WSDL_modositas'
	--WRITE YOUR SCRIPT HERE. PREPARE TO BE RUN MULTIPLE TIMES!
	/****** Object:  Index [NonClusteredIndex-20200729-220641]    Script Date: 7/29/2020 10:07:16 PM ******/
DROP INDEX [NonClusteredIndex-20200729-220641] ON [dbo].[KRT_Partnerek]
GO

/****** Object:  Index [NonClusteredIndex-20200729-220641]    Script Date: 7/29/2020 10:07:16 PM ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20200729-220641] ON [dbo].[KRT_Partnerek]
(
	[AspAdoTorolve] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



 GO
