﻿PRINT 'Executing: Bug_13233_Felulvizsgalati_lista_Egyeb_szervezetnek_atadhato_ugyiratok_eMigration-ben'
	
DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

DECLARE @Id uniqueidentifier = 'CFF2C30F-9326-4F0C-A916-ECEB6C36EB8D'

DECLARE @Ertek nvarchar(400)

IF @ORG_KOD = 'BOPMH'
	SET @Ertek = '1'
ELSE
	SET @Ertek = '0'


IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE Id = @Id)
BEGIN
	PRINT 'INSERT KRT_Parameterek: EMIGRATIONBEN_SELEJTEZES_JEGYZEKRE_HELYEZESSEL - ' + @Ertek

	INSERT INTO KRT_Parameterek
		(Id,
		Org,
		Nev,
		Ertek,
		Karbantarthato,
		Note)
	VALUES
		(@Id,
		@ORG_ID,
		'EMIGRATIONBEN_SELEJTEZES_JEGYZEKRE_HELYEZESSEL',
		@Ertek,
		'1',
		'Ha az értéke 1, akkor az eMigration-ben a selejtezés jegyzékre helyezéssel is végrehajtható. Ha 0 az értéke, akkor a selejtezés a hagyományos módon történik.')

END
 
GO


DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

DECLARE @menuID_Felulvizsgalat uniqueidentifier = '4A75ACC4-59AF-43C1-98C2-BBB948D7DF7E'
DECLARE @funckioID_FelulvizsgalatList uniqueidentifier = 'B3BCFACB-C449-42D7-80A3-602878D630B1'
DECLARE @modulID_FelulvizsgalatList uniqueidentifier = '0339B5D2-0D46-4859-9189-17A3E6D05D35'

DECLARE @menuID_Selejtezes uniqueidentifier = '67B71DF1-C5E5-4DED-AA31-C712C41C3CCB'


IF NOT EXISTS (SELECT 1 FROM KRT_Menuk WHERE Id = @menuID_Selejtezes)
BEGIN
	PRINT 'INSERT KRT_Menuk: Selejtezheto migralt tetelek keresese'

	INSERT INTO KRT_Menuk
	(Id,
	Menu_Id_Szulo,
	Kod,
	Nev,
	Funkcio_Id,
	Modul_Id,
	Parameter,
	Sorrend)
	VALUES
	(@menuID_Selejtezes,
	@menuID_Felulvizsgalat,
	'eMigration',
	'Selejtezhető migrált tételek keresése',
	@funckioID_FelulvizsgalatList,
	@modulID_FelulvizsgalatList,
	'Startup=SelejtezesSearchForm',
	'209')

END

DECLARE @menuID_LeveltarbaAdas uniqueidentifier = '47E8C53A-DA49-4B80-BF43-8678A73F3DF0'

IF NOT EXISTS (SELECT 1 FROM KRT_Menuk WHERE Id = @menuID_LeveltarbaAdas)
BEGIN
	PRINT 'INSERT KRT_Menuk: Leveltarba adhato migralt tetelek keresese'

	INSERT INTO KRT_Menuk
	(Id,
	Menu_Id_Szulo,
	Kod,
	Nev,
	Funkcio_Id,
	Modul_Id,
	Parameter,
	Sorrend)
	VALUES
	(@menuID_LeveltarbaAdas,
	@menuID_Felulvizsgalat,
	'eMigration',
	'Levéltárba adható migrált tételek keresése',
	@funckioID_FelulvizsgalatList,
	@modulID_FelulvizsgalatList,
	'Startup=LeveltarbaAdasSearchForm',
	'210')

END

GO
