﻿DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD ='BOPMH')
	BEGIN 
	PRINT 'Executing: BUG_13248_Nem_jon_letre_tertiveveny_HKP_postazaskor'
	
		INSERT INTO EREC_KuldTertivevenyek
			   (Kuldemeny_Id,
				Ragszam,
				Allapot,
				Ver,
				ErvKezd,
				ErvVege)
		 (Select k.Id,
				 e.KR_ErkeztetesiSzam,
				 '0',
				 1,
				 k.ErvKezd,
				 k.ErvVege
			 from EREC_KuldKuldemenyek k
				  inner join EREC_eBeadvanyok e on e.KuldKuldemeny_Id = k.Id
			Where k.KuldesMod = '19' And k.Allapot = '06' and k.PostazasIranya = 2 and k.Ragszam is null)	

		UPDATE EREC_KuldKuldemenyek  SET RagSzam = e.KR_ErkeztetesiSzam from EREC_KuldKuldemenyek k
              inner join EREC_eBeadvanyok e on e.KuldKuldemeny_Id = k.Id
		Where k.KuldesMod = '19' And k.Allapot = '06' and k.PostazasIranya = 2 and k.Ragszam is null 	
	END
GO
