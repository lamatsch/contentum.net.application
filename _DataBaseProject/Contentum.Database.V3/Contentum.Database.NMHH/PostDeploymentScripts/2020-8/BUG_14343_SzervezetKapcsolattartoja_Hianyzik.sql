﻿-- BUG_14343 Szervezet kapcsolattartója hiányzik
-- BLG_2070-ben létrehozva korábban, Prodon nem futott, ujrafuttatandó

-- BLG_2070: PARTNERKAPCSOLAT_TIPUSA kódcsoportba új kódtár érték: Szervezet kapcsolattartója

Print 'PARTNERKAPCSOLAT_TIPUSA kódcsoport'


 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_KodCsoportok
			where Id='A17AB343-1EB3-4035-868C-7E48F2AC3019') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'ERROR: Nincs PARTNERKAPCSOLAT_TIPUSA kódcsoport vagy nem jó az ID-ja' 

END 
ELSE 
BEGIN 
    Print 'Szervezet kapcsolattartója kódtárérték' 
	SET @record_id= null
    SET @record_id = (select Id from KRT_KodTarak
		where Id='F110F6AE-BB18-E811-80C9-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
		
		Print 'INSERT' 
			insert into KRT_KodTarak(
				KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			) values (
			'A17AB343-1EB3-4035-868C-7E48F2AC3019'
			,null
			,null
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'F110F6AE-BB18-E811-80C9-00155D020DD3'
			,'6'
			,'Szervezet kapcsolattartója'
			,''
			,'0'
			,'6'
			); 
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
		END 
		ELSE 
		BEGIN 
			Print 'UPDATE'
			UPDATE KRT_KodTarak
			SET 
				KodCsoport_Id='A17AB343-1EB3-4035-868C-7E48F2AC3019'
			,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Kod='6'
			,Nev='Szervezet kapcsolattartója'
			,RovidNev=''
			,Modosithato='0'
			,Sorrend='6'
			WHERE Id='F110F6AE-BB18-E811-80C9-00155D020DD3'
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
	END
END

GO
-- BLG_2070 end