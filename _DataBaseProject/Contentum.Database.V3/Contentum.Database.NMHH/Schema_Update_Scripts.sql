﻿/*
--------------------------------------------------------------------------------------				
--------------------------------------------------------------------------------------
SCHEMA Update Script							
--------------------------------------------------------------------------------------				
--------------------------------------------------------------------------------------
*/

PRINT 'BLG 3530'

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Ugy_Fajtaja' AND Object_ID = Object_ID(N'dbo.EREC_IraIratok'))
	ALTER TABLE [dbo].[EREC_IraIratok] 
	ADD [Ugy_Fajtaja] [nvarchar] (64)
GO
--------------------------------------------------------------------------------------


PRINT 'BLG 5177'
 
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'ElteltIdoUtolsoModositas' AND Object_ID = Object_ID(N'dbo.EREC_UgyUgyiratok'))
		ALTER TABLE [dbo].[EREC_UgyUgyiratok] 
		ADD [ElteltIdoUtolsoModositas] [datetime]
GO
--------------------------------------------------------------------------------------


PRINT 'BLG 4731'

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'NevSTR_Szallito' AND Object_ID = Object_ID(N'dbo.EREC_Szamlak'))
	ALTER TABLE [dbo].[EREC_Szamlak] 
	ADD NevSTR_Szallito [nvarchar] (400) null
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'CimSTR_Szallito' AND Object_ID = Object_ID(N'dbo.EREC_Szamlak'))
	ALTER TABLE [dbo].[EREC_Szamlak] 
	ADD CimSTR_Szallito [nvarchar] (400) null
GO

ALTER TABLE [dbo].[EREC_Szamlak] 
	ALTER COLUMN Partner_Id_Szallito UNIQUEIDENTIFIER NULL
GO
--------------------------------------------------------------------------------------
-- BUG_7153
SET ANSI_PADDING ON
GO

/****** Object:  Index [IX_EREC_EBEADVANYCSATOLMANYOK_eUzenetList]    Script Date: 3/11/2019 9:43:48 AM ******/
if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_EBEADVANYCSATOLMANYOK_eUzenetList' AND object_id = OBJECT_ID('EREC_eBeadvanyCsatolmanyok'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_EREC_EBEADVANYCSATOLMANYOK_eUzenetList] ON [dbo].[EREC_eBeadvanyCsatolmanyok]
	(
		   [eBeadvany_Id] ASC,
		   [Nev] ASC,
		   [ErvKezd] ASC,
		   [ErvVege] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
---------------------------------------BLG_7246---------------------------------------

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'MinositesFelulvizsgalatBizTag' AND Object_ID = Object_ID(N'dbo.EREC_IraIratok'))
BEGIN
	PRINT 'ADD Column EREC_IraIratok.MinositesFelulvizsgalatBizTag'
	ALTER TABLE [dbo].[EREC_IraIratok] 
	ADD [MinositesFelulvizsgalatBizTag] [nvarchar] (4000)
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'MinositesFelulvizsgalatBizTag' AND Object_ID = Object_ID(N'dbo.EREC_KuldKuldemenyek'))
BEGIN
	PRINT 'ADD Column EREC_KuldKuldemenyek.MinositesFelulvizsgalatBizTag'
	ALTER TABLE [dbo].[EREC_KuldKuldemenyek] 
	ADD [MinositesFelulvizsgalatBizTag] [nvarchar] (4000)
END
GO

--------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'MinositoSzervezet' AND Object_ID = Object_ID(N'dbo.EREC_KuldKuldemenyek'))
BEGIN

	PRINT 'Add Column EREC_KuldKuldemenyek.MinositoSzervezet'

	ALTER TABLE [dbo].[EREC_KuldKuldemenyek] 
	ADD [MinositoSzervezet] UNIQUEIDENTIFIER NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Partner_Id_BekuldoKapcsolt' AND Object_ID = Object_ID(N'EREC_KuldKuldemenyek'))
BEGIN
		PRINT 'ADD COLUMN EREC_KuldKuldemenyek.Partner_Id_BekuldoKapcsolt'
		ALTER TABLE [dbo].[EREC_KuldKuldemenyek] 
		ADD [Partner_Id_BekuldoKapcsolt] UNIQUEIDENTIFIER NULL
END
GO
--------------------------------------------------------------------------------------
PRINT 'BLG 5991'

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Erteknyilvanitas' AND Object_ID = Object_ID(N'dbo.EREC_KuldKuldemenyek'))
BEGIN

	PRINT 'Add Column EREC_KuldKuldemenyek.Erteknyilvanitas'

	ALTER TABLE [dbo].[EREC_KuldKuldemenyek] 
	ADD [Erteknyilvanitas] CHAR(1) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'ErteknyilvanitasOsszege' AND Object_ID = Object_ID(N'EREC_KuldKuldemenyek'))
BEGIN
		PRINT 'ADD COLUMN EREC_KuldKuldemenyek.ErteknyilvanitasOsszege'
		ALTER TABLE [dbo].[EREC_KuldKuldemenyek] 
		ADD [ErteknyilvanitasOsszege] INT NULL
END
GO
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Kapacitas' AND Object_ID = Object_ID(N'EREC_IrattariHelyek'))
BEGIN
		PRINT 'ADD COLUMN EREC_IrattariHelyek.Kapacitas'
		ALTER TABLE [dbo].[EREC_IrattariHelyek] 
		ADD [Kapacitas] FLOAT NULL
END
GO
----------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'IrattarHelyfoglalas' AND Object_ID = Object_ID(N'EREC_UgyUgyiratok'))
BEGIN
		PRINT 'ADD COLUMN EREC_UgyUgyiratok.IrattarHelyfoglalas'
		ALTER TABLE [dbo].[EREC_UgyUgyiratok] 
		ADD [IrattarHelyfoglalas] FLOAT NULL
END
GO
----------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Allapot' AND Object_ID = Object_ID(N'EREC_TomegesIktatasFolyamat'))
BEGIN
		PRINT 'ADD COLUMN EREC_TomegesIktatasFolyamat.Allapot'
		ALTER TABLE [dbo].[EREC_TomegesIktatasFolyamat] 
		ADD [Allapot] NVARCHAR(64) COLLATE Hungarian_CS_AS NULL
END
GO
----------------------------------------------------------------------------------------

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'IrattarId' AND Object_ID = Object_ID(N'dbo.EREC_KuldKuldemenyek'))
	ALTER TABLE [dbo].[EREC_KuldKuldemenyek] 
	ADD [IrattarId] [uniqueidentifier] NULL
GO
-----------------------------------------------------------------------------

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'IrattariHely' AND Object_ID = Object_ID(N'dbo.EREC_KuldKuldemenyek'))
	ALTER TABLE [dbo].[EREC_KuldKuldemenyek] 
	ADD [IrattariHely] [nvarchar](100) NULL
GO

-----------------------------------------------------------------------------

PRINT 'BLG 9124 -  EREC_KuldKuldemenyek.KulsoAzonosito'

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'KulsoAzonosito' AND Object_ID = Object_ID(N'dbo.EREC_KuldKuldemenyek'))
	ALTER TABLE [dbo].[EREC_KuldKuldemenyek] 
	ADD [KulsoAzonosito] [NVARCHAR] (400) NULL
GO
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'IrattarId' AND Object_ID = Object_ID(N'EREC_PldIratPeldanyok'))
BEGIN
		PRINT 'ADD COLUMN EREC_PldIratPeldanyok.IrattarId'
		ALTER TABLE [dbo].[EREC_PldIratPeldanyok] 
		ADD [IrattarId] UNIQUEIDENTIFIER NULL
END
GO

--------------------------------------BUG_6058------------------------------------------

PRINT 'BUG 6058'

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Expedialas' AND Object_ID = Object_ID(N'dbo.EREC_TomegesIktatasTetelek'))
BEGIN

	PRINT 'Add Column EREC_TomegesIktatasTetelek.Expedialas'

	ALTER TABLE [dbo].[EREC_TomegesIktatasTetelek] 
	ADD [Expedialas] INT NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'TobbIratEgyKuldemenybe' AND Object_ID = Object_ID(N'dbo.EREC_TomegesIktatasTetelek'))
BEGIN

	PRINT 'Add Column EREC_TomegesIktatasTetelek.TobbIratEgyKuldemenybe'

	ALTER TABLE [dbo].[EREC_TomegesIktatasTetelek] 
	ADD [TobbIratEgyKuldemenybe] INT NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'KuldemenyAtadas' AND Object_ID = Object_ID(N'dbo.EREC_TomegesIktatasTetelek'))
BEGIN

	PRINT 'Add Column EREC_TomegesIktatasTetelek.KuldemenyAtadas'
	ALTER TABLE [dbo].[EREC_TomegesIktatasTetelek] 
	ADD [KuldemenyAtadas] UNIQUEIDENTIFIER NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'HatosagiStatAdat' AND Object_ID = Object_ID(N'dbo.EREC_TomegesIktatasTetelek'))
BEGIN

	PRINT 'Add Column EREC_TomegesIktatasTetelek.HatosagiStatAdat'

	ALTER TABLE [dbo].[EREC_TomegesIktatasTetelek] 
	ADD [HatosagiStatAdat] Nvarchar(Max) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'EREC_IratAlairok' AND Object_ID = Object_ID(N'dbo.EREC_TomegesIktatasTetelek'))
BEGIN

	PRINT 'Add Column EREC_TomegesIktatasTetelek.EREC_IratAlairok'

	ALTER TABLE [dbo].[EREC_TomegesIktatasTetelek] 
	ADD [EREC_IratAlairok] Nvarchar(Max) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Lezarhato' AND Object_ID = Object_ID(N'dbo.EREC_TomegesIktatasTetelek'))
BEGIN

	PRINT 'Add Column EREC_TomegesIktatasTetelek.Lezarhato'

	ALTER TABLE [dbo].[EREC_TomegesIktatasTetelek] 
	ADD [Lezarhato] INT NULL
END
GO

-----------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'FeldolgozasStatusz' AND Object_ID = Object_ID(N'EREC_eBeadvanyok'))
BEGIN
		PRINT 'ADD COLUMN EREC_eBeadvanyok.FeldolgozasStatusz'
		ALTER TABLE [dbo].[EREC_eBeadvanyok] 
		ADD [FeldolgozasStatusz] INT NULL
END


IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'FeldolgozasiHiba' AND Object_ID = Object_ID(N'EREC_eBeadvanyok'))
BEGIN
		PRINT 'ADD COLUMN EREC_eBeadvanyok.FeldolgozasiHiba'
		ALTER TABLE [dbo].[EREC_eBeadvanyok] 
		ADD [FeldolgozasiHiba] NVARCHAR(4000) NULL
END
-----------------------------------------------------------------------------


GO
IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'INT_ETDR_Objektumok'))
BEGIN
	-- ADD TABLE INT_ETDR_Objektumok
    CREATE TABLE [dbo].[INT_ETDR_Objektumok]
	(
		   ETDR_Id int IDENTITY(1,1) PRIMARY KEY,
		   Obj_Tip_Id uniqueidentifier not null references KRT_ObjTipusok (Id),
		   Obj_Id uniqueidentifier not null, 
		   [ETDR_Data] NVARCHAR(MAX) NULL, 
		   [ETDR_Note] NVARCHAR(MAX) NULL, 
	)
	CREATE UNIQUE INDEX INT_ETDR_Objektumok_Idx_Obj_Id 
		On INT_ETDR_Objektumok (Obj_Tip_Id, Obj_Id) ;

END
GO


/*
--------------------------------------------------------------------------------------				
--------------------------------------------------------------------------------------
LAST COMMANDS							
--------------------------------------------------------------------------------------				
--------------------------------------------------------------------------------------
*/
GO
--------------------------------------------------------------------------------------
-- Frissiti az osszes view-t chema alapjan.
--------------------------------------------------------------------------------------
SET NOCOUNT ON

DECLARE @CURRENTVIEW VARCHAR(255)

DECLARE AllView CURSOR FAST_FORWARD
FOR
SELECT
	DISTINCT s.name + '.' + o.name AS ViewName
	FROM sys.objects o JOIN sys.schemas s ON o.schema_id = s.schema_id 
	WHERE	o.[type] = 'V'
		AND OBJECTPROPERTY(o.[object_id], 'IsSchemaBound') <> 1
		AND OBJECTPROPERTY(o.[object_id], 'IsMsShipped') <> 1

OPEN AllView

FETCH NEXT FROM AllView 
INTO @CurrentView

WHILE @@FETCH_STATUS = 0
BEGIN

	--PRINT @CurrentView
	EXEC sp_refreshview @CurrentView
	
	FETCH NEXT FROM AllView
	INTO @CurrentView
	
END
CLOSE AllView
DEALLOCATE AllView
GO
-----------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'UtolsoHasznalat' AND Object_ID = Object_ID(N'KRT_Partnerek'))
BEGIN
PRINT '11840 UtolsoHasznalat mező  hozzáadása a KRT_PartnerekTáblához'
		ALTER TABLE [dbo].[KRT_Partnerek] add[UtolsoHasznalat]  AS [dbo].[fn_GetPartnerUtolsoHasznalat]([Id])
END
GO
----------------------------------------------------------------------------

-----------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'AspAdoTorolve' AND Object_ID = Object_ID(N'KRT_Partnerek'))
BEGIN
PRINT '14157 AspAdoTorolve mező  hozzáadása a KRT_PartnerekTáblához'
		ALTER TABLE [dbo].[KRT_Partnerek] add [AspAdoTorolve]  char(1)  default '0'
END
GO
----------------------------------------------------------------------------

-----------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'CustomId' AND Object_ID = Object_ID(N'EREC_Csatolmanyok'))
BEGIN
PRINT 'CustomId mező  hozzáadása a EREC_Csatolmanyok'
		ALTER TABLE [dbo].[EREC_Csatolmanyok] add [CustomId]  char(1)  default '0'
END
GO
----------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'AspAdoTorolve' AND Object_ID = Object_ID(N'KRT_Partnerek'))
BEGIN
PRINT 'AspAdoTorolve mező  hozzáadása a KRT_Partnerek'
		ALTER TABLE [dbo].[KRT_Partnerek] add [AspAdoTorolve]  char(1)  default '0'
END
GO























--------------------------------------------------------------------------------------
-- Frissiti az osszes view-t chema alapjan.
--------------------------------------------------------------------------------------
SET NOCOUNT ON

DECLARE @CURRENTVIEW VARCHAR(255)

DECLARE AllView CURSOR FAST_FORWARD
FOR
SELECT
	DISTINCT s.name + '.' + o.name AS ViewName
	FROM sys.objects o JOIN sys.schemas s ON o.schema_id = s.schema_id 
	WHERE	o.[type] = 'V'
		AND OBJECTPROPERTY(o.[object_id], 'IsSchemaBound') <> 1
		AND OBJECTPROPERTY(o.[object_id], 'IsMsShipped') <> 1

OPEN AllView

FETCH NEXT FROM AllView 
INTO @CurrentView

WHILE @@FETCH_STATUS = 0
BEGIN

	--PRINT @CurrentView
	EXEC sp_refreshview @CurrentView
	
	FETCH NEXT FROM AllView
	INTO @CurrentView
	
END
CLOSE AllView
DEALLOCATE AllView
GO
----  BUG_13213  Partner_Id_CimzettKapcsolt mező hozzáadása az EREC_PldIratpeldanyok táblához ---------
IF NOT EXISTS(SELECT 1 FROM sys.columns 
    WHERE Name = N'Partner_Id_CimzettKapcsolt' AND Object_ID = Object_ID(N'EREC_PldIratPeldanyok'))
BEGIN
PRINT 'BUG_13213  Partner_Id_CimzettKapcsolt mező hozzáadása az EREC_PldIratpeldanyok táblához'
		ALTER TABLE [dbo].[EREC_PldIratPeldanyok] add [Partner_Id_CimzettKapcsolt] UNIQUEIDENTIFIER NULL 
END
GO
----------------------------------------------------------------------------
