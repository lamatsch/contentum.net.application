﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
PRINT ''
PRINT 'POST-DEPLOYMENT SCRIPT TEMPLATE: START'
PRINT ''

-- BLG_619
-- FELELOS_SZERV_KOD_MEGJELENITES rendszerparameter	 
-- FPH = BOPMH = 0, NMHH = 1

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='D2E92679-F9C5-E711-80C7-00155D027E9B') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'D2E92679-F9C5-E711-80C7-00155D027E9B'
		,'FELELOS_SZERV_KOD_MEGJELENITES'
		,'0'
		,'1'
		,'Felelős szervezeti egség kódjának megjelenítése (1=igen,0=nem)'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='D2E92679-F9C5-E711-80C7-00155D027E9B'
	--	,Nev='FELELOS_SZERV_KOD_MEGJELENITES'
	--	,Ertek='0'
	--	,Karbantarthato='1'
	--	,Note='Felelős szervezeti egség kódjának megjelenítése (1=igen,0=nem)'
	-- WHERE Id=@record_id 
 --END

-- if @org_kod = 'NMHH' 
--BEGIN 
--	Print @org_kod +': FELELOS_SZERV_KOD_MEGJELENITES = 1'
--	UPDATE KRT_Parameterek
--	   SET Ertek='1'
--	   WHERE Id='D2E92679-F9C5-E711-80C7-00155D027E9B'
--END

-- Add_eBeadvanyokFunkciok kezdete
update KRT_Funkciok
set Kod = REPLACE(Kod, 'DokumentumAdatok', 'eBeadvanyok')
where Kod like '%DokumentumAdatok%'

update KRT_Modulok
set Kod = REPLACE(Kod, 'DokumentumAdatok', 'eBeadvanyok')
where Kod Like '%DokumentumAdatok%'

update KRT_ObjTipusok
set Kod = 'EREC_eBeadvanyok'
    ,Nev = 'EREC_eBeadvanyok tábla'
where Kod = 'HKP_DokumentumAdatok'
-- Add_eBeadvanyokFunkciok vege
go

declare @Org uniqueidentifier
set @Org = '450b510a-7caa-46b0-83e3-18445c0c53a9'

declare @Tanz_Id uniqueidentifier
set @Tanz_Id = 'B93532A5-05FF-4D3E-91B1-4CF9284E6A75'

declare @Stat_Id uniqueidentifier
set @Stat_Id = 'a0848405-e664-4e79-8fab-cfeca4a290af'

DECLARE @record_id uniqueidentifier 
DECLARE @KodCsoport_Id uniqueidentifier


--szakrendszerek
set @KodCsoport_Id = '20BCD8F5-EA6E-E711-80C2-00155D020D7E'

SET @record_id = (select Id from KRT_KodCsoportok where Id=@KodCsoport_Id) 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
	insert into KRT_KodCsoportok(
	 BesorolasiSema
	,Id
	,Kod
	,Nev
	,Modosithato
	,Note
	,Tranz_id
	, Stat_id
	) values (
	'0'
	,@KodCsoport_Id
	,'RENDSZERKOD'
	,'Szakrendszerek rendszerkódja'
	,'1'
	, null
	, @Tanz_Id
	, @Stat_Id
	); 

END 
ELSE 
BEGIN 
	Print 'UPDATE'
	UPDATE KRT_KodCsoportok
	SET 
	BesorolasiSema='0'
	,Kod='RENDSZERKOD '
	,Nev='Szakrendszerek rendszerkódja'
	,Modosithato='1'	
	,Note=null
	,Tranz_id = @Tanz_Id
	,Stat_id = @Stat_Id
	WHERE Id=@KodCsoport_Id
	
END


set @KodCsoport_Id = '97B40CB9-53FC-E711-80C9-00155D020DD3'

SET @record_id = (select Id from KRT_KodCsoportok where Id=@KodCsoport_Id) 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
	insert into KRT_KodCsoportok(
	 BesorolasiSema
	,Id
	,Kod
	,Nev
	,Modosithato
	,Note
	,Tranz_id
	, Stat_id
	) values (
	'0'
	,@KodCsoport_Id
	,'RESTRICTED'
	,'Restricted'
	,'1'
	, null
	, @Tanz_Id
	, @Stat_Id
	); 

END 
ELSE 
BEGIN 
	Print 'UPDATE'
	UPDATE KRT_KodCsoportok
	SET 
	BesorolasiSema='0'
	,Kod='RESTRICTED '
	,Nev='Restricted'
	,Modosithato='1'	
	,Note=null
	,Tranz_id = @Tanz_Id
	,Stat_id = @Stat_Id
	WHERE Id=@KodCsoport_Id
	
END

SET @record_id = (select Id from KRT_KodTarak where Id='28F104F6-53FC-E711-80C9-00155D020DD3') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'28F104F6-53FC-E711-80C9-00155D020DD3'
    ,'IGEN'
    ,'Igen'
	,'1'
    ,'01'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='IGEN'
    ,Nev='Igen'
	,RovidNev='1'
    ,Sorrend='01'
    ,Modosithato='1'
    WHERE Id=@record_id
       
  END

SET @record_id = (select Id from KRT_KodTarak where Id='3773130F-54FC-E711-80C9-00155D020DD3') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'3773130F-54FC-E711-80C9-00155D020DD3'
    ,'NEM'
    ,'Nem'
	,'2'
    ,'02'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='NEM'
    ,Nev='Nem'
	,RovidNev='2'
    ,Sorrend='02'
    ,Modosithato='1'
    WHERE Id=@record_id
       
  END


go

-- AddPrarameter_eUzenetForras kezdete
declare @modulId uniqueidentifier
set @modulId = 'AA6D9944-40C0-40CD-8B75-9116B2E46C3D'

IF NOT EXISTS (Select 1 FROM INT_Modulok where Id = @modulId)
BEGIN
	INSERT INTO INT_Modulok
	(Id, Nev)
	VALUES
	(@modulId,'eUzenet')
END


declare @recordId uniqueidentifier
set @recordId = '7B452F3A-C485-473B-B905-629D4333E7EB'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @recordId)
BEGIN
	INSERT INTO INT_Parameterek
	(Id, Modul_id, Nev, Ertek, Karbantarthato)
	VALUES
	(@recordId, @modulId, 'eUzenetForras', 'ELHISZ','1')
END

-- schema
set @recordId = '524F7A66-6032-4E09-A233-6688270EB763'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @recordId)
BEGIN
	INSERT INTO INT_Parameterek
	(Id, Modul_id, Nev, Ertek, Karbantarthato)
	VALUES
	(@recordId, @modulId, 'eUzenetSchema', 'elhiszdb','1')
END
-- AddPrarameter_eUzenetForras vege

GO

-- BLG_619
-- FELELOS_SZERV_KOD_MEGJELENITES rendszerparameter	 
-- FPH = BOPMH = 0, NMHH = 1

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='D2E92679-F9C5-E711-80C7-00155D027E9B') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'D2E92679-F9C5-E711-80C7-00155D027E9B'
		,'FELELOS_SZERV_KOD_MEGJELENITES'
		,'0'
		,'1'
		,'Felelős szervezeti egség kódjának megjelenítése (1=igen,0=nem)'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='D2E92679-F9C5-E711-80C7-00155D027E9B'
	--	,Nev='FELELOS_SZERV_KOD_MEGJELENITES'
	--	,Ertek='0'
	--	,Karbantarthato='1'
	--	,Note='Felelős szervezeti egség kódjának megjelenítése (1=igen,0=nem)'
	-- WHERE Id=@record_id 
 --END

-- if @org_kod = 'NMHH' 
--BEGIN 
--	Print @org_kod +': FELELOS_SZERV_KOD_MEGJELENITES = 1'
--	UPDATE KRT_Parameterek
--	   SET Ertek='1'
--	   WHERE Id='D2E92679-F9C5-E711-80C7-00155D027E9B'
--END

GO

Print 'VISSZAFIZETES_JOGCIME kódcsoport'

declare @Org uniqueidentifier
set @Org = '450b510a-7caa-46b0-83e3-18445c0c53a9'

declare @Tanz_Id uniqueidentifier
set @Tanz_Id = 'A7523745-836D-4113-9B2B-3E0533C612F9'

declare @Stat_Id uniqueidentifier
set @Stat_Id = 'a0848405-e664-4e79-8fab-cfeca4a290af'

DECLARE @record_id uniqueidentifier 
DECLARE @KodCsoport_Id uniqueidentifier
set @KodCsoport_Id = 'DB84D958-E369-4B9D-BB20-8464A9CEBF94'

SET @record_id = (select Id from KRT_KodCsoportok where Id=@KodCsoport_Id) 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
	insert into KRT_KodCsoportok(
	 BesorolasiSema
	,Id
	,Kod
	,Nev
	,Modosithato
	,Note
	,Tranz_id
	, Stat_id
	) values (
	'0'
	,@KodCsoport_Id
	,'VISSZAFIZETES_JOGCIME '
	,'Visszafizetés jogcíme'
	,'1'
	,'Visszafizetés jogcíme: díj, illeték'
	, @Tanz_Id
	, @Stat_Id
	); 

END 
ELSE 
BEGIN 
	Print 'UPDATE'
	UPDATE KRT_KodCsoportok
	SET 
	BesorolasiSema='0'
	,Kod='VISSZAFIZETES_JOGCIME '
	,Nev='Visszafizetés jogcíme'
	,Modosithato='1'	
	,Note='Visszafizetés jogcíme: díj, illeték'
	,Tranz_id = @Tanz_Id
	,Stat_id = @Stat_Id
	WHERE Id=@KodCsoport_Id
	
END



-- díj
SET @record_id = (select Id from KRT_KodTarak where Id='308CBC6A-DC2F-471E-BEC9-9B9AC47E1749') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'308CBC6A-DC2F-471E-BEC9-9B9AC47E1749'
    ,'01'
    ,'Díj'
    ,'01'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='01'
    ,Nev='Díj'
    ,Sorrend='01'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END

-- illeték
SET @record_id = (select Id from KRT_KodTarak where Id='A74A5DD2-3A8A-4AB1-8CD7-CDF9A8D38BDD') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tanz_Id
    ,@Stat_id
    ,@Org
    ,'A74A5DD2-3A8A-4AB1-8CD7-CDF9A8D38BDD'
    ,'02'
    ,'Illeték'
    ,'02'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tanz_Id
    ,Stat_id=@Stat_id
    ,Org=@Org
    ,Id=@record_id
    ,Kod='02'
    ,Nev='Illeték'
    ,Sorrend='02'
    ,Modosithato='1'
    WHERE Id=@record_id 
       
  END

GO


/*---------------------------------------------------------------------------------------*/
/*BLG_577*/
PRINT ''
PRINT 'BLG_577 START'

DECLARE @BLG_577_Kcs_Record_id UNIQUEIDENTIFIER 
SET @BLG_577_Kcs_Record_id = '75CD041F-DE57-44EC-B56B-02D531280C93'
DECLARE @BLG_577_Kod_Record_id UNIQUEIDENTIFIER
SET @BLG_577_Kod_Record_id= '1696FF50-F26E-E711-80C2-00155D020D7E'

IF NOT EXISTS(select Id from KRT_KodCsoportok where Id=@BLG_577_Kcs_Record_id)
	BEGIN 
		Print 'ERROR: Nincs IRAT_MINOSITES_BIZALMAS kódcsoport hianyzik' 
	END 
ELSE 
BEGIN 

	Print 'IRAT_MINOSITES kódtárérték: Mind' 
	IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE Id = @BLG_577_Kod_Record_id)
	BEGIN 
		DECLARE @BLG_577_Org_id UNIQUEIDENTIFIER
		SELECT @BLG_577_Org_id = Org FROM KRT_KodTarak WHERE Id = @BLG_577_Kod_Record_id
		Print 'INSERT' 
		INSERT INTO KRT_KodTarak(
			KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			@BLG_577_Kcs_Record_id
			,null
			,null
			,@BLG_577_Org_id
			,@BLG_577_Kod_Record_id
			,'40'
			,'Mind'
			,'Mind'
			,'0'
			,'40'
			,'Irat minosites bizalmas: Mind'
			); 
		if @@ERROR<>0 
			begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
	END 
END

GO

/*---------------------------------------------------------------------------------------*/
Print 'PARTNERKAPCSOLAT_TIPUSA KÓDCSOPORT'

DECLARE @BLG_577_B_KCS_PartnerKapcsTipus UNIQUEIDENTIFIER 
SELECT @BLG_577_B_KCS_PartnerKapcsTipus= ID FROM KRT_KodCsoportok where kod = 'PARTNERKAPCSOLAT_TIPUSA'

DECLARE @BLG_577_B_Kodtar_Id UNIQUEIDENTIFIER
SET @BLG_577_B_Kodtar_Id= 'EF2A141F-46B2-4297-9407-7DBC31F482DD'

DECLARE @BLG_577_B_ORG_ID uniqueidentifier 

IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok where Id=@BLG_577_B_KCS_PartnerKapcsTipus)
	BEGIN 
		Print 'ERROR: Nincs PARTNERKAPCSOLAT_TIPUSA Kódcsoport.' 
	END 
ELSE 
	BEGIN 
		
		SET @BLG_577_B_ORG_ID = '450b510a-7caa-46b0-83e3-18445c0c53a9'

		Print 'SZERVEZET ASSZISZTENSE Kódtárérték' 

		IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @BLG_577_B_Kodtar_Id)
		BEGIN 
		
			Print 'INSERT' 
			insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @BLG_577_B_KCS_PartnerKapcsTipus
			,null
			,null
			,@BLG_577_B_ORG_ID
			,@BLG_577_B_Kodtar_Id
			,'SZA'
			,'Szervezet asszisztense'
			,'Assziszten'
			,'0'
			,'SZA'
			,'Szervezet asszisztense'
			); 
		END 
	
	END
/*---------------------------------------------------------------------------------------*/

PRINT 'BLG_577 STOP'

GO
/*BLG_577*/
/*---------------------------------------------------------------------------------------*/






-- BLG_587 PeresEloadoiIvNyomtatas Funkciókód Felvitele

Print 'PeresEloadoiIvNyomtatas Funkcio felvitele'
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='584E044A-5BF4-E711-80C9-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
		,'0'
		,'584E044A-5BF4-E711-80C9-00155D020DD3'
		,'PeresEloadoiIvNyomtatas'
		,'Peres előadói ív nyomtatás'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
		,Modosithato='0'
		,Id='584E044A-5BF4-E711-80C9-00155D020DD3'
		,Kod='PeresEloadoiIvNyomtatas'
		,Nev='Peres előadói ív nyomtatás'
	 WHERE Id=@record_id 
 END
go

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9')

IF @org_kod = 'NMHH'
BEGIN
	delete from KRT_Szerepkor_Funkcio
	where Funkcio_Id = '584E044A-5BF4-E711-80C9-00155D020DD3'
END
ELSE
BEGIN	
Print 'KRT_Szerepkor_Funkcio - hozzarendeles FEJLESZTO-hoz (PeresEloadoiIvNyomtatas)'
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='555ED1E2-5EF4-E711-80C9-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'e855f681-36ed-41b6-8413-576fcb5d1542'
			,'555ED1E2-5EF4-E711-80C9-00155D020DD3'
			,'584E044A-5BF4-E711-80C9-00155D020DD3'
			);
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Szerepkor_Funkcio
		 SET 
			 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
			,Id='555ED1E2-5EF4-E711-80C9-00155D020DD3'
			,Funkcio_Id='584E044A-5BF4-E711-80C9-00155D020DD3'
		 WHERE Id=@record_id
	 END
END

GO

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
if @org_kod = 'FPH' 
BEGIN 
	Print 'KRT_Szerepkor_Funkcio - hozzarendeles FPH_WordIktato-hoz csak FPH-ban (PeresEloadoiIvNyomtatas)'	 
	DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
				where Id='B4566519-DEF5-E711-80C9-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
			
		Print 'INSERT' 
		insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'C9DC51D8-9765-DE11-8138-001EC9E754BC'
			,'B4566519-DEF5-E711-80C9-00155D020DD3'
			,'584E044A-5BF4-E711-80C9-00155D020DD3'
			);
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Szerepkor_Funkcio
		 SET 
			 Szerepkor_Id='C9DC51D8-9765-DE11-8138-001EC9E754BC'
			,Id='B4566519-DEF5-E711-80C9-00155D020DD3'
			,Funkcio_Id='584E044A-5BF4-E711-80C9-00155D020DD3'
		 WHERE Id=@record_id
	 END
END
go
-- BLG_587 PeresEloadoiIvNyomtatas Funkciókód Felvitele vége

-- BLG_1070 KRT_Nezetek definiálás NMHH_Szamla-hoz

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
if @org_kod = 'NMHH' 
BEGIN 
	Print 'KRT_Nezetek definiálás NMHH_Szamla-hoz'
	DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Nezetek
				where Id='B36430F1-38F1-E711-80C9-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
			
		Print 'INSERT' 
		INSERT [dbo].[KRT_Nezetek] ([Id], [Org], [Form_Id], [Nev], [Leiras], [Csoport_Id], [Muvelet_Id], [DisableControls], [InvisibleControls], [ReadOnlyControls], [Prioritas]) 
						VALUES (N'b36430f1-38f1-e711-80c9-00155d020dd3', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'78471b4c-ee48-4268-8a26-a99e4fc85e4d', N'NMHH_Szamla', NULL, NULL, NULL, NULL, N'ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$cbDevizaKodMegorzes,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$tr_Adoszam_PIRAllapot,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$tr_Bankszla,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$tr_SzlaFizetesiHatarideje,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$FizMod,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$FizMod2,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$tr_SzlaTeljesitesIdopontja,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$labelMegjegyzes,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$Megjegyzes_TextBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$tr_SzlaMegjegyzes,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$tr_SzlaBruttoVegosszeg,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$labelSzamlaBruttoVegosszege,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$SzamlaBruttoVegosszege_NumberTextBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$Labels25',NULL, 1)

	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Nezetek
		 SET 
			 Form_Id ='78471b4c-ee48-4268-8a26-a99e4fc85e4d'
			,Nev='NMHH_Szamla'
			,DisableControls=NULL
			,InvisibleControls = 'ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$cbDevizaKodMegorzes,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$tr_Adoszam_PIRAllapot,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$tr_Bankszla,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$tr_SzlaFizetesiHatarideje,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$FizMod,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$FizMod2,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$tr_SzlaTeljesitesIdopontja,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$labelMegjegyzes,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$Megjegyzes_TextBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$tr_SzlaMegjegyzes,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$tr_SzlaBruttoVegosszeg,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$labelSzamlaBruttoVegosszege,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$SzamlaAdatokPanel$SzamlaBruttoVegosszege_NumberTextBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$Labels25'
			,ReadOnlyControls = NULL
			,Prioritas = 1
		 WHERE Id=@record_id
	 END
END
go

-- BLG_1070 KRT_Nezetek definiálás NMHH_Szamla-hoz vége

-- BLG_292
-- IKTATOSZAM_FORMATUM, ERKEZTETOSZAM_FORMATUM rendszerparameter	 
-- TÜK-os iktatószám formátum pl. így nézne ki a rendszer paraméterben: *U*$K.MegkulJelzes$-#U#*U*$K.Azonosito$-#U#*U*$K.Iktatohely$-#U#*U*$U.Foszam$#U#*I*-$I.Alszam$#I#*U*/$K.Ev$#U#*P* ($P.Sorszam$)#P#
-- FPH-s iktatószám pedig így nézne ki: *U*$K.Iktatohely$ /#U#*U*$U.Foszam$#U#*I* - $I.Alszam$#I#*U* /$K.Ev$#U#*U* /$K.MegkulJelzes$#U#*P* ($P.Sorszam$)#P#
-- BOPMH-s iktatószám pedig így nézne ki: *U*$K.Iktatohely$/#U#*U*$U.Foszam$#U#*I*-$I.Alszam$#I#*U*/$K.Ev$#U#*U*/$K.MegkulJelzes$#U#*P* ($P.Sorszam$)#P#
-- Érkeztetőszám: *K*$K.MegkulJelzes$ - #K#*K*$E.Erkezteto_Szam$#K#*K* / $K.Ev$#K#
-- Érkeztetőszám BOPMH: *K*$K.MegkulJelzes$-#K#*K*$E.Erkezteto_Szam$#K#*K*/$K.Ev$#K#

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='224E5D1E-96D6-E711-80C7-00155D027E9B') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

print 'Iktatószám formátum'
if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'224E5D1E-96D6-E711-80C7-00155D027E9B'
		,'IKTATOSZAM_FORMATUM'
		,'*U*$K.Iktatohely$ /#U#*U*$U.Foszam$#U#*I* - $I.Alszam$#I#*U* /$K.Ev$#U#*U* /$K.MegkulJelzes$#U#*P* ($P.Sorszam$)#P#'
		,'1'
		,'Iktatószám fomátuma'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='224E5D1E-96D6-E711-80C7-00155D027E9B'
	--	,Nev='IKTATOSZAM_FORMATUM'
	--	,Ertek='*U*$K.Iktatohely$ /#U#*U*$U.Foszam$#U#*I* - $I.Alszam$#I#*U* /$K.Ev$#U#*U* /$K.MegkulJelzes$#U#*P* ($P.Sorszam$)#P#'
	--	,Karbantarthato='1'
	--	,Note='Iktatószám fomátuma'
	-- WHERE Id=@record_id 
 --END

 -- 2018 év elejéig kiszedve (addig marad az fph-s, régi)
--if @org_kod = 'BOPMH' 
--BEGIN 
--	Print @org_kod +': IKTATOSZAM_FORMATUM = *U*$K.Iktatohely$/#U#*U*$U.Foszam$#U#*I*-$I.Alszam$#I#*U*/$K.Ev$#U#*U*/$K.MegkulJelzes$#U#*P* ($P.Sorszam$)#P#'
--	UPDATE KRT_Parameterek
--	   SET Ertek='*U*$K.Iktatohely$/#U#*U*$U.Foszam$#U#*I*-$I.Alszam$#I#*U*/$K.Ev$#U#*U*/$K.MegkulJelzes$#U#*P* ($P.Sorszam$)#P#'
--	   WHERE Id='224E5D1E-96D6-E711-80C7-00155D027E9B'
--END


DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
-- if @isTUK = '1' 
--BEGIN 
--	Print @org_kod +' - TUK : IKTATOSZAM_FORMATUM = *U*$K.MegkulJelzes$-#U#*U*$K.Azonosito$-#U#*U*$K.Iktatohely$-#U#*U*$U.Foszam$#U#*I*-$I.Alszam$#I#*U*/$K.Ev$#U#*P* ($P.Sorszam$)#P#'
--	UPDATE KRT_Parameterek
--	   SET Ertek='*U*$K.MegkulJelzes$-#U#*U*$K.Azonosito$-#U#*U*$K.Iktatohely$-#U#*U*$U.Foszam$#U#*I*-$I.Alszam$#I#*U*/$K.Ev$#U#*P* ($P.Sorszam$)#P#'
--	   WHERE Id='224E5D1E-96D6-E711-80C7-00155D027E9B'
--END

print 'Érkeztető szám'
SET @record_id = null 
SET @record_id = (select Id from KRT_Parameterek
			where Id='6017A889-BFD9-E711-80C7-00155D027E9B') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'6017A889-BFD9-E711-80C7-00155D027E9B'
		,'ERKEZTETOSZAM_FORMATUM'
		,'*K*$K.MegkulJelzes$ -#K#*K* $E.Erkezteto_Szam$#K#*K* / $K.Ev$#K#'
		,'1'
		,'Érkeztetőszám fomátuma'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='6017A889-BFD9-E711-80C7-00155D027E9B'
	--	,Nev='ERKEZTETOSZAM_FORMATUM'
	--	,Ertek='*K*$K.MegkulJelzes$ -#K#*K* $E.Erkezteto_Szam$#K#*K* / $K.Ev$#K#'
	--	,Karbantarthato='1'
	--	,Note='Érkeztetőszám fomátuma'
	-- WHERE Id=@record_id 
 --END

-- 2018 év elejéig kiszedve (addig marad az fph-s, régi)
--  if @org_kod = 'BOPMH' 
--BEGIN 
--	Print @org_kod +': ERKEZTETOSZAM_FORMATUM = *K*$K.MegkulJelzes$-#K#*K*$E.Erkezteto_Szam$#K#*K*/$K.Ev$#K#'
--	UPDATE KRT_Parameterek
--	   SET Ertek='*K*$K.MegkulJelzes$-#K#*K*$E.Erkezteto_Szam$#K#*K*/$K.Ev$#K#'
--	   WHERE Id='6017A889-BFD9-E711-80C7-00155D027E9B'
--END

GO

--------------------------------------------------------------------------

print ''
print '---'
print '/* ----- Levéltári átadás #315 ----- */'
print '/* --- KRT_Parameterek tábla ''LEVELTARI_ATADAS_*'' paraméterek beszúrása --- */'
print '/* Kezdete: ' + convert( nvarchar(30), sysdatetime() ) + ' */'
print '---'

BEGIN
  print ''
  print '- Ellenőrzések -'

  declare cur_ORG CURSOR FOR
                     select ID, Kod, Nev from KRT_Orgok;
  declare @Org_ID  uniqueidentifier
         ,@Org_Kod nvarchar(100)
		 ,@Org_Nev nvarchar(100)
		 ;

  declare @AxisUser uniqueidentifier;
  --- SET @AdminUser = convert(uniqueidentifier,'54E861A5-36ED-44CA-BAA7-C287D125B309')
  --- SET @AxisUser = convert(uniqueidentifier,'838DB5BB-B8C6-E611-90F7-0050569A6FBA')
  declare @n_count int = 0
        , @n_siker int = 0
		;

  declare @ResultUID uniqueidentifier;
  declare @xml xml
		, @ExecTime datetime
		;
  SET @xml=convert(xml,N'<root><Nev/><Ertek/><Karbantarthato/><Note/><Ver/><Letrehozo_ID/><LetrehozasIdo/></root>');
  SET @ExecTime = getdate()                              --- mert egyébként nem tudom átadni paraméterként !?

  declare 
          @Atado_Rovidneve        nvarchar(400)
		 ,@Atadas_Dokumentum      nvarchar(400)
		 ,@Atadas_Iktato          nvarchar(400)
		 ,@Atadas_Tipus           nvarchar(400) 
		 ,@Iratkepzo_Neve         nvarchar(400)
		 ,@Iratkepzo_Azonositoja  nvarchar(400)
         ,@Leveltar_Neve          nvarchar(400)
         ,@Leveltar_Azonositoja   nvarchar(400)
         ;
  					 
  open cur_ORG;
  fetch next from cur_ORG into @Org_ID, @Org_Kod, @Org_Nev;
  WHILE @@FETCH_STATUS = 0  
  BEGIN 
    SET @n_count = 0;
    SET @n_siker = 0;
					 
    SET @AxisUser = convert(uniqueidentifier, Null);					 
    SET @AxisUser = ( select ID from KRT_Felhasznalok f
                       where f.Nev collate database_default = 'Adminisztrátor' collate database_default
                         and f.Org = @Org_Id );
    if @AxisUser is Null
      SET @AxisUser = ( select ID from KRT_Felhasznalok f
                         where Nev collate database_default = 'Axis felhasználó' collate database_default
						   and f.Org = @Org_Id );
    if @AxisUser is Null 
	  begin
	    print '';
	    print 'Axis felhasználó nem definiált - a ''Levéltári átadás'' paraméterei nem kerülnek felvételre ( Org: ' + @Org_Kod + ' ).';
	    --- RETURN;     /* ORG ciklusban vagyunk - nem hagyhatunk el minden ágat */        --- Itt kiszáll - !!! de csak az adott BATCH-ből
	  end;
    else 
	  begin
		print '';
        print '- LEVELTARI_ATADAS_* paraméterek INSERT ( Org: ' +@Org_Kod + ' ) -';

        /* ??? PARAMETEREK ???  */	  
        SET @Atado_Rovidneve        = ISNULL(@Org_Kod, N'<Iratképző rövidneve>');                      --- 'FPH'
        SET @Atadas_Dokumentum      = N'<Átvételi csomag típusát leíró dokumentum címe>;<elérhetősége>';
        SET @Atadas_Iktato          = N'<Átadás-átvételi megállapodás iktatószáma>;<dátuma>';
        SET @Atadas_Tipus           = N'TANÚSÍTOTT IKTATÁS';
        SET @Iratkepzo_Neve         = ISNULL(@Org_Nev, N'<Iratképző megnevezése>');                      --- 'Főpolgármesteri hivatal'
        SET @Iratkepzo_Azonositoja  = N'<Iratképző azonosítója>';                    --- '1357924680'      
        SET @Leveltar_Neve          = N'Magyar Nemzeti Levéltár';
        SET @Leveltar_Azonositoja   = N'<Levéltár azonosítója>';                     --- '0864297531'
        /* - */

---
        SET @n_count +=1
        if NOT exists( select 1 from [KRT_Parameterek] p 
	                    where p.Nev = 'LEVELTARI_ATADAS_IRATKEPZO_ROVIDNEVE'
			              and p.Org = @Org_Id )
          begin
            exec [sp_KRT_ParameterekInsert] 
               @Nev   = 'LEVELTARI_ATADAS_IRATKEPZO_ROVIDNEVE'
	          ,@Ertek = @Atado_Rovidneve
              ,@Karbantarthato = 0
	          ,@Note = 'Az átadó elnevezéséből képzett, legfeljebb húsz, ékezet nélküli nagybetűs karakterből álló egyedi azonosító'
              ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
              ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
              ,@LetrehozasIdo  = @ExecTime
              ,@UpdatedColumns = @xml
	          ,@ResultUID = @ResultUID
	          ;
            if @@ERROR = 0                  --- ha ennyi hibakezelés nem elég, akkor TRY / CATCH blokkok kellenek
              SET @n_siker += 1; 
          end;

---
        SET @n_count +=1
        if NOT exists( select 1 from [KRT_Parameterek] p 
	                    where p.Nev = 'LEVELTARI_ATADAS_TIPUS'
			              and p.Org = @Org_Id )
          begin
            exec [sp_KRT_ParameterekInsert] 
               @Nev   = 'LEVELTARI_ATADAS_TIPUS'
	          ,@Ertek = @Atadas_Tipus
              ,@Karbantarthato = 0
	          ,@Note = 'Az elektronikus átvételi csomag átadási típusa: „tanúsított iratkezelési szoftver”, amelynek reprezentációja „TANÚSÍTOTT IKTATÁS”;... '
              ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
              ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
              ,@LetrehozasIdo  = @ExecTime
              ,@UpdatedColumns = @xml
	          ,@ResultUID = @ResultUID
	          ;
            if @@ERROR = 0                  --- ha ennyi hibakezelés nem elég, akkor TRY / CATCH blokkok kellenek
              SET @n_siker += 1; 
          end;

---
        SET @n_count +=1
        if NOT exists( select 1 from [KRT_Parameterek] p 
	                    where p.Nev = 'LEVELTARI_ATADAS_DOKUMENTUM'
			              and p.Org = @Org_Id )
          begin
            exec [sp_KRT_ParameterekInsert] 
               @Nev   = 'LEVELTARI_ATADAS_DOKUMENTUM'
	          ,@Ertek = @Atadas_Dokumentum
              ,@Karbantarthato = 0
	          ,@Note = 'Az elektronikus átvételi csomag átvételi típusát részletesen meghatározó dokumentum címe és elérhetősége.'
              ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
              ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
              ,@LetrehozasIdo  = @ExecTime
              ,@UpdatedColumns = @xml
	          ,@ResultUID = @ResultUID
	          ;
            if @@ERROR = 0                  --- ha ennyi hibakezelés nem elég, akkor TRY / CATCH blokkok kellenek
              SET @n_siker += 1; 
          end;

---
        SET @n_count +=1
        if NOT exists( select 1 from [KRT_Parameterek] p 
	                    where p.Nev = 'LEVELTARI_ATADAS_IKTATO'
			              and p.Org = @Org_Id )
          begin
            exec [sp_KRT_ParameterekInsert] 
               @Nev   = 'LEVELTARI_ATADAS_IKTATO'
	          ,@Ertek = @Atadas_Iktato
              ,@Karbantarthato = 0
	          ,@Note = 'Az iratátadó és a levéltár között létrejött levéltári átadás-átvételi megállapodás iktatószáma és dátuma. A két adatot pontosvesszővel kell elválasztani.'
              ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
              ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
              ,@LetrehozasIdo  = @ExecTime
              ,@UpdatedColumns = @xml
	          ,@ResultUID = @ResultUID
	          ;
            if @@ERROR = 0                  --- ha ennyi hibakezelés nem elég, akkor TRY / CATCH blokkok kellenek
              SET @n_siker += 1; 
          end;

---
        SET @n_count +=1
        if NOT exists( select 1 from [KRT_Parameterek] p 
	                    where p.Nev = 'LEVELTARI_ATADAS_IRATKEPZO_NEVE'
			              and p.Org = @Org_Id )
          begin
            exec [sp_KRT_ParameterekInsert] 
               @Nev   = 'LEVELTARI_ATADAS_IRATKEPZO_NEVE'
	          ,@Ertek = @Iratkepzo_Neve
              ,@Karbantarthato = 0
	          ,@Note = 'Az elektronikus átvételi csomagban szereplő iratokat keletkeztető szervezet pontos neve.'
              ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
              ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
              ,@LetrehozasIdo  = @ExecTime
              ,@UpdatedColumns = @xml
	          ,@ResultUID = @ResultUID
	          ;
            if @@ERROR = 0                  --- ha ennyi hibakezelés nem elég, akkor TRY / CATCH blokkok kellenek
              SET @n_siker += 1; 
          end;

---
        SET @n_count +=1
        if NOT exists( select 1 from [KRT_Parameterek] p 
	                    where p.Nev = 'LEVELTARI_ATADAS_IRATKEPZO_AZONOSITOJA'
			              and p.Org = @Org_Id )
          begin
            exec [sp_KRT_ParameterekInsert] 
               @Nev   = 'LEVELTARI_ATADAS_IRATKEPZO_AZONOSITOJA'
	          ,@Ertek = @Iratkepzo_Azonositoja
              ,@Karbantarthato = 0
	          ,@Note = 'Az elektronikus átvételi csomagban szereplő iratokat keletkeztető szervezet egyedi azonosítója.'
              ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
              ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
              ,@LetrehozasIdo  = @ExecTime
              ,@UpdatedColumns = @xml
	          ,@ResultUID = @ResultUID
	          ;
            if @@ERROR = 0                  --- ha ennyi hibakezelés nem elég, akkor TRY / CATCH blokkok kellenek
              SET @n_siker += 1; 
          end;

---
        SET @n_count +=1
        if NOT exists( select 1 from [KRT_Parameterek] p 
	                    where p.Nev = 'LEVELTARI_ATADAS_LEVELTAR_NEVE'
			              and p.Org = @Org_Id )
          begin
            exec [sp_KRT_ParameterekInsert] 
               @Nev   = 'LEVELTARI_ATADAS_LEVELTAR_NEVE'
	          ,@Ertek = @Leveltar_Neve
              ,@Karbantarthato = 0
	          ,@Note = 'Annak a levéltári szervezetnek a neve, amely az elektronikus átvételi csomag befogadását végzi.'
              ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
              ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
              ,@LetrehozasIdo  = @ExecTime
              ,@UpdatedColumns = @xml
	          ,@ResultUID = @ResultUID
	          ;
            if @@ERROR = 0                  --- ha ennyi hibakezelés nem elég, akkor TRY / CATCH blokkok kellenek
              SET @n_siker += 1; 
          end;

---
        SET @n_count +=1
        if NOT exists( select 1 from [KRT_Parameterek] p 
	                    where p.Nev = 'LEVELTARI_ATADAS_LEVELTAR_AZONOSITOJA'
			              and p.Org = @Org_Id )
          begin
            exec [sp_KRT_ParameterekInsert] 
               @Nev   = 'LEVELTARI_ATADAS_LEVELTAR_AZONOSITOJA'
	          ,@Ertek = @Leveltar_Azonositoja
              ,@Karbantarthato = 0
	          ,@Note = 'Annak a levéltári szervezetnek az azonosítója, amely az elektronikus átvételi csomag befogadását végzi.'
              ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
              ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
              ,@LetrehozasIdo  = @ExecTime
              ,@UpdatedColumns = @xml
	          ,@ResultUID = @ResultUID
	          ;
            if @@ERROR = 0                  --- ha ennyi hibakezelés nem elég, akkor TRY / CATCH blokkok kellenek
              SET @n_siker += 1; 
          end;
      end;
	  
---

  print ''
  print '---------------'
  print 'Összesen: ' + convert( nvarchar(10), @n_count );
  print 'Sikeres : ' + convert( nvarchar(10), @n_siker );
  print '---------------'	  
	  
	fetch next from cur_ORG into @Org_ID, @Org_Kod, @Org_Nev;
  END;                                                             --- ORG cursor
  Close cur_ORG;
  Deallocate cur_ORG; 
		  
END;

  --select substring( p.Nev,1,40 ) as 'Név'
		--,substring( p.Ertek,1,50 ) as 'Érték'
  --  FROM [KRT_Parameterek] p
  -- where p.Nev like 'LEVELTARI_ATADAS_%'
  -- order by p.Nev, p.LetrehozasIdo
  --;

print ''
print '/* --- Vége --- ' + convert( nvarchar(30), sysdatetime() ) + ' */'
GO

-- CREATE_LEVELTARI_ADATCSOMAG rendszerparameter	 
print 'CREATE_LEVELTARI_ADATCSOMAG rendszerparameter'

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='78C2CD19-EBEA-41FE-891F-3A32AD535E14') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'78C2CD19-EBEA-41FE-891F-3A32AD535E14'
		,'CREATE_LEVELTARI_ADATCSOMAG'
		,'1'
		,'1'
		,'Levéltári átadáskor létejöjjön-e átadási atadcsomag.'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='78C2CD19-EBEA-41FE-891F-3A32AD535E14'
	--	,Nev='CREATE_LEVELTARI_ADATCSOMAG'
	--	,Ertek='1'
	--	,Karbantarthato='1'
	--	,Note='Levéltári átadáskor létejöjjön-e átadási atadcsomag.'
	-- WHERE Id=@record_id 
 --END

 GO

 --Nézetek engedélyezése
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
--if @org_kod = 'NMHH' 
--BEGIN 
--	print 'nézetek engedélyezése'
--	UPDATE KRT_Parameterek
--		Set Ertek = '1'
--	WHERE Id = '0F42EC73-EBE9-405C-84E9-9E2A00C76967'
--END

GO

-- Elektronikus kiküldés technikai felhasználó
declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
declare @felhasznaloId uniqueidentifier = '3B0D45F7-429C-4A13-8E30-9E561B5C1921'

IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalok WHERE Id = @felhasznaloId)
BEGIN
	print 'INSERT KRT_Felhasznalok - Elektronikus kiküldés technikai felhasználó'

	INSERT INTO KRT_Felhasznalok
	(
		Id,
		Org,
		Tipus,
		UserNev,
		Nev,
		[System],
		Engedelyezett,
		Jelszo
	)
	VALUES
	(
		@felhasznaloId,
		@org,
		'Basic',
		'Elhisz',
		'Elektronikus kiküldés technikai felhasználó',
		'1',
		'1',
		'7C4A8D09CA3762AF61E59520943DC26494F8941B' -- 123456
	)
END
ELSE
BEGIN
	print 'UPDATE KRT_Felhasznalok - Elektronikus kiküldés technikai felhasználó'

	UPDATE KRT_Felhasznalok
		SET Jelszo = '7C4A8D09CA3762AF61E59520943DC26494F8941B',
		    Engedelyezett = '1'
	WHERE Id = @felhasznaloId 
END

IF NOT EXISTS (SELECT 1 FROM KRT_Csoportok WHERE Id = @felhasznaloId)
BEGIN
	print 'INSERT KRT_Csoportok - Elektronikus kiküldés technikai felhasználó'

	INSERT INTO KRT_Csoportok
	(
		Id,
		Org,
		Nev,
		Tipus,
		[System]
	)
	VALUES
	(
		@felhasznaloId,
		@org,
		'Elektronikus kiküldés technikai felhasználó',
		'1',
		'1'
	)
END

declare @recordId uniqueidentifier = '4554EBDB-5685-407D-9BDE-E96A0C6888F8'
--FEJLESZTO
declare @szerepkorId uniqueidentifier = 'E855F681-36ED-41B6-8413-576FCB5D1542'

IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @recordId)
BEGIN
	print 'INSERT KRT_Felhasznalo_Szerepkor - Elektronikus kiküldés technikai felhasználó - FEJLESZTO'

	INSERT INTO KRT_Felhasznalo_Szerepkor
	(
		Id,
		Felhasznalo_Id,
		Szerepkor_Id
	)
	VALUES
	(
		@recordId,
		@felhasznaloId,
		@szerepkorId
	)
END 

declare @csoportId uniqueidentifier = 'BD00C8D0-CF99-4DFC-8792-33220B7BFCC6' --Adminisztrátorok (BASIC)
SET @recordId = 'D82B1EDA-BF41-42AA-8354-9304BBCF37A7'

IF NOT EXISTS (SELECT 1 FROM KRT_CsoportTagok WHERE Id = @recordId)
BEGIN
	print 'INSERT KRT_CsoportTagok - Elektronikus kiküldés technikai felhasználó - Adminisztrátorok'

	INSERT INTO KRT_CsoportTagok
	(
		Id,
		Csoport_Id,
		Csoport_Id_Jogalany,
		Tipus
	)
	VALUES
	(
		@recordId,
		@csoportId,
		@felhasznaloId,
		'2'
	)
END 
 

SET @recordId = '859D1315-CE5E-4E67-AC1F-4DA53F0E2B22' 

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE Id = @recordId)
BEGIN 
	Print 'INSERT KRT_Parameterek - ElhiszUserId' 
	 
	insert into KRT_Parameterek
	(
	     Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
	) 
	values 
	(
		@recordId,
		@org
		,'ElhiszUserId'
		,CAST(@felhasznaloId as nvarchar(100))
		,'1'
		,'Elektronikus kiküldés technikai felhasználó id-ja'
	);
 END 

 declare @KodcsoportId uniqueidentifier = '6982F668-C8CD-47A3-AC62-9F16E439473C'
 set @recordId = 'CC0D5F87-279B-45C1-905B-DD721A1FB870'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @recordId)
BEGIN 
	Print 'INSERT KRT_Kodtarak - KULDEMENY_KULDES_MODJA.Elhisz' 

	 insert into KRT_KodTarak
	 (
		 Id,
		 Org,
		 KodCsoport_Id,
		 Kod,
		 Nev,
		 Sorrend,
		 Modosithato
    ) 
	values (
		@recordId,
		@org,
		@kodcsoportId,
		'190',
		'Elhisz',
		'999',
		'1'
    ); 
	 
 END
 --ELSE
 --BEGIN
	--UPDATE KRT_KodTarak
	--	SET Nev = 'Elhisz'
	--WHERE Id = @recordId
 --END 

 -----KULDEMENY_KULDES_MODJA.Elhisz ERVVEGE-----
DECLARE @org_kod_E nvarchar(100) 
SET @org_kod_E = (select kod from KRT_Orgok	where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @recordId_ELHISZ uniqueidentifier 
SET @recordId_ELHISZ= 'CC0D5F87-279B-45C1-905B-DD721A1FB870'
IF (@org_kod_E = 'NMHH')
	UPDATE KRT_KodTarak SET ErvVege = '4700.01.01' WHERE Id = @recordId_ELHISZ
ELSE
	UPDATE KRT_KodTarak SET ErvVege = GETDATE() WHERE Id = @recordId_ELHISZ

------KULDEMENY_KULDES_MODJA.HIVATALI KAPU-----------------
GO
DECLARE @ORG_KOD_HKP nvarchar(100) 
SET @ORG_KOD_HKP = (select kod from KRT_Orgok	where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
DECLARE @ERVVEGE_HKP DATETIME
IF (@ORG_KOD_HKP = 'NMHH')
	SET @ERVVEGE_HKP = GETDATE() 
ELSE
	SET @ERVVEGE_HKP = '4700.01.01'

DECLARE @recordId_HKP uniqueidentifier
SET @recordId_HKP = 'B5A320E1-DBB3-4C9A-B131-8189008F22F1'
IF EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @recordId_HKP)
	BEGIN 
		IF (@ORG_KOD_HKP = 'NMHH')
			UPDATE KRT_KodTarak
			Set ErvVege = GETDATE() 
			where Id = @recordId_HKP
		ELSE
			UPDATE KRT_KodTarak
			Set ErvVege = '4700.01.01'
			where Id = @recordId_HKP
	END
ELSE
	BEGIN
		PRINT 'INSERT'

		IF (@ORG_KOD_HKP = 'NMHH')
			INSERT [dbo].[KRT_KodTarak] ([Id], [Org], [KodCsoport_Id], [ObjTip_Id_AdatElem], [Obj_Id], [Kod], [Nev], [RovidNev], [Egyeb], [Modosithato], [Sorrend], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo]) 
			VALUES (N'b5a320e1-dbb3-4c9a-b131-8189008f22f1', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'6982f668-c8cd-47a3-ac62-9f16e439473c', NULL, NULL, N'19', N'Hivatali kapu', NULL, NULL, N'0', N'99', 1, NULL, NULL, NULL,  GETDATE() , NULL, GETDATE())
		ELSE
			INSERT [dbo].[KRT_KodTarak] ([Id], [Org], [KodCsoport_Id], [ObjTip_Id_AdatElem], [Obj_Id], [Kod], [Nev], [RovidNev], [Egyeb], [Modosithato], [Sorrend], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo]) 
			VALUES (N'b5a320e1-dbb3-4c9a-b131-8189008f22f1', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'6982f668-c8cd-47a3-ac62-9f16e439473c', NULL, NULL, N'19', N'Hivatali kapu', NULL, NULL, N'0', N'99', 1, NULL, NULL, NULL ,CAST(N'4700-01-01 00:01:08.000' AS DateTime) , NULL, GETDATE())
	END
-----------------------------------------------
declare @recordId uniqueidentifier
DECLARE @KodcsoportId uniqueidentifier
DECLARE @Org uniqueidentifier
set @Org='450B510A-7CAA-46B0-83E3-18445C0C53A9'
--Kiküldött dokumentum
set @KodcsoportId = '05B7B2D4-C84A-4F9A-B466-565EF2954C46'
SET @recordId = '28418FB8-64EF-405C-A720-A87B860FDE9D'


IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @recordId)
BEGIN 
    
    Print 'INSERT KRT_Kodtarak - DOKUMENTUM_SZEREP.Kiküldött dokumentum' 

	 insert into KRT_KodTarak
	 (
		 Id,
		 Org,
		 KodCsoport_Id,
		 Kod,
		 Nev,
		 Sorrend,
		 Modosithato
    ) 
	values (
		@recordId,
		@org,
		@kodcsoportId,
		'04',
		'Kiküldött dokumentum',
		'4',
		'1'
    ); 
     
       
  END 

GO


------------------------------------------------------------------------------------------------
PRINT ''
PRINT 'START BLG_326'

DECLARE @MODULID_BLG_326 UNIQUEIDENTIFIER
SET @MODULID_BLG_326 = 'C6CCE24E-1050-4243-8F31-C65E3F468D19'

IF NOT EXISTS (Select 1 FROM INT_Modulok where Id = @MODULID_BLG_326)
BEGIN
	INSERT INTO INT_Modulok
	(Id, Nev)
	VALUES
	(@MODULID_BLG_326,'AdatKapuIntegracio')
END
---------------------------------

DECLARE @RECORD_ID_BLG_326 uniqueidentifier 
SET @RECORD_ID_BLG_326 ='1F5E187B-5339-436F-AB02-024C746E87A3'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @RECORD_ID_BLG_326)
BEGIN
	INSERT INTO INT_Parameterek
	(Id, Modul_id, Nev, Ertek, Karbantarthato)
	VALUES
	(@RECORD_ID_BLG_326, @MODULID_BLG_326, 'ADATKAPU_PARTNER_SYNC_VIEW_SCHEMA', 'neaddfel','1')
END

PRINT 'STOP BLG 326'
PRINT ''
GO
------------------------------------------------------------------------------------------------

/*---------------------------------------------------------------------------------------*/
/*BLG_612*/
PRINT ''
PRINT 'BLG_612 START'

DECLARE @BLG_612_Kcs_Record_id UNIQUEIDENTIFIER 
SET @BLG_612_Kcs_Record_id = 'A3C6D8AD-49CF-44CC-B987-B23484A9EDCC'
DECLARE @BLG_612_Kod_Record_id UNIQUEIDENTIFIER
SET @BLG_612_Kod_Record_id= 'EF93D175-4395-4BE0-A222-E1A49F9458D1'
DECLARE @BLG_612_Org_id UNIQUEIDENTIFIER
SELECT @BLG_612_Org_id = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS(select Id from KRT_KodCsoportok where Id=@BLG_612_Kcs_Record_id)
	BEGIN 
		Print 'ERROR: ELOSZTOIV_FAJTA kódcsoport hianyzik' 
	END 
ELSE 
BEGIN 

	Print 'ELOSZTOIV_FAJTA kódtárérték: Címzettlista' 
	IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE Id = @BLG_612_Kod_Record_id)
	BEGIN 
		
		Print 'INSERT' 
		INSERT INTO KRT_KodTarak(
			KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			@BLG_612_Kcs_Record_id
			,null
			,null
			,@BLG_612_Org_id
			,@BLG_612_Kod_Record_id
			,'10'
			,'Címzettlista'
			,'Címlista'
			,'0'
			,'10'
			,'Címzettlista'
			); 
	
	END 
END
PRINT 'BLG_612 STOP'
GO

/*---------------------------------------------------------------------------------------*/

--------------------------------------------------------
PRINT ''
PRINT 'E-UZENETSZABALY MENU: START'
PRINT ''
BEGIN TRANSACTION AddMenuItemEUzenetSzabaly;  
GO  
		--TIPUSOK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier=	null
		DECLARE @ObjTipus_Id_Tipus uniqueidentifier=	null
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Obj_Id_Szulo uniqueidentifier	=	null
		DECLARE @KodCsoport_Id uniqueidentifier=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id			='FE6C9EC8-6EB5-4F10-96C5-FB7537051326'
		set @Kod		='INT_AutoEUzenetSzabaly'
		set @Nev		='INT_AutoEUzenetSzabaly tábla'
		if not exists(select 1 from KRT_ObjTipusok where id= @id)
		EXECUTE @RC = [dbo].[sp_KRT_ObjTipusokInsert] 
		   @Id
		  ,@ObjTipus_Id_Tipus
		  ,@Kod
		  ,@Nev
		  ,@Obj_Id_Szulo
		  ,@KodCsoport_Id
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--TIPUSOK END
		-----------------------------------------------------------------------
		--FUNKCIÓK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= '8E6E7003-B040-4272-B203-578599CC70C2'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '5B9AC7B3-879C-48A7-BEBA-EF73A49AF893'
		set @ObjTipus_Id_AdatElem	= 'FE6C9EC8-6EB5-4F10-96C5-FB7537051326'
		set @Kod					= 'INT_AutoEUzenetSzabalyKezeles'
		set @Nev					= 'INT_AutoEUzenetSzabaly'
		
		if not exists(select 1 from KRT_Funkciok where id= @id)
		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCIÓK END

		-----------------------------------
		--MODULOK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Org uniqueidentifier=	null
		DECLARE @Tipus nvarchar(10)=	null
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @SelectString nvarchar(4000)=	null
		DECLARE @Csoport_Id_Tulaj uniqueidentifier=	null
		DECLARE @Parameterek nvarchar(4000)=	null
		DECLARE @ObjTip_Id_Adatelem uniqueidentifier=	null
		DECLARE @ObjTip_Adatelem nvarchar(100)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		SET @Id			='8D94BFB8-B9ED-4A9E-B62C-217A84B47825'
		SET @Kod		='TestPageEUzenetSzabaly.aspx'
		SET @Nev		='E-Üzenet szabályok'
		SET @Tipus		='F'
		if not exists(select 1 from KRT_Modulok where id= @id)
		EXECUTE @RC = [dbo].[sp_KRT_ModulokInsert] 
		   @Id
		  ,@Org
		  ,@Tipus
		  ,@Kod
		  ,@Nev
		  ,@Leiras
		  ,@SelectString
		  ,@Csoport_Id_Tulaj
		  ,@Parameterek
		  ,@ObjTip_Id_Adatelem
		  ,@ObjTip_Adatelem
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--MODULOK END
		-----------------------------------
		--MENUK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Org uniqueidentifier=	null
		DECLARE @Menu_Id_Szulo uniqueidentifier=	null
		DECLARE @Kod nvarchar(20)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Funkcio_Id uniqueidentifier=	null
		DECLARE @Modul_Id uniqueidentifier=	null
		DECLARE @Parameter nvarchar(400)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ImageURL nvarchar(400)=	null
		DECLARE @Sorrend nvarchar(100)=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @id				= '98C84D42-5121-4FB8-961E-820A7AD459B7'
		set @Menu_Id_Szulo	= 'AA1421B9-0498-4CA3-B075-60F699CB1B7C'
		set @Kod			= 'eAdmin'
		set @Nev			= 'E-Üzenet szabályok'
		set @Funkcio_Id		= 'CFCBD8A8-CC4B-48DA-9F52-DDDCC3355A06'
		set @Modul_Id		= '8D94BFB8-B9ED-4A9E-B62C-217A84B47825'
		set @Sorrend		= 82
		if not exists(select 1 from krt_menuk where id= @id)
		EXECUTE @RC = [dbo].[sp_KRT_MenukInsert] 
		   @Id
		  ,@Org
		  ,@Menu_Id_Szulo
		  ,@Kod
		  ,@Nev
		  ,@Funkcio_Id
		  ,@Modul_Id
		  ,@Parameter
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ImageURL
		  ,@Sorrend
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--MENUK END


		DECLARE @record_id_1781 uniqueidentifier 
		SET @record_id_1781 = (select Id 
							   from KRT_Szerepkor_Funkcio 
							   where Id='DF7AEE42-ED77-47D4-BE01-D64CAC4CA4A7') 
		Print '@RECORD_ID='+convert(NVARCHAR(100),@record_id_1781) 
		IF @record_id_1781 IS NULL 
			BEGIN 
			     Print 'INSERT' 
				 insert into KRT_Szerepkor_Funkcio(
					 Szerepkor_Id
					,Tranz_id
					,Stat_id
					,Id
					,Funkcio_Id
					) values (
					 'e855f681-36ed-41b6-8413-576fcb5d1542'
					,'f60ad910-541a-470d-b888-ba5a6a061668'
					,'a0848405-e664-4e79-8fab-dfeca4a290af'
					,'DF7AEE42-ED77-47D4-BE01-D64CAC4CA4A7'
					,'3D34F3CF-CADF-4043-A50A-A67EC02872BA'
					); 
			 END 
			 ELSE 
			 BEGIN 
				 Print 'UPDATE'
				 UPDATE KRT_Szerepkor_Funkcio
				 SET 
					 Szerepkor_Id= 'e855f681-36ed-41b6-8413-576fcb5d1542'
					,Tranz_id='f60ad910-541a-470d-b888-ba5a6a061668'
					,Stat_id='a0848405-e664-4e79-8fab-dfeca4a290af'
					,Id='DF7AEE42-ED77-47D4-BE01-D64CAC4CA4A7'
					,Funkcio_Id='3D34F3CF-CADF-4043-A50A-A67EC02872BA'
				 WHERE Id=@record_id_1781 
			 END 

GO  
COMMIT TRANSACTION AddMenuItemEUzenetSzabaly;  
GO 
PRINT ''
PRINT 'E-UZENETSZABALY MENU: STOP'
PRINT ''

--------------------------------------------------------------------------
-- BLG_1054 Kezelési utasítások cimke fordítások (megvalósítva BLG_1053)

declare @id uniqueidentifier

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
if ((@org_kod = 'NMHH') AND (@isTUK = '1')) 
BEGIN
	print 'eRecordComponent/FeladatokTab'
	print 'BoundField_Altipus_Nev'
	set @id = 'EB5ADFF4-391B-E811-80C9-00155D020DD3'
	if not exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'eRecordComponent/FeladatokTab.ascx',
			'BoundField_Altipus_Nev',
			'Intézkedési utasítások'
		)
	END

	print 'eRecordComponent/KezelesiFeljegyzesPanel'
	print 'labelKezelesiFeljegyzes'
	set @id = 'EC5ADFF4-391B-E811-80C9-00155D020DD3'

	if not exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'eRecordComponent/KezelesiFeljegyzesPanel.ascx',
			'labelKezelesiFeljegyzes',
			'Intézkedési utasítások:'
		)
	END
END
GO
--------------------------------------------------------
-- BLG_1940 VONALKOD_PREFIX beállítás
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
-- VONALKOD_PREFIX
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='96472D53-55BC-E611-80BB-00155D020D34') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

print 'VONALKOD_PREFIX beállítás'
if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'96472D53-55BC-E611-80BB-00155D020D34'
		,'VONALKOD_PREFIX'
		,''
		,'0'
		,'A vonalkód ezzel a karaktersorozattal kezdődik'
		); 
 END 

 -- if @org_kod = 'NMHH' 
	-- BEGIN 
		-- Print @org_kod +': NMHH' 
		-- UPDATE KRT_Parameterek
		   -- SET Ertek='NMHH'
		   -- WHERE Id='96472D53-55BC-E611-80BB-00155D020D34'
	-- END
  -- ELSE IF @org_kod = 'FPH' 
	-- BEGIN 
		-- Print @org_kod +': FPH' 
		-- UPDATE KRT_Parameterek
		   -- SET Ertek='FPH'
		   -- WHERE Id='96472D53-55BC-E611-80BB-00155D020D34'
	-- END
   -- ELSE IF @org_kod = 'BOPMH' 
	-- BEGIN 
		-- Print @org_kod +': BOPMH' 
		-- UPDATE KRT_Parameterek
		   -- SET Ertek='BOPMH'
		   -- WHERE Id='96472D53-55BC-E611-80BB-00155D020D34'
	-- END
   -- ELSE IF  @org_kod = 'CSPH' 
	-- BEGIN 
		-- Print @org_kod +': CSPH' 
		-- UPDATE KRT_Parameterek
		   -- SET Ertek='CSPH'
		   -- WHERE Id='96472D53-55BC-E611-80BB-00155D020D34'
	-- END
   -- ELSE IF  @org_kod = 'AXIS' 
	-- BEGIN 
		-- Print @org_kod +': AXIS' 
		-- UPDATE KRT_Parameterek
		   -- SET Ertek='AXIS'
		   -- WHERE Id='96472D53-55BC-E611-80BB-00155D020D34'
	-- END
GO
-------------------------------------------------------------
DECLARE @ID UNIQUEIDENTIFIER
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
		where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

If ((@org_kod = 'NMHH')) 
	BEGIN

SET @ID = '6DA36C6F-A300-4563-A6BC-49A4C212A2B2'
if not exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'eRecordComponent/IraIratFormTab.ascx',
			'LabelEljarasFefuggesztesenekOka',
			'Eljárás felfüggesztésének, szünetelésének oka'
		)
	END
	END
GO

-------------------------------------------------------------
DECLARE @ID UNIQUEIDENTIFIER
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
		where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

If ((@org_kod = 'NMHH')) 
	BEGIN

SET @ID = 'BBB2D4CD-AE50-4A54-BA63-88D4D3A22663'
if not exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'eRecordComponent/IraIratFormTab.ascx',
			'LabelIratHatasaUgyintezesre',
			'Irat hatása az ügyintézésre (sakkóra):'
		)
	END
	END
GO

-------------------------------------------------------------
-------------------------------------------------------------
 -- BLG_1974 VONALKOD_KIMENO_GENERALT beállítás
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
-- VONALKOD_KIMENO_GENERALT
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='E1213508-AB0A-4BE3-A2E5-A0A57D1E0BE3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

print 'VONALKOD_KIMENO_GENERALT beállítás'
if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'E1213508-AB0A-4BE3-A2E5-A0A57D1E0BE3'
		,'VONALKOD_KIMENO_GENERALT'
		,'0'
		,'1'
		,'A vonalkód generálása belső keletkezésű irat iktatása esetén'
		); 
 END 
 
-- if @org_kod = 'NMHH' 
--BEGIN 
--	Print @org_kod +': NMHH' 
--	UPDATE KRT_Parameterek
--	   SET Ertek='1'
--	   WHERE Id='E1213508-AB0A-4BE3-A2E5-A0A57D1E0BE3'
--END
GO

--BLG 1880 AUTO_RAGSZAMOSZTAS_TOMEGESEN
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
-- AUTO_RAGSZAMOSZTAS_TOMEGESEN
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='D52FA2B8-8E0F-4CAC-AAC0-C286306ED18F') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

print 'AUTO_RAGSZAMOSZTAS_TOMEGESEN beállítás'
if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'D52FA2B8-8E0F-4CAC-AAC0-C286306ED18F'
		,'AUTO_RAGSZAMOSZTAS_TOMEGESEN'
		,'1'
		,'1'
		,'Automatikus ragszámosztás tömegesen'
		); 
 END 
 
-- if @org_kod = 'NMHH' 
--BEGIN 
--	Print @org_kod +': NMHH' 
--	UPDATE KRT_Parameterek
--	   SET Ertek='1'
--	   WHERE Id='E1213508-AB0A-4BE3-A2E5-A0A57D1E0BE3'
--END
GO

--------------------------------------------------------
-- BUG_1338
-- Iratpéldányok keresése menüpont beszúrása

--DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Menuk
--			where Id='BA8E977F-1F50-4ECE-A537-1E55B8C29148') 
--Print '@record_id='+convert(NVARCHAR(100),@record_id) 
--if @record_id IS NULL 
--	BEGIN 
		
--Print 'INSERT' 
--		 insert into KRT_Menuk(
--			 Menu_Id_Szulo
--			,Tranz_id
--			,Stat_id
--			,Id
--			,Kod
--			,Nev
--			,Funkcio_Id
--			,Modul_Id
--			,Parameter
--			,Sorrend
--			) values (
--			 'D4C7F94F-C5AF-4897-8D06-3DF59B6DF7EB'
--			,null
--			,null
--			,'BA8E977F-1F50-4ECE-A537-1E55B8C29148'
--			,'eRecord'
--			,'Iratpéldányok keresése'
--			,'05228912-CC61-4F2B-8E1C-C66448BF4C99'
--			,'60A12BE6-5A1B-410B-920E-A4A81F6412F8'		
--			,'Startup=SearchForm'
--			,'281'
--			); 
--	 END 
--	 ELSE 
--	 BEGIN 
--		 Print 'UPDATE'
--		 UPDATE KRT_Menuk
--		 SET 
--			 Menu_Id_Szulo='D4C7F94F-C5AF-4897-8D06-3DF59B6DF7EB'
--			,Tranz_id=null
--			,Stat_id=null
--			,Id='BA8E977F-1F50-4ECE-A537-1E55B8C29148'
--			,Kod='eRecord'
--			,Nev='Iratpéldányok keresése'
--			,Funkcio_Id='05228912-CC61-4F2B-8E1C-C66448BF4C99'
--			,Modul_Id='60A12BE6-5A1B-410B-920E-A4A81F6412F8'
--			,Parameter='Startup=SearchForm'
--			,Sorrend='281'
--		 WHERE Id=@record_id 
--	 END
--	 go

--BUG_1338 end
--------------------------------------------------------

-- BUG_1417
-- eMigration menüpont érvényesítése

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Menuk
			where Id='1FA15858-98A0-4A40-8A7E-9864E5107C49') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Menuk(
			 Menu_Id_Szulo
			,Tranz_id
			,Stat_id
			,Id
			,Kod
			,Nev
			,Funkcio_Id
			,Modul_Id
			,Sorrend
			) values (
			 '7EE2290F-DF48-4589-BEBC-462DAD8549E1'
			,null
			,null
			,'1FA15858-98A0-4A40-8A7E-9864E5107C49'
			,'eRecord'
			,'eMigration főmenü'
			,'7320D48D-66C7-4103-AE5B-2EC771FDC9BE'
			,'CF39E180-0045-44BA-8C08-6AAE2A715191'					
			,'31'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE/érvénesítés'		
		 UPDATE KRT_Menuk
		 SET 
			 Menu_Id_Szulo='7EE2290F-DF48-4589-BEBC-462DAD8549E1'
			,Tranz_id=null
			,Stat_id=null
			,Id='1FA15858-98A0-4A40-8A7E-9864E5107C49'
			,Kod='eRecord'
			,Nev='eMigration főmenü'
			,Funkcio_Id='7320D48D-66C7-4103-AE5B-2EC771FDC9BE'
			,Modul_Id='CF39E180-0045-44BA-8C08-6AAE2A715191'	
			,Parameter=null
			,Sorrend='31'
			,ErvVege = '4700-12-31 00:00:00.000'		
		 WHERE Id=@record_id 
	 END
	 go

-- BUG_1417 end
--------------------------------------------------------


------- BLG_354
------- BLG_2020 kapcsán kiderült a hiánya

-- HATOSAGI_ADATOK_BEJOVOHOZ rendszerparameter	 
-- FPH = Igen, BOPMH = NMHH = Nem

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='AAF9CFA4-3EA7-E711-80C6-00155D020BD9') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'AAF9CFA4-3EA7-E711-80C6-00155D020BD9'
		,'HATOSAGI_ADATOK_BEJOVOHOZ'
		,'Nem'
		,'1'
		,'Igen = Bejövő iratnál lehet hatósági adatot kitölteni, Nem = Csak kimenő iratnál lehet'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='AAF9CFA4-3EA7-E711-80C6-00155D020BD9'
	--	,Nev='HATOSAGI_ADATOK_BEJOVOHOZ'
	--	,Ertek='Nem'
	--	,Karbantarthato='1'
	--	,Note='Igen = Bejövő iratnál lehet hatósági adatot kitölteni, Nem = Csak kimenő iratnál lehet'
	-- WHERE Id=@record_id 
 --END

-- if @org_kod = 'FPH' 
--BEGIN 
--	Print @org_kod +': HATOSAGI_ADATOK_BEJOVOHOZ = Igen'
--	UPDATE KRT_Parameterek
--	   SET Ertek='Igen'
--	   WHERE Id='AAF9CFA4-3EA7-E711-80C6-00155D020BD9'
--END
GO
--------- BLG_354 


--------------------------------------------
-- BLG#224: Bejövő tömeges iktatás támogatása

Print 'Tömeges iktatás csomópont menü'
	 
	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Menuk
			where Id='7723F976-C4E8-46E1-AED6-4EB0E8BE382B') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 

	-- Át kell nevezni előtte a régi ugyanolyan nevű menüt, különben elszáll egy UK constrainten...
	UPDATE KRT_Menuk
	SET Menu_Id_Szulo = '7723F976-C4E8-46E1-AED6-4EB0E8BE382B'
		, Nev = 'Tömeges iktatás (Kimenő)'
	WHERE Id = '3E676FDD-00DF-E611-829A-4C3488C40116'

		
Print 'INSERT' 
		 insert into KRT_Menuk(
			 Menu_Id_Szulo
			,Tranz_id
			,Stat_id
			,Id
			,Kod
			,Nev
			,Modul_Id
			,Sorrend			
			) values (
			 '7EE2290F-DF48-4589-BEBC-462DAD8549E1'
			,'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'7723F976-C4E8-46E1-AED6-4EB0E8BE382B'
			,'eRecord'
			,'Tömeges iktatás'
			,null
			,'31'
			); 		 
			 
	 END 
	 go
	 

print 'TomegesIktatasBejovo.aspx modul'

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Modulok
			where Id='B46076FE-BD1F-4F6D-A7F9-A5BBE19B4C6F') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Modulok(
			 Tranz_id
			,Stat_id
			,Id
			,Tipus
			,Kod
			,Nev
			,Leiras
			) values (
			 'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'B46076FE-BD1F-4F6D-A7F9-A5BBE19B4C6F'
			,'F'
			,'TomegesIktatasBejovo.aspx'
			,'Tömeges iktatás (Bejövő)'
			,null
			); 
	 END 	
	 go
	 

	 
Print 'Tömeges iktatás (Bejövő) menü'
	 
	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Menuk
			where Id='CEEAA804-5DC7-44D8-AEAB-FD1A6E047BBE') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Menuk(
			 Menu_Id_Szulo
			,Tranz_id
			,Stat_id
			,Id
			,Kod
			,Nev
			,Modul_Id
			,Sorrend		
			,Funkcio_Id	
			) values (
			 '7723F976-C4E8-46E1-AED6-4EB0E8BE382B'
			,'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'CEEAA804-5DC7-44D8-AEAB-FD1A6E047BBE'
			,'eRecord'
			,'Tömeges iktatás (Bejövő)'
			,'B46076FE-BD1F-4F6D-A7F9-A5BBE19B4C6F'
			,'1'
			, '37BA9C69-FFDE-E611-829A-4C3488C40116' -- Tömeges iktatás funkciójog
			); 
		 
			 
	 END 

-- Meglévő Tömeges iktatás (ami a kimenő) Update-elése:
-- Áthelyezni a csomópont menü alá, és módosítani a nevét:

UPDATE KRT_Menuk
SET Menu_Id_Szulo = '7723F976-C4E8-46E1-AED6-4EB0E8BE382B'
, Nev = 'Tömeges iktatás (Kimenő)'
WHERE Id = '3E676FDD-00DF-E611-829A-4C3488C40116'

GO
-- BLG#224 END
-------------------------------

----------------------------------
--BLG 2014
BEGIN TRANSACTION AddMenuItem;  
GO  
		--TIPUSOK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier=	null
		DECLARE @ObjTipus_Id_Tipus uniqueidentifier=	null
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Obj_Id_Szulo uniqueidentifier	=	null
		DECLARE @KodCsoport_Id uniqueidentifier=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id			='100D78CD-0835-4E64-9405-F99CF8D6D4F1'
		set @Kod		='INT_AutoEUzenetSzabaly'
		set @Nev		='INT_AutoEUzenetSzabaly tábla'
		IF NOT EXISTS(select 1 from KRT_ObjTipusok where Id = @Id)
		BEGIN
			EXECUTE @RC = [dbo].[sp_KRT_ObjTipusokInsert] 
		   @Id
		  ,@ObjTipus_Id_Tipus
		  ,@Kod
		  ,@Nev
		  ,@Obj_Id_Szulo
		  ,@KodCsoport_Id
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		END
		GO
		--TIPUSOK END
		-----------------------------------------------------------------------
		--FUNKCIÓK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= 'C741CE36-DC29-4FAA-933C-848648B5F047'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '5B9AC7B3-879C-48A7-BEBA-EF73A49AF893'
		set @ObjTipus_Id_AdatElem	= '100D78CD-0835-4E64-9405-F99CF8D6D4F1'
		set @Kod					= 'INT_AutoEUzenetSzabalyKezeles'
		set @Nev					= 'AutoEUzenetSzabaly kezelése'

		IF NOT EXISTS(select 1 from KRT_Funkciok where Id = @Id)
		BEGIN
			EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		END
		GO
		--FUNKCIÓK END

		--FUNKCIÓK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= '6F566B5D-2E64-423E-9DFE-36D74CA0A02D'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '5B9AC7B3-879C-48A7-BEBA-EF73A49AF893'
		set @ObjTipus_Id_AdatElem	= '100D78CD-0835-4E64-9405-F99CF8D6D4F1'
		set @Kod					= 'INT_AutoEUzenetSzabalyModify'
		set @Nev					= 'AutoEUzenetSzabaly módosítása'

		IF NOT EXISTS(select 1 from KRT_Funkciok where Id = @Id)
		BEGIN
			EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		END
		GO
		--FUNKCIÓK END

		--FUNKCIÓK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= '58A92B91-6F08-4D16-8CE7-4CE377E1CA75'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '939A0831-2914-4348-93FA-9DED0225747E'
		set @ObjTipus_Id_AdatElem	= '100D78CD-0835-4E64-9405-F99CF8D6D4F1'
		set @Kod					= 'INT_AutoEUzenetSzabalyNew'
		set @Nev					= 'AutoEUzenetSzabaly létrehozása'

		IF NOT EXISTS(SELECT 1 FROM KRT_Funkciok WHERE ID =@Id )
		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCIÓK END

		--FUNKCIÓK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= '3CA19738-E0F1-4724-8E70-A9EBC90A6961'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '9508908E-AEFA-41C6-B6D7-807E3AE3A3AB'
		set @ObjTipus_Id_AdatElem	= '100D78CD-0835-4E64-9405-F99CF8D6D4F1'
		set @Kod					= 'INT_AutoEUzenetSzabalyList'
		set @Nev					= 'AutoEUzenetSzabaly lista'
		
		IF NOT EXISTS(SELECT 1 FROM KRT_Funkciok WHERE ID =@Id )
		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCIÓK END
	
		--FUNKCIÓK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= 'DDD2D704-FC8D-4F2B-9BB4-D4BCAA323581'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '78F0D5A2-E048-4A1F-9F11-5D57AB78B985'
		set @ObjTipus_Id_AdatElem	= '100D78CD-0835-4E64-9405-F99CF8D6D4F1'
		set @Kod					= 'INT_AutoEUzenetSzabalyView'
		set @Nev					= 'AutoEUzenetSzabaly megtekintése'
		
		IF NOT EXISTS(SELECT 1 FROM KRT_Funkciok WHERE ID =@Id )
		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCIÓK END

		--FUNKCIÓK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= '1F104734-17E8-4698-9B07-4BC7637542F1'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '805C7634-F205-4AE7-8580-DB3C73D8F43B'
		set @ObjTipus_Id_AdatElem	= '100D78CD-0835-4E64-9405-F99CF8D6D4F1'
		set @Kod					= 'INT_AutoEUzenetSzabalyInvalidate'
		set @Nev					= 'AutoEUzenetSzabaly érvénytelenítése'
		
		IF NOT EXISTS(SELECT 1 FROM KRT_Funkciok WHERE ID =@Id )
		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCIÓK END
		
		--FUNKCIÓK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= '67654187-78BB-4EEE-A631-2D4B5D23E213'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= 'FBDDC6B8-6671-4BA7-9BDD-885FFC031986'
		set @ObjTipus_Id_AdatElem	= '100D78CD-0835-4E64-9405-F99CF8D6D4F1'
		set @Kod					= 'INT_AutoEUzenetSzabalyLock'
		set @Nev					= 'AutoEUzenetSzabaly zárolása'
		
		IF NOT EXISTS(SELECT 1 FROM KRT_Funkciok WHERE ID =@Id )
		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCIÓK END

		-----------------------------------
		--MODULOK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Org uniqueidentifier=	null
		DECLARE @Tipus nvarchar(10)=	null
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @SelectString nvarchar(4000)=	null
		DECLARE @Csoport_Id_Tulaj uniqueidentifier=	null
		DECLARE @Parameterek nvarchar(4000)=	null
		DECLARE @ObjTip_Id_Adatelem uniqueidentifier=	null
		DECLARE @ObjTip_Adatelem nvarchar(100)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		SET @Id			='AD774A6D-C702-4E96-8B44-9B98B9FFA4AE'
		SET @Kod		='EUzenetSzabalyList.aspx'
		SET @Nev		='EUzenetSzabaly kezelése'
		SET @Tipus		='F'

		IF NOT EXISTS(SELECT 1 FROM KRT_Modulok WHERE ID =@Id )
		EXECUTE @RC = [dbo].[sp_KRT_ModulokInsert] 
		   @Id
		  ,@Org
		  ,@Tipus
		  ,@Kod
		  ,@Nev
		  ,@Leiras
		  ,@SelectString
		  ,@Csoport_Id_Tulaj
		  ,@Parameterek
		  ,@ObjTip_Id_Adatelem
		  ,@ObjTip_Adatelem
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--MODULOK END
		-----------------------------------
		--MENUK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Org uniqueidentifier=	null
		DECLARE @Menu_Id_Szulo uniqueidentifier=	null
		DECLARE @Kod nvarchar(20)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Funkcio_Id uniqueidentifier=	null
		DECLARE @Modul_Id uniqueidentifier=	null
		DECLARE @Parameter nvarchar(400)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ImageURL nvarchar(400)=	null
		DECLARE @Sorrend nvarchar(100)=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @id				= '6EB9760D-8BE5-4872-9507-6DC89B2C3E14'
		set @Menu_Id_Szulo	= 'AA1421B9-0498-4CA3-B075-60F699CB1B7C'
		set @Kod			= 'eAdmin'
		set @Nev			= 'EUzenetSzabalyok kezelése'
		set @Funkcio_Id		= '1F104734-17E8-4698-9B07-4BC7637542F1'
		set @Modul_Id		= 'AD774A6D-C702-4E96-8B44-9B98B9FFA4AE'
		set @Sorrend		= 84

		IF NOT EXISTS(SELECT 1 FROM KRT_Menuk WHERE ID =@Id )
		EXECUTE @RC = [dbo].[sp_KRT_MenukInsert] 
		   @Id
		  ,@Org
		  ,@Menu_Id_Szulo
		  ,@Kod
		  ,@Nev
		  ,@Funkcio_Id
		  ,@Modul_Id
		  ,@Parameter
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ImageURL
		  ,@Sorrend
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--MENUK END
		-------------------------------------------------
		Print 'KRT_Szerepkor_Funkcio - hozzarendeles FEJLESZTO-hoz -euzen'
		DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
					where Id='4FCA6D9A-DB4B-4792-A06E-BCC1C5865B56') 
		Print '@record_id='+convert(NVARCHAR(100),@record_id) 
		if @record_id IS NULL 
		BEGIN 	
			Print 'INSERT' 
			 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,'4FCA6D9A-DB4B-4792-A06E-BCC1C5865B56'
				,'C741CE36-DC29-4FAA-933C-848648B5F047'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Szerepkor_Funkcio
			 SET 
				 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
				,Id='4FCA6D9A-DB4B-4792-A06E-BCC1C5865B56'
				,Funkcio_Id='C741CE36-DC29-4FAA-933C-848648B5F047'
			 WHERE Id=@record_id
		 END
        go
		--------------------------------
		Print 'KRT_Szerepkor_Funkcio - hozzarendeles FEJLESZTO-hoz -euzen'
		DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
					where Id='14EF6B37-34E4-41CC-B3CE-CF09EA416836') 
		Print '@record_id='+convert(NVARCHAR(100),@record_id) 
		if @record_id IS NULL 
		BEGIN 	
			Print 'INSERT' 
			 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,'14EF6B37-34E4-41CC-B3CE-CF09EA416836'
				,'6F566B5D-2E64-423E-9DFE-36D74CA0A02D'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Szerepkor_Funkcio
			 SET 
				 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
				,Id='14EF6B37-34E4-41CC-B3CE-CF09EA416836'
				,Funkcio_Id='6F566B5D-2E64-423E-9DFE-36D74CA0A02D'
			 WHERE Id=@record_id
		 END
		 go
		--------------------------------
		Print 'KRT_Szerepkor_Funkcio - hozzarendeles FEJLESZTO-hoz -euzen'
		DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
					where Id='7064A1F9-A505-449C-BD7C-8C318B70830E') 
		Print '@record_id='+convert(NVARCHAR(100),@record_id) 
		if @record_id IS NULL 
		BEGIN 	
			Print 'INSERT' 
			 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,'7064A1F9-A505-449C-BD7C-8C318B70830E'
				,'58A92B91-6F08-4D16-8CE7-4CE377E1CA75'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Szerepkor_Funkcio
			 SET 
				 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
				,Id='7064A1F9-A505-449C-BD7C-8C318B70830E'
				,Funkcio_Id='58A92B91-6F08-4D16-8CE7-4CE377E1CA75'
			 WHERE Id=@record_id
		 END
		 go
		--------------------------------
		Print 'KRT_Szerepkor_Funkcio - hozzarendeles FEJLESZTO-hoz -euzen'
		DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
					where Id='01390993-73DD-4421-AF88-159D3A9A9DA7') 
		Print '@record_id='+convert(NVARCHAR(100),@record_id) 
		if @record_id IS NULL 
		BEGIN 	
			Print 'INSERT' 
			 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,'01390993-73DD-4421-AF88-159D3A9A9DA7'
				,'3CA19738-E0F1-4724-8E70-A9EBC90A6961'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Szerepkor_Funkcio
			 SET 
				 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
				,Id='01390993-73DD-4421-AF88-159D3A9A9DA7'
				,Funkcio_Id='3CA19738-E0F1-4724-8E70-A9EBC90A6961'
			 WHERE Id=@record_id
		 END
		 go
		--------------------------------
		Print 'KRT_Szerepkor_Funkcio - hozzarendeles FEJLESZTO-hoz -euzen'
		DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
					where Id='8D205454-11C3-4274-9C1C-C0D1ECED2A25') 
		Print '@record_id='+convert(NVARCHAR(100),@record_id) 
		if @record_id IS NULL 
		BEGIN 	
			Print 'INSERT' 
			 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,'8D205454-11C3-4274-9C1C-C0D1ECED2A25'
				,'DDD2D704-FC8D-4F2B-9BB4-D4BCAA323581'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Szerepkor_Funkcio
			 SET 
				 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
				,Id='8D205454-11C3-4274-9C1C-C0D1ECED2A25'
				,Funkcio_Id='DDD2D704-FC8D-4F2B-9BB4-D4BCAA323581'
			 WHERE Id=@record_id
		 END
		 go
		--------------------------------
		Print 'KRT_Szerepkor_Funkcio - hozzarendeles FEJLESZTO-hoz -euzen'
		DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
					where Id='0AA5222B-021E-4FBC-B3B7-133C600239F3') 
		Print '@record_id='+convert(NVARCHAR(100),@record_id) 
		if @record_id IS NULL 
		BEGIN 	
			Print 'INSERT' 
			 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,'0AA5222B-021E-4FBC-B3B7-133C600239F3'
				,'1F104734-17E8-4698-9B07-4BC7637542F1'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Szerepkor_Funkcio
			 SET 
				 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
				,Id='0AA5222B-021E-4FBC-B3B7-133C600239F3'
				,Funkcio_Id='1F104734-17E8-4698-9B07-4BC7637542F1'
			 WHERE Id=@record_id
		 END
		 go
		--------------------------------
		Print 'KRT_Szerepkor_Funkcio - hozzarendeles FEJLESZTO-hoz -euzen'
		DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
					where Id='6BF1C2B4-042E-45BD-8C6F-04F8C6327813') 
		Print '@record_id='+convert(NVARCHAR(100),@record_id) 
		if @record_id IS NULL 
		BEGIN 	
			Print 'INSERT' 
			 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,'6BF1C2B4-042E-45BD-8C6F-04F8C6327813'
				,'67654187-78BB-4EEE-A631-2D4B5D23E213'
				);
		 END 
		 ELSE 
		 BEGIN 
			 Print 'UPDATE'
			 UPDATE KRT_Szerepkor_Funkcio
			 SET 
				 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
				,Id='6BF1C2B4-042E-45BD-8C6F-04F8C6327813'
				,Funkcio_Id='67654187-78BB-4EEE-A631-2D4B5D23E213'
			 WHERE Id=@record_id
		 END
		 go
		--------------------------------

-- Kimenő tömeges iktatás menü

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

--IF @org_kod = 'NMHH'
--BEGIN
			
	DECLARE @record_id uniqueidentifier 

	SET @record_id = 'F58EF17D-66AE-407D-9A24-1CC1C2DFCE51'

	IF NOT EXISTS (Select 1 from KRT_Modulok where Id = @record_id)
	BEGIN
	    print 'Add KRT_Modulok.TomegesIktatasKimeno'
		INSERT INTO KRT_Modulok
		(
			Id,
			Org,
			Kod,
			Nev,
			Tipus
		)
		VALUES
		(
			@record_id,
			'450B510A-7CAA-46B0-83E3-18445C0C53A9',
			'TomegesIktatasKimeno.aspx',
			'Tömeges iktatás (Kimenő)',
			'F'
		)

		UPDATE KRT_Menuk
		set Modul_Id = @record_id
		where Id = '3E676FDD-00DF-E611-829A-4C3488C40116'
	END

--END

GO




GO  
COMMIT TRANSACTION AddMenuItem;  
GO
--BLG 2014
----------------------------------

-- BLG_2096: PARTNER_FORRAS kódcsoportba új kódtár érték: Excel

Print 'PARTNER_FORRAS kódcsoport'


 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_KodCsoportok
			where Id='552F33EC-0BAC-400E-86F2-8AB624959DA4') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'ERROR: Nincs PARTNER_FORRAS kódcsoport vagy nem jó az ID-ja' 

END 
ELSE 
BEGIN 
    Print 'Excel kódtárérték' 
	SET @record_id= null
    SET @record_id = (select Id from KRT_KodTarak
		where Id='7493BCC5-BB18-E811-80C9-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
		
		Print 'INSERT' 
			insert into KRT_KodTarak(
				KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			) values (
			'552F33EC-0BAC-400E-86F2-8AB624959DA4'
			,null
			,null
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'7493BCC5-BB18-E811-80C9-00155D020DD3'
			,'X'
			,'Excel'
			,''
			,'0'
			,'16'
			); 
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
		END 
		ELSE 
		BEGIN 
			Print 'UPDATE'
			UPDATE KRT_KodTarak
			SET 
				KodCsoport_Id='552F33EC-0BAC-400E-86F2-8AB624959DA4'
			,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Kod='X'
			,Nev='Excel'
			,RovidNev=''
			,Modosithato='0'
			,Sorrend='16'
			WHERE Id='7493BCC5-BB18-E811-80C9-00155D020DD3'
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
	END
END

GO
--- BLG_2096 end


-- BLG_609
-- Belső keletkezésű (kimenő és belső) irat iktatása menüpont átnevezése


DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
if ((@org_kod = 'NMHH') AND (@isTUK = '1')) 
BEGIN
    -- BUG_5271
	--if @org_kod = 'NMHH' 
	--BEGIN 
	--	Print @org_kod +': NMHH-ban VONALKOD_KIMENO_GENERALT = 1 beállítása' 
	--	UPDATE KRT_Parameterek
	--	   SET Ertek='1'
	--	   WHERE Id='E1213508-AB0A-4BE3-A2E5-A0A57D1E0BE3'
	--END
	DECLARE @id uniqueidentifier SET @id = (select Id from KRT_Menuk
				where Id='C8456E19-2193-4D52-A249-194D9BABBF49') 
	Print '@record_id='+convert(NVARCHAR(100),@id) 
	if @id IS NULL 
	BEGIN 
		
		Print 'ERROR: Nincs meg a menüpont (Belső keletkezésű (kimenő és belső) irat iktatása) vagy rossz az id' 
		 
	END 
	ELSE 
	BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Menuk
		 SET 
			 Nev='Saját keletkezésű (kimenő és belső irat) iktatása'
		 WHERE Id=@id 
	 END
END

GO

-- selejtezés
declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege)

IF @isTUK = '1'
BEGIN
	declare @KodcsoportId uniqueidentifier = '70AFC286-4308-49ED-AC01-40DDF12C30F4'
	declare @recordId uniqueidentifier = 'D687143C-575D-44A0-B805-1927F62F3FF2'

	--Jegyzékre helyezett
	IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @recordId)
	BEGIN 
		 Print 'INSERT KRT_Kodtarak - IRAT_ALLAPOT.Jegyzékre helyezett' 

		 insert into KRT_KodTarak
		 (
			 Id,
			 Org,
			 KodCsoport_Id,
			 Kod,
			 Nev,
			 Sorrend,
			 Modosithato
		) 
		values (
			@recordId,
			@org,
			@kodcsoportId,
			'31',
			'Jegyzékre helyezett',
			'71',
			'0'
			); 
	 
	 END

	set @recordId = '7D4F9968-B61E-4008-9BDD-8D5062334BAF'
	--Lezárt jegyzékben lévő
	IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @recordId)
	BEGIN 
		 Print 'INSERT KRT_Kodtarak - IRAT_ALLAPOT.Lezárt jegyzékben lévő' 

		 insert into KRT_KodTarak
		 (
			 Id,
			 Org,
			 KodCsoport_Id,
			 Kod,
			 Nev,
			 Sorrend,
			 Modosithato
		) 
		values (
			@recordId,
			@org,
			@kodcsoportId,
			'32',
			'Lezárt jegyzékben lévő',
			'72',
			'0'
		); 
	 
	 END

	set @recordId = '5071223C-3BD5-4821-A4B7-DD19A05F9760F'
	--Selejtezett
	IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @recordId)
	BEGIN 
		 Print 'INSERT KRT_Kodtarak - IRAT_ALLAPOT.Selejtezett' 

		 insert into KRT_KodTarak
		 (
			 Id,
			 Org,
			 KodCsoport_Id,
			 Kod,
			 Nev,
			 Sorrend,
			 Modosithato
		) 
		values (
			@recordId,
			@org,
			@kodcsoportId,
			'33',
			'Selejtezett',
			'73',
			'0'
		); 
	 
	 END

	set @recordId = '6C7C968B-2460-435F-9088-059426654021'
	--Levéltárba adott
	IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @recordId)
	BEGIN 
		 Print 'INSERT KRT_Kodtarak - IRAT_ALLAPOT.Levéltárba adott' 

		 insert into KRT_KodTarak
		 (
			 Id,
			 Org,
			 KodCsoport_Id,
			 Kod,
			 Nev,
			 Sorrend,
			 Modosithato
		) 
		values (
			@recordId,
			@org,
			@kodcsoportId,
			'34',
			'Levéltárba adott',
			'74',
			'0'
		); 
	 
	 END

	set @recordId = 'A97F48AB-53A8-4F9A-A9E3-0418E1BB7CCD'
	--Egyéb szervezetnek átadott
	IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @recordId)
	BEGIN 
		 Print 'INSERT KRT_Kodtarak - IRAT_ALLAPOT.Egyéb szervezetnek átadott' 

		 insert into KRT_KodTarak
		 (
			 Id,
			 Org,
			 KodCsoport_Id,
			 Kod,
			 Nev,
			 Sorrend,
			 Modosithato
		) 
		values (
			@recordId,
			@org,
			@kodcsoportId,
			'35',
			'Egyéb szervezetnek átadott',
			'75',
			'0'
		); 
	 
	 END

	 set @kodcsoportId = '664B8A53-9B2C-4B1B-B279-9765DE1C31A9'
	 set @recordId = '7ACFA089-02C2-4BEC-AAE3-C1A7B091E76EF'
	--Selejtezett
	IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @recordId)
	BEGIN 
		 Print 'INSERT KRT_Kodtarak - IRATPELDANY_ALLAPOT.Selejtezett' 

		 insert into KRT_KodTarak
		 (
			 Id,
			 Org,
			 KodCsoport_Id,
			 Kod,
			 Nev,
			 Sorrend,
			 Modosithato
		) 
		values (
			@recordId,
			@org,
			@kodcsoportId,
			'33',
			'Selejtezett',
			'33',
			'0'
		); 
	 
	 END

	set @recordId = 'A66258AD-0F54-46B5-BB87-64FD7EBB7ADC'
	--Levéltárba adott
	IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @recordId)
	BEGIN 
		 Print 'INSERT KRT_Kodtarak - IRATPELDANY_ALLAPOT.Levéltárba adott' 

		 insert into KRT_KodTarak
		 (
			 Id,
			 Org,
			 KodCsoport_Id,
			 Kod,
			 Nev,
			 Sorrend,
			 Modosithato
		) 
		values (
			@recordId,
			@org,
			@kodcsoportId,
			'34',
			'Levéltárba adott',
			'34',
			'0'
		); 
	 
	 END

	set @recordId = 'D32A001C-7750-4706-9751-C48B1073CD63'
	--Egyéb szervezetnek átadott
	IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @recordId)
	BEGIN 
		 Print 'INSERT KRT_Kodtarak - IRATPELDANY_ALLAPOT.Egyéb szervezetnek átadott' 

		 insert into KRT_KodTarak
		 (
			 Id,
			 Org,
			 KodCsoport_Id,
			 Kod,
			 Nev,
			 Sorrend,
			 Modosithato
		) 
		values (
			@recordId,
			@org,
			@kodcsoportId,
			'35',
			'Egyéb szervezetnek átadott',
			'35',
			'0'
		); 
	 
	 END
END

GO

--megsemmisítési napló
declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' 
and getdate() between ervkezd and ervvege)

--BLG 2954 - JegyzekMegsemmisites ne csak TÜK alatt
declare @funkcioIdJ uniqueidentifier
set @funkcioIdJ = 'B2EBB120-2255-49F2-ADC1-D552E7DA129B'

--JegyzekMegsemmisites
IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE Id = @funkcioIdJ)
BEGIN 
	Print 'INSERT KRT_Funkciok - JegyzekMegsemmisites' 

	insert into KRT_Funkciok
	(
		Id,
		Kod,
		Nev,
		ObjTipus_Id_AdatElem,
		Alkalmazas_Id
) 
values (
	@funkcioIdJ,
	'JegyzekMegsemmisites',
	'Jegyzék megsemmisítés',
	'FD52245E-A1F8-4EF4-AE05-259A33AA5BA3',
	'20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
); 
	 
END

declare @szerepkorFunkcioId uniqueidentifier
set @szerepkorFunkcioId = '6145F4F0-C17C-4C16-AC7A-17C59C2BBBA0'

--JegyzekMegsemmisites
IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE Id = @szerepkorFunkcioId)
BEGIN 
	Print 'INSERT KRT_Szerepkor_Funkcio - FEJLESZTO-JegyzekMegsemmisites' 

	insert into KRT_Szerepkor_Funkcio
	(
		Id,
		Funkcio_Id,
		Szerepkor_Id
) 
values (
	@szerepkorFunkcioId,
	@funkcioIdJ,
	'E855F681-36ED-41B6-8413-576FCB5D1542'
); 
	 
END

	 

IF @isTUK = '1'
BEGIN
	declare @ikatokonyvId uniqueidentifier
	set @ikatokonyvId = 'A79B2734-E83C-48CA-9E7F-3C1C6FEAE731'

	IF NOT EXISTS (SELECT 1 from EREC_IraIktatoKonyvek WHERE Id = @ikatokonyvId)
	BEGIN
	   print 'Add EREC_IraIktatoKonyvek - Megsemmisítési jegyzőkönyv'

	   declare @azonosito nvarchar(20)
	   set @azonosito = (select top 1 Azonosito from EREC_IraIktatoKonyvek order by Azonosito desc)
       set @azonosito = right('00000' + cast(cast(@azonosito as int) + 1 as nvarchar), 5)

	   INSERT INTO EREC_IraIktatoKonyvek
	   (
			Id,
			Org,
			Ev,
			Azonosito,
			Nev,
			MegkulJelzes,
			Iktatohely,
			KozpontiIktatasJelzo,
			UtolsoFoszam,
			IktSzamOsztas,
			Titkos,
			IktatoErkezteto,
			Statusz,
			KezelesTipusa
	   )
	   VALUES
	   (
			@ikatokonyvId,
			@org,
			 DATEPART(year, GETDATE()),
			@azonosito,
			'Megsemmisítési jegyzőkönyv',
			'NMHH',
			'M',
			'0',
			 0,
			'1',
			'N',
			'S',
			'1',
			'E'

	   )
	END

	declare @jogosultId uniqueidentifier
	set @jogosultId = 'A2415EE7-FE9A-4D38-9E95-A8A4D4426C66'

	IF NOT EXISTS (SELECT 1 from KRT_jogosultak WHERE Id = @jogosultId)
	BEGIN
	   print 'Add KRT_jogosultak - Adminisztrátor'

	   INSERT INTO KRT_jogosultak
	   (
			Id,
			Csoport_Id_Jogalany,
			Jogszint,
			Obj_Id,
			Orokolheto,
			Kezi,
			Tipus
	   )
	   VALUES
	   (
			@jogosultId,
			'54E861A5-36ED-44CA-BAA7-C287D125B309',
			 'I',
			@ikatokonyvId,
			'0',
			'N',
			'T'

	   )
	END

	declare @kodcsoportId uniqueidentifier
	set @kodcsoportId = '11C09473-599F-409E-9924-E7F5CB39CD74'

	IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @kodcsoportId)
	BEGIN
	   print 'Add KRT_KodCsoportok - SPEC_KONYVEK'

	   	insert into KRT_KodCsoportok
		(
			 Id
			,Kod
			,Nev
			,Modosithato
			,Note
			,BesorolasiSema
		) 
		values
		(
			@kodcsoportId
			,'SPEC_KONYVEK'
			,'Speciális könyvek'
			,'1'
			, null
			,'0'
		)

	END

	declare @kodtarId uniqueidentifier
	set @kodtarId = '7D469C6E-21CB-44AC-90B3-138FB829BD18'

	IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
	BEGIN 
		 Print 'INSERT KRT_Kodtarak - SPEC_KONYVEK.MEGSEMMISITESINAPLO' 

		 insert into KRT_KodTarak
		 (
			 Id,
			 Org,
			 KodCsoport_Id,
			 Kod,
			 Nev,
			 Sorrend,
			 Modosithato,
			 ObjTip_Id_AdatElem,
			 Obj_Id
		) 
		values (
			@kodtarId,
			@org,
			@kodcsoportId,
			'SPEC_KONYVEK.MEGSEMMISITESINAPLO',
			'Megsemmisítési napló',
			'1',
			'1',
			(select Id from KRT_ObjTipusok where Kod = 'EREC_IraIktatokonyvek'),
			@ikatokonyvId
		); 
	 
	 END
	 ELSE
	 BEGIN
		Print 'UPDATE KRT_Kodtarak - SPEC_KONYVEK.MEGSEMMISITESINAPLO' 

		update KRT_KodTarak
			set Obj_Id = @ikatokonyvId
		WHERE Id = @kodtarId 
	 END

	 declare @funkcioId uniqueidentifier
	 set @funkcioId = 'D7E5C917-EDD4-449F-BE41-131A0914EBD0'

	 --JegyzekMegsemmisitesNaplo
	 IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE Id = @funkcioId)
	BEGIN 
		 Print 'INSERT KRT_Funkciok - JegyzekMegsemmisitesNaplo' 

		 insert into KRT_Funkciok
		 (
			 Id,
			 Kod,
			 Nev,
			 ObjTipus_Id_AdatElem,
			 Alkalmazas_Id,
			 Muvelet_Id
		) 
		values (
			@funkcioId,
			'JegyzekMegsemmisitesNaplo',
			'Jegyzék megsemmisítési napló',
			'FD52245E-A1F8-4EF4-AE05-259A33AA5BA3',
			'20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3',
			'9508908E-AEFA-41C6-B6D7-807E3AE3A3AB'
		); 
	 
	 END

	 set @szerepkorFunkcioId = 'AB81FD47-A37E-4FFB-85E4-EA9A015340FB'

	 --JegyzekMegsemmisites
	 IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE Id = @szerepkorFunkcioId)
	BEGIN 
		 Print 'INSERT KRT_Szerepkor_Funkcio - FEJLESZTO-JegyzekMegsemmisitesNaplo' 

		 insert into KRT_Szerepkor_Funkcio
		 (
			 Id,
			 Funkcio_Id,
			 Szerepkor_Id
		) 
		values (
			@szerepkorFunkcioId,
			@funkcioId,
			'E855F681-36ED-41B6-8413-576FCB5D1542'
		); 
	 
	 END

     declare @menuId uniqueidentifier
	 set @menuId = '2DA998E4-A921-4255-94EF-ECABD703366F'

	 --delete KRT_Menuk where Id = @menuId

	 --Megsemmisítési napló
	 IF NOT EXISTS (SELECT 1 FROM KRT_Menuk WHERE Id = @menuId)
	BEGIN 
		 Print 'INSERT KRT_Menuk - Megsemmisítési napló' 

		 insert into KRT_Menuk
		 (
			 Id,
			 Menu_Id_Szulo,
			 Kod,
			 Nev,
			 Funkcio_Id,
			 Modul_Id,
			 Parameter,
			 Sorrend
		) 
		values (
			@menuId,
			'C18E50DF-316D-46EB-AA55-AA4FDE99D183',
			'eRecord',
			'Megsemmisítési napló',
			@funkcioId,
			'C3F75D15-FBA0-4303-A847-B85A5314ED29',
			'Mode=MegsemmisitesiNaplo',
			'461'
		); 
	 
	 END
END

GO

----------------------------------------------------------
 --BLG 2963 MaintenancePlan
	 declare @mainTFunkcioId uniqueidentifier
	 set @mainTFunkcioId = '81181E11-281C-4801-A724-6F97B0BA3823'

	 IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE Id = @mainTFunkcioId)
	BEGIN 
		 Print 'INSERT KRT_Funkciok - MaintenancePlan' 

		 insert into KRT_Funkciok
		 (
			 Id,
			 Kod,
			 Nev,
			 ObjTipus_Id_AdatElem,
			 Alkalmazas_Id
		) 
		values (
			@mainTFunkcioId,
			'MaintenancePlan',
			'Maintenance Plan Karbantartás',
			'FD52245E-A1F8-4EF4-AE05-259A33AA5BA3',
			'20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		); 
	 
	 END

	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Modulok
			where Id='DE36D2B9-751B-4EB0-9193-7FC1B836E974') 
			Print '@record_id='+convert(NVARCHAR(100),@record_id) 
			if @record_id IS NULL 
			BEGIN 
	
				Print 'INSERT' 
				 insert into KRT_Modulok(
					 Tranz_id
					,Stat_id
					,Id
					,Tipus
					,Kod
					,Nev
					,Leiras
					) values (
					 null
					,null
					,'DE36D2B9-751B-4EB0-9193-7FC1B836E974'
					,'F'
					,'MaintenancePlanKezeles.aspx'
					,'Maintenance plan kezelés'
					,null
					); 
			END 	

	 declare @menuIdMainT uniqueidentifier
	 set @menuIdMainT = 'BF664F16-146D-4CF4-BE2B-8691056F76AA'
	IF NOT EXISTS (SELECT 1 FROM KRT_Menuk WHERE Id = @menuIdMainT)
	BEGIN 
		 Print 'INSERT KRT_Menuk - Maintenance plan' 

		 insert into KRT_Menuk
		 (
			 Id,
			 Menu_Id_Szulo,
			 Kod,
			 Nev,
			 Funkcio_Id,
			 Modul_Id,
			 Parameter,
			 Sorrend
		) 
		values (
			@menuIdMainT,
			'9E795C32-E7A6-445C-A4EB-6E5D48F2F42C',
			'eAdmin',
			'Maintenance plan kezelés',
			@mainTFunkcioId,
			'DE36D2B9-751B-4EB0-9193-7FC1B836E974',
			NULL,
			'45'
		); 
	 
	 END

	 	declare @blg_FEJL_szerepkorid uniqueidentifier
		select @blg_FEJL_szerepkorid = id from krt_szerepkorok where nev ='FEJLESZTO'

	 	IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = 'BB546698-B239-4D13-895E-338274CA618D')
	    INSERT INTO KRT_Szerepkor_Funkcio(
					 Szerepkor_Id
					,Id
					,Funkcio_Id
					) values (
					 @blg_FEJL_szerepkorid
					,'BB546698-B239-4D13-895E-338274CA618D'
					,@mainTFunkcioId
					); 





----------------------------------
-- BUG_5271
	--DECLARE @id uniqueidentifier SET @id = (select Id from KRT_Menuk
	--			where Id='C8456E19-2193-4D52-A249-194D9BABBF49') 
	--Print '@record_id='+convert(NVARCHAR(100),@id) 
	--if @id IS NULL 
	--BEGIN 
		
	--	Print 'ERROR: Nincs meg a menüpont (Belső keletkezésű (kimenő és belső) irat iktatása) vagy rossz az id' 
		 
	--END 
	--ELSE 
	--BEGIN 
	--	 Print 'UPDATE'
	--	 UPDATE KRT_Menuk
	--	 SET 
	--		 Nev='Saját keletkezésű (kimenő és belső irat) iktatása'
	--	 WHERE Id=@id 
	-- END
go

--------------------------------------------------------------------------
-- BLG 1052 - Vonalkód nyomtatásos gombok eltávolítása
-- VONALKOD_NYOMTATAS_FELULETEN rendszerparameter	 
-- FPH = BOPMH = NMHH-ban 1, NMHH TÜK-ben 0 [rejtett]
-- Csak TÜK esetén vizsgáljuk, szóval egyéb esetben nem számít az értéke

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='0EF009B6-3331-E811-80C9-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'0EF009B6-3331-E811-80C9-00155D020DD3'
		,'VONALKOD_NYOMTATAS_FELULETEN'
		,'0'
		,'1'
		,'Vonalkód nyomtatás láthatósága a felületen. 0-rejtve; 1-látható'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='0EF009B6-3331-E811-80C9-00155D020DD3'
	--	,Nev='VONALKOD_NYOMTATAS_FELULETEN'
	--	,Ertek='0'
	--	,Karbantarthato='1'
	--	,Note='Vonalkód nyomtatás láthatósága a felületen. 0-rejtve; 1-látható'
	-- WHERE Id=@record_id 
 --END
 --------------------------------------------------------------------------
 -- BLG_589 
declare @id uniqueidentifier
set @id = '4C12A702-7AF9-4150-A46B-724326EB26D9'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	print 'insert'

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/KuldKuldemenyFormTab.ascx',
		'labelBeerkezesModja',
		'Beérkezés módja:'
	)
END

GO

declare @id uniqueidentifier
set @id = 'B833F8B3-7738-43B5-80C6-8CF0D20CD514'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	print 'insert'

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'EgyszerusitettIktatasForm.aspx',
		'labelBeerkezesModja',
		'Beérkezés módja:'
	)
END

GO

-- BLG_589 end

-- BLG_1014 Ugyintezo cimke fordítások

declare @id uniqueidentifier

print 'NMHH'
print 'EgyszerusitettIktatasForm'
print 'labelUgyiratUgyintezo'
set @id = '0E7EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'EgyszerusitettIktatasForm.aspx',
		'labelUgyiratUgyintezo',
		'Felelős ügyintéző:'
	)
END
print '--'
print 'labelIratUgyintezo'

set @id = '0F7EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'EgyszerusitettIktatasForm.aspx',
		'labelIratUgyintezo',
		'Intézkedő ügyintéző:'
	)
END
print '----'
print '--'
print 'eRecordComponent/IraIratFormTab'
print 'labelUgyUgyirat_Ugyintezo'
set @id = '107EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/IraIratFormTab.ascx',
		'labelUgyUgyirat_Ugyintezo',
		'Felelős ügyintéző:'
	)
END

print '--'
print 'labelIratUgyintezo'

set @id = '117EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	
	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/IraIratFormTab.ascx',
		'labelIratUgyintezo',
		'Intézkedő ügyintéző:'
	)
END

print '--'
print 'resultPanel_UgyiratUgyintezo'

set @id = '127EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	
	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/IraIratFormTab.ascx',
		'resultPanel_UgyiratUgyintezo',
		'Felelős ügyintéző:'
	)
END

print '----'
print '--'
print 'eRecordComponent/UgyUgyiratFormTab'
print 'Ugyintezo_felirat'

set @id = '137EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	
	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/UgyUgyiratFormTab.ascx',
		'Ugyintezo_felirat',
		'Felelős ügyintéző:'
	)
END

print '----'
print '--'
print 'eRecordComponent/UgyiratokList'
print 'BoundField_UgyiratUgyintezo'
set @id = '147EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/UgyiratokList.ascx',
		'BoundField_UgyiratUgyintezo',
		'Felelős ügyintéző'
	)
END

print '----'
print '--'
print 'IraIratokList'
print 'BoundField_UgyiratUgyintezo'
set @id = '157EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'IraIratokList.aspx',
		'BoundField_UgyiratUgyintezo',
		'Felelős ügyintéző'
	)
END

print '--'
print 'BoundField_IratUgyintezo'
set @id = '167EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'IraIratokList.aspx',
		'BoundField_IratUgyintezo',
		'Intézkedő ügyintéző'
	)
END

print '----'
print '--'
print 'IraIratokSearch'
print 'labelIratUgyintezo'

set @id = '177EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	
	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'IraIratokSearch.aspx',
		'labelIratUgyintezo',
		'Intézkedő ügyintéző:'
	)
END


print '----'
print '--'
print 'UgyUgyiratokSearch'
print 'labelUgyUgyirat_Ugyintezo'

set @id = '187EE376-A0C3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	
	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'UgyUgyiratokSearch.aspx',
		'labelUgyUgyirat_Ugyintezo',
		'Felelős ügyintéző:'
	)
END

print '----'
print '--'
print 'eRecordComponent/UgyiratTerkepTab'
print 'StrUgyiratUgyintezo'

set @id = 'A396C9F8-BAC3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	
	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/UgyiratTerkepTab.ascx',
		'StrUgyiratUgyintezo',
		'Felelős ügyintéző:'
	)
END

print '--'
print 'StrIratUgyintezo'

set @id = '707F2D61-BDC3-E711-80C7-00155D027E9B'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	
	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		'NMHH',
		'eRecord',
		'eRecordComponent/UgyiratTerkepTab.ascx',
		'StrIratUgyintezo',
		'Intézkedő ügyintéző:'
	)
END

GO

-- BLG_1014 end
-------------------------------------------------------------------------------
PRINT 'BLG_2130 START'
IF EXISTS (SELECT 1 FROM KRT_MENUK WHERE ID = '98C84D42-5121-4FB8-961E-820A7AD459B7')
	BEGIN
		UPDATE KRT_MENUK
		SET Nev = 'E-Üzenet szabályok teszt futtatása'
	    WHERE ID = '98C84D42-5121-4FB8-961E-820A7AD459B7'
	END
PRINT 'BLG_2130 STOP'
-------------------------------------------------------------------------------

















-------------------------------------------------------
PRINT 'BLG_607 START' 

DECLARE @org_kod nvarchar(100) 
SET @org_kod = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
			
DECLARE @record_id uniqueidentifier 
SET @record_id = 'CF417B44-DEA8-4C01-8317-35000DBE3EDC'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE ID = @record_id) 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,@org_kod
		,@record_id
		,'TOBBES_IRATPELDANY_IKTATASKOR'
		,'2'
		,'1'
		,'Többszörös iratpéldány megadása iktatáskor (1=igen,2=nem)'
		); 
 END 

 PRINT 'BLG_607 STOP' 
 GO
-------------------------------------------------------

PRINT 'BLG_1915 START' 

--BLG 1915: Új partner validálása a partnertörzsbe.
--Az érkeztetés, iktatás közben felvételre került partnerek utólagosan validálásra kerülnek.
--INSERT funkció

IF NOT EXISTS (select 1 from [KRT_Funkciok] where Id = '2a61bdb1-35e2-4cb3-8cd0-4fa02fbeecc8')
	INSERT INTO [dbo].[KRT_Funkciok]
			   ([Id]
			   ,[Kod]
			   ,[Nev]
			   ,[ObjTipus_Id_AdatElem]
			   ,[ObjStat_Id_Kezd]
			   ,[Alkalmazas_Id]
			   ,[Muvelet_Id]
			   ,[ObjStat_Id_Veg]
			   ,[Leiras]
			   ,[Funkcio_Id_Szulo]
			   ,[Csoportosito]
			   ,[Modosithato]
			   ,[Org]
			   ,[MunkanaploJelzo]
			   ,[FeladatJelzo]
			   ,[KeziFeladatJelzo]
			   ,[Ver]
			   ,[Note]
			   ,[Stat_id]
			   ,[ErvKezd]
			   ,[ErvVege]
			   ,[Letrehozo_id]
			   ,[LetrehozasIdo]
			   ,[Modosito_id]
			   ,[ModositasIdo]
			   ,[Zarolo_id]
			   ,[ZarolasIdo]
			   ,[Tranz_id]
			   ,[UIAccessLog_id])
		 VALUES
			   ('2a61bdb1-35e2-4cb3-8cd0-4fa02fbeecc8',
			   'PartnerApproval',
			   'Partner jóváhagyása',
			   NULL,
			   NULL,
			 NULL,
			 NULL,
			 NULL,
			 'Az érkeztetés, iktatás közben felvételre került partnerek utólagosan validálása.',
			 NULL,
			 NULL,
			 1,
			 NULL,
			 NULL,
			 NULL,
			 NULL,
			 1,
			 NULL,
			 NULL,
			 '2018-04-14',
			 '4700-12-31',
			 NULL,
			 '2018-04-14',
			 NULL,
			 NULL,
			 NULL,
			 NULL,
			 NULL,
			 NULL
	)
GO

--BLG 1915: Új partner validálása a partnertörzsbe.
--Funkció FEJLESZTŐ szerepkörhöz rendelése
IF NOT EXISTS (select 1 from [KRT_Szerepkor_Funkcio] where Id = '8d349384-d8fa-404f-8941-6f471fe44bbd')
	INSERT INTO [dbo].[KRT_Szerepkor_Funkcio]
           ([Id]
           ,[Funkcio_Id]
           ,[Szerepkor_Id]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('8d349384-d8fa-404f-8941-6f471fe44bbd',
           '2a61bdb1-35e2-4cb3-8cd0-4fa02fbeecc8',
           'E855F681-36ED-41B6-8413-576FCB5D1542',
           1,
           NULL,
           NULL,
           '2018-04-14',
           '4700-12-31',
           NULL,
           '2018-04-14',
           NULL,
		   NULL,
		   NULL,
		   NULL,
		   NULL,
		   NULL)
GO

--BLG 1915: Új partner validálása a partnertörzsbe.
--PARTNER_ALLAPOT kódcsoport hozzáadása
IF NOT EXISTS (select 1 from [KRT_KodCsoportok] where Id = '781539d0-69e7-4338-9a52-439cfcbb155e')
	INSERT INTO [dbo].[KRT_KodCsoportok]
           ([Id]
           ,[Org]
           ,[KodTarak_Id_KodcsoportTipus]
           ,[Kod]
           ,[Nev]
           ,[Modosithato]
           ,[Hossz]
           ,[Csoport_Id_Tulaj]
           ,[Leiras]
           ,[BesorolasiSema]
           ,[KiegAdat]
           ,[KiegMezo]
           ,[KiegAdattipus]
           ,[KiegSzotar]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('781539d0-69e7-4338-9a52-439cfcbb155e',
           NULL,
           NULL,
           'PARTNER_ALLAPOT',
           'Partnerek validálásának állapotkódja',
           1,
           0,
            NULL,
           'jóváhagyandó vagy jóváhagyott',
           0,
		   NULL,
           NULL,
           NULL,
           NULL,
           1,
           'Partner validálásának állapotkódja',
           NULL,
           '2018-04-14',
           '4700-12-31',
            NULL,
           '2018-04-14',
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
			)
GO

--BLG 1915: Új partner validálása a partnertörzsbe.
--PARTNER_ALLAPOT [JOVAHAGYANDO] érték hozzáadása kódtárba
IF NOT EXISTS (select 1 from [KRT_KodTarak] where Id = 'e26d1a6d-4d1c-46f1-aafd-1e886849c7c5')
	INSERT INTO [dbo].[KRT_KodTarak]
           ([Id]
           ,[Org]
           ,[KodCsoport_Id]
           ,[ObjTip_Id_AdatElem]
           ,[Obj_Id]
           ,[Kod]
           ,[Nev]
           ,[RovidNev]
           ,[Egyeb]
           ,[Modosithato]
           ,[Sorrend]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('e26d1a6d-4d1c-46f1-aafd-1e886849c7c5',
           '450B510A-7CAA-46B0-83E3-18445C0C53A9',
           '781539d0-69e7-4338-9a52-439cfcbb155e',
           NULL,
           NULL,
          'JOVAHAGYANDO',
           'Jóváhagyandó',
           NULL,
           NULL,
           1,
           1,
           1,
           NULL,
           NULL,
            '2018-04-14',
           '4700-12-31',
            NULL,
           '2018-04-14',
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
			)
GO

--BLG 1915: Új partner validálása a partnertörzsbe.
--PARTNER_ALLAPOT [JOVAHAGYOTT] érték hozzáadása kódtárba
IF NOT EXISTS (select 1 from [KRT_KodTarak] where Id = 'c8d1095e-b4aa-485d-a3b0-3dd7fa6133f3')
INSERT INTO [dbo].[KRT_KodTarak]
           ([Id]
           ,[Org]
           ,[KodCsoport_Id]
           ,[ObjTip_Id_AdatElem]
           ,[Obj_Id]
           ,[Kod]
           ,[Nev]
           ,[RovidNev]
           ,[Egyeb]
           ,[Modosithato]
           ,[Sorrend]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('c8d1095e-b4aa-485d-a3b0-3dd7fa6133f3',
           '450B510A-7CAA-46B0-83E3-18445C0C53A9',
           '781539d0-69e7-4338-9a52-439cfcbb155e',
           NULL,
           NULL,
          'JOVAHAGYOTT',
           'Jóváhagyott',
           NULL,
           NULL,
           1,
           2,
           1,
           NULL,
           NULL,
            '2018-04-14',
           '4700-12-31',
            NULL,
           '2018-04-14',
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
			)
GO
PRINT 'BLG_1915 STOP' 
 GO


 -- BLG_1721 

-- PostazasForm.aspx felvitele a modulok közé








-- BLG_1721 vége


-- BLG_1952 Partnerform fordítások

declare @OrgKod nvarchar(100)
set @OrgKod = (select Kod from KRT_Orgok where Id = '450B510A-7CAA-46B0-83E3-18445C0C53A9')

print '@Org = ' + @OrgKod

declare @id uniqueidentifier

-- Születési név -> Születési családi és utóneve
set @id = 'E41AF5DC-AC43-E811-80C9-00155D020DD3'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	print 'insert Születési családi és utóneve'

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		@OrgKod,
		'eAdmin',
		'PartnerekForm.aspx',
		'labelSzuletesiNev',
		'Születési családi és utóneve:'
	)
END

-- Személyi igazolvány szám -> Személyazonosító igazolvány okmányazonosítója

set @id = 'E51AF5DC-AC43-E811-80C9-00155D020DD3'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	print 'insert Személyazonosító igazolvány okmányazonosítója'

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		@OrgKod,
		'eAdmin',
		'PartnerekForm.aspx',
		'labelSzemelyi',
		'Személyazonosító igazolvány okmányazonosítója:'
	)
END

-- Személyi azonosító -> Személyazonosító jel

set @id = 'E61AF5DC-AC43-E811-80C9-00155D020DD3'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	print 'insert Személyazonosító jel'

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		@OrgKod,
		'eAdmin',
		'PartnerekForm.aspx',
		'labelSzemelyiAzonosito',
		'Személyazonosító jel:'
	)
END

-- Cég típus -> Cégforma

set @id = 'E71AF5DC-AC43-E811-80C9-00155D020DD3'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	print 'insert Cégforma'

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		@OrgKod,
		'eAdmin',
		'PartnerekForm.aspx',
		'labelCegTipus',
		'Cégforma:'
	)
END
GO

-- BLG_1952 end

-- BLG_1607 Elintézési időtartam fordítás
declare @OrgKod nvarchar(100)
set @OrgKod = (select Kod from KRT_Orgok where Id = '450B510A-7CAA-46B0-83E3-18445C0C53A9')

print '@Org = ' + @OrgKod

declare @id uniqueidentifier

-- Időtartam -> Üzemzavar időrtartama
set @id = '7D6BC875-6E44-E811-80C9-00155D020DD3'

if not exists (select 1 from KRT_Forditasok where Id =@id)
BEGIN
	print 'insert Üzemzavar időrtartama'

	insert into KRT_Forditasok
     (
		 [Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
	)
	VALUES
	(
		@id,
		'hu-HU',
		@OrgKod,
		'eRecord',
		'eRecordComponent/ElintezesAdatok.ascx',
		'labelIdotartam',
		'Üzemzavar időrtartama:'
	)
END

-- BLG_1607 end


--------------------------------------------------------------------------------
	DECLARE @1606_KCSID UNIQUEIDENTIFIER
	SELECT TOP 1 @1606_KCSID = Id FROM KRT_KODCSOPORTOK WHERE KOD = 'FELADAT_TIPUS'

	DECLARE @1606_RECORDID UNIQUEIDENTIFIER
	SET @1606_RECORDID = '3AC43DCE-DE9E-4090-8D88-5240D47C39EA'

	IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @1606_RECORDID)
	BEGIN 
		 Print 'INSERT KRT_Kodtarak - FELADAT_TIPUS ElintezesMegjegyzes' 

		 insert into KRT_KodTarak
		 (
			 Id,
			 Org,
			 KodCsoport_Id,
			 Kod,
			 Nev,
			 Sorrend,
			 Modosithato
		) 
		values (
			@1606_RECORDID,
			'450B510A-7CAA-46B0-83E3-18445C0C53A9',
			@1606_KCSID,
			'E',
			'ElintezesMegjegyzes',
			'03',
			'0'
		); 
	 
	 END
--------------------------------------------------------------------------------
-------------------------------------------------------

--BLG 2166 korábbi ragszámsávokra darabszám számítás
PRINT 'BLG_2166 START' 
DECLARE @numberOfRecords int
SET @numberOfRecords = (select count(*) from KRT_RagszamSavok where IgenyeltDarabszam is null) 

Print '@numberOfRecords='+convert(NVARCHAR(100),@numberOfRecords) 
if @numberOfRecords > 0 
BEGIN 
    Print 'UPDATE'
    UPDATE KRT_RagszamSavok
    SET 
    IgenyeltDarabszam = FLOOR((SavVegeNum - SavKezdNum) / 10)
    WHERE IgenyeltDarabszam is null 
       
  END

PRINT 'BLG_2166 END' 




-- BLG_1721 

-- PostazasForm.aspx felvitele a modulok közé

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Modulok
			where Id='F010C312-6E39-E811-80C9-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
	
	Print 'INSERT' 
	 insert into KRT_Modulok(
		 Tranz_id
		,Stat_id
		,Id
		,Tipus
		,Kod
		,Nev
		,Leiras
		) values (
		 null
		,null
		,'F010C312-6E39-E811-80C9-00155D020DD3'
		,'F'
		,'PostazasForm.aspx'
		,'Postázás'
		,null
		); 
END 	
go

	 
	 
-- KRT_Nezetek definiálás TÜK Postázáshoz
-- PostazasForm
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
if ((@org_kod = 'NMHH') AND (@isTUK = '1')) 
BEGIN
	Print 'KRT_Nezetek definiálás TÜK Postázáshoz - PostazasForm'
	DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Nezetek
				where Id='5997A8F7-7639-E811-80C9-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
			
		Print 'INSERT' 
		INSERT [dbo].[KRT_Nezetek] ([Id], [Org], [Form_Id], [Nev], 
									[Leiras],[Csoport_Id], [Muvelet_Id], 
									[DisableControls], 
									[InvisibleControls], 
									[ReadOnlyControls], [Prioritas]) 
						VALUES (N'5997A8F7-7639-E811-80C9-00155D020DD3', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'F010C312-6E39-E811-80C9-00155D020DD3', N'TUK_Postazas', 
									NULL, NULL, NULL, 
									NULL, 
									N'ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_Ragszam,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$trViszonylatKod,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_mennyiseg,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_szolgaltatasok1,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_szolgaltatasok2,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_TertivevenyVonalkod,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$Tertiveveny_VonalkodTextBox,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$Ragszam_TextBox,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$VonalKodTextBox1,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$cbAblakosBoritek,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_Vonalkod,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$Label27,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$Ar_TextBox,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$cbKuldemenyFajtajaMegorzes,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$Label26,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$KimenoKuldFajta_KodtarakDropDownList,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$Elsobbsegi_CheckBox,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_ar,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_kuldemenyfajtaja',
									NULL, 1)

	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Nezetek
		 SET 
			 Form_Id ='F010C312-6E39-E811-80C9-00155D020DD3'
			,Nev='TUK_Postazas'
			,DisableControls=NULL
			,InvisibleControls = 'ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_Ragszam,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$trViszonylatKod,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_mennyiseg,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_szolgaltatasok1,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_szolgaltatasok2,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_TertivevenyVonalkod,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$Tertiveveny_VonalkodTextBox,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$Ragszam_TextBox,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$VonalKodTextBox1,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$cbAblakosBoritek,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_Vonalkod,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$Label27,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$Ar_TextBox,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$cbKuldemenyFajtajaMegorzes,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$Label26,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$KimenoKuldFajta_KodtarakDropDownList,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$Elsobbsegi_CheckBox,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_ar,ctl00$ContentPlaceHolder1$PostazasiAdatokPanel1$tr_kuldemenyfajtaja'
			,ReadOnlyControls = NULL
			,Prioritas = 1
		 WHERE Id=@record_id
	 END
END

-- KuldKuldemenyekForm
if ((@org_kod = 'NMHH') AND (@isTUK = '1')) 
BEGIN
	Print 'KRT_Nezetek definiálás TÜK Postázáshoz - KuldKuldemenyekForm'
	SET @record_id = (select Id from KRT_Nezetek
				where Id='BC9EC910-6A41-E811-80C9-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
			
		Print 'INSERT' 
		INSERT [dbo].[KRT_Nezetek] ([Id], [Org], [Form_Id], [Nev], 
									[Leiras],[Csoport_Id], [Muvelet_Id], 
									[DisableControls], 
									[InvisibleControls], 
									[ReadOnlyControls], [Prioritas]) 
						VALUES (N'BC9EC910-6A41-E811-80C9-00155D020DD3', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'78471B4C-EE48-4268-8A26-A99E4FC85E4D', N'TUK_Postazas', 
									NULL, NULL, NULL, 
									NULL, 
									N'ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$cbKuldemenyFajtajaMegorzes,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$Ajanlott_CheckBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$Tertiveveny_CheckBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$SajatKezbe_CheckBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$E_Elorejelzes_CheckBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_kuldemenyfajtaja,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_szolgaltatasok1,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_szolgaltatasok2,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_ar,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_mennyiseg,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$Ar_TextBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$Peldanyszam_RequiredNumberBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$E_Ertesites_CheckBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$PostaiLezaroSzolgalat_CheckBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$KimenoKuldFajta_KodtarakDropDownList,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$Elsobbsegi_CheckBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_Vonalkod,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$VonalKodTextBox1,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$cbAblakosBoritek,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_Ragszam,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_TertivevenyVonalkod,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$trViszonylatKod',
									NULL, 1)

	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Nezetek
		 SET 
			 Form_Id ='78471B4C-EE48-4268-8A26-A99E4FC85E4D'
			,Nev='TUK_Postazas'
			,DisableControls=NULL
			,InvisibleControls = 'ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$cbKuldemenyFajtajaMegorzes,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$Ajanlott_CheckBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$Tertiveveny_CheckBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$SajatKezbe_CheckBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$E_Elorejelzes_CheckBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_kuldemenyfajtaja,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_szolgaltatasok1,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_szolgaltatasok2,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_ar,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_mennyiseg,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$Ar_TextBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$Peldanyszam_RequiredNumberBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$E_Ertesites_CheckBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$PostaiLezaroSzolgalat_CheckBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$KimenoKuldFajta_KodtarakDropDownList,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$Elsobbsegi_CheckBox,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_Vonalkod,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$VonalKodTextBox1,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$cbAblakosBoritek,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_Ragszam,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$tr_TertivevenyVonalkod,ctl00$ContentPlaceHolder1$KuldKuldemenyTabContainer$TabKuldemenyPanel$KuldemenyTab1$KimenoKuld_PostazasiAdatokPanel$trViszonylatKod'
			,ReadOnlyControls = NULL
			,Prioritas = 1
		 WHERE Id=@record_id
	 END
END
go

-- BLG_1721 vége

-- Rendszerparaméter:  JEGYZEK_CSAK_SAJATSZERVEZET_LATHATO
DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege)

		
DECLARE @record_id uniqueidentifier 
SET @record_id = 'F3B69701-CA8F-4C55-9A91-A4D798A640B4'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE ID = @record_id) 
BEGIN 
		Print 'INSERT - JEGYZEK_CSAK_SAJATSZERVEZET_LATHATO' 
		insert into KRT_Parameterek(
			Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) 
		values 
		(
		@record_id
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'JEGYZEK_CSAK_SAJATSZERVEZET_LATHATO'
		,case when (@isTUK = '1') then '1' else '0' end
		,'1'
		,'1 = csak annak a szervezetnek a jegyzékei láthatóak, amellyel a felhasználó bejelentkezett; 0 = minden tétel látszik függetlenül a bejelentkezett felhasználó szervezetétől'
		); 
END

GO


-------------------------------------------------------------------------------
-- BLG 2199 - Főnyilvántartókönyvi szám megjelenítés
-- A paraméter értéke megadja, hogy az iktatókönyv azonosítója minimum milyen hosszú legyen - amennyiben ennél rövidebb lenne, akkor vezető 0-val kiegészítjük a számot	
-- IKTATOKONYV_AZONOSITO_HOSSZ rendszerparameter	 
-- NMHH TÜK-ben 0 [nincs kiegészítés]
DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 

DECLARE @record_id uniqueidentifier 

if (@isTUK = '1') 
BEGIN
			
	SET @record_id = (select Id from KRT_Parameterek
				where Id='451a31e1-6dc4-418f-9fa1-881334fbcee8') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 

	if @record_id IS NULL 
	BEGIN 
		Print 'INSERT' 
		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Id
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 null
			,null
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'451a31e1-6dc4-418f-9fa1-881334fbcee8'
			,'IKTATOKONYV_AZONOSITO_HOSSZ'
			,'0'
			,'0'
			,'A paraméter értéke megadja, hogy az iktatókönyv azonosítója minimum milyen hosszú legyen - amennyiben ennél rövidebb lenne, akkor vezető 0-val kiegészítjük a számot.'
			); 
	 END 
	 --ELSE 
	 --BEGIN 
		-- Print 'UPDATE'
		-- UPDATE KRT_Parameterek
		-- SET 
		--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		--	,Id='451a31e1-6dc4-418f-9fa1-881334fbcee8'
		--	,Nev='IKTATOKONYV_AZONOSITO_HOSSZ'
		--	,Ertek='0'
		--	,Karbantarthato='0'
		--	,Note='A paraméter értéke megadja, hogy az iktatókönyv azonosítója minimum milyen hosszú legyen - amennyiben ennél rövidebb lenne, akkor vezető 0-val kiegészítjük a számot.'
		-- WHERE Id=@record_id 
	 --END
 END

 -- NMHH-ban 5 az érték
 if (@isTUK = '0') 
 BEGIN
   SET @record_id = (select Id from KRT_Parameterek
				where Id='451a31e1-6dc4-418f-9fa1-881334fbcee8') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 

	if @record_id IS NULL 
	BEGIN 
		Print 'INSERT' 
		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Id
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 null
			,null
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'451a31e1-6dc4-418f-9fa1-881334fbcee8'
			,'IKTATOKONYV_AZONOSITO_HOSSZ'
			,'5'
			,'0'
			,'A paraméter értéke megadja, hogy az iktatókönyv azonosítója minimum milyen hosszú legyen - amennyiben ennél rövidebb lenne, akkor vezető 0-val kiegészítjük a számot.'
			); 
	 END 
	 --ELSE 
	 --BEGIN 
		-- Print 'UPDATE'
		-- UPDATE KRT_Parameterek
		-- SET 
		--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		--	,Id='451a31e1-6dc4-418f-9fa1-881334fbcee8'
		--	,Nev='IKTATOKONYV_AZONOSITO_HOSSZ'
		--	,Ertek='5'
		--	,Karbantarthato='0'
		--	,Note='A paraméter értéke megadja, hogy az iktatókönyv azonosítója minimum milyen hosszú legyen - amennyiben ennél rövidebb lenne, akkor vezető 0-val kiegészítjük a számot.'
		-- WHERE Id=@record_id 
	 --END
 END

 GO
 --------------------------------------------------------------------------------
 ------------ BLG 2199 - Főnyilvántartókönyvi szám megjelenítés vége ------------


DECLARE @isTUK nvarchar(400) 

SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			  where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' 
			  and getdate() between ervkezd and ervvege) 

--IF @isTUK = '1'
--BEGIN
--	print 'Set IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS = FileSystem'
--	update KRT_Parameterek
--	set Ertek = 'FileSystem'
--	where Nev = 'IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS'
--END

GO



-----------------------------------------------------
PRINT 'BLG_323 START'

		--TIPUSOK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier=	null
		DECLARE @ObjTipus_Id_Tipus uniqueidentifier=	null
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Obj_Id_Szulo uniqueidentifier	=	null
		DECLARE @KodCsoport_Id uniqueidentifier=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id			='6454C1AD-DE40-45AB-930F-34484372145F'
		set @Kod		='EREC_FeladatErtesites'
		set @Nev		='EREC_FeladatErtesites tábla'

		IF NOT EXISTS (SELECT 1 FROM KRT_ObjTipusok WHERE ID = @ID)
		EXECUTE @RC = [dbo].[sp_KRT_ObjTipusokInsert] 
		   @Id
		  ,@ObjTipus_Id_Tipus
		  ,@Kod
		  ,@Nev
		  ,@Obj_Id_Szulo
		  ,@KodCsoport_Id
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--TIPUSOK END
		-----------------------------------------------------------------------
		--FUNKCIÓK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= '0F0E1DAB-5D4E-4E1F-A455-56997F796FEF'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '5B9AC7B3-879C-48A7-BEBA-EF73A49AF893'
		set @ObjTipus_Id_AdatElem	= '6454C1AD-DE40-45AB-930F-34484372145F'
		set @Kod					= 'FeladatErtesitesKezeles'
		set @Nev					= 'Feladat értesítés kezelése'

		IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE ID = @ID)
		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCIÓK END

		--FUNKCIÓK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= '99443E9F-7750-4C53-9E5C-045637DC075E'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '5B9AC7B3-879C-48A7-BEBA-EF73A49AF893'
		set @ObjTipus_Id_AdatElem	= '6454C1AD-DE40-45AB-930F-34484372145F'
		set @Kod					= 'FeladatErtesitesModify'
		set @Nev					= 'Feladat értesítés módosítása'

		IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE ID = @ID)
		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCIÓK END

		--FUNKCIÓK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= 'C215F6E1-DCFC-4AC5-B771-1AAF8C818740'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '939A0831-2914-4348-93FA-9DED0225747E'
		set @ObjTipus_Id_AdatElem	= '6454C1AD-DE40-45AB-930F-34484372145F'
		set @Kod					= 'FeladatErtesitesNew'
		set @Nev					= 'Feladat értesítés létrehozás'

		IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE ID = @ID)
		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCIÓK END

		--FUNKCIÓK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= '38AF1BF0-1AAB-4E0A-AB87-79768487785C'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '9508908E-AEFA-41C6-B6D7-807E3AE3A3AB'
		set @ObjTipus_Id_AdatElem	= '6454C1AD-DE40-45AB-930F-34484372145F'
		set @Kod					= 'FeladatErtesitesList'
		set @Nev					= 'Feladat értesítés lista'

		IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE ID = @ID)
		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCIÓK END
	
		--FUNKCIÓK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= 'DA36E651-4A54-46DF-9376-AE2601D5A61E'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '78F0D5A2-E048-4A1F-9F11-5D57AB78B985'
		set @ObjTipus_Id_AdatElem	= '6454C1AD-DE40-45AB-930F-34484372145F'
		set @Kod					= 'FeladatErtesitesView'
		set @Nev					= 'Feladat értesítés megtekintése'

		IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE ID = @ID)
		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCIÓK END

		--FUNKCIÓK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @ObjTipus_Id_AdatElem uniqueidentifier=	null
		DECLARE @ObjStat_Id_Kezd uniqueidentifier=	null
		DECLARE @Alkalmazas_Id uniqueidentifier=	null
		DECLARE @Muvelet_Id uniqueidentifier=	null
		DECLARE @ObjStat_Id_Veg uniqueidentifier=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @Funkcio_Id_Szulo uniqueidentifier=	null
		DECLARE @Csoportosito char(1)=	null
		DECLARE @Modosithato char(1)=	null
		DECLARE @Org uniqueidentifier=	null
		DECLARE @MunkanaploJelzo char(1)=	null
		DECLARE @FeladatJelzo char(1)=	null
		DECLARE @KeziFeladatJelzo char(1)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @Id						= 'CA9A4EC4-8F7A-41F0-964A-435A6FA3B0D8'
		set @Alkalmazas_Id			= '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		set @Muvelet_Id				= '805C7634-F205-4AE7-8580-DB3C73D8F43B'
		set @ObjTipus_Id_AdatElem	= '6454C1AD-DE40-45AB-930F-34484372145F'
		set @Kod					= 'FeladatErtesitesInvalidate'
		set @Nev					= 'Feladat értesítés érvénytelenítés'

		IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE ID = @ID)
		EXECUTE @RC = [dbo].[sp_KRT_FunkciokInsert] 
		   @Id
		  ,@Kod
		  ,@Nev
		  ,@ObjTipus_Id_AdatElem
		  ,@ObjStat_Id_Kezd
		  ,@Alkalmazas_Id
		  ,@Muvelet_Id
		  ,@ObjStat_Id_Veg
		  ,@Leiras
		  ,@Funkcio_Id_Szulo
		  ,@Csoportosito
		  ,@Modosithato
		  ,@Org
		  ,@MunkanaploJelzo
		  ,@FeladatJelzo
		  ,@KeziFeladatJelzo
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--FUNKCIÓK END
		
		-----------------------------------
		--MODULOK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Org uniqueidentifier=	null
		DECLARE @Tipus nvarchar(10)=	null
		DECLARE @Kod nvarchar(100)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Leiras nvarchar(4000)=	null
		DECLARE @SelectString nvarchar(4000)=	null
		DECLARE @Csoport_Id_Tulaj uniqueidentifier=	null
		DECLARE @Parameterek nvarchar(4000)=	null
		DECLARE @ObjTip_Id_Adatelem uniqueidentifier=	null
		DECLARE @ObjTip_Adatelem nvarchar(100)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		SET @Id			='429730AF-32E4-4DFE-97CA-9504CDB68F94'
		SET @Kod		='FeladatErtesitesList.aspx'
		SET @Nev		='Feladat értesítés lista'
		SET @Tipus		='F'

		IF NOT EXISTS (SELECT 1 FROM KRT_Modulok WHERE ID = @ID)
		EXECUTE @RC = [dbo].[sp_KRT_ModulokInsert] 
		   @Id
		  ,@Org
		  ,@Tipus
		  ,@Kod
		  ,@Nev
		  ,@Leiras
		  ,@SelectString
		  ,@Csoport_Id_Tulaj
		  ,@Parameterek
		  ,@ObjTip_Id_Adatelem
		  ,@ObjTip_Adatelem
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--MODULOK END
		-----------------------------------
		--MENUK
		DECLARE @RC int
		DECLARE @Id uniqueidentifier
		DECLARE @Org uniqueidentifier=	null
		DECLARE @Menu_Id_Szulo uniqueidentifier=	null
		DECLARE @Kod nvarchar(20)=	null
		DECLARE @Nev nvarchar(100)=	null
		DECLARE @Funkcio_Id uniqueidentifier=	null
		DECLARE @Modul_Id uniqueidentifier=	null
		DECLARE @Parameter nvarchar(400)=	null
		DECLARE @Ver int=	null
		DECLARE @Note nvarchar(4000)=	null
		DECLARE @Stat_id uniqueidentifier=	null
		DECLARE @ImageURL nvarchar(400)=	null
		DECLARE @Sorrend nvarchar(100)=	null
		DECLARE @ErvKezd datetime=	null
		DECLARE @ErvVege datetime=	null
		DECLARE @Letrehozo_id uniqueidentifier=	null
		DECLARE @LetrehozasIdo datetime=	null
		DECLARE @Modosito_id uniqueidentifier=	null
		DECLARE @ModositasIdo datetime=	null
		DECLARE @Zarolo_id uniqueidentifier=	null
		DECLARE @ZarolasIdo datetime=	null
		DECLARE @Tranz_id uniqueidentifier=	null
		DECLARE @UIAccessLog_id uniqueidentifier=	null
		DECLARE @UpdatedColumns xml=	null
		DECLARE @ResultUid uniqueidentifier=	null

		set @id				= '7BE4D810-1B50-495F-B690-2D3EF9319586'
		set @Menu_Id_Szulo	= 'BA087AE2-E0D5-4744-9062-EE4B802B955C'
		set @Kod			= 'eRecord'
		set @Nev			= 'Feladat értesítés kezelése'
		set @Funkcio_Id		= '0F0E1DAB-5D4E-4E1F-A455-56997F796FEF'
		set @Modul_Id		= '429730AF-32E4-4DFE-97CA-9504CDB68F94'
		set @Sorrend		= 782

		IF NOT EXISTS (SELECT 1 FROM KRT_MENUK WHERE ID = @ID)
		EXECUTE @RC = [dbo].[sp_KRT_MenukInsert] 
		   @Id
		  ,@Org
		  ,@Menu_Id_Szulo
		  ,@Kod
		  ,@Nev
		  ,@Funkcio_Id
		  ,@Modul_Id
		  ,@Parameter
		  ,@Ver
		  ,@Note
		  ,@Stat_id
		  ,@ImageURL
		  ,@Sorrend
		  ,@ErvKezd
		  ,@ErvVege
		  ,@Letrehozo_id
		  ,@LetrehozasIdo
		  ,@Modosito_id
		  ,@ModositasIdo
		  ,@Zarolo_id
		  ,@ZarolasIdo
		  ,@Tranz_id
		  ,@UIAccessLog_id
		  ,@UpdatedColumns
		  ,@ResultUid OUTPUT
		GO
		--MENUK END

		declare @blg_323_szerepkorid uniqueidentifier
		select @blg_323_szerepkorid = id from krt_szerepkorok where nev ='FEJLESZTO'
		IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = '4A0D54DC-FB7E-418A-993B-EFCA3FBC1D42')
	    INSERT INTO KRT_Szerepkor_Funkcio(
					 Szerepkor_Id
					,Tranz_id
					,Stat_id
					,Id
					,Funkcio_Id
					) values (
					 @blg_323_szerepkorid
					,'F3437B80-97AC-4A12-A274-3A594E0FB61D'
					,'F3437B80-97AC-4A12-A274-3A594E0FB61A'
					,'4A0D54DC-FB7E-418A-993B-EFCA3FBC1D42'
					,'0F0E1DAB-5D4E-4E1F-A455-56997F796FEF'
					); 

		IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = '4A0D54DC-FB7E-418A-993B-EFCA3FBC1D43')
	    INSERT INTO KRT_Szerepkor_Funkcio(
					 Szerepkor_Id
					,Tranz_id
					,Stat_id
					,Id
					,Funkcio_Id
					) values (
					 @blg_323_szerepkorid
					,'F3437B80-97AC-4A12-A274-3A594E0FB61D'
					,'F3437B80-97AC-4A12-A274-3A594E0FB61A'
					,'4A0D54DC-FB7E-418A-993B-EFCA3FBC1D43'
					,'99443E9F-7750-4C53-9E5C-045637DC075E'
					); 

		IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = '4A0D54DC-FB7E-418A-993B-EFCA3FBC1D44')
	    INSERT INTO KRT_Szerepkor_Funkcio(
					 Szerepkor_Id
					,Tranz_id
					,Stat_id
					,Id
					,Funkcio_Id
					) values (
					 @blg_323_szerepkorid
					,'F3437B80-97AC-4A12-A274-3A594E0FB61D'
					,'F3437B80-97AC-4A12-A274-3A594E0FB61A'
					,'4A0D54DC-FB7E-418A-993B-EFCA3FBC1D44'
					,'C215F6E1-DCFC-4AC5-B771-1AAF8C818740'
					); 

		IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = '4A0D54DC-FB7E-418A-993B-EFCA3FBC1D45')
	    INSERT INTO KRT_Szerepkor_Funkcio(
					 Szerepkor_Id
					,Tranz_id
					,Stat_id
					,Id
					,Funkcio_Id
					) values (
					 @blg_323_szerepkorid
					,'F3437B80-97AC-4A12-A274-3A594E0FB61D'
					,'F3437B80-97AC-4A12-A274-3A594E0FB61A'
					,'4A0D54DC-FB7E-418A-993B-EFCA3FBC1D45'
					,'38AF1BF0-1AAB-4E0A-AB87-79768487785C'
					); 

		IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = '4A0D54DC-FB7E-418A-993B-EFCA3FBC1D46')
	    INSERT INTO KRT_Szerepkor_Funkcio(
					 Szerepkor_Id
					,Tranz_id
					,Stat_id
					,Id
					,Funkcio_Id
					) values (
					 @blg_323_szerepkorid
					,'F3437B80-97AC-4A12-A274-3A594E0FB61D'
					,'F3437B80-97AC-4A12-A274-3A594E0FB61A'
					,'4A0D54DC-FB7E-418A-993B-EFCA3FBC1D46'
					,'DA36E651-4A54-46DF-9376-AE2601D5A61E'
					);

		IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = '4A0D54DC-FB7E-418A-993B-EFCA3FBC1D47')
	    INSERT INTO KRT_Szerepkor_Funkcio(
					 Szerepkor_Id
					,Tranz_id
					,Stat_id
					,Id
					,Funkcio_Id
					) values (
					 @blg_323_szerepkorid
					,'F3437B80-97AC-4A12-A274-3A594E0FB61D'
					,'F3437B80-97AC-4A12-A274-3A594E0FB61A'
					,'4A0D54DC-FB7E-418A-993B-EFCA3FBC1D47'
					,'CA9A4EC4-8F7A-41F0-964A-435A6FA3B0D8'
					);
GO  
PRINT 'BLG_323 STOP'
-----------------------------------------------------

 -- BLG_608: KodtarakListBox kód felvitele a verérlőelemek közé	 

 PRINT 'BLG_608'
 
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
		where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
--if @org_kod = 'BOPMH' 
--BEGIN 

	Print @org_kod 
	print 'KodtarakListBox'
 	DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_KodTarak
		where Id='A697D2DA-3B45-E811-80C9-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
		
		Print 'INSERT' 
			insert into KRT_KodTarak(
				KodCsoport_Id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			) values (
			'89F4B044-2DB6-4FB8-BECB-8BDBC9E1DC95'
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'A697D2DA-3B45-E811-80C9-00155D020DD3'
			,'~/Component/KodTarakListBox.ascx'
			,'Kódválasztó lista (KodtarakListBox)'
			,''
			,'1'
			,'14'
			); 
		END 
		ELSE 
		BEGIN 
			Print 'UPDATE'
			UPDATE KRT_KodTarak
			SET 
			KodCsoport_Id='89F4B044-2DB6-4FB8-BECB-8BDBC9E1DC95'
			,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Kod='~/Component/KodTarakListBox.ascx'
			,Nev='Kódválasztó lista (KodtarakListBox)'
			,RovidNev=''
			,Modosithato='1'
			,Sorrend='14'
			WHERE Id='A697D2DA-3B45-E811-80C9-00155D020DD3'

		END

go

print 'EREC_Obj_MetaAdatai.opcionalis beállítás'
declare @beforedate datetime 
set @beforedate = '2018-05-01'

print 'dátum:'
print @beforedate 

update EREC_Obj_MetaAdatai
	set opcionalis = CASE WHEN opcionalis > '0' THEN '0'  ELSE '1' END ,
	    modositasIdo = getdate()
where isnull(modositasIdo,letrehozasIdo) < @beforedate

go
PRINT 'BLG_608 STOP'

-- BUG_2159 FoszamIrattarbaVetel Funkciókód Felvitele
--        és FoszamIrattarbolKikeres Funkciókód Felvitele  

Print 'FoszamIrattarbaVetel Funkcio felvitele'
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='82D6BF43-EB51-E811-80CB-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
		,'0'
		,'82D6BF43-EB51-E811-80CB-00155D020DD3'
		,'FoszamIrattarbaVetel'
		,'Irattárba átvétel'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
		,Modosithato='0'
		,Id='82D6BF43-EB51-E811-80CB-00155D020DD3'
		,Kod='FoszamIrattarbaVetel'
		,Nev='Irattárba átvétel'
	 WHERE Id=@record_id 
 END
go
	
Print 'KRT_Szerepkor_Funkcio - hozzarendeles FEJLESZTO-hoz (FoszamIrattarbaVetel)'
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='0293448F-EB51-E811-80CB-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Szerepkor_Funkcio(
		 Szerepkor_Id
		,Id
		,Funkcio_Id
		) values (
		 'e855f681-36ed-41b6-8413-576fcb5d1542'
		,'0293448F-EB51-E811-80CB-00155D020DD3'
		,'82D6BF43-EB51-E811-80CB-00155D020DD3'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Szerepkor_Funkcio
	 SET 
		 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
		,Id='0293448F-EB51-E811-80CB-00155D020DD3'
		,Funkcio_Id='82D6BF43-EB51-E811-80CB-00155D020DD3'
	 WHERE Id=@record_id
 END
 go

  Print 'FoszamIrattarbolKikeres Funkcio felvitele'
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='C0968FBE-EF51-E811-80CB-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
		,'0'
		,'C0968FBE-EF51-E811-80CB-00155D020DD3'
		,'FoszamIrattarbolKikeres'
		,'Irattárból kikérés (Kölcsönzés)'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
		,Modosithato='0'
		,Id='C0968FBE-EF51-E811-80CB-00155D020DD3'
		,Kod='FoszamIrattarbolKikeres'
		,Nev='Irattárból kikérés (Kölcsönzés)'
	 WHERE Id=@record_id 
 END
go
	
Print 'KRT_Szerepkor_Funkcio - hozzarendeles FEJLESZTO-hoz (FoszamIrattarbolKikeres)'
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='0983A4EE-EF51-E811-80CB-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Szerepkor_Funkcio(
		 Szerepkor_Id
		,Id
		,Funkcio_Id
		) values (
		 'e855f681-36ed-41b6-8413-576fcb5d1542'
		,'0983A4EE-EF51-E811-80CB-00155D020DD3'
		,'C0968FBE-EF51-E811-80CB-00155D020DD3'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Szerepkor_Funkcio
	 SET 
		 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
		,Id='0983A4EE-EF51-E811-80CB-00155D020DD3'
		,Funkcio_Id='C0968FBE-EF51-E811-80CB-00155D020DD3'
	 WHERE Id=@record_id
 END

 go
--  BUG_2159 FoszamIrattarbaVetel, FoszamIrattarbolKikeres Funkciókód Felvitele vége
-- Rendszerparaméter:  JEGYZEK_CSAK_SAJATSZERVEZET_LATHATO
DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege)

		
DECLARE @record_id uniqueidentifier 
SET @record_id = 'F3B69701-CA8F-4C55-9A91-A4D798A640B4'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE ID = @record_id) 
BEGIN 
		Print 'INSERT - JEGYZEK_CSAK_SAJATSZERVEZET_LATHATO' 
		insert into KRT_Parameterek(
			Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) 
		values 
		(
		@record_id
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'JEGYZEK_CSAK_SAJATSZERVEZET_LATHATO'
		,case when (@isTUK = '1') then '1' else '0' end
		,'1'
		,'1 = csak annak a szervezetnek a jegyzékei láthatóak, amellyel a felhasználó bejelentkezett; 0 = minden tétel látszik függetlenül a bejelentkezett felhasználó szervezetétől'
		); 
END

GO
PRINT 'BLG_2167 START'
----------------------------------------------------------
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='F883ECD4-DCD9-4964-8280-943D4599E37C') 
PRINT '@record_id='+convert(NVARCHAR(100),@record_id) 

PRINT 'SCAN_EHITELES_MASOLAT'
if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'F883ECD4-DCD9-4964-8280-943D4599E37C'
		,'SCAN_EHITELES_MASOLAT'
		,'1'
		,'1'
		,'Azonosítós vonalkód kezelés esetén a küldemény vonalkódjába az e-hiteles másolat készítését vezérlő helyiértékre milyen érték kerüljön (pl. 1 - készül másolat, 0 - nem készül másolat)'
		); 
 END 
GO

--------------------------------------------------------
-- BLG_1039
DECLARE @org_kod nvarchar(100) 
print 'PartnerekList.aspx menü'

-- Partnerlista menüpont beszúrása
DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
if ((@org_kod = 'NMHH') AND (@isTUK = '1')) 
BEGIN
	DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Menuk
				where Id='ff3bd8a8-cc7b-48da-9f52-27bcc3d71a41') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
		 Print 'INSERT' 
		 insert into KRT_Menuk(
			 Menu_Id_Szulo
			,Tranz_id
			,Stat_id
			,Id
			,Kod
			,Nev
			,Funkcio_Id
			,Modul_Id
			,Parameter
			,Sorrend
			) values (
			 '19b72b61-0ff3-4239-8d0f-6fb20792d79c'
			,null
			,null
			,'ff3bd8a8-cc7b-48da-9f52-27bcc3d71a41'
			,'eRecord'
			,'Partnerek'
			,'49A585FE-BEBD-48E9-B131-B5B68E01FA94'
			,'1E4A3ECE-DD29-48F3-B81C-4BB63FCF9FE7'		
			,''
			,'83'
			); 
	 END 
END
	 go

--BLG_1039 end
--------------------------------------------------------

--------------------------------------------------------------------
GO
PRINT 'ELARASI SZAKASZ'

DECLARE @record_id uniqueidentifier 
DECLARE @KodCsoport_Id uniqueidentifier
set @KodCsoport_Id = '7FE3437B-D7EC-4170-A3A1-FE07AB35C305'

DECLARE @org nvarchar(100) 
SET @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

SET @record_id = (select Id from KRT_KodCsoportok where Id=@KodCsoport_Id) 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
	insert into KRT_KodCsoportok(
	 Id
	,Kod
	,Nev
	,Modosithato
	,BesorolasiSema
	,Note
	,Tranz_id
	, Stat_id
	) values (
	@KodCsoport_Id
	,'ELJARASI_SZAKASZ_FOK'
	,'Eljárási szakasz'
	,'1'
	,'0'
	,'Eljárási szakaszok kódjai.'
	, null
	, null
	); 

END 

-- ESZ 1
declare @record_id_ESZ_1 UNIQUEIDENTIFIER
SET @record_id_ESZ_1 = '5CDC56D1-F8B5-45E2-AF54-C64830053E8E'

IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_ESZ_1)
BEGIN 
    Print 'INSERT I. fok' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org
    ,@record_id_ESZ_1
    ,'1'
    ,'I. fok'
    ,'001'
    ,'1'
    );        
END 
-- ESZ 2
declare @record_id_ESZ_2 UNIQUEIDENTIFIER
SET @record_id_ESZ_2 = '5CDC56D1-F8B5-45E2-AF54-C64830053E9E'

IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_ESZ_2)
BEGIN 
    Print 'INSERT II. fok ' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org
    ,@record_id_ESZ_2
    ,'2'
    ,'II. fok '
    ,'002'
    ,'1'
    );        
END 
GO
--------------------------------------------------------------------
--------------------------------------------------------------------
  GO

  PRINT 'UGY_JELLEG_HATOSAGI'

  UPDATE KRT_KodTarak
  SET  Egyeb='HATOSAGI'
  WHERE Id='73C2AB4A-D56A-E711-80C2-00155D020D7E' 
	 OR Id='4A566659-D56A-E711-80C2-00155D020D7E'
	 OR Id='2BD24C8F-D56A-E711-80C2-00155D020D7E'
	 OR Id='0D106B98-D56A-E711-80C2-00155D020D7E'
	 OR Id='BBCEB4AF-D56A-E711-80C2-00155D020D7E'
	 OR Id='0BC730BD-D56A-E711-80C2-00155D020D7E'
	 OR Id='102F4FE2-D56A-E711-80C2-00155D020D7E'
	 OR Id='68C2F4F2-D56A-E711-80C2-00155D020D7E'
	 OR Id='12051446-D66A-E711-80C2-00155D020D7E'
	 OR Id='CDC18A51-D66A-E711-80C2-00155D020D7E'
	 OR Id='4BFA877D-D66A-E711-80C2-00155D020D7E'
	 OR Id='E5852EA7-D66A-E711-80C2-00155D020D7E'
	 OR Id='D44E5DC5-D66A-E711-80C2-00155D020D7E'

  UPDATE KRT_KodTarak
  SET  Egyeb='NEM_HATOSAGI'
  WHERE Id='0733E67F-D56A-E711-80C2-00155D020D7E'
     OR Id='8C4CF564-D66A-E711-80C2-00155D020D7E'
	 OR Id='0A35C96A-E166-E711-80C2-00155D020D7E'
	 OR Id='62FC11C1-E266-E711-80C2-00155D020D7E'
	 OR Id='12812786-55E5-44CC-B529-3EB4B00CFEDC'
	 OR Id='801B608C-D66A-E711-80C2-00155D020D7E'
	 OR Id='68A995AF-D66A-E711-80C2-00155D020D7E'
	 OR Id='171827BD-D66A-E711-80C2-00155D020D7E'

--------------------------------------------------------------------
--------------------------------------------------------------------
GO
PRINT 'UGYIRAT_FELFUGGESZTES_OKA'

DECLARE @record_id uniqueidentifier 
DECLARE @KodCsoport_Id uniqueidentifier
set @KodCsoport_Id = 'FDC8D0D4-B6B6-4900-4900-868686868686'

DECLARE @org nvarchar(100) 
SET @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

-- INVALIDATE 
UPDATE KRT_KodTarak
SET ErvVege = GETDATE()
WHERE KOD = 'HIANYPOTLAS' AND KodCsoport_Id = @KodCsoport_Id

-- OK 1
declare @record_id_OK_1 UNIQUEIDENTIFIER
SET @record_id_OK_1 = '45185280-7F5F-4A9C-ADA7-CB5DB53FFAB0'

if NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_OK_1)
BEGIN 
    Print 'BIZONYITASI_ELJARASBAN' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Org
    ,@record_id_OK_1
    ,'BIZONYITASI_ELJARASBAN'
    ,'Bizonyítási eljárásban'
    ,'0'
    );        
END 

-- OK 2
declare @record_id_OK_2 UNIQUEIDENTIFIER
SET @record_id_OK_2 = '45185280-7F5F-4A9C-ADA7-CB5DB53FFAB1'

if NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_OK_2)
BEGIN 
    Print 'ELJARAS_FELFUGGESZTESE' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Org
    ,@record_id_OK_2
    ,'ELJARAS_FELFUGGESZTESE'
    ,'Eljárás felfüggesztése'
    ,'0'
    );        
END 

-- OK 3
declare @record_id_OK_3 UNIQUEIDENTIFIER
SET @record_id_OK_3 = '45185280-7F5F-4A9C-ADA7-CB5DB53FFAB2'

if NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_OK_3)
BEGIN 
    Print 'ADATSZOLGALTATAS' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Org
    ,@record_id_OK_3
    ,'ADATSZOLGALTATAS'
    ,'Adatszolgáltatás'
    ,'0'
    );        
END 

-- OK 4
declare @record_id_OK_4 UNIQUEIDENTIFIER
SET @record_id_OK_4 = '45185280-7F5F-4A9C-ADA7-CB5DB53FFAB3'

if NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_OK_4)
BEGIN 
    Print 'ADATSZOLGALTATAS' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Org
    ,@record_id_OK_4
    ,'KEZBESITESI_KOZLESI_IDOTARTAM'
    ,'Kézbesítési, közlési időtartam'
    ,'0'
    );        
END 
--------------------------------------------------------------------
GO
PRINT 'UGYIRAT_LEZARAS_OKA'

DECLARE @record_id uniqueidentifier 
DECLARE @KodCsoport_Id uniqueidentifier
set @KodCsoport_Id = 'BD00D394-249C-4582-B03E-1CF5B6A1EFE0'

DECLARE @org nvarchar(100) 
SET @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

SET @record_id = (select Id from KRT_KodCsoportok where Id=@KodCsoport_Id) 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
	insert into KRT_KodCsoportok(
	 BesorolasiSema
	,Id
	,Kod
	,Nev
	,Modosithato
	,Note
	) values (
	'0'
	,@KodCsoport_Id
	,'UGYIRAT_LEZARAS_OKA'
	,'Ügyirat lezárás oka'
	,'0'
	,'UGYIRAT_LEZARAS_OKA'
	); 

END 

-- LZ 1
declare @record_id_LZ_1 UNIQUEIDENTIFIER
SET @record_id_LZ_1 = '4793E710-AC38-486D-AEC8-10754FD22A70'

if NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_LZ_1)
BEGIN 
    Print 'ATTETEL' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Org
    ,@record_id_LZ_1
    ,'ATTETEL'
    ,'Áttétel'
    ,'001'
    ,'0'
    );        
END 

-----------------------
declare @record_id_LZ_2 UNIQUEIDENTIFIER
SET @record_id_LZ_2 = '4793E710-AC38-486D-AEC8-10754FD22A71'

if NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_LZ_2)
BEGIN 
    Print 'ELJARAST_MEGSZUNTETO_VEGZES' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Org
    ,@record_id_LZ_2
    ,'ELJARAST_MEGSZUNTETO_VEGZES'
    ,'eljárást megszüntető végzés'
    ,'002'
    ,'0'
    );        
END 
-----------------------
declare @record_id_LZ_3 UNIQUEIDENTIFIER
SET @record_id_LZ_3 = '4793E710-AC38-486D-AEC8-10754FD22A72'

if NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_LZ_3)
BEGIN 
    Print 'ELSO_FOKU_HATAROZAT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Org
    ,@record_id_LZ_3
    ,'ELSO_FOKU_HATAROZAT'
    ,'Első fokú határozat'
    ,'003'
    ,'0'
    );        
END
-----------------------
declare @record_id_LZ_4 UNIQUEIDENTIFIER
SET @record_id_LZ_4 = '4793E710-AC38-486D-AEC8-10754FD22A73'

if NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_LZ_4)
BEGIN 
    Print 'ELSO_FOKU_HATAROZAT_MODOSITASA' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Org
    ,@record_id_LZ_4
    ,'ELSO_FOKU_HATAROZAT_MODOSITASA'
    ,'Első fokú határozat módosítása'
    ,'004'
    ,'0'
    );        
END
-----------------------
declare @record_id_LZ_5 UNIQUEIDENTIFIER
SET @record_id_LZ_5 = '4793E710-AC38-486D-AEC8-10754FD22A74'

if NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_LZ_5)
BEGIN 
    Print 'ELSO_FOKU_HATAROZAT_VISSZAVONASA' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Org
    ,@record_id_LZ_5
    ,'ELSO_FOKU_HATAROZAT_VISSZAVONASA'
    ,'Első fokú határozat visszavonása'
    ,'005'
    ,'0'
    );        
END
-----------------------
declare @record_id_LZ_6 UNIQUEIDENTIFIER
SET @record_id_LZ_6 = '4793E710-AC38-486D-AEC8-10754FD22A75'

if NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_LZ_6)
BEGIN 
    Print 'VIZSGALAT_NELKULI_ELUTASITO_VEGZES' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Org
    ,@record_id_LZ_6
    ,'VIZSGALAT_NELKULI_ELUTASITO_VEGZES'
    ,'Vizsgálat nélküli elutasító végzés'
    ,'006'
    ,'0'
    );        
END


-----------------------
-----------------------
declare @record_id_LZ_7 UNIQUEIDENTIFIER
SET @record_id_LZ_7 = '4793E710-AC38-486D-AEC8-10754FD22A76'

if NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_LZ_7)
BEGIN 
    Print 'MASODFOKU_ELJARAST_MEGSZUNTETO_VEGZES' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Org
    ,@record_id_LZ_7
    ,'MASODFOKU_ELJARAST_MEGSZUNTETO_VEGZES'
    ,'Másodfokú eljárást megszüntető végzés'
    ,'007'
    ,'0'
    );        
END 
-----------------------
declare @record_id_LZ_8 UNIQUEIDENTIFIER
SET @record_id_LZ_8 = '4793E710-AC38-486D-AEC8-10754FD22A77'

if NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_LZ_8)
BEGIN 
    Print 'MASODFOKU_HATAROZAT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Org
    ,@record_id_LZ_8
    ,'MASODFOKU_HATAROZAT'
    ,'Másodfokú határozat'
    ,'008'
    ,'0'
    );        
END
-----------------------
declare @record_id_LZ_9 UNIQUEIDENTIFIER
SET @record_id_LZ_9 = '4793E710-AC38-486D-AEC8-10754FD22A78'

if NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_LZ_9)
BEGIN 
    Print 'MASODFOKU_HATAROZAT_MODOSITASA' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Org
    ,@record_id_LZ_9
    ,'MASODFOKU_HATAROZAT_MODOSITASA'
    ,'Másodfokú határozat módosítása'
    ,'009'
    ,'0'
    );        
END
-----------------------
declare @record_id_LZ_10 UNIQUEIDENTIFIER
SET @record_id_LZ_10 = '4793E710-AC38-486D-AEC8-10754FD22A79'

if NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @record_id_LZ_10)
BEGIN 
    Print 'MASODFOKU_HATAROZAT_VISSZAVONASA' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Org
    ,@record_id_LZ_10
    ,'MASODFOKU_HATAROZAT_VISSZAVONASA'
    ,'Másodfokú határozat visszavonása'
    ,'010'
    ,'0'
    );        
END
--------------------------------------------------------------------
-------------------------------------------------------------
DECLARE @ID UNIQUEIDENTIFIER
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
		where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

If ((@org_kod = 'NMHH')) 
	BEGIN

SET @ID = '6DA36C6F-A300-4563-A6BC-49A4C212A2B3'
if not exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'eRecordComponent/IraIratFormTab.ascx',
			'LabelEljarasLezarasOka',
			'Eljárás lezárásának oka'
		)
	END
	END
GO
--------------------------------------------------------------------
-------------------------------------------------------------
DECLARE @ID UNIQUEIDENTIFIER
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
		where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

If ((@org_kod = 'NMHH')) 
	BEGIN

SET @ID = '6DA36C6F-A300-4563-A6BC-49A4C212A2B2'
if not exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'eRecordComponent/IraIratFormTab.ascx',
			'LabelEljarasFefuggesztesenekOka',
			'Eljárás felfüggesztésének, szünetelésének oka'
		)
	END
	END
GO

-------------------------------------------------------------
DECLARE @ID UNIQUEIDENTIFIER
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
		where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

If ((@org_kod = 'NMHH')) 
	BEGIN

SET @ID = 'BBB2D4CD-AE50-4A54-BA63-88D4D3A22663'
if not exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'eRecordComponent/IraIratFormTab.ascx',
			'LabelIratHatasaUgyintezesre',
			'Irat hatása az ügyintézésre (sakkóra):'
		)
	END
	END
GO

-------------------------------------------------------------
DECLARE @Org UNIQUEIDENTIFIER
SET @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @KodCsoport_Id UNIQUEIDENTIFIER
set @KodCsoport_Id = '8DECBC3D-6466-4918-8FB1-ED0E402E6CD4'

	IF EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = 'B5E6226C-24DA-4D23-8BD5-AB2BF543CD31')
		UPDATE KRT_KodTarak
		SET 
		 Kod='1'
		,Nev='[1] - A hatósági ügyintézési idő indul/folytatódik'
		,Egyeb = '{"tipusok":["ON_PROGRESS"],"elozmenytipus":[],"atmeneteklista":[{"ugyfajta":"NH","atmenetek":["0_ON_PROGRESS","2","3"]},{"ugyfajta":"H1","atmenetek":["0_ON_PROGRESS","2","3"]},{"ugyfajta":"H2","atmenetek":[]}],"felfuggesztesOkai":["BIZONYITASI_ELJARASBAN","ELJARAS_FELFUGGESZTESE","IRAT_FORDITASA","HATÁSKORI_VAGY_ILLETEKESSÉGI_VITA","ADATSZOLGALTATAS","JOGSEGELYELJARAS","KEZBESITESI_KOZLESI_IDOTARTAM","SZAKERTOI_VELEMENY_ELKESZITESE","SZAKHATOSAGI_ELJARAS"],"lezarasOkai":[]}'
		,Modosithato='1'
		WHERE Id='B5E6226C-24DA-4D23-8BD5-AB2BF543CD31' 

	IF EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = '86A5DC9A-C604-4178-AFC7-D70E73868CC1')
		UPDATE KRT_KodTarak
		SET 
		 Kod='2'
		,Nev='[2] - A hatósági ügyintézési idő megáll'
		,Egyeb = '{"tipusok":["PAUSE"],"elozmenytipus":[],"atmeneteklista":[{"ugyfajta":"NH","atmenetek":["0_PAUSE","1","8","9"]},{"ugyfajta":"H1","atmenetek":["0_PAUSE","1","8","9"]},{"ugyfajta":"H2","atmenetek":[]}],"felfuggesztesOkai":["BIZONYITASI_ELJARASBAN","ELJARAS_FELFUGGESZTESE","IRAT_FORDITASA","HATÁSKORI_VAGY_ILLETEKESSÉGI_VITA","ADATSZOLGALTATAS","JOGSEGELYELJARAS","KEZBESITESI_KOZLESI_IDOTARTAM","SZAKERTOI_VELEMENY_ELKESZITESE","SZAKHATOSAGI_ELJARAS"],"lezarasOkai":[]}'
		,Modosithato='1'
		WHERE Id='86A5DC9A-C604-4178-AFC7-D70E73868CC1' 

	IF EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = '8AD1623B-041C-475F-88AE-049A735A5DFA')
		UPDATE KRT_KodTarak
		SET 
		 Kod='3'
		,Nev='[3] - Hatósági ügyintézési idő vége'
		,Egyeb = '{"tipusok":["STOP"],"elozmenytipus":[],"atmeneteklista":[{"ugyfajta":"NH","atmenetek":["0_STOP"]},{"ugyfajta":"H1","atmenetek":["0_STOP","4"]},{"ugyfajta":"H2","atmenetek":["0_STOP","5","10","11"]}],"felfuggesztesOkai":[],"lezarasOkai":["ATTETEL","ELJARAST_MEGSZUNTETO_VEGZES","VIZSGALAT_NELKULI_ELUTASITO_VEGZES","ELSO_FOKU_HATAROZAT","ELSO_FOKU_HATAROZAT_MODOSITASA","ELSO_FOKU_HATAROZAT_VISSZAVONASA"]}'
		,Modosithato='1'
		WHERE Id='8AD1623B-041C-475F-88AE-049A735A5DFA' 

	IF EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = '449A11CD-C83A-4F58-8FCE-B168A2B38635')
		UPDATE KRT_KodTarak
		SET 
		 Kod='4'
		,Nev='[4] - Az elsőfok fellebbezési kérelem alapján módosít/visszavon'
		,Egyeb = '{"tipusok":["ON_PROGRESS"],"elozmenytipus":[],"atmeneteklista":[{"ugyfajta":"NH","atmenetek":["0_ON_PROGRESS","2","3"]},{"ugyfajta":"H1","atmenetek":["0_ON_PROGRESS","2","3"]},{"ugyfajta":"H2","atmenetek":[]}],"felfuggesztesOkai":[],"lezarasOkai":[]}'
		,Modosithato='1'
		WHERE Id='449A11CD-C83A-4F58-8FCE-B168A2B38635' 

	IF EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = '5DF1584A-05C5-4B6C-AD53-DB05444C83E1')
		UPDATE KRT_KodTarak
		SET 
		 Kod='5'
		,Nev='[5] - Másodfokú hatósági ügyintézési idő indul/folytatódik'
 		,Egyeb = '{"tipusok":["ON_PROGRESS"],"elozmenytipus":[],"atmeneteklista":[{"ugyfajta":"NH","atmenetek":["0_ON_PROGRESS"]},{"ugyfajta":"H1","atmenetek":[]},{"ugyfajta":"H2","atmenetek":["0_ON_PROGRESS","6","7"]}],"felfuggesztesOkai":[],"lezarasOkai":[]}'
		,Modosithato='1'
		WHERE Id='5DF1584A-05C5-4B6C-AD53-DB05444C83E1' 

	IF EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = 'C6793F21-62D8-489B-A3AE-235680639037')
		UPDATE KRT_KodTarak
		SET 
		 Kod='6'
		,Nev='[6] - Másodfokú hatósági ügyintézési idő megáll'
		,Egyeb = '{"tipusok":["PAUSE"],"elozmenytipus":[],"atmeneteklista":[{"ugyfajta":"NH","atmenetek":["0_PAUSE"]},{"ugyfajta":"H1","atmenetek":[]},{"ugyfajta":"H2","atmenetek":["0_PAUSE","5","10","11"]}],"felfuggesztesOkai":["BIZONYITASI_ELJARASBAN","ELJARAS_FELFUGGESZTESE","IRAT_FORDITASA","HATÁSKORI_VAGY_ILLETEKESSÉGI_VITA","ADATSZOLGALTATAS","JOGSEGELYELJARAS","KEZBESITESI_KOZLESI_IDOTARTAM","SZAKERTOI_VELEMENY_ELKESZITESE","SZAKHATOSAGI_ELJARAS"],"lezarasOkai":[]}'
		,Modosithato='1'
		WHERE Id='C6793F21-62D8-489B-A3AE-235680639037' 

	IF EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = 'A7CA3503-D543-4879-915C-C38AA55E017B')
		UPDATE KRT_KodTarak
		SET 
		 Kod='7'
		,Nev='[7] - Másodfokú hatósági ügyintézési idő vége'
		,Egyeb = '{"tipusok":["STOP"],"elozmenytipus":[],"atmeneteklista":[{"ugyfajta":"NH","atmenetek":["0_STOP"]},{"ugyfajta":"H1","atmenetek":[]},{"ugyfajta":"H2","atmenetek":["0_STOP"]}],"felfuggesztesOkai":[],"lezarasOkai":["ATTETEL","VIZSGALAT_NELKULI_ELUTASITO_VEGZES","MASODFOKU_ELJARAST_MEGSZUNTETO_VEGZES","MASODFOKU_HATAROZAT","MASODFOKU_HATAROZAT_MODOSITASA","ELSO_FOKU_HATAROZAT_VISSZAVONASA"]}'
		,Modosithato='1'
		WHERE Id='A7CA3503-D543-4879-915C-C38AA55E017B' 

	IF EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = '2A72AAD6-6B55-4DBC-8328-9BC57D2F423D')
		UPDATE KRT_KodTarak
		SET 
		 Kod='8'
		,Nev='[8] - Hatósági ügyintézési idő indul/újraindul és vége'
		,Egyeb = '{"tipusok":["STOP"],"elozmenytipus":[],"atmeneteklista":[{"ugyfajta":"NH","atmenetek":["0_STOP","4","5"]},{"ugyfajta":"H1","atmenetek":["0_STOP","4"]},{"ugyfajta":"H2","atmenetek":[]}],"felfuggesztesOkai":[],"lezarasOkai":["ATTETEL","ELJARAST_MEGSZUNTETO_VEGZES","VIZSGALAT_NELKULI_ELUTASITO_VEGZES","ELSO_FOKU_HATAROZAT","ELSO_FOKU_HATAROZAT_MODOSITASA","ELSO_FOKU_HATAROZAT_VISSZAVONASA"]}'
		,Modosithato='1'
		WHERE Id='2A72AAD6-6B55-4DBC-8328-9BC57D2F423D' 

	IF EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = '5E807261-4F21-4630-81E9-E3CF57F7D80C')
		UPDATE KRT_KodTarak
		SET 
		 Kod='9'
		,Nev='[9] - A hatósági ügyintézési idő indul/újraindul és megáll'
  		,Egyeb = '{"tipusok":["PAUSE"],"elozmenytipus":[],"atmeneteklista":[{"ugyfajta":"NH","atmenetek":["0_PAUSE","1","8"]},{"ugyfajta":"H1","atmenetek":["0_PAUSE","1","8"]},{"ugyfajta":"H2","atmenetek":[]}],"felfuggesztesOkai":["BIZONYITASI_ELJARASBAN","ELJARAS_FELFUGGESZTESE","IRAT_FORDITASA","HATÁSKORI_VAGY_ILLETEKESSÉGI_VITA","ADATSZOLGALTATAS","JOGSEGELYELJARAS","KEZBESITESI_KOZLESI_IDOTARTAM","SZAKERTOI_VELEMENY_ELKESZITESE","SZAKHATOSAGI_ELJARAS"],"lezarasOkai":[]}'
		,Modosithato='1'
		WHERE Id='5E807261-4F21-4630-81E9-E3CF57F7D80C' 

	IF EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = 'FC351F3E-BBDF-4F8D-9A85-103C2E507CA0')
		UPDATE KRT_KodTarak
		SET 
		 Kod='10'
		,Nev='[10] - Másodfokú hatósági ügyintézési idő indul/újraindul és vége'
  		,Egyeb = '{"tipusok":["STOP"],"elozmenytipus":[],"atmeneteklista":[{"ugyfajta":"NH","atmenetek":["0_STOP"]},{"ugyfajta":"H1","atmenetek":[]},{"ugyfajta":"H2","atmenetek":["0_STOP"]}],"felfuggesztesOkai":[],"lezarasOkai":["ATTETEL","VIZSGALAT_NELKULI_ELUTASITO_VEGZES","MASODFOKU_ELJARAST_MEGSZUNTETO_VEGZES","MASODFOKU_HATAROZAT","MASODFOKU_HATAROZAT_MODOSITASA","ELSO_FOKU_HATAROZAT_VISSZAVONASA"]}'
		,Modosithato='1'
		WHERE Id='FC351F3E-BBDF-4F8D-9A85-103C2E507CA0' 

	IF EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = '5926DF13-FE0F-4178-9EE0-3B0C51E041BA')
		UPDATE KRT_KodTarak
		SET 
		 Kod='11'
		,Nev='[11] - Másodfokú hatósági ügyintézési idő indul/újraindul és megáll'
		,Egyeb = '{"tipusok":["PAUSE"],"elozmenytipus":[],"atmeneteklista":[{"ugyfajta":"NH","atmenetek":["0_PAUSE"]},{"ugyfajta":"H1","atmenetek":[]},{"ugyfajta":"H2","atmenetek":["0_PAUSE","5","10"]}],"felfuggesztesOkai":["BIZONYITASI_ELJARASBAN","ELJARAS_FELFUGGESZTESE","IRAT_FORDITASA","HATÁSKORI_VAGY_ILLETEKESSÉGI_VITA","ADATSZOLGALTATAS","JOGSEGELYELJARAS","KEZBESITESI_KOZLESI_IDOTARTAM","SZAKERTOI_VELEMENY_ELKESZITESE","SZAKHATOSAGI_ELJARAS"],"lezarasOkai":[]}'
		,Modosithato='1'
		WHERE Id='5926DF13-FE0F-4178-9EE0-3B0C51E041BA' 

DECLARE @ID_IRAT_HATASA_DEFAULT UNIQUEIDENTIFIER
SET @ID_IRAT_HATASA_DEFAULT = '9F7000CB-4730-4477-937D-6330F75F52AB'
IF EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = @ID_IRAT_HATASA_DEFAULT)
	BEGIN 
		DELETE FROM KRT_KODTARAK WHERE ID = @ID_IRAT_HATASA_DEFAULT
	END 

DECLARE @ID_IRAT_HATASA_0 UNIQUEIDENTIFIER
SET @ID_IRAT_HATASA_0 = 'A970E4FB-106F-45E9-809E-3A921CA18812'
IF EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = @ID_IRAT_HATASA_0)
	BEGIN 
		DELETE FROM KRT_KODTARAK WHERE ID = @ID_IRAT_HATASA_0
	END 

IF EXISTS (select 1 FROM KRT_Kodcsoportok where Id = @kodcsoport_id)
BEGIN
	
	DECLARE @ID_IRAT_HATASA_0_NONE UNIQUEIDENTIFIER
	SET @ID_IRAT_HATASA_0_NONE = '9F7000CB-4730-4477-937D-6330F75F5200'
	IF NOT EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = @ID_IRAT_HATASA_0_NONE)
		BEGIN 
			Print 'INSERT' 
			INSERT INTO KRT_KodTarak(
		 		 Kodcsoport_id
				,Org
				,Id
				,Kod
				,Nev
				,Sorrend
				,Modosithato
				,Egyeb
				) values (
				 @kodcsoport_id
				,@Org
				,@ID_IRAT_HATASA_0_NONE
				,'0_NONE'
				,'[0] - A hatósági ügyintézési időre nincs hatással'
				,'000'
				,'1'
				,
				'{
	  "tipusok": [
		"NONE"
	  ],
	  "elozmenytipus": [
		"NONE"
	  ],
	  "atmeneteklista": [
		{
		  "ugyfajta": "NH",
		  "atmenetek": [
			"0_ON_PROGRESS",
			"1",
			"8",
			"9"
		  ]
		},
		{
		  "ugyfajta": "H1",
		  "atmenetek": [
			"0_ON_PROGRESS",
			"1",
			"8",
			"9"
		  ]
		},
		{
		  "ugyfajta": "H2",
		  "atmenetek": [
			"0_ON_PROGRESS",
			"5",
			"10",
			"11"
		  ]
		}
	  ],
	  "felfuggesztesOkai": [],
	  "lezarasOkai": []
	}'
				); 
		END 
	ELSE
		UPDATE KRT_KodTarak
			SET 
			KodCsoport_Id=@KodCsoport_Id
			,Kod='0_NONE'
			,Nev='[0] - A hatósági ügyintézési időre nincs hatással'
			,Egyeb = '{
	  "tipusok": [
		"NONE"
	  ],
	  "elozmenytipus": [
		"NONE"
	  ],
	  "atmeneteklista": [
		{
		  "ugyfajta": "NH",
		  "atmenetek": [
			"0_ON_PROGRESS",
			"1",
			"8",
			"9"
		  ]
		},
		{
		  "ugyfajta": "H1",
		  "atmenetek": [
			"0_ON_PROGRESS",
			"1",
			"8",
			"9"
		  ]
		},
		{
		  "ugyfajta": "H2",
		  "atmenetek": [
			"0_ON_PROGRESS",
			"5",
			"10",
			"11"
		  ]
		}
	  ],
	  "felfuggesztesOkai": [],
	  "lezarasOkai": []
	}'
			,Modosithato='1'
			WHERE Id=@ID_IRAT_HATASA_0_NONE


	DECLARE @ID_IRAT_HATASA_0_START UNIQUEIDENTIFIER
	SET @ID_IRAT_HATASA_0_START = '9F7000CB-4730-4477-937D-6330F75F5201'
	IF NOT EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = @ID_IRAT_HATASA_0_START)
		BEGIN 
			Print 'INSERT' 
			INSERT INTO KRT_KodTarak(
		 		 Kodcsoport_id
				,Org
				,Id
				,Kod
				,Nev
				,Sorrend
				,Modosithato
				,Egyeb
				) values (
				 @kodcsoport_id
				,@Org
				,@ID_IRAT_HATASA_0_START
				,'0_ON_PROGRESS'
				,'[0] - A hatósági ügyintézési időre nincs hatással'
				,'000'
				,'1'
				,'{
	  "tipusok": [
		"NONE"
	  ],
	  "elozmenytipus": [
		"ON_PROGRESS"
	  ],
	  "atmeneteklista": [
		{
		  "ugyfajta": "NH",
		  "atmenetek": []
		},
		{
		  "ugyfajta": "H1",
		  "atmenetek": [
			"0_ON_PROGRESS",
			"2",
			"3"
		  ]
		},
		{
		  "ugyfajta": "H2",
		  "atmenetek": [
			"0_ON_PROGRESS",
			"0_NONE",
			"0_PAUSE",
			"0_STOP",
			"6",
			"7"
		  ]
		}
	  ],
	  "felfuggesztesOkai": [],
	  "lezarasOkai": []
	}'
				); 
		END 
	ELSE
		UPDATE KRT_KodTarak
			SET 
			KodCsoport_Id=@KodCsoport_Id
			,Kod='0_ON_PROGRESS'
			,Nev='[0] - A hatósági ügyintézési időre nincs hatással'
			,Egyeb = '{
	  "tipusok": [
		"NONE"
	  ],
	  "elozmenytipus": [
		"ON_PROGRESS"
	  ],
	  "atmeneteklista": [
		{
		  "ugyfajta": "NH",
		  "atmenetek": []
		},
		{
		  "ugyfajta": "H1",
		  "atmenetek": [
			"0_ON_PROGRESS",
			"2",
			"3"
		  ]
		},
		{
		  "ugyfajta": "H2",
		  "atmenetek": [
			"0_ON_PROGRESS",
			"0_NONE",
			"0_PAUSE",
			"0_STOP",
			"6",
			"7"
		  ]
		}
	  ],
	  "felfuggesztesOkai": [],
	  "lezarasOkai": []
	}'
			,Modosithato='1'
			WHERE Id=@ID_IRAT_HATASA_0_START

	DECLARE @ID_IRAT_HATASA_0_PAUSE UNIQUEIDENTIFIER
	SET @ID_IRAT_HATASA_0_PAUSE = '9F7000CB-4730-4477-937D-6330F75F5202'
	IF NOT EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = @ID_IRAT_HATASA_0_PAUSE)
		BEGIN 
			Print 'INSERT' 
			INSERT INTO KRT_KodTarak(
		 		 Kodcsoport_id
				,Org
				,Id
				,Kod
				,Nev
				,Sorrend
				,Modosithato
				,Egyeb
				) values (
				 @kodcsoport_id
				,@Org
				,@ID_IRAT_HATASA_0_PAUSE
				,'0_PAUSE'
				,'[0] - A hatósági ügyintézési időre nincs hatással'
				,'000'
				,'1'
				,'{"tipusok":["NONE"],"elozmenytipus":["PAUSE"],"atmeneteklista":[{"ugyfajta":"NH","atmenetek":["0_PAUSE","1","8","9"]},{"ugyfajta":"H1","atmenetek":[]},{"ugyfajta":"H2","atmenetek":["0_PAUSE","7","10"]}],"felfuggesztesOkai":[],"lezarasOkai":[]}'
				); 
		END 
	ELSE
		UPDATE KRT_KodTarak
			SET 
			KodCsoport_Id=@KodCsoport_Id
			,Kod='0_PAUSE'
			,Nev='[0] - A hatósági ügyintézési időre nincs hatással'
			,Egyeb = '{"tipusok":["NONE"],"elozmenytipus":["PAUSE"],"atmeneteklista":[{"ugyfajta":"NH","atmenetek":["0_PAUSE","1","8","9"]},{"ugyfajta":"H1","atmenetek":[]},{"ugyfajta":"H2","atmenetek":["0_PAUSE","7","10"]}],"felfuggesztesOkai":[],"lezarasOkai":[]}'
			,Modosithato='1'
			WHERE Id=@ID_IRAT_HATASA_0_PAUSE

	DECLARE @ID_IRAT_HATASA_0_STOP UNIQUEIDENTIFIER
	SET @ID_IRAT_HATASA_0_STOP = '9F7000CB-4730-4477-937D-6330F75F5203'
	IF NOT EXISTS(SELECT 1 FROM KRT_KODTARAK WHERE ID = @ID_IRAT_HATASA_0_STOP)
		BEGIN 
			Print 'INSERT' 
			INSERT INTO KRT_KodTarak(
		 		 Kodcsoport_id
				,Org
				,Id
				,Kod
				,Nev
				,Sorrend
				,Modosithato
				,Egyeb
				) values (
				 @kodcsoport_id
				,@Org
				,@ID_IRAT_HATASA_0_STOP
				,'0_STOP'
				,'[0] - A hatósági ügyintézési időre nincs hatással'
				,'000'
				,'1'
				,'{"tipusok":["NONE"],"elozmenytipus":["STOP"],"atmeneteklista":[{"ugyfajta":"NH","atmenetek":["0_STOP","1","4","5"]},{"ugyfajta":"H1","atmenetek":[]},{"ugyfajta":"H2","atmenetek":["0_STOP","1"]}],"felfuggesztesOkai":[],"lezarasOkai":[]}'
				); 
		END 
	ELSE
		UPDATE KRT_KodTarak
			SET 
			KodCsoport_Id=@KodCsoport_Id
			,Kod='0_STOP'
			,Nev='[0] - A hatósági ügyintézési időre nincs hatással'
			,Egyeb = '{"tipusok":["NONE"],"elozmenytipus":["STOP"],"atmeneteklista":[{"ugyfajta":"NH","atmenetek":["0_STOP","1","4","5"]},{"ugyfajta":"H1","atmenetek":[]},{"ugyfajta":"H2","atmenetek":["0_STOP","1"]}],"felfuggesztesOkai":[],"lezarasOkai":[]}'
			,Modosithato='1'
			WHERE Id=@ID_IRAT_HATASA_0_STOP

	END
GO
--------------------------------------------------------------------
PRINT 'INVALIDATE ODAP UGY FAJTA'
UPDATE KRT_KODTARAK
SET ERVVEGE = '2018.05.14'
WHERE KODCSOPORT_ID = '72554FDB-8323-4110-B7B2-65576AE3204A'
AND KOD IN ('19','20','21')
--------------------------------------------------------------------






print 'UgyUgyiratokCalendarView.aspx modul'

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Modulok
			where Id='49C6041C-70F0-45A8-9E27-4A1E159A1285') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
         Print 'INSERT - UgyUgyiratokCalendarView.aspx' 

		 insert into KRT_Modulok(
			 Tranz_id
			,Stat_id
			,Id
			,Tipus
			,Kod
			,Nev
			,Leiras
			) values (
			 'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'49C6041C-70F0-45A8-9E27-4A1E159A1285'
			,'F'
			,'UgyUgyiratokCalendarView.aspx'
			,'Határidő figyelő'
			,null
			); 
	 END 	
	 go
	 

	 
Print 'Határidő figyelése menü'
	 
	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Menuk
			where Id='92CDFB6F-BA6B-4187-8641-9AE16063129C') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
         Print 'INSERT - Határidő figyelése' 

		 insert into KRT_Menuk(
			 Menu_Id_Szulo
			,Tranz_id
			,Stat_id
			,Id
			,Kod
			,Nev
			,Modul_Id
			,Sorrend		
			,Funkcio_Id	
			) values (
			 'EE9EA307-ED15-4FE6-B323-A12D2E06AC6D'
			,'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'92CDFB6F-BA6B-4187-8641-9AE16063129C'
			,'eRecord'
			,'Határidő figyelése'
			,'49C6041C-70F0-45A8-9E27-4A1E159A1285'
			,'38'
			, NULL -- Tömeges iktatás funkciójog
			); 
		 
			 
	 END 

GO
--------------------------------------------------------
-- BLG_2521 eRecord/Partnerek átnevezés

print 'TÜK Felhasználók menü'

DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

-- Partnerlista menüpont beszúrása
DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 

if ((@org_kod = 'NMHH') AND (@isTUK = '1')) 
BEGIN

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Menuk
			where Id='ff3bd8a8-cc7b-48da-9f52-27bcc3d71a41') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		 Print 'INSERT' 
		 insert into KRT_Menuk(
			 Menu_Id_Szulo
			,Tranz_id
			,Stat_id
			,Id
			,Kod
			,Nev
			,Funkcio_Id
			,Modul_Id
			,Parameter
			,Sorrend
			) values (
			 '19b72b61-0ff3-4239-8d0f-6fb20792d79c'
			,null
			,null
			,'ff3bd8a8-cc7b-48da-9f52-27bcc3d71a41'
			,'eRecord'
			,'TÜK Felhasználók'
			,'49A585FE-BEBD-48E9-B131-B5B68E01FA94'
			,'1E4A3ECE-DD29-48F3-B81C-4BB63FCF9FE7'		
			,''
			,'83'
			); 
	 END 
	 ELSE
	 BEGIN
	 Print 'UPDATE'		
		 UPDATE KRT_Menuk
		 SET 
			 Menu_Id_Szulo='19b72b61-0ff3-4239-8d0f-6fb20792d79c'
			,Id='ff3bd8a8-cc7b-48da-9f52-27bcc3d71a41'
			,Kod='eRecord'
			,Nev='TÜK Felhasználók'
			,Funkcio_Id='49A585FE-BEBD-48E9-B131-B5B68E01FA94'
			,Modul_Id='1E4A3ECE-DD29-48F3-B81C-4BB63FCF9FE7'	
			,Parameter=null
			,Sorrend='83'
		 WHERE Id=@record_id 
	 END
END

GO

--BLG_2521 end

-- BUG_2196 Számlázás interface beállítás NMHH-ban
-- Megvalósítva BLG_1500
DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

--NMHH-ban PIR_Interface_Enabled = 1, PIR_Rendszer = 'PROCUSYS' 
if (@org_kod = 'NMHH') 
BEGIN
	Print 'PIR_Rendszer = PROCUSYS'		
	DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
				where Id='e788153a-3620-e711-80bf-00155d027e51') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 

	if @record_id IS NULL 
	BEGIN 
		Print 'INSERT' 
		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Id
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 null
			,null
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'e788153a-3620-e711-80bf-00155d027e51'
			,'PIR_RENDSZER'
			,'PROCUSYS'
			,'0'
			,'A paraméter értéke megadja, hogy milyen számlázási interface-en keresztül történik a számlaadatok átadása'
			); 
	 END 
	 --ELSE 
	 --BEGIN 
		-- Print 'UPDATE'
		-- UPDATE KRT_Parameterek
		-- SET 
		--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		--	,Id='e788153a-3620-e711-80bf-00155d027e51'
		--	,Nev='PIR_RENDSZER'
		--	,Ertek='PROCUSYS'
		--	,Karbantarthato='0'
		--	,Note='A paraméter értéke megadja, hogy milyen számlázási interface-en keresztül történik a számlaadatok átadása'
		-- WHERE Id=@record_id 
	 --END
	 
	Print 'PIR_INTERFACE_ENABLED = 1'		
	DECLARE @record_id2 uniqueidentifier SET @record_id2 = (select Id from KRT_Parameterek
				where Id='5a7d4177-0368-4e44-873c-98237956a434') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 

	if @record_id2 IS NULL 
	BEGIN 
		Print 'INSERT' 
		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Id
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 null
			,null
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'5a7d4177-0368-4e44-873c-98237956a434'
			,'PIR_INTERFACE_ENABLED'
			,'1'
			,'1'
			,'Az Org-specifikus PirEdokInterface (számlakezelés) használatának engedélyezése vagy letiltása. Érték(1 - igen)'
			); 
	 END 
	 --ELSE 
	 --BEGIN 
		-- Print 'UPDATE'
		-- UPDATE KRT_Parameterek
		-- SET 
		--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		--	,Id='5a7d4177-0368-4e44-873c-98237956a434'
		--	,Nev='PIR_INTERFACE_ENABLED'
		--	,Ertek='1'
		--	,Karbantarthato='1'
		--	,Note='Az Org-specifikus PirEdokInterface (számlakezelés) használatának engedélyezése vagy letiltása. Érték(1 - igen)'
		-- WHERE Id=@record_id2 
	 --END
 END
GO
-- BUG_2196 (BUG_2598) end
--------------------------------------------------------

------------------------------------------------------------------------------------------------------------------
PRINT 'ALAIROK_CSATOLMANY_NELKUL rendszerparaméter'

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @record_id uniqueidentifier
SET @record_id = 'EEF5C3A7-D382-424F-BC41-556C14F8EC23'

declare @Ertek nvarchar(400)

-- Default
set @Ertek = '3' --az utólagosan adminisztrált aláírás típus esetén lehet csak aláírót feltölteni

IF not exists (select 1 from KRT_Parameterek where Id = @record_id)
BEGIN 
    PRINT 'INSERT - ALAIROK_CSATOLMANY_NELKUL - ' + @Ertek
     INSERT INTO KRT_Parameterek(
         Id
        ,Org
        ,Nev
        ,Ertek
        ,Karbantarthato
        ,Note
        ) VALUES (
        @record_id
        ,'450b510a-7caa-46b0-83e3-18445c0c53a9'
        ,'ALAIROK_CSATOLMANY_NELKUL'
        ,@Ertek
        ,'1'
        ,'Aláírók felvétele csatolmány nélkül. Értékek: 1 - csatolmány feltöltés nélkül lehet aláírót rögzíteni, 2 - egyetlen aláírás típussal sem lehet aláírót rögzíteni, 3 - az utólagosan adminisztrált aláírás típus esetén lehet csak aláírót feltölteni'
    ); 
END

GO

--AUTO_EUZENET_KIKULDES_KIADMANYOZAS_UTAN-------------------------------------------------------------
PRINT 'AUTO_EUZENET_KIKULDES_KIADMANYOZAS_UTAN rendszerparaméter'

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @record_id uniqueidentifier
SET @record_id = '9785237A-A9CD-43C1-9A4A-5591701F8621'

declare @Ertek nvarchar(400)
-- Default
set @Ertek = '0' --nincs automatikus kiküldés

IF not exists (select 1 from KRT_Parameterek where Id = @record_id)
BEGIN 
    PRINT 'INSERT - AUTO_EUZENET_KIKULDES_KIADMANYOZAS_UTAN - ' + @Ertek
     INSERT INTO KRT_Parameterek(
         Id
        ,Org
        ,Nev
        ,Ertek
        ,Karbantarthato
        ,Note
        ) VALUES (
        @record_id
        ,'450b510a-7caa-46b0-83e3-18445c0c53a9'
        ,'AUTO_EUZENET_KIKULDES_KIADMANYOZAS_UTAN'
        ,@Ertek
        ,'1'
        ,'0 - nincs automatikus kiküldés, 1 - amennyiben teljesülnek az expediálás és postázás feltételei akkor a kiadmányozás után automatikusan lefutnak a folyamatok'
    ); 
END

GO

---------------------------------------------------------------------------------------------------------
PRINT '------START----- 2.1.0.29'

print ''
print '---'
print '/* ----- Levéltári átadás #1674 ----- */'
print '/* --- KRT_Parameterek tábla ''LEVELTARI_ATADAS_*'' paraméterek beszúrása/módosítása --- */'
print '/* Kezdete: ' + convert( nvarchar(30), sysdatetime() ) + ' */'
print '---'

BEGIN
  print ''
  print '- Ellenőrzések -'

  declare cur_ORG CURSOR FOR
                     select ID, Kod, Nev from KRT_Orgok;
  declare @Org_ID  uniqueidentifier
         ,@Org_Kod nvarchar(100)
		 ,@Org_Nev nvarchar(100)
		 ;

  declare @AxisUser uniqueidentifier;
  --- SET @AdminUser = convert(uniqueidentifier,'54E861A5-36ED-44CA-BAA7-C287D125B309')
  --- SET @AxisUser = convert(uniqueidentifier,'838DB5BB-B8C6-E611-90F7-0050569A6FBA')
  declare @n_count    int = 0
         ,@n_InsertOK int = 0
		 ,@n_UpdateOK int = 0
		;

  declare @ResultUID uniqueidentifier;
  declare @xmlInsert xml
         ,@xmlUpdate xml
		 ,@ExecTime  datetime
		 ,@ActVer    int
		 ,@recordID  uniqueidentifier
		;
  SET @xmlInsert=convert(xml,N'<root><Nev/><Ertek/><Karbantarthato/><Note/><Ver/><Letrehozo_ID/><LetrehozasIdo/></root>');
  SET @xmlUpdate=convert(xml,N'<root><Karbantarthato/><Ver/><Modosito_ID/><ModositasIdo/></root>');  
  SET @ExecTime = getdate()                              --- mert egyébként nem tudom átadni paraméterként !?

  declare 
          @Atado_Rovidneve        nvarchar(400)
		 ,@Atadas_Dokumentum      nvarchar(400)
		 ,@Atadas_Iktato          nvarchar(400)
		 ,@Atadas_Tipus           nvarchar(400) 
		 ,@Iratkepzo_Neve         nvarchar(400)
		 ,@Iratkepzo_Azonositoja  nvarchar(400)
         ,@Leveltar_Neve          nvarchar(400)
         ,@Leveltar_Azonositoja   nvarchar(400)
         ;
  					 
  open cur_ORG;
  fetch next from cur_ORG into @Org_ID, @Org_Kod, @Org_Nev;
  WHILE @@FETCH_STATUS = 0  
  BEGIN 
    SET @n_count = 0;
    SET @n_InsertOK = 0;
	SET @n_UpdateOK =0;
					 
    SET @AxisUser = convert(uniqueidentifier, Null);					 
    SET @AxisUser = ( select ID from KRT_Felhasznalok f
                       where f.Nev collate database_default = 'Adminisztrátor' collate database_default
                         and f.Org = @Org_Id );
    if @AxisUser is Null
      SET @AxisUser = ( select ID from KRT_Felhasznalok f
                         where Nev collate database_default = 'Axis felhasználó' collate database_default
						   and f.Org = @Org_Id );
    if @AxisUser is Null 
	  begin
	    print '';
	    print 'Axis felhasználó nem definiált - a ''Levéltári átadás'' paraméterei nem kerülnek felvételre/módosításra ( Org: ' + @Org_Kod + ' ).';
	    --- RETURN;     /* ORG ciklusban vagyunk - nem hagyhatunk el minden ágat */        --- Itt kiszáll - !!! de csak az adott BATCH-ből
	  end;
    else 
	  begin
		print '';
        print '- LEVELTARI_ATADAS_* paraméterek INSERT/UPDATE ( Org: ' +@Org_Kod + ' ) -';

        /* ??? PARAMETEREK ???  */	  
        SET @Atado_Rovidneve        = ISNULL(@Org_Kod, N'<Iratképző rövidneve>');                      --- 'FPH'
        SET @Atadas_Dokumentum      = N'<Átvételi csomag típusát leíró dokumentum címe>;<elérhetősége>';
        SET @Atadas_Iktato          = N'<Átadás-átvételi megállapodás iktatószáma>;<dátuma>';
        SET @Atadas_Tipus           = N'TANÚSÍTOTT IKTATÁS';
        SET @Iratkepzo_Neve         = ISNULL(@Org_Nev, N'<Iratképző megnevezése>');                      --- 'Főpolgármesteri hivatal'
        SET @Iratkepzo_Azonositoja  = N'<Iratképző azonosítója>';                    --- '1357924680'      
        SET @Leveltar_Neve          = N'Magyar Nemzeti Levéltár';
        SET @Leveltar_Azonositoja   = N'HU-MNLOL';                                   --- <Levéltár azonosítója>';                     --- '0864297531'
        /* - */

---
        SET @n_count +=1
		SET @recordID = ( select top 1 p.ID from [KRT_Parameterek] p 
	                       where p.Nev = 'LEVELTARI_ATADAS_IRATKEPZO_ROVIDNEVE'
			                 and p.Org = @Org_Id )                                --- ha nem találja, akkor NULL kerül bele
        if @recordID is NULL
          begin try
            exec [sp_KRT_ParameterekInsert] 
               @Nev   = 'LEVELTARI_ATADAS_IRATKEPZO_ROVIDNEVE'
	          ,@Ertek = @Atado_Rovidneve
              ,@Karbantarthato = 1               
	          ,@Note = 'Az átadó elnevezéséből képzett, legfeljebb húsz, ékezet nélküli nagybetűs karakterből álló egyedi azonosító'
              ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
              ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
              ,@LetrehozasIdo  = @ExecTime
              ,@UpdatedColumns = @xmlInsert
	          ,@ResultUID = @ResultUID
	          ;
            SET @n_InsertOK += 1; 
          end try
		  begin catch
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_IRATKEPZO_ROVIDNEVE'' insert): ' + ERROR_MESSAGE();			
          end catch;
		else  
          begin try
		    SET @ActVer = ( select p.Ver from [KRT_Parameterek] p 
	                         where p.ID = @recordID );
            exec [sp_KRT_ParameterekUpdate] 
			   @ID = @recordID
			  ,@ExecutorUserID = @AxisUser
			  ,@ExecutionTime = @ExecTime
              ,@Karbantarthato = 1               
              ,@Ver = @ActVer
              ,@Modosito_Id   = @AxisUser
              ,@ModositasIdo  = @ExecTime
              ,@UpdatedColumns = @xmlUpdate
	          ;
            SET @n_UpdateOK += 1; 
          end try
		  begin catch
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_IRATKEPZO_ROVIDNEVE'' update): ' + ERROR_MESSAGE();			
          end catch;

---
        SET @n_count +=1
		SET @recordID = ( select top 1 p.ID from [KRT_Parameterek] p 
	                       where p.Nev = 'LEVELTARI_ATADAS_TIPUS'
			                 and p.Org = @Org_Id )
        if @recordID is NULL
          begin try
            exec [sp_KRT_ParameterekInsert] 
               @Nev   = 'LEVELTARI_ATADAS_TIPUS'
	          ,@Ertek = @Atadas_Tipus
              ,@Karbantarthato = 0                                    --- NEM karbantartható; általunk megadott fix érték: N'TANÚSÍTOTT IKTATÁS'
	          ,@Note = 'Az elektronikus átvételi csomag átadási típusa: „tanúsított iratkezelési szoftver”, amelynek reprezentációja „TANÚSÍTOTT IKTATÁS”;... '
              ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
              ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
              ,@LetrehozasIdo  = @ExecTime
              ,@UpdatedColumns = @xmlInsert
	          ,@ResultUID = @ResultUID
	          ;
            SET @n_InsertOK += 1; 
          end try
		  begin catch
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_TIPUS'' insert): ' + ERROR_MESSAGE();			
          end catch;
		else  
          begin try
		    SET @ActVer = ( select p.Ver from [KRT_Parameterek] p 
	                         where p.ID = @recordID );
            exec [sp_KRT_ParameterekUpdate] 
			   @ID = @recordID
			  ,@ExecutorUserID = @AxisUser
			  ,@ExecutionTime = @ExecTime
              ,@Karbantarthato = 0                                    --- NEM karbantartható       
              ,@Ver = @ActVer
              ,@Modosito_Id   = @AxisUser
              ,@ModositasIdo  = @ExecTime
              ,@UpdatedColumns = @xmlUpdate
	          ;
            SET @n_UpdateOK += 1; 
          end try
		  begin catch
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_TIPUS'' update): ' + ERROR_MESSAGE();			
          end catch;

---
        SET @n_count +=1
		SET @recordID = ( select top 1 p.ID from [KRT_Parameterek] p 
	                       where p.Nev = 'LEVELTARI_ATADAS_DOKUMENTUM'
			                 and p.Org = @Org_Id )
        if @recordID is NULL
          begin try
            exec [sp_KRT_ParameterekInsert] 
               @Nev   = 'LEVELTARI_ATADAS_DOKUMENTUM'
	          ,@Ertek = @Atadas_Dokumentum
              ,@Karbantarthato = 1
	          ,@Note = 'Az elektronikus átvételi csomag átvételi típusát részletesen meghatározó dokumentum címe és elérhetősége.'
              ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
              ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
              ,@LetrehozasIdo  = @ExecTime
              ,@UpdatedColumns = @xmlInsert
	          ,@ResultUID = @ResultUID
	          ;
            SET @n_InsertOK += 1; 
          end try
		  begin catch
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_DOKUMENTUM'' insert): ' + ERROR_MESSAGE();			
          end catch;
		else  
          begin try
		    SET @ActVer = ( select p.Ver from [KRT_Parameterek] p 
	                         where p.ID = @recordID );
            exec [sp_KRT_ParameterekUpdate] 
			   @ID = @recordID
			  ,@ExecutorUserID = @AxisUser
			  ,@ExecutionTime = @ExecTime
              ,@Karbantarthato = 1
              ,@Ver = @ActVer
              ,@Modosito_Id   = @AxisUser
              ,@ModositasIdo  = @ExecTime
              ,@UpdatedColumns = @xmlUpdate
	          ;
            SET @n_UpdateOK += 1; 
          end try
		  begin catch
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_DOKUMENTUM'' update): ' + ERROR_MESSAGE();			
          end catch;

---
        SET @n_count +=1
		SET @recordID = ( select top 1 p.ID from [KRT_Parameterek] p 
	                       where p.Nev = 'LEVELTARI_ATADAS_IKTATO'
			                 and p.Org = @Org_Id )
        if @recordID is NULL
          begin try
            exec [sp_KRT_ParameterekInsert] 
               @Nev   = 'LEVELTARI_ATADAS_IKTATO'
	          ,@Ertek = @Atadas_Iktato
              ,@Karbantarthato = 1
	          ,@Note = 'Az iratátadó és a levéltár között létrejött levéltári átadás-átvételi megállapodás iktatószáma és dátuma. A két adatot pontosvesszővel kell elválasztani.'
              ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
              ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
              ,@LetrehozasIdo  = @ExecTime
              ,@UpdatedColumns = @xmlInsert
	          ,@ResultUID = @ResultUID
	          ;
            SET @n_InsertOK += 1; 
          end try
		  begin catch
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_IKTATO'' insert): ' + ERROR_MESSAGE();			
          end catch;
		else  
          begin try
		    SET @ActVer = ( select p.Ver from [KRT_Parameterek] p 
	                         where p.ID = @recordID );
            exec [sp_KRT_ParameterekUpdate] 
			   @ID = @recordID
			  ,@ExecutorUserID = @AxisUser
			  ,@ExecutionTime = @ExecTime
              ,@Karbantarthato = 1
              ,@Ver = @ActVer
              ,@Modosito_Id   = @AxisUser
              ,@ModositasIdo  = @ExecTime
              ,@UpdatedColumns = @xmlUpdate
	          ;
            SET @n_UpdateOK += 1; 
          end try
		  begin catch
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_IKTATO'' update): ' + ERROR_MESSAGE();			
          end catch;

---
        SET @n_count +=1
		SET @recordID = ( select top 1 p.ID from [KRT_Parameterek] p 
	                       where p.Nev = 'LEVELTARI_ATADAS_IRATKEPZO_NEVE'
			                 and p.Org = @Org_Id )
        if @recordID is NULL
          begin try
            exec [sp_KRT_ParameterekInsert] 
               @Nev   = 'LEVELTARI_ATADAS_IRATKEPZO_NEVE'
	          ,@Ertek = @Iratkepzo_Neve
              ,@Karbantarthato = 1
	          ,@Note = 'Az elektronikus átvételi csomagban szereplő iratokat keletkeztető szervezet pontos neve.'
              ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
              ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
              ,@LetrehozasIdo  = @ExecTime
              ,@UpdatedColumns = @xmlInsert
	          ,@ResultUID = @ResultUID
	          ;
            SET @n_InsertOK += 1; 
          end try
		  begin catch
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_IRATKEPZO_NEVE'' insert): ' + ERROR_MESSAGE();			
          end catch;
		else  
          begin try
		    SET @ActVer = ( select p.Ver from [KRT_Parameterek] p 
	                         where p.ID = @recordID );
            exec [sp_KRT_ParameterekUpdate] 
			   @ID = @recordID
			  ,@ExecutorUserID = @AxisUser
			  ,@ExecutionTime = @ExecTime
              ,@Karbantarthato = 1
              ,@Ver = @ActVer
              ,@Modosito_Id   = @AxisUser
              ,@ModositasIdo  = @ExecTime
              ,@UpdatedColumns = @xmlUpdate
	          ;
            SET @n_UpdateOK += 1; 
          end try
		  begin catch
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_IRATKEPZO_NEVE'' update): ' + ERROR_MESSAGE();			
          end catch;

---
        SET @n_count +=1
		SET @recordID = ( select top 1 p.ID from [KRT_Parameterek] p 
	                       where p.Nev = 'LEVELTARI_ATADAS_IRATKEPZO_AZONOSITOJA'
			                 and p.Org = @Org_Id )
        if @recordID is NULL
          begin try
            exec [sp_KRT_ParameterekInsert] 
               @Nev   = 'LEVELTARI_ATADAS_IRATKEPZO_AZONOSITOJA'
	          ,@Ertek = @Iratkepzo_Azonositoja
              ,@Karbantarthato = 1
	          ,@Note = 'Az elektronikus átvételi csomagban szereplő iratokat keletkeztető szervezet egyedi azonosítója.'
              ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
              ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
              ,@LetrehozasIdo  = @ExecTime
              ,@UpdatedColumns = @xmlInsert
	          ,@ResultUID = @ResultUID
	          ;
            SET @n_InsertOK += 1; 
          end try
		  begin catch
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_IRATKEPZO_AZONOSITOJA'' insert): ' + ERROR_MESSAGE();			
          end catch;
		else  
          begin try
		    SET @ActVer = ( select p.Ver from [KRT_Parameterek] p 
	                         where p.ID = @recordID );
            exec [sp_KRT_ParameterekUpdate] 
			   @ID = @recordID
			  ,@ExecutorUserID = @AxisUser
			  ,@ExecutionTime = @ExecTime
              ,@Karbantarthato = 1
              ,@Ver = @ActVer
              ,@Modosito_Id   = @AxisUser
              ,@ModositasIdo  = @ExecTime
              ,@UpdatedColumns = @xmlUpdate
	          ;
            SET @n_UpdateOK += 1; 
          end try
		  begin catch
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_IRATKEPZO_AZONOSITOJA'' update): ' + ERROR_MESSAGE();			
          end catch;

---
        SET @n_count +=1
		SET @recordID = ( select top 1 p.ID from [KRT_Parameterek] p 
	                       where p.Nev = 'LEVELTARI_ATADAS_LEVELTAR_NEVE'
			                 and p.Org = @Org_Id )
        if @recordID is NULL
          begin try
            exec [sp_KRT_ParameterekInsert] 
               @Nev   = 'LEVELTARI_ATADAS_LEVELTAR_NEVE'
	          ,@Ertek = @Leveltar_Neve
              ,@Karbantarthato = 1
	          ,@Note = 'Annak a levéltári szervezetnek a neve, amely az elektronikus átvételi csomag befogadását végzi.'
              ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
              ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
              ,@LetrehozasIdo  = @ExecTime
              ,@UpdatedColumns = @xmlInsert
	          ,@ResultUID = @ResultUID
	          ;
            SET @n_InsertOK += 1; 
          end try
		  begin catch
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_LEVELTAR_NEVE'' insert): ' + ERROR_MESSAGE();			
          end catch;
		else  
          begin try
		    SET @ActVer = ( select p.Ver from [KRT_Parameterek] p 
	                         where p.ID = @recordID );
            exec [sp_KRT_ParameterekUpdate] 
			   @ID = @recordID
			  ,@ExecutorUserID = @AxisUser
			  ,@ExecutionTime = @ExecTime
              ,@Karbantarthato = 1
              ,@Ver = @ActVer
              ,@Modosito_Id   = @AxisUser
              ,@ModositasIdo  = @ExecTime
              ,@UpdatedColumns = @xmlUpdate
	          ;

            SET @n_UpdateOK += 1; 
          end try
		  begin catch
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_LEVELTAR_NEVE'' update): ' + ERROR_MESSAGE();			
          end catch;

---
        SET @n_count +=1		
        SET @recordID = ( select top 1 p.ID from [KRT_Parameterek] p 
	                       where p.Nev = 'LEVELTARI_ATADAS_LEVELTAR_AZONOSITOJA'
		                     and p.Org = @Org_Id )
        if @recordID is Null
          begin try
            exec [sp_KRT_ParameterekInsert] 
                 @Nev   = 'LEVELTARI_ATADAS_LEVELTAR_AZONOSITOJA'
	            ,@Ertek = @Leveltar_Azonositoja
                ,@Karbantarthato = 1
	            ,@Note = 'Annak a levéltári szervezetnek az azonosítója, amely az elektronikus átvételi csomag befogadását végzi.'
                ,@Ver = 0                                               /* nem kell vmi verziószám ??? */
                ,@Letrehozo_Id   = @AxisUser                            /* ORG ez alapján kerül meghatározásra */
                ,@LetrehozasIdo  = @ExecTime
                ,@UpdatedColumns = @xmlInsert
                ,@ResultUID = @ResultUID
	            ;
            SET @n_InsertOK += 1; 				
		  end try
          begin catch			
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_LEVELTAR_AZONOSITOJA'' insert): ' + ERROR_MESSAGE();			
          end catch;
		else  
          begin try
		    SET @ActVer = ( select p.Ver from [KRT_Parameterek] p 
	                         where p.ID = @recordID );
            exec [sp_KRT_ParameterekUpdate] 
                 @ID = @recordID
			    ,@ExecutorUserID = @AxisUser
  			    ,@ExecutionTime = @ExecTime
                ,@Karbantarthato = 1
                ,@Ver = @ActVer
                ,@Modosito_Id   = @AxisUser
                ,@ModositasIdo  = @ExecTime
                ,@UpdatedColumns = @xmlUpdate
  	            ;
            SET @n_UpdateOK += 1; 				      --- ez csak akkor kaphat értéket, ha az exec raiserror nélkül futott le
		  end try
		  begin catch
		    print ''
		    print 'HIBA (''LEVELTARI_ATADAS_LEVELTAR_AZONOSITOJA'' update): ' + ERROR_MESSAGE();			
          end catch;

      end;                      -- @AxisUser is NOT Null
	  
---

  print ''
  print '---------------'
  print 'Összesen:       ' + convert( nvarchar(10), @n_count );
  print 'Sikeres INSERT: ' + convert( nvarchar(10), @n_InsertOK );
  print '        UPDATE: ' + convert( nvarchar(10), @n_UpdateOK );  
  print '---------------'	  
	  
	fetch next from cur_ORG into @Org_ID, @Org_Kod, @Org_Nev;
  END;                                                             --- ORG cursor
  Close cur_ORG;
  Deallocate cur_ORG; 
		  
END;
  /* --- 
  select substring( p.Nev,1,40 ) as 'Név'
		,substring( p.Ertek,1,50 ) as 'Érték'
		,case when p.Karbantarthato = 1 then 'Igen'
		      else 'Nem'
		 end as 'Karbantartható'
    FROM KRT_Parameterek p
   where p.Nev like 'LEVELTARI_ATADAS_%'
   order by p.Nev, p.LetrehozasIdo
  ;
   --- */

print ''
print '/* --- Vége --- ' + convert( nvarchar(30), sysdatetime() ) + ' */'
GO

--------------------------------------------------------------------------

DECLARE @BLG_324_KCS_CimTipus UNIQUEIDENTIFIER 
SELECT TOP 1 @BLG_324_KCS_CimTipus= ID FROM KRT_KodCsoportok where kod = 'CIM_TIPUS'

DECLARE @BLG_324_A_Kodtar_Id UNIQUEIDENTIFIER
DECLARE @BLG_324_B_Kodtar_Id UNIQUEIDENTIFIER
DECLARE @BLG_324_C_Kodtar_Id UNIQUEIDENTIFIER
DECLARE @BLG_324_D_Kodtar_Id UNIQUEIDENTIFIER

SET @BLG_324_A_Kodtar_Id= '575D1469-66D9-4898-86F6-EB31A7203FEB'
SET @BLG_324_B_Kodtar_Id= '575D1469-66D9-4898-86F6-EB31A7203FEC'
SET @BLG_324_C_Kodtar_Id= '575D1469-66D9-4898-86F6-EB31A7203FED'
SET @BLG_324_D_Kodtar_Id= '575D1469-66D9-4898-86F6-EB31A7203FEE'

DECLARE @BLG_324_ORG_ID uniqueidentifier 

IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok where Id=@BLG_324_KCS_CimTipus)
	BEGIN 
		Print 'ERROR: Nincs CIM_TIPUS Kódcsoport.' 
	END 
ELSE 
	BEGIN 
		
		SET @BLG_324_ORG_ID = '450b510a-7caa-46b0-83e3-18445c0c53a9'

		Print 'CIM_TIPUS 07 Kódtárértékek' 

		IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @BLG_324_A_Kodtar_Id)
		BEGIN 
		
			Print 'INSERT' 
			insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @BLG_324_KCS_CimTipus
			,null
			,null
			,@BLG_324_ORG_ID
			,@BLG_324_A_Kodtar_Id
			,'07'
			,'Titkos kapcsolati kód'
			,'TitkosKaKo'
			,'0'
			,'7'
			,'Titkos kapcsolati kód'
			); 
		END
	
		IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @BLG_324_B_Kodtar_Id)
		BEGIN 
			insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @BLG_324_KCS_CimTipus
			,null
			,null
			,@BLG_324_ORG_ID
			,@BLG_324_B_Kodtar_Id
			,'01RNY'
			,'Postai Cim RNY'
			,'01RNY'
			,'0'
			,'8'
			,'Postai Cim RNY'
			); 
		END
	
		IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @BLG_324_C_Kodtar_Id)
		BEGIN 	
			insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @BLG_324_KCS_CimTipus
			,null
			,null
			,@BLG_324_ORG_ID
			,@BLG_324_C_Kodtar_Id
			,'03RNY'
			,'Email Cim RNY'
			,'03RNY'
			,'0'
			,'9'
			,'Email Cim RNY'
			); 
		END
		
		IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @BLG_324_D_Kodtar_Id)
		BEGIN 	
			insert into KRT_KodTarak(
			 KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			,Egyeb
			) values (
			 @BLG_324_KCS_CimTipus
			,null
			,null
			,@BLG_324_ORG_ID
			,@BLG_324_D_Kodtar_Id
			,'04RNY'
			,'Telefonszam RNY'
			,'04RNY'
			,'0'
			,'10'
			,'Telefonszam RNY'
			); 
		END 
	
	END


PRINT 'BLG_324 START'
----------------------------------------------------------
go

-------------------------------------------------------
print 'Add ADATHORDOZO_TIPUSA.KapuRendszer vevény'
declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '025C2C0E-368D-4E19-A660-36B146F5F6C8'
declare @recordId uniqueidentifier
set @recordId = '7756B7FE-D213-4E6C-8EEE-A7AB0243F103'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
declare @tranzId uniqueidentifier
set @tranzId = 'F60AD910-541A-470D-B888-DA5A6A061668'

--delete from KRT_KodTarak where Id = @recordId

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	,Tranz_id
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'16'
	,'KapuRendszer vevény'
	,'1'
	,'16'
	,@tranzId
	); 
END 

GO

--IratMozgatasPanelEngedelyezes-----------------------------------------------------
declare @funkcioId uniqueidentifier
set @funkcioId = '75689F33-C801-4549-B64D-04C172688201'
	 
IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE Id = @funkcioId)
BEGIN 
	Print 'INSERT KRT_Funkciok - IratMozgatasPanelEngedelyezes' 

	insert into KRT_Funkciok
	(
		Id,
		Kod,
		Nev,
		Modosithato
) 
values (
	@funkcioId,
	'IratMozgatasPanelEngedelyezes',
	'Ügyirat térképen az IratMozgatasPanel megjelenítésének engedélyezése',
	'1'
); 
	 
END

GO

--------------------------------------------------------------------------
-- AddPrarameter_eUzenetForras kezdete
declare @modulId uniqueidentifier
set @modulId = 'AA6D9944-40C0-40CD-8B75-9116B2E46C3D'

IF NOT EXISTS (Select 1 FROM INT_Modulok where Id = @modulId)
BEGIN
	print 'INSERT INT_Modulok - eUzenet'

	INSERT INTO INT_Modulok
	(Id, Nev)
	VALUES
	(@modulId,'eUzenet')
END

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

declare @recordId uniqueidentifier
set @recordId = '7B452F3A-C485-473B-B905-629D4333E7EB'

declare @Ertek nvarchar(400)
-- Default - FPH
set @Ertek = 'HivataliKapu'

IF @org_kod = 'BOPMH'
BEGIN
	set @Ertek = 'KOMDAT'
END

IF @org_kod = 'NMHH'
BEGIN
	set @Ertek = 'ELHISZ'
END

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @recordId)
BEGIN
    print 'INSERT INT_Parameterek - eUzenetForras - ' + @Ertek

	INSERT INTO INT_Parameterek
	(Id, Modul_id, Nev, Ertek, Karbantarthato)
	VALUES
	(@recordId, @modulId, 'eUzenetForras', @Ertek,'1')
END

GO

PRINT 'BLG_2445 START'
----------------------------------------------------------

DECLARE @record_id uniqueidentifier
SET @record_id = NULL 
SET @record_id = (SELECT Id FROM KRT_Parameterek WHERE Id='E12A78A5-9C6B-420D-AFA9-C0D980DB7D35') 
PRINT '@record_id=' + CONVERT(NVARCHAR(100), @record_id)

IF @record_id IS NULL 
BEGIN 
	PRINT 'INSERT' 
	 INSERT INTO KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) VALUES (
		 NULL
		,NULL
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'E12A78A5-9C6B-420D-AFA9-C0D980DB7D35'
		,'SCAN_INPUT_DELAY'
		,'60'
		,'1'
		,'a megadott érték megadja, hogy hány másodpercnél régebben létrehozott fájlokat olvasson fel a SCAN_INPUT_PATH-ról a program (a félkész fájlok felolvasását elkerülendő)'
	); 
END

SET @record_id = NULL 
SET @record_id = (SELECT Id FROM KRT_Parameterek WHERE Id='E2197BEC-A655-4B34-8C08-1567B2309E2B') 
PRINT '@record_id=' + CONVERT(NVARCHAR(100), @record_id)

IF @record_id IS NULL 
BEGIN 
	PRINT 'INSERT' 
	 INSERT INTO KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) VALUES (
		 NULL
		,NULL
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'E2197BEC-A655-4B34-8C08-1567B2309E2B'
		,'SCAN_INPUT_PATH'
		,'\\localhost\c$\Temp\Input'
		,'1'
		,'a szkennelt állományokat tartalmazó könyvtár útvonala'
	); 
END

SET @record_id = NULL 
SET @record_id = (SELECT Id FROM KRT_Parameterek WHERE Id='22D6EC4C-423C-4BDD-ABD2-98B066BA6A9D')
PRINT '@record_id=' + CONVERT(NVARCHAR(100), @record_id)

IF @record_id IS NULL 
BEGIN 
	PRINT 'INSERT' 
	 INSERT INTO KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) VALUES (
		 NULL
		,NULL
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'22D6EC4C-423C-4BDD-ABD2-98B066BA6A9D'
		,'SCAN_ARCHIVE_PATH'
		,'\\localhost\c$\Temp\Archive'
		,'1'
		,'a szkennelt állományok feldolgozás utáni archiválási mappájának útvonala, ha a rendszer paraméter üres, akkor a fájlok törlésre kerülnek, ha az útvonal hibás, akkor az input mappa archive almappájában kerülnek a fájlok'
	); 
END

SET @record_id = NULL 
SET @record_id = (SELECT Id FROM KRT_Parameterek WHERE Id='0CAC8FDD-53E8-44F5-BE8D-C5D62B825CCC')
PRINT '@record_id=' + CONVERT(NVARCHAR(100), @record_id)

IF @record_id IS NULL 
BEGIN 
	PRINT 'INSERT' 
	 INSERT INTO KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) VALUES (
		 NULL
		,NULL
		,'00000000-0000-0000-0000-000000000000'
		,'0CAC8FDD-53E8-44F5-BE8D-C5D62B825CCC'
		,'SCAN_USER_NAME'
		,'DOMAIN\scan.user'
		,'1'
		,'a ScanService authentikációhoz használt alapértelmezett felhasználó'
	); 
END

SET @record_id = NULL 
SET @record_id = (SELECT Id FROM KRT_Felhasznalok WHERE Id='D95FD4AF-3CBE-40BE-8D1E-18F6F50CFB4C')
PRINT '@record_id=' + CONVERT(NVARCHAR(100), @record_id)

SET @record_id = NULL 
SET @record_id = (SELECT Id FROM KRT_Felhasznalok WHERE Id='D95FD4AF-3CBE-40BE-8D1E-18F6F50CFB4C')
PRINT '@record_id=' + CONVERT(NVARCHAR(100), @record_id)

IF @record_id IS NULL 
BEGIN 
	PRINT 'INSERT' 
	INSERT INTO KRT_Felhasznalok (
		 Id
		,Org
		,Partner_id
		,Tipus
		,UserNev
		,Nev
		,JelszoLejaratIdo
		,[System]
		,Engedelyezett
		,Ver
		,ErvKezd
		,ErvVege
		,LetrehozasIdo
	) VALUES (
		 'D95FD4AF-3CBE-40BE-8D1E-18F6F50CFB4C'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,null
		,'Windows'
		,'DOMAIN\scan.user'
		,'Scan User'
		,CONVERT(datetime, '4700-12-31')
		,1
		,1
		,1
		,GETDATE()
		,CONVERT(datetime, '4700-12-31')
		,GETDATE()
	);
END
ELSE
BEGIN
	PRINT 'UPDATE' 
	update KRT_Felhasznalok
		set Partner_id = null
	where Id = 'D95FD4AF-3CBE-40BE-8D1E-18F6F50CFB4C'
END
GO

PRINT 'BUG_2545 START'
----------------------------------------------------------

DECLARE @record_id uniqueidentifier
SET @record_id = NULL 
SET @record_id = (SELECT Id FROM KRT_Parameterek WHERE Id='DE0E2F37-5752-440B-A39D-60DC81F5F8D7') 
PRINT '@record_id=' + CONVERT(NVARCHAR(100), @record_id)

IF @record_id IS NULL 
BEGIN 
	PRINT 'INSERT' 
	 INSERT INTO KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) VALUES (
		 NULL
		,NULL
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'DE0E2F37-5752-440B-A39D-60DC81F5F8D7'
		,'SCAN_ERROR_EMAIL'
		,'contentum@axis.hu'
		,'1'
		,'a FileShare-re szkennelt fájlok betöltése során bekövetkező hibaüzenetek erre az e-mail címre kerültnek elküldésre'
	); 
END

GO

-- BLG_1392 IKTATAS_KOTELEZO_IRATTIPUS és IKTATAS_KOTELEZO_IRATUGYINTEZO rendszerparaméterek létrehozása 
PRINT 'BLG_1392 START'
Print 'IKTATAS_KOTELEZO_IRATTIPUS'			
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='7203C38A-FE6A-E811-80CB-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'7203C38A-FE6A-E811-80CB-00155D020DD3'
		,'IKTATAS_KOTELEZO_IRATTIPUS'
		,'0'
		,'1'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='7203C38A-FE6A-E811-80CB-00155D020DD3'
	--	,Nev='IKTATAS_KOTELEZO_IRATTIPUS'
	--	,Ertek='0'
	--	,Karbantarthato='1'
	-- WHERE Id=@record_id 
 --END

--if @org_kod = 'BOPMH' 
--BEGIN 
--	Print @org_kod +': 1'
--	UPDATE KRT_Parameterek
--	   SET Ertek='1'
--	   WHERE Id='7203C38A-FE6A-E811-80CB-00155D020DD3'
--END

GO 

Print 'IKTATAS_KOTELEZO_IRATUGYINTEZO'		
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
	
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='EB50F352-FE6A-E811-80CB-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'EB50F352-FE6A-E811-80CB-00155D020DD3'
		,'IKTATAS_KOTELEZO_IRATUGYINTEZO'
		,'0'
		,'1'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='EB50F352-FE6A-E811-80CB-00155D020DD3'
	--	,Nev='IKTATAS_KOTELEZO_IRATUGYINTEZO'
	--	,Ertek='0'
	--	,Karbantarthato='1'
	-- WHERE Id=@record_id 
 --END

--if @org_kod = 'BOPMH' 
--BEGIN 
--	Print @org_kod +': 1'
--	UPDATE KRT_Parameterek
--	   SET Ertek='1'
--	   WHERE Id='EB50F352-FE6A-E811-80CB-00155D020DD3'
--END
PRINT 'BLG_1392 END'

GO

PRINT 'BLG_1560'
DECLARE @org_kod nvarchar(100) 
DECLARE @record_id uniqueidentifier 
SET @org_kod   = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
SET @record_id = '7B9A6629-A0BF-4E67-9457-34B6C296BB3B'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek where Id=@record_id) 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@org_kod,
		@record_id
		,'VONALKOD_BEJOVO_ELEKTRONIKUS_GENERALT'
		,'1'
		,'1'
		,'Amennyiben a rendszer paraméter értéke 1, akkor a bejövő elektronikus iratok iktatásakor nem kötelező a vonalkód megadása, azt a rendszer automatikusan generálja.'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org=@org_kod
	--	,Id=@record_id
	--	,Nev='VONALKOD_BEJOVO_ELEKTRONIKUS_GENERALT'
	--	,Ertek='1'
	--	,Karbantarthato='1'
	--	,Note='Amennyiben a rendszer paraméter értéke 1, akkor a bejövő elektronikus iratok iktatásakor nem kötelező a vonalkód megadása, azt a rendszer automatikusan generálja.'
	-- WHERE Id=@record_id 
 --END

GO



-------------------------------------------------------------------------------
-- BLG 1632 - Feladat kiadásról e-mail minden prioritás esetén
-- A kézi kiadású feladatokat csak magas prioritással lehet majd felvenni (nem lesz más választható), így minden esetben menni fog e-mail üzenet a feladatról.
-- FELADAT_MANUALIS_PRIORITASOK rendszerparameter	 
-- ertek: 11,21,31
-- FPH-ban
-- A FELADAT_PRIORITAS kódcsoport értékei közül azok kerüljenek felsorolásra vesszővel elválasztva, amelyek manuális feladat kiadás esetén kiválaszthatók legyenek a képernyőn.

DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
where Id='3921f379-4773-42ec-9245-68e20d912726') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'3921f379-4773-42ec-9245-68e20d912726'
		,'FELADAT_MANUALIS_PRIORITASOK'
		,'11,21,31'
		,'1'
		,'A FELADAT_PRIORITAS kódcsoport értékei közül azok kerüljenek felsorolásra vesszővel elválasztva, amelyek manuális feladat kiadás esetén kiválaszthatók legyenek a képernyőn.'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='3921f379-4773-42ec-9245-68e20d912726'
	--	,Nev='FELADAT_MANUALIS_PRIORITASOK'
	--	,Ertek='11,21,31'
	--	,Karbantarthato='1'
	--	,Note='A FELADAT_PRIORITAS kódcsoport értékei közül azok kerüljenek felsorolásra vesszővel elválasztva, amelyek manuális feladat kiadás esetén kiválaszthatók legyenek a képernyőn.'
	-- WHERE Id=@record_id 
 --END
 ---------------------------------------------------------------------------------------------------------------
 -------------------- BLG 1632 - Feladat kiadásról e-mail minden prioritás esetén ------------------------------
  ---------------------------------------------VÉGE-------------------------------------------------------------
GO


PRINT '------STOP------ BLG 1632'

---------------------------------------------------------------------------------------------------------------
------------------ BLG 2204 - Iratpéldány fajtája mező legyen átnevezve Iratpéldány típusa-ra -----------------
---------------------------------------------------------------------------------------------------------------

IF NOT EXISTS (select 1 from KRT_Forditasok where Id = 'b041d9c2-2462-403c-a732-972d87f6bb95')
	insert into KRT_Forditasok
	(
	[Id]
	,[NyelvKod]
	,[OrgKod]
	,[Modul]
	,[Komponens]
	,[ObjAzonosito]
	,[Forditas]
	)
	VALUES
	(
	'b041d9c2-2462-403c-a732-972d87f6bb95',
	'hu-HU',
	'NMHH',
	'eRecord',
	'eRecordComponent/PldIratPeldanyFormTab.ascx',
	'labelIratpeldanyTipusa',
	'Iratpéldány típusa:'
	)
---------------------------------------------------------------------------------------------------------------
------------------ BLG 2204 - Iratpéldány fajtája mező legyen átnevezve Iratpéldány típusa-ra -----------------
-----------------------------------------------VÉGE------------------------------------------------------------

PRINT '------STOP------ BLG 2204'

DECLARE @org_id UNIQUEIDENTIFIER 
SET @org_id ='450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @RECORD_ID_OCR1 uniqueidentifier
set @RECORD_ID_OCR1 = 'B5965DFE-F931-430B-8DB9-2D8A791CE121'
declare @RECORD_ID_OCR2 uniqueidentifier
set @RECORD_ID_OCR2 = 'B5965DFE-F931-430B-8DB9-2D8A791CE122'
declare @RECORD_ID_OCR3 uniqueidentifier
set @RECORD_ID_OCR3 = 'B5965DFE-F931-430B-8DB9-2D8A791CE123'
declare @RECORD_ID_OCR4 uniqueidentifier
set @RECORD_ID_OCR4 = 'B5965DFE-F931-430B-8DB9-2D8A791CE124'

IF NOT EXISTS(SELECT 1 FROM KRT_PARAMETEREK WHERE ID =@RECORD_ID_OCR1)
BEGIN 
	Print 'INSERT SCAN_OCR_EXPORT_PATH' 
	 insert into KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @org_id
		,@record_id_OCR1
		,'SCAN_OCR_EXPORT_PATH'
		,'\\dms-kofax\external\DEV\Export'
		,'1'
		,'A mappa amelybe az OCR-ezésre szánt fájlokat el kell helyezni'
		); 
 END 

IF NOT EXISTS(SELECT 1 FROM KRT_PARAMETEREK WHERE ID =@RECORD_ID_OCR2)
BEGIN 
	Print 'INSERT SCAN_OCR_IMPORT_PATH' 
	 insert into KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @org_id
		,@record_id_OCR2
		,'SCAN_OCR_IMPORT_PATH'
		,'\\dms-kofax\external\DEV\Import'
		,'1'
		,'A mappa amelyből az elkészült OCR-ezett PDF-eket fel kell olvasni'
		); 
 END 
 
IF NOT EXISTS(SELECT 1 FROM KRT_PARAMETEREK WHERE ID =@RECORD_ID_OCR3)
BEGIN 
	Print 'INSERT SCAN_OCR_IMPORT_DELAY' 
	 insert into KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @org_id
		,@record_id_OCR3
		,'SCAN_OCR_IMPORT_DELAY'
		,'60'
		,'1'
		,'A paraméter érték megadja, hogy hány másodpercnél régebben létrehozott fájlokat olvasson fel a OCR_IMPORT_PATH mappából a program (a félkész fájlok felolvasását elkerülendő)'
		); 
 END 
 
IF NOT EXISTS(SELECT 1 FROM KRT_PARAMETEREK WHERE ID =@RECORD_ID_OCR4)
BEGIN 
	 Print 'INSERT SCAN_OCR_IMPORT_TIMEOUT' 
	 insert into KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @org_id
		,@record_id_OCR4
		,'SCAN_OCR_IMPORT_TIMEOUT'
		,'60'
		,'1'
		,'A paraméter érték megadja, hogy mennyi perc után tegyük elérhetővé a fájlok OCR-ezésének újra indítását)'
		); 
 END 
 GO
----------------
DECLARE @ORG UNIQUEIDENTIFIER
SET @ORG = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @KodCsoport_Id_OCR uniqueidentifier
SET @KodCsoport_Id_OCR = '9A32381D-5F8F-4AE2-83E0-32F440191C9E'

IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = @KodCsoport_Id_OCR)
	BEGIN 
		Print 'INSERT KCS OCR' 
		insert into KRT_KodCsoportok(
		 BesorolasiSema
		,Id
		,Kod
		,Nev
		,Modosithato
		,Note
		) values (
		'0'
		,@KodCsoport_Id_OCR
		,'OCR'
		,'OCR'
		,'0'
		,'OCR'
		); 
	END
GO
----------------
IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '886F4366-A424-0F66-A443-52105A272EF2') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id] ,[Org] ,[KodCsoport_Id] ,[Kod] ,[Nev] ,[RovidNev] ,[Egyeb] ,[Modosithato] ,[Sorrend] ,[Note]) VALUES ('886F4366-A424-0F66-A443-52105A272EF2' ,'450B510A-7CAA-46B0-83E3-18445C0C53A9' ,'9A32381D-5F8F-4AE2-83E0-32F440191C9E' ,'Igen' ,'Igen' ,'I' ,'Igen' ,'0' ,'1' ,'' ) END ELSE UPDATE KRT_KodTarak SET Kod='Igen' ,Nev='Igen' ,Egyeb = 'Igen' ,Modosithato= '0' WHERE Id='886F4366-A424-0F66-A443-52105A272EF2' 

GO
----------------
IF NOT EXISTS (SELECT 1 FROM KRT_KODTARFUGGOSEG WHERE ID='5baa1825-0b56-46f6-9080-eceac81fb227')
BEGIN
	PRINT 'INSERT KodtarFuggoseg: OCR - Igen'
	INSERT [dbo].[KRT_KodtarFuggoseg] ([Id], [Org], [Vezerlo_KodCsoport_Id], [Fuggo_KodCsoport_Id], [Adat], [Aktiv], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) 
	VALUES (N'5baa1825-0b56-46f6-9080-eceac81fb227', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'644d3323-050c-4fbe-a380-25fedc063b8e', N'9a32381d-5f8f-4ae2-83e0-32f440191c9e', N'{"VezerloKodCsoportId":"644d3323-050c-4fbe-a380-25fedc063b8e","FuggoKodCsoportId":"9a32381d-5f8f-4ae2-83e0-32f440191c9e","Items":[{"VezerloKodTarId":"9dbb20cd-f53c-458f-b783-ec7524b5ab34","FuggoKodtarId":"886f4366-a424-0f66-a443-52105a272ef2","Aktiv":true},{"VezerloKodTarId":"d42fa9f8-321b-4c8b-b684-e7a858c074e3","FuggoKodtarId":"886f4366-a424-0f66-a443-52105a272ef2","Aktiv":true},{"VezerloKodTarId":"9752d9bc-27fe-4f2e-a0c5-8f8b93bb62db","FuggoKodtarId":"886f4366-a424-0f66-a443-52105a272ef2","Aktiv":true},{"VezerloKodTarId":"dda4036d-2e88-498f-8d47-ef4086e2b98f","FuggoKodtarId":"886f4366-a424-0f66-a443-52105a272ef2","Aktiv":true},{"VezerloKodTarId":"7921a68d-70be-4484-accd-7837665c3c90","FuggoKodtarId":"886f4366-a424-0f66-a443-52105a272ef2","Aktiv":true}],"ItemsNincs":[]}', N'1', 1, NULL, NULL, CAST(N'2018-06-20 00:00:00.000' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2018-06-20 00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2018-06-20 00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL)
END
GO
----------------


GO
---------------------------------------------------------------------------------------------------------

GO
DELETE FROM KRT_Parameterek WHERE ID = '7B9A6629-A0BF-4E67-9457-34B6C296BB3B'
PRINT 'BLG_1560'
DECLARE @org_kod nvarchar(100) 
DECLARE @record_id uniqueidentifier 
SET @org_kod   = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
SET @record_id = '7B9A6629-A0BF-4E67-9457-34B6C296BB3B'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek where Id=@record_id) 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@org_kod,
		@record_id
		,'VONALKOD_BEJOVO_ELEKTRONIKUS_GENERALT'
		,'1'
		,'1'
		,'Amennyiben a rendszer paraméter értéke 1, akkor a bejövő elektronikus iratok iktatásakor nem kötelező a vonalkód megadása, azt a rendszer automatikusan generálja.'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org=@org_kod
	--	,Id=@record_id
	--	,Nev='VONALKOD_BEJOVO_ELEKTRONIKUS_GENERALT'
	--	,Ertek='1'
	--	,Karbantarthato='1'
	--	,Note='Amennyiben a rendszer paraméter értéke 1, akkor a bejövő elektronikus iratok iktatásakor nem kötelező a vonalkód megadása, azt a rendszer automatikusan generálja.'
	-- WHERE Id=@record_id 
 --END

GO

-------------- BUG 3140 JS hiba ------------------

UPDATE KRT_KodTarak
SET ErvVege = '2018-08-01 00:00:00.000'  
WHERE Id = '3ac43dce-de9e-4090-8d88-5240d47c39ea'

-------------- BUG 3140 JS hiba ------------------
----------------- VÉGE ---------------------------

GO

------------- SZURGetFoszamWS - FTargyszo mezőhöz tárgyszó --------------

print 'Add_Targyszok kezdete'

declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @OrgKod nvarchar(100)
set @OrgKod = (select Kod from KRT_Orgok where Id = @Org)

declare @UgyObjTip_Id uniqueidentifier
set @UgyObjTip_Id = 'A04417A6-54AC-4AF1-9B63-5BABF0203D42'

declare @DefinicioTipus nvarchar(64)
set @DefinicioTipus = 'B1'

declare @Letrehozo_Id uniqueidentifier
set @Letrehozo_Id = '54E861A5-36ED-44CA-BAA7-C287D125B309'

declare @now datetime
set @now = GETDATE()

declare @Tranz_id uniqueidentifier
set @Tranz_id = 'C3A05D27-8946-4B7A-BAA9-5D49D334F7DF'

declare @objMetaDefId_UgyiratMetaadatok uniqueidentifier
set @objMetaDefId_UgyiratMetaadatok = '16DD6E89-5D11-E811-80C5-00155D027EA9'

IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaDefinicio WHERE Id = @objMetaDefId_UgyiratMetaadatok)
BEGIN
	print 'insert - EREC_Obj_MetaDefinicio: EREC_UgyUgyiratok_metaadatai'

	INSERT INTO EREC_Obj_MetaDefinicio
	(
		[Id]
       ,[Org]
       ,[Objtip_Id]
       ,[DefinicioTipus]
       ,[ContentType]
	   ,[Letrehozo_id]
	   ,[Tranz_id]
	)
	VALUES
	(
		@objMetaDefId_UgyiratMetaadatok
	   ,@Org
	   ,@UgyObjTip_Id
	   ,@DefinicioTipus
	   ,'EREC_UgyUgyiratok_metaadatai'
	   ,@Letrehozo_Id
	   ,@Tranz_id
	)
END
ELSE
BEGIN
	print 'Rekord már létezik: EREC_Obj_MetaDefinicio: EREC_UgyUgyiratok_metaadatai'
END

--Tárgyszavak

-- 'Targyszok' tárgyszó
declare @Tipus char(1)
set @Tipus = '1'

declare @ControlTypeSource nvarchar(64)
set @ControlTypeSource = 'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls'

declare @targyszoId uniqueidentifier
set @targyszoId = '5F6A65F7-E2DF-E711-80C2-00155D020D7E'


IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId)
BEGIN
	print 'insert - Targyszok'

	INSERT INTO EREC_TargySzavak
	(
		[Id]
       ,[Org]
       ,[Tipus]
       ,[TargySzavak]
       ,[BelsoAzonosito]
       ,[ControlTypeSource]
       ,[ControlTypeDataSource]
	   ,[Letrehozo_id]
	   ,[Tranz_id]
	)
	VALUES
	(
		@targyszoId
	   ,@Org
	   ,@Tipus
	   ,'Tárgyszók'
	   ,'Targyszok'
	   ,@ControlTypeSource
	   ,NULL
	   ,@Letrehozo_Id
	   ,@Tranz_id
	)
END
ELSE
BEGIN
	print 'Rekord már létezik: EREC_TargySzavak: Targyszok'
END


-- EREC_Obj_MetaAdatai
declare @recordId uniqueidentifier
set @recordId = '6F247DC3-5D11-E811-80C5-00155D027EA9'

IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
BEGIN
	print 'insert - EREC_UgyUgyiratok_metaadatai - Targyszok'

	INSERT INTO EREC_Obj_MetaAdatai
	(
		[Id]
       ,[Obj_MetaDefinicio_Id]
       ,[Targyszavak_Id]
       ,[Sorszam]
       ,[Opcionalis]
       ,[Ismetlodo]
	   ,[Letrehozo_id]
	   ,[Tranz_id]
	)
	VALUES
	(
		@recordId
	   ,@objMetaDefId_UgyiratMetaadatok
	   ,@targyszoId
	   ,0
	   ,'1'
	   ,'0'
	   ,@Letrehozo_Id
	   ,@Tranz_id
	)
END

print 'Add_Targyszok vege'


GO
------------------------------------------------------------------------- 

-------------- BLG 2961 Megjegyzés mező fixen kerüljön ki az aláíró felületre, ne csak elutasítás esetén szerepeljen ------------------
if not exists (select 1 from KRT_Forditasok where Id ='38AED4AB-7325-4A52-9215-0EC1C566C7B9')
insert into KRT_Forditasok
(
[Id]
,[NyelvKod]
,[OrgKod]
,[Modul]
,[Komponens]
,[ObjAzonosito]
,[Forditas]
)
VALUES
(
'38AED4AB-7325-4A52-9215-0EC1C566C7B9',
'hu-HU',
'NMHH',
'eRecord',
'DokumentumAlairas.aspx',
'MegjegyzesLabel',
'Megjegyzés:'
) 
if not exists (select 1 from KRT_Forditasok where Id ='77edee72-0932-441c-95f2-565426bbe828')
insert into KRT_Forditasok
(
[Id]
,[NyelvKod]
,[OrgKod]
,[Modul]
,[Komponens]
,[ObjAzonosito]
,[Forditas]
)
VALUES
(
'77edee72-0932-441c-95f2-565426bbe828',
'hu-HU',
'NMHH',
'eRecord',
'DokumentumAlairas.aspx',
'MegjegyzesLabel2',
'Elutasítás oka:')


-------------- BLG 2961 Megjegyzés mező fixen kerüljön ki az aláíró felületre, ne csak elutasítás esetén szerepeljen-------------------

------------------  BLG_2156 start  -------------------------
-- BLG_2156: IDOEGYSEG kódcsoportba új kódtár értékek: Hét, Hónap, Év
--			 IDOEGYSEG_FELHASZNALAS kódcsoport felvétele, Határidő, Selejtezés kódtárértékkel

Print 'IDOEGYSEG kódcsoport'

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_KodCsoportok
			where Id='BCFFD393-D18E-42B5-8AE1-F4B294642E29') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'ERROR: Nincs IDOEGYSEG kódcsoport vagy nem jó az ID-ja' 

END 
ELSE 
BEGIN 
    Print 'HÉT kódtárérték' 
	SET @record_id= null
    SET @record_id = (select Id from KRT_KodTarak
		where Id='D839DD6B-A3B5-E811-80CC-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
		
		Print 'INSERT' 
			insert into KRT_KodTarak(
				KodCsoport_Id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			) values (
			'BCFFD393-D18E-42B5-8AE1-F4B294642E29'
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'D839DD6B-A3B5-E811-80CC-00155D020DD3'
			,'10080'
			,'Hét'
			,'Hét'
			,'0'
			,'5'
			); 
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
		END 
		ELSE 
		BEGIN 
			Print 'UPDATE'
			UPDATE KRT_KodTarak
			SET 
				KodCsoport_Id='BCFFD393-D18E-42B5-8AE1-F4B294642E29'
			,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Kod='10080'
			,Nev='Hét'
			,RovidNev='Hét'
			,Modosithato='0'
			,Sorrend='5'
			WHERE Id='D839DD6B-A3B5-E811-80CC-00155D020DD3'
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
	END
	
	Print 'HÓNAP kódtárérték' 
	SET @record_id= null
    SET @record_id = (select Id from KRT_KodTarak
		where Id='D939DD6B-A3B5-E811-80CC-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
		
		Print 'INSERT' 
			insert into KRT_KodTarak(
				KodCsoport_Id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			) values (
			'BCFFD393-D18E-42B5-8AE1-F4B294642E29'
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'D939DD6B-A3B5-E811-80CC-00155D020DD3'
			,'28'
			,'Hónap'
			,'Hónap'
			,'0'
			,'6'
			); 
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
		END 
		ELSE 
		BEGIN 
			Print 'UPDATE'
			UPDATE KRT_KodTarak
			SET 
				KodCsoport_Id='BCFFD393-D18E-42B5-8AE1-F4B294642E29'
			,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Kod='28'
			,Nev='Hónap'
			,RovidNev='Hónap'
			,Modosithato='0'
			,Sorrend='6'
			WHERE Id='D939DD6B-A3B5-E811-80CC-00155D020DD3'
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
	END
	
	Print 'ÉV kódtárérték' 
	SET @record_id= null
    SET @record_id = (select Id from KRT_KodTarak
		where Id='DA39DD6B-A3B5-E811-80CC-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
		
		Print 'INSERT' 
			insert into KRT_KodTarak(
				KodCsoport_Id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			) values (
			'BCFFD393-D18E-42B5-8AE1-F4B294642E29'
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'DA39DD6B-A3B5-E811-80CC-00155D020DD3'
			,'365'
			,'Év'
			,'Év'
			,'0'
			,'7'
			); 
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
		END 
		ELSE 
		BEGIN 
			Print 'UPDATE'
			UPDATE KRT_KodTarak
			SET 
				KodCsoport_Id='BCFFD393-D18E-42B5-8AE1-F4B294642E29'
			,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Kod='365'
			,Nev='Év'
			,RovidNev='Év'
			,Modosithato='0'
			,Sorrend='7'
			WHERE Id='DA39DD6B-A3B5-E811-80CC-00155D020DD3'
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
	END
END

GO

Print 'IDOEGYSEG_FELHASZNALAS kódcsoport'

 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_KodCsoportok
			where Id='DB39DD6B-A3B5-E811-80CC-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
	insert into KRT_KodCsoportok(
	 BesorolasiSema
	,Id
	,Kod
	,Nev
	,Modosithato
	,Note
	) values (
	'0'
	,'DB39DD6B-A3B5-E811-80CC-00155D020DD3'
	,'IDOEGYSEG_FELHASZNALAS'
	,'Időegység felhasználás'
	,'0'
	,'Időegység felhasználás módja (Határidő / Selejtezés)'
	); 

END 
ELSE 
BEGIN 
	Print 'UPDATE'
	UPDATE KRT_KodCsoportok
	SET 
	BesorolasiSema='0'
	,Kod='IDOEGYSEG_FELHASZNALAS'
	,Nev='Időegység felhasználás'
	,Modosithato='0'	
	,Note='Időegység felhasználás módja (Határidő / Selejtezés)'
	WHERE Id='DB39DD6B-A3B5-E811-80CC-00155D020DD3'
	
END

SET @record_id= null
SET @record_id = (select Id from KRT_KodCsoportok
			where Id='DB39DD6B-A3B5-E811-80CC-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'ERROR: Nincs IDOEGYSEG_FELHASZNALAS kódcsoport vagy nem jó az ID-ja' 

END 
ELSE 
BEGIN 
    Print 'HATARIDO (H) kódtárérték' 
	SET @record_id= null
    SET @record_id = (select Id from KRT_KodTarak
		where Id='DC39DD6B-A3B5-E811-80CC-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
		
		Print 'INSERT' 
			insert into KRT_KodTarak(
				KodCsoport_Id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			) values (
			'DB39DD6B-A3B5-E811-80CC-00155D020DD3'
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'DC39DD6B-A3B5-E811-80CC-00155D020DD3'
			,'H'
			,'HATARIDO'
			,'Határidő'
			,'0'
			,'1'
			); 
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
		END 
		ELSE 
		BEGIN 
			Print 'UPDATE'
			UPDATE KRT_KodTarak
			SET 
				KodCsoport_Id='DB39DD6B-A3B5-E811-80CC-00155D020DD3'
			,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Kod='H'
			,Nev='HATARIDO'
			,RovidNev='Határidő'
			,Modosithato='0'
			,Sorrend='1'
			WHERE Id='DC39DD6B-A3B5-E811-80CC-00155D020DD3'
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
	END
	
	Print 'SELEJTEZES (S) kódtárérték' 
	SET @record_id= null
    SET @record_id = (select Id from KRT_KodTarak
		where Id='DD39DD6B-A3B5-E811-80CC-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
		
		Print 'INSERT' 
			insert into KRT_KodTarak(
				KodCsoport_Id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			) values (
			'DB39DD6B-A3B5-E811-80CC-00155D020DD3'
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'DD39DD6B-A3B5-E811-80CC-00155D020DD3'
			,'S'
			,'SELEJTEZES'
			,'Selejtezés'
			,'0'
			,'2'
			); 
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
		END 
		ELSE 
		BEGIN 
			Print 'UPDATE'
			UPDATE KRT_KodTarak
			SET 
				KodCsoport_Id='DB39DD6B-A3B5-E811-80CC-00155D020DD3'
			,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Kod='S'
			,Nev='SELEJTEZES'
			,RovidNev='Selejtezés'
			,Modosithato='0'
			,Sorrend='2'
			WHERE Id='DD39DD6B-A3B5-E811-80CC-00155D020DD3'
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
	END
	
	-- Kódtár Függőség összerendelés IDOEGYSEG_FELHASZNALAS - IDOEGYSEG
	
	IF NOT EXISTS (SELECT 1 FROM KRT_KODTARFUGGOSEG WHERE ID='0F9547DB-F65E-4D07-8CCB-A5DA1BFDAE65')
	BEGIN
		PRINT 'Kódtár Függőség összerendelés IDOEGYSEG_FELHASZNALAS - IDOEGYSEG'
		INSERT [dbo].[KRT_KodtarFuggoseg] ([Id], [Org], [Vezerlo_KodCsoport_Id], [Fuggo_KodCsoport_Id], [Adat], [Aktiv]) 
		VALUES (N'0F9547DB-F65E-4D07-8CCB-A5DA1BFDAE65', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'DB39DD6B-A3B5-E811-80CC-00155D020DD3', N'BCFFD393-D18E-42B5-8AE1-F4B294642E29', N'{"VezerloKodCsoportId":"db39dd6b-a3b5-e811-80cc-00155d020dd3","FuggoKodCsoportId":"bcffd393-d18e-42b5-8ae1-f4b294642e29","Items":[{"VezerloKodTarId":"dc39dd6b-a3b5-e811-80cc-00155d020dd3","FuggoKodtarId":"fd0dd946-1d18-4315-b2a3-3ce9d6f1a25a","Aktiv":true},{"VezerloKodTarId":"dc39dd6b-a3b5-e811-80cc-00155d020dd3","FuggoKodtarId":"1f3b6445-990b-4cf9-9128-211284f58dca","Aktiv":true},{"VezerloKodTarId":"dc39dd6b-a3b5-e811-80cc-00155d020dd3","FuggoKodtarId":"f0ffd9c8-939d-40e5-893a-a31f658a26ec","Aktiv":true},{"VezerloKodTarId":"dc39dd6b-a3b5-e811-80cc-00155d020dd3","FuggoKodtarId":"0d88c2a5-b876-4b0b-b6d5-b79f6e68f69f","Aktiv":true},{"VezerloKodTarId":"dd39dd6b-a3b5-e811-80cc-00155d020dd3","FuggoKodtarId":"da39dd6b-a3b5-e811-80cc-00155d020dd3","Aktiv":true},{"VezerloKodTarId":"dd39dd6b-a3b5-e811-80cc-00155d020dd3","FuggoKodtarId":"d839dd6b-a3b5-e811-80cc-00155d020dd3","Aktiv":true},{"VezerloKodTarId":"dd39dd6b-a3b5-e811-80cc-00155d020dd3","FuggoKodtarId":"d939dd6b-a3b5-e811-80cc-00155d020dd3","Aktiv":true},{"VezerloKodTarId":"dd39dd6b-a3b5-e811-80cc-00155d020dd3","FuggoKodtarId":"1f3b6445-990b-4cf9-9128-211284f58dca","Aktiv":true}],"ItemsNincs":[]}', N'1')
	END
END

go

--- BLG_2156 
---  Idoegyseg default update
print 'Idoegyseg default update Ugyiratok'
UPDATE EREC_UgyUgyiratok
SET UjOrzesiidoidoegyseg = 365
FROM EREC_UgyUgyiratok
where UjOrzesiIdo is not null
go

print 'Idoegyseg default update Irattaritetelek'
UPDATE EREC_IraIrattariTetelek
SET Idoegyseg = 365
WHERE  MegorzesiIdo <> 'HN'
go


------------------  BLG_2156 end  -------------------------

--------------------------------------------------- VÉGE ------------------------------------------------------------------------------


---------------
--BLG_143
--------------------------------------------------- START ------------------------------------------------------------------------------
GO
PRINT 'BLG_143'
print 'ChangePassword.aspx modul'

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Modulok
			where Id='f09254f6-13ff-4555-9806-2b2473141010') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Modulok(
			 Tranz_id
			,Stat_id
			,Id
			,Tipus
			,Kod
			,Nev
			,Leiras
			) values (
			 'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'f09254f6-13ff-4555-9806-2b2473141010'
			,'F'
			,'ChangePassword.aspx'
			,'Saját jelszó módosítás'
			,null
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Modulok
		 SET 
			 Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Id='f09254f6-13ff-4555-9806-2b2473141010'
			,Tipus='F'
			,Kod='ChangePassword.aspx'
			,Nev='Saját jelszó módosítás'
			,Leiras=null
		 WHERE Id=@record_id 
	 END
	 go
	 

	 
Print 'Saját jelszó módosítás menü'
	 
	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Menuk
			where Id='d2bd6225-7821-4afa-a368-73146eb7ab78') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Menuk(
			 Menu_Id_Szulo
			,Tranz_id
			,Stat_id
			,Id
			,Kod
			,Nev
			,Modul_Id
			,Sorrend			
			) values (
			 '19B72B61-0FF3-4239-8D0F-6FB20792D79C'
			,'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'d2bd6225-7821-4afa-a368-73146eb7ab78'
			,'eRecord'
			,'Jelszó módosítása'
			,'f09254f6-13ff-4555-9806-2b2473141010'
			,'76'
			); 
		 
			 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Menuk
		 SET 
			 Menu_Id_Szulo='19B72B61-0FF3-4239-8D0F-6FB20792D79C'
			,Tranz_id='f60ad910-541a-470d-b888-da5a6a061668'
			,Stat_id='a0848405-e664-4e79-8fab-cfeca4a290af'
			,Id='d2bd6225-7821-4afa-a368-73146eb7ab78'
			,Kod='eRecord'
			,Nev='Jelszó módosítása'
			,Modul_Id='f09254f6-13ff-4555-9806-2b2473141010'
			,Sorrend='76'			
		 WHERE Id=@record_id 
		 
			 
	 END
	 go
	 
-- PASSWORD_POLICY_* rendszerparameterek
print '''PASSWORD_POLICY'' rendszerparameter hozzaadasa'

/*
password_policy 
password_policy_pass_length
password_policy_regexp
password_policy_history_number
password_policy_expire_days
password_policy_wrong_pass_try_number
*/

declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'  
DECLARE @org_kod nvarchar(100) 
SET @org_kod = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
/*select kod from KRT_Orgok where id=@org)*/

print 'Org = ' + @org_kod

declare @tranz_id uniqueidentifier = newid()

declare @record_id uniqueidentifier = '353c3582-e8ce-49a8-a8fa-74503a4a7434'

declare @Ertek nvarchar(400)

set @Ertek = 'STRICT'

print '@record_id = ' + cast(@record_id as nvarchar(36))

print '@Ertek = ' + @Ertek

declare @Nev nvarchar(400) = 'PASSWORD_POLICY'

declare @Note nvarchar(4000) = 'Az alkalmazott jelszó házirend.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		, LetrehozasIdo
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note
		,getdate()
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--     Tranz_id=@tranz_id
	--	,Org=@org
	--	,Id= @record_id
	--	,Nev= @Nev
	--	,Ertek= @Ertek
	--	,Karbantarthato='1'
	--	,Note = @Note
	-- WHERE Id=@record_id 
 --END

print '''PASSWORD_POLICY_REGEXP'' rendszerparameter hozzaadasa'
print 'Org = ' + @org_kod

set @tranz_id  = newid()
set @record_id  = 'ef0e39f5-ae8c-48c7-864a-2a43ff095fa2'
set @Ertek = '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.+[!@#$%^&*-]).{PASSLENGTH,}'
print '@record_id = ' + cast(@record_id as nvarchar(36))
print '@Ertek = ' + @Ertek

set @Nev = 'PASSWORD_POLICY_REGEXP'
set @Note = 'A jelszó erõsségére vonatkozó reguláris kifejezés értéke.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		, LetrehozasIdo
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note		
		,getdate()
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--     Tranz_id=@tranz_id
	--	,Org=@org
	--	,Id= @record_id
	--	,Nev= @Nev
	--	,Ertek= @Ertek
	--	,Karbantarthato='1'
	--	,Note = @Note
	-- WHERE Id=@record_id 
 --END

 print '''PASSWORD_POLICY_EXPIRE_DAYS'' rendszerparameter hozzaadasa'
print 'Org = ' + @org_kod

set @tranz_id  = newid()
set @record_id  = 'ef262764-27d3-40dd-9f42-ea6852d95318'
set @Ertek = '90'
print '@record_id = ' + cast(@record_id as nvarchar(36))
print '@Ertek = ' + @Ertek

set @Nev = 'PASSWORD_POLICY_EXPIRE_DAYS'
set @Note = 'Az erõs jelszó házirend alkalmazása esetén, hány nap mulva járjon le a jelszó.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		, LetrehozasIdo
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note
		,getdate()
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--     Tranz_id=@tranz_id
	--	,Org=@org
	--	,Id= @record_id
	--	,Nev= @Nev
	--	,Ertek= @Ertek
	--	,Karbantarthato='1'
	--	,Note = @Note
	-- WHERE Id=@record_id 
 --END

 print '''PASSWORD_POLICY_PASS_LENGTH'' rendszerparameter hozzaadasa'
print 'Org = ' + @org_kod

set @tranz_id  = newid()
set @record_id  = '86ecf355-eeab-4712-b6b5-62dba6787a35'
set @Ertek = '8'
print '@record_id = ' + cast(@record_id as nvarchar(36))
print '@Ertek = ' + @Ertek

set @Nev = 'PASSWORD_POLICY_PASS_LENGTH'
set @Note = 'Legalább hány karakter hosszú kell, hogy legyen a jelszó.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		, LetrehozasIdo
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note
		,getdate()
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--     Tranz_id=@tranz_id
	--	,Org=@org
	--	,Id= @record_id
	--	,Nev= @Nev
	--	,Ertek= @Ertek
	--	,Karbantarthato='1'
	--	,Note = @Note
	-- WHERE Id=@record_id 
 --END


print '''PASSWORD_POLICY_HISTORY_NUMBER'' rendszerparameter hozzaadasa'
print 'Org = ' + @org_kod

set @tranz_id  = newid()
set @record_id  = 'c754a343-5377-4698-a0de-115dcb0772ba'
set @Ertek = '3'
print '@record_id = ' + cast(@record_id as nvarchar(36))
print '@Ertek = ' + @Ertek

set @Nev = 'PASSWORD_POLICY_HISTORY_NUMBER'
set @Note = 'A strong jelszó házirendhez tartozik, hány visszamenõleges jelszó nem lehet újra jelszó.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		, LetrehozasIdo
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note
		,getdate()
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--     Tranz_id=@tranz_id
	--	,Org=@org
	--	,Id= @record_id
	--	,Nev= @Nev
	--	,Ertek= @Ertek
	--	,Karbantarthato='1'
	--	,Note = @Note
	-- WHERE Id=@record_id 
 --END
 
 print '''PASSWORD_POLICY_WRONG_PASS_TRY_NUMBER'' rendszerparameter hozzaadasa'
print 'Org = ' + @org_kod

set @tranz_id  = newid()
set @record_id  = 'f9189884-1712-4675-8ddd-42b9a7a48766'
set @Ertek = '3'
print '@record_id = ' + cast(@record_id as nvarchar(36))
print '@Ertek = ' + @Ertek

set @Nev = 'PASSWORD_POLICY_WRONG_PASS_TRY_NUMBER'
set @Note = 'A strong jelszó házirendhez tartozik, hányszor lehet  újra elrontani a jelszót.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT' 
	  insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		, LetrehozasIdo
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note
		,getdate()
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--     Tranz_id=@tranz_id
	--	,Org=@org
	--	,Id= @record_id
	--	,Nev= @Nev
	--	,Ertek= @Ertek
	--	,Karbantarthato='1'
	--	,Note = @Note
	-- WHERE Id=@record_id 
 --END
 

GO
--------------------------------------------------- VÉGE ------------------------------------------------------------------------------

-- BLG_2217

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='CF97D0BA-A0A4-E811-80CB-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

print 'TabIndex order'
if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'CF97D0BA-A0A4-E811-80CB-00155D020DD3'
		,'PAGE_VIEW_CUSTOM_TAB_ORDER'
		,'0'
		,'1'
		,'Az űralpokon az alapértelmezetten használt tab sorrendet felülírja a Nézeten beállított: 1-igen, 0-nem'
		); 
 END 


GO
----------------------------------------------------------------------------------------------------------------------------
GO

-------------------------------------------------------------------------------------------
--BLG_3863 START
-------------------------------------------------------------------------------------------
DECLARE @BLG_3863_record_id UNIQUEIDENTIFIER
SET @BLG_3863_record_id = '67B366FD-8472-4710-AD29-01AC52E0150F'

IF NOT EXISTS(SELECT 1 FROM KRT_FUNKCIOK WHERE ID= @BLG_3863_record_id)
	BEGIN
		/*-------------------------------------------------------------------------*/
		PRINT 'KRT_Funkcio - UgyiratUgyintezesiIdoUjraSzamolas létrehozása' 
		/*-------------------------------------------------------------------------*/
		 INSERT INTO KRT_Funkciok(
				 Tranz_id
				,Stat_id
				,Alkalmazas_Id
				,Modosithato
				,Id
				,ObjTipus_Id_AdatElem
				,Muvelet_Id
				,Kod
				,Nev
				) values (
				 null
				,null
				,'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
				,'0'
				,@BLG_3863_record_id
				,'A04417A6-54AC-4AF1-9B63-5BABF0203D42'
				,'5B9AC7B3-879C-48A7-BEBA-EF73A49AF893'
				,'UgyiratUgyintezesiIdoUjraSzamolas'
				,'Ugyirat Ugyintezesi Ido Ujra Szamolas'
				);
	END
	
DECLARE @BLG_3863_SZF_record_id UNIQUEIDENTIFIER
SET @BLG_3863_SZF_record_id = '2A122AE1-39C8-4D48-84AD-787119A47F01'

IF NOT EXISTS(SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID= @BLG_3863_SZF_record_id)
	BEGIN
		/*-------------------------------------------------------------------------*/
		PRINT 'KRT_Szerepkor_Funkcio - UgyiratUgyintezesiIdoUjraSzamolas 2 FEJLESZTO szerepkorhoz kotese' 
		/*-------------------------------------------------------------------------*/
		 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Tranz_id
				,Stat_id
				,Id
				,Funkcio_Id
				) values (
				 'e855f681-36ed-41b6-8413-576fcb5d1542'
				,null
				,null
				,@BLG_3863_SZF_record_id
				,@BLG_3863_record_id
				);
	END
-------------------------------------------------------------------------------------------
--BLG_3863 STOP
-------------------------------------------------------------------------------------------
GO
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='A6F8A83B-7084-4A23-B383-7A439BB31710') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

print 'KULDEMENYAZONOSITO_FORMATUM'
if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'A6F8A83B-7084-4A23-B383-7A439BB31710'
		,'KULDEMENYAZONOSITO_FORMATUM'
		,'*L*$K.MegkulJelzes$/#L#*L*$E.Erkezteto_Szam$#L#'
		,'1'
		,'Küldemény azonosító formátuma'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='A6F8A83B-7084-4A23-B383-7A439BB31710'
	--	,Nev='KULDEMENYAZONOSITO_FORMATUM'
	--	,Ertek='*L*$K.MegkulJelzes$/#L#*L*$E.Erkezteto_Szam$#L#'
	--	,Karbantarthato='1'
	--	,Note='Küldemény azonosító formátuma'
	-- WHERE Id=@record_id 
 --END
 GO
----------------------------------------------------------------------------------------------------------------------------
GO
---------------------------------------------------------------------------------------
--BLG_268 START
---------------------------------------------------------------------------------------
GO
PRINT 'BLG_268 START'
DECLARE @RECORDID 	UNIQUEIDENTIFIER 
DECLARE @ORGID 		UNIQUEIDENTIFIER 

SET @RECORDID = '577CD62A-91E5-436E-B4E1-3608C50935D3'
SET @ORGID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE Id = @RECORDID)			
BEGIN
	 Print 'INSERT-KRT_Parameterek-ALKALMAZASGAZDA_EMAIL_CIM' 
	 INSERT INTO KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) VALUES (
		 @ORGID
		,@RECORDID
		,'ALKALMAZASGAZDA_EMAIL_CIM'
		,'contentum@axis.hu'
		,'1'
		,'ALKALMAZASGAZDA_EMAIL_CIM'
		); 
END

GO

DECLARE @RECORDID 	UNIQUEIDENTIFIER 
DECLARE @ORGID 		UNIQUEIDENTIFIER 

SET @RECORDID = '577CD62A-91E5-436E-B4E1-3608C50935D4'
SET @ORGID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE Id = @RECORDID)			
BEGIN
	 Print 'INSERT-KRT_Parameterek-FELHASZNALO_CSOPORT_TAGSAG_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_SABLON' 
	 INSERT INTO KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) VALUES (
		 @ORGID
		,@RECORDID
		,'FELHASZNALO_CSOPORT_TAGSAG_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_SABLON'
		, ''
		,'1'
		,'
<html>
<body>
    <h1>
        Értesítés : Ügy jogosultság(ok) felülvizsgálatának szükségességéről
    </h1>
	<h4>
        Oka: Felhasználó csoport tagság törlés
    </h4>
    <br />
	<h6>
		Irat átadás szükséges, ha az irat helye, felelőse megváltozott.
		<br />
		Jogosultsági listáról eltávolítás is szükséges lehet.
		<br />
	    A felhasználóhoz rendelt tételek elérhetőek az elszámoltatási jegyzőkönyvben.
		<br />
    </h6>
    <br />
    <h5>Érintett ügyek:</h5>
	<table style="border: 1px solid black;border-collapse: collapse;">
        <tr>
             <th style="background-color: #4CAF50;color: white;">Felhasználó</th>
             <th style="background-color: #4CAF50;color: white;">Ügy típus</th>
			 <th style="background-color: #4CAF50;color: white;">Ügy azonosító</th>
        </tr>
        {0}
    </table>

    <br /><br />

    <small>Contentum.NET</small>
</body>
</html>
		'
		); 
END

GO

DECLARE @RECORDID 	UNIQUEIDENTIFIER 
DECLARE @ORGID 		UNIQUEIDENTIFIER 

SET @RECORDID = '577CD62A-91E5-436E-B4E1-3608C50935E4'
SET @ORGID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE Id = @RECORDID)			
BEGIN
	 Print 'INSERT-KRT_Parameterek-FELHASZNALO_CSOPORT_TAGSAG_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_TARGY' 
	 INSERT INTO KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) VALUES (
		 @ORGID
		,@RECORDID
		,'FELHASZNALO_CSOPORT_TAGSAG_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_TARGY'
		,'Értesítés - Felhasználó csoport tagság törlés - Ügy jogosultság(ok) felülvizsgálata'
		,'1'
		,'FELHASZNALO_CSOPORT_TAGSAG_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_TARGY'
		); 
END

GO

DECLARE @RECORDID 	UNIQUEIDENTIFIER 
DECLARE @ORGID 		UNIQUEIDENTIFIER 

SET @RECORDID = '577CD62A-91E5-436E-B4E1-3608C50935D5'
SET @ORGID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE Id = @RECORDID)			
BEGIN
    Print 'INSERT-KRT_Parameterek-FELHASZNALO_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_SABLON' 
	 INSERT INTO KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) VALUES (
		 @ORGID
		,@RECORDID
		,'FELHASZNALO_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_SABLON'
		,''
		,'1'
		,'
<html>
<body>
    <h1>
        Értesítés : Ügy jogosultság(ok) felülvizsgálatának szükségességéről
    </h1>
	<h4>
        Oka: Felhasználó törlés
   </h4>
    <br />
	<h6>
		Irat átadás szükséges, ha az irat helye, felelőse megváltozott.
		<br />
		Jogosultsági listáról eltávolítás is szükséges lehet.
		<br />
	    A felhasználóhoz rendelt tételek elérhetőek az elszámoltatási jegyzőkönyvben.
		<br />
    </h6>
    <br />
    <h5>Érintett ügyek:</h5>
	<table style="border: 1px solid black;border-collapse: collapse;">
        <tr>
             <th style="background-color: #4CAF50;color: white;">Felhasználó</th>
             <th style="background-color: #4CAF50;color: white;">Ügy típus</th>
			 <th style="background-color: #4CAF50;color: white;">Ügy azonosító</th>
        </tr>
        {0}
    </table>

    <br /><br />

    <small>Contentum.NET</small>
</body>
</html>'
		); 
END
GO

DECLARE @RECORDID 	UNIQUEIDENTIFIER 
DECLARE @ORGID 		UNIQUEIDENTIFIER 

SET @RECORDID = '577CD62A-91E5-436E-B4E1-3608C50935E5'
SET @ORGID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE Id = @RECORDID)			
BEGIN
     Print 'INSERT-KRT_Parameterek-FELHASZNALO_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_TARGY' 
	 INSERT INTO KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) VALUES (
		 @ORGID
		,@RECORDID
		,'FELHASZNALO_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_TARGY'
		,'Értesítés - Felhasználó törlés - Ügy jogosultság(ok) felülvizsgálata'
		,'1'
		,'FELHASZNALO_TORLES_KAPCSOLT_UGYEK_ERTESITES_EMAIL_TARGY'
		); 
END
PRINT 'BLG_268 STOP'
GO
---------------------------------------------------------------------------------------
--BLG_268 STOP
---------------------------------------------------------------------------------------



PRINT '------START 3233-----------'

	DECLARE @ORG_ID UNIQUEIDENTIFIER 
	SET @ORG_ID ='450B510A-7CAA-46B0-83E3-18445C0C53A9'

	DECLARE @RECORD_ID_OCR_3233 UNIQUEIDENTIFIER
	SET @RECORD_ID_OCR_3233 = 'B5965DFE-F931-430B-8DB9-2D8A791CE130'

	IF NOT EXISTS(SELECT 1 FROM KRT_PARAMETEREK WHERE ID =@RECORD_ID_OCR_3233)
	BEGIN 
 		 Print 'INSERT SCAN_OCR_ENABLED' 
		 INSERT INTO KRT_Parameterek(
			 Org
			,Id
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 @ORG_ID
			,@RECORD_ID_OCR_3233
			,'SCAN_OCR_ENABLED'
			,'0'
			,'1'
			,'0 - a feltöltött fájlok utólag nem OCR-ezhetők, 
			  1 - a feltöltött fájlok OCR-ezése utólag indítható'
			); 
	 END 

 
    GO

	DECLARE @KodCsoport_Id UNIQUEIDENTIFIER
	SET @KodCsoport_Id = 'CEF6A0DC-BB5D-42C4-9AF2-2281C0F89994'

	IF NOT EXISTS(select Id from KRT_KodCsoportok where Id=@KodCsoport_Id)
	BEGIN 
		
		Print 'INSERT KCS_OCR_Allapot' 
		INSERT INTO KRT_KodCsoportok(
	 BesorolasiSema
	,Id
	,Kod
	,Nev
	,Modosithato
	,Note
	) values (
	'0'
	,@KodCsoport_Id
	,'OCR_Allapot'
	,'OCR Állapot'
	,'1'
	, 'OCR Allapot'
	); 

	END 
	-------------------------------------------
	DECLARE @KT_OCR1_RECORDID UNIQUEIDENTIFIER
	SET @KT_OCR1_RECORDID = 'F942EEC4-08E8-42DB-8427-C4FD4BF1C7D1'

	IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @KT_OCR1_RECORDID)
	BEGIN 
		 Print 'INSERT KRT_Kodtarak - OCR_Allapot 1' 

		 INSERT INTO KRT_KodTarak
		 (
			 Id,
			 Org,
			 KodCsoport_Id,
			 Kod,
			 Nev,
			 Sorrend,
			 Modosithato
		) 
		values (
			@KT_OCR1_RECORDID,
			'450B510A-7CAA-46B0-83E3-18445C0C53A9',
			@KodCsoport_Id,
			'1',
			'OCR Ezendo Dokumentum',
			'01',
			'0'
		); 
	 
	 END
	 ---------------------------------------------
	DECLARE @KT_OCR2_RECORDID UNIQUEIDENTIFIER
	SET @KT_OCR2_RECORDID = 'F942EEC4-08E8-42DB-8427-C4FD4BF1C7D2'

	IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @KT_OCR2_RECORDID)
	BEGIN 
		 Print 'INSERT KRT_Kodtarak - OCR_Allapot 2' 

		 INSERT INTO KRT_KodTarak
		 (
			 Id,
			 Org,
			 KodCsoport_Id,
			 Kod,
			 Nev,
			 Sorrend,
			 Modosithato
		) 
		values (
			@KT_OCR2_RECORDID,
			'450B510A-7CAA-46B0-83E3-18445C0C53A9',
			@KodCsoport_Id,
			'2',
			'OCR Alatt Levo Dokumentum',
			'02',
			'0'
		); 
	 
	 END
	 ---------------------------------------------
	DECLARE @KT_OCR3_RECORDID UNIQUEIDENTIFIER
	SET @KT_OCR3_RECORDID = 'F942EEC4-08E8-42DB-8427-C4FD4BF1C7D3'

	IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @KT_OCR3_RECORDID)
	BEGIN 
		 Print 'INSERT KRT_Kodtarak - OCR_Allapot 3' 

		 INSERT INTO KRT_KodTarak
		 (
			 Id,
			 Org,
			 KodCsoport_Id,
			 Kod,
			 Nev,
			 Sorrend,
			 Modosithato
		) 
		values (
			@KT_OCR3_RECORDID,
			'450B510A-7CAA-46B0-83E3-18445C0C53A9',
			@KodCsoport_Id,
			'3',
			'OCR Ezett Dokumentum',
			'03',
			'0'
		); 
	 
	 END
 GO

 PRINT '------STOP 3233-----------'
GO
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='1B3E8BF1-93C1-E811-80CE-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

print 'SIGNED_XML_PATH'
if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'1B3E8BF1-93C1-E811-80CE-00155D020DD3'
		,'SIGNED_XML_PATH'
		,'/ev/2018/csatolmanyok/signedxml'
		,'1'
		,'Az (aláírt) XML helye a dokumentum tárba'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='1B3E8BF1-93C1-E811-80CE-00155D020DD3'
	--	,Nev='SIGNED_XML_PATH'
	--	,Ertek='/ev/2018/csatolmanyok/signedxml'
	--	,Karbantarthato='1'
	--	,Note='Az (aláírt) XML helye a dokumentum tárba'
	-- WHERE Id=@record_id 
 --END
 
GO
----------------------------------------------------------------------------------------------------------------------------

---------------------------------KRX_BEERKEZES_MODJA------------------------------------------------------------------------

declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '08DA76C0-F8BA-49E1-8220-E849C7BB38BF'

IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @kodcsoportId)
BEGIN
	print 'Add KRT_KodCsoportok - KRX_BEERKEZES_MODJA'

	insert into KRT_KodCsoportok
	(
	     Id
		,Kod
		,Nev
		,Modosithato
		,Note
		,BesorolasiSema
	) 
	values
	(
		@kodcsoportId
		,'KRX_BEERKEZES_MODJA'
		,'KRX beérkezés módja'
		,'1'
		, 'KRX metaadataiban lehetséges beérkezési módok'
		,'0'
	)

END


declare @kodtarId uniqueidentifier

-- elektronikus
set @kodtarId =  '76009526-ea34-46cc-b767-cd828cf28f3c'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - elektronikus' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.01',
    'elektronikus',
    '01',
    '1'
  ); 
   
END

-- egyéb
set @kodtarId =  '069b9708-d062-4226-8d02-f737d3e3ee44'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - egyéb' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.02',
    'egyéb',
    '02',
    '1'
  ); 
   
END

-- futár
set @kodtarId =  '85704eeb-ae1e-420a-8c65-36260f84368c'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - futár' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.03',
    'futár',
    '03',
    '1'
  ); 
   
END

-- kézbesítő útján
set @kodtarId =  '1a5d43c7-206a-43e1-a442-5ad38bd246bc'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - kézbesítő útján' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.04',
    'kézbesítő útján',
    '04',
    '1'
  ); 
   
END

-- posta
set @kodtarId =  '09726540-8517-4c7f-8402-2163174ba626'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - posta' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.05',
    'posta',
    '05',
    '1'
  ); 
   
END

-- nincs küldés
set @kodtarId =  'c53f2769-bd13-43ed-9397-417c836ed013'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - nincs küldés' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.07',
    'nincs küldés',
    '07',
    '1'
  ); 
   
END

-- belső átadás
set @kodtarId =  '880b9f64-1187-4fe8-8638-a40d4bdd1bbc'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - belső átadás' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.08',
    'belső átadás',
    '08',
    '1'
  ); 
   
END

-- személyes benyújtás
set @kodtarId =  'b7085b25-f35e-4576-b9f9-a490f8e8fd3e'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - személyes benyújtás' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.10',
    'személyes benyújtás',
    '10',
    '1'
  ); 
   
END

-- nova szeüsz elektronikus küldés
set @kodtarId =  'bea4de30-0663-4db4-8d8c-ab9d3abdf208'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - nova szeüsz elektronikus küldés' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.11',
    'nova szeüsz elektronikus küldés',
    '11',
    '1'
  ); 
   
END

-- belső iratelőállítás neo-val
set @kodtarId =  'd700b8bf-c69e-4769-884f-3d506ac3219e'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - belső iratelőállítás neo-val' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.12',
    'belső iratelőállítás neo-val',
    '12',
    '1'
  ); 
   
END

-- ügyfélkapu (elektronikus küldés állampolgárnak)
set @kodtarId =  '377546dd-33e5-4796-8256-270a396b35dc'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - ügyfélkapu (elektronikus küldés állampolgárnak)' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.13',
    'ügyfélkapu (elektronikus küldés állampolgárnak)',
    '13',
    '1'
  ); 
   
END

-- hivatali kapu (elektronikus küldés hivatalnak)
set @kodtarId =  'ce82f2af-64ca-4847-9b41-1f0cabe95142'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - hivatali kapu (elektronikus küldés hivatalnak)' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.14',
    'hivatali kapu (elektronikus küldés hivatalnak)',
    '14',
    '1'
  ); 
   
END

-- e-mail
set @kodtarId =  'f7139761-ba3f-4b50-81a6-5e328fd41330'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - e-mail' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.15',
    'e-mail',
    '15',
    '1'
  ); 
   
END

-- rzs portál
set @kodtarId =  'a3da2a3b-b864-47dc-b47c-7435e69a8c32'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - rzs portál' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.16',
    'rzs portál',
    '16',
    '1'
  ); 
   
END

-- belső ügyészi revízó nzs-val
set @kodtarId =  '5027fbd2-df1f-4605-a494-01f956a07abc'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - belső ügyészi revízó nzs-val' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.17',
    'belső ügyészi revízó nzs-val',
    '17',
    '1'
  ); 
   
END

-- elektronikus (védett rendszerengedéllyel rendelkező)
set @kodtarId =  '52613721-cb6e-4551-aa3f-e8b1588c1572'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - elektronikus (védett rendszerengedéllyel rendelkező)' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.18',
    'elektronikus (védett rendszerengedéllyel rendelkező)',
    '18',
    '1'
  ); 
   
END

-- dhl futárszolgálat
set @kodtarId =  '6dc0b8e3-2b76-4cdf-b636-47f92985758a'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - dhl futárszolgálat' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.19',
    'dhl futárszolgálat',
    '19',
    '1'
  ); 
   
END

-- ah futár
set @kodtarId =  '3fd14db9-62e2-4124-8eb1-16edd5b5aaf5'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - ah futár' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.20',
    'ah futár',
    '20',
    '1'
  ); 
   
END

-- mkü
set @kodtarId =  'e043b046-18a7-434e-8e2e-b216f1a935f5'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - mkü' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.21',
    'mkü',
    '21',
    '1'
  ); 
   
END

-- ih futár
set @kodtarId =  '007626c4-ee30-4453-b837-adbb5b6e63ed'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - ih futár' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.22',
    'ih futár',
    '22',
    '1'
  ); 
   
END

-- diplomáciai futárszolgálat
set @kodtarId =  '7537d0cc-8fe1-4fc2-b5f3-ab5bbe8ff3e3'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - diplomáciai futárszolgálat' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.23',
    'diplomáciai futárszolgálat',
    '23',
    '1'
  ); 
   
END

-- belső futár
set @kodtarId =  '4373e98a-ae51-4056-a0ec-719d73364834'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - belső futár' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.24',
    'belső futár',
    '24',
    '1'
  ); 
   
END

-- állami futárszolgálat
set @kodtarId =  '089299dc-7653-49c8-85a2-76fcc57ad445'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - állami futárszolgálat' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.25',
    'állami futárszolgálat',
    '25',
    '1'
  ); 
   
END

-- kézbesítő útján
set @kodtarId =  '051bd0c0-5c77-41b8-b657-2dfd1a3f892d'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - kézbesítő útján' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.26',
    'kézbesítő útján',
    '26',
    '1'
  ); 
   
END

-- postai ajánlott
set @kodtarId =  '5795adea-c671-46ba-8ae7-4bf7109328ed'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - postai ajánlott' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.27',
    'postai ajánlott',
    '27',
    '1'
  ); 
   
END

-- postai csomag küldemény
set @kodtarId =  '8e40ab27-ac8a-4c91-a7ee-8dad1dd6a8fa'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - postai csomag küldemény' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.28',
    'postai csomag küldemény',
    '28',
    '1'
  ); 
   
END

-- postai elsőbbségi
set @kodtarId =  '4e1ec8b5-6741-4783-808d-e7fed768e8f5'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - postai elsőbbségi' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.29',
    'postai elsőbbségi',
    '29',
    '1'
  ); 
   
END

-- postai értéklevél
set @kodtarId =  'bb506edd-9ecb-427a-bea4-47f65257c4b8'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - postai értéklevél' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.30',
    'postai értéklevél',
    '30',
    '1'
  ); 
   
END

-- belső posta
set @kodtarId =  'd7c93877-90bf-4ef9-a716-8b4ee99945ba'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - belső posta' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.31',
    'belső posta',
    '31',
    '1'
  ); 
   
END

-- irm
set @kodtarId =  '710c7ddd-8958-4d26-b671-f2c4190e374a'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - irm' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.32',
    'irm',
    '32',
    '1'
  ); 
   
END

-- postai sima
set @kodtarId =  'fe3b9fca-1b00-458a-bc45-f50fc9284779'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - postai sima' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.33',
    'postai sima',
    '33',
    '1'
  ); 
   
END

-- postai légiposta
set @kodtarId =  '183d4d88-11e6-4fc2-a12b-160eadd2b4ee'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - postai légiposta' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.34',
    'postai légiposta',
    '34',
    '1'
  ); 
   
END

-- postai értékbiztosított csomag
set @kodtarId =  'c514cdd3-d517-4331-bf3b-d3f1748cba76'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - postai értékbiztosított csomag' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.35',
    'postai értékbiztosított csomag',
    '35',
    '1'
  ); 
   
END

-- postai tértivevényes
set @kodtarId =  '70c2d6d0-3e8c-436f-ab9a-64027414c65e'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - postai tértivevényes' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.36',
    'postai tértivevényes',
    '36',
    '1'
  ); 
   
END

-- postai távirat
set @kodtarId =  '42440122-c0bb-4fed-8b23-296b12638e1e'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - postai távirat' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.37',
    'postai távirat',
    '37',
    '1'
  ); 
   
END

-- telefax
set @kodtarId =  '200c21a3-28cd-40e1-a7e0-5da5fe6b19d5'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - telefax' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.38',
    'telefax',
    '38',
    '1'
  ); 
   
END

-- telefax (rejtjelezett)
set @kodtarId =  '5b538dba-e5e7-4340-9112-96e7eed25d90'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
BEGIN 
  Print 'INSERT KRT_Kodtarak - KRX_BEERKEZES_MODJA - telefax (rejtjelezett)' 

  insert into KRT_KodTarak
  (
    Id,
    Org,
    KodCsoport_Id,
    Kod,
    Nev,
    Sorrend,
    Modosithato
  ) 
  values (
    @kodtarId,
    @org,
    @kodcsoportId,
    'KRX_BEERKEZES_MODJA.39',
    'telefax (rejtjelezett)',
    '39',
    '1'
  ); 
   
END

GO

declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id=@org) 

declare @kodtarFuggosegId uniqueidentifier
set @kodtarFuggosegId = '068DE84A-4B30-4213-BAA7-C457EAE1117F'

declare @adat nvarchar(max)

-- hivatali kapu helyett elhisz
if @org_kod = 'NMHH'
BEGIN
	set @adat = '{"VezerloKodCsoportId":"08da76c0-f8ba-49e1-8220-e849c7bb38bf","FuggoKodCsoportId":"6982f668-c8cd-47a3-ac62-9f16e439473c",
"Items":[{"VezerloKodTarId":"76009526-ea34-46cc-b767-cd828cf28f3c","FuggoKodtarId":"42496278-92f6-4c45-96fe-d0356281270a","Aktiv":true},
{"VezerloKodTarId":"85704eeb-ae1e-420a-8c65-36260f84368c","FuggoKodtarId":"13c07c55-9330-4801-a30d-eaa04e540daa","Aktiv":true},
{"VezerloKodTarId":"1a5d43c7-206a-43e1-a442-5ad38bd246bc","FuggoKodtarId":"70436f6b-396e-4994-a720-184689023aa1","Aktiv":true},
{"VezerloKodTarId":"09726540-8517-4c7f-8402-2163174ba626","FuggoKodtarId":"ed318e7b-2642-4491-809c-5e8ddc38d7a9","Aktiv":true},
{"VezerloKodTarId":"288b7301-3aba-4b52-87e5-47509c88b999","FuggoKodtarId":"e590c4a5-6bbf-46a9-8933-85a62a9e84a2","Aktiv":true},
{"VezerloKodTarId":"c53f2769-bd13-43ed-9397-417c836ed013","FuggoKodtarId":"f9b80f0d-fe46-4921-8b66-359b0e384aef","Aktiv":true},
{"VezerloKodTarId":"880b9f64-1187-4fe8-8638-a40d4bdd1bbc","FuggoKodtarId":"f9b80f0d-fe46-4921-8b66-359b0e384aef","Aktiv":true},
{"VezerloKodTarId":"b7085b25-f35e-4576-b9f9-a490f8e8fd3e","FuggoKodtarId":"14a94d2f-0916-49d5-94b1-ac9eb2bb6392","Aktiv":true},
{"VezerloKodTarId":"377546dd-33e5-4796-8256-270a396b35dc","FuggoKodtarId":"cc0d5f87-279b-45c1-905b-dd721a1fb870","Aktiv":true},
{"VezerloKodTarId":"ce82f2af-64ca-4847-9b41-1f0cabe95142","FuggoKodtarId":"cc0d5f87-279b-45c1-905b-dd721a1fb870","Aktiv":true},
{"VezerloKodTarId":"f7139761-ba3f-4b50-81a6-5e328fd41330","FuggoKodtarId":"42496278-92f6-4c45-96fe-d0356281270a","Aktiv":true},
{"VezerloKodTarId":"6dc0b8e3-2b76-4cdf-b636-47f92985758a","FuggoKodtarId":"072d82f1-0220-448f-9aa8-9e646bde3b8d","Aktiv":true},
{"VezerloKodTarId":"3fd14db9-62e2-4124-8eb1-16edd5b5aaf5","FuggoKodtarId":"072d82f1-0220-448f-9aa8-9e646bde3b8d","Aktiv":true},
{"VezerloKodTarId":"007626c4-ee30-4453-b837-adbb5b6e63ed","FuggoKodtarId":"072d82f1-0220-448f-9aa8-9e646bde3b8d","Aktiv":true},
{"VezerloKodTarId":"7537d0cc-8fe1-4fc2-b5f3-ab5bbe8ff3e3","FuggoKodtarId":"632664b7-c298-4c2c-9f9e-f7122115ea75","Aktiv":true},
{"VezerloKodTarId":"089299dc-7653-49c8-85a2-76fcc57ad445","FuggoKodtarId":"13c07c55-9330-4801-a30d-eaa04e540daa","Aktiv":true},
{"VezerloKodTarId":"5795adea-c671-46ba-8ae7-4bf7109328ed","FuggoKodtarId":"ed318e7b-2642-4491-809c-5e8ddc38d7a9","Aktiv":true},
{"VezerloKodTarId":"fe3b9fca-1b00-458a-bc45-f50fc9284779","FuggoKodtarId":"ed318e7b-2642-4491-809c-5e8ddc38d7a9","Aktiv":true},
{"VezerloKodTarId":"183d4d88-11e6-4fc2-a12b-160eadd2b4ee","FuggoKodtarId":"e7a6fc86-2de6-4a9c-aa70-2bd3c4328b88","Aktiv":true},
{"VezerloKodTarId":"70c2d6d0-3e8c-436f-ab9a-64027414c65e","FuggoKodtarId":"35654f8c-a8fc-4d64-a46d-82007c4fcfb9","Aktiv":true},
{"VezerloKodTarId":"42440122-c0bb-4fed-8b23-296b12638e1e","FuggoKodtarId":"3512557f-3e1b-44ef-b341-54775356a5d7","Aktiv":true},
{"VezerloKodTarId":"5b538dba-e5e7-4340-9112-96e7eed25d90","FuggoKodtarId":"e590c4a5-6bbf-46a9-8933-85a62a9e84a2","Aktiv":true}],
"ItemsNincs":[]}'
END
ELSE
BEGIN
	set @adat = '{"VezerloKodCsoportId":"08da76c0-f8ba-49e1-8220-e849c7bb38bf","FuggoKodCsoportId":"6982f668-c8cd-47a3-ac62-9f16e439473c",
"Items":[{"VezerloKodTarId":"76009526-ea34-46cc-b767-cd828cf28f3c","FuggoKodtarId":"42496278-92f6-4c45-96fe-d0356281270a","Aktiv":true},
{"VezerloKodTarId":"85704eeb-ae1e-420a-8c65-36260f84368c","FuggoKodtarId":"13c07c55-9330-4801-a30d-eaa04e540daa","Aktiv":true},
{"VezerloKodTarId":"1a5d43c7-206a-43e1-a442-5ad38bd246bc","FuggoKodtarId":"70436f6b-396e-4994-a720-184689023aa1","Aktiv":true},
{"VezerloKodTarId":"09726540-8517-4c7f-8402-2163174ba626","FuggoKodtarId":"ed318e7b-2642-4491-809c-5e8ddc38d7a9","Aktiv":true},
{"VezerloKodTarId":"288b7301-3aba-4b52-87e5-47509c88b999","FuggoKodtarId":"e590c4a5-6bbf-46a9-8933-85a62a9e84a2","Aktiv":true},
{"VezerloKodTarId":"c53f2769-bd13-43ed-9397-417c836ed013","FuggoKodtarId":"f9b80f0d-fe46-4921-8b66-359b0e384aef","Aktiv":true},
{"VezerloKodTarId":"880b9f64-1187-4fe8-8638-a40d4bdd1bbc","FuggoKodtarId":"f9b80f0d-fe46-4921-8b66-359b0e384aef","Aktiv":true},
{"VezerloKodTarId":"b7085b25-f35e-4576-b9f9-a490f8e8fd3e","FuggoKodtarId":"14a94d2f-0916-49d5-94b1-ac9eb2bb6392","Aktiv":true},
{"VezerloKodTarId":"377546dd-33e5-4796-8256-270a396b35dc","FuggoKodtarId":"b5a320e1-dbb3-4c9a-b131-8189008f22f1","Aktiv":true},
{"VezerloKodTarId":"ce82f2af-64ca-4847-9b41-1f0cabe95142","FuggoKodtarId":"b5a320e1-dbb3-4c9a-b131-8189008f22f1","Aktiv":true},
{"VezerloKodTarId":"f7139761-ba3f-4b50-81a6-5e328fd41330","FuggoKodtarId":"42496278-92f6-4c45-96fe-d0356281270a","Aktiv":true},
{"VezerloKodTarId":"6dc0b8e3-2b76-4cdf-b636-47f92985758a","FuggoKodtarId":"072d82f1-0220-448f-9aa8-9e646bde3b8d","Aktiv":true},
{"VezerloKodTarId":"3fd14db9-62e2-4124-8eb1-16edd5b5aaf5","FuggoKodtarId":"072d82f1-0220-448f-9aa8-9e646bde3b8d","Aktiv":true},
{"VezerloKodTarId":"007626c4-ee30-4453-b837-adbb5b6e63ed","FuggoKodtarId":"072d82f1-0220-448f-9aa8-9e646bde3b8d","Aktiv":true},
{"VezerloKodTarId":"7537d0cc-8fe1-4fc2-b5f3-ab5bbe8ff3e3","FuggoKodtarId":"632664b7-c298-4c2c-9f9e-f7122115ea75","Aktiv":true},
{"VezerloKodTarId":"089299dc-7653-49c8-85a2-76fcc57ad445","FuggoKodtarId":"13c07c55-9330-4801-a30d-eaa04e540daa","Aktiv":true},
{"VezerloKodTarId":"5795adea-c671-46ba-8ae7-4bf7109328ed","FuggoKodtarId":"ed318e7b-2642-4491-809c-5e8ddc38d7a9","Aktiv":true},
{"VezerloKodTarId":"fe3b9fca-1b00-458a-bc45-f50fc9284779","FuggoKodtarId":"ed318e7b-2642-4491-809c-5e8ddc38d7a9","Aktiv":true},
{"VezerloKodTarId":"183d4d88-11e6-4fc2-a12b-160eadd2b4ee","FuggoKodtarId":"e7a6fc86-2de6-4a9c-aa70-2bd3c4328b88","Aktiv":true},
{"VezerloKodTarId":"70c2d6d0-3e8c-436f-ab9a-64027414c65e","FuggoKodtarId":"35654f8c-a8fc-4d64-a46d-82007c4fcfb9","Aktiv":true},
{"VezerloKodTarId":"42440122-c0bb-4fed-8b23-296b12638e1e","FuggoKodtarId":"3512557f-3e1b-44ef-b341-54775356a5d7","Aktiv":true},
{"VezerloKodTarId":"5b538dba-e5e7-4340-9112-96e7eed25d90","FuggoKodtarId":"e590c4a5-6bbf-46a9-8933-85a62a9e84a2","Aktiv":true}],
"ItemsNincs":[]}'
END



IF NOT EXISTS (SELECT 1 FROM KRT_KodtarFuggoseg WHERE Id = @kodtarFuggosegId)
BEGIN 
  Print 'INSERT KRT_KodtarFuggosegek - KRX_BEERKEZES_MODJA - KULDEMENY_KULDES_MODJA' 

  insert into KRT_KodtarFuggoseg
  (
    Id,
    Org,
    Vezerlo_KodCsoport_Id,
    Fuggo_KodCsoport_Id,
    Adat,
    Aktiv
  ) 
  values (
    @kodtarFuggosegId,
    @org,
    '08DA76C0-F8BA-49E1-8220-E849C7BB38BF',
    '6982F668-C8CD-47A3-AC62-9F16E439473C',
    @adat,
    '1'
  ); 
   
END
ELSE
BEGIN
    Print 'UPDATE KRT_KodtarFuggosegek - KRX_BEERKEZES_MODJA - KULDEMENY_KULDES_MODJA' 
	update KRT_KodtarFuggoseg
		set Adat = @adat
	where Id = @kodtarFuggosegId
END

GO
----------------------------------------------------------------------------------------------------------------------------
-- EMAIL_KIMENO_TERTIVEVENY rendszerparameter	 
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='B7A376F8-E2BF-E811-80CE-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 


if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'B7A376F8-E2BF-E811-80CE-00155D020DD3'
		,'EMAIL_KIMENO_TERTIVEVENY'
		,'0'
		,'1'
		,'add-in-ból kimenő e-mail iktatás: 0 - esetén nem kötelező az olvasási visszajelzés, nincs tértivevény bejegyzés, 1 - olvasási visszaigazolás kérés bejelölésre kerül, tértivevény bejegyzés kerül rögzítésre a kimenő küldeményhez'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='B7A376F8-E2BF-E811-80CE-00155D020DD3'
	--	,Nev='EMAIL_KIMENO_TERTIVEVENY'
	--	,Ertek='0'
	--	,Karbantarthato='1'
	--	,Note='add-in-ból kimenő e-mail iktatás: 0 - esetén nem kötelező az olvasási visszajelzés, nincs tértivevény bejegyzés, 1 - olvasási visszaigazolás kérés bejelölésre kerül, tértivevény bejegyzés kerül rögzítésre a kimenő küldeményhez'
	-- WHERE Id=@record_id 
 --END
 ----------------------------------------------------------------------------------------------------------------------------

PRINT 'BLG_2956'

-------------------------------- BLG 2956 14. § Az ISZ az ügyirathoz és az iktatott irathoz kapcsolódóan képes támogatni a kiadott feladat továbbdelegálásának lehetőségét --------------------------------------------------------------------------------
--------------------------------------------------------------------------- Művelet hozzáadása ------------------------------------------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM KRT_Muveletek WHERE ID ='C3C40E32-6B86-4410-AFB0-3C678D766F1C')
INSERT INTO [dbo].[KRT_Muveletek]
           ([Id]
           ,[Org]
           ,[Kod]
           ,[Nev]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('C3C40E32-6B86-4410-AFB0-3C678D766F1C'
           ,NULL
           ,'Delegalas'
           ,'Delegálás'
           ,1
           ,'Feladat delegálása'
           ,'A0848405-E664-4E79-8FAB-CFECA4A290AF'
           ,'2018-09-19'
           ,'4700-12-31 00:00:00.000'
           ,NULL
           ,'2018-09-19'
           ,NULL 
           ,NULL 
           ,NULL
           ,NULL
           ,'F60AD910-541A-470D-B888-DA5A6A061668'
           ,NULL)
GO
-------------------------------- BLG 2956 14. § Az ISZ az ügyirathoz és az iktatott irathoz kapcsolódóan képes támogatni a kiadott feladat továbbdelegálásának lehetőségét --------------------------------------------------------------------------------
--------------------------------------------------------------------------- Művelet hozzáadása ------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------- VÉGE --------------------------------------------------------------------------------------------------------------------------------------------------------------------


-------------------------------- BLG 2956 14. § Az ISZ az ügyirathoz és az iktatott irathoz kapcsolódóan képes támogatni a kiadott feladat továbbdelegálásának lehetőségét --------------------------------------------------------------------------------
------------------------------------------------------------------ Feladat állapot hozzáadása kódtárak táblához -------------------------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM KRT_KODTARAK WHERE ID ='ca9f5afb-f94c-487b-b928-f98ad30d5b4b')
INSERT INTO [dbo].[KRT_KodTarak]
           ([Id]
           ,[Org]
           ,[KodCsoport_Id]
           ,[ObjTip_Id_AdatElem]
           ,[Obj_Id]
           ,[Kod]
           ,[Nev]
           ,[RovidNev]
           ,[Egyeb]
           ,[Modosithato]
           ,[Sorrend]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('ca9f5afb-f94c-487b-b928-f98ad30d5b4b',
           '450B510A-7CAA-46B0-83E3-18445C0C53A9',
           '36E2E42D-BA30-4FBB-8AA0-2D79AB57C5C8',
           NULL,
           NULL,
          '7',
           'Delegált',
           'Delegált',
           'Delegált feladat',
           1,
           11,
           1,
           NULL,
           NULL,
            '2018-09-18',
           '4700-12-31',
            NULL,
           '2018-09-18',
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
			)
-------------------------------- BLG 2956 14. § Az ISZ az ügyirathoz és az iktatott irathoz kapcsolódóan képes támogatni a kiadott feladat továbbdelegálásának lehetőségét --------------------------------------------------------------------------------
----------------------------------------------------------------- Feladat állapot hozzáadása kódtárak táblához --------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------- VÉGE --------------------------------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------- BLG 2956 14. § Az ISZ az ügyirathoz és az iktatott irathoz kapcsolódóan képes támogatni a kiadott feladat továbbdelegálásának lehetőségét --------------------------------------------------------------------------------
------------------------------------------------------------------ Delegálás funkció hozzáadása funkciók táblához -------------------------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM KRT_FUNKCIOK WHERE ID ='32B273D9-9B56-4EC8-8C32-A88E9BB265A6')
INSERT INTO [dbo].[KRT_Funkciok]
           ([Id]
           ,[Kod]
           ,[Nev]
           ,[ObjTipus_Id_AdatElem]
           ,[ObjStat_Id_Kezd]
           ,[Alkalmazas_Id]
           ,[Muvelet_Id]
           ,[ObjStat_Id_Veg]
           ,[Leiras]
           ,[Funkcio_Id_Szulo]
           ,[Csoportosito]
           ,[Modosithato]
           ,[Org]
           ,[MunkanaploJelzo]
           ,[FeladatJelzo]
           ,[KeziFeladatJelzo]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('32B273D9-9B56-4EC8-8C32-A88E9BB265A6',
		   'FeladatDelegate',
		   'Feladat delegálás',
		   '5E90E91A-78FF-44F5-A89D-39B712C905F3',
		   NULL,
           '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3',
           'C3C40E32-6B86-4410-AFB0-3C678D766F1C',
           NULL,
           'Feladat delegálása',
           NULL,
           NULL,
           0,
           NULL,
           NULL,
           NULL,
           NULL,
           1,
           NULL,
           'A0848405-E664-4E79-8FAB-CFECA4A290AF',
           '2018-09-19',
           '4700-12-31',
           NULL,
           '2018-09-19',
           NULL,
           NULL,
           NULL,
           NULL,
           'F60AD910-541A-470D-B888-DA5A6A061668',
           NULL


)
GO
-------------------------------- BLG 2956 14. § Az ISZ az ügyirathoz és az iktatott irathoz kapcsolódóan képes támogatni a kiadott feladat továbbdelegálásának lehetőségét --------------------------------------------------------------------------------
----------------------------------------------------------------- Delegálás funkció hozzáadása funkciók táblához --------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------- VÉGE --------------------------------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------- BLG 2956 14. § Az ISZ az ügyirathoz és az iktatott irathoz kapcsolódóan képes támogatni a kiadott feladat továbbdelegálásának lehetőségét --------------------------------------------------------------------------------
------------------------------------------------------------------ Delegálás funkció hozzárendelése az adminisztrátor szerepkörhöz -------------------------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM KRT_SZEREPKOR_FUNKCIO WHERE ID ='2963DA77-8D1F-40A8-B271-5F2692FADD38')
INSERT INTO [dbo].[KRT_Szerepkor_Funkcio]
           ([Id]
           ,[Funkcio_Id]
           ,[Szerepkor_Id]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('2963DA77-8D1F-40A8-B271-5F2692FADD38',
           '32B273D9-9B56-4EC8-8C32-A88E9BB265A6',
           'E855F681-36ED-41B6-8413-576FCB5D1542',
           1,
           NULL,
           NULL,
           '2018-09-17',
           '4700-12-31',
           NULL,
           '2018-09-17',
           NULL,
		   NULL,
		   NULL,
		   NULL,
		   NULL,
		   NULL)
GO
-------------------------------- BLG 2956 14. § Az ISZ az ügyirathoz és az iktatott irathoz kapcsolódóan képes támogatni a kiadott feladat továbbdelegálásának lehetőségét --------------------------------------------------------------------------------
----------------------------------------------------------------- Delegálás funkció hozzárendelése az adminisztrátor szerepkörhöz --------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------- VÉGE --------------------------------------------------------------------------------------------------------------------------------------------------------------------



PRINT 'BLG_2936'

-------------------------------------------------------------------------------------------
--BLG_2936 START
-------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------
---------------------------- SoapMessageExcelExport funkció hozzáadása --------------------
-------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM KRT_FUNKCIOK WHERE ID ='591921AE-4BA7-4357-8886-3C788E356D0F')
INSERT INTO [dbo].[KRT_Funkciok]
           ([Id]
           ,[Kod]
           ,[Nev]
           ,[ObjTipus_Id_AdatElem]
           ,[ObjStat_Id_Kezd]
           ,[Alkalmazas_Id]
           ,[Muvelet_Id]
           ,[ObjStat_Id_Veg]
           ,[Leiras]
           ,[Funkcio_Id_Szulo]
           ,[Csoportosito]
           ,[Modosithato]
           ,[Org]
           ,[MunkanaploJelzo]
           ,[FeladatJelzo]
           ,[KeziFeladatJelzo]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('591921ae-4ba7-4357-8886-3c788e356d0f',
		   'SoapMessageExcelExport',
		   'SoapMessage excel export',
		   '093B9194-1515-4691-B0E5-6B1C9006F3CD',
		   NULL,
           '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3',
           '594DC394-395E-4EB6-8E32-F05A8548D460',
           NULL,
           'Lista exportálása Excelbe.',
           '11B4905F-BD56-4742-938B-E768B94F8F42',
           NULL,
           0,
           NULL,
           NULL,
           NULL,
           NULL,
           1,
           NULL,
           'A0848405-E664-4E79-8FAB-CFECA4A290AF',
           '2018-09-17',
           '4700-12-31',
           NULL,
           '2018-09-17',
           NULL,
           NULL,
           NULL,
           NULL,
           'F60AD910-541A-470D-B888-DA5A6A061668',
           NULL


)
-------------------------------------------------------------------------------------------
---------------------------- SoapMessageExcelExport funkció hozzáadása --------------------
----------------------------------------------- VÉGE --------------------------------------

-------------------------------------------------------------------------------------------
-------------------- SoapMessageExcelExport szerepkörhöz funkció hozzárendelése -----------
-------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM KRT_SZEREPKOR_FUNKCIO WHERE ID ='0D789A94-D579-4AEF-8C25-8F50D4E85B94')
INSERT INTO [dbo].[KRT_Szerepkor_Funkcio]
           ([Id]
           ,[Funkcio_Id]
           ,[Szerepkor_Id]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('0d789a94-d579-4aef-8c25-8f50d4e85b94',
           '591921AE-4BA7-4357-8886-3C788E356D0F',
           'E855F681-36ED-41B6-8413-576FCB5D1542',
           1,
           NULL,
           NULL,
           '2018-09-17',
           '4700-12-31',
           NULL,
           '2018-09-17',
           NULL,
		   NULL,
		   NULL,
		   NULL,
		   NULL,
		   NULL)
GO



-------------------------------------------------------------------------------------------
-------------------- SoapMessageExcelExport szerepkörhöz funkció hozzárendelése -----------
----------------------------------------------- VÉGE --------------------------------------

-------------------------------------------------------------------------------------------
--BLG_2936 END
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- BLG 232 - 10. Kettős iktatás elkerülésének támogatása 
-- A kézi kiadásIrat iktatásakor figyelmeztető üzenetet kell megjeleníteni a felhasználónak, amennyiben a hivatkozási szám (EREC_IraIratok.HivatkozasiSzam) mezőbe olyan érték kerülne, amely már szerepel az iratok táblában. 
-- Az ellenőrzés rendszerparaméterrel ki és bekapcsolható legyen. A rendszerparaméter neve HIVSZAM_ELLENORZES legyen , értékei 0 és 1, alapértelmezetten 0 legyen.
-- HIVSZAM_ELLENORZES rendszerparameter	 
-- ertek: 0,1
-- NMHH-ban
-------------------------------------------------------------------------------

DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
where Id='6a93bb65-1f7f-49d0-bc9c-7ddf704d5d1f') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'6a93bb65-1f7f-49d0-bc9c-7ddf704d5d1f'
		,'HIVSZAM_ELLENORZES'
		,'0'
		,'1'
		,'A kézi kiadásIrat iktatásakor figyelmeztető üzenetet kell megjeleníteni a felhasználónak, amennyiben a hivatkozási szám (EREC_IraIratok.HivatkozasiSzam) mezőbe olyan érték kerülne, amely már szerepel az iratok táblában.'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='6a93bb65-1f7f-49d0-bc9c-7ddf704d5d1f'
	--	,Nev='HIVSZAM_ELLENORZES'
	--	,Ertek='0'
	--	,Karbantarthato='1'
	--	,Note='A kézi kiadásIrat iktatásakor figyelmeztető üzenetet kell megjeleníteni a felhasználónak, amennyiben a hivatkozási szám (EREC_IraIratok.HivatkozasiSzam) mezőbe olyan érték kerülne, amely már szerepel az iratok táblában.'
	-- WHERE Id=@record_id 
 --END

 GO
 ---------------------------------------------------------------------------------------------------------------
 -------------------- BLG 232 - 10. Kettős iktatás elkerülésének támogatása  -------------------------

 GO

 DECLARE @record UNIQUEIDENTIFIER
	SET @record = 'C6663933-F9C7-E811-80CE-00155D020DD3'

	IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @record)
	BEGIN 
		 Print 'INSERT KRT_Kodtarak - Szignalt' 

		 INSERT INTO KRT_KodTarak
		 (
			 Id,
			 Org,
			 KodCsoport_Id,
			 Kod,
			 Nev,
			 Sorrend,
			 Modosithato
		) 
		values (
			@record,
			'450B510A-7CAA-46B0-83E3-18445C0C53A9',
			'70AFC286-4308-49ED-AC01-40DDF12C30F4',
			'05',
			'Szignált',
			'9',
			'0'
		); 
	 
	 END

GO

DECLARE @record UNIQUEIDENTIFIER
SET @record = '8B1639F7-DFC6-E811-80CE-00155D020DD3'

IF NOT EXISTS(SELECT 1 FROM KRT_FUNKCIOK WHERE ID= @record)
	BEGIN
		/*-------------------------------------------------------------------------*/
		PRINT 'KRT_Funkcio - Irat szignálás' 
		/*-------------------------------------------------------------------------*/
		 INSERT INTO KRT_Funkciok(
				 Tranz_id
				,Stat_id
				,Alkalmazas_Id
				,Modosithato
				,Id
				,ObjTipus_Id_AdatElem
				,Muvelet_Id
				,Kod
				,Nev
				) values (
				 null
				,null
				,null
				,'0'
				,@record
				,null
				,null
				,'IratSzignalas'
				,'Irat szignálás'
				);
	END

	DECLARE @SZF_record_id UNIQUEIDENTIFIER
SET @SZF_record_id = 'E0C3650A-E0C6-E811-80CE-00155D020DD3'

IF NOT EXISTS(SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID= @SZF_record_id)
	BEGIN
		/*-------------------------------------------------------------------------*/
		PRINT 'KRT_Szerepkor_Funkcio - Irat szignálás FEJLESZTO szerepkorhoz kotese' 
		/*-------------------------------------------------------------------------*/
		 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Tranz_id
				,Stat_id
				,Id
				,Funkcio_Id
				) values (
				 'E855F681-36ED-41B6-8413-576FCB5D1542'
				,null
				,null
				,@SZF_record_id
				,@record
				);
	END

GO

--BLG_549
-- IRATTARI_TETELSZAM_SORREND rendszerparameterhez új (3-as) érték beállítása
-- FPH = BOPMH = nem változik, NMHH = 3
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='EEE526EF-5F6C-4BBF-A706-E49DC116217E') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
	Print 'ERROR - Nincs IRATTARI_TETELSZAM_SORREND paraméter vagy nem ez az id-ja' 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 --UPDATE KRT_Parameterek
	 --SET 
		--Note='A paraméter értékek lehetnek: 0 - ágazati jel / tételszám / megőrzési idő; 1 - tételszám / ágazati jel / megőrzési idő; 2 - tételszám / megőrzési idő (I.); 3 -  tételszám / megőrzési idő (II.) '
	 --WHERE Id=@record_id 
 END
-- if @org_kod = 'NMHH' 
--BEGIN 
--	Print @org_kod +': IRATTARI_TETELSZAM_SORREND = 3'
--	UPDATE KRT_Parameterek
--	   SET Ertek='3'
--	   WHERE Id='EEE526EF-5F6C-4BBF-A706-E49DC116217E'
--END

GO

-------------------------------------------MultiLineTextBox-------------------------------------------------------------
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'


declare @Tranz_id uniqueidentifier
set @Tranz_id = '5AF0A02B-DBC3-4DE8-B013-158E06009727'

declare @KodCsoport_Id uniqueidentifier
declare @recordId uniqueidentifier
set @KodCsoport_Id = (select Id from KRT_Kodcsoportok where Kod = 'CONTROLTYPE_SOURCE')
SET @recordId = '9EE7A917-5A57-4684-946C-95306CB54E5A'


IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE Id = @recordId)
BEGIN 
    
    Print 'insert - CONTROLTYPE_SOURCE.MultiLineTextBox' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Org
    ,Id
    ,Kod
    ,Nev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,@Tranz_id
    ,@Org
    , @recordId
    ,'~/Component/MultiLineTextBox.ascx'
    ,'Többsoros szövegmező (MultiLineTextBox)'
    ,'16'
    ,'1'
    ); 
     
      
  END 
  ELSE 
  BEGIN 
    Print 'update - CONTROLTYPE_SOURCE.MultiLineTextBox'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=@Tranz_id
    ,Org=@Org
    ,Id=@recordId
    ,Kod='~/Component/MultiLineTextBox.ascx'
    ,Nev='Többsoros szövegmező (MultiLineTextBox)'
    ,Sorrend='16'
    ,Modosithato='1'
    WHERE Id=@recordId 
       
  END

GO

-------------------------------------------MultiLineTextBox-------------------------------------------------------------

-- BUG_4384 TUK Iktatókönyv létrehozás azonosító hiba
-- Hibásan létrehozott Iktatókönyv törlése

declare @id uniqueidentifier

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
if ((@org_kod = 'NMHH') AND (@isTUK = '1')) 
BEGIN
	DELETE FROM EREC_IraIktatoKonyvek
	WHERE Iktatohely ='T' AND MegKuljelzes='TEST' AND Azonosito='100' 
END

GO

DECLARE @record UNIQUEIDENTIFIER
SET @record = '6FA3BD18-9DD0-E811-80CF-00155D020DD3'

IF NOT EXISTS(SELECT 1 FROM KRT_FUNKCIOK WHERE ID= @record)
	BEGIN
		/*-------------------------------------------------------------------------*/
		PRINT 'KRT_Funkcio - Jegyzék átadás' 
		/*-------------------------------------------------------------------------*/
		 INSERT INTO KRT_Funkciok(
				 Tranz_id
				,Stat_id
				,Alkalmazas_Id
				,Modosithato
				,Id
				,ObjTipus_Id_AdatElem
				,Muvelet_Id
				,Kod
				,Nev
				) values (
				 null
				,null
				,null
				,'0'
				,@record
				,null
				,null
				,'JegyzekTetelAtadas'
				,'Jegyzék tételek átadása'
				);
	END

	DECLARE @SZF_record_id UNIQUEIDENTIFIER
SET @SZF_record_id = 'A0A9EC97-A3D0-E811-80CF-00155D020DD3'

IF NOT EXISTS(SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID= @SZF_record_id)
	BEGIN
		/*-------------------------------------------------------------------------*/
		PRINT 'KRT_Szerepkor_Funkcio - Irat szignálás FEJLESZTO szerepkorhoz kotese' 
		/*-------------------------------------------------------------------------*/
		 insert into KRT_Szerepkor_Funkcio(
				 Szerepkor_Id
				,Tranz_id
				,Stat_id
				,Id
				,Funkcio_Id
				) values (
				 'E855F681-36ED-41B6-8413-576FCB5D1542'
				,null
				,null
				,@SZF_record_id
				,@record
				);
	END

GO

GO

-- BLG_4271: PARTNER_FORRAS kódcsoportba új kódtár érték: Hivatali kapu ---------------------------------------------------------

Print 'PARTNER_FORRAS kódcsoport'


DECLARE @kodCsoportId uniqueidentifier 
SET @kodCsoportId = (select Id from KRT_KodCsoportok where Id='552F33EC-0BAC-400E-86F2-8AB624959DA4') 

if @kodCsoportId IS NULL 
BEGIN 
		
	Print 'ERROR: Nincs PARTNER_FORRAS kódcsoport vagy nem jó az ID-ja' 

END 
ELSE 
BEGIN 
    Print 'Hivatali kapu kódtárérték' 
	DECLARE @record_id uniqueidentifier
    SET @record_id = '5F32CEBE-8AA1-4F4B-AB84-F704F215F3FC'
	 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 

	if NOT EXISTS (select 1 from KRT_KodTarak where Id = @record_id)
	BEGIN 
		
		Print 'INSERT' 
			insert into KRT_KodTarak(
				KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			) values (
			@kodCsoportId
			,null
			,null
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,@record_id
			,'E'
			,'Hivatali kapu'
			,'HKP'
			,'0'
			,'17'
			); 
		END 
		ELSE 
		BEGIN 
			Print 'UPDATE'
			UPDATE KRT_KodTarak
			SET 
				KodCsoport_Id=@kodCsoportId
			,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Kod='E'
			,Nev='Hivatali kapu'
			,RovidNev='HKP'
			,Modosithato='0'
			,Sorrend='17'
			WHERE Id=@record_id
	END
END
-------------------------------------------------------------------
PRINT 'BLG_2200'

DECLARE @org_ID uniqueidentifier
SET @org_ID ='450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @record2200_id uniqueidentifier
SET @record2200_id ='99E92679-F9C5-E711-80C7-99995D027E9B'
			
IF NOT EXISTS(SELECT 1 FROM KRT_Parameterek where Id=@record2200_id)
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @org_ID
		,@record2200_id
		,'PARTNERKAPCSOLATOK_ERKEZTETESKOR_ENABLED'
		,'1'
		,'1'
		,'Bejövő irat iktatásakor  partnerhez felvett kapcsolattartó típust írjuk be a partner neve mellé összefűzve (1=igen,0=nem)'
		); 
 END 
 GO
-------------------------------------------------------------------
GO
------------------------------------------------------------------------
print 'BLG 3231'

IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = 'B5841CEB-1F80-5559-25C4-62257F8C37F9') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id] ,[Org] ,[KodCsoport_Id] ,[Kod] ,[Nev] ,[RovidNev] ,[Egyeb] ,[Modosithato] ,[Sorrend] ,[Note]) VALUES ('B5841CEB-1F80-5559-25C4-62257F8C37F9' ,'450B510A-7CAA-46B0-83E3-18445C0C53A9' ,'644D3323-050C-4FBE-A380-25FEDC063B8E' ,'pcx' ,'Kép (pcx)' ,'Kép (pcx)' ,'pcx' ,'1' ,'66' ,'' ) END ELSE UPDATE KRT_KodTarak SET Kod='pcx' ,Nev='Kép (pcx)' ,Egyeb = 'pcx' ,Modosithato= '1' WHERE Id='B5841CEB-1F80-5559-25C4-62257F8C37F9' 
-----------------
UPDATE KRT_DOKUMENTUMOK 
SET Formatum = 'pcx',
	Tipus = 'Kép (pcx)'
WHERE FajlNev like '%.pcx%' and Formatum not like 'pcx'
------------------------------------------------------------------------
GO
-------------------------------------------------------------------------------------------
-- BLG_4187 UgyJelleg kód kiszedése a megnevezésből
PRINT 'BLG_4187'
update [KRT_KodTarak]
set Nev = ltrim(substring(Nev, charindex(')', [Nev]) + 1, len(Nev)))
where KodCsoport_Id = '72554FDB-8323-4110-B7B2-65576AE3204A'
-- BLG_4187 END
-------------------------------------------------------------------------------------------
GO
------------------------------------BLG_125------------------------------------------------
declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @funkcioId uniqueidentifier
set @funkcioId = 'B22018A2-A0D1-468C-815D-2836F8A780DA'

--RNYMeghatalmazasokList
IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE Id = @funkcioId)
BEGIN 
	Print 'INSERT KRT_Funkciok - RNYMeghatalmazasokList' 

	insert into KRT_Funkciok
	(
		Id,
		Kod,
		Nev,
		ObjTipus_Id_AdatElem,
		Alkalmazas_Id
) 
values (
	@funkcioId,
	'RNYMeghatalmazasokList',
	'RNY meghatalmazások lekérdezése',
	'FD52245E-A1F8-4EF4-AE05-259A33AA5BA3',
	'20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
); 
	 
END

declare @szerepkorFunkcioId uniqueidentifier
set @szerepkorFunkcioId = '4A4CF233-BC02-4DA5-9C24-CDD9FDD0A45B'

--RNYMeghatalmazasokList
IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE Id = @szerepkorFunkcioId)
BEGIN 
	Print 'INSERT KRT_Szerepkor_Funkcio - FEJLESZTO - RNYMeghatalmazasokList' 

	insert into KRT_Szerepkor_Funkcio
	(
		Id,
		Funkcio_Id,
		Szerepkor_Id
) 
values (
	@szerepkorFunkcioId,
	@funkcioId,
	'E855F681-36ED-41B6-8413-576FCB5D1542'
); 
	 
END

--RNYMeghatalmazasList.aspx
DECLARE @modul_id uniqueidentifier 
SET @modul_id = 'E8D97434-F108-4DC1-B5BB-E8F368E1224A'

if not exists (select 1 from KRT_Modulok where Id = @modul_id)
BEGIN 
	
	Print 'INSERT' 
	 insert into KRT_Modulok(
		Id
		,Tipus
		,Kod
		,Nev
		,Leiras
		) values (
		@modul_id
		,'F'
		,'RNYMeghatalmazasList.aspx'
		,'RNY meghatalmazások'
		,null
		); 
END 

declare @menuId uniqueidentifier
set @menuId = 'EF5DA565-8A4E-43AD-84AD-4A3CB1081DE1'

--RNY meghatalmazások lekérdezése
IF NOT EXISTS (SELECT 1 FROM KRT_Menuk WHERE Id = @menuId)
BEGIN 
	Print 'INSERT KRT_Menuk - RNY meghatalmazások lekérdezése' 

	insert into KRT_Menuk
	(
		Id,
		Menu_Id_Szulo,
		Kod,
		Nev,
		Funkcio_Id,
		Modul_Id,
		Parameter,
		Sorrend
) 
values (
	@menuId,
	'EE9EA307-ED15-4FE6-B323-A12D2E06AC6D',
	'eRecord',
	'RNY meghatalmazások lekérdezése',
	@funkcioId,
	@modul_id,
	'',
	'40'
); 
	 
END

GO
------------------------------------BLG_125------------------------------------------------

------------------------------------------ BUG_4425 -----------------------------------------------------------------
-- Nem használt kapcsolattípusok invalidálása

UPDATE [dbo].[KRT_KodTarak]
   SET ErvVege = '2018-10-16'
   WHERE KodCsoport_Id = 'A17AB343-1EB3-4035-868C-7E48F2AC3019' AND Kod IN ('7', '8', '9', 'A', 'B');
GO

------------------------------------------ BUG_4425 -----------------------------------------------------------------


-----------------------------------------------  BLG_2203  --------------------------------------------------------------------

-- BLG_2203: FELHASZNALASI_KORLATOZASOK  kódcsoport	 

Print 'FELHASZNALASI_KORLATOZASOK  kódcsoport'

DECLARE @isTUK char(1) 
DECLARE @record_id uniqueidentifier
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
if (@isTUK = '1') 
BEGIN
	PRINT 'TUK esetén nem hozzuk létre'
END
ELSE
BEGIN 
	DECLARE @KodCsopId uniqueidentifier 
	SET @KodCsopId ='1BBA03DE-55DB-E811-80CF-00155D020DD3'
	SET @record_id = NULL
	SET @record_id = (select Id from KRT_KodCsoportok where Id=@KodCsopId) 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
			
		Print 'INSERT' 
		insert into KRT_KodCsoportok(
		 BesorolasiSema
		,Id
		,Kod
		,Nev
		,Modosithato
		,Note
		) values (
		'0'
		,@KodCsopId
		,'FELHASZNALASI_KORLATOZASOK'
		,'Felhasználási korlátozások'
		,'0'
		,'Felhasználás: Felhasználási korlátozások metaadat megadásnál választható értékek (TÜK-nél nem használjuk)'
		); 

	END 
	ELSE 
	BEGIN 
		Print 'UPDATE'
		UPDATE KRT_KodCsoportok
		SET 
		BesorolasiSema='0'
		,Kod='FELHASZNALASI_KORLATOZASOK'
		,Nev='Felhasználási korlátozások'
		,Modosithato='0'	
		,Note='Felhasználás: Felhasználási korlátozások metaadat megadásnál választható értékek (TÜK-nél nem használjuk)'
		WHERE Id=@KodCsopId
		
	END

	print 'Kódtárértékek felvitele:'
	print 'Nincs korlátozás'
	SET @record_id = NULL
 	SET @record_id = (select Id from KRT_KodTarak
		where Id='65A135D8-5DDB-E811-80CF-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 		
		Print 'INSERT' 
		insert into KRT_KodTarak(
			KodCsoport_Id
		,Org
		,Id
		,Kod
		,Nev
		,RovidNev
		,Modosithato
		,Sorrend
		) values (
		@KodCsopId
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'65A135D8-5DDB-E811-80CF-00155D020DD3'
		,'0'
		,'Nincs korlátozás'
		,'0'
		,'0'
		,'01'
		); 
	END 
	ELSE 
	BEGIN 
		Print 'UPDATE'
		UPDATE KRT_KodTarak
		SET 
		KodCsoport_Id=@KodCsopId
		,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		,Kod='0'
		,Nev='Nincs korlátozás'
		,RovidNev='0'
		,Modosithato='0'
		,Sorrend='01'
		WHERE Id='65A135D8-5DDB-E811-80CF-00155D020DD3'

	END

	print 'Nem másolható!'
	SET @record_id = NULL
 	SET @record_id = (select Id from KRT_KodTarak
		where Id='66A135D8-5DDB-E811-80CF-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 		
		Print 'INSERT' 
		insert into KRT_KodTarak(
		KodCsoport_Id
		,Org
		,Id
		,Kod
		,Nev
		,RovidNev
		,Modosithato
		,Sorrend
		) values (
		@KodCsopId
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'66A135D8-5DDB-E811-80CF-00155D020DD3'
		,'1'
		,'Nem másolható!'
		,'1'
		,'0'
		,'02'
		); 
	END 
	ELSE 
	BEGIN 
		Print 'UPDATE'
		UPDATE KRT_KodTarak
		SET 
		KodCsoport_Id=@KodCsopId
		,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		,Kod='1'
		,Nev='Nem másolható!'
		,RovidNev='1'
		,Modosithato='0'
		,Sorrend='02'
		WHERE Id='66A135D8-5DDB-E811-80CF-00155D020DD3'

	END
	
	print 'Kivonat nem készíthető!'
	SET @record_id = NULL
 	SET @record_id = (select Id from KRT_KodTarak
		where Id='67A135D8-5DDB-E811-80CF-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 		
		Print 'INSERT' 
		insert into KRT_KodTarak(
		KodCsoport_Id
		,Org
		,Id
		,Kod
		,Nev
		,RovidNev
		,Modosithato
		,Sorrend
		) values (
		@KodCsopId
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'67A135D8-5DDB-E811-80CF-00155D020DD3'
		,'2'
		,'Kivonat nem készíthető!'
		,'2'
		,'0'
		,'03'
		); 
	END 
	ELSE 
	BEGIN 
		Print 'UPDATE'
		UPDATE KRT_KodTarak
		SET 
		KodCsoport_Id=@KodCsopId
		,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		,Kod='2'
		,Nev='Kivonat nem készíthető!'
		,RovidNev='2'
		,Modosithato='0'
		,Sorrend='03'
		WHERE Id='67A135D8-5DDB-E811-80CF-00155D020DD3'
	END	
	
	print 'Elolvasás után visszaküldendő!'
	SET @record_id = NULL
 	SET @record_id = (select Id from KRT_KodTarak
		where Id='68A135D8-5DDB-E811-80CF-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 		
		Print 'INSERT' 
		insert into KRT_KodTarak(
		KodCsoport_Id
		,Org
		,Id
		,Kod
		,Nev
		,RovidNev
		,Modosithato
		,Sorrend
		) values (
		@KodCsopId
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'68A135D8-5DDB-E811-80CF-00155D020DD3'
		,'3'
		,'Elolvasás után visszaküldendő!'
		,'3'
		,'0'
		,'04'
		); 
	END 
	ELSE 
	BEGIN 
		Print 'UPDATE'
		UPDATE KRT_KodTarak
		SET 
		KodCsoport_Id=@KodCsopId
		,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		,Kod='3'
		,Nev='Elolvasás után visszaküldendő!'
		,RovidNev='3'
		,Modosithato='0'
		,Sorrend='04'
		WHERE Id='68A135D8-5DDB-E811-80CF-00155D020DD3'
	END	
		
	print 'Zárt borítékban tárolandó!'
	SET @record_id = NULL
 	SET @record_id = (select Id from KRT_KodTarak
		where Id='69A135D8-5DDB-E811-80CF-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 		
		Print 'INSERT' 
		insert into KRT_KodTarak(
		KodCsoport_Id
		,Org
		,Id
		,Kod
		,Nev
		,RovidNev
		,Modosithato
		,Sorrend
		) values (
		@KodCsopId
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'69A135D8-5DDB-E811-80CF-00155D020DD3'
		,'4'
		,'Zárt borítékban tárolandó!'
		,'4'
		,'0'
		,'05'
		); 
	END 
	ELSE 
	BEGIN 
		Print 'UPDATE'
		UPDATE KRT_KodTarak
		SET 
		KodCsoport_Id=@KodCsopId
		,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		,Kod='4'
		,Nev='Zárt borítékban tárolandó!'
		,RovidNev='4'
		,Modosithato='0'
		,Sorrend='05'
		WHERE Id='69A135D8-5DDB-E811-80CF-00155D020DD3'
	END		
END
go

-- BLG_2203: FELHASZNALASI_KORLATOZASOK Metaadat létrehozása 

Print 'FELHASZNALASI_KORLATOZASOK metaadat'

DECLARE @isTUK char(1) 
DECLARE @record_id uniqueidentifier
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
if (@isTUK = '1') 
BEGIN
	PRINT 'TUK esetén nem hozzuk létre'
END
ELSE
BEGIN 
	print 'FELHASZNALASI_KORLATOZASOK tárgyszó létrehozás'

	declare @Org uniqueidentifier
	set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

	declare @OrgKod nvarchar(100)
	set @OrgKod = (select Kod from KRT_Orgok where Id = @Org)

	declare @UgyObjTip_Id uniqueidentifier
	set @UgyObjTip_Id = 'A04417A6-54AC-4AF1-9B63-5BABF0203D42'

	declare @DefinicioTipus nvarchar(64)
	set @DefinicioTipus = 'B1'

	declare @now datetime
	set @now = GETDATE()

	declare @objMetaDefId_UgyiratMetaadatok uniqueidentifier
	set @objMetaDefId_UgyiratMetaadatok = '16DD6E89-5D11-E811-80C5-00155D027EA9'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaDefinicio WHERE Id = @objMetaDefId_UgyiratMetaadatok)
	BEGIN
		print 'insert - EREC_Obj_MetaDefinicio: EREC_UgyUgyiratok_metaadatai'

		INSERT INTO EREC_Obj_MetaDefinicio
		(
			[Id]
		   ,[Org]
		   ,[Objtip_Id]
		   ,[DefinicioTipus]
		   ,[ContentType]
		)
		VALUES
		(
			@objMetaDefId_UgyiratMetaadatok
		   ,@Org
		   ,@UgyObjTip_Id
		   ,@DefinicioTipus
		   ,'EREC_UgyUgyiratok_metaadatai'
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_Obj_MetaDefinicio: EREC_UgyUgyiratok_metaadatai'
	END

	--Tárgyszavak

	-- 'Felhasznalasi_korlatozasok' tárgyszó
	declare @Tipus char(1)
	set @Tipus = '1'

--	declare @ControlTypeSource nvarchar(64)
--	set @ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'

	declare @targyszoId uniqueidentifier
	set @targyszoId = 'A6B9448D-60DB-E811-80CF-00155D020DD3'


	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId)
	BEGIN
		print 'insert - Felhasznalasi_korlatozasok'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[AlapertelmezettErtek]
		)
		VALUES
		(
			@targyszoId
		   ,@Org
		   ,@Tipus
		   ,'Felhasználási korlátozások'
		   ,'Felhasznalasi_korlatozasok'
		   ,'~/Component/KodTarakDropDownList.ascx'
		   ,'FELHASZNALASI_KORLATOZASOK'
		   ,'0'
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Felhasznalasi_korlatozasok'
	END


	-- EREC_Obj_MetaAdatai
	declare @recordId uniqueidentifier
	set @recordId = '7B1F61B4-60DB-E811-80CF-00155D020DD3'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - EREC_UgyUgyiratok_metaadatai - Felhasznalasi_korlatozasok'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_UgyiratMetaadatok
		   ,@targyszoId
		   ,0
		   ,'1'
		   ,'0'
		)
	END
END
GO

 GO

 -------------------------------------------------------------------------------------------
--BLG_3217 END
-------------------------------------------------------------------------------------------

DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
where Id='537C4C4A-EFBB-E811-80CE-00155D020DD3') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'537C4C4A-EFBB-E811-80CE-00155D020DD3'
		,'ENCRYPT_KITITKOSITANDO_FAJL_TIPUSOK'
		,'enc'
		,'1'
		,'A megadott - "|"-vel elválasztott - fájl típusok esetén a rendszer megpróbálja a fájlt kititkosítani az ENCRYPT_SAJAT_PRIVAT_KULCS_PATH rendszer paraméterben megadott fájlban található privát kulccsal'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='537C4C4A-EFBB-E811-80CE-00155D020DD3'
	--	,Nev='ENCRYPT_KITITKOSITANDO_FAJL_TIPUSOK'
	--	,Ertek='enc'
	--	,Karbantarthato='1'
	--	,Note='A megadott - "|"-vel elválasztott - fájl típusok esetén a rendszer megpróbálja a fájlt kititkosítani az ENCRYPT_SAJAT_PRIVAT_KULCS_PATH rendszer paraméterben megadott fájlban található privát kulccsal'
	-- WHERE Id=@record_id 
 --END

 GO

 DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
where Id='C703A259-EFBB-E811-80CE-00155D020DD3') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'C703A259-EFBB-E811-80CE-00155D020DD3'
		,'ENCRYPT_SAJAT_PRIVAT_KULCS_PATH'
		,''
		,'1'
		,'A kimenő és bejövő üzenetek kititkosításához használt saját privát kulcsot tartalmazó fájl a csatolmányok megnyithatóságának érdekében'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='C703A259-EFBB-E811-80CE-00155D020DD3'
	--	,Nev='ENCRYPT_SAJAT_PRIVAT_KULCS_PATH'
	--	,Ertek='c:\Contentum.Net\KRTitok\Kulcs\FPH_EUGYINTEZES_prv.asc'
	--	,Karbantarthato='1'
	--	,Note='A kimenő és bejövő üzenetek kititkosításához használt saját privát kulcsot tartalmazó fájl a csatolmányok megnyithatóságának érdekében'
	-- WHERE Id=@record_id 
 --END
 
 GO

 DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
where Id='FDF82F4D-31CD-E811-80CF-00155D020DD3') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'FDF82F4D-31CD-E811-80CF-00155D020DD3'
		,'ENCRYPT_SAJAT_PRIVAT_KULCS_PASSWORD'
		,''
		,'1'
		,'A kimenő és bejövő üzenetek kititkosításához használt saját privát kulcsot tartalmazó jelszó fájl a csatolmányok megnyithatóságának érdekében'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='FDF82F4D-31CD-E811-80CF-00155D020DD3'
	--	,Nev='ENCRYPT_SAJAT_PRIVAT_KULCS_PASSWORD'
	--	,Ertek='c:\Contentum.Net\KRTitok\Kulcs\jelszo.txt'
	--	,Karbantarthato='1'
	--	,Note='A kimenő és bejövő üzenetek kititkosításához használt saját privát kulcsot tartalmazó jelszó fájl a csatolmányok megnyithatóságának érdekében'
	-- WHERE Id=@record_id 
 --END
 
 GO

 ---------------------------------------------------------------------------------------------------------------
 -------------------- BLG 3217 - Titkosítot csatolmányok kititkosítása  -------------------------

 
 -------------------- BLG 3215:HKP-n kiküldött csatolmányok titkosítása -------------------------
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
where Id='CC86E61D-0296-4CE7-9518-FA744710EC48') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT - ENCRYPT_SAJAT_PUBLIKUS_KULCS_PATH' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'CC86E61D-0296-4CE7-9518-FA744710EC48'
		,'ENCRYPT_SAJAT_PUBLIKUS_KULCS_PATH'
		,''
		,'1'
		,'A kimenő és bejövő üzenetek titkosításához használt saját publikus kulcsot tartalmazó fájl a csatolmányok megnyithatóságának érdekében'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE - ENCRYPT_SAJAT_PUBLIKUS_KULCS_PATH'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='CC86E61D-0296-4CE7-9518-FA744710EC48'
	--	,Nev='ENCRYPT_SAJAT_PUBLIKUS_KULCS_PATH'
	--	,Ertek='c:\TFS\Contentum.Net\Contentum.Net.Application\ExternalPrograms\GPG\Kulcs\FPH_EUGYINTEZES_pub.asc'
	--	,Karbantarthato='1'
	--	,Note='A kimenő és bejövő üzenetek titkosításához használt saját publikus kulcsot tartalmazó fájl a csatolmányok megnyithatóságának érdekében'
	-- WHERE Id=@record_id 
 --END
 
 GO

  -------------------- BLG 3215:HKP-n kiküldött csatolmányok titkosítása -------------------------

-- BLG_2203 Irat és Küldemény minősítés cimke fordítások

declare @id uniqueidentifier

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
if  (@isTUK = '0')
BEGIN
	print 'EgyszerusitettIktatasForm'
	print 'labelIratMinosites'
	set @id = '3CF99F63-E11A-E811-80C9-00155D020DD3'
	if exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN
		Print 'UPDATE EgyszerusitettIktatasForm Forditas'
		UPDATE KRT_Forditasok
		SET 
			NyelvKod='hu-HU'
			,OrgKod=@org_kod
			,Modul='eRecord'
			,Komponens='EgyszerusitettIktatasForm.aspx'
			,ObjAzonosito='labelIratMinosites'
			,Forditas='Hozzáférési korlátozások:'
		WHERE Id=@id 
	END
	ELSE
	BEGIN
		print 'insert EgyszerusitettIktatasForm Forditas'
		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			@org_kod,
			'eRecord',
			'EgyszerusitettIktatasForm.aspx',
			'labelIratMinosites',
			'Hozzáférési korlátozások:'
		)
	END

	print 'eRecordComponent/IraIratFormTab'
	print 'labelIratMinosites'
	set @id = '3DF99F63-E11A-E811-80C9-00155D020DD3'

	if exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN
		Print 'UPDATE IraIratFormTab Forditas'
		UPDATE KRT_Forditasok
		SET 
			NyelvKod='hu-HU'
			,OrgKod=@org_kod
			,Modul='eRecord'
			,Komponens='eRecordComponent/IraIratFormTab.ascx'
			,ObjAzonosito='labelIratMinosites'
			,Forditas='Hozzáférési korlátozások:'
		WHERE Id=@id 
	END
	ELSE
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			@org_kod,
			'eRecord',
			'eRecordComponent/IraIratFormTab.ascx',
			'labelIratMinosites',
			'Hozzáférési korlátozások:'
		)
	END
	
	print 'eRecordComponent/KuldKuldemenyFormTab'
	print 'labelKuldemenyMinosites'
	set @id = '1A961B1C-1DDC-E811-80CF-00155D020DD3'

	if exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN
		Print 'UPDATE KuldKuldemenyFormTab Forditas'
		UPDATE KRT_Forditasok
		SET 
			NyelvKod='hu-HU'
			,OrgKod=@org_kod
			,Modul='eRecord'
			,Komponens='eRecordComponent/KuldKuldemenyFormTab.ascx'
			,ObjAzonosito='labelKuldemenyMinosites'
			,Forditas='Hozzáférési korlátozások:'
		WHERE Id=@id 
	END
	ELSE
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			@org_kod,
			'eRecord',
			'eRecordComponent/KuldKuldemenyFormTab.ascx',
			'labelKuldemenyMinosites',
			'Hozzáférési korlátozások:'
		)
	END
END

GO
-----------------------------------------------  BLG_2203 vége  --------------------------------------------------------------------
DECLARE @org_ID uniqueidentifier 
SET @org_ID='450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @RECORD_ID_ENCPRIVKEY uniqueidentifier
set @RECORD_ID_ENCPRIVKEY = 'C703A259-EFBB-E811-80CE-00155D020DD3'

IF NOT EXISTS(SELECT 1 FROM KRT_PARAMETEREK WHERE ID =@RECORD_ID_ENCPRIVKEY)
BEGIN 
	Print 'INSERT ENCRYPT_SAJAT_PRIVAT_KULCS_PATH' 
	 insert into KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @org_id
		,@RECORD_ID_ENCPRIVKEY
		,'ENCRYPT_SAJAT_PRIVAT_KULCS_PATH'
		,''
		,'1'
		,'A kimenő és bejövő üzenetek kititkosításához használt saját privát kulcsot tartalmazó fájl a csatolmányok megnyithatóságának érdekében'
		); 
 END 

 GO
----------
DECLARE @org_ID uniqueidentifier 
SET @org_ID='450B510A-7CAA-46B0-83E3-18445C0C53A9'
declare @RECORD_ID_ENCPUBKEY uniqueidentifier
set @RECORD_ID_ENCPUBKEY = 'CC86E61D-0296-4CE7-9518-FA744710EC48'

IF NOT EXISTS(SELECT 1 FROM KRT_PARAMETEREK WHERE ID =@RECORD_ID_ENCPUBKEY)
BEGIN 
	Print 'INSERT ENCRYPT_SAJAT_PUBLIKUS_KULCS_PATH' 
	 insert into KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @org_id
		,@RECORD_ID_ENCPUBKEY
		,'ENCRYPT_SAJAT_PUBLIKUS_KULCS_PATH'
		,''
		,'1'
		,'A kimenő és bejövő üzenetek titkosításához használt saját publikus kulcsot tartalmazó fájl a csatolmányok megnyithatóságának érdekében'
		); 
 END 
GO
----------
DECLARE @org_ID uniqueidentifier 
SET @org_ID='450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @RECORD_ID_ENCPRIVKEYPASS uniqueidentifier
set @RECORD_ID_ENCPRIVKEYPASS = 'FDF82F4D-31CD-E811-80CF-00155D020DD3'

IF NOT EXISTS(SELECT 1 FROM KRT_PARAMETEREK WHERE ID =@RECORD_ID_ENCPRIVKEYPASS)
BEGIN 
	Print 'INSERT ENCRYPT_SAJAT_PRIVAT_KULCS_PASSWORD' 
	 insert into KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @org_id
		,@RECORD_ID_ENCPRIVKEYPASS
		,'ENCRYPT_SAJAT_PRIVAT_KULCS_PASSWORD'
		,''
		,'1'
		,'A kimenő és bejövő üzenetek kititkosításához használt saját privát kulcsot tartalmazó jelszó fájl a csatolmányok megnyithatóságának érdekében'
		); 
 END 
GO
-------------------------------------------------------------------------
DECLARE @org_ID uniqueidentifier 
SET @org_ID='450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @RECORD_ID_SIGNPRIVKEY uniqueidentifier
set @RECORD_ID_SIGNPRIVKEY = 'C703A259-EFBB-E811-80CE-00155D020EE3'

IF NOT EXISTS(SELECT 1 FROM KRT_PARAMETEREK WHERE ID =@RECORD_ID_SIGNPRIVKEY)
BEGIN 
	Print 'INSERT SIGN_SAJAT_PRIVAT_KULCS_PATH' 
	 insert into KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @org_id
		,@RECORD_ID_SIGNPRIVKEY
		,'SIGN_SAJAT_PRIVAT_KULCS_PATH'
		,''
		,'1'
		,'Elektronikus lenyomathoz használt saját privát kulcsot tartalmazó fájl (PFX)'
		); 
 END 

 GO
----------
DECLARE @org_ID uniqueidentifier 
SET @org_ID='450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @RECORD_ID_SIGNPUBKEY uniqueidentifier
set @RECORD_ID_SIGNPUBKEY = 'C703A259-EFBB-E811-80CE-00155D020EE5'

IF NOT EXISTS(SELECT 1 FROM KRT_PARAMETEREK WHERE ID =@RECORD_ID_SIGNPUBKEY)
BEGIN 
	Print 'INSERT SIGN_SAJAT_PUBLIKUS_KULCS_PATH' 
	 insert into KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @org_id
		,@RECORD_ID_SIGNPUBKEY
		,'SIGN_SAJAT_PUBLIKUS_KULCS_PATH'
		,''
		,'1'
		,'Elektronikus lenyomathoz használt saját publikus kulcsot tartalmazó fájl'
		); 
 END 

 GO
----------
DECLARE @org_ID uniqueidentifier 
SET @org_ID='450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @RECORD_ID_SIGNPRIVKEYPASS uniqueidentifier
set @RECORD_ID_SIGNPRIVKEYPASS = 'FDF82F4D-31CD-E811-80CF-00155D020EE4'

IF NOT EXISTS(SELECT 1 FROM KRT_PARAMETEREK WHERE ID =@RECORD_ID_SIGNPRIVKEYPASS)
BEGIN 
	Print 'INSERT SIGN_SAJAT_PRIVAT_KULCS_PASSWORD_PATH' 
	 insert into KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @org_id
		,@RECORD_ID_SIGNPRIVKEYPASS
		,'SIGN_SAJAT_PRIVAT_KULCS_PASSWORD_PATH'
		,''
		,'1'
		,'Elektronikus lenyomathoz használt saját privát kulcshoz tartozó jelszófájl.'
		); 
 END 
GO
-------------------------------------------------------------------------

-----------------------------------------BLG_4930------------------------------------------------------------
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
where Id='96AEBCC2-B494-45EF-B10A-901F01230FD3') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT - MUNKAIRAT_AUTO_IKTATAS_KIADMANYOZASKOR' 
	 insert into KRT_Parameterek(
		Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 '450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'96AEBCC2-B494-45EF-B10A-901F01230FD3'
		,'MUNKAIRAT_AUTO_IKTATAS_KIADMANYOZASKOR'
		,'1'
		,'1'
		,'Értéke: 1/0. Ha 1 akkor a munkairat kiadmányozásakor az automatikus iktatás megtörténik, ha 0, akkor nincs automatikus iktatás kiadmányozáskor.'
		); 
 END 

 GO
-----------------------------------------BLG_4930------------------------------------------------------------


--------------------------------------------- END -------------------------------------------------------------------
GO
------------------------------------------------------------------------------
PRINT 'BLG_3231'
UPDATE KRT_KodtarFuggoseg
SET ADAT = '{"VezerloKodCsoportId":"644d3323-050c-4fbe-a380-25fedc063b8e","FuggoKodCsoportId":"9a32381d-5f8f-4ae2-83e0-32f440191c9e","Items":[{"VezerloKodTarId":"9dbb20cd-f53c-458f-b783-ec7524b5ab34","FuggoKodtarId":"886f4366-a424-0f66-a443-52105a272ef2","Aktiv":true},{"VezerloKodTarId":"d42fa9f8-321b-4c8b-b684-e7a858c074e3","FuggoKodtarId":"886f4366-a424-0f66-a443-52105a272ef2","Aktiv":true},{"VezerloKodTarId":"9752d9bc-27fe-4f2e-a0c5-8f8b93bb62db","FuggoKodtarId":"886f4366-a424-0f66-a443-52105a272ef2","Aktiv":true},{"VezerloKodTarId":"b5841ceb-1f80-5559-25c4-62257f8c37f9","FuggoKodtarId":"886f4366-a424-0f66-a443-52105a272ef2","Aktiv":true},{"VezerloKodTarId":"dda4036d-2e88-498f-8d47-ef4086e2b98f","FuggoKodtarId":"886f4366-a424-0f66-a443-52105a272ef2","Aktiv":true},{"VezerloKodTarId":"7921a68d-70be-4484-accd-7837665c3c90","FuggoKodtarId":"886f4366-a424-0f66-a443-52105a272ef2","Aktiv":true}],"ItemsNincs":[]}'
WHERE ID ='5baa1825-0b56-46f6-9080-eceac81fb227'
------------------------------------------------------------------------------
PRINT 'OKTATASI SEGEDANYAG'
IF NOT EXISTS (SELECT 1 FROM [KRT_Funkciok] WHERE ID='173a0e17-26d4-e811-80cb-00155d027ea9')
  INSERT [dbo].[KRT_Funkciok] ([Id], [Kod], [Nev], [ObjTipus_Id_AdatElem], [ObjStat_Id_Kezd], [Alkalmazas_Id], [Muvelet_Id], [ObjStat_Id_Veg], [Leiras], [Funkcio_Id_Szulo], [Csoportosito], [Modosithato], [Org], [MunkanaploJelzo], [FeladatJelzo], [KeziFeladatJelzo], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) VALUES (N'173a0e17-26d4-e811-80cb-00155d027ea9', N'OktatasiSegedanyagok', N'Oktatási segédanyagok kezelése', NULL, NULL, NULL, NULL, NULL, N'Oktatási segédanyagok menüpont kezeléséhez', NULL, NULL, N'1', NULL, N'0', N'0', N'0', 1, NULL, NULL, CAST(N'2018-10-20 07:07:50.557' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2018-10-20 07:07:50.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)

IF NOT EXISTS (SELECT 1 FROM KRT_MENUK WHERE ID='f8b3f47a-26d4-e811-80cb-00155d027ea9')
  INSERT [dbo].[KRT_Menuk] ([Id], [Org], [Menu_Id_Szulo], [Kod], [Nev], [Funkcio_Id], [Modul_Id], [Parameter], [Ver], [Note], [Stat_id], [ImageURL], [Sorrend], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) VALUES (N'f8b3f47a-26d4-e811-80cb-00155d027ea9', NULL, N'1dd07cb7-3b00-4a08-98f7-efd00de6fea2', N'eRecord', N'Oktatási segédanyagok', N'173a0e17-26d4-e811-80cb-00155d027ea9', N'58633506-fb19-4e2a-9371-0a8f0e5280cb', N'Startup=Standalone', 6, NULL, NULL, NULL, N'660', CAST(N'2018-10-20 00:00:00.000' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2018-10-20 07:10:37.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2018-10-20 08:11:13.000' AS DateTime), NULL, NULL, NULL, NULL)

IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID='8068bda4-26d4-e811-80cb-00155d027ea9')
  INSERT [dbo].[KRT_KodTarak] ([Id], [Org], [KodCsoport_Id], [ObjTip_Id_AdatElem], [Obj_Id], [Kod], [Nev], [RovidNev], [Egyeb], [Modosithato], [Sorrend], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) VALUES (N'8068bda4-26d4-e811-80cb-00155d027ea9', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'b084d417-0e1a-4072-a628-088207a4c99e', NULL, NULL, N'40', N'Oktatási segédanyag', NULL, NULL, N'1', NULL, 1, NULL, NULL, CAST(N'2018-10-20 07:11:48.253' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2018-10-20 07:11:47.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)

  
		DECLARE @record_id_OKT uniqueidentifier 
		SET @record_id_OKT ='8C24DBB3-E7E8-E811-80CB-00155D027EA9'
		
		IF NOT EXISTS(SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID=@record_id_OKT )
			BEGIN 
			     Print 'INSERT' 
				 insert into KRT_Szerepkor_Funkcio(
					 Szerepkor_Id
					,Id
					,Funkcio_Id
					) values (
					 'E855F681-36ED-41B6-8413-576FCB5D1542'
					,@record_id_OKT
					,'173A0E17-26D4-E811-80CB-00155D027EA9'
					); 
			 END 

------------------------------------------------------------------------------
GO
/*****************************************************************************
**  
**  BLG - 223
**  
******************************************************************************/
DECLARE @RECORDID 	UNIQUEIDENTIFIER 
DECLARE @ORGID 		UNIQUEIDENTIFIER 

SET @RECORDID = '577CD62A-91E5-436E-B4E1-3608C50946D4'
SET @ORGID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE Id = @RECORDID)			
BEGIN
	 Print 'INSERT-KRT_Parameterek-ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_LEVELTARIATADAS_EMAIL_SABLON' 
	 INSERT INTO KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) VALUES (
		 @ORGID
		,@RECORDID
		,'ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_LEVELTARIATADAS_EMAIL_SABLON'
		, ''
		,'1'
		,'
<html>
<body>
    <h1>
        Értesítés : Lejárt megőrzési idő - Levéltári átadás
    </h1>
    <br />
    <br />
    <h5>Érintett ügyek:</h5>
	<table style="border: 1px solid black;border-collapse: collapse;">
        <tr>
			 <th style="background-color: #4CAF50;color: white;">Ügy azonosító</th>
        </tr>
        {0}
    </table>
	<br />
	Megtekintés: {1}
    <br /><br />

    <small>Contentum.NET</small>
</body>
</html>
		'
		); 
END

GO

DECLARE @RECORDID 	UNIQUEIDENTIFIER 
DECLARE @ORGID 		UNIQUEIDENTIFIER 

SET @RECORDID = '577CD62A-91E5-436E-B4E1-3608C50946E4'
SET @ORGID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE Id = @RECORDID)			
BEGIN
	 Print 'INSERT-KRT_Parameterek-ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_LEVELTARIATADAS_EMAIL_TARGY' 
	 INSERT INTO KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) VALUES (
		 @ORGID
		,@RECORDID
		,'ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_LEVELTARIATADAS_EMAIL_TARGY'
		,'Értesítés - Lejárt megőrzési idő - Levéltári átadás'
		,'1'
		,'ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_LEVELTARIATADAS_EMAIL_TARGY'
		); 
END

GO
--------------
DECLARE @RECORDID 	UNIQUEIDENTIFIER 
DECLARE @ORGID 		UNIQUEIDENTIFIER 

SET @RECORDID = '577CD62A-91E5-436E-B4E1-3608C50946D5'
SET @ORGID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE Id = @RECORDID)			
BEGIN
	 Print 'INSERT-KRT_Parameterek-ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_SELEJTEZES_EMAIL_SABLON' 
	 INSERT INTO KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) VALUES (
		 @ORGID
		,@RECORDID
		,'ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_SELEJTEZES_EMAIL_SABLON'
		, ''
		,'1'
		,'
<html>
<body>
    <h1>
        Értesítés : Lejárt megőrzési idő - Selejtezés
    </h1>
    <br />
    <br />
    <h5>Érintett ügyek:</h5>
	<table style="border: 1px solid black;border-collapse: collapse;">
        <tr>
			 <th style="background-color: #4CAF50;color: white;">Ügy azonosító</th>
        </tr>
        {0}
    </table>
	<br />
	Megtekintés: {1}
    <br /><br />

    <small>Contentum.NET</small>
</body>
</html>
		'
		); 
END

GO

DECLARE @RECORDID 	UNIQUEIDENTIFIER 
DECLARE @ORGID 		UNIQUEIDENTIFIER 

SET @RECORDID = '577CD62A-91E5-436E-B4E1-3608C50946E5'
SET @ORGID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE Id = @RECORDID)			
BEGIN
	 Print 'INSERT-KRT_Parameterek-ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_SELEJTEZES_EMAIL_TARGY' 
	 INSERT INTO KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) VALUES (
		 @ORGID
		,@RECORDID
		,'ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_SELEJTEZES_EMAIL_TARGY'
		,'Értesítés - Lejárt megőrzési idő - Selejtezés'
		,'1'
		,'ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_SELEJTEZES_EMAIL_TARGY'
		); 
END

GO






/*OBSOLTE START*/

--PRINT 'BLG 223'

--DECLARE @RECORD_223A_ID	UNIQUEIDENTIFIER 
--SET		@RECORD_223A_ID	=	'5A23B50C-56FA-47F9-A645-6412D7DB7823'
--DECLARE @APP_ID			UNIQUEIDENTIFIER
--SET	    @APP_ID			=	'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
--DECLARE @NAME			NVARCHAR(100)
--SET		@NAME			=	'LejartMegorzesiIdo_Selejtezes'
--DECLARE @DESCR			NVARCHAR(100)
--SET		@DESCR			=	'Figyelmeztetés lejárt megőrzési idejű tételekre - Selejtezés, FeladatJelzo: 1'
--DECLARE @MODIFY			NVARCHAR(1)
--SET		@MODIFY			=	'1'
--IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE ID = @RECORD_223A_ID)
--BEGIN 	
--	 PRINT 'INSERT 2 KRT_Funkciok' 
--	 INSERT INTO KRT_Funkciok(
--		 Alkalmazas_Id
--		,Modosithato
--		,Id
--		,Kod
--		,Nev
--		,FeladatJelzo
--		) 
--	  VALUES (
--		 @APP_ID
--		,@MODIFY
--		,@RECORD_223A_ID
--		,@NAME
--		,@DESCR
--		,'1'
--		);
-- END 

--GO

--DECLARE @RECORD_223B_ID	UNIQUEIDENTIFIER 
--SET		@RECORD_223B_ID	=	'5A23B50C-56FA-47F9-A645-6412D7DB7833'
--DECLARE @APP_ID			UNIQUEIDENTIFIER
--SET	    @APP_ID			=	'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
--DECLARE @NAME			NVARCHAR(100)
--SET		@NAME			=	'LejartMegorzesiIdo_LeveltariAtadas'
--DECLARE @DESCR			NVARCHAR(100)
--SET		@DESCR			=	'Figyelmeztetés lejárt megőrzési idejű tételekre - Levéltári átadás , FeladatJelzo: 1'
--DECLARE @MODIFY			NVARCHAR(1)
--SET		@MODIFY			=	'1'
--IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok WHERE ID = @RECORD_223B_ID)
--BEGIN 	
--	 PRINT 'INSERT 2 KRT_Funkciok' 
--	 INSERT INTO KRT_Funkciok(
--		 Alkalmazas_Id
--		,Modosithato
--		,Id
--		,Kod
--		,Nev
--		,FeladatJelzo
--		) 
--	  VALUES (
--		 @APP_ID
--		,@MODIFY
--		,@RECORD_223B_ID
--		,@NAME
--		,@DESCR
--		,'1'
--		);
-- END 
-- -------------------------------------------------------------------------------
-- DECLARE @FUTTATANDO UNIQUEIDENTIFIER
-- SELECT TOP 1  @FUTTATANDO=id FROM KRT_Funkciok WHERE Kod= 'EsemenyErtesites'
-- IF NOT EXISTS (SELECT 1 FROM EREC_FeladatDefinicio WHERE ID = '99800EBF-B443-4329-84B9-7267F68CA55D')
--BEGIN 	
--	 INSERT [dbo].[EREC_FeladatDefinicio] (
--				[Id], 
--				[Org], 
--				[Funkcio_Id_Kivalto], 
--				[FeladatDefinicioTipus], 
--				[FeladatSorszam], 
--				[MuveletDefinicio], 
--				[SpecMuveletDefinicio], 
--				[Funkcio_Id_Inditando], 
--				[Funkcio_Id_Futtatando], 
--				[BazisObjLeiro], 
--				[FelelosTipus], 
--				[Obj_Id_Felelos], 
--				[FelelosLeiro], 
--				[Idobazis], 
--				[ObjTip_Id_DateCol], 
--				[AtfutasiIdo], 
--				[Idoegyseg], 
--				[FeladatLeiras], 
--				[Tipus], 
--				[Prioritas], 
--				[LezarasPrioritas], 
--				[Allapot], 
--				[ObjTip_Id], 
--				[Obj_type], 
--				[Obj_SzukitoFeltetel], 
--				[Ver]
--				) 
--	VALUES (
--			  N'99800EBF-B443-4329-84B9-7267F68CA55D',
--			  N'450b510a-7caa-46b0-83e3-18445c0c53a9',
--			  N'5A23B50C-56FA-47F9-A645-6412D7DB7823', 
--			  N'5', 
--			  1, 
--			  N'sp_WF_FeladatKezelo', 
--			  N'sp_WF_LejartMegorzesiIdoErtesito_Selejtezes', 
--			  NULL, 
--			  @FUTTATANDO, 
--			  NULL, 
--			  N'0', 
--			  NULL, 
--			  NULL, 
--			  N'1', 
--			  NULL, 
--			  0, 
--			  NULL, 
--			  N'Selejtezésre vár {0} darab tétel ! Tételek: {1}', 
--			  NULL, 
--			  N'31', 
--			  NULL, 
--			  NULL, 
--			  N'A04417A6-54AC-4AF1-9B63-5BABF0203D42', 
--			  N'EREC_UgyUgyiratok', 
--			  NULL, 
--			  1
--			   )
--END

--IF NOT EXISTS (SELECT 1 FROM EREC_FeladatDefinicio WHERE ID = '99800EBF-B443-4329-84B9-7267F68CA56D')
--BEGIN
--	INSERT [dbo].[EREC_FeladatDefinicio] (
--				[Id], 
--				[Org], 
--				[Funkcio_Id_Kivalto], 
--				[FeladatDefinicioTipus], 
--				[FeladatSorszam], 
--				[MuveletDefinicio], 
--				[SpecMuveletDefinicio], 
--				[Funkcio_Id_Inditando], 
--				[Funkcio_Id_Futtatando], 
--				[BazisObjLeiro], 
--				[FelelosTipus], 
--				[Obj_Id_Felelos], 
--				[FelelosLeiro], 
--				[Idobazis], 
--				[ObjTip_Id_DateCol], 
--				[AtfutasiIdo], 
--				[Idoegyseg], 
--				[FeladatLeiras], 
--				[Tipus], 
--				[Prioritas], 
--				[LezarasPrioritas], 
--				[Allapot], 
--				[ObjTip_Id], 
--				[Obj_type], 
--				[Obj_SzukitoFeltetel], 
--				[Ver]
--				) 
--	VALUES (
--			  N'99800EBF-B443-4329-84B9-7267F68CA56D',
--			  N'450b510a-7caa-46b0-83e3-18445c0c53a9',
--			  N'5A23B50C-56FA-47F9-A645-6412D7DB7833', 
--			  N'5', 
--			  1, 
--			  N'sp_WF_FeladatKezelo', 
--			  N'sp_WF_LejartMegorzesiIdoErtesito_LeveltariAtadas', 
--			  NULL, 
--			  @FUTTATANDO, 
--			  NULL, 
--			  N'0', 
--			  NULL, 
--			  NULL, 
--			  N'1', 
--			  NULL, 
--			  0, 
--			  NULL, 
--			  N'Levéltári átadásra vár {0} darab tétel ! Tételek: {1}', 
--			  NULL, 
--			  N'31', 
--			  NULL, 
--			  NULL, 
--			  N'A04417A6-54AC-4AF1-9B63-5BABF0203D42', 
--			  N'EREC_UgyUgyiratok', 
--			  NULL, 
--			  1
--			   )
--END
/*OBSOLTE STOP*/

 PRINT 'BLG 223'
 GO
/******************************************************************************/












------------------------------------------------------------------------------
GO

--BLG 2954

-- Megsemmisítve technikai csoport
declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
declare @csoportId uniqueidentifier = 'F15F2B45-1196-4B81-A165-F423BFEC9366'

IF NOT EXISTS (SELECT 1 FROM KRT_Csoportok WHERE Id = @csoportId)
BEGIN
	print 'INSERT KRT_Csoportok - Megsemmisítve technikai csoport'

	INSERT INTO KRT_Csoportok
	(
		Id,
		Org,
		Nev,
		Tipus,
		[System]
	)
	VALUES
	(
		@csoportId,
		@org,
		'Megsemmisítve',
		'1',
		'1'
	)
END

GO

--------------------------------BLG_2965_HSZ_szolgáltatás_Posta_Hibrid-------------------------------
 declare @org uniqueidentifier
 declare @KodcsoportId uniqueidentifier
 declare @recordId uniqueidentifier
 set @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
 set @KodcsoportId = '6982F668-C8CD-47A3-AC62-9F16E439473C'
 set @recordId = '815F0C4F-B1EF-41E2-A41F-41E2CAC6E5EB'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @recordId)
BEGIN 
	Print 'INSERT KRT_Kodtarak - KULDEMENY_KULDES_MODJA -Posta hibrid szolgáltatás' 

	 insert into KRT_KodTarak
	 (
		 Id,
		 Org,
		 KodCsoport_Id,
		 Kod,
		 Nev,
		 Sorrend,
		 Modosithato
    ) 
	values (
		@recordId,
		@org,
		@kodcsoportId,
		'191',
		'Posta hibrid szolgáltatás',
		'9999',
		'1'
    ); 
	 
 END

 GO

 declare @modulId uniqueidentifier
set @modulId = '4403E521-3947-419A-AD46-558749B94360'

IF NOT EXISTS (Select 1 FROM INT_Modulok where Id = @modulId)
BEGIN
	INSERT INTO INT_Modulok
	(Id, Nev)
	VALUES
	(@modulId,'PostaHibrid')
END


declare @recordId uniqueidentifier
set @recordId = '47416DC1-E7B1-4B91-82A2-35C013837F21'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @recordId)
BEGIN
	INSERT INTO INT_Parameterek
	(Id, Modul_id, Nev, Ertek, Karbantarthato)
	VALUES
	(@recordId, @modulId, 'Posta_PartnerNev', 'Magyar Posta Zrt.','1')
END

set @recordId = '52E02B83-16A0-48EF-8430-EE58755C6752'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @recordId)
BEGIN
	INSERT INTO INT_Parameterek
	(Id, Modul_id, Nev, Ertek, Karbantarthato)
	VALUES
	(@recordId, @modulId, 'Posta_RovidNev', 'MPZRT','1')
END

set @recordId = '01D5C2EC-DDD2-45CA-A652-3FD9B0B0EF49'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @recordId)
BEGIN
	INSERT INTO INT_Parameterek
	(Id, Modul_id, Nev, Ertek, Karbantarthato)
	VALUES
	(@recordId, @modulId, 'Posta_KRID', '506341775','1')
END


GO

			
DECLARE @recordId uniqueidentifier 
SET @recordId = '8FC54483-CE67-4915-BEC8-A6979CC9102C'

IF NOT EXISTS (Select 1 FROM KRT_Parameterek where Id = @recordId)
BEGIN 
	Print 'INSERT - KRT_Parameterek - PHSZ_ENABLED_FILETYPES' 
	 insert into KRT_Parameterek(
		Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,@recordId
		,'PHSZ_ENABLED_FILETYPES'
		,'.odt,.ods,.odp,.docx,.xlsx,.pptx,.pdf,.tiff,.jpeg,.jpg,.png'
		,'1'
		,'A posta hibrid szolgáltatás által kezelt állományok formátumai.'
		); 
 END 

 GO

 declare @kodcsoportId uniqueidentifier
  set @kodcsoportId = '654055cd-7cf6-4db8-a440-cc3f3f96ee72'

  IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @kodcsoportId)
  BEGIN
     print 'Insert KRT_KodCsoportok - PHSZ_LETTYPE'

       insert into KRT_KodCsoportok
    (
       Id
      ,Kod
      ,Nev
      ,Modosithato
      ,Note
      ,BesorolasiSema
    ) 
    values
    (
      @kodcsoportId
      ,'PHSZ_LETTYPE'
      ,'Posta Hibrid Szolgáltatás levél típusok'
      ,'1'
      , null
      ,'0'
    )

  END
  
  declare @org uniqueidentifier
  set @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
  declare @kodtarId uniqueidentifier
set @kodtarId = '1471c143-9146-45c6-8689-fb0468c1e1bb'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_LETTYPE.NM' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'NM',
      'nem levél',
      '1',
      '1'
    )
  END

set @kodtarId = '4e49b2c9-77d1-4e3c-a143-c9b064e1dad9'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_LETTYPE.N' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'N',
      'közönséges küldemény',
      '2',
      '1'
    )
  END

set @kodtarId = 'e2b78615-01be-4193-b1b5-44e47bfaceb4'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_LETTYPE.E' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'E',
      'elsőbbségi küldemény',
      '3',
      '1'
    )
  END

set @kodtarId = '5cead1cd-23cc-44b8-a9cc-ae6a90cd5642'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_LETTYPE.A' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'A',
      'könyvelt küldemény',
      '4',
      '1'
    )
  END

set @kodtarId = '794d51a1-15b8-4326-847e-dcf1db73b09c'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_LETTYPE.T' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'T',
      'könyvelt küldemény tértivevénnyel',
      '5',
      '1'
    )
  END

set @kodtarId = 'd31628d6-eaf9-4d8b-8016-309d1e2b64c7'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_LETTYPE.AE' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'AE',
      'elsőbbségi könyvelt küldemény',
      '6',
      '1'
    )
  END

set @kodtarId = 'b6d173c0-2846-4c29-98d2-35dcf18c7e76'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_LETTYPE.TE' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'TE',
      'elsőbbségi könyvelt küldemény tértivevénnyel',
      '7',
      '1'
    )
  END

set @kodtarId = 'b1ea2772-6c67-4b57-8623-ca5c349db955'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_LETTYPE.OT' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'OT',
      'hivatalos levél tértivevénnyel',
      '8',
      '1'
    )
  END

GO

declare @kodcsoportId uniqueidentifier
  set @kodcsoportId = '6e6f9cc6-7648-4e33-b21b-c44819bc5889'

  IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @kodcsoportId)
  BEGIN
     print 'Insert KRT_KodCsoportok - PHSZ_ENVTYPE'

       insert into KRT_KodCsoportok
    (
       Id
      ,Kod
      ,Nev
      ,Modosithato
      ,Note
      ,BesorolasiSema
    ) 
    values
    (
      @kodcsoportId
      ,'PHSZ_ENVTYPE'
      ,'Posta Hibrid Szolgáltatás boríték típusok'
      ,'1'
      , null
      ,'0'
    )

  END
  
  declare @org uniqueidentifier
  set @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
  declare @kodtarId uniqueidentifier
set @kodtarId = 'df8da40c-7b04-404c-a1e1-abb31d95f4d8'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_ENVTYPE.C4' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'C4',
      'C4',
      '1',
      '1'
    )
  END

set @kodtarId = '10313083-190b-4733-bfb8-01b95ccd19dd'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_ENVTYPE.C5' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'C5',
      'C5',
      '2',
      '1'
    )
  END

set @kodtarId = 'b8cf5e40-6845-4d94-99c1-bb42ead68be8'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_ENVTYPE.C65' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'C65',
      'C65',
      '3',
      '1'
    )
  END

set @kodtarId = 'b466c27d-f559-4dc7-9951-53afe00b8146'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_ENVTYPE.TC4' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'TC4',
      'TC4',
      '4',
      '1'
    )
  END

set @kodtarId = '611469e7-de44-433c-8f75-007b768cd5b4'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_ENVTYPE.TB4' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'TB4',
      'TB4',
      '5',
      '1'
    )
  END

GO

declare @kodcsoportId uniqueidentifier
  set @kodcsoportId = 'ae2a973c-580b-4cdf-93cc-01e1b1952e18'

  IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @kodcsoportId)
  BEGIN
     print 'Insert KRT_KodCsoportok - PHSZ_ENVADDR'

       insert into KRT_KodCsoportok
    (
       Id
      ,Kod
      ,Nev
      ,Modosithato
      ,Note
      ,BesorolasiSema
    ) 
    values
    (
      @kodcsoportId
      ,'PHSZ_ENVADDR'
      ,'Posta Hibrid Szolgáltatás boríték címzés típusok'
      ,'1'
      , null
      ,'0'
    )

  END
  
  declare @org uniqueidentifier
  set @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
  declare @kodtarId uniqueidentifier
set @kodtarId = '2474bce5-e902-4a9a-aba8-2ed6eec86a1c'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_ENVADDR.N' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'N',
      'egyiket sem',
      '1',
      '1'
    )
  END

set @kodtarId = '569d60aa-fc7b-41b2-b79b-8936f153146e'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_ENVADDR.S' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'S',
      'feladót',
      '2',
      '1'
    )
  END

set @kodtarId = '22cd7de9-1cfd-44e1-b3db-cd0739fd7d46'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_ENVADDR.R' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'R',
      'címzettet',
      '3',
      '1'
    )
  END

set @kodtarId = 'e1a84e82-5d1a-4704-b677-43c74115a9ea'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_ENVADDR.A' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'A',
      'mind a feladót és a címzettet',
      '4',
      '1'
    )
  END

GO

declare @kodcsoportId uniqueidentifier
  set @kodcsoportId = '3aaed898-4124-4f75-9de0-2afbb3728024'

  IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @kodcsoportId)
  BEGIN
     print 'Insert KRT_KodCsoportok - PHSZ_CLEANADDR'

       insert into KRT_KodCsoportok
    (
       Id
      ,Kod
      ,Nev
      ,Modosithato
      ,Note
      ,BesorolasiSema
    ) 
    values
    (
      @kodcsoportId
      ,'PHSZ_CLEANADDR'
      ,'Posta Hibrid Szolgáltatás hibás címzés kezelés típusok'
      ,'1'
      , null
      ,'0'
    )

  END
  
  declare @org uniqueidentifier
  set @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
  declare @kodtarId uniqueidentifier
set @kodtarId = '98397265-ef6b-4621-bcae-cafcd1fb239f'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_CLEANADDR.SPM' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato,
	   ErvVege
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'SPM',
      'Helyettesít, Nyomtat, Értesít',
      '1',
      '1',
	  GETDATE()
    )
  END

set @kodtarId = 'f63534d0-b4bd-4f4d-bfe5-c15127d9e33d'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_CLEANADDR.SNPM' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato,
	   ErvVege
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'SNPM',
      'Helyettesít, Nem nyomtat, Értesít',
      '2',
      '1',
	  GETDATE()
    )
  END

set @kodtarId = '12ec0dc0-574f-422a-974a-ca8063041ae7'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_CLEANADDR.NSPM' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato,
	   ErvVege
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'NSPM',
      'Nem helyettesít, Nyomtat, Értesít',
      '3',
      '1',
	  GETDATE()
    )
  END

set @kodtarId = '33fd8cf9-bd25-44f4-9f00-1acd12979233'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_CLEANADDR.SPNM' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato,
	   ErvVege
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'SPNM',
      'Helyettesít, Nyomtat, Nem értesít',
      '4',
      '1',
	  GETDATE()
    )
  END

set @kodtarId = '06f2b131-e6e7-4a52-ae35-37e1c267fece'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_CLEANADDR.NSPNM' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'NSPNM',
      'Nem helyettesít, Nyomtat, Nem értesít',
      '5',
      '1'
    )
  END

 GO

 declare @kodcsoportId uniqueidentifier
  set @kodcsoportId = '481bf1d3-d423-42d0-9ec4-178179708460'

  IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @kodcsoportId)
  BEGIN
     print 'Insert KRT_KodCsoportok - PHSZ_AWAYMARKER'

       insert into KRT_KodCsoportok
    (
       Id
      ,Kod
      ,Nev
      ,Modosithato
      ,Note
      ,BesorolasiSema
    ) 
    values
    (
      @kodcsoportId
      ,'PHSZ_AWAYMARKER'
      ,'Posta Hibrid Szolgáltatás átvevő távolléte esetén alkalmazott értesítés típusok'
      ,'1'
      , null
      ,'0'
    )

  END
  
  declare @org uniqueidentifier
  set @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
  declare @kodtarId uniqueidentifier
set @kodtarId = 'a344465e-9335-499a-885f-c775a973a8e3'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_AWAYMARKER.1' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '1',
      'Értesítés hivatalos irat érkezéséről közigazgatási ügyben',
      '1',
      '1'
    )
  END

set @kodtarId = 'ad5d36f2-b7ee-4571-9e7f-0740ec13fc85'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_AWAYMARKER.1/SK' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '1/SK',
      'Értesítés saját kezéhez kézbesítendő hivatalos irat érkezéséről közigazgatási ügyben',
      '2',
      '1'
    )
  END

set @kodtarId = 'adc555e1-33df-4a41-9e8f-82a13e3f8e46'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_AWAYMARKER.2' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '2',
      'Értesítés polgári ügyben keletkezett hivatalos irat érkezéséről',
      '3',
      '1'
    )
  END

set @kodtarId = 'e487e78f-195a-4230-8384-a99119002a7f'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_AWAYMARKER.3' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '3',
      'Értesítés hivatalos irat érkezéséről büntetőügyben',
      '4',
      '1'
    )
  END

set @kodtarId = '4d6c8d19-a253-4640-98f6-ac2e3f0cb3da'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_AWAYMARKER.4' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '4',
      'Értesítés saját kezéhez kézbesítendő hivatalos irat érkezéséről büntetőügyben',
      '5',
      '1'
    )
  END

set @kodtarId = '7945891d-5224-4fef-a6bc-41b8fc52fab5'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_AWAYMARKER.5' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '5',
      'Értesítés saját kezéhez kézbesítendő hivatalos irat érkezéséről büntetővégzés meghozatalára irányuló büntetőügyben',
      '6',
      '1'
    )
  END

set @kodtarId = '724e30ec-2716-4329-87fa-237cb20ddac6'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_AWAYMARKER.6' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '6',
      'Értesítés saját kezéhez kézbesítendő hivatalos irat érkezéséről polgári ügyben',
      '7',
      '1'
    )
  END

set @kodtarId = '412daa4b-1290-4037-98eb-21184b919e39'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_AWAYMARKER.7' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '7',
      'Értesítés belföldi bíróság megkeresésével feladott nemzetközi viszonylatú hivatalos irat érkezéséről',
      '8',
      '1'
    )
  END

set @kodtarId = '9c477b42-60b8-4047-a393-94d220e9b459'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_AWAYMARKER.7/2' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '7/2',
      'Értesítés nemzetközi viszonylatú hivatalos irat érkezéséről',
      '9',
      '1'
    )
  END

set @kodtarId = '5fa5a949-6fc1-444b-9265-2c4e099e4ca8'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_AWAYMARKER.8' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '8',
      'Értesítés hivatalos irat érkezéséről szabálysértési ügyben',
      '10',
      '1'
    )
  END

set @kodtarId = '1d999913-aec4-4f2b-8f75-a579d20849ea'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_AWAYMARKER.9' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '9',
      'Értesítés saját kezéhez kézbesítendő hivatalos irat érkezéséről szabálysértési ügyben',
      '11',
      '1'
    )
  END

set @kodtarId = 'bbd93e52-5cdf-416b-9030-aa9111c20837'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_AWAYMARKER.10' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '10',
      'Értesítés hivatalos irat érkezéséről sportfegyelmi ügyben',
      '12',
      '1'
    )
  END

set @kodtarId = '02791bf0-4e97-4f98-b89f-0e57ed0ecdf7'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_AWAYMARKER.10/SK' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '10/SK',
      'Értesítés saját kezéhez kézbesítendő hivatalos irat érkezéséről sportfegyelmi ügyben',
      '13',
      '1'
    )
  END

GO

declare @kodcsoportId uniqueidentifier
  set @kodcsoportId = '60c20e8c-787a-47bf-9737-43e90ff50996'

  IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @kodcsoportId)
  BEGIN
     print 'Insert KRT_KodCsoportok - HSZ_PAPERTYPE'

       insert into KRT_KodCsoportok
    (
       Id
      ,Kod
      ,Nev
      ,Modosithato
      ,Note
      ,BesorolasiSema
    ) 
    values
    (
      @kodcsoportId
      ,'PHSZ_PAPERTYPE'
      ,'Posta Hibrid Szolgáltatás papír típusok'
      ,'1'
      , null
      ,'0'
    )

  END
  
  declare @org uniqueidentifier
  set @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
  declare @kodtarId uniqueidentifier
set @kodtarId = '5d5ce9b9-2a8b-4273-90b3-15ca5699893c'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - HSZ_PAPERTYPE.StandardPaper' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'StandardPaper',
      'Normál papír',
      '1',
      '1'
    )
  END

GO

declare @kodcsoportId uniqueidentifier
  set @kodcsoportId = '2ba55fdb-1d5c-4308-8ae8-07b6cc1f11f0'

  IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @kodcsoportId)
  BEGIN
     print 'Insert KRT_KodCsoportok - PHSZ_PAPERSIZE'

       insert into KRT_KodCsoportok
    (
       Id
      ,Kod
      ,Nev
      ,Modosithato
      ,Note
      ,BesorolasiSema
    ) 
    values
    (
      @kodcsoportId
      ,'PHSZ_PAPERSIZE'
      ,'Posta Hibrid Szolgáltatás papír méretek'
      ,'1'
      , null
      ,'0'
    )

  END
  
  declare @org uniqueidentifier
  set @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
  declare @kodtarId uniqueidentifier
set @kodtarId = '5e43808d-cdd4-41df-876e-d6ceec6cc81d'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_PAPERSIZE.A0' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'A0',
      'A0',
      '1',
      '1'
    )
  END

set @kodtarId = '0334d5b7-1156-4fa8-9c87-6476b46a7403'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_PAPERSIZE.A1' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'A1',
      'A1',
      '2',
      '1'
    )
  END

set @kodtarId = '59e791c5-6741-4beb-a458-99494d5ab15c'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_PAPERSIZE.A2' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'A2',
      'A2',
      '3',
      '1'
    )
  END

set @kodtarId = 'c3ff9a65-04e9-4afb-9e17-1e6c30794383'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_PAPERSIZE.A3' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'A3',
      'A3',
      '4',
      '1'
    )
  END

set @kodtarId = '2562d93f-9755-4039-a6d7-6764a6c7f2b1'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_PAPERSIZE.A4' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'A4',
      'A4',
      '5',
      '1'
    )
  END

set @kodtarId = '9e9c2b46-9d70-442c-8d53-a669419c46bd'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_PAPERSIZE.A5' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'A5',
      'A5',
      '6',
      '1'
    )
  END

set @kodtarId = 'cfb685f3-26e8-4f7a-bd47-46a63ba5815b'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_PAPERSIZE.A6' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'A6',
      'A6',
      '7',
      '1'
    )
  END

set @kodtarId = '97fda561-e2bc-44de-9e34-7ab67e7f3688'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_PAPERSIZE.4INC' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '4INC',
      '4 1/6 inch magas és 210 milliméter széles papír',
      '8',
      '1'
    )
  END

set @kodtarId = 'a8a74519-9806-4138-8ecd-fe752d6a8c68'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_PAPERSIZE.12INC' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '12INC',
      'A4-es lap 8 mm-rel történő meghosszabbított változata',
      '9',
      '1'
    )
  END

GO

declare @kodcsoportId uniqueidentifier
  set @kodcsoportId = '83e76873-480c-4f26-8bbc-52e173dea1ca'

  IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @kodcsoportId)
  BEGIN
     print 'Insert KRT_KodCsoportok - PHSZ_FINISHING'

       insert into KRT_KodCsoportok
    (
       Id
      ,Kod
      ,Nev
      ,Modosithato
      ,Note
      ,BesorolasiSema
    ) 
    values
    (
      @kodcsoportId
      ,'PHSZ_FINISHING'
      ,'Posta Hibrid Szolgáltatás készre gyártás típusok'
      ,'1'
      , null
      ,'0'
    )

  END
  
  declare @org uniqueidentifier
  set @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
  declare @kodtarId uniqueidentifier
set @kodtarId = '5fbf5235-6ad9-4ad1-b383-d3e151992f0b'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_FINISHING.Enveloped' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'Enveloped',
      'borítékolt',
      '1',
      '1'
    )
  END

set @kodtarId = 'a7124992-59f4-4aad-be1e-7efa0ca47efe'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_FINISHING.Folded' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'Folded',
      'hajtogatott',
      '2',
      '1'
    )
  END

set @kodtarId = 'ac34a954-8a58-4b8a-9db4-d2ed8d93b1c8'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_FINISHING.Cutting' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'Cutting',
      'vágott',
      '3',
      '1'
    )
  END

set @kodtarId = 'c4691e94-4916-4104-82a9-823ac6da1cd8'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - PHSZ_FINISHING.NoFinishing' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      'NoFinishing',
      'nem kezelendő',
      '4',
      '1'
    )
  END

GO

DECLARE @recordId uniqueidentifier 
SET @recordId = '203201E6-765E-4DE6-B289-F287A2DD178B'

IF NOT EXISTS (Select 1 FROM KRT_Parameterek where Id = @recordId)
BEGIN 
	Print 'INSERT - KRT_Parameterek - PHSZ_ContractNumber' 
	 insert into KRT_Parameterek(
		Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,@recordId
		,'PHSZ_ContractNumber'
		,'123456789'
		,'1'
		,'A postával kötött szerződés azonosítója'
		); 
 END 

 GO


declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '025C2C0E-368D-4E19-A660-36B146F5F6C8'
declare @recordId uniqueidentifier
set @recordId = '668208B1-B27F-4746-8E28-353DBFE2EDB1'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
    print 'Add ADATHORDOZO_TIPUSA - Posta Hibrid Szolgáltatás visszaigazolás'

	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'17'
	,'Posta Hibrid Szolgáltatás visszaigazolás'
	,'1'
	,'17'
	); 
END 

GO

--------------------------------BLG_2965_HSZ_szolgáltatás_Posta_Hibrid-------------------------------


-------------------------------- BUG_5518 - Partnerkapcsoaltok érvénytelenítése ------------------------------
--*Javítja a hibásan érvénytelenített partnerkapcsolatokat*
UPDATE [dbo].[KRT_KodTarak]
   SET ErvVege = '4700-12-31 00:00:00.000'
   WHERE KodCsoport_Id = 'A17AB343-1EB3-4035-868C-7E48F2AC3019' AND Kod IN ('11','3','4','5','6');
GO
-------------------------------- BUG_5518 - Partnerkapcsoaltok érvénytelenítése ------------------------------
------------------------------------------------- VÉGE -------------------------------------------------------


GO


------------------- BLG_4574 update script for old records -----------------------------------------

UPDATE [dbo].[EREC_IraIktatoKonyvek]
   SET Statusz = '0'
  WHERE ErvVege != '4700-12-31 00:00:00.000'
GO
------------------- BLG_4574 update script for old records -----------------------------------------
------------------- VÉGE -----------------------------------------



----------------------------------------------------------------------------------------------------
PRINT 'BLG_1642'
DECLARE @ORG UNIQUEIDENTIFIER
SET @ORG = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @KodCsoport_Id_eBeadCelrendszer uniqueidentifier
SET @KodCsoport_Id_eBeadCelrendszer = '9992381D-FFFF-4AE2-83E0-32F440191C9E'

IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = @KodCsoport_Id_eBeadCelrendszer)
	BEGIN 
		Print 'INSERT KCS ebeadvany celrendszer' 
		insert into KRT_KodCsoportok(
		 BesorolasiSema
		,Id
		,Kod
		,Nev
		,Modosithato
		,Note
		) values (
		'0'
		,@KodCsoport_Id_eBeadCelrendszer
		,'eBeadvany_CelRendszer'
		,'eBeadvany_CelRendszer'
		,'0'
		,'eBeadvany_CelRendszer'
		); 
	END
GO
-------------
declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '9992381D-FFFF-4AE2-83E0-32F440191C9E'
declare @recordId_CelRend1 uniqueidentifier
set @recordId_CelRend1 = '9992381D-FFFF-4AE2-83E0-32F440191C91'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId_CelRend1)
BEGIN 	
    print 'Add eBeadvany_CelRendszer - WEB'

	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId_CelRend1
	,@Org
	,@kodcsoportId
	,'WEB'
	,'EDOK'
	,'0'
	,'1'
	); 
END 
GO
-----------
declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '9992381D-FFFF-4AE2-83E0-32F440191C9E'
declare @recordId_CelRend2 uniqueidentifier
set @recordId_CelRend2 = '9992381D-FFFF-4AE2-83E0-32F440191C92'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId_CelRend2)
BEGIN 	
    print 'Add eBeadvany_CelRendszer - HAIR'

	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId_CelRend2
	,@Org
	,@kodcsoportId
	,'HAIR'
	,'HAIR'
	,'0'
	,'1'
	); 
END 
GO
----------------------
declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '9992381D-FFFF-4AE2-83E0-32F440191C9E'
declare @recordId_CelRend3 uniqueidentifier
set @recordId_CelRend3 = '9992381D-FFFF-4AE2-83E0-32F440191C93'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId_CelRend3)
BEGIN 	
    print 'Add eBeadvany_CelRendszer - FILESHARE'

	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId_CelRend3
	,@Org
	,@kodcsoportId
	,'FILESHARE'
	,'FILESHARE'
	,'0'
	,'1'
	); 
END 
GO
-------------------------------- BLG_1643_Bejövő Hivatali Kapus Üzenetek Automatikus érkeztetése start -------------------------------
-- BLG_1643
-- Bejövő Hivatali Kapus Üzenetek Automatikus érkeztetése	 

DECLARE @record_id uniqueidentifier 
SET @record_id = 'F86A1EB1-F959-4246-9F91-6DD4CE85329B'
Print '@record_id='+convert(NVARCHAR(100),@record_id) 


IF NOT EXISTS (select 1 from INT_AutoEUzenetSzabaly where id = @record_id)
BEGIN 
	Print 'INSERT' 
	INSERT INTO [dbo].[INT_AutoEUzenetSzabaly]
           ([Id]
           ,[Nev]
           ,[Leiras]
           ,[Szabaly]
           ,[Sorrend]
           ,[Tipus]
           ,[Leallito]
           ,[Note])
     VALUES ( @record_id
           ,'HKPErkeztetes'
           ,'Automatikus érkeztetés'
           ,'{"VersionProperty":"1.0.0","IdProperty":"f86a1eb1-f959-4246-9f91-6dd4ce85329b","NameProperty":null,"OrderProperty":9999,"ParametersProperty":{"NameProperty":"HKP Érkeztetés","DescriptionProperty":"Automatikus érkeztetés","StopRuleProperty":true,"TypeProperty":0,"UserIdProperty":"54e861a5-36ed-44ca-baa7-c287d125b309","UserGroupProperty":"bd00c8d0-cf99-4dfc-8792-33220b7bfcc6"},"Conditions":[{"FieldProperty":"KR_ErkeztetesiSzam","IsXmlAttributeProperty":false,"IsXmlElementProperty":false,"OperatorProperty":6,"ConditionProperty":"NULL"}],"Operation":{"ParameterProperty":{"Parameters":[{"IdProperty":"ErkeztetoKonyv","NameProperty":"ErkeztetoKonyv","ValueProperty":"08d139d8-38ec-4370-9de8-c62523be8428"},{"IdProperty":"ErkeztetoKonyvMegkulonboztetoJel","NameProperty":"ErkeztetoKonyvMegkulonboztetoJel","ValueProperty":null},{"IdProperty":"Cimzett","NameProperty":"Cimzett","ValueProperty":"54e861a5-36ed-44ca-baa7-c287d125b309"},{"IdProperty":"Felelos","NameProperty":"Felelos","ValueProperty":"54e861a5-36ed-44ca-baa7-c287d125b309"}],"OperationParameterType":0,"TypeFullName":"Contentum.eUtility.ElektronikusKuldemenyFeldolgozas.EKFOperationParameters"},"OperationName":1,"OperationType":1,"TypeFullName":"Contentum.eUtility.ElektronikusKuldemenyFeldolgozas.EKFMainOperationErkeztetes","MessagesEKF":null},"PostOperations":[{"ParameterProperty":{"Parameters":[{"IdProperty":"Riport_Szerver_Url","NameProperty":"Riport_Szerver_Url","ValueProperty":"http://ax-vfphtst02/ReportServer"},{"IdProperty":"Valasz_Uzenet_Riport_Sablon","NameProperty":"Valasz_Uzenet_Riport_Sablon","ValueProperty":"/NMHH_R2T/Sablonok/eBeadvanyAutoValasz"},{"IdProperty":"Elektronikus_Alairas_Kell","NameProperty":"Elektronikus_Alairas_Kell","ValueProperty":"False"},{"IdProperty":"Iktatni_Kell","NameProperty":"Iktatni_Kell","ValueProperty":"False"},{"IdProperty":"Irattarozni_Kell","NameProperty":"Irattarozni_Kell","ValueProperty":"False"},{"IdProperty":"Email_Kuldo","NameProperty":"Email_Kuldo","ValueProperty":""},{"IdProperty":"Email_CC","NameProperty":"Email_CC","ValueProperty":""},{"IdProperty":"Email_BCC","NameProperty":"Email_BCC","ValueProperty":""},{"IdProperty":"Email_Targy","NameProperty":"Email_Targy","ValueProperty":""},{"IdProperty":"Valasz_Uzenet_Sablon_Expedialas_Modja","NameProperty":"Valasz_Uzenet_Sablon_Expedialas_Modja","ValueProperty":"19"}],"OperationParameterType":0,"TypeFullName":"Contentum.eUtility.ElektronikusKuldemenyFeldolgozas.EKFOperationParameters"},"OperationName":8,"OperationType":2,"TypeFullName":"Contentum.eUtility.ElektronikusKuldemenyFeldolgozas.EKFPostOperationAutoValasz","MessagesEKF":null}]}'
           ,9999
           ,'ElektronikusBeadvany'
           ,'T'
           ,'Feltétel: KR_ErkeztetesiSzam NEM_EGYENLO NULL Művelet: ERKEZTETES UtóMűvelet: AUTO_VALASZ'
			);
 END 

GO

-------------------------------- BLG_1643_Bejövő Hivatali Kapus Üzenetek Automatikus érkeztetése end -------------------------------
-----------------------------------------------------------------------------------------------------
--- BUG_5588 Hiányzó rendszerparaméterek

-- IKTATAS_UGYTIPUS_IRATMETADEFBOL rendszerparameter	 
-- FPH = BOPMH = 1, NMHH = 0

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='7B8BBA83-8997-E711-80C6-00155D020BD9') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'7B8BBA83-8997-E711-80C6-00155D020BD9'
		,'IKTATAS_UGYTIPUS_IRATMETADEFBOL'
		,'1'
		,'0'
		,'Ügytipus választás iratmetadefinició alapján = 1, Nincs iratmetadefiniciós összerendelés = 0'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='7B8BBA83-8997-E711-80C6-00155D020BD9'
	--	,Nev='IKTATAS_UGYTIPUS_IRATMETADEFBOL'
	--	,Ertek='1'
	--	,Karbantarthato='0'
	--	,Note='Ügytipus választás iratmetadefinició alapján = 1, Nincs iratmetadefiniciós összerendelés = 0'
	-- WHERE Id=@record_id 
 --END

-- if @org_kod = 'NMHH' 
--BEGIN 
--	Print @org_kod +': IKTATAS_UGYTIPUS_IRATMETADEFBOL = 0'
--	UPDATE KRT_Parameterek
--	   SET Ertek='0'
--	   WHERE Id='7B8BBA83-8997-E711-80C6-00155D020BD9'
--END

GO

 -- REGEXP_PATTERN_EMAIL_ADDRESS rendszerparaméter létrehozás
 DECLARE @RC int
DECLARE @Id uniqueidentifier
DECLARE @Org uniqueidentifier
DECLARE @Felhasznalo_id uniqueidentifier
DECLARE @Nev nvarchar(400)
DECLARE @Ertek nvarchar(400)
DECLARE @Karbantarthato char(1)
DECLARE @Ver int
DECLARE @Note nvarchar(4000)
DECLARE @Stat_id uniqueidentifier
DECLARE @ErvKezd datetime
DECLARE @ErvVege datetime
DECLARE @Letrehozo_id uniqueidentifier
DECLARE @LetrehozasIdo datetime
DECLARE @Modosito_id uniqueidentifier
DECLARE @ModositasIdo datetime
DECLARE @Zarolo_id uniqueidentifier
DECLARE @ZarolasIdo datetime
DECLARE @Tranz_id uniqueidentifier
DECLARE @UIAccessLog_id uniqueidentifier
DECLARE @UpdatedColumns xml
DECLARE @ResultUid uniqueidentifier

SET @Id = '011A800E-2418-4AAB-8E05-74D9F09BB4D8'
SET @Nev = 'REGEXP_PATTERN_EMAIL_ADDRESS'
SET @Ertek = N'^(?(")(".+?(?<!\\)"@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&''\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$'
SET @Note = 'Regular expression for email address'
SET @Karbantarthato = '1'
SET @Letrehozo_id = '54E861A5-36ED-44CA-BAA7-C287D125B309'

IF NOT EXISTS (select 1 from KRT_Parameterek where Id = @Id)
BEGIN
	EXECUTE @RC = [dbo].[sp_KRT_ParameterekInsert] 
	   @Id
	  ,@Org
	  ,@Felhasznalo_id
	  ,@Nev
	  ,@Ertek
	  ,@Karbantarthato
	  ,@Ver
	  ,@Note
	  ,@Stat_id
	  ,@ErvKezd
	  ,@ErvVege
	  ,@Letrehozo_id
	  ,@LetrehozasIdo
	  ,@Modosito_id
	  ,@ModositasIdo
	  ,@Zarolo_id
	  ,@ZarolasIdo
	  ,@Tranz_id
	  ,@UIAccessLog_id
	  ,@UpdatedColumns
	  ,@ResultUid OUTPUT
END

GO

-- UGYINTEZESI_IDO_FELFUGGESZTES rendszerparaméter rögzítés
DECLARE @RC int
DECLARE @Id uniqueidentifier
DECLARE @Org uniqueidentifier
DECLARE @Felhasznalo_id uniqueidentifier
DECLARE @Nev nvarchar(400)
DECLARE @Ertek nvarchar(400)
DECLARE @Karbantarthato char(1)
DECLARE @Ver int
DECLARE @Note nvarchar(4000)
DECLARE @Stat_id uniqueidentifier
DECLARE @ErvKezd datetime
DECLARE @ErvVege datetime
DECLARE @Letrehozo_id uniqueidentifier
DECLARE @LetrehozasIdo datetime
DECLARE @Modosito_id uniqueidentifier
DECLARE @ModositasIdo datetime
DECLARE @Zarolo_id uniqueidentifier
DECLARE @ZarolasIdo datetime
DECLARE @Tranz_id uniqueidentifier
DECLARE @UIAccessLog_id uniqueidentifier
DECLARE @UpdatedColumns xml
DECLARE @ResultUid uniqueidentifier

SET @Id = 'BC67EEBF-D437-4BE8-92EC-7732F5C02AD8'
SET @Nev = 'UGYINTEZESI_IDO_FELFUGGESZTES'
SET @Ertek = 'FANCY'
SET @Note = 'FANCY,SIMPLE'
SET @Karbantarthato = '1'
SET @Letrehozo_id = '54E861A5-36ED-44CA-BAA7-C287D125B309'

IF NOT EXISTS (select 1 from KRT_Parameterek where Id = @Id)
BEGIN
	EXECUTE @RC = [dbo].[sp_KRT_ParameterekInsert] 
	   @Id
	  ,@Org
	  ,@Felhasznalo_id
	  ,@Nev
	  ,@Ertek
	  ,@Karbantarthato
	  ,@Ver
	  ,@Note
	  ,@Stat_id
	  ,@ErvKezd
	  ,@ErvVege
	  ,@Letrehozo_id
	  ,@LetrehozasIdo
	  ,@Modosito_id
	  ,@ModositasIdo
	  ,@Zarolo_id
	  ,@ZarolasIdo
	  ,@Tranz_id
	  ,@UIAccessLog_id
	  ,@UpdatedColumns
	  ,@ResultUid OUTPUT
END

GO

-- UGYIRAT_HATARIDO_KEZELES rendszerparameter
print '''UGYIRAT_HATARIDO_KEZELES'' rendszerparameter hozzaadasa'

declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9' 
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id=@org)

print 'Org = ' + @org_kod

declare @tranz_id uniqueidentifier = newid()

declare @record_id uniqueidentifier = 'C662BF4F-DB33-4094-938D-509B90F0A24A'

declare @Ertek nvarchar(400)

set @Ertek = (case @org_kod when 'NMHH' then '1' else '0' end)

print '@record_id = ' + cast(@record_id as nvarchar(36))

print '@Ertek = ' + @Ertek

declare @Nev nvarchar(400) = 'UGYIRAT_HATARIDO_KEZELES'

declare @Note nvarchar(4000) = 'Ügyirat határidő számítási módja. Értékek: 0 - mostani, 1 - NMHH.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT: ' + @Nev 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE: ' + @Nev
	-- UPDATE KRT_Parameterek
	-- SET 
	--     Tranz_id=@tranz_id
	--	,Org=@org
	--	,Id= @record_id
	--	,Nev = @Nev
	--	,Ertek= @Ertek
	--	,Karbantarthato='1'
	--	,Note = @Note
	-- WHERE Id=@record_id 
 --END
 GO

declare @recordid uniqueidentifier
declare @tranz_id uniqueidentifier
DECLARE @orgkod nvarchar(100) 
declare @org uniqueidentifier 
 -- ÜGYTIPUS_STATISZTIKA
SET @recordId = '1C519435-7757-4B88-9104-A02558E63CC7'
set @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

set @orgkod = (select kod from krt_orgok where Id = @org)

declare @Ertek nvarchar(400)

IF @OrgKod = 'NMHH'
	set @Ertek = '1'
ELSE
	set @Ertek = '0'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE Id = @recordId)
BEGIN 
	Print 'insert - UGYTIPUS_STATISZTIKA - '  + @Ertek
	 insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @Tranz_Id
		,@Org
		,@recordId
		,'UGYTIPUS_STATISZTIKA'
		,@Ertek
		,'1'
		,'Ügyirat- és irattípus statisztika megjelenik-e.'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'update - UGYTIPUS_STATISZTIKA - '  + @Ertek
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org=@Org
	--	,Id=@recordId
	--	,Nev='UGYTIPUS_STATISZTIKA'
	--	,Ertek=@Ertek
	--	,Karbantarthato='1'
	--	,Note='Ügyirat- és irattípus statisztika megjelenik-e.'
	-- WHERE Id=@recordId 
 --END
 GO

 -- EXPEDIALAS_MODJA_DEFAULT rendszerparameter
print '''EXPEDIALAS_MODJA_DEFAULT'' rendszerparameter hozzaadasa'

declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'  
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id=@org)

print 'Org = ' + @org_kod

declare @tranz_id uniqueidentifier = newid()

declare @record_id uniqueidentifier = '06EBA217-7AEE-495F-860C-3D5E30CD5DAE'

declare @Ertek nvarchar(400)

-- BOPMH: Posta - hivatalos, FPH: Helyben
set @Ertek = (case @org_kod when 'BOPMH' then '01' else '13' end)

print '@record_id = ' + cast(@record_id as nvarchar(36))

print '@Ertek = ' + @Ertek

declare @Nev nvarchar(400) = 'EXPEDIALAS_MODJA_DEFAULT'

declare @Note nvarchar(4000) = 'Word-ból iktatás esetén az alapértelmezetten beállított expediálás mód kódja.'

if not exists(select 1 from KRT_Parameterek where Id= @record_id)
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		, Note
		) values (
		 @tranz_id
		,@org
		,@record_id
		,@Nev
		,@Ertek
		,'1'
		,@Note
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--     Tranz_id=@tranz_id
	--	,Org=@org
	--	,Id= @record_id
	--	,Nev= @Nev
	--	,Ertek= @Ertek
	--	,Karbantarthato='1'
	--	,Note = @Note
	-- WHERE Id=@record_id 
 --END
GO

-- ELINTEZETTE_NYILVANITAS_ENABLED rendszerparameter	 
-- FPH = BOPMH = 0, CSEPEL = NMHH = 1

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='A1F0D678-A5BB-4911-B585-A00100C84E9F') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

declare @ertek nvarchar(400)

IF @org_kod = 'NMHH' or @org_kod = 'CSPH'
	set @ertek = '1'
else
	set @ertek = '0'

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'A1F0D678-A5BB-4911-B585-A00100C84E9F'
		,'ELINTEZETTE_NYILVANITAS_ENABLED'
		,'0'
		,'1'
		,'Értékek: Igen: 1, i, igen, true. Nem: minden más érték. A hagyományos (fph-ban nem hazsnált) elintézetté nyilvánítás funkció engedélyezése.'
		); 
 END 


GO
-----------------------------------------------------------------------------------------------------
-- BUG_5588
-- Eltérő id-val rögzített rendszerparaméterek egységesítése

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

create table #parameterIds (
		id		uniqueidentifier not null,
		nev	nvarchar(400) not null
	)
			
insert #parameterIds (id, nev)
					values ('BA1D3C71-3DE5-DD11-8945-001EC9E754BC','eDocumentWebServiceUrl')	
insert #parameterIds (id, nev)
					values ('9ABD054E-14EF-DE11-929D-001EC9E754BC' ,'KOLCSONZES_JOVAHAGYAS_BY_UGYINTEZO_DISABLED')	
insert #parameterIds (id, nev)
					values ('B0FE5762-BB02-E811-80C4-00155D027EA9', 'LEVELTARI_ATADAS_DOKUMENTUM')
insert #parameterIds (id, nev)
					values ('B2FE5762-BB02-E811-80C4-00155D027EA9', 'LEVELTARI_ATADAS_IKTATO')	
insert #parameterIds (id, nev)
					values ('B6FE5762-BB02-E811-80C4-00155D027EA9', 'LEVELTARI_ATADAS_IRATKEPZO_AZONOSITOJA')
insert #parameterIds (id, nev)
					values ('B4FE5762-BB02-E811-80C4-00155D027EA9', 'LEVELTARI_ATADAS_IRATKEPZO_NEVE')			
insert #parameterIds (id, nev)
					values ('ACFE5762-BB02-E811-80C4-00155D027EA9', 'LEVELTARI_ATADAS_IRATKEPZO_ROVIDNEVE')			
insert #parameterIds (id, nev)
					values ('BAFE5762-BB02-E811-80C4-00155D027EA9', 'LEVELTARI_ATADAS_LEVELTAR_AZONOSITOJA')
insert #parameterIds (id, nev)
					values ('B8FE5762-BB02-E811-80C4-00155D027EA9', 'LEVELTARI_ATADAS_LEVELTAR_NEVE')
insert #parameterIds (id, nev)
					values ('AEFE5762-BB02-E811-80C4-00155D027EA9', 'LEVELTARI_ATADAS_TIPUS')
insert #parameterIds (id, nev)
					values ('65D2B8B2-2736-4C6B-A19F-3588FA5512D5', 'MAX_ITEM_NUMBER')	
insert #parameterIds (id, nev)
					values ('7B579400-D1BA-40D7-A366-0BB80CAD87B0', 'PKI_INTEGRACIO')
insert #parameterIds (id, nev)
					values ('A97B3467-BC7F-411D-9CCB-58A053FBB255', 'POSTAI_RAGSZAM_EXPEDIALASKOR')					
insert #parameterIds (id, nev)
					values ('F0B0E0F7-D52C-49D5-A47F-06DA48CB58A6', 'TERTIVEVENY_RAGSZAM_KIOSZTAS_MODJA')				
insert #parameterIds (id, nev)
					values ('100F0F73-E2FC-E611-80BF-00155D020B8D', 'TOBB_CIM_PARTNER_KERESOBEN')
insert #parameterIds (id, nev)
					values ('0EF009B6-3331-E811-80C9-00155D020DD3', 'VONALKOD_NYOMTATAS_FELULETEN')					
					
					
DECLARE @id uniqueidentifier
DECLARE @nev nvarchar(400) 					
DECLARE parameterIds_cursor CURSOR FOR  
SELECT id, nev FROM #parameterIds 

OPEN parameterIds_cursor;  


FETCH NEXT FROM parameterIds_cursor
		INTO @id,@nev;  

WHILE @@FETCH_STATUS = 0  
BEGIN  
	IF (SELECT COUNT(*) FROM KRT_Parameterek WHERE nev = @nev 
												   and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' 
												   and ervvege>=getdate()) > 1 
	BEGIN
		DECLARE @topId uniqueidentifier
		SET @topId =(SELECT TOP 1 id FROM KRT_Parameterek WHERE nev = @nev 
												   and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' 
												   and ervvege>=getdate())
		print 'delete (csak egy maradhat) ' + @nev										   
		DELETE FROM KRT_Parameterek WHERE id != @topId 			
										  and nev = @nev 
										  and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' 
										  and ervvege>=getdate()
	END									  
	IF EXISTS(SELECT id FROM KRT_Parameterek WHERE nev = @nev 
												   and id !=@id 
												   and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' 
												   and ervvege>=getdate())	
	BEGIN
		print 'update ' + @nev
		--UPDATE KRT_Parameterek SET id = @id WHERE nev = @nev 
		--										   and id !=@id 
		--										   and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' 
		--										   and ervvege>=getdate()
	END

	FETCH NEXT FROM parameterIds_cursor
		INTO @id,@nev;  
END  

CLOSE parameterIds_cursor;  
DEALLOCATE parameterIds_cursor;  

DROP table #parameterIds		
GO			
-------------------------------------- BUG_5588 end ---------------------------------------------------------------
PRINT 'BLG_350'

-- UGYIRATI_PELDANY_SZUKSEGES rendszerparameter	 
-- FPH = BOPMH = 0, NMHH = 1, TUK = 0

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
			
DECLARE @record_id uniqueidentifier 
SET @record_id = '5BC55766-D4AC-E711-80C6-00155D020BD9'
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

IF NOT EXISTS (SELECT 1 from KRT_Parameterek WHERE ID = @record_id) 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,@record_id
		,'UGYIRATI_PELDANY_SZUKSEGES'
		,'0'
		,'1'
		,'1-e érték esetén a Belső iktatásnál az Ügyirati példány checkbox alapértelmezetten jelölt lesz, 0-nál nem'
		); 
 END 

-- if @org_kod = 'NMHH' 
--BEGIN 
--	IF @isTUK = '1'
--	BEGIN
--		Print @org_kod +'_TUK: UGYIRATI_PELDANY_SZUKSEGES = 0'
--		UPDATE KRT_Parameterek
--		   SET Ertek='0'
--		   WHERE Id='5BC55766-D4AC-E711-80C6-00155D020BD9'
--	END
--	ELSE
--	BEGIN
--		Print @org_kod +': UGYIRATI_PELDANY_SZUKSEGES = 1'
--		UPDATE KRT_Parameterek
--		   SET Ertek='1'
--		   WHERE Id='5BC55766-D4AC-E711-80C6-00155D020BD9'
--	END
--END

----------------------------------------------------------------------

GO
--------------------------------------------------Tömeges iktatás (Paraméterezett)-----------------------

DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

IF @org_kod = 'BOPMH'
BEGIN 
	DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Menuk
				where Id='51B0E2FC-5AB2-4FB7-AD7D-A12878E48B1F') 

	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
		BEGIN 
		
	Print 'INSERT menü: Tömeges iktatás (Paraméterezett)' 
			 insert into KRT_Menuk(
				 Menu_Id_Szulo
				,Tranz_id
				,Stat_id
				,Id
				,Kod
				,Nev
				,Modul_Id
				,Sorrend		
				,Funkcio_Id	
				,Org
				) values (
				 '7723F976-C4E8-46E1-AED6-4EB0E8BE382B'
				,'f60ad910-541a-470d-b888-da5a6a061668'
				,'a0848405-e664-4e79-8fab-cfeca4a290af'
				,'51B0E2FC-5AB2-4FB7-AD7D-A12878E48B1F'
				,'eRecord'
				,'Tömeges iktatás (Paraméterezett)'
				,'8D253CE4-FDDE-E611-829A-4C3488C40116'
				,'32'
				,'37BA9C69-FFDE-E611-829A-4C3488C40116' -- Tömeges iktatás funkciójog
				,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
				); 
		 
			 
	END 

END

GO

--------------------------------------------------Tömeges iktatás (Paraméterezett)-----------------------

--------------------------------------------------EMAIL_SERVER--------------------------------------------
DECLARE @record_id uniqueidentifier 

SET @record_id = '08960DAC-A8D1-4F48-982A-970FD13D2E7D'
if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - EMAIL_SERVER_PORT' 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'EMAIL_SERVER_PORT'
		,'0'
		,'1'
		,'Az email szerver portja. 0 - alapértelmezett port.'
		); 
 END
 
SET @record_id = '318A00B7-F532-41BF-91E1-CF82C2998FF2'
if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - EMAIL_SERVER_USERNAME' 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'EMAIL_SERVER_USERNAME'
		,''
		,'1'
		,'Email szerver felhasználó.'
		); 
 END
 
SET @record_id = '3732A5C0-23F4-48F5-8282-85181A94B705'
if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - EMAIL_SERVER_PASSWORD' 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'EMAIL_SERVER_PASSWORD'
		,''
		,'1'
		,'Email szerver jelszó.'
		); 
 END 
 
SET @record_id = '757A7037-DDFB-4CC1-BC8D-B3CF1FA9CC3D'
if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - EMAIL_SERVER_USESSL' 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'EMAIL_SERVER_USESSL'
		,'0'
		,'1'
		,'Email küldésnél SSL használata.'
		); 
 END    

 GO

 --------------------------------------------------EMAIL_SERVER--------------------------------------------

 ------------------------------------------------   BLG_1312  -----------------------------------------------------------------
-- BLG_1312 Éves IratForgalmi Statisztika Menüpont
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='A14D18EA-CFE0-E811-80CF-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Funkciok(
			 Alkalmazas_Id
			,Modosithato
			,Id
			,Kod
			,Nev
			) values (
			'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,'0'
			,'A14D18EA-CFE0-E811-80CF-00155D020DD3'
			,'EvesIratForgalmiStatisztika'
			,'Éves Iratforgalmi Statisztika'
			);
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Funkciok
		 SET 
			Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,Modosithato='0'
			,Id='A14D18EA-CFE0-E811-80CF-00155D020DD3'
			,Kod='EvesIratForgalmiStatisztika'
			,Nev='Éves Iratforgalmi Statisztika'
		 WHERE Id=@record_id 
	 END
	 go

DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
if  (@isTUK = '1')
BEGIN
	DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
		where Id='AD91773E-D0E0-E811-80CF-00155D020DD3') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
		
		Print 'INSERT Fejlesztő szerepkör hozzárendelés' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'e855f681-36ed-41b6-8413-576fcb5d1542'
			,'AD91773E-D0E0-E811-80CF-00155D020DD3'
			,'A14D18EA-CFE0-E811-80CF-00155D020DD3'
			);
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Szerepkor_Funkcio
		 SET 
			 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
			,Id='AD91773E-D0E0-E811-80CF-00155D020DD3'
			,Funkcio_Id='A14D18EA-CFE0-E811-80CF-00155D020DD3'
		 WHERE Id=@record_id
	 END
END	 
	 go


DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Modulok
		where Id='745C5182-D0E0-E811-80CF-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
	 insert into KRT_Modulok(
		Id
		,Tipus
		,Kod
		,Nev
		) values (
		 '745C5182-D0E0-E811-80CF-00155D020DD3'
		,'F'
		,'EvesIratForgalmiStatisztikaPrintForm.aspx'
		,'Éves Iratforgalmi Statisztika'
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Modulok
	 SET 
		 Id='745C5182-D0E0-E811-80CF-00155D020DD3'
		,Tipus='F'
		,Kod='EvesIratForgalmiStatisztikaPrintForm.aspx'
		,Nev = 'Éves Iratforgalmi Statisztika'
	 WHERE Id=@record_id 
 END
 go

 
 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Menuk
		where Id='09AC4006-FAEB-E811-80CF-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
	 insert into KRT_Menuk(
		 Menu_Id_Szulo
		,Id
		,Kod
		,Nev
		,Sorrend
		,Funkcio_id
		,modul_id
		) values (
		 'EE9EA307-ED15-4FE6-B323-A12D2E06AC6D'
		,'09AC4006-FAEB-E811-80CF-00155D020DD3'
		,'eRecord'
		,'Statisztikák'
		,'39'
		,'A14D18EA-CFE0-E811-80CF-00155D020DD3'
		,'745C5182-D0E0-E811-80CF-00155D020DD3'
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Menuk
	 SET 
		 Menu_Id_Szulo='EE9EA307-ED15-4FE6-B323-A12D2E06AC6D'
		,Id='09AC4006-FAEB-E811-80CF-00155D020DD3'
		,Kod='eRecord'
		,Nev='Statisztikák'
		,Funkcio_Id='A14D18EA-CFE0-E811-80CF-00155D020DD3'
		,Modul_Id='745C5182-D0E0-E811-80CF-00155D020DD3'
		,Parameter=''
		,Sorrend='39'
	 WHERE Id=@record_id 
 END
 go

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Menuk
		where Id='40A0E237-D1E0-E811-80CF-00155D020DD3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
	 insert into KRT_Menuk(
		 Menu_Id_Szulo
		,Id
		,Kod
		,Nev
		,Funkcio_Id
		,Modul_Id
		,Parameter
		,Sorrend
		) values (
		 '09AC4006-FAEB-E811-80CF-00155D020DD3'
		,'40A0E237-D1E0-E811-80CF-00155D020DD3'
		,'eRecord'
		,'Éves Iratforgalmi Statisztika'
		,'A14D18EA-CFE0-E811-80CF-00155D020DD3'
		,'745C5182-D0E0-E811-80CF-00155D020DD3'
		,''
		,'01'
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Menuk
	 SET 
		 Menu_Id_Szulo='09AC4006-FAEB-E811-80CF-00155D020DD3'
		,Id='40A0E237-D1E0-E811-80CF-00155D020DD3'
		,Kod='eRecord'
		,Nev='Éves Iratforgalmi Statisztika'
		,Funkcio_Id='A14D18EA-CFE0-E811-80CF-00155D020DD3'
		,Modul_Id='745C5182-D0E0-E811-80CF-00155D020DD3'
		,Parameter=''
		,Sorrend='01'
	 WHERE Id=@record_id 
 END
 go
------------------------------------------------   BLG_1312 vége -----------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------
PRINT 'BLG_3208'

DECLARE @record_id_3208 UNIQUEIDENTIFIER
SET @record_id_3208 = 'EF5BED7F-A5A3-4F44-927D-BA978EF36F98'

IF NOT EXISTS(SELECT 1 FROM KRT_Parameterek WHERE ID =@record_id_3208) 
BEGIN 
	PRINT 'INSERT' 
	 INSERT INTO KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) VALUES (
		 '450b510a-7caa-46b0-83e3-18445c0c53a9'
		,@record_id_3208
		,'DEFAULT_TERTIVEVENY_KERESES'
		,'1'
		,'1'
		,'Tértivevény érkeztetés oldalon az alapértelmezett kereső értéke: 1 - vonalkód alapján, 2 - ragszám (postai azonosító) alapján'
	); 
END

DECLARE @org_kod_3208 nvarchar(100) 
SET @org_kod_3208 = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

--IF @org_kod_3208 = 'NMHH'
--	UPDATE KRT_Parameterek
--	SET Ertek = '2'
--	WHERE ID=@record_id_3208
--ELSE IF @org_kod_3208 = 'FPH'
--	UPDATE KRT_Parameterek
--	SET Ertek = '1'
--	WHERE ID=@record_id_3208
--ELSE IF @org_kod_3208 = 'BOPMH'
--	UPDATE KRT_Parameterek
--	SET Ertek = '2'
--	WHERE ID=@record_id_3208
--ELSE IF @org_kod_3208 = 'CSPH'
--	UPDATE KRT_Parameterek
--	SET Ertek = '2'
--	WHERE ID=@record_id_3208
--ELSE
--	UPDATE KRT_Parameterek
--	SET Ertek = '1'
--	WHERE ID=@record_id_3208
---------------------------------------------------------------------------------------------------------------
GO

------------------------------------------------   BUG_5102  -----------------------------------------------------------------

IF OBJECT_ID('dbo.[AK_OBJMETADEFINICIO]', 'UQ') IS NOT NULL 
	ALTER TABLE [dbo].[EREC_Obj_MetaDefinicio] DROP CONSTRAINT [AK_OBJMETADEFINICIO]
GO

IF OBJECT_ID('dbo.[CK_EREC_Obj_MetaDefinicio]', 'C') IS  NULL
BEGIN 
	ALTER TABLE [dbo].[EREC_Obj_MetaDefinicio]  WITH CHECK ADD  CONSTRAINT [CK_EREC_Obj_MetaDefinicio] CHECK  (((0)=[dbo].[fn_EREC_Obj_MetaDefinicioCheckUK]([Org],[ContentType],[Id],[ErvKezd],[ErvVege])))
	ALTER TABLE [dbo].[EREC_Obj_MetaDefinicio] CHECK CONSTRAINT [CK_EREC_Obj_MetaDefinicio]
END

GO


------------------------------------------------   BUG_5102 vége -----------------------------------------------------------------

------------------------------------------------   BUG_5926  -----------------------------------------------------------------
GO

DECLARE @record_id uniqueidentifier 

SET @record_id = '0A1FEC1B-3F1A-E911-80CC-00155D020DFE'
if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRX_META_FORRAS_KRID' 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'KRX_META_FORRAS_KRID'
		,''
		,'1'
		,'Milyen feladó esetén kerüljenek a KRX-ben lévő meta adatok betöltésre az érkeztetési információkhoz.'
		); 
 END
 
 GO

------------------------------------------------ UGYIRAT_ELINTEZES_FELTETEL ------------------------------------------------------

DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = '7E9F9FF8-4635-4A17-9883-9AC1D18BE0FF'

IF @org_kod = 'NMHH'
set @ertek = '1'
else
set @ertek = '0'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - UGYIRAT_ELINTEZES_FELTETEL - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'UGYIRAT_ELINTEZES_FELTETEL'
		,@ertek
		,'1'
		,'Ügyirat elintézésnél az ellenőrzések kikapcsolása.'
		); 
 END

 GO

 ------------------------------------------------ UGYIRAT_ELINTEZES_FELTETEL ------------------------------------------------------

-- BUG_6107
-- IRATTARI_HELY_KOTELEZO rendszerparameter	 

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='08CB13DD-7B1D-E911-80CC-00155D020DFE') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT IRATTARI_HELY_KOTELEZO' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'08CB13DD-7B1D-E911-80CC-00155D020DFE'
		,'IRATTARI_HELY_KOTELEZO'
		,'1'
		,'1'
		,'Irattárba vételkor kötelező-e kiválasztani az irattári helyet. 1-Kötelező, 0-Nem kötelező'
		); 
END 
ELSE 
BEGIN 	
	Print 'UPDATE IRATTARI_HELY_KOTELEZO'
	 --UPDATE KRT_Parameterek
	 --SET 
		--Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		--,Id='08CB13DD-7B1D-E911-80CC-00155D020DFE'
		--,Nev='IRATTARI_HELY_KOTELEZO'
		--,Ertek='1'
		--,Karbantarthato='1'
		--,Note='Irattárba vételkor kötelező-e kiválasztani az irattári helyet. 1-Kötelező, 0-Nem kötelező'
	 --WHERE Id='08CB13DD-7B1D-E911-80CC-00155D020DFE'
	
END
IF (@org_kod = 'FPH')  
BEGIN
	Print 'UPDATE IRATTARI_HELY_KOTELEZO: FPH = 0'
	 --UPDATE KRT_Parameterek
	 --SET 
		--Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		--,Id='08CB13DD-7B1D-E911-80CC-00155D020DFE'
		--,Nev='IRATTARI_HELY_KOTELEZO'
		--,Ertek='0'
		--,Karbantarthato='1'
		--,Note='Irattárba vételkor kötelező-e kiválasztani az irattári helyet. 1-Kötelező, 0-Nem kötelező'
	 --WHERE Id='08CB13DD-7B1D-E911-80CC-00155D020DFE'

END
GO
------------------------------------------------   BUG_5102  -----------------------------------------------------------------

--ALTER TABLE [dbo].[EREC_Obj_MetaDefinicio] DROP CONSTRAINT [AK_OBJMETADEFINICIO]
--GO

--ALTER TABLE [dbo].[EREC_Obj_MetaDefinicio]  WITH NOCHECK ADD  CONSTRAINT [CK_EREC_Obj_MetaDefinicio] CHECK  (((0)=[dbo].[fn_EREC_Obj_MetaDefinicioCheckUK]([Org],[ContentType],[Id],[ErvKezd],[ErvVege])))
--GO

--ALTER TABLE [dbo].[EREC_Obj_MetaDefinicio] CHECK CONSTRAINT [CK_EREC_Obj_MetaDefinicio]
--GO

IF NOT EXISTS (SELECT 1 FROM KRT_Menuk WHERE ID = 'cfcbd8a8-cc4b-48da-9f52-dddcc3355a08' )
INSERT [dbo].[KRT_Menuk] ([Id], [Org], [Menu_Id_Szulo], [Kod], [Nev], [Funkcio_Id], [Modul_Id], [Parameter], [Ver], [Note], [Stat_id], [ImageURL], [Sorrend], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) VALUES (N'cfcbd8a8-cc4b-48da-9f52-dddcc3355a08', NULL, N'aa1421b9-0498-4ca3-b075-60f699cb1b7c', N'eAdmin', N'Kódtár függőség kezelése', N'cfcbd8a8-cc4b-48da-9f52-dddcc3355a01', N'cfcbd8a8-cc4b-48da-9f52-dddcc3355a07', NULL, 1, NULL, NULL, NULL, N'81', CAST(N'2017-09-22T12:00:09.923' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2017-09-22T12:00:09.923' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO

IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = '351CCE21-3E33-4059-9FF6-B8223899E8CB' )
	INSERT INTO KRT_Szerepkor_Funkcio(
					 Id,
					 Szerepkor_Id
					,Funkcio_Id
					) values (
					 '351CCE21-3E33-4059-9FF6-B8223899E8CB'
					,'e855f681-36ed-41b6-8413-576fcb5d1542'
					,'cfcbd8a8-cc4b-48da-9f52-dddcc3355a01'
					); 

IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = '351CCE21-3E33-4059-9FF6-B8323899E8CB' )
	INSERT INTO KRT_Szerepkor_Funkcio(
					 Id,
					 Szerepkor_Id
					,Funkcio_Id
					) values (
					 '351CCE21-3E33-4059-9FF6-B8323899E8CB'
					,'e855f681-36ed-41b6-8413-576fcb5d1542'
					,'cfcbd8a8-cc4b-48da-9f52-dddcc3355a02'
					); 
IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = '351CCE21-3E33-4059-9FF6-B8423899E8CB' )
	INSERT INTO KRT_Szerepkor_Funkcio(
					 Id,
					 Szerepkor_Id
					,Funkcio_Id
					) values (
					 '351CCE21-3E33-4059-9FF6-B8423899E8CB'
					,'e855f681-36ed-41b6-8413-576fcb5d1542'
					,'cfcbd8a8-cc4b-48da-9f52-dddcc3355a03'
					); 
IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = '351CCE21-3E33-4059-9FF6-B8523899E8CB' )
	INSERT INTO KRT_Szerepkor_Funkcio(
					 Id,
					 Szerepkor_Id
					,Funkcio_Id
					) values (
					 '351CCE21-3E33-4059-9FF6-B8523899E8CB'
					,'e855f681-36ed-41b6-8413-576fcb5d1542'
					,'cfcbd8a8-cc4b-48da-9f52-dddcc3355a04'
					); 
IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = '351CCE21-3E33-4059-9FF6-B8623899E8CB' )
	INSERT INTO KRT_Szerepkor_Funkcio(
					 Id,
					 Szerepkor_Id
					,Funkcio_Id
					) values (
					 '351CCE21-3E33-4059-9FF6-B8623899E8CB'
					,'e855f681-36ed-41b6-8413-576fcb5d1542'
					,'cfcbd8a8-cc4b-48da-9f52-dddcc3355a05'
					); 
IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = '351CCE21-3E33-4059-9FF6-B8723899E8CB' )
	INSERT INTO KRT_Szerepkor_Funkcio(
					 Id,
					 Szerepkor_Id
					,Funkcio_Id
					) values (
					 '351CCE21-3E33-4059-9FF6-B8723899E8CB'
					,'e855f681-36ed-41b6-8413-576fcb5d1542'
					,'cfcbd8a8-cc4b-48da-9f52-dddcc3355a06'
					); 
IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = '351CCE21-3E33-4059-9FF6-B8823899E8CB' )
	INSERT INTO KRT_Szerepkor_Funkcio(
					 Id,
					 Szerepkor_Id
					,Funkcio_Id
					) values (
					 '351CCE21-3E33-4059-9FF6-B8823899E8CB'
					,'e855f681-36ed-41b6-8413-576fcb5d1542'
					,'cfcbd8a8-cc4b-48da-9f52-dddcc3355a09'
					); 

-- END: fuggo kodtar menu
-----------------------------------------------------------------------
-----------------------------------------------------------------------

-- BUG_5094 FeladatDefiníció History feltöltése (insertek)
declare @userId uniqueidentifier = '54E861A5-36ED-44CA-BAA7-C287D125B309' -- ADMIN
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'c954e8bc-b15a-e711-80c2-00155d020d7e' )
BEGIN
	print 'c954e8bc-b15a-e711-80c2-00155d020d7e'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) 
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2017-06-26 22:55:09.307' AS DateTime), N'c954e8bc-b15a-e711-80c2-00155d020d7e', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'f698e0da-4ce3-4424-83fa-52d4ad113ccb', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_KozpontiIrattarKolcsonzesErtesito', NULL, N'24379f12-35f4-dd11-83e0-005056c00008', NULL, N'0', NULL, NULL, N'1', NULL, 0, NULL, N'Központi irattárból kikérés történt!', NULL, N'31', NULL, NULL, N'a04417a6-54ac-4af1-9b63-5babf0203d42', N'EREC_UgyUgyiratok', NULL, 1, NULL, NULL, CAST(N'2017-06-26 22:55:09.307' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2017-06-26 22:55:09.307' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
-- IF NOT EXIST ( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'da106ce1-d43b-e711-80c0-00155d027e53' )
-- BEGIN
	-- INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) 
										 -- VALUES (0, NULL, CAST(N'2017-05-18 16:18:36.903' AS DateTime), N'da106ce1-d43b-e711-80c0-00155d027e53', N'590b2da1-44b2-e611-80bb-00155d020d34', N'f698e0da-4ce3-4424-83fa-52d4ad113ccb', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_KozpontiIrattarKolcsonzesErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'0', NULL, NULL, N'1', NULL, 0, NULL, N'Központi irattárból kikérés történt!', NULL, N'31', NULL, NULL, N'a04417a6-54ac-4af1-9b63-5babf0203d42', N'EREC_UgyUgyiratok', NULL, 1, NULL, NULL, CAST(N'2017-05-18 16:18:36.903' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2017-05-18 16:18:36.903' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
-- END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'db106ce1-d43b-e711-80c0-00155d027e53' )
BEGIN
	print 'db106ce1-d43b-e711-80c0-00155d027e53'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) 
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2017-05-18 16:18:36.903' AS DateTime), N'db106ce1-d43b-e711-80c0-00155d027e53', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'f698e0da-4ce3-4424-83fa-52d4ad113ccb', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_KozpontiIrattarKolcsonzesErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'0', NULL, NULL, N'1', NULL, 0, NULL, N'Központi irattárból kikérés történt!', NULL, N'31', NULL, NULL, N'a04417a6-54ac-4af1-9b63-5babf0203d42', N'EREC_UgyUgyiratok', NULL, 1, NULL, NULL, CAST(N'2017-05-18 16:18:36.903' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2017-05-18 16:18:36.903' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'b312bafd-5c8c-e311-8529-001ec9e754bc' )
BEGIN
	print 'b312bafd-5c8c-e311-8529-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) 
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2010-12-21 16:49:04.697' AS DateTime), N'b312bafd-5c8c-e311-8529-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'7dd6c002-7a9b-489a-8c1e-7012413c267a', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_BejovoIratIktatasErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'1', N'98ec194f-ac7e-4d6f-815e-579bee58476a', NULL, N'1', NULL, 0, NULL, N'A {0}-n érkezett {1} érkeztetőszámú küldemény a(z) {2} iktatószámra iktatásra/átiktatásra került!', NULL, N'31', NULL, NULL, N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'EREC_IraIratok', NULL, 1, NULL, NULL, CAST(N'2013-08-13 00:00:00.000' AS DateTime), CAST(N'4700-12-12 00:00:00.000' AS DateTime), NULL, CAST(N'2010-12-21 16:49:04.697' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'b412bafd-5c8c-e311-8529-001ec9e754bc' )
BEGIN
	print 'b412bafd-5c8c-e311-8529-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) 
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2010-12-21 16:49:04.697' AS DateTime), N'b412bafd-5c8c-e311-8529-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'771498fe-c512-4136-837a-beb0333077e3', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_BejovoIratIktatasErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'1', N'98ec194f-ac7e-4d6f-815e-579bee58476a', NULL, N'1', NULL, 0, NULL, N'A {0}-n érkezett {1} érkeztetőszámú küldemény a(z) {2} iktatószámra iktatásra/átiktatásra került!', NULL, N'31', NULL, NULL, N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'EREC_IraIratok', NULL, 1, NULL, NULL, CAST(N'2013-08-13 00:00:00.000' AS DateTime), CAST(N'4700-12-12 00:00:00.000' AS DateTime), NULL, CAST(N'2010-12-21 16:49:04.697' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'e68f14d2-608c-e311-8529-001ec9e754bc' )
BEGIN
	print 'e68f14d2-608c-e311-8529-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) 
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2010-12-21 16:49:04.697' AS DateTime), N'e68f14d2-608c-e311-8529-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'7dd6c002-7a9b-489a-8c1e-7012413c267a', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_BejovoIratIktatasErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'1', N'991fd9d0-29ee-e011-9c29-001ec9e754bc', NULL, N'1', NULL, 0, NULL, N'A {0}-n érkezett {1} érkeztetőszámú küldemény a(z) {2} iktatószámra iktatásra/átiktatásra került!', NULL, N'31', NULL, NULL, N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'EREC_IraIratok', NULL, 1, NULL, NULL, CAST(N'2014-02-02 23:54:38.317' AS DateTime), CAST(N'2014-07-18 13:00:38.220' AS DateTime), NULL, CAST(N'2010-12-21 16:49:04.697' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'e78f14d2-608c-e311-8529-001ec9e754bc' )
BEGIN
	print 'e78f14d2-608c-e311-8529-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) 
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2010-12-21 16:49:04.697' AS DateTime), N'e78f14d2-608c-e311-8529-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'771498fe-c512-4136-837a-beb0333077e3', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_BejovoIratIktatasErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'1', N'991fd9d0-29ee-e011-9c29-001ec9e754bc', NULL, N'1', NULL, 0, NULL, N'A {0}-n érkezett {1} érkeztetőszámú küldemény a(z) {2} iktatószámra iktatásra/átiktatásra került!', NULL, N'31', NULL, NULL, N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'EREC_IraIratok', NULL, 1, NULL, NULL, CAST(N'2014-02-02 23:54:38.317' AS DateTime), CAST(N'2014-07-18 13:00:38.220' AS DateTime), NULL, CAST(N'2010-12-21 16:49:04.697' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'06c7faac-6982-e211-859e-001ec9e754bc' )
BEGIN
	print '06c7faac-6982-e211-859e-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) 
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2010-12-21 16:49:04.697' AS DateTime), N'06c7faac-6982-e211-859e-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'7dd6c002-7a9b-489a-8c1e-7012413c267a', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_BejovoIratIktatasErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'1', N'82a402f0-8a52-479b-b7a3-1c91873862c3', NULL, N'1', NULL, 0, NULL, N'A {0}-n érkezett {1} érkeztetőszámú küldemény a(z) {2} iktatószámra iktatásra/átiktatásra került!', NULL, N'31', NULL, NULL, N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'EREC_IraIratok', NULL, 1, NULL, NULL, CAST(N'2010-12-21 16:49:04.697' AS DateTime), CAST(N'2013-08-12 00:00:00.000' AS DateTime), NULL, CAST(N'2010-12-21 16:49:04.697' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id= N'07c7faac-6982-e211-859e-001ec9e754bc' )
BEGIN
	print '07c7faac-6982-e211-859e-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2010-12-21 16:49:04.697' AS DateTime), N'07c7faac-6982-e211-859e-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'771498fe-c512-4136-837a-beb0333077e3', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_BejovoIratIktatasErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'1', N'82a402f0-8a52-479b-b7a3-1c91873862c3', NULL, N'1', NULL, 0, NULL, N'A {0}-n érkezett {1} érkeztetőszámú küldemény a(z) {2} iktatószámra iktatásra/átiktatásra került!', NULL, N'31', NULL, NULL, N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'EREC_IraIratok', NULL, 1, NULL, NULL, CAST(N'2010-12-21 16:49:04.697' AS DateTime), CAST(N'2013-08-12 00:00:00.000' AS DateTime), NULL, CAST(N'2010-12-21 16:49:04.697' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'3fce1a4a-e09d-e211-859e-001ec9e754bc' )
BEGIN
	print '3fce1a4a-e09d-e211-859e-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2010-12-21 16:49:04.697' AS DateTime), N'3fce1a4a-e09d-e211-859e-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'7dd6c002-7a9b-489a-8c1e-7012413c267a', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_BejovoIratIktatasErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'1', N'4efbd173-1dda-e111-992c-001ec9e754bc', NULL, N'1', NULL, 0, NULL, N'A {0}-n érkezett {1} érkeztetőszámú küldemény a(z) {2} iktatószámra iktatásra/átiktatásra került!', NULL, N'31', NULL, NULL, N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'EREC_IraIratok', NULL, 1, NULL, NULL, CAST(N'2013-07-26 00:00:00.000' AS DateTime), CAST(N'2013-08-12 00:00:00.000' AS DateTime), NULL, CAST(N'2010-12-21 16:49:04.697' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'40ce1a4a-e09d-e211-859e-001ec9e754bc' )
BEGIN
	print '40ce1a4a-e09d-e211-859e-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2010-12-21 16:49:04.697' AS DateTime), N'40ce1a4a-e09d-e211-859e-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'771498fe-c512-4136-837a-beb0333077e3', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_BejovoIratIktatasErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'1', N'4efbd173-1dda-e111-992c-001ec9e754bc', NULL, N'1', NULL, 0, NULL, N'A {0}-n érkezett {1} érkeztetőszámú küldemény a(z) {2} iktatószámra iktatásra/átiktatásra került!', NULL, N'31', NULL, NULL, N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'EREC_IraIratok', NULL, 1, NULL, NULL, CAST(N'2013-07-26 00:00:00.000' AS DateTime), CAST(N'2013-08-12 00:00:00.000' AS DateTime), NULL, CAST(N'2010-12-21 16:49:04.697' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'd19eb587-6a0e-e411-863e-001ec9e754bc' )
BEGIN
	print 'd19eb587-6a0e-e411-863e-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2010-12-21 16:49:04.697' AS DateTime), N'd19eb587-6a0e-e411-863e-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'7dd6c002-7a9b-489a-8c1e-7012413c267a', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_BejovoIratIktatasErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'1', N'4257ff33-5c0e-e411-863e-001ec9e754bc', NULL, N'1', NULL, 0, NULL, N'A {0}-n érkezett {1} érkeztetőszámú küldemény a(z) {2} iktatószámra iktatásra/átiktatásra került!', NULL, N'31', NULL, NULL, N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'EREC_IraIratok', NULL, 1, NULL, NULL, CAST(N'2014-02-02 23:54:38.317' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2010-12-21 16:49:04.697' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'd29eb587-6a0e-e411-863e-001ec9e754bc' )
BEGIN
	print 'd29eb587-6a0e-e411-863e-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2010-12-21 16:49:04.697' AS DateTime), N'd29eb587-6a0e-e411-863e-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'771498fe-c512-4136-837a-beb0333077e3', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_BejovoIratIktatasErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'1', N'4257ff33-5c0e-e411-863e-001ec9e754bc', NULL, N'1', NULL, 0, NULL, N'A {0}-n érkezett {1} érkeztetőszámú küldemény a(z) {2} iktatószámra iktatásra/átiktatásra került!', NULL, N'31', NULL, NULL, N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'EREC_IraIratok', NULL, 1, NULL, NULL, CAST(N'2014-02-02 23:54:38.317' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2010-12-21 16:49:04.697' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'64e1250c-8cf7-dd11-8945-001ec9e754bc' )
BEGIN
	print '64e1250c-8cf7-dd11-8945-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2009-02-10 17:01:17.210' AS DateTime), N'64e1250c-8cf7-dd11-8945-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'295cd885-4f77-43f0-85bb-7979482856a9', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_UgyiratSzignalasFeladat', N'bb821009-0995-4556-a079-0ef669c9b529', N'f15ea63d-63ba-4ef4-9e8a-68b295e91a52', NULL, N'2', N'9cb812a8-a4b3-4e7a-8512-54dda039e2bf', NULL, N'1', NULL, NULL, NULL, N'A szignált ügyirat átvétele.', N'F', N'21', N'21', N'0', N'a04417a6-54ac-4af1-9b63-5babf0203d42', N'EREC_UgyUgyiratok', NULL, 1, NULL, NULL, CAST(N'2009-02-10 17:01:17.210' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2009-02-10 17:01:17.210' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'6be1250c-8cf7-dd11-8945-001ec9e754bc' )
BEGIN
	print '6be1250c-8cf7-dd11-8945-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2009-02-10 17:01:17.240' AS DateTime), N'6be1250c-8cf7-dd11-8945-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'2a30d833-c0f2-dd11-80ec-005056c00008', NULL, NULL, N'5', 1, N'sp_WF_FeladatKezelo', N'sp_WF_AtvetelEllenorzes', N'bb821009-0995-4556-a079-0ef669c9b529', N'65e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, NULL, NULL, NULL, N'1', N'f05f7b9d-0380-43f6-8acb-ddf759b7ccbd', -1, N'1440', N'Több, mint 24 órája átadott anyagok átvétele még nem történt meg!', NULL, N'21', N'21', N'0', N'97b83e22-a0d9-4369-8830-ebaf7329ec84', N'EREC_IraKezbesitesiTetelek', NULL, 1, NULL, NULL, CAST(N'2009-02-10 17:01:17.240' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2009-02-10 17:01:17.240' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'6ce1250c-8cf7-dd11-8945-001ec9e754bc' )
BEGIN
	print '6ce1250c-8cf7-dd11-8945-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2009-02-10 17:01:17.257' AS DateTime), N'6ce1250c-8cf7-dd11-8945-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'2b30d833-c0f2-dd11-80ec-005056c00008', NULL, NULL, N'5', 1, N'sp_WF_FeladatKezelo', N'sp_WF_KolcsonzesEllenorzes', N'846c3a3b-2832-4a4b-b1c9-b68270a38fed', N'65e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, NULL, NULL, NULL, N'1', N'ee65235e-3e8d-488f-bbca-77228b70c489', 12, N'60', N'Az ügyirat kölcsönzési határideje 12 órán belül lejár!', NULL, N'21', N'21', N'0', N'bcd5f3d7-9c81-4008-b9e8-e73f4bc63826', N'EREC_IrattariKikero', NULL, 1, NULL, NULL, CAST(N'2009-02-10 17:01:17.257' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2009-02-10 17:01:17.257' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'6de1250c-8cf7-dd11-8945-001ec9e754bc' )
BEGIN
	print '6de1250c-8cf7-dd11-8945-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2009-02-10 17:01:17.257' AS DateTime), N'6de1250c-8cf7-dd11-8945-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'2c30d833-c0f2-dd11-80ec-005056c00008', NULL, NULL, N'5', 1, N'sp_WF_FeladatKezelo', N'sp_WF_KolcsonzesEllenorzes', N'846c3a3b-2832-4a4b-b1c9-b68270a38fed', N'65e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, NULL, NULL, NULL, N'1', N'ee65235e-3e8d-488f-bbca-77228b70c489', 0, N'1', N'Az ügyirat kölcsönzési határideje lejárt!', NULL, N'21', N'21', N'0', N'bcd5f3d7-9c81-4008-b9e8-e73f4bc63826', N'EREC_IrattariKikero', NULL, 1, NULL, NULL, CAST(N'2009-02-10 17:01:17.257' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2009-02-10 17:01:17.257' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'6fe1250c-8cf7-dd11-8945-001ec9e754bc' )
BEGIN
	print '6fe1250c-8cf7-dd11-8945-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2009-02-10 17:01:17.273' AS DateTime), N'6ee1250c-8cf7-dd11-8945-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'2d30d833-c0f2-dd11-80ec-005056c00008', NULL, NULL, N'5', 1, N'sp_WF_FeladatKezelo', N'sp_WF_UgyiratHataridoEllenorzes', N'2574e0e2-373f-4bce-a9f1-e84128ec08b4', N'65e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, NULL, NULL, NULL, N'1', N'bf1ea56a-d172-4651-a20f-23947a3f15f6', 12, N'60', N'Az ügyirat intézkedési határideje 12 órán belül lejár!', NULL, N'21', N'21', N'0', N'a04417a6-54ac-4af1-9b63-5babf0203d42', N'EREC_UgyUgyiratok', NULL, 1, NULL, NULL, CAST(N'2009-02-10 17:01:17.273' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2009-02-10 17:01:17.273' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'6fe1250c-8cf7-dd11-8945-001ec9e754bc' )
BEGIN
	print '6fe1250c-8cf7-dd11-8945-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2009-02-10 17:01:17.287' AS DateTime), N'6fe1250c-8cf7-dd11-8945-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'2e30d833-c0f2-dd11-80ec-005056c00008', NULL, NULL, N'5', 1, N'sp_WF_FeladatKezelo', N'sp_WF_UgyiratHataridoEllenorzes', N'2574e0e2-373f-4bce-a9f1-e84128ec08b4', N'65e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, NULL, NULL, NULL, N'1', N'bf1ea56a-d172-4651-a20f-23947a3f15f6', 0, N'1', N'Az ügyirat intézkedési határideje lejárt!', NULL, N'21', N'21', N'0', N'a04417a6-54ac-4af1-9b63-5babf0203d42', N'EREC_UgyUgyiratok', NULL, 1, NULL, NULL, CAST(N'2009-02-10 17:01:17.287' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2009-02-10 17:01:17.287' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'71e1250c-8cf7-dd11-8945-001ec9e754bc' )
BEGIN
	print '71e1250c-8cf7-dd11-8945-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2009-02-10 17:01:21.553' AS DateTime), N'71e1250c-8cf7-dd11-8945-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'4719eb59-fae0-4f1e-99a0-0cb4adbee8f2', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_MunkaanyagIktatasErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'2', N'400bd2cf-a3b4-4ebe-91e4-c9fa20be8260', NULL, N'1', NULL, 0, NULL, N'A munkaanyag iktatása megtörtént!', NULL, N'31', N'31', NULL, N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'EREC_IraIratok', NULL, 1, NULL, NULL, CAST(N'2009-02-10 17:01:21.553' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2009-02-10 17:01:21.553' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'72e1250c-8cf7-dd11-8945-001ec9e754bc' )
BEGIN
	print '72e1250c-8cf7-dd11-8945-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2009-02-10 17:01:21.553' AS DateTime), N'72e1250c-8cf7-dd11-8945-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'214b11b0-86c1-4f34-a56a-08f79fa9de16', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_MunkaanyagIktatasErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'2', N'400bd2cf-a3b4-4ebe-91e4-c9fa20be8260', NULL, N'1', NULL, 0, NULL, N'A munkaanyag iktatása megtörtént!', NULL, N'31', N'31', NULL, N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'EREC_IraIratok', NULL, 1, NULL, NULL, CAST(N'2009-02-10 17:01:21.553' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2009-02-10 17:01:21.553' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'4de882b0-053f-de11-aa3f-001ec9e754bc' )
BEGIN
	print '4de882b0-053f-de11-aa3f-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2009-05-12 17:00:54.660' AS DateTime), N'4de882b0-053f-de11-aa3f-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'4cc234f1-0f25-de11-a70d-005056c00008', NULL, NULL, N'5', 1, N'sp_WF_FeladatKezelo', NULL, N'f698e0da-4ce3-4424-83fa-52d4ad113ccb', N'65e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, NULL, NULL, NULL, N'1', N'01ecbcff-3a8b-4d95-a46a-08864d52b13e', -1, N'1440', N'Több, mint 24 órája igényelt irattári kölcsönzés jóváhagyása még nem történt meg!', NULL, N'1', NULL, N'0', N'bcd5f3d7-9c81-4008-b9e8-e73f4bc63826', N'EREC_IrattariKikero', NULL, 1, NULL, NULL, CAST(N'2009-05-12 17:00:54.660' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2009-05-12 17:00:54.660' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'4ee882b0-053f-de11-aa3f-001ec9e754bc' )
BEGIN
	print '4ee882b0-053f-de11-aa3f-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2009-05-12 17:00:54.660' AS DateTime), N'4ee882b0-053f-de11-aa3f-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'4dc234f1-0f25-de11-a70d-005056c00008', NULL, NULL, N'5', 1, N'sp_WF_FeladatKezelo', NULL, N'd8f5dbf2-3d09-448e-8110-21f0e677d03b', N'65e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, NULL, NULL, NULL, N'1', N'94872e7e-0dbb-4a25-bda3-90ab8b5deaa8', -1, N'1440', N'Több, mint 24 órája igényelt irattározás jóváhagyása még nem történt meg!', NULL, N'1', NULL, N'0', N'a04417a6-54ac-4af1-9b63-5babf0203d42', N'EREC_UgyUgyiratok', NULL, 1, NULL, NULL, CAST(N'2009-05-12 17:00:54.660' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2009-05-12 17:00:54.660' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'4fe882b0-053f-de11-aa3f-001ec9e754bc' )
BEGIN
	print '4fe882b0-053f-de11-aa3f-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2009-05-12 17:00:54.660' AS DateTime), N'4fe882b0-053f-de11-aa3f-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'4ec234f1-0f25-de11-a70d-005056c00008', NULL, NULL, N'5', 1, N'sp_WF_FeladatKezelo', NULL, N'0df80031-ae13-4867-b42d-0fc4de6b52c8', N'65e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, NULL, NULL, NULL, N'1', N'68eefe34-129b-4c4b-ac50-c249f51373e9', -1, N'1440', N'Több, mint 24 órája igényelt skontro jóváhagyása még nem történt meg!', NULL, N'1', NULL, N'0', N'a04417a6-54ac-4af1-9b63-5babf0203d42', N'EREC_UgyUgyiratok', NULL, 1, NULL, NULL, CAST(N'2009-05-12 17:00:54.660' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2009-05-12 17:00:54.660' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'50e882b0-053f-de11-aa3f-001ec9e754bc' )
BEGIN
	print '50e882b0-053f-de11-aa3f-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2009-05-12 17:00:54.673' AS DateTime), N'50e882b0-053f-de11-aa3f-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'4fc234f1-0f25-de11-a70d-005056c00008', NULL, NULL, N'5', 1, N'sp_WF_FeladatKezelo', NULL, N'5b9aef98-8158-476a-9d81-3a0ede669409', N'65e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, NULL, NULL, NULL, N'1', N'b4ab32c9-b6af-444a-919c-b0008f245d2b', -1, N'1440', N'Több, mint 24 órája beérkezett küldemény kezelése még nem történt meg!', NULL, N'1', NULL, N'0', N'265e5281-ef84-4ab2-bd54-5847cc4da106', N'EREC_KuldKuldemenyek', NULL, 1, NULL, NULL, CAST(N'2009-05-12 17:00:54.673' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2009-05-12 17:00:54.673' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'51e882b0-053f-de11-aa3f-001ec9e754bc' )
BEGIN
	print '51e882b0-053f-de11-aa3f-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2009-05-12 17:00:54.673' AS DateTime), N'51e882b0-053f-de11-aa3f-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'50c234f1-0f25-de11-a70d-005056c00008', NULL, NULL, N'5', 1, N'sp_WF_FeladatKezelo', NULL, N'8f55c9c7-eb7b-4652-bb81-5cf8365ebe1e', N'65e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, NULL, NULL, NULL, N'1', N'9b7edb8c-b72b-4f61-b8e4-f5821f4d9bb9', 0, N'1', N'Az iratpéldánnyal kapcsolatos intézkedési határidő lejárt!', NULL, N'1', NULL, N'0', N'b29081b9-3106-4df8-ab1f-d2008dced7aa', N'EREC_PldIratPeldanyok', NULL, 1, NULL, NULL, CAST(N'2009-05-12 17:00:54.673' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2009-05-12 17:00:54.673' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'78670d7e-0fcb-df11-b018-001ec9e754bc' )
BEGIN
	print '78670d7e-0fcb-df11-b018-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2010-09-28 16:48:45.413' AS DateTime), N'78670d7e-0fcb-df11-b018-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'dae2bdab-9e59-4cbf-8af8-c528f7354406', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_AtmenetiIrattarKolcsonzesErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'0', NULL, NULL, N'1', NULL, 0, NULL, N'Átmeneti irattárból kikérés történt!', NULL, N'31', NULL, NULL, N'a04417a6-54ac-4af1-9b63-5babf0203d42', N'EREC_UgyUgyiratok', NULL, 1, NULL, NULL, CAST(N'2010-09-28 16:48:45.413' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2010-09-28 16:48:45.413' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'0db43819-1713-de11-bd91-001ec9e754bc' )
BEGIN
	print '0db43819-1713-de11-bd91-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2009-03-17 18:14:42.660' AS DateTime), N'0db43819-1713-de11-bd91-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'644de649-a90f-de11-b663-0015af225a5a', NULL, NULL, N'5', 1, N'sp_WF_FeladatKezelo', N'sp_WF_SkontroHataridoEllenorzes', N'2574e0e2-373f-4bce-a9f1-e84128ec08b4', N'65e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, NULL, NULL, NULL, N'1', N'20cd00d5-9d7e-44c4-b9c2-c31c199a5946', 0, N'1', N'Az ügyirat skontró határideje lejárt!', NULL, N'31', N'31', N'0', N'a04417a6-54ac-4af1-9b63-5babf0203d42', N'EREC_UgyUgyiratok', NULL, 1, NULL, NULL, CAST(N'2009-03-17 18:14:42.660' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2009-03-17 18:14:42.660' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'a31e3cce-8d18-de11-bd91-001ec9e754bc' )
BEGIN
	print 'a31e3cce-8d18-de11-bd91-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2010-10-06 16:01:12.000' AS DateTime), N'a31e3cce-8d18-de11-bd91-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'5a2db9e2-5115-de11-b663-0015af225a5a', NULL, NULL, N'5', 1, N'sp_WF_FeladatKezelo', N'sp_WF_SzerelesHataridoEllenorzes', N'2574e0e2-373f-4bce-a9f1-e84128ec08b4', N'65e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, NULL, NULL, NULL, N'1', N'94872e7e-0dbb-4a25-bda3-90ab8b5deaa8', -2, N'1440', N'Az ügyirat előzetes szerelésének véglegesítési határideje lejárt!', NULL, N'31', NULL, N'0', N'a04417a6-54ac-4af1-9b63-5babf0203d42', N'EREC_UgyUgyiratok', NULL, 2, NULL, NULL, CAST(N'2009-03-24 00:00:00.000' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2009-03-24 17:07:03.360' AS DateTime), N'7f84d7af-e973-4d92-be62-564592a6e951', CAST(N'2010-10-06 16:01:12.000' AS DateTime), NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'28d802d6-190d-e011-bdc5-001ec9e754bc' )
BEGIN
	print '28d802d6-190d-e011-bdc5-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2010-12-21 16:49:04.697' AS DateTime), N'28d802d6-190d-e011-bdc5-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'7dd6c002-7a9b-489a-8c1e-7012413c267a', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_BejovoIratIktatasErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'1', N'95e412d0-228b-433a-9e7e-0a336ffa6a56', NULL, N'1', NULL, 0, NULL, N'A {0}-n érkezett {1} érkeztetőszámú küldemény a(z) {2} iktatószámra iktatásra/átiktatásra került!', NULL, N'31', NULL, NULL, N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'EREC_IraIratok', NULL, 1, NULL, NULL, CAST(N'2013-08-12 00:00:00.000' AS DateTime), CAST(N'2014-01-12 00:00:00.000' AS DateTime), NULL, CAST(N'2010-12-21 16:49:04.697' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'29d802d6-190d-e011-bdc5-001ec9e754bc' )
BEGIN
	print '29d802d6-190d-e011-bdc5-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2010-12-21 16:49:04.697' AS DateTime), N'29d802d6-190d-e011-bdc5-001ec9e754bc', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'771498fe-c512-4136-837a-beb0333077e3', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_BejovoIratIktatasErtesito', NULL, N'70e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, N'1', N'95e412d0-228b-433a-9e7e-0a336ffa6a56', NULL, N'1', NULL, 0, NULL, N'A {0}-n érkezett {1} érkeztetőszámú küldemény a(z) {2} iktatószámra iktatásra/átiktatásra került!', NULL, N'31', NULL, NULL, N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'EREC_IraIratok', NULL, 1, NULL, NULL, CAST(N'2013-08-12 00:00:00.000' AS DateTime), CAST(N'2014-01-12 00:00:00.000' AS DateTime), NULL, CAST(N'2010-12-21 16:49:04.697' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'2ad802d6-190d-e011-bdc5-001ec9e754bc' )
BEGIN
	print '2ad802d6-190d-e011-bdc5-001ec9e754bc'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2010-12-21 16:49:05.337' AS DateTime), N'2ad802d6-190d-e011-bdc5-001ec9e754bc', N'00000000-0000-0000-0000-000000000000', N'64dd5019-ed80-4a6c-95d6-9e4000de6f1a', NULL, NULL, N'5', 1, N'sp_WF_FeladatKezelo', N'sp_WF_HelyettesitesEllenorzes', N'797f5c8a-59ad-4ae0-b37f-a68f2b9e1739', N'65e1250c-8cf7-dd11-8945-001ec9e754bc', NULL, NULL, NULL, NULL, N'1', N'f25ea95c-d49f-45dd-b105-9e4000e41564', 24, N'60', N'Önnek {0} darab helyettesítése {1} órán belül le fog járni! Ha szeretné meghosszabítani akkor tegye meg!', NULL, N'31', NULL, N'0', N'e036636e-cce2-4d6d-ab93-e0d21301ebee', N'KRT_Helyettesitesek', NULL, 1, NULL, NULL, CAST(N'2010-12-21 16:49:05.337' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2010-12-21 16:49:05.337' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END
IF NOT EXISTS( SELECT TOP 1 id FROM [dbo].[EREC_FeladatDefinicioHistory] WHERE [HistoryMuvelet_Id]=0 AND Id=N'2a20c22f-96ba-440c-b54a-ecdae4d11152' )
BEGIN
	print '2a20c22f-96ba-440c-b54a-ecdae4d11152'
	INSERT [dbo].[EREC_FeladatDefinicioHistory] ([HistoryMuvelet_Id], [HistoryVegrehajto_Id],[HistoryVegrehajtasIdo], [Id], [Org], [Funkcio_Id_Kivalto], [Obj_Metadefinicio_Id], [ObjStateValue_Id], [FeladatDefinicioTipus], [FeladatSorszam], [MuveletDefinicio], [SpecMuveletDefinicio], [Funkcio_Id_Inditando], [Funkcio_Id_Futtatando], [BazisObjLeiro], [FelelosTipus], [Obj_Id_Felelos], [FelelosLeiro], [Idobazis], [ObjTip_Id_DateCol], [AtfutasiIdo], [Idoegyseg], [FeladatLeiras], [Tipus], [Prioritas], [LezarasPrioritas], [Allapot], [ObjTip_Id], [Obj_type], [Obj_SzukitoFeltetel], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id])  
										 VALUES (0, '54E861A5-36ED-44CA-BAA7-C287D125B309', CAST(N'2017-03-30 07:23:42.707' AS DateTime), N'2a20c22f-96ba-440c-b54a-ecdae4d11152', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'6231af81-a504-4286-9e86-0ac41a7be973', NULL, NULL, N'1', 1, N'sp_WF_FeladatKezelo', N'sp_WF_IratAlairokFeladatUgyintezonek', N'e118d25c-95b5-4bac-b103-0c7c35326c98', N'f15ea63d-63ba-4ef4-9e8a-68b295e91a52', NULL, N'2', NULL, NULL, N'1', NULL, 0, NULL, N'A(z) {0} iktatószámu irat aláírási folyamatában változás állt be, {1} {2} az aláírást.', N'F', N'31', N'31', N'0', N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'EREC_IraIratok', N'NULL', 1, NULL, NULL, CAST(N'2017-03-30 07:23:42.707' AS DateTime), CAST(N'4700-12-31 00:00:00.000' AS DateTime), NULL, CAST(N'2017-03-30 07:23:42.707' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
END


------------------------------------------------   BUG_6107 vége -----------------------------------------------------------------
------------------------------------------------   BUG_5102 vége -----------------------------------------------------------------





------------------------------------------------------------------------------------------------------------------------------------------------
GO
PRINT 'START BLG 3530'

PRINT '-- KODTAR --'

DECLARE @Kodtar_Id_HS UNIQUEIDENTIFIER
SET @Kodtar_Id_HS =  '8B108922-4312-8385-3934-63FBCB4B8A03'

DECLARE @Kodtar_Nev_HS NVARCHAR(64)
SET @Kodtar_Nev_HS = 'HA'

PRINT '-- KODTAR:UgyFajta.Államigazgatási --'
IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE KodCsoport_Id ='72554FDB-8323-4110-B7B2-65576AE3204A' AND Nev = 'Államigazgatási hatósági ügy' )
BEGIN
	INSERT INTO [dbo].[KRT_KodTarak] ([Id] ,[Org] ,[KodCsoport_Id] ,[Kod] ,[Nev] ,[RovidNev] ,[Egyeb] ,[Modosithato] ,[Sorrend] ,[Note]) 
	VALUES ('1B54791A-CA5B-4D04-BB8A-3DB27B3385EC' ,'450B510A-7CAA-46B0-83E3-18445C0C53A9' ,'72554FDB-8323-4110-B7B2-65576AE3204A' ,'A' ,'Államigazgatási hatósági ügy' ,'A' ,'HATOSAGI' ,'0' ,'20' ,'' )
END
PRINT '-- KODTAR:UgyFajta.Önkormányzati --'
IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE KodCsoport_Id ='72554FDB-8323-4110-B7B2-65576AE3204A' AND Nev = 'Önkormányzati hatósági ügy' )
BEGIN
	INSERT INTO [dbo].[KRT_KodTarak] ([Id] ,[Org] ,[KodCsoport_Id] ,[Kod] ,[Nev] ,[RovidNev] ,[Egyeb] ,[Modosithato] ,[Sorrend] ,[Note]) 
	VALUES ('79E6FB17-5A1B-45A8-9346-CE68B4910252' ,'450B510A-7CAA-46B0-83E3-18445C0C53A10' ,'72554FDB-8323-4110-B7B2-65576AE3204A' ,'O' ,'Önkormányzati hatósági ügy' ,'O' ,'HATOSAGI' ,'0' ,'21' ,'' ) 
END


PRINT '-- KODTAR:Hatósági adatok --'
IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = @Kodtar_Id_HS) BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id] ,[Org] ,[KodCsoport_Id] ,[Kod] ,[Nev] ,[RovidNev] ,[Egyeb] ,[Modosithato] ,[Sorrend] ,[Note]) VALUES (@Kodtar_Id_HS ,'450B510A-7CAA-46B0-83E3-18445C0C53A9' ,'8CACBC1F-A987-405F-B2AA-FF09F9CF603E' ,@Kodtar_Nev_HS ,'Hatósági adatok' ,'HA' ,'' ,'0' ,'33' ,'' ) END ELSE UPDATE KRT_KodTarak SET Kod=@Kodtar_Nev_HS ,Nev='Hatósági adatok' ,Egyeb = '' ,Modosithato= '0' WHERE Id=@Kodtar_Id_HS 

IF NOT EXISTS (SELECT 1 FROM KRT_ObjTipusok WHERE ID ='192b5685-c763-4239-98bc-d298b33eca2e')
	INSERT [dbo].[KRT_ObjTipusok] ([Id], [ObjTipus_Id_Tipus], [Kod], [Nev], [Obj_Id_Szulo], [KodCsoport_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) VALUES (N'192b5685-c763-4239-98bc-d298b33eca2e', N'76b08484-4b5b-4648-a95f-ee3e40d5defe', N'Ugy_Fajtaja', N'Ugy_Fajtaja oszlop', N'aa5e7bba-96a0-4c17-8709-06a6d297e107', NULL, 1, NULL, NULL, CAST(N'2019-01-27T20:18:09.840' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T20:18:09.840' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO


PRINT '-- EREC_OBJ_METADEFINICIO --'

DECLARE @KODTAR_UGYFAJTA_ONKORM NVARCHAR(64)
SELECT TOP 1 @KODTAR_UGYFAJTA_ONKORM = KOD FROM KRT_KodTarak WHERE KodCsoport_Id ='72554FDB-8323-4110-B7B2-65576AE3204A' AND Nev = 'Önkormányzati hatósági ügy' 
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaDefinicio WHERE ID ='4a05253e-6922-e911-80c9-00155d020b39')
	INSERT [dbo].[EREC_Obj_MetaDefinicio] ([Id], [Org], [Objtip_Id], [Objtip_Id_Column], [ColumnValue], [DefinicioTipus], [Felettes_Obj_Meta], [MetaXSD], [ImportXML], [EgyebXML], [ContentType], [SPSSzinkronizalt], [SPS_CTT_Id], [WorkFlowVezerles], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) VALUES (N'4a05253e-6922-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'192b5685-c763-4239-98bc-d298b33eca2e', @KODTAR_UGYFAJTA_ONKORM, N'HA', NULL, NULL, NULL, NULL, N'Onkormanyzati_hatosagi_ugy', N'0', NULL, N'0', 1, NULL, NULL, CAST(N'2019-01-27T20:25:02.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T20:25:03.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO

DECLARE @KODTAR_UGYFAJTA_ALLAMIG NVARCHAR(64)
SELECT TOP 1 @KODTAR_UGYFAJTA_ALLAMIG = KOD FROM KRT_KodTarak WHERE KodCsoport_Id ='72554FDB-8323-4110-B7B2-65576AE3204A' AND Nev = 'Államigazgatási hatósági ügy'
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaDefinicio WHERE ID ='b7de48c2-7022-e911-80c9-00155d020b39')
	INSERT [dbo].[EREC_Obj_MetaDefinicio] ([Id], [Org], [Objtip_Id], [Objtip_Id_Column], [ColumnValue], [DefinicioTipus], [Felettes_Obj_Meta], [MetaXSD], [ImportXML], [EgyebXML], [ContentType], [SPSSzinkronizalt], [SPS_CTT_Id], [WorkFlowVezerles], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id]) VALUES (N'b7de48c2-7022-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'aa5e7bba-96a0-4c17-8709-06a6d297e107', N'192b5685-c763-4239-98bc-d298b33eca2e', @KODTAR_UGYFAJTA_ALLAMIG, N'HA', NULL, NULL, NULL, NULL, N'Allamigazgatasi_hatosagi_ugy', N'0', NULL, N'0', 1, NULL, NULL, CAST(N'2019-01-27T21:18:51.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T21:18:51.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO



PRINT '-- EREC_TARGYSZAVAK --'
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='d3db2868-6922-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'd3db2868-6922-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Döntést hozta', NULL, N'Dontest_hozta', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'DONTEST_HOZTA', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T20:26:13.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='295de9b7-6922-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'295de9b7-6922-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Döntés formája önkormányzati', NULL, N'Dontes_formaja_onkormanyzati', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'DONTES_FORMAJA_ONK', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T20:28:27.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='cfbdb7da-6922-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'cfbdb7da-6922-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Ügyintézés időtartama', NULL, N'Ugyintezes_idotartama', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'UGYINTEZES_IDOTARTAMA', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T20:29:25.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='89fc37fe-6922-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'89fc37fe-6922-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Határidő túllépés napokban', NULL, N'Hatarido_tullepes_napokban', N'0', NULL, NULL, NULL, NULL, N'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls', NULL, NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T20:30:25.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='6c4eb8fd-6a22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'6c4eb8fd-6a22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Jogorvoslati eljárás típusa', NULL, N'Jogorvoslati_eljaras_tipusa', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'JOGORVOSLATI_ELJARAS_TIPUSA', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T20:37:34.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='38d88f24-6b22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'38d88f24-6b22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Jogorv_eljárásban_döntés_típusa', NULL, N'Jogorv_eljarasban_dontes_tipusa', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'JOGORVOSLATI_DONTES_TIPUSA', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T20:38:39.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='365911a6-6b22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'365911a6-6b22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Jogorv_eljárásban_döntést_hozta', NULL, N'Jogorv_eljarasban_dontest_hozta', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'JOGORVOSLATI_DONTEST_HOZTA', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T20:42:16.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='d034dbd1-6b22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'd034dbd1-6b22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Jogorv_eljárásban_döntés_tartalma', NULL, N'Jogorv_eljarasban_dontes_tartalma', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'JOGORVOSLATI_DONTES_TARTALMA', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T20:43:30.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='7e22d83e-6c22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'7e22d83e-6c22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Jogorv_eljárásban_döntés_változása', NULL, N'Jogorv_eljarasban_dontes_valtozasa', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'JOGORVOSLATI_DONTES', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T20:46:32.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='093372a9-6d22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'093372a9-6d22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Hatósági_ellenőrzés_történt', NULL, N'Hatosagi_ellenorzes_tortent', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'IGEN_NEM', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T20:56:41.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='127cb717-6e22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'127cb717-6e22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Döntés munkaórák száma', NULL, N'Dontes_munkaorak_szama', N'0', NULL, NULL, NULL, NULL, N'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls', NULL, NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T20:59:46.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='c7d8a03f-6e22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'c7d8a03f-6e22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Megállapított eljárási költség', NULL, N'Megallapitott_eljarasi_koltseg', N'0', NULL, NULL, NULL, NULL, N'~/Component/RequiredNumberBox.ascx', NULL, NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T21:00:53.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='3fba6e62-6e22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'3fba6e62-6e22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Kiszabott közigazgatási bírság', NULL, N'Kiszabott_kozigazgatasi_birsag', N'0', NULL, NULL, NULL, NULL, N'~/Component/RequiredNumberBox.ascx', NULL, NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T21:01:51.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='04a5a397-6e22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'04a5a397-6e22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Sommás eljárásban hozott döntés', NULL, N'Sommas_eljarasban_hozott_dontes', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'IGEN_NEM', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T21:03:20.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='f5afd3be-6e22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'f5afd3be-6e22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'8napon belül lezárt nem sommás', NULL, N'8napon_belul_lezart_nem_sommas', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'IGEN_NEM', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T21:04:26.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='75fb85dd-6e22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'75fb85dd-6e22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Függő hatályú határozat', NULL, N'Fuggo_hatalyu_hatarozat', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'IGEN_NEM', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T21:05:18.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='63da410a-6f22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'63da410a-6f22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Határozat hatályba lépése', NULL, N'Hatarozat_hatalyba_lepese', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'HATAROZAT_HATALYBA_LEPESE', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T21:06:33.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='f642a92a-6f22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'f642a92a-6f22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Függő hatályú végzés', NULL, N'Fuggo_hatalyu_vegzes', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'IGEN_NEM', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T21:07:27.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='84893b81-6f22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'84893b81-6f22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Végzés hatályba lépése', NULL, N'Vegzes_hatalyba_lepese', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'VEGZES_HATALYBA_LEPESE', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T21:09:52.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='1b4210ab-6f22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'1b4210ab-6f22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Hatóság által visszafizetett összeg', NULL, N'Hatosag_altal_visszafizetett_osszeg', N'0', NULL, NULL, NULL, NULL, N'~/Component/RequiredNumberBox.ascx', NULL, NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T21:11:02.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='887e0bd3-6f22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'887e0bd3-6f22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Hatóságot terhelő eljárási költség', NULL, N'Hatosagot_terhelo_eljarasi_koltseg', N'0', NULL, NULL, NULL, NULL, N'~/Component/RequiredNumberBox.ascx', NULL, NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T21:12:10.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='d95e10f6-6f22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'd95e10f6-6f22-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Felfüggesztett határozat', NULL, N'Felfuggesztett_hatarozat', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'IGEN_NEM', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T21:13:08.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE ID ='69f8042f-7122-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_TargySzavak] ([Id], [Org], [Tipus], [TargySzavak], [AlapertelmezettErtek], [BelsoAzonosito], [SPSSzinkronizalt], [SPS_Field_Id], [Csoport_Id_Tulaj], [RegExp], [ToolTip], [ControlTypeSource], [ControlTypeDataSource], [XSD], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_id], [LetrehozasIdo], [Modosito_id], [ModositasIdo], [Zarolo_id], [ZarolasIdo], [Tranz_id], [UIAccessLog_id], [Targyszo_Id_Parent]) VALUES (N'69f8042f-7122-e911-80c9-00155d020b39', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'1', N'Döntés formája államigazgatási', NULL, N'Dontes_formaja_allamigazgatasi', N'0', NULL, NULL, NULL, NULL, N'~/Component/KodTarakDropDownList.ascx', N'DONTES_FORMAJA_ALLAMIG', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), N'54e861a5-36ed-44ca-baa7-c287d125b309', CAST(N'2019-01-27T21:21:53.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
-----------------------------


PRINT '-- EREC_OBJ_METAADATAI --'
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='15ed3b7c-6922-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'15ed3b7c-6922-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'd3db2868-6922-e911-80c9-00155d020b39', NULL, 3, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 2, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T20:26:47.000' AS DateTime), NULL, CAST(N'2019-01-27T20:27:24.000' AS DateTime), NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='20e998c1-6922-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'20e998c1-6922-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'295de9b7-6922-e911-80c9-00155d020b39', NULL, 4, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T20:28:43.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='f0c83de6-6922-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'f0c83de6-6922-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'cfbdb7da-6922-e911-80c9-00155d020b39', NULL, 5, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T20:29:45.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='78d7c706-6a22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'78d7c706-6a22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'89fc37fe-6922-e911-80c9-00155d020b39', NULL, 6, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T20:30:39.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='e5a12b05-6b22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'e5a12b05-6b22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'6c4eb8fd-6a22-e911-80c9-00155d020b39', NULL, 7, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T20:37:46.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='f22a9a31-6b22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'f22a9a31-6b22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'38d88f24-6b22-e911-80c9-00155d020b39', NULL, 8, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 2, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T20:39:01.000' AS DateTime), NULL, CAST(N'2019-01-27T20:39:49.000' AS DateTime), NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='084d5bae-6b22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'084d5bae-6b22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'365911a6-6b22-e911-80c9-00155d020b39', NULL, 9, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T20:42:30.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='582b0ed9-6b22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'582b0ed9-6b22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'd034dbd1-6b22-e911-80c9-00155d020b39', NULL, 10, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T20:43:42.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='31eafa4a-6c22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'31eafa4a-6c22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'7e22d83e-6c22-e911-80c9-00155d020b39', NULL, 11, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T20:46:53.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='216bc7b0-6d22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'216bc7b0-6d22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'093372a9-6d22-e911-80c9-00155d020b39', NULL, 12, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T20:56:53.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='0bcc3820-6e22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'0bcc3820-6e22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'127cb717-6e22-e911-80c9-00155d020b39', NULL, 13, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T21:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='1f3b4647-6e22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'1f3b4647-6e22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'c7d8a03f-6e22-e911-80c9-00155d020b39', NULL, 14, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T21:01:06.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='01709369-6e22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'01709369-6e22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'3fba6e62-6e22-e911-80c9-00155d020b39', NULL, 15, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T21:02:03.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='70b9f49f-6e22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'70b9f49f-6e22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'04a5a397-6e22-e911-80c9-00155d020b39', NULL, 16, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T21:03:34.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='08aa6fc6-6e22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'08aa6fc6-6e22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'f5afd3be-6e22-e911-80c9-00155d020b39', NULL, 17, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T21:04:39.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='fae6b0e5-6e22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'fae6b0e5-6e22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'75fb85dd-6e22-e911-80c9-00155d020b39', NULL, 18, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T21:05:31.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='c0727412-6f22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'c0727412-6f22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'63da410a-6f22-e911-80c9-00155d020b39', NULL, 19, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T21:06:46.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='40f9d832-6f22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'40f9d832-6f22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'f642a92a-6f22-e911-80c9-00155d020b39', NULL, 20, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T21:07:41.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='79627d8a-6f22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'79627d8a-6f22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'84893b81-6f22-e911-80c9-00155d020b39', NULL, 21, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T21:10:08.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='963da2b4-6f22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'963da2b4-6f22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'1b4210ab-6f22-e911-80c9-00155d020b39', NULL, 22, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T21:11:19.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='841f77db-6f22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'841f77db-6f22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'887e0bd3-6f22-e911-80c9-00155d020b39', NULL, 23, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T21:12:24.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='678dcfff-6f22-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'678dcfff-6f22-e911-80c9-00155d020b39', N'4a05253e-6922-e911-80c9-00155d020b39', N'd95e10f6-6f22-e911-80c9-00155d020b39', NULL, 24, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T21:13:25.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO


IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='cc0b90ec-7022-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'cc0b90ec-7022-e911-80c9-00155d020b39', N'b7de48c2-7022-e911-80c9-00155d020b39', N'd3db2868-6922-e911-80c9-00155d020b39', NULL, 3, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T21:20:02.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO
IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE ID ='eaa2c13a-7122-e911-80c9-00155d020b39')
INSERT [dbo].[EREC_Obj_MetaAdatai] ([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [AlapertelmezettErtek], [Sorszam], [Opcionalis], [Ismetlodo], [Funkcio], [ControlTypeSource], [ControlTypeDataSource], [SPSSzinkronizalt], [SPS_Field_Id], [Ver], [Note], [Stat_id], [ErvKezd], [ErvVege], [Letrehozo_Id], [LetrehozasIdo], [Modosito_Id], [ModositasIdo], [Zarolo_Id], [ZarolasIdo], [Tranz_Id], [UIAccessLog_Id]) VALUES (N'eaa2c13a-7122-e911-80c9-00155d020b39', N'b7de48c2-7022-e911-80c9-00155d020b39', N'69f8042f-7122-e911-80c9-00155d020b39', NULL, 4, N'1', N'0', NULL, NULL, NULL, N'0', NULL, 1, NULL, NULL, CAST(N'2019-01-27T00:00:00.000' AS DateTime), CAST(N'4700-12-31T00:00:00.000' AS DateTime), NULL, CAST(N'2019-01-27T21:22:13.000' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL)
GO

PRINT 'STOP BLG 3530'
GO

------------------------------------------------------------------------------------------------------------------------------------------------



GO
------------------------------------------------------ BUG_5094 vége -------------------------------------------------------------------------------------


------------------------------------------------   BUG_4742      --------------------------
-- BUG_4742 Fordítás: NMHH-ban: "Borító típusa" --> "Vonalkód helye"

declare @id uniqueidentifier

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

if (@org_kod = 'NMHH') 
BEGIN
	print 'eRecordComponent/KuldKuldemenyFormTab.ascx'
	print 'labelBoritoTipus'

	set @id = '3278A447-4735-469A-93E1-5B1A644275D4'

	if not exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'eRecordComponent/KuldKuldemenyFormTab.ascx',
			'labelBoritoTipus',
			'Vonalkód helye:'
		)
	END


	print 'eRecordComponent/KuldKuldemenyFormTab.ascx'
	print 'labelBoritoTipusMegorzese'

	set @id = 'C7C85439-A2A2-4262-BCD8-500DCE0A18CF'

	if not exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'eRecordComponent/KuldKuldemenyFormTab.ascx',
			'labelBoritoTipusMegorzese',
			'Vonalkód helyének megőrzése'
		)
	END


	print 'EgyszerusitettIktatasForm.aspx'
	print 'labelBoritoTipus'

	set @id = '9A9051BC-F9EB-472C-A45D-1229FFA33F79'

	if not exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'EgyszerusitettIktatasForm.aspx',
			'labelBoritoTipus',
			'Vonalkód helye:'
		)
	END


	print 'EgyszerusitettIktatasForm.aspx'
	print 'labelBoritoTipusMegorzese'

	set @id = 'F5BFCDD3-2E69-4E93-8E52-11F5388D948E'

	if not exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'EgyszerusitettIktatasForm.aspx',
			'labelBoritoTipusMegorzese',
			'Vonalkód helyének megőrzése'
		)
	END

END
GO
------------------------------------------------   BUG_4742 vége   --------------------------


-- BUG 4780
-- UGYIRATBAN_MARADO_PELDANY_CIMZETTJE rendszerparameter
declare @recordId uniqueidentifier 
SET @recordId = 'FB44D795-0D71-41AE-8628-FC97D43DCD7D' 
DECLARE @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE Id = @recordId)
BEGIN 
	Print 'INSERT KRT_Parameterek - UGYIRATBAN_MARADO_PELDANY_CIMZETTJE' 
	 
	insert into KRT_Parameterek
	(
		Tranz_id
		,Stat_id
	    ,Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
	) 
	values 
	(
		NULL,
		NULL,
		@recordId,
		@org
		,'UGYIRATBAN_MARADO_PELDANY_CIMZETTJE'
		,'Átmeneti irattár'
		,'1'
		,'Iktatáskor az ügyiratban maradó példány címzettje'
	);
 END 


 GO




-------------------------------------------------------------------------------
-- BLG 6234 - Partner létrehozáskor alapértelmezett állapot legyen rendszerparaméterrel szabályozható
-- Rendszerparaméterrel legyen szabályozható az, hogy új partner létrehozásakor a partner állapota Jóváhagyandó vagy Jóváhagyott lesz.
-- PARTNER_LETREHOZAS_JOVAHAGYANDO rendszerparameter	 
-- ertek: 0,1
-- Default: 0
-- '0 - Partner létrehozáskor alapértelmezett állapot JOVAHAGYOTT lesz.
-- '1 - Partner létrehozáskor alapértelmezett állapot JOVAHAGYANDO lesz.

DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
where Id='EE557085-5AB9-4DF1-8F9E-561609B90CBB') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'EE557085-5AB9-4DF1-8F9E-561609B90CBB'
		,'PARTNER_LETREHOZAS_JOVAHAGYANDO'
		,'0'
		,'1'
		,'0 - Partner létrehozáskor alapértelmezett állapot JOVAHAGYOTT lesz.'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='EE557085-5AB9-4DF1-8F9E-561609B90CBB'
	--	,Nev='PARTNER_LETREHOZAS_JOVAHAGYANDO'
	--	,Ertek='0'
	--	,Karbantarthato='1'
	--	,Note='0 - Partner létrehozáskor alapértelmezett állapot JOVAHAGYOTT lesz.'
	-- WHERE Id=@record_id 
 --END

--IF @org_kod = 'NMHH' 
--BEGIN 
--	Print @org_kod +': PARTNER_LETREHOZAS_JOVAHAGYANDO = 1'
--	UPDATE KRT_Parameterek
--	   SET Ertek='1',
--	       Note= '1 - Partner létrehozáskor alapértelmezett állapot JOVAHAGYANDO lesz.'
--	   WHERE Id='EE557085-5AB9-4DF1-8F9E-561609B90CBB'
--END
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------
 ----------------------------- BLG 6234 - Partner létrehozáskor alapértelmezett állapot legyen rendszerparaméterrel szabályozható ---------------------------------

 GO

 ---------------------------
 ---BLG 5889 Minősítési jelölés alapja mező felvétele
 --------------------------

 --TUKIratok
 IF exists (select 1 from EREC_Obj_MetaDefinicio where Id = 'FD93D408-0973-E711-80C2-00155D020D7E')
 BEGIN

 DECLARE @org uniqueidentifier 
 set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

 --Tárgyszavak

	-- 'Minősítési jelölés alapja' tárgyszó

	declare @targyszoId uniqueidentifier
	set @targyszoId = 'BCB57F8B-869E-460B-A2DE-752178F2B1DC'


	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId)
	BEGIN
		print 'insert - Minősítési jelölés alapja'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[AlapertelmezettErtek]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   
		   
		)
		VALUES
		(
			@targyszoId
		   ,@Org
		   ,'1'
		   ,'Minősítési jelölés alapja'
		   ,''
		   ,'Minosites_jeloles_alapja'
		   ,'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls'
		   
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak:Minősítési jelölés alapja'
	END

	--OBJ_metaadatai bejegyzés
	declare @metaadatId uniqueidentifier
	set @metaadatId = 'C8E06689-A5B0-4A5B-86F8-BD03180A04B5'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @metaadatId)
	BEGIN
		print 'insert - EREC_Obj_MetaAdatai'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		  ,[Opcionalis]
		  ,[Ismetlodo]
		)
		VALUES
		(
			@metaadatId
		   ,'FD93D408-0973-E711-80C2-00155D020D7E'
		   ,@targyszoId
		   ,2
		   ,1
		   ,0
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_Obj_MetaAdatai:Minősítési jelölés alapja'
	END
 
 END
 GO
 -------------------------------------------------------------------------------
-- BUG 5658 - TÜK felhasználók listája/ TÜK adatok
--Kell egy SQL script a kódtár érték érvénytelenítésére.
--Adminisztréció/ Tük felhasználók -> felhasználó kijelölése/ Tük adatok fülre kattintás.
--Új nyomógombra kattintva a Típus lenyíló listából vegyük ki az Oktatási segédanyag-ot.
--Dokumentumtípus kódcsoportból jönnek a kódtár értékek. Csak innen kell kivenni az Oktatási segédanyag értéket.

 UPDATE [dbo].[KRT_KodTarak]
   SET [ErvVege] = '2019-02-07 00:00'
 WHERE Id = '8068BDA4-26D4-E811-80CB-00155D027EA9'
 --------------------------------------------------------------------------------------------------------------
 ----------------------------- BUG 5658 - TÜK felhasználók listája/ TÜK adatok --------------------------------
 --------------------------------------------------- VÉGE -----------------------------------------------------
 --------------------------------------------------------------------------------------------------------------


GO

 ------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Bug 6481:Ügyiratdarabok karbantartása
update KRT_Szerepkor_Funkcio
set ErvVege = GETDATE(),
	ModositasIdo  = GETDATE(),
	Modosito_id = '54E861A5-36ED-44CA-BAA7-C287D125B309',
	Ver = Ver + 1
where Funkcio_Id = '9A4020AE-0009-4AF8-BC7C-F7E0030F96C8' --Ügyiratdarabok listázás
and ErvVege > GETDATE()

GO

-----------------------------------------

----- BUG#5700 / Task#6681 ------------
-- Ügytípus statisztika objektum metadefinícióhoz új metaadat hozzárendelések létrehozása (SZURFoszamKeszWS-hez kell)
---------------------------------------

DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok
            where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

if (@org_kod = 'NMHH') and exists (select 1 from EREC_Obj_MetaDefinicio where Id = '74861630-2A05-4F57-9288-37BAE6225D0F')
BEGIN

	-- Tárgyszavak létrehozása: statElsofok, statJogorv, statElsofokNap, statElsoJogNap, statMasodNap, statAtlag, statOsszesDb, statKarigeny, statFelugyelet
	-- + EREC_Obj_MetaAdatai táblába is beszúrni (Ügyirattípus statisztikához kell bekötni)

	declare @targyszoId uniqueidentifier

	set @targyszoId = 'F9E009BE-9606-43BE-8684-6F250C370A9C'
	if (not exists(select Id from EREC_TargySzavak where Id = @targyszoId))
	BEGIN
		INSERT INTO EREC_TargySzavak (Id, Org, Tipus, TargySzavak, BelsoAzonosito, SPSSzinkronizalt, ControlTypeSource)
			VALUES (@targyszoId, '450B510A-7CAA-46B0-83E3-18445C0C53A9', 1
					, 'Első fokon jogerőre emelkedett határozatok száma', 'statElsofok', 0, 'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls')

		INSERT INTO EREC_Obj_MetaAdatai (Id, Obj_MetaDefinicio_Id, Targyszavak_Id, Sorszam, Opcionalis, Ismetlodo, SPSSzinkronizalt)
			VALUES ('7A309F81-A283-4F4B-8D0E-66A3A9574435', '74861630-2A05-4F57-9288-37BAE6225D0F', @targyszoId
					, 4, 1, 0, 0)
	END

	set @targyszoId = '6A23BE04-5B82-43B4-8D73-36CA8DA79951'
	if (not exists(select Id from EREC_TargySzavak where Id = @targyszoId))
	BEGIN
		INSERT INTO EREC_TargySzavak (Id, Org, Tipus, TargySzavak, BelsoAzonosito, SPSSzinkronizalt, ControlTypeSource)
			VALUES (@targyszoId, '450B510A-7CAA-46B0-83E3-18445C0C53A9', 1
					, 'Jogorvoslat során megváltoztatott határozatok száma', 'statJogorv', 0, 'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls')
					
		INSERT INTO EREC_Obj_MetaAdatai (Id, Obj_MetaDefinicio_Id, Targyszavak_Id, Sorszam, Opcionalis, Ismetlodo, SPSSzinkronizalt)
			VALUES ('C142AD2E-4E91-4ABB-AC15-7073A92C51E5', '74861630-2A05-4F57-9288-37BAE6225D0F', @targyszoId
					, 5, 1, 0, 0)
	END

	set @targyszoId = 'A4E2E575-686D-48E9-B25F-20C9809E58AA'
	if (not exists(select Id from EREC_TargySzavak where Id = @targyszoId))
	BEGIN
		INSERT INTO EREC_TargySzavak (Id, Org, Tipus, TargySzavak, BelsoAzonosito, SPSSzinkronizalt, ControlTypeSource)
			VALUES (@targyszoId, '450B510A-7CAA-46B0-83E3-18445C0C53A9', 1
					, 'Ügyintézési határidő túllépésének átlagos mértéke első fokon', 'statElsofokNap', 0, 'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls')
					
		INSERT INTO EREC_Obj_MetaAdatai (Id, Obj_MetaDefinicio_Id, Targyszavak_Id, Sorszam, Opcionalis, Ismetlodo, SPSSzinkronizalt)
			VALUES ('ABE22405-570F-402F-A07D-E88DE8B28981', '74861630-2A05-4F57-9288-37BAE6225D0F', @targyszoId
					, 6, 1, 0, 0)
	END

	set @targyszoId = '011B0776-C12C-447C-AD62-978ED00FF2EF'
	if (not exists(select Id from EREC_TargySzavak where Id = @targyszoId))
	BEGIN
		INSERT INTO EREC_TargySzavak (Id, Org, Tipus, TargySzavak, BelsoAzonosito, SPSSzinkronizalt, ControlTypeSource)
			VALUES (@targyszoId, '450B510A-7CAA-46B0-83E3-18445C0C53A9', 1
					, 'Ügyintézési határidő túllépésének átlagos mértéke első fokú jogorvoslatban', 'statElsoJogNap', 0, 'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls')
					
		INSERT INTO EREC_Obj_MetaAdatai (Id, Obj_MetaDefinicio_Id, Targyszavak_Id, Sorszam, Opcionalis, Ismetlodo, SPSSzinkronizalt)
			VALUES ('C06C1C94-CE3D-4C40-BD87-DECFE02B02E9', '74861630-2A05-4F57-9288-37BAE6225D0F', @targyszoId
					, 7, 1, 0, 0)
	END

	set @targyszoId = '714B73CB-E1CA-49FA-B858-9C3FC91BDC35'
	if (not exists(select Id from EREC_TargySzavak where Id = @targyszoId))
	BEGIN
		INSERT INTO EREC_TargySzavak (Id, Org, Tipus, TargySzavak, BelsoAzonosito, SPSSzinkronizalt, ControlTypeSource)
			VALUES (@targyszoId, '450B510A-7CAA-46B0-83E3-18445C0C53A9', 1
					, 'Ügyintézési határidő túllépésének átlagos mértéke másodfokú jogorvoslatban', 'statMasodNap', 0, 'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls')
										
		INSERT INTO EREC_Obj_MetaAdatai (Id, Obj_MetaDefinicio_Id, Targyszavak_Id, Sorszam, Opcionalis, Ismetlodo, SPSSzinkronizalt)
			VALUES ('8916B65F-8E72-49A6-ABB5-9E948164F5F7', '74861630-2A05-4F57-9288-37BAE6225D0F', @targyszoId
					, 8, 1, 0, 0)
	END

	set @targyszoId = '69749908-5CDB-4B8B-97F0-00168B86C400'
	if (not exists(select Id from EREC_TargySzavak where Id = @targyszoId))
	BEGIN
		INSERT INTO EREC_TargySzavak (Id, Org, Tipus, TargySzavak, BelsoAzonosito, SPSSzinkronizalt, ControlTypeSource)
			VALUES (@targyszoId, '450B510A-7CAA-46B0-83E3-18445C0C53A9', 1
					, 'Ügyintézési határidő túllépés átlagos mértéke', 'statAtlag', 0, 'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls')
						
		INSERT INTO EREC_Obj_MetaAdatai (Id, Obj_MetaDefinicio_Id, Targyszavak_Id, Sorszam, Opcionalis, Ismetlodo, SPSSzinkronizalt)
			VALUES ('68CE7BDE-2EE9-4424-8315-31EC58A1FB2A', '74861630-2A05-4F57-9288-37BAE6225D0F', @targyszoId
					, 9, 1, 0, 0)
	END

	set @targyszoId = '39C7811F-C1D7-4CEC-ACC9-518D97248665'
	if (not exists(select Id from EREC_TargySzavak where Id = @targyszoId))
	BEGIN
		INSERT INTO EREC_TargySzavak (Id, Org, Tipus, TargySzavak, BelsoAzonosito, SPSSzinkronizalt, ControlTypeSource)
			VALUES (@targyszoId, '450B510A-7CAA-46B0-83E3-18445C0C53A9', 1
					, 'Az iratcsomagban található ügyet lezáró iratok száma', 'statOsszesDb', 0, 'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls')
					
		INSERT INTO EREC_Obj_MetaAdatai (Id, Obj_MetaDefinicio_Id, Targyszavak_Id, Sorszam, Opcionalis, Ismetlodo, SPSSzinkronizalt)
			VALUES ('AFFD04D2-B29F-451E-99B2-C400B4C428C4', '74861630-2A05-4F57-9288-37BAE6225D0F', @targyszoId
					, 10, 1, 0, 0)
	END

	set @targyszoId = 'B0F85527-83C7-49FC-9F56-4C5EBA61F717'
	if (not exists(select Id from EREC_TargySzavak where Id = @targyszoId))
	BEGIN
		INSERT INTO EREC_TargySzavak (Id, Org, Tipus, TargySzavak, BelsoAzonosito, SPSSzinkronizalt, ControlTypeSource)
			VALUES (@targyszoId, '450B510A-7CAA-46B0-83E3-18445C0C53A9', 1
					, 'Hatósággal szemben benyújtott kártérítési igények száma', 'statKarigeny', 0, 'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls')
					
		INSERT INTO EREC_Obj_MetaAdatai (Id, Obj_MetaDefinicio_Id, Targyszavak_Id, Sorszam, Opcionalis, Ismetlodo, SPSSzinkronizalt)
			VALUES ('FE98B065-9470-4B0C-A89F-540933AD42FF', '74861630-2A05-4F57-9288-37BAE6225D0F', @targyszoId
					, 11, 1, 0, 0)
	END
		
	set @targyszoId = 'DBB8D947-BF6E-488E-AC69-034FFCC84AE8'
	if (not exists(select Id from EREC_TargySzavak where Id = @targyszoId))
	BEGIN
		INSERT INTO EREC_TargySzavak (Id, Org, Tipus, TargySzavak, BelsoAzonosito, SPSSzinkronizalt, ControlTypeSource)
			VALUES (@targyszoId, '450B510A-7CAA-46B0-83E3-18445C0C53A9', 1
					, 'Felügyeleti intézkedések száma', 'statFelugyelet', 0, 'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls')
					
		INSERT INTO EREC_Obj_MetaAdatai (Id, Obj_MetaDefinicio_Id, Targyszavak_Id, Sorszam, Opcionalis, Ismetlodo, SPSSzinkronizalt)
			VALUES ('7265B1E0-0A85-42BC-A638-98B1BF209275', '74861630-2A05-4F57-9288-37BAE6225D0F', @targyszoId
					, 12, 1, 0, 0)
	END

END

GO
-----------------------------------------

-----------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- BUG#4758 - IRATTARBA_IKTATAS_ENABLED rendszerparaméter
-- Rendszerparaméterrel lehessen kikapcsolni, hogy lehet-e iktatni irattárban levő ügyiratokra 
-- (akár központi irattárban, akár átmeneti irattárban van az ügyirat). 
-- Az ügyirat állapota nem lehet Irattárban őrzött, Kölcsönzött, Engedélyezett kikérőn lévő, Irattárból elkért.
--
-- IRATTARBA_IKTATAS_ENABLED rendszerparameter	 
-- ertek: 0,1
-- Default: 1
-- 0 - Nem lehet iktatni irattárban lévő ügyiratokba
-- 1 - Lehet iktatni irattárban lévő ügyiratokba

DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
where Id='E196288F-AD1D-4022-92D3-109EF885E9C0') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'insert into KRT_Parameterek: IRATTARBA_IKTATAS_ENABLED' 

	declare @paramValue nvarchar(10)

	-- NMHH-nál kell csak letiltani az irattárba iktatást:
	if (@org_kod = 'NMHH')
		set @paramValue = '0'	
	else 
		set @paramValue = '1'
	
	 insert into KRT_Parameterek(
		Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'E196288F-AD1D-4022-92D3-109EF885E9C0'
		,'IRATTARBA_IKTATAS_ENABLED'
		, @paramValue
		,'1'
		,'0 - Nem lehet iktatni irattárban lévő ügyiratokba; 1 - Lehet iktatni irattárban lévő ügyiratokba'
		); 
 END 

 GO


-------------------------------------------------------------------------------
-- BUG 6309 - Rendszerparaméterrel legyen szabályozható, hogy az ügyfél neve és címe kötelező mező legyen-e
-- Amikor az értéke '1', akkor minden iktatóképernyőn (egyszerűsített érkeztetés-iktatás, belső irat iktatás, bejövő irat iktatása) legyen kötelező az ügyfél nevének és címének megadása. Minden egyéb adat esetén maradjon úgy a működés, ahogy most van.
-- UGYFEL_ADATOK_KOTELEZOSEG rendszerparameter	 
-- ertek: 0,1
-- Default: 0
-- '1 - minden iktatóképernyőn (egyszerűsített érkeztetés-iktatás, belső irat iktatás, bejövő irat iktatása) legyen kötelező az ügyfél nevének és címének megadása, 0 - maradjon úgy a működés, ahogy most van.'

DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
where Id='0650B322-3C78-40F3-A8A7-8628F236FAA0') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'0650B322-3C78-40F3-A8A7-8628F236FAA0'
		,'UGYFEL_ADATOK_KOTELEZOSEG'
		,'0'
		,'1'
		,'1 - minden iktatóképernyőn (egyszerűsített érkeztetés-iktatás, belső irat iktatás, bejövő irat iktatása) legyen kötelező az ügyfél nevének és címének megadása, 0 - maradjon úgy a működés, ahogy most van.'
		); 
 END 

GO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 ----------------------------- BUG 6309 - Rendszerparaméterrel legyen szabályozható, hogy az ügyfél neve és címe kötelező mező legyen-e ---------------------------------
 --------------------------------------------------------------------------------------- Vége --------------------------------- -----------------------------------------
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------

 ------------------------------------------------ POSTAZOTT_TELJESSEGELLENORZES_KELL --------------------------------------------------------------------------------------

DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = '32330806-9911-488C-9CCB-42AA54EE7AFB'

IF @org_kod = 'NMHH'
set @ertek = '0'
else
set @ertek = '1'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - POSTAZOTT_TELJESSEGELLENORZES_KELL - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'POSTAZOTT_TELJESSEGELLENORZES_KELL'
		,@ertek
		,'1'
		,'Ügyirat elintézésnél az ellenőrzések kikapcsolása.'
		); 
 END

 GO

 ------------------------------------------------ POSTAZOTT_TELJESSEGELLENORZES_KELL -------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- BUG 6295 - SSRS riportok automatikus letöltés PDF-ben
-- Megadott riportok automatikus letöltése PDF formátumban.
-- SSRS_AUTO_PDF_RENDER rendszerparameter     
-- ertek: 0,1
-- Default: 0
-- '1 - adott riportok PDF formátumban letöltésre kerülnek, 0 - a riportok felugró ablakban jelennek meg'
-- 1-es érték megadásakor a következő esetekben fog működni a letöltés:
-- ElőadóiÍv nyomtatása
-- Tömeges előadóiív nyomtatása
-- Küldemény kísérő nyomtatása
-- Küldemény átvételi elismervény nyomtatása
-- Ügyirat kísérő nyomtatása
-- Ügyirat borító nyomtatása

print 'SSRS_AUTO_PDF_RENDER rendszerparameter'

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='2ACF9D0C-1625-46D4-BA26-3571F908F02A') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'2ACF9D0C-1625-46D4-BA26-3571F908F02A'
		,'SSRS_AUTO_PDF_RENDER'
		,'0'
		,'1'
		,'Megadott riportok automatikus letöltése PDF formátumban.'
		); 
 END 
 -- -- ELSE 
 -- -- BEGIN 
	 -- -- Print 'UPDATE'
	 -- -- UPDATE KRT_Parameterek
	 -- -- SET 
		-- -- Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		-- -- ,Id='2ACF9D0C-1625-46D4-BA26-3571F908F02A'
		-- -- ,Nev='SSRS_AUTO_PDF_RENDER'
		-- -- ,Ertek='0'
		-- -- ,Karbantarthato='1'
		-- -- ,Note='Megadott riportok automatikus letöltése PDF formátumban.'
	 -- -- WHERE Id=@record_id 
 -- -- END

 GO
 -------------- end of BUG 6295

 ------------------------------------------------------------------------------------------------------------------------------------------------------------------

 ---- BUG#5901 
 ---- NMHH_EXPMOD kódcsoport létrehozása kódtárelemekkel, plusz függő kódtár az NMHH_EXPMOD - KULDEMENY_KULDES_MODJA mappinggel

DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

if (@org_kod = 'NMHH')
BEGIN

	if (not exists(select Id from KRT_KodCsoportok where id = '47157097-8933-E911-80CD-00155D020DFE'))
	BEGIN
	
		print 'NMHH_EXPMOD kódcsoport létrehozása...'

		INSERT INTO KRT_KodCsoportok (Id, Kod, Nev, Modosithato, BesorolasiSema)
			values ('47157097-8933-E911-80CD-00155D020DFE', 'NMHH_EXPMOD', 'NMHH Expediálás módja', '1', '0')

		-- Kódtárelemek felvétele:		

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('D2F46BD8-8933-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','01','a hatóság kézbesítője útján','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('5CE1C6E9-8933-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','02','Alapvető Jogok Biztosa levelező rendszerén keresztül','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('0E81FA15-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','03','e-mailen','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('22B4721C-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','04','elektronikus úton (Hivatali kapu)','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('25005328-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','05','elektronikus úton (Ügyfélkapu)','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('28005328-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','06','faxon','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('685B1731-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','07','faxon és postán','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('04BDC537-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','08','futárpostán','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('B7CCF93F-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','09','hatósági kapcsolattartás elektronikus úton','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('289F574D-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','10','helyben maradó','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('3FE1C154-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','11','hirdetményi úton','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('A71D6360-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','12','hivatali kapun','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('E351AA66-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','13','Ket. szerinti elektronikus úton','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('D0386972-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','14','Ket. szerinti feltételekkel szóban','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('981C6279-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','15','kézbesítési meghatalmazott útján','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('986338D1-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','16','kézbesítési ügygondnok útján','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('3B9578DD-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','17','másik dokumentummal továbbítva','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('F1A8BEE3-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','18','NFM járat','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('7384FBE9-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','19','NMHH járat','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('D7F223F5-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','20','postai úton','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('DFAA21FB-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','21','személyesen átadott irat útján','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('E2AA21FB-8A33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','22','táviratban','1')

		INSERT INTO KRT_KodTarak (Id, Org, KodCsoport_Id, Kod, Nev, Modosithato) 
		VALUES('65AECE06-8B33-E911-80CD-00155D020DFE','450B510A-7CAA-46B0-83E3-18445C0C53A9','47157097-8933-E911-80CD-00155D020DFE','23','telefonon','1')
		
	END

	-- NMHH_EXPMOD - KULDEMENY_KULDES_MODJA kódtárfüggőség:
	if (not exists(select Id from KRT_KodtarFuggoseg where Id = '855C031E-3A70-410A-86BE-290155F2DC7A'))
	BEGIN
		print 'NMHH_EXPMOD - KULDEMENY_KULDES_MODJA kódtárfüggőség létrehozása...'

		INSERT INTO KRT_KodtarFuggoseg (Id, Org, Vezerlo_KodCsoport_Id, Fuggo_KodCsoport_Id, Adat, Aktiv)
			VALUES ('855C031E-3A70-410A-86BE-290155F2DC7A', '450B510A-7CAA-46B0-83E3-18445C0C53A9', '47157097-8933-E911-80CD-00155D020DFE', '6982F668-C8CD-47A3-AC62-9F16E439473C'
					, '{"VezerloKodCsoportId":"47157097-8933-e911-80cd-00155d020dfe","FuggoKodCsoportId":"6982f668-c8cd-47a3-ac62-9f16e439473c","Items":[{"VezerloKodTarId":"22b4721c-8a33-e911-80cd-00155d020dfe","FuggoKodtarId":"cc0d5f87-279b-45c1-905b-dd721a1fb870","Aktiv":true},{"VezerloKodTarId":"25005328-8a33-e911-80cd-00155d020dfe","FuggoKodtarId":"cc0d5f87-279b-45c1-905b-dd721a1fb870","Aktiv":true},{"VezerloKodTarId":"0e81fa15-8a33-e911-80cd-00155d020dfe","FuggoKodtarId":"42496278-92f6-4c45-96fe-d0356281270a","Aktiv":true},{"VezerloKodTarId":"28005328-8a33-e911-80cd-00155d020dfe","FuggoKodtarId":"e590c4a5-6bbf-46a9-8933-85a62a9e84a2","Aktiv":true},{"VezerloKodTarId":"b7ccf93f-8a33-e911-80cd-00155d020dfe","FuggoKodtarId":"871d6057-a117-4695-8aa2-d6cf89a5ede1","Aktiv":true},{"VezerloKodTarId":"289f574d-8a33-e911-80cd-00155d020dfe","FuggoKodtarId":"3ef9bdb4-117a-e811-80c7-00155d027ea9","Aktiv":true},{"VezerloKodTarId":"3fe1c154-8a33-e911-80cd-00155d020dfe","FuggoKodtarId":"072d82f1-0220-448f-9aa8-9e646bde3b8d","Aktiv":true},{"VezerloKodTarId":"a71d6360-8a33-e911-80cd-00155d020dfe","FuggoKodtarId":"cc0d5f87-279b-45c1-905b-dd721a1fb870","Aktiv":true},{"VezerloKodTarId":"3b9578dd-8a33-e911-80cd-00155d020dfe","FuggoKodtarId":"f9b80f0d-fe46-4921-8b66-359b0e384aef","Aktiv":true},{"VezerloKodTarId":"d7f223f5-8a33-e911-80cd-00155d020dfe","FuggoKodtarId":"7eb11332-5a91-e711-80c2-00155d020d7e","Aktiv":true},{"VezerloKodTarId":"e2aa21fb-8a33-e911-80cd-00155d020dfe","FuggoKodtarId":"3512557f-3e1b-44ef-b341-54775356a5d7","Aktiv":true},{"VezerloKodTarId":"65aece06-8b33-e911-80cd-00155d020dfe","FuggoKodtarId":"e7a6fc86-2de6-4a9c-aa70-2bd3c4328b88","Aktiv":true}],"ItemsNincs":[]}'
					, '1')
	END

	-- KULDEMENY_KULDES_MODJA - NMHH_EXPMOD kódtárfüggőség:
	if (not exists(select Id from KRT_KodtarFuggoseg where Id = '2DEF9F1B-6CE8-4EDC-9C43-8FB78F3BB5FC'))
	BEGIN
		print 'KULDEMENY_KULDES_MODJA - NMHH_EXPMOD kódtárfüggőség létrehozása...'

		INSERT INTO KRT_KodtarFuggoseg (Id, Org, Vezerlo_KodCsoport_Id, Fuggo_KodCsoport_Id, Adat, Aktiv)
			VALUES ('2DEF9F1B-6CE8-4EDC-9C43-8FB78F3BB5FC', '450B510A-7CAA-46B0-83E3-18445C0C53A9', '6982F668-C8CD-47A3-AC62-9F16E439473C', '47157097-8933-E911-80CD-00155D020DFE'
					, '{"VezerloKodCsoportId":"6982f668-c8cd-47a3-ac62-9f16e439473c","FuggoKodCsoportId":"47157097-8933-e911-80cd-00155d020dfe","Items":[{"VezerloKodTarId":"871d6057-a117-4695-8aa2-d6cf89a5ede1","FuggoKodtarId":"b7ccf93f-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"42496278-92f6-4c45-96fe-d0356281270a","FuggoKodtarId":"0e81fa15-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"cc0d5f87-279b-45c1-905b-dd721a1fb870","FuggoKodtarId":"25005328-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"3ef9bdb4-117a-e811-80c7-00155d027ea9","FuggoKodtarId":"289f574d-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"072d82f1-0220-448f-9aa8-9e646bde3b8d","FuggoKodtarId":"3fe1c154-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"7eb11332-5a91-e711-80c2-00155d020d7e","FuggoKodtarId":"d7f223f5-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"4411423d-5a91-e711-80c2-00155d020d7e","FuggoKodtarId":"d7f223f5-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"4c95095b-3fbe-4ffe-9be6-e445843071b8","FuggoKodtarId":"d7f223f5-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"14a94d2f-0916-49d5-94b1-ac9eb2bb6392","FuggoKodtarId":"d7f223f5-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"13c07c55-9330-4801-a30d-eaa04e540daa","FuggoKodtarId":"d7f223f5-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"632664b7-c298-4c2c-9f9e-f7122115ea75","FuggoKodtarId":"d7f223f5-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"ed318e7b-2642-4491-809c-5e8ddc38d7a9","FuggoKodtarId":"d7f223f5-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"35654f8c-a8fc-4d64-a46d-82007c4fcfb9","FuggoKodtarId":"d7f223f5-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"d7f223f5-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"f9b80f0d-fe46-4921-8b66-359b0e384aef","FuggoKodtarId":"3b9578dd-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"815f0c4f-b1ef-41e2-a41f-41e2cac6e5eb","FuggoKodtarId":"d7f223f5-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"fd72ac44-c675-4099-9609-8264f12e1109","FuggoKodtarId":"d7f223f5-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"3512557f-3e1b-44ef-b341-54775356a5d7","FuggoKodtarId":"e2aa21fb-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"e590c4a5-6bbf-46a9-8933-85a62a9e84a2","FuggoKodtarId":"28005328-8a33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"e7a6fc86-2de6-4a9c-aa70-2bd3c4328b88","FuggoKodtarId":"65aece06-8b33-e911-80cd-00155d020dfe","Aktiv":true},{"VezerloKodTarId":"70436f6b-396e-4994-a720-184689023aa1","FuggoKodtarId":"d7f223f5-8a33-e911-80cd-00155d020dfe","Aktiv":true}],"ItemsNincs":[]}'
					, '1')
	END

END

 GO
 
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- BUG 6466 - irat szignálás
-- Rendszerparaméterrel lehessen szabályozni, hogy az Irat szignálás funkcionalitás elérhető-e a rendszerben, IRAT_SZIGNALAS_ELERHETO, alapértelmezetten értéke 0.
-- Ha értéke 1, akkor jelenjen meg.
-- Ha az értéke 0 vagy bármi más, akkor az irat szignálás funkcionalitás gomb akkor se jelenjen az Iratok listáján és semmilyen abból származtatott listán.
-- IRAT_SZIGNALAS_ELERHETO rendszerparameter     
-- ertek: 0,1
-- Default: 0

print 'IRAT_SZIGNALAS_ELERHETO rendszerparameter'

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier 
SET @record_id = '0B74C977-8618-4AA7-8862-A1C9CB2C3C22'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE ID = @record_id) 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,@record_id
		,'IRAT_SZIGNALAS_ELERHETO'
		,'0'
		,'1'
		,'Irat szignálás funkcionalitás elérhető-e a rendszerben.'
		); 
 END 
 GO
 -------------- end of BUG 6466


------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- BUG 6549 - Iratpéldány expediálás hiba
-- Belső keletkezésű, kimenő irat iktatásakor a Címzett címe mező legyen kötelező.
-- BELSO_KIMENO_CIMZETT_CIME_KOTELEZO rendszerparameter
-- ertek: 0,1
-- Default: NMHH: 1, különben 0
-- '1 - akkor Belső keletkezésű, kimenő irat iktatásakor a Címzett címe mező legyen kötelezően kitöltendő.'
print 'BELSO_KIMENO_CIMZETT_CIME_KOTELEZO rendszerparameter'

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='16A1C3D9-5A9B-4783-8246-651A30F7EEF7') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'16A1C3D9-5A9B-4783-8246-651A30F7EEF7'
		,'BELSO_KIMENO_CIMZETT_CIME_KOTELEZO'
		,'0'
		,'1'
		,'Belső keletkezésű irat iktatásakor címzett címe kötelező.'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Nev='BELSO_KIMENO_CIMZETT_CIME_KOTELEZO'
	--	,Ertek='0'
	--	,Karbantarthato='1'
	--	,Note='Belső keletkezésű irat iktatásakor címzett címe kötelező.'
	-- WHERE Id=@record_id 
 --END

--if @org_kod = 'NMHH' 
--BEGIN 
--	Print @org_kod +': BELSO_KIMENO_CIMZETT_CIME_KOTELEZO = 1'
--	UPDATE KRT_Parameterek
--	   SET Ertek='1'
--	   WHERE Id='16A1C3D9-5A9B-4783-8246-651A30F7EEF7'
--END

 GO
 -------------- end of BUG 6549

 -------------- start of BUG 2601
 
 GO
 
 DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='9726E6F1-E83F-E911-80CF-00155D020B58') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

DECLARE @ertek nvarchar(100)=''

if @org_kod = 'FPH' 
	SET @ertek='735638'	   
IF @org_kod = 'BOPMH' 
	SET @ertek='730105'	   
IF @org_kod = 'CSPH' 
	SET @ertek='735847'	   	

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'9726E6F1-E83F-E911-80CF-00155D020B58'
		,'PIR_AZONOSITO'
		,@ertek
		,'1'
		,'A szervezet PIR azonosítósáját tartalmazza'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Nev='PIR_AZONOSITO'
	--	,Ertek=@ertek
	--	,Karbantarthato='1'
	--	,Note='A szervezet PIR azonosítósáját tartalmazza'
	-- WHERE Id=@record_id 
 --END

 GO
 
declare @modulIdUgyirat uniqueidentifier
set @modulIdUgyirat  = '2fa6b0f7-7c8f-40a9-abfd-33f327e01f0e'
declare @modulIdStatisztika uniqueidentifier
set @modulIdStatisztika  = '73f2ac45-7127-43f4-b48d-782d3c489f9d'

IF NOT EXISTS (Select 1 FROM INT_Modulok where Id = @modulIdUgyirat)
BEGIN
	INSERT INTO INT_Modulok
	(Id, Org, Nev,Parancs,Gyakorisag,GyakorisagMertekegyseg,UtolsoFutas)
	VALUES
	(@modulIdUgyirat,'450b510a-7caa-46b0-83e3-18445c0c53a9', 'ASPDWH','ugyirat', 1, '1', '2019.01.01')
END

IF NOT EXISTS (Select 1 FROM INT_Modulok where Id = @modulIdStatisztika)
BEGIN
	INSERT INTO INT_Modulok
	(Id, Org, Nev,Parancs,Gyakorisag,GyakorisagMertekegyseg,UtolsoFutas)
	VALUES
	(@modulIdStatisztika, '450b510a-7caa-46b0-83e3-18445c0c53a9', 'ASPDWH','statisztika', 1, '28', '2019.01.01')
END
 
 GO
 
 declare @recordIdUgyirat uniqueidentifier
set @recordIdUgyirat = '13dfa35e-2d44-4419-8f05-3411885f1886'
 declare @recordIdStatisztika uniqueidentifier
set @recordIdStatisztika = 'fa654df9-b9ad-4dd8-a8ff-4a9ad6e26d6c'
declare @modulIdUgyirat uniqueidentifier
set @modulIdUgyirat  = '2fa6b0f7-7c8f-40a9-abfd-33f327e01f0e'
declare @modulIdStatisztika uniqueidentifier
set @modulIdStatisztika  = '73f2ac45-7127-43f4-b48d-782d3c489f9d'

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @recordIdUgyirat)
BEGIN
	INSERT INTO INT_Parameterek
	(Id, Org, Modul_id, Nev, Ertek, Karbantarthato)
	VALUES
	(@recordIdUgyirat, '450b510a-7caa-46b0-83e3-18445c0c53a9', @modulIdUgyirat, 'FEJLESZTO_AZONOSITO', 'axis','1')
END

IF NOT EXISTS (Select 1 FROM INT_Parameterek where Id = @recordIdStatisztika)
BEGIN
	INSERT INTO INT_Parameterek
	(Id, Org, Modul_id, Nev, Ertek, Karbantarthato)
	VALUES
	(@recordIdStatisztika, '450b510a-7caa-46b0-83e3-18445c0c53a9', @modulIdStatisztika, 'FEJLESZTO_AZONOSITO', 'axis','1')
END
 
 GO
 
 DECLARE @ORG UNIQUEIDENTIFIER
SET @ORG = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @KodCsoport_Id_SYNC uniqueidentifier
SET @KodCsoport_Id_SYNC = '8AB7A74C-5B40-E911-80CF-00155D020B58'

IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = @KodCsoport_Id_SYNC)
	BEGIN 		
		insert into KRT_KodCsoportok(
		 BesorolasiSema
		,Id
		,Kod
		,Nev
		,Modosithato
		,Note
		) values (
		'0'
		,@KodCsoport_Id_SYNC
		,'SYNC_ALLAPOT'
		,'Adattárház szinkronizálás állapota'
		,'0'
		,'Adattárház szinkronizálás állapota'
		); 
	END
GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '8AB7A74C-5B40-E911-80CF-00155D020B58'
declare @recordId uniqueidentifier
set @recordId = '76454761-5B40-E911-80CF-00155D020B58'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'0'
	,'Folyamatban'
	,'0'
	,'0'
	); 
END 

 GO
 
 declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '8AB7A74C-5B40-E911-80CF-00155D020B58'
declare @recordId uniqueidentifier
set @recordId = '9780646B-5B40-E911-80CF-00155D020B58'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'1'
	,'Sikeres'
	,'0'
	,'1'
	); 
END 

 GO
 
  declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '8AB7A74C-5B40-E911-80CF-00155D020B58'
declare @recordId uniqueidentifier
set @recordId = 'A6AD0175-5B40-E911-80CF-00155D020B58'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'9'
	,'Hiba'
	,'0'
	,'9'
	); 
END 

 GO
 
 DECLARE @ORG UNIQUEIDENTIFIER
SET @ORG = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @KodCsoport_Id_SYNC uniqueidentifier
SET @KodCsoport_Id_SYNC = '4E24DB2D-1840-E911-80CC-00155D020D03'

IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = @KodCsoport_Id_SYNC)
	BEGIN 		
		insert into KRT_KodCsoportok(
		 BesorolasiSema
		,Id
		,Kod
		,Nev
		,Modosithato
		,Note
		) values (
		'0'
		,@KodCsoport_Id_SYNC
		,'ASP_EXPEDIALAS'
		,'ASP Expediálás'
		,'1'
		,'ASP Expediálás'
		); 
	END
GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '4E24DB2D-1840-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '3D1F5D3A-1840-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'001'
	,'Postával kézbesítendő'
	,'1'	
	,NULL
	); 
END 

 GO

 declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '4E24DB2D-1840-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '5E453445-1840-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'003'
	,'Futárszolgálattal kézbesítendő'
	,'0'
	,NULL
	); 
END 

 GO

  declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '4E24DB2D-1840-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = 'E7A9FC53-1840-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'005'
	,'E-mail'
	,'0'
	,NULL
	); 
END 

 GO

   declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '4E24DB2D-1840-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '781C205F-1840-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'006'
	,'KÉR'
	,'0'
	,NULL
	); 
END 

 GO

 
   declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '4E24DB2D-1840-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '0D57C66B-1840-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'007'
	,'KÉR Postai hibrid'
	,'0'
	,NULL
	); 
END 

 GO

    declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '4E24DB2D-1840-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '09FBFB77-1840-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'008'
	,'Hivatali kapu'
	,'0'
	,NULL
	); 
END 

 GO

   declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '4E24DB2D-1840-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '365EAF82-1840-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'002'
	,'Külön kézbesítendő'
	,'0'
	,NULL
	); 
END 

 GO

 
   declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '4E24DB2D-1840-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '9849C98D-1840-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'010'
	,'Hivatali Kapu2'
	,'0'
	,NULL
	); 
END 

 GO

 DECLARE @ORG UNIQUEIDENTIFIER
SET @ORG = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @KodCsoport_Id_SYNC uniqueidentifier
SET @KodCsoport_Id_SYNC = '67FD01ED-1440-E911-80CC-00155D020D03'

IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = @KodCsoport_Id_SYNC)
	BEGIN 		
		insert into KRT_KodCsoportok(
		 BesorolasiSema
		,Id
		,Kod
		,Nev
		,Modosithato
		,Note
		) values (
		'0'
		,@KodCsoport_Id_SYNC
		,'ASP_ADATHORDOZO'
		,'ASP Adathordozó'
		,'1'
		,'ASP Adathordozó'
		); 
	END
GO
 
    declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '67FD01ED-1440-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '5D6A3033-1640-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'001'
	,'e-mail'
	,'0'
	,NULL
	); 
END 

 GO

  declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '67FD01ED-1440-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = 'D7A3D43E-1640-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'002'
	,'CD, DVD'
	,'0'
	,NULL
	); 
END 

 GO

   declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '67FD01ED-1440-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '63AEE249-1640-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'003'
	,'pendrive, floppy'
	,'0'
	,NULL
	); 
END 

 GO

    declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '67FD01ED-1440-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '17116D52-1640-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'004'
	,'papír'
	,'0'
	,NULL
	); 
END 

 GO

     declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '67FD01ED-1440-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '8A8BF55C-1640-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'006'
	,'elektronikus űrlap'
	,'0'
	,NULL
	); 
END 

 GO

      declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '67FD01ED-1440-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '4D795567-1640-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'007'
	,'fax'
	,'0'
	,NULL
	); 
END 

 GO

       declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '67FD01ED-1440-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '2CEE5E77-1640-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'008'
	,'KR boríték'
	,'0'
	,NULL
	); 
END 

 GO

        declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '67FD01ED-1440-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = 'A4A09283-1640-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'009'
	,'KRX boríték'
	,'0'
	,NULL
	); 
END 

 GO

         declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '67FD01ED-1440-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '6CD4068A-1640-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'010'
	,'állomány'
	,'0'
	,NULL
	); 
END 

 GO

          declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '67FD01ED-1440-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '27D07994-1640-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'011'
	,'elektronikus NAV űrlap'
	,'0'
	,NULL
	); 
END 

 GO

           declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '67FD01ED-1440-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = 'F5FDFE9D-1640-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'012'
	,'elektronikus egyéb űrlap'
	,'0'
	,NULL
	); 
END 

 GO

            declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '67FD01ED-1440-E911-80CC-00155D020D03'
declare @recordId uniqueidentifier
set @recordId = '99F9ADA7-1640-E911-80CC-00155D020D03'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId	
	,'013'
	,'külön_kézbesítés'
	,'0'
	,NULL
	); 
END 

 GO

 IF NOT EXISTS (SELECT 1 FROM KRT_KODTARFUGGOSEG WHERE ID='66EA803E-AF1C-44E6-B575-C42B63F40392')
	BEGIN		
		INSERT [dbo].[KRT_KodtarFuggoseg] ([Id], [Org], [Vezerlo_KodCsoport_Id], [Fuggo_KodCsoport_Id], [Adat], [Aktiv]) 
		VALUES (N'66EA803E-AF1C-44E6-B575-C42B63F40392', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'6982F668-C8CD-47A3-AC62-9F16E439473C', N'4E24DB2D-1840-E911-80CC-00155D020D03', N'{"VezerloKodCsoportId":"6982f668-c8cd-47a3-ac62-9f16e439473c","FuggoKodCsoportId":"4e24db2d-1840-e911-80cc-00155d020d03","Items":[{"VezerloKodTarId":"4c95095b-3fbe-4ffe-9be6-e445843071b8","FuggoKodtarId":"3d1f5d3a-1840-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"9ef5a8f3-89a8-e511-a662-001ec9e754bc","FuggoKodtarId":"3d1f5d3a-1840-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"42496278-92f6-4c45-96fe-d0356281270a","FuggoKodtarId":"e7a9fc53-1840-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"582bfcde-89a8-e511-a662-001ec9e754bc","FuggoKodtarId":"3d1f5d3a-1840-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"e590c4a5-6bbf-46a9-8933-85a62a9e84a2","FuggoKodtarId":"e7a9fc53-1840-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"072d82f1-0220-448f-9aa8-9e646bde3b8d","FuggoKodtarId":"5e453445-1840-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"b5a320e1-dbb3-4c9a-b131-8189008f22f1","FuggoKodtarId":"09fbfb77-1840-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"0d7fdddb-d041-e611-942d-00155dce0fe1","FuggoKodtarId":"09fbfb77-1840-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"815f0c4f-b1ef-41e2-a41f-41e2cac6e5eb","FuggoKodtarId":"0d57c66b-1840-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"fd72ac44-c675-4099-9609-8264f12e1109","FuggoKodtarId":"3d1f5d3a-1840-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"ed318e7b-2642-4491-809c-5e8ddc38d7a9","FuggoKodtarId":"3d1f5d3a-1840-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"35654f8c-a8fc-4d64-a46d-82007c4fcfb9","FuggoKodtarId":"3d1f5d3a-1840-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"14a94d2f-0916-49d5-94b1-ac9eb2bb6392","FuggoKodtarId":"365eaf82-1840-e911-80cc-00155d020d03","Aktiv":true}],"ItemsNincs":[]}', N'1')
	END

 GO

 IF NOT EXISTS (SELECT 1 FROM KRT_KODTARFUGGOSEG WHERE ID='B4ABAEEA-4E2E-450B-8D36-23E8C077676D')
	BEGIN		
		INSERT [dbo].[KRT_KodtarFuggoseg] ([Id], [Org], [Vezerlo_KodCsoport_Id], [Fuggo_KodCsoport_Id], [Adat], [Aktiv]) 
		VALUES (N'B4ABAEEA-4E2E-450B-8D36-23E8C077676D', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'025C2C0E-368D-4E19-A660-36B146F5F6C8', N'67FD01ED-1440-E911-80CC-00155D020D03', N'{"VezerloKodCsoportId":"025c2c0e-368d-4e19-a660-36b146f5f6c8","FuggoKodCsoportId":"67fd01ed-1440-e911-80cc-00155d020d03","Items":[{"VezerloKodTarId":"20f9ce3b-4efd-4c36-bff3-118bd975e7ef","FuggoKodtarId":"d7a3d43e-1640-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"9d3cdd9f-b6cb-4547-b493-425aa0e291fb","FuggoKodtarId":"d7a3d43e-1640-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"709f8fe9-b282-4911-bfee-2ef156cbe7f7","FuggoKodtarId":"17116d52-1640-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"44de44d5-df6c-41b7-bee9-24519196fb3b","FuggoKodtarId":"5d6a3033-1640-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"a1fe8710-a5ca-4cbe-a1f7-8ec0e89b8738","FuggoKodtarId":"4d795567-1640-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"0ff7b280-f85b-4660-a981-c9cd9f564125","FuggoKodtarId":"63aee249-1640-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"81b4e99d-95db-46ae-856c-10dfaf8cf6a2","FuggoKodtarId":"63aee249-1640-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"3fa9cf1e-3773-4864-89bc-e4884e3de1d0","FuggoKodtarId":"17116d52-1640-e911-80cc-00155d020d03","Aktiv":true},{"VezerloKodTarId":"4938192b-2c5e-4d3b-adf2-6cf363ad5605","FuggoKodtarId":"63aee249-1640-e911-80cc-00155d020d03","Aktiv":true}],"ItemsNincs":[]}', N'1')
	END

GO


DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='CFB5EAB3-6B51-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		NULL
		,'0'
		,'CFB5EAB3-6B51-E911-80D1-00155D020D17'
		,'INTModulList'
		,'INTModulok listázása'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id=NULL
		,Modosithato='0'
		,Id='CFB5EAB3-6B51-E911-80D1-00155D020D17'
		,Kod='INTModulList'
		,Nev='INTModulok listázása'
	 WHERE Id=@record_id 
 END
go

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='DF505FC4-8151-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		NULL
		,'0'
		,'DF505FC4-8151-E911-80D1-00155D020D17'
		,'INTModulNew'
		,'INTModul felvitel'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id=NULL
		,Modosithato='0'
		,Id='DF505FC4-8151-E911-80D1-00155D020D17'
		,Kod='INTModulNew'
		,Nev='INTModul felvitel'
	 WHERE Id=@record_id 
 END
go

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='BCAAF6D3-8151-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		NULL
		,'0'
		,'BCAAF6D3-8151-E911-80D1-00155D020D17'
		,'INTModulView'
		,'INTModul megnézés'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id=NULL
		,Modosithato='0'
		,Id='BCAAF6D3-8151-E911-80D1-00155D020D17'
		,Kod='INTModulView'
		,Nev='INTModul megnézés'
	 WHERE Id=@record_id 
 END
go

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='38FBFBE0-8151-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		NULL
		,'0'
		,'38FBFBE0-8151-E911-80D1-00155D020D17'
		,'INTModulModify'
		,'INTModul módosítás'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id=NULL
		,Modosithato='0'
		,Id='38FBFBE0-8151-E911-80D1-00155D020D17'
		,Kod='INTModulModify'
		,Nev='INTModul módosítás'
	 WHERE Id=@record_id 
 END
go

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='93B676F3-8151-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		NULL
		,'0'
		,'93B676F3-8151-E911-80D1-00155D020D17'
		,'INTModulInvalidate'
		,'INTModul törlés'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id=NULL
		,Modosithato='0'
		,Id='93B676F3-8151-E911-80D1-00155D020D17'
		,Kod='INTModulInvalidate'
		,Nev='INTModul törlés'
	 WHERE Id=@record_id 
 END
go

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='CBA72C01-8251-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		NULL
		,'0'
		,'CBA72C01-8251-E911-80D1-00155D020D17'
		,'INTModulRevalidate'
		,'INTModul revalidálás'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id=NULL
		,Modosithato='0'
		,Id='CBA72C01-8251-E911-80D1-00155D020D17'
		,Kod='INTModulRevalidate'
		,Nev='INTModul revalidálás'
	 WHERE Id=@record_id 
 END
go

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='ECA0DC6E-8251-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		NULL
		,'0'
		,'ECA0DC6E-8251-E911-80D1-00155D020D17'
		,'INTModulLock'
		,'INTModul zárolás'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id=NULL
		,Modosithato='0'
		,Id='ECA0DC6E-8251-E911-80D1-00155D020D17'
		,Kod='INTModulLock'
		,Nev='INTModul zárolás'
	 WHERE Id=@record_id 
 END
go

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='6F7EFD8A-8251-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		NULL
		,'0'
		,'6F7EFD8A-8251-E911-80D1-00155D020D17'
		,'INTParameterList'
		,'INTParameter listázás'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id=NULL
		,Modosithato='0'
		,Id='6F7EFD8A-8251-E911-80D1-00155D020D17'
		,Kod='INTParameterList'
		,Nev='INTParameter listázás'
	 WHERE Id=@record_id 
 END
go

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='40F41A97-8251-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		NULL
		,'0'
		,'40F41A97-8251-E911-80D1-00155D020D17'
		,'INTParameterView'
		,'INTParameter megnézés'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id=NULL
		,Modosithato='0'
		,Id='40F41A97-8251-E911-80D1-00155D020D17'
		,Kod='INTParameterView'
		,Nev='INTParameter megnézés'
	 WHERE Id=@record_id 
 END
go

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='F970E39F-8251-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		NULL
		,'0'
		,'F970E39F-8251-E911-80D1-00155D020D17'
		,'INTParameterNew'
		,'INTParameter felvitel'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id=NULL
		,Modosithato='0'
		,Id='F970E39F-8251-E911-80D1-00155D020D17'
		,Kod='INTParameterNew'
		,Nev='INTParameter felvitel'
	 WHERE Id=@record_id 
 END
go

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='19910FAA-8251-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		NULL
		,'0'
		,'19910FAA-8251-E911-80D1-00155D020D17'
		,'INTParameterModify'
		,'INTParameter módosítás'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id=NULL
		,Modosithato='0'
		,Id='19910FAA-8251-E911-80D1-00155D020D17'
		,Kod='INTParameterModify'
		,Nev='INTParameter módosítás'
	 WHERE Id=@record_id 
 END
go

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='8A1B82B5-8251-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		NULL
		,'0'
		,'8A1B82B5-8251-E911-80D1-00155D020D17'
		,'INTParameterInvalidate'
		,'INTParameter törlés'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id=NULL
		,Modosithato='0'
		,Id='8A1B82B5-8251-E911-80D1-00155D020D17'
		,Kod='INTParameterInvalidate'
		,Nev='INTParameter törlés'
	 WHERE Id=@record_id 
 END
go

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='1A260FC0-8251-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		NULL
		,'0'
		,'1A260FC0-8251-E911-80D1-00155D020D17'
		,'INTParameterRevalidate'
		,'INTParameter revalidálás'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id=NULL
		,Modosithato='0'
		,Id='1A260FC0-8251-E911-80D1-00155D020D17'
		,Kod='INTParameterRevalidate'
		,Nev='INTParameter revalidálás'
	 WHERE Id=@record_id 
 END
go


DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='E86398CA-8251-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		NULL
		,'0'
		,'E86398CA-8251-E911-80D1-00155D020D17'
		,'INTLogList'
		,'INTLog listázás'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id=NULL
		,Modosithato='0'
		,Id='E86398CA-8251-E911-80D1-00155D020D17'
		,Kod='INTLogList'
		,Nev='INTLog listázás'
	 WHERE Id=@record_id 
 END
go

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='B41EF2D3-8251-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		NULL
		,'0'
		,'B41EF2D3-8251-E911-80D1-00155D020D17'
		,'INTLogView'
		,'INTLog megnézés'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id=NULL
		,Modosithato='0'
		,Id='B41EF2D3-8251-E911-80D1-00155D020D17'
		,Kod='INTLogView'
		,Nev='INTLog megnézés'
	 WHERE Id=@record_id 
 END
go

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Modulok
			where Id='EC7AFC7D-7351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Modulok(
			 Tranz_id
			,Stat_id
			,Id
			,Tipus
			,Kod
			,Nev
			,Leiras
			) values (
			 'f60ad910-541a-470d-b888-da5a6a061668'
			,'a0848405-e664-4e79-8fab-cfeca4a290af'
			,'EC7AFC7D-7351-E911-80D1-00155D020D17'
			,'F'
			,'INTModulokList.aspx'
			,'INTModulok listája'
			,null
			); 
	 END 	
	 go
	 
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Menuk
			where Id='FC23EBB4-7351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Menuk(
			 Menu_Id_Szulo
			,Tranz_id
			,Stat_id
			,Id
			,Kod
			,Nev
			,Funkcio_Id
			,Modul_Id
			,Parameter
			,Sorrend
			) values (
			 'AA1421B9-0498-4CA3-B075-60F699CB1B7C'
			,null
			,null
			,'FC23EBB4-7351-E911-80D1-00155D020D17'
			,'eAdmin'
			,'eIntegrator modulok'
			,'CFB5EAB3-6B51-E911-80D1-00155D020D17'
			,'EC7AFC7D-7351-E911-80D1-00155D020D17'		
			,'NULL'
			,'55'
			); 
	 END 
	 
Go 
	 
	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='473DA428-7651-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'E855F681-36ED-41B6-8413-576FCB5D1542'
			,'473DA428-7651-E911-80D1-00155D020D17'
			,'CFB5EAB3-6B51-E911-80D1-00155D020D17'
			);
END 	

GO

	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='1F79D33E-8351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'E855F681-36ED-41B6-8413-576FCB5D1542'
			,'1F79D33E-8351-E911-80D1-00155D020D17'
			,'E86398CA-8251-E911-80D1-00155D020D17'
			);
END 	

GO
	 
	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='9D491746-8351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'E855F681-36ED-41B6-8413-576FCB5D1542'
			,'9D491746-8351-E911-80D1-00155D020D17'
			,'B41EF2D3-8251-E911-80D1-00155D020D17'
			);
END 	

GO

	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='9999964D-8351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'E855F681-36ED-41B6-8413-576FCB5D1542'
			,'9999964D-8351-E911-80D1-00155D020D17'
			,'DF505FC4-8151-E911-80D1-00155D020D17'
			);
END 	

GO

	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='DB2CCE57-8351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'E855F681-36ED-41B6-8413-576FCB5D1542'
			,'DB2CCE57-8351-E911-80D1-00155D020D17'
			,'BCAAF6D3-8151-E911-80D1-00155D020D17'
			);
END 	

GO
	
	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='6AA9FF5D-8351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'E855F681-36ED-41B6-8413-576FCB5D1542'
			,'6AA9FF5D-8351-E911-80D1-00155D020D17'
			,'38FBFBE0-8151-E911-80D1-00155D020D17'
			);
END 	

GO	

	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='6FB77A67-8351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'E855F681-36ED-41B6-8413-576FCB5D1542'
			,'6FB77A67-8351-E911-80D1-00155D020D17'
			,'CBA72C01-8251-E911-80D1-00155D020D17'
			);
END 	

GO	

	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='30D8586E-8351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'E855F681-36ED-41B6-8413-576FCB5D1542'
			,'30D8586E-8351-E911-80D1-00155D020D17'
			,'93B676F3-8151-E911-80D1-00155D020D17'
			);
END 	

GO

	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='A9903F75-8351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'E855F681-36ED-41B6-8413-576FCB5D1542'
			,'A9903F75-8351-E911-80D1-00155D020D17'
			,'ECA0DC6E-8251-E911-80D1-00155D020D17'
			);
END 	

GO
	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='CBE2CB81-8351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'E855F681-36ED-41B6-8413-576FCB5D1542'
			,'CBE2CB81-8351-E911-80D1-00155D020D17'
			,'F970E39F-8251-E911-80D1-00155D020D17'
			);
END 	

GO

	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='F86F7689-8351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'E855F681-36ED-41B6-8413-576FCB5D1542'
			,'F86F7689-8351-E911-80D1-00155D020D17'
			,'6F7EFD8A-8251-E911-80D1-00155D020D17'
			);
END 	

GO

	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='D6B2A590-8351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'E855F681-36ED-41B6-8413-576FCB5D1542'
			,'D6B2A590-8351-E911-80D1-00155D020D17'
			,'40F41A97-8251-E911-80D1-00155D020D17'
			);
END 	

GO

	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='823A4797-8351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'E855F681-36ED-41B6-8413-576FCB5D1542'
			,'823A4797-8351-E911-80D1-00155D020D17'
			,'19910FAA-8251-E911-80D1-00155D020D17'
			);
END 	

GO

	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='4C0DF09D-8351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'E855F681-36ED-41B6-8413-576FCB5D1542'
			,'4C0DF09D-8351-E911-80D1-00155D020D17'
			,'1A260FC0-8251-E911-80D1-00155D020D17'
			);
END 	

GO


	 	 DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='C8917BA4-8351-E911-80D1-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'E855F681-36ED-41B6-8413-576FCB5D1542'
			,'C8917BA4-8351-E911-80D1-00155D020D17'
			,'8A1B82B5-8251-E911-80D1-00155D020D17'
			);
END 	

GO
 -------------- end of BUG 2601
 
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------
 -- BUG_6209
-- ELINTEZETTE_NYILV_KIADM_FELTETELE rendszerparameter	 

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='76749558-243F-E911-80CF-00155D020B58') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'ELINTEZETTE_NYILV_KIADM_FELTETELE' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'76749558-243F-E911-80CF-00155D020B58'
		,'ELINTEZETTE_NYILV_KIADM_FELTETELE'
		,'0'
		,'1'
		,'Kiadmányozás aláírásnak feltétele-e az irat Elintézett státusza. 1-Igen, 0-Nem'
		); 
END 
--ELSE 
--BEGIN 	
--	Print 'UPDATE ELINTEZETTE_NYILV_KIADM_FELTETELE'
--	 UPDATE KRT_Parameterek
--	 SET 
--		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
--		,Id='76749558-243F-E911-80CF-00155D020B58'
--		,Nev='ELINTEZETTE_NYILV_KIADM_FELTETELE'
--		,Ertek='0'
--		,Karbantarthato='1'
--		,Note='Kiadmányozás aláírásnak feltétele-e az irat Elintézett státusza. 1-Igen, 0-Nem'
--	 WHERE Id='76749558-243F-E911-80CF-00155D020B58'
	
--END
--IF (@org_kod = 'NMHH')  
--BEGIN
--	Print 'UPDATE ELINTEZETTE_NYILV_KIADM_FELTETELE: NMHH = 1'
--	 UPDATE KRT_Parameterek
--	 SET 
--		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
--		,Id='76749558-243F-E911-80CF-00155D020B58'
--		,Nev='ELINTEZETTE_NYILV_KIADM_FELTETELE'
--		,Ertek='1'
--		,Karbantarthato='1'
--		,Note='Kiadmányozás aláírásnak feltétele-e az irat Elintézett státusza. 1-Igen, 0-Nem'
--	 WHERE Id='76749558-243F-E911-80CF-00155D020B58'

--END
GO
---------------------------------------------------- BUG_6209 end --------------------------------------------------------------------------------------------------------------


 ------------------------------------------------------------------------------------------------------------------------------------------------------------------
 -- BUG_4731
-- BEKULDO_PARTNER_CIM_NEM_TORZSBOL_ENABLED rendszerparameter beállítás	 

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='86F567CD-62FE-40DB-8941-9DA60082E9D4') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'BEKULDO_PARTNER_CIM_NEM_TORZSBOL_ENABLED' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'86F567CD-62FE-40DB-8941-9DA60082E9D4'
		,'BEKULDO_PARTNER_CIM_NEM_TORZSBOL_ENABLED'
		,'1'
		,'1'
		,'Az érkeztető képernyőn a partner és cím szabad bevitelét engedélyezi/tiltja. Érték: 1-Igen (tetszőleges partnernév és cím bevihető) 0-Nem (beküldő partner és cím csak a törzsből választható)'
		); 
END 
--ELSE 
--BEGIN 	
--	Print 'UPDATE BEKULDO_PARTNER_CIM_NEM_TORZSBOL_ENABLED'
--	 UPDATE KRT_Parameterek
--	 SET 
--		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
--		,Id='86F567CD-62FE-40DB-8941-9DA60082E9D4'
--		,Nev='BEKULDO_PARTNER_CIM_NEM_TORZSBOL_ENABLED'
--		,Ertek='1'
--		,Karbantarthato='1'
--		,Note='Az érkeztető képernyőn a partner és cím szabad bevitelét engedélyezi/tiltja. Érték: 1-Igen (tetszőleges partnernév és cím bevihető) 0-Nem (beküldő partner és cím csak a törzsből választható)'
--	 WHERE Id='86F567CD-62FE-40DB-8941-9DA60082E9D4'
	
--END

 --BUG_4731
-- Korábbi Számlák Partner-és címtextjeinek töltése

 update EREC_Szamlak
set nevstr_szallito = KRT_Partnerek.Nev
from erec_szamlak 
inner join KRT_Partnerek on KRT_Partnerek.id = EREC_Szamlak.Partner_Id_Szallito and EREC_Szamlak.LetrehozasIdo between KRT_Partnerek.ErvKezd and KRT_Partnerek.ErvVege
where NevSTR_Szallito is null and Partner_Id_Szallito is not null

 update EREC_Szamlak
set CimSTR_Szallito = KRT_Cimek.Nev
from erec_szamlak 
inner join KRT_Cimek on KRT_Cimek.id = EREC_Szamlak.Cim_Id_Szallito and EREC_Szamlak.LetrehozasIdo between KRT_Cimek.ErvKezd and KRT_Cimek.ErvVege
where CimSTR_Szallito is null and Cim_Id_Szallito is not null
 GO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------
 -- BLG_591
-- TERTIVONALKOD_KELL rendszerparameter	 

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='1801CC9A-C440-E911-80CF-00155D020B58') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'TERTIVONALKOD_KELL' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'1801CC9A-C440-E911-80CF-00155D020B58'
		,'TERTIVONALKOD_KELL'
		,'0'
		,'1'
		,'Postázáskor a tértivevény vonalkód a felületen'
		); 
END 
--ELSE 
--BEGIN 	
--	Print 'UPDATE TERTIVONALKOD_KELL'
--	 UPDATE KRT_Parameterek
--	 SET 
--		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
--		,Id='1801CC9A-C440-E911-80CF-00155D020B58'
--		,Nev='TERTIVONALKOD_KELL'
--		,Ertek='0'
--		,Karbantarthato='1'
--		,Note='Postázáskor a tértivevény vonalkód a felületen'
--	 WHERE Id='1801CC9A-C440-E911-80CF-00155D020B58'
	
--END
--IF (@org_kod = 'FPH')  
--BEGIN
--	Print 'UPDATE TERTIVONALKOD_KELL: FPH = 1'
--	 UPDATE KRT_Parameterek
--	 SET 
--		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
--		,Id='1801CC9A-C440-E911-80CF-00155D020B58'
--		,Nev='TERTIVONALKOD_KELL'
--		,Ertek='1'
--		,Karbantarthato='1'
--		,Note='Postázáskor a tértivevény vonalkód a felületen'
--	 WHERE Id='1801CC9A-C440-E911-80CF-00155D020B58'

--END
GO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------
 ------------------------------------------------ UGYIRATLEZARAS_POSTAZOTT_IRATPELDANY_ENABLED --------------------------------------------------------------------------------------

DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = '6255FDB9-29AE-4720-B83A-EB06D03EC2AA'

IF (@org_kod = 'NMHH' or @org_kod = 'BOPMH')
set @ertek = '1'
else
set @ertek = '0'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - UGYIRATLEZARAS_POSTAZOTT_IRATPELDANY_ENABLED - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'UGYIRATLEZARAS_POSTAZOTT_IRATPELDANY_ENABLED'
		,@ertek
		,'1'
		,'1 = akkor lehet ügyiratot lezárni vagy elintézetté nyilvánítani ha nincs postázás alatti iratpéldány, 0 = nem kerül figyelésre, hogy az iratpéldány postázás alatti'
		); 
 END

 GO

 ------------------------------------------------ UGYIRATLEZARAS_POSTAZOTT_IRATPELDANY_ENABLED -------------------------------------------------------------------------------------

 -------------------------------------------------------------------------------------------------------------------------------------------
 ---------- 'Államigazgatási hatósági ügy', 'Önkormányzati hatósági ügy' Egyeb mező beállítása 'HATOSAGI' -ra, ahol még nincs ('UGY_FAJTAJA' kódcsoportban) (BUG#7171) -------

UPDATE KRT_Kodtarak
SET Egyeb = 'HATOSAGI'
WHERE KodCsoport_Id = '72554FDB-8323-4110-B7B2-65576AE3204A'	
	and Nev in ('Államigazgatási hatósági ügy', 'Önkormányzati hatósági ügy') -- Azért nem fix Id-val van, mert valamelyik környezeten kézzel lett felvéve, más Id-val
	and isnull(Egyeb, '') = ''

 GO

 ------------------------------------------------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------IRATTAROZAS_ATMENETI_AZONOS_KEZELES---------------------------------------------------------------------
DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = '4A61D574-762A-4E6D-BCEB-19F86445BBB5'

IF @org_kod = 'NMHH'
set @ertek = '1'
else
set @ertek = '0'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - IRATTAROZAS_ATMENETI_AZONOS_KEZELES - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'IRATTAROZAS_ATMENETI_AZONOS_KEZELES'
		,@ertek
		,'1'
		,'1 érték  - az átmeneti irattárból történő kikérés paraméterezése megegyezik a központi irattárból történő kölcsönzés beállításaival, 0 érték  - az átmeneti irattárból történő kikéréshez nem kell vezetői jóváhagyás'
		); 
 END

 GO

-----------------------------------------------------------IRATTAROZAS_ATMENETI_AZONOS_KEZELES---------------------------------------------------------------------

-----------------------------------------------------------AtmenetiIrattarKikeresJovahagyas------------------------------------------------------------------------

DECLARE @record_id uniqueidentifier
SET @record_id = 'D6EC5D67-9127-4195-A779-D2A4989A7013'

IF NOT EXISTS (select 1 from KRT_Funkciok where Id = @record_id)
BEGIN
	print 'insert KRT_Funkciok - AtmenetiIrattarKikeresJovahagyas'
	insert into KRT_Funkciok
	(Id, Kod, Nev, 
	ObjTipus_Id_AdatElem, Alkalmazas_Id, Muvelet_Id, 
	Modosithato, MunkanaploJelzo, FeladatJelzo, KeziFeladatJelzo)
	values
	(@record_id, 'AtmenetiIrattarKikeresJovahagyas', 'Ámeneti irattárban lévő ügyiratok kikérésének jóváhagyása', 
	'A04417A6-54AC-4AF1-9B63-5BABF0203D42', '272277BB-27B7-4689-BE45-DB5994A0FBA9','52C89273-669A-4995-8DD4-C86DF815525D', 
	'0', '0', '1','1')

	-- szerepkör-funckió összerendelés
	insert into KRT_Szerepkor_Funkcio
	(Funkcio_Id, Szerepkor_Id)
	select @record_id, Szerepkor_Id
	from KRT_Szerepkor_Funkcio szf
	where szf.Funkcio_Id = (select f.id from KRT_Funkciok f where f.Kod = 'KolcsonzesJovahagyas')
	and szf.Szerepkor_Id not in (select szf2.Szerepkor_Id from KRT_Szerepkor_Funkcio szf2 where szf2.Funkcio_Id = @record_id)
	and GETDATE() between szf.ErvKezd and szf.ErvVege
END

GO
-----------------------------------------------------------AtmenetiIrattarKikeresJovahagyas------------------------------------------------------------------------

-----------------------------------------------------------IRATTAROZAS_JOVAHAGYO_UGYFELELOS ---------------------------------------------------------------------
DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = 'FF91AE58-1357-4451-A477-2CEE162D8FD3'

IF @org_kod = 'NMHH'
set @ertek = '1'
else
set @ertek = '0'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - IRATTAROZAS_JOVAHAGYO_UGYFELELOS  - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'IRATTAROZAS_JOVAHAGYO_UGYFELELOS'
		,@ertek
		,'1'
		,'1 - Az átmeneti irattárból történő kikérést és központi irattárból történő kölcsönzést a kikérendő ügyirat ügyfelelős szervezeti egység vezetőjének kell jóváhagynia'
		); 
 END

 GO

-----------------------------------------------------------IRATTAROZAS_JOVAHAGYO_UGYFELELOS---------------------------------------------------------------------

-----------------7203--------------------------

GO
 
 DECLARE @ORG UNIQUEIDENTIFIER
SET @ORG = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @KodCsoport_Id_SYNC uniqueidentifier
SET @KodCsoport_Id_SYNC = 'B306DE82-2B4B-E911-80D0-00155D020B57'

IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = @KodCsoport_Id_SYNC)
	BEGIN 		
		insert into KRT_KodCsoportok(
		 BesorolasiSema
		,Id
		,Kod
		,Nev
		,Modosithato
		,Note
		) values (
		'0'
		,@KodCsoport_Id_SYNC
		,'ASPADO_DocumentType'
		,'Az ASP.ADO által küldött irattípusok. A saját "IRATTIPUS" kódtárunkhoz való hozzárendeléshez szükséges függő kódtár által.'
		,'1'
		,NULL
		); 
	END
GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '121B5FEA-2B4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'hatarozat'
	,'hatarozat'
	,'1'	
	,NULL
	); 
END 

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '325FF7F1-2B4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'vegzes'
	,'vegzes'
	,'1'	
	,NULL
	); 
END 

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'C37D2AF8-2B4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'jegyzokonyv'
	,'jegyzokonyv'
	,'1'	
	,NULL
	); 
END 

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '4EDBBF01-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'hivatalos_feljegyzes'
	,'hivatalos_feljegyzes'
	,'1'	
	,NULL
	); 
END 

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'E42F930B-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'hianypotlas'
	,'hianypotlas'
	,'1'	
	,NULL
	); 
END 


GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '5E24B013-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'felhivas'
	,'felhivas'
	,'1'	
	,NULL
	); 
END 


GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '4456112E-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'ertesites'
	,'ertesites'
	,'1'	
	,NULL
	); 
END 

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'B4D7F140-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'idezes'
	,'idezes'
	,'1'	
	,NULL
	); 
END 

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'E1DE4C4C-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'egyeb'
	,'egyeb'
	,'0'	
	,NULL
	); 
END 

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '2BEA5057-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'hatszer'
	,'hatszer'
	,'0'	
	,NULL
	); 
END 

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'E98ACD60-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'fellebbezes'
	,'fellebbezes'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '18238B6A-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'eredeti'
	,'eredeti'
	,'0'	
	,NULL
	); 
END 

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '24249C7E-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'kerelem_modositas'
	,'kerelem_modositas'
	,'0'	
	,NULL
	); 
END  

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '19A28A87-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'szamla'
	,'szamla'
	,'0'	
	,NULL
	); 
END  

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '62F62B91-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'megrendelo'
	,'megrendelo'
	,'0'	
	,NULL
	); 
END 

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'AD62079B-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'koztartozas'
	,'koztartozas'
	,'0'	
	,NULL
	); 
END 

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '754A02A4-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'vegrehajtas_inditas'
	,'vegrehajtas_inditas'
	,'0'	
	,NULL
	); 
END  

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '81BD6CAF-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'vegrehajtas_lefolytatas'
	,'vegrehajtas_lefolytatas'
	,'0'	
	,NULL
	); 
END  

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '542A2EB8-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'korlevel'
	,'korlevel'
	,'0'	
	,NULL
	); 
END  

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '78BEE3C0-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'konnyites'
	,'konnyites'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'C6FA8FCA-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'konyveles'
	,'konyveles'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'F0B9AEDC-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'ertesito'
	,'ertesito'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'F03663E6-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'specialis'
	,'specialis'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'E32994F7-2C4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'adatszolgaltatas'
	,'adatszolgaltatas'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'FFEE6502-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'kerelem'
	,'kerelem'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '06A75D0B-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'nyilatkozat'
	,'nyilatkozat'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'BBABAD18-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'adobevallas'
	,'adobevallas'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'CD78FD28-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'bejelentkezes'
	,'bejelentkezes'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '27BA4E30-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'adatbejelentes'
	,'adatbejelentes'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '7C02B536-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'valtozasbejelentes'
	,'valtozasbejelentes'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '60670040-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'eszrevetel'
	,'eszrevetel'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '1ED8864F-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'kifogas'
	,'kifogas'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '035FF358-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'adatkeres'
	,'adatkeres'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '065FF358-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'megkereses'
	,'megkereses'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '8FADB966-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'keresetlevel'
	,'keresetlevel'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'DE46B779-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'meghatalmazas'
	,'meghatalmazas'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '59FC2B82-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'feljegyzes'
	,'feljegyzes'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'AFAD2E8C-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'itelet'
	,'itelet'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'F1A4A795-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'hianypotlasifelhivas'
	,'hianypotlasifelhivas'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '09EF0E9F-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'adoigazolas'
	,'adoigazolas'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '90D60BA8-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'hatosagibizonyitvany'
	,'hatosagibizonyitvany'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'D83727B4-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'megbizolevel'
	,'megbizolevel'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'E76EEABD-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'felterjesztes'
	,'felterjesztes'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '2B12C4C5-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'vedirat'
	,'vedirat'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = 'F32CC2EF-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'adategyeztetes'
	,'adategyeztetes'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = 'B306DE82-2B4B-E911-80D0-00155D020B57'
declare @recordId uniqueidentifier
set @recordId = '5751EFF7-2D4B-E911-80D0-00155D020B57'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'tajekoztatas'
	,'tajekoztatas'
	,'0'	
	,NULL
	); 
END

GO

 IF NOT EXISTS (SELECT 1 FROM KRT_KODTARFUGGOSEG WHERE ID='A545131A-75FF-4A81-9AB0-5899EB44D157')
	BEGIN		
		INSERT [dbo].[KRT_KodtarFuggoseg] ([Id], [Org], [Vezerlo_KodCsoport_Id], [Fuggo_KodCsoport_Id], [Adat], [Aktiv]) 
		VALUES (N'A545131A-75FF-4A81-9AB0-5899EB44D157', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'B306DE82-2B4B-E911-80D0-00155D020B57', N'272DC3FD-4A8D-4EA2-88ED-C795A55D0373', N'{"VezerloKodCsoportId":"b306de82-2b4b-e911-80d0-00155d020b57","FuggoKodCsoportId":"272dc3fd-4a8d-4ea2-88ed-c795a55d0373","Items":[],"ItemsNincs":[]}', N'1')
	END


GO

GO
 
 DECLARE @ORG UNIQUEIDENTIFIER
SET @ORG = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @KodCsoport_Id_SYNC uniqueidentifier
SET @KodCsoport_Id_SYNC = '824CD0FC-0D50-E911-80D1-00155D020D17'

IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = @KodCsoport_Id_SYNC)
	BEGIN 		
		insert into KRT_KodCsoportok(
		 BesorolasiSema
		,Id
		,Kod
		,Nev
		,Modosithato
		,Note
		) values (
		'0'
		,@KodCsoport_Id_SYNC
		,'ASPADO_DefaultExpedialasCode'
		,'Az ASP.ADO által küldött expediálás kódok. A saját "KULDEMENY_KULDES_MODJA" kódtárunkhoz való hozzárendeléshez szükséges függő kódtár által.'
		,'1'
		,NULL
		); 
	END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '824CD0FC-0D50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = '75D0A7E3-A65A-E911-93DD-0050569A6FD5'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'001'
	,'Postai'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '824CD0FC-0D50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = '78A797EA-A65A-E911-93DD-0050569A6FD5'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'002'
	,'Külön kézbesítés'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '824CD0FC-0D50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = '15E33FF7-A65A-E911-93DD-0050569A6FD5'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'008'
	,'Elektronikus'
	,'0'	
	,NULL
	); 
END

GO

 IF NOT EXISTS (SELECT 1 FROM KRT_KODTARFUGGOSEG WHERE ID='44406271-57D5-4189-A864-E24E47392F2A')
	BEGIN		
		INSERT [dbo].[KRT_KodtarFuggoseg] ([Id], [Org], [Vezerlo_KodCsoport_Id], [Fuggo_KodCsoport_Id], [Adat], [Aktiv]) 
		VALUES (N'44406271-57D5-4189-A864-E24E47392F2A', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'824CD0FC-0D50-E911-80D1-00155D020D17', N'6982F668-C8CD-47A3-AC62-9F16E439473C', N'{"VezerloKodCsoportId":"824cd0fc-0d50-e911-80d1-00155d020d17","FuggoKodCsoportId":"6982f668-c8cd-47a3-ac62-9f16e439473c","Items":[],"ItemsNincs":[]}', N'1')
	END

GO

GO
 
 DECLARE @ORG UNIQUEIDENTIFIER
SET @ORG = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @KodCsoport_Id_SYNC uniqueidentifier
SET @KodCsoport_Id_SYNC = '2B5C2F2C-6F50-E911-80D1-00155D020D17'

IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = @KodCsoport_Id_SYNC)
	BEGIN 		
		insert into KRT_KodCsoportok(
		 BesorolasiSema
		,Id
		,Kod
		,Nev
		,Modosithato
		,Note
		) values (
		'0'
		,@KodCsoport_Id_SYNC
		,'ASPADO_TaxType'
		,'Az ASP.ADO által küldött taxtype kódok.'
		,'1'
		,NULL
		); 
	END

GO


GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = 'B715E881-6F50-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code70'
	,'A103_Code70'
	,'0'	
	,NULL
	); 
END


GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = 'E215901E-7656-4154-8AE0-13318E4616B8'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code42B'
	,'A103_Code42B'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = 'F2EB875D-7050-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code96'
	,'A103_Code96'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = '359E8881-7050-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code92'
	,'A103_Code92'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = '47998AA9-7050-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A101_Code96'
	,'A101_Code96'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = 'C91FB9BF-7050-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A107_Code97'
	,'A107_Code97'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = 'E7D718D2-7050-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Codemeghat'
	,'A103_Codemeghat'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = '08FB07E5-7050-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A106_Code88'
	,'A106_Code88'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = '5C2C46FC-7050-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code87N'
	,'A103_Code87N'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = 'A98CB209-7150-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code41'
	,'A103_Code41'
	,'0'	
	,NULL
	); 
END


GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = '5B63351D-7150-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code50'
	,'A103_Code50'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = '214F9D70-7150-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code87'
	,'A103_Code87'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = 'FE7DBA89-7150-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code33'
	,'A103_Code33'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = '0742DC9F-7150-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code51'
	,'A103_Code51'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = '123B55C9-7150-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code35'
	,'A103_Code35'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = '625F7BD7-7150-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code31'
	,'A103_Code31'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = 'D73830ED-7150-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code37'
	,'A103_Code37'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = 'E81385FD-7150-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code57'
	,'A103_Code57'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = 'CA0D740E-7250-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code60'
	,'A103_Code60'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = '39002620-7250-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code32'
	,'A103_Code32'
	,'0'	
	,NULL
	); 
END

GO

declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '2B5C2F2C-6F50-E911-80D1-00155D020D17'
declare @recordId uniqueidentifier
set @recordId = '4E4D9730-7250-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'A103_Code200'
	,'A103_Code200'
	,'0'	
	,NULL
	); 
END


GO
 
 DECLARE @ORG UNIQUEIDENTIFIER
SET @ORG = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @KodCsoport_Id_SYNC uniqueidentifier
SET @KodCsoport_Id_SYNC = '7FA4445D-7550-E911-80D1-00155D020D17'

IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = @KodCsoport_Id_SYNC)
	BEGIN 		
		insert into KRT_KodCsoportok(
		 BesorolasiSema
		,Id
		,Kod
		,Nev
		,Modosithato
		,Note
		) values (
		'0'
		,@KodCsoport_Id_SYNC
		,'ASPADO_UgyTipus'
		,'Az ASP.ADO által küldött taxtype kódokhoz tartozó saját kódjaink.'
		,'1'
		,NULL
		); 
	END

GO

  declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '7FA4445D-7550-E911-80D1-00155D020D17'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) select  
	 NEWID()  
	,@Org
	,@kodcsoportId
	,o.kod
	,o.kod
	,'0'	
	,NULL
	from 
	(
 select distinct m.UgykorKod COLLATE DATABASE_DEFAULT + '_' + m.Ugytipus COLLATE DATABASE_DEFAULT as kod
  from EREC_IratMetaDefinicio m  
  where m.UgykorKod in ('A101','A103','A106','A107')
and m.Ugytipus is not null
and not exists (select 1 from KRT_KodTarak k where k.Kod COLLATE DATABASE_DEFAULT=m.UgykorKod COLLATE DATABASE_DEFAULT + '_' + m.Ugytipus COLLATE DATABASE_DEFAULT)
) as o

GO 


 IF NOT EXISTS (SELECT 1 FROM KRT_KODTARFUGGOSEG WHERE ID='E79DBC84-D525-4089-A80A-9057B0C20F64')
	BEGIN		
		INSERT [dbo].[KRT_KodtarFuggoseg] ([Id], [Org], [Vezerlo_KodCsoport_Id], [Fuggo_KodCsoport_Id], [Adat], [Aktiv]) 
		VALUES (N'E79DBC84-D525-4089-A80A-9057B0C20F64', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'2B5C2F2C-6F50-E911-80D1-00155D020D17', N'7FA4445D-7550-E911-80D1-00155D020D17', N'{"VezerloKodCsoportId":"2b5c2f2c-6f50-e911-80d1-00155d020d17","FuggoKodCsoportId":"7fa4445d-7550-e911-80d1-00155d020d17","Items":[],"ItemsNincs":[]}', N'1')
	END

GO

-----------------end of 7203-------------------


-- BUG 6878 - SZURSetSakkoraStatusWS Okkod megjelenítés hiba
UPDATE [KRT_KodCsoportok]
SET Modosithato = 1, Nev = 'Eljárás lezárásának oka'
WHERE Id = 'BD00D394-249C-4582-B03E-1CF5B6A1EFE0'

UPDATE [KRT_KodCsoportok]
SET Modosithato = 1
WHERE Id = 'FDC8D0D4-B6B6-4900-4900-868686868686'
-- BUG 6878 - SZURSetSakkoraStatusWS Okkod megjelenítés hiba
----END----

GO

-- BLG_7742 - FPH-nál paraméterezetten tűnjenek el az ügyintézéshez kapcsolódó oszlopok a listáról
-- értéke alapértelmezetten 1, ekkor a jelenlegi megoldás szerint bármikor lezárható egy ügyirat.
-- UGYINTEZES_OSZLOPOK_ELREJTESE rendszerparameter     
-- ertek: 0,1
-- Default: 1 (FPH)
-- 1: Ne jelenjenek meg az alábbi oszlopok:
-- Minden ügyiratok listája alapú listában: Ügyintézési határidő, Eltelt ügyintézési idő, Sakkóra, IH, Eljárási fok, Hátralevő napok, Hátralevő munkanapok
-- Iratok listájában: Irat hatása, IH, Hátralevő napok, Hátralevő munkanapok, Eljárási fok

print 'UGYINTEZES_OSZLOPOK_ELREJTESE rendszerparameter'

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='941F0AAD-3D3F-458F-B2D5-DFEC954E10DE') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'941F0AAD-3D3F-458F-B2D5-DFEC954E10DE'
		,'UGYINTEZES_OSZLOPOK_ELREJTESE'
		,'0'
		,'1'
		,'Ügyintézéshez kapcsolódó oszlopok elrejtése a listáról.'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='941F0AAD-3D3F-458F-B2D5-DFEC954E10DE'
	--	,Nev='UGYINTEZES_OSZLOPOK_ELREJTESE'
	--	,Ertek='0'
	--	,Karbantarthato='1'
	--	,Note='Ügyintézéshez kapcsolódó oszlopok elrejtése a listáról.'
	-- WHERE Id=@record_id 
 --END
 
--if @org_kod = 'FPH' 
--BEGIN 
--	Print @org_kod +': UGYINTEZES_OSZLOPOK_ELREJTESE = 1'
--	UPDATE KRT_Parameterek
--	   SET Ertek='1'
--	   WHERE Id='941F0AAD-3D3F-458F-B2D5-DFEC954E10DE'
--END

GO
-------------- end of BLG_7742

------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- BUG 5085 - Ügyirat lezárás
-- értéke alapértelmezetten 1, ekkor a jelenlegi megoldás szerint bármikor lezárható egy ügyirat.
-- LEZARAS_ELINTEZES_NELKUL_ENABLED rendszerparameter     
-- ertek: 0,1
-- Default: 1
-- 1 - bármikor lezárható egy ügyirat, 0 - az ügyirat akkor zárható csak le, ha:
-- 1. az ügyirat állapota Elintézett (az iratokat nem kell figyelni, mert az az ügyirat elintézésének amúgy is feltétele)
-- 2. ügyirat sakkóra az alábbi értékeket veszi fel: 3, 7, 8, 10 

print 'LEZARAS_ELINTEZES_NELKUL_ENABLED rendszerparameter'

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='F30C0BC5-3B3F-464C-8D5C-C71EF3B0A811') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'F30C0BC5-3B3F-464C-8D5C-C71EF3B0A811'
		,'LEZARAS_ELINTEZES_NELKUL_ENABLED'
		,'1'
		,'1'
		,'Lezárás elintézés nélkül.'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='F30C0BC5-3B3F-464C-8D5C-C71EF3B0A811'
	--	,Nev='LEZARAS_ELINTEZES_NELKUL_ENABLED'
	--	,Ertek='1'
	--	,Karbantarthato='1'
	--	,Note='Lezárás elintézés nélkül.'
	-- WHERE Id=@record_id 
 --END
 
--if @org_kod = 'NMHH' 
--BEGIN 
--	Print @org_kod +': LEZARAS_ELINTEZES_NELKUL_ENABLED = 0'
--	UPDATE KRT_Parameterek
--	   SET Ertek='0'
--	   WHERE Id='F30C0BC5-3B3F-464C-8D5C-C71EF3B0A811'
--END

 GO
 -------------- end of BUG 5085
 --BUG 4755
 ------Kimenő elektronikus irat
DECLARE @KodcsoportId uniqueidentifier
DECLARE @Org uniqueidentifier
DECLARE @kodtarId uniqueidentifier
DECLARE @AlairasFajtaNev varchar(50)
DECLARE @org_kod NVARCHAR(100) 
SET @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
SET @org_kod = (select kod from KRT_Orgok where id=@org) 

set @kodcsoportId = 'FAB03917-45E4-49B3-8547-0E29446CB8A4'

  IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @kodcsoportId)
  BEGIN
     print 'Insert KRT_KodCsoportok - ALAIRAS_FAJTA'

       insert into KRT_KodCsoportok
    (
       Id
      ,Kod
      ,Nev
      ,Modosithato
      ,Note
      ,BesorolasiSema
    ) 
    values
    (
      @kodcsoportId
      ,'ALAIRAS_FAJTA'
      ,'Lehetséges aláírás fajták'
      ,'1'
      , null
      ,'0'
    )

  END
  
-- Kártyás aláírás
set @kodtarId = '40FF98D8-9710-4DC4-89FA-718046FD6956'
--BUG 7471 - NMHH esetében hívjuk Elektronikus aláírásnak
set @AlairasFajtaNev = 'Kártyás aláírás';
 if @org_kod = 'NMHH'
 BEGIN
	set @AlairasFajtaNev = 'Elektronikus aláírás'
 END

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - ALAIRAS_FAJTA.1' 
     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '1',
      @AlairasFajtaNev, 
      '1',
      '0'
    )
  END	
  ELSE
  BEGIN
	Print 'UPDATE KRT_Kodtarak - ALAIRAS_FAJTA.1' 
	update KRT_KodTarak set Nev = @AlairasFajtaNev where Id = @kodtarId
  END

  -- Központi aláírás
set @kodtarId = 'B1DD099E-9DE1-4875-A822-77BB96A08866'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - ALAIRAS_FAJTA.2' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '2',
      'Központi aláírás',
      '2',
      '0'
    )
  END

  -- Manuális aláírás
set @kodtarId = '7966F4D9-E79F-46AF-B5A3-999EA5E3C3FA'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - ALAIRAS_FAJTA.3' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '3',
      'Manuális aláírás',
      '3',
      '0'
    )
  END

  -- KR kiküldés jóváhagyása
set @kodtarId = '0ACC4986-A663-4781-BDC4-951991DA1827'

  IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - ALAIRAS_FAJTA.4' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '4',
      'KR kiküldés jóváhagyása',
      '4',
      '0'
    )
  END

  --függő kódtár létrehozása
  --BUG 7471 - UGYINTEZES_ALAPJA legyen a vezérlő kódtár!
if(exists(select Id from KRT_KodCsoportok where Kod = 'UGYINTEZES_ALAPJA'))
BEGIN
	DECLARE @Vezerlo_Kodcsoport_id uniqueidentifier
	DECLARE @Fuggoseg_Id uniqueidentifier = '2A37FBA6-A3C9-4ED8-8C89-A0969E38DE9B'
	SET @Vezerlo_Kodcsoport_id = (select Id from KRT_KodCsoportok where Kod = 'UGYINTEZES_ALAPJA')
	if (@org_kod = 'NMHH')
	BEGIN
		if (not exists(select Id from KRT_KodtarFuggoseg where Id = @Fuggoseg_Id))
		BEGIN
			print 'UGYINTEZES_ALAPJA - ALAIRAS_FAJTA kódtárfüggőség létrehozása...'
			
			INSERT INTO KRT_KodtarFuggoseg (Id, Org, Vezerlo_KodCsoport_Id, Fuggo_KodCsoport_Id, Adat, Aktiv)
				VALUES (@Fuggoseg_Id, @org, @Vezerlo_Kodcsoport_id, @kodcsoportId
						, '{"VezerloKodCsoportId":"'+cast(@Vezerlo_Kodcsoport_id AS NVARCHAR(36))+'","FuggoKodCsoportId":"'+cast(@kodcsoportId AS NVARCHAR(36))+'","Items":[{"VezerloKodTarId":"82F2FAFC-A312-4C13-A1FF-E86DB76B131F","FuggoKodtarId":"40FF98D8-9710-4DC4-89FA-718046FD6956","Aktiv":true},{"VezerloKodTarId":"82F2FAFC-A312-4C13-A1FF-E86DB76B131F","FuggoKodtarId":"0ACC4986-A663-4781-BDC4-951991DA1827","Aktiv":true},{"VezerloKodTarId":"9DDC5255-0AC3-4FFF-AC94-E8089682B020","FuggoKodtarId":"40FF98D8-9710-4DC4-89FA-718046FD6956","Aktiv":true},{"VezerloKodTarId":"9DDC5255-0AC3-4FFF-AC94-E8089682B020","FuggoKodtarId":"0ACC4986-A663-4781-BDC4-951991DA1827","Aktiv":true},{"VezerloKodTarId":"22E7EDE2-BFCA-483A-975B-FFDB17B93272","FuggoKodtarId":"7966F4D9-E79F-46AF-B5A3-999EA5E3C3FA","Aktiv":true}],"ItemsNincs":[]}'
						, '1')
		END
		ELSE
		BEGIN
		print 'UGYINTEZES_ALAPJA - ALAIRAS_FAJTA kódtárfüggőség frissités...'
			
			UPDATE KRT_KodtarFuggoseg SET ORG = @org,
										Vezerlo_KodCsoport_Id = @Vezerlo_Kodcsoport_id,
										Fuggo_KodCsoport_Id = @kodcsoportId,
										Adat = '{"VezerloKodCsoportId":"'+cast(@Vezerlo_Kodcsoport_id AS NVARCHAR(36))+'","FuggoKodCsoportId":"'+cast(@kodcsoportId AS NVARCHAR(36))+'","Items":[{"VezerloKodTarId":"82F2FAFC-A312-4C13-A1FF-E86DB76B131F","FuggoKodtarId":"40FF98D8-9710-4DC4-89FA-718046FD6956","Aktiv":true},{"VezerloKodTarId":"82F2FAFC-A312-4C13-A1FF-E86DB76B131F","FuggoKodtarId":"0ACC4986-A663-4781-BDC4-951991DA1827","Aktiv":true},{"VezerloKodTarId":"9DDC5255-0AC3-4FFF-AC94-E8089682B020","FuggoKodtarId":"40FF98D8-9710-4DC4-89FA-718046FD6956","Aktiv":true},{"VezerloKodTarId":"9DDC5255-0AC3-4FFF-AC94-E8089682B020","FuggoKodtarId":"0ACC4986-A663-4781-BDC4-951991DA1827","Aktiv":true},{"VezerloKodTarId":"22E7EDE2-BFCA-483A-975B-FFDB17B93272","FuggoKodtarId":"7966F4D9-E79F-46AF-B5A3-999EA5E3C3FA","Aktiv":true}],"ItemsNincs":[]}',
										AKTIV = '1'
					where Id = @Fuggoseg_Id;
			
			 
		END
	END
	ELSE
	BEGIN
		if (not exists(select Id from KRT_KodtarFuggoseg where Id = @Fuggoseg_Id))
		BEGIN
			print 'UGYINTEZES_ALAPJA - ALAIRAS_FAJTA kódtárfüggőség létrehozása...'
			
			INSERT INTO KRT_KodtarFuggoseg (Id, Org, Vezerlo_KodCsoport_Id, Fuggo_KodCsoport_Id, Adat, Aktiv)
				VALUES (@Fuggoseg_Id, @org, @Vezerlo_Kodcsoport_id, @kodcsoportId
						, '{"VezerloKodCsoportId":"'+cast(@Vezerlo_Kodcsoport_id AS NVARCHAR(36))+'","FuggoKodCsoportId":"'+cast(@kodcsoportId AS NVARCHAR(36))+'","Items":[{"VezerloKodTarId":"82F2FAFC-A312-4C13-A1FF-E86DB76B131F","FuggoKodtarId":"40FF98D8-9710-4DC4-89FA-718046FD6956","Aktiv":true},{"VezerloKodTarId":"82F2FAFC-A312-4C13-A1FF-E86DB76B131F","FuggoKodtarId":"B1DD099E-9DE1-4875-A822-77BB96A08866","Aktiv":true},{"VezerloKodTarId":"82F2FAFC-A312-4C13-A1FF-E86DB76B131F","FuggoKodtarId":"0ACC4986-A663-4781-BDC4-951991DA1827","Aktiv":true},{"VezerloKodTarId":"9DDC5255-0AC3-4FFF-AC94-E8089682B020","FuggoKodtarId":"40FF98D8-9710-4DC4-89FA-718046FD6956","Aktiv":true},{"VezerloKodTarId":"9DDC5255-0AC3-4FFF-AC94-E8089682B020","FuggoKodtarId":"B1DD099E-9DE1-4875-A822-77BB96A08866","Aktiv":true},{"VezerloKodTarId":"9DDC5255-0AC3-4FFF-AC94-E8089682B020","FuggoKodtarId":"0ACC4986-A663-4781-BDC4-951991DA1827","Aktiv":true},{"VezerloKodTarId":"9DDC5255-0AC3-4FFF-AC94-E8089682B020","FuggoKodtarId":"7966F4D9-E79F-46AF-B5A3-999EA5E3C3FA","Aktiv":true},{"VezerloKodTarId":"22E7EDE2-BFCA-483A-975B-FFDB17B93272","FuggoKodtarId":"40FF98D8-9710-4DC4-89FA-718046FD6956","Aktiv":true},{"VezerloKodTarId":"22E7EDE2-BFCA-483A-975B-FFDB17B93272","FuggoKodtarId":"B1DD099E-9DE1-4875-A822-77BB96A08866","Aktiv":true},{"VezerloKodTarId":"22E7EDE2-BFCA-483A-975B-FFDB17B93272","FuggoKodtarId":"0ACC4986-A663-4781-BDC4-951991DA1827","Aktiv":true},{"VezerloKodTarId":"22E7EDE2-BFCA-483A-975B-FFDB17B93272","FuggoKodtarId":"7966F4D9-E79F-46AF-B5A3-999EA5E3C3FA","Aktiv":true},{"VezerloKodTarId":"82F2FAFC-A312-4C13-A1FF-E86DB76B131F","FuggoKodtarId":"7966F4D9-E79F-46AF-B5A3-999EA5E3C3FA","Aktiv":true}],"ItemsNincs":[]}'
						, '1')
		END
		ELSE
		BEGIN
		print 'UGYINTEZES_ALAPJA - ALAIRAS_FAJTA kódtárfüggőség frissités...'
			
			UPDATE KRT_KodtarFuggoseg SET ORG = @org,
										Vezerlo_KodCsoport_Id = @Vezerlo_Kodcsoport_id,
										Fuggo_KodCsoport_Id = @kodcsoportId,
										Adat = '{"VezerloKodCsoportId":"'+cast(@Vezerlo_Kodcsoport_id AS NVARCHAR(36))+'","FuggoKodCsoportId":"'+cast(@kodcsoportId AS NVARCHAR(36))+'","Items":[{"VezerloKodTarId":"82F2FAFC-A312-4C13-A1FF-E86DB76B131F","FuggoKodtarId":"40FF98D8-9710-4DC4-89FA-718046FD6956","Aktiv":true},{"VezerloKodTarId":"82F2FAFC-A312-4C13-A1FF-E86DB76B131F","FuggoKodtarId":"B1DD099E-9DE1-4875-A822-77BB96A08866","Aktiv":true},{"VezerloKodTarId":"82F2FAFC-A312-4C13-A1FF-E86DB76B131F","FuggoKodtarId":"0ACC4986-A663-4781-BDC4-951991DA1827","Aktiv":true},{"VezerloKodTarId":"9DDC5255-0AC3-4FFF-AC94-E8089682B020","FuggoKodtarId":"40FF98D8-9710-4DC4-89FA-718046FD6956","Aktiv":true},{"VezerloKodTarId":"9DDC5255-0AC3-4FFF-AC94-E8089682B020","FuggoKodtarId":"B1DD099E-9DE1-4875-A822-77BB96A08866","Aktiv":true},{"VezerloKodTarId":"9DDC5255-0AC3-4FFF-AC94-E8089682B020","FuggoKodtarId":"0ACC4986-A663-4781-BDC4-951991DA1827","Aktiv":true},{"VezerloKodTarId":"9DDC5255-0AC3-4FFF-AC94-E8089682B020","FuggoKodtarId":"7966F4D9-E79F-46AF-B5A3-999EA5E3C3FA","Aktiv":true},{"VezerloKodTarId":"22E7EDE2-BFCA-483A-975B-FFDB17B93272","FuggoKodtarId":"40FF98D8-9710-4DC4-89FA-718046FD6956","Aktiv":true},{"VezerloKodTarId":"22E7EDE2-BFCA-483A-975B-FFDB17B93272","FuggoKodtarId":"B1DD099E-9DE1-4875-A822-77BB96A08866","Aktiv":true},{"VezerloKodTarId":"22E7EDE2-BFCA-483A-975B-FFDB17B93272","FuggoKodtarId":"0ACC4986-A663-4781-BDC4-951991DA1827","Aktiv":true},{"VezerloKodTarId":"22E7EDE2-BFCA-483A-975B-FFDB17B93272","FuggoKodtarId":"7966F4D9-E79F-46AF-B5A3-999EA5E3C3FA","Aktiv":true},{"VezerloKodTarId":"82F2FAFC-A312-4C13-A1FF-E86DB76B131F","FuggoKodtarId":"7966F4D9-E79F-46AF-B5A3-999EA5E3C3FA","Aktiv":true}],"ItemsNincs":[]}',
										AKTIV = '1'
					where Id = @Fuggoseg_Id;

		END
	END
END
go
-------------------------------
--BLG 7148 - új kódtárak
-------------------------------
DECLARE @KodcsoportId uniqueidentifier
DECLARE @Org uniqueidentifier
DECLARE @kodtarId uniqueidentifier
set @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'


--DOKUMENTUM_ALAIRAS_PKI kodcsoport
set @kodcsoportId = 'D2633053-269F-4603-8613-C5F467E552D3'
set @kodtarId = '2E6DCF38-FFC5-48FD-A1E6-79B77082AD6A'

IF NOT EXISTS (SELECT 1 FROM KRT_Kodtarak WHERE Id = @kodtarId)
  BEGIN 
     Print 'INSERT KRT_Kodtarak - KR kiküldés' 

     insert into KRT_KodTarak
     (
       Id,
       Org,
       KodCsoport_Id,
       Kod,
       Nev,
       Sorrend,
       Modosithato
    ) 
    values (
      @kodtarId,
      @org,
      @kodcsoportId,
      '5',
      'KR kiküldés jóváhagyás',
      '5',
      '0'
    )
  END
  ELSE
  BEGIN
    Print 'UPDATE KRT_Kodtarak - KR kiküldés' 

	UPDATE KRT_KodTarak
	SET Nev = 'KR kiküldés jóváhagyás'
	WHERE Id = @kodtarId
  END

go





 ------------- BUG_ 7247 - Személyi anyag típusok alatti felsorolt szakaszok létrehozása --------------

DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
IF @isTUK = '1'
BEGIN

	print 'Add_Targyszok kezdete'

	declare @Org uniqueidentifier
	set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

	declare @OrgKod nvarchar(100)
	set @OrgKod = (select Kod from KRT_Orgok where Id = @Org)

	declare @UgyObjTip_Id uniqueidentifier
	set @UgyObjTip_Id = 'A04417A6-54AC-4AF1-9B63-5BABF0203D42'

	declare @DefinicioTipus nvarchar(64)
	set @DefinicioTipus = 'B1'

	declare @Letrehozo_Id uniqueidentifier
	set @Letrehozo_Id = '54E861A5-36ED-44CA-BAA7-C287D125B309'

	declare @now datetime
	set @now = GETDATE()

	declare @Tranz_id uniqueidentifier
	set @Tranz_id = 'C3A05D27-8946-4B7A-BAA9-5D49D334F7DF'

	declare @objMetaDefId_Nemztebizt_ell_metaadatai uniqueidentifier
	set @objMetaDefId_Nemztebizt_ell_metaadatai = '1A1B2EEC-9B6A-E811-80C7-00155D027EA9'

	declare @objMetaDefId_SZBT_metaadatai uniqueidentifier
	set @objMetaDefId_SZBT_metaadatai = '29AB86FC-61BD-E711-80C7-00155D027E9B'

	declare @objMetaDefId_FelhasznaloiEngedely_metaadatai uniqueidentifier
	set @objMetaDefId_FelhasznaloiEngedely_metaadatai  = 'CFEBB7C0-AEBD-E711-80C7-00155D027E9B'

	declare @objMetaDefId_Titoktartasi_metaadatai uniqueidentifier
	set @objMetaDefId_Titoktartasi_metaadatai  = 'BDCDFDB5-B1BD-E711-80C7-00155D027E9B'

	declare @objMetaDefId_Megbizas_metaadatai uniqueidentifier
	set @objMetaDefId_Megbizas_metaadatai  = '6D899731-A175-E811-80C7-00155D027EA9'

	declare @objMetaDefId_Kinevezes_metaadatai uniqueidentifier
	set @objMetaDefId_Kinevezes_metaadatai  = '88AB1F05-3295-44ED-BB66-9E87C83F2579'

	--Tárgyszavak

	declare @Tipus char(1)
	set @Tipus = '1'

	declare @ControlTypeSourceTextBox nvarchar(64)
	set @ControlTypeSourceTextBox = 'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls'

	declare @ControlTypeSourceDropDownList nvarchar(64)
	set @ControlTypeSourceDropDownList = '~/Component/KodTarakDropDownList.ascx'

	declare @ControlTypeSourceCheckBox nvarchar(64)
	set @ControlTypeSourceCheckBox = 'System.Web.UI.WebControls.CheckBox;System.Web'

	declare @ControlTypeSourceCalendar nvarchar(64)
	set @ControlTypeSourceCalendar = '~/Component/CalendarControl.ascx'

	declare @ControlTypeSourceMultiLineTextBox nvarchar(64)
	set @ControlTypeSourceMultiLineTextBox = '~/Component/MultiLineTextBox.ascx'

	declare @targyszoId_Kibocsatas uniqueidentifier
	set @targyszoId_Kibocsatas = 'CDB06D23-0709-499F-A8B9-62846D3B2883'

	declare @targyszoId_Iktatoszam uniqueidentifier
	set @targyszoId_Iktatoszam = '6AE15533-7215-43A6-B1A9-159E4DBC9BA3'

	declare @targyszoId_SelejtezesIdeje uniqueidentifier
	set @targyszoId_SelejtezesIdeje = 'AE226B1B-A856-4406-9A75-DD4971CCBDB1'

	declare @targyszoId_TUK_DokumentumTipus uniqueidentifier
	set @targyszoId_TUK_DokumentumTipus = '41E0EC13-A075-E811-80C7-00155D027EA9'

	declare @targyszoId_MinositesSzintje uniqueidentifier
	set @targyszoId_MinositesSzintje = 'EE3AE8B2-8B7B-4F5E-A9B9-5A86C6F0DBB5'

	declare @targyszoId_VisszavonasigErvenyes uniqueidentifier
	set @targyszoId_VisszavonasigErvenyes = '6851C5D1-A8FC-42CF-A328-C1B513A295ED'

	declare  @targyszoId_Jogosultsag uniqueidentifier
	set @targyszoId_Jogosultsag = '95427C77-1DCD-48E7-B136-4AC0C048A530'

	declare @targyszoId_TitoktartasiVisszavonasIktatoszama uniqueidentifier
	set @targyszoId_TitoktartasiVisszavonasIktatoszama = 'B7F0D347-40D8-4B0F-BC73-8EF5A55387F5'

	declare  @targyszoId_TitoktartasiVisszavonasIdeje uniqueidentifier
	set @targyszoId_TitoktartasiVisszavonasIdeje = '5C678165-0EE7-4830-B38A-D224474B709E'

	declare @targyszoId_TitoktartasiSelejtezesIktatoszama uniqueidentifier
	set @targyszoId_TitoktartasiSelejtezesIktatoszama = '6CA6DC1A-028C-459C-907D-7AF5636EA44E'

	declare @targyszoId_TitoktartasiSelejtezesIdeje uniqueidentifier
	set @targyszoId_TitoktartasiSelejtezesIdeje = '6C3492D7-1DB2-4328-A543-811002534FA5'

	declare @targyszoId_MegbizasTargy uniqueidentifier
	set @targyszoId_MegbizasTargy = 'DDC2076A-97BA-4B63-BC82-9284B2AE5EEE'

	declare @targyszoId_MegbizasIktatoszam uniqueidentifier
	set @targyszoId_MegbizasIktatoszam = '8D5D9462-9C7E-434B-99EF-0D8A529E96AF'

	declare @targyszoId_MegbizasKibocsatasIdeje uniqueidentifier
	set @targyszoId_MegbizasKibocsatasIdeje = '93C13599-EF71-4139-BB75-1FFB6F547D72'

	declare @targyszoId_MegbizasVisszavonasIktatoszama uniqueidentifier
	set @targyszoId_MegbizasVisszavonasIktatoszama = 'AD0535A3-1F4A-49B0-9659-FDF652358337'

	declare @targyszoId_MegbizasVisszavonasIdeje uniqueidentifier
	set @targyszoId_MegbizasVisszavonasIdeje = '5CB9EECC-E0FD-420A-88C8-B0F13AC86EF4'

	declare @targyszoId_KinevezesTargy uniqueidentifier
	set @targyszoId_KinevezesTargy = 'BE79F934-0175-47AD-868E-39FEAB34304C'

	declare @targyszoId_KinevezesIktatoszam uniqueidentifier
	set @targyszoId_KinevezesIktatoszam = 'E544F0F2-B4EC-43EB-8002-C8191C224A4C'

	declare @targyszoId_KinevezesKibocsatasIdeje uniqueidentifier
	set @targyszoId_KinevezesKibocsatasIdeje = 'D4000383-E751-4AA3-B805-9412B2FE6331'

	declare @targyszoId_KinevezesVisszavonasIktatoszama uniqueidentifier
	set @targyszoId_KinevezesVisszavonasIktatoszama = '60EED760-F275-41E7-A97C-6BA3A37573FD'

	declare @targyszoId_KinevezesVisszavonasIdeje uniqueidentifier
	set @targyszoId_KinevezesVisszavonasIdeje = 'CD930D22-28AD-4659-9C48-84FA2B244FDE'

	-- Nemzetbiztonsági ellenőrzés kezdeményezése - 'Kibocsátás' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_Kibocsatas)
	BEGIN
		print 'insert - Nemzetbiztonsági ellenőrzés kezdeményezése - Kibocsátás'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_Kibocsatas
		   ,@Org
		   ,@Tipus
		   ,'Kibocsátás'
		   ,'Kibocsatas'
		   ,@ControlTypeSourceTextBox
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Nemzetbiztonsági ellenőrzés kezdeményezése - Kibocsatas'
	END

	-- Nemzetbiztonsági ellenőrzés kezdeményezése - 'Iktatószám' tárgyszó
	set @Tipus = '1'


	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_Iktatoszam)
	BEGIN
		print 'insert - Nemzetbiztonsági ellenőrzés kezdeményezése - Iktatószám'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_Iktatoszam
		   ,@Org
		   ,@Tipus
		   ,'Iktatószám'
		   ,'Iktatoszam'
		   ,@ControlTypeSourceTextBox
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Nemzetbiztonsági ellenőrzés kezdeményezése - Iktatószám'
	END

	-- SZBT - 'Selejtezés ideje' tárgyszó
	set @Tipus = '1'

	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_SelejtezesIdeje)
	BEGIN
		print 'insert - SZBT - Selejtezés ideje'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_SelejtezesIdeje
		   ,@Org
		   ,@Tipus
		   ,'Selejtezés ideje'
		   ,'Selejtezes ideje'
		   ,@ControlTypeSourceCalendar
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: SZBT - Selejtezés ideje'
	END

	-- Felhasználói engedély - 'Minősítés szintje' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_MinositesSzintje)
	BEGIN
		print 'insert - Felhasználói engedély - Minősítés szintje'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_MinositesSzintje
		   ,@Org
		   ,@Tipus
		   ,'Minősítés szintje'
		   ,'Minosites szintje'
		   ,@ControlTypeSourceDropDownList
		   ,'IRAT_MINOSITES'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Felhasználói engedély - Minősítés szintje'
	END

	-- Felhasználói engedély - 'Visszavonásig érvényes' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_VisszavonasigErvenyes)
	BEGIN
		print 'insert - Felhasználói engedély - Visszavonásig érvényes'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_VisszavonasigErvenyes
		   ,@Org
		   ,@Tipus
		   ,'Visszavonásig érvényes'
		   ,'Visszavonasig ervenyes'
		   ,@ControlTypeSourceCheckBox
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Felhasználói engedély - Visszavonásig érvényes'
	END


	-- Felhasználói engedély - 'Jogosultság' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_Jogosultsag)
	BEGIN
		print 'insert - Felhasználói engedély - Jogosultság'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_Jogosultsag
		   ,@Org
		   ,@Tipus
		   ,'Jogosultság'
		   ,'Jogosultsag'
		   ,@ControlTypeSourceCheckBox
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Felhasználói engedély - Jogosultság'
	END

	-- 'Titoktartás - Visszavonás iktatószáma' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_TitoktartasiVisszavonasIktatoszama)
	BEGIN
		print 'insert - Titoktartás - Visszavonás iktatószáma'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_TitoktartasiVisszavonasIktatoszama
		   ,@Org
		   ,@Tipus
		   ,'Visszavonás iktatószáma'
		   ,'Visszavonas iktatoszama'
		   ,@ControlTypeSourceTextBox
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Titoktartás - Titoktartás - Visszavonás iktatószáma'
	END

	-- 'Titoktartás - Visszavonás ideje' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_TitoktartasiVisszavonasIdeje)
	BEGIN
		print 'insert - Titoktartás - Visszavonás ideje'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_TitoktartasiVisszavonasIdeje
		   ,@Org
		   ,@Tipus
		   ,'Visszavonás ideje'
		   ,'Visszavonas ideje'
		   ,@ControlTypeSourceCalendar
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Titoktartás - Visszavonás ideje'
	END


	-- 'Titoktartás - Selejtezés iktatószáma' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_TitoktartasiSelejtezesIktatoszama)
	BEGIN
		print 'insert - Titoktartás - Selejtezés iktatószáma'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_TitoktartasiSelejtezesIktatoszama
		   ,@Org
		   ,@Tipus
		   ,'Selejtezés iktatószáma'
		   ,'Selejtezes iktatoszama'
		   ,@ControlTypeSourceTextBox
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Titoktartás - Selejtezés iktatószáma'
	END


	-- 'Titoktartás - Selejtezés ideje' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_TitoktartasiSelejtezesIdeje)
	BEGIN
		print 'insert - Titoktartás - Selejtezés ideje'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_TitoktartasiSelejtezesIdeje
		   ,@Org
		   ,@Tipus
		   ,'Selejtezés ideje'
		   ,'Selejtezes ideje'
		   ,@ControlTypeSourceCalendar
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Titoktartás - Selejtezés ideje'
	END


	-- Megbízás - 'Tárgy' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_MegbizasTargy)
	BEGIN
		print 'insert - Megbízás - Tárgy'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_MegbizasTargy
		   ,@Org
		   ,@Tipus
		   ,'Tárgy'
		   ,'Targy'
		   ,@ControlTypeSourceMultiLineTextBox
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Megbízás - Tárgy'
	END

	-- Megbízás - 'Iktatószám' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_MegbizasIktatoszam)
	BEGIN
		print 'insert - Megbízás - Iktatószám'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_MegbizasIktatoszam
		   ,@Org
		   ,@Tipus
		   ,'Iktatószám'
		   ,'Iktatoszam'
		   ,@ControlTypeSourceTextBox
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Megbízás - Iktatószám'
	END

	-- Megbízás - 'Kibocsátás ideje' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_MegbizasKibocsatasIdeje)
	BEGIN
		print 'insert - Megbízás - Kibocsátás ideje'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_MegbizasKibocsatasIdeje
		   ,@Org
		   ,@Tipus
		   ,'Kibocsátás ideje'
		   ,'Kibocsatas ideje'
		   ,@ControlTypeSourceCalendar
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Megbízás - Kibocsátás ideje'
	END

	-- Megbízás - 'Visszavonás iktatószáma' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_MegbizasVisszavonasIktatoszama)
	BEGIN
		print 'insert - Megbízás - Visszavonás iktatószáma'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_MegbizasVisszavonasIktatoszama
		   ,@Org
		   ,@Tipus
		   ,'Visszavonás iktatószáma'
		   ,'Visszavonas iktatoszama'
		   ,@ControlTypeSourceTextBox
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Megbízás - Visszavonás iktatószáma'
	END

	-- Megbízás - 'Visszavonás ideje' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_MegbizasVisszavonasIdeje)
	BEGIN
		print 'insert - Megbízás - Visszavonás ideje'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_MegbizasVisszavonasIdeje
		   ,@Org
		   ,@Tipus
		   ,'Visszavonás ideje'
		   ,'Visszavonas ideje'
		   ,@ControlTypeSourceCalendar
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Megbízás - Visszavonás ideje'
	END

	-- Kinevezés - 'Tárgy' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_KinevezesTargy)
	BEGIN
		print 'insert - Kinevezés - Tárgy'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_KinevezesTargy
		   ,@Org
		   ,@Tipus
		   ,'Tárgy'
		   ,'Targy'
		   ,@ControlTypeSourceMultiLineTextBox
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Kinevezés - Tárgy'
	END

	-- Kinevezés - 'Iktatószám' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_KinevezesIktatoszam)
	BEGIN
		print 'insert - Kinevezés - Iktatószám'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_KinevezesIktatoszam
		   ,@Org
		   ,@Tipus
		   ,'Iktatószám'
		   ,'Iktatoszam'
		   ,@ControlTypeSourceTextBox
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Kinevezés - Iktatószám'
	END

	-- Kinevezés - 'Kibocsátás ideje' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_KinevezesKibocsatasIdeje)
	BEGIN
		print 'insert - Kinevezés - Kibocsátás ideje'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_KinevezesKibocsatasIdeje
		   ,@Org
		   ,@Tipus
		   ,'Kibocsátás ideje'
		   ,'Kibocsatas ideje'
		   ,@ControlTypeSourceCalendar
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Kinevezés - Kibocsátás ideje'
	END

	-- Kinevezés - 'Visszavonás iktatószáma' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_KinevezesVisszavonasIktatoszama)
	BEGIN
		print 'insert - Kinevezés - Visszavonás iktatószáma'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_KinevezesVisszavonasIktatoszama
		   ,@Org
		   ,@Tipus
		   ,'Visszavonás iktatószáma'
		   ,'Visszavonas iktatoszama'
		   ,@ControlTypeSourceTextBox
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Kinevezés - Visszavonás iktatószáma'
	END

	-- Kinevezés - 'Visszavonás ideje' tárgyszó
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_KinevezesVisszavonasIdeje)
	BEGIN
		print 'insert - Kinevezés - Visszavonás ideje'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_KinevezesVisszavonasIdeje
		   ,@Org
		   ,@Tipus
		   ,'Visszavonás ideje'
		   ,'Visszavonas ideje'
		   ,@ControlTypeSourceCalendar
		   ,NULL
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Kinevezés - Visszavonás ideje'
	END

	-------------------------------------------- EREC_Obj_MetaDefinicio létrehozás -----------------------------------------
	------------------------------------------------------------------------------------------------------------------------
	--Kinevezes metadefiníció létrehozása
	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaDefinicio WHERE Id = @objMetaDefId_Kinevezes_metaadatai)
	BEGIN
		INSERT INTO [dbo].[EREC_Obj_MetaDefinicio]
			   ([Id]
			   ,[Org]
			   ,[Objtip_Id]
			   ,[Objtip_Id_Column]
			   ,[ColumnValue]
			   ,[DefinicioTipus]
			   ,[Felettes_Obj_Meta]
			   ,[MetaXSD]
			   ,[ImportXML]
			   ,[EgyebXML]
			   ,[ContentType]
			   ,[SPSSzinkronizalt]
			   ,[SPS_CTT_Id]
			   ,[WorkFlowVezerles]
			   ,[Ver]
			   ,[Note]
			   ,[Stat_id]
			   ,[ErvKezd]
			   ,[ErvVege]
			   ,[Letrehozo_id]
			   ,[LetrehozasIdo]
			   ,[Modosito_id]
			   ,[ModositasIdo]
			   ,[Zarolo_id]
			   ,[ZarolasIdo]
			   ,[Tranz_id]
			   ,[UIAccessLog_id])
		 VALUES
			   (@objMetaDefId_Kinevezes_metaadatai
			   ,@Org
			   ,'CDAC02CA-1763-4391-91D7-6914EE6F7BE5'
			   ,'DF918DEB-C303-4DFD-A8DD-67769FCAB930'
			   ,'G'
			   ,'B2'
			   ,NULL
			   ,NULL
			   ,NULL
			   ,NULL
			   ,'Kinevezés'
			   ,'0'
			   ,NULL
			   ,'0'
			   ,1
			   ,NULL
			   ,NULL
			   ,@now
			   ,'4700-12-31'
			   ,@Letrehozo_Id
			   ,@now
			   ,NULL
			   ,NULL
			   ,NULL
			   ,NULL
			   ,NULL
			   ,NULL)
	END
	-------------------------------------------- EREC_Obj_MetaAdatai -------------------------------------------------------
	------------------------------------------------------------------------------------------------------------------------


	-- EREC_Obj_MetaAdatai - Nemztebizt_ell metaadatai - Kibocsatas
	declare @recordId uniqueidentifier
	set @recordId = '2E84EAF2-65A1-46DB-8845-AAEF2FD9DB76'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Nemztebizt_ell metaadatai - Kibocsatas'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Nemztebizt_ell_metaadatai
		   ,@targyszoId_Kibocsatas
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - Nemztebizt_ell metaadatai - Iktatószám
	set @recordId = 'CEAF6FDA-DDA7-4880-A951-D8232EC8CE77'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Nemztebizt_ell metaadatai - Iktatószám'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Nemztebizt_ell_metaadatai
		   ,@targyszoId_Iktatoszam
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - SZBT metaadatai - Selejtezés ideje
	set @recordId = 'BD3B81A4-0628-46C8-9FB8-86ACBF5ADE24'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - SZBT - Selejtezés ideje'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_SZBT_metaadatai
		   ,@targyszoId_SelejtezesIdeje
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - SZBT metaadatai - TÜK dokumentum típus
	set @recordId = 'B12790A1-6AA4-4455-A13D-CA94755EA09F'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - SZBT metaadatai - TÜK dokumentum típus'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_SZBT_metaadatai
		   ,@targyszoId_TUK_DokumentumTipus
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - Felhasznalói engedély metaadatai - Minősítés szintje
	set @recordId = 'EA245F96-0195-4689-9751-2A7F41DABE9B'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Felhasznalói engedély metaadatai - Minősítés szintje'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_FelhasznaloiEngedely_metaadatai
		   ,@targyszoId_MinositesSzintje
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END



	-- EREC_Obj_MetaAdatai - Felhasznalói engedély metaadatai - Visszavonásig érvényes
	set @recordId = 'C1537251-8A7A-4AAE-BECF-3750BCA5E144'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Felhasznalói engedély metaadatai - Visszavonásig érvényes'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_FelhasznaloiEngedely_metaadatai
		   ,@targyszoId_VisszavonasigErvenyes
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - Felhasznalói engedély metaadatai - Jogosultság
	set @recordId = '5BED575A-A7F2-407D-BD9A-624D78755690'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Felhasznalói engedély metaadatai - Jogosultság'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_FelhasznaloiEngedely_metaadatai
		   ,@targyszoId_Jogosultsag
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - Titoktartási nyilatkozat - Visszavonás iktatószáma
	set @recordId = '27437D65-7C25-4031-82B4-74406D2E4C14'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Titoktartási nyilatkozat - Visszavonás iktatószáma'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Titoktartasi_metaadatai
		   ,@targyszoId_TitoktartasiVisszavonasIktatoszama
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - Titoktartási nyilatkozat - Visszavonás ideje
	set @recordId = 'D54C57A0-28EB-4E59-9727-CF4F488CDC11'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Titoktartási nyilatkozat - Visszavonás ideje'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Titoktartasi_metaadatai
		   ,@targyszoId_TitoktartasiVisszavonasIdeje
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - Titoktartási nyilatkozat - Selejtezés iktatószáma
	set @recordId = '14E7A38C-E4E0-4FF7-9149-3B9C633812AE'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Titoktartási nyilatkozat - Selejtezés iktatószáma'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Titoktartasi_metaadatai
		   ,@targyszoId_TitoktartasiSelejtezesIktatoszama
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - Titoktartási nyilatkozat - Selejtezés ideje
	set @recordId = '944DD962-7B24-41FD-ABAD-08BB67FF2F04'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Titoktartási nyilatkozat - Selejtezés ideje'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Titoktartasi_metaadatai
		   ,@targyszoId_TitoktartasiSelejtezesIdeje
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - Megbízás - Tárgy
	set @recordId = 'AF7A6D5A-A2CD-48C3-AFD1-D99DEC4A305E'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Megbízás - Tárgy'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Megbizas_metaadatai
		   ,@targyszoId_MegbizasTargy
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - Megbízás - Iktatószám
	set @recordId = 'A5163C30-2245-4A5C-ACB9-88CA32459E21'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Megbízás - Iktatószám'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Megbizas_metaadatai
		   ,@targyszoId_MegbizasIktatoszam
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - Megbízás - Kibocsátás ideje
	set @recordId = '08120EF0-3E0A-4AF3-A0CC-BAA83BC435E8'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Megbízás - Kibocsátás ideje'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Megbizas_metaadatai
		   ,@targyszoId_MegbizasKibocsatasIdeje
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - Megbízás - Visszavonás iktatószáma
	set @recordId = '95B233E2-F4B8-42DD-BEBF-43EB6E46C4F8'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Megbízás - Visszavonás iktatószáma'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Megbizas_metaadatai
		   ,@targyszoId_MegbizasVisszavonasIktatoszama
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END


	-- EREC_Obj_MetaAdatai - Megbízás - Visszavonás ideje
	set @recordId = 'E32024FF-6733-4439-9734-6CA2F98EA9A0'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Megbízás - Visszavonás ideje'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Megbizas_metaadatai
		   ,@targyszoId_MegbizasVisszavonasIdeje
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END


	-- EREC_Obj_MetaAdatai - Kinevezés - Tárgy
	set @recordId = '3EE4A3CC-0E46-48EE-BE6F-54482594F48E'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Kinevezés - Tárgy'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Kinevezes_metaadatai
		   ,@targyszoId_KinevezesTargy
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - Kinevezés - Iktatószám
	set @recordId = '5BF11F8B-D008-4143-8538-BB85D803A970'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Kinevezés - Iktatószám'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Kinevezes_metaadatai
		   ,@targyszoId_KinevezesIktatoszam
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - Kinevezés - Kibocsátás ideje
	set @recordId = '790FE589-EEBF-45BF-AE6A-967ADA57B121'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Kinevezés - Kibocsátás ideje'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Kinevezes_metaadatai
		   ,@targyszoId_KinevezesKibocsatasIdeje
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- EREC_Obj_MetaAdatai - Kinevezés - Visszavonás iktatószáma
	set @recordId = 'D61EF34F-CD51-468A-8F16-61AB1EDA2F9B'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Kinevezés - Visszavonás iktatószáma'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Kinevezes_metaadatai
		   ,@targyszoId_KinevezesVisszavonasIktatoszama
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END


	-- EREC_Obj_MetaAdatai - Kinevezés - Visszavonás ideje
	set @recordId = '32A09E4F-75A8-4CC3-91B7-9153B94ED1DA'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Kinevezés - Visszavonás ideje'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Kinevezes_metaadatai
		   ,@targyszoId_KinevezesVisszavonasIdeje
		   ,0
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	print 'Add_Targyszok vege'

	print 'Érvénytelenítés'
	-- ÉRVÉNYTELENÍTÉS - EREC_Obj_MetaAdatai - Megbízás - Küldemény átvitel és egyéb
	UPDATE EREC_TargySzavak
	SET EREC_TargySzavak.ErvVege = DATEADD(DAY,-1,@now)
	WHERE EREC_TargySzavak.Id = 'C2076159-A175-E811-80C7-00155D027EA9' or
	EREC_TargySzavak.Id = 'E5461F6B-A175-E811-80C7-00155D027EA9'

-----------------------------------------------------------------------------------
	
	print 'Kinevezés kódtár hozzáadása KRT_Kodtarak táblához'
	-- Kinevezés kódtár hozzáadása KRT_Kodtarak táblához
			IF not exists (select 1 from KRT_KodTarak where Id ='8D7D2F2F-4EBC-4297-9BF6-7603F8C94715')
				BEGIN
					INSERT INTO [dbo].[KRT_KodTarak]
					   ([Id]
					   ,[Org]
					   ,[KodCsoport_Id]
					   ,[ObjTip_Id_AdatElem]
					   ,[Obj_Id]
					   ,[Kod]
					   ,[Nev]
					   ,[RovidNev]
					   ,[Egyeb]
					   ,[Modosithato]
					   ,[Sorrend]
					   ,[Ver]
					   ,[Note]
					   ,[Stat_id]
					   ,[ErvKezd]
					   ,[ErvVege]
					   ,[Letrehozo_id]
					   ,[LetrehozasIdo]
					   ,[Modosito_id]
					   ,[ModositasIdo]
					   ,[Zarolo_id]
					   ,[ZarolasIdo]
					   ,[Tranz_id]
					   ,[UIAccessLog_id])
					 VALUES
					   ('8D7D2F2F-4EBC-4297-9BF6-7603F8C94715',
					   @Org,
					   'B084D417-0E1A-4072-A628-088207A4C99E',
					   NULL,
					   NULL,
					  'G',
					   'Kinevezés',
					   NULL,
					   NULL,
					   1,
					   '6',
					   1,
					   NULL,
					   NULL,
						@now,
					   '4700-12-31',
						@Letrehozo_Id,
					    @now,
						NULL,
						NULL,
						NULL,
						NULL,
						NULL,
						NULL
						)
			END
			
	END

GO
-------------------------------BUG_7247--------------------------------------
-----------Személyi anyag típusok alatti felsorolt szakaszok létrehozása-----
-------------------------------VÉGE------------------------------------------ 
-------------------------BUG 7248 - TÜK - kódtár lista értékek módosítása-----------------------
Print('BUG 7248 - TÜK - kódtár lista értékek módosítása')
DECLARE @isTUK char(1); 

SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 


--[Posta hibrid szolgáltatás] érvénytelenítése
Print('[Posta hibrid szolgáltatás] érvénytelenítése')
IF @isTUK = '1'
	BEGIN
		IF exists (select 1 from KRT_KodTarak where Id ='815F0C4F-B1EF-41E2-A41F-41E2CAC6E5EB')
			BEGIN
				UPDATE KRT_KodTarak
				SET ErvVege = getdate() - 1
				WHERE  Id = '815F0C4F-B1EF-41E2-A41F-41E2CAC6E5EB' AND
					   KodCsoport_Id = '6982F668-C8CD-47A3-AC62-9F16E439473C'
						
				PRINT(CHAR(13)+CHAR(10) + 'EXISTED AND UPDATED KODTAR RECORD -[815F0C4F-B1EF-41E2-A41F-41E2CAC6E5EB]: [Posta hibrid szolgáltatás] -> ErvVege = getdate() -1')
			END
END


-------------------------------------------------------------------------------------------------------------------
 IF @isTUK = '1'
	BEGIN
		IF not exists (select 1 from KRT_KodTarak where Id ='910412E8-ECCE-400A-9F6B-01CC888A2B25')
			BEGIN
			Print('[Belső] beszúrása')
				INSERT INTO [dbo].[KRT_KodTarak]
				   ([Id]
				   ,[Org]
				   ,[KodCsoport_Id]
				   ,[ObjTip_Id_AdatElem]
				   ,[Obj_Id]
				   ,[Kod]
				   ,[Nev]
				   ,[RovidNev]
				   ,[Egyeb]
				   ,[Modosithato]
				   ,[Sorrend]
				   ,[Ver]
				   ,[Note]
				   ,[Stat_id]
				   ,[ErvKezd]
				   ,[ErvVege]
				   ,[Letrehozo_id]
				   ,[LetrehozasIdo]
				   ,[Modosito_id]
				   ,[ModositasIdo]
				   ,[Zarolo_id]
				   ,[ZarolasIdo]
				   ,[Tranz_id]
				   ,[UIAccessLog_id])
				 VALUES
				   ('910412E8-ECCE-400A-9F6B-01CC888A2B25',
				   '450B510A-7CAA-46B0-83E3-18445C0C53A9',
				   '3D4E9FB1-861D-40BF-8497-B84658A114D6',
				   NULL,
				   NULL,
				  '2',
				   'Belső',
				   NULL,
				   NULL,
				   1,
				   '2',
				   6,
				   NULL,
				   NULL,
					'2019-03-29',
				   '4700-12-31',
					NULL,
				   '2019-05-29',
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL
					)

					PRINT(CHAR(13)+CHAR(10) + 'INSERTED KODTAR RECORD - [Belső], [910412E8-ECCE-400A-9F6B-01CC888A2B25]')
					DECLARE @xmltmp1 xml = (SELECT * FROM KRT_KodTarak WHERE  KodCsoport_Id = '3D4E9FB1-861D-40BF-8497-B84658A114D6' AND Nev = 'Belső' FOR XML AUTO)
					PRINT CONVERT(NVARCHAR(MAX), @xmltmp1)
		END
		ELSE
			BEGIN
				UPDATE KRT_KodTarak
				SET ErvVege = '4700-12-31'
				WHERE  KodCsoport_Id = '3D4E9FB1-861D-40BF-8497-B84658A114D6' AND Nev = 'Belső'

				PRINT(CHAR(13)+CHAR(10) + 'EXISTED AND UPDATED KODTAR RECORD - [Belső], [910412E8-ECCE-400A-9F6B-01CC888A2B25]')
				
				DECLARE @xmltmp2 xml = (SELECT * FROM KRT_KodTarak WHERE  KodCsoport_Id = '3D4E9FB1-861D-40BF-8497-B84658A114D6' AND Nev = 'Belső' FOR XML AUTO)
				PRINT CONVERT(NVARCHAR(MAX), @xmltmp2)
			END
END

IF @isTUK = '1'
	BEGIN
		IF not exists (select 1 from KRT_KodTarak where Id ='DFC0C155-A7AD-4BC4-8CE2-9B1EA09CCA50')
			BEGIN
				INSERT INTO [dbo].[KRT_KodTarak]
				   ([Id]
				   ,[Org]
				   ,[KodCsoport_Id]
				   ,[ObjTip_Id_AdatElem]
				   ,[Obj_Id]
				   ,[Kod]
				   ,[Nev]
				   ,[RovidNev]
				   ,[Egyeb]
				   ,[Modosithato]
				   ,[Sorrend]
				   ,[Ver]
				   ,[Note]
				   ,[Stat_id]
				   ,[ErvKezd]
				   ,[ErvVege]
				   ,[Letrehozo_id]
				   ,[LetrehozasIdo]
				   ,[Modosito_id]
				   ,[ModositasIdo]
				   ,[Zarolo_id]
				   ,[ZarolasIdo]
				   ,[Tranz_id]
				   ,[UIAccessLog_id])
				 VALUES
				   ('DFC0C155-A7AD-4BC4-8CE2-9B1EA09CCA50',
				   '450B510A-7CAA-46B0-83E3-18445C0C53A9',
				   '6982F668-C8CD-47A3-AC62-9F16E439473C',
				   NULL,
				   NULL,
				  '110',
				   'Helyben maradó',
				   NULL,
				   NULL,
				   1,
				   '6',
				   1,
				   NULL,
				   NULL,
					'2019-03-29',
				   '4700-12-31',
					NULL,
				   '2019-03-29',
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL
					)

					PRINT(CHAR(13)+CHAR(10) + 'INSERTED KODTAR RECORD - [Helyben maradó], [DFC0C155-A7AD-4BC4-8CE2-9B1EA09CCA50]')
					DECLARE @xmltmp3  xml = (SELECT * FROM KRT_KodTarak WHERE Id = 'DFC0C155-A7AD-4BC4-8CE2-9B1EA09CCA50' FOR XML AUTO)
					PRINT CONVERT(NVARCHAR(MAX), @xmltmp3)
		END
		ELSE
			BEGIN
				UPDATE [dbo].[KRT_KodTarak]
				SET Kod = '110'
				WHERE Id = 'DFC0C155-A7AD-4BC4-8CE2-9B1EA09CCA50'

				PRINT( CHAR(13)+CHAR(10) + 'EXISTED AND UPDATED KODTAR RECORD - [Helyben maradó], [DFC0C155-A7AD-4BC4-8CE2-9B1EA09CCA50]')
				DECLARE @xmltmp4  xml = (SELECT * FROM KRT_KodTarak WHERE Id = 'DFC0C155-A7AD-4BC4-8CE2-9B1EA09CCA50' FOR XML AUTO)
				PRINT CONVERT(NVARCHAR(MAX), @xmltmp4)
			END
END

IF @isTUK = '1'
	BEGIN
		IF not exists (select 1 from KRT_KodTarak where Id ='5628406A-C9D9-4BEB-9F7B-61FE5FCF8487')
			BEGIN
				INSERT INTO [dbo].[KRT_KodTarak]
				   ([Id]
				   ,[Org]
				   ,[KodCsoport_Id]
				   ,[ObjTip_Id_AdatElem]
				   ,[Obj_Id]
				   ,[Kod]
				   ,[Nev]
				   ,[RovidNev]
				   ,[Egyeb]
				   ,[Modosithato]
				   ,[Sorrend]
				   ,[Ver]
				   ,[Note]
				   ,[Stat_id]
				   ,[ErvKezd]
				   ,[ErvVege]
				   ,[Letrehozo_id]
				   ,[LetrehozasIdo]
				   ,[Modosito_id]
				   ,[ModositasIdo]
				   ,[Zarolo_id]
				   ,[ZarolasIdo]
				   ,[Tranz_id]
				   ,[UIAccessLog_id])
				 VALUES
				   ('5628406A-C9D9-4BEB-9F7B-61FE5FCF8487',
				   '450B510A-7CAA-46B0-83E3-18445C0C53A9',
				   '6982F668-C8CD-47A3-AC62-9F16E439473C',
				   NULL,
				   NULL,
				  '4',
				   'Egyéb',
				   NULL,
				   NULL,
				   1,
				   '4',
				   1,
				   NULL,
				   NULL,
					'2019-03-29',
				   '4700-12-31',
					NULL,
				   '2019-03-29',
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL
					)

					PRINT(CHAR(13)+CHAR(10) + 'INSERTED KODTAR RECORD - [Egyéb], [5628406A-C9D9-4BEB-9F7B-61FE5FCF8487]')
					DECLARE @xmltmp5  xml = (SELECT * FROM KRT_KodTarak WHERE Id = '5628406A-C9D9-4BEB-9F7B-61FE5FCF8487' FOR XML AUTO)
					PRINT CONVERT(NVARCHAR(MAX), @xmltmp5)
		END
		ELSE
			BEGIN
				PRINT(CHAR(13)+CHAR(10) + 'EXISTED KODTAR RECORD - [Egyéb], [5628406A-C9D9-4BEB-9F7B-61FE5FCF8487]')
				
				DECLARE @xmltmp6  xml = (SELECT * FROM KRT_KodTarak WHERE Id = '5628406A-C9D9-4BEB-9F7B-61FE5FCF8487' FOR XML AUTO)
				PRINT CONVERT(NVARCHAR(MAX), @xmltmp6)
			END
END

IF @isTUK = '1'
	BEGIN
		IF not exists (select 1 from KRT_KodTarak where Id ='BB22300C-C924-4D82-91DB-09F36DE06168')
			BEGIN
				INSERT INTO [dbo].[KRT_KodTarak]
				   ([Id]
				   ,[Org]
				   ,[KodCsoport_Id]
				   ,[ObjTip_Id_AdatElem]
				   ,[Obj_Id]
				   ,[Kod]
				   ,[Nev]
				   ,[RovidNev]
				   ,[Egyeb]
				   ,[Modosithato]
				   ,[Sorrend]
				   ,[Ver]
				   ,[Note]
				   ,[Stat_id]
				   ,[ErvKezd]
				   ,[ErvVege]
				   ,[Letrehozo_id]
				   ,[LetrehozasIdo]
				   ,[Modosito_id]
				   ,[ModositasIdo]
				   ,[Zarolo_id]
				   ,[ZarolasIdo]
				   ,[Tranz_id]
				   ,[UIAccessLog_id])
				 VALUES
				   ('BB22300C-C924-4D82-91DB-09F36DE06168',
				   '450B510A-7CAA-46B0-83E3-18445C0C53A9',
				   '272DC3FD-4A8D-4EA2-88ED-C795A55D0373',
				   NULL,
				   NULL,
				  '341',
				   'meghívó',
				   NULL,
				   NULL,
				   1,
				   2,
				   1,
				   NULL,
				   NULL,
					'2019-03-29',
				   '4700-12-31',
					NULL,
				   '2019-03-29',
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL
					)
		PRINT(CHAR(13)+CHAR(10) + 'INSERTED KODTAR RECORD - [meghívó], [BB22300C-C924-4D82-91DB-09F36DE06168]')
		DECLARE @xmltmp7  xml = (SELECT * FROM KRT_KodTarak WHERE Id = 'BB22300C-C924-4D82-91DB-09F36DE06168' FOR XML AUTO)
		PRINT CONVERT(NVARCHAR(MAX), @xmltmp7)
		
		END
		ELSE
			BEGIN
				PRINT(CHAR(13)+CHAR(10) + 'EXISTED KODTAR RECORD - [meghívó], [BB22300C-C924-4D82-91DB-09F36DE06168]')
				DECLARE @xmltmp8  xml = (SELECT * FROM KRT_KodTarak WHERE Id = 'BB22300C-C924-4D82-91DB-09F36DE06168' FOR XML AUTO)
				PRINT CONVERT(NVARCHAR(MAX), @xmltmp8)
			END
END

IF @isTUK = '1'
	BEGIN
		IF not exists (select 1 from KRT_KodTarak where Id ='AEC97007-1AE1-4B79-B6DC-163FEA5A5333')
			BEGIN
				INSERT INTO [dbo].[KRT_KodTarak]
				   ([Id]
				   ,[Org]
				   ,[KodCsoport_Id]
				   ,[ObjTip_Id_AdatElem]
				   ,[Obj_Id]
				   ,[Kod]
				   ,[Nev]
				   ,[RovidNev]
				   ,[Egyeb]
				   ,[Modosithato]
				   ,[Sorrend]
				   ,[Ver]
				   ,[Note]
				   ,[Stat_id]
				   ,[ErvKezd]
				   ,[ErvVege]
				   ,[Letrehozo_id]
				   ,[LetrehozasIdo]
				   ,[Modosito_id]
				   ,[ModositasIdo]
				   ,[Zarolo_id]
				   ,[ZarolasIdo]
				   ,[Tranz_id]
				   ,[UIAccessLog_id])
				 VALUES
				   ('AEC97007-1AE1-4B79-B6DC-163FEA5A5333',
				   '450B510A-7CAA-46B0-83E3-18445C0C53A9',
				   '992520A2-5892-413A-A374-8515E4781307',
				   NULL,
				   NULL,
				  '42',
				   '42',
				   NULL,
				   NULL,
				   1,
				   '42',
				   1,
				   NULL,
				   NULL,
					'2019-03-29',
				   '4700-12-31',
					NULL,
				   '2019-03-29',
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL
					)
		PRINT(CHAR(13)+CHAR(10) + 'INSERTED KODTAR RECORD - [42], [AEC97007-1AE1-4B79-B6DC-163FEA5A5333]')
		DECLARE @xmltmp9  xml = (SELECT * FROM KRT_KodTarak WHERE Id = 'AEC97007-1AE1-4B79-B6DC-163FEA5A5333' FOR XML AUTO)
		PRINT CONVERT(NVARCHAR(MAX), @xmltmp9)
		
		END
		ELSE
			BEGIN
				PRINT(CHAR(13)+CHAR(10) + 'EXISTED KODTAR RECORD - [42], [AEC97007-1AE1-4B79-B6DC-163FEA5A5333]')
				DECLARE @xmltmp10  xml = (SELECT * FROM KRT_KodTarak WHERE Id = 'AEC97007-1AE1-4B79-B6DC-163FEA5A5333' FOR XML AUTO)
				PRINT CONVERT(NVARCHAR(MAX), @xmltmp10)
			END
END


IF @isTUK = '1'
	BEGIN
		UPDATE KRT_KodTarak
		SET Nev = 'EU RESTRICTED - „Korlátozott terjesztésű!”'
		WHERE Kod = 'EU002'

		UPDATE KRT_KodTarak
		SET Nev = 'EU CONFIDENTIAL - „Bizalmas!”'
		WHERE Kod = 'EU003'

		UPDATE KRT_KodTarak
		SET Nev = 'EU SECRET- „Titkos!”'
		WHERE Kod = 'EU004'

		UPDATE KRT_KodTarak
		SET Nev = 'EU TOP SECRET - „Szigorúan titkos!”'
		WHERE Kod = 'EU005'

		UPDATE KRT_KodTarak
		SET Nev = 'NATO RESTRICTED - „Korlátozott terjesztésű!”'
		WHERE Kod = 'NATO002'

		UPDATE KRT_KodTarak
		SET Nev = 'NATO CONFIDENTIAL - „Bizalmas!”'
		WHERE Kod = 'NATO003'

		UPDATE KRT_KodTarak
		SET Nev = 'NATO SECRET - „Titkos!”'
		WHERE Kod = 'NATO004'

		UPDATE KRT_KodTarak
		SET Nev = 'COSMIC TOP SECRET - „Szigorúan titkos!”'
		WHERE Kod = 'NATO005'

END

UPDATE KRT_KodTarak
SET Nev = 'Bejövő'
WHERE Id = '1BE2CD25-1180-4EFF-8B95-F3B7019F2EE5'

Print('BUG 7248 - TÜK - kódtár lista értékek módosítása')
Print('VÉGE')
-------------------------BUG 7248 - TÜK - kódtár lista értékek módosítása-----------------------
-------------------------------------------VÉGE-------------------------------------------------
GO


-------------- BUG 7922: FPH - Tömeges tértivevény érkeztetés menüpont
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
		
if @org_kod = 'FPH' 
BEGIN 
	Print @org_kod +': Tömeges tértivevény érkeztetés menüpont'

	DECLARE @menuId uniqueidentifier = '3A586AD3-2C5C-E911-80D0-00155D020D03'
	IF NOT EXISTS (SELECT [Id] FROM [KRT_Menuk] WHERE [Id]=@menuId)
	BEGIN
		INSERT INTO [KRT_Menuk](Id, Menu_Id_Szulo, Kod, Nev, Funkcio_Id, Modul_Id, Parameter, Stat_id, Sorrend) 
		VALUES(@menuId, 'CCDA184D-7FB8-4BDE-88DB-CD0BCCE6CED2', 'eRecord', 'Tömeges tértivevény érkeztetés', 'BEEF7DEE-EF62-4D46-BAEC-AD920C262E53', 'F7C749E2-E122-4E64-AA41-05A5BE2CD762', 'Tomeges=1', 'A0848405-E664-4E79-8FAB-CFECA4A290AF', 16)
	END
END

GO
-------------- end of BUG 7922

-------------- BUG 6600: NMHH: IRATTAROZAS_JOVAHAGYAS_ENABLED=1
DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = '65A776FF-4862-4A96-8719-3C3E3A5186F4'

--if @org_kod = 'NMHH' and exists (select 1 from KRT_Parameterek where Id = @record_id) 
--BEGIN 
--	Print 'UPDATE - NMHH: IRATTAROZAS_JOVAHAGYAS_ENABLED=1'
--	--UPDATE KRT_Parameterek SET Ertek='1' WHERE Id=@record_id
--END
GO
-------------- end of BUG 6600


---------- BUG 4747: NMHH Elszámoltatási jegyzőkönyv menü javítás
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
		
if @org_kod = 'NMHH' 
BEGIN 
	-- Lekérdezés menüpont megjelenítése
	UPDATE KRT_Menuk SET [ErvVege]='4700-12-31' WHERE [Id]='707EB03A-9FB6-4A12-9C3A-3F24A9F70DA9'

	-- Régi elszámoltatási jegyzőkönyv érvénytelenítése
	UPDATE KRT_Menuk SET [ErvVege]='2019-04-17', [Nev]='Elszámoltatási jegyzőkönyv régi' WHERE [Menu_Id_Szulo]='707EB03A-9FB6-4A12-9C3A-3F24A9F70DA9' AND [Nev] LIKE 'Elszámoltatási jegyz%' AND [Modul_Id]<>'874AB960-D76F-4349-8CC5-D95A400B6335' AND [Funkcio_Id] IS NULL AND [ErvVege]>GetDate()

	-- Átnevezés (nem lehet két ugyanolyan nevű) : Elszámoltatási jegyzokönyv => Elszámoltatási jegyzőkönyv
	UPDATE KRT_Menuk SET [Nev]='Elszámoltatási jegyzőkönyv' WHERE [Menu_Id_Szulo]='707EB03A-9FB6-4A12-9C3A-3F24A9F70DA9' AND [Nev] LIKE 'Elszámoltatási jegyz%' AND [Modul_Id]='874AB960-D76F-4349-8CC5-D95A400B6335' AND [Funkcio_Id] IS NULL AND [ErvVege]>GetDate()
END
GO
---------- end of BUG 4747

---BLG_8126--------------------
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

IF @org_kod = 'NMHH' 
BEGIN 
	Print @org_kod
	UPDATE KRT_Forditasok
		SET Forditas = 'Tárgy'
	WHERE ObjAzonosito = 'BoundField_MJ'
END
---END-BLG_8126----------------
GO

-- BUG_8293
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

IF @org_kod = 'NMHH' 
BEGIN
declare @id uniqueidentifier
set @id = '4C12A702-7AF9-4150-A46B-724326EB26D9'

IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @id)
	BEGIN
		print 'insert'

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'eRecordComponent/KuldKuldemenyFormTab.ascx',
			'labelBeerkezesModja',
			'Küldemény fajtája:'
		)
		PRINT 'INSERTED Komponens = eRecordComponent/KuldKuldemenyFormTab.ascx AND ObjAzonosito = labelBeerkezesModja'
	END
ELSE
	BEGIN
	UPDATE KRT_Forditasok
		SET Forditas = 'Küldemény fajtája:'
	WHERE Komponens = 'eRecordComponent/KuldKuldemenyFormTab.ascx' AND ObjAzonosito = 'labelBeerkezesModja'

	PRINT 'UPDATED Komponens = eRecordComponent/KuldKuldemenyFormTab.ascx AND ObjAzonosito = labelBeerkezesModja'
	END

-------------------------------------------------------------------------------------------------------------

set @id = '99C7A4B3-0566-43C3-9DA7-0097E3A9D892'

IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @id)
	BEGIN
		print 'insert'

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'eRecordComponent/KuldKuldemenyFormTab.ascx',
			'lblKezbesitesModja',
			'Beérkezés módja:'
		)
		
		PRINT 'INSERTED Komponens = eRecordComponent/KuldKuldemenyFormTab.ascx AND ObjAzonosito = lblKezbesitesModja'
	END
ELSE
	BEGIN
	UPDATE KRT_Forditasok
		SET Forditas = 'Beérkezés módja:'
	WHERE Komponens = 'eRecordComponent/KuldKuldemenyFormTab.ascx' AND ObjAzonosito = 'lblKezbesitesModja'

	PRINT 'UPDATED Komponens = eRecordComponent/KuldKuldemenyFormTab.ascx AND ObjAzonosito = lblKezbesitesModja'
	END


------------------------------------------------------------------------------

set @id = 'B833F8B3-7738-43B5-80C6-8CF0D20CD514'

IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @id)
	BEGIN
		print 'insert'

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'EgyszerusitettIktatasForm.aspx',
			'labelBeerkezesModja',
			'Küldemény fajtája:'
		)

		PRINT 'INSERTED Komponens = EgyszerusitettIktatasForm.aspx AND ObjAzonosito = labelBeerkezesModja'
	END
ELSE
	BEGIN
	UPDATE KRT_Forditasok
		SET Forditas = 'Küldemény fajtája:'
	WHERE Komponens = 'EgyszerusitettIktatasForm.aspx' AND ObjAzonosito = 'labelBeerkezesModja'

	PRINT 'UPDATED Komponens = EgyszerusitettIktatasForm.aspx AND ObjAzonosito = labelBeerkezesModja'
	END

-------------------------------------------------------------------------------------------------------------

set @id = 'E3A84248-E844-4C71-8F48-4AFB7DD23399'

IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @id)
	BEGIN
		print 'insert'

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'EgyszerusitettIktatasForm.aspx',
			'lblKezbesitesModja',
			'Beérkezés módja:'
		)
		
		PRINT 'INSERTED Komponens = EgyszerusitettIktatasForm.aspx AND ObjAzonosito = lblKezbesitesModja'
	END
ELSE
	BEGIN
	UPDATE KRT_Forditasok
		SET Forditas = 'Beérkezés módja:'
	WHERE Komponens = 'EgyszerusitettIktatasForm.aspx' AND ObjAzonosito = 'lblKezbesitesModja'

	PRINT 'UPDATED Komponens = EgyszerusitettIktatasForm.aspx AND ObjAzonosito = lblKezbesitesModja'
	END
END


---END-BLG_8293----------------
GO


---BUG_7471 Elektronikus kimenő - PDF rendszerparaméterek létrehozása
---1. paraméter
---ALAIRAS_CSAK_FODOKUMENTUM
---kezdeti érték NMHH = 1, egyébként = 0
DECLARE @Org_id uniqueidentifier
SET @Org_id = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id=@Org_id) 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='05F49BAF-1E21-48B8-95B6-9A919582E4EC') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,@Org_id
		,'05F49BAF-1E21-48B8-95B6-9A919582E4EC'
		,'ALAIRAS_CSAK_FODOKUMENTUM'
		,'0'
		,'1'
		,'Csak a fődokumentum kerüljön aláírásra (1=igen,0=nem)'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org=@Org_id
	--	,Nev='ALAIRAS_CSAK_FODOKUMENTUM'
	--	,Ertek='0'
	--	,Karbantarthato='1'
	--	,Note='Csak a fődokumentum kerüljön aláírásra (1=igen,0=nem)'
	-- WHERE Id=@record_id 
 --END

-- if @org_kod = 'NMHH' 
--BEGIN 
--	Print @org_kod +': ALAIRAS_CSAK_FODOKUMENTUM = 1'
--	UPDATE KRT_Parameterek
--	   SET Ertek='1'
--	   WHERE Id=@record_id
--END


---2. paraméter
---KR_CSAK_ELEKTRONIKUS_IRAT
---kezdeti érték NMHH = 1, egyébként = 0
SET @record_id = (select Id from KRT_Parameterek
			where Id='333E514B-9D50-473A-BA8E-35203660B08D') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,@Org_id
		,'333E514B-9D50-473A-BA8E-35203660B08D'
		,'KR_CSAK_ELEKTRONIKUS_IRAT'
		,'0'
		,'1'
		,'KR kiterjesztésű állomány csak elektronikus iratban lehet csatolmány (1=igen,0=nem)'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org=@Org_id
	--	,Nev='KR_CSAK_ELEKTRONIKUS_IRAT'
	--	,Ertek='0'
	--	,Karbantarthato='1'
	--	,Note='KR kiterjesztésű állomány csak elektronikus iratban lehet csatolmány (1=igen,0=nem)'
	-- WHERE Id=@record_id 
 --END

-- if @org_kod = 'NMHH' 
--BEGIN 
--	Print @org_kod +': KR_CSAK_ELEKTRONIKUS_IRAT = 1'
--	UPDATE KRT_Parameterek
--	   SET Ertek='1'
--	   WHERE Id=@record_id
--END

---KRT_AlairasTipusok rekord beszúrása


SET @record_id = (select Id from KRT_AlairasTipusok
			where Id='CB3DB854-C955-4F57-B719-6BF5F88B98F3') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_AlairasTipusok(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,AlairasKod
		,AlairasMod
		,AlairasSzint
		) values (
		 null
		,null
		,@Org_id
		,'CB3DB854-C955-4F57-B719-6BF5F88B98F3'
		,'Külső rendszerbeli aláírás adminisztrálása'
		,'KR'
		,'E_DET'
		,'1'
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_AlairasTipusok
	 SET 
		Org=@Org_id
		,Nev='Külső rendszerbeli aláírás adminisztrálása'
		,AlairasKod='KR'
		,AlairasMod='E_DET'
		,AlairasSzint='1'
	 WHERE Id=@record_id 
 END

GO
---BUG_7471 end

---B_8521 START

GO

DECLARE @Org_id uniqueidentifier
SET @Org_id = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id=@Org_id) 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_UIMezoObjektumErtekek
			where Id='64348C90-7678-E911-80D3-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 


if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_UIMezoObjektumErtekek(
		 Tranz_id
		,Stat_id
		,Org_Id
		,Id
		,TemplateTipusNev		
		,Nev
		,Alapertelmezett
		,Felhasznalo_Id
		,TemplateXML
		,Publikus
		) values (
		 null
		,null
		,@Org_id
		,'64348C90-7678-E911-80D3-00155D020D17'
		,'HatosagiStatisztikaTargyszavakTemplate'
		,'ASPADO_postai'
		,'0'
		,'A1806670-DC77-E911-80D3-00155D020D17'
		,'<HatosagiStatisztikaTargyszavakTemplate>
		  <Dontest_hozta />
		  <Dontest_hoza_allamigazgatasi>5</Dontest_hoza_allamigazgatasi>
		  <Ugyintezes_idotartama>1</Ugyintezes_idotartama>
		  <Hatarido_tullepes_napokban>0</Hatarido_tullepes_napokban>
		  <Jogorvoslati_eljaras_tipusa />
		  <Jogorv_eljarasban_dontes_tipusa />
		  <Jogorv_eljarasban_dontest_hozta />
		  <Jogorv_eljarasban_dontes_tartalma />
		  <Jogorv_eljarasban_dontes_valtozasa />
		  <Hatosagi_ellenorzes_tortent>0</Hatosagi_ellenorzes_tortent>
		  <Dontes_munkaorak_szama>0,5</Dontes_munkaorak_szama>
		  <Megallapitott_eljarasi_koltseg>370</Megallapitott_eljarasi_koltseg>
		  <Kiszabott_kozigazgatasi_birsag>0</Kiszabott_kozigazgatasi_birsag>
		  <Sommas_eljarasban_hozott_dontes>0</Sommas_eljarasban_hozott_dontes>
		  <Nyolc_napon_belul_lezart_nem_sommas>0</Nyolc_napon_belul_lezart_nem_sommas>
		  <Fuggo_hatalyu_hatarozat>0</Fuggo_hatalyu_hatarozat>
		  <Hatarozat_hatalyba_lepese />
		  <Fuggo_hatalyu_vegzes>0</Fuggo_hatalyu_vegzes>
		  <Vegzes_hatalyba_lepese />
		  <Hatosag_altal_visszafizetett_osszeg>0</Hatosag_altal_visszafizetett_osszeg>
		  <Hatosagot_terhelo_eljarasi_koltseg>0</Hatosagot_terhelo_eljarasi_koltseg>
		  <Felfuggesztett_hatarozat>0</Felfuggesztett_hatarozat>
		  <Dontes_formaja_allamigazgatasi>1</Dontes_formaja_allamigazgatasi>
		</HatosagiStatisztikaTargyszavakTemplate>'
	  ,'0'	
		); 
 END  

GO

DECLARE @Org_id uniqueidentifier
SET @Org_id = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_UIMezoObjektumErtekek
			where Id='3799397E-7678-E911-80D3-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 


if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_UIMezoObjektumErtekek(
		 Tranz_id
		,Stat_id
		,Org_Id
		,Id
		,TemplateTipusNev		
		,Nev
		,Alapertelmezett
		,Felhasznalo_Id
		,TemplateXML
		,Publikus
		) values (
		 null
		,null
		,@Org_id
		,'3799397E-7678-E911-80D3-00155D020D17'
		,'HatosagiStatisztikaTargyszavakTemplate'
		,'ASPADO_nempostai'
		,'0'
		,'A1806670-DC77-E911-80D3-00155D020D17'
		,'<HatosagiStatisztikaTargyszavakTemplate>
		  <Dontest_hozta />
		  <Dontest_hoza_allamigazgatasi>5</Dontest_hoza_allamigazgatasi>
		  <Ugyintezes_idotartama>1</Ugyintezes_idotartama>
		  <Hatarido_tullepes_napokban>0</Hatarido_tullepes_napokban>
		  <Jogorvoslati_eljaras_tipusa />
		  <Jogorv_eljarasban_dontes_tipusa />
		  <Jogorv_eljarasban_dontest_hozta />
		  <Jogorv_eljarasban_dontes_tartalma />
		  <Jogorv_eljarasban_dontes_valtozasa />
		  <Hatosagi_ellenorzes_tortent>0</Hatosagi_ellenorzes_tortent>
		  <Dontes_munkaorak_szama>0,5</Dontes_munkaorak_szama>
		  <Megallapitott_eljarasi_koltseg>0</Megallapitott_eljarasi_koltseg>
		  <Kiszabott_kozigazgatasi_birsag>0</Kiszabott_kozigazgatasi_birsag>
		  <Sommas_eljarasban_hozott_dontes>0</Sommas_eljarasban_hozott_dontes>
		  <Nyolc_napon_belul_lezart_nem_sommas>0</Nyolc_napon_belul_lezart_nem_sommas>
		  <Fuggo_hatalyu_hatarozat>0</Fuggo_hatalyu_hatarozat>
		  <Hatarozat_hatalyba_lepese />
		  <Fuggo_hatalyu_vegzes>0</Fuggo_hatalyu_vegzes>
		  <Vegzes_hatalyba_lepese />
		  <Hatosag_altal_visszafizetett_osszeg>0</Hatosag_altal_visszafizetett_osszeg>
		  <Hatosagot_terhelo_eljarasi_koltseg>0</Hatosagot_terhelo_eljarasi_koltseg>
		  <Felfuggesztett_hatarozat>0</Felfuggesztett_hatarozat>
		  <Dontes_formaja_allamigazgatasi>1</Dontes_formaja_allamigazgatasi>
		</HatosagiStatisztikaTargyszavakTemplate>'
	  ,'0'	
		); 
 END  

GO

DECLARE @Org_id uniqueidentifier
SET @Org_id = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Partnerek
			where Id='9D806670-DC77-E911-80D3-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 


if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Partnerek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Tipus		
		,Nev
		,MinositesKezdDat
		,MinositesVegDat
		,Belso
		,Allapot
		) values (
		 null
		,null
		,@Org_id
		,'9D806670-DC77-E911-80D3-00155D020D17'
		,'20'
		,'ASP.Adó technikai felhasználó'
		,getdate()
		,'4700-12-31 00:00:00.000'
		,'1'
		,'JOVAHAGYANDO' 
		); 
 END  

GO

DECLARE @Org_id uniqueidentifier
SET @Org_id = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Felhasznalok
			where Id='A1806670-DC77-E911-80D3-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

IF @record_id IS NULL 
BEGIN 
	PRINT 'INSERT' 
	INSERT INTO KRT_Felhasznalok (
		 Id
		,Org
		,Partner_id
		,Tipus
		,UserNev
		,Nev
		,JelszoLejaratIdo
		,[System]
		,Engedelyezett
		,Ver
		,ErvKezd
		,ErvVege
		,LetrehozasIdo
	) VALUES (
		 'A1806670-DC77-E911-80D3-00155D020D17'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'9D806670-DC77-E911-80D3-00155D020D17'
		,'Windows'
		,'ASP.Ado'
		,'ASP.Adó technikai felhasználó'
		,CONVERT(datetime, '4700-12-31')
		,null
		,1
		,1
		,GETDATE()
		,CONVERT(datetime, '4700-12-31')
		,GETDATE()
	);
END

GO

declare @org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
declare @csoportId uniqueidentifier = 'A1806670-DC77-E911-80D3-00155D020D17'

IF NOT EXISTS (SELECT 1 FROM KRT_Csoportok WHERE Id = @csoportId)
BEGIN
	print 'INSERT KRT_Csoportok - ASP.Adó technikai felhasználó'

	INSERT INTO KRT_Csoportok
	(
		Id,
		Org,
		Nev,
		Tipus,
		[System],
		Jogalany
	)
	VALUES
	(
		@csoportId,
		@org,
		'ASP.Adó technikai felhasználó',
		'1',
		'1',
		'1'
	)
END

GO

declare @csoportId uniqueidentifier = '38DE439E-9DB1-E611-92C1-0050569A6FBA'
DECLARE @recordId uniqueidentifier = '56BCD606-DD77-E911-80D3-00155D020D17'

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

if @org_kod = 'FPH' 
	SET @csoportId='6F3E0F33-6131-4853-95A6-65AA07F5D876'	   
IF @org_kod = 'BOPMH' 
	SET @csoportId='38DE439E-9DB1-E611-92C1-0050569A6FBA'
IF @org_kod = 'CSPH' 
	SET @csoportId='0DF7EF41-DEE8-E811-80CB-00155D027EA9'	 
	
  	
IF @org_kod != 'NMHH'
BEGIN
	IF NOT EXISTS (SELECT 1 FROM KRT_CsoportTagok WHERE Id = @recordId)
	BEGIN
		print 'INSERT KRT_CsoportTagok - ASP.Adó technikai felhasználó'

		INSERT INTO KRT_CsoportTagok
		(
			Id,
			Csoport_Id,
			Csoport_Id_Jogalany,
			Tipus,
			System
		)
		VALUES
		(
			@recordId,
			@csoportId,
			'A1806670-DC77-E911-80D3-00155D020D17',
			'2',
			'1'
		)
	END 
END

GO

DECLARE @Org_id uniqueidentifier
SET @Org_id = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_PartnerKapcsolatok
			where Id='56BCD606-DD77-E911-80D3-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @ertek nvarchar(100)=''

if @org_kod = 'FPH' 
	SET @ertek='6F3E0F33-6131-4853-95A6-65AA07F5D876'	   
IF @org_kod = 'BOPMH' 
	SET @ertek='38DE439E-9DB1-E611-92C1-0050569A6FBA'
IF @org_kod = 'CSPH' 
	SET @ertek='0DF7EF41-DEE8-E811-80CB-00155D027EA9'	

IF @org_kod != 'NMHH'
BEGIN
	IF @record_id IS NULL 
	BEGIN 
		PRINT 'INSERT' 
		INSERT INTO KRT_PartnerKapcsolatok (
			 Id
			 ,Tipus
			 ,Partner_id
			 ,Partner_id_kapcsolt
			 ,ErvKezd
			 ,ErvVege
		) VALUES (
			 '56BCD606-DD77-E911-80D3-00155D020D17'
			 ,'2'
			 ,'9D806670-DC77-E911-80D3-00155D020D17'
			 ,@ertek
			 ,GETDATE()
			 ,'4700-12-31 00:00:00.000'
		);
	END
END

GO

declare @recordId uniqueidentifier = 'A5806670-DC77-E911-80D3-00155D020D17'
--FEJLESZTO
declare @szerepkorId uniqueidentifier SET @szerepkorId = (
select TOP 1 sz.Id 			
			from  KRT_Szerepkorok sz			
			inner join 	KRT_Parameterek p on sz.Nev=p.Ertek
			where p.Nev='DEFAULT_SZEREPKOR')

IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @recordId)
BEGIN
	print 'INSERT KRT_Felhasznalo_Szerepkor - ASP.ADo technikai - BOPMH'

	INSERT INTO KRT_Felhasznalo_Szerepkor
	(
		Id,
		Felhasznalo_Id,
		Szerepkor_Id
	)
	VALUES
	(
		@recordId,
		'A1806670-DC77-E911-80D3-00155D020D17',
		@szerepkorId
	)
END 

GO
---B_8521 END

 ------------------------------------------------------------------------------------------------------------------------------------------------------------------

 -------------------------------------- BUG_8294 Belső keletkezésű, kimenő irat - KIMENŐ KÜLDEMÉNY FAJTA --------------------------------------------------------------
------------------------------------------------------------------- START --------------------------------------------------------------------------------------------
DECLARE @KodCsoport_Id uniqueidentifier, @record_Id uniqueidentifier, @Org_Id uniqueidentifier

SET @KodCsoport_Id = '11F9DC99-3708-49BE-A5DA-F7B6DB343784'
SET @record_Id = '7AE43D60-1E60-4A0B-8377-E2E00A8FC534'
SET @Org_Id = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'09'
    ,'hivatalos irat 2-20 kg-ig'
	,NULL
    ,'21'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=NULL
    ,Stat_id=NULL
    ,Org=@Org_Id
    ,Id=@record_Id
    ,Kod='09'
    ,Nev='hivatalos irat 2-20 kg-ig'
	,RovidNev=NULL
    ,Sorrend='21'
    ,Modosithato='1'
    WHERE Id=@record_Id
       
  END

SET @record_Id = '653991E5-01C5-414A-8DF4-72152CD396FE'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'11'
    ,'hivatalos irat 20-40 kg-ig'
	,NULL
    ,'22'
    ,'1'
    ); 
     
       
  END 
  ELSE 
  BEGIN 
    Print 'UPDATE'
    UPDATE KRT_KodTarak
    SET 
    KodCsoport_Id=@KodCsoport_Id
    ,Tranz_id=NULL
    ,Stat_id=NULL
    ,Org=@Org_Id
    ,Id=@record_Id
    ,Kod='11'
    ,Nev='hivatalos irat 20-40 kg-ig'
	,RovidNev=NULL
    ,Sorrend='22'
    ,Modosithato='1'
    WHERE Id=@record_Id
       
  END
 
GO 
------------------------------------- BUG_8294 Belső keletkezésű, kimenő irat - KIMENŐ KÜLDEMÉNY FAJTA --------------------------------------------------------------
---------------------------------------------------------------- END ------------------------------------------------------------------------------------------------

 
 --------------- BUG 8751 ----------------------------------- 

UPDATE KRT_Esemenyek
SET Funkcio_Id = 'A5BDC3BD-58EC-42C8-9ABA-0011EC48D9C0'
WHERE ObjTip_Id = '265E5281-EF84-4AB2-BD54-5847CC4DA106'
AND Funkcio_Id = 'A1DFED1D-8F1D-420B-8923-92389A494EE2'

--------------- BUG 8751 -----------------------------------

--------------- BUG 8706 -----------------------------------
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'IX_Allapot' AND object_id = OBJECT_ID('[dbo].[EREC_IraIratok]'))
    BEGIN
		PRINT ''
		PRINT 'EREC_IraIratok IX_Allapot index letrehozása a NEMAKTIV_IRAT_ALLAPOT rsz. parameterben megadott ertekek hatekony szuresere'
		
		
		CREATE NONCLUSTERED INDEX [IX_Allapot]
		ON [dbo].[EREC_IraIratok] ([Allapot])
		INCLUDE ([Id],[UgyUgyIratDarab_Id],[Ugyirat_Id])
		
		PRINT 'Kész'
		PRINT ''
    END
--------------- BUG 8706 -----------------------------------

-- BUG_7099 - Központi irattárnak átadás menüpont
DECLARE @org_kod nvarchar(100) 

declare @id uniqueidentifier
set @id = '4EAC1427-33B4-4461-B92B-D931A9080E83'

IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @id)
	BEGIN
		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'IrattarbaAdasTomegesForm.aspx',
			'labelFeljegyzes',
			'Feljegyzés:'
		)
		PRINT 'INSERTED Komponens = IrattarbaAdasTomegesForm.aspx AND ObjAzonosito = labelFeljegyzes'
	END
ELSE
	BEGIN
	UPDATE KRT_Forditasok
		SET Forditas = 'Feljegyzés:'
	WHERE Komponens = 'IrattarbaAdasTomegesForm.aspx' AND ObjAzonosito = 'labelFeljegyzes'

	PRINT 'UPDATED Komponens = IrattarbaAdasTomegesForm.aspx AND ObjAzonosito = labelFeljegyzes'
	END

-------------------------------------------------------------------------------------------------------------

set @id = '7BB016CE-CA9B-4066-AB9C-454EE267E35A'

IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @id)
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'IrattarbaAdasTomegesForm.aspx',
			'labelMegjegyzes',
			'Megjegyzés:'
		)
		
		PRINT 'INSERTED Komponens = IrattarbaAdasTomegesForm.aspx AND ObjAzonosito = labelMegjegyzes'
	END
ELSE
	BEGIN
	UPDATE KRT_Forditasok
		SET Forditas = 'Megjegyzés:'
	WHERE Komponens = 'IrattarbaAdasTomegesForm.aspx' AND ObjAzonosito = 'labelMegjegyzes'

	PRINT 'UPDATED Komponens = IrattarbaAdasTomegesForm.aspx AND ObjAzonosito = labelMegjegyzes'
	END
-------------------------------------------------------------------------------------------------------------

set @id = '98D24613-60F7-4214-8D2F-5614577D2088'

IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @id)
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'eRecordComponent/UgyiratokList.ascx',
			'BoundField_Megjegyzes',
			'Megjegyzés'
		)
		
		PRINT 'INSERTED Komponens = eRecordComponent/UgyiratokList.ascx AND ObjAzonosito = BoundField_Megjegyzes'
	END
ELSE
	BEGIN
	UPDATE KRT_Forditasok
		SET Forditas = 'Megjegyzés'
	WHERE Komponens = 'eRecordComponent/UgyiratokList.ascx' AND ObjAzonosito = 'BoundField_Megjegyzes'

	PRINT 'UPDATED Komponens = eRecordComponent/UgyiratokList.ascx AND ObjAzonosito = BoundField_Megjegyzes'
	END

	-------------------------------------------------------------------------------------------------------------

set @id = '4530E593-10FB-4217-A42D-4138147D96F1'

IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @id)
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'UgyUgyiratokSearch.aspx',
			'lblMegjegyzes',
			'Megjegyzés'
		)
		
		PRINT 'INSERTED Komponens = UgyUgyiratokSearch.aspx AND ObjAzonosito = lblMegjegyzes'
	END
ELSE
	BEGIN
	UPDATE KRT_Forditasok
		SET Forditas = 'Megjegyzés'
	WHERE Komponens = 'UgyUgyiratokSearch.aspx' AND ObjAzonosito = 'lblMegjegyzes'

	PRINT 'UPDATED Komponens = UgyUgyiratokSearch.aspx AND ObjAzonosito = lblMegjegyzes'
	END

	-------------------------------------------------------------------------------------------------------------

set @id = '5C348D9C-BF52-4185-95A9-A5565B40E10D'

IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @id)
	BEGIN

		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'UgyUgyiratFormTab.ascx',
			'lblUgyiratMegjegyzes',
			'Megjegyzés'
		)
		
		PRINT 'INSERTED Komponens = UgyUgyiratFormTab.ascx AND ObjAzonosito = lblUgyiratMegjegyzes'
	END
ELSE
	BEGIN
	UPDATE KRT_Forditasok
		SET Forditas = 'Megjegyzés'
	WHERE Komponens = 'UgyUgyiratFormTab.ascx' AND ObjAzonosito = 'lblUgyiratMegjegyzes'

	PRINT 'UPDATED Komponens = UgyUgyiratFormTab.ascx AND ObjAzonosito = lblUgyiratMegjegyzes'
	END

---END-BUG_7099----------------
GO


--------------- BLG 6875 -----------------------------------

GO

declare @record_id uniqueidentifier = '092EB4E5-5693-E911-80D6-00155D027E06'
SET @record_id = (select Id from KRT_Parameterek
			where Id='092EB4E5-5693-E911-80D6-00155D027E06') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'092EB4E5-5693-E911-80D6-00155D027E06'
		,'IRATTARIHELY_KAPACITAS'
		,'1'
		,'1'
		,'0 - nem kezeljük a kapacitás értékeket, 1 - kezeljük az irattári hely kapacitás értékeket'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450B510A-7CAA-46B0-83E3-18445C0C53A9'
	--	,Nev='IRATTARIHELY_KAPACITAS'
	--	,Ertek='1'
	--	,Karbantarthato='1'
	--	,Note='0 - nem kezeljük a kapacitás értékeket, 1 - kezeljük az irattári hely kapacitás értékeket'
	-- WHERE Id='092EB4E5-5693-E911-80D6-00155D027E06' 
 --END

GO

--------------- BLG 6875 -----------------------------------

--------------- VÉGE ---------------------------------------
---------------------------------------------------------------Bug_5431----------------------------------------------------------------------------------------------

--Cégkapu cím típusnak el volt írva az Org-ja
UPDATE KRT_KodTarak
set Org='450B510A-7CAA-46B0-83E3-18445C0C53A9'
where Id='0AB74D79-55E2-4881-0A96-68A5A2B73871'
and Org != '450B510A-7CAA-46B0-83E3-18445C0C53A9'

GO

declare @orgKod nvarchar(100)
set @orgKod = (SELECT Kod FROM KRT_Orgok WHERE Id = '450B510A-7CAA-46B0-83E3-18445C0C53A9')

declare @record_id uniqueidentifier
set @record_id = '4F4D8A1B-09CB-48E2-9B96-4134858E492A'

declare @adat nvarchar(max)

IF (@orgKod = 'NMHH')
	SET @adat = '{"VezerloKodCsoportId":"6982f668-c8cd-47a3-ac62-9f16e439473c","FuggoKodCsoportId":"bf162994-ab06-4dbf-aac2-176fafd6c7ef","Items":[{"VezerloKodTarId":"cc0d5f87-279b-45c1-905b-dd721a1fb870","FuggoKodtarId":"0ab74d79-55e2-4881-0a96-68a5a2b73871","Aktiv":true},{"VezerloKodTarId":"cc0d5f87-279b-45c1-905b-dd721a1fb870","FuggoKodtarId":"575d1469-66d9-4898-86f6-eb31a7203feb","Aktiv":true},{"VezerloKodTarId":"cc0d5f87-279b-45c1-905b-dd721a1fb870","FuggoKodtarId":"6f1b8b2b-5f82-245f-6d73-ae6c5d395351","Aktiv":true}],"ItemsNincs":[]}'
ELSE
	SET @adat = ''

IF NOT EXISTS (SELECT 1 FROM KRT_KodtarFuggoseg WHERE Id = @record_id)
BEGIN
	PRINT 'INSERT - KRT_KodtarFuggoseg - KULDEMENY_KULDES_MODJA - CIM_TIPUS'
	INSERT INTO KRT_KodtarFuggoseg
	(Id
	,Org
	,Vezerlo_KodCsoport_Id
	,Fuggo_KodCsoport_Id
	,Adat
	,Aktiv)
	VALUES
	(@record_id
	,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
	,'6982F668-C8CD-47A3-AC62-9F16E439473C' -- KULDEMENY_KULDES_MODJA
	,'BF162994-AB06-4DBF-AAC2-176FAFD6C7EF' -- CIM_TIPUS
	,@adat
	,'1')
END
--ELSE
--BEGIN
--	PRINT 'UPDATE - KRT_KodtarFuggoseg - KULDEMENY_KULDES_MODJA - CIM_TIPUS'
--	UPDATE KRT_KodtarFuggoseg
--		SET Adat = @adat
--	WHERE Id = @record_id
--END

GO

DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = 'BAA16807-8595-45F9-A13F-2F2D27F5B5A3'

IF @org_kod = 'NMHH'
	SET @ertek = '1'
ELSE
	SET @ertek = '0'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - CIM_TIPUS_FILTER_ENABLED - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'CIM_TIPUS_FILTER_ENABLED'
		,@ertek
		,'1'
		,'Ha a paraméter értéke 1, akkor a példány expediálás módja szűri, hogy milyen típusú címek választhatóak a címzett címének.'
		); 
 END

 GO

 declare @orgKod nvarchar(100)
set @orgKod = (SELECT Kod FROM KRT_Orgok WHERE Id = '450B510A-7CAA-46B0-83E3-18445C0C53A9')

declare @record_id uniqueidentifier
set @record_id = '97E2B6BA-2894-4E52-B15C-2A45D3D853E8'

declare @adat nvarchar(max)

IF (@orgKod = 'NMHH')
	SET @adat = '{"VezerloKodCsoportId":"5ee17808-3eee-42cc-86e0-cf3624b95615","FuggoKodCsoportId":"6982f668-c8cd-47a3-ac62-9f16e439473c","Items":[{"VezerloKodTarId":"82f2fafc-a312-4c13-a1ff-e86db76b131f","FuggoKodtarId":"871d6057-a117-4695-8aa2-d6cf89a5ede1","Aktiv":true},{"VezerloKodTarId":"82f2fafc-a312-4c13-a1ff-e86db76b131f","FuggoKodtarId":"42496278-92f6-4c45-96fe-d0356281270a","Aktiv":true},{"VezerloKodTarId":"82f2fafc-a312-4c13-a1ff-e86db76b131f","FuggoKodtarId":"cc0d5f87-279b-45c1-905b-dd721a1fb870","Aktiv":true},{"VezerloKodTarId":"82f2fafc-a312-4c13-a1ff-e86db76b131f","FuggoKodtarId":"3ef9bdb4-117a-e811-80c7-00155d027ea9","Aktiv":true},{"VezerloKodTarId":"82f2fafc-a312-4c13-a1ff-e86db76b131f","FuggoKodtarId":"815f0c4f-b1ef-41e2-a41f-41e2cac6e5eb","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"46be6f86-5a91-e711-80c2-00155d020d7e","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"b147e971-5a91-e711-80c2-00155d020d7e","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"447bb87b-5a91-e711-80c2-00155d020d7e","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"3ef9bdb4-117a-e811-80c7-00155d027ea9","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"072d82f1-0220-448f-9aa8-9e646bde3b8d","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"7eb11332-5a91-e711-80c2-00155d020d7e","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"4411423d-5a91-e711-80c2-00155d020d7e","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"4c95095b-3fbe-4ffe-9be6-e445843071b8","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"14a94d2f-0916-49d5-94b1-ac9eb2bb6392","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"13c07c55-9330-4801-a30d-eaa04e540daa","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"632664b7-c298-4c2c-9f9e-f7122115ea75","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"ed318e7b-2642-4491-809c-5e8ddc38d7a9","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"35654f8c-a8fc-4d64-a46d-82007c4fcfb9","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"482d5aa1-5991-e711-80c2-00155d020d7e","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"f9b80f0d-fe46-4921-8b66-359b0e384aef","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"fd72ac44-c675-4099-9609-8264f12e1109","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"3512557f-3e1b-44ef-b341-54775356a5d7","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"e590c4a5-6bbf-46a9-8933-85a62a9e84a2","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"e7a6fc86-2de6-4a9c-aa70-2bd3c4328b88","Aktiv":true},{"VezerloKodTarId":"22e7ede2-bfca-483a-975b-ffdb17b93272","FuggoKodtarId":"70436f6b-396e-4994-a720-184689023aa1","Aktiv":true}],"ItemsNincs":[]}'
ELSE
	SET @adat = ''

IF NOT EXISTS (SELECT 1 FROM KRT_KodtarFuggoseg WHERE Id = @record_id)
BEGIN
	PRINT 'INSERT - KRT_KodtarFuggoseg - UGYINTEZES_ALAPJA - KULDEMENY_KULDES_MODJA'
	INSERT INTO KRT_KodtarFuggoseg
	(Id
	,Org
	,Vezerlo_KodCsoport_Id
	,Fuggo_KodCsoport_Id
	,Adat
	,Aktiv)
	VALUES
	(@record_id
	,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
	,'5EE17808-3EEE-42CC-86E0-CF3624B95615' -- UGYINTEZES_ALAPJA
	,'6982F668-C8CD-47A3-AC62-9F16E439473C' -- KULDEMENY_KULDES_MODJA
	,@adat
	,'1')
END

GO

DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = '617CA921-D866-4180-B889-57CBC3EF2B95'

IF @org_kod = 'NMHH'
	SET @ertek = '1'
ELSE
	SET @ertek = '0'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA_FILTER_ENABLED - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA_FILTER_ENABLED'
		,@ertek
		,'1'
		,'Ha a paraméter értéke 1, akkor az iratpéldány fajtája alapján függő kódtár vezérli a kiválasztható expediálási módokat.'
		); 
 END

 GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------


 -- BLG_8097 - ORTT iratok átmeneti irattárba adásának támogatása
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='71497519-4169-460E-9147-8C7CA288F757') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Funkciok(
			 Alkalmazas_Id
			,Modosithato
			,Id
			,Kod
			,Nev
			,Leiras
			) values (
			'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,'0'
			,'71497519-4169-460E-9147-8C7CA288F757'
			,'ORTT_atmeneti_irattarozas'
			,'ORTT átmeneti irattározás'
			,'Akinek ez a funkciójoga van, annak az eMigration alszám lista fejlécében megjelenik egy új gomb, "Ál átadás" néven.'
			);
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Funkciok
		 SET 
			Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
			,Modosithato='0'
			,Id='71497519-4169-460E-9147-8C7CA288F757'
			,Kod='ORTT_atmeneti_irattarozas'
			,Nev='ORTT átmeneti irattározás'
			,Leiras='Akinek ez a funkciójoga van, annak az eMigration alszám lista fejlécében megjelenik egy új gomb, "Ál átadás" néven.'
		 WHERE Id=@record_id 
	 END
	 GO

	
Print 'KRT_Szerepkor_Funkcio - hozzarendeles FEJLESZTO-hoz (ORTT_atmeneti_irattarozas)'
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='E4161D99-A592-E911-80D6-00155D027E06') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
		Print 'INSERT' 
		 insert into KRT_Szerepkor_Funkcio(
			 Szerepkor_Id
			,Id
			,Funkcio_Id
			) values (
			 'e855f681-36ed-41b6-8413-576fcb5d1542'
			,'E4161D99-A592-E911-80D6-00155D027E06'
			,'71497519-4169-460E-9147-8C7CA288F757'
			);
END 
ELSE 
BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Szerepkor_Funkcio
		 SET 
			 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
			,Id='E4161D99-A592-E911-80D6-00155D027E06'
			,Funkcio_Id='71497519-4169-460E-9147-8C7CA288F757'
		 WHERE Id=@record_id
END


GO

 -- BLG_8534 - Hatósági statisztika mező megnevezések
Print 'BLG_8534 - Hatósági statisztika mező megnevezések'
Print 'Tárggyszavak átnevezése'

UPDATE EREC_TargySzavak
SET TargySzavak = 'Döntést hozta'
WHERE BelsoAzonosito = 'Dontest_hozta'

UPDATE EREC_TargySzavak
SET TargySzavak = 'Döntés formája'
WHERE BelsoAzonosito = 'Dontes_formaja_allamigazgatasi'


UPDATE EREC_TargySzavak
SET TargySzavak = 'Az érdemi döntéssel kapcsolatos esetleges határidő-túllépés időtartama'
WHERE BelsoAzonosito = 'Hatarido_tullepes_napokban'

UPDATE EREC_TargySzavak
SET TargySzavak = 'Jogorvoslati eljárás során érintett döntés típusa'
WHERE BelsoAzonosito = 'Jogorv_eljarasban_dontes_tipusa'

UPDATE EREC_TargySzavak
SET TargySzavak = 'Jogorvoslati eljárásban született döntést meghozta'
WHERE BelsoAzonosito = 'Jogorv_eljarasban_dontest_hozta'

UPDATE EREC_TargySzavak
SET TargySzavak = 'Jogorvoslati eljárásban született döntést tartalma'
WHERE BelsoAzonosito = 'Jogorv_eljarasban_dontes_tartalma'

UPDATE EREC_TargySzavak
SET TargySzavak = 'Jogorvoslati eljárásban született döntés változása'
WHERE BelsoAzonosito = 'Jogorv_eljarasban_dontes_valtozasa'

UPDATE EREC_TargySzavak
SET TargySzavak = 'Sor került-e hatósági ellenőrzés lefolytatására az adott ügyben'
WHERE BelsoAzonosito = 'Hatosagi_ellenorzes_tortent'

UPDATE EREC_TargySzavak
SET TargySzavak = 'Az adott döntés meghozatalára fordított munkaórák száma'
WHERE BelsoAzonosito = 'Dontes_munkaorak_szama'

UPDATE EREC_TargySzavak
SET TargySzavak = 'Az adott döntésben megállapított eljárási költség összege (Ft-ban)'
WHERE BelsoAzonosito = 'Megallapitott_eljarasi_koltseg'

UPDATE EREC_TargySzavak
SET TargySzavak = 'Az adott döntésben kiszabott közigazgatási bírság összege (Ft-ban)'
WHERE BelsoAzonosito = 'Kiszabott_kozigazgatasi_birsag'

UPDATE EREC_TargySzavak
SET TargySzavak = '8 napon belül lezárt, nem sommás eljárásban hozott döntés(Igen/Nem)'
WHERE BelsoAzonosito = '8napon_belul_lezart_nem_sommas'

UPDATE EREC_TargySzavak
SET TargySzavak = 'Függő hatályú határozat(Igen/Nem)'
WHERE BelsoAzonosito = 'Fuggo_hatalyu_hatarozat'

UPDATE EREC_TargySzavak
SET TargySzavak = 'Függő hatályú végzés(Igen/Nem)'
WHERE BelsoAzonosito = 'Fuggo_hatalyu_vegzes'

UPDATE EREC_TargySzavak
SET TargySzavak = 'Az Ákr.43.§(2)bekezdés a,pontja alapján a hatóság által visszafizetett összeg (Ft-ban)'
WHERE BelsoAzonosito = 'Hatosag_altal_visszafizetett_osszeg'

UPDATE EREC_TargySzavak
SET TargySzavak = 'Az Ákr.43.§(2)bekezdés b,pontja alapján a hatóságot terhelő eljárási költségek összege (Ft-ban)'
WHERE BelsoAzonosito = 'Hatosagot_terhelo_eljarasi_koltseg'

UPDATE EREC_TargySzavak
SET TargySzavak = 'Felfüggesztett határozat(Igen/Nem)'
WHERE BelsoAzonosito = 'Felfuggesztett_hatarozat'

Print 'BLG_8534 - Hatósági statisztika mező megnevezések'
Print 'Tárggyszavak átnevezése --- VÉGE ---'

-- BLG_8534 - Hatósági statisztika mező megnevezések
------------------------- VÉGE -----------------------
GO

---------- BUG_9232 - NMHH-s környezetekben ORG értékek Update-elése -----------------------------
UPDATE [KRT_KodTarak]
SET Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
WHERE Id = '0AB74D79-55E2-4881-0A96-68A5A2B73871' OR 
	  Id = '79E6FB17-5A1B-45A8-9346-CE68B4910252'

Print 'UPDATED ORG -> 450B510A-7CAA-46B0-83E3-18445C0C53A9'
---------- BUG_9232 - NMHH-s környezetekben ORG értékek Update-elése -----------------------------
----------------------------------------- VÉGE ---------------------------------------------------
GO

---------- BUG_9556 - Rendszerparaméter megjegyzése karakterkódolási hibás (9011) -----------------------------
--UPDATE KRT_Parameterek
--SET Note = 'Titkos ügykezelés. 0-nincs; 1-van'
--WHERE Nev = 'TUK'
---------- BUG_9556 - Rendszerparaméter megjegyzése karakterkódolási hibás (9011) -----------------------------
----------------------------------------- VÉGE ---------------------------------------------------
GO

------------------------------------------------------------------------------
--BLG 63
------------------------------------------------------------------------------
DECLARE @record_id uniqueidentifier 
SET @record_id = (select Id from KRT_Parameterek
			      where Id='1CE6E4BC-3CB2-4B2B-AFD6-2B2B5EEEC7D4') -- select newId()

Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 

	insert into KRT_Parameterek(
	Tranz_id
	,Stat_id
	,Org
	,Id
	,Nev
	,Ertek
	,Karbantarthato
	) 
	values 
	(
	 null
	,null
	,'450b510a-7caa-46b0-83e3-18445c0c53a9'
	,'1CE6E4BC-3CB2-4B2B-AFD6-2B2B5EEEC7D4'
	,'PKI_FILETYPES'
	,'PDF'
	,'1'
	); 
END 

go
------------------  BUG_9612 TUK- érkeztetés, Beérkezés módja törölt kódérték -----------------------
 UPDATE KRT_KodTarak
 SET ErvVege = '4700-12-31 00:00:00.000'
 WHERE Id = '3FBF8CB2-E28D-E511-A275-00155D011E21'
 ------------------ BUG_9612 TUK- érkeztetés, Beérkezés módja törölt kódérték ------------------------
 -------------------------------------------- VÉGE ---------------------------------------------------
GO

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- BUG_9248: Csepeli paraméter állítások (+ BUG_9849: csepeli paraméterek értéke helytelen)
-- LEZARAS_ELINTEZES_NELKUL_ENABLED: 0
-- IKTATAS_KOTELEZO_IRATTIPUS: 1
-- IKTATAS_KOTELEZO_IRATUGYINTEZO: 1

--DECLARE @org_kod nvarchar(100) 
--SET @org_kod = (select kod from KRT_Orgok WHERE Id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
	
--DECLARE @mdt datetime = '2019-07-18'
	
--IF (@org_kod = 'CSPH')  
--BEGIN
--	Print 'UPDATE LEZARAS_ELINTEZES_NELKUL_ENABLED: CSPH = 0'
--	UPDATE KRT_Parameterek SET Ertek='0', ModositasIdo=@mdt WHERE Id='F30C0BC5-3B3F-464C-8D5C-C71EF3B0A811' AND (ModositasIdo IS NULL OR ModositasIdo < @mdt)
	 
--	Print 'UPDATE IKTATAS_KOTELEZO_IRATTIPUS: CSPH = 1'
--	UPDATE KRT_Parameterek SET Ertek='1', ModositasIdo=@mdt WHERE Id='7203C38A-FE6A-E811-80CB-00155D020DD3' AND (ModositasIdo IS NULL OR ModositasIdo < @mdt)
	
--	Print 'UPDATE IKTATAS_KOTELEZO_IRATUGYINTEZO: CSPH = 1'
--	UPDATE KRT_Parameterek SET Ertek='1', ModositasIdo=@mdt WHERE Id='EB50F352-FE6A-E811-80CB-00155D020DD3' AND (ModositasIdo IS NULL OR ModositasIdo < @mdt)
--END
--GO

-- BUG_6165
-- eAdmin\Funkcionális jogok\Menü karbantartása menüpont elrejtése felületről.

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Menuk
where Id='C108BBD7-B467-47D0-B8F2-95255D665922' and ErvVege > GETDATE()) 

DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
if ((@org_kod != 'NMHH') AND  (@isTUK != '1')) 
BEGIN
	if @record_id IS NOT NULL 
	BEGIN	
	update KRT_Menuk set ErvVege = '4700-12-31 00:00:00.000' where Id = 'C108BBD7-B467-47D0-B8F2-95255D665922'	
	END 
END
else
BEGIN
if @record_id IS NOT NULL 
	BEGIN
	print 'eAdmin\Funkcionális jogok\Menü karbantartása menüpont elrejtése felületről.'
	update KRT_Menuk set ErvVege = GETDATE() where Id = 'C108BBD7-B467-47D0-B8F2-95255D665922'	
	END 
END

GO



--BUG 5105: Hibás karakterkódolás.
--
--UPDATE funkció
PRINT 'BUG 5105: Hibás karakterkódolás. - START' 

IF EXISTS (select 1 from [KRT_Funkciok] where Id = '2a61bdb1-35e2-4cb3-8cd0-4fa02fbeecc8')
	UPDATE [dbo].[KRT_Funkciok]
	SET [Nev] = 'Partner jóváhagyása',
	    [Leiras] = 'Az érkeztetés, iktatás közben felvételre került partnerek utólagosan validálása.'
	WHERE Id = '2a61bdb1-35e2-4cb3-8cd0-4fa02fbeecc8'

PRINT 'BUG 5105: Hibás karakterkódolás. - END' 
--BUG 5105: Hibás karakterkódolás - END.--

GO


-------------------------------------------------------------------------------
-- BLG 7070 - Hianyzó funkciógomb
-- SCAN_OCR_ENABLED rendszer paraméter értékét NMHH esetén állítsuk be 1-esre a post deployment scriptben.
-- SCAN_OCR_ENABLED rendszerparameter	 
-- értékei
-- 1; igen (be van jelölve a chekbox)
-- 0; nem (nincs bejelölve a chekbox)
--NMHH ban  1-re

DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

--NMHH-ban 1 az érték
--if ((@org_kod = 'NMHH')) 
--BEGIN
			
--	DECLARE @RECORD_ID_OCR UNIQUEIDENTIFIER
--	SET @RECORD_ID_OCR = 'B5965DFE-F931-430B-8DB9-2D8A791CE130'

--	Print '@RECORD_ID_OCR='+convert(NVARCHAR(100),@RECORD_ID_OCR) 

--	if @RECORD_ID_OCR IS NOT NULL 
--	BEGIN  
--		 Print 'UPDATE'
--		 UPDATE KRT_Parameterek
--		 SET 
--			Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
--			,Id=@RECORD_ID_OCR
--			,Nev='SCAN_OCR_ENABLED'
--			,Ertek='1'
--			,Karbantarthato='1'
--			,Note='0 - a feltöltött fájlok utólag nem OCR-ezhetők, 
--			  1 - a feltöltött fájlok OCR-ezése utólag indítható'
--		 WHERE Id=@RECORD_ID_OCR 
--	 END
-- END
 --------------------------------------------------------------------------------
 -------------------- BLG 7070 - Hianyzó funkciógomb -------------------------


GO
-----------9128---------------
declare @id uniqueidentifier
set @id = 'A7822E99-2393-49B4-8A12-0158C13D2950'

IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @id)
	BEGIN
		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'eBeadvanyokForm.aspx',
			'labelKR_DokTipusHivatal',
			'Dokumentum típus hivatal:'
		)
		PRINT 'INSERTED Komponens = eBeadvanyokForm.aspx AND ObjAzonosito = labelKR_DokTipusHivatal'
	END
ELSE
	BEGIN
	UPDATE KRT_Forditasok
		SET Forditas = 'Dokumentum típus hivatal:'
	WHERE Komponens = 'eBeadvanyokForm.aspx' AND ObjAzonosito = 'labelKR_DokTipusHivatal'

	PRINT 'UPDATED Komponens = eBeadvanyokForm.aspx AND ObjAzonosito = labelKR_DokTipusHivatal'
	END
GO

declare @id uniqueidentifier
set @id = 'E404F5C8-455F-424F-83D0-8D63228469F8'

IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @id)
	BEGIN
		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'eBeadvanyokForm.aspx',
			'labelKR_DokTipusAzonosito',
			'Dokumentum típus azonosító:'
		)
		PRINT 'INSERTED Komponens = eBeadvanyokForm.aspx AND ObjAzonosito = labelKR_DokTipusAzonosito'
	END
ELSE
	BEGIN
	UPDATE KRT_Forditasok
		SET Forditas = 'Dokumentum típus azonosító:'
	WHERE Komponens = 'eBeadvanyokForm.aspx' AND ObjAzonosito = 'labelKR_DokTipusAzonosito'

	PRINT 'UPDATED Komponens = eBeadvanyokForm.aspx AND ObjAzonosito = labelKR_DokTipusAzonosito'
	END
GO

declare @id uniqueidentifier
set @id = '2FCC8562-F2AF-40D7-AD15-1F980B694C27'

IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @id)
	BEGIN
		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'eBeadvanyokForm.aspx',
			'labelKR_DokTipusLeiras',
			'Dokumentum típus leírás:'
		)
		PRINT 'INSERTED Komponens = eBeadvanyokForm.aspx AND ObjAzonosito = labelKR_DokTipusLeiras'
	END
ELSE
	BEGIN
	UPDATE KRT_Forditasok
		SET Forditas = 'Dokumentum típus leírás:'
	WHERE Komponens = 'eBeadvanyokForm.aspx' AND ObjAzonosito = 'labelKR_DokTipusLeiras'

	PRINT 'UPDATED Komponens = eBeadvanyokForm.aspx AND ObjAzonosito = labelKR_DokTipusLeiras'
	END
GO


------------------------------------------------------------------------------------
-- BUG_9177: Lekérdezések/Statisztikák/Időszaki ügyiratforgalmi statisztika menüpont nevének javítása
UPDATE [dbo].[KRT_Menuk]
SET Nev = 'Időszaki ügyiratforgalmi statisztika'
WHERE Id = 'B321E85F-02FF-4432-892A-7BE4C4109F89'
------------------------------------------------------------------------------------
-- BUG_9177: Lekérdezések/Statisztikák/Időszaki ügyiratforgalmi statisztika menüpont nevének javítása
-- VÉGE --

--- BUG_9177 - Lekérdezések ---
--Kivesszük a Lekérdezések/Statisztikák menüből a 
--'F336B2D0-3471-47AB-BF87-87A29C489FDF' - Ügytípusonként ügyintézésre fordított idő - Visszarakva 2019.09.19.-én [LZS]
--'777A1C1E-5CFB-4C3C-A541-A4790788D885' - Hatósági statisztikák
--'C6FC48A4-4BA7-4609-929C-5293550066FB' - Vezetői statisztikák (Word) - LZS - Visszarakva 2019.09.26.án BUG_10249
--menüpontokat.
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

IF(@org_kod = 'NMHH')
BEGIN
UPDATE KRT_Menuk
SET ErvVege = GETDATE()
WHERE Id in ('F336B2D0-3471-47AB-BF87-87A29C489FDF','777A1C1E-5CFB-4C3C-A541-A4790788D885','C6FC48A4-4BA7-4609-929C-5293550066FB')
and ErvVege > GETDATE()
END
ELSE -- BUG_10472
BEGIN
	UPDATE KRT_Menuk
		SET ErvVege = '4700-12-31'
	WHERE Id in ('F336B2D0-3471-47AB-BF87-87A29C489FDF','777A1C1E-5CFB-4C3C-A541-A4790788D885','C6FC48A4-4BA7-4609-929C-5293550066FB')
	AND ErvVege < '4700-12-31'
END

--'F336B2D0-3471-47AB-BF87-87A29C489FDF' - Ügytípusonként ügyintézésre fordított idő - Visszarakva 2019.09.19.-én [LZS]
IF(@org_kod = 'NMHH')
BEGIN
	UPDATE KRT_Menuk
	SET ErvVege = '4700-12-31'
	WHERE Id in ('F336B2D0-3471-47AB-BF87-87A29C489FDF')
	and ErvVege < '4700-12-31'
END


--- BUG_10249 - LZS - Visszarakva 2019.09.26.án
IF(@org_kod = 'NMHH')
BEGIN
	UPDATE KRT_Menuk
    SET ErvVege = '4700-12-31', Nev = 'Vezetői statisztikák'
	WHERE Id in ('C6FC48A4-4BA7-4609-929C-5293550066FB')
	--AND ErvVege < '4700-12-31'
END
--- BUG_9177 - Lekérdezések ---


GO

----------------------------------4788 start---------------------------------------

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok WHERE Id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
DECLARE @ertek	nvarchar(1) 
IF (@org_kod = 'NMHH')  
BEGIN	
	SET @ertek = 'K'
END
ELSE
BEGIN	 
	SET @ertek = 'A'
END

DECLARE @record_id uniqueidentifier  
SET @record_id = 'BC19300C-80B7-E911-80DB-00155D027E1B'
SET @record_id = (select Id from KRT_Parameterek
			where Id='BC19300C-80B7-E911-80DB-00155D027E1B') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'BC19300C-80B7-E911-80DB-00155D027E1B'
		,'POSTAI_RAGSZAM_KIOSZTAS_SORREND'
		,@ertek
		,'1'
		,'Ragszámkiosztás sorrendje. A - objektum azonosító (iktatószám, stb.), K - kattintás sorrendjében'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450B510A-7CAA-46B0-83E3-18445C0C53A9'
	--	,Nev='POSTAI_RAGSZAM_KIOSZTAS_SORREND'
	--	,Ertek=@ertek
	--	,Karbantarthato='1'
	--	,Note='Ragszámkiosztás sorrendje. A - objektum azonosító (iktatószám, stb.), K - kattintás sorrendjében'
	-- WHERE Id='BC19300C-80B7-E911-80DB-00155D027E1B' 
 --END

GO

----------------------------------4788 end-----------------------------------------

----------------------------------7061 start-----------------------------------------

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Modulok
			where Id='ADE8DB20-36BF-E911-80DB-00155D027E1B') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
	BEGIN 
		
Print 'INSERT' 
		 insert into KRT_Modulok(			 
			Id
			,Tipus
			,Kod
			,Nev
			,Leiras
			) values (			 
			'ADE8DB20-36BF-E911-80DB-00155D027E1B'
			,'F'
			,'SoapMessageForm.aspx'
			,'Soap üzenetek megtekintése'
			,null
			); 
	 END 	
	 go

----------------------------------7061 end-----------------------------------------

----------------------------------10574 start-----------------------------------------

GO
 
 DECLARE @ORG UNIQUEIDENTIFIER
SET @ORG = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @KodCsoport_Id_SYNC uniqueidentifier
SET @KodCsoport_Id_SYNC = 'A82DDBBF-69CD-E911-80DF-00155D017B07'

IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = @KodCsoport_Id_SYNC)
	BEGIN 		
		insert into KRT_KodCsoportok(
		 BesorolasiSema
		,Id
		,Kod
		,Nev
		,Modosithato
		,Note
		) values (
		'0'
		,@KodCsoport_Id_SYNC
		,'ASPADO_ReceivingInformation'
		,'ASP.ADÓ Tértivevény kézbesítés eredménye'
		,'1'
		,NULL
		); 
	END

GO


declare @record_id uniqueidentifier
set @record_id = '103B4FC2-B36C-49BD-B956-12181BD74E98'

declare @adat nvarchar(max)


SET @adat = '{"VezerloKodCsoportId":"a82ddbbf-69cd-e911-80df-00155d017b07","FuggoKodCsoportId":"0c4ac87c-84fc-4d20-a9f0-3fc519f4d9a1","Items":[],"ItemsNincs":[]}'


IF NOT EXISTS (SELECT 1 FROM KRT_KodtarFuggoseg WHERE Id = @record_id)
BEGIN
	PRINT 'INSERT - KRT_KodtarFuggoseg - ASPADO_ReceivingInformation - TERTIVEVENY_VISSZA_KOD'
	INSERT INTO KRT_KodtarFuggoseg
	(Id
	,Org
	,Vezerlo_KodCsoport_Id
	,Fuggo_KodCsoport_Id
	,Adat
	,Aktiv)
	VALUES
	(@record_id
	,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
	,'A82DDBBF-69CD-E911-80DF-00155D017B07' -- ASPADO_ReceivingInformation
	,'0C4AC87C-84FC-4D20-A9F0-3FC519F4D9A1' -- TERTIVEVENY_VISSZA_KOD
	,@adat
	,'1')
END

GO

DECLARE @KodCsoport_Id uniqueidentifier, @record_Id uniqueidentifier, @Org_Id uniqueidentifier

SET @KodCsoport_Id = 'A82DDBBF-69CD-E911-80DF-00155D017B07'
SET @record_Id = 'A4F4EAD8-20D9-E911-9400-00155D144602'
SET @Org_Id = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'000'
    ,'Irat válaszvra várva'
	,NULL
    ,NULL
    ,'1'
    ); 
END

SET @record_Id = '6FE1C6E3-20D9-E911-9400-00155D144602'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'010'
    ,'Átvétel megtagadva'
	,NULL
    ,NULL
    ,'1'
    ); 
END

SET @record_Id = '0C1887ED-20D9-E911-9400-00155D144602'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'008'
    ,'Bejelentve: meghalt/megszűnt'
	,NULL
    ,NULL
    ,'1'
    ); 
END

SET @record_Id = 'A82F3DF5-20D9-E911-9400-00155D144602'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'005'
    ,'Cím nem azonosítható'
	,NULL
    ,NULL
    ,'1'
    ); 
END

SET @record_Id = '87ECDFFF-20D9-E911-9400-00155D144602'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'009'
    ,'Cím ismeretlen'
	,NULL
    ,NULL
    ,'1'
    ); 
END

SET @record_Id = '026E3E09-21D9-E911-9400-00155D144602'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'001'
    ,'Címzettnek kézbesítve'
	,NULL
    ,NULL
    ,'1'
    ); 
END

SET @record_Id = 'E0B92512-21D9-E911-9400-00155D144602'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'007'
    ,'Elköltözött'
	,NULL
    ,NULL
    ,'1'
    ); 
END

SET @record_Id = '51672020-21D9-E911-9400-00155D144602'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'013'
    ,'Feladó visszakérte'
	,NULL
    ,NULL
    ,'1'
    ); 
END

SET @record_Id = 'BB8DCA27-21D9-E911-9400-00155D144602'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'004'
    ,'Helyettes átvevőnek kézbesítve'
	,NULL
    ,NULL
    ,'1'
    ); 
END

SET @record_Id = 'EF6D9030-21D9-E911-9400-00155D144602'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'011'
    ,'Kézbesítés akadályozott'
	,NULL
    ,NULL
    ,'1'
    ); 
END

SET @record_Id = '58B9FB38-21D9-E911-9400-00155D144602'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'002'
    ,'Közvetlen kézbesítőnek kézbesítve'
	,NULL
    ,NULL
    ,'1'
    ); 
END

SET @record_Id = '18CC1147-21D9-E911-9400-00155D144602'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'003'
    ,'Meghatalmazottnak kézbesítve'
	,NULL
    ,NULL
    ,'1'
    ); 
END

SET @record_Id = '01B05B4F-21D9-E911-9400-00155D144602'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'006'
    ,'Nem kereste'
	,NULL
    ,NULL
    ,'1'
    ); 
END

SET @record_Id = 'C596A557-21D9-E911-9400-00155D144602'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'015'
    ,'Nincs megadva'
	,NULL
    ,NULL
    ,'1'
    ); 
END

SET @record_Id = 'D7BB445E-21D9-E911-9400-00155D144602'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'012'
    ,'Szabálytalan'
	,NULL
    ,NULL
    ,'1'
    ); 
END

SET @record_Id = '70237C68-21D9-E911-9400-00155D144602'

Print '@record_Id='+convert(NVARCHAR(100),@record_id) 
IF NOT EXISTS (SELECT 1  FROM KRT_KodTarak where Id = @record_Id)
BEGIN 
    
    Print 'INSERT' 
    insert into KRT_KodTarak(
    KodCsoport_Id
    ,Tranz_id
    ,Stat_id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,RovidNev
    ,Sorrend
    ,Modosithato
    ) values (
    @KodCsoport_Id
    ,NULL
    ,NULL
    ,@Org_Id
    ,@record_Id
    ,'014'
    ,'Sérült, nem továbbított'
	,NULL
    ,NULL
    ,'1'
    ); 
END

GO


----------------------------------10574 end-----------------------------------------


------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

---------------start 8766 -----------------------------------------------------------
GO

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok WHERE Id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
DECLARE @ertek	nvarchar(1) ='0'
--IF (@org_kod = 'NMHH')  
--BEGIN	
--	SET @ertek = 'K'
--END
--ELSE
--BEGIN	 
--	SET @ertek = 'A'
--END

DECLARE @record_id uniqueidentifier  
SET @record_id = 'C162D996-00D4-E911-80DF-00155D017B07'
SET @record_id = (select Id from KRT_Parameterek
			where Id='C162D996-00D4-E911-80DF-00155D017B07') 

DECLARE @isTUK char(1); 

SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 

IF @isTUK = '1'
	BEGIN
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'C162D996-00D4-E911-80DF-00155D017B07'
		,'TOMEGES_ATVETEL_ENABLED'
		,@ertek
		,'1'
		,'Ha 1, akkor mindig a tömeges átvétel form jön elő átvételkor.'
		); 
 END 
END

GO

-------------------end8766-------------------------------------------------------------------


PRINT ''
PRINT 'POST-DEPLOYMENT SCRIPT TEMPLATE: STOP'
PRINT ''