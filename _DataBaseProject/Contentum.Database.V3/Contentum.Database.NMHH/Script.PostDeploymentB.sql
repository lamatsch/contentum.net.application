﻿/*
Post-Deployment Script Template							
*/
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
GO
PRINT '-----------------------'
PRINT 'PARAMETER_FORRAS_EUZENETSZABALY_TARGY'

DECLARE @KODCSOPORT_ID_5169 UNIQUEIDENTIFIER
SET @KODCSOPORT_ID_5169 = '8FCFCE0E-3319-4538-8CBA-E462D534DC88'

PRINT 'KODCSOPORT'
IF NOT EXISTS (SELECT 1 FROM KRT_KODCSOPORTOK WHERE Id=@KODCSOPORT_ID_5169)
BEGIN 		
	PRINT 'INSERT - KCS - PARAMETER_FORRAS_EUZENETSZABALY_TARGY' 
	INSERT INTO KRT_KodCsoportok(
		 BesorolasiSema
		,Id
		,Kod
		,Nev
		,Modosithato
		,Note
		,Leiras
		) 
	VALUES (
		'0'
		,@KODCSOPORT_ID_5169
		,'PARAMETER_FORRAS_EUZENETSZABALY_TARGY'
		,'PARAMETER_FORRAS_EUZENETSZABALY_TARGY'
		,'0'
		,'E-beadvany szabályok - tárgy mező - paraméter forrás'
		,'E-beadvany szabályok - tárgy mező - paraméter forrás'
		); 
END 
------------------------------------
PRINT 'KODTAR'
IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '09BCE199-8ADD-2290-54A3-0FBFEC9086C6') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id] ,[Org] ,[KodCsoport_Id] ,[Kod] ,[Nev] ,[RovidNev] ,[Egyeb] ,[Modosithato] ,[Sorrend] ,[Note]) VALUES ('09BCE199-8ADD-2290-54A3-0FBFEC9086C6' ,'450B510A-7CAA-46B0-83E3-18445C0C53A9' ,'8FCFCE0E-3319-4538-8CBA-E462D534DC88' ,'TXT' ,'TXT' ,'TXT' ,'Szöveg' ,'0' ,'1' ,'Szöveg' ) END ELSE UPDATE KRT_KodTarak SET Kod='TXT' ,Nev='TXT' ,Egyeb = 'Szöveg' ,Modosithato= '0' WHERE Id='09BCE199-8ADD-2290-54A3-0FBFEC9086C6' 
IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '09BCE199-8ADD-2290-54A3-0FBFEC9086C7') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id] ,[Org] ,[KodCsoport_Id] ,[Kod] ,[Nev] ,[RovidNev] ,[Egyeb] ,[Modosithato] ,[Sorrend] ,[Note]) VALUES ('09BCE199-8ADD-2290-54A3-0FBFEC9086C7' ,'450B510A-7CAA-46B0-83E3-18445C0C53A9' ,'8FCFCE0E-3319-4538-8CBA-E462D534DC88' ,'XML' ,'XML' ,'XML' ,'XML csatolmány mező' ,'0' ,'2' ,'XML csatolmány mező' ) END ELSE UPDATE KRT_KodTarak SET Kod='XML' ,Nev='XML' ,Egyeb = 'XML csatolmány mező' ,Modosithato= '0' WHERE Id='09BCE199-8ADD-2290-54A3-0FBFEC9086C7' 
IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '09BCE199-8ADD-2290-54A3-0FBFEC9086C8') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id] ,[Org] ,[KodCsoport_Id] ,[Kod] ,[Nev] ,[RovidNev] ,[Egyeb] ,[Modosithato] ,[Sorrend] ,[Note]) VALUES ('09BCE199-8ADD-2290-54A3-0FBFEC9086C8' ,'450B510A-7CAA-46B0-83E3-18445C0C53A9' ,'8FCFCE0E-3319-4538-8CBA-E462D534DC88' ,'DB' ,'DB' ,'DB' ,'Adatbázis mező' ,'0' ,'3' ,'Adatbázis mező' ) END ELSE UPDATE KRT_KodTarak SET Kod='DB' ,Nev='DB' ,Egyeb = 'Adatbázis mező' ,Modosithato= '0' WHERE Id='09BCE199-8ADD-2290-54A3-0FBFEC9086C8' 

PRINT '-----------------------'
GO
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

GO
PRINT 'BLG_2163 START' 

DECLARE @org nvarchar(100) 
SET @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
			
DECLARE @record_id_1 uniqueidentifier 
SET @record_id_1 = 'B6AB32F1-1FC3-4418-BB31-56090B3A9D86'

DECLARE @org_kod_2163 nvarchar(100) 
SET @org_kod_2163 = (select kod from KRT_Orgok where id=@org) 

DECLARE @NA_UGYIRAT_ALLAPOT_VALUE	NVARCHAR(MAX)  
DECLARE @NA_IRAT_ALLAPOT_VALUE		NVARCHAR(MAX)  
DECLARE @NA_IRATPLD_ALLAPOT_VALUE	NVARCHAR(MAX)  
DECLARE @NA_EV_VALUE				NVARCHAR(MAX)  

IF @org_kod_2163 = 'NMHH'
	BEGIN
		SET @NA_UGYIRAT_ALLAPOT_VALUE	='90,10'
		SET @NA_IRAT_ALLAPOT_VALUE		='90'
		SET @NA_IRATPLD_ALLAPOT_VALUE	='90'
		SET @NA_EV_VALUE				='2017'
	END
ELSE IF @org_kod_2163 = 'FPH'
	BEGIN
		SET @NA_UGYIRAT_ALLAPOT_VALUE	='90'
		SET @NA_IRAT_ALLAPOT_VALUE		='90'
		SET @NA_IRATPLD_ALLAPOT_VALUE	='90'
		SET @NA_EV_VALUE				='2010'
	END
ELSE IF @org_kod_2163 = 'BOPMH'
	BEGIN
		SET @NA_UGYIRAT_ALLAPOT_VALUE	='90'
		SET @NA_IRAT_ALLAPOT_VALUE		='90'
		SET @NA_IRATPLD_ALLAPOT_VALUE	='90'
		SET @NA_EV_VALUE				='2010'
	END
ELSE IF @org_kod_2163 = 'CSPH'
	BEGIN
		SET @NA_UGYIRAT_ALLAPOT_VALUE	='90,10'
		SET @NA_IRAT_ALLAPOT_VALUE		='90'
		SET @NA_IRATPLD_ALLAPOT_VALUE	='90'
		SET @NA_EV_VALUE				='2017'
	END
ELSE
	BEGIN
		SET @NA_UGYIRAT_ALLAPOT_VALUE	='90'
		SET @NA_IRAT_ALLAPOT_VALUE		='90'
		SET @NA_IRATPLD_ALLAPOT_VALUE	='90'
		SET @NA_EV_VALUE				='2017'
	END



IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE ID = @record_id_1) 
BEGIN 
	Print 'INSERT' 
	 INSERT INTO KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @org
		,@record_id_1
		,'NEMAKTIV_UGYIRAT_ALLAPOT'
		,@NA_UGYIRAT_ALLAPOT_VALUE
		,'1'
		,'Nem aktív Ügyirat állapotok listája. (Vesszővel elválasztva)'
		); 
 END 


 DECLARE @record_id_2 uniqueidentifier 
SET @record_id_2 = 'B6AB32F1-1FC3-4418-BB31-56090B3A9D87'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE ID = @record_id_2) 
BEGIN 
	Print 'INSERT' 
	 INSERT INTO KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @org
		,@record_id_2
		,'NEMAKTIV_IRAT_ALLAPOT'
		,@NA_IRAT_ALLAPOT_VALUE
		,'1'
		,'Nem aktív Irat állapotok listája. (Vesszővel elválasztva)'
		); 
 END 


DECLARE @record_id_3 uniqueidentifier 
SET @record_id_3 = 'B6AB32F1-1FC3-4418-BB31-56090B3A9D88'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE ID = @record_id_3) 
BEGIN 
	Print 'INSERT' 
	 INSERT INTO KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @org
		,@record_id_3
		,'NEMAKTIV_IRATPELDANY_ALLAPOT'
		,@NA_IRATPLD_ALLAPOT_VALUE
		,'1'
		,'Nem aktív Irat példány állapotok listája. (Vesszővel elválasztva)'
		); 
 END 

DECLARE @record_id_4 uniqueidentifier 
SET @record_id_4 = 'B6AB32F1-1FC3-4418-BB31-56090B3A9D89'

IF NOT EXISTS (SELECT 1 FROM KRT_Parameterek WHERE ID = @record_id_4) 
BEGIN 
	Print 'INSERT' 
	 INSERT INTO KRT_Parameterek(
		 Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 @org
		,@record_id_4
		,'NEMAKTIV_EV'
		,@NA_EV_VALUE
		,'1'
		,'Nem aktív év. (Adott év előttiek kiszűrve)'
		); 
 END 


 PRINT 'BLG_2163 STOP' 
 GO

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--GO
--PRINT 'BLG_7318 START' 

--DECLARE @org nvarchar(100) 
--SET @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
			
--DECLARE @org_kod_7318 nvarchar(100) 
--SET @org_kod_7318 = (select kod from KRT_Orgok where id=@org) 

--IF @org_kod_7318 = 'NMHH'
--	BEGIN
--		UPDATE KRT_Parameterek
--		SET Ertek = '1'
--		WHERE Nev = 'IRAT_SZIGNALAS_ELERHETO'
--	END
--ELSE
--	BEGIN
--		UPDATE KRT_Parameterek
--		SET Ertek = '0'
--		WHERE Nev = 'IRAT_SZIGNALAS_ELERHETO'
--	END
GO

------------------------------------------------------------------------------------
----------------------------BUG_7496------------------------------------------------
-- duplikált kódcsoport törlése

DECLARE @KodCsoport_Id uniqueidentifier
set @KodCsoport_Id = 'DB84D958-E369-4B9D-BB20-8464A9CEBF94'

IF EXISTS(select 1 from KRT_KodCsoportok where Kod = 'VISSZAFIZETES_JOGCIME' and Id != @KodCsoport_Id)
BEGIN
	print 'Duplikalt kodcsoport torlese: VISSZAFIZETES_JOGCIME'

	delete from KRT_Kodtarak
	where KodCsoport_id in (select Id from KRT_KodCsoportok where Kod = 'VISSZAFIZETES_JOGCIME' and Id != @KodCsoport_Id)

	delete from KRT_KodCsoportok
    where Kod = 'VISSZAFIZETES_JOGCIME' and Id != @KodCsoport_Id
END

GO
------------------------------------------------------------------------------------
--------------------------BLG_3441--------------------------------------------------
declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '025C2C0E-368D-4E19-A660-36B146F5F6C8'
declare @recordId uniqueidentifier
set @recordId = 'FEF77928-A458-42A1-8A0B-C2FFFA08F9DB'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN
    print 'Add ADATHORDOZO_TIPUSA - Automatikus visszaigazolás' 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'18'
	,'Automatikus visszaigazolás'
	,'1'
	,'18'
	); 
END 

GO
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- BLG_7678
-- PAGE_FELADASIIDO_KOTELEZO rendszerparameter	 

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='8CBDA721-5154-E911-80D0-00155D020D03') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'PAGE_FELADASIIDO_KOTELEZO' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'8CBDA721-5154-E911-80D0-00155D020D03'
		,'PAGE_FELADASIIDO_KOTELEZO'
		,'0'
		,'1'
		,'Küldemény érkeztetéskor és az Egyszerűsített érkeztetés, iktatás képernyőn a Feladási idő kitöltése kötelező-e (1 = igen, 0 = nem)'
		); 
END 
--ELSE 
--BEGIN 	
--	Print 'UPDATE PAGE_FELADASIIDO_KOTELEZO'
--	 UPDATE KRT_Parameterek
--	 SET 
--		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
--		,Id='8CBDA721-5154-E911-80D0-00155D020D03'
--		,Nev='PAGE_FELADASIIDO_KOTELEZO'
--		,Ertek='0'
--		,Karbantarthato='1'
--		,Note='Küldemény érkeztetéskor és az Egyszerűsített érkeztetés, iktatás képernyőn a Feladási idő kitöltése kötelező-e (1 = igen, 0 = nem)'
--	 WHERE Id='8CBDA721-5154-E911-80D0-00155D020D03'
	
--END
--IF (@org_kod = 'CSPH')  
--BEGIN
--	Print 'UPDATE PAGE_FELADASIIDO_KOTELEZO: CSPH = 1'
--	 UPDATE KRT_Parameterek
--	 SET 
--		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
--		,Id='8CBDA721-5154-E911-80D0-00155D020D03'
--		,Nev='PAGE_FELADASIIDO_KOTELEZO'
--		,Ertek='1'
--		,Karbantarthato='1'
--		,Note='Küldemény érkeztetéskor és az Egyszerűsített érkeztetés, iktatás képernyőn a Feladási idő kitöltése kötelező-e (1 = igen, 0 = nem)'
--	 WHERE Id='8CBDA721-5154-E911-80D0-00155D020D03'
--END
GO
------------------------------------------------------------------------------------
-- BUG_7723 SzamlaExport Funkciókód Felvitele

Print 'SzamlaExport Funkcio felvitele'
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
			where Id='1858066E-7E54-E911-80D0-00155D020D03') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id

		,Kod
		,Nev
		) values (
		'20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
		,'0'
		,'1858066E-7E54-E911-80D0-00155D020D03'
		,'SzamlaExport'
		,'Számla export'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Funkciok
	 SET 
		Alkalmazas_Id='20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3'
		,Modosithato='0'
		,Id='1858066E-7E54-E911-80D0-00155D020D03'
		,Kod='SzamlaExport'
		,Nev='Számla export'
	 WHERE Id=@record_id 
 END
go
	
Print 'KRT_Szerepkor_Funkcio - hozzarendeles FEJLESZTO-hoz (SzamlaExport)'
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Szerepkor_Funkcio
			where Id='9EDB76C2-7E54-E911-80D0-00155D020D03') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Szerepkor_Funkcio(
		 Szerepkor_Id
		,Id
		,Funkcio_Id
		) values (
		 'e855f681-36ed-41b6-8413-576fcb5d1542'
		,'9EDB76C2-7E54-E911-80D0-00155D020D03'
		,'1858066E-7E54-E911-80D0-00155D020D03'
		);
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Szerepkor_Funkcio
	 SET 
		 Szerepkor_Id='e855f681-36ed-41b6-8413-576fcb5d1542'
		,Id='9EDB76C2-7E54-E911-80D0-00155D020D03'
		,Funkcio_Id='1858066E-7E54-E911-80D0-00155D020D03'
	 WHERE Id=@record_id
 END
 go

------------------------------------------------------------------------------------

DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = 'BE9A8CA4-8AB5-4739-B600-C1D0B16CC313'

IF @org_kod = 'NMHH'
set @ertek = '1'
else
set @ertek = '0'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - ALSZAM_SZIGNALT_ENABLED  - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'ALSZAM_SZIGNALT_ENABLED'
		,@ertek
		,'1'
		,'Ha 1 az értéke, akkor a belső keletkezésű alszámok állapota automatikusan szignált lesz és ügyirat szignáláskor az 1. alszám állapota is szignáltra vált. '
		); 
 END

 GO
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
DECLARE @record UNIQUEIDENTIFIER
SET @record = '7E77AF27-B2C4-4029-931E-18CB0911B2DE'

IF NOT EXISTS(SELECT 1 FROM KRT_FUNKCIOK WHERE ID= @record)
	BEGIN
		/*-------------------------------------------------------------------------*/
		PRINT 'KRT_Funkcio - Automatikus irat szignálás' 
		/*-------------------------------------------------------------------------*/
		 INSERT INTO KRT_Funkciok(
				 Tranz_id
				,Stat_id
				,Alkalmazas_Id
				,Modosithato
				,Id
				,ObjTipus_Id_AdatElem
				,Muvelet_Id
				,Kod
				,Nev
				) values (
				 'F60AD910-541A-470D-B888-DA5A6A061668'
				,'A0848405-E664-4E79-8FAB-CFECA4A290AF'
				,'272277BB-27B7-4689-BE45-DB5994A0FBA9'
				,'1'
				,@record
				,'AA5E7BBA-96A0-4C17-8709-06A6D297E107'
				,'303334F1-5AB1-4C5B-90A4-4EE0BE066008'
				,'AutomatikusIratSzignalas'
				,'Automatikus irat szignálás'
				);
	END
GO
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
GO
Print 'ePdfTertivevenyKezelesForm - KRT_Szerepkor_Funkcio '
IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = '5BBB3EC2-CFE6-4D76-85F8-83D7BC7003E0')
	INSERT INTO KRT_Szerepkor_Funkcio(Id,Szerepkor_Id,Funkcio_Id) 
	VALUES (     '5BBB3EC2-CFE6-4D76-85F8-83D7BC7003E0'
				,'E855F681-36ED-41B6-8413-576FCB5D1542'
				,'2533E523-4444-5555-9BE0-D477854E1306'
			); 
GO
------------------------------------------------------------------------------------
----------------------------------BUG_7614------------------------------------------
--DECLARE @org nvarchar(100) 
--SET @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

----NEMAKTIV_UGYIRAT_ALLAPOT		
--DECLARE @record_id uniqueidentifier 
--SET @record_id = 'B6AB32F1-1FC3-4418-BB31-56090B3A9D86'

--DECLARE @org_kod nvarchar(100) 
--SET @org_kod = (select kod from KRT_Orgok where id=@org)

--IF @org_kod = 'NMHH'
--BEGIN
--	PRINT 'NEMAKTIV_UGYIRAT_ALLAPOT=''54'',''34'',''20'',''11'',''10'',''55'',''52'',''30'',''13'',''33'',''09'',''31'',''32'',''90'',''56'''
--	UPDATE KRT_Parameterek
--		SET Ertek = '''54'',''34'',''20'',''11'',''10'',''55'',''52'',''30'',''13'',''33'',''09'',''31'',''32'',''90'',''56'''
--	where Id = @record_id
--END

GO
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- BUG 6459 - ügyiratok, iratok, iratpéldányok, kimenő iratpéldányok listáján alapértelmezett rendezési mód
-- LISTA_RENDEZETTSEG_IKTATOSZAM rendszerparameter     
-- ertek: 0,1
-- Default: 1 (NMHH)
-- értéke 1, ha iktatószámra kell rendezni csökkenő sorrendben, 0 ha maradjon a jelenlegi rendezés.

print 'LISTA_RENDEZETTSEG_IKTATOSZAM rendszerparameter'

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='44A438CD-D37D-4181-87E3-C5448D990CF4') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'44A438CD-D37D-4181-87E3-C5448D990CF4'
		,'LISTA_RENDEZETTSEG_IKTATOSZAM'
		,'0'
		,'1'
		,'Ügyiratok, iratok, iratpéldányok, kimenő iratpéldányok listáján alapértelmezett rendezési mód'
		); 
END 
 
--if @org_kod = 'NMHH' 
--BEGIN 
--	Print @org_kod +': LISTA_RENDEZETTSEG_IKTATOSZAM = 1'
--	UPDATE KRT_Parameterek
--	   SET Ertek='1'
--	   WHERE Id='44A438CD-D37D-4181-87E3-C5448D990CF4'
	
--	-- NEMAKTIV_IRAT_ALLAPOT
--	Print @org_kod +': NEMAKTIV_IRAT_ALLAPOT = ''90'',''06'',''08'',''30'',''0'',''1'',''2'',''31'',''32'',''33'',''34'',''35'',''20'''
--	update KRT_Parameterek set ertek='''90'',''06'',''08'',''30'',''0'',''1'',''2'',''31'',''32'',''33'',''34'',''35'',''20''' where id='B6AB32F1-1FC3-4418-BB31-56090B3A9D87'
	
--	-- NEMAKTIV_IRATPELDANY_ALLAPOT
--	Print @org_kod +': NEMAKTIV_IRATPELDANY_ALLAPOT = ''90'',''0'',''06'',''10'',''30'',''31'',''32'',''40'',''41'',''45'',''60'',''61'',''63'',''64'',''33'',''34'',''35'''
--	update KRT_Parameterek set ertek='''90'',''0'',''06'',''10'',''30'',''31'',''32'',''40'',''41'',''45'',''60'',''61'',''63'',''33'',''34'',''35''' where id='B6AB32F1-1FC3-4418-BB31-56090B3A9D88'
--END
GO

-------------- end of BUG 6459
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
DECLARE @recordTomegesOlvasasiJog UNIQUEIDENTIFIER
SET @recordTomegesOlvasasiJog = '920F054B-C12F-4A80-A566-9FAC9BC02CADE'

IF NOT EXISTS(SELECT 1 FROM KRT_FUNKCIOK WHERE ID= @recordTomegesOlvasasiJog)
BEGIN
		PRINT 'KRT_Funkcio - Olvasási jog tömeges megadása' 
		INSERT INTO KRT_Funkciok(
			 Id
			,Kod
			,Nev
			,Alkalmazas_Id
			,Muvelet_Id
			,Modosithato
			,Stat_id
			,Tranz_id
			) values (
			@recordTomegesOlvasasiJog
			,'TomegesOlvasasiJog'
			,'Olvasási jog tömeges megadása'
			,'272277BB-27B7-4689-BE45-DB5994A0FBA9'
			,'01B026C0-25D4-4AE3-B6E2-10CC9A97D679'
			,'1'
			,'A0848405-E664-4E79-8FAB-CFECA4A290AF'
			,'F60AD910-541A-470D-B888-DA5A6A061668'
			);
END

DECLARE @record uniqueidentifier 
SET @record = '259379FA-0EAD-4FF5-AE87-ADC907902687'

if NOT EXISTS (SELECT 1 from KRT_Szerepkor_Funkcio where Id = @record) 
BEGIN 	
		PRINT 'INSERT KRT_Szerepkor_Funkcio - Olvasási jog tömeges megadása - FEJLESZTO' 
		INSERT INTO KRT_Szerepkor_Funkcio(
			Id
			,Funkcio_Id
			,Szerepkor_Id
			) values (
			 @record
			,@recordTomegesOlvasasiJog
			,'e855f681-36ed-41b6-8413-576fcb5d1542'
			);
END

GO
------------------------------------------------------------------------------------
-------------------------------Bug_7131---------------------------------------------
DECLARE @org nvarchar(100) 
SET @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id=@org)

--Egyéb szervezetnek átadott
DECLARE @record_id uniqueidentifier 
SET @record_id = '3C5141D6-9A41-E911-80CC-00155D020D03'



IF @org_kod = 'NMHH'
BEGIN
	IF NOT EXISTS(select 1 from KRT_Csoportok where Id = @record_id)
	BEGIN
		PRINT 'INSERT KRT_Csoportok - Egyéb szervezetnek átadott'

		INSERT INTO KRT_Csoportok
		(Id,
		Org,
		Nev,
		Tipus,
		[System],
		JogosultsagOroklesMod)
		Values
		(@record_Id,
		@org,
		'Egyéb szervezetnek átadott',
		'0',
		'0',
		'1')

		--SPEC_SZERVEK.EGYEBSZERVEZETIRATTAR
		PRINT 'SET SPEC_SZERVEK.EGYEBSZERVEZETIRATTAR - Egyéb szervezetnek átadott'
		UPDATE KRT_KodTarak
		SET Obj_Id = @record_id,
			ModositasIdo = GETDATE(),
			Modosito_id = '54E861A5-36ED-44CA-BAA7-C287D125B309',
			Ver = Ver + 1
		WHERE Id = '3F59FAAD-8391-4841-8745-BF614BD59CFB'
	END


END


GO
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- BUG_3517
-- ITSZ MegorzesiIdo (SELEJTEZESI_IDO Kódtár) Ervkezd Update

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
Declare @org uniqueidentifier
IF (@org_kod = 'NMHH')  
BEGIN
	SET @org ='450B510A-7CAA-46B0-83E3-18445C0C53A9'


	update KRT_KodTarak
	set  ErvKezd = a.itsz_ervkezd
	from  krt_kodtarak
	inner join KRT_KodCsoportok on KRT_KodCsoportok.Kod='SELEJTEZESI_IDO'   
	inner join (select    
			EREC_IraIrattariTetelek.MegorzesiIdo,
			KRT_KodTarak_MegorzesiIdo.Kod,
			KRT_KodTarak_MegorzesiIdo.Nev,
			KRT_KodTarak_MegorzesiIdo.ErvKezd as MegorzesiIdo_ervkezd, 
			KRT_KodTarak_MegorzesiIdo.ErvVege as MegorzesiIdo_ervvege, 
			min(EREC_IraIrattariTetelek.ErvKezd)  as itsz_ervkezd
		FROM
		 EREC_IraIrattariTetelek as EREC_IraIrattariTetelek 
		 left join KRT_KodCsoportok as KRT_KodCsoportok_MegorzesiIdo on KRT_KodCsoportok_MegorzesiIdo.Kod='SELEJTEZESI_IDO'   
		left join KRT_KodTarak as KRT_KodTarak_MegorzesiIdo ON KRT_KodCsoportok_MegorzesiIdo.Id = KRT_KodTarak_MegorzesiIdo.KodCsoport_Id 
															and EREC_IraIrattariTetelek.MegorzesiIdo = KRT_KodTarak_MegorzesiIdo.Kod 
															and KRT_KodTarak_MegorzesiIdo.Org= CAST(@Org as NVarChar(40))
		where EREC_IraIrattariTetelek.ErvKezd <= KRT_KodTarak_MegorzesiIdo.ErvKezd
		 group by EREC_IraIrattariTetelek.MegorzesiIdo,
		 KRT_KodTarak_MegorzesiIdo.Kod,
			KRT_KodTarak_MegorzesiIdo.Nev,
			KRT_KodTarak_MegorzesiIdo.ErvKezd, 
				KRT_KodTarak_MegorzesiIdo.ErvVege  	
		) a on a.MegorzesiIdo= KRT_KodTarak.Kod and KRT_KodTarak.KodCsoport_Id=KRT_KodCsoportok.id
		and KRT_KodTarak.Org= CAST(@Org as NVarChar(40))
		

END
GO

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
----------------------------------BLG_8102------------------------------------------
UPDATE EREC_Obj_MetaDefinicio SET ErvVege='4700-12-31' WHERE DefinicioTipus='HA'
GO
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- BUG_6935: Ügyirat lezárásnál hibaüzenethez Függő kodtár létrehozás
-- 			 POSTAZOTT kódcsoport létrehozása

Print 'POSTAZOTT kódcsoport'

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_KodCsoportok
			where Id='593764D1-4D75-E911-80D3-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
	insert into KRT_KodCsoportok(
	 BesorolasiSema
	,Id
	,Kod
	,Nev
	,Modosithato
	,Note
	) values (
	'0'
	,'593764D1-4D75-E911-80D3-00155D020D17'
	,'POSTAZOTT'
	,'Postázott'
	,'0'
	,'Postázott státusz az iratpéldány állapotokban'
	); 

END 
ELSE 
BEGIN 
	Print 'UPDATE'
	UPDATE KRT_KodCsoportok
	SET 
	BesorolasiSema='0'
	,Kod='POSTAZOTT'
	,Nev='Postázott'
	,Modosithato='0'	
	,Note='Postázott státusz az iratpéldány állapotokban'
	WHERE Id='593764D1-4D75-E911-80D3-00155D020D17'
	
END

SET @record_id= null
SET @record_id = (select Id from KRT_KodCsoportok
			where Id='593764D1-4D75-E911-80D3-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'ERROR: Nincs POSTAZOTT kódcsoport vagy nem jó az ID-ja' 

END 
ELSE 
BEGIN 
    Print 'Igen kódtárérték' 
	SET @record_id= null
    SET @record_id = (select Id from KRT_KodTarak
		where Id='44B1D939-4E75-E911-80D3-00155D020D17') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
		
		Print 'INSERT' 
			insert into KRT_KodTarak(
				KodCsoport_Id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			) values (
			'593764D1-4D75-E911-80D3-00155D020D17'
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'44B1D939-4E75-E911-80D3-00155D020D17'
			,'IGEN'
			,'Igen'
			,'Igen'
			,'0'
			,'1'
			); 
		END 
		ELSE 
		BEGIN 
			Print 'UPDATE'
			UPDATE KRT_KodTarak
			SET 
			 KodCsoport_Id='593764D1-4D75-E911-80D3-00155D020D17'
			,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Kod='IGEN'
			,Nev='IGEN'
			,RovidNev='Igen'
			,Modosithato='0'
			,Sorrend='1'
			WHERE Id='44B1D939-4E75-E911-80D3-00155D020D17'
	END
	-- Kódtár Függőség összerendelés POSTAZOTT - Iratpéldány állapota
	
	IF NOT EXISTS (SELECT 1 FROM KRT_KODTARFUGGOSEG WHERE ID='8CEEA6A9-0179-4C43-BB88-E67DC1BB24D2')
	BEGIN
		PRINT 'Kódtár Függőség összerendelés POSTAZOTT - Iratpéldány állapota'
		INSERT [dbo].[KRT_KodtarFuggoseg] ([Id], [Org], [Vezerlo_KodCsoport_Id], [Fuggo_KodCsoport_Id], [Adat], [Aktiv]) 
		VALUES (N'8CEEA6A9-0179-4C43-BB88-E67DC1BB24D2', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'664B8A53-9B2C-4B1B-B279-9765DE1C31A9', N'593764D1-4D75-E911-80D3-00155D020D17', N'{"VezerloKodCsoportId":"664b8a53-9b2c-4b1b-b279-9765de1c31a9","FuggoKodCsoportId":"593764d1-4d75-e911-80d3-00155d020d17","Items":[{"VezerloKodTarId":"08fe89a6-1d24-4ac8-aa07-083950ff4245","FuggoKodtarId":"44b1d939-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"7bab33e1-5635-4441-bc66-878a13be1749","FuggoKodtarId":"44b1d939-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"9f901781-bc3d-48c4-b33f-f7d6a3670145","FuggoKodtarId":"44b1d939-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"fe5d96e1-ce05-49e1-a71e-09282de4d3be","FuggoKodtarId":"44b1d939-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"355775a0-a49a-4195-8dcf-4839cf7580df","FuggoKodtarId":"44b1d939-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"ac156dfd-40d8-4ec6-938a-6138511db92b","FuggoKodtarId":"44b1d939-4e75-e911-80d3-00155d020d17","Aktiv":true}],"ItemsNincs":[]}', N'1')
	END
END


Print 'POSTAZANDO kódcsoport'

SET @record_id= null
SET @record_id = (select Id from KRT_KodCsoportok
			where Id='B049D472-4E75-E911-80D3-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
	insert into KRT_KodCsoportok(
	 BesorolasiSema
	,Id
	,Kod
	,Nev
	,Modosithato
	,Note
	) values (
	'0'
	,'B049D472-4E75-E911-80D3-00155D020D17'
	,'POSTAZANDO'
	,'Postázandó'
	,'0'
	,'Postázandó státusz a küldemény küldésmódja mezőben'
	); 

END 
ELSE 
BEGIN 
	Print 'UPDATE'
	UPDATE KRT_KodCsoportok
	SET 
	BesorolasiSema='0'
	,Kod='POSTAZANDO'
	,Nev='Postázandó'
	,Modosithato='0'	
	,Note='Postázandó státusz a küldemény küldésmódja mezőben'
	WHERE Id='B049D472-4E75-E911-80D3-00155D020D17'
	
END

SET @record_id= null
SET @record_id = (select Id from KRT_KodCsoportok
			where Id='B049D472-4E75-E911-80D3-00155D020D17') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'ERROR: Nincs POSTAZANDO kódcsoport vagy nem jó az ID-ja' 

END 
ELSE 
BEGIN 
    Print 'Igen kódtárérték' 
	SET @record_id= null
    SET @record_id = (select Id from KRT_KodTarak
		where Id='578E1AA9-4E75-E911-80D3-00155D020D17') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
		
		Print 'INSERT' 
			insert into KRT_KodTarak(
				KodCsoport_Id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			) values (
			'B049D472-4E75-E911-80D3-00155D020D17'
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'578E1AA9-4E75-E911-80D3-00155D020D17'
			,'IGEN'
			,'Igen'
			,'Igen'
			,'0'
			,'1'
			); 
		END 
		ELSE 
		BEGIN 
			Print 'UPDATE'
			UPDATE KRT_KodTarak
			SET 
			 KodCsoport_Id='B049D472-4E75-E911-80D3-00155D020D17'
			,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Kod='IGEN'
			,Nev='IGEN'
			,RovidNev='Igen'
			,Modosithato='0'
			,Sorrend='1'
			WHERE Id='578E1AA9-4E75-E911-80D3-00155D020D17'
	END
	
	
	-- Kódtár Függőség összerendelés POSTAZANDO - Küldemény küldésmódja
	
	IF NOT EXISTS (SELECT 1 FROM KRT_KODTARFUGGOSEG WHERE ID='595303D3-40EF-469E-9647-F6E8B194995F')
	BEGIN
		PRINT 'Kódtár Függőség összerendelés POSTAZANDO - Küldemény küldésmódja'
		INSERT [dbo].[KRT_KodtarFuggoseg] ([Id], [Org], [Vezerlo_KodCsoport_Id], [Fuggo_KodCsoport_Id], [Adat], [Aktiv]) 
		VALUES (N'595303D3-40EF-469E-9647-F6E8B194995F', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'6982F668-C8CD-47A3-AC62-9F16E439473C', N'B049D472-4E75-E911-80D3-00155D020D17', N'{"VezerloKodCsoportId":"6982f668-c8cd-47a3-ac62-9f16e439473c","FuggoKodCsoportId":"b049d472-4e75-e911-80d3-00155d020d17","Items":[{"VezerloKodTarId":"46be6f86-5a91-e711-80c2-00155d020d7e","FuggoKodtarId":"578e1aa9-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"b147e971-5a91-e711-80c2-00155d020d7e","FuggoKodtarId":"578e1aa9-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"447bb87b-5a91-e711-80c2-00155d020d7e","FuggoKodtarId":"578e1aa9-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"7eb11332-5a91-e711-80c2-00155d020d7e","FuggoKodtarId":"578e1aa9-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"4c95095b-3fbe-4ffe-9be6-e445843071b8","FuggoKodtarId":"578e1aa9-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"14a94d2f-0916-49d5-94b1-ac9eb2bb6392","FuggoKodtarId":"578e1aa9-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"13c07c55-9330-4801-a30d-eaa04e540daa","FuggoKodtarId":"578e1aa9-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"632664b7-c298-4c2c-9f9e-f7122115ea75","FuggoKodtarId":"578e1aa9-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"ed318e7b-2642-4491-809c-5e8ddc38d7a9","FuggoKodtarId":"578e1aa9-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"35654f8c-a8fc-4d64-a46d-82007c4fcfb9","FuggoKodtarId":"578e1aa9-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"578e1aa9-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"3512557f-3e1b-44ef-b341-54775356a5d7","FuggoKodtarId":"578e1aa9-4e75-e911-80d3-00155d020d17","Aktiv":true},{"VezerloKodTarId":"70436f6b-396e-4994-a720-184689023aa1","FuggoKodtarId":"578e1aa9-4e75-e911-80d3-00155d020d17","Aktiv":true}],"ItemsNincs":[]}', N'1')
	END
END

go

------------------------------------------------------------------------------------
GO
 -----KULDEMENY_KULDES_MODJA.Elhisz atnevezes Adatkapu-----
DECLARE @org_kod_E nvarchar(100) 
SET @org_kod_E = (select kod from KRT_Orgok	where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @recordId_ELHISZ uniqueidentifier 
SET @recordId_ELHISZ= 'CC0D5F87-279B-45C1-905B-DD721A1FB870'
IF (@org_kod_E = 'NMHH')
	UPDATE KRT_KodTarak SET Nev = 'elektronikus úton (Adatkapu)' WHERE Id = @recordId_ELHISZ

GO
------------------------------------------------------------------------------------
-- BUG_8507 Felülvizsgáló címke fordítása

declare @id uniqueidentifier

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
if  (@isTUK = '1')
BEGIN

	print 'labelUserNev'
	set @id = '06EAB3CB-AE77-E911-80D3-00155D020D17'
	if not exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN
		print 'insert FelulvizsgalatForm Forditas'
		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			@org_kod,
			'eRecord',
			'FelulvizsgalatForm.aspx',
			'labelUserNev',
			'Felülvizsgálati ügyintéző:'
		)
	END
	
	set @id = '07EAB3CB-AE77-E911-80D3-00155D020D17'
	if not exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN
		print 'insert MinositesFelulvizsgalatTab Forditas'
		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			@org_kod,
			'eRecord',
			'MinositesFelulvizsgalatTab.ascx',
			'labelUserNev',
			'Felülvizsgálati ügyintéző:'
		)
	END
	
	set @id = '08EAB3CB-AE77-E911-80D3-00155D020D17'
	if not exists (select 1 from KRT_Forditasok where Id =@id)
	BEGIN
		print 'insert KozpontiIrattarForm Forditas'
		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			@org_kod,
			'eRecord',
			'KozpontiIrattarForm.aspx',
			'labelUserNev',
			'Felülvizsgálati ügyintéző:'
		)
	END
END

GO
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- BUG_8360 Alkalmazásgazdák csoport

DECLARE @alkalmazasgazdakCsoportId uniqueidentifier = '28A777B5-1EBB-415A-BD60-CEFEA9F8DDE9'
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9')

IF @org_kod = 'NMHH'
BEGIN
	IF NOT EXISTS (SELECT 1 FROM KRT_Csoportok WHERE Id = @alkalmazasgazdakCsoportId)
	BEGIN
		print 'INSERT KRT_Csoportok - Alkalmazásgazdák'

		INSERT INTO KRT_Csoportok
		(
			Id,
			Org,
			Nev,
			Tipus,
			[System]
		)
		VALUES
		(
			@alkalmazasgazdakCsoportId,
			'450B510A-7CAA-46B0-83E3-18445C0C53A9',
			'Alkalmazásgazdák',
			'R',
			'1'	
		)
	END
	
	DECLARE @csoportTagId uniqueidentifier = '825AFEEE-CA20-4953-B03B-163659118AB8'
	
	IF NOT EXISTS (SELECT 1 FROM KRT_CsoportTagok WHERE Id = @csoportTagId)
	BEGIN
		print 'INSERT KRT_CsoportTagok - Adminisztrátor'

		INSERT INTO KRT_CsoportTagok
		(
			Id,
			Csoport_Id,
			Csoport_Id_Jogalany,
			Tipus,
			System
		)
		VALUES
		(
			@csoportTagId,
			@alkalmazasgazdakCsoportId,
			'54E861A5-36ED-44CA-BAA7-C287D125B309',
			'2',
			'1'
		)
	END 
END
GO
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- BUG_8613: KIMENOKULDEMENY_SULY kódcsoport és függő kódtár 

Print 'KIMENOKULDEMENY_SULY  kódcsoport'

DECLARE @record_id uniqueidentifier
DECLARE @KodCsopId uniqueidentifier 
SET @KodCsopId ='0AE318FC-DE85-E911-80D6-00155D027E06'
SET @record_id = NULL
SET @record_id = (select Id from KRT_KodCsoportok where Id=@KodCsopId) 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
	insert into KRT_KodCsoportok(
	 BesorolasiSema
	,Id
	,Kod
	,Nev
	,Modosithato
	,Note
	) values (
	'0'
	,@KodCsopId
	,'KIMENOKULDEMENY_SULY'
	,'Kimenő küldemények súlya'
	,'1'
	,'Felhasználás: Kimenő küldemények súlya eFeladójegyzék készítésénél'
	); 

END 
ELSE 
BEGIN 
	Print 'UPDATE'
	UPDATE KRT_KodCsoportok
	SET 
	BesorolasiSema='0'
	,Kod='KIMENOKULDEMENY_SULY'
	,Nev='Kimenő küldemények súlya'
	,Modosithato='0'	
	,Note='Felhasználás: Kimenő küldemények súlya eFeladójegyzék készítésénél'
	WHERE Id=@KodCsopId
	
END

print 'Kódtárértékek felvitele:'
print '20 gramm'
SET @record_id = NULL
SET @record_id = (select Id from KRT_KodTarak
	where Id='03236C53-E085-E911-80D6-00155D027E06') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 		
	Print 'INSERT' 
	insert into KRT_KodTarak(
	KodCsoport_Id
	,Org
	,Id
	,Kod
	,Nev
	,RovidNev
	,Modosithato
	,Sorrend
	, ervkezd
	) values (
	@KodCsopId
	,'450b510a-7caa-46b0-83e3-18445c0c53a9'
	,'03236C53-E085-E911-80D6-00155D027E06'
	,'20'
	,'20 gramm'
	,'20 gramm'
	,'0'
	,'01'
	,'1900.01.01'
	); 
END 

print '30 gramm'
SET @record_id = NULL
SET @record_id = (select Id from KRT_KodTarak
	where Id='04236C53-E085-E911-80D6-00155D027E06') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 		
	Print 'INSERT' 
	insert into KRT_KodTarak(
		KodCsoport_Id
	,Org
	,Id
	,Kod
	,Nev
	,RovidNev
	,Modosithato
	,Sorrend
	,ervkezd
	) values (
	@KodCsopId
	,'450b510a-7caa-46b0-83e3-18445c0c53a9'
	,'04236C53-E085-E911-80D6-00155D027E06'
	,'30'
	,'30 gramm'
	,'30 gramm'
	,'0'
	,'02'
	,'1900.01.01'
	); 
END 

print '50 gramm'
SET @record_id = NULL
SET @record_id = (select Id from KRT_KodTarak
	where Id='05236C53-E085-E911-80D6-00155D027E06') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 		
	Print 'INSERT' 
	insert into KRT_KodTarak(
		KodCsoport_Id
	,Org
	,Id
	,Kod
	,Nev
	,RovidNev
	,Modosithato
	,Sorrend
	,ervkezd
	) values (
	@KodCsopId
	,'450b510a-7caa-46b0-83e3-18445c0c53a9'
	,'05236C53-E085-E911-80D6-00155D027E06'
	,'50'
	,'50 gramm'
	,'50 gramm'
	,'0'
	,'03'
	,'1900.01.01'
	); 
END 

print '100 gramm'
SET @record_id = NULL
SET @record_id = (select Id from KRT_KodTarak
	where Id='06236C53-E085-E911-80D6-00155D027E06') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 		
	Print 'INSERT' 
	insert into KRT_KodTarak(
		KodCsoport_Id
	,Org
	,Id
	,Kod
	,Nev
	,RovidNev
	,Modosithato
	,Sorrend
	,ervkezd
	) values (
	@KodCsopId
	,'450b510a-7caa-46b0-83e3-18445c0c53a9'
	,'06236C53-E085-E911-80D6-00155D027E06'
	,'100'
	,'100 gramm'
	,'100 gramm'
	,'0'
	,'04'
	,'1900.01.01'
	); 
END 

print '250 gramm'
SET @record_id = NULL
SET @record_id = (select Id from KRT_KodTarak
	where Id='07236C53-E085-E911-80D6-00155D027E06') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 		
	Print 'INSERT' 
	insert into KRT_KodTarak(
		KodCsoport_Id
	,Org
	,Id
	,Kod
	,Nev
	,RovidNev
	,Modosithato
	,Sorrend
	,ervkezd
	) values (
	@KodCsopId
	,'450b510a-7caa-46b0-83e3-18445c0c53a9'
	,'07236C53-E085-E911-80D6-00155D027E06'
	,'250'
	,'250 gramm'
	,'250 gramm'
	,'0'
	,'05'
	,'1900.01.01'
	); 
END 

print '500 gramm'
SET @record_id = NULL
SET @record_id = (select Id from KRT_KodTarak
	where Id='08236C53-E085-E911-80D6-00155D027E06') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 		
	Print 'INSERT' 
	insert into KRT_KodTarak(
	KodCsoport_Id
	,Org
	,Id
	,Kod
	,Nev
	,RovidNev
	,Modosithato
	,Sorrend
	,ervkezd
	) values (
	@KodCsopId
	,'450b510a-7caa-46b0-83e3-18445c0c53a9'
	,'08236C53-E085-E911-80D6-00155D027E06'
	,'500'
	,'500 gramm'
	,'500 gramm'
	,'0'
	,'06'
	,'1900.01.01'
	); 
END 

print '750 gramm'
SET @record_id = NULL
SET @record_id = (select Id from KRT_KodTarak
	where Id='879D6AB6-E485-E911-80D6-00155D027E06') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 		
	Print 'INSERT' 
	insert into KRT_KodTarak(
	KodCsoport_Id
	,Org
	,Id
	,Kod
	,Nev
	,RovidNev
	,Modosithato
	,Sorrend
	,ervkezd
	) values (
	@KodCsopId
	,'450b510a-7caa-46b0-83e3-18445c0c53a9'
	,'879D6AB6-E485-E911-80D6-00155D027E06'
	,'750'
	,'750 gramm'
	,'750 gramm'
	,'0'
	,'07'
	,'1900.01.01'
	); 
END 

print '1000 gramm'
SET @record_id = NULL
SET @record_id = (select Id from KRT_KodTarak
	where Id='09236C53-E085-E911-80D6-00155D027E06') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 		
	Print 'INSERT' 
	insert into KRT_KodTarak(
	KodCsoport_Id
	,Org
	,Id
	,Kod
	,Nev
	,RovidNev
	,Modosithato
	,Sorrend
	,ervkezd
	) values (
	@KodCsopId
	,'450b510a-7caa-46b0-83e3-18445c0c53a9'
	,'09236C53-E085-E911-80D6-00155D027E06'
	,'1000'
	,'1000 gramm'
	,'1000 gramm'
	,'0'
	,'08'
	,'1900.01.01'
	); 
END 

print '1500 gramm'
SET @record_id = NULL
SET @record_id = (select Id from KRT_KodTarak
	where Id='0A236C53-E085-E911-80D6-00155D027E06') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 		
	Print 'INSERT' 
	insert into KRT_KodTarak(
	KodCsoport_Id
	,Org
	,Id
	,Kod
	,Nev
	,RovidNev
	,Modosithato
	,Sorrend
	,ervkezd
	) values (
	@KodCsopId
	,'450b510a-7caa-46b0-83e3-18445c0c53a9'
	,'0A236C53-E085-E911-80D6-00155D027E06'
	,'1500'
	,'1500 gramm'
	,'1500 gramm'
	,'0'
	,'09'
	,'1900.01.01'
	); 
END 

print '2000 gramm'
SET @record_id = NULL
SET @record_id = (select Id from KRT_KodTarak
	where Id='0B236C53-E085-E911-80D6-00155D027E06') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 		
	Print 'INSERT' 
	insert into KRT_KodTarak(
	KodCsoport_Id
	,Org
	,Id
	,Kod
	,Nev
	,RovidNev
	,Modosithato
	,Sorrend
	,ervkezd
	) values (
	@KodCsopId
	,'450b510a-7caa-46b0-83e3-18445c0c53a9'
	,'0B236C53-E085-E911-80D6-00155D027E06'
	,'2000'
	,'2000 gramm'
	,'2000 gramm'
	,'0'
	,'10'
	,'1900.01.01'
	); 
END 

	-- Kódtár Függőség összerendelés IDOEGYSEG_FELHASZNALAS - IDOEGYSEG
	
	IF NOT EXISTS (SELECT 1 FROM KRT_KODTARFUGGOSEG WHERE ID='4ADBD31F-ACA9-4AFF-B1BD-372453FE7A70')
	BEGIN
		PRINT 'Kódtár Függőség összerendelés KIMENO_KULDEMENY_FAJTA - KIMENOKULDEMENY_SULY'
		INSERT [dbo].[KRT_KodtarFuggoseg] ([Id], [Org], [Vezerlo_KodCsoport_Id], [Fuggo_KodCsoport_Id], [Adat], [Aktiv]) 
		VALUES (N'4ADBD31F-ACA9-4AFF-B1BD-372453FE7A70', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'11F9DC99-3708-49BE-A5DA-F7B6DB343784', N'0AE318FC-DE85-E911-80D6-00155D027E06', N'{"VezerloKodCsoportId":"11f9dc99-3708-49be-a5da-f7b6db343784","FuggoKodCsoportId":"0ae318fc-de85-e911-80d6-00155d027e06","Items":[{"VezerloKodTarId":"eb9b526c-eb09-47df-bb9f-e8b8aa29e496","FuggoKodtarId":"04236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"653991e5-01c5-414a-8df4-72152cd396fe","FuggoKodtarId":"04236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"7ae43d60-1e60-4a0b-8377-e2e00a8fc534","FuggoKodtarId":"04236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"6ffb572c-1868-4853-a856-41795db1a139","FuggoKodtarId":"04236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"3ec6843c-45f1-439e-bf5a-ab5a93afa646","FuggoKodtarId":"06236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"7c9d45c9-f7f1-4e1e-9b85-9e4c01103d94","FuggoKodtarId":"06236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"75d4fb5d-0ee0-4636-a900-9e4c01103d94","FuggoKodtarId":"06236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"5773839f-8522-408c-8274-9e4c01103d94","FuggoKodtarId":"09236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"324a68d1-1cea-4c8e-9fd0-9e4c01103d94","FuggoKodtarId":"09236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"666e0a27-60dd-4a13-a2f6-9e4c01103d94","FuggoKodtarId":"0a236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"df9352a8-b8dd-46d9-928e-9e4c01103d94","FuggoKodtarId":"0a236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"b69bd549-9f17-4886-a3e5-9e4c01103d94","FuggoKodtarId":"03236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"76f1af5f-b41d-45b5-bda5-9e4c01103d94","FuggoKodtarId":"03236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"5bcdf77e-18c0-413a-9dc9-9e4c01103d94","FuggoKodtarId":"0b236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"4e6eeb1b-14c4-47f1-8847-a075741c2ac5","FuggoKodtarId":"0b236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"a3dff134-7356-419f-8a43-9e4c01103d94","FuggoKodtarId":"0b236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"7ee12e4a-e1d6-4f93-9f74-9e4c01103d94","FuggoKodtarId":"07236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"7ef8b0b5-e5e3-4dc7-8c41-9e4c01103d94","FuggoKodtarId":"07236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"f3c307f2-70c0-4800-8d07-8de31fff12b2","FuggoKodtarId":"07236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"569feba8-0145-468f-8d5c-c355d1997973","FuggoKodtarId":"05236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"ae5f2804-4d0a-4789-a74d-9e4c01103d94","FuggoKodtarId":"05236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"8e1e4598-39d9-4e88-9df2-9e4c01103d94","FuggoKodtarId":"05236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"8c595d64-9d85-41a4-8a54-9e4c01103d94","FuggoKodtarId":"08236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"a8e1aa9e-94f6-4e3c-8043-9e4c01103d94","FuggoKodtarId":"08236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"4f71af66-0cf8-4824-887a-f93a863fceb3","FuggoKodtarId":"08236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"bd163177-35ae-4af3-b4a1-b7e8b58e31cb","FuggoKodtarId":"879d6ab6-e485-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"b4fd4d43-c2e2-4cff-8e45-a200919456f7","FuggoKodtarId":"04236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"a3d47428-d6d9-431e-9b6d-9e4c01103d94","FuggoKodtarId":"03236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"8d870cc0-5fcd-40b9-94d3-9e4c01103d94","FuggoKodtarId":"03236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"ff098455-7a76-4c2f-92cb-9e4c01103d94","FuggoKodtarId":"03236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"47d932e8-47f6-42b9-b76d-9e4c01103d94","FuggoKodtarId":"03236c53-e085-e911-80d6-00155d027e06","Aktiv":true},{"VezerloKodTarId":"e455b504-a926-4747-aeed-b0df37d06d7d","FuggoKodtarId":"04236c53-e085-e911-80d6-00155d027e06","Aktiv":true}],"ItemsNincs":[]}', N'1')
	END
go


------------------------------------------------------------------------------------
------------------------------------------------------------------------------------	
-- BUG_4765: Elektronikus irat kiadmányozása, IRATTAROZAS_ELEKTRONIKUS_JOVAHAGYAS_NELKUL
-- BUG_14775: DUP - Elektronikus irattárban lévő iratok további kezelése 1-ről 0-ra állítás minden ügyfélnél
-- Ha a paraméter értéke 1, akkor elektronikus ügyiratnál elintézetté nyilvánításkor automatikusan elektronikus irattárba kerül az ügyirat.
-- Ha a paraméter értéke 0, akkor vezetői jóváhagyás szükséges az irattárba küldéshez.

DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = '24385A76-E7B9-4B31-8459-FB2FC4923C89'

IF @org_kod = 'NMHH'
	SET @ertek = '0'
ELSE
	SET @ertek = '0'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - IRATTAROZAS_ELEKTRONIKUS_JOVAHAGYAS_NELKUL  - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'IRATTAROZAS_ELEKTRONIKUS_JOVAHAGYAS_NELKUL'
		,@ertek
		,'1'
		,'Ha a paraméter értéke 1, akkor elektronikus ügyiratnál elintézetté nyilvánításkor automatikusan elektronikus irattárba kerül az ügyirat. Ha a paraméter értéke 0, akkor vezetői jóváhagyás szükséges az irattárba küldéshez.'
		); 
 END

 GO
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- BUG_8876: Döntést hozta államigazgatási/önkormányzati egymást kizáró mezők
-- A 'Döntést hozta önkormányzati' tárgyszó fel volt véve az Allamigazgatasi_hatosagi_ugy metadefinícióhoz (ID=B7DE48C2-7022-E911-80C9-00155D020B39) is
 UPDATE EREC_Obj_MetaAdatai SET ErvVege='2019-05-31' WHERE Targyszavak_Id='d3db2868-6922-e911-80c9-00155d020b39' AND Obj_MetaDefinicio_Id='B7DE48C2-7022-E911-80C9-00155D020B39'
 GO

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- BUG_8001: IKTATAS_BELSO_UGYINTEZO_IKTATO paraméter
-- Ha a paraméter értéke 1, akkor a BelsoIratIktatas képernyőn az Irat Ügyintéző mező módosíthatósága letiltásra kerül, az iktató ügyintéző csak a saját szervezeti egységei közül állíthat be egyet az irat "Felelős szervezet"-nek.
-- Ha a paraméter értéke 0, akkor az Irat Ügyintéző mező módosítható marad.

DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = '88B975C7-2CCF-45A1-B36C-3088BA58AD76'

IF @org_kod = 'NMHH'
set @ertek = '1'
else
set @ertek = '0'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - IKTATAS_BELSO_UGYINTEZO_IKTATO  - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'IKTATAS_BELSO_UGYINTEZO_IKTATO'
		,@ertek
		,'1'
		,'Ha a paraméter értéke 1, akkor a BelsoIratIktatas képernyőn az Irat Ügyintéző mező módosíthatósága letiltásra kerül, az iktató ügyintéző csak a saját szervezeti egységei közül állíthat be egyet az irat "Felelős szervezet"-nek. Ha a paraméter értéke 0, akkor az Irat Ügyintéző mező módosítható marad.'
		); 
 END
 GO
-------------- end of BUG 8001

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- BUG_8951: A bejövő irat elintézetté nyilvánítható legyen iratstatisztika kitöltése nélkül (TÜK)
--DECLARE @org_kod nvarchar(100) 
--SET @org_kod = (select kod from KRT_Orgok
--			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

--DECLARE @isTUK char(1) 
--SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
--			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
--IF (@isTUK = '1')
--BEGIN
--  Print 'UPDATE - KRT_Parameterek - UGYIRAT_ELINTEZES_FELTETEL - 0'
--  UPDATE KRT_Parameterek SET Ertek='0' WHERE ID='7E9F9FF8-4635-4A17-9883-9AC1D18BE0FF'
--END

GO
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- BUG_9207 Alairó szerepek érvénytelenítése
print 'BUG_9207 Alairó szerepek érvénytelenítése'
update KRT_Kodtarak 
SET ErvVege = '2019-06-24'
where 
KodCsoport_Id = '42994C00-8924-4159-B079-6704941D3EDA'
and nev not in ('Láttamozás', 'Jóváhagyó','Kiadmányozó')

GO

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

print 'BUG_9167 Rendszerautomata kikerüljön a jogosultak köréből'
IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '5FC61BAF-1EDA-0ACE-A1FC-C4AC51FA1A69') 
BEGIN 
	INSERT INTO [dbo].[KRT_KodTarak] ([Id] ,[Org] ,[KodCsoport_Id] ,[Kod] ,[Nev] ,[RovidNev] ,[Egyeb] ,[Modosithato] ,[Sorrend] ,[Note]) VALUES ('5FC61BAF-1EDA-0ACE-A1FC-C4AC51FA1A69' ,'450B510A-7CAA-46B0-83E3-18445C0C53A9' ,'AB0F1DB5-C9FE-40E9-926F-0B0AAC091DFC' ,'T' ,'Technikai felhasználók' ,'T' ,'Technikai felhasználók' ,'0' ,'1' ,'Technikai felhasználók' ) 
END  

IF NOT EXISTS (SELECT 1 FROM KRT_Csoportok WHERE ID = '6A35D138-04B6-0B8D-4E0C-7E7906BA1A8B') 
	BEGIN
		INSERT INTO [dbo].[KRT_Csoportok]
			   ([Id] ,[Org] ,[Kod]  ,[Nev] ,[Tipus]  ,[System], [JogosultsagOroklesMod] )
				 VALUES
					   ('6A35D138-04B6-0B8D-4E0C-7E7906BA1A8B'
					   ,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
					   ,'T'
					   ,'Technikai felhasználók'
					   ,'T'
					   ,'1'
					   ,'1'
			)
	END
GO

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

-- BLG_8826: UGYINT_IDO_MOD paraméter
-- Ha a paraméter értéke 1, akkor az "Ügyirat ügyintézési kezdete" mező módosítható (kivéve megtekintéskor)
-- Ha a paraméter értéke 0, akkor az "Ügyirat ügyintézési kezdete" mező nem módosítható

DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = '87CAA8A6-19EE-4F41-9348-97A92FF37430'

IF @org_kod = 'CSPH' OR @org_kod = 'BOPMH'
set @ertek = '1'
else
set @ertek = '0'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - UGYINT_IDO_MOD  - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'UGYINT_IDO_MOD'
		,@ertek
		,'1'
		,'Ha a paraméter értéke 1, akkor az "Ügyirat ügyintézési kezdete" mező módosítható (kivéve megtekintéskor). Ha a paraméter értéke 0, akkor az "Ügyirat ügyintézési kezdete" mező nem módosítható.'
		); 
 END
 GO

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- BUG_9248: Csepeli paraméter állítások
-- LEZARAS_ELINTEZES_NELKUL_ENABLED: 0
-- IKTATAS_KOTELEZO_IRATTIPUS: 1
-- IKTATAS_KOTELEZO_IRATUGYINTEZO: 1

--DECLARE @org_kod nvarchar(100) 
--SET @org_kod = (select kod from KRT_Orgok WHERE Id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
	
--DECLARE @mdt datetime = '2019-07-02'
	
--IF (@org_kod = 'CSPH')  
--BEGIN
--	Print 'UPDATE LEZARAS_ELINTEZES_NELKUL_ENABLED: CSPH = 0'
--	UPDATE KRT_Parameterek SET Ertek='0', ModositasIdo=@mdt WHERE Id='F30C0BC5-3B3F-464C-8D5C-C71EF3B0A811' AND (ModositasIdo IS NULL OR ModositasIdo < @mdt)
	 
--	Print 'UPDATE IKTATAS_KOTELEZO_IRATTIPUS: CSPH = 1'
--	UPDATE KRT_Parameterek SET Ertek='1', ModositasIdo=@mdt WHERE Id='7203C38A-FE6A-E811-80CB-00155D020DD3' AND (ModositasIdo IS NULL OR ModositasIdo < @mdt)
	
--	Print 'UPDATE IKTATAS_KOTELEZO_IRATUGYINTEZO: CSPH = 1'
--	UPDATE KRT_Parameterek SET Ertek='1', ModositasIdo=@mdt WHERE Id='EB50F352-FE6A-E811-80CB-00155D020DD3' AND (ModositasIdo IS NULL OR ModositasIdo < @mdt)
--END
GO

-- BLG_5991 Értéknyilvánításos küldemények kezelése

DECLARE @KODCSOPORT_ID_5991 UNIQUEIDENTIFIER
SET @KODCSOPORT_ID_5991 = 'F78DDDB8-A009-4B73-81EB-EF899BBAA404' -- KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS

IF EXISTS (SELECT 1 FROM KRT_KODCSOPORTOK WHERE Id=@KODCSOPORT_ID_5991)
BEGIN
	IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '7A4E1583-6D1D-4C15-B796-CA3ADA4E1F00') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id], [Org], [KodCsoport_Id], [Kod], [Nev], [Egyeb], [Modosithato], [Sorrend]) VALUES ('7A4E1583-6D1D-4C15-B796-CA3ADA4E1F00', '450B510A-7CAA-46B0-83E3-18445C0C53A9', @KODCSOPORT_ID_5991, 'K_ENY_10000', '535', 'Értéknyilvánítás 10 000 Ft-ig', '0', '01') END
	IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '7A4E1583-6D1D-4C15-B796-CA3ADA4E1F01') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id], [Org], [KodCsoport_Id], [Kod], [Nev], [Egyeb], [Modosithato], [Sorrend]) VALUES ('7A4E1583-6D1D-4C15-B796-CA3ADA4E1F01', '450B510A-7CAA-46B0-83E3-18445C0C53A9', @KODCSOPORT_ID_5991, 'K_ENY_20000', '860', 'Értéknyilvánítás 20 000 Ft-ig', '0', '02') END
	IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '7A4E1583-6D1D-4C15-B796-CA3ADA4E1F02') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id], [Org], [KodCsoport_Id], [Kod], [Nev], [Egyeb], [Modosithato], [Sorrend]) VALUES ('7A4E1583-6D1D-4C15-B796-CA3ADA4E1F02', '450B510A-7CAA-46B0-83E3-18445C0C53A9', @KODCSOPORT_ID_5991, 'K_ENY_30000', '1380', 'Értéknyilvánítás 30 000 Ft-ig', '0', '03') END
	IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '7A4E1583-6D1D-4C15-B796-CA3ADA4E1F03') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id], [Org], [KodCsoport_Id], [Kod], [Nev], [Egyeb], [Modosithato], [Sorrend]) VALUES ('7A4E1583-6D1D-4C15-B796-CA3ADA4E1F03', '450B510A-7CAA-46B0-83E3-18445C0C53A9', @KODCSOPORT_ID_5991, 'K_ENY_40000', '2165', 'Értéknyilvánítás 40 000 Ft-ig', '0', '04') END
	IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '7A4E1583-6D1D-4C15-B796-CA3ADA4E1F04') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id], [Org], [KodCsoport_Id], [Kod], [Nev], [Egyeb], [Modosithato], [Sorrend]) VALUES ('7A4E1583-6D1D-4C15-B796-CA3ADA4E1F04', '450B510A-7CAA-46B0-83E3-18445C0C53A9', @KODCSOPORT_ID_5991, 'K_ENY_50000', '2760', 'Értéknyilvánítás 50 000 Ft-ig', '0', '05') END
	IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '7A4E1583-6D1D-4C15-B796-CA3ADA4E1F05') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id], [Org], [KodCsoport_Id], [Kod], [Nev], [Egyeb], [Modosithato], [Sorrend]) VALUES ('7A4E1583-6D1D-4C15-B796-CA3ADA4E1F05', '450B510A-7CAA-46B0-83E3-18445C0C53A9', @KODCSOPORT_ID_5991, 'K_ENY_75000', '4105', 'Értéknyilvánítás 75 000 Ft-ig', '0', '06') END
	IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '7A4E1583-6D1D-4C15-B796-CA3ADA4E1F06') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id], [Org], [KodCsoport_Id], [Kod], [Nev], [Egyeb], [Modosithato], [Sorrend]) VALUES ('7A4E1583-6D1D-4C15-B796-CA3ADA4E1F06', '450B510A-7CAA-46B0-83E3-18445C0C53A9', @KODCSOPORT_ID_5991, 'K_ENY_100000', '5445', 'Értéknyilvánítás 100 000 Ft-ig', '0', '07') END
	IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '7A4E1583-6D1D-4C15-B796-CA3ADA4E1F07') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id], [Org], [KodCsoport_Id], [Kod], [Nev], [Egyeb], [Modosithato], [Sorrend]) VALUES ('7A4E1583-6D1D-4C15-B796-CA3ADA4E1F07', '450B510A-7CAA-46B0-83E3-18445C0C53A9', @KODCSOPORT_ID_5991, 'K_ENY_150000', '8230', 'Értéknyilvánítás 150 000 Ft-ig', '0', '08') END
	IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '7A4E1583-6D1D-4C15-B796-CA3ADA4E1F08') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id], [Org], [KodCsoport_Id], [Kod], [Nev], [Egyeb], [Modosithato], [Sorrend]) VALUES ('7A4E1583-6D1D-4C15-B796-CA3ADA4E1F08', '450B510A-7CAA-46B0-83E3-18445C0C53A9', @KODCSOPORT_ID_5991, 'K_ENY_150001', '9980', 'Értéknyilvánítás 150 001 Ft-tól', '0', '09') END
END
GO

------------------------------------------------------------------------------------
-------------------------------BUG_9694---------------------------------------------
--DECLARE @org_kod nvarchar(100) 

--SET @org_kod = (select kod from KRT_Orgok
--			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

--IF @org_kod != 'NMHH'
--BEGIN
--   print 'set TERTIVEVENY_RAGSZAM_KIOSZTAS_MODJA=K'

--   update KRT_Parameterek
--   set Ertek = 'K'
--   where id = 'F0B0E0F7-D52C-49D5-A47F-06DA48CB58A6'
--END

GO
------------------------------------------------------------------------------------

----------------Bug_9692_Hatósági_statisztika_adatok_korlátozása--------------------
declare @kodcsoportId uniqueidentifier
set @kodcsoportId = '89F4B044-2DB6-4FB8-BECB-8BDBC9E1DC95'
declare @recordId uniqueidentifier
set @recordId = '3CD94373-43FE-4771-BA63-D3CF86C4114F'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN
    print 'Add CONTROLTYPE_SOURCE - ~/Component/RequiredDecimalNumberBox.ascx' 	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'~/Component/RequiredDecimalNumberBox.ascx'
	,'Törtszám mező (RequiredDecimalNumberBox)'
	,'1'
	,'17'
	); 
END 

GO

--határidő túllépés egász szám
PRINT 'UFDATE EREC_TargySzavak.Hatarido_tullepes_napokban'
UPDATE EREC_TargySzavak
SET ControlTypeSource = '~/Component/RequiredNumberBox.ascx'
where BelsoAzonosito = 'Hatarido_tullepes_napokban'

-- munkaórák száma tört szám
PRINT 'UFDATE EREC_TargySzavak.Dontes_munkaorak_szama'
UPDATE EREC_TargySzavak
SET ControlTypeSource = '~/Component/RequiredDecimalNumberBox.ascx'
where BelsoAzonosito = 'Dontes_munkaorak_szama'

GO

------------------------------------------------------------------------------------

------------------------------------------------------------------------------------



------------------------------------------------------------------------------------
Print '----------------------------------------------------------------------------'
Print 'PUBLIKUS SABLON LETREHOZASA FUNKCIOJOG'
DECLARE @record_id_5172_FUNKCIO uniqueidentifier 
SET @record_id_5172_FUNKCIO = '9EDB76C2-7E54-E911-80D0-00155D020D00'

DECLARE @record_id_5172_MUVELET_NEW uniqueidentifier 
SET @record_id_5172_MUVELET_NEW = '939A0831-2914-4348-93FA-9DED0225747E'

IF NOT EXISTS (SELECT 1 FROM KRT_Funkciok where id = @record_id_5172_FUNKCIO) 
BEGIN 	
	Print 'INSERT' 
	 insert into KRT_Funkciok(
		 Alkalmazas_Id
		,Modosithato
		,Id
		,Kod
		,Nev
		,Muvelet_Id
		) values (
		'272277BB-27B7-4689-BE45-DB5994A0FBA9'
		,'0'
		,@record_id_5172_FUNKCIO
		,'PublikusSablonNew'
		,'Publikus sablon letrehozasa'
		,@record_id_5172_MUVELET_NEW
		);
 END 

Print 'KRT_Szerepkor_Funkcio - FEJLESZTO-hoz, VEZETO-hoz '

DECLARE @record_id_SZEREPKOR_FEJLESZTO uniqueidentifier 
DECLARE @record_id_SZEREPKOR_VEZETO uniqueidentifier 
DECLARE @record_id_5172_SZEREPKORFUNKCIO_FEJLESZTO uniqueidentifier 
DECLARE @record_id_5172_SZEREPKORFUNKCIO_VEZETO uniqueidentifier 

SELECT TOP 1 @record_id_SZEREPKOR_VEZETO    = Id FROM KRT_Szerepkorok where Nev='VEZETO'
SELECT TOP 1 @record_id_SZEREPKOR_FEJLESZTO = Id FROM KRT_Szerepkorok where Nev='FEJLESZTO'

SET @record_id_5172_SZEREPKORFUNKCIO_FEJLESZTO = '9EDB76C2-7E54-E911-80D0-00155D020D03'
SET @record_id_5172_SZEREPKORFUNKCIO_VEZETO    = '9EDB76C2-7E54-E911-80D0-00155D020D04' 

IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = @record_id_5172_SZEREPKORFUNKCIO_FEJLESZTO) AND @record_id_SZEREPKOR_FEJLESZTO IS NOT NULL
BEGIN 	
	Print 'INSERT 2 FEJLESZTO' 
	 INSERT INTO KRT_Szerepkor_Funkcio(
		 Szerepkor_Id
		,Id
		,Funkcio_Id
		) values (
		 @record_id_SZEREPKOR_FEJLESZTO
		,@record_id_5172_SZEREPKORFUNKCIO_FEJLESZTO
		,@record_id_5172_FUNKCIO
		);
 END 

IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkor_Funkcio WHERE ID = @record_id_5172_SZEREPKORFUNKCIO_VEZETO) AND @record_id_SZEREPKOR_VEZETO IS NOT NULL
BEGIN 	
	Print 'INSERT 2 VEZETO' 
	 INSERT INTO KRT_Szerepkor_Funkcio(
		 Szerepkor_Id
		,Id
		,Funkcio_Id
		) values (
		 @record_id_SZEREPKOR_VEZETO
		,@record_id_5172_SZEREPKORFUNKCIO_VEZETO
		,@record_id_5172_FUNKCIO
		);
 END 

Print '----------------------------------------------------------------------------'

GO

------------------------------------------------------------------------------------




-------------------------TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT---------------------------
declare @kodcsoportId uniqueidentifier
declare @Org uniqueidentifier
declare @recordId uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

set @kodcsoportId = 'E9771186-EBEF-484A-B44B-E85651853ED7'

IF NOT EXISTS (SELECT 1 from KRT_KodCsoportok WHERE Id = @kodcsoportId)
BEGIN
	print 'KRT_KodCsoportok - TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT'

	insert into KRT_KodCsoportok
	(
		 Id
		,Kod
		,Nev
		,Modosithato
		,Note
		,BesorolasiSema
	) 
	values
	(
		@kodcsoportId
		,'TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT'
		,'Tömeges iktatási folyamat állapot'
		,'1'
		, null
		,'0'
	)

END

-- 01 - Inicializálás folyamatban
set @recordId = 'FFF2A67D-9C85-4014-B905-65215AA588FE'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN
    print 'TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT - Inicializálás folyamatban'  	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Sorrend
	,Modosithato
	,Egyeb	
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'01'
	,'Inicializálás folyamatban'
	,'01'
	,'1'
	,''	
	); 
END

-- 02 - Inicializálási hiba
set @recordId = 'F82057D7-7D4C-48B4-9027-4B078957637A'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN
    print 'TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT - Inicializálási hiba'  	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Sorrend
	,Modosithato
	,Egyeb	
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'02'
	,'Inicializálási hiba'
	,'02'
	,'1'
	,''	
	); 
END

-- 03 - Inicializált
set @recordId = '34312D09-626D-4DD0-9AE1-B68CF5838088'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN
    print 'TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT - Inicializált'  	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Sorrend
	,Modosithato
	,Egyeb	
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'03'
	,'Inicializált'
	,'03'
	,'1'
	,''	
	); 
END

-- 04 - Végrehajtás folyamatban
set @recordId = '439AB957-2EC3-4FBF-8812-8F870DAC7A9C'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN
    print 'TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT - Végrehajtás folyamatban'  	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Sorrend
	,Modosithato
	,Egyeb	
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'04'
	,'Végrehajtás folyamatban'
	,'04'
	,'1'
	,''	
	); 
END

-- 05 - Végrehajtási hiba
set @recordId = '1FC35052-5CB7-4763-8CAF-9C23AA6709DB'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN
    print 'TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT - Végrehajtási hiba'  	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Sorrend
	,Modosithato
	,Egyeb	
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'05'
	,'Végrehajtási hiba'
	,'05'
	,'1'
	,''	
	); 
END

-- 06 - Végrehajott
set @recordId = 'F74000F9-9977-4B63-B28F-032833F2F481'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId)
BEGIN
    print 'TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT - Végrehajott'  	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Sorrend
	,Modosithato
	,Egyeb	
	) values (
	 @recordId
	,@Org
	,@kodcsoportId
	,'06'
	,'Végrehajott'
	,'06'
	,'1'
	,''	
	); 
END

GO


-- BUG_8765 - TUK_SZIGNALAS_ENABLED létrehozása (TUK esetén 1, máshol 0)
--			- IRAT_SZIGNALAS_ELERHETO paraméter állítása TUK esetén (0-ra)
-- 			- ALSZAM_SZIGNALT_ENABLED paraméter állítása TUK esetén (0-ra)

print 'TUK_SZIGNALAS_ENABLED létrehozása (TUK esetén 1, máshol 0)'		
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='50C64695-72A4-E911-80D7-00155D020D3E') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'50C64695-72A4-E911-80D7-00155D020D3E'
		,'TUK_SZIGNALAS_ENABLED'
		,'0'
		,'1'
		,'Egyszerűsített szignálás bekapcsolása TUK számára.'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='50C64695-72A4-E911-80D7-00155D020D3E'
	--	,Nev='TUK_SZIGNALAS_ENABLED'
	--	,Ertek='0'
	--	,Karbantarthato='1'
	--	,Note='Egyszerűsített szignálás bekapcsolása TUK számára.'
	-- WHERE Id=@record_id 
 --END


--DECLARE @isTUK char(1) 
--SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
--			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
--if  (@isTUK = '1')
--BEGIN
--	 Print 'TUK'
--	 Print 'UPDATE'
--	 UPDATE KRT_Parameterek
--	 SET 
--		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
--		,Id='50C64695-72A4-E911-80D7-00155D020D3E'
--		,Nev='TUK_SZIGNALAS_ENABLED'
--		,Ertek='1'
--		,Karbantarthato='1'
--		,Note='Egyszerűsített szignálás bekapcsolása TUK számára.'
--	 WHERE Id='50C64695-72A4-E911-80D7-00155D020D3E'
	 
--	 print 'IRAT_SZIGNALAS_ELERHETO paraméter állítása TUK esetén (0-ra)'
	 
--	 SET @record_id = (select Id from KRT_Parameterek
--			where Nev='IRAT_SZIGNALAS_ELERHETO') 
--	 Print '@record_id='+convert(NVARCHAR(100),@record_id) 

--	if @record_id IS NOT NULL 
--	BEGIN 
--		 Print 'UPDATE'
--		 UPDATE KRT_Parameterek
--		 SET Ertek='0'
--		 WHERE Id=@record_id 
--	 END
	 
--	  print 'ALSZAM_SZIGNALT_ENABLED paraméter állítása TUK esetén (0-ra)'
	  
--	 SET @record_id = (select Id from KRT_Parameterek
--			where Nev='ALSZAM_SZIGNALT_ENABLED') 
--	 Print '@record_id='+convert(NVARCHAR(100),@record_id) 

--	if @record_id IS NOT NULL 
--	BEGIN 
--		 Print 'UPDATE'
--		 UPDATE KRT_Parameterek
--		 SET Ertek='0'
--		 WHERE Id=@record_id 
--	 END
-- END
GO
 --------------------------------------------------------------------------------
 -------------------- BUG_8765 vége -------------------------
-------------------------------Bug 10198-----------------------------------------
DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = '9281A0C3-8117-4E3A-AA58-402B06467D4A'

IF @org_kod = 'NMHH'
set @ertek = '0'
else
set @ertek = '1'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - TULAJDONOS_JOGOSULTSAG_ROGZITES  - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'TULAJDONOS_JOGOSULTSAG_ROGZITES'
		,@ertek
		,'1'
		,'Ha 1 az értéke, akkor az iktatáskor rögzítsük az iktató felhasználót az ügyirat jogosultak közé Tulajdonos-ként (nem jelenik meg a felületen, de minden iratot és csatolmányt lát - ha a IGNORE_TULAJDONOS_JOGOSULTSAG rsz. param értéke 0). Ha értéke 0, akkor  az iktatót ne rögzítsük, így ne legyen alapértelmezett jogosultsága az ügyirathoz.'
		); 
 END
 GO
-------------------------------Bug 10198-----------------------------------------------------

 -------------------- BUG_9320 -------------------------
 
DECLARE @org_kod NVARCHAR(100)
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9')

declare @ertek nvarchar(400)
set @ertek = case when @org_kod = 'NMHH' then '1' else '0' end

DECLARE @record_id uniqueidentifier
set @record_id = '5BBF8F4A-9E72-4A68-9496-49E071BDF3F6'

if not exists (select 1 from KRT_Parameterek where Id=@record_id)
BEGIN
	Print 'INSERT' 
	insert into KRT_Parameterek(
		Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,@record_id
		,'KOLCSONZESFUL_ATMENETI_IRATTAR_ENABLED'
		,@ertek
		,'0'
		,'A paraméter értéke megadja, hogy a kölcsönzés fülön meg kell-e jeleníteni az Átmeneti irattárból történő kikéréseket is.'
		); 
END 
--ELSE 
--BEGIN 
--	Print 'UPDATE'
--	UPDATE KRT_Parameterek
--	SET 
--		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
--		,Id=@record_id
--		,Nev='KOLCSONZESFUL_ATMENETI_IRATTAR_ENABLED'
--		,Ertek=@ertek
--		,Karbantarthato='0'
--		,Note='A paraméter értéke megadja, hogy a kölcsönzés fülön meg kell-e jeleníteni az Átmeneti irattárból történő kikéréseket is.'
--	WHERE Id=@record_id 
--END
GO
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-------------------------------Bug 10198-----------------------------------------------------
-------------------------------Bug 9392------------------------------------------------------

--JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN
DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = '4C6B4C11-1637-40E8-B5B0-611939B4611B'

IF @org_kod = 'NMHH'
set @ertek = '1'
else
set @ertek = '0'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN  - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN'
		,@ertek
		,'1'
		,'0 - A vezető minden beosztottja után örökli az iratkezelési objektumok láthatóságát, 1 - A vezető csak azon iratkezelési objektumok láthatóságát örökli, amelyeknél az obejektumon rögzített Felelős szervezet (Csoport_Id_Ugyfelelos) az a csoport, amelyben ő a vezető.'
		); 
 END
 GO

-------------------------------Bug 9392------------------------------------------------------

-------------------------------Bug 10245------------------------------------------------------

--POSTAZAS_KULDEMENY_FAJTA_ENABLED
DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = 'C5A1D797-C4A9-47EE-A49F-BD0488359BFF'

IF @org_kod = 'NMHH'
set @ertek = '1'
else
set @ertek = '0'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - POSTAZAS_KULDEMENY_FAJTA_ENABLED  - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'POSTAZAS_KULDEMENY_FAJTA_ENABLED'
		,@ertek
		,'1'
		,'Ha a paraméter értéke 1, a küldemény fajta kiválasztása korlátozva van függő kódtár alapján a küldésmódnak megfelelően'
		); 
 END
 GO

declare @orgKod nvarchar(100)
set @orgKod = (SELECT Kod FROM KRT_Orgok WHERE Id = '450B510A-7CAA-46B0-83E3-18445C0C53A9')

declare @record_id uniqueidentifier
set @record_id = '0A80E7CD-D174-4751-99B3-331EB88B3CAB'

declare @adat nvarchar(max)

IF (@orgKod = 'NMHH')
	SET @adat = '{"VezerloKodCsoportId":"6982f668-c8cd-47a3-ac62-9f16e439473c","FuggoKodCsoportId":"11f9dc99-3708-49be-a5da-f7b6db343784","Items":[{"VezerloKodTarId":"7eb11332-5a91-e711-80c2-00155d020d7e","FuggoKodtarId":"eb9b526c-eb09-47df-bb9f-e8b8aa29e496","Aktiv":true},{"VezerloKodTarId":"7eb11332-5a91-e711-80c2-00155d020d7e","FuggoKodtarId":"653991e5-01c5-414a-8df4-72152cd396fe","Aktiv":true},{"VezerloKodTarId":"7eb11332-5a91-e711-80c2-00155d020d7e","FuggoKodtarId":"7ae43d60-1e60-4a0b-8377-e2e00a8fc534","Aktiv":true},{"VezerloKodTarId":"7eb11332-5a91-e711-80c2-00155d020d7e","FuggoKodtarId":"6ffb572c-1868-4853-a856-41795db1a139","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"75d4fb5d-0ee0-4636-a900-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"3ec6843c-45f1-439e-bf5a-ab5a93afa646","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"7c9d45c9-f7f1-4e1e-9b85-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"324a68d1-1cea-4c8e-9fd0-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"5773839f-8522-408c-8274-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"df9352a8-b8dd-46d9-928e-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"666e0a27-60dd-4a13-a2f6-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"b69bd549-9f17-4886-a3e5-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"76f1af5f-b41d-45b5-bda5-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"4e6eeb1b-14c4-47f1-8847-a075741c2ac5","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"5bcdf77e-18c0-413a-9dc9-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"a3dff134-7356-419f-8a43-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"f3c307f2-70c0-4800-8d07-8de31fff12b2","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"7ef8b0b5-e5e3-4dc7-8c41-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"7ee12e4a-e1d6-4f93-9f74-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"8e1e4598-39d9-4e88-9df2-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"ae5f2804-4d0a-4789-a74d-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"569feba8-0145-468f-8d5c-c355d1997973","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"4f71af66-0cf8-4824-887a-f93a863fceb3","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"a8e1aa9e-94f6-4e3c-8043-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"8c595d64-9d85-41a4-8a54-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"bd163177-35ae-4af3-b4a1-b7e8b58e31cb","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"ff098455-7a76-4c2f-92cb-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"47d932e8-47f6-42b9-b76d-9e4c01103d94","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"e455b504-a926-4747-aeed-b0df37d06d7d","Aktiv":true}],"ItemsNincs":[]}'
ELSE
	SET @adat = ''

IF NOT EXISTS (SELECT 1 FROM KRT_KodtarFuggoseg WHERE Id = @record_id)
BEGIN
	PRINT 'INSERT - KRT_KodtarFuggoseg - KULDEMENY_KULDES_MODJA - KIMENO_KULDEMENY_FAJTA'
	INSERT INTO KRT_KodtarFuggoseg
	(Id
	,Org
	,Vezerlo_KodCsoport_Id
	,Fuggo_KodCsoport_Id
	,Adat
	,Aktiv)
	VALUES
	(@record_id
	,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
	,'6982F668-C8CD-47A3-AC62-9F16E439473C' -- KULDEMENY_KULDES_MODJA
	,'11F9DC99-3708-49BE-A5DA-F7B6DB343784' -- KIMENO_KULDEMENY_FAJTA
	,@adat
	,'1')
END

GO

-------------------------------Bug 10245------------------------------------------------------
-- BUG_10336 - BORITO_PRINT rendszerparaméter létrehozása (NMHH esetén 0, máshol 1)

print 'BORITO_PRINT létrehozása (NMHH esetén 0, máshol 1)'		
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='3CCEEC6D-49BF-E911-80DB-00155D027E1B') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'3CCEEC6D-49BF-E911-80DB-00155D027E1B'
		,'BORITO_PRINT'
		,'1'
		,'1'
		,'A felületen van-e lehetőség az iratkezelési objektumoknak borítót nyomtatni. 1 - igen, 0 - nem'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='3CCEEC6D-49BF-E911-80DB-00155D027E1B'
	--	,Nev='BORITO_PRINT'
	--	,Ertek='1'
	--	,Karbantarthato='1'
	--	,Note='A felületen van-e lehetőség az iratkezelési objektumoknak borítót nyomtatni. 1 - igen, 0 - nem'
	-- WHERE Id=@record_id 
 --END

--if @org_kod = 'NMHH' 
-- BEGIN 
-- Print 'UPDATE NMHH ->0'
--	 UPDATE KRT_Parameterek
--	 SET 
--		Ertek='0'
--	 WHERE Id=@record_id 
--END
GO
 --------------------------------------------------------------------------------
 -------------------- BUG_10336 vége -------------------------




-------------------------------------------------
GO

DECLARE @ORG_ID UNIQUEIDENTIFIER 
SET @ORG_ID ='450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id=@ORG_ID) 

DECLARE @RECORD_ID_7718 UNIQUEIDENTIFIER
SET @RECORD_ID_7718 = '1C44A703-8854-4609-A90F-F2CD2ED9B63C'

IF NOT EXISTS(SELECT 1 FROM KRT_PARAMETEREK WHERE ID =@RECORD_ID_7718)
BEGIN 

	DECLARE @RECORD_ID_7718_VALUE NVARCHAR(100) 
	IF @ORG_KOD = 'NMHH' 
		BEGIN 
				SET @RECORD_ID_7718_VALUE='1'
		END
	ELSE
		BEGIN 
				SET @RECORD_ID_7718_VALUE='0'
		END

 	Print 'INSERT SCAN_OCR_ENABLED' 
	INSERT INTO KRT_Parameterek(
	 Org
	,Id
	,Nev
	,Ertek
	,Karbantarthato
	,Note
	) values (
	 @ORG_ID
	,@RECORD_ID_7718
	,'IRAT_ELINT_SAKKORA_FIGYELES'
	,@RECORD_ID_7718_VALUE
	,'1'
	,' 0 - érték esetén az irat elintézésekor nem figyeljük a sakkóra állását,
       1 - érték esetén az irat elintézésekor figyeljük a sakkóra állását'
	); 
END 

GO
------------------------------------------------------------------------------
 -------------------- BUG_10336 vége -------------------------

--------------------------------------------------------------------------------
-- BUG_10364: ALAIRO_SZEREPE kódcsoportba új kódtár érték (ha nincs): Láttamozó. kód = 3
--			  1,2,3 kódtárétéken kívül mindent érvényteleníteni kell


-- BUG_10364: ALAIRO_SZEREPE kódcsoportba új kódtár érték (ha nincs): Láttamozó. kód = 3
--			  1,2,3 kódtárétéken kívül mindent érvényteleníteni kell

DECLARE @ErvVege datetime
SET @ErvVege = '2019-08-26'
PRINT 'Nem létező ALAIRO_SZEREPE-k törlése (Csak 1, 2, 3 lehet)'
update KRT_Kodtarak 
SET ErvVege = @ErvVege
where 
KodCsoport_Id = '42994C00-8924-4159-B079-6704941D3EDA'
and kod not in ('1','2','3')
and @ErvVege > GETDATE()

Print 'ALAIRO_SZEREPE kódcsoport'

 DECLARE @kcs_id uniqueidentifier SET @kcs_id = (select Id from KRT_KodCsoportok
			where Id='42994C00-8924-4159-B079-6704941D3EDA') 
Print '@kcs_id='+convert(NVARCHAR(100),@kcs_id) 
if @kcs_id IS NULL 
BEGIN 
		
	Print 'ERROR: Nincs ALAIRO_SZEREPE kódcsoport vagy nem jó az ID-ja' 

END 
ELSE 
BEGIN 
    Print 'Látamozó kódtárérték javítás' 
	DECLARE @record_id uniqueidentifier
	DECLARE @kod nvarchar(64)
	DECLARE @nev nvarchar(400)
    select @record_id=Id, @kod=kod, @nev=nev from KRT_KodTarak
		where Id='D2996598-EF6E-E711-80C2-00155D020D7E'
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	DECLARE @lattamozoId uniqueidentifier 
	SET @lattamozoId = (select Id from KRT_KodTarak
		where KodCsoport_Id = '42994C00-8924-4159-B079-6704941D3EDA' and kod='3') 
	Print '@lattamozoId='+convert(NVARCHAR(100),@lattamozoId) 	
	if @record_id IS NULL 
	BEGIN 
		-- Nincs az adott id-val tétel
		if @lattamozoId IS NULL
		BEGIN
			-- Nincs se az adott id-val, se 3-as kóddal tétel
			Print 'INSERT' 
			insert into KRT_KodTarak(
				KodCsoport_Id
			,Tranz_id
			,Stat_id
			,Org
			,Id
			,Kod
			,Nev
			,Modosithato
			,Sorrend
			) values (
			'42994C00-8924-4159-B079-6704941D3EDA'
			,null
			,null
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'D2996598-EF6E-E711-80C2-00155D020D7E'
			,'3'
			,'Láttamozó'
			,'0'
			,'1'
			); 
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
		END 
		ELSE 
		BEGIN
			-- nincs adott id-val, de van 3-as kódú tétel -> update
			Print 'UPDATE id, név, ervvege'
			UPDATE KRT_KodTarak
			SET 
			 KodCsoport_Id='42994C00-8924-4159-B079-6704941D3EDA'
			,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Id ='D2996598-EF6E-E711-80C2-00155D020D7E'
			,Kod='3'
			,Nev='Láttamozó'
			,RovidNev=NULL
			,Modosithato='0'
			,Sorrend='1'
			,ErvVege ='4700-12-31 00:00:00.000'
			WHERE Id=@lattamozoId
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
		END
	END
	ELSE
	BEGIN 
		-- van az adott id-val tétel
		if (@lattamozoId is not null) and (@lattamozoId <> @record_id)
		BEGIN
			-- van az adott id-val tétel, és van 3-as kódú is, de más id-val ->  record_id-s tételnek (ami nem 3-as)  új id-t adunk és érvénytelenítjük
			Print 'UPDATE id, ervvege (record_id)' 
			UPDATE KRT_KodTarak
			SET 
			id = NEWID()
			,ErvVege =@ErvVege
			WHERE Id=@record_id
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
					-- van az adott id-val tétel, de nincs 3-as kódú, vagy van, de az id-ja megegyezik a kívánt id-val -> update adott kódú
			Print 'UPDATE id, kód, név, ervvege (lattamozoId)' 
			UPDATE KRT_KodTarak
			SET 
			KodCsoport_Id='42994C00-8924-4159-B079-6704941D3EDA'
			,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Id='D2996598-EF6E-E711-80C2-00155D020D7E'
			,Kod='3'
			,Nev='Láttamozó'
			,RovidNev=NULL
			,Modosithato='0'
			,Sorrend='1'
			,ErvVege ='4700-12-31 00:00:00.000'
			WHERE Id=@lattamozoId
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
		END
		ELSE
		BEGIN
			-- van az adott id-val tétel, de nincs 3-as kódú, vagy van, de az id-ja megegyezik a kívánt id-val -> update adott kódú
			Print 'UPDATE kód, név, ervvege (record_id)' 
			UPDATE KRT_KodTarak
			SET 
			KodCsoport_Id='42994C00-8924-4159-B079-6704941D3EDA'
			,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Kod='3'
			,Nev='Láttamozó'
			,RovidNev=NULL
			,Modosithato='0'
			,Sorrend='1'
			,ErvVege ='4700-12-31 00:00:00.000'
			WHERE Id=@record_id
			if @@ERROR<>0 
				begin UPDATE #NumberOfErrorsTable SET noe = noe + 1 end
		END
	END
	
END
GO
 -------------------- BUG_10364 vége -------------------------
  --------------------------------------------------------------------------------
 --------------------------------------------------------------------------------

DECLARE @ORG UNIQUEIDENTIFIER
SET @ORG = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @ORG_KOD NVARCHAR(100) 
SET @ORG_KOD = (SELECT KOD FROM KRT_ORGOK WHERE ID=@ORG)

DECLARE @KODCSOPORTID UNIQUEIDENTIFIER
SET @KODCSOPORTID = 'BB3ACD54-E18D-E511-A275-00155D011E21'
--------------------
DECLARE @recordId_KKBM_10 UNIQUEIDENTIFIER
set @recordId_KKBM_10 = 'FD0C1733-3592-E711-80C2-00155D020D7E'
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId_KKBM_10)
BEGIN
    PRINT '10	elektronikus úton (Hivatali kapu)'  	
	INSERT INTO KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Sorrend
	,Modosithato
	,Egyeb	
	) values (
	 @recordId_KKBM_10
	,@Org
	,@kodcsoportId
	,'10'
	,'elektronikus úton (Hivatali kapu)'
	,NULL
	,'1'
	,NULL
	); 

	IF @org_kod <> 'NMHH'
		UPDATE KRT_KodTarak SET ErvVege= GETDATE() WHERE ID=@recordId_KKBM_10
END
--------------------
--------------------
DECLARE @recordId_KKBM_11 UNIQUEIDENTIFIER
set @recordId_KKBM_11 = '47F4763B-3592-E711-80C2-00155D020D7E'
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId_KKBM_11)
BEGIN
    PRINT '11	elektronikus úton (Ügyfélkapu)'  	
	INSERT INTO KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Sorrend
	,Modosithato
	,Egyeb	
	) values (
	 @recordId_KKBM_11
	,@Org
	,@kodcsoportId
	,'11'
	,'elektronikus úton (Ügyfélkapu)'
	,NULL
	,'1'
	,NULL
	); 

	IF @org_kod <> 'NMHH'
		UPDATE KRT_KodTarak SET ErvVege= GETDATE() WHERE ID=@recordId_KKBM_11
END
--------------------
--------------------
DECLARE @recordId_KKBM_30 UNIQUEIDENTIFIER
set @recordId_KKBM_30 = '8F6CAAB4-3792-E711-80C2-00155D020D7E'
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId_KKBM_30)
BEGIN
    PRINT '30   elektronikus úton (Adatkapu)'  	
	INSERT INTO KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Sorrend
	,Modosithato
	,Egyeb	
	) values (
	 @recordId_KKBM_30
	,@Org
	,@kodcsoportId
	,'30'
	,'elektronikus úton (Adatkapu)'
	,NULL
	,'1'
	,NULL
	); 

	IF @org_kod <> 'NMHH'
		UPDATE KRT_KodTarak SET ErvVege= GETDATE() WHERE ID=@recordId_KKBM_30
END
--------------------
--------------------
DECLARE @recordId_KKBM_31 UNIQUEIDENTIFIER
set @recordId_KKBM_31 = '8F6CAAB4-3792-E712-80C2-00155D020D7E'
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId_KKBM_31)
BEGIN
    PRINT '31   elektronikus úton (e-NMHH nem azonosított felhasználó)'  	
	INSERT INTO KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Sorrend
	,Modosithato
	,Egyeb	
	) values (
	 @recordId_KKBM_31
	,@Org
	,@kodcsoportId
	,'31'
	,'elektronikus úton (e-NMHH nem azonosított felhasználó)'
	,NULL
	,'1'
	,NULL
	); 

	IF @org_kod <> 'NMHH'
		UPDATE KRT_KodTarak SET ErvVege= GETDATE() WHERE ID=@recordId_KKBM_31
END
--------------------
--------------------
DECLARE @recordId_KKBM_32 UNIQUEIDENTIFIER
set @recordId_KKBM_32 = '8F6CAAB4-3792-E713-80C2-00155D020D7E'
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId_KKBM_32)
BEGIN
    PRINT '32  elektronikus úton (e-NMHH azonosított felhasználó)'  	
	INSERT INTO KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Sorrend
	,Modosithato
	,Egyeb	
	) values (
	 @recordId_KKBM_32
	,@Org
	,@kodcsoportId
	,'32'
	,'elektronikus úton (e-NMHH azonosított felhasználó)'
	,NULL
	,'1'
	,NULL
	); 

	IF @org_kod <> 'NMHH'
		UPDATE KRT_KodTarak SET ErvVege= GETDATE() WHERE ID=@recordId_KKBM_32
END
--------------------
--------------------
DECLARE @recordId_KKBM_33 UNIQUEIDENTIFIER
set @recordId_KKBM_33 = '8F6CAAB4-3792-E714-80C2-00155D020D7E'
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId_KKBM_33)
BEGIN
    PRINT '33  elektronikus úton (Cégkapu)'  	
	INSERT INTO KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Sorrend
	,Modosithato
	,Egyeb	
	) values (
	 @recordId_KKBM_33
	,@Org
	,@kodcsoportId
	,'33'
	,'elektronikus úton (Cégkapu)'
	,NULL
	,'1'
	,NULL
	); 

	IF @org_kod <> 'NMHH'
		UPDATE KRT_KodTarak SET ErvVege= GETDATE() WHERE ID=@recordId_KKBM_33
END
--------------------
--------------------
DECLARE @recordId_KKBM_34 UNIQUEIDENTIFIER
set @recordId_KKBM_34 = '8F6CAAB4-3792-E715-80C2-00155D020D7E'
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId_KKBM_34)
BEGIN
    PRINT '34  elektronikus úton'  	
	INSERT INTO KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Sorrend
	,Modosithato
	,Egyeb	
	) values (
	 @recordId_KKBM_34
	,@Org
	,@kodcsoportId
	,'34'
	,'elektronikus úton'
	,NULL
	,'1'
	,NULL
	); 

	IF @org_kod <> 'NMHH'
		UPDATE KRT_KodTarak SET ErvVege= GETDATE() WHERE ID=@recordId_KKBM_34
END
GO
--------------------

-- BUG_10521, BUG_10522
-- eMigration/Selejtezés és eRecord/Személyek listája menüpont érvénytelenítés

DECLARE @ErvVege datetime
SET @ErvVege = '2019-09-01'

UPDATE KRT_MENUK
	SET ERVVEGE = @ErvVege
FROM KRT_MENUK
WHERE  (id='E9FCA57F-1B52-41FA-96E4-3C23C9DF1C99'  -- Személyek listája
	 or id='90A90DC3-2ADE-49DD-B447-DC990A608966'  -- Selejtezés 
	   )
  and ervvege > getdate()
	
GO
 --------------------------------------------------------------------------------
 --------------------------------------------------------------------------------
---------------------4677----------------------------
declare @recordid uniqueidentifier
set @recordid = '2E4B08BA-DA43-4436-A0DD-E982AF8B9322'

if not exists (select 1 from KRT_Funkciok where id = @recordid)
begin
	print 'insert'
	insert into KRT_Funkciok(
		Alkalmazas_Id
		,Modosithato
		,Id
		,Kod
		,Nev
		,Muvelet_Id
		) values (
		'20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		,'0'
		,@recordid
		,'TaroltEljarasList'
		,'Tárolt eljárások listázása'
		,'9508908E-AEFA-41C6-B6D7-807E3AE3A3AB'
		)
end

set @recordid = '223C8340-44D8-4620-A50B-85A195090E6F'
if not exists (select 1 from KRT_Szerepkor_Funkcio where id = @recordid)
and exists (select 1 from KRT_Szerepkorok where Id = 'E1676527-45DE-E711-80C2-00155D020D7E')
begin
	print 'insert into KRT_Szerepkor_Funkcio - vezeto'
	insert into KRT_Szerepkor_Funkcio(
		Szerepkor_Id
		,Id
		,Funkcio_Id
		) values (
		'E1676527-45DE-E711-80C2-00155D020D7E'
		,@recordid
		,'2E4B08BA-DA43-4436-A0DD-E982AF8B9322'
		)
end

set @recordid = 'F12A5351-3F3E-470C-B6C2-AFED4AE32833'
if not exists (select 1 from KRT_Szerepkor_Funkcio where id = @recordid)
begin
	print 'insert into KRT_Szerepkor_Funkcio - fejleszto'
	insert into KRT_Szerepkor_Funkcio(
		Szerepkor_Id
		,Id
		,Funkcio_Id
		) values (
		'E855F681-36ED-41B6-8413-576FCB5D1542'
		,@recordid
		,'2E4B08BA-DA43-4436-A0DD-E982AF8B9322'
		)
end
go

declare @recordid uniqueidentifier
set @recordid = '4C654566-9D1C-48B2-83C5-3BDDC6F27F67'

if not exists (select 1 from KRT_Funkciok where id = @recordid)
begin
	print 'insert'
	insert into KRT_Funkciok(
		Alkalmazas_Id
		,Modosithato
		,Id
		,Kod
		,Nev
		,Muvelet_Id
		) values (
		'20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		,'0'
		,@recordid
		,'WebSzolgaltatasList'
		,'Webszolgáltatások listázása'
		,'9508908E-AEFA-41C6-B6D7-807E3AE3A3AB'
		)
end

set @recordid = 'F2A4FD89-3479-4EF9-8225-13CAAF10B2D4'
if not exists (select 1 from KRT_Szerepkor_Funkcio where id = @recordid)
and exists (select 1 from KRT_Szerepkorok where Id = 'E1676527-45DE-E711-80C2-00155D020D7E')
begin
	print 'insert into KRT_Szerepkor_Funkcio - vezeto'
	insert into KRT_Szerepkor_Funkcio(
		Szerepkor_Id
		,Id
		,Funkcio_Id
		) values (
		'E1676527-45DE-E711-80C2-00155D020D7E'
		,@recordid
		,'4C654566-9D1C-48B2-83C5-3BDDC6F27F67'
		)
end

set @recordid = '05E7C480-F014-4987-9B6A-E191B140ED6A'
if not exists (select 1 from KRT_Szerepkor_Funkcio where id = @recordid)
begin
	print 'insert into KRT_Szerepkor_Funkcio - fejleszto'
	insert into KRT_Szerepkor_Funkcio(
		Szerepkor_Id
		,Id
		,Funkcio_Id
		) values (
		'E855F681-36ED-41B6-8413-576FCB5D1542'
		,@recordid
		,'4C654566-9D1C-48B2-83C5-3BDDC6F27F67'
		)
end
go

declare @recordid uniqueidentifier
set @recordid = 'B201F2FA-4150-47CA-BE61-1909750F35C6'

if not exists (select 1 from KRT_Funkciok where id = @recordid)
begin
	print 'insert'
	insert into KRT_Funkciok(
		Alkalmazas_Id
		,Modosithato
		,Id
		,Kod
		,Nev
		,Muvelet_Id
		) values (
		'20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'
		,'0'
		,@recordid
		,'WebOldalList'
		,'Oldalak listázása'
		,'9508908E-AEFA-41C6-B6D7-807E3AE3A3AB'
		)
end

set @recordid = '5692379C-AD0D-4209-9A33-2A8A7D87FE7E'
if not exists (select 1 from KRT_Szerepkor_Funkcio where id = @recordid)
and exists (select 1 from KRT_Szerepkorok where Id = 'E1676527-45DE-E711-80C2-00155D020D7E')
begin
	print 'insert into KRT_Szerepkor_Funkcio - vezeto'
	insert into KRT_Szerepkor_Funkcio(
		Szerepkor_Id
		,Id
		,Funkcio_Id
		) values (
		'E1676527-45DE-E711-80C2-00155D020D7E'
		,@recordid
		,'B201F2FA-4150-47CA-BE61-1909750F35C6'
		)
end

set @recordid = '91F31BE5-F613-4D13-A1D0-7EE0BAF4D49A'
if not exists (select 1 from KRT_Szerepkor_Funkcio where id = @recordid)
begin
	print 'insert into KRT_Szerepkor_Funkcio - fejleszto'
	insert into KRT_Szerepkor_Funkcio(
		Szerepkor_Id
		,Id
		,Funkcio_Id
		) values (
		'E855F681-36ED-41B6-8413-576FCB5D1542'
		,@recordid
		,'B201F2FA-4150-47CA-BE61-1909750F35C6'
		)
end
else
begin
	print 'update KRT_Szerepkor_Funkcio - fejleszto'
	update KRT_Szerepkor_Funkcio
	set
		Szerepkor_Id='E855F681-36ED-41B6-8413-576FCB5D1542'
		,Funkcio_Id='B201F2FA-4150-47CA-BE61-1909750F35C6'
	where Id = @recordid
end
go
-----------------4677 end----------------------------
------------------------------------------------------------------------------------
-- BUG_10649 Postai azonosító (Ragszám) kötelező érkeztetéskor függő kódtár (KULDEMENY_KULDES_MODJA) és rendszerparaméter (POSTAI_RAGSZAM_ERKEZTETESKOR) létrehozása

print 'POSTAI_RAGSZAM_ERKEZTETESKOR létrehozása  (NMHH-ban 1, TUK, FPH, BOPMH, stb: 0)'		
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='A690AC23-BECF-E911-80DF-00155D017B07') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'A690AC23-BECF-E911-80DF-00155D017B07'
		,'POSTAI_RAGSZAM_ERKEZTETESKOR'
		,'0'
		,'1'
		,'Postai ragszám megadása Érkeztetéskor kötelező (1=igen, 0=nem)'
		); 
 END 
 --ELSE 
 --BEGIN 
	-- Print 'UPDATE'
	-- UPDATE KRT_Parameterek
	-- SET 
	--	Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
	--	,Id='A690AC23-BECF-E911-80DF-00155D017B07'
	--	,Nev='POSTAI_RAGSZAM_ERKEZTETESKOR'
	--	,Ertek='0'
	--	,Karbantarthato='1'
	--	,Note='Postai ragszám megadása Érkeztetéskor kötelező (1=igen, 0=nem)'
	-- WHERE Id=@record_id 
 --END

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 


DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
--if  (@org_kod = 'NMHH') and (@isTUK = '0')
--BEGIN
--	 Print 'NMHH (nem TUK)'
--	 Print 'UPDATE'
--	 UPDATE KRT_Parameterek
--	 SET 
--		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
--		,Id='A690AC23-BECF-E911-80DF-00155D017B07'
--		,Nev='POSTAI_RAGSZAM_ERKEZTETESKOR'
--		,Ertek='1'
--		,Karbantarthato='1'
--		,Note='Postai ragszám megadása Érkeztetéskor kötelező (1=igen, 0=nem)'
--	 WHERE Id='A690AC23-BECF-E911-80DF-00155D017B07'
--END


  
Print 'POSTAI_AZONOSITO_KOTELEZO kódcsoport'


SET @record_id= null
SET @record_id = (select Id from KRT_KodCsoportok
			where Id='F3349CB0-B7CF-E911-80DF-00155D017B07') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
	insert into KRT_KodCsoportok(
	 BesorolasiSema
	,Id
	,Kod
	,Nev
	,Modosithato
	,Note
	) values (
	'0'
	,'F3349CB0-B7CF-E911-80DF-00155D017B07'
	,'POSTAI_AZONOSITO_KOTELEZO'
	,'Postai azonosító kötelező'
	,'0'
	,'Postai azonosító kötelezőség a küldemény küldésmódja mező alapján (függő kódtárhoz)'
	); 

END 
ELSE 
BEGIN 
	Print 'UPDATE'
	UPDATE KRT_KodCsoportok
	SET 
	BesorolasiSema='0'
	,Kod='POSTAI_AZONOSITO_KOTELEZO'
	,Nev='Postai azonosító kötelező'
	,Modosithato='0'	
	,Note='Postai azonosító kötelezőség a küldemény küldésmódja mező alapján (függő kódtárhoz)'
	WHERE Id='F3349CB0-B7CF-E911-80DF-00155D017B07'
	
END

SET @record_id= null
SET @record_id = (select Id from KRT_KodCsoportok
			where Id='F3349CB0-B7CF-E911-80DF-00155D017B07') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'ERROR: Nincs POSTAI_AZONOSITO_KOTELEZO kódcsoport vagy nem jó az ID-ja' 

END 
ELSE 
BEGIN 
    Print 'Igen kódtárérték' 
	SET @record_id= null
    SET @record_id = (select Id from KRT_KodTarak
		where Id='6ED574D9-B9CF-E911-80DF-00155D017B07') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 
	if @record_id IS NULL 
	BEGIN 
		
		Print 'INSERT' 
			insert into KRT_KodTarak(
				KodCsoport_Id
			,Org
			,Id
			,Kod
			,Nev
			,RovidNev
			,Modosithato
			,Sorrend
			) values (
			'F3349CB0-B7CF-E911-80DF-00155D017B07'
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'6ED574D9-B9CF-E911-80DF-00155D017B07'
			,'IGEN'
			,'Igen'
			,'Igen'
			,'0'
			,'1'
			); 
		END 
		ELSE 
		BEGIN 
			Print 'UPDATE'
			UPDATE KRT_KodTarak
			SET 
			 KodCsoport_Id='F3349CB0-B7CF-E911-80DF-00155D017B07'
			,Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Kod='IGEN'
			,Nev='IGEN'
			,RovidNev='Igen'
			,Modosithato='0'
			,Sorrend='1'
			WHERE Id='6ED574D9-B9CF-E911-80DF-00155D017B07'
	END
	
	
	-- Kódtár Függőség összerendelés POSTAI_AZONOSITO_KOTELEZO - Küldemény küldésmódja
	
	IF NOT EXISTS (SELECT 1 FROM KRT_KODTARFUGGOSEG WHERE ID='C0C27AAF-322D-4C93-A746-E67DF3DA56E0')
	BEGIN
		PRINT 'Kódtár Függőség összerendelés POSTAZANDO - Küldemény küldésmódja'
		INSERT [dbo].[KRT_KodtarFuggoseg] ([Id], [Org], [Vezerlo_KodCsoport_Id], [Fuggo_KodCsoport_Id], [Adat], [Aktiv]) 
		VALUES (N'C0C27AAF-322D-4C93-A746-E67DF3DA56E0', N'450b510a-7caa-46b0-83e3-18445c0c53a9', N'6982F668-C8CD-47A3-AC62-9F16E439473C', N'F3349CB0-B7CF-E911-80DF-00155D017B07', N'{"VezerloKodCsoportId":"6982f668-c8cd-47a3-ac62-9f16e439473c","FuggoKodCsoportId":"f3349cb0-b7cf-e911-80df-00155d017b07","Items":[{"VezerloKodTarId":"7eb11332-5a91-e711-80c2-00155d020d7e","FuggoKodtarId":"6ed574d9-b9cf-e911-80df-00155d017b07","Aktiv":true},{"VezerloKodTarId":"4411423d-5a91-e711-80c2-00155d020d7e","FuggoKodtarId":"6ed574d9-b9cf-e911-80df-00155d017b07","Aktiv":true},{"VezerloKodTarId":"35654f8c-a8fc-4d64-a46d-82007c4fcfb9","FuggoKodtarId":"6ed574d9-b9cf-e911-80df-00155d017b07","Aktiv":true},{"VezerloKodTarId":"482d5aa1-5991-e711-80c2-00155d020d7e","FuggoKodtarId":"6ed574d9-b9cf-e911-80df-00155d017b07","Aktiv":true}],"ItemsNincs":[]}', N'1')
	END
END

go

------------------------------------------------------------------------------------
   --NÉV: [Átmeneti irattár - Pécs] ID:[7ED4FB55-CEB9-E911-80CB-005056B1E698] -> Csoport név:[Átmeneti irattár - Pécs] Csoport Id:[E14F9B05-3373-E911-80D5-00155D59B14B]
  --Felelos_Csoport_Id: [9139D543-2D33-E811-80C6-005056B1E698] -> [E14F9B05-3373-E911-80D5-00155D59B14B]
  UPDATE [dbo].[EREC_IrattariHelyek]
  SET Felelos_Csoport_Id = 'E14F9B05-3373-E911-80D5-00155D59B14B'
  WHERE Id = '7ED4FB55-CEB9-E911-80CB-005056B1E698'
  
--BUG_5028 - Hibaüzenet a felugró ablakban
--VÉGE--

-------------------------------------------------------------------------------
-- BUG 8844 - Listák megjelenítési ideje (1619 folytatása)
-- A lekérdezések esetén a találati listában egy oldalon maximum ennyi elem jelenhet meg
-- 'LISTA_ELEMSZAM_MAX' rendszerparameter	 
-- 1000; (alapértelmezés)


DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
where Id='A18862F4-9642-4868-872B-97C9ED9836CB') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 

if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'A18862F4-9642-4868-872B-97C9ED9836CB'
		,'LISTA_ELEMSZAM_MAX'
		,'1000'
		,'1'
		,'A lekérdezések esetén a találati listában egy oldalon maximum ennyi elem jelenhet meg'
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Parameterek
	 SET 
		Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
		,Id='A18862F4-9642-4868-872B-97C9ED9836CB'
		,Nev='LISTA_ELEMSZAM_MAX'
		,Ertek='1000'
		,Karbantarthato='1'
		,Note='A lekérdezések esetén a találati listában egy oldalon maximum ennyi elem jelenhet meg'
	 WHERE Id=@record_id 
 END
 ----------------------------------------------------------------------------------------------------------------------------------------------
 -------------------- BUG 8844 - Listák megjelenítési ideje (1619 folytatása) -------------------------
 ---------------------------------------------------------------------VÉGE---------------------------------------------------------------------
 GO

 ----------- BUG 9608 ---------------
DECLARE @org_kod nvarchar(100) 
DECLARE @ertek nvarchar(1)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 
			
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='f33ceb0d-2c56-406c-b6fb-873b51f5374e') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 

IF (@org_kod = 'NMHH' OR @org_kod = 'FPH' OR @org_kod = 'CSPH')
BEGIN
	SET @ertek = '1'
END

IF (@org_kod = 'BOPMH')
BEGIN
	SET @ertek = '2'
END

if @record_id IS NULL 
BEGIN 
	Print 'ALAIRAS_VISSZAUTASITAS_KEZELES' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'f33ceb0d-2c56-406c-b6fb-873b51f5374e'
		,'ALAIRAS_VISSZAUTASITAS_KEZELES'
		,@ertek
		,'1'
		,'Alairas visszautasitasanak kezelese ugyfelenkent.'
		); 
END 
GO
------- BUG 9608 - VEGE ---------------


 ----------- BUG 8263 ---------------

DECLARE @org_kod nvarchar(100) 

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

IF (@org_kod = 'BOPMH')
BEGIN
	UPDATE KRT_Parameterek
	set Ertek = '1'
	where Id = 'FACE484E-5FE1-42CE-B967-3987F5F96338'
END
GO

------- BUG 8263 - VEGE ---------------