﻿
--------------------------------------------------------------------------------------------
GO
PRINT '-----------------------'
PRINT 'BUG 8879 ELHISZ BEKULDO'

DECLARE @KODCSOPORT_ID_8879 UNIQUEIDENTIFIER
SET @KODCSOPORT_ID_8879 = '8FCFCE0E-3319-4538-8CBA-E462D534AAAA'

PRINT 'KODCSOPORT'
IF NOT EXISTS (SELECT 1 FROM KRT_KODCSOPORTOK WHERE Id=@KODCSOPORT_ID_8879)
BEGIN 		
	PRINT 'INSERT - KCS - EBEADVANYFELADOTIPUS' 
	INSERT INTO KRT_KodCsoportok(
		 BesorolasiSema
		,Id
		,Kod
		,Nev
		,Modosithato
		,Note
		,Leiras
		) 
	VALUES (
		'0'
		,@KODCSOPORT_ID_8879
		,'EBEADVANYFELADOTIPUS'
		,'EBEADVANYFELADOTIPUS'
		,'0'
		,'E-beadvany felado típus'
		,'E-beadvany felado típus'
		); 
END 


declare @recordId_8879_0 uniqueidentifier
set @recordId_8879_0 = '7696CCF1-545A-4213-AE77-6B7B51AD2BD0'
declare @recordId_8879_1 uniqueidentifier
set @recordId_8879_1 = '7696CCF1-545A-4213-AE77-6B7B51AD2BD1'
declare @recordId_8879_2 uniqueidentifier
set @recordId_8879_2 = '7696CCF1-545A-4213-AE77-6B7B51AD2BD2'
declare @recordId_8879_3 uniqueidentifier
set @recordId_8879_3 = '7696CCF1-545A-4213-AE77-6B7B51AD2BD3'
declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id=@Org) 

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId_8879_0)
BEGIN
  	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId_8879_0
	,@Org
	,@KODCSOPORT_ID_8879
	,'0'
	,' 0 '
	,'0'
	,'0'
	); 

	IF (@org_kod = 'NMHH')  
		BEGIN
			 UPDATE KRT_KodTarak
			 SET  Nev='Elektronikus úton (Ügyfélkapu)'
				,Note='Elektronikus úton (Ügyfélkapu)'
			 WHERE Id=@recordId_8879_0
		END
	ELSE
		BEGIN
			 UPDATE KRT_KodTarak
			 SET  Nev='Állampolgár'
				,Note='Állampolgár'
			 WHERE Id=@recordId_8879_0
		END
END 

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId_8879_1)
BEGIN
  	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId_8879_1
	,@Org
	,@KODCSOPORT_ID_8879
	,'1'
	,' 1 '
	,'0'
	,'1'
	); 

	IF (@org_kod = 'NMHH')  
		BEGIN
			 UPDATE KRT_KodTarak
			 SET  Nev='Elektronikus úton (Hivatali kapu)'
				,Note='Elektronikus úton (Hivatali kapu)'
			 WHERE Id=@recordId_8879_1
		END
	ELSE
		BEGIN
			 UPDATE KRT_KodTarak
			 SET  Nev='Nem természetes személy (hivatali kapu, perkapu, cégkapu)'
				,Note='Nem természetes személy (hivatali kapu, perkapu, cégkapu)'
			 WHERE Id=@recordId_8879_1
		END
END 

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId_8879_2)
BEGIN
  	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId_8879_2
	,@Org
	,@KODCSOPORT_ID_8879
	,'2'
	,' 2 '
	,'0'
	,'2'
	); 

	IF (@org_kod = 'NMHH')  
		BEGIN
			 UPDATE KRT_KodTarak
			 SET  Nev='Rendszer'
				,Note='Rendszer'
			 WHERE Id=@recordId_8879_2
		END
	ELSE
		BEGIN
			 UPDATE KRT_KodTarak
			 SET  Nev='Rendszer'
				,Note='Rendszer'
			 WHERE Id=@recordId_8879_2
		END
END 

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId_8879_3)
BEGIN
  	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId_8879_3
	,@Org
	,@KODCSOPORT_ID_8879
	,'3'
	,' 3 '
	,'0'
	,'3'
	); 

	IF (@org_kod = 'NMHH')  
		BEGIN
			 UPDATE KRT_KodTarak
			 SET  Nev='Elektronikus úton (Cégkapu)'
				,Note='Elektronikus úton (Cégkapu)'
			 WHERE Id=@recordId_8879_3
		END
	ELSE
		BEGIN
			 UPDATE KRT_KodTarak
			 SET  Nev='Elektronikus úton (Cégkapu)'
				,Note='Elektronikus úton (Cégkapu)'
				,ErvVege= GETDATE()
			 WHERE Id=@recordId_8879_3
		END
END 

GO

-----------------

PRINT 'ADD NEW CIM TIPUS: MAK KOD'

declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @KODCSOPORT_ID_8879_CIM_TIPUS UNIQUEIDENTIFIER
SELECT TOP 1 @KODCSOPORT_ID_8879_CIM_TIPUS=ID FROM KRT_KodCsoportok WHERE KOD='CIM_TIPUS'

declare @recordId_8879_MAKKOD uniqueidentifier
set @recordId_8879_MAKKOD = 'CED4A781-1312-484B-AC88-EF3D96C388AA'

IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = @recordId_8879_MAKKOD)
BEGIN
  	
	insert into KRT_KodTarak(
	 Id
	,Org
	,KodCsoport_Id
	,Kod
	,Nev
	,Modosithato
	,Sorrend
	) values (
	 @recordId_8879_MAKKOD
	,@Org
	,@KODCSOPORT_ID_8879_CIM_TIPUS
	,'MAK_KOD'
	,'MÁK Kód'
	,'0'
	,'12'
	); 
END
GO
------------------------------------------------------------------------------------
----------------------------------BUG_9254------------------------------------------ 
/* Modify NEM_AKTIV_UGYIRAT_ALLAPOT value only for CSPH environment*/	
DECLARE @ORG_ID nvarchar(100) 
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

DECLARE @NEM_AKTIV_UGYIRAT_ALLAPOT_PARAMETER_ID uniqueidentifier 
SET @NEM_AKTIV_UGYIRAT_ALLAPOT_PARAMETER_ID = 'B6AB32F1-1FC3-4418-BB31-56090B3A9D86'
				 
--IF @ORG_KOD = 'CSPH'
--BEGIN
--	PRINT 'NEMAKTIV_UGYIRAT_ALLAPOT=''09'',''10'',''90'',''99'''
--	UPDATE KRT_Parameterek
--		SET Ertek = '''09'',''10'',''90'',''99'''
--	where Id = @NEM_AKTIV_UGYIRAT_ALLAPOT_PARAMETER_ID
--END
GO

------------------------------------------------------------------------------------
----------------------------------BUG_10774-----------------------------------------
/*
Add users to KözpontiIrattár group:
	Baranyi-Tóth Zsuzsanna
	Darabos Andrea
	Majorosné Filák Judit
	Lukács Barbara
	Forczekné Makkai Judit
	Kovács Tímea
	Szabóné Kiss Márta
	Grósz Anna
	Oláhné Jokán Ildikó
*/
DECLARE @ORG_ID nvarchar(100) 
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

DECLARE @KOZPONTI_IKTATO_CSOPORT_NEV nvarchar(100) 
SET @KOZPONTI_IKTATO_CSOPORT_NEV = 'Központi Iktató'

DECLARE @KOZPONTI_IKTATO_CSOPORT_ID uniqueidentifier
SET @KOZPONTI_IKTATO_CSOPORT_ID  = (select Id from KRT_Csoportok where Nev = @KOZPONTI_IKTATO_CSOPORT_NEV)

IF @ORG_KOD = 'NMHH' AND EXISTS(SELECT 1 FROM KRT_Csoportok WHERE Nev = @KOZPONTI_IKTATO_CSOPORT_NEV)
BEGIN
	PRINT 'Creating new user connection to Központi Iktató csoport group and add them as partner in hierarchy'
	DECLARE @LINK_ID uniqueidentifier
	DECLARE @NAME nvarchar(100)
	DECLARE @TIPUS nvarchar(64)
	
	SET @TIPUS = '2'

	SET @LINK_ID = '0AC9939D-AA1B-4D9F-B807-1D42221AD67A'
	SET @NAME = 'Baranyi-Tóth Zsuzsanna'
	PRINT @NAME
	IF NOT EXISTS (SELECT 1 FROM KRT_CsoportTagok WHERE Id = @LINK_ID)
	BEGIN		 		
		INSERT INTO KRT_CsoportTagok
		(
			Id,
			Tipus,
			Csoport_Id,
			Csoport_id_Jogalany			 
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
			@KOZPONTI_IKTATO_CSOPORT_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	 

	IF NOT EXISTS (SELECT 1 FROM KRT_PartnerKapcsolatok WHERE Id = @LINK_ID)
	BEGIN
		INSERT INTO KRT_PartnerKapcsolatok
		(
			Id,
			Tipus,
			Partner_id,
			Partner_id_kapcsolt			
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
		    (select Partner_id from KRT_Felhasznalok where Nev=@NAME),
			@KOZPONTI_IKTATO_CSOPORT_ID			 	
		)
	END	

	SET @LINK_ID = 'C4EFF87F-748D-4448-820A-C5127C82FB11'
	SET @NAME = 'Darabosné Deák Andrea'	
	PRINT @NAME
	IF NOT EXISTS (SELECT 1 FROM KRT_CsoportTagok WHERE Id = @LINK_ID)
	BEGIN	
		PRINT @NAME
		INSERT INTO KRT_CsoportTagok
		(
			Id,
			Tipus,
			Csoport_Id,
			Csoport_id_Jogalany			 
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
			@KOZPONTI_IKTATO_CSOPORT_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME or Nev='Deák Andrea') /* In the bug description the name is 'Deak Andrea' but in the DB there is only 'Darabosné Deák Andrea' exists*/			 	
		)
	END

	IF NOT EXISTS (SELECT 1 FROM KRT_PartnerKapcsolatok WHERE Id = @LINK_ID)
	BEGIN		 
		PRINT @NAME
		INSERT INTO KRT_PartnerKapcsolatok
		(
			Id,
			Tipus,
			Partner_id,
			Partner_id_kapcsolt			
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
		    (select Partner_id from KRT_Felhasznalok where Nev=@NAME),
			@KOZPONTI_IKTATO_CSOPORT_ID			 	
		)
	END	

	SET @LINK_ID = '96F96338-2C65-4124-8935-FB2FFAE83795'
	SET @NAME = 'Majorosné Filák Judit'
	PRINT @NAME
	IF NOT EXISTS (SELECT 1 FROM KRT_CsoportTagok WHERE Id = @LINK_ID)
 	BEGIN		 
	PRINT @NAME
		INSERT INTO KRT_CsoportTagok
		(
			Id,
			Tipus,
			Csoport_Id,
			Csoport_id_Jogalany			 
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
			@KOZPONTI_IKTATO_CSOPORT_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END
	IF NOT EXISTS (SELECT 1 FROM KRT_PartnerKapcsolatok WHERE Id = @LINK_ID)
	BEGIN		 
		PRINT @NAME
		INSERT INTO KRT_PartnerKapcsolatok
		(
			Id,
			Tipus,
			Partner_id,
			Partner_id_kapcsolt			
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
		    (select Partner_id from KRT_Felhasznalok where Nev=@NAME),
			@KOZPONTI_IKTATO_CSOPORT_ID			 	
		)
	END

	SET @LINK_ID = '71BC71E5-2C53-45E4-8464-5AF49AE32555'
	SET @NAME = 'Lukács Barbara'
	PRINT @NAME
	IF NOT EXISTS (SELECT 1 FROM KRT_CsoportTagok WHERE Id = @LINK_ID)
	BEGIN		 
		PRINT @NAME
		INSERT INTO KRT_CsoportTagok
		(
			Id,
			Tipus,
			Csoport_Id,
			Csoport_id_Jogalany			 
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
			@KOZPONTI_IKTATO_CSOPORT_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END
	
	IF NOT EXISTS (SELECT 1 FROM KRT_PartnerKapcsolatok WHERE Id = @LINK_ID)
	BEGIN		 
		PRINT @NAME
		INSERT INTO KRT_PartnerKapcsolatok
		(
			Id,
			Tipus,
			Partner_id,
			Partner_id_kapcsolt			
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
		    (select Partner_id from KRT_Felhasznalok where Nev=@NAME),
			@KOZPONTI_IKTATO_CSOPORT_ID			 	
		)
	END

	SET @LINK_ID = '9B13D929-E4DE-44C6-80AC-A092C9E80F94'
	SET @NAME = 'Forczekné Makkai Judit'
	PRINT @NAME
	IF NOT EXISTS (SELECT 1 FROM KRT_CsoportTagok WHERE Id = @LINK_ID)
	BEGIN		 
		PRINT @NAME
		INSERT INTO KRT_CsoportTagok
		(
			Id,
			Tipus,
			Csoport_Id,
			Csoport_id_Jogalany			 
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
			@KOZPONTI_IKTATO_CSOPORT_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END
	
	IF NOT EXISTS (SELECT 1 FROM KRT_PartnerKapcsolatok WHERE Id = @LINK_ID)
	BEGIN		 
		PRINT @NAME
		INSERT INTO KRT_PartnerKapcsolatok
		(
			Id,
			Tipus,
			Partner_id,
			Partner_id_kapcsolt			
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
		    (select Partner_id from KRT_Felhasznalok where Nev=@NAME),
			@KOZPONTI_IKTATO_CSOPORT_ID			 	
		)
	END

	SET @LINK_ID = 'B16D5185-4C5A-45B7-8555-752FDC3E7169'
	SET @NAME = 'Kovács Tímea'
	PRINT @NAME
	IF NOT EXISTS (SELECT 1 FROM KRT_CsoportTagok WHERE Id = @LINK_ID)
	BEGIN		 
	PRINT @NAME
		INSERT INTO KRT_CsoportTagok
		(
			Id,
			Tipus,
			Csoport_Id,
			Csoport_id_Jogalany			 
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
			@KOZPONTI_IKTATO_CSOPORT_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END

	IF NOT EXISTS (SELECT 1 FROM KRT_PartnerKapcsolatok WHERE Id = @LINK_ID)
	BEGIN		 
		PRINT @NAME
		INSERT INTO KRT_PartnerKapcsolatok
		(
			Id,
			Tipus,
			Partner_id,
			Partner_id_kapcsolt			
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
		    (select Partner_id from KRT_Felhasznalok where Nev=@NAME),
			@KOZPONTI_IKTATO_CSOPORT_ID			 	
		)
	END
	
	SET @LINK_ID = '3DC672DE-47E6-4D60-A765-BA7BE849FDFD'
	SET @NAME = 'Szabóné Kiss Márta'
	PRINT @NAME
	IF NOT EXISTS (SELECT 1 FROM KRT_CsoportTagok WHERE Id = @LINK_ID)
	BEGIN	
	PRINT @NAME
		INSERT INTO KRT_CsoportTagok
		(
			Id,
			Tipus,
			Csoport_Id,
			Csoport_id_Jogalany			 
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
			@KOZPONTI_IKTATO_CSOPORT_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END

	IF NOT EXISTS (SELECT 1 FROM KRT_PartnerKapcsolatok WHERE Id = @LINK_ID)
	BEGIN		 
		PRINT @NAME
		INSERT INTO KRT_PartnerKapcsolatok
		(
			Id,
			Tipus,
			Partner_id,
			Partner_id_kapcsolt			
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
		    (select Partner_id from KRT_Felhasznalok where Nev=@NAME),
			@KOZPONTI_IKTATO_CSOPORT_ID			 	
		)
	END

	
	SET @LINK_ID = '22358AEF-E49C-4025-B6F4-B74E9AEB9101'
	SET @NAME = 'Grósz Anna'
	PRINT @NAME
	IF NOT EXISTS (SELECT 1 FROM KRT_CsoportTagok WHERE Id = @LINK_ID)
	BEGIN		 
	PRINT @NAME
		INSERT INTO KRT_CsoportTagok
		(
			Id,
			Tipus,
			Csoport_Id,
			Csoport_id_Jogalany			 
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
			@KOZPONTI_IKTATO_CSOPORT_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END

	IF NOT EXISTS (SELECT 1 FROM KRT_PartnerKapcsolatok WHERE Id = @LINK_ID)
	BEGIN		 
		PRINT @NAME
		INSERT INTO KRT_PartnerKapcsolatok
		(
			Id,
			Tipus,
			Partner_id,
			Partner_id_kapcsolt			
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
		    (select Partner_id from KRT_Felhasznalok where Nev=@NAME),
			@KOZPONTI_IKTATO_CSOPORT_ID			 	
		)
	END
	 
	SET @LINK_ID = '827D35EE-6A16-439F-8C6B-136CAF9EF1CC'
	SET @NAME = 'Oláhné Jokán Ildikó'
	PRINT @NAME
	IF NOT EXISTS (SELECT 1 FROM KRT_CsoportTagok WHERE Id = @LINK_ID)	
	BEGIN		
	PRINT @NAME
		INSERT INTO KRT_CsoportTagok
		(
			Id,
			Tipus,
			Csoport_Id,
			Csoport_id_Jogalany			 
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
			@KOZPONTI_IKTATO_CSOPORT_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	 

	IF NOT EXISTS (SELECT 1 FROM KRT_PartnerKapcsolatok WHERE Id = @LINK_ID)
	BEGIN		 
		PRINT @NAME
		INSERT INTO KRT_PartnerKapcsolatok
		(
			Id,
			Tipus,
			Partner_id,
			Partner_id_kapcsolt			
		)
		VALUES
		(
			@LINK_ID,
			@TIPUS,
		    (select Partner_id from KRT_Felhasznalok where Nev=@NAME),
			@KOZPONTI_IKTATO_CSOPORT_ID			 	
		)
	END
END
GO

--------------------------BUG_10525 Hibás adatmez?k az "Egyszer?sített érkeztetés, iktatás" lapon ----------------
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

IF(@org_kod = 'NMHH')
BEGIN
	UPDATE [EREC_Obj_MetaAdatai]
	SET Opcionalis = 1
	WHERE Targyszavak_Id = (SELECT Id FROM [EREC_TargySzavak] WHERE TargySzavak = 'Illeték fizetés')
	and ErvVege > GETDATE()
END
GO
-----------------------------------------------------------------------------------------------------
----------------------------------------------BUG_10927----------------------------------------------

--TÜK-nél Oktatási segédanyagok menüpont eltávolítása
DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 

IF @isTUK = '1' 
BEGIN 
	UPDATE KRT_Menuk
	SET ErvVege = GETDATE()
	WHERE Id = 'F8B3F47A-26D4-E811-80CB-00155D027EA9'
END

GO

--Új Oktatási segédanyag modul létrehozása:
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Modulok
		where Id='FBA87F7A-FF5C-4DA4-9550-F10DA71CAA48') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
		
	Print 'INSERT' 
	 insert into KRT_Modulok(
		Id
		,Tipus
		,Kod
		,Nev
		) values (
		 'FBA87F7A-FF5C-4DA4-9550-F10DA71CAA48'
		,'F'
		,'OktatasiAnyagokList.aspx'
		,'Oktatási anyagok listája'
		); 
 END 
 ELSE 
 BEGIN 
	 Print 'UPDATE'
	 UPDATE KRT_Modulok
	 SET 
		 Id='FBA87F7A-FF5C-4DA4-9550-F10DA71CAA48'
		,Tipus='F'
		,Kod='OktatasiAnyagokList.aspx'
		,Nev = 'Oktatási anyagok listája'
	 WHERE Id=@record_id 
 END
 GO

 --Oktatási segédanyagok listája menü moduljának beállítása:
 UPDATE KRT_Menuk
 SET Modul_Id = 'FBA87F7A-FF5C-4DA4-9550-F10DA71CAA48',
 Parameter = 'Startup=OktatasiAnyagok'
 WHERE Id = 'F8B3F47A-26D4-E811-80CB-00155D027EA9'

 GO

--Oktatási segédanyag érvényességének beállítása NMHH-nál:
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

IF(@org_kod = 'NMHH')
BEGIN
	UPDATE KRT_KodTarak
	SET ErvVege = '4700-12-31'		
	WHERE Id = '8068BDA4-26D4-E811-80CB-00155D027EA9'
END
----------------------------------------------------------------------------------------------------
----------------------------------------------BUG_10927----------------------------------------------
------------------------------------------------VÉGE-------------------------------------------------
GO
-----------------------------------------------Bug_10895-------------------------------------------------------------
DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = 'EE715582-0B5D-4C4C-8135-A2A4D4DF9452'

IF @org_kod = 'NMHH'
set @ertek = '1'
else
set @ertek = '0'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - IRATTAROZAS_LISTA_SZURES_IRATTAROS_ALTAL  - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'IRATTAROZAS_LISTA_SZURES_IRATTAROS_ALTAL'
		,@ertek
		,'1'
		,'Ha a paraméter értéke 0, akkor ha irattáros a bejelentkezett felhasználó, nem szűrjük az átmeneti és központi irattárak listáját. Ha 1 az értéke, akkor ha irattáros a bejelentkezett felhasználó, szűrjük az átmeneti és központi irattárak listáját.'
		); 
 END
 GO
----------------------------------------------------------------------------------------------------
----------------------------------------------BUG_11233----------------------------------------------

DECLARE @ORG_ID uniqueidentifier
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF(@ORG_KOD = 'NMHH')
BEGIN
	PRINT 'BUG_11233: Minden technikai postázóhoz hozzá kell rendelni a postázás elvégzéséhez szükséges funkcionalitásokkal rendelkező szerepkört.' 
	DECLARE @POSTAZO_SZEREPKOR_NEV nvarchar(100) 
	SET @POSTAZO_SZEREPKOR_NEV = 'POSTAZO'
	DECLARE @POSTAZO_SZEREPKOR_ID uniqueidentifier
	SET @POSTAZO_SZEREPKOR_ID = 'D966D634-9C0E-456B-BFB2-AF00573905A3'

	IF NOT EXISTS (SELECT 1 FROM KRT_Szerepkorok WHERE Id = @POSTAZO_SZEREPKOR_ID)
	BEGIN
		PRINT 'Creating POSTAZO in SzerepKorok table' 
		INSERT INTO KRT_Szerepkorok (Id,Nev,ErvKezd,Org) VALUES(@POSTAZO_SZEREPKOR_ID,@POSTAZO_SZEREPKOR_NEV,GETDATE(),@ORG_ID)
	END	 

	DECLARE @FUNKCIO_KOD nvarchar(100) 
	PRINT 'Starting checking interested Szerepkor-s to Funkcio-s' 

	--SET @FUNKCIO_KOD = 'Munkanaplo'
	--IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	--INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	--BEGIN
	--	PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
	--	INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('1E8EE6B1-EF17-DE11-BD91-001EC9E754BC',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	--END

	SET @FUNKCIO_KOD = 'KimenoKuldemenySztorno'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('98617706-68F7-E611-9444-00155D1098F2',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	--SET @FUNKCIO_KOD = 'Statisztikak'
	--IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	--INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	--BEGIN
	--	PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
	--	INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('6BAE3167-47A7-DD11-9B9D-001EC9E754BC',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	--END
	
	SET @FUNKCIO_KOD = 'IratPeldanyAtadas'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('88D112C1-EAEC-488E-ADF3-0D8C3F963510',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KuldemenyView'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('99D0F132-8B27-439A-A68B-A211F9A52E81',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'IratPeldanyAtvetel'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('330956F6-836F-40AB-BC0A-6CBD44B4B003',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'HelyettesitesekList'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('80F7A41C-1019-DE11-BD91-001EC9E754BC',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KimenoKuldemenyekList'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('42467DFB-3576-4805-8045-E1EC991C5F76',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'MegbizasokList'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('14537356-1019-DE11-BD91-001EC9E754BC',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'Atadandok'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD )
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('B14F9F01-4812-4CE8-AB64-2BDF711DA481',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD and KRT_Funkciok.Id ='B8C2330E-CD9B-423C-A72C-3140313C65DF'),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'Atveendok'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('97393C3F-095D-4FBC-ADAD-48198138EA4E',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'Expedialas'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('F22011A2-53D2-441F-9EE0-3817710ECCDD',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KimenoKuldemenyFelvitel'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('0CBB50E7-F9ED-4DC5-A1A3-4EBA749CB11F',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KuldTertivevenyekList'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('D3FC8CC6-BB32-E011-889B-001EC9E754BC',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'Postazas'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('C9F0D84C-C65C-40BC-8654-3F20DCFA36BC',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KuldTertivevenyErkeztetes'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('30726F4D-BB32-E011-889B-001EC9E754BC',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KRT_RagszamSavokInvalidate'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('45754A9A-FFDF-4B5B-9302-40F4666B8617',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END
 
 	SET @FUNKCIO_KOD = 'KRT_RagszamSavokKezeles'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('A19015A6-DCEE-4F29-B765-4919AFEDFB67',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KRT_RagszamSavokNew'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('D0791846-A962-4AC3-B92B-3626B38F4F0D',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KRT_RagszamSavokList'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('371E8D42-B693-4FF0-8009-84879B9CB867',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KRT_RagszamSavokView'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('0FB73FD4-DA89-4808-9757-EBC273ADDB0B',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'KRT_RagszamSavokModify'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('86C01C9E-71E4-42ED-8806-96A9BB248989',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'IratPeldanyokList'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('34EC8E9B-9D12-4385-8A29-C24C57DF00B9',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD and ErvVege > GETDATE()),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'IratPeldanyView'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('2932A700-89A0-4B40-A66A-96914796B7F0',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD and ErvVege > GETDATE()),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'IratpeldanyUgyiratbaHelyezes'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('760670F2-3B10-4167-BD91-2B982786AE82',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD and ErvVege > GETDATE()),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	SET @FUNKCIO_KOD = 'IratPeldanyAtadasSztorno'
	IF NOT EXISTS (SELECT KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Id, KRT_Szerepkor_Funkcio.Szerepkor_Id,KRT_Szerepkor_Funkcio.Funkcio_Id FROM KRT_Szerepkor_Funkcio
	INNER JOIN KRT_Funkciok ON KRT_Szerepkor_Funkcio.Funkcio_Id = KRT_Funkciok.Id WHERE KRT_Szerepkor_Funkcio.Szerepkor_Id = @POSTAZO_SZEREPKOR_ID and KRT_Funkciok.Kod = @FUNKCIO_KOD)
	BEGIN
		PRINT 'Linking ' + @FUNKCIO_KOD + ' funkcio to ' + @POSTAZO_SZEREPKOR_NEV + ' szerepkor' 
		INSERT INTO KRT_Szerepkor_Funkcio(Id,Funkcio_id,Szerepkor_Id,ErvKezd) VALUES('2E22F1D8-F3D9-42D0-B24F-E10BCC52228C',(select distinct Id from KRT_Funkciok where Kod = @FUNKCIO_KOD and ErvVege > GETDATE()),@POSTAZO_SZEREPKOR_ID,GETDATE())
	END

	PRINT 'Linking users to POSTAZO szerepkor.'
	DECLARE @LINK_ID uniqueidentifier
	DECLARE @NAME nvarchar(100)
	DECLARE @TIPUS nvarchar(64)
	
	SET @TIPUS = '2'
	
	SET @LINK_ID = 'D2499E62-7B6D-45C7-B18D-E3FEB3669197'
	SET @NAME = 'Debrecen Technikai Postázó'
	
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN		 		
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	 

	SET @LINK_ID = 'F25E0759-980D-4417-A1EC-5700B81F10B0'
	SET @NAME = 'Miskolc Technikai Postázó'
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN	
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	

	SET @LINK_ID = 'AAA59999-73F2-44AD-BAD8-94FDB755C105'
	SET @NAME = 'Ostrom Technikai Postázó'
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN		
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	

	SET @LINK_ID = 'DFD830FA-DF15-4132-80B2-54FA4D4C2FDC'
	SET @NAME = 'Pécs Technikai Postázó'
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN		
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	

	SET @LINK_ID = '51B0671E-2CDA-437F-99AE-3DC78B751BE3'
	SET @NAME = 'Reviczky Technikai Postázó'
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN		
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	

	SET @LINK_ID = 'F3ADF785-8CDA-4C01-9892-E82DF25BA753'
	SET @NAME = 'Sopron Technikai Postázó'
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN	
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	

	SET @LINK_ID = '58308079-95D3-450E-9A81-37C9F7F9BF5F'
	SET @NAME = 'Szeged Technikai Postázó'
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN		
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	

	SET @LINK_ID = '3BDD6509-F694-418E-8400-36734D4AE053'
	SET @NAME = 'Visegrádi Technikai Postázó'
	IF NOT EXISTS (SELECT 1 FROM KRT_Felhasznalo_Szerepkor WHERE Id = @LINK_ID)
	BEGIN		
	PRINT 'Adding user: ' +  @NAME
		INSERT INTO KRT_Felhasznalo_Szerepkor
		(
			Id,			
			Szerepkor_Id,
			Felhasznalo_Id			 
		)
		VALUES
		(
			@LINK_ID,
			@POSTAZO_SZEREPKOR_ID,
			(select Id from KRT_Felhasznalok where Nev=@NAME) 			 	
		)
	END	
END
GO

----------------------------------------------------------------------------------------------------
----------------------------------------------BUG_11233 VÉGE----------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------
 -----------------------------------------------Bug_10557-------------------------------------------------------------
DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = 'AC383BF3-5C74-4729-88D3-747817C533D0'

IF @org_kod = 'BOPMH' or @org_kod = 'CSPH'
set @ertek = '23|28|32|36|45'
else
set @ertek = ''

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - PKI_RETRY_CODES - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'PKI_RETRY_CODES'
		,@ertek
		,'1'
		,'A paraméter tartalmazza azokat a visszatérési értékek listáját "|" jellel elválasztva, amelyek esetén az adott fájl aláírását még a folyamaton belül újra kell próbálni (pl. kommunikációs hibára utaló hibakódok).'
		); 
 END
 GO

 DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = '4E1394B2-6001-4EFD-B37C-91E72F996169'

IF @org_kod = 'BOPMH' or @org_kod = 'CSPH'
set @ertek = '2'
else
set @ertek = '0'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - PKI_RETRY_NUM - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'PKI_RETRY_NUM'
		,@ertek
		,'1'
		,'A paraméter meghatározza, hogy a PKI_RETRY_CODES értékkel visszatérő tételeket a rendszer hányszor próbálja meg újra aláírni.'
		); 
 END
 GO
 ---------------------------------------------------------------------------------------------------------------------
----------------------------------BUG_8847--------------------------------------------
/* Modify Vezetoi Panel aspx path when org is NMHH*/	 
DECLARE @ORG_ID nvarchar(100) 
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

DECLARE @NEM_AKTIV_UGYIRAT_ALLAPOT_PARAMETER_ID uniqueidentifier 
SET @NEM_AKTIV_UGYIRAT_ALLAPOT_PARAMETER_ID = 'B6AB32F1-1FC3-4418-BB31-56090B3A9D86'
				 
IF @ORG_KOD = 'NMHH'
BEGIN
	PRINT 'Updating VezetoPanel.aspx link in menu to VezetoiPanelNMHH.aspx'
	UPDATE KRT_Modulok
	SET Kod = 'VezetoiPanelNMHH.aspx'
	WHERE Id = 'FADB2FE0-36FF-401A-A5E0-C1D7712CCB0F'


-- 8847 (Áthozva Script.PostDeployment.sql és javítva itt ---------------
	declare @id uniqueidentifier
	set @id = '14D5C588-67B3-E911-80DB-00155D027E1B'

	IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @id)
	BEGIN
		insert into KRT_Forditasok
		 (
			 [Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'VezetoiPanelNMHH.aspx',
			'Label_FuttatasIdeje',
			'Lekérdezés időszaka:'
		)
		PRINT 'INSERTED Komponens = VezetoiPanelNMHH.aspx AND ObjAzonosito = Label_FuttatasIdeje'
	END
	ELSE
	BEGIN
		UPDATE KRT_Forditasok
			SET  Modul = 'eRecord'
				,Komponens = 'VezetoiPanelNMHH.aspx'
				,ObjAzonosito = 'Label_FuttatasIdeje'
				,Forditas = 'Lekérdezés időszaka:'
		WHERE id =@id  -- Komponens = 'VezetoiPanelNMHH.aspx' AND ObjAzonosito = 'Label_FuttatasIdeje'

		PRINT 'UPDATED Komponens = VezetoiPanelNMHH.aspx AND ObjAzonosito = Label_FuttatasIdeje'
	END

	set @id = '6567F7CB-67B3-E911-80DB-00155D027E1B'

	IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @id)
	BEGIN
		insert into KRT_Forditasok
			(
				[Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'VezetoiPanelNMHH.aspx',
			'labelAlosztaly',
			'Szervezeti egység:'
		)
		PRINT 'INSERTED Komponens = VezetoiPanelNMHH.aspx AND ObjAzonosito = labelAlosztaly'
	END
	ELSE
	BEGIN
		UPDATE KRT_Forditasok
			SET  Modul = 'eRecord'
				,Komponens = 'VezetoiPanelNMHH.aspx'
				,ObjAzonosito = 'labelAlosztaly'
				,Forditas = 'Szervezeti egység:'
		WHERE id =@id -- Komponens = 'VezetoiPanelNMHH.aspx' AND ObjAzonosito = 'labelAlosztaly'

		PRINT 'UPDATED Komponens = VezetoiPanelNMHH.aspx AND ObjAzonosito = labelAlosztaly'
	END	


	set @id = 'B08AA689-CD29-4CEB-83B1-7D3F8A89928E'

	IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = @id)
	BEGIN
		insert into KRT_Forditasok
			(
				[Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
		)
		VALUES
		(
			@id,
			'hu-HU',
			'NMHH',
			'eRecord',
			'VezetoiPanelNMHH.aspx',
			'Label_ErkeztetettKuldemenyekSzama',
			'Érkezett küldemények száma (teljes szervezet):'
		)
		PRINT 'INSERTED Komponens = VezetoiPanelNMHH.aspx AND ObjAzonosito = Label_ErkeztetettKuldemenyekSzama'
	END
	ELSE
	BEGIN
		UPDATE KRT_Forditasok
			SET Modul = 'eRecord'
				,Komponens = 'VezetoiPanelNMHH.aspx'
				,ObjAzonosito = 'Label_ErkeztetettKuldemenyekSzama'
				,Forditas = 'Érkezett küldemények száma (teljes szervezet):'
		WHERE id = @id  --Komponens = 'VezetoiPanelNMHH.aspx' AND ObjAzonosito = 'Label_ErkeztetettKuldemenyekSzama'

		PRINT 'UPDATED Komponens = VezetoiPanelNMHH.aspx AND ObjAzonosito = Label_ErkeztetettKuldemenyekSzama'
	END
	
END
GO
 ---------------------------------------------------------------------------------------------------------------------

 
-------------------------------------------------------------------------------
-- BLG 4391 - Fődokumentum kezelés
-- 'Ha a paraméter értéke 1, akkor PDF csatolmány feltöltésnél kerüljön figyelésre, hogy van-e 
-- feltöltve azonos nevű Word (doc, docx) dokumentum fődokumentumként és ha igen, akkor a feltöltött fájl lesz a fődokumentum, 
-- az eredeti fájl pedig 'Tervezet' dokumentum szerepet kap. Ha 0, akkor nem figyeljük.
-- AZONOS_NEVU_CSATOLMANY_ENABLED rendszerparameter	 
-- BOPMH-ban: 1 
-- NMHH, CSPH, FPH _0
DECLARE @org_kod NVARCHAR(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='FFBEA8E0-676E-48E5-8371-F8AC28045CCF') 

IF ((@org_kod = 'BOPMH')) 
BEGIN
			
	

	Print '@record_id='+convert(NVARCHAR(100),@record_id) 

	if @record_id IS NULL 
	BEGIN 
		Print 'INSERT' 
		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Id
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 null
			,null
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'FFBEA8E0-676E-48E5-8371-F8AC28045CCF'
			,'AZONOS_NEVU_CSATOLMANY_ENABLED'
			,'1'
			,'1'
			,'Ha a paraméter értéke 1, akkor PDF csatolmány feltöltésnél kerüljön figyelésre, hogy van-e 
			  feltöltve azonos nevű Word (doc, docx) dokumentum fődokumentumként és ha igen, akkor a feltöltött fájl lesz a f?dokumentum, 
			  az eredeti fájl pedig ''Tervezet'' dokumentum szerepet kap. Ha 0, akkor nem figyeljük.'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Parameterek
		 SET 
			Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Id=@record_id
			,Nev='AZONOS_NEVU_CSATOLMANY_ENABLED'
			,Ertek='1'
			,Karbantarthato='1'
			,Note='Ha a paraméter értéke 1, akkor PDF csatolmány feltöltésnél kerüljön figyelésre, hogy van-e 
			  feltöltve azonos nevű Word (doc, docx) dokumentum fődokumentumként és ha igen, akkor a feltöltött fájl lesz a fődokumentum, 
			  az eredeti fájl pedig ''Tervezet'' dokumentum szerepet kap. Ha 0, akkor nem figyeljük.'
		 WHERE Id=@record_id 
	 END
 END
 ELSE -- NMHH, CSPH, FPH: 0 érték
 BEGIN
 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 

	if @record_id IS NULL 
	BEGIN 
		Print 'INSERT' 
		 insert into KRT_Parameterek(
			 Tranz_id
			,Stat_id
			,Org
			,Id
			,Nev
			,Ertek
			,Karbantarthato
			,Note
			) values (
			 null
			,null
			,'450b510a-7caa-46b0-83e3-18445c0c53a9'
			,'FFBEA8E0-676E-48E5-8371-F8AC28045CCF'
			,'AZONOS_NEVU_CSATOLMANY_ENABLED'
			,'0'
			,'1'
			,'Ha a paraméter értéke 1, akkor PDF csatolmány feltöltésnél kerüljön figyelésre, hogy van-e 
			  feltöltve azonos nevű Word (doc, docx) dokumentum fődokumentumként és ha igen, akkor a feltöltött fájl lesz a fődokumentum, 
			  az eredeti fájl pedig ''Tervezet'' dokumentum szerepet kap. Ha 0, akkor nem figyeljük.'
			); 
	 END 
	 ELSE 
	 BEGIN 
		 Print 'UPDATE'
		 UPDATE KRT_Parameterek
		 SET 
			Org='450b510a-7caa-46b0-83e3-18445c0c53a9'
			,Id=@record_id
			,Nev='AZONOS_NEVU_CSATOLMANY_ENABLED'
			,Ertek='0'
			,Karbantarthato='1'
			,Note='Ha a paraméter értéke 1, akkor PDF csatolmány feltöltésnél kerüljön figyelésre, hogy van-e 
			  feltöltve azonos nevű Word (doc, docx) dokumentum fődokumentumként és ha igen, akkor a feltöltött fájl lesz a fődokumentum, 
			  az eredeti fájl pedig ''Tervezet'' dokumentum szerepet kap. Ha 0, akkor nem figyeljük.'
		 WHERE Id=@record_id 
	 END
 END

 GO
 
 -------Dokumentum szerep kódcsoporthoz kerüljön felvételre a 'Tervezet' érték-------------

DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_KodTarak
			where Id='52E10234-2449-474B-A0D6-C7C95CCBAF99') 

Print '@record_id='+convert(NVARCHAR(100),@record_id) 


IF @record_id IS NULL 
BEGIN
INSERT INTO [dbo].[KRT_KodTarak]
           ([Id]
           ,[Org]
           ,[KodCsoport_Id]
           ,[ObjTip_Id_AdatElem]
           ,[Obj_Id]
           ,[Kod]
           ,[Nev]
           ,[RovidNev]
           ,[Egyeb]
           ,[Modosithato]
           ,[Sorrend]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('52E10234-2449-474B-A0D6-C7C95CCBAF99'
           ,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
           ,'05B7B2D4-C84A-4F9A-B466-565EF2954C46'
           ,null
           ,null
           ,'05'
           ,'Tervezet' 
           ,null
           ,null
           ,1
           ,5
           ,1
           ,null
           ,'A0848405-E664-4E79-8FAB-CFECA4A290AF'
           ,'2019-10-24 00:00:00.000'
           ,'4700-12-31 00:00:00.000'
           ,null
           ,'2019-10-24 00:00:00.000'
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null)
END
-------Dokumentum szerep kódcsoporthoz kerüljön felvételre a 'Tervezet' érték-------------
-----------------------------------VÉGE---------------------------------------------------



 --------------------------------------------------------------------------------
 ----------------- BLG 4391 - Fődokumentum kezelés vége -------------------------


----------------------------------------------------------------------------------------------------
----------------------------------------------BUG_8536----------------------------------------------
----------------------------------------* KRT_KODCSOPORTOK *----------------------------------------
IF NOT EXISTS (SELECT 1 FROM KRT_KodCsoportok WHERE Id ='ECD56FB6-8558-4AE6-99AD-635C5F29F33E')
INSERT INTO [dbo].[KRT_KodCsoportok]
           ([Id]
           ,[Org]
           ,[KodTarak_Id_KodcsoportTipus]
           ,[Kod]
           ,[Nev]
           ,[Modosithato]
           ,[Hossz]
           ,[Csoport_Id_Tulaj]
           ,[Leiras]
           ,[BesorolasiSema]
           ,[KiegAdat]
           ,[KiegMezo]
           ,[KiegAdattipus]
           ,[KiegSzotar]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('ECD56FB6-8558-4AE6-99AD-635C5F29F33E',
           NULL,
           'A54AF681-ADA2-420F-860F-826257E95932',
           'TERTI_VISSZA_KOD_POSTA',
           'TERTI_VISSZA_KOD_POSTA',
           1,
           0,
            NULL,
           'Tértivevény visszaérkeztetés postai megnevezése',
           0,
		   NULL,
           NULL,
           NULL,
           NULL,
           1,
           'Tértivevény visszaérkeztetés postai megnevezése',
           NULL,
           '2019-10-16',
           '4700-12-31',
            NULL,
           '2019-10-16',
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
			)
GO
-------------------------------------------* KRT_KODCSOPORTOK *------------------------------------------
--------------------------------------------------* VÉGE *-----------------------------------------------

---------------------------------------------* KRT_KODTARAK *--------------------------------------------

IF NOT EXISTS (SELECT 1 FROM KRT_KODTARAK WHERE ID ='A6768FB9-0984-4232-8D04-B354290C9F36')
INSERT INTO [dbo].[KRT_KodTarak]
           ([Id]
           ,[Org]
           ,[KodCsoport_Id]
           ,[ObjTip_Id_AdatElem]
           ,[Obj_Id]
           ,[Kod]
           ,[Nev]
           ,[RovidNev]
           ,[Egyeb]
           ,[Modosithato]
           ,[Sorrend]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('A6768FB9-0984-4232-8D04-B354290C9F36',
           '450B510A-7CAA-46B0-83E3-18445C0C53A9',
           'ECD56FB6-8558-4AE6-99AD-635C5F29F33E',
           NULL,
           NULL,
          '1',
           'Nem kereste',
           NULL,
           NULL,
           1,
           1,
           1,
           NULL,
           NULL,
            '2019-10-16',
           '4700-12-31',
            NULL,
           '2019-10-16',
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
			)
GO
if not exists (select 1 from KRT_KodTarak where Id ='855A7BF1-AE91-4292-B034-1F75A1090353')
INSERT INTO [dbo].[KRT_KodTarak]
           ([Id]
           ,[Org]
           ,[KodCsoport_Id]
           ,[ObjTip_Id_AdatElem]
           ,[Obj_Id]
           ,[Kod]
           ,[Nev]
           ,[RovidNev]
           ,[Egyeb]
           ,[Modosithato]
           ,[Sorrend]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('855A7BF1-AE91-4292-B034-1F75A1090353',
           '450B510A-7CAA-46B0-83E3-18445C0C53A9',
           'ECD56FB6-8558-4AE6-99AD-635C5F29F33E',
           NULL,
           NULL,
          '2',
           'Meghatalmazottnak',
           NULL,
           NULL,
           1,
           2,
           1,
           NULL,
           NULL,
            '2019-10-16',
           '4700-12-31',
            NULL,
           '2019-10-16',
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
			)
GO
IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE Id ='0C87EA7B-547E-414A-A776-9C11BEFDB3A7')
INSERT INTO [dbo].[KRT_KodTarak]
           ([Id]
           ,[Org]
           ,[KodCsoport_Id]
           ,[ObjTip_Id_AdatElem]
           ,[Obj_Id]
           ,[Kod]
           ,[Nev]
           ,[RovidNev]
           ,[Egyeb]
           ,[Modosithato]
           ,[Sorrend]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('0C87EA7B-547E-414A-A776-9C11BEFDB3A7',
           '450B510A-7CAA-46B0-83E3-18445C0C53A9',
           'ECD56FB6-8558-4AE6-99AD-635C5F29F33E',
           NULL,
           NULL,
          '3',
           'Helyettes átvevőnek',
           NULL,
           NULL,
           1,
           3,
           1,
           NULL,
           NULL,
            '2019-10-16',
           '4700-12-31',
            NULL,
           '2019-10-16',
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
			)
GO

IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE Id ='C25D41D8-778A-4588-9898-C7FE0BE2E4A6')
INSERT INTO [dbo].[KRT_KodTarak]
           ([Id]
           ,[Org]
           ,[KodCsoport_Id]
           ,[ObjTip_Id_AdatElem]
           ,[Obj_Id]
           ,[Kod]
           ,[Nev]
           ,[RovidNev]
           ,[Egyeb]
           ,[Modosithato]
           ,[Sorrend]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('C25D41D8-778A-4588-9898-C7FE0BE2E4A6',
           '450B510A-7CAA-46B0-83E3-18445C0C53A9',
           'ECD56FB6-8558-4AE6-99AD-635C5F29F33E',
           NULL,
           NULL,
          '4',
           'Közvetett kézbesítőnek',
           NULL,
           NULL,
           1,
           4,
           1,
           NULL,
           NULL,
            '2019-10-16',
           '4700-12-31',
            NULL,
           '2019-10-16',
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
			)
GO

IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE Id ='7F4442D7-3218-421C-BE8D-C5BE7969EB9B')
INSERT INTO [dbo].[KRT_KodTarak]
           ([Id]
           ,[Org]
           ,[KodCsoport_Id]
           ,[ObjTip_Id_AdatElem]
           ,[Obj_Id]
           ,[Kod]
           ,[Nev]
           ,[RovidNev]
           ,[Egyeb]
           ,[Modosithato]
           ,[Sorrend]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id])
     VALUES
           ('7F4442D7-3218-421C-BE8D-C5BE7969EB9B',
           '450B510A-7CAA-46B0-83E3-18445C0C53A9',
           'ECD56FB6-8558-4AE6-99AD-635C5F29F33E',
           NULL,
           NULL,
          '5',
           'Címzettnek',
           NULL,
           NULL,
           1,
           5,
           1,
           NULL,
           NULL,
            '2019-10-16',
           '4700-12-31',
            NULL,
           '2019-10-16',
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
			)
GO
---------------------------------------------* KRT_KODTARAK *--------------------------------------------
--------------------------------------------------* VÉGE *-----------------------------------------------
---------------------------------------------------------------------------------------------------------
----------------------------------------------BUG_8536 VÉGE----------------------------------------------

-------------------------BLG 11345 Automatikus ügyintézés FPH e-szabály érkeztetés nélkül-------------------------
DECLARE @Org UNIQUEIDENTIFIER
SET @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id=@Org) 

IF (@org_kod = 'NMHH')  
BEGIN
	IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = 'F4067B24-EDA7-4FA8-976C-B2C7414F3216')
	BEGIN
		INSERT INTO KRT_Forditasok
		(
		[Id]
		,[NyelvKod]
		,[OrgKod]
		,[Modul]
		,[Komponens]
		,[ObjAzonosito]
		,[Forditas]
		)
		VALUES
		(
		'F4067B24-EDA7-4FA8-976C-B2C7414F3216',
		'hu-HU',
		@org_kod,
		'eAdmin',
		'EUzenetSzabalyForm.aspx',
		'lblInterfeszAdatlaptipus',
		'Interfész adatlaptípus:'
		)
	END
END
ELSE
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM KRT_Forditasok WHERE Id = 'F4067B24-EDA7-4FA8-976C-B2C7414F3216')
		BEGIN
			INSERT INTO KRT_Forditasok
			(
			[Id]
			,[NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas]
			)
			VALUES
			(
			'F4067B24-EDA7-4FA8-976C-B2C7414F3216',
			'hu-HU',
			@org_kod,
			'eAdmin',
			'EUzenetSzabalyForm.aspx',
			'lblInterfeszAdatlaptipus',
			'Név:'
			)
		END
END

-------------------------BLG 11345 Automatikus ügyintézés FPH e-szabály érkeztetés nélkül-------------------------
-------------------------------------------------------VÉGE-------------------------------------------------------

------- BLG 11646
DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Parameterek
			where Id='2170ba34-636d-4eb6-8d2d-2e2bdbc25898') 
Print '@record_id='+convert(NVARCHAR(100),@record_id) 
if @record_id IS NULL 
BEGIN 
	Print 'INSERT' 
	 insert into KRT_Parameterek(
		 Tranz_id
		,Stat_id
		,Org
		,Id
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		 null
		,null
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'2170ba34-636d-4eb6-8d2d-2e2bdbc25898'
		,'CSATOLMANY_LETOLTES_MOD'
		,'1'
		,'1'
		,'A bongeszot a file letoltesere vagy megnyitasara kenyszeriti (1=letoltes, 0=megnyitas)'
		); 
 END 