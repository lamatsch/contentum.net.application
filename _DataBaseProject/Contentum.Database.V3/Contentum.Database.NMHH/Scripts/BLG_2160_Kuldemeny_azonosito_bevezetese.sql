-------------------------------- BLG 2160 K�ldem�ny azonos�t� bevezet�se --------------------------------------------------------------------------------
------------------------------------- SQL postak�nyv SCRIPT ---------------------------------------------------------------------------------------------
--V�ltoz�k deklar�l�sa
DECLARE @TableID UNIQUEIDENTIFIER	-- T�blaId iter�tor.
DECLARE @Count INT					-- Kimen� k�ldem�nyek sz�m�t t�rolja.

--Iktat�k�nyvek azonos�t�inak �sszegy�jt�se a #Control_EREC_IraIktatoKonyvek temporary t�bl�ba .
SELECT Id
INTO #Control_EREC_IraIktatoKonyvek 
FROM EREC_IraIktatoKonyvek
WHERE IktatoErkezteto = 'P'

-- Ciklus, amely v�gigmegy az iktat�k�nyveken, pontosabban azok azonos�t�inak t�bl�j�n.
WHILE EXISTS (SELECT * FROM #Control_EREC_IraIktatoKonyvek)
BEGIN
	--Iter�tor be�ll�t�sa: Az els� elem a a #Control_EREC_IraIktatoKonyvek temporary t�bl�b�l.
    SELECT @TableID = (SELECT TOP 1 Id
                       FROM #Control_EREC_IraIktatoKonyvek
                       ORDER BY Id ASC)

	--�sszesz�molja, hogy az adott iktat�k�nyvh�z mennyi kimen� k�ldem�ny tartozik.
	SELECT @Count = COUNT(*)
	FROM EREC_KuldKuldemenyek
	WHERE IraIktatokonyv_Id = @TableID AND (Allapot = '06' OR Allapot = '90')  AND PostazasIranya = '2'

	--Be�rja az iktat�k�nyvbe a kimen� k�ldem�nyek sz�m�t az 'UtolsoFoszam' mez�be.
	UPDATE EREC_IraIktatoKonyvek
	SET UtolsoFoszam = @Count
	WHERE Id = @TableID;	

	--T�rli az aktu�lis iktat�k�nyv id-j�t a temporary t�bl�b�l.
    DELETE #Control_EREC_IraIktatoKonyvek
    WHERE Id = @TableID

END

--V�g�l eldobjuk a temporary t�bl�t.
DROP TABLE #Control_EREC_IraIktatoKonyvek

-------------------------------- BLG 2160 K�ldem�ny azonos�t� bevezet�se ----------------------------------------------------------------------------
------------------------------------- SQL postak�nyv SCRIPT -----------------------------------------------------------------------------------------
---------------------------------------------------- V�GE -------------------------------------------------------------------------------------------

-------------------------------- BLG 2160 K�ldem�ny azonos�t� bevezet�se --------------------------------------------------------------------------------
------------------------------------- SQL azonos�t� SCRIPT ---------------------------------------------------------------------------------------------
GO
--V�ltoz�k deklar�l�sa
DECLARE @TableID UNIQUEIDENTIFIER						-- T�blaId iter�tor az EREC_IraIktatokonyvek azonos�t�inak temporary t�bl�j�hoz
DECLARE @KuldKuldemenyek_TableID UNIQUEIDENTIFIER		-- KuldKuldemenyek_TableID iter�tor az EREC_KuldKuldemenyek azonos�t�inak temporary t�bl�j�hoz
DECLARE @Count INT										-- Kimen� k�ldem�nyek sz�m�t t�rolja
DECLARE @MegkulJelzes NVARCHAR(100)						-- Az EREC_IraIktatoKonyvek �ltal t�rolt megk�l�nb�ztet� jelz�st fogja t�rolni adott iktat�k�nyvh�z

--Iktat�k�nyvek azonos�t�inak �sszegy�jt�se a #Control_EREC_IraIktatoKonyvek temporary t�bl�ba 
SELECT Id
INTO #Control_EREC_IraIktatoKonyvek 
FROM EREC_IraIktatoKonyvek
WHERE IktatoErkezteto = 'P'

-- Ciklus, amely v�gigmegy az iktat�k�nyveken, pontosabban azok azonos�t�inak t�bl�j�n
WHILE EXISTS (SELECT * FROM #Control_EREC_IraIktatoKonyvek)
BEGIN
	--Iktat�k�nyvek iter�tor be�ll�t�sa: Az els� elem a a #Control_EREC_IraIktatoKonyvek temporary t�bl�b�l.
	SELECT @TableID = (SELECT TOP 1 Id
						FROM #Control_EREC_IraIktatoKonyvek
						ORDER BY Id ASC)

	--Kigy�jti azokat a kimen� k�ldem�nyeket, pontosabban azoknak az azonos�t�j�t, amelyek az adott (iter�tor @TableId-val megegyez�) iktat�k�nyvh�z tartoznak.
	SELECT Id, LetrehozasIdo
	INTO #Control_EREC_KuldKuldemenyek
	FROM EREC_KuldKuldemenyek
	WHERE IraIktatokonyv_Id = @TableID AND (Allapot = '06' OR Allapot = '90')  AND PostazasIranya = '2'
	ORDER BY LetrehozasIdo DESC

		-- Ciklus, amely v�gigmegy az k�ldem�nyeken, pontosabban azok azonos�t�inak t�bl�j�n
		WHILE EXISTS (SELECT * FROM #Control_EREC_KuldKuldemenyek)
		BEGIN

			--A k�ldem�nyek temporary t�bla elemeinek sz�m�t adja vissza, amit az azonos�t� k�pz�s�hez haszn�lunk fel.
			SELECT @Count = COUNT(*) FROM #Control_EREC_KuldKuldemenyek
               
			--K�ldem�nyek iter�tor be�ll�t�sa: Az els� elem a a #Control_EREC_KuldKuldemenyek temporary t�bl�b�l.     
			SELECT @KuldKuldemenyek_TableID = (SELECT TOP 1 Id
						   FROM #Control_EREC_KuldKuldemenyek
						   ORDER BY LetrehozasIdo DESC)

			--Lek�rdezz�k az EREC_IraIktatoKonyvek �ltal t�rolt megk�l�nb�ztet� jelz�st az adott iktat�k�nyvh�z.
			SELECT @MegkulJelzes = MegkulJelzes FROM EREC_IraIktatoKonyvek WHERE Id = @TableID 

			--Be�ll�tjuk az 'Azonosito' mez�t az adott (K�ldem�nyek iter�tor �ltal mutatott) kimen� k�ldem�nyhez.
			UPDATE EREC_KuldKuldemenyek
			SET Azonosito = @MegkulJelzes + '/' + CONVERT(NVARCHAR(255),  @Count)
				 -- BLG_2160_hotfix
				,Erkezteto_Szam = @Count
			WHERE Id = @KuldKuldemenyek_TableID 

			--T�rli az aktu�lis kimen� k�ldem�ny id-j�t a temporary t�bl�b�l
			DELETE #Control_EREC_KuldKuldemenyek
			WHERE Id = @KuldKuldemenyek_TableID

		END
		--V�g�l eldobjuk a temporary t�bl�t.
		DROP TABLE #Control_EREC_KuldKuldemenyek
	
	--T�rli az aktu�lis iktat�k�nyv id-j�t a temporary t�bl�b�l.
	DELETE #Control_EREC_IraIktatoKonyvek
	WHERE Id = @TableID

END

--V�g�l eldobjuk a temporary t�bl�t.
DROP TABLE #Control_EREC_IraIktatoKonyvek

-------------------------------- BLG 2160 K�ldem�ny azonos�t� bevezet�se ----------------------------------------------------------------------------
------------------------------------- SQL azonos�t� SCRIPT -----------------------------------------------------------------------------------------
---------------------------------------------------- V�GE -------------------------------------------------------------------------------------------