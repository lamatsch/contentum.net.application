UPDATE I
SET I.Ugy_Fajtaja = O.UgyFajtaja
FROM EREC_IraIratok AS I
INNER JOIN EREC_IraOnkormAdatok AS O
ON I.Id = O.IraIratok_Id
WHERE I.Ugy_Fajtaja IS NULL
	AND 
	  O.UgyFajtaja IS NOT NULL


-- UPDATE U
-- SET U.Ugy_Fajtaja = I.Ugy_Fajtaja
-- FROM EREC_UgyUgyiratok AS U
-- INNER JOIN EREC_IraIratok I ON U.Id = I.Ugyirat_Id
-- WHERE U.Ugy_Fajtaja IS NULL
	-- AND 
	  -- I.Ugy_Fajtaja IS NOT NULL

