﻿---------------------- BLG 7883

-- drop #Insert_Forditas
IF OBJECT_ID('tempdb..#Insert_Forditas') IS NOT NULL
BEGIN
    DROP PROC #Insert_Forditas
END
GO

-- create #Insert_Forditas

DECLARE @sql NVARCHAR(2000)
SET @sql = N'
CREATE PROCEDURE #Insert_Forditas
(  
   @objazonosito NVARCHAR(400),
   @forditas NVARCHAR(4000)  
)  
AS  
BEGIN  
	IF not EXISTS (SELECT 1 FROM [KRT_Forditasok] WHERE 
	[Modul]=@modul AND [Komponens]=@komponens AND [NyelvKod]=@nyelvKod AND [OrgKod]=@orgKod AND [ObjAzonosito]=@objazonosito)
	BEGIN				
		INSERT INTO [dbo].[KRT_Forditasok]
		   ([NyelvKod]
			,[OrgKod]
			,[Modul]
			,[Komponens]
			,[ObjAzonosito]
			,[Forditas])
		VALUES
		   (@nyelvKod, -- NyelvKod
			@orgKod, -- OrgKod
			@modul, -- Modul
			@komponens, -- Komponens
			@objazonosito, -- ObjAzonosito
			@forditas -- Forditas
		);
	END
END
'

SET @sql = REPLACE(@sql, '@nyelvKod', '''hu-HU''')
SET @sql = REPLACE(@sql, '@orgKod', '''NMHH''')
SET @sql = REPLACE(@sql, '@modul', '''eMigration''')
SET @sql = REPLACE(@sql, '@komponens', '''FoszamList.aspx''')

--PRINT @sql
EXEC sp_executesql @sql
	
PRINT 'Fordítások felvétele: '
EXEC #Insert_Forditas 'BoundField_Csatolmany', 'Csny.';
EXEC #Insert_Forditas 'BoundField_EdokSav', 'Iktatókönyv';
EXEC #Insert_Forditas 'BoundField_UI_YEAR', 'Év';
EXEC #Insert_Forditas 'BoundField_UI_NUM', 'Főszám';
EXEC #Insert_Forditas 'BoundField_UI_IRJ', 'Irattári jel';
EXEC #Insert_Forditas 'BoundField_UI_NAME', 'Ügyfél';
EXEC #Insert_Forditas 'BoundField_MEMO', 'Tárgy';
EXEC #Insert_Forditas 'BoundField_UI_OT_ID', 'Azonosító';
EXEC #Insert_Forditas 'BoundField_Eloado_Nev', 'Ügyintéző';
EXEC #Insert_Forditas 'BoundField_Eloirat', 'Előirat';
EXEC #Insert_Forditas 'BoundField_Utoirat', 'Utóirat';
EXEC #Insert_Forditas 'BoundField_UGYHOL', 'Irat helye';
EXEC #Insert_Forditas 'BoundField_Ugyirat_tipus', 'Irat típusa';
EXEC #Insert_Forditas 'BoundField_Hatarido', 'Határidő';
EXEC #Insert_Forditas 'BoundField_IRATTARBA', 'Irattárba';
EXEC #Insert_Forditas 'BoundField_SCONTRO', 'Skontró vége';
EXEC #Insert_Forditas 'BoundField_Feladat', 'Feladat';
EXEC #Insert_Forditas 'BoundField_Szervezet', 'Szervezet';
EXEC #Insert_Forditas 'BoundField_Kikero', 'Kikérő';
EXEC #Insert_Forditas 'BoundField_IrattarbolKikeres_Datuma', 'Kikérés dátuma';
EXEC #Insert_Forditas 'BoundField_MegorzesiIdo', 'Megőrzési idő';
EXEC #Insert_Forditas 'BoundField_ALNO', 'Alszám';
EXEC #Insert_Forditas 'BoundField_IrattariHely', 'Irattári hely';
EXEC #Insert_Forditas 'BoundField_IDNUM', 'Hiv.szám';
EXEC #Insert_Forditas 'BoundField_BKNEV', 'Beküldő';
EXEC #Insert_Forditas 'BoundField_EloadoNev', 'Előadó';
EXEC #Insert_Forditas 'BoundField_MJ', 'Megjegyzés';
EXEC #Insert_Forditas 'BoundField_IRATTARBA', 'Irattárba adás';
EXEC #Insert_Forditas 'BoundField_Adathordozo_tipusa', 'Adath. típus';
EXEC #Insert_Forditas 'BoundField_Irattipus', 'Irattípus';
EXEC #Insert_Forditas 'BoundField_Feladat', 'Csatolt irat';

GO
---------------------- End of BLG 7883