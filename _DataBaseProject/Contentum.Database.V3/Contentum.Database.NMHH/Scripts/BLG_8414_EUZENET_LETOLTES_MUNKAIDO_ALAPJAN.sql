DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = 'B3DC1FA5-82BB-455F-B899-0E553DB93F0C'

IF @org_kod = 'FPH' or @org_kod = 'CSPH' 
	set @ertek = '1'
else
	set @ertek = '0'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - EUZENET_LETOLTES_MUNKAIDO_ALAPJAN  - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'EUZENET_LETOLTES_MUNKAIDO_ALAPJAN'
		,@ertek
		,'1'
		,'0 - a be�ll�tott gyakoris�ggal minden �zenet let�lt�sre ker�l, 1 - a h�t utols� munkanapj�n d�li 12:00 ut�nt�l, a k�vetkez� h�t els� munkanapj�n 00:00-ig csak a rendszer �zeneteket t�ltse le a rendszer a Hivatali Kapur�l.'
		); 
 END
 GO