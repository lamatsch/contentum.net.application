--BEGIN TRANSACTION
DECLARE 
    @id uniqueidentifier, 
    @anyjaneveFetched   nvarchar(100),
	@szuletesinevFetched   nvarchar(100),
	@ujutonevFetched   nvarchar(100),
	@ujcsaladinevFetched   nvarchar(100),

	@AnyjaNeveCsaladiNev nvarchar(100),
	@AnyjaNeveElsoUtonev nvarchar(100),
	@AnyjaNeveTovabbiUtonev nvarchar(100),

	@SzuletesiCsaladiNev nvarchar(100),
	@SzuletesiElsoUtonev nvarchar(100),
	@SzuletesiTovabbiUtonev nvarchar(100),

	@UjUtonev nvarchar(100),
	@UjTovabbiUtonev nvarchar(100);

DECLARE @Nevek TABLE
(
  Id uniqueidentifier,
  AnyjaNeve nvarchar(100),
  AnyjaNeveCsaladiNev nvarchar(100), 
  AnyjaNeveElsoUtonev  nvarchar(100), 
  AnyjaNeveTovabbiUtonev  nvarchar(100),

  SzuletesiNev nvarchar(100),
  SzuletesiCsaladiNev nvarchar(100),
  SzuletesiElsoUtonev nvarchar(100),
  SzuletesiTovabbiUtonev nvarchar(100),

  UjCsaladiNev nvarchar(100),
  UjUtonev nvarchar(100),
  UjTovabbiUtonev nvarchar(100)
)
 
DECLARE nevekcursor CURSOR
FOR SELECT 
        Id,AnyjaNeve,SzuletesiNev,UjUtonev, UjCsaladiNev         
    FROM 
        KRT_Szemelyek;

	
OPEN nevekcursor;

FETCH NEXT FROM nevekcursor INTO 
    @id, 
    @anyjaneveFetched,
	@szuletesinevFetched,
	@ujutonevFetched,
	@ujcsaladinevFetched
 
WHILE @@FETCH_STATUS = 0
    BEGIN
        
		select 
		@AnyjaNeveCsaladiNev=(select t.Value from fn_SplitWithPos(@anyjaneveFetched,' ') as t where t.Pos=1 ),
		@AnyjaNeveElsoUtonev = (select t.Value from fn_SplitWithPos(@anyjaneveFetched,' ') as t where t.Pos=2 ),
		@SzuletesiCsaladiNev=(select t.Value from fn_SplitWithPos(@szuletesinevFetched,' ') as t where t.Pos=1 ),
		@SzuletesiElsoUtonev = (select t.Value from fn_SplitWithPos(@szuletesinevFetched,' ') as t where t.Pos=2 ),
		@UjUtonev = (select t.Value from fn_SplitWithPos(@ujutonevFetched,' ') as t where t.Pos=1 )

;WITH Splitted (Pos, Value)		
		AS
		(
		  select 1 as Pos, t.Value from fn_SplitWithPos(@anyjaneveFetched,' ') as t where t.Pos > 2 
		)
		select @AnyjaNeveTovabbiUtonev =( SELECT DISTINCT
    SUBSTRING(
        (
            SELECT ' '+ST1.Value  AS [text()]
            FROM Splitted ST1
            WHERE ST1.Pos = Splitted.Pos
            ORDER BY ST1.Pos
            FOR XML PATH ('')
        ), 2, 1000) [Students]
FROM Splitted  );

;WITH Splitted (Pos, Value)		
		AS
		(
		  select 1 as Pos, t.Value from fn_SplitWithPos(@szuletesinevFetched,' ') as t where t.Pos > 2 
		)
		select @SzuletesiTovabbiUtonev =( SELECT DISTINCT
    SUBSTRING(
        (
            SELECT ' '+ST1.Value  AS [text()]
            FROM Splitted ST1
            WHERE ST1.Pos = Splitted.Pos
            ORDER BY ST1.Pos
            FOR XML PATH ('')
        ), 2, 1000) [Students]
FROM Splitted  );

;WITH Splitted (Pos, Value)		
		AS
		(
		  select 1 as Pos, t.Value from fn_SplitWithPos(@ujutonevFetched,' ') as t where t.Pos > 1 
		)
		select @UjTovabbiUtonev =( SELECT DISTINCT
    SUBSTRING(
        (
            SELECT ' '+ST1.Value  AS [text()]
            FROM Splitted ST1
            WHERE ST1.Pos = Splitted.Pos
            ORDER BY ST1.Pos
            FOR XML PATH ('')
        ), 2, 1000) [Students]
FROM Splitted  );


insert into @Nevek values(@id,@anyjaneveFetched, @AnyjaNeveCsaladiNev,@AnyjaNeveElsoUtonev,@AnyjaNeveTovabbiUtonev,@szuletesinevFetched,@SzuletesiCsaladiNev,@SzuletesiElsoUtonev,@SzuletesiTovabbiUtonev,@ujcsaladinevFetched, @UjUtonev,@UjTovabbiUtonev) 

        FETCH NEXT FROM nevekcursor INTO 
           @id, 
    @anyjaneveFetched,
	@szuletesinevFetched,
	@ujutonevFetched,
	@ujcsaladinevFetched;

 END;

CLOSE nevekcursor;

DEALLOCATE nevekcursor;

UPDATE
    KRT_Szemelyek
SET
    KRT_Szemelyek.AnyjaNeveCsaladiNev = RAN.AnyjaNeveCsaladiNev,
	KRT_Szemelyek.AnyjaNeveElsoUtonev = RAN.AnyjaNeveElsoUtonev,
	KRT_Szemelyek.AnyjaNeveTovabbiUtonev = RAN.AnyjaNeveTovabbiUtonev,	
	
	KRT_Szemelyek.SzuletesiCsaladiNev = RAN.SzuletesiCsaladiNev,
	KRT_Szemelyek.SzuletesiElsoUtonev = RAN.SzuletesiElsoUtonev,
	KRT_Szemelyek.SzuletesiTovabbiUtonev = RAN.SzuletesiTovabbiUtonev,

	KRT_Szemelyek.UjUtonev = RAN.UjUtonev,
	KRT_Szemelyek.UjTovabbiUtonev = ISNULL(RAN.UjTovabbiUtonev, SI.UjTovabbiUtonev)
	
FROM
    KRT_Szemelyek SI
INNER JOIN
    @Nevek RAN
ON 
    SI.Id = RAN.Id;
	
--ROLLBACK

        