IF NOT EXISTS(SELECT 1 FROM KRT_KodtarFuggoseg WHERE Vezerlo_KodCsoport_Id='0C4AC87C-84FC-4D20-A9F0-3FC519F4D9A1' AND Fuggo_KodCsoport_Id='664B8A53-9B2C-4B1B-B279-9765DE1C31A9')
BEGIN
	PRINT 'INSERT KRT_KODTARFUGGOSEG: TERTIVEVENY_VISSZA_KOD-IRATPELDANY_ALLAPOT'
	INSERT INTO KRT_KodtarFuggoseg
	(
		Id,
		Org,
		Vezerlo_KodCsoport_Id,
		Fuggo_KodCsoport_Id,
		Adat,
		Aktiv
	)
	VALUES
	(
		'A9D9C317-6233-407C-BCC8-86F6F069CAC7',
		'450B510A-7CAA-46B0-83E3-18445C0C53A9',
		'0C4AC87C-84FC-4D20-A9F0-3FC519F4D9A1',
		'664B8A53-9B2C-4B1B-B279-9765DE1C31A9',
		'{"VezerloKodCsoportId":"0c4ac87c-84fc-4d20-a9f0-3fc519f4d9a1","FuggoKodCsoportId":"664b8a53-9b2c-4b1b-b279-9765de1c31a9","Items":[{"VezerloKodTarId":"6f2f4e4e-d307-4ee4-9ca0-227c8b51b608","FuggoKodtarId":"08fe89a6-1d24-4ac8-aa07-083950ff4245","Aktiv":true},{"VezerloKodTarId":"be44998f-83ec-45bf-a525-eca0d025db72","FuggoKodtarId":"7bab33e1-5635-4441-bc66-878a13be1749","Aktiv":true},{"VezerloKodTarId":"70e0bd54-e891-49ec-a56e-cbb04438c7ef","FuggoKodtarId":"08fe89a6-1d24-4ac8-aa07-083950ff4245","Aktiv":true},{"VezerloKodTarId":"2e9cbafd-805c-4519-927c-5c937f222667","FuggoKodtarId":"7bab33e1-5635-4441-bc66-878a13be1749","Aktiv":true}],"ItemsNincs":[]}',
		'1'
	)
END