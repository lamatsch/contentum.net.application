USE $(DataBaseName)

DECLARE @csoportok_id VARCHAR(100)
DECLARE @elektronikus_id VARCHAR(100)
DECLARE @odap_id VARCHAR(100)
DECLARE @szervezetek_cursor CURSOR
DECLARE @REQUESTTEXT VARCHAR(8000)
DECLARE @REQUESTTEXT1 VARCHAR(8000)
DECLARE @REQUESTTEXT2 VARCHAR(8000)
DECLARE @RESPONSETEXT VARCHAR(8000)

SELECT @elektronikus_id = Id FROM [dbo].[EREC_IraIktatoKonyvek]
WHERE Nev = 'Elektronikus Postak�nyv'

SELECT @odap_id = Id FROM [dbo].[EREC_IraIktatoKonyvek]
WHERE Nev = 'NMHH postak�nyv � ODAP'

SET @szervezetek_cursor = CURSOR FOR
    SELECT Id
          FROM [dbo].[KRT_Csoportok]
          WHERE ErvVege > getDate()
          AND Tipus = '0' AND Nev != 'Technikai Post�z�k'

OPEN @szervezetek_cursor
FETCH NEXT FROM @szervezetek_cursor INTO @csoportok_id
WHILE @@FETCH_STATUS = 0
BEGIN

    SET @REQUESTTEXT1 = '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Body>
                <AddCsoportToJogtargy xmlns="Contentum.eAdmin.WebService">
                    <execParam>
                    <Typed>
                        <Felhasznalo_Id>54e861a5-36ed-44ca-baa7-c287d125b309</Felhasznalo_Id>
                        <FelhasznaloSzervezet_Id>bd00c8d0-cf99-4dfc-8792-33220b7bfcc6</FelhasznaloSzervezet_Id>
                        <Org_Id>450b510a-7caa-46b0-83e3-18445c0c53a9</Org_Id>
                    </Typed>
                    </execParam>
                    <JogtargyId>'

    SET @REQUESTTEXT2 = CONCAT('</JogtargyId>
                    <CsoportId>', @csoportok_id, '</CsoportId>
                    <Kezi>', ascii('I'), '</Kezi>
                </AddCsoportToJogtargy>
              </soap:Body>
            </soap:Envelope>')

    SET @REQUESTTEXT = CONCAT(@REQUESTTEXT1, @elektronikus_id, @REQUESTTEXT2)
    PRINT @csoportok_id + ' -> ' + @elektronikus_id
    IF NOT EXISTS (SELECT * FROM [dbo].[KRT_Jogosultak] WHERE Obj_Id = @elektronikus_id AND Csoport_Id_Jogalany = @csoportok_id)
    BEGIN
		PRINT @csoportok_id + ' -> ' + @elektronikus_id
        EXEC [sp_HTTPRequest]
            '$(RightsService)',
            'POST',
            @REQUESTTEXT,
            'Contentum.eAdmin.WebService/AddCsoportToJogtargy',
            '',
            '',
            @RESPONSETEXT OUT
        PRINT @RESPONSETEXT
    END
    ELSE PRINT 'Kor�bban hozz�adva'

    SET @REQUESTTEXT = CONCAT(@REQUESTTEXT1, @odap_id, @REQUESTTEXT2)
    PRINT @csoportok_id + ' -> ' + @odap_id
    IF NOT EXISTS (SELECT * FROM [dbo].[KRT_Jogosultak] WHERE Obj_Id = @odap_id AND Csoport_Id_Jogalany = @csoportok_id)
    BEGIN
		PRINT @csoportok_id + ' -> ' + @odap_id
        EXEC [sp_HTTPRequest]
            '$(RightsService)',
            'POST',
            @REQUESTTEXT,
            'Contentum.eAdmin.WebService/AddCsoportToJogtargy',
            '',
            '',
            @RESPONSETEXT OUT
        PRINT @RESPONSETEXT
    END
    ELSE PRINT 'Kor�bban hozz�adva'

    FETCH NEXT FROM @szervezetek_cursor INTO @csoportok_id
END

CLOSE @szervezetek_cursor
DEALLOCATE @szervezetek_cursor

GO
