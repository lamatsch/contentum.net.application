USE $(DataBaseName)

DECLARE @REQUESTTEXT VARCHAR(8000)
DECLARE @RESPONSETEXT VARCHAR(8000)

DECLARE @Tech_Postazok_Users TABLE (name VARCHAR(100))
INSERT @Tech_Postazok_Users(name) VALUES
            ('Debrecen'),
            ('Miskolc'),
            ('Ostrom'),
            ('P�cs'),
            ('Reviczky'),
            ('Sopron'),
            ('Szeged'),
            ('Visegr�di')

DECLARE @csoportok_id VARCHAR(100)
DECLARE @iktatokonyv_id VARCHAR(100)
DECLARE @csop_ikt_cursor CURSOR

SET @csop_ikt_cursor = CURSOR FOR
    SELECT cs.Id, ik.id FROM @Tech_Postazok_Users tpu
    INNER JOIN [dbo].[KRT_Csoportok] cs ON cs.Nev = tpu.name + ' Technikai Post�z�'
    INNER JOIN [dbo].[EREC_IraIktatoKonyvek] ik ON ik.Nev like 'NMHH postak�nyv _ ' + tpu.name

OPEN @csop_ikt_cursor
FETCH NEXT FROM @csop_ikt_cursor INTO @csoportok_id, @iktatokonyv_id
WHILE @@FETCH_STATUS = 0
BEGIN
    PRINT @csoportok_id + ' -> ' + @iktatokonyv_id

    IF NOT EXISTS (SELECT * FROM [dbo].[KRT_Jogosultak] WHERE Obj_Id = @iktatokonyv_id AND Csoport_Id_Jogalany = @csoportok_id)
    BEGIN
        SET @REQUESTTEXT = CONCAT ( '<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                    <soap:Body>
                    <AddCsoportToJogtargy xmlns="Contentum.eAdmin.WebService">
                        <execParam>
                        <Typed>
                            <Felhasznalo_Id>54e861a5-36ed-44ca-baa7-c287d125b309</Felhasznalo_Id>
                            <FelhasznaloSzervezet_Id>bd00c8d0-cf99-4dfc-8792-33220b7bfcc6</FelhasznaloSzervezet_Id>
                            <Org_Id>450b510a-7caa-46b0-83e3-18445c0c53a9</Org_Id>
                        </Typed>
                        </execParam>
                        <JogtargyId>', @iktatokonyv_id, '</JogtargyId>
                        <CsoportId>', @csoportok_id, '</CsoportId>
                        <Kezi>', ascii('I'), '</Kezi>
                    </AddCsoportToJogtargy>
                    </soap:Body>
                </soap:Envelope>')
        PRINT @REQUESTTEXT

        EXEC [sp_HTTPRequest]
            '$(RightsService)',
            'POST',
            @REQUESTTEXT,
            'Contentum.eAdmin.WebService/AddCsoportToJogtargy',
            '',
            '',
            @RESPONSETEXT OUT
        PRINT @RESPONSETEXT
    END
    ELSE PRINT 'Kor�bban hozz�adva'
    FETCH NEXT FROM @csop_ikt_cursor INTO @csoportok_id, @iktatokonyv_id
END

CLOSE @csop_ikt_cursor
DEALLOCATE @csop_ikt_cursor

GO