﻿-- BUG_10140: Adatkapu szinkro rendbetétele

DECLARE @Org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @OrgKod nvarchar(100) = (select Kod from KRT_Orgok where [Id] = @Org)

IF @OrgKod = 'NMHH'
BEGIN
	UPDATE [dbo].[KRT_Partnerek] SET [Belso]='0' WHERE [Forras] IN ('E', 'U') AND [Belso] IS NULL
END

GO