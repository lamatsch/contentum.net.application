
USE $(DataBaseName)

IF NOT EXISTS (select * from [dbo].[krt_partnerek] where [Nev] = 'Technikai Post�z�k' )
BEGIN
    
	-- Technikai Post�z�k szervezet l�trehoz�sa
    DECLARE @REQUESTTEXT VARCHAR(8000)
    DECLARE @RESPONSETEXT VARCHAR(8000)
    SET @REQUESTTEXT = '<?xml version="1.0" encoding="UTF-8"?>
    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
               xmlns:xsd="http://www.w3.org/2001/XMLSchema"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <soap:Header/>
   <soap:Body>
      <Insert xmlns="Contentum.eAdmin.WebService">
         <ExecParam>
            <Typed>
               <Felhasznalo_Id>54e861a5-36ed-44ca-baa7-c287d125b309</Felhasznalo_Id>
               <FelhasznaloSzervezet_Id>bd00c8d0-cf99-4dfc-8792-33220b7bfcc6</FelhasznaloSzervezet_Id>
            </Typed>
         </ExecParam>
         <Record xsi:type="KRT_Partnerek">
            <Typed>
               <Tipus>10</Tipus>
               <Nev>Technikai Post�z�k</Nev>
               <Belso>1</Belso>
               <ErvKezd xsi:nil="true" />
               <ErvVege>4700-12-31T00:00:00.000</ErvVege>
            </Typed>
            <Vallalkozasok>
               <Typed>
                  <KulfoldiAdoszamJelolo>0</KulfoldiAdoszamJelolo>
                  <ErvVege>4700-12-31T00:00:00.000</ErvVege>
               </Typed>
            </Vallalkozasok>
         </Record>
      </Insert>
   </soap:Body>
</soap:Envelope>'

exec [dbo].[sp_HTTPRequest] 
        '$(PartnerekServiceUrl)',
        'POST', 
        @REQUESTTEXT,
        'Contentum.eAdmin.WebService/Insert',
        '', 
        '',
        @RESPONSETEXT
END
IF EXISTS  (select * from [dbo].[krt_csoportok] where [Nev] = 'Technikai Post�z�k')
BEGIN 

	-- Csoport dolgoz�i l�thatj�k egym�s t�teleit -> false
    UPDATE  [dbo].[krt_csoportok]
    SET [JogosultsagOroklesMod] = 0
    WHERE [Nev] = 'Technikai Post�z�k'


    DECLARE @TECH_POST_ID VARCHAR(8000) = (select [Id] from [dbo].[krt_csoportok] where [Nev] = 'Technikai Post�z�k')
    DECLARE @Tech_Postazok_Users TABLE (name VARCHAR(100))

    INSERT @Tech_Postazok_Users(name) VALUES
                ('Debrecen Technikai Post�z�'),
                ('Miskolc Technikai Post�z�'),
                ('Ostrom Technikai Post�z�'),
                ('P�cs Technikai Post�z�'),
                ('Reviczky Technikai Post�z�'),
                ('Sopron Technikai Post�z�'), 
                ('Szeged Technikai Post�z�'),
                ('Visegr�di Technikai Post�z�')

    DECLARE Users_cursor CURSOR FOR (SELECT name FROM @Tech_Postazok_Users)
    DECLARE @user VARCHAR(60)

    OPEN Users_cursor
    FETCH NEXT FROM Users_cursor INTO @user
    WHILE @@FETCH_STATUS = 0

    BEGIN
		
		--Felhaszn�l� l�trehoz�sa , ha nem l�tezik
        IF NOT EXISTS (SELECT * FROM [dbo].[KRT_Felhasznalok] WHERE Nev = @user)
			BEGIN

			DECLARE @USER_REQUEST VARCHAR(8000)
			DECLARE @USER_RESPONSE VARCHAR(8000)
			SET @USER_REQUEST = CONCAT ( '<?xml version="1.0" encoding="UTF-8"?>
            <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
               <soap:Body>
                  <InsertAndCreateNewPartner xmlns="Contentum.eAdmin.WebService">
                     <execParam>
                        <Typed>
                           <Felhasznalo_Id>54e861a5-36ed-44ca-baa7-c287d125b309</Felhasznalo_Id>
                           <FelhasznaloSzervezet_Id>bd00c8d0-cf99-4dfc-8792-33220b7bfcc6</FelhasznaloSzervezet_Id>
                           <Org_Id>450b510a-7caa-46b0-83e3-18445c0c53a9</Org_Id>
                        </Typed>
                     </execParam>
                     <krt_felhasznalok>
                        <Typed>
                           <Tipus>Windows</Tipus>
                           <UserNev>', @user , '</UserNev>
                           <Nev>', @user , '</Nev>
                           <Jelszo xsi:nil="true" />
                           <JelszoLejaratIdo>2099-12-31T00:00:00.000</JelszoLejaratIdo>
                           <System xsi:nil="true" />
                           <EMail>teszt2@budapest.hu</EMail>
                           <Engedelyezett xsi:nil="true" />
                           <Telefonszam xsi:nil="true" />
                           <Beosztas xsi:nil="true" />
                           <ErvKezd xsi:nil="true" />
                           <ErvVege>4700-12-31T00:00:00.000</ErvVege>
                        </Typed>
                     </krt_felhasznalok>
                  </InsertAndCreateNewPartner>
               </soap:Body>
            </soap:Envelope>');

            exec [dbo].[sp_HTTPRequest] 
                '$(FelhasznalokServiceUrl)',
                'POST', 
                @USER_REQUEST,
                'Contentum.eAdmin.WebService/InsertAndCreateNewPartner',
                '', 
                '',
                @USER_RESPONSE
            
			END

			--Felhaszn�l� - Partner kapcsolat be�ll�t�sa

			DECLARE @PARTNER_KAPCS_REQUEST VARCHAR(8000)
			DECLARE @PARTNER_KAPCS_RESPONSE VARCHAR(8000)
			
			DECLARE @PARTNER_ID_KAPCSOLT VARCHAR(8000) = (SELECT Partner_Id FROM [dbo].[KRT_Felhasznalok] Where Nev = @user );
	
			--Felhaszn�l�hoz kapcsolt partner lek�r�se
			DECLARE @UserKapcsoltPartnerNev VARCHAR(8000) =
						(SELECT Nev FROM  [dbo].[KRT_Partnerek] WHERE Id = 
						
							(SELECT Partner_id_kapcsolt FROM [dbo].[KRT_PartnerKapcsolatok] WHERE Partner_id = @PARTNER_ID_KAPCSOLT))
	
	
			IF (  @UserKapcsoltPartnerNev != 'Technikai Post�z�k' OR @UserKapcsoltPartnerNev is null )
			
			BEGIN
				--Technikai Post�z�khoz rendel�s
				SET @PARTNER_KAPCS_REQUEST = CONCAT ( '<?xml version="1.0" encoding="utf-8"?>
							<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
							<soap:Body>
								<Insert xmlns="Contentum.eAdmin.WebService">
									<ExecParam>
										<Typed>				
											<Felhasznalo_Id>54e861a5-36ed-44ca-baa7-c287d125b309</Felhasznalo_Id>
											<FelhasznaloSzervezet_Id>bd00c8d0-cf99-4dfc-8792-33220b7bfcc6</FelhasznaloSzervezet_Id>					
										</Typed>				
									</ExecParam>
										<Record>
											<Typed>
												<Id xsi:nil="true" />
												<Tipus>', '2' ,'</Tipus>
												<Partner_id>' , @TECH_POST_ID , ' </Partner_id> 
												<Partner_id_kapcsolt>' , @PARTNER_ID_KAPCSOLT ,' </Partner_id_kapcsolt>	
												<ErvVege>4700-12-31T00:00:00.000</ErvVege>
											</Typed>				
										</Record>
										</Insert>
										</soap:Body>
									</soap:Envelope>' );

					exec [dbo].[sp_HTTPRequest] 
							'$(PartnerKapcsolatokServiceUrl)',
							'POST', 
							@PARTNER_KAPCS_REQUEST,
							'Contentum.eAdmin.WebService/Insert',
							'',
							'',
							@PARTNER_KAPCS_RESPONSE

					END
				ELSE PRINT @user+' m�r a Technikai Post�z�khoz van rendelve.'

        FETCH NEXT FROM Users_cursor INTO @user
    END
END 
ELSE PRINT 'Hiba - Technikai Post�z�k csopot nem j�tt l�tre'
