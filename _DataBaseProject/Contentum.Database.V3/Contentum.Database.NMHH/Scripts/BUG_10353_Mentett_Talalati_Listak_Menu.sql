declare @record_id uniqueidentifier
set @record_id = '338950F8-9206-4691-ABCC-9013E5DF0227'

IF NOT EXISTS(SELECT 1 FROM KRT_Modulok WHERE Id = @record_id)
BEGIN
	PRINT 'INSERT - KRT_Modulok -  Mentett tal�lati list�k'

	INSERT INTO KRT_Modulok
	(
		Id,
		Tipus,
		Kod,
		Nev
	)
	VALUES
	(
		@record_id,
		'F',
		'MentettTalalatokList.aspx',
		'Mentett tal�lati list�k'
	)
END

GO

declare @record_id uniqueidentifier
set @record_id = '932751CA-9BE0-4F3F-9129-B75E78790FC3'

IF NOT EXISTS(SELECT 1 FROM KRT_Menuk WHERE Id = @record_id)
BEGIN
	PRINT 'INSERT - KRT_Menuk -  Mentett tal�lati list�k'

	INSERT INTO KRT_Menuk
	(
		Id,
		Menu_Id_Szulo,
		Kod,
		Nev,
		Funkcio_Id,
		Modul_Id,
		Sorrend
	)
	VALUES
	(
		@record_id,
		'EE9EA307-ED15-4FE6-B323-A12D2E06AC6D',
		'eRecord',
		'Mentett tal�lati list�k',
		'D83535E3-6E94-E511-A275-00155D011E21',
		'338950F8-9206-4691-ABCC-9013E5DF0227',
		'37'
	)
END