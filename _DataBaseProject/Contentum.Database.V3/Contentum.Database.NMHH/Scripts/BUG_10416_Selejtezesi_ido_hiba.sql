BEGIN TRAN

declare @tranz_id uniqueidentifier
set @tranz_id = NEWID()

declare @now datetime
set @now = getdate()

declare @admin uniqueidentifier
set @admin = '54E861A5-36ED-44CA-BAA7-C287D125B309'

;with UgyiratMegorzesiIdo as
(
select u.Id,
MegorzesiIdoVege = 
(
	case it.Idoegyseg
	when '365' THEN DATEADD(YEAR, CAST(KRT_KodTarak_MegorzesiIdo.Nev as int), DATEFROMPARTS(YEAR(u.LezarasDat),12,31)) 
	when '1440' THEN DATEADD(DAY, CAST(KRT_KodTarak_MegorzesiIdo.Nev as int), DATEFROMPARTS(YEAR(u.LezarasDat),12,31)) 
	when '10080' THEN DATEADD(WEEK, CAST(KRT_KodTarak_MegorzesiIdo.Nev as int), DATEFROMPARTS(YEAR(u.LezarasDat),12,31)) 
	when '28' THEN DATEADD(MONTH, CAST(KRT_KodTarak_MegorzesiIdo.Nev as int), DATEFROMPARTS(YEAR(u.LezarasDat),12,31)) 
	ELSE DATEADD(YEAR, CAST(KRT_KodTarak_MegorzesiIdo.Nev as int), DATEFROMPARTS(YEAR(u.LezarasDat),12,31)) 
END
)
from EREC_UgyUgyiratok u
join EREC_IraIrattariTetelek it on u.IraIrattariTetel_Id = it.Id
left join KRT_KodCsoportok as KRT_KodCsoportok_MegorzesiIdo on KRT_KodCsoportok_MegorzesiIdo.Kod='SELEJTEZESI_IDO'
left join KRT_KodTarak as KRT_KodTarak_MegorzesiIdo 
ON KRT_KodCsoportok_MegorzesiIdo.Id = KRT_KodTarak_MegorzesiIdo.KodCsoport_Id and it.MegorzesiIdo = KRT_KodTarak_MegorzesiIdo.Kod
and it.ErvKezd between KRT_KodTarak_MegorzesiIdo.ErvKezd and KRT_KodTarak_MegorzesiIdo.ErvVege
WHERE u.LezarasDat is not null
and u.IrattarbaKuldDatuma is not null
and it.IrattariJel in ('S','L')
and it.MegorzesiIdo != 'HN'
and it.MegorzesiIdo != '0'
and TRY_CAST(KRT_KodTarak_MegorzesiIdo.Nev as int) is not null
)

update EREC_UgyUgyiratok
set MegorzesiIdoVege = u2.MegorzesiIdoVege,
	Ver = Ver + 1,
	ModositasIdo = @now,
	Modosito_id = @admin,
	Tranz_id = @tranz_id
from EREC_UgyUgyiratok u1 join UgyiratMegorzesiIdo u2 on u1.Id = u2.Id
where cast(u1.MegorzesiIdoVege as date) != cast(u2.MegorzesiIdoVege as date)


INSERT INTO [EREC_UgyUgyiratokHistory]
      ([HistoryMuvelet_Id]
      ,[HistoryVegrehajto_Id]
      ,[HistoryVegrehajtasIdo]
      ,[Id]
      ,[Foszam]
      ,[Sorszam]
      ,[Ugyazonosito]
      ,[Alkalmazas_Id]
      ,[UgyintezesModja]
      ,[Hatarido]
      ,[SkontrobaDat]
      ,[LezarasDat]
      ,[IrattarbaKuldDatuma]
      ,[IrattarbaVetelDat]
      ,[FelhCsoport_Id_IrattariAtvevo]
      ,[SelejtezesDat]
      ,[FelhCsoport_Id_Selejtezo]
      ,[LeveltariAtvevoNeve]
      ,[FelhCsoport_Id_Felulvizsgalo]
      ,[FelulvizsgalatDat]
      ,[IktatoszamKieg]
      ,[Targy]
      ,[UgyUgyirat_Id_Szulo]
      ,[UgyUgyirat_Id_Kulso]
      ,[UgyTipus]
      ,[IrattariHely]
      ,[SztornirozasDat]
      ,[Csoport_Id_Felelos]
      ,[FelhasznaloCsoport_Id_Orzo]
      ,[Csoport_Id_Cimzett]
      ,[Jelleg]
      ,[IraIrattariTetel_Id]
      ,[IraIktatokonyv_Id]
      ,[SkontroOka]
      ,[SkontroVege]
      ,[Surgosseg]
      ,[SkontrobanOsszesen]
      ,[MegorzesiIdoVege]
      ,[Partner_Id_Ugyindito]
      ,[NevSTR_Ugyindito]
      ,[ElintezesDat]
      ,[FelhasznaloCsoport_Id_Ugyintez]
      ,[Csoport_Id_Felelos_Elozo]
      ,[KolcsonKikerDat]
      ,[KolcsonKiadDat]
      ,[Kolcsonhatarido]
      ,[BARCODE]
      ,[IratMetadefinicio_Id]
      ,[Allapot]
      ,[TovabbitasAlattAllapot]
      ,[Megjegyzes]
      ,[Azonosito]
      ,[Fizikai_Kezbesitesi_Allapot]
      ,[Kovetkezo_Orzo_Id]
      ,[Csoport_Id_Ugyfelelos]
      ,[Elektronikus_Kezbesitesi_Allap]
      ,[Kovetkezo_Felelos_Id]
      ,[UtolsoAlszam]
      ,[UtolsoSorszam]
      ,[IratSzam]
      ,[ElintezesMod]
      ,[RegirendszerIktatoszam]
      ,[GeneraltTargy]
      ,[Cim_Id_Ugyindito]
      ,[CimSTR_Ugyindito]
      ,[Ver]
      ,[Note]
      ,[Stat_id]
      ,[ErvKezd]
      ,[ErvVege]
      ,[Letrehozo_id]
      ,[LetrehozasIdo]
      ,[Modosito_id]
      ,[ModositasIdo]
      ,[Zarolo_id]
      ,[ZarolasIdo]
      ,[Tranz_id]
      ,[UIAccessLog_id]
      ,[UjOrzesiIdo]
      ,[IrattarId]
      ,[LezarasOka]
      ,[SkontroOka_Kod]
      ,[AKTIV]
      ,[ElozoAllapot]
      ,[UjOrzesiIdoIdoegyseg]
      ,[UgyintezesKezdete]
      ,[FelfuggesztettNapokSzama]
      ,[IntezesiIdo]
      ,[IntezesiIdoegyseg]
      ,[Ugy_Fajtaja]
      ,[FelfuggesztesOka]
      ,[SzignaloId]
      ,[SzignalasIdeje]
      ,[ElteltIdo]
      ,[ElteltIdoIdoEgyseg]
      ,[ElteltidoAllapot]
      ,[SakkoraAllapot]
      ,[ElteltIdoUtolsoModositas]
      ,[IrattarHelyfoglalas]
      ,[HatralevoNapok]
      ,[HatralevoMunkaNapok])
SELECT 1
      ,@admin
      ,@now
	  ,[Id]
      ,[Foszam]
      ,[Sorszam]
      ,[Ugyazonosito]
      ,[Alkalmazas_Id]
      ,[UgyintezesModja]
      ,[Hatarido]
      ,[SkontrobaDat]
      ,[LezarasDat]
      ,[IrattarbaKuldDatuma]
      ,[IrattarbaVetelDat]
      ,[FelhCsoport_Id_IrattariAtvevo]
      ,[SelejtezesDat]
      ,[FelhCsoport_Id_Selejtezo]
      ,[LeveltariAtvevoNeve]
      ,[FelhCsoport_Id_Felulvizsgalo]
      ,[FelulvizsgalatDat]
      ,[IktatoszamKieg]
      ,[Targy]
      ,[UgyUgyirat_Id_Szulo]
      ,[UgyUgyirat_Id_Kulso]
      ,[UgyTipus]
      ,[IrattariHely]
      ,[SztornirozasDat]
      ,[Csoport_Id_Felelos]
      ,[FelhasznaloCsoport_Id_Orzo]
      ,[Csoport_Id_Cimzett]
      ,[Jelleg]
      ,[IraIrattariTetel_Id]
      ,[IraIktatokonyv_Id]
      ,[SkontroOka]
      ,[SkontroVege]
      ,[Surgosseg]
      ,[SkontrobanOsszesen]
      ,[MegorzesiIdoVege]
      ,[Partner_Id_Ugyindito]
      ,[NevSTR_Ugyindito]
      ,[ElintezesDat]
      ,[FelhasznaloCsoport_Id_Ugyintez]
      ,[Csoport_Id_Felelos_Elozo]
      ,[KolcsonKikerDat]
      ,[KolcsonKiadDat]
      ,[Kolcsonhatarido]
      ,[BARCODE]
      ,[IratMetadefinicio_Id]
      ,[Allapot]
      ,[TovabbitasAlattAllapot]
      ,[Megjegyzes]
      ,[Azonosito]
      ,[Fizikai_Kezbesitesi_Allapot]
      ,[Kovetkezo_Orzo_Id]
      ,[Csoport_Id_Ugyfelelos]
      ,[Elektronikus_Kezbesitesi_Allap]
      ,[Kovetkezo_Felelos_Id]
      ,[UtolsoAlszam]
      ,[UtolsoSorszam]
      ,[IratSzam]
      ,[ElintezesMod]
      ,[RegirendszerIktatoszam]
      ,[GeneraltTargy]
      ,[Cim_Id_Ugyindito]
      ,[CimSTR_Ugyindito]
      ,[Ver]
      ,[Note]
      ,[Stat_id]
      ,[ErvKezd]
      ,[ErvVege]
      ,[Letrehozo_id]
      ,[LetrehozasIdo]
      ,[Modosito_id]
      ,[ModositasIdo]
      ,[Zarolo_id]
      ,[ZarolasIdo]
      ,[Tranz_id]
      ,[UIAccessLog_id]
      ,[UjOrzesiIdo]
      ,[IrattarId]
      ,[LezarasOka]
      ,[SkontroOka_Kod]
      ,[AKTIV]
      ,[ElozoAllapot]
      ,[UjOrzesiIdoIdoegyseg]
      ,[UgyintezesKezdete]
      ,[FelfuggesztettNapokSzama]
      ,[IntezesiIdo]
      ,[IntezesiIdoegyseg]
      ,[Ugy_Fajtaja]
      ,[FelfuggesztesOka]
      ,[SzignaloId]
      ,[SzignalasIdeje]
      ,[ElteltIdo]
      ,[ElteltIdoIdoEgyseg]
      ,[ElteltidoAllapot]
      ,[SakkoraAllapot]
      ,[ElteltIdoUtolsoModositas]
      ,[IrattarHelyfoglalas]
      ,[HatralevoNapok]
      ,[HatralevoMunkaNapok]
  FROM [EREC_UgyUgyiratok]
  WHERE Tranz_id = @tranz_id

 COMMIT
--ROLLBACK