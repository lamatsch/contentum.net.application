-- BUG_10425: EUZENET_OSSZES param�ter
--

DECLARE @Org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100)
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok where id=@Org) 

SET @record_id = '142CCFE3-5CD5-4008-9031-D540662D95F2'

IF @org_kod = 'NMHH'
	SET @ertek = '1'
ELSE
	SET @ertek = '0'

IF NOT EXISTS (SELECT 1 FROM [KRT_Parameterek] WHERE [Id] = @record_id)
BEGIN 
	Print 'INSERT - KRT_Parameterek - EUZENET_OSSZES  - ' + @ertek 
	INSERT INTO [KRT_Parameterek](
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,@Org
		,'EUZENET_OSSZES'
		,@ertek
		,'1'
		,'Ha a param�ter �rt�ke 1, akkor alap�rtelmezetten az �sszes elektronikus �zenet megjelenik.'
		)
END
