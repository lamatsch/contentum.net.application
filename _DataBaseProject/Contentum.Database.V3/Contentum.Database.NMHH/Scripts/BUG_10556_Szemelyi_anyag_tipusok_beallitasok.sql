﻿---- BUG_10556: Személyi anyag típusok alatti felsorolt szakaszok - beállítások

declare @Org uniqueidentifier
set @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek where nev='TUK' and org= @Org and getdate() between ervkezd and ervvege) 
IF @isTUK = '1'
BEGIN
	PRINT 'BUG_10556: Személyi anyag típusok alatti felsorolt szakaszok - beállítások'
	
	-- jogosultság lista checkbox-okkal
	UPDATE [EREC_TargySzavak] SET [ControlTypeSource]='~/Component/KodTarakCheckBoxList.ascx' WHERE [Id]='0A78D660-DED6-4BC0-B06B-029F8A75D0D8'

	-- jogosultság lista módosítása:
	UPDATE [KRT_KodTarak] SET [Nev]='a) állami vagy közfeladat végrehajtása érdekében történő ügyintézés, feldolgozás' WHERE [Id]='1D77F19C-C366-468F-819A-3E9D114B62A3';
	UPDATE [KRT_KodTarak] SET [Nev]='b) minősített adat nyilvántartásával kapcsolatos valamennyi tevékenység' WHERE [Id]='863ACF3D-B76C-46E0-9589-CF4BB871DD6C';
	UPDATE [KRT_KodTarak] SET [Nev]='c) minősített adat birtokban tartása' WHERE [Id]='BBAEEE87-1949-41B6-8A13-0FE74E4DA8CD';
	UPDATE [KRT_KodTarak] SET [Nev]='d) minősítési jelölés megismétlése' WHERE [Id]='711E31D7-3D0F-4594-B2A3-3F152ECCB8C3';
	UPDATE [KRT_KodTarak] SET [Nev]='e) minősített adat másolása, sokszorosítása' WHERE [Id]='815E8AD5-9BF6-4775-B641-1F9B0BEB90F9';
	UPDATE [KRT_KodTarak] SET [Nev]='f) minősített adat fordítása' WHERE [Id]='AF4C7581-515E-41F7-809E-A169864BC7A0';
	UPDATE [KRT_KodTarak] SET [Nev]='g) kivonat készítése' WHERE [Id]='24579EC5-4C3D-4A0C-BC1D-CAC2AB23C414';
	UPDATE [KRT_KodTarak] SET [Nev]='h) szerven belüli átadás' WHERE [Id]='FCB6B3FD-560D-40E6-A19A-7929C1C2B388';
	UPDATE [KRT_KodTarak] SET [Nev]='i) szerven kívülre továbbítás' WHERE [Id]='6CDF38FA-DAFC-407A-B976-215C47EB4BF1';
	UPDATE [KRT_KodTarak] SET [Nev]='j) szerven kívülre szállítás' WHERE [Id]='33AA2BAF-5AAE-47FE-B278-42E5DDFAE67D';
	UPDATE [KRT_KodTarak] SET [Nev]='k) selejtezés illetve megsemmisítés' WHERE [Id]='568459D2-E9F9-4B4A-A9CA-FF9ED4127A05';
	UPDATE [KRT_KodTarak] SET [Nev]='l) felhasználói engedély kiadása' WHERE [Id]='0E2847AB-C02C-4E6A-B06C-693ECDFA3F47';
	UPDATE [KRT_KodTarak] SET [Nev]='m) megismerési engedély kiadása' WHERE [Id]='01ED5D88-3EBD-4C10-858E-AE2F1CCB1025';
	UPDATE [KRT_KodTarak] SET [Nev]='n) nemzeti minősített adat felülvizsgálata' WHERE [Id]='6B416DE8-10D5-4F21-9FDA-1008014C50E1';
	UPDATE [KRT_KodTarak] SET [Nev]='o) minősített adat külföldi személy vagy külföldi szerv részére hozzáférhetővé tételének engedélyezése' WHERE [Id]='1F669B1C-31D7-4E86-870F-27007D723534';
	UPDATE [KRT_KodTarak] SET [Nev]='p) minősített adat külföldre vitelének vagy külföldről való behozatalának engedélyezése' WHERE [Id]='1D9A337A-428A-45C8-B3BB-873D4C809BBC';
	UPDATE [KRT_KodTarak] SET [Nev]='q) titoktartási kötelezettség alóli felmentés' WHERE [Id]='4D87A770-E926-45F3-BC6C-B305CE24C540';
	UPDATE [KRT_KodTarak] SET [Nev]='r) minősítési jelölés megismétlésének megtiltása' WHERE [Id]='B5F4C228-960B-4BED-883F-76ECCB521935';

	-- 'Minősítés szintje' mező átnevezése 'Felhasználás minősítési szintje' mezőre
	UPDATE [EREC_TargySzavak] SET [Targyszavak]='Felhasználás minősítési szintje' WHERE [Id]='EE3AE8B2-8B7B-4F5E-A9B9-5A86C6F0DBB5'

	-- Megbízásnál a mezők sorrendje
	update erec_obj_metaadatai set sorszam=1 where Id='AF7A6D5A-A2CD-48C3-AFD1-D99DEC4A305E'
	update erec_obj_metaadatai set sorszam=2 where Id='A5163C30-2245-4A5C-ACB9-88CA32459E21'
	update erec_obj_metaadatai set sorszam=3 where Id='08120EF0-3E0A-4AF3-A0CC-BAA83BC435E8'
	update erec_obj_metaadatai set sorszam=4 where Id='95B233E2-F4B8-42DD-BEBF-43EB6E46C4F8'
	update erec_obj_metaadatai set sorszam=5 where Id='E32024FF-6733-4439-9734-6CA2F98EA9A0'

	-- Kinevezésnél a mezők sorrendje
	update erec_obj_metaadatai set sorszam=1 where Id='3EE4A3CC-0E46-48EE-BE6F-54482594F48E'
	update erec_obj_metaadatai set sorszam=2 where Id='5BF11F8B-D008-4143-8538-BB85D803A970'
	update erec_obj_metaadatai set sorszam=3 where Id='790FE589-EEBF-45BF-AE6A-967ADA57B121'
	update erec_obj_metaadatai set sorszam=4 where Id='D61EF34F-CD51-468A-8F16-61AB1EDA2F9B'
	update erec_obj_metaadatai set sorszam=5 where Id='32A09E4F-75A8-4CC3-91B7-9153B94ED1DA'
	
	-- Iktatószám => AH iktatószám
	UPDATE [EREC_TargySzavak] SET [Targyszavak]='AH Iktatószám', [BelsoAzonosito]='AH_iktatoszam' WHERE [Id]='6AE15533-7215-43A6-B1A9-159E4DBC9BA3'
	
	---- NMHH iktatószám: 
	declare @objMetaDefId_Nemztebizt_ell_metaadatai uniqueidentifier
	set @objMetaDefId_Nemztebizt_ell_metaadatai = '1A1B2EEC-9B6A-E811-80C7-00155D027EA9'
	
	declare @Letrehozo_Id uniqueidentifier
	set @Letrehozo_Id = '54E861A5-36ED-44CA-BAA7-C287D125B309'
	
	declare @ControlTypeSourceTextBox nvarchar(64)
	set @ControlTypeSourceTextBox = 'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls'
	
	declare @Tipus char(1)
	set @Tipus = '1'
	
	-- EREC_Obj_MetaAdatai - Nemztebizt_ell metaadatai - NMHH Iktatószám
	declare @targyszoId_NMHH_Iktatoszam uniqueidentifier
	set @targyszoId_NMHH_Iktatoszam = '116C5845-4692-4BB9-B239-4F82890265D1'
	
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_NMHH_Iktatoszam)
	BEGIN
		print 'insert - Nemzetbiztonsági ellenorzés kezdeményezése - NMHH Iktatószám'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_NMHH_Iktatoszam
		   ,@Org
		   ,@Tipus
		   ,'NMHH Iktatószám'
		   ,'NMHH Iktatoszam'
		   ,@ControlTypeSourceTextBox
		   ,NULL
		   ,@Letrehozo_Id
		   ,NULL
		)
	END
	ELSE
	BEGIN
		print 'Rekord már létezik: EREC_TargySzavak: Nemzetbiztonsági ellenorzés kezdeményezése - NMHH Iktatószám'
	END
	
	-- EREC_Obj_MetaAdatai - Nemztebizt_ell metaadatai - NMHH Iktatószám	
	declare @recordId uniqueidentifier
	set @recordId = '92350AA1-E09C-4A76-B85C-7F06B1ABBF05'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Nemztebizt_ell metaadatai - NMHH Iktatószám'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_Nemztebizt_ell_metaadatai
		   ,@targyszoId_NMHH_Iktatoszam
		   ,1
		   ,'1'
		   ,'0'
		   ,@Letrehozo_Id
		   ,NULL
		)
	END
END
GO