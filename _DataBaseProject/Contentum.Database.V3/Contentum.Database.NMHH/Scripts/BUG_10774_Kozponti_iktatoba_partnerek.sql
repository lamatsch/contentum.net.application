----------------------------------BUG_10774-----------------------------------------
/*
Add users to K�zpontiIratt�r group:
	Baranyi-T�th Zsuzsanna KI
	Darabos Andrea
	D�zsa �gnes
	Forczekn� Makkai Judit
	Gr�sz Anna
	Kov�cs T�mea
	Luk�cs Barbara
	Majorosn� Fil�k Judit
	N�meth Anna
	Ol�h Barbara
	Ol�hn� Jok�n Ildik�
	Prajczern� Nagy Anik�
	P�lkov�cs Andrea
	Solymosi N�ra
	Somogyv�ri-Jech Marina
	Szab�n� Kiss M�rta
	Tak�cs D. Orsolya
	Tam�si T�mea
	T�th Zsanett
	Ujsz�szi-V�mos Aranka
	Zombory Eszter
*/
DECLARE @ADMIN_ID nvarchar(100) = '54E861A5-36ED-44CA-BAA7-C287D125B309'
DECLARE @ORG_ID nvarchar(100) = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) = (select kod from KRT_Orgok where id=@ORG_ID) 
DECLARE @KOZPONTI_IKTATO_CSOPORT_NEV nvarchar(100) = 'K�zponti Iktat�'
DECLARE @KOZPONTI_IKTATO_CSOPORT_ID uniqueidentifier = (select Id from KRT_Csoportok where Nev = @KOZPONTI_IKTATO_CSOPORT_NEV)
DECLARE @PartnerColumns xml = convert(xml,N'<root><Tipus/><Partner_id/><Partner_id_kapcsolt/><ErvKezd/><ErvVege/><Ver/><Letrehozo_id/><LetrehozasIdo/></root>')
DECLARE @CsoportTagokColumns xml = convert(xml,N'<root><Id/><Tipus/><Csoport_Id/><Csoport_Id_Jogalany/><System/><ErvKezd/><ErvVege/><Ver/><Letrehozo_id/><LetrehozasIdo/></root>')
DECLARE @LINK_ID uniqueidentifier
DECLARE @FELHASZNALO_ID uniqueidentifier
DECLARE @PARTNER_ID uniqueidentifier
DECLARE @NAME nvarchar(100)
DECLARE @NOW datetime = getDate()

IF @ORG_KOD = 'NMHH' AND EXISTS(SELECT 1 FROM KRT_Csoportok WHERE Nev = @KOZPONTI_IKTATO_CSOPORT_NEV)
BEGIN
	PRINT 'Creating new user connection to K�zponti Iktat� csoport group and add them as partner in hierarchy'
	
	DECLARE @NAMES TABLE (name VARCHAR(100))
	INSERT @NAMES (name) VALUES
		('Baranyi-T�th Zsuzsanna KI'),
		('Darabos Andrea'),
		('D�zsa �gnes'),
		('Forczekn� Makkai Judit'),
		('Gr�sz Anna'),
		('Kov�cs T�mea'),
		('Luk�cs Barbara'),
		('Majorosn� Fil�k Judit'),
		('N�meth Anna'),
		('Ol�h Barbara'),
		('Ol�hn� Jok�n Ildik�'),
		('Prajczern� Nagy Anik�'),
		('P�lkov�cs Andrea'),
		('Solymosi N�ra'),
		('Somogyv�ri-Jech Marina'),
		('Szab�n� Kiss M�rta'),
		('Tak�cs D. Orsolya'),
		('Tam�si T�mea'),
		('T�th Zsanett'),
		('Ujsz�szi-V�mos Aranka'),
		('Zombory Eszter')

	DECLARE NAME_CURSOR CURSOR FOR SELECT name FROM @NAMES
	
	OPEN NAME_CURSOR
	FETCH NEXT FROM NAME_CURSOR INTO @NAME
	WHILE @@FETCH_STATUS = 0
	BEGIN
        PRINT @NAME
        IF EXISTS (SELECT Id, Partner_id FROM KRT_Felhasznalok WHERE Nev=@NAME)
        BEGIN
            SELECT @FELHASZNALO_ID = Id, @PARTNER_ID = Partner_id FROM KRT_Felhasznalok WHERE Nev=@NAME

		    IF NOT EXISTS (
                SELECT 1 
                FROM KRT_PartnerKapcsolatok pk
                JOIN KRT_Felhasznalok f ON f.Partner_id = pk.Partner_id
                WHERE f.Nev = @NAME AND pk.Partner_id_kapcsolt = @KOZPONTI_IKTATO_CSOPORT_ID AND pk.Partner_id = @PARTNER_ID)
		    BEGIN
                PRINT 'Inserting into KRT_PartnerKapcsolatok' 		
                EXEC [sp_KRT_PartnerKapcsolatokInsert] 
	                @Id=NULL,
	                @Tipus=N'2',
	                @Partner_id=@PARTNER_ID,
	                @Partner_id_kapcsolt=@KOZPONTI_IKTATO_CSOPORT_ID,
	                @ErvKezd=@NOW,
	                @ErvVege='4700-12-31 00:00:00',
	                @Ver=0,
	                @Note=NULL,
	                @Stat_id=NULL,
	                @Letrehozo_id=@ADMIN_ID,
	                @LetrehozasIdo=@NOW,
	                @Modosito_id=NULL,
	                @ModositasIdo=NULL,
	                @Zarolo_id=NULL,
	                @ZarolasIdo=NULL,
	                @Tranz_id=NULL,
	                @UIAccessLog_id=NULL,
	                @UpdatedColumns=@PartnerColumns,
	                @ResultUid=@LINK_ID OUTPUT
                SELECT @LINK_ID
		    END	
            ELSE 
            BEGIN
                PRINT 'Already exists in KRT_PartnerKapcsolatok' 
                SELECT @LINK_ID = pk.Id
                FROM KRT_PartnerKapcsolatok pk
                JOIN KRT_Felhasznalok f ON f.Partner_id = pk.Partner_id
                WHERE f.Nev = @NAME AND pk.Partner_id_kapcsolt = @KOZPONTI_IKTATO_CSOPORT_ID AND pk.Partner_id = @PARTNER_ID
            END

		    IF NOT EXISTS (
                SELECT 1 FROM KRT_CsoportTagok t
                JOIN KRT_Felhasznalok f ON f.Id = t.Csoport_id_Jogalany 
                WHERE f.Nev = @NAME AND t.Csoport_Id = @KOZPONTI_IKTATO_CSOPORT_ID AND Csoport_id_Jogalany = @FELHASZNALO_ID)
		    BEGIN		
                PRINT 'Inserting into KRT_CsoportTagok' 		
                EXEC [sp_KRT_CsoportTagokInsert] 
	                @Id=@LINK_ID,
	                @Tipus=N'2',
	                @Csoport_Id=@KOZPONTI_IKTATO_CSOPORT_ID,
	                @Csoport_Id_Jogalany=@FELHASZNALO_ID,
	                @ObjTip_Id_Jogalany=NULL,
	                @ErtesitesMailCim=NULL,
	                @ErtesitesKell=NULL,
	                @System='1',
	                @Orokolheto=NULL,
	                @ErvKezd=@NOW,
	                @ErvVege='4700-12-31 00:00:00',
	                @Ver=0,
	                @Note=NULL,
	                @Stat_id=NULL,
	                @Letrehozo_id=@ADMIN_ID,
	                @LetrehozasIdo=@NOW,
	                @Modosito_id=NULL,
	                @ModositasIdo=NULL,
	                @Zarolo_id=NULL,
	                @ZarolasIdo=NULL,
	                @Tranz_id=NULL,
	                @UIAccessLog_id=NULL,
	                @UpdatedColumns=@CsoportTagokColumns,
	                @ResultUid=@LINK_ID OUTPUT
                SELECT @LINK_ID

		    END	
            ELSE PRINT 'Already exists in KRT_CsoportTagok' 

		END
        ELSE PRINT 'Not exists in KRT_Felhasznalok' 
		FETCH NEXT FROM NAME_CURSOR INTO @NAME
	END

END
GO
