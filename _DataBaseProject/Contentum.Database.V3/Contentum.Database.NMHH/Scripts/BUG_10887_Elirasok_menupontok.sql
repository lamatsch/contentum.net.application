﻿UPDATE [KRT_Menuk]
SET Nev = 'Teljesség ellenőrzés'
WHERE Id = 'D7FCEAD1-C16D-4546-B383-6010E4BAE9A6' AND Nev = 'Teljesség ellenorzés'


UPDATE [KRT_Menuk]
SET Nev = 'Küldemény érkeztetés mail-ből'
WHERE Id = '76A77588-9264-4D91-95BB-551CC9253317' AND Nev = 'Küldemény érkeztetés mail-bol'


UPDATE KRT_KodTarak
SET Nev = 'Postai elsőbbségi'
FROM [KRT_KodTarak] KT
JOIN [KRT_KodCsoportok] KCS ON KCS.Id = KT.KodCsoport_Id
WHERE KT.Nev like '%Postai elsobbségi%' AND KCS.Nev = 'KÜLDEMÉNY KÜLDÉS MÓDJA' AND KCS.Kod = '17' 


UPDATE KRT_Funkciok
SET Nev = 'Teljesség ellenőrzés'
FROM [KRT_Funkciok] KF
JOIN [KRT_Muveletek] KM ON KM.Id = KF.Muvelet_Id
WHERE KF.Nev like '%tTeljesség ellenorzés%' AND KM.Id = 'AF946A61-038A-4DA5-96AC-D226A9627862' AND KM.Kod = 'Ellenorzes'


UPDATE KRT_Funkciok
SET Nev = 'Belső irat iktatása CSAK alszámra (Korlátozó jog)'
FROM [KRT_Funkciok] KF
JOIN [KRT_Muveletek] KM ON KM.Id = KF.Muvelet_Id
WHERE KF.Nev like '%Belso irat iktatása CSAK alszámra %' AND KM.Id = '645621BD-EF6E-4139-8F21-231C9BD13A82' AND KM.Kod = 'Iktatas'


UPDATE KRT_Funkciok
SET Nev = 'Bejövő irat iktatása CSAK alszámra (Korlátozó jog)'
FROM [KRT_Funkciok] KF
JOIN [KRT_Muveletek] KM ON KM.Id = KF.Muvelet_Id
WHERE KF.Nev like '%Bejövo irat iktatása CSAK alszámra %' AND KM.Id = '645621BD-EF6E-4139-8F21-231C9BD13A82' AND KM.Kod = 'Iktatas'
 