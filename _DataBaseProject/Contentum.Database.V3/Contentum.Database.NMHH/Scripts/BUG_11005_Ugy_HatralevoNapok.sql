BEGIN TRANSACTION

------------------
ALTER TABLE dbo.EREC_UgyUgyiratok
DROP COLUMN HatralevoNapok

ALTER TABLE dbo.EREC_UgyUgyiratok
ADD HatralevoNapok AS ([dbo].[FN_UgyUgyirat_HatralevoNapok](GETDATE(),[Hatarido]))
------------------
ALTER TABLE dbo.EREC_UgyUgyiratok
DROP COLUMN HatralevoMunkaNapok

ALTER TABLE dbo.EREC_UgyUgyiratok
ADD HatralevoMunkaNapok  AS ([dbo].[FN_UgyUgyirat_HatralevoMunkaNapokV3](GETDATE(),[Hatarido]))
------------------

COMMIT