--------------------------------------------------------------------------------
-- BUG_11085 - Ügyirat szignálás esetén FeladatJelzo kikapcsolása
-- Elég ha egyszer lefut és csak TUK-on kell

DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 

if  (@isTUK = '1')
BEGIN

	print 'KRT_Funkciok.UgyiratSzignalas id'		
	DECLARE @record_id uniqueidentifier SET @record_id = (select Id from KRT_Funkciok
				where Kod='UgyiratSzignalas') 
	Print '@record_id='+convert(NVARCHAR(100),@record_id) 

	if @record_id IS NOT NULL 
	BEGIN 
		 Print 'UPDATE FeladatJelzo -> 0'
		 UPDATE KRT_Funkciok
		 SET
			FeladatJelzo = '0'
		 WHERE Id=@record_id 
	END
END
GO

--------------------------------------------------------------------------------
------------------------------ BUG_11085 vége ----------------------------------