----------------------------------BUG_11499-----------------------------------------
/*
Szervezetek hozz�ad�sa T�K-h�z:
	- Nyilv�ntart�: NY
	- a Reviczky utcai kezel� pont: KR
	- a Visegr�di utcai kezel� pont: KV
	- a Debreceni kezel� pont: CD
	- a Miskolci kezel� pont : CM
	- a P�csi kezel� pont: CP
	- a Soproni kezel� pont: CS
	- a Szegedi kezel� pont: CZ

Hozz�rendel�sek ezekhez a szervezetekhez:

	Nyilv�ntart�:
		- Csoma Imre		Biztons�gszervez�si �s Min�s�tettadat-v�delmi Oszt�ly
		- Haumann D�vid 	Biztons�gszervez�si �s Min�s�tettadat-v�delmi Oszt�ly
		- S�gi Dorottya		Biztons�gszervez�si �s Min�s�tettadat-v�delmi Oszt�ly
		- Szekeres Attila 	Biztons�gszervez�si �s Min�s�tettadat-v�delmi Oszt�ly

	Reviczky utcai Kezel� Pont:
		- Ol�hn� Jok�n Ildik�		Igazgat�si Oszt�ly

	Visegr�di utcai Kezel� Pont:
		- G�rg�nyi L�szl� Istv�nn�	V�delmi �s Rend�szeti Frekvenciagazd�lkod�si F�oszt�ly
		- Szer�mi Krisztina  		Frekvencia- �s Azonos�t�gazd�lkod�si F�oszt�ly

	Debreceni Kezel� Pont:
		- N�meth Ern� Attila		Debreceni Hat�s�gi Iroda
		- N�meth J�zsef				Debreceni Hat�s�gi Iroda

	Miskolci Kezel� Pont:
		- Varga Zolt�n					Miskolci Hat�s�gi Iroda
		- Kormosn� dr. Kriston Ildik�	Miskolci Hat�s�gi Iroda
		- Csecs�di J�zsefn�      		Miskolci Hat�s�gi Iroda

	P�csi Kezel� Pont:
		- K�ti Zolt�n				P�csi Hat�s�gi Iroda
		- Horv�th Melinda			P�csi Hat�s�gi Iroda

	Soproni Kezel� Pont:
		- Horv�th Tamara Katalin	Soproni Hat�s�gi Iroda
		- Sl�ber Tibor				Soproni Hat�s�gi Iroda

	Szegedi Kezel� Pont:
		- F�n�d Zolt�n J�nosn�		Szegedi Hat�s�gi Iroda
		- K�vesdyn� Szab� Ir�n		Szegedi Hat�s�gi Iroda

*/

DECLARE @ADMIN_ID nvarchar(100) = '54E861A5-36ED-44CA-BAA7-C287D125B309'
DECLARE @ORG_ID nvarchar(100) = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD nvarchar(100) = (select kod from KRT_Orgok where id=@ORG_ID)
DECLARE @TUK nvarchar(100) = (select Ertek from KRT_Parameterek where Nev='TUK')
DECLARE @Szervezet_Id uniqueidentifier -- A KRT_Partnerek �s a KRT_Csoportok t�bl�kban a szervezethez tartoz� ID.
DECLARE @Partner_Id uniqueidentifier -- A KRT_Partnerek t�bl�ban a szem�lyhez tartoz� ID.
DECLARE @Munkahely_Id uniqueidentifier -- A KRT_Partnerek t�bl�ban a szem�ly munkahely�hez tartoz� ID.
DECLARE @Felhasznalo_Id uniqueidentifier -- A KRT_Felhasznalok �s a KRT_Csoportok t�bl�kban a szem�lyhez tartoz� ID.
DECLARE @Szemely_Id uniqueidentifier -- A KRT_Szemelyek t�bl�ban a szem�lyhez tartoz� ID.
DECLARE @PartnerKapcsolat_Id uniqueidentifier -- A KRT_PartnerKapcsolatok �s a KRT_Csoporttagok t�bl�kban a k�z�s ID.
DECLARE @Szerepkor_Id uniqueidentifier -- A KRT_Szerepkorok t�bl�ban a FEJLESZTO-h�z tartoz� ID.
DECLARE @Felhasznalo_Szerepkor_Id uniqueidentifier -- A KRT_Felhasznalo_Szerepkorok t�bl�ban az ID.
DECLARE @szervezet nvarchar(100)
DECLARE @munkahely nvarchar(100)
DECLARE @tag nvarchar(100)
DECLARE @felhnev nvarchar(100)
DECLARE @kod nvarchar(10)
DECLARE @NOW datetime = getDate()
DECLARE @MAXDATE datetime = '4700-12-31 00:00:00'

DECLARE @TeljesNev nvarchar(100)
DECLARE @CsaladiNev nvarchar(100)
DECLARE	@Utonev nvarchar(100)
DECLARE	@TovabbiUtonev nvarchar(100)
DECLARE @Titulus nvarchar(100)
DECLARE @elsoszokoz int
DECLARE @masodikszokoz int

IF @ORG_KOD = 'NMHH' AND @TUK = '1'
BEGIN
	PRINT 'Nyilv�ntart� �s kezel� pontok hozz�ad�sa a szervezetekhez'

	DECLARE @Szervezetek TABLE (szervezet NVARCHAR(100), kod NVARCHAR(10), munkahely NVARCHAR(100))
	INSERT @Szervezetek (szervezet, kod, munkahely) VALUES
		('Nyilv�ntart�', 'NY', 'Biztons�gszervez�si �s Min�s�tettadat-v�delmi Oszt�ly'),
		('Reviczky utcai kezel� pont', 'KR', 'Igazgat�si oszt�ly'),
		('Visegr�di utcai kezel� pont', 'KV', 'V�delmi �s Rend�szeti Frekvenciagazd�lkod�si F�oszt�ly'),
		('Debreceni kezel� pont', 'CD', 'Debreceni Hat�s�gi Iroda'),
		('Miskolci kezel� pont', 'CM', 'Miskolci Hat�s�gi Iroda'),
		('P�csi kezel� pont', 'CP', 'P�csi Hat�s�gi Iroda'),
		('Soproni kezel� pont', 'CS', 'Soproni Hat�s�gi Iroda'),
		('Szegedi kezel� pont', 'CZ', 'Szegedi Hat�s�gi Iroda')

	DECLARE @Csoporttagok TABLE (szervezet NVARCHAR(100), tag NVARCHAR(100), felhnev NVARCHAR(100))
	INSERT @Csoporttagok (szervezet, tag, felhnev) VALUES
		('Nyilv�ntart�', 'Csoma Imre', 'belso\csomaim'),
		('Nyilv�ntart�', 'Haumann D�vid', 'belso\haumannd'),
		('Nyilv�ntart�', 'S�gi Dorottya', 'belso\sagido'),
		('Nyilv�ntart�', 'Szekeres Attila', 'belso\szekeresa'),
		('Reviczky utcai kezel� pont', 'Ol�hn� Jok�n Ildik�', 'belso\olahne'),
		('Visegr�di utcai kezel� pont', 'G�rg�nyi L�szl� Istv�nn�', 'belso\gorglne'),
		('Visegr�di utcai kezel� pont', 'Szer�mi Krisztina', 'belso\szeremi'),
		('Debreceni kezel� pont', 'N�meth Ern� Attila', 'belso\nemetha1'),
		('Debreceni kezel� pont', 'N�meth J�zsef', 'belso\nemethjo'),
		('Miskolci kezel� pont', 'Varga Zolt�n', 'belso\vargazo'),
		('Miskolci kezel� pont', 'Kormosn� dr. Kriston Ildik�', 'belso\kormokri'),
		('Miskolci kezel� pont', 'Csecs�di J�zsefn�', 'belso\csecsodine'),
		('P�csi kezel� pont', 'K�ti Zolt�n', 'belso\kutizoli'),
		('P�csi kezel� pont', 'Horv�th Melinda', 'belso\horvmel'),
		('Soproni kezel� pont', 'Horv�th Tamara Katalin', 'belso\horvatam'),
		('Soproni kezel� pont', 'Sl�ber Tibor', 'belso\sleber'),
		('Szegedi kezel� pont', 'F�n�d Zolt�n J�nosn�', 'belso\fonad'),
		('Szegedi kezel� pont', 'K�vesdyn� Szab� Ir�n', 'belso\kovesdy')

	SELECT @Szerepkor_Id = Id FROM KRT_Szerepkorok WHERE Nev = 'FEJLESZTO'

	DECLARE szervezet_cursor CURSOR FOR SELECT szervezet, kod, munkahely FROM @Szervezetek

	OPEN szervezet_cursor
	FETCH NEXT FROM szervezet_cursor INTO @szervezet, @kod, @munkahely
	WHILE @@FETCH_STATUS = 0
	BEGIN

		IF NOT EXISTS (SELECT 1 FROM KRT_Partnerek WHERE Nev=@szervezet AND ErvVege > @NOW)
		BEGIN
			PRINT 'Hozz�ad�s a KRT_Partnerek -hez: ' + @szervezet
			EXEC [sp_KRT_PartnerekInsert]
				@Id=NULL,
				@Tipus=N'10',
				@Nev=@szervezet,
				@Belso='1',
				@Allapot=N'JOVAHAGYANDO',
				@ErvVege=@MAXDATE,
				@Ver=0,
				@Letrehozo_id=@ADMIN_ID,
				@LetrehozasIdo=@NOW,
				@ResultUid=@Szervezet_Id OUTPUT
		END
		ELSE
		BEGIN
			PRINT 'M�r l�tezik a KRT_Partnerek -ben: ' + @szervezet
			SET @Szervezet_Id = NULL
			SELECT @Szervezet_Id = Id FROM KRT_Partnerek WHERE Nev = @szervezet AND ErvVege > @NOW
		END

		IF NOT EXISTS (SELECT 1 FROM KRT_Csoportok WHERE Nev = @szervezet AND Id = @Szervezet_Id AND ErvVege > @NOW)
		BEGIN
			PRINT 'Hozz�ad�s a KRT_Csoportok -hoz: ' + @szervezet
			EXEC [sp_KRT_CsoportokInsert]
				@Id=@Szervezet_Id,
				@Kod=@kod,
				@Nev=@szervezet,
				@Tipus=N'0',
				@System='0',
				@JogosultsagOroklesMod='1',
				@ErvVege=@MAXDATE,
				@Ver=0,
				@Letrehozo_id=@ADMIN_ID,
				@LetrehozasIdo=@NOW,
				@ResultUid=@Szervezet_Id OUTPUT
		END
		ELSE
		BEGIN
			PRINT 'M�r l�tezik a KRT_Csoportok-ban: ' + @szervezet
		END

		SET @Munkahely_Id = NULL
		SELECT @Munkahely_Id = Id FROM KRT_Partnerek WHERE Nev = @munkahely
		IF @Munkahely_Id IS NULL PRINT 'Munkahely nem l�tezik: ' + @munkahely

		DECLARE tag_cursor CURSOR FOR SELECT tag, felhnev FROM @Csoporttagok WHERE szervezet = @szervezet

		OPEN tag_cursor
		FETCH NEXT FROM tag_cursor INTO @tag, @felhnev
		WHILE @@FETCH_STATUS = 0
		BEGIN

			IF NOT EXISTS (SELECT 1 FROM KRT_Partnerek WHERE Nev=@tag AND ErvVege > @NOW)
			BEGIN
				-- Felhaszn�l� felv�tele

				PRINT 'Hozz�ad�s a KRT_Partnerekhez, KRT_Szemelyekhez, KRT_Felhasznalokhoz, KRT_Csoportokhoz: ' + @tag
				EXEC [sp_KRT_PartnerekInsert]
					@Id=NULL,
					@Org=@ORG_ID,
					@Tipus=N'20',
					@Nev=@tag,
					@Belso='1',
					@Allapot=N'JOVAHAGYANDO',
					@ErvVege=@MAXDATE,
					@Ver=1,
					@Letrehozo_id=@ADMIN_ID,
					@LetrehozasIdo=@NOW,
					@ResultUid=@Partner_Id OUTPUT

				SET @TeljesNev = @tag
				SET @Titulus = NULL
				IF (charindex('dr. ', @TeljesNev) > 0)
				BEGIN
					SET @Titulus = 'dr.'
					SET @TeljesNev = REPLACE(@TeljesNev, 'dr. ','')
				END
				SET @elsoszokoz = charindex(' ', @TeljesNev)
				SET @masodikszokoz = charindex(' ', @TeljesNev, @elsoszokoz + 1)
				SET @CsaladiNev = substring(@TeljesNev, 1, @elsoszokoz-1)
				IF (@masodikszokoz > 0)
				BEGIN
					SET @Utonev = substring(@TeljesNev, @elsoszokoz+1, @masodikszokoz - @elsoszokoz - 1)
					SET @TovabbiUtonev = substring(@TeljesNev, @masodikszokoz + 1, len(@TeljesNev) - @masodikszokoz)
				END
				ELSE
				BEGIN
					SET @Utonev = substring(@TeljesNev, @elsoszokoz + 1, len(@TeljesNev) - @elsoszokoz)
					SET @TovabbiUtonev = NULL
				END

				EXEC [sp_KRT_SzemelyekInsert]
					@Id=NULL,
					@Partner_Id=@Partner_Id,
					@UjTitulis=@Titulus,
					@UjCsaladiNev=@CsaladiNev,
					@UjUtonev=@Utonev,
					@UjTovabbiUtonev=@TovabbiUtonev,
					@Letrehozo_id=@ADMIN_ID,
					@LetrehozasIdo=@NOW,
					@ResultUid=@Szemely_Id OUTPUT

				EXEC [sp_KRT_FelhasznalokInsert]
					@Id=NULL,
					@Org=@ORG_ID,
					@Partner_id=@Partner_Id,
					@Tipus=N'Windows',
					@UserNev=@felhnev,
					@Nev=@tag,
					@JelszoLejaratIdo=@NOW,
					@WrongPassCnt=0,
					@Partner_Id_Munkahely=@Munkahely_Id,
					@ErvVege=@MAXDATE,
					@Ver=0,
					@Letrehozo_id=@ADMIN_ID,
					@LetrehozasIdo=@NOW,
					@ResultUid=@Felhasznalo_Id OUTPUT

				EXEC [sp_KRT_CsoportokInsert]
					@Id=@Felhasznalo_Id,
					@Org=@ORG_ID,
					@Kod=NULL,
					@Nev=@tag,
					@Tipus=N'1',
					@Jogalany='1',
					@System='1',
					@ErvVege=@MAXDATE,
					@Letrehozo_id=@ADMIN_ID,
					@LetrehozasIdo=@NOW,
					@ResultUid=@Felhasznalo_Id OUTPUT

				PRINT 'FEJLESZTO szerepk�r hozz�ad�sa'
				EXEC [sp_KRT_Felhasznalo_SzerepkorInsert]
					@Id=NULL,
					@Felhasznalo_Id=@Felhasznalo_Id,
					@Szerepkor_Id=@Szerepkor_Id,
					@Letrehozo_id=@ADMIN_ID,
					@LetrehozasIdo=@NOW,
					@ResultUid=@Felhasznalo_Szerepkor_Id OUTPUT

			END

			SELECT @Partner_id = Id FROM KRT_Partnerek WHERE Nev=@tag AND ErvVege > @NOW

			IF NOT EXISTS (SELECT 1 FROM KRT_PartnerKapcsolatok WHERE Partner_id_kapcsolt = @Szervezet_Id AND Partner_id = @Partner_id AND ErvVege > @NOW)
			BEGIN
				PRINT 'Partner hozz�rendel�s a ' + @szervezet + ' -hoz : ' + @tag
				EXEC [sp_KRT_PartnerKapcsolatokInsert]
					@Tipus=N'2',
					@Partner_id=@Partner_id,
					@Partner_id_kapcsolt=@Szervezet_Id,
					@ErvKezd=@NOW,
					@ErvVege=@MAXDATE,
					@Ver=0,
					@Letrehozo_id=@ADMIN_ID,
					@LetrehozasIdo=@NOW,
					@ResultUid=@PartnerKapcsolat_Id OUTPUT
			END
			ELSE
			BEGIN
				PRINT 'Partner m�r hozz�rendelt a ' + @szervezet + ' -hoz : ' + @tag
				SET @PartnerKapcsolat_Id = NULL
				SELECT @PartnerKapcsolat_Id = Id FROM KRT_PartnerKapcsolatok WHERE Partner_id_kapcsolt = @Szervezet_Id AND Partner_id = @Partner_id AND ErvVege > @NOW
			END

			IF EXISTS (
				SELECT 1
				FROM KRT_Csoportok cs
				JOIN KRT_Felhasznalok f ON f.Id = cs.Id
				WHERE f.Partner_Id = @Partner_Id
				AND f.Nev = @tag AND cs.Nev = @tag
				AND f.ErvVege > @NOW AND cs.ErvVege > @NOW)
			BEGIN
				SELECT @Felhasznalo_Id = Id FROM KRT_Felhasznalok WHERE Nev = @tag
				IF NOT EXISTS (SELECT 1 FROM KRT_Csoporttagok WHERE Csoport_Id = @Szervezet_Id AND Csoport_Id_Jogalany = @Felhasznalo_Id AND Id = @PartnerKapcsolat_Id AND ErvVege > @NOW)
				BEGIN
					PRINT 'Csoport hozz�rendel�s a ' + @szervezet + ' -hoz : ' + @tag
					EXEC [sp_KRT_CsoportTagokInsert]
						@Id=@PartnerKapcsolat_Id,
						@Tipus=N'2',
						@Csoport_Id=@Szervezet_Id,
						@Csoport_Id_Jogalany=@Felhasznalo_Id,
						@ErvKezd=@NOW,
						@ErvVege=@MAXDATE,
						@Ver=0,
						@Letrehozo_id=@ADMIN_ID,
						@LetrehozasIdo=@NOW,
						@ResultUid=@PartnerKapcsolat_Id OUTPUT
				END
				ELSE
				BEGIN
					PRINT 'Csoport m�r hozz�rendelt a ' + @szervezet + ' -hoz : ' + @tag
				END
			END
			ELSE
			BEGIN
				PRINT 'Csoportok k�z�tt nem tal�lhat�: ' + @tag
			END

			FETCH NEXT FROM tag_cursor INTO @tag, @felhnev
		END

		CLOSE tag_cursor
		DEALLOCATE tag_cursor

		FETCH NEXT FROM szervezet_cursor INTO @szervezet, @kod, @munkahely
	END

	CLOSE szervezet_cursor
	DEALLOCATE szervezet_cursor

END
GO
