DECLARE @ADMIN_ID nvarchar(100) = '54E861A5-36ED-44CA-BAA7-C287D125B309'
DECLARE @NOW datetime = getDate()
DECLARE @SzerepkorokColumns xml
SET @SzerepkorokColumns=convert(xml,N'<root><Funkcio_Id/><Szerepkor_Id/><ErvKezd/><ErvVege/><Ver/><Letrehozo_id/><LetrehozasIdo/></root>')
DECLARE @ResultUid uniqueidentifier
DECLARE @FunkcioId nvarchar(100) = 'FD325725-FC11-4C1D-90C9-522D7E523C5C' -- Küldemény tértivevény megtekintés
DECLARE @SzerepkorId nvarchar(100) = 'D966D634-9C0E-456B-BFB2-AF00573905A3' -- Postázó


IF NOT EXISTS (
	SELECT 1 FROM KRT_Szerepkor_Funkcio
	WHERE Funkcio_Id=@FunkcioId
	AND Szerepkor_Id=@SzerepkorId)
BEGIN
	PRINT 'Inserting into KRT_Szerepkor_Funkcio'
	EXEC [sp_KRT_Szerepkor_FunkcioInsert]
		@Id=NULL,
		@Funkcio_Id=@FunkcioId,
		@Szerepkor_Id=@SzerepkorId,
		@ErvKezd=NULL,
		@ErvVege='4700-12-31 00:00:00',
		@Ver=0,@Note=NULL,
		@Stat_id=NULL,
		@Letrehozo_id=@ADMIN_ID,
		@LetrehozasIdo=@NOW,
		@Modosito_id=NULL,
		@ModositasIdo=NULL,
		@Zarolo_id=NULL,
		@ZarolasIdo=NULL,
		@Tranz_id=NULL,
		@UIAccessLog_id=NULL,
		@UpdatedColumns=@SzerepkorokColumns,
		@ResultUid=@ResultUid output
	SELECT @ResultUid
END
ELSE
	PRINT 'Already exists in KRT_Szerepkor_Funkcio'

GO