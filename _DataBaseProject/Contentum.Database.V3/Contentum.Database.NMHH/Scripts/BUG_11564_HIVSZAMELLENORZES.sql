------------------------------------------------------------------
-----------
--BUG 11564
-----------
GO
DECLARE @ORG UNIQUEIDENTIFIER
SET @ORG = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @ORG_KOD NVARCHAR(100) 
SET @ORG_KOD = (SELECT KOD FROM KRT_ORGOK WHERE ID=@ORG) 

DECLARE @RPARAM_HIVSZAM UNIQUEIDENTIFIER
SELECT @RPARAM_HIVSZAM = ID FROM KRT_Parameterek WHERE NEV = 'HIVSZAM_ELLENORZES'

IF (@org_kod = 'FPH' AND @RPARAM_HIVSZAM IS NOT NULL)  
		BEGIN
			 UPDATE KRT_Parameterek
			 SET Ertek = '1',
				 ModositasIdo = GETDATE()
			 WHERE Id=@RPARAM_HIVSZAM AND Ertek <> '1'
		END
GO
------------------------------------------------------------------