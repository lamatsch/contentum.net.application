﻿---------------------- BUG 11586

-- drop #Insert_Kodtar
IF OBJECT_ID('tempdb..#Insert_Kodtar') IS NOT NULL
BEGIN
    DROP PROC #Insert_Kodtar
END
GO

-- 
DECLARE @KODCSOPORT_ID_11586 uniqueidentifier = 'ECD56FB6-8558-4AE6-99AD-635C5F29F33E'
DECLARE @Letrehozo_Id uniqueidentifier = '54E861A5-36ED-44CA-BAA7-C287D125B309'
DECLARE @Org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @OrgKod nvarchar(100) = (select Kod from KRT_Orgok where Id = @Org)

-- create #Insert_Kodtar

DECLARE @sql NVARCHAR(2000)
SET @sql = N'
CREATE PROCEDURE #Insert_Kodtar
(  
   @kodtar_id uniqueidentifier,
   @kodtar_kod NVARCHAR(64), 
   @kodtar_nev NVARCHAR(400)  
)  
AS  
BEGIN  
	IF not exists (select 1 from KRT_KodTarak where Id=@kodtar_id)
	BEGIN				
		DECLARE @sorrend NVARCHAR(32)
		SET @sorrend = (SELECT COALESCE(MAX(CAST(Sorrend AS Int))+1,1) FROM krt_kodtarak where kodcsoport_id=@p_KcsId)
		INSERT INTO [dbo].[KRT_KodTarak]
		   ([Id]
		   ,[Org]
		   ,[KodCsoport_Id]
		   ,[Kod]
		   ,[Sorrend]
		   ,[Nev]
		   ,[Modosithato]
		   ,[Letrehozo_id])
		VALUES
		   (@kodtar_id, -- Id
			@p_Org, -- Org
			@p_KcsId, -- KodCsoport_Id
			@kodtar_kod,
			@sorrend, -- Sorrend
			@kodtar_nev, -- Nev
			1, -- Modosithato
			@p_Letrehozo -- Letrehozo_id
		);
	END
	ELSE
	BEGIN
		UPDATE [dbo].[KRT_KodTarak] SET [Nev]=@kodtar_nev, [Kod]=@kodtar_kod WHERE [Id]=@kodtar_id
	END
END
'

SET @sql = REPLACE(@sql, '@p_Org', '''' + CAST(@Org AS NVARCHAR(36)) + '''')
SET @sql = REPLACE(@sql, '@p_KcsId', '''' + CAST(@KODCSOPORT_ID_11586 AS NVARCHAR(36)) + '''')
SET @sql = REPLACE(@sql, '@p_Letrehozo', '''' + CAST(@Letrehozo_Id AS NVARCHAR(36)) + '''')

EXEC sp_executesql @sql

-- csak FPH-nál kell
IF @OrgKod = 'FPH'
BEGIN		
	-- Kódtárak
	BEGIN
		PRINT 'Kódtár rekordok felvétele a kódcsoportba: TERTI_VISSZA_KOD_POSTA'
		EXEC #Insert_Kodtar '1dfb5bd6-37f3-4e0c-8147-5a20f31c6f8f', '1C', 'Kézbesítve - Címzettnek';
		EXEC #Insert_Kodtar '333f3abf-ff89-450a-9d09-bbb2ef70c6fb', '1K', 'Kézbesítve - Közvetett kézbesítőnek';
		EXEC #Insert_Kodtar '440a46fe-0d38-406f-8f3c-82bcbbf6513a', '1M', 'Kézbesítve - Meghatalmazottnak';
		EXEC #Insert_Kodtar '1f94f365-174a-4842-9a7a-02cc4e0938cf', '1H', 'Kézbesítve - Helyettes átvevőnek';
		EXEC #Insert_Kodtar '7703bc72-39d8-4ea7-bd13-2b755e3eb9c4', '0C', 'Visszaküldött - Cím nem azonosítható';
		EXEC #Insert_Kodtar '68f64028-2fb8-448d-86e4-f1f300dd121d', '0I', 'Visszaküldött - Címzett ismeretlen';
		EXEC #Insert_Kodtar 'f9265841-e0bd-47d6-9caf-ddea5cf81309', '0A', 'Visszaküldött - Kézbesítés akadályozott';
		EXEC #Insert_Kodtar 'e0255206-3fc7-4a7a-9c60-6ea7f794e979', '0N', 'Visszaküldött - Nem kereste';
		EXEC #Insert_Kodtar 'b063089d-2aad-4642-98e5-cf7a2a6a364f', '0E', 'Visszaküldött - Elköltözött';
		EXEC #Insert_Kodtar 'c916b757-2c3d-4b70-a383-af4852d59880', '0M', 'Visszaküldött - Átvételt megtagadta';
		EXEC #Insert_Kodtar 'bd96dbd1-b206-49a0-997d-2538fb260601', '0B', 'Visszaküldött - Bejelentve: meghalt, megszűnt';
	END	
	
	-- Kódtár függőség beállítása
	UPDATE KRT_KodtarFuggoseg SET Adat='{"VezerloKodCsoportId":"ecd56fb6-8558-4ae6-99ad-635c5f29f33e","FuggoKodCsoportId":"0c4ac87c-84fc-4d20-a9f0-3fc519f4d9a1","Items":[{"VezerloKodTarId":"7f4442d7-3218-421c-be8d-c5be7969eb9b","FuggoKodtarId":"6f2f4e4e-d307-4ee4-9ca0-227c8b51b608","Aktiv":true},{"VezerloKodTarId":"0c87ea7b-547e-414a-a776-9c11befdb3a7","FuggoKodtarId":"70e0bd54-e891-49ec-a56e-cbb04438c7ef","Aktiv":true},{"VezerloKodTarId":"1dfb5bd6-37f3-4e0c-8147-5a20f31c6f8f","FuggoKodtarId":"6f2f4e4e-d307-4ee4-9ca0-227c8b51b608","Aktiv":true},{"VezerloKodTarId":"1f94f365-174a-4842-9a7a-02cc4e0938cf","FuggoKodtarId":"70e0bd54-e891-49ec-a56e-cbb04438c7ef","Aktiv":true},{"VezerloKodTarId":"333f3abf-ff89-450a-9d09-bbb2ef70c6fb","FuggoKodtarId":"70e0bd54-e891-49ec-a56e-cbb04438c7ef","Aktiv":true},{"VezerloKodTarId":"440a46fe-0d38-406f-8f3c-82bcbbf6513a","FuggoKodtarId":"70e0bd54-e891-49ec-a56e-cbb04438c7ef","Aktiv":true},{"VezerloKodTarId":"c25d41d8-778a-4588-9898-c7fe0be2e4a6","FuggoKodtarId":"70e0bd54-e891-49ec-a56e-cbb04438c7ef","Aktiv":true},{"VezerloKodTarId":"855a7bf1-ae91-4292-b034-1f75a1090353","FuggoKodtarId":"70e0bd54-e891-49ec-a56e-cbb04438c7ef","Aktiv":true},{"VezerloKodTarId":"a6768fb9-0984-4232-8d04-b354290c9f36","FuggoKodtarId":"2e9cbafd-805c-4519-927c-5c937f222667","Aktiv":true},{"VezerloKodTarId":"c916b757-2c3d-4b70-a383-af4852d59880","FuggoKodtarId":"2e9cbafd-805c-4519-927c-5c937f222667","Aktiv":true},{"VezerloKodTarId":"bd96dbd1-b206-49a0-997d-2538fb260601","FuggoKodtarId":"2e9cbafd-805c-4519-927c-5c937f222667","Aktiv":true},{"VezerloKodTarId":"7703bc72-39d8-4ea7-bd13-2b755e3eb9c4","FuggoKodtarId":"be44998f-83ec-45bf-a525-eca0d025db72","Aktiv":true},{"VezerloKodTarId":"68f64028-2fb8-448d-86e4-f1f300dd121d","FuggoKodtarId":"be44998f-83ec-45bf-a525-eca0d025db72","Aktiv":true},{"VezerloKodTarId":"b063089d-2aad-4642-98e5-cf7a2a6a364f","FuggoKodtarId":"be44998f-83ec-45bf-a525-eca0d025db72","Aktiv":true},{"VezerloKodTarId":"f9265841-e0bd-47d6-9caf-ddea5cf81309","FuggoKodtarId":"be44998f-83ec-45bf-a525-eca0d025db72","Aktiv":true},{"VezerloKodTarId":"e0255206-3fc7-4a7a-9c60-6ea7f794e979","FuggoKodtarId":"2e9cbafd-805c-4519-927c-5c937f222667","Aktiv":true}],"ItemsNincs":[]}' 
	WHERE Vezerlo_KodCsoport_Id='ECD56FB6-8558-4AE6-99AD-635C5F29F33E' AND Fuggo_KodCsoport_Id='0C4AC87C-84FC-4D20-A9F0-3FC519F4D9A1' 
	AND ErvKezd<=GetDate() AND GetDate()<=ErvVege
END
GO
---------------------- End of BUG 11586