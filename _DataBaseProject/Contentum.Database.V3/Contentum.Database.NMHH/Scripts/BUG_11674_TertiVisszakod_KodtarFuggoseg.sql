GO
IF NOT EXISTS ( SELECT 1 
				FROM KRT_KodtarFuggoseg 
				WHERE Vezerlo_KodCsoport_Id = 'ECD56FB6-8558-4AE6-99AD-635C5F29F33E' 
				  AND Fuggo_KodCsoport_Id   = '0C4AC87C-84FC-4D20-A9F0-3FC519F4D9A1'
				)
	BEGIN
		INSERT INTO KRT_KodtarFuggoseg (ID, Org,Vezerlo_KodCsoport_Id, Fuggo_KodCsoport_Id, Adat, Aktiv)
		VALUES('FCE10ED3-4DCD-4301-A8D9-51D619BF29C4', '450B510A-7CAA-46B0-83E3-18445C0C53A9',
			   'ECD56FB6-8558-4AE6-99AD-635C5F29F33E', '0C4AC87C-84FC-4D20-A9F0-3FC519F4D9A1',
			   '{"VezerloKodCsoportId":"ecd56fb6-8558-4ae6-99ad-635c5f29f33e","FuggoKodCsoportId":"0c4ac87c-84fc-4d20-a9f0-3fc519f4d9a1","Items":[{"VezerloKodTarId":"7f4442d7-3218-421c-be8d-c5be7969eb9b","FuggoKodtarId":"6f2f4e4e-d307-4ee4-9ca0-227c8b51b608","Aktiv":true},{"VezerloKodTarId":"0c87ea7b-547e-414a-a776-9c11befdb3a7","FuggoKodtarId":"70e0bd54-e891-49ec-a56e-cbb04438c7ef","Aktiv":true},{"VezerloKodTarId":"c25d41d8-778a-4588-9898-c7fe0be2e4a6","FuggoKodtarId":"70e0bd54-e891-49ec-a56e-cbb04438c7ef","Aktiv":true},{"VezerloKodTarId":"855a7bf1-ae91-4292-b034-1f75a1090353","FuggoKodtarId":"70e0bd54-e891-49ec-a56e-cbb04438c7ef","Aktiv":true},{"VezerloKodTarId":"a6768fb9-0984-4232-8d04-b354290c9f36","FuggoKodtarId":"2e9cbafd-805c-4519-927c-5c937f222667","Aktiv":true}],"ItemsNincs":[]}',
			   '1'
			  )
	END
GO