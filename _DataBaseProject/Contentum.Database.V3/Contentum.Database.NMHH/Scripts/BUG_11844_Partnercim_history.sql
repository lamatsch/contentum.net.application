  insert into KRT_CimekHistory(HistoryMuvelet_Id, HistoryVegrehajto_Id, HistoryVegrehajtasIdo, 
  Id, Org, Kategoria, Tipus, Nev, KulsoAzonositok, Forras, Orszag_Id, OrszagNev, Telepules_Id, TelepulesNev,
  IRSZ, CimTobbi, Kozterulet_Id, KozteruletNev, KozteruletTipusNev, 
  Hazszam, Hazszamig, HazszamBetujel, MindketOldal, HRSZ, Lepcsohaz, Szint, Ajto, AjtoBetujel, Tobbi, Note,
  Ver, ErvKezd, ErvVege, Letrehozo_id, LetrehozasIdo, Modosito_id, ModositasIdo)
   
  select 0, '54E861A5-36ED-44CA-BAA7-C287D125B309', GETDATE(), 
  Id, Org, Kategoria, Tipus, Nev, KulsoAzonositok, Forras, Orszag_Id, OrszagNev, Telepules_Id, TelepulesNev,
  IRSZ, CimTobbi, Kozterulet_Id, KozteruletNev, KozteruletTipusNev, 
  Hazszam, Hazszamig, HazszamBetujel, MindketOldal, HRSZ, Lepcsohaz, Szint, Ajto, AjtoBetujel, Tobbi, Note,
  Ver, ErvKezd, ErvVege, Letrehozo_id, LetrehozasIdo, Modosito_id, ModositasIdo 
  from KRT_Cimek c 
  where not exists (select HistoryId from KRT_CimekHistory ch where ch.Id=c.Id)