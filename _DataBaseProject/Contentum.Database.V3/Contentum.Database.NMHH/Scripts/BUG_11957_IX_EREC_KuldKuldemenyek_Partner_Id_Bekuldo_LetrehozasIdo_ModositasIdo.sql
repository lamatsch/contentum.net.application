if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_KuldKuldemenyek_LetrehozasIdo' AND object_id = OBJECT_ID('EREC_KuldKuldemenyek'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_EREC_KuldKuldemenyek_LetrehozasIdo] ON [dbo].[EREC_KuldKuldemenyek]
	(
		[LetrehozasIdo] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO

if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_KuldKuldemenyek_Partner_Id_Bekuldo_ModositasIdo_LetrehozasIdo' AND object_id = OBJECT_ID('EREC_KuldKuldemenyek'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_EREC_KuldKuldemenyek_Partner_Id_Bekuldo_ModositasIdo_LetrehozasIdo] ON [dbo].[EREC_KuldKuldemenyek]
	(
		[Partner_Id_Bekuldo] ASC
	)
	INCLUDE([ModositasIdo],[LetrehozasIdo]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO