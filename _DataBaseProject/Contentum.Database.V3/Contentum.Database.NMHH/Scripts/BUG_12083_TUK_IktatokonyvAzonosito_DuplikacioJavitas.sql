-- BUG_12083
-- Iktatókönyv azonosító duplikáció javító script

DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 
if  (@isTUK = '1')
BEGIN
	BEGIN TRAN
	-- select azonosito, COUNT(azonosito)
	-- from EREC_IraIktatoKonyvek
	-- where getdate() between ErvKezd and ErvVege
	-- group by Azonosito 
	-- having count(azonosito)>1
	-- order by azonosito--,

	DECLARE @maxazon int
	SELECT @maxazon = max(convert(int, azonosito)) FROM EREC_IraIktatoKonyvek 
						where getdate() between ErvKezd and ErvVege

	DECLARE iktkonyv_cursor CURSOR DYNAMIC
		FOR SELECT Azonosito FROM EREC_IraIktatoKonyvek
		where azonosito in
				(select azonosito
					from EREC_IraIktatoKonyvek
					where getdate() between ErvKezd and ErvVege
					group by Azonosito 
					having count(azonosito)>1) 
		order by azonosito
	FOR UPDATE OF Azonosito


	DECLARE @azonosito nvarchar(20) 
	DECLARE @last_azonosito nvarchar(20) 
	SET @last_azonosito=''
	OPEN iktkonyv_cursor  
	FETCH NEXT FROM iktkonyv_cursor INTO @azonosito

	DECLARE @change BIT
	WHILE @@FETCH_STATUS = 0
	  BEGIN
		  print '---'
		  print @last_azonosito
		  print @azonosito
		  IF (@last_azonosito <> @azonosito)
		  BEGIN
			print 'not change'
			SET @change = 0;
			SET @last_azonosito = @azonosito
		  END
		  ELSE
		  BEGIN
			print 'change'
			SET @change = 1
		  END

		  IF (@change = 1)
		  BEGIN
			  SET @maxazon=@maxazon+1
			  print convert(nvarchar(20), @maxazon)
			  declare @azonositoHossz int
				SET @azonositoHossz = IsNull(dbo.fn_GetKRT_ParameterekErtek('IKTATOKONYV_AZONOSITO_HOSSZ', '450B510A-7CAA-46B0-83E3-18445C0C53A9'),0)

				declare @len int 
				select @len = iif(@azonositoHossz  > len(@maxazon), @azonositoHossz-len(@maxazon), 0)

			  print replicate('0', @len) + convert(nvarchar(20),@maxazon)

			  UPDATE EREC_IraIktatoKonyvek
				SET azonosito =replicate('0', @len) + convert(nvarchar(20),@maxazon) 
				WHERE  CURRENT OF iktkonyv_cursor;
		  END
		  FETCH NEXT FROM iktkonyv_cursor INTO @azonosito

	  END;

	CLOSE iktkonyv_cursor;

	DEALLOCATE iktkonyv_cursor;  

	-- select azonosito, COUNT(azonosito)
	-- --max(convert(int,Azonosito)), max(azonosito) 
	-- from EREC_IraIktatoKonyvek
	-- --						--	UNION
	-- --						--select IktatoErkezteto, Azonosito, ErvKezd, ErvVege from #tmp_EREC_IktatoKonyvek_New
	-- where getdate() between ErvKezd and ErvVege
	-- --and utolsofoszam >0
	-- group by Azonosito 
	-- having count(azonosito)>1
	-- order by azonosito--,

	-- select * from EREC_IraIktatoKonyvek
	-- order by Azonosito 
	
	COMMIT TRAN
END
