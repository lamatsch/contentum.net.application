set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON

-- BUG_13667 Hibás azonosítók javítása 
-- Ügyirat

print 'BUG_13667 Hibás azonosítók javítása Ugyirat'
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

IF (@org_kod = 'FPH' OR @org_kod = 'BOPMH')
BEGIN
	Print 'FPH, BOPMH nincs javítandó'
	RETURN
END

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TEMP_UGYIRAT_AZONOSITO_UPDATES_13677]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[TEMP_UGYIRAT_AZONOSITO_UPDATES_13677](
		[Ugyirat_Id] [uniqueidentifier],
		[Azonosito] [nvarchar](100) null, 
		[KalkulaltAzonosito] [nvarchar](100) null,
--		[LetrehozasiIdo] [datetime] NULL, 
		[ModositasiIdo] [datetime] NULL
	)
END

DECLARE @KalkulaltAzonosito nvarchar(200)
IF (@org_kod = 'CSPH')
BEGIN
	SET @KalkulaltAzonosito = 'konyv.Iktatohely + '' /'' + convert (varchar, ugyirat.Foszam) + '' /'' + convert (varchar, konyv.Ev)'
END

DECLARE @sqlcmd nvarchar(4000)

SET @sqlcmd ='UPDATE EREC_UgyUgyiratok 
SET Azonosito = ' + @KalkulaltAzonosito +'
OUTPUT inserted.id,  
       deleted.Azonosito,  
       inserted.Azonosito,  
	   Getdate()  	   
INTO [dbo].[TEMP_UGYIRAT_AZONOSITO_UPDATES_13677]
from EREC_UgyUgyiratok as ugyirat
		inner join EREC_IraIktatoKonyvek as konyv on ugyirat.IraIktatokonyv_Id = konyv.Id
	where konyv.Ev = ''2020'' and
         (ugyirat.Azonosito <>' + @KalkulaltAzonosito+')'

exec sp_executesql @sqlcmd;


