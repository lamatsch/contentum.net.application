DECLARE @ORG_ID nvarchar(100) 
SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @ORG_KOD nvarchar(100) 
SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 

IF @ORG_KOD = 'NMHH' 
		BEGIN
				DECLARE @IraIratAlairoModifyId  NVARCHAR(100)
				DECLARE @IraIratAlairoModifyName  NVARCHAR(100) 
				SET @IraIratAlairoModifyName = 'IraIratAlairoModify'
				SET @IraIratAlairoModifyId = (select Id from KRT_Funkciok where Kod  = @IraIratAlairoModifyName)

				UPDATE KRT_Funkciok SET FeladatJelzo = '0' WHERE Kod = @IraIratAlairoModifyName
				UPDATE EREC_FeladatDefinicio SET ErvVege = GETDATE() where Funkcio_Id_Kivalto = @IraIratAlairoModifyId
		END

