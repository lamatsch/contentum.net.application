﻿------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- BUG_4785: IRATTAROZAS_HELY_FELH_ALTAL paraméter
-- Ha a paraméter értéke 1, akkor az átmeneti irattár és az átmeneti -> központi irattár átadásnál az irattári hely kiválasztása a felhasználóra van bízva.
-- Ha a paraméter értéke 0, akkor az átmeneti irattáros csak a szervezeti egységéhez tartozó átmeneti irattárba küldött tételeket láthatja, a központi irattáros csak olyan fizikai helyre teheti az ügyiratokat, amelyhez van jogosultsága.

DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = '5D6329CE-9EA9-4965-8B87-B47A8F15D806'

IF @org_kod = 'NMHH'
set @ertek = '0'
else
set @ertek = '1'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - IRATTAROZAS_HELY_FELH_ALTAL  - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'IRATTAROZAS_HELY_FELH_ALTAL'
		,@ertek
		,'1'
		,'Ha a paraméter értéke 1, akkor az átmeneti irattár és az átmeneti -> központi irattár átadásnál az irattári hely kiválasztása a felhasználóra van bízva. Ha a paraméter értéke 0, akkor az átmeneti irattáros csak a szervezeti egységéhez tartozó átmeneti irattárba küldött tételeket láthatja, a központi irattáros csak olyan fizikai helyre teheti az ügyiratokat, amelyhez van jogosultsága.'
		); 
 END
 
-------
-- create #Insert_IrattariHelyek

DECLARE @sql NVARCHAR(2000)
SET @sql = N'
CREATE PROCEDURE #Insert_IrattariHelyek
(  
   @IrattariHelyek_id uniqueidentifier,
   @IrattariHelyek_nev NVARCHAR(100),
   @IrattariHelyek_SzuloId uniqueidentifier,
   @IrattariHelyek_Felelos_Csoport_Id uniqueidentifier
)  
AS  
BEGIN  
	IF not exists (select 1 from EREC_IrattariHelyek where Id=@IrattariHelyek_id)
	BEGIN
	
		DECLARE @org uniqueidentifier
		SET	@org=''450B510A-7CAA-46B0-83E3-18445C0C53A9''

		DECLARE	@return_value int,
			@ResultUid uniqueidentifier,
			@VonalKod nvarchar(100)

		EXEC	@return_value = [dbo].[sp_BarkodSav_Igenyles]
				@Szervezet_Id = ''54E861A5-36ED-44CA-BAA7-C287D125B309'',
				@SavType = N''G'',
				@FoglalandoDarab = 1,
				@ResultUid = @ResultUid OUTPUT

		select @VonalKod=Kod from KRT_Barkodok
		where Id=@ResultUid

				INSERT INTO [dbo].[EREC_IrattariHelyek]
				   ([Id]
				   ,[Ertek]
				   ,[Nev]
				   ,[Vonalkod]
				   ,[SzuloId]
				   ,Felelos_Csoport_Id
				   )
				VALUES
				   (@IrattariHelyek_id,
					@IrattariHelyek_nev,
					@IrattariHelyek_nev,
					@VonalKod,
					@IrattariHelyek_SzuloId,
					@IrattariHelyek_Felelos_Csoport_Id
				);

				Update KRT_Barkodok
		  SET Obj_type=''EREC_IrattariHelyek'',Allapot=''F'',Obj_Id=@IrattariHelyek_id
		  where Id=@ResultUid

	END
	ELSE BEGIN
		UPDATE EREC_IrattariHelyek
			SET Ertek = @IrattariHelyek_nev,
				Nev = @IrattariHelyek_nev,
			    SzuloId = @IrattariHelyek_SzuloId,
				Felelos_Csoport_Id = @IrattariHelyek_Felelos_Csoport_Id,
				ErvVege = ''4700-12-31''
		WHERE Id = @IrattariHelyek_id 
	END
END
'
EXEC sp_executesql @sql

set @sql = N'
CREATE PROCEDURE #Insert_IrattariHely_Jogosult
(  
   @irattariHely_id uniqueidentifier,
   @csoport_nev nvarchar(400)
)  
AS  
BEGIN
	declare @csoport_id uniqueidentifier
	set @csoport_id = (select top 1 Id from KRT_Csoportok
	where Nev = @csoport_nev
	and getdate() between ErvKezd And ErvVege)

	if @csoport_id is null
	BEGIN
		print ''Csoport nem talalhato: '' + @csoport_nev
	END
	ELSE 
	BEGIN
		IF NOT EXISTS(select 1 from KRT_Jogosultak where Csoport_Id_Jogalany = @csoport_id and Obj_Id = @irattariHely_id)
		BEGIN
			insert into KRT_Jogosultak
			(Csoport_Id_Jogalany, Jogszint, Obj_Id, Orokolheto, Kezi, Tipus)
			select @csoport_id,''I'',@irattariHely_id,''0'',''I'',''0''
		END
	END
END
'

EXEC sp_executesql @sql


-------

IF @org_kod = 'NMHH'
BEGIN	

	-- központi irattárak létrehozása
	DECLARE @kozpontiIrattar uniqueidentifier = 'BEA1065D-C91D-E811-80C6-00155D027EA9'
	DECLARE @felelosCsoportId uniqueidentifier = (select Obj_Id from KRT_KodTarak where Kod = 'SPEC_SZERVEK.KOZPONTIIRATTAR')
	EXEC #Insert_IrattariHelyek 'BBD122F0-C81D-E811-80C6-00155D027EA9', 'Budapest Ostrom', @kozpontiIrattar, @felelosCsoportId
	EXEC #Insert_IrattariHelyek 'C2F779E5-511D-E811-80C6-00155D027EA9', 'Budapest Visegrádi', @kozpontiIrattar, @felelosCsoportId
	EXEC #Insert_IrattariHelyek 'ED8B9E08-C91D-E811-80C6-00155D027EA9', 'Budapest Reviczky', @kozpontiIrattar, @felelosCsoportId
	EXEC #Insert_IrattariHelyek '41D47853-9346-48FF-B4C3-EB861E514E5B', 'Sopron', @kozpontiIrattar, @felelosCsoportId
    EXEC #Insert_IrattariHelyek '0F124237-CA74-4013-83E2-31F596468B27', 'Miskolc', @kozpontiIrattar, @felelosCsoportId
	EXEC #Insert_IrattariHelyek '160A7F47-B8D9-4B40-8433-142DE7A12CC8', 'Debrecen', @kozpontiIrattar, @felelosCsoportId
	EXEC #Insert_IrattariHelyek '1FED8E71-5234-4085-82EF-B4A3A146FE92', 'Pécs', @kozpontiIrattar, @felelosCsoportId
	EXEC #Insert_IrattariHelyek '2BF7BEF5-FB38-4493-AF26-00C6945BA512', 'Szeged', @kozpontiIrattar, @felelosCsoportId

	declare @irattariHely_id uniqueidentifier
	set @irattariHely_id = 'BBD122F0-C81D-E811-80C6-00155D027EA9' --Központi irattár - Budapest Ostrom
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Majorosné Filák Judit'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Baranyi-Tóth Zsuzsanna KI'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Darabos Andrea'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Lukács Barbara'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Oláh Barbara'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Ujszászi-Vámos Aranka'

	set @irattariHely_id = 'C2F779E5-511D-E811-80C6-00155D027EA9' --'Központi irattár - Budapest Visegrádi
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Lukács Barbara'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Kovács Tímea'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Szabóné Kiss Márta'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Forczekné Makkai Judit'

	set @irattariHely_id = 'ED8B9E08-C91D-E811-80C6-00155D027EA9' --'Központi irattár - Budapest Reviczky'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Oláhné Jokán Ildikó'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Grósz Anna'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Lukács Barbara'

	set @irattariHely_id = '41D47853-9346-48FF-B4C3-EB861E514E5B' --'Központi irattár - Sopron'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Prajczerné Nagy Anikó'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Tamási Tímea'

	set @irattariHely_id = '0F124237-CA74-4013-83E2-31F596468B27' --'Központi irattár - Miskolc'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Pálkovács Andrea'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Tóth Zsanett'

	set @irattariHely_id = '160A7F47-B8D9-4B40-8433-142DE7A12CC8' --'Központi irattár - Debrecen'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Takács D. Orsolya'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Németh Anna'

	set @irattariHely_id = '1FED8E71-5234-4085-82EF-B4A3A146FE92' --'Központi irattár - Pécs'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Somogyvári-Jech Marina'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Zombory Eszter'

	set @irattariHely_id = '2BF7BEF5-FB38-4493-AF26-00C6945BA512' --'Központi irattár - Szeged'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Dózsa Ágnes'
	exec #Insert_IrattariHely_Jogosult @irattariHely_id, 'Solymosi Nóra'

END

DROP PROCEDURE #Insert_IrattariHelyek
DROP PROCEDURE #Insert_IrattariHely_Jogosult