CREATE PROCEDURE #Insert_Template
(  
     @record_id uniqueidentifier,
     @felhasznaloNev nvarchar(100),
     @irattarak nvarchar(4000)
)  
AS  
BEGIN  
	declare @irattariHelyek nvarchar(4000)
	declare @felhasznaloCsoport_Id_Orzo nvarchar(4000)

	create table #irattarak_table(id uniqueidentifier, nev nvarchar(400))

	declare @sqlcmd nvarchar(max)
	set @sqlcmd = 'insert into #irattarak_table select Id, Nev from KRT_Csoportok where Nev in (' + @irattarak + ')'
	EXEC sp_executesql @sqlcmd

	set @irattariHelyek =  STUFF((select ', ' + Nev from #irattarak_table FOR XML PATH('')),1,2,'')
	set @felhasznaloCsoport_Id_Orzo =  STUFF((select ',''' + cast(Id as nvarchar(36)) + '''' from #irattarak_table FOR XML PATH('')),1,1,'')

	drop table #irattarak_table


	declare @templateTipusNev nvarchar(100)
	set @templateTipusNev = 'EREC_UgyUgyiratokSearch_KozpontiIrattarbaVetel'

	declare @nev  nvarchar(100)
	set @nev = '�tmeneti iratt�r'

	declare @templateXML nvarchar(max)
	set @templateXML = '<EREC_UgyUgyiratokSearch>
	  <ReadableWhere>Irat Helye: #IratHelye#</ReadableWhere>
	  <Extended_EREC_KuldKuldemenyekSearch>
		<ReadableWhere />
		<WhereByManual />
		<OrderBy />
		<TopRow>0</TopRow>
		<Manual_ModositasIdo>
		  <Name>EREC_KuldKuldemenyek.ModositasIdo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_ModositasIdo>
		<Manual_Sajat>
		  <Name>EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Sajat>
		<Manual_Iktathatok_Allapot>
		  <Name>EREC_KuldKuldemenyek.Allapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Iktathatok_Allapot>
		<Manual_Iktathatok_IktatniKell>
		  <Name>EREC_KuldKuldemenyek.IktatniKell</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Iktathatok_IktatniKell>
		<Manual_Sztornozhatok>
		  <Name>EREC_KuldKuldemenyek.Allapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Sztornozhatok>
		<Manual_Iktatottak>
		  <Name>EREC_KuldKuldemenyek.Allapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Iktatottak>
		<Manual_Lezartak>
		  <Name>EREC_KuldKuldemenyek.Allapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Lezartak>
		<Manual_Sajat_Allapot>
		  <Name>EREC_KuldKuldemenyek.Allapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Sajat_Allapot>
		<Manual_Sajat_TovabbitasAlattAllapot>
		  <Name>EREC_KuldKuldemenyek.TovabbitasAlattAllapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Sajat_TovabbitasAlattAllapot>
		<Manual_Feljegyzes_Altipus>
		  <Name>EREC_HataridosFeladatok.Altipus</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Feljegyzes_Altipus>
		<Manual_Feljegyzes_Leiras>
		  <Name>EREC_HataridosFeladatok.Leiras</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Feljegyzes_Leiras>
		<Manual_PostazasIranya_DefaultFilter>
		  <Name>EREC_KuldKuldemenyek.PostazasIranya</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_PostazasIranya_DefaultFilter>
		<Note>
		  <Name>EREC_KuldKuldemenyek.Note</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Note>
		<Manual_Erkezteto_Id>
		  <Name>EREC_KuldKuldemenyek.Letrehozo_id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Erkezteto_Id>
		<Manual_Csatolhato>
		  <Name>EREC_KuldKuldemenyek.Allapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Csatolhato>
		<Manual_ErkeztetesIdeje>
		  <Name>EREC_KuldKuldemenyek.LetrehozasIdo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_ErkeztetesIdeje>
		<Id>
		  <Name>EREC_KuldKuldemenyek.Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Id>
		<Allapot>
		  <Name>EREC_KuldKuldemenyek.Allapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Allapot>
		<BeerkezesIdeje>
		  <Name>EREC_KuldKuldemenyek.BeerkezesIdeje</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</BeerkezesIdeje>
		<FelbontasDatuma>
		  <Name>EREC_KuldKuldemenyek.FelbontasDatuma</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelbontasDatuma>
		<IraIktatokonyv_Id>
		  <Name>EREC_KuldKuldemenyek.IraIktatokonyv_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IraIktatokonyv_Id>
		<KuldesMod>
		  <Name>EREC_KuldKuldemenyek.KuldesMod</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KuldesMod>
		<Erkezteto_Szam>
		  <Name>EREC_KuldKuldemenyek.Erkezteto_Szam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Erkezteto_Szam>
		<HivatkozasiSzam>
		  <Name>EREC_KuldKuldemenyek.HivatkozasiSzam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</HivatkozasiSzam>
		<Targy>
		  <Name>EREC_KuldKuldemenyek.Targy</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>FTSString</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Targy>
		<Tartalom>
		  <Name>EREC_KuldKuldemenyek.Tartalom</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Tartalom>
		<RagSzam>
		  <Name>EREC_KuldKuldemenyek.RagSzam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</RagSzam>
		<Surgosseg>
		  <Name>EREC_KuldKuldemenyek.Surgosseg</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Surgosseg>
		<BelyegzoDatuma>
		  <Name>EREC_KuldKuldemenyek.BelyegzoDatuma</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</BelyegzoDatuma>
		<UgyintezesModja>
		  <Name>EREC_KuldKuldemenyek.UgyintezesModja</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UgyintezesModja>
		<PostazasIranya>
		  <Name>EREC_KuldKuldemenyek.PostazasIranya</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</PostazasIranya>
		<Tovabbito>
		  <Name>EREC_KuldKuldemenyek.Tovabbito</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Tovabbito>
		<PeldanySzam>
		  <Name>EREC_KuldKuldemenyek.PeldanySzam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</PeldanySzam>
		<IktatniKell>
		  <Name>EREC_KuldKuldemenyek.IktatniKell</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IktatniKell>
		<Iktathato>
		  <Name>EREC_KuldKuldemenyek.Iktathato</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Iktathato>
		<SztornirozasDat>
		  <Name>EREC_KuldKuldemenyek.SztornirozasDat</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</SztornirozasDat>
		<KuldKuldemeny_Id_Szulo>
		  <Name>EREC_KuldKuldemenyek.KuldKuldemeny_Id_Szulo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KuldKuldemeny_Id_Szulo>
		<Erkeztetes_Ev>
		  <Name>EREC_KuldKuldemenyek.Erkeztetes_Ev</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Erkeztetes_Ev>
		<Csoport_Id_Cimzett>
		  <Name>EREC_KuldKuldemenyek.Csoport_Id_Cimzett</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Csoport_Id_Cimzett>
		<Csoport_Id_Felelos>
		  <Name>EREC_KuldKuldemenyek.Csoport_Id_Felelos</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Csoport_Id_Felelos>
		<FelhasznaloCsoport_Id_Expedial>
		  <Name>EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Expedial</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Expedial>
		<ExpedialasIdeje>
		  <Name>EREC_KuldKuldemenyek.ExpedialasIdeje</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ExpedialasIdeje>
		<FelhasznaloCsoport_Id_Orzo>
		  <Name>EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Orzo>
		<FelhasznaloCsoport_Id_Atvevo>
		  <Name>EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Atvevo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Atvevo>
		<Partner_Id_Bekuldo>
		  <Name>EREC_KuldKuldemenyek.Partner_Id_Bekuldo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Partner_Id_Bekuldo>
		<Cim_Id>
		  <Name>EREC_KuldKuldemenyek.Cim_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Cim_Id>
		<CimSTR_Bekuldo>
		  <Name>EREC_KuldKuldemenyek.CimSTR_Bekuldo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</CimSTR_Bekuldo>
		<NevSTR_Bekuldo>
		  <Name>EREC_KuldKuldemenyek.NevSTR_Bekuldo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>FTSString</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</NevSTR_Bekuldo>
		<AdathordozoTipusa>
		  <Name>EREC_KuldKuldemenyek.AdathordozoTipusa</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</AdathordozoTipusa>
		<ElsodlegesAdathordozoTipusa>
		  <Name>EREC_KuldKuldemenyek.ElsodlegesAdathordozoTipusa</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ElsodlegesAdathordozoTipusa>
		<FelhasznaloCsoport_Id_Alairo>
		  <Name>EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Alairo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Alairo>
		<BarCode>
		  <Name>EREC_KuldKuldemenyek.BarCode</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</BarCode>
		<FelhasznaloCsoport_Id_Bonto>
		  <Name>EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Bonto>
		<CsoportFelelosEloszto_Id>
		  <Name>EREC_KuldKuldemenyek.CsoportFelelosEloszto_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</CsoportFelelosEloszto_Id>
		<Csoport_Id_Felelos_Elozo>
		  <Name>EREC_KuldKuldemenyek.Csoport_Id_Felelos_Elozo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Csoport_Id_Felelos_Elozo>
		<Kovetkezo_Felelos_Id>
		  <Name>EREC_KuldKuldemenyek.Kovetkezo_Felelos_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Kovetkezo_Felelos_Id>
		<Elektronikus_Kezbesitesi_Allap>
		  <Name>EREC_KuldKuldemenyek.Elektronikus_Kezbesitesi_Allap</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Elektronikus_Kezbesitesi_Allap>
		<Kovetkezo_Orzo_Id>
		  <Name>EREC_KuldKuldemenyek.Kovetkezo_Orzo_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Kovetkezo_Orzo_Id>
		<Fizikai_Kezbesitesi_Allapot>
		  <Name>EREC_KuldKuldemenyek.Fizikai_Kezbesitesi_Allapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Fizikai_Kezbesitesi_Allapot>
		<IraIratok_Id>
		  <Name>EREC_KuldKuldemenyek.IraIratok_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IraIratok_Id>
		<BontasiMegjegyzes>
		  <Name>EREC_KuldKuldemenyek.BontasiMegjegyzes</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</BontasiMegjegyzes>
		<Tipus>
		  <Name>EREC_KuldKuldemenyek.Tipus</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Tipus>
		<Minosites>
		  <Name>EREC_KuldKuldemenyek.Minosites</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Minosites>
		<MegtagadasIndoka>
		  <Name>EREC_KuldKuldemenyek.MegtagadasIndoka</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</MegtagadasIndoka>
		<Megtagado_Id>
		  <Name>EREC_KuldKuldemenyek.Megtagado_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Megtagado_Id>
		<MegtagadasDat>
		  <Name>EREC_KuldKuldemenyek.MegtagadasDat</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</MegtagadasDat>
		<KimenoKuldemenyFajta>
		  <Name>EREC_KuldKuldemenyek.KimenoKuldemenyFajta</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KimenoKuldemenyFajta>
		<Elsobbsegi>
		  <Name>EREC_KuldKuldemenyek.Elsobbsegi</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Elsobbsegi>
		<Ajanlott>
		  <Name>EREC_KuldKuldemenyek.Ajanlott</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Ajanlott>
		<Tertiveveny>
		  <Name>EREC_KuldKuldemenyek.Tertiveveny</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Tertiveveny>
		<SajatKezbe>
		  <Name>EREC_KuldKuldemenyek.SajatKezbe</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</SajatKezbe>
		<E_ertesites>
		  <Name>EREC_KuldKuldemenyek.E_ertesites</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</E_ertesites>
		<E_elorejelzes>
		  <Name>EREC_KuldKuldemenyek.E_elorejelzes</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</E_elorejelzes>
		<PostaiLezaroSzolgalat>
		  <Name>EREC_KuldKuldemenyek.PostaiLezaroSzolgalat</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</PostaiLezaroSzolgalat>
		<Ar>
		  <Name>EREC_KuldKuldemenyek.Ar</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Ar>
		<KimenoKuld_Sorszam>
		  <Name>EREC_KuldKuldemenyek.KimenoKuld_Sorszam </Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KimenoKuld_Sorszam>
		<IrattarId>
		  <Name>EREC_KuldKuldemenyek.IrattarId</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IrattarId>
		<IrattariHely>
		  <Name>EREC_KuldKuldemenyek.IrattariHely</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IrattariHely>
		<TovabbitasAlattAllapot>
		  <Name>EREC_KuldKuldemenyek.TovabbitasAlattAllapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</TovabbitasAlattAllapot>
		<Azonosito>
		  <Name>EREC_KuldKuldemenyek.Azonosito</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Azonosito>
		<BoritoTipus>
		  <Name>EREC_KuldKuldemenyek.BoritoTipus</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</BoritoTipus>
		<MegorzesJelzo>
		  <Name>EREC_KuldKuldemenyek.MegorzesJelzo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</MegorzesJelzo>
		<KulsoAzonosito>
		  <Name>EREC_KuldKuldemenyek.KulsoAzonosito</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KulsoAzonosito>
		<ErvKezd>
		  <Name>EREC_KuldKuldemenyek.ErvKezd</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ErvKezd>
		<ErvVege>
		  <Name>EREC_KuldKuldemenyek.ErvVege</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ErvVege>
		<IktatastNemIgenyel>
		  <Name>EREC_KuldKuldemenyek.IktatastNemIgenyel</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IktatastNemIgenyel>
		<KezbesitesModja>
		  <Name>EREC_KuldKuldemenyek.KezbesitesModja</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KezbesitesModja>
		<Munkaallomas>
		  <Name>EREC_KuldKuldemenyek.Munkaallomas</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Munkaallomas>
		<SerultKuldemeny>
		  <Name>EREC_KuldKuldemenyek.SerultKuldemeny</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</SerultKuldemeny>
		<TevesCimzes>
		  <Name>EREC_KuldKuldemenyek.TevesCimzes</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</TevesCimzes>
		<TevesErkeztetes>
		  <Name>EREC_KuldKuldemenyek.TevesErkeztetes</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</TevesErkeztetes>
		<CimzesTipusa>
		  <Name>EREC_KuldKuldemenyek.CimzesTipusa</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</CimzesTipusa>
		<FutarJegyzekListaSzama>
		  <Name>EREC_KuldKuldemenyek.FutarJegyzekListaSzama</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FutarJegyzekListaSzama>
		<KezbesitoAtvetelIdopontja>
		  <Name>EREC_KuldKuldemenyek.KezbesitoAtvetelIdopontja</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KezbesitoAtvetelIdopontja>
		<KezbesitoAtvevoNeve>
		  <Name>EREC_KuldKuldemenyek.KezbesitoAtvevoNeve</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KezbesitoAtvevoNeve>
		<Minosito>
		  <Name>EREC_KuldKuldemenyek.Minosito</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Minosito>
		<MinositesErvenyessegiIdeje>
		  <Name>EREC_KuldKuldemenyek.MinositesErvenyessegiIdeje</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</MinositesErvenyessegiIdeje>
		<TerjedelemMennyiseg>
		  <Name>EREC_KuldKuldemenyek.TerjedelemMennyiseg</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</TerjedelemMennyiseg>
		<TerjedelemMennyisegiEgyseg>
		  <Name>EREC_KuldKuldemenyek.TerjedelemMennyisegiEgyseg</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</TerjedelemMennyisegiEgyseg>
		<TerjedelemMegjegyzes>
		  <Name>EREC_KuldKuldemenyek.TerjedelemMegjegyzes</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</TerjedelemMegjegyzes>
		<ModositasErvenyessegKezdete>
		  <Name>EREC_KuldKuldemenyek.ModositasErvenyessegKezdete</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ModositasErvenyessegKezdete>
		<MegszuntetoHatarozat>
		  <Name>EREC_KuldKuldemenyek.MegszuntetoHatarozat</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</MegszuntetoHatarozat>
		<FelulvizsgalatDatuma>
		  <Name>EREC_KuldKuldemenyek.FelulvizsgalatDatuma</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelulvizsgalatDatuma>
		<Felulvizsgalo_id>
		  <Name>EREC_KuldKuldemenyek.Felulvizsgalo_id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Felulvizsgalo_id>
		<MinositesFelulvizsgalatEredm>
		  <Name>EREC_KuldKuldemenyek.MinositesFelulvizsgalatEredm</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</MinositesFelulvizsgalatEredm>
		<MinositoSzervezet>
		  <Name>EREC_KuldKuldemenyek.MinositoSzervezet</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</MinositoSzervezet>
		<Partner_Id_BekuldoKapcsolt>
		  <Name>EREC_KuldKuldemenyek.Partner_Id_BekuldoKapcsolt</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Partner_Id_BekuldoKapcsolt>
		<MinositesFelulvizsgalatBizTag>
		  <Name>EREC_KuldKuldemenyek.MinositesFelulvizsgalatBizTag</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</MinositesFelulvizsgalatBizTag>
		<Erteknyilvanitas>
		  <Name>EREC_KuldKuldemenyek.Erteknyilvanitas</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Erteknyilvanitas>
		<ErteknyilvanitasOsszege>
		  <Name>EREC_KuldKuldemenyek.ErteknyilvanitasOsszege</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ErteknyilvanitasOsszege>
	  </Extended_EREC_KuldKuldemenyekSearch>
	  <Extended_EREC_UgyUgyiratdarabokSearch>
		<ReadableWhere />
		<WhereByManual />
		<OrderBy />
		<TopRow>0</TopRow>
		<Id>
		  <Name>EREC_UgyUgyiratdarabok.Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Id>
		<UgyUgyirat_Id>
		  <Name>EREC_UgyUgyiratdarabok.UgyUgyirat_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UgyUgyirat_Id>
		<EljarasiSzakasz>
		  <Name>EREC_UgyUgyiratdarabok.EljarasiSzakasz</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</EljarasiSzakasz>
		<Sorszam>
		  <Name>EREC_UgyUgyiratdarabok.Sorszam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Sorszam>
		<Azonosito>
		  <Name>EREC_UgyUgyiratdarabok.Azonosito</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Azonosito>
		<Hatarido>
		  <Name>EREC_UgyUgyiratdarabok.Hatarido</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Hatarido>
		<FelhasznaloCsoport_Id_Ugyintez>
		  <Name>EREC_UgyUgyiratdarabok.FelhasznaloCsoport_Id_Ugyintez</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Ugyintez>
		<ElintezesDat>
		  <Name>EREC_UgyUgyiratdarabok.ElintezesDat</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ElintezesDat>
		<LezarasDat>
		  <Name>EREC_UgyUgyiratdarabok.LezarasDat</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</LezarasDat>
		<ElintezesMod>
		  <Name>EREC_UgyUgyiratdarabok.ElintezesMod</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ElintezesMod>
		<LezarasOka>
		  <Name>EREC_UgyUgyiratdarabok.LezarasOka</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</LezarasOka>
		<Leiras>
		  <Name>EREC_UgyUgyiratdarabok.Leiras</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Leiras>
		<UgyUgyirat_Id_Elozo>
		  <Name>EREC_UgyUgyiratdarabok.UgyUgyirat_Id_Elozo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UgyUgyirat_Id_Elozo>
		<IraIktatokonyv_Id>
		  <Name>EREC_UgyUgyiratdarabok.IraIktatokonyv_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IraIktatokonyv_Id>
		<Foszam>
		  <Name>EREC_UgyUgyiratdarabok.Foszam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Foszam>
		<UtolsoAlszam>
		  <Name>EREC_UgyUgyiratdarabok.UtolsoAlszam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UtolsoAlszam>
		<Csoport_Id_Felelos>
		  <Name>EREC_UgyUgyiratdarabok.Csoport_Id_Felelos</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Csoport_Id_Felelos>
		<Csoport_Id_Felelos_Elozo>
		  <Name>EREC_UgyUgyiratdarabok.Csoport_Id_Felelos_Elozo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Csoport_Id_Felelos_Elozo>
		<IratMetadefinicio_Id>
		  <Name>EREC_UgyUgyiratdarabok.IratMetadefinicio_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IratMetadefinicio_Id>
		<Allapot>
		  <Name>EREC_UgyUgyiratdarabok.Allapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Allapot>
		<ErvKezd>
		  <Name>EREC_UgyUgyiratdarabok.ErvKezd</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ErvKezd>
		<ErvVege>
		  <Name>EREC_UgyUgyiratdarabok.ErvVege</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ErvVege>
	  </Extended_EREC_UgyUgyiratdarabokSearch>
	  <Extended_EREC_IraIratokSearch>
		<ReadableWhere />
		<ObjektumTargyszavai_ObjIdFilter />
		<WhereByManual />
		<OrderBy />
		<TopRow>0</TopRow>
		<Manual_Allapot_Filter>
		  <Name>EREC_IraIratok.Allapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Allapot_Filter>
		<Manual_LetrehozasIdo>
		  <Name>EREC_IraIratok.LetrehozasIdo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_LetrehozasIdo>
		<Manual_ModositasIdo>
		  <Name>EREC_IraIratok.ModositasIdo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_ModositasIdo>
		<Manual_Csatolhato>
		  <Name>EREC_IraIratok.Allapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Csatolhato>
		<Manual_Alszam_MunkaanyagFilter>
		  <Name>EREC_IraIratok.Alszam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Alszam_MunkaanyagFilter>
		<Manual_UgyiratFoszam_MunkaanyagFilter>
		  <Name>EREC_UgyUgyiratok.Foszam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_UgyiratFoszam_MunkaanyagFilter>
		<Manual_IktatasDatuma_Munkanap>
		  <Name>EREC_IraIratok.IktatasDatuma</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Munkanap</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_IktatasDatuma_Munkanap>
		<Manual_Alszam_Sztornozott>
		  <Name>EREC_IraIratok.Allapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Alszam_Sztornozott>
		<IsUgyiratTerkepQuery>false</IsUgyiratTerkepQuery>
		<Csoporttagokkal>true</Csoporttagokkal>
		<CsakAktivIrat>false</CsakAktivIrat>
		<CsatolmanySzures>
		  <Name />
		  <Operator />
		  <Value>0</Value>
		  <ValueTo />
		  <Type />
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</CsatolmanySzures>
		<Id>
		  <Name>EREC_IraIratok.Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Id>
		<PostazasIranya>
		  <Name>EREC_IraIratok.PostazasIranya</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</PostazasIranya>
		<Alszam>
		  <Name>EREC_IraIratok.Alszam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Alszam>
		<Sorszam>
		  <Name>EREC_IraIratok.Sorszam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Sorszam>
		<UtolsoSorszam>
		  <Name>EREC_IraIratok.UtolsoSorszam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UtolsoSorszam>
		<UgyUgyIratDarab_Id>
		  <Name>EREC_IraIratok.UgyUgyIratDarab_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UgyUgyIratDarab_Id>
		<Kategoria>
		  <Name>EREC_IraIratok.Kategoria</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Kategoria>
		<HivatkozasiSzam>
		  <Name>EREC_IraIratok.HivatkozasiSzam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</HivatkozasiSzam>
		<IktatasDatuma>
		  <Name>EREC_IraIratok.IktatasDatuma</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IktatasDatuma>
		<ExpedialasDatuma>
		  <Name>EREC_IraIratok.ExpedialasDatuma</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ExpedialasDatuma>
		<ExpedialasModja>
		  <Name>EREC_IraIratok.ExpedialasModja</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ExpedialasModja>
		<FelhasznaloCsoport_Id_Expedial>
		  <Name>EREC_IraIratok.FelhasznaloCsoport_Id_Expedial</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Expedial>
		<Targy>
		  <Name>EREC_IraIratok.Targy</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>FTSString</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Targy>
		<Jelleg>
		  <Name>EREC_IraIratok.Jelleg</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Jelleg>
		<SztornirozasDat>
		  <Name>EREC_IraIratok.SztornirozasDat</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</SztornirozasDat>
		<FelhasznaloCsoport_Id_Iktato>
		  <Name>EREC_IraIratok.FelhasznaloCsoport_Id_Iktato</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Iktato>
		<FelhasznaloCsoport_Id_Kiadmany>
		  <Name>EREC_IraIratok.FelhasznaloCsoport_Id_Kiadmany</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Kiadmany>
		<UgyintezesAlapja>
		  <Name>EREC_IraIratok.UgyintezesAlapja</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UgyintezesAlapja>
		<AdathordozoTipusa>
		  <Name>EREC_IraIratok.AdathordozoTipusa</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</AdathordozoTipusa>
		<Csoport_Id_Felelos>
		  <Name>EREC_IraIratok.Csoport_Id_Felelos</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Csoport_Id_Felelos>
		<Csoport_Id_Ugyfelelos>
		  <Name>EREC_IraIratok.Csoport_Id_Ugyfelelos</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Csoport_Id_Ugyfelelos>
		<FelhasznaloCsoport_Id_Orzo>
		  <Name>EREC_IraIratok.FelhasznaloCsoport_Id_Orzo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Orzo>
		<KiadmanyozniKell>
		  <Name>EREC_IraIratok.KiadmanyozniKell</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KiadmanyozniKell>
		<FelhasznaloCsoport_Id_Ugyintez>
		  <Name>EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Ugyintez>
		<Hatarido>
		  <Name>EREC_IraIratok.Hatarido</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Hatarido>
		<MegorzesiIdo>
		  <Name>EREC_IraIratok.MegorzesiIdo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</MegorzesiIdo>
		<IratFajta>
		  <Name>EREC_IraIratok.IratFajta</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IratFajta>
		<Irattipus>
		  <Name>EREC_IraIratok.Irattipus</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Irattipus>
		<KuldKuldemenyek_Id>
		  <Name>EREC_IraIratok.KuldKuldemenyek_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KuldKuldemenyek_Id>
		<Allapot>
		  <Name>EREC_IraIratok.Allapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Allapot>
		<Azonosito>
		  <Name>EREC_IraIratok.Azonosito</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Azonosito>
		<GeneraltTargy>
		  <Name>EREC_IraIratok.GeneraltTargy</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</GeneraltTargy>
		<IratMetaDef_Id>
		  <Name>EREC_IraIratok.IratMetaDef_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IratMetaDef_Id>
		<IrattarbaKuldDatuma>
		  <Name>EREC_IraIratok.IrattarbaKuldDatuma </Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IrattarbaKuldDatuma>
		<IrattarbaVetelDat>
		  <Name>EREC_IraIratok.IrattarbaVetelDat</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IrattarbaVetelDat>
		<Ugyirat_Id>
		  <Name>EREC_IraIratok.Ugyirat_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Ugyirat_Id>
		<Minosites>
		  <Name>EREC_IraIratok.Minosites</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Minosites>
		<Elintezett>
		  <Name>EREC_IraIratok.Elintezett</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Elintezett>
		<IratHatasaUgyintezesre>
		  <Name>EREC_IraIratok.IratHatasaUgyintezesre</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IratHatasaUgyintezesre>
		<FelfuggesztesOka>
		  <Name>EREC_IraIratok.FelfuggesztesOka</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelfuggesztesOka>
		<LezarasOka>
		  <Name>EREC_IraIratok.LezarasOka</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</LezarasOka>
		<Munkaallomas>
		  <Name>EREC_IraIratok.Munkaallomas</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Munkaallomas>
		<UgyintezesModja>
		  <Name>EREC_IraIratok.UgyintezesModja</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UgyintezesModja>
		<IntezesIdopontja>
		  <Name>EREC_IraIratok.IntezesIdopontja</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IntezesIdopontja>
		<IntezesiIdo>
		  <Name>EREC_IraIratok.IntezesiIdo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IntezesiIdo>
		<IntezesiIdoegyseg>
		  <Name>EREC_IraIratok.IntezesiIdoegyseg</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IntezesiIdoegyseg>
		<UzemzavarKezdete>
		  <Name>EREC_IraIratok.UzemzavarKezdete</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UzemzavarKezdete>
		<UzemzavarVege>
		  <Name>EREC_IraIratok.UzemzavarVege</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UzemzavarVege>
		<UzemzavarIgazoloIratSzama>
		  <Name>EREC_IraIratok.UzemzavarIgazoloIratSzama</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UzemzavarIgazoloIratSzama>
		<FelfuggesztettNapokSzama>
		  <Name>EREC_IraIratok.FelfuggesztettNapokSzama</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelfuggesztettNapokSzama>
		<VisszafizetesJogcime>
		  <Name>EREC_IraIratok.VisszafizetesJogcime</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</VisszafizetesJogcime>
		<VisszafizetesOsszege>
		  <Name>EREC_IraIratok.VisszafizetesOsszege</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</VisszafizetesOsszege>
		<VisszafizetesHatarozatSzama>
		  <Name>EREC_IraIratok.VisszafizetesHatarozatSzama</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</VisszafizetesHatarozatSzama>
		<Partner_Id_VisszafizetesCimzet>
		  <Name>EREC_IraIratok.Partner_Id_VisszafizetesCimzet</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Partner_Id_VisszafizetesCimzet>
		<Partner_Nev_VisszafizetCimzett>
		  <Name>EREC_IraIratok.Partner_Nev_VisszafizetCimzett</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Partner_Nev_VisszafizetCimzett>
		<VisszafizetesHatarido>
		  <Name>EREC_IraIratok.VisszafizetesHatarido</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</VisszafizetesHatarido>
		<Minosito>
		  <Name>EREC_IraIratok.Minosito</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Minosito>
		<MinositesErvenyessegiIdeje>
		  <Name>EREC_IraIratok.MinositesErvenyessegiIdeje</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</MinositesErvenyessegiIdeje>
		<TerjedelemMennyiseg>
		  <Name>EREC_IraIratok.TerjedelemMennyiseg</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</TerjedelemMennyiseg>
		<TerjedelemMennyisegiEgyseg>
		  <Name>EREC_IraIratok.TerjedelemMennyisegiEgyseg</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</TerjedelemMennyisegiEgyseg>
		<TerjedelemMegjegyzes>
		  <Name>EREC_IraIratok.TerjedelemMegjegyzes</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</TerjedelemMegjegyzes>
		<SelejtezesDat>
		  <Name>EREC_IraIratok.SelejtezesDat</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</SelejtezesDat>
		<FelhCsoport_Id_Selejtezo>
		  <Name>EREC_IraIratok.FelhCsoport_Id_Selejtezo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhCsoport_Id_Selejtezo>
		<LeveltariAtvevoNeve>
		  <Name>EREC_IraIratok.LeveltariAtvevoNeve</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</LeveltariAtvevoNeve>
		<ModositasErvenyessegKezdete>
		  <Name>EREC_IraIratok.ModositasErvenyessegKezdete</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ModositasErvenyessegKezdete>
		<MegszuntetoHatarozat>
		  <Name>EREC_IraIratok.MegszuntetoHatarozat</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</MegszuntetoHatarozat>
		<FelulvizsgalatDatuma>
		  <Name>EREC_IraIratok.FelulvizsgalatDatuma</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelulvizsgalatDatuma>
		<Felulvizsgalo_id>
		  <Name>EREC_IraIratok.Felulvizsgalo_id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Felulvizsgalo_id>
		<MinositesFelulvizsgalatEredm>
		  <Name>EREC_IraIratok.MinositesFelulvizsgalatEredm</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</MinositesFelulvizsgalatEredm>
		<UgyintezesKezdoDatuma>
		  <Name>EREC_IraIratok.UgyintezesKezdoDatuma</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UgyintezesKezdoDatuma>
		<EljarasiSzakasz>
		  <Name>EREC_IraIratok.EljarasiSzakasz</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</EljarasiSzakasz>
		<HatralevoNapok>
		  <Name>EREC_IraIratok.HatralevoNapok</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</HatralevoNapok>
		<HatralevoMunkaNapok>
		  <Name>EREC_IraIratok.HatralevoMunkaNapok</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</HatralevoMunkaNapok>
		<Ugy_Fajtaja>
		  <Name>EREC_IraIratok.Ugy_Fajtaja</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Ugy_Fajtaja>
		<MinositoSzervezet>
		  <Name>EREC_IraIratok.MinositoSzervezet</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</MinositoSzervezet>
		<MinositesFelulvizsgalatBizTag>
		  <Name>EREC_IraIratok.MinositesFelulvizsgalatBizTag</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</MinositesFelulvizsgalatBizTag>
		<ErvKezd>
		  <Name>EREC_IraIratok.ErvKezd</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ErvKezd>
		<ErvVege>
		  <Name>EREC_IraIratok.ErvVege</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ErvVege>
	  </Extended_EREC_IraIratokSearch>
	  <Extended_EREC_IraIktatoKonyvekSearch>
		<ReadableWhere />
		<WhereByManual />
		<OrderBy />
		<TopRow>0</TopRow>
		<Id>
		  <Name>EREC_IraIktatoKonyvek.Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Id>
		<Org>
		  <Name>EREC_IraIktatoKonyvek.Org</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Org>
		<Ev>
		  <Name>EREC_IraIktatoKonyvek.Ev</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Ev>
		<Azonosito>
		  <Name>EREC_IraIktatoKonyvek.Azonosito</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Azonosito>
		<DefaultIrattariTetelszam>
		  <Name>EREC_IraIktatoKonyvek.DefaultIrattariTetelszam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</DefaultIrattariTetelszam>
		<Nev>
		  <Name>EREC_IraIktatoKonyvek.Nev</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Nev>
		<MegkulJelzes>
		  <Name>EREC_IraIktatoKonyvek.MegkulJelzes</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</MegkulJelzes>
		<Iktatohely>
		  <Name>EREC_IraIktatoKonyvek.Iktatohely</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Iktatohely>
		<KozpontiIktatasJelzo>
		  <Name>EREC_IraIktatoKonyvek.KozpontiIktatasJelzo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KozpontiIktatasJelzo>
		<UtolsoFoszam>
		  <Name>EREC_IraIktatoKonyvek.UtolsoFoszam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UtolsoFoszam>
		<UtolsoSorszam>
		  <Name>EREC_IraIktatoKonyvek.UtolsoSorszam</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UtolsoSorszam>
		<Csoport_Id_Olvaso>
		  <Name>EREC_IraIktatoKonyvek.Csoport_Id_Olvaso</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Csoport_Id_Olvaso>
		<Csoport_Id_Tulaj>
		  <Name>EREC_IraIktatoKonyvek.Csoport_Id_Tulaj</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Csoport_Id_Tulaj>
		<IktSzamOsztas>
		  <Name>EREC_IraIktatoKonyvek.IktSzamOsztas</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IktSzamOsztas>
		<FormatumKod>
		  <Name>EREC_IraIktatoKonyvek.FormatumKod</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FormatumKod>
		<Titkos>
		  <Name>EREC_IraIktatoKonyvek.Titkos</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Titkos>
		<IktatoErkezteto>
		  <Name>EREC_IraIktatoKonyvek.IktatoErkezteto</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</IktatoErkezteto>
		<LezarasDatuma>
		  <Name>EREC_IraIktatoKonyvek.LezarasDatuma</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</LezarasDatuma>
		<PostakonyvVevokod>
		  <Name>EREC_IraIktatoKonyvek.PostakonyvVevokod</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</PostakonyvVevokod>
		<PostakonyvMegallapodasAzon>
		  <Name>EREC_IraIktatoKonyvek.PostakonyvMegallapodasAzon</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</PostakonyvMegallapodasAzon>
		<EFeladoJegyzekUgyfelAdatok>
		  <Name>EREC_IraIktatoKonyvek.EFeladoJegyzekUgyfelAdatok</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</EFeladoJegyzekUgyfelAdatok>
		<HitelesExport_Dok_ID>
		  <Name>EREC_IraIktatoKonyvek.HitelesExport_Dok_ID</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</HitelesExport_Dok_ID>
		<Ujranyitando>
		  <Name>EREC_IraIktatoKonyvek.Ujranyitando</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Ujranyitando>
		<ErvKezd>
		  <Name>EREC_IraIktatoKonyvek.ErvKezd</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ErvKezd>
		<ErvVege>
		  <Name>EREC_IraIktatoKonyvek.ErvVege</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ErvVege>
		<Statusz>
		  <Name>EREC_IraIktatoKonyvek.Statusz</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Statusz>
		<Terjedelem>
		  <Name>EREC_IraIktatoKonyvek.Terjedelem</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Terjedelem>
		<SelejtezesDatuma>
		  <Name>EREC_IraIktatoKonyvek.SelejtezesDatuma</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</SelejtezesDatuma>
		<KezelesTipusa>
		  <Name>EREC_IraIktatoKonyvek.KezelesTipusa</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KezelesTipusa>
	  </Extended_EREC_IraIktatoKonyvekSearch>
	  <Extended_KRT_MappakSearch>
		<ReadableWhere />
		<WhereByManual />
		<OrderBy />
		<TopRow>0</TopRow>
		<Id>
		  <Name>KRT_Mappak.Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Id>
		<SzuloMappa_Id>
		  <Name>KRT_Mappak.SzuloMappa_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</SzuloMappa_Id>
		<BarCode>
		  <Name>KRT_Mappak.BarCode</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</BarCode>
		<Tipus>
		  <Name>KRT_Mappak.Tipus</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Tipus>
		<Nev>
		  <Name>KRT_Mappak.Nev</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Nev>
		<Leiras>
		  <Name>KRT_Mappak.Leiras</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Leiras>
		<Csoport_Id_Tulaj>
		  <Name>KRT_Mappak.Csoport_Id_Tulaj</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Csoport_Id_Tulaj>
		<ErvKezd>
		  <Name>KRT_Mappak.ErvKezd</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ErvKezd>
		<ErvVege>
		  <Name>KRT_Mappak.ErvVege</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ErvVege>
	  </Extended_KRT_MappakSearch>
	  <Extended_EREC_IrattariKikeroSearch>
		<ReadableWhere />
		<WhereByManual />
		<OrderBy />
		<TopRow>0</TopRow>
		<Id>
		  <Name>EREC_IrattariKikero.Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Id>
		<Keres_note>
		  <Name>EREC_IrattariKikero.Keres_note</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Keres_note>
		<FelhasznalasiCel>
		  <Name>EREC_IrattariKikero.FelhasznalasiCel</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznalasiCel>
		<DokumentumTipus>
		  <Name>EREC_IrattariKikero.DokumentumTipus</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</DokumentumTipus>
		<Indoklas_note>
		  <Name>EREC_IrattariKikero.Indoklas_note</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Indoklas_note>
		<FelhasznaloCsoport_Id_Kikero>
		  <Name>EREC_IrattariKikero.FelhasznaloCsoport_Id_Kikero</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Kikero>
		<KeresDatuma>
		  <Name>EREC_IrattariKikero.KeresDatuma</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KeresDatuma>
		<KulsoAzonositok>
		  <Name>EREC_IrattariKikero.KulsoAzonositok</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KulsoAzonositok>
		<UgyUgyirat_Id>
		  <Name>EREC_IrattariKikero.UgyUgyirat_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UgyUgyirat_Id>
		<Tertiveveny_Id>
		  <Name>EREC_IrattariKikero.Tertiveveny_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Tertiveveny_Id>
		<Obj_Id>
		  <Name>EREC_IrattariKikero.Obj_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Obj_Id>
		<ObjTip_Id>
		  <Name>EREC_IrattariKikero.ObjTip_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ObjTip_Id>
		<KikerKezd>
		  <Name>EREC_IrattariKikero.KikerKezd</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KikerKezd>
		<KikerVege>
		  <Name>EREC_IrattariKikero.KikerVege</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KikerVege>
		<FelhasznaloCsoport_Id_Jovahagy>
		  <Name>EREC_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Jovahagy>
		<JovagyasDatuma>
		  <Name>EREC_IrattariKikero.JovagyasDatuma</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</JovagyasDatuma>
		<FelhasznaloCsoport_Id_Kiado>
		  <Name>EREC_IrattariKikero.FelhasznaloCsoport_Id_Kiado</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Kiado>
		<KiadasDatuma>
		  <Name>EREC_IrattariKikero.KiadasDatuma</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</KiadasDatuma>
		<FelhasznaloCsoport_Id_Visszaad>
		  <Name>EREC_IrattariKikero.FelhasznaloCsoport_Id_Visszaad</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Visszaad>
		<FelhasznaloCsoport_Id_Visszave>
		  <Name>EREC_IrattariKikero.FelhasznaloCsoport_Id_Visszave</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</FelhasznaloCsoport_Id_Visszave>
		<VisszaadasDatuma>
		  <Name>EREC_IrattariKikero.VisszaadasDatuma</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</VisszaadasDatuma>
		<SztornirozasDat>
		  <Name>EREC_IrattariKikero.SztornirozasDat</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</SztornirozasDat>
		<Allapot>
		  <Name>EREC_IrattariKikero.Allapot</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Allapot>
		<BarCode>
		  <Name>EREC_IrattariKikero.BarCode</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</BarCode>
		<Irattar_Id>
		  <Name>EREC_IrattariKikero.Irattar_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Irattar_Id>
		<ErvKezd>
		  <Name>EREC_IrattariKikero.ErvKezd</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ErvKezd>
		<ErvVege>
		  <Name>EREC_IrattariKikero.ErvVege</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ErvVege>
	  </Extended_EREC_IrattariKikeroSearch>
	  <Extended_EREC_IratMetaDefinicioSearch>
		<ReadableWhere />
		<WhereByManual />
		<OrderBy />
		<TopRow>0</TopRow>
		<Manual_Csoport_Id_Felelos>
		  <Name>EREC_SzignalasiJegyzekek.Csoport_Id_Felelos</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Csoport_Id_Felelos>
		<Manual_SzignalasTipusa>
		  <Name>EREC_SzignalasiJegyzekek.SzignalasTipusa</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_SzignalasTipusa>
		<Manual_Ugytipus_isNull>
		  <Name>EREC_IratMetaDefinicio.Ugytipus</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Manual_Ugytipus_isNull>
		<Id>
		  <Name>EREC_IratMetaDefinicio.Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Id>
		<Org>
		  <Name>EREC_IratMetaDefinicio.Org</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Org>
		<Ugykor_Id>
		  <Name>EREC_IratMetaDefinicio.Ugykor_Id</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Guid</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Ugykor_Id>
		<UgykorKod>
		  <Name>EREC_IratMetaDefinicio.UgykorKod</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UgykorKod>
		<Ugytipus>
		  <Name>EREC_IratMetaDefinicio.Ugytipus</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Ugytipus>
		<UgytipusNev>
		  <Name>EREC_IratMetaDefinicio.UgytipusNev</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UgytipusNev>
		<EljarasiSzakasz>
		  <Name>EREC_IratMetaDefinicio.EljarasiSzakasz</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</EljarasiSzakasz>
		<Irattipus>
		  <Name>EREC_IratMetaDefinicio.Irattipus</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Irattipus>
		<GeneraltTargy>
		  <Name>EREC_IratMetaDefinicio.GeneraltTargy</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</GeneraltTargy>
		<Rovidnev>
		  <Name>EREC_IratMetaDefinicio.Rovidnev</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Rovidnev>
		<UgyFajta>
		  <Name>EREC_IratMetaDefinicio.UgyFajta</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UgyFajta>
		<UgyiratIntezesiIdo>
		  <Name>EREC_IratMetaDefinicio.UgyiratIntezesiIdo</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Int32</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UgyiratIntezesiIdo>
		<Idoegyseg>
		  <Name>EREC_IratMetaDefinicio.Idoegyseg</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>String</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</Idoegyseg>
		<UgyiratIntezesiIdoKotott>
		  <Name>EREC_IratMetaDefinicio.UgyiratIntezesiIdoKotott</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UgyiratIntezesiIdoKotott>
		<UgyiratHataridoKitolas>
		  <Name>EREC_IratMetaDefinicio.UgyiratHataridoKitolas</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>Char</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</UgyiratHataridoKitolas>
		<ErvKezd>
		  <Name>EREC_IratMetaDefinicio.ErvKezd</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ErvKezd>
		<ErvVege>
		  <Name>EREC_IratMetaDefinicio.ErvVege</Name>
		  <Operator />
		  <Value />
		  <ValueTo />
		  <Type>DateTime</Type>
		  <Group>0</Group>
		  <GroupOperator>and</GroupOperator>
		</ErvVege>
	  </Extended_EREC_IratMetaDefinicioSearch>
	  <ObjektumTargyszavai_ObjIdFilter />
	  <WhereByManual />
	  <OrderBy />
	  <TopRow>0</TopRow>
	  <Manual_LetrehozasIdo>
		<Name>EREC_UgyUgyiratok.LetrehozasIdo</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_LetrehozasIdo>
	  <Manual_ModositasIdo>
		<Name>EREC_UgyUgyiratok.ModositasIdo</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_ModositasIdo>
	  <Manual_Sajat>
		<Name>EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Sajat>
	  <Manual_Sajat_Allapot>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Sajat_Allapot>
	  <Manual_Sajat_TovabbitasAlattAllapot>
		<Name>EREC_UgyUgyiratok.TovabbitasAlattAllapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Sajat_TovabbitasAlattAllapot>
	  <Manual_Jovahagyandok>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Jovahagyandok>
	  <Manual_Jovahagyo>
		<Name>EREC_UgyUgyiratok.Kovetkezo_Felelos_Id</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Jovahagyo>
	  <Manual_Elo_Ugyiratok_Allapot>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Elo_Ugyiratok_Allapot>
	  <Manual_Elo_Ugyiratok_TovabbitasAlattAllapot>
		<Name>EREC_UgyUgyiratok.TovabbitasAlattAllapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Elo_Ugyiratok_TovabbitasAlattAllapot>
	  <Manual_Lezartak>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Lezartak>
	  <Manual_Hataridoben>
		<Name>EREC_UgyUgyiratok.Hatarido</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Hataridoben>
	  <Manual_Skontroban>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Skontroban>
	  <Manual_Sztornozottak>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Sztornozottak>
	  <Manual_Hataridon_tul>
		<Name>EREC_UgyUgyiratok.Hatarido</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Hataridon_tul>
	  <Manual_Elintezett>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Elintezett>
	  <Manual_Szereltek>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Szereltek>
	  <Manual_SzereltekNelkul>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_SzereltekNelkul>
	  <Manual_SzerelesreElokeszitettek>
		<Name>EREC_UgyUgyiratok_ElokeszitettSzerelesCount.ElokeszitettSzerelesCount</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_SzerelesreElokeszitettek>
	  <Manual_Szerelendok>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Szerelendok>
	  <Manual_HataridoElottXNappal>
		<Name>EREC_UgyUgyiratok.Hatarido</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_HataridoElottXNappal>
	  <Manual_AlszamraIktathatoak>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_AlszamraIktathatoak>
	  <Manual_Allapot_DefaultFilter>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Allapot_DefaultFilter>
	  <Manual_Allapot_Filter>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Allapot_Filter>
	  <Manual_IrattariJel>
		<Name>EREC_IraIrattariTetelek.IrattariJel</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_IrattariJel>
	  <Manual_Szereles_Alatt>
		<Name>EREC_UgyUgyiratok.TovabbitasAlattAllapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Szereles_Alatt>
	  <Manual_Csatolhato>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Csatolhato>
	  <Manual_Foszam_MunkaanyagFilter>
		<Name>EREC_UgyUgyiratok.Foszam</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Int32</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_Foszam_MunkaanyagFilter>
	  <Manual_TovabbitasAlattiakNelkul>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Manual_TovabbitasAlattiakNelkul>
	  <IsTovabbitasAlattAllapotIncluded>true</IsTovabbitasAlattAllapotIncluded>
	  <IsSzereltekExcluded>false</IsSzereltekExcluded>
	  <Csoporttagokkal>true</Csoporttagokkal>
	  <CsakAktivIrat>false</CsakAktivIrat>
	  <NaponBelulNincsUjAlszam>-1</NaponBelulNincsUjAlszam>
	  <Id>
		<Name>EREC_UgyUgyiratok.Id</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Id>
	  <Foszam>
		<Name>EREC_UgyUgyiratok.Foszam</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Int32</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Foszam>
	  <Sorszam>
		<Name>EREC_UgyUgyiratok.Sorszam</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Int32</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Sorszam>
	  <Ugyazonosito>
		<Name>EREC_UgyUgyiratok.Ugyazonosito </Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Ugyazonosito>
	  <Alkalmazas_Id>
		<Name>EREC_UgyUgyiratok.Alkalmazas_Id</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Alkalmazas_Id>
	  <UgyintezesModja>
		<Name>EREC_UgyUgyiratok.UgyintezesModja</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </UgyintezesModja>
	  <Hatarido>
		<Name>EREC_UgyUgyiratok.Hatarido</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Hatarido>
	  <SkontrobaDat>
		<Name>EREC_UgyUgyiratok.SkontrobaDat</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </SkontrobaDat>
	  <LezarasDat>
		<Name>EREC_UgyUgyiratok.LezarasDat</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </LezarasDat>
	  <IrattarbaKuldDatuma>
		<Name>EREC_UgyUgyiratok.IrattarbaKuldDatuma </Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </IrattarbaKuldDatuma>
	  <IrattarbaVetelDat>
		<Name>EREC_UgyUgyiratok.IrattarbaVetelDat</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </IrattarbaVetelDat>
	  <FelhCsoport_Id_IrattariAtvevo>
		<Name>EREC_UgyUgyiratok.FelhCsoport_Id_IrattariAtvevo</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </FelhCsoport_Id_IrattariAtvevo>
	  <SelejtezesDat>
		<Name>EREC_UgyUgyiratok.SelejtezesDat</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </SelejtezesDat>
	  <FelhCsoport_Id_Selejtezo>
		<Name>EREC_UgyUgyiratok.FelhCsoport_Id_Selejtezo</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </FelhCsoport_Id_Selejtezo>
	  <LeveltariAtvevoNeve>
		<Name>EREC_UgyUgyiratok.LeveltariAtvevoNeve</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </LeveltariAtvevoNeve>
	  <FelhCsoport_Id_Felulvizsgalo>
		<Name>EREC_UgyUgyiratok.FelhCsoport_Id_Felulvizsgalo</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </FelhCsoport_Id_Felulvizsgalo>
	  <FelulvizsgalatDat>
		<Name>EREC_UgyUgyiratok.FelulvizsgalatDat</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </FelulvizsgalatDat>
	  <IktatoszamKieg>
		<Name>EREC_UgyUgyiratok.IktatoszamKieg</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </IktatoszamKieg>
	  <Targy>
		<Name>EREC_UgyUgyiratok.Targy</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>FTSString</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Targy>
	  <UgyUgyirat_Id_Szulo>
		<Name>EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </UgyUgyirat_Id_Szulo>
	  <UgyUgyirat_Id_Kulso>
		<Name>EREC_UgyUgyiratok.UgyUgyirat_Id_Kulso</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </UgyUgyirat_Id_Kulso>
	  <UgyTipus>
		<Name>EREC_UgyUgyiratok.UgyTipus</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </UgyTipus>
	  <IrattariHely>
		<Name>EREC_UgyUgyiratok.IrattariHely</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </IrattariHely>
	  <SztornirozasDat>
		<Name>EREC_UgyUgyiratok.SztornirozasDat</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </SztornirozasDat>
	  <Csoport_Id_Felelos>
		<Name>EREC_UgyUgyiratok.Csoport_Id_Felelos</Name>
		<Operator>=</Operator>
		<Value>08a90214-1d0e-e811-80c5-00155d027ea9</Value>
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Csoport_Id_Felelos>
	  <FelhasznaloCsoport_Id_Orzo>
		<Name>EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo</Name>
		<Operator>in</Operator>
		<Value>#FelhasznaloCsoport_Id_Orzo#</Value>
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </FelhasznaloCsoport_Id_Orzo>
	  <Csoport_Id_Cimzett>
		<Name>EREC_UgyUgyiratok.Csoport_Id_Cimzett</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Csoport_Id_Cimzett>
	  <Jelleg>
		<Name>EREC_UgyUgyiratok.Jelleg</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Jelleg>
	  <IraIrattariTetel_Id>
		<Name>EREC_UgyUgyiratok.IraIrattariTetel_Id</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </IraIrattariTetel_Id>
	  <IraIktatokonyv_Id>
		<Name>EREC_UgyUgyiratok.IraIktatokonyv_Id</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </IraIktatokonyv_Id>
	  <SkontroOka>
		<Name>EREC_UgyUgyiratok.SkontroOka</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </SkontroOka>
	  <SkontroVege>
		<Name>EREC_UgyUgyiratok.SkontroVege</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </SkontroVege>
	  <Surgosseg>
		<Name>EREC_UgyUgyiratok.Surgosseg</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Surgosseg>
	  <SkontrobanOsszesen>
		<Name>EREC_UgyUgyiratok.SkontrobanOsszesen</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Int32</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </SkontrobanOsszesen>
	  <MegorzesiIdoVege>
		<Name>EREC_UgyUgyiratok.MegorzesiIdoVege</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </MegorzesiIdoVege>
	  <Partner_Id_Ugyindito>
		<Name>EREC_UgyUgyiratok.Partner_Id_Ugyindito</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Partner_Id_Ugyindito>
	  <NevSTR_Ugyindito>
		<Name>EREC_UgyUgyiratok.NevSTR_Ugyindito</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>FTSString</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </NevSTR_Ugyindito>
	  <ElintezesDat>
		<Name>EREC_UgyUgyiratok.ElintezesDat</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </ElintezesDat>
	  <FelhasznaloCsoport_Id_Ugyintez>
		<Name>EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </FelhasznaloCsoport_Id_Ugyintez>
	  <Csoport_Id_Felelos_Elozo>
		<Name>EREC_UgyUgyiratok.Csoport_Id_Felelos_Elozo</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Csoport_Id_Felelos_Elozo>
	  <KolcsonKikerDat>
		<Name>EREC_UgyUgyiratok.KolcsonKikerDat</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </KolcsonKikerDat>
	  <KolcsonKiadDat>
		<Name>EREC_UgyUgyiratok.KolcsonKiadDat</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </KolcsonKiadDat>
	  <Kolcsonhatarido>
		<Name>EREC_UgyUgyiratok.Kolcsonhatarido</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Kolcsonhatarido>
	  <BARCODE>
		<Name>EREC_UgyUgyiratok.BARCODE</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </BARCODE>
	  <IratMetadefinicio_Id>
		<Name>EREC_UgyUgyiratok.IratMetadefinicio_Id</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </IratMetadefinicio_Id>
	  <Allapot>
		<Name>EREC_UgyUgyiratok.Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Allapot>
	  <TovabbitasAlattAllapot>
		<Name>EREC_UgyUgyiratok.TovabbitasAlattAllapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </TovabbitasAlattAllapot>
	  <Megjegyzes>
		<Name>EREC_UgyUgyiratok.Megjegyzes</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Megjegyzes>
	  <Azonosito>
		<Name>EREC_UgyUgyiratok.Azonosito</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Azonosito>
	  <Fizikai_Kezbesitesi_Allapot>
		<Name>EREC_UgyUgyiratok.Fizikai_Kezbesitesi_Allapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Fizikai_Kezbesitesi_Allapot>
	  <Kovetkezo_Orzo_Id>
		<Name>EREC_UgyUgyiratok.Kovetkezo_Orzo_Id</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Kovetkezo_Orzo_Id>
	  <Csoport_Id_Ugyfelelos>
		<Name>EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Csoport_Id_Ugyfelelos>
	  <Elektronikus_Kezbesitesi_Allap>
		<Name>EREC_UgyUgyiratok.Elektronikus_Kezbesitesi_Allap</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Elektronikus_Kezbesitesi_Allap>
	  <Kovetkezo_Felelos_Id>
		<Name>EREC_UgyUgyiratok.Kovetkezo_Felelos_Id</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Kovetkezo_Felelos_Id>
	  <UtolsoAlszam>
		<Name>EREC_UgyUgyiratok.UtolsoAlszam</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Int32</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </UtolsoAlszam>
	  <UtolsoSorszam>
		<Name>EREC_UgyUgyiratok.UtolsoSorszam</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Int32</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </UtolsoSorszam>
	  <IratSzam>
		<Name>EREC_UgyUgyiratok.IratSzam</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Int32</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </IratSzam>
	  <ElintezesMod>
		<Name>EREC_UgyUgyiratok.ElintezesMod</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </ElintezesMod>
	  <RegirendszerIktatoszam>
		<Name>EREC_UgyUgyiratok.RegirendszerIktatoszam</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </RegirendszerIktatoszam>
	  <GeneraltTargy>
		<Name>EREC_UgyUgyiratok.GeneraltTargy</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </GeneraltTargy>
	  <Cim_Id_Ugyindito>
		<Name>EREC_UgyUgyiratok.Cim_Id_Ugyindito </Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Cim_Id_Ugyindito>
	  <CimSTR_Ugyindito>
		<Name>EREC_UgyUgyiratok.CimSTR_Ugyindito </Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </CimSTR_Ugyindito>
	  <LezarasOka>
		<Name>EREC_UgyUgyiratok.LezarasOka</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </LezarasOka>
	  <FelfuggesztesOka>
		<Name>EREC_UgyUgyiratok.FelfuggesztesOka</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </FelfuggesztesOka>
	  <IrattarId>
		<Name>EREC_UgyUgyiratok.IrattarId</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </IrattarId>
	  <SkontroOka_Kod>
		<Name>EREC_UgyUgyiratok.SkontroOka_Kod</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </SkontroOka_Kod>
	  <UjOrzesiIdo>
		<Name>EREC_UgyUgyiratok.UjOrzesiIdo</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Int32</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </UjOrzesiIdo>
	  <UjOrzesiIdoIdoegyseg>
		<Name>EREC_UgyUgyiratok.UjOrzesiIdoIdoegyseg</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </UjOrzesiIdoIdoegyseg>
	  <UgyintezesKezdete>
		<Name>EREC_UgyUgyiratok.UgyintezesKezdete</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </UgyintezesKezdete>
	  <FelfuggesztettNapokSzama>
		<Name>EREC_UgyUgyiratok.FelfuggesztettNapokSzama</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Int32</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </FelfuggesztettNapokSzama>
	  <IntezesiIdo>
		<Name>EREC_UgyUgyiratok.IntezesiIdo</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </IntezesiIdo>
	  <IntezesiIdoegyseg>
		<Name>EREC_UgyUgyiratok.IntezesiIdoegyseg</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </IntezesiIdoegyseg>
	  <Ugy_Fajtaja>
		<Name>EREC_UgyUgyiratok.Ugy_Fajtaja</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Ugy_Fajtaja>
	  <SzignaloId>
		<Name>EREC_UgyUgyiratok.SzignaloId</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Guid</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </SzignaloId>
	  <SzignalasIdeje>
		<Name>EREC_UgyUgyiratok.SzignalasIdeje</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </SzignalasIdeje>
	  <ElteltIdo>
		<Name>EREC_UgyUgyiratok.ElteltIdo</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </ElteltIdo>
	  <ElteltIdoIdoEgyseg>
		<Name>EREC_UgyUgyiratok.ElteltIdoIdoEgyseg</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </ElteltIdoIdoEgyseg>
	  <ElteltidoAllapot>
		<Name>EREC_UgyUgyiratok.ElteltidoAllapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Int32</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </ElteltidoAllapot>
	  <ElteltIdoUtolsoModositas>
		<Name>EREC_UgyUgyiratok.ElteltIdoUtolsoModositas</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </ElteltIdoUtolsoModositas>
	  <SakkoraAllapot>
		<Name>EREC_UgyUgyiratok.SakkoraAllapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </SakkoraAllapot>
	  <HatralevoNapok>
		<Name>EREC_UgyUgyiratok.HatralevoNapok</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Int32</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </HatralevoNapok>
	  <HatralevoMunkaNapok>
		<Name>EREC_UgyUgyiratok.HatralevoMunkaNapok</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Int32</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </HatralevoMunkaNapok>
	  <ElozoAllapot>
		<Name>EREC_UgyUgyiratok.ElozoAllapot</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>String</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </ElozoAllapot>
	  <Aktiv>
		<Name>EREC_UgyUgyiratok.Aktiv</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Int32</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </Aktiv>
	  <IrattarHelyfoglalas>
		<Name>EREC_UgyUgyiratok.IrattarHelyfoglalas</Name>
		<Operator />
		<Value />
		<ValueTo />
		<Type>Double</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </IrattarHelyfoglalas>
	  <ErvKezd>
		<Name>EREC_UgyUgyiratok.ErvKezd</Name>
		<Operator>&lt;=</Operator>
		<Value>getdate()</Value>
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </ErvKezd>
	  <ErvVege>
		<Name>EREC_UgyUgyiratok.ErvVege</Name>
		<Operator>&gt;=</Operator>
		<Value>getdate()</Value>
		<ValueTo />
		<Type>DateTime</Type>
		<Group>0</Group>
		<GroupOperator>and</GroupOperator>
	  </ErvVege>
	</EREC_UgyUgyiratokSearch>'

	declare @felhasznalo_id uniqueidentifier
	set @felhasznalo_id = (select top 1 Id from KRT_Csoportok where nev = @felhasznaloNev and getdate() between ErvKezd and ErvVege)

	set @templateXML = REPLACE(@templateXML,'#IratHelye#', @irattariHelyek)
	set @templateXML = REPLACE(@templateXML,'#FelhasznaloCsoport_Id_Orzo#', @felhasznaloCsoport_Id_Orzo)

	IF @felhasznalo_id is null
	BEGIN
		print 'Felhasznalo nem talalhato: ' + @felhasznaloNev
	END
	ELSE
	BEGIN
		IF NOT EXISTS(SELECT 1 FROM KRT_UIMezoObjektumErtekek WHERE ID = @record_id)
		BEGIN
			PRINT 'Insert KRT_UIMezoObjektumErtekek: ' + @nev + ' - ' + @felhasznaloNev
			INSERT INTO KRT_UIMezoObjektumErtekek
			(
				Id,
				TemplateTipusNev,
				Nev,
				Alapertelmezett,
				Felhasznalo_Id,
				TemplateXML,
				Publikus
			)
			VALUES
			(
				@record_id,
				@templateTipusNev,
				@nev,
				'1',
				@felhasznalo_id,
				@templateXML,
				'0'
			)
		END
	END
END

GO

DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

IF @org_kod = 'NMHH'
BEGIN

	exec #Insert_Template 'C2BD3C2D-2B12-46FD-BB75-AB3A8E40515F','Majorosn� Fil�k Judit','''�tmeneti iratt�r - Budapest Ostrom'''
	exec #Insert_Template '637BD7D2-88EC-46A2-A861-8F0A349F77FF','Baranyi-T�th Zsuzsanna KI','''�tmeneti iratt�r - Budapest Ostrom'''
	exec #Insert_Template '1CA24E10-1241-4193-A1EF-751B5928EE50','Darabos Andrea','''�tmeneti iratt�r - Budapest Ostrom'''
	exec #Insert_Template '95AEBF34-9AB9-468B-8D04-CC80664F9FFF','Luk�cs Barbara','''�tmeneti iratt�r - Budapest Ostrom'',''�tmeneti iratt�r - Budapest Visegr�di'',''�tmeneti iratt�r - Budapest Reviczky'''
	exec #Insert_Template 'C7CE137D-229B-4BE8-8C28-14E5E23E1284','Ol�h Barbara','''�tmeneti iratt�r - Budapest Ostrom'''
	exec #Insert_Template '284F11FE-8884-4CC0-941E-E4D6CD2D1C9A','Ujsz�szi-V�mos Aranka','''�tmeneti iratt�r - Budapest Ostrom'''

	exec #Insert_Template '75C39342-1DD8-42FC-B66B-46E4963FFD2C','Kov�cs T�mea','''�tmeneti iratt�r - Budapest Visegr�di'''
	exec #Insert_Template '171B76EF-E98F-41E8-A713-1E04E302CB42','Szab�n� Kiss M�rta','''�tmeneti iratt�r - Budapest Visegr�di'''
	exec #Insert_Template '09D3B075-0F13-4901-A267-96371A849DD0','Forczekn� Makkai Judit','''�tmeneti iratt�r - Budapest Visegr�di'''

	exec #Insert_Template '540B6166-C91D-460C-9DC6-375848D8FB61','Ol�hn� Jok�n Ildik�','''�tmeneti iratt�r - Budapest Reviczky'''
	exec #Insert_Template 'ECA669A7-03A1-4BFE-AE56-9C439727D200','Gr�sz Anna','''�tmeneti iratt�r - Budapest Reviczky'''

	exec #Insert_Template 'A994C608-2606-4365-889E-565310263FBD','Prajczern� Nagy Anik�','''Soproni hat�s�gi iroda'''
	exec #Insert_Template '057F93F9-9066-42C1-A4E0-89A2654FB0E4','Tam�si T�mea','''Soproni hat�s�gi iroda'''

	exec #Insert_Template '00EA81DD-5E25-451A-B4F8-D77CE4548E2F','P�lkov�cs Andrea','''Miskolci hat�s�gi iroda'''
	exec #Insert_Template '4B60ABA3-AF5F-448B-91C6-EC1BC479C65F','T�th Zsanett','''Miskolci hat�s�gi iroda'''

	exec #Insert_Template '43F6DDE9-151E-47E1-8446-077A6DCC78F0','Tak�cs D. Orsolya','''Debreceni hat�s�gi iroda'''
	exec #Insert_Template '4BAC34F3-EF37-4F79-A1DF-57085C69027B','N�meth Anna','''Debreceni hat�s�gi iroda'''

	exec #Insert_Template '935AF4F9-5508-472A-8685-426BDBC65F86','Somogyv�ri-Jech Marina','''P�csi hat�s�gi iroda'''
	exec #Insert_Template '4F303E5B-F781-400A-B203-F9E0D0DD70FB','Zombory Eszter','''P�csi hat�s�gi iroda'''

	exec #Insert_Template '5ACC229E-5092-4CEC-AE94-E49B1E848DF5','D�zsa �gnes','''Szegedi hat�s�gi iroda'''
	exec #Insert_Template '272DAD55-0D82-4598-B8F7-CE08D9999391','Solymosi N�ra','''Szegedi hat�s�gi iroda'''

END

GO

DROP PROCEDURE #Insert_Template