--MEGHIUSULASI_IGAZOLAS_TERTI_VISSZA_KOD
DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok
			where id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = 'C7FEF9C4-773C-4770-8971-EC5ADD26C090'

IF @org_kod = 'BOPMH'
	set @ertek = '7'
else
	set @ertek = '9'

if not exists (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - MEGHIUSULASI_IGAZOLAS_TERTI_VISSZA_KOD  - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'MEGHIUSULASI_IGAZOLAS_TERTI_VISSZA_KOD'
		,@ertek
		,'1'
		,'A megadott k�dot �ll�tja be a hivatali kapur�l �rkez� meghi�sul�si igazol�s k�zbes�t�si eredm�nyk�nt a t�rtivev�nyen.'
		); 
 END
 GO