DECLARE @Org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @OrgKod nvarchar(100) = (select Kod from KRT_Orgok where [Id] = @Org)

IF @OrgKod = 'NMHH'
BEGIN
	-- �gyirat felf�ggeszt�s oka
	BEGIN TRAN

	declare @UGYIRAT_FELFUGGESZTES_OKA uniqueidentifier
	set @UGYIRAT_FELFUGGESZTES_OKA = 'FDC8D0D4-B6B6-4900-4900-868686868686'

	declare @ertekek table(Id uniqueidentifier, Nev nvarchar(400), Kod nvarchar(64), RovidNev nvarchar(10), Modosithato char(1))

	insert into @ertekek
	(Id, Nev, Kod, RovidNev, Modosithato)
	values
	('45185280-7F5F-4A9C-ADA7-CB5DB53FFAB0','bizony�t�si elj�r�sban a 70.� (1) eset�n','BIZONYITASI_ELJARASBAN', NULL, '0')

	insert into @ertekek
	(Id, Nev, Kod, RovidNev, Modosithato)
	values
	('45185280-7F5F-4A9C-ADA7-CB5DB53FFAB1','elj�r�s felf�ggeszt�se','ELJARAS_FELFUGGESZTESE', NULL, '0')

	insert into @ertekek
	(Id, Nev, Kod, RovidNev, Modosithato)
	values
	('FDC8D0D4-B6B6-4900-4901-868686868686','hat�sk�r, illet�kess�g meg�llap�t�sa','HAT�SKORI_VAGY_ILLETEKESS�GI_VITA', 'hat�sk�ri ', '0')

	insert into @ertekek
	(Id, Nev, Kod, RovidNev, Modosithato)
	values
	('FDC8D0D4-B6B6-4900-4903-868686868686','hi�nyp�tl�s (adatszolg�ltat�s)','HIANYPOTLAS', 'Hi�nyp�tl�', '0')

	insert into @ertekek
	(Id, Nev, Kod, RovidNev, Modosithato)
	values
	('FDC8D0D4-B6B6-4900-4905-868686868686','ford�t�s','IRAT_FORDITASA','Irat ford�', '0')

	insert into @ertekek
	(Id, Nev, Kod, RovidNev, Modosithato)
	values
	('FDC8D0D4-B6B6-4900-4902-868686868686','jogseg�ly elj�r�s','JOGSEGELYELJARAS', 'Jogseg�lye', '0')

	insert into @ertekek
	(Id, Nev, Kod, RovidNev, Modosithato)
	values
	('45185280-7F5F-4A9C-ADA7-CB5DB53FFAB3','k�zbes�t�si, k�zl�si id�tartam a 33.� (3) bekezd�s k) pont szerint','KEZBESITESI_KOZLESI_IDOTARTAM', NULL, '0')

	insert into @ertekek
	(Id, Nev, Kod, RovidNev, Modosithato)
	values
	('FDC8D0D4-B6B6-4900-4906-868686868686','szak�rt�i v�lem�ny','SZAKERTOI_VELEMENY_ELKESZITESE', 'Szak�rt�i', '0')

	insert into @ertekek
	(Id, Nev, Kod, RovidNev, Modosithato)
	values
	('FDC8D0D4-B6B6-4900-4904-868686868686','szakhat�s�gi elj�r�s','SZAKHATOSAGI_ELJARAS','Szakhat�s�','0')
	

	-- nevek jav�t�sa
	UPDATE KRT_KodTarak
		SET Nev = ert.Nev,
		    Kod = ert.Kod,
			ErvVege = '4700-12-31'
	FROM 
	KRT_KodTarak kt
    join @ertekek ert on kt.Id = ert.Id
	where kt.KodCsoport_Id = @UGYIRAT_FELFUGGESZTES_OKA

	-- feleslegesek �rv�nytelen�t�se
	UPDATE KRT_KodTarak
		SET ErvVege = GETDATE()
	WHERE KodCsoport_Id = @UGYIRAT_FELFUGGESZTES_OKA
	and Id not in (select Id from @ertekek)
	and GETDATE() between ErvKezd and ErvVege

	-- hi�nyz�k felv�tele
	INSERT INTO KRT_KodTarak
	(Id, Org, KodCsoport_Id, Kod, Nev, RovidNev, Modosithato)
	SELECT Id, @Org, @UGYIRAT_FELFUGGESZTES_OKA, Kod, Nev, RovidNev, Modosithato
	FROM @ertekek
	WHERE Id not in
	(
		select Id from KRT_KodTarak
		where KodCsoport_Id = @UGYIRAT_FELFUGGESZTES_OKA
	)

	--select * from KRT_KodTarak
	--where KodCsoport_Id = @UGYIRAT_FELFUGGESZTES_OKA
	--and GETDATE() < ErvVege
	--order by Nev
	
	COMMIT

END