DECLARE @Org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @OrgKod nvarchar(100) = (select Kod from KRT_Orgok where [Id] = @Org)

IF @OrgKod = 'NMHH'
BEGIN
	-- �gyirat lez�r�s�nak oka
	BEGIN TRAN

	declare @UGYIRAT_LEZARAS_OKA uniqueidentifier
	set @UGYIRAT_LEZARAS_OKA = 'BD00D394-249C-4582-B03E-1CF5B6A1EFE0'

	declare @ertekek table(Id uniqueidentifier, Nev nvarchar(400), Kod nvarchar(64), RovidNev nvarchar(10), Modosithato char(1), Sorrend nvarchar(100))

	insert into @ertekek
	(Id, Kod, Nev, RovidNev, Modosithato, Sorrend)
	values
	('4793E710-AC38-486D-AEC8-10754FD22A70', 'ATTETEL', '�tt�tel', NULL, '0', '001')

	insert into @ertekek
	(Id, Kod, Nev, RovidNev, Modosithato, Sorrend)
	values
	('4793E710-AC38-486D-AEC8-10754FD22A71', 'ELJARAST_MEGSZUNTETO_VEGZES', 'elj�r�st megsz�ntet� v�gz�s', NULL, '0', '002')

	insert into @ertekek
	(Id, Kod, Nev, RovidNev, Modosithato, Sorrend)
	values
	('4793E710-AC38-486D-AEC8-10754FD22A72', 'ELSO_FOKU_HATAROZAT', 'els�fok� hat�rozat', NULL, '0', '003')

	insert into @ertekek
	(Id, Kod, Nev, RovidNev, Modosithato, Sorrend)
	values
	('4793E710-AC38-486D-AEC8-10754FD22A73', 'ELSO_FOKU_HATAROZAT_MODOSITASA', 'els�fok� hat�rozat m�dos�t�sa', NULL, '0', '004')

	insert into @ertekek
	(Id, Kod, Nev, RovidNev, Modosithato, Sorrend)
	values
	('4793E710-AC38-486D-AEC8-10754FD22A74', 'ELSO_FOKU_HATAROZAT_VISSZAVONASA', 'els�fok� hat�rozat visszavon�sa', NULL, '0', '005')

		insert into @ertekek
	(Id, Kod, Nev, RovidNev, Modosithato, Sorrend)
	values
	('4793E710-AC38-486D-AEC8-10754FD22A75', 'VIZSGALAT_NELKULI_ELUTASITO_VEGZES', '�rdemi vizsg�lat n�lk�l elutas�t� v�gz�s', NULL, '0', '006')

	insert into @ertekek
	(Id, Kod, Nev, RovidNev, Modosithato, Sorrend)
	values
	('4793E710-AC38-486D-AEC8-10754FD22A76', 'MASODFOKU_ELJARAST_MEGSZUNTETO_VEGZES', 'm�sodfok� elj�r�st megsz�ntet� v�gz�s', NULL, '0', '007')

	insert into @ertekek
	(Id, Kod, Nev, RovidNev, Modosithato, Sorrend)
	values
	('4793E710-AC38-486D-AEC8-10754FD22A77', 'MASODFOKU_HATAROZAT', 'm�sodfok� hat�rozat', NULL, '0', '008')

	insert into @ertekek
	(Id, Kod, Nev, RovidNev, Modosithato, Sorrend)
	values
	('F604AB98-46AA-E911-80DA-00155D59B14B', 'MASODFOKU_VEGZES', 'm�sodfok� v�gz�s', NULL, '0', '009')

	insert into @ertekek
	(Id, Kod, Nev, RovidNev, Modosithato, Sorrend)
	values
	('BE66FAC1-0BB1-42E8-AD7D-30258DA1CFF6', 'TAJEKOZTATAS', 't�j�koztat�s', NULL, '0', '010')


	-- nevek jav�t�sa
	UPDATE KRT_KodTarak
		SET Nev = ert.Nev,
		    Kod = ert.Kod,
			Sorrend = ert.Sorrend
	FROM 
	KRT_KodTarak kt
    join @ertekek ert on kt.Id = ert.Id
	where kt.KodCsoport_Id = @UGYIRAT_LEZARAS_OKA

	-- feleslegesek �rv�nytelen�t�se
	UPDATE KRT_KodTarak
		SET ErvVege = GETDATE()
	WHERE KodCsoport_Id = @UGYIRAT_LEZARAS_OKA
	and Id not in (select Id from @ertekek)
	and GETDATE() between ErvKezd and ErvVege

	-- hi�nyz�k felv�tele
	INSERT INTO KRT_KodTarak
	(Id, Org, KodCsoport_Id, Kod, Nev, RovidNev, Modosithato, Sorrend)
	SELECT Id, @Org, @UGYIRAT_LEZARAS_OKA, Kod, Nev, RovidNev, Modosithato, Sorrend
	FROM @ertekek
	WHERE Id not in
	(
		select Id from KRT_KodTarak
		where KodCsoport_Id = @UGYIRAT_LEZARAS_OKA
	)

	--select * from KRT_KodTarak
	--where KodCsoport_Id = @UGYIRAT_LEZARAS_OKA
	--and ErvVege > GETDATE()
	--order by Sorrend
	
	COMMIT

END