print 'Krt_partnerek üres Allapot mező javítása'

DECLARE @wrongrowCount int SET @wrongrowCount = (select count(*) from krt_partnerek where Allapot is null) 

if @wrongrowCount > 0 
BEGIN
	print 'Hibás tételek javítása'
	Update krt_partnerek set Allapot = 'JOVAHAGYOTT' where Allapot is null;
END
ELSE
BEGIN
	print 'Nem található hibás tétel!'
END

GO



