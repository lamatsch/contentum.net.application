-- BUG_7247 Selejtez�s iktat�sz�ma mez�


-- drop #Insert_Kodtar
IF OBJECT_ID('tempdb..#Insert_Kodtar') IS NOT NULL
BEGIN
    DROP PROC #Insert_Kodtar
END
GO

-- 
DECLARE @KODCSOPORT_ID_7247 uniqueidentifier = '912B916C-B20E-4990-BD00-BCF60892F5AD'
DECLARE @KODCSOPORT_Kod_7247 nvarchar(100) = 'FELHASZNALOI_ENGEDELY_JOGOSULTSAG_LISTA'
DECLARE @KODCSOPORT_Nev_7247 nvarchar(100) = 'Jogosults�g lista'
DECLARE @Letrehozo_Id uniqueidentifier = '54E861A5-36ED-44CA-BAA7-C287D125B309'
DECLARE @Org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @OrgKod nvarchar(100) = (select Kod from KRT_Orgok where Id = @Org)

-- create #Insert_Kodtar

DECLARE @sql NVARCHAR(2000)
SET @sql = N'
CREATE PROCEDURE #Insert_Kodtar
(  
   @kodtar_id uniqueidentifier,
   @kodtar_nev NVARCHAR(400)  
)  
AS  
BEGIN  
	IF not exists (select 1 from KRT_KodTarak where Id=@kodtar_id)
	BEGIN				
		DECLARE @sorrend NVARCHAR(32)
		SET @sorrend = (SELECT COALESCE(MAX(CAST(Sorrend AS Int))+1,1) FROM krt_kodtarak where kodcsoport_id=@p_KcsId)
		SET @sorrend = RIGHT(''00000'' + CONVERT(NVARCHAR(32), @sorrend), 5)
		INSERT INTO [dbo].[KRT_KodTarak]
		   ([Id]
		   ,[Org]
		   ,[KodCsoport_Id]
		   ,[Kod]
		   ,[Sorrend]
		   ,[Nev]
		   ,[Modosithato]
		   ,[Letrehozo_id])
		VALUES
		   (@kodtar_id, -- Id
			@p_Org, -- Org
			@p_KcsId, -- KodCsoport_Id
			SUBSTRING(@kodtar_nev, 1, 64), -- Kod
			@sorrend, -- Sorrend
			@kodtar_nev, -- Nev
			1, -- Modosithato
			@p_Letrehozo -- Letrehozo_id
		);
	END
	ELSE
	BEGIN
		UPDATE [dbo].[KRT_KodTarak] SET [Nev]=@kodtar_nev, [Kod]=SUBSTRING(@kodtar_nev, 1, 64) WHERE [Id]=@kodtar_id
	END
END
'

SET @sql = REPLACE(@sql, '@p_Org', '''' + CAST(@Org AS NVARCHAR(36)) + '''')
SET @sql = REPLACE(@sql, '@p_KcsId', '''' + CAST(@KODCSOPORT_ID_7247 AS NVARCHAR(36)) + '''')
SET @sql = REPLACE(@sql, '@p_Letrehozo', '''' + CAST(@Letrehozo_Id AS NVARCHAR(36)) + '''')

EXEC sp_executesql @sql

----
DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek where nev='TUK' and org= @Org and getdate() between ervkezd and ervvege) 
IF @isTUK = '1'
BEGIN

	declare @ControlTypeSourceTextBox nvarchar(64) = 'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls'
	
	--declare @ControlTypeSourceDropDownList nvarchar(64) = '~/Component/KodTarakDropDownList.ascx'
	declare @ControlTypeSourceListBox nvarchar(64) = '~/Component/KodTarakListBox.ascx'
	
	declare @now datetime
	set @now = GETDATE()

	declare @Tranz_id uniqueidentifier = 'C3A05D27-8946-4B7A-BAA9-5D49D334F7DF'

	declare @objMetaDefId_SZBT_metaadatai uniqueidentifier = '29AB86FC-61BD-E711-80C7-00155D027E9B'

	declare @objMetaDefId_FelhasznaloiEngedely_metaadatai uniqueidentifier = 'CFEBB7C0-AEBD-E711-80C7-00155D027E9B'

	-- SZBT - 'Selejtez�s iktat�sz�ma' t�rgysz�
	declare @targyszoId_SZBT_SelejtezesIktatoszama uniqueidentifier = '5F7C3921-1AE6-4FA8-8C0B-AC5FD52809BA'
	declare @Tipus char(1) = '1'

	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_SZBT_SelejtezesIktatoszama)
	BEGIN
		print 'insert - SZBT - Selejtez�s iktat�sz�ma'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
			,[Org]
			,[Tipus]
			,[TargySzavak]
			,[BelsoAzonosito]
			,[ControlTypeSource]
			,[ControlTypeDataSource]
			,[Letrehozo_id]
			,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_SZBT_SelejtezesIktatoszama
			,@Org
			,@Tipus
			,'Selejtez�s iktat�sz�ma'
			,'Selejtezes iktat�sz�ma'
			,@ControlTypeSourceTextBox
			,NULL
			,@Letrehozo_Id
			,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord m�r l�tezik: EREC_TargySzavak: SZBT - Selejtez�s iktat�sz�ma'
	END

	-- EREC_Obj_MetaAdatai - SZBT - Selejtez�s iktat�sz�ma
	declare @recordId uniqueidentifier = '60B3A21E-F85A-4153-890F-E0E8789F296B'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - SZBT metaadatai - Selejtez�s iktat�sz�ma'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
			,[Obj_MetaDefinicio_Id]
			,[Targyszavak_Id]
			,[Sorszam]
			,[Opcionalis]
			,[Ismetlodo]
			,[Letrehozo_id]
			,[Tranz_id]
		)
		VALUES
		(
			@recordId
			,@objMetaDefId_SZBT_metaadatai
			,@targyszoId_SZBT_SelejtezesIktatoszama
			,20
			,'1'
			,'0'
			,@Letrehozo_Id
			,@Tranz_id
		)
	END

	-- sorrend
	UPDATE EREC_Obj_MetaAdatai SET Sorszam=21 WHERE [Id]='BD3B81A4-0628-46C8-9FB8-86ACBF5ADE24' -- SZBT selejtez�s ideje alulra, iktat�sz�m al�

	-- Felhaszn�l�i enged�ly - 'Jogosults�g' lista t�rgysz�
	declare @targyszoId_JogosultsagLista uniqueidentifier = '0A78D660-DED6-4BC0-B06B-029F8A75D0D8'
	set @Tipus = '1'
	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_JogosultsagLista)
	BEGIN
		print 'insert - Felhaszn�l�i enged�ly - Jogosults�g lista'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
		   ,[Org]
		   ,[Tipus]
		   ,[TargySzavak]
		   ,[BelsoAzonosito]
		   ,[ControlTypeSource]
		   ,[ControlTypeDataSource]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_JogosultsagLista
		   ,@Org
		   ,@Tipus
		   ,'Jogosults�g lista'
		   ,'Jogosultsag lista'
		   ,@ControlTypeSourceListBox
		   ,@KODCSOPORT_Kod_7247
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord m�r l�tezik: EREC_TargySzavak: Felhaszn�l�i enged�ly - Jogosults�g lista'
	END

	-- EREC_Obj_MetaAdatai - Felhasznal�i enged�ly metaadatai - Jogosults�g lista
	set @recordId = '66D48EAB-2271-4E99-AC06-8DFCFB5CC4B3'

	IF NOT EXISTS (SELECT 1 FROM EREC_Obj_MetaAdatai WHERE Id = @recordId)
	BEGIN
		print 'insert - Felhasznal�i enged�ly metaadatai - Jogosults�g lista'

		INSERT INTO EREC_Obj_MetaAdatai
		(
			[Id]
		   ,[Obj_MetaDefinicio_Id]
		   ,[Targyszavak_Id]
		   ,[Sorszam]
		   ,[Opcionalis]
		   ,[Ismetlodo]
		   ,[Letrehozo_id]
		   ,[Tranz_id]
		)
		VALUES
		(
			@recordId
		   ,@objMetaDefId_FelhasznaloiEngedely_metaadatai
		   ,@targyszoId_JogosultsagLista
		   ,0
		   ,'1'
		   ,'1'
		   ,@Letrehozo_Id
		   ,@Tranz_id
		)
	END

	-- K�dcsoport felv�tele: Jogosults�g lista
	IF not exists (select 1 from KRT_Kodcsoportok where Id=@KODCSOPORT_ID_7247)
	BEGIN
		PRINT 'K�dcsoport felv�tele: ' + @KODCSOPORT_Kod_7247

		INSERT INTO KRT_KodCsoportok
		(
			 Id
			,Kod
			,Nev
			,Modosithato
			,BesorolasiSema
		) 
		values
		(
			@KODCSOPORT_ID_7247
			,@KODCSOPORT_Kod_7247
			,@KODCSOPORT_Nev_7247
			,'1' -- Modosithato
			,'0' -- BesorolasiSema
		)
	END
	
	-- K�dt�rak felv�tele: Jogosults�g lista
	BEGIN
		PRINT 'K�dt�r rekordok felv�tele a k�dcsoportba: ' + @KODCSOPORT_Kod_7247
		EXEC #Insert_Kodtar '1D77F19C-C366-468F-819A-3E9D114B62A3', '�llami vagy k�zfeladat v�grehajt�sa �rdek�ben t�rt�n� �gyint�z�s, feldolgoz�s';
		EXEC #Insert_Kodtar '863ACF3D-B76C-46E0-9589-CF4BB871DD6C', 'min�s�tett adat nyilv�ntart�s�val kapcsolatos valamennyi tev�kenys�g';
		EXEC #Insert_Kodtar 'BBAEEE87-1949-41B6-8A13-0FE74E4DA8CD', 'min�s�tett adat birtokban tart�sa';
		EXEC #Insert_Kodtar '711E31D7-3D0F-4594-B2A3-3F152ECCB8C3', 'min�s�t�si jel�l�s megism�tl�se';
		EXEC #Insert_Kodtar '815E8AD5-9BF6-4775-B641-1F9B0BEB90F9', 'min�s�tett adat m�sol�sa, sokszoros�t�sa';
		EXEC #Insert_Kodtar 'AF4C7581-515E-41F7-809E-A169864BC7A0', 'min�s�tett adat ford�t�sa';
		EXEC #Insert_Kodtar '24579EC5-4C3D-4A0C-BC1D-CAC2AB23C414', 'kivonat k�sz�t�se';
		EXEC #Insert_Kodtar 'FCB6B3FD-560D-40E6-A19A-7929C1C2B388', 'szerven bel�li �tad�s';
		EXEC #Insert_Kodtar '6CDF38FA-DAFC-407A-B976-215C47EB4BF1', 'szerven k�v�lre tov�bb�t�s';
		EXEC #Insert_Kodtar '33AA2BAF-5AAE-47FE-B278-42E5DDFAE67D', 'szerven k�v�lre sz�ll�t�s';
		EXEC #Insert_Kodtar '568459D2-E9F9-4B4A-A9CA-FF9ED4127A05', 'selejtez�s illetve megsemmis�t�s';
		EXEC #Insert_Kodtar '0E2847AB-C02C-4E6A-B06C-693ECDFA3F47', 'felhaszn�l�i enged�ly kiad�sa';
		EXEC #Insert_Kodtar '01ED5D88-3EBD-4C10-858E-AE2F1CCB1025', 'megismer�si enged�ly kiad�sa';
		EXEC #Insert_Kodtar '6B416DE8-10D5-4F21-9FDA-1008014C50E1', 'nemzeti min�s�tett adat fel�lvizsg�lata';
		EXEC #Insert_Kodtar '1F669B1C-31D7-4E86-870F-27007D723534', 'min�s�tett adat k�lf�ldi szem�ly vagy k�lf�ldi szerv r�sz�re hozz�f�rhet�v� t�tel�nek enged�lyez�se';
		EXEC #Insert_Kodtar '1D9A337A-428A-45C8-B3BB-873D4C809BBC', 'min�s�tett adat k�lf�ldre vitel�nek vagy k�lf�ldr�l val� behozatal�nak enged�lyez�se';
		EXEC #Insert_Kodtar '4D87A770-E926-45F3-BC6C-B305CE24C540', 'titoktart�si k�telezetts�g al�li felment�s';
		EXEC #Insert_Kodtar 'B5F4C228-960B-4BED-883F-76ECCB521935', 'min�s�t�si jel�l�s megism�tl�s�nek megtilt�sa';
	END

	-- Titoktart�si nyilatkozat sorrend
	UPDATE EREC_Obj_MetaAdatai SET Sorszam=20 WHERE [Id]='14E7A38C-E4E0-4FF7-9149-3B9C633812AE' -- Titoktart�si nyilatkozat selejtez�s iktat�sz�ma
	UPDATE EREC_Obj_MetaAdatai SET Sorszam=21 WHERE [Id]='944DD962-7B24-41FD-ABAD-08BB67FF2F04' -- Titoktart�si nyilatkozat selejtez�s ideje
	
	-- Titoktart�si nyilatkozat - 'Al��r�s ideje' t�rgysz�
	declare @ControlTypeSourceCalendar nvarchar(64) = '~/Component/CalendarControl.ascx'
	declare @targyszoId_TitoktartasiNyilatkozat_AlairasIdeje uniqueidentifier = '68EFCCBC-EC4B-4586-A6B9-88A0AA27BDF2'
	declare @Tipus char(1) = '1'

	IF NOT EXISTS (SELECT 1 FROM EREC_TargySzavak WHERE Id = @targyszoId_TitoktartasiNyilatkozat_AlairasIdeje)
	BEGIN
		print 'insert - Titoktart�si nyilatkozat - Al��r�s ideje'

		INSERT INTO EREC_TargySzavak
		(
			[Id]
			,[Org]
			,[Tipus]
			,[TargySzavak]
			,[BelsoAzonosito]
			,[ControlTypeSource]
			,[ControlTypeDataSource]
			,[Letrehozo_id]
			,[Tranz_id]
		)
		VALUES
		(
			@targyszoId_TitoktartasiNyilatkozat_AlairasIdeje
			,@Org
			,@Tipus
			,'Al��r�s ideje'
			,'Alairas_ideje'
			,@ControlTypeSourceCalendar
			,NULL
			,@Letrehozo_Id
			,@Tranz_id
		)
	END
	ELSE
	BEGIN
		print 'Rekord m�r l�tezik: EREC_TargySzavak: Titoktart�si nyilatkozat - Al��r�s ideje'
	END

	-- Titoktart�si nyilatkozat - 'Kibocs�t�s ideje' helyett 'Al��r�s ideje'

	UPDATE EREC_Obj_MetaAdatai SET Targyszavak_Id=@targyszoId_TitoktartasiNyilatkozat_AlairasIdeje WHERE [Id]='2C9681DF-B1BD-E711-80C7-00155D027E9B'
END

