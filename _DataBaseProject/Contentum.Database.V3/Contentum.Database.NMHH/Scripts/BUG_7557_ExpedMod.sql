﻿------------------------------------------------------------------------
DECLARE @org nvarchar(100) 
SET @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (select kod from KRT_Orgok where id=@org) 

IF @org_kod = 'NMHH'
	BEGIN
		PRINT 'START BLG_7557'
		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '6F1B8B2B-5F82-245F-6D73-AE6C5D395351') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id] ,[Org] ,[KodCsoport_Id] ,[Kod] ,[Nev] ,[RovidNev] ,[Egyeb] ,[Modosithato] ,[Sorrend] ,[Note]) VALUES ('6F1B8B2B-5F82-245F-6D73-AE6C5D395351' ,'450B510A-7CAA-46B0-83E3-18445C0C53A9' ,'BF162994-AB06-4DBF-AAC2-176FAFD6C7EF' ,'UGYFELKAPU' ,'Ügyfélkapu' ,'Ügyfélkapu' ,'Ügyfélkapu' ,'0' ,'12' ,GETDATE() ) END 
		IF NOT EXISTS (SELECT 1 FROM KRT_KodTarak WHERE ID = '0AB74D79-55E2-4881-0A96-68A5A2B73871') BEGIN INSERT INTO [dbo].[KRT_KodTarak] ([Id] ,[Org] ,[KodCsoport_Id] ,[Kod] ,[Nev] ,[RovidNev] ,[Egyeb] ,[Modosithato] ,[Sorrend] ,[Note]) VALUES ('0AB74D79-55E2-4881-0A96-68A5A2B73871' ,'450B510A-7CAA-46B0-83E3-18445C0C53A10' ,'BF162994-AB06-4DBF-AAC2-176FAFD6C7EF' ,'CEGKAPU' ,'Cégkapu' ,'Cégkapu' ,'Cégkapu' ,'0' ,'13' ,GETDATE() ) END  

		IF EXISTS(	SELECT 1 
					FROM KRT_KodTarak  
					WHERE KodCsoport_Id ='BF162994-AB06-4DBF-AAC2-176FAFD6C7EF' 
						  AND KOD = 07 
						  AND Nev = 'Titkos kapcsolati kód')
			BEGIN
				UPDATE KRT_KodTarak
				SET Nev = 'Hivatali kapu',
					RovidNev = 'HKP',
					Egyeb = 'Hivatali kapu',
					Note = GETDATE()
				WHERE KodCsoport_Id ='BF162994-AB06-4DBF-AAC2-176FAFD6C7EF' 
					  AND KOD = '07' 

				PRINT 'UPDATE KRT_KodTarak 07 CIM_TIPUS TO Hivatali kapu'
			END
	END

IF @org_kod = 'NMHH'
	BEGIN
		------------------------------------
		DECLARE @id			UNIQUEIDENTIFIER
		DECLARE @adoszam	NVARCHAR(100)
		DECLARE @adoszamsub	NVARCHAR(100)
		DECLARE @partnerId	UNIQUEIDENTIFIER
		
		DECLARE CURSOR_7557 CURSOR FOR  
		SELECT Id, Adoszam,SUBSTRING(Adoszam,1,8),Partner_Id AS AdoszamSub FROM KRT_Vallalkozasok WHERE Adoszam IS NOT NULL
		OPEN CURSOR_7557;  

		FETCH NEXT FROM CURSOR_7557 INTO @id,@adoszam,@adoszamsub,@partnerId;  
		DECLARE @CIM_TIPUS_ID NVARCHAR(MAX)
		SELECT TOP 1 @CIM_TIPUS_ID = Kod 
			FROM KRT_kodtarak 
			WHERE Kod = 'CEGKAPU' 
			AND KodCsoport_Id = (SELECT TOP 1 Id FROM KRT_KodCsoportok WHERE Kod = 'CIM_TIPUS')
		IF (@CIM_TIPUS_ID IS NOT NULL)
		BEGIN
			WHILE @@FETCH_STATUS = 0  
			BEGIN  
				DECLARE @CIMID UNIQUEIDENTIFIER
				SELECT TOP 1 @CIMID = Id FROM KRT_Cimek WHERE Nev = @adoszamsub AND Tipus = @CIM_TIPUS_ID
				IF (@CIMID IS NULL)
					BEGIN
						SET @CIMID = NEWID()

						INSERT INTO KRT_Cimek (Id,Nev,Tipus,Note, Kategoria, CimTobbi, Forras)
						VALUES (@CIMID, @adoszamsub, @CIM_TIPUS_ID, GETDATE(),'K',@adoszamsub, 'E')

						PRINT 'ADD NEW KRT_Cimek'
					END

				IF NOT EXISTS (SELECT 1 FROM KRT_PartnerCimek WHERE Partner_id= @partnerId AND Cim_Id =@CIMID )
					BEGIN
						INSERT INTO KRT_PartnerCimek (Id, Partner_id, Cim_Id, Note)
						VALUES (NEWID(), @partnerId, @CIMID, GETDATE())
						PRINT 'ADD NEW KRT_PartnerCimek'
					END
		
				FETCH NEXT FROM CURSOR_7557 INTO @id,@adoszam,@adoszamsub,@partnerId; 
			END  
		END

		CLOSE CURSOR_7557;  
		DEALLOCATE CURSOR_7557;  
END
GO			
------------------------------------------------------------------------