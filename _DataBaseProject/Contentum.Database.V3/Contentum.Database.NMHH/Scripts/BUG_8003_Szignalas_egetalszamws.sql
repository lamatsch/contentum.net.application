-- IRAT_INTEZKEDESREATVETEL_ELERHETO paraméter
--

DECLARE @Org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100)
declare @ertek nvarchar(400)

SET @org_kod = (select kod from KRT_Orgok where id=@Org) 

SET @record_id = '589DCBD2-C50F-4887-A777-D7F50C273356'

IF @org_kod = 'NMHH'
	SET @ertek = '1'
ELSE
	SET @ertek = '0'

IF NOT EXISTS (SELECT 1 FROM [KRT_Parameterek] WHERE [Id] = @record_id)
BEGIN 
	Print 'INSERT - KRT_Parameterek - IRAT_INTEZKEDESREATVETEL_ELERHETO  - ' + @ertek 
	INSERT INTO [KRT_Parameterek](
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,@Org
		,'IRAT_INTEZKEDESREATVETEL_ELERHETO'
		,@ertek
		,'1'
		,'Ha a paraméter értéke 1, akkor az "Irat átvétel intézkedésre" funkcionalitás elérhető.'
		)
END

-- IRAT_INTEZKEDESREATVETEL_SAKKORA_DEFAULT paraméter
--

SET @record_id = '77AFB29F-B83B-45DA-A165-0E12E8D4E3A6'

IF @org_kod = 'NMHH'
	SET @ertek = '1'
ELSE
	SET @ertek = '0'

IF NOT EXISTS (SELECT 1 FROM [KRT_Parameterek] WHERE [Id] = @record_id)
BEGIN 
	Print 'INSERT - KRT_Parameterek - IRAT_INTEZKEDESREATVETEL_SAKKORA_DEFAULT  - ' + @ertek 
	INSERT INTO [KRT_Parameterek](
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,@Org
		,'IRAT_INTEZKEDESREATVETEL_SAKKORA_DEFAULT'
		,@ertek
		,'1'
		,'"Irat átvétel intézkedésre" funkcionalitásnál az alapértelmezett sakkóra érték. A paraméter értékei a sakkóra kódok lehetnek.'
		)
 END

 ---- IRAT_ALLAPOT kódcsoport kibővítése
 --

DECLARE @kodcsoport_id uniqueidentifier
SET @kodcsoport_id = (SELECT [Id] FROM [KRT_KodCsoportok] WHERE [Kod]='IRAT_ALLAPOT')
 
SET @record_id = '769CB4F6-4D6E-48B5-8B8C-92D878A644E7'
IF NOT EXISTS (SELECT 1 FROM [KRT_KodTarak] WHERE [Id]=@record_id)
BEGIN
	PRINT 'INSERT - KRT_KodTarak - @record_id=' + convert(NVARCHAR(100), @record_id)
    INSERT INTO [KRT_KodTarak](
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,Modosithato
    ,Sorrend
    ) 
	VALUES
	(
    @kodcsoport_id
    ,@Org
    ,@record_id
    ,'07'
    ,'Intézkedés alatt'
	,'0'
    ,'10'
    )
END

SET @record_id = 'F6430024-F8FF-4404-BAE6-BF2ACC27D6A9'
IF NOT EXISTS (SELECT 1 FROM [KRT_KodTarak] WHERE [Id]=@record_id)
BEGIN
	PRINT 'INSERT - KRT_KodTarak - @record_id=' + convert(NVARCHAR(100), @record_id)
    INSERT INTO [KRT_KodTarak](
    KodCsoport_Id
    ,Org
    ,Id
    ,Kod
    ,Nev
	,Modosithato
    ,Sorrend
    ) 
	VALUES 
	(
    @kodcsoport_id
    ,@Org
    ,@record_id
    ,'09'
    ,'Elintézett'
	,'0'
    ,'11'
    )
END

---- IratAtvetelIntezkedesre funkció kód
--
DECLARE @alkalmazas_id uniqueidentifier
SET @alkalmazas_id = '20B67BA7-F156-46B2-93BF-B7FD2E8C9FA3'

DECLARE @funkcio_id uniqueidentifier
SET @funkcio_id = '32D4AB77-2E3B-4664-9670-927E996D54DD'

IF NOT EXISTS (SELECT 1 FROM [KRT_Funkciok] WHERE [Id]=@funkcio_id)
BEGIN
	PRINT 'INSERT - KRT_Funkciok - IratAtvetelIntezkedesre'

	INSERT INTO [KRT_Funkciok](
		Alkalmazas_Id
		,Modosithato
		,Id
		,ObjTipus_Id_AdatElem
		,Muvelet_Id
		,Kod
		,Nev
	) 
	VALUES 
	(
		@alkalmazas_id,
		'0',
		@funkcio_id,
		'AA5E7BBA-96A0-4C17-8709-06A6D297E107', -- ObjTipus_Id_AdatElem: EREC_IraIratok
		NULL, -- Muvelet_Id
		'IratAtvetelIntezkedesre',
		'Irat átvétel intézkedésre'
	)
END

-- IratAtvetelIntezkedesre funkció FEJLESZTO szerepkörhöz kötése
--

DECLARE @FEJLESZTO_Szerepkor_Id uniqueidentifier
SET @FEJLESZTO_Szerepkor_Id = 'E855F681-36ED-41B6-8413-576FCB5D1542'

SET @record_id = 'DBE424BC-3397-423A-ACF1-2CA12ABC7FEF'
IF NOT EXISTS (SELECT 1 FROM [KRT_Szerepkor_Funkcio] WHERE [Id]=@record_id)
BEGIN
	PRINT 'INSERT - KRT_Szerepkor_Funkcio - @FEJLESZTO_Szerepkor_Id=' + convert(NVARCHAR(100), @FEJLESZTO_Szerepkor_Id)
	INSERT INTO [KRT_Szerepkor_Funkcio](
		Id,
		Szerepkor_Id,
		Funkcio_Id
	) 
	VALUES 
	(
		@record_id,
		@FEJLESZTO_Szerepkor_Id,
		@funkcio_id
	)
END 

-- IratAtvetelIntezkedesre funkció DEFAULT_SZEREPKOR rendszerparaméter által megadott szerepkörhöz kötése
--

DECLARE @DEFAULT_Szerepkor_Id uniqueidentifier
SET @DEFAULT_Szerepkor_Id = (SELECT [Id] FROM [KRT_Szerepkorok] WHERE [Nev] = (SELECT [Ertek] FROM [KRT_Parameterek] WHERE [Nev] = 'DEFAULT_SZEREPKOR'))

IF @DEFAULT_Szerepkor_Id IS NOT NULL AND @DEFAULT_Szerepkor_Id <> @FEJLESZTO_Szerepkor_Id
BEGIN
	PRINT 'INSERT - KRT_Szerepkor_Funkcio - @DEFAULT_Szerepkor_Id=' + convert(NVARCHAR(100), @DEFAULT_Szerepkor_Id)
	SET @record_id = '60BA2AC0-AB17-45B6-ABA1-76E057DEFAE4'
	INSERT INTO [KRT_Szerepkor_Funkcio](
		Id,
		Szerepkor_Id,
		Funkcio_Id
	) 
	VALUES 
	(
		@record_id,
		@DEFAULT_Szerepkor_Id,
		@funkcio_id
	)
END

GO
-------------- end of BUG_8003
