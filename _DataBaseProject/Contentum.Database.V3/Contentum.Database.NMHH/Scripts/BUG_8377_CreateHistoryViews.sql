--select REPLACE('IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = ''#view#'')
--BEGIN
--	PRINT ''create view [dbo].[#view#]''
--	exec dbo.sp_executesql @statement=N''create view [dbo].[#view#] as select * from $(HISTORYDB).dbo.#view#''
--END

--GO','#view#',name), * from sys.views
--where name like '%History'
--order by name asc

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_AgazatiJelekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_AgazatiJelekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_AgazatiJelekHistory] as select * from $(HISTORYDB).dbo.EREC_AgazatiJelekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_Alairas_Folyamat_TetelekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_Alairas_Folyamat_TetelekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_Alairas_Folyamat_TetelekHistory] as select * from $(HISTORYDB).dbo.EREC_Alairas_Folyamat_TetelekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_Alairas_FolyamatokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_Alairas_FolyamatokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_Alairas_FolyamatokHistory] as select * from $(HISTORYDB).dbo.EREC_Alairas_FolyamatokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_CsatolmanyokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_CsatolmanyokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_CsatolmanyokHistory] as select * from $(HISTORYDB).dbo.EREC_CsatolmanyokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_eBeadvanyCsatolmanyokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_eBeadvanyCsatolmanyokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_eBeadvanyCsatolmanyokHistory] as select * from $(HISTORYDB).dbo.EREC_eBeadvanyCsatolmanyokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_eBeadvanyokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_eBeadvanyokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_eBeadvanyokHistory] as select * from $(HISTORYDB).dbo.EREC_eBeadvanyokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_eMailBoritekCimeiHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_eMailBoritekCimeiHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_eMailBoritekCimeiHistory] as select * from $(HISTORYDB).dbo.EREC_eMailBoritekCimeiHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_eMailBoritekCsatolmanyokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_eMailBoritekCsatolmanyokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_eMailBoritekCsatolmanyokHistory] as select * from $(HISTORYDB).dbo.EREC_eMailBoritekCsatolmanyokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_eMailBoritekokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_eMailBoritekokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_eMailBoritekokHistory] as select * from $(HISTORYDB).dbo.EREC_eMailBoritekokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_eMailFiokokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_eMailFiokokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_eMailFiokokHistory] as select * from $(HISTORYDB).dbo.EREC_eMailFiokokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_FeladatDefinicioHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_FeladatDefinicioHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_FeladatDefinicioHistory] as select * from $(HISTORYDB).dbo.EREC_FeladatDefinicioHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_FeladatErtesitesHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_FeladatErtesitesHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_FeladatErtesitesHistory] as select * from $(HISTORYDB).dbo.EREC_FeladatErtesitesHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_Hatarid_ObjektumokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_Hatarid_ObjektumokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_Hatarid_ObjektumokHistory] as select * from $(HISTORYDB).dbo.EREC_Hatarid_ObjektumokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_HataridosFeladatokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_HataridosFeladatokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_HataridosFeladatokHistory] as select * from $(HISTORYDB).dbo.EREC_HataridosFeladatokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IraElosztoivekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IraElosztoivekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IraElosztoivekHistory] as select * from $(HISTORYDB).dbo.EREC_IraElosztoivekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IraElosztoivTetelekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IraElosztoivTetelekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IraElosztoivTetelekHistory] as select * from $(HISTORYDB).dbo.EREC_IraElosztoivTetelekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IraIktatoKonyvekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IraIktatoKonyvekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IraIktatoKonyvekHistory] as select * from $(HISTORYDB).dbo.EREC_IraIktatoKonyvekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IraIratokDokumentumokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IraIratokDokumentumokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IraIratokDokumentumokHistory] as select * from $(HISTORYDB).dbo.EREC_IraIratokDokumentumokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IraIratokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IraIratokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IraIratokHistory] as select * from $(HISTORYDB).dbo.EREC_IraIratokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IraIrattariTetelekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IraIrattariTetelekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IraIrattariTetelekHistory] as select * from $(HISTORYDB).dbo.EREC_IraIrattariTetelekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IraJegyzekekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IraJegyzekekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IraJegyzekekHistory] as select * from $(HISTORYDB).dbo.EREC_IraJegyzekekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IraJegyzekTetelekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IraJegyzekTetelekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IraJegyzekTetelekHistory] as select * from $(HISTORYDB).dbo.EREC_IraJegyzekTetelekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IraKezbesitesiFejekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IraKezbesitesiFejekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IraKezbesitesiFejekHistory] as select * from $(HISTORYDB).dbo.EREC_IraKezbesitesiFejekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IraKezbesitesiTetelekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IraKezbesitesiTetelekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IraKezbesitesiTetelekHistory] as select * from $(HISTORYDB).dbo.EREC_IraKezbesitesiTetelekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IraKezFeljegyzesekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IraKezFeljegyzesekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IraKezFeljegyzesekHistory] as select * from $(HISTORYDB).dbo.EREC_IraKezFeljegyzesekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IraOnkormAdatokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IraOnkormAdatokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IraOnkormAdatokHistory] as select * from $(HISTORYDB).dbo.EREC_IraOnkormAdatokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_Irat_IktatokonyveiHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_Irat_IktatokonyveiHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_Irat_IktatokonyveiHistory] as select * from $(HISTORYDB).dbo.EREC_Irat_IktatokonyveiHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IratAlairokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IratAlairokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IratAlairokHistory] as select * from $(HISTORYDB).dbo.EREC_IratAlairokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IratelemKapcsolatokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IratelemKapcsolatokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IratelemKapcsolatokHistory] as select * from $(HISTORYDB).dbo.EREC_IratelemKapcsolatokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IratKapcsolatokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IratKapcsolatokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IratKapcsolatokHistory] as select * from $(HISTORYDB).dbo.EREC_IratKapcsolatokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IratMellekletekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IratMellekletekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IratMellekletekHistory] as select * from $(HISTORYDB).dbo.EREC_IratMellekletekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IratMetaDefinicioHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IratMetaDefinicioHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IratMetaDefinicioHistory] as select * from $(HISTORYDB).dbo.EREC_IratMetaDefinicioHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IrattariHelyekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IrattariHelyekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IrattariHelyekHistory] as select * from $(HISTORYDB).dbo.EREC_IrattariHelyekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IrattariKikeroHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IrattariKikeroHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IrattariKikeroHistory] as select * from $(HISTORYDB).dbo.EREC_IrattariKikeroHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_IrattariTetel_IktatokonyvHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_IrattariTetel_IktatokonyvHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_IrattariTetel_IktatokonyvHistory] as select * from $(HISTORYDB).dbo.EREC_IrattariTetel_IktatokonyvHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_KuldBekuldokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_KuldBekuldokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_KuldBekuldokHistory] as select * from $(HISTORYDB).dbo.EREC_KuldBekuldokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_KuldDokumentumokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_KuldDokumentumokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_KuldDokumentumokHistory] as select * from $(HISTORYDB).dbo.EREC_KuldDokumentumokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_Kuldemeny_IratPeldanyaiHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_Kuldemeny_IratPeldanyaiHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_Kuldemeny_IratPeldanyaiHistory] as select * from $(HISTORYDB).dbo.EREC_Kuldemeny_IratPeldanyaiHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_KuldKapcsolatokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_KuldKapcsolatokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_KuldKapcsolatokHistory] as select * from $(HISTORYDB).dbo.EREC_KuldKapcsolatokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_KuldKezFeljegyzesekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_KuldKezFeljegyzesekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_KuldKezFeljegyzesekHistory] as select * from $(HISTORYDB).dbo.EREC_KuldKezFeljegyzesekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_KuldKuldemenyekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_KuldKuldemenyekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_KuldKuldemenyekHistory] as select * from $(HISTORYDB).dbo.EREC_KuldKuldemenyekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_KuldMellekletekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_KuldMellekletekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_KuldMellekletekHistory] as select * from $(HISTORYDB).dbo.EREC_KuldMellekletekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_KuldTertivevenyekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_KuldTertivevenyekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_KuldTertivevenyekHistory] as select * from $(HISTORYDB).dbo.EREC_KuldTertivevenyekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_MellekletekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_MellekletekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_MellekletekHistory] as select * from $(HISTORYDB).dbo.EREC_MellekletekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_Obj_MetaAdataiHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_Obj_MetaAdataiHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_Obj_MetaAdataiHistory] as select * from $(HISTORYDB).dbo.EREC_Obj_MetaAdataiHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_Obj_MetaDefinicioHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_Obj_MetaDefinicioHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_Obj_MetaDefinicioHistory] as select * from $(HISTORYDB).dbo.EREC_Obj_MetaDefinicioHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_Obj_MetaXmlHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_Obj_MetaXmlHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_Obj_MetaXmlHistory] as select * from $(HISTORYDB).dbo.EREC_Obj_MetaXmlHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_ObjektumTargyszavaiHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_ObjektumTargyszavaiHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_ObjektumTargyszavaiHistory] as select * from $(HISTORYDB).dbo.EREC_ObjektumTargyszavaiHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_PldIratPeldanyokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_PldIratPeldanyokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_PldIratPeldanyokHistory] as select * from $(HISTORYDB).dbo.EREC_PldIratPeldanyokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_PldKapjakMegHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_PldKapjakMegHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_PldKapjakMegHistory] as select * from $(HISTORYDB).dbo.EREC_PldKapjakMegHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_SzamlakHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_SzamlakHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_SzamlakHistory] as select * from $(HISTORYDB).dbo.EREC_SzamlakHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_SzignalasiJegyzekekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_SzignalasiJegyzekekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_SzignalasiJegyzekekHistory] as select * from $(HISTORYDB).dbo.EREC_SzignalasiJegyzekekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_TargySzavakHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_TargySzavakHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_TargySzavakHistory] as select * from $(HISTORYDB).dbo.EREC_TargySzavakHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_TomegesIktatasFolyamatHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_TomegesIktatasFolyamatHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_TomegesIktatasFolyamatHistory] as select * from $(HISTORYDB).dbo.EREC_TomegesIktatasFolyamatHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_TomegesIktatasTetelekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_TomegesIktatasTetelekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_TomegesIktatasTetelekHistory] as select * from $(HISTORYDB).dbo.EREC_TomegesIktatasTetelekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_UgyiratKapcsolatokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_UgyiratKapcsolatokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_UgyiratKapcsolatokHistory] as select * from $(HISTORYDB).dbo.EREC_UgyiratKapcsolatokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_UgyiratObjKapcsolatokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_UgyiratObjKapcsolatokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_UgyiratObjKapcsolatokHistory] as select * from $(HISTORYDB).dbo.EREC_UgyiratObjKapcsolatokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_UgyKezFeljegyzesekHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_UgyKezFeljegyzesekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_UgyKezFeljegyzesekHistory] as select * from $(HISTORYDB).dbo.EREC_UgyKezFeljegyzesekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_UgyUgyiratdarabokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_UgyUgyiratdarabokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_UgyUgyiratdarabokHistory] as select * from $(HISTORYDB).dbo.EREC_UgyUgyiratdarabokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'EREC_UgyUgyiratokHistory')
BEGIN
	PRINT 'create view [dbo].[EREC_UgyUgyiratokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[EREC_UgyUgyiratokHistory] as select * from $(HISTORYDB).dbo.EREC_UgyUgyiratokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'INT_AutoEUzenetSzabalyHistory')
BEGIN
	PRINT 'create view [dbo].[INT_AutoEUzenetSzabalyHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[INT_AutoEUzenetSzabalyHistory] as select * from $(HISTORYDB).dbo.INT_AutoEUzenetSzabalyHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'INT_AutoEUzenetSzabalyNaploHistory')
BEGIN
	PRINT 'create view [dbo].[INT_AutoEUzenetSzabalyNaploHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[INT_AutoEUzenetSzabalyNaploHistory] as select * from $(HISTORYDB).dbo.INT_AutoEUzenetSzabalyNaploHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'INT_ExternalIDsHistory')
BEGIN
	PRINT 'create view [dbo].[INT_ExternalIDsHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[INT_ExternalIDsHistory] as select * from $(HISTORYDB).dbo.INT_ExternalIDsHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'INT_LogHistory')
BEGIN
	PRINT 'create view [dbo].[INT_LogHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[INT_LogHistory] as select * from $(HISTORYDB).dbo.INT_LogHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'INT_ManagedEmailAddressHistory')
BEGIN
	PRINT 'create view [dbo].[INT_ManagedEmailAddressHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[INT_ManagedEmailAddressHistory] as select * from $(HISTORYDB).dbo.INT_ManagedEmailAddressHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'INT_ModulokHistory')
BEGIN
	PRINT 'create view [dbo].[INT_ModulokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[INT_ModulokHistory] as select * from $(HISTORYDB).dbo.INT_ModulokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'INT_ParameterekHistory')
BEGIN
	PRINT 'create view [dbo].[INT_ParameterekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[INT_ParameterekHistory] as select * from $(HISTORYDB).dbo.INT_ParameterekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_AlairtDokumentumokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_AlairtDokumentumokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_AlairtDokumentumokHistory] as select * from $(HISTORYDB).dbo.KRT_AlairtDokumentumokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_AlkalmazasokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_AlkalmazasokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_AlkalmazasokHistory] as select * from $(HISTORYDB).dbo.KRT_AlkalmazasokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_BankszamlaszamokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_BankszamlaszamokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_BankszamlaszamokHistory] as select * from $(HISTORYDB).dbo.KRT_BankszamlaszamokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_BarkodokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_BarkodokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_BarkodokHistory] as select * from $(HISTORYDB).dbo.KRT_BarkodokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_BarkodSavokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_BarkodSavokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_BarkodSavokHistory] as select * from $(HISTORYDB).dbo.KRT_BarkodSavokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_CimekHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_CimekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_CimekHistory] as select * from $(HISTORYDB).dbo.KRT_CimekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_CsoportokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_CsoportokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_CsoportokHistory] as select * from $(HISTORYDB).dbo.KRT_CsoportokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_CsoportTagokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_CsoportTagokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_CsoportTagokHistory] as select * from $(HISTORYDB).dbo.KRT_CsoportTagokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_DokuAlairasKapcsolatokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_DokuAlairasKapcsolatokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_DokuAlairasKapcsolatokHistory] as select * from $(HISTORYDB).dbo.KRT_DokuAlairasKapcsolatokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_DokumentumAlairasokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_DokumentumAlairasokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_DokumentumAlairasokHistory] as select * from $(HISTORYDB).dbo.KRT_DokumentumAlairasokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_DokumentumKapcsolatokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_DokumentumKapcsolatokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_DokumentumKapcsolatokHistory] as select * from $(HISTORYDB).dbo.KRT_DokumentumKapcsolatokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_DokumentumokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_DokumentumokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_DokumentumokHistory] as select * from $(HISTORYDB).dbo.KRT_DokumentumokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_Erintett_TablakHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_Erintett_TablakHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_Erintett_TablakHistory] as select * from $(HISTORYDB).dbo.KRT_Erintett_TablakHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_Extra_NapokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_Extra_NapokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_Extra_NapokHistory] as select * from $(HISTORYDB).dbo.KRT_Extra_NapokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_Felhasznalo_SzerepkorHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_Felhasznalo_SzerepkorHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_Felhasznalo_SzerepkorHistory] as select * from $(HISTORYDB).dbo.KRT_Felhasznalo_SzerepkorHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_Felhasznalok_Halozati_NyomtatoiHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_Felhasznalok_Halozati_NyomtatoiHistory] as select * from $(HISTORYDB).dbo.KRT_Felhasznalok_Halozati_NyomtatoiHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_FelhasznalokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_FelhasznalokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_FelhasznalokHistory] as select * from $(HISTORYDB).dbo.KRT_FelhasznalokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_FelhasznaloProfilokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_FelhasznaloProfilokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_FelhasznaloProfilokHistory] as select * from $(HISTORYDB).dbo.KRT_FelhasznaloProfilokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_ForditasokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_ForditasokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_ForditasokHistory] as select * from $(HISTORYDB).dbo.KRT_ForditasokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_FunkciokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_FunkciokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_FunkciokHistory] as select * from $(HISTORYDB).dbo.KRT_FunkciokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_FunkcioListaHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_FunkcioListaHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_FunkcioListaHistory] as select * from $(HISTORYDB).dbo.KRT_FunkcioListaHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_Halozati_NyomtatokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_Halozati_NyomtatokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_Halozati_NyomtatokHistory] as select * from $(HISTORYDB).dbo.KRT_Halozati_NyomtatokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_HelyettesitesekHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_HelyettesitesekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_HelyettesitesekHistory] as select * from $(HISTORYDB).dbo.KRT_HelyettesitesekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_KodCsoportokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_KodCsoportokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_KodCsoportokHistory] as select * from $(HISTORYDB).dbo.KRT_KodCsoportokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_KodTarakHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_KodTarakHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_KodTarakHistory] as select * from $(HISTORYDB).dbo.KRT_KodTarakHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_KodtarFuggosegHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_KodtarFuggosegHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_KodtarFuggosegHistory] as select * from $(HISTORYDB).dbo.KRT_KodtarFuggosegHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_KozteruletekHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_KozteruletekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_KozteruletekHistory] as select * from $(HISTORYDB).dbo.KRT_KozteruletekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_KozteruletTipusokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_KozteruletTipusokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_KozteruletTipusokHistory] as select * from $(HISTORYDB).dbo.KRT_KozteruletTipusokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_MappakHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_MappakHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_MappakHistory] as select * from $(HISTORYDB).dbo.KRT_MappakHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_MappaTartalmakHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_MappaTartalmakHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_MappaTartalmakHistory] as select * from $(HISTORYDB).dbo.KRT_MappaTartalmakHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_MappaUtakHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_MappaUtakHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_MappaUtakHistory] as select * from $(HISTORYDB).dbo.KRT_MappaUtakHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_MenukHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_MenukHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_MenukHistory] as select * from $(HISTORYDB).dbo.KRT_MenukHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_Modul_FunkcioHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_Modul_FunkcioHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_Modul_FunkcioHistory] as select * from $(HISTORYDB).dbo.KRT_Modul_FunkcioHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_ModulokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_ModulokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_ModulokHistory] as select * from $(HISTORYDB).dbo.KRT_ModulokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_MuveletekHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_MuveletekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_MuveletekHistory] as select * from $(HISTORYDB).dbo.KRT_MuveletekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_NezetekHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_NezetekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_NezetekHistory] as select * from $(HISTORYDB).dbo.KRT_NezetekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_ObjTipusokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_ObjTipusokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_ObjTipusokHistory] as select * from $(HISTORYDB).dbo.KRT_ObjTipusokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_OrgokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_OrgokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_OrgokHistory] as select * from $(HISTORYDB).dbo.KRT_OrgokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_OrszagokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_OrszagokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_OrszagokHistory] as select * from $(HISTORYDB).dbo.KRT_OrszagokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_ParameterekHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_ParameterekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_ParameterekHistory] as select * from $(HISTORYDB).dbo.KRT_ParameterekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_Partner_DokumentumokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_Partner_DokumentumokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_Partner_DokumentumokHistory] as select * from $(HISTORYDB).dbo.KRT_Partner_DokumentumokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_PartnerCimekHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_PartnerCimekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_PartnerCimekHistory] as select * from $(HISTORYDB).dbo.KRT_PartnerCimekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_PartnerekHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_PartnerekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_PartnerekHistory] as select * from $(HISTORYDB).dbo.KRT_PartnerekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_PartnerKapcsolatokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_PartnerKapcsolatokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_PartnerKapcsolatokHistory] as select * from $(HISTORYDB).dbo.KRT_PartnerKapcsolatokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_PartnerMinositesekHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_PartnerMinositesekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_PartnerMinositesekHistory] as select * from $(HISTORYDB).dbo.KRT_PartnerMinositesekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_RagSzamokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_RagSzamokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_RagSzamokHistory] as select * from $(HISTORYDB).dbo.KRT_RagSzamokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_RagszamSavokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_RagszamSavokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_RagszamSavokHistory] as select * from $(HISTORYDB).dbo.KRT_RagszamSavokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_SoapMessageLogHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_SoapMessageLogHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_SoapMessageLogHistory] as select * from $(HISTORYDB).dbo.KRT_SoapMessageLogHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_SzemelyekHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_SzemelyekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_SzemelyekHistory] as select * from $(HISTORYDB).dbo.KRT_SzemelyekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_Szerepkor_FunkcioHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_Szerepkor_FunkcioHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_Szerepkor_FunkcioHistory] as select * from $(HISTORYDB).dbo.KRT_Szerepkor_FunkcioHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_SzerepkorokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_SzerepkorokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_SzerepkorokHistory] as select * from $(HISTORYDB).dbo.KRT_SzerepkorokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_Tartomanyok_SzervezetekHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_Tartomanyok_SzervezetekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_Tartomanyok_SzervezetekHistory] as select * from $(HISTORYDB).dbo.KRT_Tartomanyok_SzervezetekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_TartomanyokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_TartomanyokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_TartomanyokHistory] as select * from $(HISTORYDB).dbo.KRT_TartomanyokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_TelepulesekHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_TelepulesekHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_TelepulesekHistory] as select * from $(HISTORYDB).dbo.KRT_TelepulesekHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_TemplateManagerHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_TemplateManagerHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_TemplateManagerHistory] as select * from $(HISTORYDB).dbo.KRT_TemplateManagerHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_Tranz_ObjHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_Tranz_ObjHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_Tranz_ObjHistory] as select * from $(HISTORYDB).dbo.KRT_Tranz_ObjHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_TranzakciokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_TranzakciokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_TranzakciokHistory] as select * from $(HISTORYDB).dbo.KRT_TranzakciokHistory'
END

GO
IF NOT EXISTS (SELECT 1 FROM sys.views WHERE name = 'KRT_VallalkozasokHistory')
BEGIN
	PRINT 'create view [dbo].[KRT_VallalkozasokHistory]'
	exec dbo.sp_executesql @statement=N'create view [dbo].[KRT_VallalkozasokHistory] as select * from $(HISTORYDB).dbo.KRT_VallalkozasokHistory'
END

GO