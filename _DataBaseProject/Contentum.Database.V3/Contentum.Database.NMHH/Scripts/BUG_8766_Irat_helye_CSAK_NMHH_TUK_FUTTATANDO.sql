﻿
DECLARE @isTUK char(1); 

SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
			where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege) 

IF @isTUK = '1'
	BEGIN

PRINT 'SZERVEZETI EGYSÉG IRATTÁRA KAPCSOLATOK INVALIDÁLÁSA START'

DECLARE @oId uniqueidentifier
DECLARE ot_cursor CURSOR FOR
select pk.Id
--select p.Nev ,p2.Nev, pk.* 
from KRT_PartnerKapcsolatok pk
inner join KRT_Partnerek p
on p.Id=pk.Partner_id --and p.Id=pk.Partner_id_kapcsolt
inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
where (pk.Tipus='5' or pk.Tipus='1')
and p.Nev 
in 
(
'Debreceni Kezelő Pont',
'Miskolci Kezelő Pont',
'Nyilvántartó',
'Reviczky utcai Kezelő Pont',
'Pécsi Kezelő Pont',
'Soproni Kezelő Pont',
'Szegedi Kezelő Pont',
'Reviczky utcai Kezelő Pont',
'Visegrádi utcai Kezelő Pont',
'Debreceni Hatósági Iroda',
'Miskolci Hatósági Iroda',
'Biztonságszervezési és Minősített Adatvédelmi Osztály',
'Igazgatási Osztály',
'Pécsi Hatósági Iroda',
'Soproni Hatósági Iroda',
'Szegedi Hatósági Iroda',
'Védelmi és Rendészeti Frekvenciagazdálkodási Főosztály',
'Frekvencia- és Azonosítógazdálkodási Főosztály'
)
and GETDATE() between p.ErvKezd and p.ErvVege
and GETDATE() between pk.ErvKezd and pk.ErvVege

declare @MyExecutionTime	datetime
set @MyExecutionTime=GETDATE()
OPEN ot_cursor 

FETCH NEXT FROM ot_cursor   
INTO @oId

WHILE @@FETCH_STATUS = 0  
BEGIN 

 PRINT convert(nvarchar(50), @oid)

 exec [sp_KRT_PartnerKapcsolatokInvalidate] @id=@oId,@ExecutorUserId='54E861A5-36ED-44CA-BAA7-C287D125B309', @ExecutionTime=@MyExecutionTime
 
 FETCH NEXT FROM ot_cursor   
    INTO @oId  
END

CLOSE ot_cursor;  
DEALLOCATE ot_cursor

PRINT 'SZERVEZETI EGYSÉG IRATTÁRA KAPCSOLATOK INVALIDÁLÁSA END'

PRINT 'Csoporttagság INVALIDÁLÁSA START'

DECLARE @csId uniqueidentifier
DECLARE cs_cursor CURSOR FOR
select pk.Id
--select p.Nev ,p2.Nev, pk.* 
from KRT_CsoportTagok pk
inner join KRT_Csoportok p
on p.Id=pk.Csoport_Id --and p.Id=pk.Partner_id_kapcsolt
inner join KRT_Csoportok p2 on p2.Id=pk.Csoport_Id_Jogalany
where pk.Tipus='1'
and p.Nev 
in 
(
'Debreceni Kezelő Pont',
'Miskolci Kezelő Pont',
'Nyilvántartó',
'Reviczky utcai Kezelő Pont',
'Pécsi Kezelő Pont',
'Soproni Kezelő Pont',
'Szegedi Kezelő Pont',
'Reviczky utcai Kezelő Pont',
'Visegrádi utcai Kezelő Pont'
)
and GETDATE() between p.ErvKezd and p.ErvVege
and GETDATE() between pk.ErvKezd and pk.ErvVege

declare @csExecutionTime	datetime
set @csExecutionTime=GETDATE()
OPEN cs_cursor 

FETCH NEXT FROM cs_cursor   
INTO @csId

WHILE @@FETCH_STATUS = 0  
BEGIN 

 --PRINT convert(nvarchar(50), @id)

 exec [sp_KRT_CsoportTagokInvalidate] @id=@csId,@ExecutorUserId='54E861A5-36ED-44CA-BAA7-C287D125B309', @ExecutionTime=@csExecutionTime
 
 FETCH NEXT FROM cs_cursor   
    INTO @csId  
END

CLOSE cs_cursor;  
DEALLOCATE cs_cursor

PRINT 'Csoporttagság INVALIDÁLÁSA END'

PRINT 'UGYIRATOK IRATTARI HELYEK NULL UPDATE START'

IF OBJECT_ID('tempdb..#Ugyiratok') IS NOT NULL
BEGIN
    DROP table #Ugyiratok
END

create table #Ugyiratok
(
    Id uniqueidentifier, 
    OldIrratarId uniqueidentifier,
	OldIrratariHely nvarchar(100)	
)

insert into #Ugyiratok(Id,OldIrratarId,OldIrratariHely)
select distinct u.Id,u.IrattarId,u.IrattariHely from EREC_UgyUgyiratok u
inner join EREC_IrattariHelyek ih on u.IrattarId=ih.Id
where 
ih.Ertek
in
(

'Debreceni Kezelő Pont',
'Miskolci Kezelő Pont',
'Nyilvántartó',
'Reviczky utcai Kezelő Pont',
'Pécsi Kezelő Pont',
'Soproni Kezelő Pont',
'Szegedi Kezelő Pont',
'Reviczky utcai Kezelő Pont',
'Visegrádi utcai Kezelő Pont'
)
and u.ErvKezd<=getdate() and u.ErvVege >=GETDATE()
and ih.ErvKezd<=getdate() and ih.ErvVege >=GETDATE()

--select * from #Ugyiratok

update EREC_UgyUgyiratok
set IrattarId=null, IrattariHely=null
where Id 
in
(select Id from #Ugyiratok)

PRINT 'UGYIRATOK IRATTARI HELYEK NULL UPDATE END'

PRINT 'LETEZŐ IRATTARI STRUKTÚRA TÖRLÉS START'

DECLARE @ihId uniqueidentifier
DECLARE ih_cursor CURSOR FOR
select ih.Id
from EREC_IrattariHelyek ih 
where 
ih.Ertek
in
(
'Debreceni Kezelő Pont',
'Miskolci Kezelő Pont',
'Nyilvántartó',
'Reviczky utcai Kezelő Pont',
'Pécsi Kezelő Pont',
'Soproni Kezelő Pont',
'Szegedi Kezelő Pont',
'Reviczky utcai Kezelő Pont',
'Visegrádi utcai Kezelő Pont'
)
and ih.ErvKezd<=getdate() and ih.ErvVege >=GETDATE()

declare @IhExecutionTime	datetime
set @IhExecutionTime=GETDATE()
OPEN ih_cursor 

FETCH NEXT FROM ih_cursor   
INTO @ihId

WHILE @@FETCH_STATUS = 0  
BEGIN 

 --PRINT convert(nvarchar(50), @id)

 exec [sp_EREC_IrattariHelyekInvalidate] @id=@ihId,@ExecutorUserId='54E861A5-36ED-44CA-BAA7-C287D125B309', @ExecutionTime=@IhExecutionTime
 
 FETCH NEXT FROM ih_cursor   
    INTO @ihId  
END

CLOSE ih_cursor;  
DEALLOCATE ih_cursor

PRINT 'LETEZŐ IRATTARI STRUKTÚRA TÖRLÉS END'

PRINT 'IRATTARI STRUKTÚRA LÉTREHOZÁSA START'

-- drop #Insert_Kodtar
IF OBJECT_ID('tempdb..#Insert_IrattariHelyek') IS NOT NULL
BEGIN
    DROP PROC #Insert_IrattariHelyek
END

-- create #Insert_Kodtar

DECLARE @sql NVARCHAR(4000)
SET @sql = N'
CREATE PROCEDURE #Insert_IrattariHelyek
(  
   @IrattariHelyek_id uniqueidentifier=NULL,  
   @IrattariHelyek_nev NVARCHAR(100),
   @IrattariHelyek_Felelos uniqueidentifier
)  
AS  
BEGIN  

DECLARE @id uniqueidentifier;

select @id=Id from EREC_IrattariHelyek where Ertek=@IrattariHelyek_nev
and ErvKezd<=getdate() and ErvVege >=GETDATE()

IF (@id is null)
BEGIN
	
	DECLARE @org uniqueidentifier
	SET	@org=''450B510A-7CAA-46B0-83E3-18445C0C53A9''

	DECLARE	@return_value int,
		@ResultUid uniqueidentifier,
		@VonalKod nvarchar(100)

EXEC	@return_value = [dbo].[sp_BarkodSav_Igenyles]
		@Szervezet_Id = ''54E861A5-36ED-44CA-BAA7-C287D125B309'',
		@SavType = N''G'',
		@FoglalandoDarab = 1,
		@ResultUid = @ResultUid OUTPUT

select @VonalKod=Kod from KRT_Barkodok
where Id=@ResultUid


IF (@IrattariHelyek_id is null)
	BEGIN
			INSERT INTO [dbo].[EREC_IrattariHelyek]
			   ([Ertek]
			   ,[Nev]
			   ,[Vonalkod]		   
			   ,[Felelos_Csoport_Id]
			   ,[Ver]
			   ,[ErvKezd]
			   ,[ErvVege]
			   ,[Letrehozo_id]
			   ,[LetrehozasIdo]
			   )
			VALUES
			   (@IrattariHelyek_nev,
				@IrattariHelyek_nev,
				@VonalKod,
				@IrattariHelyek_Felelos,
				''1'',
				GETDATE(),
				''4700-12-31 00:00:00.000'',
				@org,
				GETDATE()
			);
	END
ELSE
	BEGIN
		INSERT INTO [dbo].[EREC_IrattariHelyek]
			   ([Id]
			   ,[Ertek]
			   ,[Nev]
			   ,[Vonalkod]		   
			   ,[Felelos_Csoport_Id]
			   ,[Ver]
			   ,[ErvKezd]
			   ,[ErvVege]
			   ,[Letrehozo_id]
			   ,[LetrehozasIdo]
			   )
			VALUES
			   (@IrattariHelyek_id,
			    @IrattariHelyek_nev,
				@IrattariHelyek_nev,
				@VonalKod,
				@IrattariHelyek_Felelos,
				''1'',
				GETDATE(),
				''4700-12-31 00:00:00.000'',
				@org,
				GETDATE()
			);
	END

select @id=Id from EREC_IrattariHelyek where Ertek=@IrattariHelyek_nev

		Update KRT_Barkodok
  SET Obj_type=''EREC_IrattariHelyek'',Allapot=''F'',Obj_Id=@id
  where Id=@ResultUid

END	

ELSE

BEGIN
		
		PRINT ''UPDATE '' +  @IrattariHelyek_nev 

		UPDATE [dbo].[EREC_IrattariHelyek]
		SET [Felelos_Csoport_Id]=@IrattariHelyek_Felelos
		WHERE Id=@id
END

END
'
select @sql
EXEC sp_executesql @sql

DECLARE @Org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @OrgKod nvarchar(100) = (select Kod from KRT_Orgok where Id = @Org)
select @OrgKod



DECLARE @partnerNev nvarchar(400) = 'Debreceni Kezelő Pont'; 
DECLARE @record_idPartner uniqueidentifier SET @record_idPartner = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @partnerNev and GETDATE() between p.ErvKezd and p.ErvVege) 
IF @record_idPartner IS NULL 
		BEGIN 
			PRINT @partnerNev + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @partnerNev+ ' partner már létezik. INSERT Debreceni Kezelő Pont' 
				EXEC #Insert_IrattariHelyek NULL,'Debreceni Kezelő Pont', @record_idPartner
		END


SET @partnerNev = 'Miskolci Kezelő Pont'; 
SET @record_idPartner = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @partnerNev and GETDATE() between p.ErvKezd and p.ErvVege) 
IF @record_idPartner IS NULL 
		BEGIN 
			PRINT @partnerNev + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @partnerNev+ ' partner már létezik INSERT Miskolci Kezelő Pont' 
				EXEC #Insert_IrattariHelyek null, 'Miskolci Kezelő Pont', @record_idPartner
		END

SET @partnerNev = 'Nyilvántartó'; 
SET @record_idPartner = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @partnerNev and GETDATE() between p.ErvKezd and p.ErvVege) 
IF @record_idPartner IS NULL 
		BEGIN 
			PRINT @partnerNev + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @partnerNev+ ' partner már létezik INSERT Nyilvántartó' 
				EXEC #Insert_IrattariHelyek null, 'Nyilvántartó', @record_idPartner
		END

SET @partnerNev = 'Reviczky utcai Kezelő Pont'; 
SET @record_idPartner = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @partnerNev and GETDATE() between p.ErvKezd and p.ErvVege) 
IF @record_idPartner IS NULL 
		BEGIN 
			PRINT @partnerNev + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @partnerNev+ ' partner már létezik INSERT Reviczky utcai Kezelő Pont' 
				EXEC #Insert_IrattariHelyek null, 'Reviczky utcai Kezelő Pont', @record_idPartner
		END

SET @partnerNev = 'Pécsi Kezelő Pont'; 
SET @record_idPartner = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @partnerNev and GETDATE() between p.ErvKezd and p.ErvVege) 
IF @record_idPartner IS NULL 
		BEGIN 
			PRINT @partnerNev + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @partnerNev+ ' partner már létezik INSERT Pécsi Kezelő Pont' 
				EXEC #Insert_IrattariHelyek null, 'Pécsi Kezelő Pont', @record_idPartner
		END

SET @partnerNev = 'Soproni Kezelő Pont'; 
SET @record_idPartner = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @partnerNev and GETDATE() between p.ErvKezd and p.ErvVege) 
IF @record_idPartner IS NULL 
		BEGIN 
			PRINT @partnerNev + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @partnerNev+ ' partner már létezik INSERT Soproni Kezelő Pont' 
				EXEC #Insert_IrattariHelyek null, 'Soproni Kezelő Pont', @record_idPartner
		END

SET @partnerNev = 'Szegedi Kezelő Pont'; 
SET @record_idPartner = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @partnerNev and GETDATE() between p.ErvKezd and p.ErvVege) 
IF @record_idPartner IS NULL 
		BEGIN 
			PRINT @partnerNev + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @partnerNev+ ' partner már létezik INSERT Szegedi Kezelő Pont' 
				EXEC #Insert_IrattariHelyek null, 'Szegedi Kezelő Pont', @record_idPartner
		END

SET @partnerNev = 'Visegrádi utcai Kezelő Pont'; 
SET @record_idPartner = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @partnerNev and GETDATE() between p.ErvKezd and p.ErvVege) 
IF @record_idPartner IS NULL 
		BEGIN 
			PRINT @partnerNev + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @partnerNev+ ' partner már létezik INSERT Visegrádi utcai Kezelő Pont' 
				EXEC #Insert_IrattariHelyek null, 'Visegrádi utcai Kezelő Pont', @record_idPartner
		END

SET @partnerNev = 'Igazgatási Osztály'; 
SET @record_idPartner = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @partnerNev and GETDATE() between p.ErvKezd and p.ErvVege) 
IF @record_idPartner IS NULL 
		BEGIN 
			PRINT @partnerNev + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @partnerNev+ ' partner már létezik INSERT Címzettnél' 
				EXEC #Insert_IrattariHelyek '4F18BB74-AEC8-E911-80DF-00155D017B07', 'Címzettnél', @record_idPartner
		END	




PRINT 'IRATTARI STRUKTÚRA LÉTREHOZÁSA END'

PRINT 'UGYIRATOK UJ IRATTARI HELYEK UPDATE START'

update u
set u.IrattarId=h.Id,u.IrattariHely=h.Ertek
FROM EREC_UgyUgyiratok u
inner join #Ugyiratok t on u.Id=t.Id
left join EREC_IrattariHelyek h on h.Ertek=t.OldIrratariHely
where GETDATE() between h.ErvKezd and h.ErvVege

PRINT 'UGYIRATOK UJ IRATTARI HELYEK UPDATE END'

PRINT 'START Szervezeti egységek összerendelése saját magukkal szervezeti egység irattára kapcsolattal' 

DECLARE @szervezet nvarchar(400) = 'Debreceni Kezelő Pont'; 

DECLARE @Org_id uniqueidentifier
	SET @Org_id = '450B510A-7CAA-46B0-83E3-18445C0C53A9'			
	DECLARE @record_idSzervezet uniqueidentifier SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 

			DECLARE @record_id uniqueidentifier SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id and p.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT 'INSERT Debreceni Kezelő Pont'
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END


SET @szervezet = 'Miskolci Kezelő Pont';
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id and p.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT 'INSERT Miskolci Kezelő Pont'
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Nyilvántartó';
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id and p.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT 'INSERT Nyilvántartó'
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Reviczky utcai Kezelő Pont';
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id and p.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT 'INSERT Reviczky utcai Kezelő Pont'
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END


SET @szervezet = 'Pécsi Kezelő Pont';
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id and p.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT 'INSERT Pécsi Kezelő Pont'
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Soproni Kezelő Pont';
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id and p.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT 'INSERT Soproni Kezelő Pont'
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Szegedi Kezelő Pont';
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id and p.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT 'INSERT Szegedi Kezelő Pont'
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

	SET @szervezet = 'Visegrádi utcai Kezelő Pont';
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id and p.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT 'INSERT Visegrádi utcai Kezelő Pont'
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END
PRINT 'END Szervezeti egységek összerendelése saját magukkal szervezeti egység irattára kapcsolattal' 

PRINT 'START 12178 Hatósági iroda a kezelőpontonthoz szervezeti egység irattárával START' 

SET @szervezet = 'Debreceni Hatósági Iroda';
DECLARE @kacsoltszervezet nvarchar(400) = 'Debreceni Kezelő Pont'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
DECLARE @record_idKapcsoltSzervezet uniqueidentifier 
SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Miskolci Hatósági Iroda';
SET @kacsoltszervezet = 'Miskolci Kezelő Pont'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Biztonságszervezési és Minősített Adatvédelmi Osztály';
SET @kacsoltszervezet = 'Nyilvántartó'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END


SET @szervezet = 'Igazgatási Osztály';
SET @kacsoltszervezet = 'Reviczky utcai Kezelő Pont'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Pécsi Hatósági Iroda';
SET @kacsoltszervezet = 'Pécsi Kezelő Pont'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Soproni Hatósági Iroda';
SET @kacsoltszervezet = 'Soproni Kezelő Pont'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Szegedi Hatósági Iroda';
SET @kacsoltszervezet = 'Szegedi Kezelő Pont'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Védelmi és Rendészeti Frekvenciagazdálkodási Főosztály';
SET @kacsoltszervezet = 'Visegrádi utcai Kezelő Pont'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Frekvencia- és Azonosítógazdálkodási Főosztály';
SET @kacsoltszervezet = 'Visegrádi utcai Kezelő Pont'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='5' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '5'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

PRINT 'END 12178 Hatósági iroda a kezelőpontonthoz szervezeti egység irattárával END' 

PRINT 'START 12180 Kezelőpontonthoz alárendelt szervezeti egység START' 

DECLARE @csoporttagId uniqueidentifier
SET @szervezet = 'Debreceni Kezelő Pont';
SET @kacsoltszervezet = 'Debreceni hatósági Iroda'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 
			set @csoporttagId =(select top 1 pk.Id from KRT_Csoportok p 
																	inner join KRT_CsoportTagok pk
																	on p.Id=pk.Csoport_Id 
																	inner join KRT_Csoportok p2 on p2.Id=pk.Csoport_Id_Jogalany
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '1'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);

					INSERT INTO KRT_CsoportTagok
					(					
					Csoport_Id,
					Csoport_Id_Jogalany,
					Tipus
					)
					VALUES
					(						
						@record_idSzervezet,
						@record_idKapcsoltSzervezet,
						'1'
					)

				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END


SET @szervezet = 'Miskolci Kezelő Pont';
SET @kacsoltszervezet = 'Miskolci Hatósági Iroda'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 
			set @csoporttagId =(select top 1 pk.Id from KRT_Csoportok p 
																	inner join KRT_CsoportTagok pk
																	on p.Id=pk.Csoport_Id 
																	inner join KRT_Csoportok p2 on p2.Id=pk.Csoport_Id_Jogalany
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '1'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);

					INSERT INTO KRT_CsoportTagok
					(					
					Csoport_Id,
					Csoport_Id_Jogalany,
					Tipus
					)
					VALUES
					(						
						@record_idSzervezet,
						@record_idKapcsoltSzervezet,
						'1'
					)

				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Nyilvántartó';
SET @kacsoltszervezet = 'Biztonságszervezési és Minősített Adatvédelmi Osztály'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege)
			set @csoporttagId =(select top 1 pk.Id from KRT_Csoportok p 
																	inner join KRT_CsoportTagok pk
																	on p.Id=pk.Csoport_Id 
																	inner join KRT_Csoportok p2 on p2.Id=pk.Csoport_Id_Jogalany
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 

			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '1'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);

					INSERT INTO KRT_CsoportTagok
					(					
					Csoport_Id,
					Csoport_Id_Jogalany,
					Tipus
					)
					VALUES
					(						
						@record_idSzervezet,
						@record_idKapcsoltSzervezet,
						'1'
					)
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Reviczky utcai Kezelő Pont';
SET @kacsoltszervezet = 'Igazgatási Osztály'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 
			set @csoporttagId =(select top 1 pk.Id from KRT_Csoportok p 
																	inner join KRT_CsoportTagok pk
																	on p.Id=pk.Csoport_Id 
																	inner join KRT_Csoportok p2 on p2.Id=pk.Csoport_Id_Jogalany
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 
			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '1'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);

					INSERT INTO KRT_CsoportTagok
					(					
					Csoport_Id,
					Csoport_Id_Jogalany,
					Tipus
					)
					VALUES
					(						
						@record_idSzervezet,
						@record_idKapcsoltSzervezet,
						'1'
					)
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END


SET @szervezet = 'Pécsi Kezelő Pont';
SET @kacsoltszervezet = 'Pécsi Hatósági Iroda'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 
			set @csoporttagId =(select top 1 pk.Id from KRT_Csoportok p 
																	inner join KRT_CsoportTagok pk
																	on p.Id=pk.Csoport_Id 
																	inner join KRT_Csoportok p2 on p2.Id=pk.Csoport_Id_Jogalany
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 
			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '1'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);

					INSERT INTO KRT_CsoportTagok
					(					
					Csoport_Id,
					Csoport_Id_Jogalany,
					Tipus
					)
					VALUES
					(						
						@record_idSzervezet,
						@record_idKapcsoltSzervezet,
						'1'
					)
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Soproni Kezelő Pont';
SET @kacsoltszervezet = 'Soproni Hatósági Iroda'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 
			set @csoporttagId =(select top 1 pk.Id from KRT_Csoportok p 
																	inner join KRT_CsoportTagok pk
																	on p.Id=pk.Csoport_Id 
																	inner join KRT_Csoportok p2 on p2.Id=pk.Csoport_Id_Jogalany
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 
			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '1'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);

					INSERT INTO KRT_CsoportTagok
					(					
					Csoport_Id,
					Csoport_Id_Jogalany,
					Tipus
					)
					VALUES
					(						
						@record_idSzervezet,
						@record_idKapcsoltSzervezet,
						'1'
					)
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Szegedi Kezelő Pont';
SET @kacsoltszervezet = 'Szegedi Hatósági Iroda'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 
			set @csoporttagId =(select top 1 pk.Id from KRT_Csoportok p 
																	inner join KRT_CsoportTagok pk
																	on p.Id=pk.Csoport_Id 
																	inner join KRT_Csoportok p2 on p2.Id=pk.Csoport_Id_Jogalany
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 
			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '1'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);

					INSERT INTO KRT_CsoportTagok
					(					
					Csoport_Id,
					Csoport_Id_Jogalany,
					Tipus
					)
					VALUES
					(						
						@record_idSzervezet,
						@record_idKapcsoltSzervezet,
						'1'
					)
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Visegrádi utcai Kezelő Pont';
SET @kacsoltszervezet = 'Védelmi és Rendészeti Frekvenciagazdálkodási Főosztály'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 
			set @csoporttagId =(select top 1 pk.Id from KRT_Csoportok p 
																	inner join KRT_CsoportTagok pk
																	on p.Id=pk.Csoport_Id 
																	inner join KRT_Csoportok p2 on p2.Id=pk.Csoport_Id_Jogalany
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 
			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '1'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);

					INSERT INTO KRT_CsoportTagok
					(					
					Csoport_Id,
					Csoport_Id_Jogalany,
					Tipus
					)
					VALUES
					(						
						@record_idSzervezet,
						@record_idKapcsoltSzervezet,
						'1'
					)
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END

SET @szervezet = 'Visegrádi utcai Kezelő Pont';
SET @kacsoltszervezet = 'Frekvencia- és Azonosítógazdálkodási Főosztály'; 
SET @record_idSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @szervezet and GETDATE() between p.ErvKezd and p.ErvVege) 

SET @record_idKapcsoltSzervezet = (select top 1 p.Id from KRT_Partnerek p 														
														where p.Nev like @kacsoltszervezet and GETDATE() between p.ErvKezd and p.ErvVege) 
	IF @record_idSzervezet IS NULL 
		BEGIN 
			PRINT @szervezet + ' partner még nem létezik' 
		END
		ELSE IF @record_idKapcsoltSzervezet IS NULL 
		BEGIN
			PRINT @kacsoltszervezet + ' partner még nem létezik' 
		END
		ELSE
		BEGIN 
			PRINT @szervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idSzervezet) 
			PRINT @kacsoltszervezet+ ' partner már létezik @record_id='+convert(NVARCHAR(100),@record_idKapcsoltSzervezet) 

			SET @record_id = (select top 1 pk.Id from KRT_Partnerek p 
																	inner join KRT_PartnerKapcsolatok pk
																	on p.Id=pk.Partner_id 
																	inner join KRT_Partnerek p2 on p2.Id=pk.Partner_id_kapcsolt
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 
			set @csoporttagId =(select top 1 pk.Id from KRT_Csoportok p 
																	inner join KRT_CsoportTagok pk
																	on p.Id=pk.Csoport_Id 
																	inner join KRT_Csoportok p2 on p2.Id=pk.Csoport_Id_Jogalany
																	where p.Nev like @szervezet and pk.Tipus='1' and GETDATE() between pk.ErvKezd and pk.ErvVege
																	and p2.Nev like @kacsoltszervezet and GETDATE() between p2.ErvKezd and p2.ErvVege
																	) 
			IF @record_id IS NULL 
				BEGIN 
	
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat még nem létezik' 
								
					PRINT @szervezet
					INSERT INTO KRT_PartnerKapcsolatok (						 
						 Tipus
						 ,Partner_id
						 ,Partner_id_kapcsolt
						 ,ErvKezd
						 ,ErvVege
					) VALUES (						 
						 '1'
						 ,@record_idSzervezet
						 ,@record_idKapcsoltSzervezet
						 ,GETDATE()
						 ,'4700-12-31 00:00:00.000'
					);

					INSERT INTO KRT_CsoportTagok
					(					
					Csoport_Id,
					Csoport_Id_Jogalany,
					Tipus
					)
					VALUES
					(						
						@record_idSzervezet,
						@record_idKapcsoltSzervezet,
						'1'
					)
				END
			ELSE
				BEGIN
					PRINT @szervezet + ' => ' + @kacsoltszervezet + ' partner kapcsolat már létezik @record_id='+convert(NVARCHAR(100),@record_id) 
				END
		END


PRINT 'END 12180 Kezelőpontonthoz alárendelt szervezeti egység END' 

END