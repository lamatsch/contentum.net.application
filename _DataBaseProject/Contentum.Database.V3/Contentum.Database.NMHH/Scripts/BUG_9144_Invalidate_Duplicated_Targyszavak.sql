declare @tranz_id uniqueidentifier
set @tranz_id = newid()

;with duplicated as
(
select Obj_Id, Targyszo_Id, MAX(ot.LetrehozasIdo) MaxLetrehozasIdo from EREC_ObjektumTargyszavai ot
join EREC_Obj_MetaAdatai om on ot.Obj_Metaadatai_Id = om.Id
join EREC_Obj_MetaDefinicio od on om.Obj_MetaDefinicio_Id = od.Id
where ot.Obj_Metaadatai_Id is not null
and od.DefinicioTipus = 'HA'
and GETDATE() between ot.ErvKezd and ot.ErvVege
group by Obj_Id, Targyszo_Id
having COUNT(*) > 1 
)
,invalidalando as
(
select ot.* from EREC_ObjektumTargyszavai ot
join duplicated dup
on ot.Obj_Id = dup.Obj_Id
and ot.Targyszo_Id = dup.Targyszo_Id
where ot.Obj_Metaadatai_Id is not null
and ot.LetrehozasIdo < dup.MaxLetrehozasIdo
)


UPDATE EREC_ObjektumTargyszavai
SET ErvVege = GETDATE(),
Tranz_id = @tranz_id,
Note = 'DUPLICATED'
where Id in (select Id from invalidalando)



