﻿---- BUG_9382: Bejövő irat felületein ne legyenek a jogerősítő mezők
--
DECLARE @Letrehozo_Id uniqueidentifier = '54E861A5-36ED-44CA-BAA7-C287D125B309'
DECLARE @Org uniqueidentifier = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
DECLARE @OrgKod nvarchar(100) = (select Kod from KRT_Orgok where [Id] = @Org)
DECLARE @ObjTip_EREC_IraIratok_Table uniqueidentifier = 'AA5E7BBA-96A0-4C17-8709-06A6D297E107'

IF @OrgKod = 'NMHH'
BEGIN

	-- PostazasIranya oszlop
	DECLARE @ObjTip_PostazasIranya_Column uniqueidentifier = '64DD420F-E2A6-400E-AFD4-95B300586A81'
	--IF not exists (select 1 from [KRT_ObjTipusok] where [Id]=@ObjTip_PostazasIranya_Column)
	--BEGIN
	--	INSERT INTO [KRT_ObjTipusok] ([Id], [ObjTipus_Id_Tipus], [Obj_Id_Szulo], [Kod], [Nev]) 
	--	VALUES(@ObjTip_PostazasIranya_Column, '76B08484-4B5B-4648-A95F-EE3E40D5DEFE', 'AA5E7BBA-96A0-4C17-8709-06A6D297E107', 'PostazasIranya', 'PostazasIranya oszlop')
	--END

	-- Kimenő irathoz
	DECLARE @metadef_KimenoIrat uniqueidentifier = '5DB71A9E-1640-417B-97CA-E341513F2209'
	IF not exists (select 1 from EREC_Obj_MetaDefinicio where [Id]=@metadef_KimenoIrat)
	BEGIN
		INSERT INTO EREC_Obj_MetaDefinicio([Id], [Org], [Objtip_Id], [Objtip_Id_Column], [ColumnValue], [DefinicioTipus], 
		[ContentType], [SPSSzinkronizalt], [WorkFlowVezerles], [ErvKezd], [ErvVege], [Letrehozo_id]) 
		VALUES (@metadef_KimenoIrat, @Org, @ObjTip_EREC_IraIratok_Table, @ObjTip_PostazasIranya_Column, 
		'0', 'B1',  'EREC_IraIratok_metaadatai kimenő irathoz', '0', '0', '2019-07-15', '4700-12-31', 
		@Letrehozo_Id)
	END

	-- Belső irathoz
	DECLARE @metadef_BelsoIrat uniqueidentifier = 'CA67CF26-FF43-4E6B-90B4-943068E93B13'
	IF not exists (select 1 from EREC_Obj_MetaDefinicio where [Id]=@metadef_BelsoIrat)
	BEGIN
		INSERT INTO EREC_Obj_MetaDefinicio([Id], [Org], [Objtip_Id], [Objtip_Id_Column], [ColumnValue], [DefinicioTipus], 
		[ContentType], [SPSSzinkronizalt], [WorkFlowVezerles], [ErvKezd], [ErvVege], [Letrehozo_id]) 
		VALUES (@metadef_BelsoIrat, @Org, @ObjTip_EREC_IraIratok_Table, @ObjTip_PostazasIranya_Column, 
		'2', 'B1', 'EREC_IraIratok_metaadatai belső irathoz', '0', '0', '2019-07-15', '4700-12-31', 
		@Letrehozo_Id)
	END

	-- EREC_Obj_MetaAdatai, kimenő irathoz ('5DB71A9E-1640-417B-97CA-E341513F2209')
	--Jogerositendo Targyszavak_Id (sorrend 1-4)
	--7EFE718A-117A-E711-80C2-00155D020D7E
	--AF68ACB7-117A-E711-80C2-00155D020D7E
	--C4ABA7CE-117A-E711-80C2-00155D020D7E
	--A5288EE1-117A-E711-80C2-00155D020D7E
	DECLARE @metaadat_Kimeno1 uniqueidentifier = '859FEF05-B75D-4649-BD54-5347DA5BBB23'
	IF not exists (select 1 from EREC_Obj_MetaAdatai where [Id]=@metaadat_Kimeno1)
	BEGIN
		INSERT INTO EREC_Obj_MetaAdatai([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [Sorszam], [Opcionalis], [Ismetlodo], [SPSSzinkronizalt])
		VALUES(@metaadat_Kimeno1, @metadef_KimenoIrat, '7EFE718A-117A-E711-80C2-00155D020D7E', '1', '1', '0', '0') 
	END

	DECLARE @metaadat_Kimeno2 uniqueidentifier = 'D3806B0F-E96E-4CAF-837D-9BCC2A0085C5'
	IF not exists (select 1 from EREC_Obj_MetaAdatai where [Id]=@metaadat_Kimeno2)
	BEGIN
		INSERT INTO EREC_Obj_MetaAdatai([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [Sorszam], [Opcionalis], [Ismetlodo], [SPSSzinkronizalt])
		VALUES(@metaadat_Kimeno2, @metadef_KimenoIrat, 'AF68ACB7-117A-E711-80C2-00155D020D7E', '2', '1', '0', '0') 
	END

	DECLARE @metaadat_Kimeno3 uniqueidentifier = 'DE807AA6-9519-4C45-B031-1B2735ACF567'
	IF not exists (select 1 from EREC_Obj_MetaAdatai where [Id]=@metaadat_Kimeno3)
	BEGIN
		INSERT INTO EREC_Obj_MetaAdatai([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [Sorszam], [Opcionalis], [Ismetlodo], [SPSSzinkronizalt])
		VALUES(@metaadat_Kimeno3, @metadef_KimenoIrat, 'C4ABA7CE-117A-E711-80C2-00155D020D7E', '3', '1', '0', '0') 
	END

	DECLARE @metaadat_Kimeno4 uniqueidentifier = 'AD150E51-8E44-41E1-BE44-233923DD75C0'
	IF not exists (select 1 from EREC_Obj_MetaAdatai where [Id]=@metaadat_Kimeno4)
	BEGIN
		INSERT INTO EREC_Obj_MetaAdatai([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [Sorszam], [Opcionalis], [Ismetlodo], [SPSSzinkronizalt])
		VALUES(@metaadat_Kimeno4, @metadef_KimenoIrat, 'A5288EE1-117A-E711-80C2-00155D020D7E', '4', '1', '0', '0')
	END

	-- EREC_Obj_MetaAdatai, belső irathoz ('CA67CF26-FF43-4E6B-90B4-943068E93B13')
	DECLARE @metaadat_Belso1 uniqueidentifier = '1218B6A6-FF61-409C-912A-843E9DC09DA2'
	IF not exists (select 1 from EREC_Obj_MetaAdatai where [Id]=@metaadat_Belso1)
	BEGIN
		INSERT INTO EREC_Obj_MetaAdatai([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [Sorszam], [Opcionalis], [Ismetlodo], [SPSSzinkronizalt])
		VALUES(@metaadat_Belso1, @metadef_BelsoIrat, '7EFE718A-117A-E711-80C2-00155D020D7E', '1', '1', '0', '0') 
	END

	DECLARE @metaadat_Belso2 uniqueidentifier = 'E82891C8-1D92-41A7-80BA-47525D6148DA'
	IF not exists (select 1 from EREC_Obj_MetaAdatai where [Id]=@metaadat_Belso2)
	BEGIN
		INSERT INTO EREC_Obj_MetaAdatai([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [Sorszam], [Opcionalis], [Ismetlodo], [SPSSzinkronizalt])
		VALUES(@metaadat_Belso2, @metadef_BelsoIrat, 'AF68ACB7-117A-E711-80C2-00155D020D7E', '2', '1', '0', '0') 
	END

	DECLARE @metaadat_Belso3 uniqueidentifier = '2F093C91-8023-4BF6-9A4F-4F85D42611F6'
	IF not exists (select 1 from EREC_Obj_MetaAdatai where [Id]=@metaadat_Belso3)
	BEGIN
		INSERT INTO EREC_Obj_MetaAdatai([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [Sorszam], [Opcionalis], [Ismetlodo], [SPSSzinkronizalt])
		VALUES(@metaadat_Belso3, @metadef_BelsoIrat, 'C4ABA7CE-117A-E711-80C2-00155D020D7E', '3', '1', '0', '0') 
	END

	DECLARE @metaadat_Belso4 uniqueidentifier = '0FD1CA96-5C01-490B-B7C2-AA7DC1902457'
	IF not exists (select 1 from EREC_Obj_MetaAdatai where [Id]=@metaadat_Belso4)
	BEGIN
		INSERT INTO EREC_Obj_MetaAdatai([Id], [Obj_MetaDefinicio_Id], [Targyszavak_Id], [Sorszam], [Opcionalis], [Ismetlodo], [SPSSzinkronizalt])
		VALUES(@metaadat_Belso4, @metadef_BelsoIrat, 'A5288EE1-117A-E711-80C2-00155D020D7E', '4', '1', '0', '0') 
	END

	-- érvénytelenítés
	UPDATE EREC_Obj_MetaAdatai SET [ErvVege]='2019-07-15' WHERE [Id] IN ('A8288EE1-117A-E711-80C2-00155D020D7E',
	'81FE718A-117A-E711-80C2-00155D020D7E', '2586E3BD-117A-E711-80C2-00155D020D7E', 'C7ABA7CE-117A-E711-80C2-00155D020D7E')

END