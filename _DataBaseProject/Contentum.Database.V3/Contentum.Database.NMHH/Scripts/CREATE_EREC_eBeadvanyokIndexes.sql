if not exists (select 1 FROM sys.indexes WHERE name='IX_EREC_eBeadvanyok_KR_HivatkozasiSzam_ErvKezd_ErvVege' AND object_id = OBJECT_ID('EREC_eBeadvanyok'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_EREC_eBeadvanyok_KR_HivatkozasiSzam_ErvKezd_ErvVege]
	ON [dbo].[EREC_eBeadvanyok] ([KR_HivatkozasiSzam],[ErvKezd],[ErvVege])
END

if not exists (select 1 FROM sys.indexes WHERE name='IX_KR_ErkeztetesiSzam' AND object_id = OBJECT_ID('EREC_eBeadvanyok'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_KR_ErkeztetesiSzam]
	ON [dbo].[EREC_eBeadvanyok] ([KR_ErkeztetesiSzam],[ErvKezd],[ErvVege])
END
