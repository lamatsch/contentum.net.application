IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = '0F5014C5-40B6-4B85-835E-3138DA141B46')
	BEGIN
	    PRINT 'INSERT KRT_KodCsoportok - DONTES_FORMAJA_ALLAMIG'
		insert into KRT_KodCsoportok(
		Id
		,Kod
		,Nev
		,Modosithato
		,BesorolasiSema
		,Note
		) values (
		'0F5014C5-40B6-4B85-835E-3138DA141B46'
		,'DONTES_FORMAJA_ALLAMIG'
		,'D�nt�s form�ja (�llamigazgat�si)'
		,'1'
		,'0'
		,NULL
		);
	END
	ELSE
	BEGIN
	     PRINT 'UPDATE KRT_KodCsoportok - DONTES_FORMAJA_ALLAMIG'
		update KRT_KodCsoportok
			set Nev = 'D�nt�s form�ja (�llamigazgat�si)'
				,Note = NULL
		where Id = '0F5014C5-40B6-4B85-835E-3138DA141B46'
	END
GO
IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = '9B7672DC-7A11-470E-8E76-FA606556F846')
	BEGIN
	    PRINT 'INSERT KRT_KodCsoportok - DONTES_FORMAJA_ONK'
		insert into KRT_KodCsoportok(
		Id
		,Kod
		,Nev
		,Modosithato
		,BesorolasiSema
		,Note
		) values (
		'9B7672DC-7A11-470E-8E76-FA606556F846'
		,'DONTES_FORMAJA_ONK'
		,'D�nt�s form�ja (�nkorm�nyzati)'
		,'1'
		,'0'
		,'Felhaszn�l�s: Hat�s�gi adatok'
		);
	END
	ELSE
	BEGIN
	     PRINT 'UPDATE KRT_KodCsoportok - DONTES_FORMAJA_ONK'
		update KRT_KodCsoportok
			set Nev = 'D�nt�s form�ja (�nkorm�nyzati)'
				,Note = 'Felhaszn�l�s: Hat�s�gi adatok'
		where Id = '9B7672DC-7A11-470E-8E76-FA606556F846'
	END
GO
IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = '066FD6CE-7442-4DFB-A52A-41BC6087A2D9')
	BEGIN
	    PRINT 'INSERT KRT_KodCsoportok - DONTEST_HOZTA'
		insert into KRT_KodCsoportok(
		Id
		,Kod
		,Nev
		,Modosithato
		,BesorolasiSema
		,Note
		) values (
		'066FD6CE-7442-4DFB-A52A-41BC6087A2D9'
		,'DONTEST_HOZTA'
		,'D�nt�st hozta (�nkorm�nyzati)'
		,'1'
		,'0'
		,'Felhaszn�l�s: Hat�s�gi adatok'
		);
	END
	ELSE
	BEGIN
	     PRINT 'UPDATE KRT_KodCsoportok - DONTEST_HOZTA'
		update KRT_KodCsoportok
			set Nev = 'D�nt�st hozta (�nkorm�nyzati)'
				,Note = 'Felhaszn�l�s: Hat�s�gi adatok'
		where Id = '066FD6CE-7442-4DFB-A52A-41BC6087A2D9'
	END
GO
IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = '780AC33B-C239-E911-80CC-00155D020D03')
	BEGIN
	    PRINT 'INSERT KRT_KodCsoportok - DONTEST_HOZTA_ALLAMIG'
		insert into KRT_KodCsoportok(
		Id
		,Kod
		,Nev
		,Modosithato
		,BesorolasiSema
		,Note
		) values (
		'780AC33B-C239-E911-80CC-00155D020D03'
		,'DONTEST_HOZTA_ALLAMIG'
		,'D�nt�st hozta (�llamigazgat�si)'
		,'1'
		,'0'
		,NULL
		);
	END
	ELSE
	BEGIN
	     PRINT 'UPDATE KRT_KodCsoportok - DONTEST_HOZTA_ALLAMIG'
		update KRT_KodCsoportok
			set Nev = 'D�nt�st hozta (�llamigazgat�si)'
				,Note = NULL
		where Id = '780AC33B-C239-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = '2A884DFB-6926-42A1-BC57-EB716C0BE55A')
	BEGIN
	    PRINT 'INSERT KRT_KodCsoportok - HATAROZAT_HATALYBA_LEPESE'
		insert into KRT_KodCsoportok(
		Id
		,Kod
		,Nev
		,Modosithato
		,BesorolasiSema
		,Note
		) values (
		'2A884DFB-6926-42A1-BC57-EB716C0BE55A'
		,'HATAROZAT_HATALYBA_LEPESE'
		,'Hat�rozat hat�lyba l�p�se'
		,'1'
		,'0'
		,NULL
		);
	END
	ELSE
	BEGIN
	     PRINT 'UPDATE KRT_KodCsoportok - HATAROZAT_HATALYBA_LEPESE'
		update KRT_KodCsoportok
			set Nev = 'Hat�rozat hat�lyba l�p�se'
				,Note = NULL
		where Id = '2A884DFB-6926-42A1-BC57-EB716C0BE55A'
	END
GO
IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = 'A5B82A4D-6D22-E911-80C9-00155D020B39')
	BEGIN
	    PRINT 'INSERT KRT_KodCsoportok - IGEN_NEM'
		insert into KRT_KodCsoportok(
		Id
		,Kod
		,Nev
		,Modosithato
		,BesorolasiSema
		,Note
		) values (
		'A5B82A4D-6D22-E911-80C9-00155D020B39'
		,'IGEN_NEM'
		,'Igen_Nem'
		,'1'
		,'0'
		,NULL
		);
	END
	ELSE
	BEGIN
	     PRINT 'UPDATE KRT_KodCsoportok - IGEN_NEM'
		update KRT_KodCsoportok
			set Nev = 'Igen_Nem'
				,Note = NULL
		where Id = 'A5B82A4D-6D22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = '5D7B98D5-EE7E-454D-AADC-A0EC00E1C9B6')
	BEGIN
	    PRINT 'INSERT KRT_KodCsoportok - JOGORVOSLATI_DONTES'
		insert into KRT_KodCsoportok(
		Id
		,Kod
		,Nev
		,Modosithato
		,BesorolasiSema
		,Note
		) values (
		'5D7B98D5-EE7E-454D-AADC-A0EC00E1C9B6'
		,'JOGORVOSLATI_DONTES'
		,'A jogorvoslati elj�r�sban sz�letett d�nt�s'
		,'1'
		,'0'
		,'Felhaszn�l�s: Hat�s�gi adatok'
		);
	END
	ELSE
	BEGIN
	     PRINT 'UPDATE KRT_KodCsoportok - JOGORVOSLATI_DONTES'
		update KRT_KodCsoportok
			set Nev = 'A jogorvoslati elj�r�sban sz�letett d�nt�s'
				,Note = 'Felhaszn�l�s: Hat�s�gi adatok'
		where Id = '5D7B98D5-EE7E-454D-AADC-A0EC00E1C9B6'
	END
GO
IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = '613AD56B-BF2D-472E-9876-C6F646AA30F6')
	BEGIN
	    PRINT 'INSERT KRT_KodCsoportok - JOGORVOSLATI_DONTES_TARTALMA'
		insert into KRT_KodCsoportok(
		Id
		,Kod
		,Nev
		,Modosithato
		,BesorolasiSema
		,Note
		) values (
		'613AD56B-BF2D-472E-9876-C6F646AA30F6'
		,'JOGORVOSLATI_DONTES_TARTALMA'
		,'Jogorvoslati elj�r�sban sz�letett d�nt�st tartalma'
		,'1'
		,'0'
		,'Felhaszn�l�s: Hat�s�gi adatok'
		);
	END
	ELSE
	BEGIN
	     PRINT 'UPDATE KRT_KodCsoportok - JOGORVOSLATI_DONTES_TARTALMA'
		update KRT_KodCsoportok
			set Nev = 'Jogorvoslati elj�r�sban sz�letett d�nt�st tartalma'
				,Note = 'Felhaszn�l�s: Hat�s�gi adatok'
		where Id = '613AD56B-BF2D-472E-9876-C6F646AA30F6'
	END
GO
IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = '54E6405E-1EE3-4B8F-BCC1-F1F37622E4E4')
	BEGIN
	    PRINT 'INSERT KRT_KodCsoportok - JOGORVOSLATI_DONTES_TIPUSA'
		insert into KRT_KodCsoportok(
		Id
		,Kod
		,Nev
		,Modosithato
		,BesorolasiSema
		,Note
		) values (
		'54E6405E-1EE3-4B8F-BCC1-F1F37622E4E4'
		,'JOGORVOSLATI_DONTES_TIPUSA'
		,'Jogorvoslati elj�r�s sor�n �rintett d�nt�s t�pusa'
		,'1'
		,'0'
		,'Felhaszn�l�s: Hat�s�gi adatok'
		);
	END
	ELSE
	BEGIN
	     PRINT 'UPDATE KRT_KodCsoportok - JOGORVOSLATI_DONTES_TIPUSA'
		update KRT_KodCsoportok
			set Nev = 'Jogorvoslati elj�r�s sor�n �rintett d�nt�s t�pusa'
				,Note = 'Felhaszn�l�s: Hat�s�gi adatok'
		where Id = '54E6405E-1EE3-4B8F-BCC1-F1F37622E4E4'
	END
GO
IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = '81922B1C-D58B-463B-9EB2-060063413978')
	BEGIN
	    PRINT 'INSERT KRT_KodCsoportok - JOGORVOSLATI_DONTEST_HOZTA'
		insert into KRT_KodCsoportok(
		Id
		,Kod
		,Nev
		,Modosithato
		,BesorolasiSema
		,Note
		) values (
		'81922B1C-D58B-463B-9EB2-060063413978'
		,'JOGORVOSLATI_DONTEST_HOZTA'
		,'Jogorvoslati elj�r�sban sz�letett d�nt�st meghozta'
		,'1'
		,'0'
		,'Felhaszn�l�s: Hat�s�gi adatok'
		);
	END
	ELSE
	BEGIN
	     PRINT 'UPDATE KRT_KodCsoportok - JOGORVOSLATI_DONTEST_HOZTA'
		update KRT_KodCsoportok
			set Nev = 'Jogorvoslati elj�r�sban sz�letett d�nt�st meghozta'
				,Note = 'Felhaszn�l�s: Hat�s�gi adatok'
		where Id = '81922B1C-D58B-463B-9EB2-060063413978'
	END
GO
IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = '55E4B70F-454F-4334-AEC6-1BF5B967E3C7')
	BEGIN
	    PRINT 'INSERT KRT_KodCsoportok - JOGORVOSLATI_ELJARAS_TIPUSA'
		insert into KRT_KodCsoportok(
		Id
		,Kod
		,Nev
		,Modosithato
		,BesorolasiSema
		,Note
		) values (
		'55E4B70F-454F-4334-AEC6-1BF5B967E3C7'
		,'JOGORVOSLATI_ELJARAS_TIPUSA'
		,'Jogorvoslati elj�r�s t�pusa'
		,'1'
		,'0'
		,'Felhaszn�l�s: Hat�s�gi adatok'
		);
	END
	ELSE
	BEGIN
	     PRINT 'UPDATE KRT_KodCsoportok - JOGORVOSLATI_ELJARAS_TIPUSA'
		update KRT_KodCsoportok
			set Nev = 'Jogorvoslati elj�r�s t�pusa'
				,Note = 'Felhaszn�l�s: Hat�s�gi adatok'
		where Id = '55E4B70F-454F-4334-AEC6-1BF5B967E3C7'
	END
GO
IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = '78830FE6-242C-47CE-B819-DAE70FA3D579')
	BEGIN
	    PRINT 'INSERT KRT_KodCsoportok - UGYINTEZES_IDOTARTAMA'
		insert into KRT_KodCsoportok(
		Id
		,Kod
		,Nev
		,Modosithato
		,BesorolasiSema
		,Note
		) values (
		'78830FE6-242C-47CE-B819-DAE70FA3D579'
		,'UGYINTEZES_IDOTARTAMA'
		,'�gyint�z�s id�tartama'
		,'1'
		,'0'
		,'Felhaszn�l�s: Hat�s�gi adatok'
		);
	END
	ELSE
	BEGIN
	     PRINT 'UPDATE KRT_KodCsoportok - UGYINTEZES_IDOTARTAMA'
		update KRT_KodCsoportok
			set Nev = '�gyint�z�s id�tartama'
				,Note = 'Felhaszn�l�s: Hat�s�gi adatok'
		where Id = '78830FE6-242C-47CE-B819-DAE70FA3D579'
	END
GO
IF NOT EXISTS(SELECT 1 FROM KRT_KodCsoportok WHERE ID = '36084CB1-630F-4A8A-A7A0-0DA03D275768')
	BEGIN
	    PRINT 'INSERT KRT_KodCsoportok - VEGZES_HATALYBA_LEPESE'
		insert into KRT_KodCsoportok(
		Id
		,Kod
		,Nev
		,Modosithato
		,BesorolasiSema
		,Note
		) values (
		'36084CB1-630F-4A8A-A7A0-0DA03D275768'
		,'VEGZES_HATALYBA_LEPESE'
		,'V�gz�s hat�lyba l�p�se'
		,'1'
		,'0'
		,NULL
		);
	END
	ELSE
	BEGIN
	     PRINT 'UPDATE KRT_KodCsoportok - VEGZES_HATALYBA_LEPESE'
		update KRT_KodCsoportok
			set Nev = 'V�gz�s hat�lyba l�p�se'
				,Note = NULL
		where Id = '36084CB1-630F-4A8A-A7A0-0DA03D275768'
	END
GO

-- KRT_Kodatarak ----------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'BC65C5EF-D5F0-410F-B383-9B729BB64B0A')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 1' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'BC65C5EF-D5F0-410F-B383-9B729BB64B0A'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'0F5014C5-40B6-4B85-835E-3138DA141B46'
		,'1'
		,'�n�ll� hat�rozat'
		,'1'	
		,'01'
		,'1'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 1' 
		update KRT_KodTarak
			set Nev = '�n�ll� hat�rozat'
				,Sorrend = '01'
				,RovidNev = '1'
		where Id = 'BC65C5EF-D5F0-410F-B383-9B729BB64B0A'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'BCDB301E-C339-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 2' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'BCDB301E-C339-E911-80CC-00155D020D03'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'0F5014C5-40B6-4B85-835E-3138DA141B46'
		,'2'
		,'egyezs�g j�v�hagy�s�t tartalmaz� hat�rozat'
		,'1'	
		,'02'
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 2' 
		update KRT_KodTarak
			set Nev = 'egyezs�g j�v�hagy�s�t tartalmaz� hat�rozat'
				,Sorrend = '02'
				,RovidNev = NULL
		where Id = 'BCDB301E-C339-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '0C63C72B-C339-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 3' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '0C63C72B-C339-E911-80CC-00155D020D03'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'0F5014C5-40B6-4B85-835E-3138DA141B46'
		,'3'
		,'hat�s�gi bizony�tv�ny/hat�s�gi igazolv�ny'
		,'1'	
		,'03'
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 3' 
		update KRT_KodTarak
			set Nev = 'hat�s�gi bizony�tv�ny/hat�s�gi igazolv�ny'
				,Sorrend = '03'
				,RovidNev = NULL
		where Id = '0C63C72B-C339-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '4C08156C-B7EC-4E1E-987C-B8C72FC3F4B5')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 4' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '4C08156C-B7EC-4E1E-987C-B8C72FC3F4B5'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'0F5014C5-40B6-4B85-835E-3138DA141B46'
		,'4'
		,'hat�s�gi szerz�d�s'
		,'1'	
		,'04'
		,'4'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 4' 
		update KRT_KodTarak
			set Nev = 'hat�s�gi szerz�d�s'
				,Sorrend = '04'
				,RovidNev = '4'
		where Id = '4C08156C-B7EC-4E1E-987C-B8C72FC3F4B5'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'AD13974F-828C-446C-B78E-AE91BE519FBF')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 5' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'AD13974F-828C-446C-B78E-AE91BE519FBF'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'0F5014C5-40B6-4B85-835E-3138DA141B46'
		,'5'
		,'k�relem visszautas�t�sa'
		,'1'	
		,'05'
		,'5'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 5' 
		update KRT_KodTarak
			set Nev = 'k�relem visszautas�t�sa'
				,Sorrend = '05'
				,RovidNev = '5'
		where Id = 'AD13974F-828C-446C-B78E-AE91BE519FBF'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '9DD9D4C9-CB78-44C0-835A-D8659AC5842E')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 6' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '9DD9D4C9-CB78-44C0-835A-D8659AC5842E'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'0F5014C5-40B6-4B85-835E-3138DA141B46'
		,'6'
		,'�KR 47. � (1) bekezd�s a) - f) pontjai alapj�n t�rt�n� megsz�ntet�s'
		,'1'	
		,'06'
		,'6'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 6' 
		update KRT_KodTarak
			set Nev = '�KR 47. � (1) bekezd�s a) - f) pontjai alapj�n t�rt�n� megsz�ntet�s'
				,Sorrend = '06'
				,RovidNev = '6'
		where Id = '9DD9D4C9-CB78-44C0-835A-D8659AC5842E'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '88A57F59-DD01-4AC1-8B48-EC6678DF0EC8')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 7' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '88A57F59-DD01-4AC1-8B48-EC6678DF0EC8'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'0F5014C5-40B6-4B85-835E-3138DA141B46'
		,'7'
		,'az els�fok� elj�r�sban hozott egy�b v�gz�s'
		,'1'	
		,'07'
		,'7'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 7' 
		update KRT_KodTarak
			set Nev = 'az els�fok� elj�r�sban hozott egy�b v�gz�s'
				,Sorrend = '07'
				,RovidNev = '7'
		where Id = '88A57F59-DD01-4AC1-8B48-EC6678DF0EC8'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '0F27B994-8218-49E8-B100-5B9738DDF9CF')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 8' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '0F27B994-8218-49E8-B100-5B9738DDF9CF'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'0F5014C5-40B6-4B85-835E-3138DA141B46'
		,'8'
		,'v�grehajt�si elj�r�sban hozott v�gz�s'
		,'1'	
		,'08'
		,'8'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 8' 
		update KRT_KodTarak
			set Nev = 'v�grehajt�si elj�r�sban hozott v�gz�s'
				,Sorrend = '08'
				,RovidNev = '8'
		where Id = '0F27B994-8218-49E8-B100-5B9738DDF9CF'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'F0C62B16-D6DD-406D-980D-4C98E92F1646')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 9' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'F0C62B16-D6DD-406D-980D-4C98E92F1646'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'0F5014C5-40B6-4B85-835E-3138DA141B46'
		,'9'
		,'�KR 47. � (1) bekezd�s g) pontja alapj�n t�rt�n� megsz�ntet�s'
		,'1'	
		,'09'
		,'9'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ALLAMIG - 9' 
		update KRT_KodTarak
			set Nev = '�KR 47. � (1) bekezd�s g) pontja alapj�n t�rt�n� megsz�ntet�s'
				,Sorrend = '09'
				,RovidNev = '9'
		where Id = 'F0C62B16-D6DD-406D-980D-4C98E92F1646'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'D5D4E079-6462-4BBB-ACF6-83290F3231A8')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ONK - 1' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'D5D4E079-6462-4BBB-ACF6-83290F3231A8'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'9B7672DC-7A11-470E-8E76-FA606556F846'
		,'1'
		,'�n�ll� hat�rozat'
		,'1'	
		,'01'
		,'1'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ONK - 1' 
		update KRT_KodTarak
			set Nev = '�n�ll� hat�rozat'
				,Sorrend = '01'
				,RovidNev = '1'
		where Id = 'D5D4E079-6462-4BBB-ACF6-83290F3231A8'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '9389369D-46B3-4507-AA2C-F80A29DBCD73')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ONK - 2' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '9389369D-46B3-4507-AA2C-F80A29DBCD73'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'9B7672DC-7A11-470E-8E76-FA606556F846'
		,'2'
		,'egyezs�g j�v�hagy�s�t tartalmaz� hat�rozat'
		,'1'	
		,'02'
		,'2'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ONK - 2' 
		update KRT_KodTarak
			set Nev = 'egyezs�g j�v�hagy�s�t tartalmaz� hat�rozat'
				,Sorrend = '02'
				,RovidNev = '2'
		where Id = '9389369D-46B3-4507-AA2C-F80A29DBCD73'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '3A3D1240-C84B-E911-80CD-00155D020D03')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ONK - 3' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '3A3D1240-C84B-E911-80CD-00155D020D03'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'9B7672DC-7A11-470E-8E76-FA606556F846'
		,'3'
		,'hat�s�gi bizony�tv�nyok/hat�s�gi igazolv�nyok'
		,'1'	
		,NULL
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ONK - 3' 
		update KRT_KodTarak
			set Nev = 'hat�s�gi bizony�tv�nyok/hat�s�gi igazolv�nyok'
				,Sorrend = NULL
				,RovidNev = NULL
		where Id = '3A3D1240-C84B-E911-80CD-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '07FB02BE-A6DD-4064-A584-25109DEC6537')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ONK - 4' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '07FB02BE-A6DD-4064-A584-25109DEC6537'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'9B7672DC-7A11-470E-8E76-FA606556F846'
		,'4'
		,'hat�s�gi szerz�d�s'
		,'1'	
		,'04'
		,'3'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ONK - 4' 
		update KRT_KodTarak
			set Nev = 'hat�s�gi szerz�d�s'
				,Sorrend = '04'
				,RovidNev = '3'
		where Id = '07FB02BE-A6DD-4064-A584-25109DEC6537'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '039534D1-EE4F-44D9-A006-C06469F6ACA2')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ONK - 5' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '039534D1-EE4F-44D9-A006-C06469F6ACA2'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'9B7672DC-7A11-470E-8E76-FA606556F846'
		,'5'
		,'k�relem visszautas�t�sa'
		,'1'	
		,'04'
		,'4'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ONK - 5' 
		update KRT_KodTarak
			set Nev = 'k�relem visszautas�t�sa'
				,Sorrend = '04'
				,RovidNev = '4'
		where Id = '039534D1-EE4F-44D9-A006-C06469F6ACA2'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '9B36DF43-FA76-4C1A-B681-2592FB6E4FC0')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ONK - 7' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '9B36DF43-FA76-4C1A-B681-2592FB6E4FC0'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'9B7672DC-7A11-470E-8E76-FA606556F846'
		,'7'
		,'az els�fok� elj�r�sban hozott egy�b v�gz�s'
		,'1'	
		,'07'
		,'6'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ONK - 7' 
		update KRT_KodTarak
			set Nev = 'az els�fok� elj�r�sban hozott egy�b v�gz�s'
				,Sorrend = '07'
				,RovidNev = '6'
		where Id = '9B36DF43-FA76-4C1A-B681-2592FB6E4FC0'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '4CA24951-E933-4C65-A271-786DF0759EA3')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ONK - 8' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '4CA24951-E933-4C65-A271-786DF0759EA3'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'9B7672DC-7A11-470E-8E76-FA606556F846'
		,'8'
		,'v�grehajt�si elj�r�sban hozott v�gz�s'
		,'1'	
		,'08'
		,'7'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ONK - 8' 
		update KRT_KodTarak
			set Nev = 'v�grehajt�si elj�r�sban hozott v�gz�s'
				,Sorrend = '08'
				,RovidNev = '7'
		where Id = '4CA24951-E933-4C65-A271-786DF0759EA3'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '4FD402DD-5245-4A8E-B095-9CBAB99DC54C')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTES_FORMAJA_ONK - 9' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '4FD402DD-5245-4A8E-B095-9CBAB99DC54C'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'9B7672DC-7A11-470E-8E76-FA606556F846'
		,'9'
		,'�KR 47. � (1) g) alapj�n t�rt�n� megsz�ntet�s'
		,'1'	
		,'05'
		,'5'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTES_FORMAJA_ONK - 9' 
		update KRT_KodTarak
			set Nev = '�KR 47. � (1) g) alapj�n t�rt�n� megsz�ntet�s'
				,Sorrend = '05'
				,RovidNev = '5'
		where Id = '4FD402DD-5245-4A8E-B095-9CBAB99DC54C'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '75081FFC-173B-4EEB-A82A-A0EC00E1C9AC')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTEST_HOZTA - 1' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '75081FFC-173B-4EEB-A82A-A0EC00E1C9AC'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'066FD6CE-7442-4DFB-A52A-41BC6087A2D9'
		,'1'
		,'k�pvisel� test�let'
		,'1'	
		,'01'
		,'1'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTEST_HOZTA - 1' 
		update KRT_KodTarak
			set Nev = 'k�pvisel� test�let'
				,Sorrend = '01'
				,RovidNev = '1'
		where Id = '75081FFC-173B-4EEB-A82A-A0EC00E1C9AC'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '66ABB0A6-B6A6-4AF6-9B8E-A0EC00E1C9B5')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTEST_HOZTA - 2' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '66ABB0A6-B6A6-4AF6-9B8E-A0EC00E1C9B5'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'066FD6CE-7442-4DFB-A52A-41BC6087A2D9'
		,'2'
		,'bizotts�g'
		,'1'	
		,'02'
		,'2'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTEST_HOZTA - 2' 
		update KRT_KodTarak
			set Nev = 'bizotts�g'
				,Sorrend = '02'
				,RovidNev = '2'
		where Id = '66ABB0A6-B6A6-4AF6-9B8E-A0EC00E1C9B5'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '46715D29-D04D-4F37-A720-A0EE00AF5868')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTEST_HOZTA - 4' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '46715D29-D04D-4F37-A720-A0EE00AF5868'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'066FD6CE-7442-4DFB-A52A-41BC6087A2D9'
		,'4'
		,'(f�)polg�rmester'
		,'1'	
		,'03'
		,'3'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTEST_HOZTA - 4' 
		update KRT_KodTarak
			set Nev = '(f�)polg�rmester'
				,Sorrend = '03'
				,RovidNev = '3'
		where Id = '46715D29-D04D-4F37-A720-A0EE00AF5868'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'C6BA0745-2214-4F48-98EB-A0EE00AF5872')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTEST_HOZTA - 5' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'C6BA0745-2214-4F48-98EB-A0EE00AF5872'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'066FD6CE-7442-4DFB-A52A-41BC6087A2D9'
		,'5'
		,'(f�)jegyz�'
		,'1'	
		,'05'
		,'4'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTEST_HOZTA - 5' 
		update KRT_KodTarak
			set Nev = '(f�)jegyz�'
				,Sorrend = '05'
				,RovidNev = '4'
		where Id = 'C6BA0745-2214-4F48-98EB-A0EE00AF5872'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'E038314D-C139-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTEST_HOZTA - 6' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'E038314D-C139-E911-80CC-00155D020D03'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'066FD6CE-7442-4DFB-A52A-41BC6087A2D9'
		,'6'
		,'r�sz�nkorm�nyzat test�lete'
		,'1'	
		,'04'
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTEST_HOZTA - 6' 
		update KRT_KodTarak
			set Nev = 'r�sz�nkorm�nyzat test�lete'
				,Sorrend = '04'
				,RovidNev = NULL
		where Id = 'E038314D-C139-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'B5633B6B-C139-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTEST_HOZTA - 7' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'B5633B6B-C139-E911-80CC-00155D020D03'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'066FD6CE-7442-4DFB-A52A-41BC6087A2D9'
		,'7'
		,'t�rsul�si tan�cs'
		,'1'	
		,'06'
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTEST_HOZTA - 7' 
		update KRT_KodTarak
			set Nev = 't�rsul�si tan�cs'
				,Sorrend = '06'
				,RovidNev = NULL
		where Id = 'B5633B6B-C139-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'ADFB8960-C239-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTEST_HOZTA_ALLAMIG - 3' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'ADFB8960-C239-E911-80CC-00155D020D03'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'780AC33B-C239-E911-80CC-00155D020D03'
		,'3'
		,'�gyint�z�'
		,'1'	
		,'03'
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTEST_HOZTA_ALLAMIG - 3' 
		update KRT_KodTarak
			set Nev = '�gyint�z�'
				,Sorrend = '03'
				,RovidNev = NULL
		where Id = 'ADFB8960-C239-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'AC84B351-C239-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTEST_HOZTA_ALLAMIG - 4' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'AC84B351-C239-E911-80CC-00155D020D03'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'780AC33B-C239-E911-80CC-00155D020D03'
		,'4'
		,'(f�)polg�rmester'
		,'1'	
		,'01'
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTEST_HOZTA_ALLAMIG - 4' 
		update KRT_KodTarak
			set Nev = '(f�)polg�rmester'
				,Sorrend = '01'
				,RovidNev = NULL
		where Id = 'AC84B351-C239-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'D7ECED58-C239-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - DONTEST_HOZTA_ALLAMIG - 5' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'D7ECED58-C239-E911-80CC-00155D020D03'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'780AC33B-C239-E911-80CC-00155D020D03'
		,'5'
		,'(f�)jegyz�'
		,'1'	
		,'02'
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - DONTEST_HOZTA_ALLAMIG - 5' 
		update KRT_KodTarak
			set Nev = '(f�)jegyz�'
				,Sorrend = '02'
				,RovidNev = NULL
		where Id = 'D7ECED58-C239-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'B8C5BB44-BE4A-4318-A17D-17BB70C2B57A')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - HATAROZAT_HATALYBA_LEPESE - 1' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'B8C5BB44-BE4A-4318-A17D-17BB70C2B57A'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'2A884DFB-6926-42A1-BC57-EB716C0BE55A'
		,'1'
		,'Hat�lyba l�pett'
		,'1'	
		,'01'
		,'1'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - HATAROZAT_HATALYBA_LEPESE - 1' 
		update KRT_KodTarak
			set Nev = 'Hat�lyba l�pett'
				,Sorrend = '01'
				,RovidNev = '1'
		where Id = 'B8C5BB44-BE4A-4318-A17D-17BB70C2B57A'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '63C1AACF-DDC8-4D6D-ABCD-261AB6147DCA')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - HATAROZAT_HATALYBA_LEPESE - 2' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '63C1AACF-DDC8-4D6D-ABCD-261AB6147DCA'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'2A884DFB-6926-42A1-BC57-EB716C0BE55A'
		,'2'
		,'Nem l�pett hat�lyba'
		,'1'	
		,'02'
		,'2'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - HATAROZAT_HATALYBA_LEPESE - 2' 
		update KRT_KodTarak
			set Nev = 'Nem l�pett hat�lyba'
				,Sorrend = '02'
				,RovidNev = '2'
		where Id = '63C1AACF-DDC8-4D6D-ABCD-261AB6147DCA'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'EB356579-6D22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - IGEN_NEM - 0' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'EB356579-6D22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'A5B82A4D-6D22-E911-80C9-00155D020B39'
		,'0'
		,'Nem'
		,'1'	
		,NULL
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - IGEN_NEM - 0' 
		update KRT_KodTarak
			set Nev = 'Nem'
				,Sorrend = NULL
				,RovidNev = NULL
		where Id = 'EB356579-6D22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '07204B67-6D22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - IGEN_NEM - 1' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '07204B67-6D22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'A5B82A4D-6D22-E911-80C9-00155D020B39'
		,'1'
		,'Igen'
		,'1'	
		,NULL
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - IGEN_NEM - 1' 
		update KRT_KodTarak
			set Nev = 'Igen'
				,Sorrend = NULL
				,RovidNev = NULL
		where Id = '07204B67-6D22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'E2F9B1BE-DFF2-4D34-A706-A0EC00E1C9B7')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTES - 1' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'E2F9B1BE-DFF2-4D34-A706-A0EC00E1C9B7'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'5D7B98D5-EE7E-454D-AADC-A0EC00E1C9B6'
		,'1'
		,'Kijav�t�sra ker�lt sor'
		,'1'	
		,'01'
		,'1'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTES - 1' 
		update KRT_KodTarak
			set Nev = 'Kijav�t�sra ker�lt sor'
				,Sorrend = '01'
				,RovidNev = '1'
		where Id = 'E2F9B1BE-DFF2-4D34-A706-A0EC00E1C9B7'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '7D6C2F4C-2E4E-412F-B82B-A0EC00E1C9B7')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTES - 2' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '7D6C2F4C-2E4E-412F-B82B-A0EC00E1C9B7'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'5D7B98D5-EE7E-454D-AADC-A0EC00E1C9B6'
		,'2'
		,'Kieg�sz�t�sre ker�lt sor'
		,'1'	
		,'02'
		,'2'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTES - 2' 
		update KRT_KodTarak
			set Nev = 'Kieg�sz�t�sre ker�lt sor'
				,Sorrend = '02'
				,RovidNev = '2'
		where Id = '7D6C2F4C-2E4E-412F-B82B-A0EC00E1C9B7'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '7205F629-F16E-422A-8D16-02108C68B68B')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTES_TARTALMA - 2' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '7205F629-F16E-422A-8D16-02108C68B68B'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'613AD56B-BF2D-472E-9876-C6F646AA30F6'
		,'2'
		,'M�dos�t�s'
		,'1'	
		,'02'
		,'1'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTES_TARTALMA - 2' 
		update KRT_KodTarak
			set Nev = 'M�dos�t�s'
				,Sorrend = '02'
				,RovidNev = '1'
		where Id = '7205F629-F16E-422A-8D16-02108C68B68B'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'EF919B5D-529C-478A-874D-27412750EB34')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTES_TARTALMA - 3' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'EF919B5D-529C-478A-874D-27412750EB34'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'613AD56B-BF2D-472E-9876-C6F646AA30F6'
		,'3'
		,'Visszavon�s'
		,'1'	
		,'03'
		,'2'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTES_TARTALMA - 3' 
		update KRT_KodTarak
			set Nev = 'Visszavon�s'
				,Sorrend = '03'
				,RovidNev = '2'
		where Id = 'EF919B5D-529C-478A-874D-27412750EB34'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'FD062925-6FBC-4262-8345-740C3EDCB31C')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTES_TARTALMA - 4' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'FD062925-6FBC-4262-8345-740C3EDCB31C'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'613AD56B-BF2D-472E-9876-C6F646AA30F6'
		,'4'
		,'�j d�nt�s'
		,'1'	
		,'04'
		,'3'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTES_TARTALMA - 4' 
		update KRT_KodTarak
			set Nev = '�j d�nt�s'
				,Sorrend = '04'
				,RovidNev = '3'
		where Id = 'FD062925-6FBC-4262-8345-740C3EDCB31C'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '3AC72E50-EFAD-465B-8390-122CE5AB5CEE')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTES_TARTALMA - 5' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '3AC72E50-EFAD-465B-8390-122CE5AB5CEE'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'613AD56B-BF2D-472E-9876-C6F646AA30F6'
		,'5'
		,'Helybenhagy�s (kereset ill. k�relem, �jrafelv�teli vagy m�lt�nyoss�gi k�relem elutas�t�sa)'
		,'1'	
		,'05'
		,'4'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTES_TARTALMA - 5' 
		update KRT_KodTarak
			set Nev = 'Helybenhagy�s (kereset ill. k�relem, �jrafelv�teli vagy m�lt�nyoss�gi k�relem elutas�t�sa)'
				,Sorrend = '05'
				,RovidNev = '4'
		where Id = '3AC72E50-EFAD-465B-8390-122CE5AB5CEE'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '0C4DF7E8-9995-4E50-87C0-856D5EAB2EE3')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTES_TARTALMA - 6' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '0C4DF7E8-9995-4E50-87C0-856D5EAB2EE3'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'613AD56B-BF2D-472E-9876-C6F646AA30F6'
		,'6'
		,'Megv�ltoztat�s'
		,'1'	
		,'06'
		,'5'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTES_TARTALMA - 6' 
		update KRT_KodTarak
			set Nev = 'Megv�ltoztat�s'
				,Sorrend = '06'
				,RovidNev = '5'
		where Id = '0C4DF7E8-9995-4E50-87C0-856D5EAB2EE3'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'FE1B0B87-E0F6-40A2-A276-9FE6AA7C98E0')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTES_TARTALMA - 7' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'FE1B0B87-E0F6-40A2-A276-9FE6AA7C98E0'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'613AD56B-BF2D-472E-9876-C6F646AA30F6'
		,'7'
		,'Megsemmis�t�s vagy hat�lyon k�v�l helyez�s'
		,'1'	
		,'07'
		,'6'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTES_TARTALMA - 7' 
		update KRT_KodTarak
			set Nev = 'Megsemmis�t�s vagy hat�lyon k�v�l helyez�s'
				,Sorrend = '07'
				,RovidNev = '6'
		where Id = 'FE1B0B87-E0F6-40A2-A276-9FE6AA7C98E0'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '008B35E1-EE4B-4BF0-959F-960770922CCF')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTES_TARTALMA - 8' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '008B35E1-EE4B-4BF0-959F-960770922CCF'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'613AD56B-BF2D-472E-9876-C6F646AA30F6'
		,'8'
		,'Megsemmis�t�s vagy hat�lyon k�v�l helyez�s �s �j elj�r�sra utas�t�s'
		,'1'	
		,'08'
		,'7'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTES_TARTALMA - 8' 
		update KRT_KodTarak
			set Nev = 'Megsemmis�t�s vagy hat�lyon k�v�l helyez�s �s �j elj�r�sra utas�t�s'
				,Sorrend = '08'
				,RovidNev = '7'
		where Id = '008B35E1-EE4B-4BF0-959F-960770922CCF'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '0A178ED7-E2B3-445E-B279-B6E40D728316')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTES_TIPUSA - 1' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '0A178ED7-E2B3-445E-B279-B6E40D728316'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'54E6405E-1EE3-4B8F-BCC1-F1F37622E4E4'
		,'1'
		,'V�gz�s'
		,'1'	
		,'01'
		,'1'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTES_TIPUSA - 1' 
		update KRT_KodTarak
			set Nev = 'V�gz�s'
				,Sorrend = '01'
				,RovidNev = '1'
		where Id = '0A178ED7-E2B3-445E-B279-B6E40D728316'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'E782F12A-60F3-4DA5-8B9E-CA4F7CDCF119')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTES_TIPUSA - 2' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'E782F12A-60F3-4DA5-8B9E-CA4F7CDCF119'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'54E6405E-1EE3-4B8F-BCC1-F1F37622E4E4'
		,'2'
		,'�rdemi d�nt�s'
		,'1'	
		,'02'
		,'2'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTES_TIPUSA - 2' 
		update KRT_KodTarak
			set Nev = '�rdemi d�nt�s'
				,Sorrend = '02'
				,RovidNev = '2'
		where Id = 'E782F12A-60F3-4DA5-8B9E-CA4F7CDCF119'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'E4566679-EA7C-4DDA-B93C-F9A55CC188FC')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTEST_HOZTA - 1' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'E4566679-EA7C-4DDA-B93C-F9A55CC188FC'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'81922B1C-D58B-463B-9EB2-060063413978'
		,'1'
		,'Az els�fok� hat�s�g'
		,'1'	
		,'01'
		,'1'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTEST_HOZTA - 1' 
		update KRT_KodTarak
			set Nev = 'Az els�fok� hat�s�g'
				,Sorrend = '01'
				,RovidNev = '1'
		where Id = 'E4566679-EA7C-4DDA-B93C-F9A55CC188FC'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '352B7D42-B539-479D-938F-AB7D4B2BD4C2')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTEST_HOZTA - 2' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '352B7D42-B539-479D-938F-AB7D4B2BD4C2'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'81922B1C-D58B-463B-9EB2-060063413978'
		,'2'
		,'Az els�fok� hat�s�g �jrafelv�teli elj�r�sban'
		,'2'	
		,'02'
		,'2'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTEST_HOZTA - 2' 
		update KRT_KodTarak
			set Nev = 'Az els�fok� hat�s�g �jrafelv�teli elj�r�sban'
				,Sorrend = '02'
				,RovidNev = '2'
		where Id = '352B7D42-B539-479D-938F-AB7D4B2BD4C2'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '9FFEC134-11DD-4566-BC86-A0EC00E1C9B6')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTEST_HOZTA - 4' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '9FFEC134-11DD-4566-BC86-A0EC00E1C9B6'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'81922B1C-D58B-463B-9EB2-060063413978'
		,'4'
		,'A k�pvisel� test�let'
		,'1'	
		,'04'
		,'3'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTEST_HOZTA - 4' 
		update KRT_KodTarak
			set Nev = 'A k�pvisel� test�let'
				,Sorrend = '04'
				,RovidNev = '3'
		where Id = '9FFEC134-11DD-4566-BC86-A0EC00E1C9B6'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '938B3BE4-1001-4F5D-A95C-A0EC00E1C9B6')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTEST_HOZTA - 5' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '938B3BE4-1001-4F5D-A95C-A0EC00E1C9B6'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'81922B1C-D58B-463B-9EB2-060063413978'
		,'5'
		,'A Korm�nyhivatal'
		,'1'	
		,'05'
		,'4'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTEST_HOZTA - 5' 
		update KRT_KodTarak
			set Nev = 'A Korm�nyhivatal'
				,Sorrend = '05'
				,RovidNev = '4'
		where Id = '938B3BE4-1001-4F5D-A95C-A0EC00E1C9B6'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '8A60253A-2C08-4CA8-9394-86FFE4E89CFA')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTEST_HOZTA - 6' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '8A60253A-2C08-4CA8-9394-86FFE4E89CFA'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'81922B1C-D58B-463B-9EB2-060063413978'
		,'6'
		,'A dekoncentr�lt szerv'
		,'1'	
		,'06'
		,'5'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTEST_HOZTA - 6' 
		update KRT_KodTarak
			set Nev = 'A dekoncentr�lt szerv'
				,Sorrend = '06'
				,RovidNev = '5'
		where Id = '8A60253A-2C08-4CA8-9394-86FFE4E89CFA'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '41D24DC9-7620-4737-8796-6D9BEBADA197')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTEST_HOZTA - 7' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '41D24DC9-7620-4737-8796-6D9BEBADA197'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'81922B1C-D58B-463B-9EB2-060063413978'
		,'7'
		,'A b�r�s�g'
		,'1'	
		,'07'
		,'6'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTEST_HOZTA - 7' 
		update KRT_KodTarak
			set Nev = 'A b�r�s�g'
				,Sorrend = '07'
				,RovidNev = '6'
		where Id = '41D24DC9-7620-4737-8796-6D9BEBADA197'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'E7B42ECE-F536-41BF-8A40-27B103FB2445')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_DONTEST_HOZTA - 8' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'E7B42ECE-F536-41BF-8A40-27B103FB2445'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'81922B1C-D58B-463B-9EB2-060063413978'
		,'8'
		,'A fel�gyeleti szerv (fel�gyeleti int�zked�s eset�n)'
		,'1'	
		,'08'
		,'7'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_DONTEST_HOZTA - 8' 
		update KRT_KodTarak
			set Nev = 'A fel�gyeleti szerv (fel�gyeleti int�zked�s eset�n)'
				,Sorrend = '08'
				,RovidNev = '7'
		where Id = 'E7B42ECE-F536-41BF-8A40-27B103FB2445'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'A0475725-A363-428F-AE4A-F0A860F3115A')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_ELJARAS_TIPUSA - 1' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'A0475725-A363-428F-AE4A-F0A860F3115A'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'55E4B70F-454F-4334-AEC6-1BF5B967E3C7'
		,'1'
		,'K�relem alapj�n indult'
		,'1'	
		,'01'
		,'1'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_ELJARAS_TIPUSA - 1' 
		update KRT_KodTarak
			set Nev = 'K�relem alapj�n indult'
				,Sorrend = '01'
				,RovidNev = '1'
		where Id = 'A0475725-A363-428F-AE4A-F0A860F3115A'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '33D1FE5C-8DB9-42F3-A194-8CC1C52B4254')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - JOGORVOSLATI_ELJARAS_TIPUSA - 2' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '33D1FE5C-8DB9-42F3-A194-8CC1C52B4254'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'55E4B70F-454F-4334-AEC6-1BF5B967E3C7'
		,'2'
		,'Hivatalb�l indult'
		,'1'	
		,'02'
		,'2'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - JOGORVOSLATI_ELJARAS_TIPUSA - 2' 
		update KRT_KodTarak
			set Nev = 'Hivatalb�l indult'
				,Sorrend = '02'
				,RovidNev = '2'
		where Id = '33D1FE5C-8DB9-42F3-A194-8CC1C52B4254'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '11A6DD15-2E2E-490E-9CF0-678F99993A92')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - UGYINTEZES_IDOTARTAMA - 1' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '11A6DD15-2E2E-490E-9CF0-678F99993A92'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'78830FE6-242C-47CE-B819-DAE70FA3D579'
		,'1'
		,'Hat�rid�n bel�l'
		,'1'	
		,'01'
		,'1'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - UGYINTEZES_IDOTARTAMA - 1' 
		update KRT_KodTarak
			set Nev = 'Hat�rid�n bel�l'
				,Sorrend = '01'
				,RovidNev = '1'
		where Id = '11A6DD15-2E2E-490E-9CF0-678F99993A92'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'F5702F34-DD84-472E-A1EE-A1D8011D9912')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - UGYINTEZES_IDOTARTAMA - 2' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'F5702F34-DD84-472E-A1EE-A1D8011D9912'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'78830FE6-242C-47CE-B819-DAE70FA3D579'
		,'2'
		,'Hat�rid�n t�l'
		,'1'	
		,'02'
		,'2'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - UGYINTEZES_IDOTARTAMA - 2' 
		update KRT_KodTarak
			set Nev = 'Hat�rid�n t�l'
				,Sorrend = '02'
				,RovidNev = '2'
		where Id = 'F5702F34-DD84-472E-A1EE-A1D8011D9912'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = 'E54F1F95-AA86-4419-8BE5-6C4F2FC8E228')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - VEGZES_HATALYBA_LEPESE - 1' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 'E54F1F95-AA86-4419-8BE5-6C4F2FC8E228'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'36084CB1-630F-4A8A-A7A0-0DA03D275768'
		,'1'
		,'Hat�lyba l�pett'
		,'1'	
		,'01'
		,'1'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - VEGZES_HATALYBA_LEPESE - 1' 
		update KRT_KodTarak
			set Nev = 'Hat�lyba l�pett'
				,Sorrend = '01'
				,RovidNev = '1'
		where Id = 'E54F1F95-AA86-4419-8BE5-6C4F2FC8E228'
	END
GO
IF NOT EXISTS (SELECT 1 from KRT_KodTarak WHERE Id = '3F566AF2-6AA5-48A0-B563-8BCDFBF0B4F1')
	BEGIN
		PRINT 'INSERT KRT_KodTarak - VEGZES_HATALYBA_LEPESE - 2' 	
		insert into KRT_KodTarak(
		 Id
		,Org
		,KodCsoport_Id
		,Kod
		,Nev
		,Modosithato
		,Sorrend
		,RovidNev
		) values (
		 '3F566AF2-6AA5-48A0-B563-8BCDFBF0B4F1'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9#'
		,'36084CB1-630F-4A8A-A7A0-0DA03D275768'
		,'2'
		,'Nem l�pett hat�lyba'
		,'1'	
		,'02'
		,'2'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE KRT_KodTarak - VEGZES_HATALYBA_LEPESE - 2' 
		update KRT_KodTarak
			set Nev = 'Nem l�pett hat�lyba'
				,Sorrend = '02'
				,RovidNev = '2'
		where Id = '3F566AF2-6AA5-48A0-B563-8BCDFBF0B4F1'
	END
GO

-- EREC_TargySzavak--------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = 'D3DB2868-6922-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - D�nt�st hozta �nkorm�nyzati' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'D3DB2868-6922-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'D�nt�st hozta �nkorm�nyzati'
		,'Dontest_hozta'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'DONTEST_HOZTA'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - D�nt�st hozta �nkorm�nyzati' 	 
		update EREC_TargySzavak
			set TargySzavak = 'D�nt�st hozta �nkorm�nyzati'
				,BelsoAzonosito = 'Dontest_hozta'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'DONTEST_HOZTA'
		where Id = 'D3DB2868-6922-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '295DE9B7-6922-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - D�nt�s form�ja �nkorm�nyzati' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'295DE9B7-6922-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'D�nt�s form�ja �nkorm�nyzati'
		,'Dontes_formaja_onkormanyzati'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'DONTES_FORMAJA_ONK'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - D�nt�s form�ja �nkorm�nyzati' 	 
		update EREC_TargySzavak
			set TargySzavak = 'D�nt�s form�ja �nkorm�nyzati'
				,BelsoAzonosito = 'Dontes_formaja_onkormanyzati'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'DONTES_FORMAJA_ONK'
		where Id = '295DE9B7-6922-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = 'CFBDB7DA-6922-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - �gyint�z�s id�tartama' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'CFBDB7DA-6922-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'�gyint�z�s id�tartama'
		,'Ugyintezes_idotartama'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'UGYINTEZES_IDOTARTAMA'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - �gyint�z�s id�tartama' 	 
		update EREC_TargySzavak
			set TargySzavak = '�gyint�z�s id�tartama'
				,BelsoAzonosito = 'Ugyintezes_idotartama'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'UGYINTEZES_IDOTARTAMA'
		where Id = 'CFBDB7DA-6922-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '89FC37FE-6922-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - Hat�rid� t�ll�p�s napokban' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'89FC37FE-6922-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'Hat�rid� t�ll�p�s napokban'
		,'Hatarido_tullepes_napokban'
		,'0'	
		,'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls'
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - Hat�rid� t�ll�p�s napokban' 	 
		update EREC_TargySzavak
			set TargySzavak = 'Hat�rid� t�ll�p�s napokban'
				,BelsoAzonosito = 'Hatarido_tullepes_napokban'
				,ControlTypeSource = 'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls'
				,ControlTypeDataSource = NULL
		where Id = '89FC37FE-6922-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '6C4EB8FD-6A22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - Jogorvoslati elj�r�s t�pusa' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'6C4EB8FD-6A22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'Jogorvoslati elj�r�s t�pusa'
		,'Jogorvoslati_eljaras_tipusa'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'JOGORVOSLATI_ELJARAS_TIPUSA'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - Jogorvoslati elj�r�s t�pusa' 	 
		update EREC_TargySzavak
			set TargySzavak = 'Jogorvoslati elj�r�s t�pusa'
				,BelsoAzonosito = 'Jogorvoslati_eljaras_tipusa'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'JOGORVOSLATI_ELJARAS_TIPUSA'
		where Id = '6C4EB8FD-6A22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '38D88F24-6B22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - Jogorv_elj�r�sban_d�nt�s_t�pusa' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'38D88F24-6B22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'Jogorv_elj�r�sban_d�nt�s_t�pusa'
		,'Jogorv_eljarasban_dontes_tipusa'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'JOGORVOSLATI_DONTES_TIPUSA'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - Jogorv_elj�r�sban_d�nt�s_t�pusa' 	 
		update EREC_TargySzavak
			set TargySzavak = 'Jogorv_elj�r�sban_d�nt�s_t�pusa'
				,BelsoAzonosito = 'Jogorv_eljarasban_dontes_tipusa'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'JOGORVOSLATI_DONTES_TIPUSA'
		where Id = '38D88F24-6B22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '365911A6-6B22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - Jogorv_elj�r�sban_d�nt�st_hozta' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'365911A6-6B22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'Jogorv_elj�r�sban_d�nt�st_hozta'
		,'Jogorv_eljarasban_dontest_hozta'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'JOGORVOSLATI_DONTEST_HOZTA'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - Jogorv_elj�r�sban_d�nt�st_hozta' 	 
		update EREC_TargySzavak
			set TargySzavak = 'Jogorv_elj�r�sban_d�nt�st_hozta'
				,BelsoAzonosito = 'Jogorv_eljarasban_dontest_hozta'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'JOGORVOSLATI_DONTEST_HOZTA'
		where Id = '365911A6-6B22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = 'D034DBD1-6B22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - Jogorv_elj�r�sban_d�nt�s_tartalma' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'D034DBD1-6B22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'Jogorv_elj�r�sban_d�nt�s_tartalma'
		,'Jogorv_eljarasban_dontes_tartalma'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'JOGORVOSLATI_DONTES_TARTALMA'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - Jogorv_elj�r�sban_d�nt�s_tartalma' 	 
		update EREC_TargySzavak
			set TargySzavak = 'Jogorv_elj�r�sban_d�nt�s_tartalma'
				,BelsoAzonosito = 'Jogorv_eljarasban_dontes_tartalma'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'JOGORVOSLATI_DONTES_TARTALMA'
		where Id = 'D034DBD1-6B22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '7E22D83E-6C22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - Jogorv_elj�r�sban_d�nt�s_v�ltoz�sa' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'7E22D83E-6C22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'Jogorv_elj�r�sban_d�nt�s_v�ltoz�sa'
		,'Jogorv_eljarasban_dontes_valtozasa'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'JOGORVOSLATI_DONTES'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - Jogorv_elj�r�sban_d�nt�s_v�ltoz�sa' 	 
		update EREC_TargySzavak
			set TargySzavak = 'Jogorv_elj�r�sban_d�nt�s_v�ltoz�sa'
				,BelsoAzonosito = 'Jogorv_eljarasban_dontes_valtozasa'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'JOGORVOSLATI_DONTES'
		where Id = '7E22D83E-6C22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '093372A9-6D22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - Hat�s�gi_ellen�rz�s_t�rt�nt' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'093372A9-6D22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'Hat�s�gi_ellen�rz�s_t�rt�nt'
		,'Hatosagi_ellenorzes_tortent'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'IGEN_NEM'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - Hat�s�gi_ellen�rz�s_t�rt�nt' 	 
		update EREC_TargySzavak
			set TargySzavak = 'Hat�s�gi_ellen�rz�s_t�rt�nt'
				,BelsoAzonosito = 'Hatosagi_ellenorzes_tortent'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'IGEN_NEM'
		where Id = '093372A9-6D22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '127CB717-6E22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - D�nt�s munka�r�k sz�ma' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'127CB717-6E22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'D�nt�s munka�r�k sz�ma'
		,'Dontes_munkaorak_szama'
		,'0'	
		,'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls'
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - D�nt�s munka�r�k sz�ma' 	 
		update EREC_TargySzavak
			set TargySzavak = 'D�nt�s munka�r�k sz�ma'
				,BelsoAzonosito = 'Dontes_munkaorak_szama'
				,ControlTypeSource = 'Contentum.eUIControls.CustomTextBox;Contentum.eUIControls'
				,ControlTypeDataSource = NULL
		where Id = '127CB717-6E22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = 'C7D8A03F-6E22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - Meg�llap�tott elj�r�si k�lts�g' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'C7D8A03F-6E22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'Meg�llap�tott elj�r�si k�lts�g'
		,'Megallapitott_eljarasi_koltseg'
		,'0'	
		,'~/Component/RequiredNumberBox.ascx'
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - Meg�llap�tott elj�r�si k�lts�g' 	 
		update EREC_TargySzavak
			set TargySzavak = 'Meg�llap�tott elj�r�si k�lts�g'
				,BelsoAzonosito = 'Megallapitott_eljarasi_koltseg'
				,ControlTypeSource = '~/Component/RequiredNumberBox.ascx'
				,ControlTypeDataSource = NULL
		where Id = 'C7D8A03F-6E22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '3FBA6E62-6E22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - Kiszabott k�zigazgat�si b�rs�g' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'3FBA6E62-6E22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'Kiszabott k�zigazgat�si b�rs�g'
		,'Kiszabott_kozigazgatasi_birsag'
		,'0'	
		,'~/Component/RequiredNumberBox.ascx'
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - Kiszabott k�zigazgat�si b�rs�g' 	 
		update EREC_TargySzavak
			set TargySzavak = 'Kiszabott k�zigazgat�si b�rs�g'
				,BelsoAzonosito = 'Kiszabott_kozigazgatasi_birsag'
				,ControlTypeSource = '~/Component/RequiredNumberBox.ascx'
				,ControlTypeDataSource = NULL
		where Id = '3FBA6E62-6E22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '04A5A397-6E22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - Somm�s elj�r�sban hozott d�nt�s' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'04A5A397-6E22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'Somm�s elj�r�sban hozott d�nt�s'
		,'Sommas_eljarasban_hozott_dontes'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'IGEN_NEM'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - Somm�s elj�r�sban hozott d�nt�s' 	 
		update EREC_TargySzavak
			set TargySzavak = 'Somm�s elj�r�sban hozott d�nt�s'
				,BelsoAzonosito = 'Sommas_eljarasban_hozott_dontes'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'IGEN_NEM'
		where Id = '04A5A397-6E22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = 'F5AFD3BE-6E22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - 8napon bel�l lez�rt nem somm�s' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'F5AFD3BE-6E22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'8napon bel�l lez�rt nem somm�s'
		,'8napon_belul_lezart_nem_sommas'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'IGEN_NEM'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - 8napon bel�l lez�rt nem somm�s' 	 
		update EREC_TargySzavak
			set TargySzavak = '8napon bel�l lez�rt nem somm�s'
				,BelsoAzonosito = '8napon_belul_lezart_nem_sommas'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'IGEN_NEM'
		where Id = 'F5AFD3BE-6E22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '75FB85DD-6E22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - F�gg� hat�ly� hat�rozat' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'75FB85DD-6E22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'F�gg� hat�ly� hat�rozat'
		,'Fuggo_hatalyu_hatarozat'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'IGEN_NEM'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - F�gg� hat�ly� hat�rozat' 	 
		update EREC_TargySzavak
			set TargySzavak = 'F�gg� hat�ly� hat�rozat'
				,BelsoAzonosito = 'Fuggo_hatalyu_hatarozat'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'IGEN_NEM'
		where Id = '75FB85DD-6E22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '63DA410A-6F22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - Hat�rozat hat�lyba l�p�se' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'63DA410A-6F22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'Hat�rozat hat�lyba l�p�se'
		,'Hatarozat_hatalyba_lepese'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'HATAROZAT_HATALYBA_LEPESE'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - Hat�rozat hat�lyba l�p�se' 	 
		update EREC_TargySzavak
			set TargySzavak = 'Hat�rozat hat�lyba l�p�se'
				,BelsoAzonosito = 'Hatarozat_hatalyba_lepese'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'HATAROZAT_HATALYBA_LEPESE'
		where Id = '63DA410A-6F22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = 'F642A92A-6F22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - F�gg� hat�ly� v�gz�s' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'F642A92A-6F22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'F�gg� hat�ly� v�gz�s'
		,'Fuggo_hatalyu_vegzes'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'IGEN_NEM'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - F�gg� hat�ly� v�gz�s' 	 
		update EREC_TargySzavak
			set TargySzavak = 'F�gg� hat�ly� v�gz�s'
				,BelsoAzonosito = 'Fuggo_hatalyu_vegzes'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'IGEN_NEM'
		where Id = 'F642A92A-6F22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '84893B81-6F22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - V�gz�s hat�lyba l�p�se' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'84893B81-6F22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'V�gz�s hat�lyba l�p�se'
		,'Vegzes_hatalyba_lepese'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'VEGZES_HATALYBA_LEPESE'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - V�gz�s hat�lyba l�p�se' 	 
		update EREC_TargySzavak
			set TargySzavak = 'V�gz�s hat�lyba l�p�se'
				,BelsoAzonosito = 'Vegzes_hatalyba_lepese'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'VEGZES_HATALYBA_LEPESE'
		where Id = '84893B81-6F22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '1B4210AB-6F22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - Hat�s�g �ltal visszafizetett �sszeg' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'1B4210AB-6F22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'Hat�s�g �ltal visszafizetett �sszeg'
		,'Hatosag_altal_visszafizetett_osszeg'
		,'0'	
		,'~/Component/RequiredNumberBox.ascx'
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - Hat�s�g �ltal visszafizetett �sszeg' 	 
		update EREC_TargySzavak
			set TargySzavak = 'Hat�s�g �ltal visszafizetett �sszeg'
				,BelsoAzonosito = 'Hatosag_altal_visszafizetett_osszeg'
				,ControlTypeSource = '~/Component/RequiredNumberBox.ascx'
				,ControlTypeDataSource = NULL
		where Id = '1B4210AB-6F22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '887E0BD3-6F22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - Hat�s�got terhel� elj�r�si k�lts�g' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'887E0BD3-6F22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'Hat�s�got terhel� elj�r�si k�lts�g'
		,'Hatosagot_terhelo_eljarasi_koltseg'
		,'0'	
		,'~/Component/RequiredNumberBox.ascx'
		,NULL
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - Hat�s�got terhel� elj�r�si k�lts�g' 	 
		update EREC_TargySzavak
			set TargySzavak = 'Hat�s�got terhel� elj�r�si k�lts�g'
				,BelsoAzonosito = 'Hatosagot_terhelo_eljarasi_koltseg'
				,ControlTypeSource = '~/Component/RequiredNumberBox.ascx'
				,ControlTypeDataSource = NULL
		where Id = '887E0BD3-6F22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = 'D95E10F6-6F22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - Felf�ggesztett hat�rozat' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'D95E10F6-6F22-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'Felf�ggesztett hat�rozat'
		,'Felfuggesztett_hatarozat'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'IGEN_NEM'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - Felf�ggesztett hat�rozat' 	 
		update EREC_TargySzavak
			set TargySzavak = 'Felf�ggesztett hat�rozat'
				,BelsoAzonosito = 'Felfuggesztett_hatarozat'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'IGEN_NEM'
		where Id = 'D95E10F6-6F22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '69F8042F-7122-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - D�nt�s form�ja �llamigazgat�si' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'69F8042F-7122-E911-80C9-00155D020B39'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'D�nt�s form�ja �llamigazgat�si'
		,'Dontes_formaja_allamigazgatasi'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'DONTES_FORMAJA_ALLAMIG'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - D�nt�s form�ja �llamigazgat�si' 	 
		update EREC_TargySzavak
			set TargySzavak = 'D�nt�s form�ja �llamigazgat�si'
				,BelsoAzonosito = 'Dontes_formaja_allamigazgatasi'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'DONTES_FORMAJA_ALLAMIG'
		where Id = '69F8042F-7122-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_TargySzavak WHERE Id = '7EF8FE72-C739-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_TargySzavak - D�nt�st hozta �llamigazgat�si' 	
		insert into EREC_TargySzavak(
		Id
		,Org
		,Tipus
		,TargySzavak
		,BelsoAzonosito
		,SPSSzinkronizalt
		,ControlTypeSource
		,ControlTypeDataSource
		) values (
		'7EF8FE72-C739-E911-80CC-00155D020D03'
		,'450B510A-7CAA-46B0-83E3-18445C0C53A9'
		,'1'
		,'D�nt�st hozta �llamigazgat�si'
		,'Dontest_hoza_allamigazgatasi'
		,'0'	
		,'~/Component/KodTarakDropDownList.ascx'
		,'DONTEST_HOZTA_ALLAMIG'
		); 
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_TargySzavak - D�nt�st hozta �llamigazgat�si' 	 
		update EREC_TargySzavak
			set TargySzavak = 'D�nt�st hozta �llamigazgat�si'
				,BelsoAzonosito = 'Dontest_hoza_allamigazgatasi'
				,ControlTypeSource = '~/Component/KodTarakDropDownList.ascx'
				,ControlTypeDataSource = 'DONTEST_HOZTA_ALLAMIG'
		where Id = '7EF8FE72-C739-E911-80CC-00155D020D03'
	END
GO

-- EREC_Obj_MetaAdatai--------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '15ED3B7C-6922-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - D�nt�st hozta �nkorm�nyzati' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'15ED3B7C-6922-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'D3DB2868-6922-E911-80C9-00155D020B39'
		,'3'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - D�nt�st hozta �nkorm�nyzati' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '3'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '15ED3B7C-6922-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '20E998C1-6922-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - D�nt�s form�ja �nkorm�nyzati' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'20E998C1-6922-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'295DE9B7-6922-E911-80C9-00155D020B39'
		,'4'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - D�nt�s form�ja �nkorm�nyzati' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '4'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '20E998C1-6922-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = 'F0C83DE6-6922-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - �gyint�z�s id�tartama' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'F0C83DE6-6922-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'CFBDB7DA-6922-E911-80C9-00155D020B39'
		,'5'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - �gyint�z�s id�tartama' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '5'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = 'F0C83DE6-6922-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '78D7C706-6A22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Hat�rid� t�ll�p�s napokban' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'78D7C706-6A22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'89FC37FE-6922-E911-80C9-00155D020B39'
		,'6'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Hat�rid� t�ll�p�s napokban' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '6'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '78D7C706-6A22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = 'E5A12B05-6B22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Jogorvoslati elj�r�s t�pusa' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'E5A12B05-6B22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'6C4EB8FD-6A22-E911-80C9-00155D020B39'
		,'7'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Jogorvoslati elj�r�s t�pusa' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '7'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = 'E5A12B05-6B22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = 'F22A9A31-6B22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�s_t�pusa' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'F22A9A31-6B22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'38D88F24-6B22-E911-80C9-00155D020B39'
		,'8'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�s_t�pusa' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '8'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = 'F22A9A31-6B22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '084D5BAE-6B22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�st_hozta' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'084D5BAE-6B22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'365911A6-6B22-E911-80C9-00155D020B39'
		,'9'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�st_hozta' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '9'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '084D5BAE-6B22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '582B0ED9-6B22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�s_tartalma' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'582B0ED9-6B22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'D034DBD1-6B22-E911-80C9-00155D020B39'
		,'10'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�s_tartalma' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '10'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '582B0ED9-6B22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '31EAFA4A-6C22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�s_v�ltoz�sa' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'31EAFA4A-6C22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'7E22D83E-6C22-E911-80C9-00155D020B39'
		,'11'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�s_v�ltoz�sa' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '11'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '31EAFA4A-6C22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '216BC7B0-6D22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Hat�s�gi_ellen�rz�s_t�rt�nt' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'216BC7B0-6D22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'093372A9-6D22-E911-80C9-00155D020B39'
		,'12'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Hat�s�gi_ellen�rz�s_t�rt�nt' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '12'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '216BC7B0-6D22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '0BCC3820-6E22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - D�nt�s munka�r�k sz�ma' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'0BCC3820-6E22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'127CB717-6E22-E911-80C9-00155D020B39'
		,'13'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - D�nt�s munka�r�k sz�ma' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '13'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '0BCC3820-6E22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '1F3B4647-6E22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Meg�llap�tott elj�r�si k�lts�g' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'1F3B4647-6E22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'C7D8A03F-6E22-E911-80C9-00155D020B39'
		,'14'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Meg�llap�tott elj�r�si k�lts�g' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '14'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '1F3B4647-6E22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '01709369-6E22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Kiszabott k�zigazgat�si b�rs�g' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'01709369-6E22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'3FBA6E62-6E22-E911-80C9-00155D020B39'
		,'15'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Kiszabott k�zigazgat�si b�rs�g' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '15'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '01709369-6E22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '70B9F49F-6E22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Somm�s elj�r�sban hozott d�nt�s' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'70B9F49F-6E22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'04A5A397-6E22-E911-80C9-00155D020B39'
		,'16'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Somm�s elj�r�sban hozott d�nt�s' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '16'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '70B9F49F-6E22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '08AA6FC6-6E22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - 8napon bel�l lez�rt nem somm�s' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'08AA6FC6-6E22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'F5AFD3BE-6E22-E911-80C9-00155D020B39'
		,'17'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - 8napon bel�l lez�rt nem somm�s' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '17'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '08AA6FC6-6E22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = 'FAE6B0E5-6E22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - F�gg� hat�ly� hat�rozat' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'FAE6B0E5-6E22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'75FB85DD-6E22-E911-80C9-00155D020B39'
		,'18'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - F�gg� hat�ly� hat�rozat' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '18'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = 'FAE6B0E5-6E22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = 'C0727412-6F22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Hat�rozat hat�lyba l�p�se' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'C0727412-6F22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'63DA410A-6F22-E911-80C9-00155D020B39'
		,'19'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Hat�rozat hat�lyba l�p�se' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '19'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = 'C0727412-6F22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '40F9D832-6F22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - F�gg� hat�ly� v�gz�s' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'40F9D832-6F22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'F642A92A-6F22-E911-80C9-00155D020B39'
		,'20'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - F�gg� hat�ly� v�gz�s' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '20'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '40F9D832-6F22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '79627D8A-6F22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - V�gz�s hat�lyba l�p�se' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'79627D8A-6F22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'84893B81-6F22-E911-80C9-00155D020B39'
		,'21'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - V�gz�s hat�lyba l�p�se' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '21'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '79627D8A-6F22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '963DA2B4-6F22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Hat�s�g �ltal visszafizetett �sszeg' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'963DA2B4-6F22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'1B4210AB-6F22-E911-80C9-00155D020B39'
		,'22'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Hat�s�g �ltal visszafizetett �sszeg' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '22'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '963DA2B4-6F22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '841F77DB-6F22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Hat�s�got terhel� elj�r�si k�lts�g' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'841F77DB-6F22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'887E0BD3-6F22-E911-80C9-00155D020B39'
		,'23'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Hat�s�got terhel� elj�r�si k�lts�g' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '23'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '841F77DB-6F22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '678DCFFF-6F22-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Felf�ggesztett hat�rozat' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'678DCFFF-6F22-E911-80C9-00155D020B39'
		,'4A05253E-6922-E911-80C9-00155D020B39'
		,'D95E10F6-6F22-E911-80C9-00155D020B39'
		,'24'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Onkormanyzati_hatosagi_ugy - Felf�ggesztett hat�rozat' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '24'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '678DCFFF-6F22-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '7BB49979-C739-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - D�nt�st hozta �llamigazgat�si' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'7BB49979-C739-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'7EF8FE72-C739-E911-80CC-00155D020D03'
		,'3'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - D�nt�st hozta �llamigazgat�si' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '3'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '7BB49979-C739-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = 'EAA2C13A-7122-E911-80C9-00155D020B39')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - D�nt�s form�ja �llamigazgat�si' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'EAA2C13A-7122-E911-80C9-00155D020B39'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'69F8042F-7122-E911-80C9-00155D020B39'
		,'4'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - D�nt�s form�ja �llamigazgat�si' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '4'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = 'EAA2C13A-7122-E911-80C9-00155D020B39'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = 'BF9DEA97-C739-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - �gyint�z�s id�tartama' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'BF9DEA97-C739-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'CFBDB7DA-6922-E911-80C9-00155D020B39'
		,'5'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - �gyint�z�s id�tartama' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '5'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = 'BF9DEA97-C739-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '7A75BD5B-C839-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Hat�rid� t�ll�p�s napokban' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'7A75BD5B-C839-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'89FC37FE-6922-E911-80C9-00155D020B39'
		,'6'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Hat�rid� t�ll�p�s napokban' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '6'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '7A75BD5B-C839-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = 'B95D3F78-C839-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Jogorvoslati elj�r�s t�pusa' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'B95D3F78-C839-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'6C4EB8FD-6A22-E911-80C9-00155D020B39'
		,'7'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Jogorvoslati elj�r�s t�pusa' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '7'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = 'B95D3F78-C839-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '6B0AA08F-C839-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�s_t�pusa' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'6B0AA08F-C839-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'38D88F24-6B22-E911-80C9-00155D020B39'
		,'8'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�s_t�pusa' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '8'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '6B0AA08F-C839-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = 'D0B84BB0-C839-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�st_hozta' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'D0B84BB0-C839-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'365911A6-6B22-E911-80C9-00155D020B39'
		,'9'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�st_hozta' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '9'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = 'D0B84BB0-C839-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = 'C7C1E97E-CA39-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�s_tartalma' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'C7C1E97E-CA39-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'D034DBD1-6B22-E911-80C9-00155D020B39'
		,'10'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�s_tartalma' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '10'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = 'C7C1E97E-CA39-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '56F22629-CB39-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�s_v�ltoz�sa' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'56F22629-CB39-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'7E22D83E-6C22-E911-80C9-00155D020B39'
		,'11'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Jogorv_elj�r�sban_d�nt�s_v�ltoz�sa' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '11'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '56F22629-CB39-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '4F9A1088-CB39-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Hat�s�gi_ellen�rz�s_t�rt�nt' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'4F9A1088-CB39-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'093372A9-6D22-E911-80C9-00155D020B39'
		,'12'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Hat�s�gi_ellen�rz�s_t�rt�nt' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '12'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '4F9A1088-CB39-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '0D276235-CC39-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - D�nt�s munka�r�k sz�ma' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'0D276235-CC39-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'127CB717-6E22-E911-80C9-00155D020B39'
		,'13'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - D�nt�s munka�r�k sz�ma' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '13'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '0D276235-CC39-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '5286894B-CC39-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Meg�llap�tott elj�r�si k�lts�g' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'5286894B-CC39-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'C7D8A03F-6E22-E911-80C9-00155D020B39'
		,'14'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Meg�llap�tott elj�r�si k�lts�g' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '14'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '5286894B-CC39-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '8C941882-CD39-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Kiszabott k�zigazgat�si b�rs�g' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'8C941882-CD39-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'3FBA6E62-6E22-E911-80C9-00155D020B39'
		,'15'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Kiszabott k�zigazgat�si b�rs�g' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '15'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '8C941882-CD39-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = 'E46CF2AC-CD39-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Somm�s elj�r�sban hozott d�nt�s' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'E46CF2AC-CD39-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'04A5A397-6E22-E911-80C9-00155D020B39'
		,'16'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Somm�s elj�r�sban hozott d�nt�s' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '16'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = 'E46CF2AC-CD39-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '3FE58FBE-CD39-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - 8napon bel�l lez�rt nem somm�s' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'3FE58FBE-CD39-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'F5AFD3BE-6E22-E911-80C9-00155D020B39'
		,'17'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - 8napon bel�l lez�rt nem somm�s' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '17'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '3FE58FBE-CD39-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '8A2C94CD-CD39-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - F�gg� hat�ly� hat�rozat' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'8A2C94CD-CD39-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'75FB85DD-6E22-E911-80C9-00155D020B39'
		,'18'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - F�gg� hat�ly� hat�rozat' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '18'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '8A2C94CD-CD39-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = 'A273ADD9-CD39-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Hat�rozat hat�lyba l�p�se' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'A273ADD9-CD39-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'63DA410A-6F22-E911-80C9-00155D020B39'
		,'19'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Hat�rozat hat�lyba l�p�se' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '19'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = 'A273ADD9-CD39-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '48006AF1-CD39-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - F�gg� hat�ly� v�gz�s' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'48006AF1-CD39-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'F642A92A-6F22-E911-80C9-00155D020B39'
		,'20'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - F�gg� hat�ly� v�gz�s' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '20'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '48006AF1-CD39-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '934D2EFB-CD39-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - V�gz�s hat�lyba l�p�se' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'934D2EFB-CD39-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'84893B81-6F22-E911-80C9-00155D020B39'
		,'21'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - V�gz�s hat�lyba l�p�se' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '21'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '934D2EFB-CD39-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = 'F0002519-CE39-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Hat�s�g �ltal visszafizetett �sszeg' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'F0002519-CE39-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'1B4210AB-6F22-E911-80C9-00155D020B39'
		,'22'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Hat�s�g �ltal visszafizetett �sszeg' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '22'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = 'F0002519-CE39-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '457F4437-CE39-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Hat�s�got terhel� elj�r�si k�lts�g' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'457F4437-CE39-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'887E0BD3-6F22-E911-80C9-00155D020B39'
		,'23'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Hat�s�got terhel� elj�r�si k�lts�g' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '23'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '457F4437-CE39-E911-80CC-00155D020D03'
	END
GO
IF NOT EXISTS (SELECT 1 from EREC_Obj_MetaAdatai WHERE Id = '84A4FF48-CE39-E911-80CC-00155D020D03')
	BEGIN
		PRINT 'INSERT EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Felf�ggesztett hat�rozat' 	
		insert into EREC_Obj_MetaAdatai(
		Id
		,Obj_MetaDefinicio_Id
		,Targyszavak_Id
		,Sorszam
		,Opcionalis
		,Ismetlodo
		,SPSSzinkronizalt
		) values (
		'84A4FF48-CE39-E911-80CC-00155D020D03'
		,'B7DE48C2-7022-E911-80C9-00155D020B39'
		,'D95E10F6-6F22-E911-80C9-00155D020B39'
		,'24'
		,'1'
		,'0'	
		,'0'
		)
	END
	ELSE
	BEGIN
	    PRINT 'UPDATE EREC_Obj_MetaAdatai - Allamigazgatasi_hatosagi_ugy - Felf�ggesztett hat�rozat' 	 
		update EREC_Obj_MetaAdatai
			set Sorszam = '24'
				,Opcionalis = '1'
				,ControlTypeSource = NULL
				,Ismetlodo = 0
		where Id = '84A4FF48-CE39-E911-80CC-00155D020D03'
	END
GO