print 'InvalidateSztornozottObjKezbesitesiTetelek.sql kezdete'

begin tran

declare @org uniqueidentifier
set @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @execTime datetime
set @execTime = getdate()
declare @ExecutorUserId uniqueidentifier
set @ExecutorUserId = (select Id from KRT_Felhasznalok where Nev='Adminisztrátor'
				and Org=@org
				and getdate() between ErvKezd and ErvVege)


if @ExecutorUserId is null
	raiserror('Az Adminisztrator felhasznalo azonositoja nem talalhato!', 16, 1)
	
-- A módosított tételek azonosításához
declare @Tranz_Id uniqueidentifier
set @Tranz_Id = NEWID()

PRINT 'Végrehajtó felhasználó (Adminisztrátor) azonosítója: ' + convert(varchar(36), @ExecutorUserId)
PRINT 'Végrehajtási idő: ' + convert(varchar(20), @execTime, 120)
PRINT 'Tranz_Id: ' + convert(varchar(36), @Tranz_Id)

update EREC_IraKezbesitesiTetelek
	set ErvVege = @execTime
	    ,Modosito_id = @ExecutorUserId
		,ModositasIdo = @execTime
		,Ver = kezb.Ver + 1
		,Tranz_id = @Tranz_Id
FROM EREC_IraKezbesitesiTetelek kezb
left join EREC_UgyUgyiratok ugy on kezb.Obj_Id = ugy.Id
left join EREC_PldIratPeldanyok pld on kezb.Obj_Id = pld.Id
left join EREC_KuldKuldemenyek kuld on kezb.Obj_Id = kuld.Id
where kezb.Allapot = '4'
and @execTime between kezb.ErvKezd and kezb.ErvVege
and
(
	ugy.Allapot = '90'
	or pld.Allapot = '90'
	or kuld.Allapot = '90'
)

-- history 
declare @row_ids varchar(MAX)
set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from EREC_IraKezbesitesiTetelek where Tranz_id = @Tranz_Id FOR XML PATH('')),1, 2, '')
if @row_ids is not null
	set @row_ids = @row_ids + '''';

exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_IraKezbesitesiTetelek'
		,@Row_Ids = @row_ids
		,@Muvelet = 2
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @execTime

commit

print 'InvalidateSztornozottObjKezbesitesiTetelek.sql vege'