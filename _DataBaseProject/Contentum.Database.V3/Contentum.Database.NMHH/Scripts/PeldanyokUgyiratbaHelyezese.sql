print 'PeldanyokUgyiratbaHelyezese kezdete'

begin tran

declare @org uniqueidentifier
set @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @execTime datetime
set @execTime = getdate()
declare @ExecutorUserId uniqueidentifier
set @ExecutorUserId = (select Id from KRT_Felhasznalok where Nev='Adminisztrátor'
				and Org=@org
				and getdate() between ErvKezd and ErvVege)


if @ExecutorUserId is null
	raiserror('Az Adminisztrator felhasznalo azonositoja nem talalhato!', 16, 1)
	
-- A módosított tételek azonosításához
declare @Tranz_Id uniqueidentifier
set @Tranz_Id = NEWID()

PRINT 'Végrehajtó felhasználó (Adminisztrátor) azonosítója: ' + convert(varchar(36), @ExecutorUserId)
PRINT 'Végrehajtási idő: ' + convert(varchar(20), @execTime, 120)
PRINT 'Tranz_Id: ' + convert(varchar(36), @Tranz_Id)

declare @plds table(Id uniqueidentifier)

insert into @plds
select p.Id from EREC_PldIratPeldanyok p
join EREC_IraIratok i on p.IraIrat_Id = i.Id
join EREC_UgyUgyiratok u on i.Ugyirat_Id = u.Id
where p.Sorszam > 1
and  p.FelhasznaloCsoport_Id_Orzo != u.FelhasznaloCsoport_Id_Orzo
and p.FelhasznaloCsoport_Id_Orzo not in (select id from KRT_Felhasznalok)


--ügyiratba helyezés
 UPDATE [EREC_PldIratPeldanyok]
	SET [EREC_PldIratPeldanyok].[FelhasznaloCsoport_Id_Orzo] = [EREC_UgyUgyiratok].[FelhasznaloCsoport_Id_Orzo]
		,[EREC_PldIratPeldanyok].[Csoport_Id_Felelos] = [EREC_UgyUgyiratok].[Csoport_Id_Felelos]
		,Modosito_id = @ExecutorUserId
		,ModositasIdo = @execTime
		,Ver = [EREC_PldIratPeldanyok].Ver + 1
		,Tranz_id = @Tranz_Id
	FROM [EREC_UgyUgyiratok] INNER JOIN [EREC_IraIratok]
	ON [EREC_UgyUgyiratok].[Id] = [EREC_IraIratok].[Ugyirat_Id]
	WHERE [EREC_PldIratPeldanyok].[Id] IN (SELECT Id FROM @plds)
	AND [EREC_IraIratok].[Id] = [EREC_PldIratPeldanyok].[IraIrat_Id]


-- history 
declare @row_ids varchar(MAX)
set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @plds FOR XML PATH('')),1, 2, '')
if @row_ids is not null
	set @row_ids = @row_ids + '''';

exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_PldIratPeldanyok'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @execTime


COMMIT

print 'PeldanyokUgyiratbaHelyezese vege'