@echo off

set sqlcmd="sqlcmd.exe"

if "%1"=="" goto :help
if "%2"=="" goto :help

@echo on
%sqlcmd% -S %1 -v DataBaseName="%2" eRecordWebServiceUrl="%3/eRecordWebService/EREC_PldIratPeldanyokService.asmx" -i Run_IratPldMeghiusIgUjraPostaz.sql

@echo off

goto :EOF

:help
echo. Hasznalat:
echo.  1. parameter: szerver nev
echo.  2. parameter: adatbazis neve
echo.
echo. Pelda:
echo. build.bat INTCSQL2T EDOK

:EOF