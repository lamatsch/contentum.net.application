﻿PRINT 'START IratPeldanyMeghiusulasiIgazolassalUjraPostazas' 
PRINT ''

USE $(DataBaseName) 

IF NOT EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_HTTPRequest]') AND type in (N'P', N'PC') )
  BEGIN
    print 'sp_HTTPRequest tárolt eljárás hiányzik az adatbázisból - az update NEM futtatható'                               /* Telepíteni kell */
    SET NOEXEC ON
  END
GO

DECLARE @REQUESTTEXT VARCHAR(8000)
DECLARE @RESPONSETEXT VARCHAR(8000)

SET @REQUESTTEXT=
	'''<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
					xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
					xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	  <soap:Body>
		<IratPeldanyMeghiusulasiIgazolassalUjraPostazas xmlns="Contentum.eRecord.WebService">
		  <felhasznaloId>54E861A5-36ED-44CA-BAA7-C287D125B309</felhasznaloId>
		  <felhasznaloSzervezetId>BD00C8D0-CF99-4DFC-8792-33220B7BFCC6</felhasznaloSzervezetId>
		</IratPeldanyMeghiusulasiIgazolassalUjraPostazas>
	  </soap:Body>
	</soap:Envelope>'''

exec SP_HTTPREQUEST 
		$(eRecordWebServiceUrl),
		'POST', 
		@REQUESTTEXT,
		'Contentum.eRecord.WebService/IratPeldanyMeghiusulasiIgazolassalUjraPostazas',
		'', 
		'', 
		@RESPONSETEXT out

PRINT 'STOP IratPeldanyMeghiusulasiIgazolassalUjraPostazas'
PRINT ''

GO