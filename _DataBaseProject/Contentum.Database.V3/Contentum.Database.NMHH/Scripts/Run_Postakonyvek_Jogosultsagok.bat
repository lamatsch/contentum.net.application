@echo off
echo %1
echo %2
echo %3

set sqlcmd="sqlcmd.exe"

@echo on
%sqlcmd% -S %1 -v DataBaseName="%2" RightsService="%3/RightsService.asmx" -i BUG_10136_Postakonyvek_Jogosultsagok_varosok.sql
%sqlcmd% -S %1 -v DataBaseName="%2" RightsService="%3/RightsService.asmx" -i BUG_10136_Postakonyvek_Jogosultsagok_elekronikus_odap.sql

@echo off

goto :EOF

:help
echo. Hasznalat:
echo.  1. parameter: szerver nev
echo.  2. parameter: adatbazis neve
echo.  3. parameter: eAdminWebServiceUrl

:EOF
