@echo off
echo %1
echo %2
echo %3

set sqlcmd="sqlcmd.exe"

@echo on
%sqlcmd% -S %1 -v DataBaseName="%2" PartnerekServiceUrl="%3/KRT_PARTNEREKService.asmx" FelhasznalokServiceUrl="%3/KRT_FelhasznalokService.asmx" PartnerKapcsolatokServiceUrl="%3/KRT_PartnerKapcsolatokService.asmx"  -i BUG_10319_TechikaiPostazokPartnerCreate.sql

@echo off

goto :EOF

:help
echo. Hasznalat:
echo.  1. parameter: szerver nev
echo.  2. parameter: adatbazis neve
echo.  3. parameter: eAdminWebServiceUrl	

:EOF
