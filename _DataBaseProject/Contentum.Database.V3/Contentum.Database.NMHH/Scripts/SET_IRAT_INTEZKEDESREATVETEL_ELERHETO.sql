declare @record_id uniqueidentifier
--IRAT_INTEZKEDESREATVETEL_ELERHETO
set @record_id = '589DCBD2-C50F-4887-A777-D7F50C273356'

DECLARE @isTUK char(1) 
SET @isTUK = (select TOP 1 ertek from KRT_Parameterek
where nev='TUK' and org='450B510A-7CAA-46B0-83E3-18445C0C53A9' and getdate() between ervkezd and ervvege)

IF @isTUK = '1'
BEGIN
	PRINT 'SET IRAT_INTEZKEDESREATVETEL_ELERHETO = 0'
	UPDATE KRT_Parameterek
		SET Ertek = '0'
	WHERE Id = @record_id
END