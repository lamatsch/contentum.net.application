--SZERELT_UGYIRAT_MEGORZESI_IDO_SZAMITASA
DECLARE @record_id uniqueidentifier
DECLARE @org_kod nvarchar(100) 
declare @ertek nvarchar(400)

SET @org_kod = (SELECT KOD FROM KRT_Orgok
			    WHERE id='450B510A-7CAA-46B0-83E3-18445C0C53A9') 

SET @record_id = '7D278A2C-BB6A-4AEC-A5CE-CE2F6700B621'

IF @org_kod = 'FPH'
	SET @ertek = '1'
ELSE IF @org_kod = 'CSPH'
	SET @ertek = '1'
ELSE IF @org_kod = 'BOPMH'
	SET @ertek = '2'
ELSE IF @org_kod = 'NMHH'
	SET @ertek = '2'

IF NOT EXISTS (select 1 from KRT_Parameterek where Id = @record_id) 
BEGIN 
	Print 'INSERT - KRT_Parameterek - SZERELT_UGYIRAT_MEGORZESI_IDO_SZAMITASA  - ' + @ertek 
	 insert into KRT_Parameterek(
		Id
		,Org
		,Nev
		,Ertek
		,Karbantarthato
		,Note
		) values (
		@record_id
		,'450b510a-7caa-46b0-83e3-18445c0c53a9'
		,'SZERELT_UGYIRAT_MEGORZESI_IDO_SZAMITASA'
		,@ertek
		,'1'
		,' - ha a SZERELT_UGYIRAT_MEGORZESI_IDO_SZAMITASA=1, akkor a szerelt lánc legutolsó elemének lezárásának dátumához hozzá kell adni az elem megőrzési idejét
           - ha a SZERELT_UGYIRAT_MEGORZESI_IDO_SZAMITASA=2, akkor a szerelt lánc minden elemén, a lezárásának dátumához hozzá kell adni az elem megőrzési idejét, és ezek maximumát venni
		   '
		); 
 END
 GO