GO
PRINT 'SAKKORA_RENDSZERPARAMETER_UPDATE' 

DECLARE @org nvarchar(100) 
SET @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
			
DECLARE @org_kod nvarchar(100) 
SET @org_kod = (SELECT KOD FROM KRT_ORGOK WHERE id=@org) 

IF EXISTS (SELECT 1 FROM KRT_Parameterek WHERE Nev = 'UGYINTEZESI_IDO_FELFUGGESZTES')
BEGIN
	IF @org_kod = 'NMHH'
		BEGIN	
			UPDATE KRT_Parameterek
			SET Ertek = 'FANCY'
			WHERE Nev = 'UGYINTEZESI_IDO_FELFUGGESZTES'
		END
	ELSE
		BEGIN
			UPDATE KRT_Parameterek
			SET Ertek = 'SIMPLE'
			WHERE Nev = 'UGYINTEZESI_IDO_FELFUGGESZTES'
		END
END
GO


UPDATE KRT_KodTarak
SET Egyeb='{ 
   "tipusok":[ 
      "NONE"
   ],
   "elozmenytipus":[ 
      "NONE"
   ],
   "atmeneteklista":[ 
      { 
         "ugyfajta":"NH",
         "atmenetek":[ 
            "0_NONE",
            "1",
            "8",
            "9"
         ]
      },
      { 
         "ugyfajta":"H1",
         "atmenetek":[ 
            "0_NONE",
            "1",
            "8",
            "9"
         ]
      },
      { 
         "ugyfajta":"H2",
         "atmenetek":[ 
            "0_NONE",
            "5",
            "10",
            "11"
         ]
      }
   ],
   "felfuggesztesOkai":[ 

   ],
   "lezarasOkai":[ 

   ]
}'
WHERE ID = '9F7000CB-4730-4477-937D-6330F75F5200'
GO