declare @azonosito nvarchar(20)

declare @iktatokonyvId uniqueidentifier
set @iktatokonyvId = 'A80F278F-90CB-E811-80CF-00155D020DD3'

IF EXISTS (SELECT 1 from EREC_IraIktatoKonyvek WHERE Id = @iktatokonyvId)
BEGIN
	SELECT @azonosito = azonosito from EREC_IraIktatokonyvek where id=@iktatokonyvId
	
	IF @azonosito is null
	BEGIN
		print 'Set ELEKTRONIKUS_POSTAKONYV Azonosito'

		declare @azonositoHossz  nvarchar(20)
		SET @azonositohossz =(SELECT TOP 1 ertek from KRT_Parameterek WHERE Nev='IKTATOKONYV_AZONOSITO_HOSSZ')
		set @azonosito = (select top 1 Azonosito from EREC_IraIktatoKonyvek where IktatoErkezteto = 'P' order by cast(Azonosito as int) desc)
		set @azonosito = right(replicate('0',@azonositohossz) + cast(cast(@azonosito as int) + 1 as nvarchar), @azonositohossz)

		Update EREC_IraIktatoKonyvek
		set Azonosito = @azonosito
		where Id = @iktatokonyvId

		update KRT_Parameterek
		set Ertek = @azonosito
		where Id = 'E202E3EE-A8C8-E811-80CE-00155D020DD3'
	END	
END


