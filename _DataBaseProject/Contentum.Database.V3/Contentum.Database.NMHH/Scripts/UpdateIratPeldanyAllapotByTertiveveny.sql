print 'UpdateIratPeldanyAllapotByTertiveveny kezdete'

begin tran

declare @org uniqueidentifier
set @org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'

declare @execTime datetime
set @execTime = getdate()

declare @ExecutorUserId uniqueidentifier
set @ExecutorUserId = (select Id from KRT_Felhasznalok where Nev='Adminisztr�tor' and Org=@org and getdate() between ErvKezd and ErvVege)

-- A m�dos�tott t�telek azonos�t�s�hoz
declare @Tranz_Id uniqueidentifier
set @Tranz_Id = NEWID()


declare @plds table(Id uniqueidentifier)

insert into @plds
select peld.id from 
EREC_KuldTertivevenyek terti 
join EREC_Kuldemeny_IratPeldanyai kuldPeld on terti.Kuldemeny_Id = kuldPeld.KuldKuldemeny_Id
join EREC_PldIratPeldanyok peld on kuldPeld.Peldany_Id = peld.Id
where terti.TertivisszaKod is not null
and TertivisszaKod in ('1','2','20','21','22')
and peld.Allapot = '41' -- c�mzett nem vett �t


 UPDATE EREC_PldIratPeldanyok
	SET Allapot = '40' -- c�mzett �tvette
		,Modosito_id = @ExecutorUserId
		,ModositasIdo = @execTime
		,Ver = [EREC_PldIratPeldanyok].Ver + 1
		,Tranz_id = @Tranz_Id
	WHERE Id IN (SELECT Id FROM @plds)


-- history 
declare @row_ids varchar(MAX)
set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @plds FOR XML PATH('')),1, 2, '')
if @row_ids is not null
	set @row_ids = @row_ids + '''';

exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_PldIratPeldanyok'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @execTime


COMMIT

print 'UpdateIratPeldanyAllapotByTertiveveny vege'