/*********
BLG#3541: Vegyes t�pus� irat kezel�se (Elektronikus/Pap�r/Vegyes)

-- EREC_IraIratok.AdathordozoTipusa friss�t�se az iratp�ld�nyok UgyintezesModja mez� alapj�n
-- EREC_UgyUgyiratok.Jelleg mez� friss�t�se az iratok AdathordozoTipusa mez�j�nek alapj�n (illetve a szerelt �gyiratok Jelleg mez�je alapj�n)
*********/


begin tran

print 'EREC_IraIratok.AdathordozoTipusa friss�t�se az iratp�ld�nyok UgyintezesModja mez� alapj�n (Elektronikus/Pap�r/Vegyes)...'

declare @iratModositasok table(Id uniqueidentifier, AdathordozoTipusa_Irat_JoErtek nvarchar(64))

insert into @iratModositasok
select 
	t3.IraIrat_Id
	--t3.*
	---- A helyes AdathordozoTipusa �rt�k az iraton:
	, case when t3.pldUgyintezesMod_Db > 1 then 'V' -- Vegyes
		else t3.UgyintezesModja_Pld end as AdathordozoTipusa_Irat_JoErtek
from
(select t2.IraIrat_Id
	, t2.pldUgyintezesMod_Db
	, i.AdathordozoTipusa as AdathordozoTipusa_Irat
	-- Az els� iratp�ld�ny �gyint�z�sm�d �rt�ke (ha homog�n volt, akkor minden iratp�ld�nyn�l ugyanez az �rt�k volt, 
	-- ha nem homog�n, akkor pedig �gyis 'Vegyes'-nek kell lennie az irat adathordoz� t�pus �rt�k�nek
	, (select top 1 pld.UgyintezesModja
	   from EREC_PldIratPeldanyok pld where pld.IraIrat_Id = i.Id and pld.Allapot != '90') as UgyintezesModja_Pld
from
	-- Egy iratban h�nyf�le �gyint�z�sm�d tal�lhat� meg: (ha 1, akkor homog�n, ha > 1, akkor mindenk�ppen vegyes)
	(select IraIrat_Id
		, count(*) pldUgyintezesMod_Db
	 from
		(select IraIrat_Id, pld.ugyintezesmodja, count(*) as db
		from EREC_PldIratPeldanyok pld
		join erec_irairatok irat on pld.irairat_id = irat.id
		join erec_UgyUgyiratok ugy on irat.ugyirat_id = ugy.id
		where pld.ugyintezesmodja is not null
			-- Nem sztorn�zott:
			and pld.Allapot != '90'
			-- Az �gyirat m�g nincs iratt�rban:
			and ugy.IrattarbaVetelDat is null
		group by IraIrat_Id, pld.ugyintezesmodja) as t1
	group by IraIrat_Id) as t2
	join erec_irairatok i on t2.IraIrat_Id = i.Id) as t3
-- Ott hib�s az irat Adathordoz� t�pusa, amelyik homog�n, de az irat adathordoz� t�pusa nem egyenl� a p�ld�ny(ok) ugyint�z�sm�dj�val,
--- illetve amelyik nem homog�n, de az irat adathordoz� t�pusa nem vegyes
where ((t3.pldUgyintezesMod_Db = 1 and t3.AdathordozoTipusa_Irat != t3.UgyintezesModja_Pld)
		or (t3.pldUgyintezesMod_Db > 1 and t3.AdathordozoTipusa_Irat != 'V'))
	-- Arra is sz�r�nk, hogy ha homog�n, akkor az UgyintezesModja_Pld �rt�k egy val�s k�dt�r�rt�k legyen az 'UGYINTEZES_ALAPJA' k�dcsoporton bel�l,
	-- hogy rossz �rt�ket ne �ll�tsunk be az irat AdathordozoTipusa mez�be:	
	and (t3.pldUgyintezesMod_Db > 1 or t3.UgyintezesModja_Pld in 					
					(select distinct kt.Kod
					from KRT_KodCsoportok kcs
						join krt_kodtarak kt on kt.kodcsoport_id = kcs.id
					where kcs.kod = 'UGYINTEZES_ALAPJA'
						and getdate() between kt.ervkezd and kt.ervvege)
						)
											
update erec_irairatok
set AdathordozoTipusa = im.AdathordozoTipusa_Irat_JoErtek
from @iratModositasok im
	join erec_irairatok i on im.Id = i.id
	

	

print 'EREC_UgyUgyiratok.Jelleg mez� friss�t�se az iratok AdathordozoTipusa mez�j�nek alapj�n (illetve a szerelt �gyiratok Jelleg mez�je alapj�n)...'


declare @ugyiratModositasok table(Id uniqueidentifier, Jelleg_JoErtek nvarchar(64))


insert into @ugyiratModositasok
select u.Id, t.UgyiratJelleg_Helyes
--select u.azonosito, u.Jelleg, t.*, u.RegirendszerIktatoszam, u.*
from
(select tJellegekIratAlapjan.*
	, tJellegekSzereltAlapjan.UgyiratJelleg_SzereltekAlapjan
	-- Az �gyirat helyes �llapota az iratok �s a szereltek alapj�n:
	, case when isnull(tJellegekSzereltAlapjan.UgyiratJelleg_SzereltekAlapjan, tJellegekIratAlapjan.UgyiratJelleg_IratokAlapjan) 
							!= tJellegekIratAlapjan.UgyiratJelleg_IratokAlapjan then '03' -- Vegyes
		   -- Ha van r�girendszer iktat�sz�m, akkor felt�telezz�k, hogy van egy pap�r alap� is, teh�t ha az iratok alapj�n elektronikus lenne, akkor Vegyes lesz az �gyirat jelleg
		   when u.RegirendszerIktatoszam is not null and tJellegekIratAlapjan.UgyiratJelleg_IratokAlapjan = '01' then '03'
		   else tJellegekIratAlapjan.UgyiratJelleg_IratokAlapjan end as UgyiratJelleg_Helyes
from
	(select t2.Ugyirat_Id
		--, t2.iratAdathordozoTipusa_Db
		-- Az els� irat adathordoz�T�pusa (ha homog�n volt, akkor minden iratn�l ugyanez az �rt�k volt, 
		-- ha nem homog�n, akkor pedig �gyis 'Vegyes' lesz az �gyirat jelleg
		, case when t2.iratAdathordozoTipusa_Db > 1 then '03' -- Vegyes
			else (select top 1							
				  case i.AdathordozoTipusa when '1' then '01' -- Elektronikus
									when 'V' then '03' -- Vegyes
									else '02'		   -- Pap�r
									end
				   from erec_irairatok i
				   where Ugyirat_Id = t2.Ugyirat_Id and i.Allapot not in ('06', '08'))
			end UgyiratJelleg_IratokAlapjan
	from
		-- Egy �gyiratban h�nyf�le adathordoz�t�pus-fajta tal�lhat� meg: (ha 1, akkor homog�n, ha > 1, akkor mindenk�ppen vegyes)
		(select Ugyirat_Id
			, count(*) iratAdathordozoTipusa_Db
		 from
			(select irat.Ugyirat_Id, irat.AdathordozoTipusa, count(*) db
			from erec_irairatok irat
			join erec_UgyUgyiratok ugy on irat.ugyirat_id = ugy.id
			where irat.Allapot not in ('06', '08')		-- nem �tiktatott �s nem Felszabad�tott		
				  and ugy.IrattarbaVetelDat is null		-- Az �gyirat m�g nincs iratt�rban
			group by irat.Ugyirat_Id, irat.AdathordozoTipusa) as t1
		group by Ugyirat_Id) as t2) as  tJellegekIratAlapjan
	join EREC_UgyUgyiratok u on tJellegekIratAlapjan.Ugyirat_Id = u.Id
	left join	
	-- �gyiratok t�pusa szereltekkel �sszevetve:
	(select t2.Ugyirat_Id
		, -- Az els� szerelt �gyirat Jelleg mez�je (ha homog�n, akkor �rdekes, ha nem homog�n, akkor 'Vegyes' lesz)
		case when t2.UgyJellegSzereltekbol_Db > 1 then '03' -- Vegyes
			else (select top 1 usz.Jelleg 
				  from EREC_UgyUgyiratok usz where usz.UgyUgyirat_Id_Szulo = t2.Ugyirat_Id
						and usz.Allapot = '60') end as UgyiratJelleg_SzereltekAlapjan
	from
		(select t1.UgyUgyirat_Id_Szulo as Ugyirat_Id, count(*) as UgyJellegSzereltekbol_Db
		from
			(select ugySzerelt.UgyUgyirat_Id_Szulo, ugySzerelt.Jelleg
			from erec_ugyugyiratok ugySzerelt
				join erec_ugyugyiratok ugySzulo on ugySzulo.Id = ugySzerelt.UgyUgyirat_Id_Szulo
			where ugySzerelt.Allapot = '60' -- Szerelt
				  and ugySzulo.IrattarbaVetelDat is null		-- Az �gyirat m�g nincs iratt�rban
			group by ugySzerelt.UgyUgyirat_Id_Szulo, ugySzerelt.Jelleg) as t1
		group by t1.UgyUgyirat_Id_Szulo) as t2) as tJellegekSzereltAlapjan on tJellegekIratAlapjan.Ugyirat_Id = tJellegekSzereltAlapjan.Ugyirat_Id
) as t
join erec_ugyugyiratok u on t.Ugyirat_Id = u.Id
WHERE u.Jelleg != t.UgyiratJelleg_Helyes


--select *
--from @ugyiratModositasok

									
update erec_ugyugyiratok
set Jelleg = um.Jelleg_JoErtek
from @ugyiratModositasok um
	join erec_ugyugyiratok u on um.Id = u.id


commit tran