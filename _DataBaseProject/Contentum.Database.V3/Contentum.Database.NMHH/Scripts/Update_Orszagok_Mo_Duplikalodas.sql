print 'Tévesen felvitt Magyarország rekord érvénytelenítése'

DECLARE @orszag_id uniqueidentifier SET @orszag_id = (select TOP 1 Id 
	from KRT_Orszagok where Nev = 'Magyarország' and upper(Kod) = '1' and ErvKezd < GETDATE() and ErvVege > GETDATE()) 

if @orszag_id IS NOT NULL 
BEGIN
	print 'Update KRT_orszagok'
	update KRT_Orszagok set ervVege = GETDATE() where Id = @orszag_id
END
ELSE
BEGIN
	print 'Ország ID meghatározása nem sikerült!'
END