print 'Hiányzó ország Id -k feltöltése a KRT_Telepulesek táblában Magyarország azonosítójával'

DECLARE @orszag_id uniqueidentifier SET @orszag_id = (select TOP 1 Id 
	from KRT_Orszagok where Nev = 'Magyarország' and upper(Kod) = 'HU' and ErvKezd < GETDATE() and ErvVege > GETDATE()) 

if @orszag_id IS NOT NULL 
BEGIN
	print 'Update KRT_Telepulesek'
	UPDATE KRT_Telepulesek set Orszag_Id = @orszag_id where Orszag_Id IS NULL
END
ELSE
BEGIN
	print 'Ország ID meghatározása nem sikerült!'
END