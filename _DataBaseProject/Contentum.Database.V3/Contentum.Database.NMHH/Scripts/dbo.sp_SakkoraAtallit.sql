USE [CONTENTUM_NMHH_R2T]
GO
/****** Object:  StoredProcedure [dbo].[sp_SakkoraTaroltEljaras]    Script Date: 10/9/2019 5:07:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Arató Csaba
-- Create date: 2019.10.09
-- Description:	Tárolt eljárás sakkóra teszteléséhez
-- =============================================
ALTER PROCEDURE [dbo].[sp_SakkoraTaroltEljaras]

	-- Add the parameters for the stored procedure here
	@Vonalkod VARCHAR(100),
	@NapKulonbseg int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	IF EXISTS (select * from [dbo].[EREC_UgyUgyiratok] where BARCODE = @Vonalkod )
	BEGIN

	DECLARE @UgyiratId UNIQUEIDENTIFIER;

	SET @UgyiratId = (
	Select Id From EREC_UgyUgyiratok
	WHERE BARCODE = @Vonalkod )

	UPDATE dbo.[EREC_UgyUgyiratok]
	SET UgyintezesKezdete = DATEADD(day, @NapKulonbseg, UgyintezesKezdete),
		SkontrobaDat = DATEADD(day, @NapKulonbseg, SkontrobaDat),
		IrattarbaVetelDat = DATEADD(day, @NapKulonbseg, IrattarbaVetelDat),
		SztornirozasDat = DATEADD(day, @NapKulonbseg, SztornirozasDat),
		ErvKezd = DATEADD(day, @NapKulonbseg, ErvKezd),
		LetrehozasIdo = DATEADD(day, @NapKulonbseg, LetrehozasIdo)
	WHERE Id = @UgyIratId

	UPDATE dbo.[EREC_UgyUgyiratdarabok]
      SET ErvKezd = DATEADD(day, @NapKulonbseg, ErvKezd ),
	  LetrehozasIdo = DATEADD(day, @NapKulonbseg, ErvKezd )
      WHERE UgyUgyirat_Id = @UgyIratId


    UPDATE dbo.[EREC_IraIratok]
       SET [IktatasDatuma] = DATEADD(day, @NapKulonbseg, [IktatasDatuma] ),
			ExpedialasDatuma = DATEADD(day, @NapKulonbseg, ExpedialasDatuma ),
			IrattarbaVetelDat = DATEADD(day, @NapKulonbseg, IrattarbaVetelDat ),
			UgyintezesKezdoDatuma = DATEADD(day, @NapKulonbseg, UgyintezesKezdoDatuma ),
			ErvKezd = DATEADD(day, @NapKulonbseg, ErvKezd ),
			LetrehozasIdo = DATEADD(day, @NapKulonbseg, LetrehozasIdo )
       WHERE Ugyirat_Id = @UgyIratId

	UPDATE dbo.[EREC_PldIratPeldanyok]
       SET [LetrehozasIdo] = DATEADD(day, @NapKulonbseg, [LetrehozasIdo] ),
			ErvKezd = DATEADD(day, @NapKulonbseg, ErvKezd )
       WHERE IraIrat_Id IN (SELECT ID FROM [EREC_IraIratok] WHERE Ugyirat_Id = @UgyIratId)
	END

	ELSE 
	BEGIN
		PRINT 'HIBA: Nincs ilyen vonalkoddal ugyirat!'
	END
END
