GO
----------------------------------------------------------------------------------------------------------------------------------------------------------
-- Frissiti az osszes view-t chema alapjan.
-----------------------------------------------------------------------------
SET NOCOUNT ON

DECLARE @CURRENTVIEW VARCHAR(255)

DECLARE AllView CURSOR FAST_FORWARD
FOR
SELECT
	DISTINCT s.name + '.' + o.name AS ViewName
	FROM sys.objects o JOIN sys.schemas s ON o.schema_id = s.schema_id 
	WHERE	o.[type] = 'V'
		AND OBJECTPROPERTY(o.[object_id], 'IsSchemaBound') <> 1
		AND OBJECTPROPERTY(o.[object_id], 'IsMsShipped') <> 1

OPEN AllView

FETCH NEXT FROM AllView 
INTO @CurrentView

WHILE @@FETCH_STATUS = 0
BEGIN

	--PRINT @CurrentView
	EXEC sp_refreshview @CurrentView
	
	FETCH NEXT FROM AllView
	INTO @CurrentView
	
END
CLOSE AllView
DEALLOCATE AllView
GO
-----------------------------------------------------------------------------