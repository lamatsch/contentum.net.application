/*
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
*/
CREATE FUNCTION [dbo].[FN_EREC_UgyUgyirat_EljarasiSzakaszFok] 
(
	@Id		UNIQUEIDENTIFIER ,
	@Org	UNIQUEIDENTIFIER 
)

RETURNS NVARCHAR(200)
AS

BEGIN
	DECLARE @RET				NVARCHAR(200)
	DECLARE @KODTAR_ESZ			NVARCHAR(64)
	DECLARE @KODTAR_SAKKORA		NVARCHAR(64)

	SELECT @KODTAR_SAKKORA = U.SakkoraAllapot 
	FROM EREC_Ugyugyiratok U
	WHERE U.Id = @Id

	IF @KODTAR_SAKKORA IS NULL
		RETURN NULL

	SELECT TOP 1 @KODTAR_ESZ = I.EljarasiSzakasz
	FROM EREC_IRAIRATOK I
	WHERE I.Ugyirat_Id = @Id
		AND I.IratHatasaUgyintezesre = @KODTAR_SAKKORA

	IF @KODTAR_ESZ IS NULL
		RETURN NULL

	RETURN  [dbo].fn_KodtarErtekNeve('ELJARASI_SZAKASZ_FOK', @KODTAR_ESZ, @Org)
	
END