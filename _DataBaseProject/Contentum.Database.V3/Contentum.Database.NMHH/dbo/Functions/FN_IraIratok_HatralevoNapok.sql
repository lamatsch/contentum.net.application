/*SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
*/
-- =============================================
-- Author:	AXIS RENDSZERHAZ KFT.
-- Create date: 2018.05
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[FN_IraIratok_HatralevoNapok]
(
	@StartDate					DATETIME,
	@EndDate					DATETIME
)
RETURNS INT WITH SCHEMABINDING
AS
BEGIN

	IF @StartDate IS NULL OR @EndDate IS NULL
		RETURN NULL
		
	RETURN DATEDIFF(DAY,@StartDate, @EndDate)

END