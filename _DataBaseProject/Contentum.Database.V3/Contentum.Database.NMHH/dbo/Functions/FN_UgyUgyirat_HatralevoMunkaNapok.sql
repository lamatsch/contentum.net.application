/*
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
*/
-- =============================================
-- Author:	AXIS RENDSZERHAZ KFT.
-- Create date: 2018.05
-- Description:	
-- =============================================
-- !!!! OBSOLETE !!!!
CREATE FUNCTION [dbo].[FN_UgyUgyirat_HatralevoMunkaNapok]
(
	@StartDate					DATETIME,
	@EndDate					DATETIME
)
RETURNS INT
AS
BEGIN

	IF @StartDate IS NULL OR @EndDate IS NULL
		RETURN NULL

	IF (@StartDate <= @EndDate)
		BEGIN
			RETURN  dbo.fn_munkanapok_szama(@StartDate,@EndDate);
		END
	ELSE
		BEGIN
			RETURN -(dbo.fn_munkanapok_szama(@EndDate,@StartDate));
		END

	RETURN NULL

END
