﻿/*
--SET ANSI_NULLS ON
--SET QUOTED_IDENTIFIER ON
--GO

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_UgyUgyirat_HatralevoMunkaNapokV3]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
--	DROP FUNCTION [dbo].[FN_UgyUgyirat_HatralevoMunkaNapokV3]
--GO
*/
-- =============================================
-- Author:	      4IG
-- Create date:   2019.11
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[FN_UgyUgyirat_HatralevoMunkaNapokV3]
(
	@StartDate					DATETIME,
	@EndDate					DATETIME
)
RETURNS INT
AS
BEGIN

	IF @StartDate IS NULL OR @EndDate IS NULL
		RETURN NULL

	IF (@StartDate <= @EndDate)
		BEGIN
			RETURN  dbo.fn_munkanapok_szamaV3(@StartDate,@EndDate);
		END
	ELSE
		BEGIN
			RETURN -(dbo.fn_munkanapok_szamaV3(@EndDate,@StartDate));
		END

	RETURN NULL

END
