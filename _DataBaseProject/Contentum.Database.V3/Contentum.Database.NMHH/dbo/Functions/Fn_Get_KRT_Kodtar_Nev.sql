﻿-- =============================================
-- Author:		AXIS RENDSZERHAZ KFT.
-- Create date: 2017.06
-- Description:	
-- =============================================
CREATE FUNCTION dbo.Fn_Get_KRT_Kodtar_Nev
(
	@KodcsoportKod				NVARCHAR(MAX),
	@KodtarKod					NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @RET				NVARCHAR(MAX)
	
	DECLARE @KCSID				UNIQUEIDENTIFIER
	SELECT TOP 1 @KCSID = Id FROM KRT_Kodcsoportok WHERE Kod = @KodcsoportKod

	IF ( @KCSID IS NULL)
		RETURN NULL
	
	SELECT @RET = Nev
	FROM KRT_KodTarak
	WHERE KodCsoport_Id = @KCSID 
			AND
		   Kod = @KodtarKod

	RETURN @RET

END