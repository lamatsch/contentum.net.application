﻿-- =============================================
-- Author:		AXIS RENDSZERHAZ KFT.
-- Create date: 2017.06
-- Description:	
-- =============================================
CREATE FUNCTION dbo.Fn_RagszamSavFelhasznaltElemSzam
(
	@RagszamSavId				UNIQUEIDENTIFIER
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @RET				BIGINT
	
	SELECT @RET = COUNT(*)
	FROM KRT_RagSzamok
	WHERE RagszamSav_Id = @RagszamSavId
	AND Allapot='F'

	RETURN @RET

END