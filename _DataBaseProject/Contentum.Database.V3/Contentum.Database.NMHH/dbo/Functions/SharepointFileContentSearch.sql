--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
-- =============================================
-- Author:		AXIS
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[SharepointFileContentSearch]
(	
	@eDocumentWebServiceUrl [nvarchar](4000), 
	@felhasznaloId [nvarchar](100), 
	@keywords [nvarchar](4000)
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT NULL AS [Title], NULL AS [Path]
) 
GO
