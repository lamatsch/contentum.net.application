﻿CREATE FUNCTION [dbo].[fnSplitString] 
( 
    @string NVARCHAR(MAX), 
    @delimiter CHAR(1) 
) 
RETURNS @output TABLE(id int, splitdata NVARCHAR(MAX) 
) 
BEGIN 
IF len(@string) - len(replace(@string, ',', ''))  = '3'	
	BEGIN
		DECLARE @start INT, @end INT, @id INT
		SET @id = 1
		SELECT @start = 1, @end = CHARINDEX(@delimiter, @string) 
		WHILE @start < LEN(@string) + 1 BEGIN 
			IF @end = 0  
				SET @end = LEN(@string) + 1
       
			INSERT INTO @output (id, splitdata)  
			VALUES(@id,SUBSTRING(@string, @start, @end - @start)) 
			SET @start = @end + 1 
			SET @end = CHARINDEX(@delimiter, @string, @start)
			SET @id = @id + 1
		END 
     END

	 IF len(@string) - len(replace(@string, ',', ''))  = '2'	
	BEGIN
	--
	INSERT INTO @output (id, splitdata)  
			VALUES(1,'Magyarország') 
	--
		DECLARE @start2 INT, @end2 INT, @id2 INT
		SET @id2 = 2
		SELECT @start2 = 1, @end2 = CHARINDEX(@delimiter, @string) 
		WHILE @start2 < LEN(@string) + 1 BEGIN 
			IF @end2 = 0  
				SET @end2 = LEN(@string) + 1
       
			INSERT INTO @output (id, splitdata)  
			VALUES(@id2,SUBSTRING(@string, @start2, @end2 - @start2)) 
			SET @start2 = @end2 + 1 
			SET @end2 = CHARINDEX(@delimiter, @string, @start2)
			SET @id2 = @id2 + 1
		END 
     END
RETURN 	
END