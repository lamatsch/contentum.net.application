﻿CREATE FUNCTION [dbo].[fn_CalculateRightedJogtargyNumber]
  (	@CsoportId	uniqueidentifier,	-- a csoport azonosítója
	@ObjTipusId	uniqueidentifier,	-- objektum típus azonosítója
	@Jogszint	char(1)	= 'I'		-- vizsgált jogosultság szintje (I/O)
  )
RETURNS int
AS
BEGIN

	DECLARE @Org uniqueidentifier
	SET @Org = (select Org from KRT_Csoportok where Id = @CsoportId)

	DECLARE @KuKId uniqueidentifier;
	select @KuKId = Obj_Id
		from KRT_Kodcsoportok
			inner join KRT_Kodtarak on KRT_Kodtarak.Kodcsoport_Id =  KRT_Kodcsoportok.Id
				and KRT_Kodtarak.Kod = 'SPEC_SZERVEK.KOZPONTIIKTATO'
				and KRT_KodTarak.Org=@Org
		where KRT_Kodcsoportok.Kod = 'SPEC_SZERVEK'

	IF exists 
	(
		select 1 
			from KRT_CsoportTagok 
			where Csoport_Id_Jogalany = @CsoportId AND Csoport_Id = @KuKId
				AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
		union all
		select 1 
			from KRT_CsoportTagok
				inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id and KRT_Csoportok.Tipus = 'R'
			where Csoport_Id_Jogalany = @CsoportId AND Csoport_Id = @KuKId
				AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
	) return (select count(Id) from KRT_ACL where ObjTipus_Id = @ObjTipusId);
	
	return
	( 
	select Count(Num) from
		(
			select distinct KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos as Num		--vezeto
				from KRT_CsoportTagok 
					inner join KRT_CsoportTagokAll on KRT_CsoportTagok.Csoport_Id = KRT_CsoportTagokAll.Csoport_Id and KRT_CsoportTagok.Tipus = '3'
					inner join KRT_Jogosultak on KRT_Jogosultak.Csoport_Id_Jogalany = KRT_CsoportTagokAll.Csoport_Id_Jogalany
					inner join KRT_OBJEKTUM_ACLEK on KRT_OBJEKTUM_ACLEK.Obj_Id = KRT_Jogosultak.Obj_Id
					inner join KRT_ACL on KRT_ACL.Id = KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos and KRT_ACL.ObjTipus_Id = @ObjTipusId
				where KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId 
					AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
			union
			select distinct KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos as Num		--dolgozó, van öröklodés
				from KRT_CsoportTagok 
					inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id and KRT_Csoportok.JogosultsagOroklesMod = '1'
					inner join KRT_Jogosultak KRT_Jogosultak on KRT_Jogosultak.Csoport_Id_Jogalany = KRT_CsoportTagok.Csoport_Id_Jogalany and KRT_Jogosultak.Tipus != 'T'
					inner join KRT_OBJEKTUM_ACLEK KRT_OBJEKTUM_ACLEK on KRT_OBJEKTUM_ACLEK.Obj_Id = KRT_Jogosultak.Obj_Id
					inner join KRT_ACL on KRT_ACL.Id = KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos and KRT_ACL.ObjTipus_Id = @ObjTipusId
				where KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId
					AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
			union
			select distinct KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos as Num		--dolgozó, nincs öröklodés
				from KRT_CsoportTagok 
					inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id and isnull(KRT_Csoportok.JogosultsagOroklesMod,'0') != '1'
					inner join KRT_Jogosultak KRT_Jogosultak on KRT_Jogosultak.Csoport_Id_Jogalany = KRT_CsoportTagok.Csoport_Id
					inner join KRT_OBJEKTUM_ACLEK KRT_OBJEKTUM_ACLEK on KRT_OBJEKTUM_ACLEK.Obj_Id = KRT_Jogosultak.Obj_Id
					inner join KRT_ACL on KRT_ACL.Id = KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos and KRT_ACL.ObjTipus_Id = @ObjTipusId
				where KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId
					AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
		) as t
	)
	
	return 0;
END