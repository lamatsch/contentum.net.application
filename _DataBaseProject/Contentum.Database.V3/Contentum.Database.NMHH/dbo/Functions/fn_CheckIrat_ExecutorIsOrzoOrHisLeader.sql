﻿CREATE FUNCTION [dbo].[fn_CheckIrat_ExecutorIsOrzoOrHisLeader]
(	
	@IraIrat_Id UNIQUEIDENTIFIER,
	@ExecutorUserId UNIQUEIDENTIFIER,
	@FelhasznaloSzervezet_Id UNIQUEIDENTIFIER
)
RETURNS INT
AS
BEGIN

DECLARE @ret INT
SET @ret = 0

-- az irat orzojét itt a példányainak az orzoi határozzák meg,
-- hiszen nyilván láthatja, akinél van példánya és annak vezetoje is

IF @IraIrat_Id IS NOT NULL and @ExecutorUserId IS NOT NULL and @FelhasznaloSzervezet_Id IS NOT NULL
BEGIN
	DECLARE @csoporttagsag_tipus nvarchar(64)
	SET @csoporttagsag_tipus = (select top 1 KRT_CsoportTagok.Tipus from KRT_CsoportTagok
						where KRT_CsoportTagok.Csoport_Id_Jogalany=@ExecutorUserId
						and KRT_CsoportTagok.Csoport_Id=@FelhasznaloSzervezet_Id
						and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
						)

	IF (@csoporttagsag_tipus is not null and exists(select 1 from EREC_PldIratPeldanyok where EREC_PldIratPeldanyok.IraIrat_Id=@IraIrat_Id
        and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo = @ExecutorUserId))
		or -- végrehajtó felhasználó az orzo vezetoje-e
		(@csoporttagsag_tipus='3' and exists(select 1 from EREC_PldIratPeldanyok where EREC_PldIratPeldanyok.IraIrat_Id=@IraIrat_Id
        and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo in (select KRT_Csoportok.Id from KRT_Csoportok
			inner join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id = @FelhasznaloSzervezet_Id 
			and KRT_Csoportok.Tipus = '1' and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
			where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege 
			and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege)))

		or -- végrehajtó felhasználó a szervezet asszisztense
		(@csoporttagsag_tipus='4' and exists(select 1 from EREC_PldIratPeldanyok where EREC_PldIratPeldanyok.IraIrat_Id=@IraIrat_Id
        and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo in (select KRT_Csoportok.Id from KRT_Csoportok
			inner join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id = @FelhasznaloSzervezet_Id 
			and KRT_Csoportok.Tipus = '1' and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
			where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege 
			and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege)))



	BEGIN
        SET @ret = 1
    END
	ELSE
	BEGIN
		IF exists(select 1 from KRT_Jogosultak
					where KRT_Jogosultak.Obj_Id in (select @IraIrat_Id
--													UNION ALL
--													select Ugyirat_Id from EREC_IraIratok where Id=@IraIrat_Id
--													UNION ALL
--													select Id from EREC_PldIratPeldanyok
--													where EREC_PldIratPeldanyok.IraIrat_Id=@IraIrat_Id
													)
					and KRT_Jogosultak.Csoport_Id_Jogalany = @ExecutorUserId
					and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege)
		BEGIN
			SET @ret = 1
		END
	END					
END

RETURN @ret
END