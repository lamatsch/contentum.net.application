﻿CREATE FUNCTION [dbo].[fn_CheckIrat_IsConfidential]
(	
	@IraIrat_Id UNIQUEIDENTIFIER
)
RETURNS INT
AS
BEGIN

DECLARE @ret INT
SET @ret = 0

DECLARE @IratMinositesParamName varchar(30);
set @IratMinositesParamName = 'IRAT_MINOSITES_BIZALMAS'

DECLARE @IratMinositesMind varchar(30);
SET @IratMinositesMind = '40'

IF @IraIrat_Id IS NOT NULL
BEGIN
	-- Org meghatározása
	DECLARE @Org uniqueidentifier
	SET @Org = (select EREC_IraIktatoKonyvek.Org from EREC_IraIktatoKonyvek
				inner join EREC_UgyUgyiratdarabok on EREC_IraIktatoKonyvek.Id=EREC_UgyUgyiratdarabok.IraIktatoKonyv_Id
				inner join EREC_IraIratok on EREC_IraIratok.UgyUgyIratDarab_Id=EREC_UgyUgyiratdarabok.Id
				where EREC_IraIratok.Id=@IraIrat_Id)

	IF (@Org is not null)
	BEGIN
		DECLARE @confidential varchar(4000)
		SET @confidential = (select top 1 Ertek from KRT_Parameterek where Nev=@IratMinositesParamName
									and Org=@Org and getdate() between ErvKezd and ErvVege)

		IF (@confidential IS NOT NULL)
		BEGIN
			IF EXISTS (SELECT 1 
						FROM fn_Split(@confidential, ',')
						WHERE VALUE = @IratMinositesMind)
					BEGIN
							SET @RET = 1
							RETURN @RET;
					END


			IF exists(select 1 from EREC_IraIratok 
					  where EREC_IraIratok.Id=@IraIrat_Id
						and EREC_IraIratok.Minosites in 
								(select Value collate Hungarian_CS_AS from fn_Split(@confidential, ',')))
			BEGIN
				SET @ret = 1
			END
		END
		ELSE
			SET @ret = 1
	END
END
             

RETURN @ret
END