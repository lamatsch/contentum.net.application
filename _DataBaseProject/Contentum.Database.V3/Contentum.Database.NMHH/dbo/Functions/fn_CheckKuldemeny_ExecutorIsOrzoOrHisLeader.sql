﻿CREATE FUNCTION [dbo].[fn_CheckKuldemeny_ExecutorIsOrzoOrHisLeader]
(	
	@KuldKuldemeny_Id UNIQUEIDENTIFIER,
	@ExecutorUserId UNIQUEIDENTIFIER,
	@FelhasznaloSzervezet_Id UNIQUEIDENTIFIER
)
RETURNS INT
AS
BEGIN

DECLARE @ret INT
SET @ret = 0

IF @KuldKuldemeny_Id IS NOT NULL and @ExecutorUserId IS NOT NULL and @FelhasznaloSzervezet_Id IS NOT NULL
BEGIN
	DECLARE @csoporttagsag_tipus nvarchar(64)
	SET @csoporttagsag_tipus = (select top 1 KRT_CsoportTagok.Tipus from KRT_CsoportTagok
						where KRT_CsoportTagok.Csoport_Id_Jogalany=@ExecutorUserId
						and KRT_CsoportTagok.Csoport_Id=@FelhasznaloSzervezet_Id
						and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
						)

	IF (@csoporttagsag_tipus is not null and exists(select 1 from EREC_KuldKuldemenyek where EREC_KuldKuldemenyek.Id=@KuldKuldemeny_Id
        and EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo = @ExecutorUserId))
		or -- végrehajtó felhasználó az orzo vezetoje-e
		(@csoporttagsag_tipus='3' and exists(select 1 from EREC_KuldKuldemenyek where EREC_KuldKuldemenyek.Id=@KuldKuldemeny_Id
        and EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo in (select KRT_Csoportok.Id from KRT_Csoportok
			inner join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id = @FelhasznaloSzervezet_Id 
			and KRT_Csoportok.Tipus = '1' and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
			where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege 
			and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege)))

		or -- végrehajtó felhasználó a szervezet asszisztense
		(@csoporttagsag_tipus='4' and 
		  exists(select 1 from EREC_KuldKuldemenyek where EREC_KuldKuldemenyek.Id=@KuldKuldemeny_Id
				    and EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo in (select KRT_Csoportok.Id from KRT_Csoportok
				    inner join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id = @FelhasznaloSzervezet_Id 
				    and KRT_Csoportok.Tipus = '1' and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
					where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege 
					and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege)))


	BEGIN
        SET @ret = 1
    END
	ELSE
	BEGIN
		IF exists(select 1 from KRT_Jogosultak
					where KRT_Jogosultak.Obj_Id = @KuldKuldemeny_Id
					and KRT_Jogosultak.Csoport_Id_Jogalany = @ExecutorUserId
					and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege)
		BEGIN
			SET @ret = 1
		END
	END					
END

RETURN @ret
END