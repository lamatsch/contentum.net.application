﻿CREATE FUNCTION [dbo].[fn_EREC_IraIrattariTetelekCheckUK] (
             @Org uniqueidentifier ,
             @IrattariTetelszam Nvarchar(20) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_IraIrattariTetelek where
         (Org = @Org or (Org is null and @Org is null)) and            
         (IrattariTetelszam = @IrattariTetelszam or (IrattariTetelszam is null and @IrattariTetelszam is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END