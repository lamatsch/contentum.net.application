﻿CREATE FUNCTION [dbo].[fn_EREC_IraJegyzekekCheckUK] (
             @Tipus char(1) ,
             @Minosites Nvarchar(2) ,
             @Csoport_Id_felelos uniqueidentifier ,
             @Nev Nvarchar(100) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_IraJegyzekek where
         (Tipus = @Tipus or (Tipus is null and @Tipus is null)) and            
         (Minosites = @Minosites or (Minosites is null and @Minosites is null)) and            
         (Csoport_Id_felelos = @Csoport_Id_felelos or (Csoport_Id_felelos is null and @Csoport_Id_felelos is null)) and            
         (Nev = @Nev or (Nev is null and @Nev is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END