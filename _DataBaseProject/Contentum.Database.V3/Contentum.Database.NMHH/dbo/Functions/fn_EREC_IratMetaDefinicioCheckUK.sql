﻿CREATE FUNCTION [dbo].[fn_EREC_IratMetaDefinicioCheckUK] (
             @Org uniqueidentifier ,
             @UgykorKod Nvarchar(10) ,
             @Ugytipus nvarchar(64) ,
             @EljarasiSzakasz nvarchar(64) ,
             @Irattipus nvarchar(64) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_IratMetaDefinicio where
         (Org = @Org or (Org is null and @Org is null)) and            
         (UgykorKod = @UgykorKod or (UgykorKod is null and @UgykorKod is null)) and            
         (Ugytipus = @Ugytipus or (Ugytipus is null and @Ugytipus is null)) and            
         (EljarasiSzakasz = @EljarasiSzakasz or (EljarasiSzakasz is null and @EljarasiSzakasz is null)) and            
         (Irattipus = @Irattipus or (Irattipus is null and @Irattipus is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END