﻿CREATE FUNCTION [dbo].[fn_EREC_Irat_IktatokonyveiCheckUK] (
             @IraIktatokonyv_Id uniqueidentifier ,
             @Csoport_Id_Iktathat uniqueidentifier ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_Irat_Iktatokonyvei where
         (IraIktatokonyv_Id = @IraIktatokonyv_Id or (IraIktatokonyv_Id is null and @IraIktatokonyv_Id is null)) and            
         (Csoport_Id_Iktathat = @Csoport_Id_Iktathat or (Csoport_Id_Iktathat is null and @Csoport_Id_Iktathat is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END