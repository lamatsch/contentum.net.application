﻿CREATE FUNCTION [dbo].[fn_EREC_KuldKezFeljegyzesekCheckUK] (
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

-- nincsenek megadva oszlopok a Unique Key ellenorzeshez
SET @ret = 0      
      
Return @ret

END