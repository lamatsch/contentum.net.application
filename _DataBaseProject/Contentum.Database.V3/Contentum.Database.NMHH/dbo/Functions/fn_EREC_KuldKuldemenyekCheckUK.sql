﻿
CREATE FUNCTION dbo.fn_EREC_KuldKuldemenyekCheckUK (
             @IraIktatokonyv_Id uniqueidentifier ,
             @Erkezteto_Szam int ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_KuldKuldemenyek where
         (IraIktatokonyv_Id = @IraIktatokonyv_Id or (IraIktatokonyv_Id is null and @IraIktatokonyv_Id is null)) and            
         (Erkezteto_Szam = @Erkezteto_Szam or (Erkezteto_Szam is null and @Erkezteto_Szam is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END