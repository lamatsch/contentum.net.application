﻿CREATE FUNCTION [dbo].[fn_EREC_Obj_MetaDefinicioCheckUK] (
             @Org uniqueidentifier ,
             @ContentType nvarchar(100) ,             
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_Obj_MetaDefinicio where
         (Org = @Org or (Org is null and @Org is null)) and            
         (ContentType = @ContentType or (ContentType is null and @ContentType is null)) and         
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END