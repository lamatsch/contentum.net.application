﻿CREATE FUNCTION [dbo].[fn_EREC_PldIratPeldanyokCheckUK] (
             @IraIrat_Id uniqueidentifier ,
             @Sorszam int ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_PldIratPeldanyok where
         (IraIrat_Id = @IraIrat_Id or (IraIrat_Id is null and @IraIrat_Id is null)) and            
         (Sorszam = @Sorszam or (Sorszam is null and @Sorszam is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END