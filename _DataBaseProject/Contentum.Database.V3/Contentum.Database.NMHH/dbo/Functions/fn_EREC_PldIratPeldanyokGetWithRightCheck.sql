﻿

/* History
-- BUG_1797 ÁNYK-s nyomtatványok csatolmáűnyainak láthatósága - 2018.04.17., BogI 
   -- létrehozva
*/

CREATE FUNCTION [dbo].[fn_EREC_PldIratPeldanyokGetWithRightCheck] (
  		 @Id                      uniqueidentifier,
         @ExecutorUserId          uniqueidentifier,
         @FelhasznaloSzervezet_Id uniqueidentifier,
		 @Jogszint	  char(1) = '0',                                --- nincs használatban
         @FromHistory char(1) = ''
)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int = 1

---

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		Set @ret = -50202;                         --- Raiserror code sp_~ számára
	end

	--if dbo.fn_IsAdmin(@ExecutorUserId) = 0 AND not exists
	IF @ret = 1 AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0 AND not exists
	(
		SELECT top(1) 1 FROM krt_jogosultak
			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			where krt_jogosultak.Obj_Id = @Id
	) AND NOT EXISTS
	(
		SELECT 1 FROM EREC_PldIratpeldanyok
			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
				ON CsoportTagokAll.Id = EREC_PldIratpeldanyok.Csoport_Id_Felelos
			where EREC_PldIratpeldanyok.Id = @Id
	) AND NOT EXISTS
	(
		SELECT 1 FROM EREC_PldIratpeldanyok
			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
				ON CsoportTagokAll.Id = EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo
			where EREC_PldIratpeldanyok.Id = @Id
	) and not exists
	(
		SELECT top(1) 1 FROM EREC_PldIratPeldanyok
			INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
			WHERE EREC_PldIratPeldanyok.Id = @Id
				AND EREC_IraIratok.Ugyirat_Id IN 
			(
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
				UNION ALL
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id --AND @Jogszint != 'I'
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
				UNION ALL
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
						ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
				UNION ALL
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
						ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
				UNION ALL
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
						ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
				UNION ALL
				SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_UgyUgyiratok')
				UNION ALL
				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
					INNER JOIN KRT_MappaTartalmak on KRT_MappaTartalmak.Obj_Id = erec_ugyugyiratok.Id
					INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
					INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
					INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
					where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
					and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
			)
	) AND NOT EXISTS
	(
		SELECT 1 FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_PldIratPeldanyok') as ids
			WHERE ids.Id = @Id
	) AND NOT EXISTS
	(
		SELECT 1 FROM EREC_PldIratpeldanyok
			INNER JOIN dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, 'EREC_IraIratok') as ids
				ON ids.Id = EREC_PldIratpeldanyok.IraIrat_Id
			where EREC_PldIratpeldanyok.Id = @Id
	) AND NOT EXISTS
	(
		SELECT 1 FROM EREC_PldIratpeldanyok
			INNER JOIN KRT_MappaTartalmak on KRT_MappaTartalmak.Obj_Id = EREC_PldIratpeldanyok.Id
			INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
			INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
			and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
			and EREC_PldIratpeldanyok.Id = @Id
	)
		Set @ret = -50102;                         --- Raiserror code sp_~ számára

	-- Org szűrés
	IF(@FromHistory='')
	BEGIN
		IF not exists(select 1 from EREC_IraIktatoKonyvek
			where EREC_IraIktatoKonyvek.Org=@Org
			and EREC_IraIktatoKonyvek.Id=
			(select EREC_UgyUgyiratdarabok.IraIktatoKonyv_Id from EREC_UgyUgyiratdarabok
			where EREC_UgyUgyiratdarabok.Id=
				(select EREC_IraIratok.UgyUgyIratDarab_Id from EREC_IraIratok
				where EREC_IraIratok.Id=
					(select EREC_PldIratPeldanyok.IraIrat_Id from EREC_PldIratPeldanyok
					where Id=@Id)
				)
			))
		BEGIN
			Set @ret = -50101
		END
	END
	ELSE
	BEGIN
		IF not exists(select 1 from EREC_IraIktatoKonyvek
			where EREC_IraIktatoKonyvek.Org=@Org
			and EREC_IraIktatoKonyvek.Id=
			(select EREC_UgyUgyiratdarabok.IraIktatoKonyv_Id from EREC_UgyUgyiratdarabok
			where EREC_UgyUgyiratdarabok.Id=
				(select EREC_IraIratok.UgyUgyIratDarab_Id from EREC_IraIratok
				where EREC_IraIratok.Id=
					(select EREC_PldIratPeldanyokHistory.IraIrat_Id from EREC_PldIratPeldanyokHistory
					where Id=@Id)
				)
			))
		BEGIN
			Set @ret = -50101
		END
	END

---
   
Return @ret

END


