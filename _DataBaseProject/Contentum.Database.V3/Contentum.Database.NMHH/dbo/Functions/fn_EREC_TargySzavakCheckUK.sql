﻿CREATE FUNCTION [dbo].[fn_EREC_TargySzavakCheckUK] (
             @Org uniqueidentifier ,
             @TargySzavak Nvarchar(100) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_TargySzavak where
         (Org = @Org or (Org is null and @Org is null)) and            
         (TargySzavak = @TargySzavak or (TargySzavak is null and @TargySzavak is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END