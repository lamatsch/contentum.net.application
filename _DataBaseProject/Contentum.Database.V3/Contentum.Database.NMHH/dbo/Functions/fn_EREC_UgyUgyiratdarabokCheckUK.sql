﻿CREATE FUNCTION [dbo].[fn_EREC_UgyUgyiratdarabokCheckUK] (
             @UgyUgyirat_Id uniqueidentifier ,
             @LetrehozasIdo datetime ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_UgyUgyiratdarabok where
         (UgyUgyirat_Id = @UgyUgyirat_Id or (UgyUgyirat_Id is null and @UgyUgyirat_Id is null)) and            
         (LetrehozasIdo = @LetrehozasIdo or (LetrehozasIdo is null and @LetrehozasIdo is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END