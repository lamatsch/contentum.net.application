﻿CREATE FUNCTION [dbo].[fn_EREC_UgyUgyiratokCheckUK] (
             @IraIktatokonyv_Id uniqueidentifier ,
             @Foszam int ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_UgyUgyiratok where
         (IraIktatokonyv_Id = @IraIktatokonyv_Id or (IraIktatokonyv_Id is null and @IraIktatokonyv_Id is null)) and            
         (Foszam = @Foszam or (Foszam is null and @Foszam is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END