﻿/*
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_EREC_UgyUgyiratok_ElteltIdo_1Eseten_Novelheto]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fn_EREC_UgyUgyiratok_ElteltIdo_1Eseten_Novelheto]
	GO
	SET ANSI_NULLS ON
	GO
	SET QUOTED_IDENTIFIER ON
	GO
*/

CREATE FUNCTION [dbo].[fn_EREC_UgyUgyiratok_ElteltIdo_1Eseten_Novelheto] 
(
	@Ugyirat_Id uniqueidentifier
)

RETURNS BIT
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return BIT;
		
	DECLARE @ID			 UNIQUEIDENTIFIER
	DECLARE @ELT_IDO_MOD DATETIME

	 SELECT @ID=U.ID, @ELT_IDO_MOD = U.ElteltIdoUtolsoModositas
	 FROM EREC_UgyUgyiratok U
	 WHERE U.ID = @Ugyirat_Id
			AND
			U.ElteltIdo = 1

	/*HA NEM 1 AZ ELTELT IDO*/
	IF @ID IS NULL
		RETURN 1

	/*HA MEG URES A MODOSITAS DATUMA */
	IF @ELT_IDO_MOD IS NULL
		RETURN 1

	/*HA 1 AZ ELTELT IDO*/
		/*HA ELTELT IDO UTOLSO MODOSITAS = MA VAGY UTOLSO MODOSITAS = MA+1*/
		IF (DATEADD(DAY,1,@ELT_IDO_MOD) > GETDATE())
			BEGIN
				---- 'NEM NOVELHETO'
				RETURN 0
			END
		ELSE
			BEGIN
				---- 'NOVELHETO'
				RETURN 1
			END
		

   SET @Return = 1;
 
   RETURN 1;

END