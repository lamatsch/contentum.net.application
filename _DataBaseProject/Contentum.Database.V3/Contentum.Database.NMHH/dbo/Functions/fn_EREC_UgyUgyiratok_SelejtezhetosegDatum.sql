﻿CREATE FUNCTION [dbo].[fn_EREC_UgyUgyiratok_SelejtezhetosegDatum] 
(
	@Ugyirat_Id uniqueidentifier
)

RETURNS DATETIME
WITH EXECUTE AS CALLER
AS

BEGIN
	DECLARE @Return DATETIME;

	;with
	ugyirat_selejtezheto as
	(
	select Id, MegorzesiIdoVege, UgyUgyirat_Id_Szulo from EREC_UgyUgyiratok where id = @Ugyirat_Id
	union all
	select u.Id, u.MegorzesiIdoVege, u.UgyUgyirat_Id_Szulo from EREC_UgyUgyiratok u inner join ugyirat_selejtezheto s on s.id = u.UgyUgyirat_Id_Szulo and u.allapot = '60'
	)

	SELECT @Return = max(MegorzesiIdoVege) from ugyirat_selejtezheto
   
   RETURN @Return;

END



