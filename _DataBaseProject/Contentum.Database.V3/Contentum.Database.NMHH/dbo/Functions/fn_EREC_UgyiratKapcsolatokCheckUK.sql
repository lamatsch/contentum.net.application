﻿CREATE FUNCTION [dbo].[fn_EREC_UgyiratKapcsolatokCheckUK] (
             @Ugyirat_Ugyirat_Beepul uniqueidentifier ,
             @Ugyirat_Ugyirat_Felepul uniqueidentifier ,
             @KapcsolatTipus nvarchar(64) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from EREC_UgyiratKapcsolatok where
         (Ugyirat_Ugyirat_Beepul = @Ugyirat_Ugyirat_Beepul or (Ugyirat_Ugyirat_Beepul is null and @Ugyirat_Ugyirat_Beepul is null)) and            
         (Ugyirat_Ugyirat_Felepul = @Ugyirat_Ugyirat_Felepul or (Ugyirat_Ugyirat_Felepul is null and @Ugyirat_Ugyirat_Felepul is null)) and            
         (KapcsolatTipus = @KapcsolatTipus or (KapcsolatTipus is null and @KapcsolatTipus is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END