﻿CREATE FUNCTION [dbo].[fn_Ervenyes]
		(
		@ErvKezd  datetime,
		@ErvVege  datetime
		)
RETURNS char(1) 
AS
BEGIN
  RETURN dbo.fn_Ervenyesseg('ERV',@ErvKezd,@ErvVege,NULL,NULL)
END
-- select dbo.fn_Ervenyes(getdate()-1,NULL) ERV
-- select top 1 * from KRT_Csoportok where dbo.fn_Ervenyes(ErvKezd,ErvVege)= 'I'