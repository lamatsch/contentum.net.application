﻿CREATE FUNCTION [dbo].[fn_Ervenyesseg]
		(
		@Tipus    char(3),
		@ErvKezd  datetime,
		@ErvVege  datetime,
		@Datumtol datetime = NULL,
		@Datumig  datetime = NULL
		)
RETURNS char(1) 
AS
/*------------------------------------------------------------------------------
Funkció: Az érvényességi dátumok és a vizsgált idoszak alapján megállapítja,
         hogy egy adott idoszakban érvényes vagy létezo-e a tétel.

Paraméterek:
         @Tipus      - a vizsgálat típusa: ERV - érvényeség, LET - létezés
         @ErvKezd    - a tétel érvényesség kezdete
         @ErvVege    - a tétel érvényesség vége
         @Datumtol   - a vizsgálandó idoszak kezdete
         @Datumig    - a vizsgálandó idoszak vége

Kimenet: a vizsgálat eredménye (I/N)
Példa: 
     select dbo.fn_Ervenyesseg('ERV',getdate()-1,NULL,NULL,NULL) ERV
--------------------------------------------------------------------------------*/ 
BEGIN

  DECLARE @Result char(1)
  SET @Datumtol = isnull(@Datumtol,getdate())
  SET @Datumig  = isnull(@Datumig,getdate())

  SET @Result = case when @Tipus = 'ERV' then
						 case when (@Datumtol between isnull(@ErvKezd,@Datumtol) and isnull(@ErvVege,@Datumig))
							   and (@Datumig  between isnull(@ErvKezd,@Datumtol) and isnull(@ErvVege,@Datumig))
							  then 'I' -- érvényes
							  else 'N' -- nem érvényes
						  end
					  when @Tipus = 'LET' then
						  case when isnull(@ErvKezd,@Datumtol) <= @Datumig
								and isnull(@ErvVege,@Datumig)  >= @Datumtol
							   then 'I' -- létezik
							   else 'N' -- nem létezik
						   end
					  else null
				 end

  RETURN @Result

END