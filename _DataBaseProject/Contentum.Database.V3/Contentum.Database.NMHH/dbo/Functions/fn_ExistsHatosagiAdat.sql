﻿--set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_ExistsHatosagiAdat]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
--	DROP FUNCTION [dbo].[fn_ExistsHatosagiAdat]
--GO

CREATE FUNCTION [dbo].[fn_ExistsHatosagiAdat]
(	
	@IratId uniqueidentifier
	
)
RETURNS char(1) 
AS
BEGIN

DECLARE @ret char(1)

DECLARE @count int

SET @count = 
              (
				 SELECT COUNT(1)
                 FROM VW_EREC_IraOnkormAdatok as EREC_IraOnkormAdatok
                 where EREC_IraOnkormAdatok.IraIratok_Id = @IratId
                 and
                 ( 
                     EREC_IraOnkormAdatok.DontestHozta is not null
                     or EREC_IraOnkormAdatok.DontesFormaja is not null
                     or EREC_IraOnkormAdatok.UgyintezesHataridore is not null
                     or EREC_IraOnkormAdatok.HataridoTullepes is not null
                     or EREC_IraOnkormAdatok.JogorvoslatiEljarasTipusa is not null
                     or EREC_IraOnkormAdatok.JogorvoslatiDontesTipusa is not null
                     or EREC_IraOnkormAdatok.JogorvoslatiDontestHozta is not null
                     or EREC_IraOnkormAdatok.JogorvoslatiDontesTartalma is not null
                     or EREC_IraOnkormAdatok.JogorvoslatiDontes is not null
                 ) 
              )

IF @count > 0
BEGIN
	SET @ret = '1'
END
ELSE
BEGIN
	SET @ret = '0'
END

RETURN @ret

END

