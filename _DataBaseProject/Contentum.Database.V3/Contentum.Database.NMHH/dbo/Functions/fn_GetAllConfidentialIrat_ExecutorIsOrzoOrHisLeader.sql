﻿CREATE FUNCTION [dbo].[fn_GetAllConfidentialIrat_ExecutorIsOrzoOrHisLeader]
(	
	@ExecutorUserId UNIQUEIDENTIFIER,
	@FelhasznaloSzervezet_Id UNIQUEIDENTIFIER
)
RETURNS @IraIrat_Ids table
(
	Id uniqueidentifier
)
AS
BEGIN

	DECLARE @IratMinositesParamName varchar(30);
	set @IratMinositesParamName = 'IRAT_MINOSITES_BIZALMAS'

	IF @ExecutorUserId IS NOT NULL and @FelhasznaloSzervezet_Id IS NOT NULL
	BEGIN
		-- Org meghatározása
		DECLARE @Org uniqueidentifier
		SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)

		IF (@Org is not null)
		BEGIN
			DECLARE @confidential varchar(4000)
			set @confidential = (select top 1 Ertek from KRT_Parameterek where Nev=@IratMinositesParamName
									and Org=@Org and getdate() between ErvKezd and ErvVege)

			IF (@confidential IS NOT NULL)
			BEGIN
				declare @Bizalmas table (val nvarchar(64) collate Hungarian_CS_AS )
				insert into @Bizalmas (val) (select Value from fn_Split(@confidential, ','))

				-- az irat orzojét itt a példányainak az orzoi határozzák meg,
				-- hiszen nyilván láthatja, akinél van példánya és annak vezetoje is

				DECLARE @csoporttagsag_tipus nvarchar(64)
				SET @csoporttagsag_tipus = (select top 1 KRT_CsoportTagok.Tipus from KRT_CsoportTagok
									where KRT_CsoportTagok.Csoport_Id_Jogalany=@ExecutorUserId
									and KRT_CsoportTagok.Csoport_Id=@FelhasznaloSzervezet_Id
									and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
									)
				IF @csoporttagsag_tipus is not null
				BEGIN
					insert into @IraIrat_Ids
						select EREC_IraIratok.Id
							from EREC_IraIratok
								inner join @Bizalmas as Bizalmas on EREC_IraIratok.Minosites = Bizalmas.val
							where 
							(
								EXISTS
								(
									select 1
										from EREC_PldIratPeldanyok
										where EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id
											and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo = @ExecutorUserId
								)
							)
						
						UNION -- végrehajtó felhasználó az orzo vezetoje-e
						select EREC_IraIratok.Id
							from EREC_IraIratok
								inner join @Bizalmas as Bizalmas on EREC_IraIratok.Minosites = Bizalmas.val
							WHERE @csoporttagsag_tipus='3'
								and EXISTS
								(
									select 1
										from EREC_PldIratPeldanyok
										where EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id
											and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo IN
											(
												select KRT_Csoportok.Id
													from KRT_Csoportok
														inner join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id = @FelhasznaloSzervezet_Id 
															and KRT_Csoportok.Tipus = '1' and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
													where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege 
														and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
											)
								)

--						UNION -- van-e  objektumon keresztül joga a példányhoz
--						select EREC_IraIratok.Id
--							from EREC_IraIratok
--								inner join @Bizalmas as Bizalmas on EREC_IraIratok.Minosites = Bizalmas.val
--							where 
--							(
--								EXISTS
--								(
--									select 1
--										from EREC_PldIratPeldanyok
--											left join KRT_Jogosultak on KRT_Jogosultak.Obj_Id=EREC_PldIratPeldanyok.Id
--										where EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id
--											and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
--											and @ExecutorUserId = KRT_Jogosultak.Csoport_Id_Jogalany
--								)
--							)
						UNION -- van-e objektumon keresztül joga az irathoz
						select EREC_IraIratok.Id
							from EREC_IraIratok
								inner join @Bizalmas as Bizalmas on EREC_IraIratok.Minosites = Bizalmas.val
							where 
							(
								EXISTS
								(
									select 1
										from KRT_Jogosultak where
											KRT_Jogosultak.Obj_Id=EREC_IraIratok.Id
											and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
											and @ExecutorUserId = KRT_Jogosultak.Csoport_Id_Jogalany
								)
							)
--						UNION -- van-e objektumon keresztül joga az ügyirathoz
--						select EREC_IraIratok.Id
--							from EREC_IraIratok
--								inner join @Bizalmas as Bizalmas on EREC_IraIratok.Minosites = Bizalmas.val
--							where 
--							(
--								EXISTS
--								(
--									select 1
--										from KRT_Jogosultak where
--											KRT_Jogosultak.Obj_Id=EREC_IraIratok.Ugyirat_Id
--											and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
--											and @ExecutorUserId = KRT_Jogosultak.Csoport_Id_Jogalany
--								)
--							)

								/*
								or -- végrehajtó felhasználó az orzo vezetoje-e
								(
									@csoporttagsag_tipus='3'
									and EXISTS
									(
										select 1
											from EREC_PldIratPeldanyok
											where EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id
												and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo IN
												(
													select KRT_Csoportok.Id
														from KRT_Csoportok
															inner join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id = @FelhasznaloSzervezet_Id 
																and KRT_Csoportok.Tipus = '1' and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
														where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege 
															and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
												)
									)
								
								)*/
				END					
			END
		END
	END

	RETURN;
END