﻿CREATE FUNCTION [dbo].[fn_GetAllIktathatoEREC_IraIktatokonyvekByCsoportId]
	(	@CsoportId		UNIQUEIDENTIFIER,
		@SzervezetId	UNIQUEIDENTIFIER
	)
RETURNS TABLE
AS
RETURN
(
	SELECT EREC_IraIktatoKonyvek.* 
		FROM EREC_IraIktatoKonyvek
			INNER JOIN EREC_Irat_Iktatokonyvei ON EREC_IraIktatoKonyvek.Id = EREC_Irat_Iktatokonyvei.IraIktatokonyv_Id
		WHERE GETDATE() BETWEEN EREC_Irat_Iktatokonyvei.ErvKezd AND EREC_Irat_Iktatokonyvei.ErvVege
			AND EREC_Irat_Iktatokonyvei.Csoport_Id_Iktathat IN (@CsoportId, @SzervezetId)
--	SELECT * FROM fn_GetAllIktathatoEREC_IraIktatokonyvekByCsoportIdAndIktatoErkezteto(@CsoportId,@SzervezetId,'I')
--	UNION
--	SELECT * FROM fn_GetAllIktathatoEREC_IraIktatokonyvekByCsoportIdAndIktatoErkezteto(@CsoportId,@SzervezetId,'E')
--	UNION
--	SELECT * FROM fn_GetAllIktathatoEREC_IraIktatokonyvekByCsoportIdAndIktatoErkezteto(@CsoportId,@SzervezetId,'P')

)