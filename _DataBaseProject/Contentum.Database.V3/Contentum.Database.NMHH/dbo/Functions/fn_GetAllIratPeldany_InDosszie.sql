﻿CREATE FUNCTION [dbo].[fn_GetAllIratPeldany_InDosszie]
	(	@Mappa_Id		UNIQUEIDENTIFIER = NULL,
		@Iratpeldany_Ids	NVARCHAR(MAX)
	)
RETURNS @Iratpeldanyok TABLE
(	
	Id	UNIQUEIDENTIFIER
)
AS
BEGIN
	declare @iratpeldanyokTable table(id uniqueidentifier);
	declare @it	int;
	declare @curId	nvarchar(36);

	set @it = 0;
	while (@it < ((len(@Iratpeldany_Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Iratpeldany_Ids,@it*39+2,37);
		insert into @iratpeldanyokTable(id) values(@curId);
		set @it = @it + 1;
	END	
	
	IF (@Mappa_Id IS NOT NULL)
	BEGIN
		INSERT INTO @Iratpeldanyok
			SELECT KRT_MappaTartalmak.Obj_Id
				FROM KRT_MappaTartalmak
				WHERE KRT_MappaTartalmak.Obj_Id IN (SELECT Id FROM @iratpeldanyokTable)
					AND KRT_MappaTartalmak.Mappa_Id = @Mappa_Id
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
	END
	ELSE
	BEGIN
		INSERT INTO @Iratpeldanyok
			SELECT KRT_MappaTartalmak.Obj_Id
				FROM KRT_MappaTartalmak
					INNER JOIN KRT_Mappak ON KRT_MappaTartalmak.Mappa_Id = KRT_Mappak.Id
				WHERE KRT_MappaTartalmak.Obj_Id IN (SELECT Id FROM @iratpeldanyokTable)
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
					AND KRT_Mappak.Tipus = '01'
	END
	RETURN;

END