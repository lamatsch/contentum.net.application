﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetAllJogosultIrattariHely]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fn_GetAllJogosultIrattariHely]
GO

CREATE FUNCTION [dbo].[fn_GetAllJogosultIrattariHely]
	(	@FelhasznaloId		UNIQUEIDENTIFIER,
		@SzervezetId	UNIQUEIDENTIFIER
	)
RETURNS @IrattariHelyek TABLE
(	
	[Id]	UNIQUEIDENTIFIER
)
AS
BEGIN
	DECLARE @isAdmin INT;
	SELECT @isAdmin = dbo.fn_IsAdminInSzervezet(@FelhasznaloId, @SzervezetId);

	with jogosult as
	(
		select Id from EREC_IrattariHelyek
		where Id in (select Obj_Id from KRT_Jogosultak j where j.Csoport_Id_Jogalany in (@FelhasznaloId, @SzervezetId))
		and GETDATE() between ErvKezd and ErvVege
	),
    szulok as 
	(
	  select Id, 
			 SzuloId
	  from EREC_IrattariHelyek
	  where Id in (select id from jogosult)

	  union all
	  select hely.Id,
			 hely.SzuloId
	  from EREC_IrattariHelyek hely
	  join szulok sz on sz.SzuloId = hely.Id
	  where GETDATE() between hely.ErvKezd and hely.ErvVege
	)
	,gyerekek as 
	(
	  select Id, 
			 SzuloId
	  from EREC_IrattariHelyek
	  where Id in (select id from jogosult)

	  union all
	  select hely.Id,
			 hely.SzuloId
	  from EREC_IrattariHelyek hely
	  join gyerekek gy on gy.Id = hely.SzuloId
	  where GETDATE() between hely.ErvKezd and hely.ErvVege
	)
	
	INSERT INTO @IrattariHelyek
	select Id
	from szulok
	union
	select Id
	from gyerekek

	RETURN;
END