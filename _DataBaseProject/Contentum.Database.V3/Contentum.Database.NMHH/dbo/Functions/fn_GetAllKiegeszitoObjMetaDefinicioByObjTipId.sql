﻿create function [dbo].[fn_GetAllKiegeszitoObjMetaDefinicioByObjTipId] (@Org uniqueidentifier, @Objtip_Id uniqueidentifier)
returns @result table ( Id uniqueidentifier
                      , ContentType nvarchar(100)
                      , ObjTip_Kod nvarchar(100)
                      , Szint int
                      ) 

/*
Objtip_Id csak az EREC_UgyUgyiratok, EREC_UgyUgyiratdarabok vagy EREC_IraIratok tábla KRT_ObjTipusok azonosítója lehet
(egyébként üras táblát ad eredményül)
feladata, hogy az objektum típusból (csak ügyiratnyilvántartási objektum - ügyirat, ügyiratdarab, irat - lehet)
és az ahhoz tartozó B2 típusú metadefiníciótól kiindulva meghatározza a kapcsolódó
B1 típusú objektum metadefiníciókat, melyek nincsenek alacsonyabb szinten, mint az adott objektum típus
(tehát pl. ügyiratdarab szintu objektum esetén az iratra vonatkozók nem jönnek vissza)
returns: a megfelelo objektum metadefiníciók azonosítóit, ContentType-jukat,
         az objektum (tábla) nevét
         és relatív hierarchiaszintjüket tartalmazó tábla,
         a 0 szinten az eredeti objektum metadefiníció található (ha létezik megfeleloje az irat metadefiníciós táblában)

-- hívás példa:
select * from dbo.fn_GetAllKiegeszitoObjMetaDefinicioByObjTipId('450B510A-7CAA-46B0-83E3-18445C0C53A9','AA5E7BBA-96A0-4C17-8709-06A6D297E107') -- EREC_IraIratok
       --> 'FA242B51-7819-4003-A279-7CEFC1D5892A	Irat_Kiegeszites	EREC_IraIratok	0
       --> '99FC16DF-3C23-4445-AD62-6E802D2AAC9A	Ugyirat_Kiegeszites	EREC_UgyUgyiratok	2
go
select * from dbo.fn_GetAllKiegeszitoObjMetaDefinicioByObjTipId('450B510A-7CAA-46B0-83E3-18445C0C53A9','A04417A6-54AC-4AF1-9B63-5BABF0203D42') -- EREC_UgyUgyiratok
       --> '99FC16DF-3C23-4445-AD62-6E802D2AAC9A'	Ugyirat_Kiegeszites	EREC_UgyUgyiratok	0
go
select * from dbo.fn_GetAllKiegeszitoObjMetaDefinicioByObjTipId('450B510A-7CAA-46B0-83E3-18445C0C53A9','00000000-0000-0000-0000-000000000000')
       --> <üres tábla>
go
*/
AS
BEGIN

    if @Objtip_Id is not null and @Org is not null
    begin
        -- ellenorizzük, hogy létezik-e az objektum típus
        declare @BottomLevelObjtip_Id uniqueidentifier
        declare @BottomLevelObjtip_Kod nvarchar(100)
        
        select @BottomLevelObjtip_Id = Id, @BottomLevelObjtip_Kod = Kod
               from KRT_ObjTipusok
               where Id = @Objtip_Id
               and getdate() between ErvKezd and ErvVege
               and ObjTipus_Id_Tipus = (select Id from KRT_ObjTipusok objtip2
                                        where objtip2.Kod = 'DbTable'
                                        and getdate() between objtip2.ErvKezd and objtip2.ErvVege)
        if @BottomLevelObjtip_Id is not null
        begin
            if @BottomLevelObjtip_Kod is not null
                and (@BottomLevelObjtip_Kod = 'EREC_UgyUgyiratok'
                or @BottomLevelObjtip_Kod = 'EREC_UgyUgyiratdarabok'
                or @BottomLevelObjtip_Kod = 'EREC_IraIratok')
            begin
                declare @Objtip_Id_Iratok uniqueidentifier
                declare @Objtip_Id_Ugyiratdarabok uniqueidentifier
                declare @Objtip_Id_Ugyiratok uniqueidentifier
                -- objektum típusok azonosítóinak lekérése
                set @Objtip_Id_Iratok = (select objtip.Id from KRT_ObjTipusok objtip
                                         where objtip.Kod = 'EREC_IraIratok'
                                         and objtip.ObjTipus_Id_Tipus = (select Id from KRT_ObjTipusok objtip2
                                                                         where objtip2.Kod = 'DbTable'
                                                                         and getdate() between objtip2.ErvKezd and objtip2.ErvVege)
                                         and getdate() between objtip.ErvKezd and objtip.ErvVege)

                set @Objtip_Id_Ugyiratdarabok = (select objtip.Id from KRT_ObjTipusok objtip
                                         where objtip.Kod = 'EREC_UgyUgyiratdarabok'
                                         and objtip.ObjTipus_Id_Tipus = (select Id from KRT_ObjTipusok objtip2
                                                                         where objtip2.Kod = 'DbTable'
                                                                         and getdate() between objtip2.ErvKezd and objtip2.ErvVege)
                                         and getdate() between objtip.ErvKezd and objtip.ErvVege)

                set @Objtip_Id_Ugyiratok = (select objtip.Id from KRT_ObjTipusok objtip
                                         where objtip.Kod = 'EREC_UgyUgyiratok'
                                         and objtip.ObjTipus_Id_Tipus = (select Id from KRT_ObjTipusok objtip2
                                                                         where objtip2.Kod = 'DbTable'
                                                                         and getdate() between objtip2.ErvKezd and objtip2.ErvVege)
                                         and getdate() between objtip.ErvKezd and objtip.ErvVege)

                -- felhasználjuk a már lekért adatokat
                insert into @result
                    select Id
                         , ContentType
                         , @BottomLevelObjtip_Kod as Objtip_Kod
                         , 0 as Szint
                         from EREC_Obj_MetaDefinicio
                                   where DefinicioTipus = 'B1'
                                   and Objtip_Id = @BottomLevelObjtip_Id
                                   and Org=@Org
                                   and getdate() between ErvKezd and ErvVege
                -- magasabb szintek felvétele
                if @BottomLevelObjtip_Kod = 'EREC_UgyUgyiratdarabok' or @BottomLevelObjtip_Kod = 'EREC_IraIratok'
                begin
                    declare @Szint int
                    if @BottomLevelObjtip_Kod = 'EREC_UgyUgyiratdarabok'
                       set @Szint = 1
                    else
                       set @Szint = 2

                    insert into @result
                        select Id
                             , ContentType
                             , 'EREC_UgyUgyiratok' as Objtip_Kod
                             , @Szint as Szint
                        from EREC_Obj_MetaDefinicio
                        where DefinicioTipus = 'B1'
                        and Objtip_Id = @Objtip_Id_Ugyiratok
                        and Org=@Org
                        and getdate() between ErvKezd and ErvVege
                end
            
                if @BottomLevelObjtip_Kod = 'EREC_IraIratok'
                begin
                    insert into @result
                        select Id
                             , ContentType
                             , 'EREC_UgyUgyiratdarabok' as Objtip_Kod
                             , 1 as Szint
                        from EREC_Obj_MetaDefinicio
                        where DefinicioTipus = 'B1'
                        and Objtip_Id = @Objtip_Id_Ugyiratdarabok
                        and Org=@Org
                        and getdate() between ErvKezd and ErvVege
                end
            end
        end
    end
    return;

END