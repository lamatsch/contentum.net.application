﻿CREATE FUNCTION [dbo].[fn_GetAllKuldemeny_InDosszie]
	(	@Mappa_Id		UNIQUEIDENTIFIER = NULL,
		@Kuldemeny_Ids	NVARCHAR(MAX)
	)
RETURNS @Kuldemenyek TABLE
(	
	Id	UNIQUEIDENTIFIER
)
AS
BEGIN
	declare @kuldemenyTable table(id uniqueidentifier);
	declare @it	int;
	declare @curId	nvarchar(36);

	set @it = 0;
	while (@it < ((len(@Kuldemeny_Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Kuldemeny_Ids,@it*39+2,37);
		insert into @kuldemenyTable(id) values(@curId);
		set @it = @it + 1;
	END	
	
	IF (@Mappa_Id IS NOT NULL)
	BEGIN
		INSERT INTO @Kuldemenyek
			SELECT KRT_MappaTartalmak.Obj_Id
				FROM KRT_MappaTartalmak
				WHERE KRT_MappaTartalmak.Obj_Id IN (SELECT Id FROM @kuldemenyTable)
					AND KRT_MappaTartalmak.Mappa_Id = @Mappa_Id
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
	END
	ELSE
	BEGIN
		INSERT INTO @Kuldemenyek
			SELECT KRT_MappaTartalmak.Obj_Id
				FROM KRT_MappaTartalmak
					INNER JOIN KRT_Mappak ON KRT_MappaTartalmak.Mappa_Id = KRT_Mappak.Id
				WHERE KRT_MappaTartalmak.Obj_Id IN (SELECT Id FROM @kuldemenyTable)
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
					AND KRT_Mappak.Tipus = '01'
	END
	RETURN;

END