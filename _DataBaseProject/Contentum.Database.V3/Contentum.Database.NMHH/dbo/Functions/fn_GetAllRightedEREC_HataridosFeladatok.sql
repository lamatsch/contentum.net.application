﻿CREATE FUNCTION [dbo].[fn_GetAllRightedEREC_HataridosFeladatok]
	(	 
	     @FelhasznaloId		UNIQUEIDENTIFIER,
		 @SzervezetId		UNIQUEIDENTIFIER
	)
RETURNS TABLE
AS
RETURN
(
	    SELECT EREC_HataridosFeladatok.Id FROM EREC_HataridosFeladatok
			INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId) as CsoportTagokAllFelhasznalo ON CsoportTagokAllFelhasznalo.Id = EREC_HataridosFeladatok.Felhasznalo_Id_Felelos
			 -- INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAllCsoport ON CsoportTagokAllCsoport.Id = EREC_HataridosFeladatok.Csoport_Id_Felelos
			WHERE ISNULL([EREC_HataridosFeladatok].[Memo],'0') = '0'
			UNION ALL
			SELECT EREC_HataridosFeladatok.Id FROM EREC_HataridosFeladatok
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId) as CsoportTagokAllFelhasznalo ON CsoportTagokAllFelhasznalo.Id = EREC_HataridosFeladatok.Felhasznalo_Id_Kiado
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId) as CsoportTagokAllCsoport ON CsoportTagokAllCsoport.Id = EREC_HataridosFeladatok.Csoport_Id_Kiado
			WHERE ISNULL([EREC_HataridosFeladatok].[Memo],'0') = '0'
			UNION ALL
			SELECT EREC_HataridosFeladatok.Id FROM EREC_HataridosFeladatok
			WHERE EREC_HataridosFeladatok.Tipus = 'M' AND ISNULL([EREC_HataridosFeladatok].[Memo],'0') = '0'
			--Memo
			UNION ALL
			SELECT EREC_HataridosFeladatok.Id FROM EREC_HataridosFeladatok
			WHERE ISNULL([EREC_HataridosFeladatok].[Memo],'0') = '1' AND [EREC_HataridosFeladatok].[Letrehozo_id] = @FelhasznaloId
)