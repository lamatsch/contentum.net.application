﻿CREATE FUNCTION [dbo].[fn_GetAllSubCsoportByCsoportId]
	(	@CsoportId		uniqueidentifier
	)
RETURNS TABLE
AS
RETURN
(
	WITH temp AS
	(
		SELECT KRT_CsoportTagok.Csoport_Id AS Id
			FROM KRT_CsoportTagok
			WHERE KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId
				AND Tipus IS NOT NULL
				AND GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
				
		UNION ALL
		
		SELECT KRT_CsoportTagok.Csoport_Id_Jogalany
			FROM KRT_CsoportTagok
				INNER JOIN temp ON temp.Id = KRT_CsoportTagok.Csoport_Id
			WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
				AND KRT_CsoportTagok.Tipus IS NOT NULL
	)
	SELECT DISTINCT KRT_Csoportok.*
		FROM KRT_Csoportok
			INNER JOIN temp ON temp.Id = KRT_Csoportok.Id
)