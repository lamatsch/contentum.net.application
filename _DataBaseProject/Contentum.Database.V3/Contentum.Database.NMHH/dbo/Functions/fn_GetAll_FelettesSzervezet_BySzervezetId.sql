﻿CREATE FUNCTION [dbo].[fn_GetAll_FelettesSzervezet_BySzervezetId]
	(	
		@SzervezetId		UNIQUEIDENTIFIER
	)
RETURNS TABLE
AS
RETURN
(
	WITH temp AS
	(
		SELECT @SzervezetId AS Id
				
		UNION ALL
		
		SELECT KRT_CsoportTagok.Csoport_Id AS Id
			FROM KRT_CsoportTagok
				INNER JOIN temp ON temp.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
			WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
				AND KRT_CsoportTagok.Tipus = '1'
	)
	SELECT temp.Id AS Id FROM temp
)