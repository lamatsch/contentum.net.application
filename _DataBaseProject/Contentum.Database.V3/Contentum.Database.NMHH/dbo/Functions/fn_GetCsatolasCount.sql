﻿CREATE FUNCTION [dbo].[fn_GetCsatolasCount]
(	
	@UgyiratId UNIQUEIDENTIFIER
	
)
RETURNS INT
AS
BEGIN

DECLARE @ret INT
              
set @ret = (SELECT COUNT(*) FROM 
	(SELECT Id FROM EREC_UgyiratObjKapcsolatok WHERE EREC_UgyiratObjKapcsolatok.KapcsolatTipus !='07' 
				AND EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt  = @UgyiratId AND EREC_UgyiratObjKapcsolatok.Obj_Type_Kapcsolt= 'EREC_UgyUgyiratok'  
				AND getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege
	UNION
	SELECT Id FROM EREC_UgyiratObjKapcsolatok WHERE EREC_UgyiratObjKapcsolatok.KapcsolatTipus !='07' 
				AND EREC_UgyiratObjKapcsolatok.Obj_Id_Elozmeny  = @UgyiratId AND EREC_UgyiratObjKapcsolatok.Obj_Type_Elozmeny= 'EREC_UgyUgyiratok'  
				AND getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege
	) tmp
) 

RETURN @ret
END