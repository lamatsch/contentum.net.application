﻿CREATE FUNCTION [dbo].[fn_GetCsatolmanyCountNotConfidential]
(	
	@UgyiratId UNIQUEIDENTIFIER,
	@ExecutorUserId UNIQUEIDENTIFIER,
	@FelhasznaloSzervezet_Id UNIQUEIDENTIFIER
)
RETURNS INT
AS
BEGIN

DECLARE @ret INT

	declare @iratok_executor table (Id uniqueidentifier)
	insert into @iratok_executor select Id from dbo.fn_GetAllIrat_ExecutorIsOrzoOrHisLeader(
			@ExecutorUserId,@FelhasznaloSzervezet_Id)

SET @ret =  (SELECT COUNT(*) FROM EREC_IraIratok
               INNER JOIN EREC_Csatolmanyok
				ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
				and (dbo.fn_CheckIrat_IsConfidential(EREC_IraIratok.Id) = 0
				or EREC_IraIratok.Id in (select Id from @iratok_executor))
				WHERE getdate() BETWEEN EREC_Csatolmanyok.ErvKezd AND EREC_Csatolmanyok.ErvVege
				AND EREC_IraIratok.Ugyirat_Id =  @UgyiratId
	        )

RETURN @ret
END