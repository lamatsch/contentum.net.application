create function [dbo].[fn_GetCsoportKod] (
	@CsoportId UNIQUEIDENTIFIER
)
/*
	Feladata, hogy visszaadja az id-val megadott csoport k�dj�t.
 */
returns nvarchar(400)

as
begin
 
DECLARE @kod nvarchar(400)

select @kod = KRT_Csoportok.Kod
	FROM KRT_Csoportok
	WHERE KRT_Csoportok.Id = @CsoportId
      
return @kod

END

--select [dbo].[fn_GetCsoportNev]('d093b485-d55f-4a18-9239-018871887059')


GO


