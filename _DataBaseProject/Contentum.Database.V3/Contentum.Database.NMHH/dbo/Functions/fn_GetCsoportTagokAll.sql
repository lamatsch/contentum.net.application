﻿CREATE FUNCTION [dbo].[fn_GetCsoportTagokAll]
	(	@CsoportId		UNIQUEIDENTIFIER
	)
RETURNS TABLE
AS
RETURN
(
	WITH temp AS
	(
		SELECT KRT_CsoportTagok.Csoport_Id AS Id
			FROM KRT_CsoportTagok
			WHERE KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId
				AND Tipus  = '3'
				AND GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
				
		UNION ALL
		
		SELECT KRT_CsoportTagok.Csoport_Id_Jogalany
			FROM KRT_CsoportTagok
				INNER JOIN temp ON temp.Id = KRT_CsoportTagok.Csoport_Id
			WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
				AND KRT_CsoportTagok.Tipus IS NOT NULL
	)
	SELECT DISTINCT Id FROM temp
	UNION
	SELECT KRT_CsoportTagok.Csoport_Id AS Id
		FROM KRT_CsoportTagok
		WHERE KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId
			AND GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
			AND KRT_CsoportTagok.Tipus IS NOT NULL
	UNION
	SELECT Dolgozok.Csoport_Id_Jogalany AS Id
		FROM KRT_CsoportTagok
			INNER JOIN KRT_Csoportok ON KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id
			INNER JOIN KRT_CsoportTagok AS Dolgozok ON KRT_Csoportok.Id = Dolgozok.Csoport_Id
		WHERE KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId
			AND GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
			AND GETDATE() BETWEEN Dolgozok.ErvKezd AND Dolgozok.ErvVege
			AND ISNULL(KRT_Csoportok.JogosultsagOroklesMod,'0') = '1'
			AND KRT_CsoportTagok.Tipus IS NOT NULL
			AND Dolgozok.Tipus != '1'
	UNION
	SELECT @CsoportId AS Id
)