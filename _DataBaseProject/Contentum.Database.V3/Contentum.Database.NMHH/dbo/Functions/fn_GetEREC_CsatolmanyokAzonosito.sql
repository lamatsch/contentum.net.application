﻿CREATE FUNCTION [dbo].[fn_GetEREC_CsatolmanyokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

 --  select @Return = Leiras from EREC_Csatolmanyok where Id = @Obj_Id;

  select @Return = FajlNev from EREC_Csatolmanyok join Krt_dokumentumok on Erec_csatolmanyok.Dokumentum_Id= Krt_dokumentumok.id  where EREC_Csatolmanyok.Id = @Obj_Id
   
   RETURN @Return;

END