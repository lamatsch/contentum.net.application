﻿CREATE FUNCTION [dbo].[fn_GetEREC_Hatarid_ObjektumokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = EREC_HataridosFeladatok.Leiras from EREC_Hatarid_Objektumok 
	inner join EREC_HataridosFeladatok on EREC_HataridosFeladatok.Id = EREC_Hatarid_Objektumok.HataridosFeladat_Id
	where EREC_Hatarid_Objektumok.Id = @Obj_Id;
   
   RETURN @Return;

END