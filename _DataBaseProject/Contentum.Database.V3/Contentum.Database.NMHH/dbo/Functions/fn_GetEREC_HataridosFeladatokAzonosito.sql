﻿create FUNCTION [dbo].[fn_GetEREC_HataridosFeladatokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(4000)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(4000);

   DECLARE @LeirasMaxLength INT
   SET @LeirasMaxLength = 20

   DECLARE @Tipus NVARCHAR(64)
   DECLARE @Leiras NVARCHAR(max)
   DECLARE @Azonosito NVARCHAR(100)
   DECLARE @ObjTypeNev NVARCHAR(400)
   DECLARE @InditandoFunkcioNev NVARCHAR(100)
   DECLARE @KezelesiFeljegyzesTipusNev NVARCHAR(400)
   DECLARE @FeladatTipusNev NVARCHAR(400)
   DECLARE @Megoldas NVARCHAR(400)
   
   select @Tipus = Tipus,
          @Leiras = Leiras, 
          @Azonosito = Azonosito,
          @ObjTypeNev = dbo.fn_GetObjTypeNev([EREC_HataridosFeladatok].[Obj_type]),
          @InditandoFunkcioNev = (SELECT Nev FROM [KRT_Funkciok] WHERE Id = EREC_HataridosFeladatok.[Funkcio_Id_Inditando]),
          @KezelesiFeljegyzesTipusNev = [dbo].fn_KodtarErtekNeve('FELADAT_ALTIPUS',[EREC_HataridosFeladatok].[Altipus],dbo.fn_GetOrgByFelhasznaloId([EREC_HataridosFeladatok].[Letrehozo_id])),
          @FeladatTipusNev = [dbo].fn_KodtarErtekNeve('FELADAT_TIPUS',[EREC_HataridosFeladatok].[Tipus],dbo.fn_GetOrgByFelhasznaloId([EREC_HataridosFeladatok].[Letrehozo_id])),
          @Megoldas = [Megoldas]
          
   from EREC_HataridosFeladatok where Id = @Obj_Id;
   
   SET @Return = ''
   
   IF (@Tipus IS NOT NULL AND @Tipus != '')
   BEGIN
		IF(@Tipus = 'F')
		BEGIN
			SET @Return = @Return + 'F:'
		END
		
		IF(@Tipus = 'M')
		BEGIN
			SET @Return = @Return + 'M:'
		END
   END
   
   IF (@Leiras IS NOT NULL AND @Leiras != '')
   BEGIN
		IF(LEN(@Leiras) > @LeirasMaxLength)
		BEGIN
			SET @Leiras = SUBSTRING(@Leiras,0,@LeirasMaxLength) + '...'
		END
   		SET @Return = @Return + @Leiras
   END
   
   IF (@InditandoFunkcioNev IS NOT NULL AND @InditandoFunkcioNev != '')
   BEGIN
		IF @Return != '' SET @Return = @Return + ' - '
   		SET @Return = @Return + @InditandoFunkcioNev
   END
   
   IF (@Azonosito IS NOT NULL AND @Azonosito != '')
   BEGIN
        IF @Return != '' SET @Return = @Return + ' - '
   		SET @Return = @Return + @Azonosito
   END
   
   IF (@ObjTypeNev IS NOT NULL AND @ObjTypeNev != '')
   BEGIN
   		SET @Return = @Return + ' (' + @ObjTypeNev + ')'
   END
   
   IF (@Megoldas IS NOT NULL AND @Megoldas != '')
   BEGIN
		IF(LEN(@Megoldas) > @LeirasMaxLength)
		BEGIN
			SET @Megoldas = SUBSTRING(@Megoldas,0,@LeirasMaxLength) + '...'
		END
		IF @Return != '' SET @Return = @Return + ' - '
   		SET @Return = @Return + @Megoldas
   END
   
   RETURN @Return;

END