﻿CREATE FUNCTION [dbo].[fn_GetEREC_IraIktatoKonyvekAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = cast(EREC_IraIktatokonyvek.Ev as nvarchar) + ' / ' + cast(EREC_IraIktatokonyvek.Azonosito as nvarchar)
	from EREC_IraIktatokonyvek where Id = @Obj_Id;
   
   RETURN @Return;

END