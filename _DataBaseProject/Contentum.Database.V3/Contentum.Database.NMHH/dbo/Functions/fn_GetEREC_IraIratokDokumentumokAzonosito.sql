﻿CREATE FUNCTION [dbo].[fn_GetEREC_IraIratokDokumentumokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Leiras from EREC_IraIratokDokumentumok where Id = @Obj_Id;
   
   RETURN @Return;

END