﻿CREATE FUNCTION [dbo].[fn_GetEREC_IraKezbesitesiTetelekAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Azonosito_szoveges from EREC_IraKezbesitesiTetelek where Id = @Obj_Id;
   
   RETURN @Return;

END