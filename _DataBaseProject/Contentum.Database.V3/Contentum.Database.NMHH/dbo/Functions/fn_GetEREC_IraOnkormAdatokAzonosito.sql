﻿CREATE FUNCTION [dbo].[fn_GetEREC_IraOnkormAdatokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = EREC_IraIratok.Targy from EREC_IraOnkormAdatok
		inner join EREC_IraIratok on EREC_IraIratok.Id = EREC_IraOnkormAdatok.IraIratok_Id
	where EREC_IraOnkormAdatok.Id = @Obj_Id;
   
   RETURN @Return;

END