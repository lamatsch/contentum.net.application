﻿CREATE FUNCTION [dbo].[fn_GetEREC_IratAlairokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = dbo.fn_GetEREC_IraIratokAzonosito(EREC_IratAlairok.Obj_Id) + ' - ' + KRT_Csoportok.Nev from EREC_IratAlairok
	LEFT JOIN KRT_Csoportok ON KRT_Csoportok.Id = EREC_IratAlairok.FelhasznaloCsoport_Id_Alairo
	where EREC_IratAlairok.Id = @Obj_Id;
   
   RETURN @Return;

END