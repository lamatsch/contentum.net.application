﻿CREATE FUNCTION [dbo].[fn_GetEREC_IratMetaDefinicioAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = RovidNev from EREC_IratMetaDefinicio where Id = @Obj_Id;
   
   RETURN @Return;

END