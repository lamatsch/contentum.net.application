﻿CREATE FUNCTION [dbo].[fn_GetEREC_Irat_IktatokonyveiAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = KRT_Csoportok.Nev from EREC_Irat_Iktatokonyvei 
		inner join KRT_Csoportok on KRT_Csoportok.Id = EREC_Irat_Iktatokonyvei.Csoport_Id_Iktathat
	where EREC_Irat_Iktatokonyvei.Id = @Obj_Id;
   
   RETURN @Return;

END