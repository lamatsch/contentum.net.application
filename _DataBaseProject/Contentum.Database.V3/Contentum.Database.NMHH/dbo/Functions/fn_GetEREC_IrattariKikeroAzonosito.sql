﻿CREATE FUNCTION [dbo].[fn_GetEREC_IrattariKikeroAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = KulsoAzonositok from EREC_IrattariKikero where Id = @Obj_Id;
   
   RETURN @Return;

END