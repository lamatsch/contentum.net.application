﻿CREATE FUNCTION [dbo].[fn_GetEREC_IrattariTetel_IktatokonyvAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Id from EREC_IrattariTetel_Iktatokonyv where Id = @Obj_Id;
   
   RETURN @Return;

END