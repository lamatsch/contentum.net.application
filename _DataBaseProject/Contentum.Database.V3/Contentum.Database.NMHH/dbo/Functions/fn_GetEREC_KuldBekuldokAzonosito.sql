﻿CREATE FUNCTION [dbo].[fn_GetEREC_KuldBekuldokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = NevSTR from EREC_KuldBekuldok where Id = @Obj_Id;
   
   RETURN @Return;

END