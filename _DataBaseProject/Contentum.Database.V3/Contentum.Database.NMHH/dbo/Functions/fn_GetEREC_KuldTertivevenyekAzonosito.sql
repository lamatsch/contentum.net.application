﻿CREATE FUNCTION [dbo].[fn_GetEREC_KuldTertivevenyekAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = dbo.fn_GetEREC_KuldKuldemenyekAzonosito(Kuldemeny_Id) from EREC_KuldTertivevenyek where Id = @Obj_Id;
   
   RETURN @Return;

END