﻿CREATE FUNCTION [dbo].[fn_GetEREC_Kuldemeny_IratPeldanyaiAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = dbo.fn_GetEREC_KuldKuldemenyekAzonosito(KuldKuldemeny_Id) from EREC_Kuldemeny_IratPeldanyai where Id = @Obj_Id;
   
   RETURN @Return;

END