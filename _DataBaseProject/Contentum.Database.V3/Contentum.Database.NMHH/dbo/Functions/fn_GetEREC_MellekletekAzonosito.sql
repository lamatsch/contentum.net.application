﻿CREATE FUNCTION [dbo].[fn_GetEREC_MellekletekAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Megjegyzes from EREC_Mellekletek where Id = @Obj_Id;
   
   RETURN @Return;

END