﻿CREATE FUNCTION [dbo].[fn_GetEREC_Obj_MetaDefinicioAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = DefinicioTipus from EREC_Obj_MetaDefinicio where Id = @Obj_Id;
   
   RETURN @Return;

END