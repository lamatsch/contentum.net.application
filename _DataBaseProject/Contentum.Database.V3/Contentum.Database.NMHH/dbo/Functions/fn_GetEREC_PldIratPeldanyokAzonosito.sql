﻿CREATE FUNCTION [dbo].[fn_GetEREC_PldIratPeldanyokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Azonosito from EREC_PldIratPeldanyok with (nolock) where Id = @Obj_Id;
   
   RETURN @Return;

END