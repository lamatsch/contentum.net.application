﻿CREATE FUNCTION [dbo].[fn_GetEREC_SzamlakAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = SzamlaSorszam from EREC_Szamlak where Id = @Obj_Id;
   
   RETURN @Return;

END