﻿CREATE FUNCTION [dbo].[fn_GetEREC_UgyiratObjKapcsolatokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Leiras from EREC_UgyiratObjKapcsolatok where Id = @Obj_Id;
   
   RETURN @Return;

END