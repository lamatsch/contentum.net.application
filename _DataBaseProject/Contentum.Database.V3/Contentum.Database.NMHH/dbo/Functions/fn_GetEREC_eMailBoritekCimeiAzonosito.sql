﻿CREATE FUNCTION [dbo].[fn_GetEREC_eMailBoritekCimeiAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = KRT_Partnerek.Nev from EREC_eMailBoritekCimei 
		inner join KRT_Partnerek on KRT_Partnerek.Id = EREC_eMailBoritekCimei.Partner_Id
		where EREC_eMailBoritekCimei.Id = @Obj_Id;
   
   RETURN @Return;

END