﻿CREATE FUNCTION [dbo].[fn_GetEREC_eMailBoritekokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Targy from EREC_eMailBoritekok where Id = @Obj_Id;
   
   RETURN @Return;

END