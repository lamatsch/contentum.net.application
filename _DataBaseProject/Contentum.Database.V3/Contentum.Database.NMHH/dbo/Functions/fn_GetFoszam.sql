﻿CREATE FUNCTION [dbo].[fn_GetFoszam]
(	
	@UgyiratId uniqueidentifier
	
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400)

 SET @ret = 
              (SELECT EREC_UgyUgyiratok.Azonosito
		      FROM  EREC_UgyUgyiratok as EREC_UgyUgyiratok			  
			  WHERE EREC_UgyUgyiratok.Id = @UgyiratId )

RETURN @ret
END