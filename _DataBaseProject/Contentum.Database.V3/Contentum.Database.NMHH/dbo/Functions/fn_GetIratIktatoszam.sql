﻿CREATE FUNCTION [dbo].[fn_GetIratIktatoszam]
(	
	@IratId uniqueidentifier
	
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400)

 SET @ret = 
          (SELECT EREC_IraIratok.[Azonosito]
		   FROM EREC_IraIratok as EREC_IraIratok						
		   WHERE EREC_IraIratok.Id = @IratId
		  )

RETURN @ret
END