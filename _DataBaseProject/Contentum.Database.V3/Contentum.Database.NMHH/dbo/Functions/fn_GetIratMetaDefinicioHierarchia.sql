﻿create function [dbo].[fn_GetIratMetaDefinicioHierarchia] (@IratMetadefinicio_Id uniqueidentifier)
returns @result table ( Id uniqueidentifier
                      , Szint int
                      )
/*
feladata, hogy az irat metadefiníciótól kiindulva meghatározza a szülo iratmetadefiníciókat,
követve az irat metadefiníció hierarchiát
returns: a megfelelo irat metadefiníciók azonosítóit és hierarchiaszintjüket tartalmazó tábla,
         a 0 szinten az eredeti irat metadefiníció található (ha létezik)

-- hívás példa:
select * from dbo.fn_GetIratMetaDefinicioHierarchia('9D23394A-C598-43D4-9483-986E97CDFF66')
       --> '9D23394A-C598-43D4-9483-986E97CDFF66'	0
       --> 'D7F44E71-F406-4461-8E9E-00B9223103F6'	1
       --> 'E6B79BD7-EAC0-412F-A540-6C4C970232CE'	2
       --> 'AF96E631-40C5-4F2E-94A7-0073D39BC594'	3
go

go
select * from dbo.fn_GetIratMetaDefinicioHierarchia('2119E8E0-A3E6-483C-924C-27576F4730F9')
       --> '2119E8E0-A3E6-483C-924C-27576F4730F9', 0
go
select * from dbo.fn_GetIratMetaDefinicioHierarchia('9BA7EE76-279A-4534-82CB-B2FC5E0AE0A1')
       --> <üres tábla>
go
*/
AS
BEGIN

    if @IratMetadefinicio_Id is not null
    begin
        -- hierarchia rekurzióval való lekérése
        with IMD_Hierarchia as
        (
            -- Base case
            select Id, 0 as Szint
            from EREC_IratMetaDefinicio
            where Id = @IratMetaDefinicio_Id
            and getdate() between ErvKezd and ErvVege

            union all

            -- Recursive step
            select imd.Id, imdh.Szint + 1 as Szint
            from EREC_IratMetaDefinicio as imd
            join IMD_Hierarchia as imdh
            on dbo.fn_GetParentIratMetaDefinicioById(imdh.Id) = imd.Id
            and getdate() between imd.ErvKezd and imd.ErvVege
        )

		insert into @result
               select Id, Szint
               from IMD_Hierarchia
               order by Szint ASC;
    end
    return;

END