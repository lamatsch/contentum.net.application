﻿CREATE FUNCTION [dbo].[fn_GetIratPeldanyIktatoszam]
(	
	@IratPeldanyId uniqueidentifier
	
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400)

 SET @ret = 
          (SELECT EREC_PldIratPeldanyok.Azonosito
		   FROM EREC_PldIratPeldanyok as EREC_PldIratPeldanyok					    
		   WHERE EREC_PldIratPeldanyok.Id = @IratPeldanyId
		  )

RETURN @ret
END