﻿
CREATE FUNCTION [dbo].[fn_GetJogosultakFilter]
(
	@Org uniqueidentifier,
	@tablePrefix nvarchar(100)
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400) = ''

DECLARE @Ertek nvarchar(400)

set @Ertek = (select Ertek from KRT_Parameterek where Nev = 'IGNORE_TULAJDONOS_JOGOSULTSAG' and Org = @org)

IF(@Ertek = '1')
	set @ret = replace(' where (krt_jogosultak.Kezi = ''I'' or krt_jogosultak.Tipus != ''T'')','krt_jogosultak', @tablePrefix)

RETURN @ret
END