﻿CREATE FUNCTION [dbo].[fn_GetKRT_BankszamlaszamokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Bankszamlaszam from KRT_Bankszamlaszamok where Id = @Obj_Id;
   
   RETURN @Return;

END