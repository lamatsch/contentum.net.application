﻿CREATE FUNCTION [dbo].[fn_GetKRT_BarkodSavokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = SavKezd + ' - ' + SavVege from KRT_BarkodSavok where Id = @Obj_Id;
   
   RETURN @Return;

END