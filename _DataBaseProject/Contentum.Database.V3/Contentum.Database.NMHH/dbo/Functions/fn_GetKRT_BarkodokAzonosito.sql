﻿CREATE FUNCTION [dbo].[fn_GetKRT_BarkodokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Kod from KRT_Barkodok where Id = @Obj_Id;
   
   RETURN @Return;

END