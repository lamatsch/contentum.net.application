﻿CREATE FUNCTION [dbo].[fn_GetKRT_CimekAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = CASE Tipus
						WHEN '03' THEN CimTobbi
						ELSE ISNULL(Nev,ISNULL(TelepulesNev,'') + ' ' + ISNULL(KozteruletNev,'') + ' ' + ISNULL(Hazszam,''))
					END
					from KRT_Cimek where Id = @Obj_Id;
   
   RETURN @Return;

END