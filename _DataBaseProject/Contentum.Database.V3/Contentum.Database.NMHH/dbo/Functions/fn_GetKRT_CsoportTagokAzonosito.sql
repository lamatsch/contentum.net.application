﻿CREATE FUNCTION [dbo].[fn_GetKRT_CsoportTagokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = cs1.Nev +' / '+ cs2.Nev from KRT_CsoportTagok 
		inner join KRT_Csoportok cs1 on cs1.Id = KRT_CsoportTagok.Csoport_Id
		inner join KRT_Csoportok cs2 on cs2.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
	where KRT_CsoportTagok.Id = @Obj_Id;
   
   RETURN @Return;

END