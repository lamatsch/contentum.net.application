﻿CREATE FUNCTION [dbo].[fn_GetKRT_EsemenyekAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Azonositoja from KRT_Esemenyek where Id = @Obj_Id;
   
   RETURN @Return;

END