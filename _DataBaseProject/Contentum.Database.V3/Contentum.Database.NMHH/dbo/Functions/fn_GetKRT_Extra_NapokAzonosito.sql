﻿CREATE FUNCTION [dbo].[fn_GetKRT_Extra_NapokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Megjegyzes from KRT_Extra_Napok where Id = @Obj_Id;
   
   RETURN @Return;

END