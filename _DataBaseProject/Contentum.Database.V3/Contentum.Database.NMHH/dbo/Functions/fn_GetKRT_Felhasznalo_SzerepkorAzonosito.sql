﻿CREATE FUNCTION [dbo].[fn_GetKRT_Felhasznalo_SzerepkorAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = dbo.fn_GetKRT_FelhasznalokAzonosito(KRT_Felhasznalo_Szerepkor.Felhasznalo_Id) + ' - ' + dbo.fn_GetKRT_SzerepkorokAzonosito(KRT_Felhasznalo_Szerepkor.Szerepkor_Id)
	FROM KRT_Felhasznalo_Szerepkor
	WHERE KRT_Felhasznalo_Szerepkor.Id = @Obj_Id
   
   RETURN @Return;

END