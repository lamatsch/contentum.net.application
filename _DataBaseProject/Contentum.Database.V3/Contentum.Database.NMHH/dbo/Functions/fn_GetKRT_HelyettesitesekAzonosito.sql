﻿CREATE FUNCTION [dbo].[fn_GetKRT_HelyettesitesekAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = cs1.Nev + ' - ' + cs2.Nev from KRT_Helyettesitesek 
		inner join KRT_Csoportok cs1 on cs1.Id = KRT_Helyettesitesek.Felhasznalo_ID_helyettesitett
		inner join KRT_Csoportok cs2 on cs2.Id = KRT_Helyettesitesek.Felhasznalo_ID_helyettesito
	where KRT_Helyettesitesek.Id = @Obj_Id;
   
   RETURN @Return;

END