﻿CREATE FUNCTION [dbo].[fn_GetKRT_KodCsoportokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Nev from KRT_KodCsoportok where Id = @Obj_Id;
   
   RETURN @Return;

END