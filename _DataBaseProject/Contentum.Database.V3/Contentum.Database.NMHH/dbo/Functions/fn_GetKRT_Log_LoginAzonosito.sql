﻿CREATE FUNCTION [dbo].[fn_GetKRT_Log_LoginAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Felhasznalo_Nev from KRT_Log_Login where Felhasznalo_Id = @Obj_Id;
   
   RETURN @Return;

END
