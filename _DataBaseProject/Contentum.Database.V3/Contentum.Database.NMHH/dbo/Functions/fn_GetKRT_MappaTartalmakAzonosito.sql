﻿CREATE FUNCTION [dbo].[fn_GetKRT_MappaTartalmakAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Leiras from KRT_MappaTartalmak where Id = @Obj_Id;
   
   RETURN @Return;

END