﻿CREATE FUNCTION [dbo].[fn_GetKRT_MappaUtakAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = PathFromORG from KRT_MappaUtak where Id = @Obj_Id;
   
   RETURN @Return;

END