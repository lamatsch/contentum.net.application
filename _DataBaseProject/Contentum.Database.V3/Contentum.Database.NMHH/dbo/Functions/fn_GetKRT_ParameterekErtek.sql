﻿
CREATE FUNCTION [dbo].[fn_GetKRT_ParameterekErtek]
(	
	@Nev    nvarchar(400),
	@Org_ID uniqueidentifier
	
)
RETURNS nvarchar(400) 
AS
BEGIN
  RETURN ( select p.Ertek from [dbo].[KRT_Parameterek] p
            where 1 = 1
			  and p.Org = @Org_ID
			  and p.Nev = @Nev
		 )
END