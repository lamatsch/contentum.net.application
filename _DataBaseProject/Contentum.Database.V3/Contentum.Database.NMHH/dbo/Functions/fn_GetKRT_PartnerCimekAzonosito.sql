﻿CREATE FUNCTION [dbo].[fn_GetKRT_PartnerCimekAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = dbo.fn_GetKRT_CimekAzonosito(Cim_Id) from KRT_PartnerCimek where Id = @Obj_Id;
   
   RETURN @Return;

END