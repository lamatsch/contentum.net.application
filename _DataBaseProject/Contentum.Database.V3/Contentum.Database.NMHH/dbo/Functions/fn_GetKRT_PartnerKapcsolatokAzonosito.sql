﻿CREATE FUNCTION [dbo].[fn_GetKRT_PartnerKapcsolatokAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = dbo.fn_GetKRT_PartnerekAzonosito(Partner_Id) + ' - ' + dbo.fn_GetKRT_PartnerekAzonosito(Partner_Id_Kapcsolt) from KRT_PartnerKapcsolatok where Id = @Obj_Id;
   
   RETURN @Return;

END