﻿CREATE FUNCTION [dbo].[fn_GetKRT_PartnerMinositesekAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Minosites from KRT_PartnerMinositesek where Id = @Obj_Id;
   
   RETURN @Return;

END