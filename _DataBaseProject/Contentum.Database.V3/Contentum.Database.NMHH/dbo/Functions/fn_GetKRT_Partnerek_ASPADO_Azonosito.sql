﻿CREATE FUNCTION [dbo].[fn_GetKRT_Partnerek_ASPADO_Azonosito] (

 @Obj_Id uniqueidentifier)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return int;

   select @Return = CONVERT(INT, CONVERT(VARBINARY, LEFT(@Obj_Id,8), 2))
   
   RETURN @Return;

END