﻿CREATE FUNCTION [dbo].[fn_GetKRT_Partnerek_ASPADO_Cimek_Azonosito] (

 @Obj_Id bigint)

RETURNS uniqueidentifier
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return uniqueidentifier;

   select top 1 @Return = Id
   from KRT_Cimek
   where [dbo].[fn_GetKRT_Partnerek_ASPADO_Azonosito](Id)=@Obj_Id
   
   RETURN @Return;

END