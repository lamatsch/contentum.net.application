﻿CREATE FUNCTION [dbo].[fn_GetKRT_SzemelyekAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = dbo.fn_GetKrt_PartnerekAzonosito(Partner_Id) from KRT_Szemelyek where Id = @Obj_Id;
   
   RETURN @Return;

END