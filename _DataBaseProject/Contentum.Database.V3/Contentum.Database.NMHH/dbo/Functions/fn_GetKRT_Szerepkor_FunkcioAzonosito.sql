﻿CREATE FUNCTION [dbo].[fn_GetKRT_Szerepkor_FunkcioAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = dbo.fn_GetKRT_FunkciokAzonosito(Funkcio_Id) + ' / ' + dbo.fn_GetKRT_SzerepkorokAzonosito(Szerepkor_Id) from KRT_Szerepkor_Funkcio where Id = @Obj_Id;
   
   RETURN @Return;

END