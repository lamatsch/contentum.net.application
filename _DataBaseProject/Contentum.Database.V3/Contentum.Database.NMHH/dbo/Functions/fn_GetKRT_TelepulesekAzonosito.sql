﻿CREATE FUNCTION [dbo].[fn_GetKRT_TelepulesekAzonosito] (

 @Obj_Id uniqueidentifier)

RETURNS nvarchar(400)
WITH EXECUTE AS CALLER
AS

BEGIN
DECLARE @Return nvarchar(400);

   select @Return = Nev from KRT_Telepulesek where Id = @Obj_Id;
   
   RETURN @Return;

END