
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetKodtarFuggosegAktiv]    Script Date: 3/13/2019 12:43:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_GetKodtarFuggosegAktiv]( @KodtarFuggosegId uniqueidentifier)
RETURNS @hierarchy TABLE
  (
   VezerloKodTarId UNIQUEIDENTIFIER, 
   FuggoKodTarId UNIQUEIDENTIFIER   
  )
AS
BEGIN
	DECLARE @json NVARCHAR(MAX)

	SET @json = (select adat from KRT_KodtarFuggoseg where id = @KodtarFuggosegId)
		
	INSERT INTO @hierarchy
	select VezerloKodTarId, FuggoKodTarId
	from
		(select parent_id, NAME, StringValue from parseJSON('[' + @json + ']')) as p 
		PIVOT 
		(max(StringValue) FOR NAME IN (VezerloKodTarId,FuggoKodTarId,Aktiv) ) as pvt 
	where pvt.aktiv = 'true'
	order by pvt.parent_id
   RETURN
END
