﻿CREATE FUNCTION [dbo].[fn_GetKuldemenyErkeztetoszam]
(	
	@KuldemenyId uniqueidentifier
	
)
RETURNS nvarchar(400) 
AS
BEGIN

DECLARE @ret Nvarchar(400)

 SET @ret = 
          (SELECT EREC_KuldKuldemenyek.Azonosito
		   FROM EREC_KuldKuldemenyek as EREC_KuldKuldemenyek					    
		   WHERE EREC_KuldKuldemenyek.Id = @KuldemenyId
		  )

RETURN @ret
END