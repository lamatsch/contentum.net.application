﻿CREATE FUNCTION  [dbo].[fn_GetLeader]
  (
	@SzervezetId	uniqueidentifier	-- a felhasználó aktuális szervezetének azonosítója
  )
RETURNS UNIQUEIDENTIFIER
AS
BEGIN
	
	DECLARE @ret UNIQUEIDENTIFIER
	
	select @ret = KRT_CsoportTagok.Csoport_Id_Jogalany
	FROM KRT_CsoportTagok
	where KRT_CsoportTagok.Csoport_Id = @SzervezetId 
	AND KRT_CsoportTagok.Tipus = '3' 
	and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
	
	RETURN @ret

END