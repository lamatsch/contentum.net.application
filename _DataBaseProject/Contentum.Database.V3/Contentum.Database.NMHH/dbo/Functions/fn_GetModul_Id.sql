﻿create function [dbo].[fn_GetModul_Id] (
	@Kod nvarchar(100)
)
/*
	Feladata, hogy visszaadja a kóddal megadott modul id-ját
 */
returns uniqueidentifier

as
begin

DECLARE @Id uniqueidentifier

select top 1 @Id = KRT_Modulok.Id
	FROM KRT_Modulok
	WHERE KRT_Modulok.Kod = @Kod
      
return @Id

END

--select [dbo].[fn_GetObjTipId]('EREC_UgyUgyiratok')