﻿/*SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO*/
CREATE FUNCTION dbo.fn_GetNumeric
	(@strAlphaNumeric VARCHAR(MAX))
RETURNS BIGINT
AS
	BEGIN
	DECLARE @intAlpha BIGINT
	SET @intAlpha = PATINDEX('%[^0-9]%', @strAlphaNumeric)
	BEGIN
		WHILE @intAlpha > 0
		BEGIN
			SET @strAlphaNumeric = STUFF(@strAlphaNumeric, @intAlpha, 1, '' )
			SET @intAlpha = PATINDEX('%[^0-9]%', @strAlphaNumeric )
		END
	END
	RETURN CAST(ISNULL(@strAlphaNumeric, 0) AS BIGINT)
END
GO