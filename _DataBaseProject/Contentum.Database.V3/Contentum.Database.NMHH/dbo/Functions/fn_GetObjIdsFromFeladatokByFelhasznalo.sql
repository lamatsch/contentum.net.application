﻿CREATE FUNCTION [dbo].[fn_GetObjIdsFromFeladatokByFelhasznalo]
    (	@FelhasznaloId		UNIQUEIDENTIFIER
		,@Obj_Type			NVARCHAR(100)
	)
    RETURNS TABLE
    AS
    RETURN
    ( 
             SELECT EREC_HataridosFeladatok.Obj_Id as ID
               FROM EREC_HataridosFeladatok
              WHERE 1 = 1
			    AND EREC_HataridosFeladatok.Felhasznalo_Id_Felelos = @FelhasznaloId
			    AND GETDATE() BETWEEN EREC_HataridosFeladatok.ErvKezd AND EREC_HataridosFeladatok.ErvVege
                AND EREC_HataridosFeladatok.Allapot IN ('0','1','2')
                AND EREC_HataridosFeladatok.Obj_type = @Obj_Type
    )