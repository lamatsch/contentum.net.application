﻿create function [dbo].[fn_GetObjTipId] (
	@Kod nvarchar(100)
)
/*
	Feladata, hogy visszaadja a kóddal megadott objektumtípus id-ját
 */
returns uniqueidentifier

as
begin

DECLARE @Id uniqueidentifier

select top 1 @Id = KRT_ObjTipusok.Id
	FROM KRT_ObjTipusok
	WHERE KRT_ObjTipusok.Kod = @Kod
      
return @Id

END

--select [dbo].[fn_GetObjTipId]('EREC_UgyUgyiratok')