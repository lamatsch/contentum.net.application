﻿CREATE FUNCTION [dbo].[fn_GetObjTipusByTablaNev]
	(	@TablaNev		sysname
	)
RETURNS CHAR(1)
AS
BEGIN
	if (@TablaNev = 'KRT_Orgok') RETURN 'A';
	if (@TablaNev = 'EREC_IraIktatoKonyvek') RETURN 'B';
	if (@TablaNev = 'EREC_UgyUgyiratok') RETURN 'C';
	if (@TablaNev = 'EREC_UgyUgyiratdarabok') RETURN 'D';
	if (@TablaNev = 'EREC_IraIratok') RETURN 'E';
	if (@TablaNev = 'EREC_PldIratPeldanyok') RETURN 'F';
	if (@TablaNev = 'EREC_KuldKuldemenyek') RETURN 'G';

RETURN null;
	
END