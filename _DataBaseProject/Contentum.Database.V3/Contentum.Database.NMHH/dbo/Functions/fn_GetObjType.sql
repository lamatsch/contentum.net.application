﻿create function [dbo].[fn_GetObjType] (
	@Id uniqueidentifier
)
/*
	Feladata, hogy visszaadja az id-val megadott objektum típus kódját
 */
returns nvarchar(100)

as
begin

DECLARE @Kod nvarchar(100)

select top 1 @Kod = KRT_ObjTipusok.Kod
	FROM KRT_ObjTipusok
	WHERE KRT_ObjTipusok.Id = @Id
      
return @Kod

END

--select [dbo].[fn_GetObjType]('A04417A6-54AC-4AF1-9B63-5BABF0203D42')