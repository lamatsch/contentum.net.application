﻿create function [dbo].[fn_GetObjTypeNev] (
	@ObjType NVARCHAR(100)
)
/*
	Feladata, hogy visszaadja az id-val megadott objektum típus kódját
 */
returns nvarchar(100)

as
begin

DECLARE @Nev nvarchar(100)

select @Nev =  (CASE @ObjType
			    WHEN 'EREC_UgyUgyiratok' THEN 'Ügyirat'
			    WHEN 'EREC_UgyUgyiratdarabok' THEN 'Ügyiratdarab'
				WHEN 'EREC_IraIratok' THEN 'Irat'
			    WHEN 'EREC_PldIratpeldanyok' THEN 'Iratpéldány'
			    WHEN 'EREC_KuldKuldemenyek' THEN 'Küldemény'
				ELSE ''
				END)
      
return @Nev

END

--select [dbo].[fn_GetObjType]('A04417A6-54AC-4AF1-9B63-5BABF0203D42')