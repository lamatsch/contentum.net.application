﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetOrgByCsoportId]
(
	@Csoport_Id uniqueidentifier
)
RETURNS uniqueidentifier
AS
BEGIN

	DECLARE @Org uniqueidentifier

	select @Org = KRT_Csoportok.Org
	from 
		KRT_Csoportok
	where
		KRT_Csoportok.Id = @Csoport_Id

	RETURN @Org

END