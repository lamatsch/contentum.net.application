﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetOrgByFelhasznaloId]
(
	@Felhasznalo_Id uniqueidentifier
)
RETURNS uniqueidentifier
AS
BEGIN

	DECLARE @Org uniqueidentifier

	select @Org = KRT_Felhasznalok.Org
	from 
		KRT_Felhasznalok
	where
		KRT_Felhasznalok.Id = @Felhasznalo_Id

	RETURN @Org

END