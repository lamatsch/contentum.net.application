﻿create function [dbo].[fn_GetParentIratMetaDefinicioById] (@IratMetadefinicio_Id uniqueidentifier)
returns uniqueidentifier
/*
feladata, hogy az azonosítóval megadott irat metadefinícióból a rekord tartalma alapján
meghatározza a hierarchiában fölérendelt irat metadefiníció azonosítóját (ha van ilyen)
returns null, ha ilyen irat metadefiníció nem taláható, vagy nincs fölérendelt irat metadefiníció,
        vagy a szülo irat metadefiníció Id

-- hívás példa:
select dbo.fn_GetParentIratMetaDefinicioById('5E71977D-E15F-41FF-ACC0-62C7B5B3C075') --> '2119E8E0-A3E6-483C-924C-27576F4730F9'
go
select dbo.fn_GetParentIratMetaDefinicioById('2119E8E0-A3E6-483C-924C-27576F4730F9') --> null
go
*/
AS
BEGIN
    declare @result uniqueidentifier
    set @result = null
    if @IratMetadefinicio_Id is not null
    begin
       -- 1. lépés: az irat metadefiníció rekord lekérése
        declare @Record_Id uniqueidentifier
        declare @Ugykor_Id uniqueidentifier
              , @Ugytipus nvarchar(64)
              , @EljarasiSzakasz nvarchar(64)
              , @Irattipus nvarchar(64)
              , @Org uniqueidentifier
        select top 1 @Record_Id = imd.Id
             , @Ugykor_Id = imd.Ugykor_Id
             , @Ugytipus = imd.Ugytipus
             , @EljarasiSzakasz = imd.EljarasiSzakasz
             , @Irattipus = imd.Irattipus
             , @Org = imd.Org
        from EREC_IratMetaDefinicio imd
        where imd.Id = @IratMetadefinicio_Id
              and getdate() between imd.ErvKezd and imd.ErvVege
        if @Record_Id is not null
        begin
            select top 1 @result = imd.Id
            from EREC_IratMetaDefinicio imd
            where imd.Ugykor_Id = @Ugykor_Id
                and ((@Irattipus is null and @EljarasiSzakasz is null and @Ugytipus is not null and imd.Ugytipus is null)
                     or
                     (@Irattipus is null and @EljarasiSzakasz is not null and imd.Ugytipus = @Ugytipus and imd.EljarasiSzakasz is null)
                     or
                     (@Irattipus is not null and imd.EljarasiSzakasz = @EljarasiSzakasz and imd.Irattipus is null)
                     )
				and imd.Org = @Org
                and getdate() between imd.ErvKezd and imd.ErvVege
        end

    end
    return @result

END