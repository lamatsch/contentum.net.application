﻿--set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--alter table KRT_Partnerek
--drop column UtolsoHasznalat

--GO

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_GetPartnerUtolsoHasznalat]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
--	DROP FUNCTION [dbo].[fn_GetPartnerUtolsoHasznalat]
--GO


create FUNCTION [dbo].[fn_GetPartnerUtolsoHasznalat]
(
	@PartnerID uniqueidentifier
)
RETURNS datetime
AS
BEGIN
	
	return (select  coalesce (max(ModositasIdo),max(LetrehozasIdo)) from EREC_KuldKuldemenyek with (nolock) where Partner_Id_Bekuldo = @PartnerID)	
END
GO

--alter table KRT_Partnerek
--add [UtolsoHasznalat]  AS ([dbo].[fn_GetPartnerUtolsoHasznalat]([Id]))

--GO

