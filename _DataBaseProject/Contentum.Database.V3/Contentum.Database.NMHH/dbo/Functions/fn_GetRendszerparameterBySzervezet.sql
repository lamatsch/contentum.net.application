﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================

CREATE FUNCTION [dbo].[fn_GetRendszerparameterBySzervezet]
(
	@CsoportId UNIQUEIDENTIFIER,
	@Nev NVARCHAR(400)
)
RETURNS NVARCHAR(400)
/*
	--Error_50202: org nem határozható meg: RAISERROR('[50202]',16,1)
*/
AS
BEGIN

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByCsoportId(@CsoportId)
	if (@Org is null)
	begin
		RETURN 'Error_50202'
	END
	
	DECLARE @Ertek NVARCHAR(400)

	select @Ertek = [KRT_Parameterek].[Ertek]
	from 
		[KRT_Parameterek]
	where
		[KRT_Parameterek].[Org] = @Org
		AND [KRT_Parameterek].[Nev] = @Nev

	RETURN @Ertek

END