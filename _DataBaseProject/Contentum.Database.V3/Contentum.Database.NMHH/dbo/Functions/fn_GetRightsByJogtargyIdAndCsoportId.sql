﻿CREATE FUNCTION [dbo].[fn_GetRightsByJogtargyIdAndCsoportId]
  (	@JogtargyId	uniqueidentifier,	-- a jogtárgy azonosítója
	@CsoportId	uniqueidentifier,	-- a csoport azonosítója
	@Jogszint	char(1)	= 'O'		-- vizsgált jogosultság szintje (I/O)
  )
RETURNS int
AS
/******************************************************************************************************** 
* Visszaadja, hogy a paraméterben megadott csoportnak van-e hozzáfárése a megadott jogtárgyhoz.
* Visszatérési érték: 1, ha van jogosultsága, 0 egyébként.
********************************************************************************************************/
BEGIN
	if @Jogszint != 'I'
	BEGIN
		if exists
		(
		select KRT_ACL.Id as Id									--rendszergazda
			from KRT_ACL
				inner join KRT_CsoportTagok on KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
				inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id and KRT_Csoportok.Tipus = 'R'
			where KRT_ACL.Id = @JogtargyId
		) return 1;

		if exists
		(
			select KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos as Id		--vezeto
				from KRT_CsoportTagok 
					inner join KRT_CsoportTagokAll on KRT_CsoportTagok.Csoport_Id = KRT_CsoportTagokAll.Csoport_Id and KRT_CsoportTagok.Tipus = '3'
					left join KRT_Jogosultak on KRT_Jogosultak.Csoport_Id_Jogalany = KRT_CsoportTagokAll.Csoport_Id_Jogalany
					left join KRT_OBJEKTUM_ACLEK on KRT_OBJEKTUM_ACLEK.Obj_Id = KRT_Jogosultak.Obj_Id
				where KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId 
					AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
					AND KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos = @JogtargyId
		) return 1;

		if exists
		(
			select KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos as Id		--dolgozó, van öröklodés
				from KRT_CsoportTagok 
					inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id and KRT_Csoportok.JogosultsagOroklesMod = '1'
					inner join KRT_Jogosultak KRT_Jogosultak on KRT_Jogosultak.Csoport_Id_Jogalany = KRT_CsoportTagok.Csoport_Id_Jogalany and KRT_Jogosultak.Tipus != 'T'
					left join KRT_OBJEKTUM_ACLEK KRT_OBJEKTUM_ACLEK on KRT_OBJEKTUM_ACLEK.Obj_Id = KRT_Jogosultak.Obj_Id
				where KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId
					AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
					AND KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos = @JogtargyId
		) return 1;

		if exists
		(
			select KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos as Id		--dolgozó, nincs öröklodés
				from KRT_CsoportTagok 
					inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id and isnull(KRT_Csoportok.JogosultsagOroklesMod,'0') != '1'
					inner join KRT_Jogosultak KRT_Jogosultak on KRT_Jogosultak.Csoport_Id_Jogalany = KRT_CsoportTagok.Csoport_Id
					left join KRT_OBJEKTUM_ACLEK KRT_OBJEKTUM_ACLEK on KRT_OBJEKTUM_ACLEK.Obj_Id = KRT_Jogosultak.Obj_Id
				where KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId
					AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
					AND KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos = @JogtargyId
		) return 1;

		if exists
		(
			select KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos as Id		--saját tételek
				from KRT_Jogosultak
					left join KRT_OBJEKTUM_ACLEK KRT_OBJEKTUM_ACLEK on KRT_OBJEKTUM_ACLEK.Obj_Id = KRT_Jogosultak.Obj_Id
				where KRT_Jogosultak.Csoport_Id_Jogalany = @CsoportId
					AND KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos = @JogtargyId
		) return 1;

		return 0;
	END
	ELSE
	BEGIN
		if exists
		(
		select KRT_ACL.Id as Id									--rendszergazda
			from KRT_ACL
				inner join KRT_CsoportTagok on KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
				inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id and KRT_Csoportok.Tipus = 'R'
			where KRT_ACL.Id = @JogtargyId
		) return 1;

		if exists
		(
			select KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos as Id		--vezeto
				from KRT_CsoportTagok 
					inner join KRT_CsoportTagokAll on KRT_CsoportTagok.Csoport_Id = KRT_CsoportTagokAll.Csoport_Id and KRT_CsoportTagok.Tipus = '3'
					left join KRT_Jogosultak on KRT_Jogosultak.Csoport_Id_Jogalany = KRT_CsoportTagokAll.Csoport_Id_Jogalany
					left join KRT_OBJEKTUM_ACLEK on KRT_OBJEKTUM_ACLEK.Obj_Id = KRT_Jogosultak.Obj_Id
				where KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId 
					AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
					AND KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos = @JogtargyId
					AND KRT_OBJEKTUM_ACLEK.OrokolhetoJogszint = @Jogszint
		) return 1;

		if exists
		(
			select KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos as Id		--dolgozó, van öröklodés
				from KRT_CsoportTagok 
					inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id and KRT_Csoportok.JogosultsagOroklesMod = '1'
					inner join KRT_Jogosultak KRT_Jogosultak on KRT_Jogosultak.Csoport_Id_Jogalany = KRT_CsoportTagok.Csoport_Id_Jogalany and KRT_Jogosultak.Tipus != 'T'
					left join KRT_OBJEKTUM_ACLEK KRT_OBJEKTUM_ACLEK on KRT_OBJEKTUM_ACLEK.Obj_Id = KRT_Jogosultak.Obj_Id
				where KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId
					AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
					AND KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos = @JogtargyId
					AND KRT_OBJEKTUM_ACLEK.OrokolhetoJogszint = @Jogszint
		) return 1;

		if exists
		(
			select KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos as Id		--dolgozó, nincs öröklodés
				from KRT_CsoportTagok 
					inner join KRT_Csoportok on KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id and isnull(KRT_Csoportok.JogosultsagOroklesMod,'0') != '1'
					inner join KRT_Jogosultak KRT_Jogosultak on KRT_Jogosultak.Csoport_Id_Jogalany = KRT_CsoportTagok.Csoport_Id
					left join KRT_OBJEKTUM_ACLEK KRT_OBJEKTUM_ACLEK on KRT_OBJEKTUM_ACLEK.Obj_Id = KRT_Jogosultak.Obj_Id
				where KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId
					AND getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
					AND KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos = @JogtargyId
					AND KRT_OBJEKTUM_ACLEK.OrokolhetoJogszint = @Jogszint
		) return 1;

		if exists
		(
			select KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos as Id		--saját tételek
				from KRT_Jogosultak
					left join KRT_OBJEKTUM_ACLEK KRT_OBJEKTUM_ACLEK on KRT_OBJEKTUM_ACLEK.Obj_Id = KRT_Jogosultak.Obj_Id
				where KRT_Jogosultak.Csoport_Id_Jogalany = @CsoportId
					AND KRT_OBJEKTUM_ACLEK.Obj_Id_Orokos = @JogtargyId
					AND KRT_OBJEKTUM_ACLEK.OrokolhetoJogszint = @Jogszint
		) return 1;

		return 0;
	END
	return 0

END