﻿CREATE FUNCTION [dbo].[fn_GetSavosVonalkod_Prefix]
(
	@SzervezetId uniqueidentifier
)
RETURNS nvarchar(100)
AS
BEGIN

-- BLG_1940
Declare @rp_vonalkod_kezeles nvarchar(100)
Declare @rp_vonalkod_prefix  nvarchar(100)

SET @rp_vonalkod_prefix =''

SELECT @rp_vonalkod_kezeles = dbo.fn_GetRendszerparameterBySzervezet(@SzervezetId,'VONALKOD_KEZELES')  

if @rp_vonalkod_kezeles = 'SAVOS'
	SELECT @rp_vonalkod_prefix = dbo.fn_GetRendszerparameterBySzervezet(@SzervezetId,'VONALKOD_PREFIX') 

RETURN @rp_vonalkod_prefix
END
