﻿CREATE FUNCTION [dbo].[fn_GetSzereltCount]
(	
	@UgyiratId UNIQUEIDENTIFIER,
	@RegirendszerIktatoszam NVARCHAR(100)
	
)
RETURNS INT
AS
BEGIN

DECLARE @ret INT

DECLARE @regi INT

IF @RegirendszerIktatoszam IS NULL
BEGIN
	SET @regi = 0
END
ELSE
BEGIN
	IF(@RegirendszerIktatoszam = 'EREC_UgyiratObjKapcsolatok')
	BEGIN
		SET @regi = (SELECT COUNT(*) FROM EREC_UgyiratObjKapcsolatok WHERE EREC_UgyiratObjKapcsolatok.KapcsolatTipus = '07' AND EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt  = @UgyiratId AND getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege)
	END
	ELSE
	BEGIN
		SET @regi = 1
	END
END

 SET @ret =  @regi + (SELECT COUNT(*) FROM EREC_UgyUgyiratok as EREC_UgyUgyiratok_Szereltek 
				WHERE EREC_UgyUgyiratok_Szereltek.UgyUgyirat_Id_Szulo = @UgyiratId)
              

RETURN @ret
END