﻿CREATE function [dbo].[fn_Get_KuldemenyIratPeldanyai_Tertiveveny] (@Kuldemeny_Id uniqueidentifier, @isCsatolmany char(1))
returns uniqueidentifier
/*
Visszaadja az Iratpéldányhoz tartozó küldeményhez rendelt visszaérkezett Tértivevény azonosítóját
@isCsatolmany = 1, akkor a Csatolmanyok között keresi (scannelt) és a DocumentumId-t adja vissza
@isCsatolmany = 0, akkor csak a melléklet Id-t adja vissza
*/
AS
BEGIN

	declare @Tertiveveny_id uniqueidentifier 
	if @isCsatolmany = '0'
	Begin

		-- Tertivevény (nincs csatolmány)
		Set @Tertiveveny_Id = (select top 1 t.Id from EREC_Mellekletek m
		inner join EREC_KuldTertivevenyek t on t.Kuldemeny_Id=m.KuldKuldemeny_Id 
		left join EREC_KuldKuldemenyek k on k.Id=m.KuldKuldemeny_Id
		where k.PostazasIranya='2' -- Kimeno
		and m.AdathordozoTipus='14' -- Tertiveveny
		and getdate() between m.ErvKezd and m.ErvVege
		and k.Allapot != '90' --Sztornozott
		and k.Id = @Kuldemeny_Id) 
	End
	Else
	Begin
-- Tertivevény-csatolmány
		Set @Tertiveveny_Id =(select top 1 cs.Dokumentum_Id from EREC_Mellekletek m
		join EREC_KuldKuldemenyek k on k.Id=m.KuldKuldemeny_Id
		join EREC_IratelemKapcsolatok kapcs on m.Id = kapcs.Melleklet_Id
		join EREC_Csatolmanyok cs on kapcs.Csatolmany_Id = cs.Id
		where k.PostazasIranya='2' -- Kimeno
		and m.AdathordozoTipus='14' -- Tertiveveny
		and getdate() between m.ErvKezd and m.ErvVege
		and getdate() between cs.ErvKezd and cs.ErvVege
		and k.Allapot != '90' --Sztornozott
		and k.Id = @Kuldemeny_Id ) 
	End

  return @Tertiveveny_Id

END