﻿
CREATE FUNCTION [dbo].[fn_Get_MetadataXML_Flocat]
  (	@Dok_ID uniqueidentifier
  )
RETURNS xml
AS
BEGIN
  declare @FlocatXML xml;
  --- SET @FlocatXML = convert( xml, Null );
  declare @s_xml     nvarchar(MAX);

  ;
  with XMLNAMESPACES( 'http://www.w3.org/1999/xlink' as xlink )
  select @FlocatXML =
         ( select 
		       'simple'                                  as [@xlink:type]    
			  ,'uuid-' + convert( nvarchar(40), d.ID )  as [@ID]               --- d.ID -ra NEM tudok hivatkozni structMap -ban, de NEM is erre kell !!!  --- required; attribute     --- "egyedi uuid azonosító"
			  ,'URL'                                     as [@LOCTYPE]
			  ,'data/' + convert( nvarchar(40), d.ID ) + '_' + d.FajlNev       --- 'A fájlok elé írjuk oda a KRT_Dokumentumok.Id mező értékét, azzal tuti egyedi lesz, (Peti)'
			                                             as [@xlink:href]      --- "Minden fájl, sűrítve kell, hogy bekerüljön a data könyvtárba, szülő mappák nélkül"
			  /*
              ,convert(varchar(4),ik.Ev) + '/' + ik.Iktatohely + '/' + 
			   convert(nvarchar(15),(u.Foszam/1000)*1000) + '_' + convert(nvarchar(15),(((u.Foszam/1000)+1)*1000)-1) + '/' +    /* u.Foszam intervallum */
			   convert(nvarchar(15),u.Foszam) + '/' + convert(nvarchar(15),i.Alszam) + '/' + d.FajlNev 
			                                             as [@xlink:href]      --- 'a fizikai file komplett elérése a CONTENT mappán belül      --- "a fájl helye a SIP fájlban"   
			  */
             from [dbo].KRT_Dokumentumok d 
            inner join [dbo].EREC_Csatolmanyok cs on cs.Dokumentum_ID = d.ID and getdate() between cs.ErvKezd and cs.ErvVege  /* ez jelzi, ha már törölt egy dokumentum */
            inner join [dbo].EREC_IraIratok i on i.ID = cs.IraIrat_ID
			inner join [dbo].EREC_UgyUgyiratok u on u.ID = i.Ugyirat_ID
			inner join [dbo].EREC_IraIktatokonyvek ik on ik.ID = u.IraIktatokonyv_ID
			where d.ID = @Dok_ID
           FOR XML PATH('FLocat') /* ,type */
	     );

  return @FlocatXML;
END
