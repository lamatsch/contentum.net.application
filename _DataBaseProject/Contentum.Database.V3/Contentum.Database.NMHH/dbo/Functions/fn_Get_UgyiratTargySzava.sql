﻿
CREATE FUNCTION [dbo].[fn_Get_UgyiratTargySzava] 
(
	@UgyiratID uniqueidentifier,
	@Targyszo varchar(100),
	@Org uniqueidentifier
)
returns nvarchar(100)

as
begin

DECLARE @nev nvarchar(100)

Set @nev = 
  ( select case when t.ControlTypeDataSource is not null 
                then (select kt.nev from
				            KRT_KodCsoportok kcs          
	                        inner join KRT_KodTarak kt on kcs.kod COLLATE DATABASE_DEFAULT = t.ControlTypeDataSource COLLATE DATABASE_DEFAULT  
							and kt.kodcsoport_id = kcs.id and kt.Kod COLLATE DATABASE_DEFAULT = ot.Ertek COLLATE DATABASE_DEFAULT
	                  and kt.Org=@Org )
				else ot.Ertek end
      from EREC_UgyUgyiratok i 
	 inner join EREC_ObjektumTargyszavai ot on ot.obj_id = i.id and getdate() between ot.ErvKezd and ot.ErvVege
	 inner join EREC_Obj_MetaAdatai oma on oma.id = ot.obj_metaadatai_id and getdate() between oma.ErvKezd and oma.ErvVege
	 inner join EREC_Targyszavak t on t.id = oma.Targyszavak_id and t.TargySzavak = @Targyszo
	        and t.Org=@Org and getdate() between t.ErvKezd and t.ErvVege
	 where i.ID = @UgyiratID
  )
      
   return @nev

end
