﻿/*
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
*/
create function [dbo].[fn_IratHatasaTipusa] (
	@Kod nvarchar(64)
)
 /*
	Irat hatasa tipus lekérése - Start, Stop, Pause
 */
returns nvarchar(100)

as
begin
	IF (@Kod IS NULL OR @Kod = '')
		RETURN NULL	

	IF (@Kod LIKE '0_%' )
		RETURN @Kod
	
	DECLARE @egyeb nvarchar(MAX)

	SELECT TOP 1 @egyeb = KT.Egyeb
	FROM   KRT_KODTARAK KT
	LEFT JOIN  dbo.KRT_KODCSOPORTOK KCS ON KT.KodCsoport_Id = KCS.Id
	WHERE KCS.Kod = 'IRAT_HATASA_UGYINTEZESRE'
		AND KT.Kod = @Kod
    
	----INFO: A [ helyett kellett a _ jel
	IF @egyeb IS NULL
		RETURN NULL
	ELSE IF (@egyeb LIKE '{"tipusok":_"START%')
		RETURN 'START'
	ELSE IF (@egyeb LIKE '{"tipusok":_"STOP%')
		RETURN 'STOP'
	ELSE IF (@egyeb LIKE '{"tipusok":_"PAUSE%')
		RETURN 'PAUSE'
	ELSE IF (@egyeb LIKE '{"tipusok":_"ON_PROGRESS%')
		RETURN 'ON_PROGRESS'
	ELSE IF (@egyeb LIKE '{"tipusok":_"NONE%')
		RETURN 'NONE'
	ELSE
		RETURN NULL
	
	RETURN NULL
end