﻿CREATE FUNCTION [dbo].[fn_IsAdmin]
  (	@CsoportId	uniqueidentifier	-- a csoport azonosítója
  )
RETURNS int
AS
BEGIN
	IF EXISTS
		(
			SELECT 1
				FROM KRT_CsoportTagok
					INNER JOIN KRT_Csoportok ON KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id
				WHERE KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId
					AND GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
					AND GETDATE() BETWEEN KRT_Csoportok.ErvKezd AND KRT_Csoportok.ErvVege
					AND KRT_Csoportok.Tipus = 'R'
		)
		RETURN 1;
	RETURN 0;
END