/*
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_IsTechnikaiFelhasznalo]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fn_IsTechnikaiFelhasznalo]
	GO
	SET ANSI_NULLS ON
	GO
	SET QUOTED_IDENTIFIER ON
	GO
*/
CREATE FUNCTION [dbo].[fn_IsTechnikaiFelhasznalo]
  (	@CsoportId	uniqueidentifier	-- a csoport azonosítója
  )
RETURNS int
AS
BEGIN
	IF EXISTS
		(
			SELECT 1
				FROM KRT_CsoportTagok
					INNER JOIN KRT_Csoportok ON KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id
				WHERE KRT_CsoportTagok.Csoport_Id_Jogalany = @CsoportId
					AND GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
					AND GETDATE() BETWEEN KRT_Csoportok.ErvKezd AND KRT_Csoportok.ErvVege
					AND KRT_Csoportok.Tipus = 'T'
		)
		RETURN 1;
	RETURN 0;
END