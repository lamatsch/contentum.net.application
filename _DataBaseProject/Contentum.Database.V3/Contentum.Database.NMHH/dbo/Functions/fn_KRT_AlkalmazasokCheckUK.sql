﻿CREATE FUNCTION [dbo].[fn_KRT_AlkalmazasokCheckUK] (
             @Org uniqueidentifier ,
             @Kod Nvarchar(100) ,
             @Nev Nvarchar(100) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_Alkalmazasok where
         (Org = @Org or (Org is null and @Org is null)) and            
         (Kod = @Kod or (Kod is null and @Kod is null)) and            
         (Nev = @Nev or (Nev is null and @Nev is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END