﻿CREATE FUNCTION [dbo].[fn_KRT_BarkodokCheckUK] (
             @Kod Nvarchar(100) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_Barkodok where
         (Kod = @Kod or (Kod is null and @Kod is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END