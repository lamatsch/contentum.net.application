﻿CREATE FUNCTION [dbo].[fn_KRT_FelhasznalokCheckUK] (
             @Org uniqueidentifier ,
             @UserNev Nvarchar(100) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_Felhasznalok where
         (Org = @Org or (Org is null and @Org is null)) and            
         (UserNev = @UserNev or (UserNev is null and @UserNev is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END