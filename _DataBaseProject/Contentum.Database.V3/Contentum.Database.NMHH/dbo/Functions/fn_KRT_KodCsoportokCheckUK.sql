﻿CREATE FUNCTION [dbo].[fn_KRT_KodCsoportokCheckUK] (
             @Org uniqueidentifier ,
             @Kod nvarchar(64) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_KodCsoportok where
         (Org = @Org or (Org is null and @Org is null)) and            
         (Kod = @Kod or (Kod is null and @Kod is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END