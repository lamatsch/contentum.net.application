﻿CREATE FUNCTION [dbo].[fn_KRT_KodTarakCheckUK] (
             @Org uniqueidentifier ,
             @KodCsoport_Id uniqueidentifier ,
             @Kod nvarchar(64) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_KodTarak where
         (Org = @Org or (Org is null and @Org is null)) and            
         (KodCsoport_Id = @KodCsoport_Id or (KodCsoport_Id is null and @KodCsoport_Id is null)) and            
         (Kod = @Kod or (Kod is null and @Kod is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END