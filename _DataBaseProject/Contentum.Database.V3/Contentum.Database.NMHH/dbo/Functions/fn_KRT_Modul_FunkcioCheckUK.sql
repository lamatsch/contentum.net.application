﻿CREATE FUNCTION [dbo].[fn_KRT_Modul_FunkcioCheckUK] (
             @Funkcio_Id uniqueidentifier ,
             @Modul_Id uniqueidentifier ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_Modul_Funkcio where
         (Funkcio_Id = @Funkcio_Id or (Funkcio_Id is null and @Funkcio_Id is null)) and            
         (Modul_Id = @Modul_Id or (Modul_Id is null and @Modul_Id is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END