﻿CREATE FUNCTION [dbo].[fn_KRT_ObjTip_TargySzoCheckUK] (
             @TargySzo_Id uniqueidentifier ,
             @Sorszam int ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_ObjTip_TargySzo where
         (TargySzo_Id = @TargySzo_Id or (TargySzo_Id is null and @TargySzo_Id is null)) and            
         (Sorszam = @Sorszam or (Sorszam is null and @Sorszam is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END