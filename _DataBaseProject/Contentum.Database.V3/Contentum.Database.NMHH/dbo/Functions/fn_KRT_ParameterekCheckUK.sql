﻿CREATE FUNCTION [dbo].[fn_KRT_ParameterekCheckUK] (
             @Org uniqueidentifier ,
             @Nev Nvarchar(400) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_Parameterek where
         (Org = @Org or (Org is null and @Org is null)) and            
         (Nev = @Nev or (Nev is null and @Nev is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END