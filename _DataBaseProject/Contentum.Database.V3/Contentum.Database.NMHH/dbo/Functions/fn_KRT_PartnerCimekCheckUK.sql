﻿CREATE FUNCTION [dbo].[fn_KRT_PartnerCimekCheckUK] (
             @Partner_id uniqueidentifier ,
             @Cim_Id uniqueidentifier ,
             @Fajta nvarchar(64) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_PartnerCimek where
         (Partner_id = @Partner_id or (Partner_id is null and @Partner_id is null)) and            
         (Cim_Id = @Cim_Id or (Cim_Id is null and @Cim_Id is null)) and            
         (Fajta = @Fajta or (Fajta is null and @Fajta is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END