﻿CREATE FUNCTION [dbo].[fn_KRT_PartnerKapcsolatokCheckUK] (
             @Partner_id uniqueidentifier ,
             @Partner_id_kapcsolt uniqueidentifier ,
             @Tipus nvarchar(64) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_PartnerKapcsolatok where
         (Partner_id = @Partner_id or (Partner_id is null and @Partner_id is null)) and            
         (Partner_id_kapcsolt = @Partner_id_kapcsolt or (Partner_id_kapcsolt is null and @Partner_id_kapcsolt is null)) and            
         (Tipus = @Tipus or (Tipus is null and @Tipus is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END