﻿CREATE FUNCTION [dbo].[fn_KRT_TelepulesekCheckUK] (
             @Org uniqueidentifier ,
             @IRSZ Nvarchar(10) ,
             @Nev Nvarchar(100) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_Telepulesek where
         (Org = @Org or (Org is null and @Org is null)) and            
         (IRSZ = @IRSZ or (IRSZ is null and @IRSZ is null)) and            
         (Nev = @Nev or (Nev is null and @Nev is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END