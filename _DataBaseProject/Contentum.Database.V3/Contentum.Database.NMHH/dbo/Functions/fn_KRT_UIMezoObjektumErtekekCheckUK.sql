﻿CREATE FUNCTION [dbo].[fn_KRT_UIMezoObjektumErtekekCheckUK] (
             @TemplateTipusNev Nvarchar(100) ,
             @Felhasznalo_Id uniqueidentifier ,
             @Nev Nvarchar(100) ,
 @Id uniqueidentifier , @ErvKezd datetime, @ErvVege datetime)

RETURNS int
WITH EXECUTE AS CALLER
AS

BEGIN
declare @ret int

select @ret = count(Id) from KRT_UIMezoObjektumErtekek where
         (TemplateTipusNev = @TemplateTipusNev or (TemplateTipusNev is null and @TemplateTipusNev is null)) and            
         (Felhasznalo_Id = @Felhasznalo_Id or (Felhasznalo_Id is null and @Felhasznalo_Id is null)) and            
         (Nev = @Nev or (Nev is null and @Nev is null)) and                  
   ErvKezd < @ErvVege
   and ErvVege > @ErvKezd 
   and Id != @Id
   
Return @ret

END