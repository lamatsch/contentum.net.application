﻿CREATE FUNCTION [dbo].[fn_KiadmanyozasElintezettVizsgalat] (@RecordId UNIQUEIDENTIFIER)

RETURNS int
AS
-- ============================================
-- Author:		Németh Kriszta
-- Create date: 2019.02.21
-- Description: azon belső keletkezésű iratoknál, ahol a Kiadmányozni kell és van Kiadmányozó, 
-- ott csak az Elintezett statuszú iratok kellenek
-- Returns : 1 - Megfelel a feltételeknek
--			 0 - Nem felel meg 
-- =============================================
BEGIN	
	
	declare @ret int
	SET @ret = 0
	--if EXISTS(SELECT Top 1 EREC_IratAlairok.id FROM EREC_IratAlairok
	--inner join EREC_IraIratok on EREC_IraIratok.Id = EREC_IratAlairok.Obj_Id
	--where EREC_IratAlairok.Id =@RecordId
	--and (
	--((EREC_IraIratok.KiadmanyozniKell='1' and EREC_IraIratok.Elintezett = '1' and EREC_IratAlairok.AlairoSzerep='1')) 
	--or (EREC_IraIratok.KiadmanyozniKell='0') 
	--))
	--BEGIN
	--	SET @ret= 1
	--END
	if EXISTS(select top 1 EREC_IratAlairok.id FROM EREC_IratAlairok
			inner join EREC_IraIratok on EREC_IraIratok.Id = EREC_IratAlairok.Obj_Id
			where EREC_IratAlairok.Obj_Id = @RecordId 
			and
			(
			(EREC_IraIratok.KiadmanyozniKell='1' and EREC_IraIratok.Elintezett = '1' and EREC_IratAlairok.AlairoSzerep='1') 
			or (EREC_IraIratok.KiadmanyozniKell='0') 
			)
			)
	BEGIN
		SET @ret= 1
	END
	return @ret
	
	
END