﻿create function [dbo].[fn_KodCsoportErtekei] (@KodCsoport nvarchar(64),@Org uniqueidentifier)
/*
	Feladata, hogy lekérdezze a megadott kódcsoporthoz
	tartozó kódtár értékeket és ezek neveit.
 */
returns @Return table
(
	Csoport	nvarchar(64),
	Kod	nvarchar(64),
	Nev nvarchar(64)
)

as
begin

    insert into @Return(Csoport, Kod, Nev)
         select dbo.krt_kodcsoportok.kod,
                dbo.krt_kodtarak.kod,
                dbo.krt_kodtarak.nev
           from dbo.krt_kodcsoportok,
                dbo.krt_kodtarak
          where dbo.krt_kodcsoportok.id = dbo.krt_kodtarak.kodcsoport_id
            and dbo.krt_kodcsoportok.kod = @KodCsoport
			and dbo.krt_kodtarak.Org = @Org
	return;
end

-- select * from [dbo].[fn_KodCsoportErtekei]('EMAILCIMCIM_TIPUS')