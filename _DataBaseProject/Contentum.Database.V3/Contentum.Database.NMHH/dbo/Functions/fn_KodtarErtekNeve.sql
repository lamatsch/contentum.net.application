﻿create function [dbo].[fn_KodtarErtekNeve] (
	@KodCsoport nvarchar(64),
	@Kod nvarchar(64),
	@Org uniqueidentifier
)
/*
	Feladata, hogy lekérdezze egy kódcsoporton belüli érték nevét.
 */
returns nvarchar(100)

as
begin

DECLARE @nev nvarchar(100)

select @nev = dbo.krt_kodtarak.nev
  from dbo.krt_kodcsoportok with (nolock),
       dbo.krt_kodtarak with (nolock)
 where dbo.krt_kodcsoportok.id = dbo.krt_kodtarak.kodcsoport_id
   and dbo.krt_kodcsoportok.kod = @KodCsoport
   and dbo.krt_kodtarak.kod = @Kod
   and dbo.krt_kodtarak.Org=@Org
   and GETDATE() between dbo.KRT_KodTarak.ErvKezd and dbo.KRT_KodTarak.ErvVege
      
   return @nev

end

-- select [dbo].[fn_KodtarErtekNeve]('EMAILCIMCIM_TIPUS', '1')