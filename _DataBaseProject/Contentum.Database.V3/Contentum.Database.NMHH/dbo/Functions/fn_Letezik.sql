﻿CREATE FUNCTION [dbo].[fn_Letezik]
		(
		@ErvKezd  datetime,
		@ErvVege  datetime,
		@Datumtol datetime,
		@Datumig  datetime
		)
RETURNS char(1) 
AS
BEGIN
  RETURN dbo.fn_Ervenyesseg('LET',@ErvKezd,@ErvVege,@Datumtol,@Datumig)
END
-- select dbo.fn_Letezik(getdate()-1,NULL,getdate()-2,NULL) LET