﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fn_MergeCim]
(	
	-- Add the parameters for the function here
	@Tipus Nvarchar(64),
	@OrszagNev Nvarchar(400),
	@IRSZ Nvarchar(20),
	@TelepulesNev Nvarchar(100),
	@KozteruletNev Nvarchar(100),
	@KozteruletTipusNev Nvarchar(100),
	@Hazszam Nvarchar(100),
	@Hazszamig Nvarchar(100),
	@HazszamBetujel Nvarchar(100),
	@Lepcsohaz Nvarchar(100),
	@Szint Nvarchar(10),
	@Ajto Nvarchar(20),
	@AjtoBetujel Nvarchar(20),
	-- BLG_1347
	@Hrsz Nvarchar(100),
	@CimTobbi Nvarchar(100)
)
RETURNS nvarchar(4000) 
AS
BEGIN
DECLARE @ret Nvarchar(4000)
select @ret =
CASE @Tipus
	WHEN '01' THEN 
	CASE WHEN @OrszagNev is NULL OR @OrszagNev = '' THEN '' ELSE @OrszagNev + ' ' END +
	CASE WHEN @IRSZ is NULL OR @IRSZ = '' THEN '' ELSE @IRSZ + ' ' END +
	CASE WHEN @TelepulesNev is NULL OR @TelepulesNev = '' THEN '' ELSE @TelepulesNev + ' ' END +
	-- BLG_1347
	--CASE WHEN @KozteruletNev is NULL OR @KozteruletNev = '' THEN '' ELSE @KozteruletNev + ' ' END +
	CASE WHEN ((@KozteruletNev is NOT NULL OR @KozteruletNev != '') 
				AND (@KozteruletTipusNev is NULL OR @KozteruletTipusNev = '') 
				AND (@Hazszam is NULL OR @Hazszam = ''))
	THEN
			@KozteruletNev + ' ' +
			CASE WHEN @Hazszamig is NULL OR @Hazszamig = '' THEN '' ELSE '-' + @Hazszamig + ' ' END +		
			CASE WHEN @HazszamBetujel is NULL OR @HazszamBetujel = '' THEN '' ELSE '/' + @HazszamBetujel + ' ' END +
			CASE WHEN @Lepcsohaz is NULL OR @Lepcsohaz = '' THEN '' ELSE @Lepcsohaz + ' lépcsőház ' END +
			CASE WHEN @Szint is NULL OR @Szint = '' THEN '' ELSE @Szint + '. emelet ' END +
			CASE WHEN @Ajto is NULL OR @Ajto = '' THEN '' ELSE 
				CASE WHEN @AjtoBetujel is NULL OR @AjtoBetujel = '' THEN @Ajto +' ajtó ' ELSE @Ajto + '/' + @AjtoBetujel + ' ajtó ' END
			END +
			CASE WHEN @Hrsz is NULL OR @Hrsz='' 
			THEN '' 
			ELSE ' HRSZ. ' + 
				CASE WHEN @Hrsz is NULL OR @Hrsz = '' 
				THEN '' 
				ELSE @Hrsz + ' ' 
				END   
			END 
	ELSE
		--BLG_12131 miatt külön eset
		CASE WHEN ((@KozteruletTipusNev is NOT NULL OR @KozteruletTipusNev != '') 
					AND (@KozteruletNev is NOT NULL OR @KozteruletNev != '') 
					AND (@Hazszam is NULL OR @Hazszam = '') )
		THEN
			@KozteruletNev + ' ' +
			@KozteruletTipusNev + ' ' +
			
		
			CASE WHEN @Hazszam is NULL OR @Hazszam = '' THEN '' ELSE @Hazszam + ' ' END +
			CASE WHEN @Hazszamig is NULL OR @Hazszamig = '' THEN '' ELSE '-' + @Hazszamig + ' ' END +		
			CASE WHEN @HazszamBetujel is NULL OR @HazszamBetujel = '' THEN '' ELSE '/' + @HazszamBetujel + ' ' END +
			CASE WHEN @Lepcsohaz is NULL OR @Lepcsohaz = '' THEN '' ELSE @Lepcsohaz + ' lépcsőház ' END +
			CASE WHEN @Szint is NULL OR @Szint = '' THEN '' ELSE @Szint + '. emelet ' END +
			CASE WHEN @Ajto is NULL OR @Ajto = '' THEN '' ELSE 
				CASE WHEN @AjtoBetujel is NULL OR @AjtoBetujel = '' THEN @Ajto +' ajtó ' ELSE @Ajto + '/' + @AjtoBetujel + ' ajtó ' END
			END +
			CASE WHEN @Hrsz is NULL OR @Hrsz='' 
			THEN '' 
			ELSE ' HRSZ. ' + 
				CASE WHEN @Hrsz is NULL OR @Hrsz = '' 
				THEN '' 
				ELSE @Hrsz + ' ' 
				END   
			END
		ELSE
			

			
			CASE WHEN @KozteruletNev is NULL OR @KozteruletNev = '' THEN '' ELSE @KozteruletNev + ' ' END +
			CASE WHEN @KozteruletTipusNev is NULL OR @KozteruletTipusNev = '' THEN '' ELSE @KozteruletTipusNev + ' ' END +
			CASE WHEN @Hazszam is NULL OR @Hazszam = '' THEN '' ELSE @Hazszam + ' ' END +
			CASE WHEN @Hazszamig is NULL OR @Hazszamig = '' THEN '' ELSE '-' + @Hazszamig + ' ' END +		
	--		CASE WHEN @HazszamBetujel is NULL OR @HazszamBetujel = '' THEN '' ELSE @HazszamBetujel + ' ' END +
			CASE WHEN @HazszamBetujel is NULL OR @HazszamBetujel = '' THEN '' ELSE '/' + @HazszamBetujel + ' ' END +
	--		CASE WHEN @Lepcsohaz is NULL OR @Lepcsohaz = '' THEN '' ELSE @Lepcsohaz + ' ' END +
			CASE WHEN @Lepcsohaz is NULL OR @Lepcsohaz = '' THEN '' ELSE @Lepcsohaz + ' lépcsőház ' END +
	--		CASE WHEN @Szint is NULL OR @Szint = '' THEN '' ELSE @Szint + ' ' END +
			CASE WHEN @Szint is NULL OR @Szint = '' THEN '' ELSE @Szint + '. emelet ' END +
	--		CASE WHEN @Ajto is NULL OR @Ajto = '' THEN '' ELSE @Ajto + ' ' END +
	--		CASE WHEN @AjtoBetujel is NULL OR @AjtoBetujel = '' THEN '' ELSE @AjtoBetujel + ' ' END
			CASE WHEN @Ajto is NULL OR @Ajto = '' THEN '' ELSE 
				CASE WHEN @AjtoBetujel is NULL OR @AjtoBetujel = '' THEN @Ajto +' ajtó ' ELSE @Ajto + '/' + @AjtoBetujel + ' ajtó ' END
			END 
			+
			CASE WHEN @KozteruletNev is NULL OR @KozteruletNev = '' 
				 THEN
					 CASE WHEN @Hrsz is NULL OR @Hrsz='' 
						  THEN '' 
						  ELSE ' HRSZ. ' + 
								CASE WHEN @Hrsz is NULL OR @Hrsz = '' 
									 THEN '' 
									 ELSE @Hrsz + ' ' 
								END   
					 END 
				ELSE ''  
			END 
		END
	END
ELSE
	@CimTobbi
END

	-- Add the SELECT statement with parameter references here
	RETURN @ret
END
