
/****** Object:  UserDefinedFunction [dbo].[fn_MergeCim]    Script Date: 2018.08.24. 13:51:40 ******/

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fn_MergeKozelebbi_Cim]
(	
	-- Add the parameters for the function here
	@Tipus Nvarchar(64),
	@KozteruletNev Nvarchar(100),
	@KozteruletTipusNev Nvarchar(100),
	@Hazszam Nvarchar(100),
	@Hazszamig Nvarchar(100),
	@HazszamBetujel Nvarchar(100),
	@Lepcsohaz Nvarchar(100),
	@Szint Nvarchar(10),
	@Ajto Nvarchar(20),
	@AjtoBetujel Nvarchar(20),
	-- BLG_1347
	@Hrsz Nvarchar(100),
	@CimTobbi Nvarchar(100)
)
RETURNS nvarchar(4000) 
AS
BEGIN
DECLARE @ret Nvarchar(4000)
select @ret =
CASE @Tipus
	WHEN '01' THEN 
		CASE WHEN @KozteruletNev is NULL OR @KozteruletNev = '' THEN
			CASE WHEN @Hrsz is NULL OR @Hrsz='' THEN '' ELSE ' HRSZ. ' + CASE WHEN @Hrsz is NULL OR @Hrsz = '' THEN '' ELSE @Hrsz + ' ' END   END 
		ELSE @KozteruletNev + ' ' END +

		CASE WHEN @KozteruletTipusNev is NULL OR @KozteruletTipusNev = '' THEN '' ELSE @KozteruletTipusNev + ' ' END +
		CASE WHEN @Hazszam is NULL OR @Hazszam = '' THEN '' ELSE @Hazszam + ' ' END +
		CASE WHEN @Hazszamig is NULL OR @Hazszamig = '' THEN '' ELSE '-' + @Hazszamig + ' ' END +		
		CASE WHEN @HazszamBetujel is NULL OR @HazszamBetujel = '' THEN '' ELSE '/' + @HazszamBetujel + ' ' END +
		CASE WHEN @Lepcsohaz is NULL OR @Lepcsohaz = '' THEN '' ELSE @Lepcsohaz + ' lépcsőház ' END +
		CASE WHEN @Szint is NULL OR @Szint = '' THEN '' ELSE @Szint + '. emelet ' END +
		CASE WHEN @Ajto is NULL OR @Ajto = '' THEN '' ELSE 
			CASE WHEN @AjtoBetujel is NULL OR @AjtoBetujel = '' THEN @Ajto +' ajtó ' ELSE @Ajto + '/' + @AjtoBetujel + ' ajtó ' END
		END
ELSE
	@CimTobbi
END

	-- Add the SELECT statement with parameter references here
	RETURN @ret
END