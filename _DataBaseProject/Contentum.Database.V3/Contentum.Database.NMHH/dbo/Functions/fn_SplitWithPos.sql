﻿--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_SplitWithPos]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
--DROP FUNCTION [dbo].[fn_SplitWithPos]
--GO
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

create function [dbo].[fn_SplitWithPos](
 @String nvarchar (max),
 @Delimiter nvarchar (10)
 )
returns @ValueTable table ([Value] nvarchar(max), Pos int)
begin
 declare @NextString nvarchar(max)
 declare @Pos int
 declare @NextPos int
 declare @CommaCheck nvarchar(1)
 declare @ItemPos int
 
 --Initialize
 set @NextString = ''
 set @ItemPos = 0

 set @String = @String + @Delimiter
 
 --Get position of first Comma
 set @Pos = charindex(@Delimiter,@String)
 set @NextPos = 1
 
 --Loop while there is still a comma in the String of levels
 while (@pos <>  0)  
 begin
  set @NextString = substring(@String,1,@Pos - 1)
  set @ItemPos = @ItemPos + 1
 
  insert into @ValueTable ( [Value], [Pos]) Values (@NextString, @ItemPos)
 
  set @String = substring(@String,@pos +1,len(@String))
  
  set @NextPos = @Pos
  set @pos  = charindex(@Delimiter,@String)
 end
 
 return
end