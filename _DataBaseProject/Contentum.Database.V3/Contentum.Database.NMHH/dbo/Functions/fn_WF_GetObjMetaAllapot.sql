﻿CREATE FUNCTION [dbo].[fn_WF_GetObjMetaAllapot]
		(
		 @Obj_Metadefinicio_Id  uniqueidentifier,
		 @MetaObj_Id            uniqueidentifier
		)

RETURNS @Result_MetaAdat  TABLE
        (ObjStateValue_Id     uniqueidentifier,
		 WorkFlow_Id          uniqueidentifier)
AS
/*------------------------------------------------------------------------------
Funkció: Adott paramétereknek megfelelo metadefiníciós adatok lekérdezése.

Paraméterek:
         @Obj_Metadefinicio_Id - metadefiníció
		 @MetaObj_Id           - objektum

Kimenet: metadefiníciós adatok
Példa: 
  select * from dbo.fn_WF_GetObjMetaAllapot
   ('7BF96899-EA8A-DD11-8549-005056C00008','47882F9E-0EB6-DD11-ACF3-005056C00008')
--------------------------------------------------------------------------------*/ 
BEGIN

DECLARE @StateMetaDefinicio_Id uniqueidentifier,
		@ObjStateValue_Id      uniqueidentifier,
		@WorkFlow_Id           uniqueidentifier,
		@sqlcmd                nvarchar(2000),
		@Obj_Id_StateColumn    uniqueidentifier,
		@Obj_StateColumnType   nvarchar(64),
		@ObjStateValue         nvarchar(64)

select @StateMetaDefinicio_Id = convert(uniqueidentifier, NULL),
	   @ObjStateValue_Id      = convert(uniqueidentifier, NULL),
	   @WorkFlow_Id           = convert(uniqueidentifier, NULL)

-- állapotdefiníció lekérdezése
select @StateMetaDefinicio_Id = Id,
	   @Obj_Id_StateColumn    = Obj_Id_StateColumn,
	   @Obj_StateColumnType   = Obj_StateColumnType
  from EREC_StateMetaDefinicio sm
 where Obj_Metadefinicio_Id = @Obj_Metadefinicio_Id
   and dbo.fn_Ervenyes(sm.ErvKezd,sm.ErvVege)='I'

-- állapotérték lekérdezése
if @StateMetaDefinicio_Id is not null
begin

	-- szabványos állapot azonosító lekérdezése
	if  @Obj_StateColumnType = '1'
		set  @ObjStateValue_Id = convert(uniqueidentifier, NULL) -- majd ha lesz ilyen...

	-- metaadat szerinti állapot azonosító lekérdezése
	if  @Obj_StateColumnType = '2'
		select @ObjStateValue_Id = v.Id
		  from EREC_ObjektumTargySzavai t,
			   EREC_ObjStateValue v
		 where t.Obj_Id                = @MetaObj_Id
		   and t.Obj_Metaadatai_Id     = @Obj_Id_StateColumn
		   and v.StateMetaDefinicio_Id = @StateMetaDefinicio_Id
		   and v.ObjStateValue         = t.Ertek
		   and dbo.fn_Ervenyes(t.ErvKezd,t.ErvVege)='I'
		   and dbo.fn_Ervenyes(v.ErvKezd,v.ErvVege)='I'

end

-- workflow lekérdezése
select @WorkFlow_Id = Id
  from EREC_WorkFlow wf
 where Obj_MetaDefinicio_Id = @Obj_Metadefinicio_Id
   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'

insert @Result_MetaAdat
select @ObjStateValue_Id, @WorkFlow_Id

RETURN

END