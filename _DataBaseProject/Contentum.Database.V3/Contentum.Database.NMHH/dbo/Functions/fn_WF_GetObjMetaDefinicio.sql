﻿CREATE FUNCTION [dbo].[fn_WF_GetObjMetaDefinicio]
		(
		 @MetaTablaNev     nvarchar(100),
		 @MetaOszlopNev    nvarchar(100),
		 @ColumnValue      nvarchar(100),
		 @MetaObj_Id       uniqueidentifier
		)

RETURNS @Result_MetaAdat  TABLE
        (Obj_Metadefinicio_Id uniqueidentifier,
         ObjStateValue_Id     uniqueidentifier)
AS
/*------------------------------------------------------------------------------
Funkció: Adott paramétereknek megfelelo metadefiníció lekérdezése.

Paraméterek:
         @MetaTablaNev    - táblanév
         @MetaOszlopNev   - oszloknév
		 @ColumnValue     - oszlopérték
		 @MetaObj_Id      - objektum

Kimenet: metadefiníció azonosítója
Példa: 
  select * from dbo.fn_WF_GetObjMetaDefinicio
   ('EREC_UgyUgyiratok','UgyTipus','KOZGY_ELOTERJ','EBDB4E26-9E63-DD11-9DE3-000F1FFFAE55')
select * from EREC_UgyUgyiratok where UgyTipus = 'KOZGY_ELOTERJ'

--------------------------------------------------------------------------------*/ 
BEGIN

DECLARE @Obj_Metadefinicio_Id  uniqueidentifier,
		@ObjStateValue_Id      uniqueidentifier,
		@StateMetaDefinicio_Id uniqueidentifier,
		@sqlcmd                nvarchar(2000),
		@Obj_Id_StateColumn    uniqueidentifier,
		@Obj_StateColumnType   nvarchar(64),
		@ObjStateValue         nvarchar(64)

select @Obj_Metadefinicio_Id  = convert(uniqueidentifier, NULL),
	   @StateMetaDefinicio_Id = convert(uniqueidentifier, NULL),
	   @ObjStateValue_Id      = convert(uniqueidentifier, NULL)

-- metadefinició lekérdezése
select @Obj_Metadefinicio_Id = md.Id
  from KRT_ObjTipusok ot,
	   KRT_ObjTipusok oc,
	   EREC_Obj_MetaDefinicio md
 where ot.Kod         = @MetaTablaNev
   and oc.Kod         = @MetaOszlopNev
   and ot.Id          = oc.Obj_Id_Szulo
   and ot.Id          = md.Objtip_Id
   and md.ColumnValue = @ColumnValue

-- állapotdefiníció lekérdezése
if @Obj_Metadefinicio_Id is not null
	select @StateMetaDefinicio_Id = Id,
		   @Obj_Id_StateColumn    = Obj_Id_StateColumn,
		   @Obj_StateColumnType   = Obj_StateColumnType
	  from EREC_StateMetaDefinicio
	 where Obj_Metadefinicio_Id = @Obj_Metadefinicio_Id

-- állapotérték lekérdezése
if @StateMetaDefinicio_Id is not null
begin

	-- szabványos állapot azonosító lekérdezése
	if  @Obj_StateColumnType = '1'
		set  @ObjStateValue_Id = convert(uniqueidentifier, NULL) -- majd ha lesz ilyen...

	-- metaadat szerinti állapot azonosító lekérdezése
	if  @Obj_StateColumnType = '2'
		select @ObjStateValue_Id = v.Id
		  from EREC_ObjektumTargySzavai t,
			   EREC_ObjStateValue v
		 where t.Obj_Id                = @MetaObj_Id
		   and t.Obj_Metaadatai_Id     = @Obj_Id_StateColumn
		   and v.StateMetaDefinicio_Id = @StateMetaDefinicio_Id
		   and v.ObjStateValue         = t.Ertek

end

insert @Result_MetaAdat
select @Obj_Metadefinicio_Id, @ObjStateValue_Id
--select m.Id, v.Id from EREC_Obj_Metadefinicio m, EREC_ObjStateValue v
-- where v.ObjStateValue = 'Belso szakmai koordináció' and m.ColumnValue = 'KOZGY_ELOTERJ'

RETURN

END