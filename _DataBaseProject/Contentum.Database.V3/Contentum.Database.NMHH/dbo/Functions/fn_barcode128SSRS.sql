



CREATE function [dbo].[fn_barcode128SSRS]


(
 @myString VARCHAR(255)
)

RETURNS VARCHAR(255)
BEGIN
declare @asciiString varchar(255)
select @asciiString = ' !"#$%&''()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~'
select @asciiString = @asciiString + char(200) -- 0xC8
select @asciiString = @asciiString + char(201) -- 0xC9
select @asciiString = @asciiString + char(202) -- 0xCA
select @asciiString = @asciiString + char(203) -- 0xCB
select @asciiString = @asciiString + char(204) -- 0xCC
select @asciiString = @asciiString + char(205) -- 0xCD
select @asciiString = @asciiString + char(206) -- 0xCE
select @asciiString = @asciiString + char(207) -- 0xCF
-- Define the stop and start characters
declare @stopchar char(1)
declare @startchar char(1)
declare @spacechar char(1)
select @stopchar = char(206) -- 0xCE
select @startchar = char(204) -- 0xCC
select @spacechar = char(194) -- 0xC2

-- Define the final holding place of our output string
declare @finalArray varchar(255)

-- Define the variables that we'll need to be using
declare @checksumTotal int
declare @checksum int
select @checksumTotal = 104;
select @checksum = 0;

-- Start building our output
select @finalArray = @startchar

-- Loop through our input variable and start pulling out stuff
declare @position int
declare @thisChar char(1)
select @position = 1
while @position <= len(@myString)
begin
	select @thisChar = substring(@myString, @position, 1)
	select @checksumTotal = @checksumTotal + (@position * (ascii(@thischar)-32))
	select @finalArray = @finalArray + @thisChar
	select @position = @position + 1
end -- We've gone past the length now

-- Now we need to figure out and add the checksum character
select @checksum = @checksumTotal % 103
if @checksum = 0
	select @finalArray = @finalArray + @spacechar
else
	-- Barcorde array assumes 0 as initial offset so we need to add 1 to checksum
	select @finalArray = @finalArray + substring(@asciiString, @checksum+1, 1)
-- Now we append the stop character
select @finalArray = @finalArray + @stopchar

-- The @final Array represents the barcode encoded string
return @finalArray
END 
 
GO


