﻿create function [dbo].[fn_barkod_check] (@barkod nvarchar(100), @cheksumVektor NVARCHAR(12))
returns integer
/*
ellenorzi, hogy a megadott bárkód megfelel-e az eloírásoknak,
azaz olyan 13-hosszúságú, csupa számjegybol álló string-e,
amelynek az utolsó számjegye egy bizonyos szabálynak eleget tévo cheksum ...

returns integer --  0 = ha OK
                   -1 = ha NULL-string
                   -2 = ha nem 13-hosszúságú string
                   -3 = ha nem csak számjegyekbol álló 13-hosszúságú string
                   -4 = ha cheksum hibás 13-hosszúságú szám

-- hívás példa:
select dbo.fn_barkod_check(NULL)                    --> -1
go
select dbo.fn_barkod_check('')                      --> -2
go
select dbo.fn_barkod_check('1234')                  --> -2
go
select dbo.fn_barkod_check('123456789A122')         --> -3
go
select dbo.fn_barkod_check('1234567890140')         --> 0
go
select dbo.fn_barkod_check('1234567890122')         --> 0
go
select dbo.fn_barkod_check('1234567890123')         --> -4
go

*/
AS
BEGIN

  declare @cs         integer
  declare @ch13       nvarchar(1)
  
  if len(@barkod) <> 13
     return -2

  select @ch13 = substring(@barkod,13,1)

  -- cheksum kalkulálás  
  select @cs = dbo.fn_barkod_checksum_calc( substring(@barkod,1,12), @cheksumVektor )

  if @cs < 0
     return @cs
     
  if @ch13 like '[^0-9]'
     return -3

  if convert(integer, @ch13) <> @cs
     return -4

  return 0

END