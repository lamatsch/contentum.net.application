﻿create function [dbo].[fn_dbcolumn_exists] (@tablename nvarchar(100), @columnname nvarchar(100))
returns integer
/*
feladata , hogy megállapítsa, egy adott táblában az adott nevu oszlop létezik-e
returns 0 = ha az oszlop nem létezik
        1 = ha az oszlop létezik


-- hívás példa:
select dbo.fn_dbcolumn_exists('EREC_IraIratok', 'IratMetaDef_Id') --> 1
go
select dbo.fn_dbcolumn_exists('EREC_UgyUgyiratok', 'NemLetezoOszlop') --> 0
go

*/
AS
BEGIN
    declare @result integer
    set @result = 0
    if @tablename is not null and @columnname is not null
    begin
        if exists (select * from syscolumns
             where id = object_id(@tablename) and name = @columnname)
        set @result = 1
    end
    return @result

END