﻿create function [dbo].[fn_dbcolumn_value_equals] (@obj_id uniqueidentifier, @value nvarchar(100), @tablename nvarchar(100), @columnname nvarchar(100))
returns integer
/*
feladata , hogy megállapítsa, egy adott táblában az adott nevu oszlopban (ha az létezik) az adott érték áll-e egy az @obj_idnek megfelelo rekordban
returns 0 = ha az oszlop nem létezik vagy a rekord nem létezik vagy az érték nem azonos
        1 = ha az oszlop és a rekord létezik és az érték azonos


-- hívás példa:
select dbo.fn_dbcolumn_value_equals('20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3', 'Metro4', 'EREC_IraIratok', 'Note') --> 1
go
select dbo.fn_dbcolumn_value_equals(20b67ba7-f156-46b2-93bf-b7fd2e8c9fa3, 'EREC_UgyUgyiratok', 'NemLetezoOszlop') --> 0
go

*/
AS
BEGIN
    declare @result integer
    declare @result_exists integer
    declare @sqlcmd nvarchar(MAX)
    set @result = 0
    if (select dbo.fn_dbcolumn_exists(@tablename,@columnname)) = 0
        set @result = 0
    else
    begin
        set @sqlcmd = 'select case when exists (select Id from ' + @tablename + ' where Id = ''' + convert(nvarchar(36), @obj_id) + ''' and ' + @columnname + ' = ''' + @value + ''') then 1 else 0'
    --print @sqlcmd
        exec sp_executesql @sqlcmd, N'@result_exists integer OUTPUT', @result_exists OUTPUT

        if @result_exists = 1
            set @result = 1
    end
    return @result

END