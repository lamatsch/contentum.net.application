﻿ALTER FUNCTION [dbo].[fn_isAlairhato] (@RecordId UNIQUEIDENTIFIER, @AlairasVisszautasitasKezeles int)

RETURNS int
AS
-- ============================================
-- Description:	A megadott RecordId-ra az 
--   EREC_IratAlairok táblából vissza adja,
--   hogy mutathatjuk-e mint alairhato  irat
-- Returns : 0 - igen
--			 1 - nem
-- =============================================
BEGIN	
	
	
	DECLARE @rowcount int,
			@AlairasSorrend int,
			@AlairandoObj_Id UNIQUEIDENTIFIER

	-- Kivenni a sorrendet
	select @AlairasSorrend = AlairasSorrend,
	@AlairandoObj_Id = Obj_Id
	 from EREC_IratAlairok
	where Id  = @RecordId
	--Megnézni hogy van e még előzetes visszautasitas
	;with elozetesAlairok as 
	(
		select Id, Allapot, AlairasSorrend
		 from EREC_IratAlairok visszautasitott
		 where Obj_Id         = @AlairandoObj_Id
		   and AlairasSorrend < @AlairasSorrend
		   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'
	)
	,alairt as
	(
		select * from elozetesAlairok
		where Allapot = '2'
	),
	visszautasitott as
	(
		select * from elozetesAlairok
		where Allapot = '3'
		and AlairasSorrend not in (select AlairasSorrend from alairt)
	)
	
	select @rowcount = count(*)
	from visszautasitott		
	
	IF @AlairasVisszautasitasKezeles = 2
	BEGIN
		IF @rowcount > 0
			BEGIN
				return 1
			END
			ELSE 
			BEGIN
				return 0
			END
	END 
	
	select @rowcount = COUNT(*)
		 from EREC_IratAlairok visszautasitott
		 where Obj_Id         = @AlairandoObj_Id
		   and AlairasSorrend < @AlairasSorrend
		   and Allapot = '3'
		   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'

	IF @AlairasVisszautasitasKezeles = 0 and @rowcount > 0
	BEGIN
		return 1
	END 

	return 0





	if @rowcount = 0
	BEGIN
		return 0
	END

	IF @AlairasVisszautasitasKezeles = 0
	BEGIN
		return 1
	END 

	IF @AlairasVisszautasitasKezeles = 1
	BEGIN
		return 0
	END 

	

	return 1
END