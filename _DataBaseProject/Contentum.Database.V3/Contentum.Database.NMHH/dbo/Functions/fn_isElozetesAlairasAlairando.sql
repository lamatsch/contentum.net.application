﻿--set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fn_isElozetesAlairasAlairando]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
--	DROP FUNCTION [dbo].[fn_isElozetesAlairasAlairando]
--GO


CREATE FUNCTION [dbo].[fn_isElozetesAlairasAlairando] (@RecordId UNIQUEIDENTIFIER, @AlairasVisszautasitasKezeles int)

RETURNS int
AS
-- ============================================
-- Author:		Nagy Csaba
-- Create date: 2017.02.08
-- Description:	A megadott RecordId-ra az 
--   EREC_IratAlairok táblából vissza adja,
--   hogy van-e előzetes aláírás, ami aláírandó.
-- Returns : 0 - ha van
--			 1 - ha nincs 
-- =============================================
BEGIN	
	
	
	DECLARE @rowcount int,
			@AlairasSorrend int,
			@AlairandoObj_Id UNIQUEIDENTIFIER

	-- Kivenni a sorrendet
	select @AlairasSorrend = AlairasSorrend,
	@AlairandoObj_Id = Obj_Id
	 from EREC_IratAlairok
	where Id  = @RecordId
	--Megnézni hogy van e még előzetes aláírandó aláírás
	select @rowcount = count(*)
		  from EREC_IratAlairok
		 where Obj_Id         = @AlairandoObj_Id
		   and Allapot        = '1' -- Aláírandó
		   and AlairasSorrend < @AlairasSorrend
		   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'		
	
	if @rowcount > 0		
		return 0
	
	;with elozetesAlairok as 
	(
		select Id, Allapot, AlairasSorrend
		 from EREC_IratAlairok visszautasitott
		 where Obj_Id         = @AlairandoObj_Id
		   and AlairasSorrend < @AlairasSorrend
		   and dbo.fn_Ervenyes(ErvKezd,ErvVege)='I'
	)
	,alairt as
	(
		select * from elozetesAlairok
		where Allapot = '2'
	),
	visszautasitott as
	(
		select * from elozetesAlairok
		where Allapot = '3'
		and AlairasSorrend not in (select AlairasSorrend from alairt)
	)
	
	select @rowcount = count(*)
	from visszautasitott

	if @rowcount > 0 AND @AlairasVisszautasitasKezeles != 1	
		return 0
								
	return 1
END