﻿create function [dbo].[fn_mapcolumnvalue_bymetadefinicio](@Obj_MetaDefinicio_Id uniqueidentifier, @Org uniqueidentifier)
returns nvarchar(100)
/*
feladata , hogy egy adott B2 vagy C2 típusú metadefinícióban, ha ott létezik tábla/oszlop/érték megadás
az oszlopértéket leképezze egy olvasható formátumra, figyelembe véve a definíció típust
returns:
null ha a metadefiníció nem létezik
C2 esetén meghatározza az irat metadefiníció nevét egy további függvényhívással
B2 esetén a KRT_ObjTipusok táblából kikeresi az oszlophoz tartozó kódcsoportot,
   és megpróbálja leképezni a kódot
EREC_Obj_MetaDefinicio.ColumnValue, ha a definíciótípus nem B2 vagy C2,
                                    ha a megadott tábla oszlopa nem létezik,
                                    ha az oszlop ténylegesen létezik, de a KRT_objTipusok táblában nem található
EREC_Obj_MetaDefinicio.ColumnValue ha a leképezés nem sikerül vagy nincs megadva kódcsoport

-- hívás példa:
select dbo.fn_mapcolumnvalue_bymetadefinicio('950B33D2-625C-4E46-B1F2-5C5F67DC883D','450B510A-7CAA-46B0-83E3-18445C0C53A9') --> C 272/01/Osztatlan/Utasítás
go


*/
AS
BEGIN
    declare @result nvarchar(100)

    declare @DefinicioTipus char(2)
    declare @Objtip_Id uniqueidentifier
    declare @Objtip_Id_Column uniqueidentifier
    declare @ColumnValue nvarchar(100)

    if not exists(select Id
                  from EREC_Obj_MetaDefinicio
                  where Id = @Obj_MetaDefinicio_Id)
    begin
        set @result = null
    end
    else
    begin
       select @DefinicioTipus = DefinicioTipus,
              @Objtip_Id = Objtip_Id,
              @Objtip_Id_Column = Objtip_Id_Column,
              @ColumnValue = ColumnValue
       from EREC_Obj_MetaDefinicio
       where Id = @Obj_MetaDefinicio_Id

        set @result = @ColumnValue	-- alapértelmezett visszatérési érték
        
        if (@DefinicioTipus = 'C2')
        begin
            set @result = '<' + Isnull((select dbo.fn_map_IratMetaDefinicio(@ColumnValue, @Org)), '?') + '>'
        end
        else if (@DefinicioTipus = 'B2')
        begin
            declare @tablename nvarchar(100)
            declare @columnname nvarchar(100)
            declare @Kodcsoport_Id uniqueidentifier
            declare @KodErtek nvarchar(100)
            
            set @tablename = (select top 1 Kod from KRT_ObjTipusok where Id = @Objtip_Id)
            set @columnname = (select top 1 Kod from KRT_ObjTipusok where Id = @Objtip_Id_Column)

            if ((select dbo.fn_dbcolumn_exists(@tablename, @columnname)) = 1)
            begin
                set @Kodcsoport_Id = (select Kodcsoport_Id from KRT_ObjTipusok where Id = @Objtip_Id_Column)
                if (@Kodcsoport_Id is not null)
                begin
                     set @KodErtek = (select Nev from KRT_KodTarak where KodCsoport_Id = @KodCsoport_Id and Kod = @ColumnValue and Org=@Org)
                     set @result = '<' + IsNull(@KodErtek, '?') + '>'
                     
                end
            end
        end
    end

    return @result

END