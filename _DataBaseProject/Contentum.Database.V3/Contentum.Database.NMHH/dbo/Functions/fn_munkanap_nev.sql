﻿create function [dbo].[fn_munkanap_nev] (@datum datetime = null)
returns nvarchar(25)
/*
egy adott napnak (@datum) megállapítja a hétközi magyar nevét
ha az adott nap nincs megadva (NULL), akkor az aktuális napot veszi!

-- hívás példa:
select dbo.fn_munkanap_nev('2007.10.19') as "2007.10.19"  -- péntek
go
select dbo.fn_munkanap_nev( getdate() )  as "MA"          -- MA!
go
select dbo.fn_munkanap_nev( NULL )       as "NULL (MA)"   -- ha NULL a megadott nap, akkor annek az aktuális napot vesszük (MA)!
go

-- ez a példa meg csak azért van, hogy lássuk azt, hogy DATEFIRST elállítása sem zavarja a függvényt!!!
set DATEFIRST 3
select dbo.fn_munkanap_nev('2007.10.19') as "2007.10.19", @@datefirst as "DATEFIRST"   -- péntek, 3
set DATEFIRST 7 -- default!
go

*/
AS
BEGIN
  declare @nap_nev   nvarchar(25)
  declare @nap_sorsz integer
  
  -- napra truncate-olás
  select @datum = isnull(@datum,getdate())
  select @datum = convert(datetime,convert(varchar,@datum,102))
    
  select  @nap_sorsz = ( @@datefirst + DATEPART(dw, @datum) ) % 7
  select  @nap_nev   = CASE
                            when @nap_sorsz = 0 then 'szombat'
                            when @nap_sorsz = 1 then 'vasárnap' 
                            when @nap_sorsz = 2 then 'hétfo' 
                            when @nap_sorsz = 3 then 'kedd'
                            when @nap_sorsz = 4 then 'szerda'
                            when @nap_sorsz = 5 then 'csütörtök'
                            when @nap_sorsz = 6 then 'péntek' 
                            else 'rossz dátum' -- ilyen eset amúgy nem lehet!
                       END
/*
megjegyzés:
itt most azt használtuk fel, hogy
akárhogy is van beállítva a DATEFIRTS paraméter, SET DATEFIRST n -nel (n=1,2,..,7)
úgy bármilyen dátum paraméter(@datum) esetén
ha ( @@datefirst + DATEPART(dw, @datum) ) % 7  = 0  --> szombat
                                               = 1  --> vasárnap 
                                               = 2  --> hétfo 
                                               = 3  --> kedd
                                               = 4  --> szerda
                                               = 5  --> csütörtök
                                               = 6  --> péntek

*/
  return @nap_nev

END