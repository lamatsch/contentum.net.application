﻿create function [dbo].[fn_munkanapok_szama] (@datum_tol datetime = null, @datum_ig datetime = null)
returns integer
/*
egy adott idoszakra [@datum_tol - @datum_ig) megállapítja, hogy hány munkanap esik bele
ha az elso nap ill. az utolsó nap nincs megadva (NULL), akkor az aktuális napot veszi helyette!

FIGYELEM! a @datum_ig már nem tartozik az idoszakba, tehát alulról zárt, felülrol nyílt az intervallum ...

returns n    -- az adott idoszakba eso munkanapok száma
        0    -- ha @datum_tol = @datum_ig
        NULL -- ha @datum_tol > @datum_ig

figyeli az extra munkanapokat és munkaszüneti napokat is 
az KRT_EXTRA_NAPOK tábla alapján!

-- hívás példa:
select dbo.fn_munkanapok_szama('2003.01.01','2004.01.01') -- 253
go
select dbo.fn_munkanapok_szama('2004.01.01','2005.01.01') -- 256
go
select dbo.fn_munkanapok_szama('2005.01.01','2006.01.01') -- 255
go
select dbo.fn_munkanapok_szama('2006.01.01','2007.01.01') -- 252
go
select dbo.fn_munkanapok_szama(NULL,NULL)                 -- 0
go
select dbo.fn_munkanapok_szama('2006.01.02','2006.01.01') -- NULL
go

*/
AS
BEGIN
  declare @osszes_napszam         int 
  declare @hetvegi_napszam        int
  declare @extra_napszam          int
  -- work
  declare @tort_het_napszam       int -- az elso hetet vesszük majd törthétnek és ennyi napja lesz!!!
  declare @tort_het_elsonap_sorsz int

  -- napra truncate-olás
  select @datum_tol = isnull(@datum_tol,getdate())
  select @datum_tol = convert(datetime,convert(varchar,@datum_tol,102))
  select @datum_ig  = isnull(@datum_ig,getdate())
  select @datum_ig  = convert(datetime,convert(varchar,@datum_ig ,102))
  
  if @datum_tol > @datum_ig
     return (NULL)   

  if @datum_tol = @datum_ig
     return (0)   
  
  ------
  select @osszes_napszam = datediff(day,@datum_tol,@datum_ig)
  
  ------
  select @hetvegi_napszam = (@osszes_napszam/7)*2 -- plussz a maradékhétbeli esetleges szombat és vasárnap ...
  
  select @tort_het_napszam = @osszes_napszam%7
  select @tort_het_elsonap_sorsz = ( @@datefirst + DATEPART(dw, @datum_tol) ) % 7
  
  -- szombat van a tört héten?
  if (0 between @tort_het_elsonap_sorsz AND @tort_het_elsonap_sorsz + @tort_het_napszam -1) OR
     (7 between @tort_het_elsonap_sorsz AND @tort_het_elsonap_sorsz + @tort_het_napszam -1)
     select @hetvegi_napszam = @hetvegi_napszam + 1
  -- vasárnap van a tört héten?
  if (1 between @tort_het_elsonap_sorsz AND @tort_het_elsonap_sorsz + @tort_het_napszam -1) OR
     (8 between @tort_het_elsonap_sorsz AND @tort_het_elsonap_sorsz + @tort_het_napszam -1)
     select @hetvegi_napszam = @hetvegi_napszam + 1
   
  ------
  select @extra_napszam = sum(jelzo)
    from KRT_EXTRA_NAPOK
   where datum >= @datum_tol
     and datum  < @datum_ig
	and getdate() between KRT_EXTRA_NAPOK.ErvKezd and KRT_EXTRA_NAPOK.ErvVege
  
  select @extra_napszam = isnull(@extra_napszam,0)
  
  ------ a tényleges munkanapok száma 
  return (@osszes_napszam - @hetvegi_napszam + @extra_napszam)

END