/*
--SET ANSI_NULLS ON
--SET QUOTED_IDENTIFIER ON
--GO

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_Munkanapok_SzamaV3]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
--	DROP FUNCTION [dbo].[FN_Munkanapok_SzamaV3]
--GO
*/
-- =============================================
-- Author:	      4IG
-- Create date:   2019.11
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[FN_Munkanapok_SzamaV3] 
(
	 @datum_tol		DATETIME	= NULL, 
	 @datum_ig		DATETIME	= NULL
)
RETURNS INTEGER
/*
	Egy adott idoszakra [@datum_tol - @datum_ig) megállapítja, hogy hány munkanap esik bele
	ha az elso nap ill. az utolsó nap nincs megadva (NULL), akkor az aktuális napot veszi helyette!

	FIGYELEM! a @datum_ig már nem tartozik az idoszakba, tehát alulról zárt, felülrol nyílt az intervallum ...

	returns n    -- az adott idoszakba eso munkanapok száma
			0    -- ha @datum_tol = @datum_ig
			NULL -- ha @datum_tol > @datum_ig

	figyeli az extra munkanapokat és munkaszüneti napokat is 
	az KRT_EXTRA_NAPOK tábla alapján!

	-- hívás példa:
	select dbo.fn_munkanapok_szama3('2003.01.01','2004.01.01') -- 253
	go
*/
AS
BEGIN
  declare @osszes_napszam         int 
  declare @hetvegi_napszam        int
  declare @extra_napszam          int
  declare @extra_szunnap          int
  -- work
  declare @tort_het_napszam       int -- az elso hetet vesszük majd törthétnek és ennyi napja lesz!!!
  declare @tort_het_elsonap_sorsz int

  -- NAPRA TRUNCATE-OLÁS
  select @datum_tol = isnull(@datum_tol,getdate())
  select @datum_tol = convert(datetime,convert(varchar,@datum_tol,102))
  select @datum_ig  = isnull(@datum_ig,getdate())
  select @datum_ig  = convert(datetime,convert(varchar,@datum_ig ,102))
  
  if @datum_tol > @datum_ig
     return (NULL)   

  if @datum_tol = @datum_ig
     return (0)   
  
  ------
  select @osszes_napszam = datediff(day,@datum_tol,@datum_ig)
  
  ------
  select @hetvegi_napszam = (@osszes_napszam/7)*2 -- plussz a maradékhétbeli esetleges szombat és vasárnap ...
  
  select @tort_het_napszam = @osszes_napszam%7
  select @tort_het_elsonap_sorsz = ( @@datefirst + DATEPART(dw, @datum_tol) ) % 7
  
  -- SZOMBAT VAN A TÖRT HÉTEN?
  if (0 between @tort_het_elsonap_sorsz AND @tort_het_elsonap_sorsz + @tort_het_napszam) OR
     (7 between @tort_het_elsonap_sorsz AND @tort_het_elsonap_sorsz + @tort_het_napszam)
     select @hetvegi_napszam = @hetvegi_napszam + 1
  -- VASÁRNAP VAN A TÖRT HÉTEN?
  if (1 between @tort_het_elsonap_sorsz AND @tort_het_elsonap_sorsz + @tort_het_napszam) OR
     (8 between @tort_het_elsonap_sorsz AND @tort_het_elsonap_sorsz + @tort_het_napszam)
     select @hetvegi_napszam = @hetvegi_napszam + 1
   
  ----------------------------------------------------------------------------
  -- EXTRA MUNKANAP
   select @extra_napszam = COUNT(Id)
    from KRT_EXTRA_NAPOK
   where datum >= @datum_tol
     and datum <= @datum_ig
	 and Jelzo = 1 -- RendkivuliMunkanap=1, 
	 and GETDATE() between KRT_EXTRA_NAPOK.ErvKezd and KRT_EXTRA_NAPOK.ErvVege
  
   select @extra_napszam = ISNULL(@extra_napszam,0)
   ----------------------------------------------------------------------------
   -- EXTRA SZUNNAP
   select @extra_szunnap = COUNT(Id)
    from KRT_EXTRA_NAPOK
   where datum >= @datum_tol
     and datum  < @datum_ig
	 and Jelzo = -1 -- MunkaszunetiNap=-1, 
	 and GETDATE() between KRT_EXTRA_NAPOK.ErvKezd and KRT_EXTRA_NAPOK.ErvVege
  
   select @extra_szunnap = ISNULL(@extra_szunnap,0)
   ----------------------------------------------------------------------------
  
  ------ A TÉNYLEGES MUNKANAPOK SZÁMA 
  return (@osszes_napszam - @hetvegi_napszam + @extra_napszam - @extra_szunnap)

  ----------------------------------------------------------------------------
END

