
GO

/****** Object:  StoredProcedure [dbo].[sp_ASPDWH_CallWebService]    Script Date: 4/3/2019 4:34:10 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE procedure [dbo].[sp_ASPDWH_CallWebService]
  @url nvarchar(4000)  
as

begin

 Declare @Object as Int;
    Declare @ResponseText as Varchar(8000);

    Exec sp_OACreate 'MSXML2.XMLHTTP', @Object OUT;    
    Exec sp_OAMethod @Object, 'open', NULL, 'get', @url,'false'    
    Exec sp_OAMethod @Object, 'send'
    Exec sp_OAGetProperty  @Object, 'responseText', @ResponseText OUTPUT
    Select @ResponseText
    Exec sp_OADestroy @Object

end


GO


