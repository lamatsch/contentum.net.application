DROP PROCEDURE [dbo].[sp_ASPDWH_statisztika_statisztika]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_ASPDWH_statisztika_statisztika]
  @startDate datetime,
  @endDate datetime
as

begin

BEGIN TRY

set nocount on

DECLARE @org uniqueidentifier
SET @org = (select top 1 id from KRT_Orgok where ErvVege > GETDATE())

DECLARE @ugykor_azon_valid table
(
	ugykor_azon nvarchar(10)
)

INSERT INTO @ugykor_azon_valid
  (ugykor_azon)
VALUES
  ('A101'),  ('A102'),  ('A103'),  ('A104'),  ('A105'),  ('A106'),  ('A107'),  ('A108'),  ('A109'),  ('A201'),  ('A202'),  ('A203'),  ('A204'),  ('A205'),  ('A206'),
  ('A207'),  ('A208'),  ('A209'),  ('A210'),  ('A211'),  ('A212'),  ('B101'),  ('B102'),  ('B103'),  ('B104'),  ('B105'),  ('B106'),  ('B107'),  ('B108'),  ('B109'),
  ('B110'),  ('B111'),  ('B112'),  ('B113'),  ('B114'),  ('B115'),  ('B116'),  ('B117'),  ('B118'),  ('B119'),  ('B120'),  ('B121'),  ('B122'),  ('B123'),  ('B124'),
  ('B125'),  ('B126'),  ('B127'),  ('C101'),  ('C102'),  ('C103'),  ('C104'),  ('C105'),  ('C106'),  ('C107'),  ('C108'),  ('C109'),  ('C110'),  ('C111'),  ('C112'),
  ('C113'),  ('C114'),  ('C115'),  ('C116'),  ('C117'),  ('C118'),  ('C119'),  ('C120'),  ('C121'),  ('C122'),  ('C123'),  ('C124'),  ('C125'),  ('C126'),  ('C127'),
  ('C128'),  ('C129'),  ('C130'),  ('C131'),  ('C132'),  ('C133'),  ('C134'),  ('C135'),  ('E101'),  ('E102'),  ('E103'),  ('E104'),  ('E105'),  ('E106'),  ('E107'),
  ('E108'),  ('E109'),  ('E110'),  ('E111'),  ('E112'),  ('E113'),  ('E114'),  ('E115'),  ('E116'),  ('E117'),  ('E118'),  ('E201'),  ('E202'),  ('E203'),  ('E204'),
  ('E205'),  ('E206'),  ('E207'),  ('E208'),  ('E209'),  ('E210'),  ('E211'),  ('E212'),  ('E213'),  ('E214'),  ('E215'),  ('E216'),  ('E217'),  ('E218'),  ('E219'),
  ('E220'),  ('E221'),  ('E222'),  ('E223'),  ('E301'),  ('E302'),  ('E303'),  ('E304'),  ('E305'),  ('E306'),  ('E307'),  ('E308'),  ('E309'),  ('E401'),  ('E402'),
  ('E403'),  ('E404'),  ('E405'),  ('E406'),  ('E407'),  ('E408'),  ('E409'),  ('E410'),  ('E411'),  ('E412'),  ('E413'),  ('F101'),  ('F102'),  ('F103'),  ('F104'),
  ('F105'),  ('F106'),  ('F107'),  ('F108'),  ('F109'),  ('F110'),  ('F111'),  ('F112'),  ('F113'),  ('F114'),  ('F115'),  ('F116'),  ('F117'),  ('F118'),  ('F119'),
  ('F120'),  ('F121'),  ('F122'),  ('F123'),  ('F124'),  ('F125'),  ('F126'),  ('F127'),  ('F128'),  ('F129'),  ('F130'),  ('F131'),  ('F132'),  ('G101'),  ('G102'),
  ('G103'),  ('G104'),  ('G105'),  ('G106'),  ('G107'),  ('G108'),  ('G109'),  ('G110'),  ('G111'),  ('G112'),  ('G113'),  ('G114'),  ('G115'),  ('G116'),  ('G117'),
  ('G118'),  ('G119'),  ('G120'),  ('G121'),  ('G122'),  ('G123'),  ('G124'),  ('G125'),  ('H101'),  ('H102'),  ('H103'),  ('H104'),  ('H105'),  ('H106'),  ('H107'),
  ('H201'),  ('H202'),  ('H203'),  ('H204'),  ('H301'),  ('H302'),  ('H303'),  ('H304'),  ('H305'),  ('H306'),  ('H307'),  ('H401'),  ('H402'),  ('H403'),  ('H404'),
  ('H405'),  ('H406'),  ('H501'),  ('H502'),  ('H503'),  ('H504'),  ('H505'),  ('H506'),  ('H507'),  ('H508'),  ('H509'),  ('H510'),  ('H601'),  ('H701'),  ('H702'),
  ('H703'),  ('H704'),  ('H705'),  ('H801'),  ('H802'),  ('H803'),  ('H804'),  ('H805'),  ('H806'),  ('H807'),  ('H808'),  ('H809'),  ('H810'),  ('H811'),  ('H812'),
  ('H813'),  ('H814'),  ('H815'),  ('H816'),  ('H817'),  ('I101'),  ('I102'),  ('I103'),  ('I104'),  ('I105'),  ('I106'),  ('I107'),  ('I108'),  ('I109'),  ('I110'),
  ('I111'),  ('I112'),  ('I113'),  ('J101'),  ('J102'),  ('J103'),  ('J104'),  ('J105'),  ('J106'),  ('J107'),  ('J108'),  ('J109'),  ('J110'),  ('J112'),  ('J113'),
  ('J114'),  ('K101'),  ('K102'),  ('K103'),  ('K104'),  ('K105'),  ('K106'),  ('K107'),  ('L101'),  ('L102'),  ('L103'),  ('L104'),  ('L105'),  ('L106'),  ('L107'),
  ('L108'),  ('L109'),  ('L110'),  ('L111'),  ('L112'),  ('L113'),  ('L114'),  ('L115'),  ('L116'),  ('L117'),  ('M101'),  ('M102'),  ('M103'),  ('M104'),  ('M105'),
  ('M106'),  ('M107'),  ('M108'),  ('M109'),  ('M110'),  ('M111'),  ('M112'),  ('M113'),  ('M114'),  ('M115'),  ('M116'),  ('M117'),  ('M118'),  ('M119'),  ('M120'),
  ('M121'),  ('M122'),  ('M123'),  ('M124'),  ('M125'),  ('M126'),  ('M127'),  ('M128'),  ('M129'),  ('M130'),  ('N101'),  ('N102'),  ('P101'),  ('P102'),  ('P103'),
  ('P104'),  ('P105'),  ('P106'),  ('P107'),  ('P108'),  ('P109'),  ('P110'),  ('P111'),  ('P112'),  ('P113'),  ('P114'),  ('P115'),  ('P116'),  ('P117'),  ('P118'),  
  ('P119'),  ('P120'),  ('P121'),  ('P122'),  ('P123'),  ('P124'),  ('P125'),  ('P126'),  ('P127'),  ('P128'),  ('P129'),  ('P130'),  ('P131'),  ('P132'),  ('P133'),
  ('P134'),  ('P135'),  ('P136'),  ('P137'),  ('P138'),  ('P139'),  ('P140'),  ('P141'),  ('P142'),  ('R101'),  ('R102'),  ('R103'),  ('R104'),  ('R105'),  ('U101'),
  ('U102'),  ('U103'),  ('U104'),  ('U105'),  ('U106'),  ('U107'),  ('U108'),  ('U109'),  ('U110'),  ('U111'),  ('U201'),  ('U202'),  ('U203'),  ('U204'),  ('U205'),
  ('U206'),  ('U207'),  ('U208'),  ('U209'),  ('U210'),  ('U211'),  ('U212'),  ('U213'),  ('U214'),  ('U215'),  ('U216'),  ('U301'),  ('U302'),  ('U303'),  ('U304'),
  ('U305'),  ('U306'),  ('U307'),  ('U308'),  ('U309'),  ('U310'),  ('U311'),  ('U312'),  ('U313'),  ('U314'),  ('U315'),  ('U316'),  ('U317'),  ('U318'),  ('U319'),
  ('U320'),  ('U321'),  ('U322'),  ('U323'),  ('U324'),  ('U325'),  ('U326'),  ('U327'),  ('U328'),  ('U329'),  ('U330'),  ('U331'),  ('U332'),  ('U333'),  ('U334'),
  ('U335'),  ('U336'),  ('U337'),  ('U338'),  ('U339'),  ('U340'),  ('U341'),  ('U342'),  ('U343'),  ('U344'),  ('U345'),  ('U346'),  ('U347'),  ('U348'),  ('U349'),
  ('U350'),  ('U351'),  ('U352'),  ('U353'),  ('U354'),  ('U355'),  ('U356'),  ('U357'),  ('U358'),  ('U359'),  ('U360'),  ('U361'),  ('U362'),  ('U363'),  ('U364'),
  ('U401'),  ('U402'),  ('U403'),  ('U404'),  ('U405'),  ('U406'),  ('U407'),  ('U408'),  ('U409'),  ('U410'),  ('U411'),  ('U412'),  ('U413'),  ('U414'),  ('U415'),
  ('U416'),  ('U501'),  ('U502'),  ('U503'),  ('U504'),  ('U505'),  ('U506'),  ('U507'),  ('U508'),  ('U509'),  ('U510'),  ('U511'),  ('U512'),  ('U513'),  ('U514'),
  ('U515'),  ('U516'),  ('U517'),  ('U518'),  ('U519'),  ('U520'),  ('U521'),  ('U522'),  ('U523'),  ('U524'),  ('U525'),  ('U526'),  ('U601'),  ('U602'),  ('U603'),
  ('U604'),  ('U605'),  ('U606'),  ('U607'),  ('U608'),  ('U609'),  ('U610'),  ('U611'),  ('U612'),  ('U613'),  ('U614'),  ('U615'),  ('U616'),  ('U617'),  ('U618'),
  ('U619'),  ('U620'),  ('U621'),  ('U622'),  ('U623'),  ('U624'),  ('U625'),  ('U626'),  ('U627'),  ('U628'),  ('U629'),  ('U630'),  ('U631'),  ('U632'),  ('X101'),
  ('X102'),  ('X103'),  ('X104'),  ('X105'),  ('X106'),  ('X107'),  ('X108'),  ('X109'),  ('X110'),  ('X111'),  ('X112'),  ('X113'),  ('X114'),  ('X115'),  ('X116'),
  ('X117'),  ('X118'),  ('X201'),  ('X202'),  ('X203'),  ('X204'),  ('X205'),  ('X206'),  ('X207'),  ('X208'),  ('X209'),  ('X210'),  ('X211'),  ('X212'),  ('X213'),
  ('X214'),  ('X215'),  ('X216'),  ('X217'),  ('X218'),  ('X219'),  ('X220'),  ('X221'),  ('X222'),  ('X223'),  ('X224'),  ('X225'),  ('X226'),  ('X227'),  ('X228'),
  ('X229'),  ('X230'),  ('X231'),  ('X232'),  ('X301'),  ('X302')
    

DECLARE @Id_kcs_UgyFajtaja uniqueidentifier
SET @Id_kcs_UgyFajtaja = (
	SELECT Id FROM KRT_Kodcsoportok
	WHERE Kod ='UGY_FAJTAJA'
)

DECLARE @TempTabla table (
	--Id uniqueidentifier,
	Kod nvarchar(20),
	Darab int,
	--Datum datetime,
	UgyFajtaja nvarchar(64),
	DontestHozta nvarchar(64),
	DontesFormaja nvarchar(64),
	UgyintezesHataridore nvarchar(64),
	JogorvoslatiEljarasTipusa nvarchar(64),
	JogorvoslatiDontesTipusa nvarchar(64),
	JogorvoslatiDontestHozta nvarchar(64),
	JogorvoslatiDontesTartalma nvarchar(64),
	JogorvoslatiDontes nvarchar(64)
)

-- 2. Iktatott ügyiratok és új hatósági adatok: HatosagiEllenorzes, MunkaorakSzama, EljarasiKoltseg, KozigazgatasiBirsagMerteke
DECLARE @TempTable_UjAdatok table (
	Kod nvarchar(20),
	Id uniqueidentifier NULL,
	Ugyirat_Id uniqueidentifier,
	UgyFajtaja nvarchar(64),
	HatosagiEllenorzes char(1),
	MunkaorakSzama float,
	EljarasiKoltseg int,
	KozigazgatasiBirsagMerteke int
)

-- 3. 2016-os új hatósági adatok 
DECLARE @TempTable_2016UjAdatok table (
	Kod nvarchar(20),
	Id uniqueidentifier NULL,
	Ugyirat_Id uniqueidentifier,
	UgyFajtaja nvarchar(64),
	SommasEljDontes nvarchar(64),
	NyolcNapBelulNemSommas nvarchar(64),
	FuggoHatalyuHatarozat nvarchar(64),
	FuggoHatHatalybaLepes nvarchar(64),
	FuggoHatalyuVegzes nvarchar(64),
	FuggoVegzesHatalyba nvarchar(64),
	HatAltalVisszafizOsszeg int,
	HatTerheloEljKtsg int,
	FelfuggHatarozat nvarchar(64)
)

-- 4. 2016-os új eljárás számadatok 
DECLARE @TempTable_2016EljSzamaAdatok table (
	Kod nvarchar(20),
	Id uniqueidentifier NULL,
	Ugyirat_Id uniqueidentifier,
	UgyFajtaja nvarchar(64),
	EljSzamaElozofelevrolLezart int, -- előző félévről áthúzódó lezárt
	EljSzamaElozofelevrolFolyamatban int, -- előző félévről áthúzódó folyamatban
	EljSzamaMegismeteltLezart int, -- megismételt lezárt
	EljSzamaMegismeteltFolyamatban int, -- megismételt folyamatban
	EljSzamaTargyfelevbenIndultLezart int, -- tárgyfélévben indult lezárt
	EljSzamaTargyfelevbenIndultFolyamatban int-- tárgyfélévben indult folyamatban
)

DECLARE @Reszletes_Tabla table (
	--Id uniqueidentifier,
	Kod nvarchar(20),
	UgyFajtaja nvarchar(64),
	Donteshozo int, --az összes döntés száma
	Donteshozo1 int, -- képviselőtestület
	Donteshozo2 int, -- bizottság
	Donteshozo4 int, -- (fő)polgármester
	Donteshozo3 int, -- részönkorm. testület
	Donteshozo5 int, -- főjegyző
	Donteshozo6 int, -- 
	Donteshozo7 int, -- társulati tanács (nincs ilyen)
	ErdemiDontes1 int, -- önálló határozat
	ErdemiDontes2 int, -- egyezség jóváhagyás
	ErdemiDontes3 int, -- hatósági bizonyítvány
	ErdemiDontes4 int, -- hatósági szerződések
	Vegzes5 int, -- Ket. 30
	Vegzes6 int, -- Ket. 31
	Vegzes9 int, -- Ket. 31 új
	Vegzes7 int, -- elsőfokú egyéb
	Vegzes8 int, -- végrehajtás
    Hatarido1 int, -- határidőn belül
    Hatarido2 int, -- határidőn túl
	JogorvoslatiDontesTartalma1 int, -- Kijavitott, kicserelt, kiegeszitett
	FellebbezesAlapjan1 int,
	FellebbezesAlapjan int, -- módosított, visszavont
	VegzesKozighivJogorv5 int, -- Végzés, közigazgatási hivatal, helybenhagy 
	VegzesKozighivJogorv6 int, -- Végzés, közigazgatási hivatal, megváltoztat 
	VegzesKozighivJogorv7 int, -- Végzés, közigazgatási hivatal, megsemmisít 
	VegzesKozighivJogorv8 int, -- Végzés, közigazgatási hivatal, megsemmisít és új
	VegzesDekoncentraltJogorv5 int, -- Végzés, dekoncentrált szerv, helybenhagy 
	VegzesDekoncentraltJogorv6 int, -- Végzés, dekoncentrált szerv, megváltoztat 
	VegzesDekoncentraltJogorv7 int, -- Végzés, dekoncentrált szerv, megsemmisít 
	VegzesDekoncentraltJogorv8 int, -- Végzés, dekoncentrált szerv, megsemmisít és új
	VegzesKepviselotestJogorv5 int, -- Végzés, képviselőtestület, helybenhagy 
	VegzesKepviselotestJogorv6 int, -- Végzés, képviselőtestület, megváltoztat 
	VegzesKepviselotestJogorv7 int, -- Végzés, képviselőtestület, megsemmisít 
	VegzesKepviselotestJogorv8 int, -- Végzés, képviselőtestület, megsemmisít és új
	VegzesBirosagJogorv5 int, -- Végzés, bíróság, elutasít 
	VegzesBirosagJogorv6 int, -- Végzés, bíróság, megváltoztat 
	VegzesBirosagJogorv7 int, -- Végzés, bíróság, hatályon kívül 
	VegzesBirosagJogorv8 int, -- Végzés, bíróság, hatályon kívül és új
    ErdemiUjrafelvJogorv5 int, -- Érdemi döntés, elsőfokú újrafelvételi, elutasít
    ErdemiUjrafelvJogorv2 int, -- Érdemi döntés, elsőfokú újrafelvételi, módosít
    ErdemiUjrafelvJogorv3 int, -- Érdemi döntés, elsőfokú újrafelvételi, visszavon
    ErdemiUjrafelvJogorv4 int, -- Érdemi döntés, elsőfokú újrafelvételi, új döntés
    ErdemiMeltanyosJogorv5 int, -- Érdemi döntés, elsőfokú méltányossági, elutasít
    ErdemiMeltanyosJogorv2 int, -- Érdemi döntés, elsőfokú méltányossági, módosít
    ErdemiMeltanyosJogorv3 int, -- Érdemi döntés, elsőfokú méltányossági, visszavon
    ErdemiKepviseloJogorv5 int, -- Érdemi döntés, képviselőtestület, helybenhagy
    ErdemiKepviseloJogorv6 int, -- Érdemi döntés, képviselőtestület, megváltoztat 
    ErdemiKepviseloJogorv7 int, -- Érdemi döntés, képviselőtestület, megsemmisít
    ErdemiKepviseloJogorv8 int, -- Érdemi döntés, képviselőtestület, megsemmisít és új
    ErdemiKozighivJogorv5 int, -- Érdemi döntés, közigazgatási hivatal, helybenhagy
    ErdemiKozighivJogorv6 int, -- Érdemi döntés, közigazgatási hivatal, megváltoztat 
    ErdemiKozighivJogorv7 int, -- Érdemi döntés, közigazgatási hivatal, megsemmisít
    ErdemiKozighivJogorv8 int, -- Érdemi döntés, közigazgatási hivatal, megsemmisít és új
    ErdemiDekoncentraltJogorv5 int, -- Érdemi döntés, dekoncentrált szerv, helybenhagy
    ErdemiDekoncentraltJogorv6 int, -- Érdemi döntés, dekoncentrált szerv, megváltoztat 
    ErdemiDekoncentraltJogorv7 int, -- Érdemi döntés, dekoncentrált szerv, megsemmisít
    ErdemiDekoncentraltJogorv8 int, -- Érdemi döntés, dekoncentrált szerv, megsemmisít és új
	ErdemiBirosagJogorv5 int, -- Érdemi döntés, bíróság, elutasít
    ErdemiBirosagJogorv6 int, -- Érdemi döntés, bíróság, megváltoztat 
    ErdemiBirosagJogorv7 int, -- Érdemi döntés, bíróság, hatályon kívül
    ErdemiBirosagJogorv8 int, -- Érdemi döntés, bíróság, hatályon kívül és új
    HivatalbolModositVisszavon int, -- Hivatalból módosított és visszavont
    HivatalbolFelugyeleti6 int, -- Hivatalból, felügyeleti szerv, megváltoztat
    HivatalbolFelugyeleti7 int, -- Hivatalból, felügyeleti szerv, megsemmisít
    HivatalbolFelugyeleti8 int, -- Hivatalból, felügyeleti szerv, megsemmisít és új
    HatosagiEllenorzesekSzama int, -- hatósági ellenőrzések száma
	MunkaorakSzamaAtlagosan numeric(7,1), -- egy ügyre fordított munkaórák száma
	AtlagosEljarasiKoltseg numeric(9,1), -- egy ügyre jutó eljárási költség
	AtlagosKozigazgatasiBirsag numeric(11,1), -- az eljárásban kiszabott közigazgatási bírság átlagos mértéke 
	EljSzamaElozofelevrolLezartSzama int, -- előző félévről áthúzódó lezárt
	EljSzamaElozofelevrolFolyamatbanSzama int, -- előző félévről áthúzódó folyamatban
	EljSzamaMegismeteltLezartSzama int, -- megismételt lezárt
	EljSzamaMegismeteltFolyamatbanSzama int, -- megismételt folyamatban
	EljSzamaTargyfelevbenIndultLezartSzama int, -- tárgyfélévben indult lezárt
	EljSzamaTargyfelevbenIndultFolyamatbanSzama int, -- tárgyfélévben indult folyamatban
	SommasEljSzama int, -- sommás eljárások száma
	NyolcnaponBelulNemSommasSzama int, -- 8 napon belül nem sommás
	FuggoHatalyuHatarozatSzama int,
	FuggoHatalyuVegzesSzama int,
	FuggoHatalyuHatarozatNemHatalyos int,
	FuggoHatalyuHatarozatHatalyos int,
	FuggoHatalyuDontesNemHatalyos int,
	FuggoHatalyuDontesHatalyos int,
	VisszafizetettOsszeg int,
	HatosagotTerheloEljarasiKoltseg int
)


-- ha WHERE feltetel van, elvesznek az "ervenytelen"
-- oszlopokhoz tartozo darabertekek, ezert ket lepesben
-- szamolunk

INSERT INTO @TempTabla
SELECT it.IrattariTetelszam AS Kod, count(onk.Id) AS Darab, onk.UgyFajtaja, onk.DontestHozta, onk.DontesFormaja, onk.UgyintezesHataridore, onk.JogorvoslatiEljarasTipusa, onk.JogorvoslatiDontesTipusa, onk.JogorvoslatiDontestHozta, onk.JogorvoslatiDontesTartalma, onk.JogorvoslatiDontes
	FROM
	EREC_AgazatiJelek a INNER JOIN EREC_IraIrattariTetelek it
	ON it.AgazatiJel_Id = a.Id and it.Org=@Org
	INNER JOIN (SELECT u.Id, u.IraIrattariTetel_Id, i.UgyFajtaja, i.DontestHozta, i.DontesFormaja, i.UgyintezesHataridore, i.JogorvoslatiEljarasTipusa, i.JogorvoslatiDontesTipusa, i.JogorvoslatiDontestHozta, i.JogorvoslatiDontesTartalma, i.JogorvoslatiDontes
				FROM EREC_UgyUgyiratok u WITH (NOLOCK) 
                INNER JOIN EREC_UgyUgyiratdarabok ud WITH (NOLOCK) 
                ON ud.UgyUgyirat_Id = u.Id
                inner JOIN EREC_IraIratok ii WITH (NOLOCK) 
                ON ii.UgyUgyiratDarab_Id = ud.Id and ii.IktatasDatuma between @startDate AND @endDate
				INNER JOIN VW_EREC_IraOnkormAdatok i WITH (NOLOCK) 
				ON i.IraIratok_Id = ii.Id					
				--WHERE u.LetrehozasIdo between @startDate AND @endDate
				AND u.SztornirozasDat is null
				AND u.Allapot != '90'
				AND i.UgyFajtaja in ('1','2')				
				) AS onk
	ON onk.IraIrattariTetel_Id = it.Id	
	GROUP BY it.IrattariTetelszam, onk.UgyFajtaja, onk.DontestHozta, onk.DontesFormaja, onk.UgyintezesHataridore, onk.JogorvoslatiEljarasTipusa, onk.JogorvoslatiDontesTipusa, onk.JogorvoslatiDontestHozta, onk.JogorvoslatiDontesTartalma, onk.JogorvoslatiDontes
	ORDER BY it.IrattariTetelszam
	
	
INSERT INTO @TempTable_UjAdatok
SELECT it.IrattariTetelszam AS Kod, 
NULL, --i.Id, 
u.Id, i.Ugyfajtaja, i.HatosagiEllenorzes, cast(REPLACE(i.MunkaorakSzama, ',', '.') as float), i.EljarasiKoltseg, i.KozigazgatasiBirsagMerteke
	FROM
	EREC_AgazatiJelek a WITH (NOLOCK) INNER JOIN EREC_IraIrattariTetelek it WITH (NOLOCK) 
	ON it.AgazatiJel_Id = a.Id and it.Org=@Org
    INNER JOIN EREC_UgyUgyiratok u WITH (NOLOCK) 
	on u.IraIrattariTetel_Id = it.Id	
    INNER JOIN EREC_UgyUgyiratdarabok ud WITH (NOLOCK) 
    ON ud.UgyUgyirat_Id = u.Id
    INNER JOIN EREC_IraIratok ii WITH (NOLOCK) 
    ON ii.UgyUgyiratDarab_Id = ud.Id and ii.IktatasDatuma between @startDate AND @endDate                      /* #1603 - 2018.01.25., BogI */
	INNER JOIN VW_EREC_IraOnkormAdatok i WITH (NOLOCK) 
	ON i.IraIratok_Id = ii.Id							
	AND u.SztornirozasDat is null
	AND u.Allapot != '90'
	AND i.UgyFajtaja in ('1','2')
	ORDER BY it.IrattariTetelszam

INSERT INTO @TempTable_2016UjAdatok
SELECT it.IrattariTetelszam AS Kod, 
NULL, --i.Id, 
u.Id, i.UgyFajtaja, i.SommasEljDontes, i.NyolcNapBelulNemSommas, i.FuggoHatalyuHatarozat, i.FuggoHatHatalybaLepes, i.FuggoHatalyuVegzes, i.FuggoVegzesHatalyba, i.HatAltalVisszafizOsszeg, i.HatTerheloEljKtsg, i.FelfuggHatarozat
	FROM
	EREC_AgazatiJelek a WITH (NOLOCK)  INNER JOIN EREC_IraIrattariTetelek it WITH (NOLOCK) 
	ON it.AgazatiJel_Id = a.Id and it.Org=@Org
    INNER JOIN EREC_UgyUgyiratok u WITH (NOLOCK) 
	on u.IraIrattariTetel_Id = it.Id	
    INNER JOIN EREC_UgyUgyiratdarabok ud WITH (NOLOCK) 
    ON ud.UgyUgyirat_Id = u.Id
    INNER JOIN EREC_IraIratok ii WITH (NOLOCK) 
    ON ii.UgyUgyiratDarab_Id = ud.Id and ii.IktatasDatuma between @startDate AND @endDate
	INNER JOIN VW_EREC_IraOnkormAdatok i WITH (NOLOCK) 
	ON i.IraIratok_Id = ii.Id						
	--and u.LetrehozasIdo between @startDate AND @endDate
	AND u.SztornirozasDat is null
	AND u.Allapot != '90'
	AND i.UgyFajtaja  in ('1','2')
	ORDER BY it.IrattariTetelszam

/* --- BUG_1532 - két blokkba szétszedve 2018.01.10., BogI */
INSERT INTO @TempTable_2016EljSzamaAdatok
SELECT it.IrattariTetelszam AS Kod, 
NULL, --i.Id, 
u.Id, i.UgyFajtaja,
		/*EljSzamaElozofelevrolLezart*/
		case when ii.IktatasDatuma < @startDate AND ii.IktatasDatuma > dateadd(month, -6, @startDate) AND ((u.LezarasDat is not null and u.LezarasDat < @endDate ) or (u.ElintezesDat is not null and u.ElintezesDat < @endDate)) then 1 else 0
		end as EljSzamaElozofelevrolLezart,
		/*EljSzamaElozofelevrolFolyamatban*/
		case when ii.IktatasDatuma < @startDate AND ii.IktatasDatuma > dateadd(month, -6, @startDate) AND ((u.LezarasDat is not null and u.LezarasDat > @endDate ) or (u.ElintezesDat is not null and u.ElintezesDat > @endDate)) then 1 else 0 
		end as EljSzamaElozofelevrolFolyamatban,
		Null,                        /*EljSzamaMegismeteltLezart*/
		Null,                        /*EljSzamaMegismeteltFolyamatban*/
		Null,                        /*EljSzamaTargyfelevbenIndultLezart*/
		Null                         /*EljSzamaTargyfelevbenIndultFolyamatban*/		
	FROM
	EREC_AgazatiJelek a WITH (NOLOCK) 
	INNER JOIN EREC_IraIrattariTetelek it WITH (NOLOCK) ON it.AgazatiJel_Id = a.Id and it.Org=@Org
    INNER JOIN EREC_UgyUgyiratok u WITH (NOLOCK) on u.IraIrattariTetel_Id = it.Id	
    INNER JOIN EREC_UgyUgyiratdarabok ud WITH (NOLOCK) ON ud.UgyUgyirat_Id = u.Id
    INNER JOIN EREC_IraIratok ii WITH (NOLOCK)  ON ii.UgyUgyiratDarab_Id = ud.Id and ii.Ugyirat_Id = u.Id
	INNER JOIN VW_EREC_IraOnkormAdatok i WITH (NOLOCK) ON i.IraIratok_Id = ii.Id						
/* --- EZ a szűrés itt KIZÁRÁST eredményezne -- and u.LetrehozasIdo between @startDate AND @endDate */
		AND u.SztornirozasDat is null
		AND u.Allapot != '90'
		AND i.UgyFajtaja  in ('1','2')
	ORDER BY it.IrattariTetelszam
;
INSERT INTO @TempTable_2016EljSzamaAdatok
SELECT it.IrattariTetelszam AS Kod, 
NULL, --i.Id, 
u.Id, 
i.UgyFajtaja,
		Null,                      /*EljSzamaElozofelevrolLezart*/
		Null,                      /*EljSzamaElozofelevrolFolyamatban*/
		/*EljSzamaMegismeteltLezart*/
		case when i.JogorvoslatiEljarasTipusa = 1
				AND i.JogorvoslatiDontesTipusa = 2
				AND i.JogorvoslatiDontestHozta = 6
				AND i.JogorvoslatiDontesTartalma = 8
				AND ((u.LezarasDat is not null and u.LezarasDat < @endDate ) or (u.ElintezesDat is not null and u.ElintezesDat < @endDate)) then 1 else 0 
		end as EljSzamaMegismeteltLezart,
		/*EljSzamaMegismeteltFolyamatban*/
		case when i.JogorvoslatiEljarasTipusa = 1
				AND i.JogorvoslatiDontesTipusa = 2
				AND i.JogorvoslatiDontestHozta = 6
				AND i.JogorvoslatiDontesTartalma = 8
				AND ((u.LezarasDat is not null and u.LezarasDat > @endDate ) or (u.ElintezesDat is not null and u.ElintezesDat > @endDate)) then 1 else 0 
		end as EljSzamaMegismeteltLezart,
		/*EljSzamaTargyfelevbenIndultLezart*/
		case when ii.IktatasDatuma between @startDate and @endDate AND ((u.LezarasDat is not null and u.LezarasDat < @endDate ) or (u.ElintezesDat is not null and u.ElintezesDat < @endDate)) then 1 else 0
		end as EljSzamaTargyfelevbenIndultLezart,
		/*EljSzamaTargyfelevbenIndultFolyamatban*/
		case when ii.IktatasDatuma between @startDate and @endDate AND ((u.LezarasDat is not null and u.LezarasDat > @endDate ) or (u.ElintezesDat is not null and u.ElintezesDat > @endDate)) then 1 else 0
		end as EljSzamaTargyfelevbenIndultFolyamatban
	FROM
	EREC_AgazatiJelek a WITH (NOLOCK) INNER JOIN 
	EREC_IraIrattariTetelek it WITH (NOLOCK) ON it.AgazatiJel_Id = a.Id and it.Org=@Org
    INNER JOIN EREC_UgyUgyiratok u WITH (NOLOCK) on u.IraIrattariTetel_Id = it.Id	
    INNER JOIN EREC_UgyUgyiratdarabok ud WITH (NOLOCK) ON ud.UgyUgyirat_Id = u.Id
    INNER JOIN EREC_IraIratok ii WITH (NOLOCK)  ON ii.UgyUgyiratDarab_Id = ud.Id and ii.Ugyirat_Id = u.Id
	INNER JOIN VW_EREC_IraOnkormAdatok i WITH (NOLOCK) ON i.IraIratok_Id = ii.Id						
		and ii.IktatasDatuma between @startDate AND @endDate		
		AND u.SztornirozasDat is null
		AND u.Allapot != '90'
		AND i.UgyFajtaja  in ('1','2')
	ORDER BY it.IrattariTetelszam
;

---- adatok mentese a @Reszletes_Tablaba
INSERT INTO @Reszletes_Tabla
--SELECT a.Kod, Donteshozo1, Donteshozo2, Donteshozo4, Donteshozo3, ErdemiDontes1, ErdemiDontes2, ErdemiDontes3, ErdemiDontes4, Vegzes5, Vegzes6, Vegzes7, Vegzes8, Hatarido1, Hatarido2, JogorvoslatiDontesTartalma1, FellebbezesAlapjan, VegzesKepviselotestJogorv5, VegzesKepviselotestJogorv6, VegzesKepviselotestJogorv7, VegzesKepviselotestJogorv8, VegzesBirosagJogorv5, VegzesBirosagJogorv6, VegzesBirosagJogorv7, VegzesBirosagJogorv8, ErdemiUjrafelvJogorv5, ErdemiUjrafelvJogorv2, ErdemiUjrafelvJogorv3, ErdemiUjrafelvJogorv4, ErdemiMeltanyosJogorv5, ErdemiMeltanyosJogorv2, ErdemiMeltanyosJogorv3, ErdemiKepviseloJogorv5, ErdemiKepviseloJogorv6, ErdemiKepviseloJogorv7, ErdemiKepviseloJogorv8, ErdemiBirosagJogorv5, ErdemiBirosagJogorv6, ErdemiBirosagJogorv7, ErdemiBirosagJogorv8, HivatalbolModositVisszavon, HivatalbolFelugyeleti6, HivatalbolFelugyeleti7, HivatalbolFelugyeleti8
SELECT a.Kod, a.UgyFajtaja, ISNULL(Donteshozo,0), ISNULL(Donteshozo1,0), ISNULL(Donteshozo2,0),
			ISNULL(Donteshozo4,0), ISNULL(Donteshozo3,0),
			ISNULL(Donteshozo5,0),ISNULL(Donteshozo6,0),ISNULL(Donteshozo7,0),
			ISNULL(ErdemiDontes1,0), ISNULL(ErdemiDontes2,0),
			ISNULL(ErdemiDontes3,0), ISNULL(ErdemiDontes4,0),
			ISNULL(Vegzes5,0), ISNULL(Vegzes6,0), ISNULL(Vegzes9,0), ISNULL(Vegzes7,0),
			ISNULL(Vegzes8,0), ISNULL(Hatarido1,0), ISNULL(Hatarido2,0),
			ISNULL(JogorvoslatiDontesTartalma1,0),ISNULL(FellebbezesAlapjan1,0), ISNULL(FellebbezesAlapjan,0),
			ISNULL(VegzesKozighivJogorv5,0), ISNULL(VegzesKozighivJogorv6,0),
			ISNULL(VegzesKozighivJogorv7,0), ISNULL(VegzesKozighivJogorv8,0),
			ISNULL(VegzesDekoncentraltJogorv5,0), ISNULL(VegzesDekoncentraltJogorv6,0),
			ISNULL(VegzesDekoncentraltJogorv7,0), ISNULL(VegzesDekoncentraltJogorv8,0),
			ISNULL(VegzesKepviselotestJogorv5,0), ISNULL(VegzesKepviselotestJogorv6,0),
			ISNULL(VegzesKepviselotestJogorv7,0), ISNULL(VegzesKepviselotestJogorv8,0),
			ISNULL(VegzesBirosagJogorv5,0), ISNULL(VegzesBirosagJogorv6,0),
			ISNULL(VegzesBirosagJogorv7,0), ISNULL(VegzesBirosagJogorv8,0),
			ISNULL(ErdemiUjrafelvJogorv5,0), ISNULL(ErdemiUjrafelvJogorv2,0),
			ISNULL(ErdemiUjrafelvJogorv3,0), ISNULL(ErdemiUjrafelvJogorv4,0),
			ISNULL(ErdemiMeltanyosJogorv5,0), ISNULL(ErdemiMeltanyosJogorv2,0),
			ISNULL(ErdemiMeltanyosJogorv3,0), ISNULL(ErdemiKepviseloJogorv5,0),
			ISNULL(ErdemiKepviseloJogorv6,0), ISNULL(ErdemiKepviseloJogorv7,0),
			ISNULL(ErdemiKepviseloJogorv8,0), ISNULL(ErdemiBirosagJogorv5,0),
			ISNULL(ErdemiKozighivJogorv6,0), ISNULL(ErdemiKozighivJogorv7,0),
			ISNULL(ErdemiKozighivJogorv8,0), ISNULL(ErdemiDekoncentraltJogorv5,0),
			ISNULL(ErdemiDekoncentraltJogorv6,0), ISNULL(ErdemiDekoncentraltJogorv7,0),
			ISNULL(ErdemiDekoncentraltJogorv8,0), ISNULL(ErdemiBirosagJogorv5,0),
			ISNULL(ErdemiBirosagJogorv6,0), ISNULL(ErdemiBirosagJogorv7,0),
			ISNULL(ErdemiBirosagJogorv8,0), ISNULL(HivatalbolModositVisszavon,0),
			ISNULL(HivatalbolFelugyeleti6,0), ISNULL(HivatalbolFelugyeleti7,0),
			ISNULL(HivatalbolFelugyeleti8,0),
			ISNULL(HatosagiEllenorzesekSzama, 0),
			ISNULL(MunkaorakSzamaAtlagosan, 0),
			ISNULL(AtlagosEljarasiKoltseg, 0),
			ISNULL(AtlagosKozigazgatasiBirsag, 0),
			ISNULL(EljSzamaElozofelevrolLezartSzama, 0),
			ISNULL(EljSzamaElozofelevrolFolyamatbanSzama, 0),
			ISNULL(EljSzamaMegismeteltLezartSzama, 0),
			ISNULL(EljSzamaMegismeteltFolyamatbanSzama, 0),
			ISNULL(EljSzamaTargyfelevbenIndultLezartSzama, 0), 
			ISNULL(EljSzamaTargyfelevbenIndultFolyamatbanSzama, 0),
			ISNULL(SommasEljSzama, 0),
			ISNULL(NyolcnaponBelulNemSommasSzama, 0),
			ISNULL(FuggoHatalyuHatarozatSzama, 0),
			ISNULL(FuggoHatalyuVegzesSzama, 0),
			ISNULL(FuggoHatalyuHatarozatNemHatalyos, 0),
			ISNULL(FuggoHatalyuHatarozatHatalyos, 0),
			ISNULL(FuggoHatalyuDontesNemHatalyos, 0),
			ISNULL(FuggoHatalyuDontesHatalyos, 0),
			ISNULL(VisszafizetettOsszeg, 0),
			ISNULL(HatosagotTerheloEljarasiKoltseg, 0) 
FROM
(
	(select ugykor_azon as kod, u.UgyFajta as Ugyfajtaja
			from 
				@ugykor_azon_valid v
				inner join (select distinct t.IrattariTetelszam, im.UgyFajta from EREC_UgyUgyiratok u WITH (NOLOCK) inner join EREC_IraIrattariTetelek t WITH (NOLOCK) on u.IraIrattariTetel_Id = t.id inner join EREC_IratMetaDefinicio im WITH (NOLOCK) on u.UgyTipus = im.Ugytipus and im.UgykorKod = t.IrattariTetelszam
						    AND u.SztornirozasDat is null
							AND im.UgyFajta in ('1','2')
	 						AND  u.Allapot != '90') u on v.ugykor_azon = u.IrattariTetelszam
	) AS a
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS Donteshozo FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontestHozta is not null
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS Donteshozo
	ON a.Kod = Donteshozo.Kod and a.UgyFajtaja = Donteshozo.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS Donteshozo1 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontestHozta = '1'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS Donteshozo1
	ON a.Kod = Donteshozo1.Kod and a.UgyFajtaja = Donteshozo1.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS Donteshozo2 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontestHozta = '2'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS Donteshozo2
	ON a.Kod = Donteshozo2.Kod and a.UgyFajtaja = Donteshozo2.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS Donteshozo4 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontestHozta = '4'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS Donteshozo4
	ON a.Kod = Donteshozo4.Kod and a.UgyFajtaja = Donteshozo4.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS Donteshozo3 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontestHozta = '3'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS Donteshozo3
	ON a.Kod = Donteshozo3.Kod and a.UgyFajtaja = Donteshozo3.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS Donteshozo5 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontestHozta = '5'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS Donteshozo5
	ON a.Kod = Donteshozo5.Kod and a.UgyFajtaja = Donteshozo5.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS Donteshozo6 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontestHozta = '6'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS Donteshozo6
	ON a.Kod = Donteshozo6.Kod and a.UgyFajtaja = Donteshozo6.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS Donteshozo7 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontestHozta = '7'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS Donteshozo7
	ON a.Kod = Donteshozo7.Kod and a.UgyFajtaja = Donteshozo7.UgyFajtaja
-- erdemi dontesek
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiDontes1 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '1'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiDontes1
	ON a.Kod = ErdemiDontes1.Kod and a.UgyFajtaja = ErdemiDontes1.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiDontes2 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '2'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiDontes2
	ON a.Kod = ErdemiDontes2.Kod and a.UgyFajtaja = ErdemiDontes2.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiDontes3 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '3'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiDontes3
	ON a.Kod = ErdemiDontes3.Kod and a.UgyFajtaja = ErdemiDontes3.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiDontes4 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '4'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiDontes4
	ON a.Kod = ErdemiDontes4.Kod and a.UgyFajtaja = ErdemiDontes4.UgyFajtaja
-- vegzesek
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS Vegzes5 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '5'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS Vegzes5
	ON a.Kod = Vegzes5.Kod and a.UgyFajtaja = Vegzes5.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS Vegzes6 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '6'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS Vegzes6
	ON a.Kod = Vegzes6.Kod and a.UgyFajtaja = Vegzes6.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS Vegzes9 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '9'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS Vegzes9
	ON a.Kod = Vegzes9.Kod and a.UgyFajtaja = Vegzes9.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS Vegzes7 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '7'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS Vegzes7
	ON a.Kod = Vegzes7.Kod and a.UgyFajtaja = Vegzes7.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS Vegzes8 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '8'
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS Vegzes8
	ON a.Kod = Vegzes8.Kod and a.UgyFajtaja = Vegzes8.UgyFajtaja
-- hatarido
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS Hatarido1 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE UgyintezesHataridore in ('1','IGEN')
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS Hatarido1
	ON a.Kod = Hatarido1.Kod and a.UgyFajtaja = Hatarido1.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS Hatarido2 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE UgyintezesHataridore not in ('1','IGEN')
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS Hatarido2
	ON a.Kod = Hatarido2.Kod and a.UgyFajtaja = Hatarido2.UgyFajtaja
-- kicserelt, kijavitott, kiegeszitett
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS JogorvoslatiDontesTartalma1 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiDontesTartalma = 1 -- 2012 előtt - Kicserélés, kiegészítés vagy kijavítás
				or JogorvoslatiDontes = 1 -- 2012 után - Kijavításra került sor
				or JogorvoslatiDontes = 2 -- 2012 után - Kiegészítésre került sor
					GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS JogorvoslatiDontesTartalma1
	ON a.Kod = JogorvoslatiDontesTartalma1.Kod and a.UgyFajtaja = JogorvoslatiDontesTartalma1.UgyFajtaja
-- fellebbezes alapjan
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS FellebbezesAlapjan1 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
			--AND JogorvoslatiDontestHozta = ???
			--	AND (JogorvoslatiDontesTartalma = 2
			--	OR JogorvoslatiDontesTartalma = 3			
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS FellebbezesAlapjan1
	ON a.Kod = FellebbezesAlapjan1.Kod and a.UgyFajtaja = FellebbezesAlapjan1.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS FellebbezesAlapjan FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 2
			--AND JogorvoslatiDontestHozta = ???
			--	AND (JogorvoslatiDontesTartalma = 2
			--	OR JogorvoslatiDontesTartalma = 3			
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS FellebbezesAlapjan
	ON a.Kod = FellebbezesAlapjan.Kod and a.UgyFajtaja = FellebbezesAlapjan.UgyFajtaja
-- jogorvoslat, kerelem, vegzes
	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS VegzesKozighivJogorv5 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesKozighivJogorv5
	ON a.Kod = VegzesKozighivJogorv5.Kod and a.UgyFajtaja = VegzesKozighivJogorv5.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS VegzesKozighivJogorv6 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesKozighivJogorv6
	ON a.Kod = VegzesKozighivJogorv6.Kod and a.UgyFajtaja = VegzesKozighivJogorv6.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS VegzesKozighivJogorv7 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesKozighivJogorv7
	ON a.Kod = VegzesKozighivJogorv7.Kod and a.UgyFajtaja = VegzesKozighivJogorv7.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS VegzesKozighivJogorv8 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesKozighivJogorv8
	ON a.Kod = VegzesKozighivJogorv8.Kod and a.UgyFajtaja = VegzesKozighivJogorv8.UgyFajtaja

	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS VegzesDekoncentraltJogorv5 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesDekoncentraltJogorv5
	ON a.Kod = VegzesDekoncentraltJogorv5.Kod and a.UgyFajtaja = VegzesDekoncentraltJogorv5.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS VegzesDekoncentraltJogorv6 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesDekoncentraltJogorv6
	ON a.Kod = VegzesDekoncentraltJogorv6.Kod and a.UgyFajtaja = VegzesDekoncentraltJogorv6.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS VegzesDekoncentraltJogorv7 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesDekoncentraltJogorv7
	ON a.Kod = VegzesDekoncentraltJogorv7.Kod and a.UgyFajtaja = VegzesDekoncentraltJogorv7.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS VegzesDekoncentraltJogorv8 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesDekoncentraltJogorv8
	ON a.Kod = VegzesDekoncentraltJogorv8.Kod and a.UgyFajtaja = VegzesDekoncentraltJogorv8.UgyFajtaja
-- jogorvoslat, kerelem, vegzes
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS VegzesKepviselotestJogorv5 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 4
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesKepviselotestJogorv5
	ON a.Kod = VegzesKepviselotestJogorv5.Kod and a.UgyFajtaja = VegzesKepviselotestJogorv5.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS VegzesKepviselotestJogorv6 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 4
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesKepviselotestJogorv6
	ON a.Kod = VegzesKepviselotestJogorv6.Kod and a.UgyFajtaja = VegzesKepviselotestJogorv6.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS VegzesKepviselotestJogorv7 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 4
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesKepviselotestJogorv7
	ON a.Kod = VegzesKepviselotestJogorv7.Kod and a.UgyFajtaja = VegzesKepviselotestJogorv7.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS VegzesKepviselotestJogorv8 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 4
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesKepviselotestJogorv8
	ON a.Kod = VegzesKepviselotestJogorv8.Kod and a.UgyFajtaja = VegzesKepviselotestJogorv8.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS VegzesBirosagJogorv5 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesBirosagJogorv5
	ON a.Kod = VegzesBirosagJogorv5.Kod and a.UgyFajtaja = VegzesBirosagJogorv5.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS VegzesBirosagJogorv6 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesBirosagJogorv6
	ON a.Kod = VegzesBirosagJogorv6.Kod and a.UgyFajtaja = VegzesBirosagJogorv6.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS VegzesBirosagJogorv7 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesBirosagJogorv7
	ON a.Kod = VegzesBirosagJogorv7.Kod and a.UgyFajtaja = VegzesBirosagJogorv7.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS VegzesBirosagJogorv8 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS VegzesBirosagJogorv8
	ON a.Kod = VegzesBirosagJogorv8.Kod and a.UgyFajtaja = VegzesBirosagJogorv8.UgyFajtaja
-- jogorvoslat, kerelem, erdemi dontes
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiUjrafelvJogorv5 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 2
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiUjrafelvJogorv5
	ON a.Kod = ErdemiUjrafelvJogorv5.Kod and a.UgyFajtaja = ErdemiUjrafelvJogorv5.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiUjrafelvJogorv2 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 2
				AND JogorvoslatiDontesTartalma = 2
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiUjrafelvJogorv2
	ON a.Kod = ErdemiUjrafelvJogorv2.Kod and a.UgyFajtaja = ErdemiUjrafelvJogorv2.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiUjrafelvJogorv3 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 2
				AND JogorvoslatiDontesTartalma = 3
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiUjrafelvJogorv3
	ON a.Kod = ErdemiUjrafelvJogorv3.Kod and a.UgyFajtaja = ErdemiUjrafelvJogorv3.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiUjrafelvJogorv4 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 2
				AND JogorvoslatiDontesTartalma = 4
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiUjrafelvJogorv4
	ON a.Kod = ErdemiUjrafelvJogorv4.Kod and a.UgyFajtaja = ErdemiUjrafelvJogorv4.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiMeltanyosJogorv5 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 3
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiMeltanyosJogorv5
	ON a.Kod = ErdemiMeltanyosJogorv5.Kod and a.UgyFajtaja = ErdemiMeltanyosJogorv5.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiMeltanyosJogorv2 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 3
				AND JogorvoslatiDontesTartalma = 2
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiMeltanyosJogorv2
	ON a.Kod = ErdemiMeltanyosJogorv2.Kod and a.UgyFajtaja = ErdemiMeltanyosJogorv2.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiMeltanyosJogorv3 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 3
				AND JogorvoslatiDontesTartalma = 3
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiMeltanyosJogorv3
	ON a.Kod = ErdemiMeltanyosJogorv3.Kod and a.UgyFajtaja = ErdemiMeltanyosJogorv3.UgyFajtaja 
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiKepviseloJogorv5 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 4
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiKepviseloJogorv5
	ON a.Kod = ErdemiKepviseloJogorv5.Kod and a.UgyFajtaja = ErdemiKepviseloJogorv5.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiKepviseloJogorv6 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 4
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiKepviseloJogorv6
	ON a.Kod = ErdemiKepviseloJogorv6.Kod and a.UgyFajtaja = ErdemiKepviseloJogorv6.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiKepviseloJogorv7 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 4
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiKepviseloJogorv7
	ON a.Kod = ErdemiKepviseloJogorv7.Kod and a.UgyFajtaja = ErdemiKepviseloJogorv7.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiKepviseloJogorv8 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 4
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiKepviseloJogorv8
	ON a.Kod = ErdemiKepviseloJogorv8.Kod and a.UgyFajtaja = ErdemiKepviseloJogorv8.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS ErdemiKozighivJogorv5 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiKozighivJogorv5
	ON a.Kod = ErdemiKozighivJogorv5.Kod and a.UgyFajtaja = ErdemiKozighivJogorv5.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS ErdemiKozighivJogorv6 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiKozighivJogorv6
	ON a.Kod = ErdemiKozighivJogorv6.Kod and a.UgyFajtaja = ErdemiKozighivJogorv6.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS ErdemiKozighivJogorv7 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiKozighivJogorv7
	ON a.Kod = ErdemiKozighivJogorv7.Kod and a.UgyFajtaja = ErdemiKozighivJogorv7.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS ErdemiKozighivJogorv8 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiKozighivJogorv8
	ON a.Kod = ErdemiKozighivJogorv8.Kod and a.UgyFajtaja = ErdemiKozighivJogorv8.UgyFajtaja

	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS ErdemiDekoncentraltJogorv5 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiDekoncentraltJogorv5
	ON a.Kod = ErdemiDekoncentraltJogorv5.Kod and a.UgyFajtaja = ErdemiDekoncentraltJogorv5.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS ErdemiDekoncentraltJogorv6 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiDekoncentraltJogorv6
	ON a.Kod = ErdemiDekoncentraltJogorv6.Kod and a.UgyFajtaja = ErdemiDekoncentraltJogorv6.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS ErdemiDekoncentraltJogorv7 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiDekoncentraltJogorv7
	ON a.Kod = ErdemiDekoncentraltJogorv7.Kod and a.UgyFajtaja = ErdemiDekoncentraltJogorv7.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod,t.UgyFajtaja, SUM(t.Darab) AS ErdemiDekoncentraltJogorv8 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiDekoncentraltJogorv8
	ON a.Kod = ErdemiDekoncentraltJogorv8.Kod and a.UgyFajtaja = ErdemiDekoncentraltJogorv8.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiBirosagJogorv5 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiBirosagJogorv5
	ON a.Kod = ErdemiBirosagJogorv5.Kod and a.UgyFajtaja = ErdemiBirosagJogorv5.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiBirosagJogorv6 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiBirosagJogorv6
	ON a.Kod = ErdemiBirosagJogorv6.Kod and a.UgyFajtaja = ErdemiBirosagJogorv6.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiBirosagJogorv7 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiBirosagJogorv7
	ON a.Kod = ErdemiBirosagJogorv7.Kod and a.UgyFajtaja = ErdemiBirosagJogorv7.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS ErdemiBirosagJogorv8 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS ErdemiBirosagJogorv8
	ON a.Kod = ErdemiBirosagJogorv8.Kod and a.UgyFajtaja = ErdemiBirosagJogorv8.UgyFajtaja
-- hivatalbol modositott, visszavont
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS HivatalbolModositVisszavon FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 2
				--AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 8
				AND (JogorvoslatiDontesTartalma = 2
					OR JogorvoslatiDontesTartalma = 3)
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS HivatalbolModositVisszavon
	ON a.Kod = HivatalbolModositVisszavon.Kod and a.UgyFajtaja = HivatalbolModositVisszavon.UgyFajtaja
-- hivatalbol, felugyeleti szerv
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS HivatalbolFelugyeleti6 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 2
				--AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 8
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS HivatalbolFelugyeleti6
	ON a.Kod = HivatalbolFelugyeleti6.Kod and a.UgyFajtaja = HivatalbolFelugyeleti6.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS HivatalbolFelugyeleti7 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 2
				--AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 8
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS HivatalbolFelugyeleti7
	ON a.Kod = HivatalbolFelugyeleti7.Kod and a.UgyFajtaja = HivatalbolFelugyeleti7.UgyFajtaja
	LEFT JOIN
	(SELECT t.Kod, t.UgyFajtaja, SUM(t.Darab) AS HivatalbolFelugyeleti8 FROM
		(SELECT Kod, UgyFajtaja, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 2
				--AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 8
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod, UgyFajtaja
		) AS t
	GROUP BY t.Kod, t.UgyFajtaja) AS HivatalbolFelugyeleti8
	ON a.Kod = HivatalbolFelugyeleti8.Kod and a.UgyFajtaja = HivatalbolFelugyeleti8.UgyFajtaja
	LEFT JOIN
   (SELECT Kod, UgyFajtaja, count(distinct Ugyirat_Id) AS HatosagiEllenorzesekSzama FROM @TempTable_UjAdatok
		    Where ISNULL(HatosagiEllenorzes,'0') in ( '1', 'IGEN')
			GROUP BY Kod, UgyFajtaja
	) AS HatosagiEllenorzesekSzama
	ON a.Kod = HatosagiEllenorzesekSzama.Kod and a.UgyFajtaja = HatosagiEllenorzesekSzama.UgyFajtaja
	LEFT JOIN
   (SELECT Kod, UgyFajtaja, sum(MunkaorakSzama) / count(distinct Ugyirat_Id) AS MunkaorakSzamaAtlagosan FROM @TempTable_UjAdatok
		    Where MunkaorakSzama is not null
			GROUP BY Kod, UgyFajtaja
	) AS MunkaorakSzamaAtlagosan
	ON a.Kod = MunkaorakSzamaAtlagosan.Kod and a.UgyFajtaja = MunkaorakSzamaAtlagosan.UgyFajtaja
	LEFT JOIN
	(SELECT Kod, UgyFajtaja, cast(sum(EljarasiKoltseg) as float) / count(distinct Ugyirat_Id) AS AtlagosEljarasiKoltseg FROM @TempTable_UjAdatok
		    Where EljarasiKoltseg is not null
			GROUP BY Kod, UgyFajtaja
	) AS AtlagosEljarasiKoltseg
	ON a.Kod = AtlagosEljarasiKoltseg.Kod and a.UgyFajtaja = AtlagosEljarasiKoltseg.UgyFajtaja
	LEFT JOIN
	(SELECT Kod, UgyFajtaja, cast(sum(KozigazgatasiBirsagMerteke) as float) / count(distinct Ugyirat_Id) AS AtlagosKozigazgatasiBirsag FROM @TempTable_UjAdatok
		    Where KozigazgatasiBirsagMerteke is not null and KozigazgatasiBirsagMerteke > 0
			GROUP BY Kod, UgyFajtaja
	) AS AtlagosKozigazgatasiBirsag
	ON a.Kod = AtlagosKozigazgatasiBirsag.Kod and a.UgyFajtaja = AtlagosKozigazgatasiBirsag.UgyFajtaja
-- 2016-os adatok
-----------------
	LEFT JOIN
	(SELECT Kod, UgyFajtaja, sum(EljSzamaElozofelevrolLezart) AS EljSzamaElozofelevrolLezartSzama FROM @TempTable_2016EljSzamaAdatok
		    Where EljSzamaElozofelevrolLezart is not null
			GROUP BY Kod, UgyFajtaja
	) AS EljSzamaElozofelevrolLezartSzama
	ON a.Kod = EljSzamaElozofelevrolLezartSzama.Kod and a.UgyFajtaja = EljSzamaElozofelevrolLezartSzama.UgyFajtaja
	LEFT JOIN
	(SELECT Kod, UgyFajtaja, sum(EljSzamaElozofelevrolFolyamatban) AS EljSzamaElozofelevrolFolyamatbanSzama FROM @TempTable_2016EljSzamaAdatok
		    Where EljSzamaElozofelevrolFolyamatban is not null
			GROUP BY Kod, UgyFajtaja
	) AS EljSzamaElozofelevrolFolyamatbanSzama
	ON a.Kod = EljSzamaElozofelevrolFolyamatbanSzama.Kod and a.UgyFajtaja = EljSzamaElozofelevrolFolyamatbanSzama.UgyFajtaja
	LEFT JOIN
	(SELECT Kod, UgyFajtaja, sum(EljSzamaMegismeteltLezart) AS EljSzamaMegismeteltLezartSzama FROM @TempTable_2016EljSzamaAdatok
		    Where EljSzamaMegismeteltLezart is not null
			GROUP BY Kod, UgyFajtaja
	) AS EljSzamaMegismeteltLezartSzama
	ON a.Kod = EljSzamaMegismeteltLezartSzama.Kod and a.UgyFajtaja = EljSzamaMegismeteltLezartSzama.UgyFajtaja
	LEFT JOIN
	(SELECT Kod, UgyFajtaja, sum(EljSzamaMegismeteltFolyamatban) AS EljSzamaMegismeteltFolyamatbanSzama FROM @TempTable_2016EljSzamaAdatok
		    Where EljSzamaMegismeteltFolyamatban is not null
			GROUP BY Kod, UgyFajtaja
	) AS EljSzamaMegismeteltFolyamatbanSzama
	ON a.Kod = EljSzamaMegismeteltFolyamatbanSzama.Kod and a.UgyFajtaja = EljSzamaMegismeteltFolyamatbanSzama.UgyFajtaja
	LEFT JOIN
	(SELECT Kod, UgyFajtaja, sum(EljSzamaTargyfelevbenIndultLezart) AS EljSzamaTargyfelevbenIndultLezartSzama FROM @TempTable_2016EljSzamaAdatok
		    Where EljSzamaTargyfelevbenIndultLezart is not null
			GROUP BY Kod, UgyFajtaja
	) AS EljSzamaTargyfelevbenIndultLezartSzama
	ON a.Kod = EljSzamaTargyfelevbenIndultLezartSzama.Kod and a.UgyFajtaja = EljSzamaTargyfelevbenIndultLezartSzama.UgyFajtaja
	LEFT JOIN
	(SELECT Kod, UgyFajtaja, sum(EljSzamaTargyfelevbenIndultFolyamatban) AS EljSzamaTargyfelevbenIndultFolyamatbanSzama FROM @TempTable_2016EljSzamaAdatok
		    Where EljSzamaTargyfelevbenIndultFolyamatban is not null
			GROUP BY Kod, UgyFajtaja
	) AS EljSzamaTargyfelevbenIndultFolyamatbanSzama
	ON a.Kod = EljSzamaTargyfelevbenIndultFolyamatbanSzama.Kod and a.UgyFajtaja = EljSzamaTargyfelevbenIndultFolyamatbanSzama.UgyFajtaja
-----------------
	LEFT JOIN
   (SELECT Kod, UgyFajtaja, count(distinct Ugyirat_Id) AS SommasEljSzama FROM @TempTable_2016UjAdatok
		    Where ISNULL(SommasEljDontes,'0') in ('1', 'IGEN')
			GROUP BY Kod, UgyFajtaja
	) AS SommasEljSzama
	ON a.Kod = SommasEljSzama.Kod and a.UgyFajtaja = SommasEljSzama.UgyFajtaja
	LEFT JOIN
   (SELECT Kod, UgyFajtaja, count(distinct Ugyirat_Id) AS NyolcnaponBelulNemSommasSzama FROM @TempTable_2016UjAdatok
		    Where ISNULL(NyolcNapBelulNemSommas,'0') in ('1', 'IGEN')
			GROUP BY Kod, UgyFajtaja
	) AS NyolcnaponBelulNemSommasSzama
	ON a.Kod = NyolcnaponBelulNemSommasSzama.Kod and a.UgyFajtaja = NyolcnaponBelulNemSommasSzama.UgyFajtaja
   /* --- #222 hiba javítása -  2017.10.10., BogI --- */
   --- függő hatályú határozat
    LEFT JOIN
   (SELECT Kod, UgyFajtaja, count(distinct Ugyirat_Id) AS FuggoHatalyuHatarozatSzama FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoHatalyuHatarozat,'0') in ( '1', 'IGEN')
			GROUP BY Kod, UgyFajtaja
	) AS FuggoHatalyuHatarozatSzama
	ON a.Kod = FuggoHatalyuHatarozatSzama.Kod and a.UgyFajtaja = FuggoHatalyuHatarozatSzama.UgyFajtaja
	LEFT JOIN
   (SELECT Kod, UgyFajtaja, count(distinct Ugyirat_Id) AS FuggoHatalyuHatarozatHatalyos FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoHatalyuHatarozat,'0') in ( '1', 'IGEN')
		      and ISNULL(FuggoHatHatalybaLepes,'0') in ( '1', 'IGEN')
			GROUP BY Kod, UgyFajtaja
	) AS FuggoHatalyuHatarozatHatalyos
	ON a.Kod = FuggoHatalyuHatarozatHatalyos.Kod and a.UgyFajtaja = FuggoHatalyuHatarozatHatalyos.UgyFajtaja
	LEFT JOIN
   (SELECT Kod, UgyFajtaja, count(distinct Ugyirat_Id) AS FuggoHatalyuHatarozatNemHatalyos FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoHatalyuHatarozat,'0') in ( '1', 'IGEN')
		      and ISNULL(FuggoHatHatalybaLepes,'0') NOT in ( '1', 'IGEN')
			GROUP BY Kod, UgyFajtaja
	) AS FuggoHatalyuHatarozatNemHatalyos
	ON a.Kod = FuggoHatalyuHatarozatNemHatalyos.Kod and a.UgyFajtaja = FuggoHatalyuHatarozatNemHatalyos.UgyFajtaja
   --- függő hatályú végzés
	LEFT JOIN
   (SELECT Kod, UgyFajtaja, count(distinct Ugyirat_Id) AS FuggoHatalyuVegzesSzama FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoHatalyuVegzes,'0') in ( '1', 'IGEN')
			GROUP BY Kod, UgyFajtaja
	) AS FuggoHatalyuVegzesSzama
	ON a.Kod = FuggoHatalyuVegzesSzama.Kod and a.UgyFajtaja = FuggoHatalyuVegzesSzama.UgyFajtaja
	LEFT JOIN
   (SELECT Kod, UgyFajtaja, count(distinct Ugyirat_Id) AS FuggoHatalyuDontesHatalyos FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoHatalyuVegzes,'0') in ( '1', 'IGEN')
		      and ISNULL(FuggoVegzesHatalyba,'0') in ( '1', 'IGEN')
			GROUP BY Kod, UgyFajtaja
	) AS FuggoHatalyuDontesHatalyos
	ON a.Kod = FuggoHatalyuDontesHatalyos.Kod and a.UgyFajtaja = FuggoHatalyuDontesHatalyos.UgyFajtaja
	LEFT JOIN
   (SELECT Kod, UgyFajtaja, count(distinct Ugyirat_Id) AS FuggoHatalyuDontesNemHatalyos FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoHatalyuVegzes,'0') in ( '1', 'IGEN')
		      and ISNULL(FuggoVegzesHatalyba,'0') NOT in ( '1', 'IGEN')
			GROUP BY Kod, UgyFajtaja
	) AS FuggoHatalyuDontesNemHatalyos
	ON a.Kod = FuggoHatalyuDontesNemHatalyos.Kod and a.UgyFajtaja = FuggoHatalyuDontesNemHatalyos.UgyFajtaja
	LEFT JOIN
   (SELECT Kod, UgyFajtaja, sum(HatAltalVisszafizOsszeg) AS VisszafizetettOsszeg FROM @TempTable_2016UjAdatok
		    Where HatAltalVisszafizOsszeg is not null
			GROUP BY Kod, UgyFajtaja 
		) AS VisszafizetettOsszeg
	ON a.Kod = VisszafizetettOsszeg.Kod and a.UgyFajtaja = VisszafizetettOsszeg.UgyFajtaja
	LEFT JOIN
   (SELECT Kod, UgyFajtaja, sum(HatTerheloEljKtsg) AS HatosagotTerheloEljarasiKoltseg FROM @TempTable_2016UjAdatok
		    Where HatTerheloEljKtsg is not null
			GROUP BY Kod, UgyFajtaja
	) AS HatosagotTerheloEljarasiKoltseg
	ON a.Kod = HatosagotTerheloEljarasiKoltseg.Kod and a.UgyFajtaja = HatosagotTerheloEljarasiKoltseg.UgyFajtaja
)

declare @stat_adat_tipus_azon_full_list table
(
	stat_tipus_azon nvarchar(64),
	stat_adat_tipus_azon nvarchar(64),
	stat_adat_group_type nvarchar(64),
	regi_kod nvarchar(64)
)

insert into @stat_adat_tipus_azon_full_list (stat_tipus_azon, stat_adat_tipus_azon, stat_adat_group_type, regi_kod)
VALUES 
('2','2_2','SUM','Donteshozo'),
('2','2_3','SUM','Donteshozo4'),
('2','2_4','SUM','Donteshozo5'),
('2','2_5','SUM','Donteshozo6'),
('2','2_6','SUM','ErdemiDontes1'),
('2','2_7','SUM','ErdemiDontes2'),
('2','2_8','SUM','ErdemiDontes3'),
('2','2_9','SUM','ErdemiDontes4'),
('2','2_10','SUM','Vegzes5'),
('2','2_11','SUM','Vegzes6'),
('2','2_12','SUM','Vegzes9'),
('2','2_13','SUM','Vegzes7'),
('2','2_14','SUM','Vegzes8'),
('2','2_15','SUM','Hatarido1'),
('2','2_16','SUM','Hatarido2'),
('2','2_17','SUM','JogorvoslatiDontesTartalma1'),
('2','2_18','SUM','FellebbezesAlapjan1'),
('2','2_19','SUM','FellebbezesAlapjan'),
('2','2_20','SUM','VegzesKozighivJogorv5'),
('2','2_21','SUM','VegzesKozighivJogorv6'),
('2','2_22','SUM','VegzesKozighivJogorv7'),
('2','2_23','SUM','VegzesKozighivJogorv8'),
('2','2_24','SUM','VegzesDekoncentraltJogorv5'),
('2','2_25','SUM','VegzesDekoncentraltJogorv6'),
('2','2_26','SUM','VegzesDekoncentraltJogorv7'),
('2','2_27','SUM','VegzesDekoncentraltJogorv8'),
('2','2_28','SUM','VegzesBirosagJogorv5'),
('2','2_29','SUM','VegzesBirosagJogorv6'),
('2','2_30','SUM','VegzesBirosagJogorv7'),
('2','2_31','SUM','VegzesBirosagJogorv8'),
('2','2_32','SUM','ErdemiKozighivJogorv5'),
('2','2_33','SUM','ErdemiKozighivJogorv6'),
('2','2_34','SUM','ErdemiKozighivJogorv7'),
('2','2_35','SUM','ErdemiKozighivJogorv8'),
('2','2_36','SUM','ErdemiDekoncentraltJogorv5'),
('2','2_37','SUM','ErdemiDekoncentraltJogorv6'),
('2','2_38','SUM','ErdemiDekoncentraltJogorv7'),
('2','2_39','SUM','ErdemiDekoncentraltJogorv8'),
('2','2_40','SUM','ErdemiBirosagJogorv5'),
('2','2_41','SUM','ErdemiBirosagJogorv6'),
('2','2_42','SUM','ErdemiBirosagJogorv7'),
('2','2_43','SUM','ErdemiBirosagJogorv8'),
('2','2_48','SUM','HivatalbolModositVisszavon'),
('2','2_49','SUM','HivatalbolFelugyeleti6'),
('2','2_50','SUM','HivatalbolFelugyeleti7'),
('2','2_51','SUM','HivatalbolFelugyeleti8'),
('2','2_52','SUM','HatosagiEllenorzesekSzama'),
('2','2_53','AVG','MunkaorakSzamaAtlagosan'),
('2','2_54','AVG','AtlagosEljarasiKoltseg'),
('2','2_55','AVG','AtlagosKozigazgatasiBirsag'),
('2','2_56','PRE','EljSzamaElozofelevrolLezartSzama'),
('2','2_57','PRE','EljSzamaElozofelevrolFolyamatbanSzama'),
('2','2_58','ISCLOSED_SUM','EljSzamaMegismeteltLezartSzama'),
('2','2_59','ISCLOSED_SUM','EljSzamaMegismeteltFolyamatbanSzama'),
('2','2_60','COUNT','EljSzamaTargyfelevbenIndultLezartSzama'),
('2','2_61','COUNT','EljSzamaTargyfelevbenIndultFolyamatbanSzama'),
('2','2_62','SUM','SommasEljSzama'),
('2','2_63','SUM','NyolcnaponBelulNemSommasSzama'),
('2','2_64','SUM','FuggoHatalyuHatarozatSzama'),
('2','2_65','SUM','FuggoHatalyuVegzesSzama'),
('2','2_66','SUM','FuggoHatalyuHatarozatNemHatalyos'),
('2','2_67','SUM','FuggoHatalyuHatarozatHatalyos'),
('2','2_68','SUM','FuggoHatalyuDontesNemHatalyos'),
('2','2_69','SUM','FuggoHatalyuDontesHatalyos'),
('2','2_70','SUM','VisszafizetettOsszeg'),
('2','2_71','SUM','HatosagotTerheloEljarasiKoltseg'),
('1','1_2','SUM','Donteshozo'),
('1','1_3','SUM','Donteshozo1'),
('1','1_4','SUM','Donteshozo2'),
('1','1_5','SUM','Donteshozo4'),
('1','1_6','SUM','Donteshozo6'),
('1','1_7','SUM','Donteshozo5'),
('1','1_8','SUM','Donteshozo7'),
('1','1_9','SUM','ErdemiDontes1'),
('1','1_10','SUM','ErdemiDontes2'),
('1','1_11','SUM','ErdemiDontes3'),
('1','1_12','SUM','ErdemiDontes4'),
('1','1_13','SUM','Vegzes5'),
('1','1_14','SUM','Vegzes6'),
('1','1_15','SUM','Vegzes9'),
('1','1_16','SUM','Vegzes7'),
('1','1_17','SUM','Vegzes8'),
('1','1_18','SUM','Hatarido1'),
('1','1_19','SUM','Hatarido2'),
('1','1_20','SUM','JogorvoslatiDontesTartalma1'),
('1','1_21','SUM','FellebbezesAlapjan1'),
('1','1_22','SUM','FellebbezesAlapjan'),
('1','1_23','SUM','VegzesKepviselotestJogorv5'),
('1','1_24','SUM','VegzesKepviselotestJogorv6'),
('1','1_25','SUM','VegzesKepviselotestJogorv7'),
('1','1_26','SUM','VegzesKepviselotestJogorv8'),
('1','1_27','SUM','VegzesBirosagJogorv5'),
('1','1_28','SUM','VegzesBirosagJogorv6'),
('1','1_29','SUM','VegzesBirosagJogorv7'),
('1','1_30','SUM','VegzesBirosagJogorv8'),
('1','1_31','SUM','ErdemiKepviseloJogorv5'),
('1','1_32','SUM','ErdemiKepviseloJogorv6'),
('1','1_33','SUM','ErdemiKepviseloJogorv7'),
('1','1_34','SUM','ErdemiKepviseloJogorv8'),
('1','1_35','SUM','ErdemiBirosagJogorv5'),
('1','1_36','SUM','ErdemiBirosagJogorv6'),
('1','1_37','SUM','ErdemiBirosagJogorv7'),
('1','1_38','SUM','ErdemiBirosagJogorv8'),
('1','1_39','SUM','HivatalbolModositVisszavon'),
('1','1_40','SUM','HivatalbolFelugyeleti6'),
('1','1_41','SUM','HivatalbolFelugyeleti7'),
('1','1_42','SUM','HivatalbolFelugyeleti8'),
('1','1_43','SUM','HatosagiEllenorzesekSzama'),
('1','1_44','AVG','MunkaorakSzamaAtlagosan'),
('1','1_45','AVG','AtlagosEljarasiKoltseg'),
('1','1_46','AVG','AtlagosKozigazgatasiBirsag'),
('1','1_47','PRE','EljSzamaElozofelevrolLezartSzama'),
('1','1_48','PRE','EljSzamaElozofelevrolFolyamatbanSzama'),
('1','1_49','ISCLOSED_SUM','EljSzamaMegismeteltLezartSzama'),
('1','1_50','ISCLOSED_SUM','EljSzamaMegismeteltFolyamatbanSzama'),
('1','1_51','COUNT','EljSzamaTargyfelevbenIndultLezartSzama'),
('1','1_52','COUNT','EljSzamaTargyfelevbenIndultFolyamatbanSzama'),
('1','1_53','SUM','SommasEljSzama'),
('1','1_54','SUM','NyolcnaponBelulNemSommasSzama'),
('1','1_55','SUM','FuggoHatalyuHatarozatSzama'),
('1','1_56','SUM','FuggoHatalyuVegzesSzama'),
('1','1_57','SUM','FuggoHatalyuHatarozatNemHatalyos'),
('1','1_58','SUM','FuggoHatalyuHatarozatHatalyos'),
('1','1_59','SUM','FuggoHatalyuDontesNemHatalyos'),
('1','1_60','SUM','FuggoHatalyuDontesHatalyos'),
('1','1_61','SUM','VisszafizetettOsszeg'),
('1','1_62','SUM','HatosagotTerheloEljarasiKoltseg')

declare @resp_wo_data table
	(
		agazat_azon nvarchar(64),
		ugykor_azon nvarchar(64),
		stat_tipus_azon nvarchar(64),
		stat_adat_tipus_azon nvarchar(64),
		regikod nvarchar(64)
	)
	
	-- ágatai jelenként létrehozzuk az üres válasz táblázatot minden statisztikai adattal
	insert into @resp_wo_data
	select		
		stat_groups.agazat_azon
		, stat_groups.ugykor_azon
		, stat_groups.stat_tipus_azon
		, fl.stat_adat_tipus_azon
		, fl.regi_kod
	from
		(
		select distinct
			left(kod,1) as  agazat_azon
		  , kod as ugykor_azon
		  , case 
				when UgyFajtaja = '1' then '2'
				when Ugyfajtaja = '2' then '1'
			end as stat_tipus_azon -- át kell forgatni, mert a DWH-ban fordítva van bekötve
		from
			@Reszletes_Tabla
		)	stat_groups
		inner join
		@stat_adat_tipus_azon_full_list fl
		on
		stat_groups.stat_tipus_azon = fl.stat_tipus_azon

-- tényleges lekérdezés
select
	r.stat_tipus_azon
	, r.stat_adat_tipus_azon
	, r.agazat_azon
	, r.ugykor_azon
	, replace(ltrim(str(isnull(t.Ertek,0),20,6)),',','.') as ertek
from 
	@resp_wo_data r
	inner join 
	(select 
	Kod, case when UgyFajtaja = '1' then '2' when UgyFajtaja = '2' then '1' end as UgyFajtaja, RegiKod, Ertek
From 
	 (select Kod,	UgyFajtaja, CAST(Donteshozo as float) as Donteshozo,	CAST(Donteshozo4 as float) as Donteshozo4,	CAST(Donteshozo5 as float) as Donteshozo5,	CAST(Donteshozo6 as float) as Donteshozo6,	CAST(ErdemiDontes1 as float) as ErdemiDontes1,	CAST(ErdemiDontes2 as float) as ErdemiDontes2,	CAST(ErdemiDontes3 as float) as ErdemiDontes3,	CAST(ErdemiDontes4 as float) as ErdemiDontes4,	CAST(Vegzes5 as float) as Vegzes5,	CAST(Vegzes6 as float) as Vegzes6,	CAST(Vegzes9 as float) as Vegzes9,	CAST(Vegzes7 as float) as Vegzes7,	CAST(Vegzes8 as float) as Vegzes8,	CAST(Hatarido1 as float) as Hatarido1,	CAST(Hatarido2 as float) as Hatarido2,	CAST(JogorvoslatiDontesTartalma1 as float) as JogorvoslatiDontesTartalma1,	CAST(FellebbezesAlapjan1 as float) as FellebbezesAlapjan1,	CAST(FellebbezesAlapjan as float) as FellebbezesAlapjan,	CAST(VegzesKozighivJogorv5 as float) as VegzesKozighivJogorv5,	CAST(VegzesKozighivJogorv6 as float) as VegzesKozighivJogorv6,	CAST(VegzesKozighivJogorv7 as float) as VegzesKozighivJogorv7,	CAST(VegzesKozighivJogorv8 as float) as VegzesKozighivJogorv8,	CAST(VegzesDekoncentraltJogorv5 as float) as VegzesDekoncentraltJogorv5,	CAST(VegzesDekoncentraltJogorv6 as float) as VegzesDekoncentraltJogorv6,	CAST(VegzesDekoncentraltJogorv7 as float) as VegzesDekoncentraltJogorv7,	CAST(VegzesDekoncentraltJogorv8 as float) as VegzesDekoncentraltJogorv8,	CAST(VegzesBirosagJogorv5 as float) as VegzesBirosagJogorv5,	CAST(VegzesBirosagJogorv6 as float) as VegzesBirosagJogorv6,	CAST(VegzesBirosagJogorv7 as float) as VegzesBirosagJogorv7,	CAST(VegzesBirosagJogorv8 as float) as VegzesBirosagJogorv8,	CAST(ErdemiKozighivJogorv5 as float) as ErdemiKozighivJogorv5,	CAST(ErdemiKozighivJogorv6 as float) as ErdemiKozighivJogorv6,	CAST(ErdemiKozighivJogorv7 as float) as ErdemiKozighivJogorv7,	CAST(ErdemiKozighivJogorv8 as float) as ErdemiKozighivJogorv8,	CAST(ErdemiDekoncentraltJogorv5 as float) as ErdemiDekoncentraltJogorv5,	CAST(ErdemiDekoncentraltJogorv6 as float) as ErdemiDekoncentraltJogorv6,	CAST(ErdemiDekoncentraltJogorv7 as float) as ErdemiDekoncentraltJogorv7,	CAST(ErdemiDekoncentraltJogorv8 as float) as ErdemiDekoncentraltJogorv8,	CAST(ErdemiBirosagJogorv5 as float) as ErdemiBirosagJogorv5,	CAST(ErdemiBirosagJogorv6 as float) as ErdemiBirosagJogorv6,	CAST(ErdemiBirosagJogorv7 as float) as ErdemiBirosagJogorv7,	CAST(ErdemiBirosagJogorv8 as float) as ErdemiBirosagJogorv8,	CAST(HivatalbolModositVisszavon as float) as HivatalbolModositVisszavon,	CAST(HivatalbolFelugyeleti6 as float) as HivatalbolFelugyeleti6,	CAST(HivatalbolFelugyeleti7 as float) as HivatalbolFelugyeleti7,	CAST(HivatalbolFelugyeleti8 as float) as HivatalbolFelugyeleti8,	CAST(HatosagiEllenorzesekSzama as float) as HatosagiEllenorzesekSzama,	CAST(MunkaorakSzamaAtlagosan as float) as MunkaorakSzamaAtlagosan,	CAST(AtlagosEljarasiKoltseg as float) as AtlagosEljarasiKoltseg,	CAST(AtlagosKozigazgatasiBirsag as float) as AtlagosKozigazgatasiBirsag,	CAST(EljSzamaElozofelevrolLezartSzama as float) as EljSzamaElozofelevrolLezartSzama,	CAST(EljSzamaElozofelevrolFolyamatbanSzama as float) as EljSzamaElozofelevrolFolyamatbanSzama,	CAST(EljSzamaMegismeteltLezartSzama as float) as EljSzamaMegismeteltLezartSzama,	CAST(EljSzamaMegismeteltFolyamatbanSzama as float) as EljSzamaMegismeteltFolyamatbanSzama,	CAST(EljSzamaTargyfelevbenIndultLezartSzama as float) as EljSzamaTargyfelevbenIndultLezartSzama,	CAST(EljSzamaTargyfelevbenIndultFolyamatbanSzama as float) as EljSzamaTargyfelevbenIndultFolyamatbanSzama,	CAST(SommasEljSzama as float) as SommasEljSzama,	CAST(NyolcnaponBelulNemSommasSzama as float) as NyolcnaponBelulNemSommasSzama,	CAST(FuggoHatalyuHatarozatSzama as float) as FuggoHatalyuHatarozatSzama,	CAST(FuggoHatalyuVegzesSzama as float) as FuggoHatalyuVegzesSzama,	CAST(FuggoHatalyuHatarozatNemHatalyos as float) as FuggoHatalyuHatarozatNemHatalyos,	CAST(FuggoHatalyuHatarozatHatalyos as float) as FuggoHatalyuHatarozatHatalyos,	CAST(FuggoHatalyuDontesNemHatalyos as float) as FuggoHatalyuDontesNemHatalyos,	CAST(FuggoHatalyuDontesHatalyos as float) as FuggoHatalyuDontesHatalyos,	CAST(VisszafizetettOsszeg as float) as VisszafizetettOsszeg,	CAST(HatosagotTerheloEljarasiKoltseg as float) as HatosagotTerheloEljarasiKoltseg,	CAST(Donteshozo1 as float) as Donteshozo1,	CAST(Donteshozo2 as float) as Donteshozo2,	CAST(Donteshozo7 as float) as Donteshozo7,	CAST(VegzesKepviselotestJogorv5 as float) as VegzesKepviselotestJogorv5,	CAST(VegzesKepviselotestJogorv6 as float) as VegzesKepviselotestJogorv6,	CAST(VegzesKepviselotestJogorv7 as float) as VegzesKepviselotestJogorv7,	CAST(VegzesKepviselotestJogorv8 as float) as VegzesKepviselotestJogorv8,	CAST(ErdemiKepviseloJogorv5 as float) as ErdemiKepviseloJogorv5,	CAST(ErdemiKepviseloJogorv6 as float) as ErdemiKepviseloJogorv6,	CAST(ErdemiKepviseloJogorv7 as float) as ErdemiKepviseloJogorv7,	CAST(ErdemiKepviseloJogorv8 as float) as ErdemiKepviseloJogorv8  from @Reszletes_Tabla) p
unpivot
	(Ertek FOR RegiKod IN
		(Donteshozo,	Donteshozo4,	Donteshozo5,	Donteshozo6,	ErdemiDontes1,	ErdemiDontes2,	ErdemiDontes3,	ErdemiDontes4,	Vegzes5,	Vegzes6,	Vegzes9,	Vegzes7,	Vegzes8,	Hatarido1,	Hatarido2,	JogorvoslatiDontesTartalma1,	FellebbezesAlapjan1,	FellebbezesAlapjan,	VegzesKozighivJogorv5,	VegzesKozighivJogorv6,	VegzesKozighivJogorv7,	VegzesKozighivJogorv8,	VegzesDekoncentraltJogorv5,	VegzesDekoncentraltJogorv6,	VegzesDekoncentraltJogorv7,	VegzesDekoncentraltJogorv8,	VegzesBirosagJogorv5,	VegzesBirosagJogorv6,	VegzesBirosagJogorv7,	VegzesBirosagJogorv8,	ErdemiKozighivJogorv5,	ErdemiKozighivJogorv6,	ErdemiKozighivJogorv7,	ErdemiKozighivJogorv8,	ErdemiDekoncentraltJogorv5,	ErdemiDekoncentraltJogorv6,	ErdemiDekoncentraltJogorv7,	ErdemiDekoncentraltJogorv8,	ErdemiBirosagJogorv5,	ErdemiBirosagJogorv6,	ErdemiBirosagJogorv7,	ErdemiBirosagJogorv8,	HivatalbolModositVisszavon,	HivatalbolFelugyeleti6,	HivatalbolFelugyeleti7,	HivatalbolFelugyeleti8,	HatosagiEllenorzesekSzama,	MunkaorakSzamaAtlagosan,	AtlagosEljarasiKoltseg,	AtlagosKozigazgatasiBirsag,	EljSzamaElozofelevrolLezartSzama,	EljSzamaElozofelevrolFolyamatbanSzama,	EljSzamaMegismeteltLezartSzama,	EljSzamaMegismeteltFolyamatbanSzama,	EljSzamaTargyfelevbenIndultLezartSzama,	EljSzamaTargyfelevbenIndultFolyamatbanSzama,	SommasEljSzama,	NyolcnaponBelulNemSommasSzama,	FuggoHatalyuHatarozatSzama,	FuggoHatalyuVegzesSzama,	FuggoHatalyuHatarozatNemHatalyos,	FuggoHatalyuHatarozatHatalyos,	FuggoHatalyuDontesNemHatalyos,	FuggoHatalyuDontesHatalyos,	VisszafizetettOsszeg,	HatosagotTerheloEljarasiKoltseg,	Donteshozo1,	Donteshozo2,	Donteshozo7,	VegzesKepviselotestJogorv5,	VegzesKepviselotestJogorv6,	VegzesKepviselotestJogorv7,	VegzesKepviselotestJogorv8,	ErdemiKepviseloJogorv5,	ErdemiKepviseloJogorv6,	ErdemiKepviseloJogorv7,	ErdemiKepviseloJogorv8)
	) unp)
	 t on r.ugykor_azon = t.Kod and r.stat_tipus_azon = t.UgyFajtaja and r.regikod = t.RegiKod

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end




GO