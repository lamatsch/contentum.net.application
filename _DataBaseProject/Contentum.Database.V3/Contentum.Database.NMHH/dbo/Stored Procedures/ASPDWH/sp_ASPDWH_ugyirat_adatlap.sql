DROP PROCEDURE [dbo].[sp_ASPDWH_ugyirat_adatlap]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[sp_ASPDWH_ugyirat_adatlap]
  @startDate datetime,
  @endDate datetime
as

begin

BEGIN TRY

set nocount on

CREATE TABLE #ugykor_azon_valid
(
	ugykor_azon nvarchar(10)
)	

INSERT INTO #ugykor_azon_valid
  (ugykor_azon)
VALUES
  ('A101'),
  ('A102'),
  ('A103'),
  ('A104'),
  ('A105'),
  ('A106'),
  ('A107'),
  ('A108'),
  ('A109'),
  ('A201'),
  ('A202'),
  ('A203'),
  ('A204'),
  ('A205'),
  ('A206'),
  ('A207'),
  ('A208'),
  ('A209'),
  ('A210'),
  ('A211'),
  ('A212'),
  ('B101'),
  ('B102'),
  ('B103'),
  ('B104'),
  ('B105'),
  ('B106'),
  ('B107'),
  ('B108'),
  ('B109'),
  ('B110'),
  ('B111'),
  ('B112'),
  ('B113'),
  ('B114'),
  ('B115'),
  ('B116'),
  ('B117'),
  ('B118'),
  ('B119'),
  ('B120'),
  ('B121'),
  ('B122'),
  ('B123'),
  ('B124'),
  ('B125'),
  ('B126'),
  ('B127'),
  ('C101'),
  ('C102'),
  ('C103'),
  ('C104'),
  ('C105'),
  ('C106'),
  ('C107'),
  ('C108'),
  ('C109'),
  ('C110'),
  ('C111'),
  ('C112'),
  ('C113'),
  ('C114'),
  ('C115'),
  ('C116'),
  ('C117'),
  ('C118'),
  ('C119'),
  ('C120'),
  ('C121'),
  ('C122'),
  ('C123'),
  ('C124'),
  ('C125'),
  ('C126'),
  ('C127'),
  ('C128'),
  ('C129'),
  ('C130'),
  ('C131'),
  ('C132'),
  ('C133'),
  ('C134'),
  ('C135'),
  ('E101'),
  ('E102'),
  ('E103'),
  ('E104'),
  ('E105'),
  ('E106'),
  ('E107'),
  ('E108'),
  ('E109'),
  ('E110'),
  ('E111'),
  ('E112'),
  ('E113'),
  ('E114'),
  ('E115'),
  ('E116'),
  ('E117'),
  ('E118'),
  ('E201'),
  ('E202'),
  ('E203'),
  ('E204'),
  ('E205'),
  ('E206'),
  ('E207'),
  ('E208'),
  ('E209'),
  ('E210'),
  ('E211'),
  ('E212'),
  ('E213'),
  ('E214'),
  ('E215'),
  ('E216'),
  ('E217'),
  ('E218'),
  ('E219'),
  ('E220'),
  ('E221'),
  ('E222'),
  ('E223'),
  ('E301'),
  ('E302'),
  ('E303'),
  ('E304'),
  ('E305'),
  ('E306'),
  ('E307'),
  ('E308'),
  ('E309'),
  ('E401'),
  ('E402'),
  ('E403'),
  ('E404'),
  ('E405'),
  ('E406'),
  ('E407'),
  ('E408'),
  ('E409'),
  ('E410'),
  ('E411'),
  ('E412'),
  ('E413'),
  ('F101'),
  ('F102'),
  ('F103'),
  ('F104'),
  ('F105'),
  ('F106'),
  ('F107'),
  ('F108'),
  ('F109'),
  ('F110'),
  ('F111'),
  ('F112'),
  ('F113'),
  ('F114'),
  ('F115'),
  ('F116'),
  ('F117'),
  ('F118'),
  ('F119'),
  ('F120'),
  ('F121'),
  ('F122'),
  ('F123'),
  ('F124'),
  ('F125'),
  ('F126'),
  ('F127'),
  ('F128'),
  ('F129'),
  ('F130'),
  ('F131'),
  ('F132'),
  ('G101'),
  ('G102'),
  ('G103'),
  ('G104'),
  ('G105'),
  ('G106'),
  ('G107'),
  ('G108'),
  ('G109'),
  ('G110'),
  ('G111'),
  ('G112'),
  ('G113'),
  ('G114'),
  ('G115'),
  ('G116'),
  ('G117'),
  ('G118'),
  ('G119'),
  ('G120'),
  ('G121'),
  ('G122'),
  ('G123'),
  ('G124'),
  ('G125'),
  ('H101'),
  ('H102'),
  ('H103'),
  ('H104'),
  ('H105'),
  ('H106'),
  ('H107'),
  ('H201'),
  ('H202'),
  ('H203'),
  ('H204'),
  ('H301'),
  ('H302'),
  ('H303'),
  ('H304'),
  ('H305'),
  ('H306'),
  ('H307'),
  ('H401'),
  ('H402'),
  ('H403'),
  ('H404'),
  ('H405'),
  ('H406'),
  ('H501'),
  ('H502'),
  ('H503'),
  ('H504'),
  ('H505'),
  ('H506'),
  ('H507'),
  ('H508'),
  ('H509'),
  ('H510'),
  ('H601'),
  ('H701'),
  ('H702'),
  ('H703'),
  ('H704'),
  ('H705'),
  ('H801'),
  ('H802'),
  ('H803'),
  ('H804'),
  ('H805'),
  ('H806'),
  ('H807'),
  ('H808'),
  ('H809'),
  ('H810'),
  ('H811'),
  ('H812'),
  ('H813'),
  ('H814'),
  ('H815'),
  ('H816'),
  ('H817'),
  ('I101'),
  ('I102'),
  ('I103'),
  ('I104'),
  ('I105'),
  ('I106'),
  ('I107'),
  ('I108'),
  ('I109'),
  ('I110'),
  ('I111'),
  ('I112'),
  ('I113'),
  ('J101'),
  ('J102'),
  ('J103'),
  ('J104'),
  ('J105'),
  ('J106'),
  ('J107'),
  ('J108'),
  ('J109'),
  ('J110'),
  ('J112'),
  ('J113'),
  ('J114'),
  ('K101'),
  ('K102'),
  ('K103'),
  ('K104'),
  ('K105'),
  ('K106'),
  ('K107'),
  ('L101'),
  ('L102'),
  ('L103'),
  ('L104'),
  ('L105'),
  ('L106'),
  ('L107'),
  ('L108'),
  ('L109'),
  ('L110'),
  ('L111'),
  ('L112'),
  ('L113'),
  ('L114'),
  ('L115'),
  ('L116'),
  ('L117'),
  ('M101'),
  ('M102'),
  ('M103'),
  ('M104'),
  ('M105'),
  ('M106'),
  ('M107'),
  ('M108'),
  ('M109'),
  ('M110'),
  ('M111'),
  ('M112'),
  ('M113'),
  ('M114'),
  ('M115'),
  ('M116'),
  ('M117'),
  ('M118'),
  ('M119'),
  ('M120'),
  ('M121'),
  ('M122'),
  ('M123'),
  ('M124'),
  ('M125'),
  ('M126'),
  ('M127'),
  ('M128'),
  ('M129'),
  ('M130'),
  ('N101'),
  ('N102'),
  ('P101'),
  ('P102'),
  ('P103'),
  ('P104'),
  ('P105'),
  ('P106'),
  ('P107'),
  ('P108'),
  ('P109'),
  ('P110'),
  ('P111'),
  ('P112'),
  ('P113'),
  ('P114'),
  ('P115'),
  ('P116'),
  ('P117'),
  ('P118'),
  ('P119'),
  ('P120'),
  ('P121'),
  ('P122'),
  ('P123'),
  ('P124'),
  ('P125'),
  ('P126'),
  ('P127'),
  ('P128'),
  ('P129'),
  ('P130'),
  ('P131'),
  ('P132'),
  ('P133'),
  ('P134'),
  ('P135'),
  ('P136'),
  ('P137'),
  ('P138'),
  ('P139'),
  ('P140'),
  ('P141'),
  ('P142'),
  ('R101'),
  ('R102'),
  ('R103'),
  ('R104'),
  ('R105'),
  ('U101'),
  ('U102'),
  ('U103'),
  ('U104'),
  ('U105'),
  ('U106'),
  ('U107'),
  ('U108'),
  ('U109'),
  ('U110'),
  ('U111'),
  ('U201'),
  ('U202'),
  ('U203'),
  ('U204'),
  ('U205'),
  ('U206'),
  ('U207'),
  ('U208'),
  ('U209'),
  ('U210'),
  ('U211'),
  ('U212'),
  ('U213'),
  ('U214'),
  ('U215'),
  ('U216'),
  ('U301'),
  ('U302'),
  ('U303'),
  ('U304'),
  ('U305'),
  ('U306'),
  ('U307'),
  ('U308'),
  ('U309'),
  ('U310'),
  ('U311'),
  ('U312'),
  ('U313'),
  ('U314'),
  ('U315'),
  ('U316'),
  ('U317'),
  ('U318'),
  ('U319'),
  ('U320'),
  ('U321'),
  ('U322'),
  ('U323'),
  ('U324'),
  ('U325'),
  ('U326'),
  ('U327'),
  ('U328'),
  ('U329'),
  ('U330'),
  ('U331'),
  ('U332'),
  ('U333'),
  ('U334'),
  ('U335'),
  ('U336'),
  ('U337'),
  ('U338'),
  ('U339'),
  ('U340'),
  ('U341'),
  ('U342'),
  ('U343'),
  ('U344'),
  ('U345'),
  ('U346'),
  ('U347'),
  ('U348'),
  ('U349'),
  ('U350'),
  ('U351'),
  ('U352'),
  ('U353'),
  ('U354'),
  ('U355'),
  ('U356'),
  ('U357'),
  ('U358'),
  ('U359'),
  ('U360'),
  ('U361'),
  ('U362'),
  ('U363'),
  ('U364'),
  ('U401'),
  ('U402'),
  ('U403'),
  ('U404'),
  ('U405'),
  ('U406'),
  ('U407'),
  ('U408'),
  ('U409'),
  ('U410'),
  ('U411'),
  ('U412'),
  ('U413'),
  ('U414'),
  ('U415'),
  ('U416'),
  ('U501'),
  ('U502'),
  ('U503'),
  ('U504'),
  ('U505'),
  ('U506'),
  ('U507'),
  ('U508'),
  ('U509'),
  ('U510'),
  ('U511'),
  ('U512'),
  ('U513'),
  ('U514'),
  ('U515'),
  ('U516'),
  ('U517'),
  ('U518'),
  ('U519'),
  ('U520'),
  ('U521'),
  ('U522'),
  ('U523'),
  ('U524'),
  ('U525'),
  ('U526'),
  ('U601'),
  ('U602'),
  ('U603'),
  ('U604'),
  ('U605'),
  ('U606'),
  ('U607'),
  ('U608'),
  ('U609'),
  ('U610'),
  ('U611'),
  ('U612'),
  ('U613'),
  ('U614'),
  ('U615'),
  ('U616'),
  ('U617'),
  ('U618'),
  ('U619'),
  ('U620'),
  ('U621'),
  ('U622'),
  ('U623'),
  ('U624'),
  ('U625'),
  ('U626'),
  ('U627'),
  ('U628'),
  ('U629'),
  ('U630'),
  ('U631'),
  ('U632'),
  ('X101'),
  ('X102'),
  ('X103'),
  ('X104'),
  ('X105'),
  ('X106'),
  ('X107'),
  ('X108'),
  ('X109'),
  ('X110'),
  ('X111'),
  ('X112'),
  ('X113'),
  ('X114'),
  ('X115'),
  ('X116'),
  ('X117'),
  ('X118'),
  ('X201'),
  ('X202'),
  ('X203'),
  ('X204'),
  ('X205'),
  ('X206'),
  ('X207'),
  ('X208'),
  ('X209'),
  ('X210'),
  ('X211'),
  ('X212'),
  ('X213'),
  ('X214'),
  ('X215'),
  ('X216'),
  ('X217'),
  ('X218'),
  ('X219'),
  ('X220'),
  ('X221'),
  ('X222'),
  ('X223'),
  ('X224'),
  ('X225'),
  ('X226'),
  ('X227'),
  ('X228'),
  ('X229'),
  ('X230'),
  ('X231'),
  ('X232'),
  ('X301'),
  ('X302')


declare @Obj_MetaDefinicio_Id as uniqueidentifier
set @Obj_MetaDefinicio_Id = (SELECT Id FROM EREC_Obj_MetaDefinicio WHERE ContentType = 'Allamigazgatasi_hatosagi_ugy')

declare @Obj_MetaDefinicio_Id_Onk as uniqueidentifier
set @Obj_MetaDefinicio_Id_Onk = (SELECT Id FROM EREC_Obj_MetaDefinicio WHERE ContentType = 'Onkormanyzati_hatosagi_ugy')

-- Dontest_hoza_allamigazgatasi paraméter --
declare @Dontest_hoza_allamigazgatasi_OMA_ID as varchar(100)
declare @Dontest_hoza_allamigazgatasi_TSZO_ID as varchar(100)
set @Dontest_hoza_allamigazgatasi_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Dontest_hoza_allamigazgatasi')
set @Dontest_hoza_allamigazgatasi_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Dontest_hoza_allamigazgatasi')

-- Dontes_formaja_allamigazgatasi paraméter --
declare @Dontes_formaja_allamigazgatasi_TSZO_ID as varchar(100)
declare @Dontes_formaja_allamigazgatasi_OMA_ID as varchar(100)
set @Dontes_formaja_allamigazgatasi_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Dontes_formaja_allamigazgatasi')
set @Dontes_formaja_allamigazgatasi_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Dontes_formaja_allamigazgatasi')

-- Dontest_hozta paraméter (ez az önkormányzati) --
declare @Dontest_hozta_OMA_ID as varchar(100)
declare @Dontest_hozta_TSZO_ID as varchar(100)
set @Dontest_hozta_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id_Onk
					and BelsoAzonosito = 'Dontest_hozta')
set @Dontest_hozta_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id_Onk
					and BelsoAzonosito = 'Dontest_hozta')

-- Dontes_formaja_onkormanyzati paraméter --
declare @Dontes_formaja_onkormanyzati_TSZO_ID as varchar(100)
declare @Dontes_formaja_onkormanyzati_OMA_ID as varchar(100)
set @Dontes_formaja_onkormanyzati_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id_Onk
					and BelsoAzonosito = 'Dontes_formaja_onkormanyzati')
set @Dontes_formaja_onkormanyzati_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id_Onk
					and BelsoAzonosito = 'Dontes_formaja_onkormanyzati')

-- Ugyintezes_idotartama paraméter --
declare @Ugyintezes_idotartama_TSZO_ID as varchar(100)
declare @Ugyintezes_idotartama_OMA_ID as varchar(100)
set @Ugyintezes_idotartama_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Ugyintezes_idotartama')
set @Ugyintezes_idotartama_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Ugyintezes_idotartama')

-- Jogorv_eljarasban_dontes_valtozasa paraméter --
declare @Jogorv_eljarasban_dontes_valtozasa_TSZO_ID as varchar(100)
declare @Jogorv_eljarasban_dontes_valtozasa_OMA_ID as varchar(100)
set @Jogorv_eljarasban_dontes_valtozasa_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Jogorv_eljarasban_dontes_valtozasa')
set @Jogorv_eljarasban_dontes_valtozasa_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Jogorv_eljarasban_dontes_valtozasa')

-- Jogorvoslati_eljaras_tipusa paraméter --
declare @Jogorvoslati_eljaras_tipusa_TSZO_ID as varchar(100)
declare @Jogorvoslati_eljaras_tipusa_OMA_ID as varchar(100)
set @Jogorvoslati_eljaras_tipusa_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Jogorvoslati_eljaras_tipusa')
set @Jogorvoslati_eljaras_tipusa_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Jogorvoslati_eljaras_tipusa')

-- Jogorv_eljarasban_dontes_tipusa paraméter --
declare @Jogorv_eljarasban_dontes_tipusa_TSZO_ID as varchar(100)
declare @Jogorv_eljarasban_dontes_tipusa_OMA_ID as varchar(100)
set @Jogorv_eljarasban_dontes_tipusa_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Jogorv_eljarasban_dontes_tipusa')
set @Jogorv_eljarasban_dontes_tipusa_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Jogorv_eljarasban_dontes_tipusa')

-- Jogorv_eljarasban_dontest_hozta paraméter --
declare @Jogorv_eljarasban_dontest_hozta_TSZO_ID as varchar(100)
declare @Jogorv_eljarasban_dontest_hozta_OMA_ID as varchar(100)
set @Jogorv_eljarasban_dontest_hozta_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Jogorv_eljarasban_dontest_hozta')
set @Jogorv_eljarasban_dontest_hozta_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Jogorv_eljarasban_dontest_hozta')

-- Jogorv_eljarasban_dontes_tartalma paraméter --
declare @Jogorv_eljarasban_dontes_tartalma_TSZO_ID as varchar(100)
declare @Jogorv_eljarasban_dontes_tartalma_OMA_ID as varchar(100)
set @Jogorv_eljarasban_dontes_tartalma_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Jogorv_eljarasban_dontes_tartalma')
set @Jogorv_eljarasban_dontes_tartalma_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Jogorv_eljarasban_dontes_tartalma')

-- Hatosagi_ellenorzes_tortent paraméter --
declare @Hatosagi_ellenorzes_tortent_TSZO_ID as varchar(100)
declare @Hatosagi_ellenorzes_tortent_OMA_ID as varchar(100)
set @Hatosagi_ellenorzes_tortent_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Hatosagi_ellenorzes_tortent')
set @Hatosagi_ellenorzes_tortent_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Hatosagi_ellenorzes_tortent')

-- Dontes_munkaorak_szama paraméter --
declare @Dontes_munkaorak_szama_TSZO_ID as varchar(100)
declare @Dontes_munkaorak_szama_OMA_ID as varchar(100)
set @Dontes_munkaorak_szama_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Dontes_munkaorak_szama')
set @Dontes_munkaorak_szama_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Dontes_munkaorak_szama')

-- Megallapitott_eljarasi_koltseg paraméter --
declare @Megallapitott_eljarasi_koltseg_TSZO_ID as varchar(100)
declare @Megallapitott_eljarasi_koltseg_OMA_ID as varchar(100)
set @Megallapitott_eljarasi_koltseg_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Megallapitott_eljarasi_koltseg')
set @Megallapitott_eljarasi_koltseg_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Megallapitott_eljarasi_koltseg')

-- Kiszabott_kozigazgatasi_birsag paraméter --
declare @Kiszabott_kozigazgatasi_birsag_TSZO_ID as varchar(100)
declare @Kiszabott_kozigazgatasi_birsag_OMA_ID as varchar(100)
set @Kiszabott_kozigazgatasi_birsag_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Kiszabott_kozigazgatasi_birsag')
set @Kiszabott_kozigazgatasi_birsag_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Kiszabott_kozigazgatasi_birsag')

-- Sommas_eljarasban_hozott_dontes paraméter --
declare @Sommas_eljarasban_hozott_dontes_TSZO_ID as varchar(100)
declare @Sommas_eljarasban_hozott_dontes_OMA_ID as varchar(100)
set @Sommas_eljarasban_hozott_dontes_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Sommas_eljarasban_hozott_dontes')
set @Sommas_eljarasban_hozott_dontes_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Sommas_eljarasban_hozott_dontes')

-- 8napon_belul_lezart_nem_sommas paraméter --
declare @8napon_belul_lezart_nem_sommas_TSZO_ID as varchar(100)
declare @8napon_belul_lezart_nem_sommas_OMA_ID as varchar(100)
set @8napon_belul_lezart_nem_sommas_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = '8napon_belul_lezart_nem_sommas')
set @8napon_belul_lezart_nem_sommas_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = '8napon_belul_lezart_nem_sommas')

-- Fuggo_hatalyu_hatarozat paraméter --
declare @Fuggo_hatalyu_hatarozat_TSZO_ID as varchar(100)
declare @Fuggo_hatalyu_hatarozat_OMA_ID as varchar(100)
set @Fuggo_hatalyu_hatarozat_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Fuggo_hatalyu_hatarozat')
set @Fuggo_hatalyu_hatarozat_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Fuggo_hatalyu_hatarozat')

-- Hatarozat_hatalyba_lepese paraméter --
declare @Hatarozat_hatalyba_lepese_TSZO_ID as varchar(100)
declare @Hatarozat_hatalyba_lepese_OMA_ID as varchar(100)
set @Hatarozat_hatalyba_lepese_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Hatarozat_hatalyba_lepese')
set @Hatarozat_hatalyba_lepese_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Hatarozat_hatalyba_lepese')

-- Fuggo_hatalyu_vegzes paraméter --
declare @Fuggo_hatalyu_vegzes_TSZO_ID as varchar(100)
declare @Fuggo_hatalyu_vegzes_OMA_ID as varchar(100)
set @Fuggo_hatalyu_vegzes_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Fuggo_hatalyu_vegzes')
set @Fuggo_hatalyu_vegzes_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Fuggo_hatalyu_vegzes')

-- Vegzes_hatalyba_lepese paraméter --
declare @Vegzes_hatalyba_lepese_TSZO_ID as varchar(100)
declare @Vegzes_hatalyba_lepese_OMA_ID as varchar(100)
set @Vegzes_hatalyba_lepese_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Vegzes_hatalyba_lepese')
set @Vegzes_hatalyba_lepese_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Vegzes_hatalyba_lepese')

-- Hatosag_altal_visszafizetett_osszeg paraméter --
declare @Hatosag_altal_visszafizetett_osszeg_TSZO_ID as varchar(100)
declare @Hatosag_altal_visszafizetett_osszeg_OMA_ID as varchar(100)
set @Hatosag_altal_visszafizetett_osszeg_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Hatosag_altal_visszafizetett_osszeg')
set @Hatosag_altal_visszafizetett_osszeg_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Hatosag_altal_visszafizetett_osszeg')

-- Hatosagot_terhelo_eljarasi_koltseg paraméter --
declare @Hatosagot_terhelo_eljarasi_koltseg_TSZO_ID as varchar(100)
declare @Hatosagot_terhelo_eljarasi_koltseg_OMA_ID as varchar(100)
set @Hatosagot_terhelo_eljarasi_koltseg_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Hatosagot_terhelo_eljarasi_koltseg')
set @Hatosagot_terhelo_eljarasi_koltseg_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Hatosagot_terhelo_eljarasi_koltseg')

-- Felfuggesztett_hatarozat paraméter --
declare @Felfuggesztett_hatarozat_TSZO_ID as varchar(100)
declare @Felfuggesztett_hatarozat_OMA_ID as varchar(100)
set @Felfuggesztett_hatarozat_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Felfuggesztett_hatarozat')
set @Felfuggesztett_hatarozat_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Felfuggesztett_hatarozat')

-- Hatarido_tullepes_napokban paraméter --
declare @Hatarido_tullepes_napokban_TSZO_ID as varchar(100)
declare @Hatarido_tullepes_napokban_OMA_ID as varchar(100)
set @Hatarido_tullepes_napokban_TSZO_ID = (select t.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Hatarido_tullepes_napokban')
set @Hatarido_tullepes_napokban_OMA_ID = (select oma.Id
					from EREC_Obj_MetaAdatai oma
					JOIN EREC_TargySzavak t on oma.Targyszavak_Id = t.Id
					where Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
					and BelsoAzonosito = 'Hatarido_tullepes_napokban')

create table #meta
(
	ugyfajtaja char,
	stat_adat_tipus_azon nvarchar(64),
	ertek int,
	mod_date datetime,
	irat_id uniqueidentifier
)

insert into #meta
-- államigazgatási hatósági ügyekben hozott döntések száma --
select '1', '1_2' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontest_hoza_allamigazgatasi_OMA_ID
		and   Targyszo_Id       = @Dontest_hoza_allamigazgatasi_TSZO_ID  
-- államigazgatási hatósági eljárásban a (fő)polgármester hozta a döntést --
union select '1', '1_3' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontest_hoza_allamigazgatasi_OMA_ID
		and   Targyszo_Id       = @Dontest_hoza_allamigazgatasi_TSZO_ID
		and   Ertek = '4'  
-- államigazgatási hatósági eljárásban a (fő)jegyző hozta a döntést --
union select '1', '1_4' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontest_hoza_allamigazgatasi_OMA_ID
		and   Targyszo_Id       = @Dontest_hoza_allamigazgatasi_TSZO_ID
		and   Ertek = '5'  
-- államigazgatási hatósági eljárásban az ügyintező hozta a döntést --
union select '1', '1_5' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontest_hoza_allamigazgatasi_OMA_ID
		and   Targyszo_Id       = @Dontest_hoza_allamigazgatasi_TSZO_ID
		and   Ertek = '3'  
-- államigazgatási hatósági eljárásban önálló határozatok száma --
union select '1', '1_6' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_allamigazgatasi_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_allamigazgatasi_TSZO_ID
		and   Ertek = '1'  
-- államigazgatási hatósági eljárásban egyezség jóváhagyását tartalmazó határozatok száma --
union select '1', '1_7' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_allamigazgatasi_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_allamigazgatasi_TSZO_ID
		and   Ertek = '2'  
-- államigazgatási hatósági eljárásban hatósági bizonyítványok/hatósági igazolványok száma --
union select '1', '1_8' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_allamigazgatasi_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_allamigazgatasi_TSZO_ID
		and   Ertek = '3'  
-- államigazgatási hatósági eljárásban hatósági szerződések száma --
union select '1', '1_9' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_allamigazgatasi_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_allamigazgatasi_TSZO_ID
		and   Ertek = '4'  
-- államigazgatási hatósági eljárásban kérelem visszautasítások száma --
union select '1', '1_10' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_allamigazgatasi_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_allamigazgatasi_TSZO_ID
		and   Ertek = '5'  
-- államigazgatási hatósági eljárásban az Ákr. 47. § (1) a)-f) alapján történő eljárást lezáró végzések száma -- 
union select '1', '1_11' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_allamigazgatasi_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_allamigazgatasi_TSZO_ID
		and   Ertek = '6' 
-- államigazgatási hatósági eljárásban az Ákr. 47. § (1) g) alapján történő eljárást lezáró végzések száma --
union select '1', '1_12' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_allamigazgatasi_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_allamigazgatasi_TSZO_ID
		and   Ertek = '9'  
-- államigazgatási hatósági eljárásban az elsőfokú eljárásban hozott egyéb végzések száma --
union select '1', '1_13' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_allamigazgatasi_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_allamigazgatasi_TSZO_ID
		and   Ertek = '7'  
-- államigazgatási hatósági eljárásban végrehajtási eljárásban hozott végzések száma --
union select '1', '1_14' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_allamigazgatasi_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_allamigazgatasi_TSZO_ID
		and   Ertek = '8'  
-- államigazgatási hatósági eljárásban határidőn belül hozott döntések száma
union select '1', '1_15' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate		
		and   Targyszo_Id       = @Ugyintezes_idotartama_TSZO_ID
		and   (Ertek = 'IGEN' or Ertek = '1') 
-- államigazgatási hatósági eljárásban határidőn túl hozott döntések száma
union select '1', '1_16' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Targyszo_Id       = @Ugyintezes_idotartama_TSZO_ID
		and   (Ertek = 'NEM' or Ertek = '2')  
-- államigazgatási hatósági eljárásban kijavított vagy kiegészített döntések száma
union select '1', '1_17' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_valtozasa_TSZO_ID  
-- államigazgatási hatósági ügyekben kérelem alapján hozott vagy megtámadott  elsőfokú döntések száma
union select '1', '1_18' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'  
-- államigazgatási hatósági ügyekben fellebezés alapján módosított vagy visszavont elsőfokú döntések száma
union select '1', '1_19' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '2'  
-- államigazgatási hatósági ügyekben kérelem alapján a kormányhivatal által helybenhagyott végzésekkel szembeni jogorvoslatok száma
union select '1', '1_20',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_20' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
	union select '1', '1_20', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
			and   Ertek = '1'
	union select '1', '1_20', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
			and   Ertek = '5'
	union select '1', '1_20', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
			and   Ertek = '5'   
	) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a kormányhivatal által megváltoztatott végzésekkel szembeni jogorvoslatok száma
union select '1', '1_21',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_21' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
	union select '1', '1_21', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
			and   Ertek = '1'
	union select '1', '1_21', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
			and   Ertek = '5'
	union select '1', '1_21', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
			and   Ertek = '6'     
	) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a kormányhivatal által megsemmisített végzésekkel szembeni jogorvoslatok száma
union select '1', '1_22',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_22' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
	union select '1', '1_22', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
			and   Ertek = '1'
	union select '1', '1_22', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
			and   Ertek = '5'
	union select '1', '1_22', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
			and   Ertek = '7'
	) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id 
-- államigazgatási hatósági ügyekben kérelem alapján a kormányhivatal által megsemmisített és új eljárásra utasított végzésekkel szembeni jogorvoslatok száma
union select '1', '1_23',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_23' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
			and   Ertek = '1'
	union select '1', '1_23', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
			and   Ertek = '1'
	union select '1', '1_23', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
			and   Ertek = '5'
	union select '1', '1_23', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
			and   Ertek = '8'   
	) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a dekoncentrált szerv által helybenhagyott végzésekkel szembeni jogorvoslatok száma
union select '1', '1_24',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_24' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
			and   Ertek = '1'
	union select '1', '1_24', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
			and   Ertek = '1'
	union select '1', '1_24', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
			and   Ertek = '6'
	union select '1', '1_24', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
			and   Ertek = '5'
			) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a dekoncentrált szerv által megváltoztatott végzésekkel szembeni jogorvoslatok száma
union select '1', '1_25',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_25' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where 1=1
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_25', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_25', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '6'
union select '1', '1_25', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '6'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id   
-- államigazgatási hatósági ügyekben kérelem alapján a dekoncentrált szerv által megsemmisített végzésekkel szembeni jogorvoslatok száma
union select '1', '1_26',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_26' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_26', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_26', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '6'
union select '1', '1_26', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '7'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id   
-- államigazgatási hatósági ügyekben kérelem alapján a dekoncentrált szerv által megsemmisített és új eljárásra utasított végzésekkel szembeni jogorvoslatok száma
union select '1', '1_27',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_27' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		

			where 1=1
			and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
			and   Ertek = '1'
	union select '1', '1_27', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
		
			and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
			and   Ertek = '1'
	union select '1', '1_27', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
			and   Ertek = '6'
	union select '1', '1_27', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
			where 1=1
			and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
			and   Ertek = '8'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id   
-- államigazgatási hatósági ügyekben kérelem alapján a bíróság által végzésekkel szembeni elutasított a kérelmet/az eljárást megszüntettő jogorvoslatok száma
union select '1', '1_28',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_28' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where 1=1
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_28', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_28', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '1', '1_28', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '5'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a bíróság által végzésekkel szembeni megváltoztatott jogorvoslatok száma
union select '1', '1_29',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_29' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where 1=1
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_29', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_29', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '1', '1_29', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '6'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a bíróság által végzésekkel szembeni hatályon kívül helyezett jogorvoslatok száma
union select '1', '1_30',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_30' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where 1=1
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_30', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_30', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where 1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '1', '1_30', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '7'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a bíróság által végzésekkel szembeni hatályon kívül helyezett és új eljárásra utasított jogorvoslatok száma
union select '1', '1_31',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_31' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		

		where  1=1
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_31', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_31', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '1', '1_31', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '8'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a kormányhivatal által helybenhagyott érdemi döntésekkel szembeni jogorvoslatok száma
union select '1', '1_32',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_32' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		

		where  1=1
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_32', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_32', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '5'
union select '1', '1_32', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '5'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a kormányhivatal által megváltoztatott érdemi döntésekkel szembeni jogorvoslatok száma
union select '1', '1_33',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_33' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_33', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_33', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '5'
union select '1', '1_33', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '6'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a kormányhivatal által megsemmisített érdemi döntésekkel szembeni jogorvoslatok száma
union select '1', '1_34',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_34' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_34', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_34', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '5'
union select '1', '1_34', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '7'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a kormányhivatal által megsemmisített és új eljárásra utasított érdemi döntésekkel szembeni jogorvoslatok száma
union select '1', '1_35',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_35' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_35', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_35', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '5'
union select '1', '1_35', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '8'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a dekoncentrált szerv által helybenhagyott érdemi döntésekkel szembeni jogorvoslatok száma
union select '1', '1_36',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_36' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_36', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_36', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '6'
union select '1', '1_36', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '5'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a dekoncentrált szerv által megváltoztatott érdemi döntésekkel szembeni jogorvoslatok száma
union select '1', '1_37',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_37' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_37', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_37', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '6'
union select '1', '1_37', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '6'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a dekoncentrált szerv által megsemmisített érdemi döntésekkel szembeni jogorvoslatok száma
union select '1', '1_38',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_38' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_38', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_38', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '6'
union select '1', '1_38', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '7'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a dekoncentrált szerv által megsemmisített és új eljárásra utasított érdemi döntésekkel szembeni jogorvoslatok száma
union select '1', '1_39',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_39' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_39', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_39', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '6'
union select '1', '1_39', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '8'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a bíróság által érdemi döntésekkel szembeni elutasított a keresetet / a pert megszüntettő döntések száma
union select '1', '1_40',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_40' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		

		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_40', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_40', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '1', '1_40', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '5'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a bíróság által érdemi döntésekkel szembeni megváltoztatott érdemi döntésekkel szembeni döntések száma
union select '1', '1_41',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_41' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_41', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_41', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '1', '1_41', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '6'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a bíróság által érdemi döntésekkel szembeni hatályon kívül helyezett döntések száma
union select '1', '1_42',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_42' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_42', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_42', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '1', '1_42', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '7'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben kérelem alapján a bíróság által érdemi döntésekkel szembeni  hatályon kívül helyezett és új eljárásra utasított  döntések száma
union select '1', '1_43',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_43' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '1', '1_43', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_43', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '1', '1_43', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '8'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben hivatalból módosított vagy visszavont elsőfokú döntések
union select '1', '1_48',  CASE WHEN SUM(ertek) = 3 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_48' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_48', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '1'
union select '1', '1_48', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   (Ertek = '2' or Ertek = '3')
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben hivatalból a felügyeleti szerv által megváltoztatott jogorvoslatok száma
union select '1', '1_49',  CASE WHEN SUM(ertek) = 3 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_49' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_49', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '8'
union select '1', '1_49', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   (Ertek = '6')
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben hivatalból a felügyeleti szerv által megsemmisített jogorvoslatok száma
union select '1', '1_50',  CASE WHEN SUM(ertek) = 3 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_50' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_50', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '8'
union select '1', '1_50', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   (Ertek = '7')
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben hivatalból a felügyeleti szerv által megsemmisített és új eljárásra utasított jogorvoslatok száma
union select '1', '1_51',  CASE WHEN SUM(ertek) = 3 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '1' as ugyfajtaja, '1_51' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '2'
union select '1', '1_51', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '8'
union select '1', '1_51', 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   (Ertek = '8')
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- államigazgatási hatósági ügyekben a hatósági ellenőrzések száma
union select '1', '1_52',  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Hatosagi_ellenorzes_tortent_TSZO_ID
		and   Ertek = '1'
-- államigazgatási hatósági ügyekben az egy ügyre fordított munkaórák száma átlagosan
union select '1', '1_53',  convert(decimal, replace(isnull(Ertek,0),',','.')) , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Dontes_munkaorak_szama_TSZO_ID
-- államigazgatási hatósági ügyekben az egy ügyre jutó átlagos eljárási költség (Ft)
union select '1', '1_54',  Ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Megallapitott_eljarasi_koltseg_TSZO_ID
-- államigazgatási hatósági ügyekben az eljárásban kiszabott közigazgatási bírság átlagos mértéke (Ft)
union select '1', '1_55' ,  Ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Kiszabott_kozigazgatasi_birsag_TSZO_ID

-- 1_56 államigazgatási hatósági ügyekben az előző félévről áthúzódó lezárt eljárások száma
-- 1_57 államigazgatási hatósági ügyekben az előző félévről áthúzódó folyamatban lévő eljárások száma
-- 1_58 államigazgatási hatósági ügyekben a megismételt lezárt eljárások száma
-- 1_59 államigazgatási hatósági ügyekben a megismételt folyamatban lévő eljárások száma
-- 1_60 államigazgatási hatósági ügyekben a tárgyfélévben indult lezárt eljárások száma
-- 1_61 államigazgatási hatósági ügyekben a tárgyfélévben indult folyamatban lévő eljárások száma

-- államigazgatási hatósági ügyekben  a sommás eljárások száma
union select '1', '1_62' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Sommas_eljarasban_hozott_dontes_TSZO_ID
		and   (Ertek = 'IGEN' or Ertek = '1')
-- államigazgatási hatósági ügyekben a 8 napon belüli lezárt, nem sommás eljárások száma
union select '1', '1_63' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @8napon_belul_lezart_nem_sommas_TSZO_ID
		and   (Ertek = 'IGEN' or Ertek = '1')
-- államigazgatási hatósági ügyekben az összes függő hatályú határozat száma
union select '1', '1_64' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Fuggo_hatalyu_hatarozat_TSZO_ID
		and   (Ertek = 'IGEN' or Ertek = '1')
-- államigazgatási hatósági ügyekben az összes függő hatályú végzés száma
union select '1', '1_65' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Fuggo_hatalyu_vegzes_TSZO_ID
		and   (Ertek = 'IGEN' or Ertek = '1')
-- államigazgatási hatósági ügyekben a nem hatályba lépett határozat száma
union select '1', '1_66' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Hatarozat_hatalyba_lepese_TSZO_ID
		and   (Ertek = 'NEM' or Ertek = '2')
-- államigazgatási hatósági ügyekben a hatályba lépett határozat száma
union select '1', '1_67' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Hatarozat_hatalyba_lepese_TSZO_ID
		and   (Ertek = 'IGEN' or Ertek = '1')
-- államigazgatási hatósági ügyekben a nem hatályba lépett végzések száma
union select '1', '1_68' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Vegzes_hatalyba_lepese_TSZO_ID
		and   (Ertek = 'NEM' or Ertek = '2')
-- államigazgatási hatósági ügyekben a hatályba lépett végzések száma
union select '1', '1_69' ,  1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Vegzes_hatalyba_lepese_TSZO_ID
		and   (Ertek = 'IGEN' or Ertek = '1')
-- államigazgatási hatósági ügyekben az Ákr. 43. § (2) a) alapján a hatóság által visszafizetett összeg (Ft)
union select '1', '1_70' ,  Ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Hatosag_altal_visszafizetett_osszeg_TSZO_ID
-- államigazgatási hatósági ügyekben az  Ákr. 43. § (2) b) alapján a hatóságot terhelő eljárási költség összege (Ft)
union select '1', '1_71' ,  Ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Hatosagot_terhelo_eljarasi_koltseg_TSZO_ID

/* ÖNKORMÁNYZATI ÜGYTÍPUSOK*/
-- 2018_2_2	önkormányzati hatósági ügyekben hozott döntések száma
union select '2', '2_2' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontest_hozta_OMA_ID
		and   Targyszo_Id       = @Dontest_hozta_TSZO_ID

-- 2018_2_3	önkormányzati hatósági eljárásban a képviselő-testület hozta a döntést
union select '2', '2_3'  , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontest_hozta_OMA_ID
		and   Targyszo_Id       = @Dontest_hozta_TSZO_ID
		and   Ertek = '1'

-- 2018_2_4	önkormányzati hatósági eljárásban a bizottság hozta a döntést
union select '2', '2_4' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontest_hozta_OMA_ID
		and   Targyszo_Id       = @Dontest_hozta_TSZO_ID
		and   Ertek = '2'

-- 2018_2_5	önkormányzati hatósági eljárásban a (fő)polgármester hozta a döntést
union select '2', '2_5' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontest_hozta_OMA_ID
		and   Targyszo_Id       = @Dontest_hozta_TSZO_ID
		and   Ertek = '4'

-- 2018_2_6	önkormányzati hatósági eljárásban a részönkormányzat testülete hozta a döntést
union select '2', '2_6' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontest_hozta_OMA_ID
		and   Targyszo_Id       = @Dontest_hozta_TSZO_ID
		and   Ertek = '6'  

-- 2018_2_7	önkormányzati hatósági eljárásban a (fő)jegyző hozta a döntést
union select '2', '2_7' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontest_hozta_OMA_ID
		and   Targyszo_Id       = @Dontest_hozta_TSZO_ID
		and   Ertek = '5'  

-- 2018_2_8	önkormányzati hatósági eljárásban a társulási tanács hozta a döntést
union select '2', '2_8' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontest_hozta_OMA_ID
		and   Targyszo_Id       = @Dontest_hozta_TSZO_ID
		and   Ertek = '7'  

-- 2018_2_9	önkormányzati hatósági eljárásban önálló határozatok száma
union select '2', '2_9' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_onkormanyzati_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_onkormanyzati_TSZO_ID
		and   Ertek = '1'  

-- 2018_2_10	önkormányzati hatósági eljárásban egyezség jóváhagyását tartalmazó határozatok száma
union select '2', '2_10' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_onkormanyzati_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_onkormanyzati_TSZO_ID
		and   Ertek = '2'  
-- 2018_2_11	önkormányzati hatósági eljárásban hatósági bizonyítványok száma
-- önkormányzati hatósági eljárásban hatósági bizonyítványok/hatósági igazolványok száma
union select '2', '2_11' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_onkormanyzati_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_onkormanyzati_TSZO_ID
		and   Ertek = '3'  
-- 2018_2_12	önkormányzati hatósági eljárásban hatósági szerződések száma
union select '2', '2_12' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_onkormanyzati_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_onkormanyzati_TSZO_ID
		and   Ertek = '4'
-- 2018_2_13	önkormányzati hatósági eljárásbanaz Ákr. 46. § alapján történő elutasítások száma
-- önkormányzati hatósági eljárásban kérelem visszautasítások száma
union select '2', '2_13' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_onkormanyzati_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_onkormanyzati_TSZO_ID
		and   Ertek = '5' 
-- 2018_2_14	önkormányzati hatósági eljárásban az Ákr. 47. § (1) a), c), d), e) és (2) alapján történő megszüntetések száma
-- önkormányzati hatósági eljárásban az Ákr. 47. § (1) a)-f) alapján történő az eljárást lezáró végzések száma
union select '2', '2_14' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_onkormanyzati_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_onkormanyzati_TSZO_ID
		and   Ertek = '6' 
-- 2018_2_15	önkormányzati hatósági eljárásban az Ákr. 47. § (1) g) alapján történő megszüntetések
-- önkormányzati hatósági eljárásban az Ákr. 47. § (1) g) alapján eljárást lezáró végzések száma
union select '2', '2_15' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_onkormanyzati_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_onkormanyzati_TSZO_ID
		and   Ertek = '9' 
-- 2018_2_16	önkormányzati hatósági eljárásban az alsőfokú eljárásban hozott egyéb végzésel száma
-- önkormányzati hatósági eljárásban az elsőfokú eljárásban hozott egyéb végzésel száma
union select '2', '2_16' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_onkormanyzati_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_onkormanyzati_TSZO_ID
		and   Ertek = '7' 
-- 2018_2_17	önkormányzati hatósági eljárásban végrehajtási eljárásban hozott végzések száma
union select '2', '2_17' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		and   Obj_Metaadatai_Id = @Dontes_formaja_onkormanyzati_OMA_ID
		and   Targyszo_Id       = @Dontes_formaja_onkormanyzati_TSZO_ID
		and   Ertek = '8' 
-- 2018_2_18	önkormányzati hatósági eljárásban határidőn belül hozott döntések száma
union select '2', '2_18' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Ugyintezes_idotartama_TSZO_ID
		and   (Ertek = 'IGEN' or Ertek = '1')  
-- 2018_2_19	önkormányzati hatósági eljárásban határidőn túl hozott döntések száma
union select '2', '2_19' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Ugyintezes_idotartama_TSZO_ID
		and   (Ertek = 'NEM' or Ertek = '2') 
-- 2018_2_20	önkormányzati hatósági eljárásban  kijavított vagy kiegészített döntések száma
union select '2', '2_20' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_valtozasa_TSZO_ID 
-- 2018_2_21	államigazgatási hatósági ügyekben kérelem alapján hozott vagy megtámadott  elsőfokú döntések száma
-- önkormányzati hatósági ügyekben hozott, megtámadott  elsőfokú döntések száma
union select '2', '2_21' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'  
-- 2018_2_22	önkormányzati hatósági ügyekben fellebezés alapján módosított vagy visszavont elsőfokú döntések száma
-- önkormányzati hatósági ügyekben fellebbezés/ keresetlevél alapján módosított vagy visszavont elsőfokú döntések száma
union select '2', '2_22' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '2' 
-- 2018_2_23	önkormányzati hatósági ügyekben kérelem alapján a képviselő-testület által helybenhagyott végzésekkel szembeni jogorvoslatok száma
union select '2', '2_23',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_23' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_23' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_23' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '4'
union select '2', '2_23' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '5'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- 2018_2_24	önkormányzati hatósági ügyekben kérelem alapján a képviselő-testület által megváltoztatott végzésekkel szembeni jogorvoslatok száma
union select '2', '2_24',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_24' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_24' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_24' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '4'
union select '2', '2_24' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '6'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- 2018_2_25	önkormányzati hatósági ügyekben kérelem alapján a képviselő-testület által megsemmisített végzésekkel szembeni jogorvoslatok száma
union select '2', '2_25',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_25' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_25' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_25' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '4'
union select '2', '2_25' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '7'	
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id	
-- 2018_2_26	önkormányzati hatósági ügyekben kérelem alapján a képviselő-testület által megsemmisített és új eljárásra utasított végzésekkel szembeni jogorvoslatok száma
union select '2', '2_26',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_26' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_26' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_26' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '4'
union select '2', '2_26' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '8'  
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id 
-- 2018_2_27	önkormányzati hatósági ügyekben kérelem alapján nemperes eljárásban a bíróság által végzésekkel szembeni elutasított jogorvoslatok száma
-- önkormányzati hatósági ügyekben kérelem alapján a bíróság által végzésekkel szembeni elutasította a kérelmet/az eljárást megszüntettő jogorvoslatok száma
union select '2', '2_27',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_27' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_27' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_27' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '2', '2_27' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '5'  
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id 
-- 2018_2_28	önkormányzati hatósági ügyekben kérelem alapján nemperes eljárásban a bíróság által végzésekkel szembeni megváltoztatott jogorvoslatok száma
-- önkormányzati hatósági ügyekben kérelem alapján a bíróság által végzésekkel szembeni megváltoztatott jogorvoslatok száma
union select '2', '2_28',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_28' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_28' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_28' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '2', '2_28' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '6' 
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- 2018_2_29	önkormányzati hatósági ügyekben kérelem alapján nemperes eljárásban a bíróság által végzésekkel szembeni hatályon kívül helyezett jogorvoslatok száma
-- önkormányzati hatósági ügyekben kérelem alapján a bíróság által végzésekkel szembeni hatályon kívül helyezett jogorvoslatok száma
union select '2', '2_29',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_29' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_29' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_29' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '2', '2_29' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '7' 
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- 2018_2_30	önkormányzati hatósági ügyekben kérelem alapján nemperes eljárásban a bíróság által végzésekkel szembeni hatályon kívül helyezett és új eljárásra utasított jogorvoslatok száma
-- önkormányzati hatósági ügyekben kérelem alapján a bíróság által végzésekkel szembeni hatályon kívül helyezett és új eljárásra utasított jogorvoslatok száma
union select '2', '2_30',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_30' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_30' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_30' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '2', '2_30' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '8' 
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- önkormányzati hatósági ügyekben kérelem alapján a képviselő-testület által helybenhagyott érdemi döntésekkel szembeni érdemi döntések száma
union select '2', '2_31',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_31' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_31' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '2', '2_31' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '4'
union select '2', '2_31' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '5' 
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- önkormányzati hatósági ügyekben kérelem alapján a képviselő-testület által megváltoztatott érdemi döntésekkel szembeni érdemi döntések száma
union select '2', '2_32',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_32' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_32' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '2', '2_32' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '4'
union select '2', '2_32' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '6'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- önkormányzati hatósági ügyekben kérelem alapján a képviselő-testület által megsemmisített érdemi döntésekkel szembeni érdemi döntések száma
union select '2', '2_33',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_33' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_33' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '2', '2_33' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '4'
union select '2', '2_33' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '7'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id		
-- önkormányzati hatósági ügyekben kérelem alapján a képviselő-testület által megsemmisített és új eljárásra utasított érdemi döntésekkel szembeni érdemi döntések száma
union select '2', '2_34',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_34' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_34' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '2', '2_34' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '4'
union select '2', '2_34' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '8'  
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id 
-- önkormányzati hatósági ügyekben kérelem alapján a bíróság által végzésekkel szembeni visszautasított a keresetlevelet/az eljárást megszüntettő érdemi döntések száma
union select '2', '2_35',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_35' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_35' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '2', '2_35' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '2', '2_35' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '5'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id   
-- önkormányzati hatósági ügyekben kérelem alapján a bíróság által végzésekkel szembeni megváltoztatott érdemi döntések száma
-- 2018_2_28	önkormányzati hatósági ügyekben kérelem alapján nemperes eljárásban a bíróság által végzésekkel szembeni megváltoztatott jogorvoslatok száma
union select '2', '2_36',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_36' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_36' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '2', '2_36' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '2', '2_36' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '6' 
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- önkormányzati hatósági ügyekben kérelem alapján a bíróság által végzésekkel szembeni hatályon kívül helyezett érdemi döntések száma
union select '2', '2_37',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_37' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_37' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '2', '2_37' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '2', '2_37' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '7' 
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- önkormányzati hatósági ügyekben kérelem alapján a bíróság által végzésekkel szembeni hatályon kívül helyezett és új eljárásra utasított érdemi döntések száma
union select '2', '2_38',  CASE WHEN SUM(ertek) = 4 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_38' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '1'
union select '2', '2_38' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tipusa_TSZO_ID
		and   Ertek = '2'
union select '2', '2_38' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '7'
union select '2', '2_38' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '8' 
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- önkormányzati hatósági ügyekben hivatalból módosított vagy visszavont elsőfokú döntések
union select '2', '2_39',  CASE WHEN SUM(ertek) = 3 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_39' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '2'
union select '2', '2_39' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '1'
union select '2', '2_39' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   (Ertek = '2' or Ertek = '3')
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- önkormányzati hatósági ügyekben hivatalból a felügyeleti szerv által megváltoztatott jogorvoslatok száma
union select '2', '2_40',  CASE WHEN SUM(ertek) = 3 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_40' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '2'
union select '2', '2_40' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '8'
union select '2', '2_40' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '6'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- 2018_2_41	önkormányzati hatósági ügyekben hivatalból a felügyeleti szerv által megsemmisített jogorvoslatok száma
union select '2', '2_41',  CASE WHEN SUM(ertek) = 3 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_41' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '2'
union select '2', '2_41' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '8'
union select '2', '2_41' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '7'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- önkormányzati hatósági ügyekben hivatalból a felügyeleti szerv által megsemmisített és új eljárásra utasított jogorvoslatok száma
union select '2', '2_42',  CASE WHEN SUM(ertek) = 3 THEN 1 ELSE 0 END, max(mod_date) as mod_date, irat_id from (
	select '2' as ugyfajtaja, '2_42' as stat_adat_tipus_azon,  1 as ertek , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)		
		where  1=1
		
		and   Targyszo_Id       = @Jogorvoslati_eljaras_tipusa_TSZO_ID
		and   Ertek = '2'
union select '2', '2_42' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontest_hozta_TSZO_ID
		and   Ertek = '8'
union select '2', '2_42' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where  1=1
		
		and   Targyszo_Id       = @Jogorv_eljarasban_dontes_tartalma_TSZO_ID
		and   Ertek = '8'
		) a
	group by a.ugyfajtaja, a.stat_adat_tipus_azon, irat_id
-- önkormányzati hatósági ügyekben a hatósági ellenőrzések száma
union select '2', '2_43' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Hatosagi_ellenorzes_tortent_TSZO_ID
		and   Ertek = '1'
-- önkormányzati hatósági ügyekben az egy ügyre fordított munkaórák száma átlagosan
union select '2', '2_44' , convert(decimal, replace(isnull(Ertek,0),',','.')) , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Dontes_munkaorak_szama_TSZO_ID
-- önkormányzati hatósági ügyekben az egy ügyre jutó átlagos eljárási költség (Ft)
union select '2', '2_45' , convert(decimal, replace(isnull(Ertek,0),',','.')) , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Megallapitott_eljarasi_koltseg_TSZO_ID
-- önkormányzati hatósági ügyekben az eljárásban kiszabott közigazgatási bírság átlagos mértéke (Ft)
union select '2', '2_46' , convert(decimal, replace(isnull(Ertek,0),',','.')) , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Kiszabott_kozigazgatasi_birsag_TSZO_ID
-- 2018_2_47	önkormányzati hatósági ügyekben az előző félévről áthúzódó lezárt eljárások száma
-- 2018_2_48	önkormányzati hatósági ügyekben az előző félévről áthúzódó folyamatban lévő eljárások száma
-- 2018_2_49	önkormányzati hatósági ügyekben a megismételt lezárt eljárások száma
-- 2018_2_50	önkormányzati hatósági ügyekben a megismételt folyamatban lévő eljárások száma
-- 2018_2_51	önkormányzati hatósági ügyekben a tárgyfélévben indult lezárt eljárások száma
-- 2018_2_52	önkormányzati hatósági ügyekben a tárgyfélévben indult folyamatban lévő eljárások száma
-- 2018_2_53	önkormányzati hatósági ügyekben  a sommás eljárások száma
union select '2', '2_53' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Sommas_eljarasban_hozott_dontes_TSZO_ID
		and   (Ertek = 'IGEN' or Ertek = '1')
-- 2018_2_58	önkormányzati hatósági ügyekben a 8 napon belüli lezárt, nem sommás eljárások száma
union select '2', '2_54' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @8napon_belul_lezart_nem_sommas_TSZO_ID
		and   (Ertek = 'IGEN' or Ertek = '1')
-- 2018_2_59	önkormányzati hatósági ügyekben az összes függő hatályú határozat száma
union select '2', '2_55' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Fuggo_hatalyu_hatarozat_TSZO_ID
		and   (Ertek = 'IGEN' or Ertek = '1')
-- 2018_2_60	önkormányzati hatósági ügyekben az összes függő hatályú végzés száma
union select '2', '2_56' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Fuggo_hatalyu_vegzes_TSZO_ID
		and   (Ertek = 'IGEN' or Ertek = '1')
-- 2018_2_61	önkormányzati hatósági ügyekben a nem hatályba lépett határozat száma
union select '2', '2_57' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Hatarozat_hatalyba_lepese_TSZO_ID
		and   (Ertek = 'NEM' or Ertek = '2')
-- 2018_2_62	önkormányzati hatósági ügyekben a hatályba lépett határozat száma
union select '2', '2_58' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Hatarozat_hatalyba_lepese_TSZO_ID
		and   (Ertek = 'IGEN' or Ertek = '1')
-- 2018_2_63	önkormányzati hatósági ügyekben a nem hatályba lépett végzések száma
union select '2', '2_59' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Vegzes_hatalyba_lepese_TSZO_ID
		and   (Ertek = 'NEM' or Ertek = '2')
-- 2018_2_64	önkormányzati hatósági ügyekben a hatályba lépett végzések száma
union select '2', '2_60' , 1 , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Vegzes_hatalyba_lepese_TSZO_ID
		and   (Ertek = 'IGEN' or Ertek = '1')
-- 2018_2_65	önkormányzati hatósági ügyekben az Ákr. 43. § (2) a) alapján a hatóság által visszafizetett összeg (Ft)
union select '2', '2_61' , convert(decimal, replace(isnull(Ertek,0),',','.')) , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Hatosag_altal_visszafizetett_osszeg_TSZO_ID
-- 2018_2_66	önkormányzati hatósági ügyekben az  Ákr. 43. § (2) b) alapján a hatóságot terhelő eljárási költség összege (Ft)
union select '2', '2_62' , convert(decimal, replace(isnull(Ertek,0),',','.')) , isnull(ModositasIdo, LetrehozasIdo) as mod_date, Obj_Id as irat_id from EREC_ObjektumTargyszavai WITH (NOLOCK)
		where isnull(ModositasIdo, LetrehozasIdo) between @startDate and @endDate
		
		and   Targyszo_Id       = @Hatosagot_terhelo_eljarasi_koltseg_TSZO_ID

select 
ik.Iktatohely as iktatokonyvek_elotag_azon
, ik.Nev as iktatokonyvek_elotag_nev
, CASE 
	WHEN i.Ugy_Fajtaja = 1 THEN 2
	WHEN i.Ugy_Fajtaja = 2 THEN 1
  END
as stat_tipus_azon
, u.Foszam as foszam
, i.Alszam as alszam
, ik.Ev as ev
, it.IrattariTetelszam as ugykor_azon
, CASE
	WHEN LEFT(meta.stat_adat_tipus_azon,1) = 1 THEN
		'2' + RIGHT(meta.stat_adat_tipus_azon,LEN(meta.stat_adat_tipus_azon)-1)
    WHEN LEFT(meta.stat_adat_tipus_azon,1) = 2 THEN
		'1' + RIGHT(meta.stat_adat_tipus_azon,LEN(meta.stat_adat_tipus_azon)-1)
  END 
as stat_adat_tipus_azon
, left(a.Kod,1) as agazat_azon
, meta.ertek as ertek
FROM EREC_UgyUgyiratok u  WITH (NOLOCK)
inner join EREC_IraIktatokonyvek ik  WITH (NOLOCK) on ik.id = u.IraIktatokonyv_Id 
inner join EREC_IraIratok i  WITH (NOLOCK) on u.id = i.Ugyirat_id AND i.Ugy_Fajtaja in ('1','2') AND i.IktatasDatuma > '2018-01-01' AND Foszam IS NOT NULL AND i.Alszam IS NOT NULL  -- kiszürjük az MU-t és MI-t
inner join EREC_IraIrattariTetelek it  WITH (NOLOCK) on it.id = u.IraIrattariTetel_Id
inner join #ugykor_azon_valid uva on uva.ugykor_azon collate database_default = it.IrattariTetelszam collate database_default
inner join EREC_AgazatiJelek a  WITH (NOLOCK) on a.id = it.AgazatiJel_Id
inner join 
(select irat_id, ugyfajtaja, stat_adat_tipus_azon, CASE WHEN SUM(ertek) = COUNT(ertek) THEN 1 ELSE 0 END as ertek from #meta group by irat_id, ugyfajtaja, stat_adat_tipus_azon) meta on meta.irat_id = i.Id and meta.ugyfajtaja collate database_default = i.Ugy_Fajtaja collate database_default
where 1=1
--order by iktatokonyvek_elotag_azon, foszam, alszam

If(OBJECT_ID('tempdb..#meta') Is Not Null)
Begin
    Drop Table #meta
End

If(OBJECT_ID('tempdb..#ugykor_azon_valid') Is Not Null)
Begin
    Drop Table #ugykor_azon_valid
End

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


GO