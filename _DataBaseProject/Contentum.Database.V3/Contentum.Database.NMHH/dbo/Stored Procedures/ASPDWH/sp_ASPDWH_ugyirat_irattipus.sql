DROP PROCEDURE [dbo].[sp_ASPDWH_ugyirat_irattipus]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_ASPDWH_ugyirat_irattipus]
  @startDate datetime,
  @endDate datetime
as

begin

BEGIN TRY



select
	distinct kt.Kod irat_tipus_azon,
	kt.Nev irat_tipus_nev
FROM 
	KRT_KodTarak kt
where 
	kt.KodCsoport_Id = '272DC3FD-4A8D-4EA2-88ED-C795A55D0373' -- IRATTIPUS kódcsoport
	and isnull(kt.ModositasIdo,kt.ErvKezd) between @startDate and @endDate and kt.ErvVege > @endDate


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
GO