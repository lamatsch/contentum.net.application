DROP PROCEDURE [dbo].[sp_ASPDWH_ugyirat_ugy]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_ASPDWH_ugyirat_ugy]
  @startDate datetime,
  @endDate datetime
as

begin

BEGIN TRY

set nocount on

CREATE TABLE #ugykor_azon_valid
(
	ugykor_azon nvarchar(10)
)	

INSERT INTO #ugykor_azon_valid
  (ugykor_azon)
VALUES
  ('A101'),
  ('A102'),
  ('A103'),
  ('A104'),
  ('A105'),
  ('A106'),
  ('A107'),
  ('A108'),
  ('A109'),
  ('A201'),
  ('A202'),
  ('A203'),
  ('A204'),
  ('A205'),
  ('A206'),
  ('A207'),
  ('A208'),
  ('A209'),
  ('A210'),
  ('A211'),
  ('A212'),
  ('B101'),
  ('B102'),
  ('B103'),
  ('B104'),
  ('B105'),
  ('B106'),
  ('B107'),
  ('B108'),
  ('B109'),
  ('B110'),
  ('B111'),
  ('B112'),
  ('B113'),
  ('B114'),
  ('B115'),
  ('B116'),
  ('B117'),
  ('B118'),
  ('B119'),
  ('B120'),
  ('B121'),
  ('B122'),
  ('B123'),
  ('B124'),
  ('B125'),
  ('B126'),
  ('B127'),
  ('C101'),
  ('C102'),
  ('C103'),
  ('C104'),
  ('C105'),
  ('C106'),
  ('C107'),
  ('C108'),
  ('C109'),
  ('C110'),
  ('C111'),
  ('C112'),
  ('C113'),
  ('C114'),
  ('C115'),
  ('C116'),
  ('C117'),
  ('C118'),
  ('C119'),
  ('C120'),
  ('C121'),
  ('C122'),
  ('C123'),
  ('C124'),
  ('C125'),
  ('C126'),
  ('C127'),
  ('C128'),
  ('C129'),
  ('C130'),
  ('C131'),
  ('C132'),
  ('C133'),
  ('C134'),
  ('C135'),
  ('E101'),
  ('E102'),
  ('E103'),
  ('E104'),
  ('E105'),
  ('E106'),
  ('E107'),
  ('E108'),
  ('E109'),
  ('E110'),
  ('E111'),
  ('E112'),
  ('E113'),
  ('E114'),
  ('E115'),
  ('E116'),
  ('E117'),
  ('E118'),
  ('E201'),
  ('E202'),
  ('E203'),
  ('E204'),
  ('E205'),
  ('E206'),
  ('E207'),
  ('E208'),
  ('E209'),
  ('E210'),
  ('E211'),
  ('E212'),
  ('E213'),
  ('E214'),
  ('E215'),
  ('E216'),
  ('E217'),
  ('E218'),
  ('E219'),
  ('E220'),
  ('E221'),
  ('E222'),
  ('E223'),
  ('E301'),
  ('E302'),
  ('E303'),
  ('E304'),
  ('E305'),
  ('E306'),
  ('E307'),
  ('E308'),
  ('E309'),
  ('E401'),
  ('E402'),
  ('E403'),
  ('E404'),
  ('E405'),
  ('E406'),
  ('E407'),
  ('E408'),
  ('E409'),
  ('E410'),
  ('E411'),
  ('E412'),
  ('E413'),
  ('F101'),
  ('F102'),
  ('F103'),
  ('F104'),
  ('F105'),
  ('F106'),
  ('F107'),
  ('F108'),
  ('F109'),
  ('F110'),
  ('F111'),
  ('F112'),
  ('F113'),
  ('F114'),
  ('F115'),
  ('F116'),
  ('F117'),
  ('F118'),
  ('F119'),
  ('F120'),
  ('F121'),
  ('F122'),
  ('F123'),
  ('F124'),
  ('F125'),
  ('F126'),
  ('F127'),
  ('F128'),
  ('F129'),
  ('F130'),
  ('F131'),
  ('F132'),
  ('G101'),
  ('G102'),
  ('G103'),
  ('G104'),
  ('G105'),
  ('G106'),
  ('G107'),
  ('G108'),
  ('G109'),
  ('G110'),
  ('G111'),
  ('G112'),
  ('G113'),
  ('G114'),
  ('G115'),
  ('G116'),
  ('G117'),
  ('G118'),
  ('G119'),
  ('G120'),
  ('G121'),
  ('G122'),
  ('G123'),
  ('G124'),
  ('G125'),
  ('H101'),
  ('H102'),
  ('H103'),
  ('H104'),
  ('H105'),
  ('H106'),
  ('H107'),
  ('H201'),
  ('H202'),
  ('H203'),
  ('H204'),
  ('H301'),
  ('H302'),
  ('H303'),
  ('H304'),
  ('H305'),
  ('H306'),
  ('H307'),
  ('H401'),
  ('H402'),
  ('H403'),
  ('H404'),
  ('H405'),
  ('H406'),
  ('H501'),
  ('H502'),
  ('H503'),
  ('H504'),
  ('H505'),
  ('H506'),
  ('H507'),
  ('H508'),
  ('H509'),
  ('H510'),
  ('H601'),
  ('H701'),
  ('H702'),
  ('H703'),
  ('H704'),
  ('H705'),
  ('H801'),
  ('H802'),
  ('H803'),
  ('H804'),
  ('H805'),
  ('H806'),
  ('H807'),
  ('H808'),
  ('H809'),
  ('H810'),
  ('H811'),
  ('H812'),
  ('H813'),
  ('H814'),
  ('H815'),
  ('H816'),
  ('H817'),
  ('I101'),
  ('I102'),
  ('I103'),
  ('I104'),
  ('I105'),
  ('I106'),
  ('I107'),
  ('I108'),
  ('I109'),
  ('I110'),
  ('I111'),
  ('I112'),
  ('I113'),
  ('J101'),
  ('J102'),
  ('J103'),
  ('J104'),
  ('J105'),
  ('J106'),
  ('J107'),
  ('J108'),
  ('J109'),
  ('J110'),
  ('J112'),
  ('J113'),
  ('J114'),
  ('K101'),
  ('K102'),
  ('K103'),
  ('K104'),
  ('K105'),
  ('K106'),
  ('K107'),
  ('L101'),
  ('L102'),
  ('L103'),
  ('L104'),
  ('L105'),
  ('L106'),
  ('L107'),
  ('L108'),
  ('L109'),
  ('L110'),
  ('L111'),
  ('L112'),
  ('L113'),
  ('L114'),
  ('L115'),
  ('L116'),
  ('L117'),
  ('M101'),
  ('M102'),
  ('M103'),
  ('M104'),
  ('M105'),
  ('M106'),
  ('M107'),
  ('M108'),
  ('M109'),
  ('M110'),
  ('M111'),
  ('M112'),
  ('M113'),
  ('M114'),
  ('M115'),
  ('M116'),
  ('M117'),
  ('M118'),
  ('M119'),
  ('M120'),
  ('M121'),
  ('M122'),
  ('M123'),
  ('M124'),
  ('M125'),
  ('M126'),
  ('M127'),
  ('M128'),
  ('M129'),
  ('M130'),
  ('N101'),
  ('N102'),
  ('P101'),
  ('P102'),
  ('P103'),
  ('P104'),
  ('P105'),
  ('P106'),
  ('P107'),
  ('P108'),
  ('P109'),
  ('P110'),
  ('P111'),
  ('P112'),
  ('P113'),
  ('P114'),
  ('P115'),
  ('P116'),
  ('P117'),
  ('P118'),
  ('P119'),
  ('P120'),
  ('P121'),
  ('P122'),
  ('P123'),
  ('P124'),
  ('P125'),
  ('P126'),
  ('P127'),
  ('P128'),
  ('P129'),
  ('P130'),
  ('P131'),
  ('P132'),
  ('P133'),
  ('P134'),
  ('P135'),
  ('P136'),
  ('P137'),
  ('P138'),
  ('P139'),
  ('P140'),
  ('P141'),
  ('P142'),
  ('R101'),
  ('R102'),
  ('R103'),
  ('R104'),
  ('R105'),
  ('U101'),
  ('U102'),
  ('U103'),
  ('U104'),
  ('U105'),
  ('U106'),
  ('U107'),
  ('U108'),
  ('U109'),
  ('U110'),
  ('U111'),
  ('U201'),
  ('U202'),
  ('U203'),
  ('U204'),
  ('U205'),
  ('U206'),
  ('U207'),
  ('U208'),
  ('U209'),
  ('U210'),
  ('U211'),
  ('U212'),
  ('U213'),
  ('U214'),
  ('U215'),
  ('U216'),
  ('U301'),
  ('U302'),
  ('U303'),
  ('U304'),
  ('U305'),
  ('U306'),
  ('U307'),
  ('U308'),
  ('U309'),
  ('U310'),
  ('U311'),
  ('U312'),
  ('U313'),
  ('U314'),
  ('U315'),
  ('U316'),
  ('U317'),
  ('U318'),
  ('U319'),
  ('U320'),
  ('U321'),
  ('U322'),
  ('U323'),
  ('U324'),
  ('U325'),
  ('U326'),
  ('U327'),
  ('U328'),
  ('U329'),
  ('U330'),
  ('U331'),
  ('U332'),
  ('U333'),
  ('U334'),
  ('U335'),
  ('U336'),
  ('U337'),
  ('U338'),
  ('U339'),
  ('U340'),
  ('U341'),
  ('U342'),
  ('U343'),
  ('U344'),
  ('U345'),
  ('U346'),
  ('U347'),
  ('U348'),
  ('U349'),
  ('U350'),
  ('U351'),
  ('U352'),
  ('U353'),
  ('U354'),
  ('U355'),
  ('U356'),
  ('U357'),
  ('U358'),
  ('U359'),
  ('U360'),
  ('U361'),
  ('U362'),
  ('U363'),
  ('U364'),
  ('U401'),
  ('U402'),
  ('U403'),
  ('U404'),
  ('U405'),
  ('U406'),
  ('U407'),
  ('U408'),
  ('U409'),
  ('U410'),
  ('U411'),
  ('U412'),
  ('U413'),
  ('U414'),
  ('U415'),
  ('U416'),
  ('U501'),
  ('U502'),
  ('U503'),
  ('U504'),
  ('U505'),
  ('U506'),
  ('U507'),
  ('U508'),
  ('U509'),
  ('U510'),
  ('U511'),
  ('U512'),
  ('U513'),
  ('U514'),
  ('U515'),
  ('U516'),
  ('U517'),
  ('U518'),
  ('U519'),
  ('U520'),
  ('U521'),
  ('U522'),
  ('U523'),
  ('U524'),
  ('U525'),
  ('U526'),
  ('U601'),
  ('U602'),
  ('U603'),
  ('U604'),
  ('U605'),
  ('U606'),
  ('U607'),
  ('U608'),
  ('U609'),
  ('U610'),
  ('U611'),
  ('U612'),
  ('U613'),
  ('U614'),
  ('U615'),
  ('U616'),
  ('U617'),
  ('U618'),
  ('U619'),
  ('U620'),
  ('U621'),
  ('U622'),
  ('U623'),
  ('U624'),
  ('U625'),
  ('U626'),
  ('U627'),
  ('U628'),
  ('U629'),
  ('U630'),
  ('U631'),
  ('U632'),
  ('X101'),
  ('X102'),
  ('X103'),
  ('X104'),
  ('X105'),
  ('X106'),
  ('X107'),
  ('X108'),
  ('X109'),
  ('X110'),
  ('X111'),
  ('X112'),
  ('X113'),
  ('X114'),
  ('X115'),
  ('X116'),
  ('X117'),
  ('X118'),
  ('X201'),
  ('X202'),
  ('X203'),
  ('X204'),
  ('X205'),
  ('X206'),
  ('X207'),
  ('X208'),
  ('X209'),
  ('X210'),
  ('X211'),
  ('X212'),
  ('X213'),
  ('X214'),
  ('X215'),
  ('X216'),
  ('X217'),
  ('X218'),
  ('X219'),
  ('X220'),
  ('X221'),
  ('X222'),
  ('X223'),
  ('X224'),
  ('X225'),
  ('X226'),
  ('X227'),
  ('X228'),
  ('X229'),
  ('X230'),
  ('X231'),
  ('X232'),
  ('X301'),
  ('X302')

select
CASE 
	WHEN u.Allapot = '90' THEN 0
	ELSE 1
END as aktiv
, ik.Iktatohely as iktatokonyvek_elotag_azon
, left(ik.Nev, 50) as iktatokonyvek_elotag_nev
, u.Foszam as foszam
, ik.Ev as ev
, it.IrattariTetelszam as ugykor_azon
, left(a.Kod,1) as agazat_azon
, i.iktatasDatuma as iktatas_datum
, u.hatarido as hatarido_datum
, u.lezarasDat as lezaras_datum
, u.IrattarbaVetelDat as irattarazas_datum
, u.SelejtezesDat as selejtezes_datum
, leveltarban.VegrehajtasDatuma as leveltari_atadas_datum -- ezt a kódból kell megnézni, mert nem találom :(
FROM EREC_UgyUgyiratok u
inner join EREC_IraIktatokonyvek ik on ik.id = u.IraIktatokonyv_Id 
inner join EREC_IraIratok i on u.id = i.Ugyirat_id
inner join EREC_IraIrattariTetelek it on it.id = u.IraIrattariTetel_Id
inner join #ugykor_azon_valid uav on uav.ugykor_azon collate database_default = it.IrattariTetelszam collate database_default
inner join EREC_AgazatiJelek a on a.id = it.AgazatiJel_Id
left join EREC_KuldKuldemenyek k ON i.KuldKuldemenyek_Id = k.Id 
left join (select ijk.Obj_Id, ij.VegrehajtasDatuma from EREC_IraJegyzekek ij inner join EREC_IraJegyzekTetelek ijk on ijk.Jegyzek_Id = ij.Id and ij.Tipus = 'L') leveltarban on leveltarban.Obj_Id = u.Id
where 1=1
AND Alszam = 1 
AND Foszam IS NOT NULL -- kiszűrjük a MU-t 
AND Alszam IS NOT NULL -- és a MI-t
AND (u.ModositasIdo between @startDate and @endDate or i.IktatasDatuma between @startDate and @endDate)
AND i.IktatasDatuma > '2018-01-01'
order by iktatas_datum

If(OBJECT_ID('tempdb..#ugykor_azon_valid') Is Not Null)
Begin
    Drop Table #ugykor_azon_valid
End

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
GO