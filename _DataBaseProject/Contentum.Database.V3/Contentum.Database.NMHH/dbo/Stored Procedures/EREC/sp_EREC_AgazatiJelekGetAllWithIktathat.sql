﻿create procedure [dbo].[sp_EREC_AgazatiJelekGetAllWithIktathat]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_AgazatiJelek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @FelhasznaloSzervezet_Id UNIQUEIDENTIFIER,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_AgazatiJelek.Id,
	   EREC_AgazatiJelek.Kod,
	   EREC_AgazatiJelek.Nev,
	   EREC_AgazatiJelek.AgazatiJel_Id,
	   EREC_AgazatiJelek.Ver,
	   EREC_AgazatiJelek.Note,
	   EREC_AgazatiJelek.Stat_id,
	   EREC_AgazatiJelek.ErvKezd,
	   EREC_AgazatiJelek.ErvVege,
	   EREC_AgazatiJelek.Letrehozo_id,
	   EREC_AgazatiJelek.LetrehozasIdo,
	   EREC_AgazatiJelek.Modosito_id,
	   EREC_AgazatiJelek.ModositasIdo,
	   EREC_AgazatiJelek.Zarolo_id,
	   EREC_AgazatiJelek.ZarolasIdo,
	   EREC_AgazatiJelek.Tranz_id,
	   EREC_AgazatiJelek.UIAccessLog_id  
   from 
     EREC_AgazatiJelek as EREC_AgazatiJelek
	where exists(select 1 from EREC_IraIrattariTetelek
		where EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_AgazatiJelek.Id
		and exists(select 1 from EREC_IrattariTetel_Iktatokonyv
		where EREC_IrattariTetel_Iktatokonyv.IrattariTetel_Id = EREC_IraIrattariTetelek.Id
		and EREC_IrattariTetel_Iktatokonyv.Iktatokonyv_Id 
			in (select Id from dbo.fn_GetAllIktathatoEREC_IraIktatokonyvekByCsoportId(@ExecutorUserId,@FelhasznaloSzervezet_Id)
				where Ev = datepart(year,getdate())
				)))
    and EREC_AgazatiJelek.Org=@Org
  '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;

   exec sp_executesql @sqlcmd,N'@ExecutorUserId uniqueidentifier, @FelhasznaloSzervezet_Id uniqueidentifier, @Org uniqueidentifier',@ExecutorUserId = @ExecutorUserId, @FelhasznaloSzervezet_Id=@FelhasznaloSzervezet_Id, @Org = @Org;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end