﻿
create procedure sp_EREC_Alairas_Folyamat_TetelekGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_Alairas_Folyamat_Tetelek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_Alairas_Folyamat_Tetelek.Id,
	   EREC_Alairas_Folyamat_Tetelek.AlairasFolyamat_Id,
	   EREC_Alairas_Folyamat_Tetelek.IraIrat_Id,
	   EREC_Alairas_Folyamat_Tetelek.Csatolmany_Id,
	   EREC_Alairas_Folyamat_Tetelek.AlairasStatus,
	   EREC_Alairas_Folyamat_Tetelek.Hiba,
	   EREC_Alairas_Folyamat_Tetelek.Ver,
	   EREC_Alairas_Folyamat_Tetelek.Note,
	   EREC_Alairas_Folyamat_Tetelek.Stat_id,
	   EREC_Alairas_Folyamat_Tetelek.ErvKezd,
	   EREC_Alairas_Folyamat_Tetelek.ErvVege,
	   EREC_Alairas_Folyamat_Tetelek.Letrehozo_id,
	   EREC_Alairas_Folyamat_Tetelek.LetrehozasIdo,
	   EREC_Alairas_Folyamat_Tetelek.Modosito_id,
	   EREC_Alairas_Folyamat_Tetelek.ModositasIdo,
	   EREC_Alairas_Folyamat_Tetelek.Zarolo_id,
	   EREC_Alairas_Folyamat_Tetelek.ZarolasIdo,
	   EREC_Alairas_Folyamat_Tetelek.Tranz_id,
	   EREC_Alairas_Folyamat_Tetelek.UIAccessLog_id  
   from 
     EREC_Alairas_Folyamat_Tetelek as EREC_Alairas_Folyamat_Tetelek      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end