﻿CREATE procedure [dbo].[sp_EREC_CsatolmanyokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_Csatolmanyok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_Csatolmanyok.Id,
	   EREC_Csatolmanyok.KuldKuldemeny_Id,
	   EREC_Csatolmanyok.IraIrat_Id,
	   EREC_Csatolmanyok.Dokumentum_Id,
	   EREC_Csatolmanyok.Leiras,
	   EREC_Csatolmanyok.Lapszam,
	   EREC_Csatolmanyok.KapcsolatJelleg,
	   EREC_Csatolmanyok.DokumentumSzerep,
	   EREC_Csatolmanyok.IratAlairoJel,
	   EREC_Csatolmanyok.Ver,
	   EREC_Csatolmanyok.Note,
	   EREC_Csatolmanyok.Stat_id,
	   EREC_Csatolmanyok.ErvKezd,
	   EREC_Csatolmanyok.ErvVege,
	   EREC_Csatolmanyok.Letrehozo_id,
	   EREC_Csatolmanyok.LetrehozasIdo,
	   EREC_Csatolmanyok.Modosito_id,
	   EREC_Csatolmanyok.ModositasIdo,
	   EREC_Csatolmanyok.Zarolo_id,
	   EREC_Csatolmanyok.ZarolasIdo,
	   EREC_Csatolmanyok.Tranz_id,
	   EREC_Csatolmanyok.UIAccessLog_id  
   from 
     EREC_Csatolmanyok as EREC_Csatolmanyok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   -- debug

--	insert into temp_debug_20090629 values (getdate(), @sqlcmd);

	-- debug

	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1
	-- debug
	
--	insert into temp_debug_20090629 values (getdate(), @errorCode);
	
	-- debug

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end