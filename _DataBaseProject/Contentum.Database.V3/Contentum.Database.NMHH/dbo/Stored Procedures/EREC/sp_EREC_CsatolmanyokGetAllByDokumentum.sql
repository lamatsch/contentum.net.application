﻿create procedure [dbo].[sp_EREC_CsatolmanyokGetAllByDokumentum]
  @DokumentumId uniqueidentifier,
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by EREC_Csatolmanyok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_Csatolmanyok.Id,
	   EREC_Csatolmanyok.KuldKuldemeny_Id,
	   EREC_Csatolmanyok.IraIrat_Id,
	Azonosito = 
		CASE
			WHEN EREC_Csatolmanyok.IraIrat_Id IS NOT NULL THEN
						(SELECT EREC_IraIratok.Azonosito
						FROM [EREC_IraIratok]							
						WHERE [EREC_IraIratok].[Id] = EREC_Csatolmanyok.IraIrat_Id)
			ELSE
						(SELECT EREC_KuldKuldemenyek.[Azonosito]
						FROM [EREC_KuldKuldemenyek]							
						WHERE [EREC_KuldKuldemenyek].[Id] = EREC_Csatolmanyok.KuldKuldemeny_Id)
		END,
	Azonosito_Tipus = 
		CASE
			WHEN EREC_Csatolmanyok.IraIrat_Id IS NOT NULL THEN ''EREC_IraIratok''
		ELSE
			''EREC_KuldKuldemenyek''
		END,
	   EREC_Csatolmanyok.Dokumentum_Id,
	   EREC_Csatolmanyok.Leiras,
	   EREC_Csatolmanyok.Lapszam,
	   EREC_Csatolmanyok.KapcsolatJelleg,
	   EREC_Csatolmanyok.DokumentumSzerep,
	   EREC_Csatolmanyok.IratAlairoJel,
	   EREC_Csatolmanyok.Ver,
	   EREC_Csatolmanyok.Note,
	   EREC_Csatolmanyok.Stat_id,
	   EREC_Csatolmanyok.ErvKezd,
	   EREC_Csatolmanyok.ErvVege,
	   EREC_Csatolmanyok.Letrehozo_id,
	   EREC_Csatolmanyok.LetrehozasIdo,
	   EREC_Csatolmanyok.Modosito_id,
	   EREC_Csatolmanyok.ModositasIdo,
	   EREC_Csatolmanyok.Zarolo_id,
	   EREC_Csatolmanyok.ZarolasIdo,
	   EREC_Csatolmanyok.Tranz_id,
	   EREC_Csatolmanyok.UIAccessLog_id  
   from 
     EREC_Csatolmanyok as EREC_Csatolmanyok   
   Where EREC_Csatolmanyok.Dokumentum_Id = @DokumentumId   
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
--   exec (@sqlcmd);
	exec sp_executesql @sqlcmd,N'@DokumentumId uniqueidentifier',@DokumentumId = @DokumentumId

END TRY
BEGIN CATCH

	-- hibakezelés:
	EXEC usp_HandleException
 
END CATCH

end