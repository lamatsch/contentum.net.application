﻿--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_CsatolmanyokGetAllWithExtension]') AND type in (N'P', N'PC'))
--DROP PROCEDURE [dbo].[sp_EREC_CsatolmanyokGetAllWithExtension]
--GO

--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

CREATE procedure [dbo].[sp_EREC_CsatolmanyokGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by EREC_Csatolmanyok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
    
-- A PKI_INTEGRACIO rendszerparameter erteketol fuggoen
-- eltero kodcsoport szerint dolgozunk az elektronikus
-- alairas kapcsan
DECLARE @kcsDokumentumAlairas nvarchar(64)

DECLARE @paramPKI_Integracio nvarchar(400)

SET @paramPKI_Integracio = (
	SELECT Ertek FROM KRT_Parameterek
	WHERE Nev ='PKI_INTEGRACIO'
	AND Org=@Org
)

IF @paramPKI_Integracio = 'Igen'
BEGIN
	SET @kcsDokumentumAlairas = 'DOKUMENTUM_ALAIRAS_PKI'
END
ELSE
BEGIN
	SET @kcsDokumentumAlairas = 'DOKUMENTUM_ALAIRAS_MANUALIS'
END

              

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_Csatolmanyok.Id,
	   EREC_Csatolmanyok.Dokumentum_Id,
	   EREC_Csatolmanyok.IraIrat_Id,
	   EREC_Csatolmanyok.KuldKuldemeny_Id,
	   EREC_Csatolmanyok.Ver as erecCsat_Ver,
EREC_Csatolmanyok.DokumentumSzerep,
dbo.fn_KodtarErtekNeve(''DOKUMENTUM_SZEREP'', EREC_Csatolmanyok.DokumentumSzerep,''' + CAST(@Org as Nvarchar(40)) + ''') as DokumentumSzerep_Nev,
	KRT_Dokumentumok.Id as krtDok_Id,
	KRT_Dokumentumok.FajlNev as FajlNev,
	KRT_Dokumentumok.External_Link,	   
	KRT_Dokumentumok.External_Id,
	KRT_Dokumentumok.External_Source,
	KRT_Dokumentumok.External_Info,
	KRT_Dokumentumok.Dokumentum_Id as krtDok_Dokumentum_Id,
	KRT_Dokumentumok.Dokumentum_Id_Kovetkezo as krtDok_Dokumentum_Id_Kovetkezo,
	KRT_Dokumentumok.VerzioJel,
	KRT_Dokumentumok.Megnyithato,
	KRT_Dokumentumok.Olvashato,
	KRT_Dokumentumok.ElektronikusAlairas,
dbo.fn_KodtarErtekNeve(''' + @kcsDokumentumAlairas +
		''', KRT_Dokumentumok.ElektronikusAlairas,''' + CAST(@Org as Nvarchar(40)) + ''') as ElektronikusAlairasNev,
	KRT_Dokumentumok.AlairasFelulvizsgalat,
	KRT_Dokumentumok.Titkositas,
	KRT_Dokumentumok.Tipus,
	KRT_Dokumentumok.Formatum,
dbo.fn_KodtarErtekNeve(''DOKUMENTUM_FORMATUM'', LOWER(KRT_Dokumentumok.Formatum),''' + CAST(@Org as Nvarchar(40)) + ''') as Formatum_Nev,
	KRT_Dokumentumok.SablonAzonosito,
	KRT_Dokumentumok.BarCode,
	KRT_Dokumentumok.TartalomHash,
	KRT_Dokumentumok.KivonatHash,
	(select top 1 KRT_Dokumentumok_alairtak.External_Link
	 from KRT_Dokumentumok as KRT_Dokumentumok_alairtak
		  join KRT_DokumentumKapcsolatok 
			on KRT_DokumentumKapcsolatok.Dokumentum_Id_Fo = KRT_Dokumentumok_alairtak.Id 
	 where KRT_DokumentumKapcsolatok.Dokumentum_Id_Al = KRT_Dokumentumok.Id
			and KRT_DokumentumKapcsolatok.Tipus = ''2'') as AlairtDokumentum_External_Link,
	(select top 1 KRT_Dokumentumok_alairtak.Id
	 from KRT_Dokumentumok as KRT_Dokumentumok_alairtak
		  join KRT_DokumentumKapcsolatok 
			on KRT_DokumentumKapcsolatok.Dokumentum_Id_Fo = KRT_Dokumentumok_alairtak.Id 
	 where KRT_DokumentumKapcsolatok.Dokumentum_Id_Al = KRT_Dokumentumok.Id
			and KRT_DokumentumKapcsolatok.Tipus = ''2'') as AlairtDokumentum_Id,
	EREC_IraIratok.[Azonosito] as IktatoSzam_Merge,
	ErkeztetoSzam_Merge = 
		CASE 
			WHEN EREC_Csatolmanyok.KuldKuldemeny_Id is not null THEN
				(SELECT EREC_KuldKuldemenyek.[Azonosito]
				 FROM [EREC_KuldKuldemenyek]
				 WHERE [EREC_KuldKuldemenyek].[Id] = EREC_Csatolmanyok.KuldKuldemeny_Id)
			ELSE ''''
		END,
	KRT_Dokumentumok.OCRAllapot,
	dbo.fn_KodtarErtekNeve(''OCR_Allapot'', KRT_Dokumentumok.OCRAllapot,''' + CAST(@Org as Nvarchar(100)) + ''') as OCRAllapot_Nev,
	'
		
SET @sqlcmd = @sqlcmd +	'	
	   EREC_Csatolmanyok.Leiras,
	   EREC_Csatolmanyok.Lapszam,
	   (select count(*) from EREC_IratelemKapcsolatok
     where EREC_IratelemKapcsolatok.Csatolmany_Id = EREC_Csatolmanyok.Id
		   and getdate() between EREC_IratelemKapcsolatok.ErvKezd and EREC_IratelemKapcsolatok.ErvVege) as MellekletCount,
	   EREC_Csatolmanyok.Ver,
	   EREC_Csatolmanyok.Note,
	   EREC_Csatolmanyok.Stat_id,
	   EREC_Csatolmanyok.ErvKezd,
CONVERT(nvarchar(10), EREC_Csatolmanyok.ErvKezd, 102) as ErvKezd_f,
	   EREC_Csatolmanyok.ErvVege,
CONVERT(nvarchar(10), EREC_Csatolmanyok.ErvVege, 102) as ErvVege_f,
	   EREC_Csatolmanyok.Letrehozo_id,
	   EREC_Csatolmanyok.LetrehozasIdo,
	   EREC_Csatolmanyok.Modosito_id,
	   EREC_Csatolmanyok.ModositasIdo,
	   EREC_Csatolmanyok.Zarolo_id,
	   EREC_Csatolmanyok.ZarolasIdo,
	KRT_Dokumentumok.Zarolo_id as krtDok_Zarolo_id,
	KRT_Dokumentumok.ZarolasIdo as krtDok_ZarolasIdo,
	   EREC_Csatolmanyok.Tranz_id,
	   EREC_Csatolmanyok.UIAccessLog_id  
   from 
     EREC_Csatolmanyok as EREC_Csatolmanyok      
inner join KRT_Dokumentumok as KRT_Dokumentumok on EREC_Csatolmanyok.Dokumentum_Id = KRT_Dokumentumok.Id      
left join EREC_IraIratok on EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
LEFT JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok
		ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
	LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok 
			ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id  
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek 
			ON EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end