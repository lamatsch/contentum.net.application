﻿CREATE procedure [dbo].[sp_EREC_CsatolmanyokTomegesInvalidate]
        @Where nvarchar(MAX),
		@ExecutorUserId				uniqueidentifier,
		@ExecutionTime					datetime			

as

BEGIN TRY
--BEGIN TRANSACTION InvalidateTransaction

  
set nocount on
   
      SET @ExecutionTime = getdate()

	  DECLARE @tranzId UNIQUEIDENTIFIER
	  SET @tranzId = NEWID()

	  DECLARE @sqlcmd nvarchar(MAX)
      
	  SET @sqlcmd = '
      UPDATE EREC_Csatolmanyok
	  SET  ErvKezd = CASE WHEN ErvKezd > @ExecutionTime THEN @ExecutionTime ELSE ErvKezd END,
		   ErvVege = @ExecutionTime,
	       Ver = ISNULL(Ver, 1) +1,
		   Tranz_id = @tranzId
	  WHERE @ExecutionTime <= ErvVege
	  and ' + @Where

	  EXECUTE sp_executesql @sqlcmd, N'@ExecutionTime datetime, @tranzId uniqueidentifier', @ExecutionTime = @ExecutionTime, @tranzId = @tranzId


	 /* History Log */

	 DECLARE @row_ids varchar(MAX)
	 /*** EREC_UgyUgyiratok history log ***/

	 SET @row_ids = 'select id from EREC_Csatolmanyok where Tranz_id = ''' + CAST(@tranzId AS NVARCHAR(36)) + ''''

	 EXEC sp_LogRecordsToHistory_Tomeges
	 @TableName = 'EREC_Csatolmanyok'
	,@Row_Ids = @row_ids
	,@Muvelet = 2
	,@Vegrehajto_Id = @ExecutorUserId
	,@VegrehajtasIdo = @ExecutionTime



--COMMIT TRANSACTION InvalidateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InvalidateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH