﻿-- Excel 'Reszletek' tabla export vezetoi statisztikához (Template Manager)
CREATE PROCEDURE [dbo].[sp_EREC_ExcelDetailsForLeaders]
	@ExecutorUserId UNIQUEIDENTIFIER,
	@FelhasznaloSzervezet_Id UNIQUEIDENTIFIER,
	@KezdDat datetime,
	@VegeDat datetime
AS 
	BEGIN
		BEGIN TRY

			SET nocount ON

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

		if not exists(select Id from KRT_CsoportTagok
			where Csoport_Id_jogalany = @ExecutorUserId
			and Csoport_Id = @FelhasznaloSzervezet_Id
			and getdate() between ErvKezd and ErvVege
		)
		begin
			RAISERROR('[52001]',16,1) -- A felhasználó nem tagja a csoportnak!
		end

	-- variables to hold the states
		DECLARE @allapot_iktatott nvarchar(64)
		DECLARE @allapot_szignalt nvarchar(64)
		DECLARE @allapot_ugyintezes_alatt nvarchar(64)
		DECLARE @allapot_skontroban nvarchar(64)
		DECLARE @allapot_elintezett nvarchar(64)
		DECLARE @allapot_adacta nvarchar(64)
		DECLARE @allapot_irattarba_kuldott nvarchar(64)
		DECLARE @allapot_kolcsonzott nvarchar(64)

		SET @allapot_iktatott = '04'
		SET @allapot_szignalt = '03'
		SET @allapot_ugyintezes_alatt = '06'
		SET @allapot_skontroban = '07'
		SET @allapot_elintezett = '99'
		SET @allapot_adacta = '09'
		SET @allapot_irattarba_kuldott = '11'
		SET @allapot_kolcsonzott = '13'


		-- Belso Szervezeti Egyseg lekerdezese
		IF (@FelhasznaloSzervezet_Id IS NOT NULL)
		BEGIN
			SELECT ISNULL(Nev, '-') AS Nev
			FROM KRT_Csoportok cs
            WHERE cs.Id = @FelhasznaloSzervezet_Id
        END
        ELSE
        BEGIN
		SELECT ISNULL(Nev, '-') AS Nev
			FROM KRT_Csoportok cs
				INNER JOIN KRT_CsoportTagok cst
				ON cs.Id = cst.Csoport_Id
					AND cst.Tipus IS NOT NULL
					AND getdate() BETWEEN cst.ErvKezd AND cst.ErvVege
			WHERE cst.Csoport_Id_Jogalany = @ExecutorUserId
        END

			--- Vezetoi statisztika meghivasa ---
			DECLARE @OverviewTable TABLE 
			(
				Megnevezes nvarchar(400),
				Ertek int
			)

			-- tarolt eljaras eredmenyenek tarolasara (INSERT-EXEC),
			-- nem lehet table valtozo, szerkezetenek pontosan
			-- meg kell felelnie a procedura eredmenyhalmazanak
			IF OBJECT_ID('tempdb..#temp_StatisticsTable') IS NOT NULL 
			BEGIN
			   DROP TABLE #temp_StatisticsTable
			END

			CREATE TABLE #temp_StatisticsTable
			(
				kuldemenyek nvarchar(10),
				kuldemenyek_ma nvarchar(10),
				kuldemenyek_1_3nap nvarchar(10),
				kuldemenyek_4_7nap nvarchar(10),
				kuldemenyek_7napfelett nvarchar(10),
				kuldemenyek_napi_atlag nvarchar(10),
				ugyiratok_osszesen nvarchar(10),
				ugyiratok nvarchar(10),
				ugyiratok_ma nvarchar(10),
				ugyiratok_1_3nap nvarchar(10),
				ugyiratok_4_7nap nvarchar(10),
				ugyiratok_7napfelett nvarchar(10),
				ugyintezes_alatt nvarchar(10),
				szignalasra_varo nvarchar(10),
				elintezett nvarchar(10),
				lejart_hatarideju nvarchar(10),
				lejart_1nap nvarchar(10),
				lejart_2_5nap nvarchar(10), 
				lejart_6_10nap nvarchar(10),
				lejart_11_15nap nvarchar(10),
				lejart_15napfelett nvarchar(10),
				kritikus_hatarideju nvarchar(10),
				kritikus_0nap nvarchar(10),
				kritikus_1nap nvarchar(10),
				kritikus_2_5nap nvarchar(10),
				kritikus_6_10nap nvarchar(10),
				atlag nvarchar(10),
				szignalt nvarchar(10),
				lezart nvarchar(10),
				skontroban_levo nvarchar(10),
				irattarba_kuldott nvarchar(10),
				kolcsonzott nvarchar(10),
                iratok_alszamra_ma nvarchar(10),
                iratok_alszamra_bejovo_ma nvarchar(10),
                iratok_alszamra_belso_ma nvarchar(10)
			)

			INSERT INTO #temp_StatisticsTable (
				kuldemenyek,
				kuldemenyek_ma,
				kuldemenyek_1_3nap,
				kuldemenyek_4_7nap,
				kuldemenyek_7napfelett,
				kuldemenyek_napi_atlag,
				ugyiratok_osszesen,
				ugyiratok,
				ugyiratok_ma,
				ugyiratok_1_3nap,
				ugyiratok_4_7nap,
				ugyiratok_7napfelett,
				ugyintezes_alatt,
				szignalasra_varo,
				elintezett,
				lejart_hatarideju,
				lejart_1nap,
				lejart_2_5nap, 
				lejart_6_10nap ,
				lejart_11_15nap,
				lejart_15napfelett,
				kritikus_hatarideju,
				kritikus_0nap,
				kritikus_1nap,
				kritikus_2_5nap,
				kritikus_6_10nap ,
				atlag,
				szignalt,
				lezart,
				skontroban_levo,
				irattarba_kuldott,
				kolcsonzott,
                iratok_alszamra_ma,
                iratok_alszamra_bejovo_ma,
                iratok_alszamra_belso_ma
			)
			EXEC sp_EREC_UgyUgyiratokForLeaders
				@Where = '',
				@OrderBy = '',
				@TopRow = '',
				@ExecutorUserId = @ExecutorUserId,
				@Jogosultak = '0',
				@FelhasznaloSzervezet_Id = @FelhasznaloSzervezet_Id,
				@KezdDat = @KezdDat,
				@VegeDat = @VegeDat,
				@Option_VezetoiPanel = 1 -- nem relevans, csak kompatibilitas miatt

			--- Ertekek osszefuzese megnevezes|ertek tablazatba ---

			-- Ugyiratforgalom
			INSERT INTO @OverviewTable (Megnevezes, Ertek)
			SELECT 'Érkezett küldemények', kuldemenyek FROM #temp_StatisticsTable 

			INSERT INTO @OverviewTable (Megnevezes, Ertek)
			SELECT 'Új ügyiratok', ugyiratok FROM #temp_StatisticsTable

			-- attekinto tablazat (megnevezes/ertek) visszadasa az eredmenyhalmazban  
			SELECT * FROM @OverviewTable
			-- tablazat torlese a kovetkezo lekerdezes elott
			DELETE FROM @OverViewTable

			-- Attekintes allapotok szerint
			INSERT INTO @OverviewTable (Megnevezes, Ertek)
			SELECT 'Ügyintézés alatt', ugyintezes_alatt FROM #temp_StatisticsTable 

			INSERT INTO @OverviewTable (Megnevezes, Ertek)
			SELECT 'Szignálásra váró', szignalasra_varo FROM #temp_StatisticsTable 

			INSERT INTO @OverviewTable (Megnevezes, Ertek)
			SELECT 'Elintézett ügyiratok', elintezett FROM #temp_StatisticsTable

			INSERT INTO @OverviewTable (Megnevezes, Ertek)
			SELECT 'Szignált ügyiratok', szignalt FROM #temp_StatisticsTable

			INSERT INTO @OverviewTable (Megnevezes, Ertek)
			SELECT 'Lezárt ügyiratok', lezart FROM #temp_StatisticsTable

			INSERT INTO @OverviewTable (Megnevezes, Ertek)
			SELECT 'Skontróban lévo ügyiratok', skontroban_levo FROM #temp_StatisticsTable

			INSERT INTO @OverviewTable (Megnevezes, Ertek)
			SELECT 'Irattárba küldött ügyiratok', irattarba_kuldott FROM #temp_StatisticsTable

			INSERT INTO @OverviewTable (Megnevezes, Ertek)
			SELECT 'Kölcsönzött ügyiratok', kolcsonzott FROM #temp_StatisticsTable

			-- attekinto tablazat (megnevezes/ertek) visszadasa az eredmenyhalmazban  
			SELECT * FROM @OverviewTable

			-- segedtabla torles
			IF OBJECT_ID('tempdb..#temp_StatisticsTable') IS NOT NULL 
			BEGIN
			   DROP TABLE #temp_StatisticsTable
			END
			--

			IF OBJECT_ID('tempdb..#temp_SzervezetTagok') IS NOT NULL 
			BEGIN
			   DROP TABLE #temp_SzervezetTagok
			END

			CREATE TABLE #temp_SzervezetTagok
			(CsoportId uniqueidentifier not null)

			INSERT INTO #temp_SzervezetTagok
			SELECT distinct Id
				FROM fn_GetAllSubCsoportBySzervezetId(@FelhasznaloSzervezet_Id)


		--- KÜLDEMÉNYEK ---
		SELECT REPLACE(CONVERT(varchar(10),k.BeerkezesIdeje,102),'.','-') AS Datum,
			ISNULL((SELECT cs.Nev --- Ugyintezo kötelezo: NULL értékek helyettesítése
					FROM [dbo].[KRT_Csoportok] cs
					WHERE cs.Id = k.Csoport_Id_Felelos
			),'-') AS Ugyintezo, -- ertelmezni kuldemenyre
			(SELECT cs.Nev
					FROM [dbo].[KRT_Csoportok] cs
					WHERE cs.Id = k.Csoport_Id_Felelos
			) AS Felelos,
			(SELECT cs.Nev
					FROM [dbo].[KRT_Csoportok] cs
					WHERE cs.Id = k.FelhasznaloCsoport_Id_Orzo
			) AS Orzo,
			'Küldemény' AS ObjektumTipus,
			ISNULL((SELECT Nev -- kuldemenytipus (kimeno, bejovo) szoveges azonositasa
					FROM KRT_Kodtarak
					WHERE KodCsoport_Id = (SELECT Id 
												FROM KRT_KodCsoportok
												WHERE Kod = 'POSTAZAS_IRANYA'
											) AND
					Kod = k.PostazasIranya
					AND Org=@Org
			),'-') AS Tipus, --- Tipus kötelezo: NULL értékek helyettesítése       
			(SELECT Nev -- allapot szoveges azonositasa
				FROM KRT_Kodtarak
				WHERE KodCsoport_Id = (SELECT Id 
												FROM KRT_KodCsoportok
												WHERE Kod = 'KULDEMENY_ALLAPOT'
										) AND
				Kod = k.Allapot
				AND Org=@Org
			) AS Allapot,
			NULL AS Hatarido, -- nincs ertelme kuldemeny eseten, csak az UNION miatt kell  
			REPLACE(CONVERT(varchar(10), t.AtadasDat, 102),'.','-') AS AtadasIdopontja,
			REPLACE(CONVERT(varchar(10), t.AtvetelDat, 102),'.','-') AS AtvetelIdopontja,
			NULL AS ElintezesIdopontja,
			NULL AS SkontrobanToltott,
			--CAST(k.Erkezteto_Szam AS nvarchar(100)) AS Azonosito
			(SELECT MegKulJelzes FROM EREC_IraIktatokonyvek WHERE Id = k.IraIktatokonyv_Id) + '/' + CAST(k.Erkezteto_Szam AS varchar) + '/' 
				+ CAST((SELECT Ev FROM EREC_IraIktatokonyvek WHERE Id = k.IraIktatokonyv_Id) AS varchar) AS Azonosito
		FROM [dbo].[EREC_KuldKuldemenyek] k
			LEFT JOIN EREC_IraKezbesitesiTetelek t
			ON t.Obj_Id = k.Id
				AND t.LetrehozasIdo = (SELECT Max(t.LetrehozasIdo)
							FROM EREC_KuldKuldemenyek k
							LEFT JOIN EREC_IraKezbesitesiTetelek t
							ON t.Obj_Id = k.Id
				)
		WHERE k.Csoport_Id_Felelos IN (SELECT CsoportId
											FROM #temp_SzervezetTagok
										) AND
			k.BeerkezesIdeje BETWEEN CONVERT(nvarchar(40), @KezdDat,121) AND CONVERT(nvarchar(40), @VegeDat,121) 
		UNION
		--- ÜGYIRATOK ---
		SELECT REPLACE(CONVERT(varchar(10),u.LetrehozasIdo,102),'.','-') AS Datum, 
			ISNULL((SELECT cs.Nev --- Ugyintezo kötelezo: NULL értékek helyettesítése
					FROM [dbo].[KRT_Csoportok] cs
					WHERE cs.Id = u.FelhasznaloCsoport_Id_Ugyintez
			),'-') AS Ugyintezo,
			(SELECT cs.Nev
					FROM [dbo].[KRT_Csoportok] cs
					WHERE cs.Id = u.Csoport_Id_Felelos
			) AS Felelos,
			(SELECT cs.Nev
					FROM [dbo].[KRT_Csoportok] cs
					WHERE cs.Id = u.FelhasznaloCsoport_Id_Orzo
			) AS Orzo,
			'Ügy' AS ObjektumTipus,
			ISNULL((SELECT Nev -- ugytipus szoveges azonositasa
					FROM KRT_Kodtarak
					WHERE KodCsoport_Id = (SELECT Id 
												FROM KRT_KodCsoportok
												WHERE Kod = 'UGYTIPUS'
										) AND
					Kod = u.UgyTipus
					AND Org=@Org
			),'-') AS Tipus, --- Tipus kötelezo: NULL értékek helyettesítése
			(SELECT Nev -- allapot szoveges azonositasa
					FROM KRT_Kodtarak
					WHERE KodCsoport_Id = (SELECT Id 
												FROM KRT_KodCsoportok
												WHERE Kod = 'UGYIRAT_ALLAPOT'
											) AND
					Kod = u.Allapot
					AND Org=@Org
			) AS Allapot,
			REPLACE(CONVERT(varchar(10), u.Hatarido, 102),'.','-') AS Hatarido,  
			REPLACE(CONVERT(varchar(10), t.AtadasDat, 102),'.','-') AS AtadasIdopontja,
			REPLACE(CONVERT(varchar(10), t.AtvetelDat,102),'.','-') AS AtvetelIdopontja,
			REPLACE(CONVERT(varchar(10), u.ElintezesDat, 102),'.','-') AS ElintezesIdopontja,
			SkontrobanOsszesen AS SkontrobanToltott, 
			-- u.Azonosito AS Azonosito
			(SELECT MegKulJelzes FROM EREC_IraIktatokonyvek WHERE Id = u.IraIktatokonyv_Id) + '/' + CAST(u.Foszam AS varchar) + '/' 
				+ CAST((SELECT Ev FROM EREC_IraIktatokonyvek WHERE Id = u.IraIktatokonyv_Id) AS varchar) AS Azonosito
		FROM [dbo].[EREC_UgyUgyiratok] u
			LEFT JOIN EREC_IraKezbesitesiTetelek t
			ON t.Obj_Id = u.Id
				AND t.LetrehozasIdo = (SELECT Max(t.LetrehozasIdo)
							FROM EREC_UgyUgyiratok u
							LEFT JOIN EREC_IraKezbesitesiTetelek t
							ON t.Obj_Id = u.Id
				)
		WHERE u.Csoport_Id_Felelos IN (SELECT CsoportId
												FROM #temp_SzervezetTagok
										)
			AND (u.LetrehozasIdo BETWEEN @KezdDat AND @VegeDat
				OR u.Allapot = @allapot_ugyintezes_alatt
				OR u.Allapot = @allapot_iktatott
				OR u.Allapot = @allapot_szignalt
				OR u.Allapot = @allapot_skontroban) 
		ORDER BY Datum DESC

		--FOR XML RAW, ELEMENTS

		IF OBJECT_ID('tempdb..#temp_SzervezetTagok') IS NOT NULL 
			BEGIN
			   DROP TABLE #temp_SzervezetTagok
			END

	END TRY
	BEGIN CATCH
		DECLARE @errorSeverity INT,
						@errorState INT
		DECLARE @errorCode NVARCHAR(1000)
		SET @errorSeverity = ERROR_SEVERITY()
		SET @errorState = ERROR_STATE()
	
		IF ERROR_NUMBER() < 50000 
			SET @errorCode = '[' + CONVERT(NVARCHAR(10), ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
		ELSE 
			SET @errorCode = ERROR_MESSAGE()
			
		IF @errorState = 0 
			SET @errorState = 1
				
		RAISERROR ( @errorCode, @errorSeverity, @errorState )
 
	END CATCH

END