/*

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_FeladatErtesitesGet')
            and   type = 'P')
   drop procedure sp_EREC_FeladatErtesitesGet
go
*/
create procedure sp_EREC_FeladatErtesitesGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_FeladatErtesites.Id,
	   EREC_FeladatErtesites.FeladatDefinicio_id,
	   EREC_FeladatErtesites.Felhasznalo_Id,
	   EREC_FeladatErtesites.Ertesites,
	   EREC_FeladatErtesites.Ver,
	   EREC_FeladatErtesites.Note,
	   EREC_FeladatErtesites.Stat_id,
	   EREC_FeladatErtesites.ErvKezd,
	   EREC_FeladatErtesites.ErvVege,
	   EREC_FeladatErtesites.Letrehozo_id,
	   EREC_FeladatErtesites.LetrehozasIdo,
	   EREC_FeladatErtesites.Modosito_id,
	   EREC_FeladatErtesites.ModositasIdo,
	   EREC_FeladatErtesites.Zarolo_id,
	   EREC_FeladatErtesites.ZarolasIdo,
	   EREC_FeladatErtesites.Tranz_id,
	   EREC_FeladatErtesites.UIAccessLog_id
	   from 
		 EREC_FeladatErtesites as EREC_FeladatErtesites 
	   where
		 EREC_FeladatErtesites.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
