/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_FeladatErtesitesGetAll')
            and   type = 'P')
   drop procedure sp_EREC_FeladatErtesitesGetAll
go
*/
create procedure sp_EREC_FeladatErtesitesGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_FeladatErtesites.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_FeladatErtesites.Id,
	   EREC_FeladatErtesites.FeladatDefinicio_id,
	   EREC_FeladatErtesites.Felhasznalo_Id,
	   EREC_FeladatErtesites.Ertesites,
	   EREC_FeladatErtesites.Ver,
	   EREC_FeladatErtesites.Note,
	   EREC_FeladatErtesites.Stat_id,
	   EREC_FeladatErtesites.ErvKezd,
	   EREC_FeladatErtesites.ErvVege,
	   EREC_FeladatErtesites.Letrehozo_id,
	   EREC_FeladatErtesites.LetrehozasIdo,
	   EREC_FeladatErtesites.Modosito_id,
	   EREC_FeladatErtesites.ModositasIdo,
	   EREC_FeladatErtesites.Zarolo_id,
	   EREC_FeladatErtesites.ZarolasIdo,
	   EREC_FeladatErtesites.Tranz_id,
	   EREC_FeladatErtesites.UIAccessLog_id  
   from 
     EREC_FeladatErtesites as EREC_FeladatErtesites      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go