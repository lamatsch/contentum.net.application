/*
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
*/
CREATE procedure [dbo].[sp_EREC_FeladatErtesitesGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' ORDER BY Funkcio_Nev_Kivalto ASC',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
	   EREC_FeladatErtesites.Id,
	   EREC_FeladatErtesites.FeladatDefinicio_id,
	   EREC_FeladatErtesites.Felhasznalo_Id,
	   EREC_FeladatErtesites.Ertesites,
	   EREC_FeladatErtesites.Ver,
	   EREC_FeladatErtesites.Note,
	   EREC_FeladatErtesites.Stat_id,
	   EREC_FeladatErtesites.ErvKezd,
	   EREC_FeladatErtesites.ErvVege,
	   EREC_FeladatErtesites.Letrehozo_id,
	   EREC_FeladatErtesites.LetrehozasIdo,
	   EREC_FeladatErtesites.Modosito_id,
	   EREC_FeladatErtesites.ModositasIdo,
	   EREC_FeladatErtesites.Zarolo_id,
	   EREC_FeladatErtesites.ZarolasIdo,
	   EREC_FeladatErtesites.Tranz_id,
	   EREC_FeladatErtesites.UIAccessLog_id,
	   FUNC_KIV.Nev as Funkcio_Nev_Kivalto, 
       DETIP.Nev as FeladatDefinicioTipus_Nev,
	   FELH.Nev as ErtesitesFelhasznaloNev
  FROM EREC_FeladatErtesites as EREC_FeladatErtesites 
  LEFT JOIN EREC_FeladatDefinicio AS FD ON EREC_FeladatErtesites.FeladatDefinicio_id = FD.Id
  LEFT JOIN KRT_Funkciok as FUNC_KIV ON FD.Funkcio_Id_Kivalto=FUNC_KIV.Id  
  LEFT JOIN KRT_KodTarak as DETIP ON DETIP.KodCsoport_Id=
				(select top 1 Id from KRT_KodCsoportok where Kod=''FELADAT_DEFINICIO_TIPUS'')
			    and FD.FeladatDefinicioTipus=DETIP.Kod COLLATE Hungarian_CI_AS and DETIP.Org=''' + CAST(@Org as NVarChar(40)) + '''
  LEFT JOIN KRT_Felhasznalok AS FELH ON EREC_FeladatErtesites.Felhasznalo_Id = FELH.Id
  Where FD.Org=''' + CAST(@Org as Nvarchar(40)) + '''
'

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end 
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
 
   /*DEBUG*/
   --PRINT @sqlcmd

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


