﻿create procedure [dbo].[sp_EREC_HataridosFeladatokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_HataridosFeladatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_HataridosFeladatok.Id,
	   EREC_HataridosFeladatok.Esemeny_Id_Kivalto,
	   EREC_HataridosFeladatok.Esemeny_Id_Lezaro,
	   EREC_HataridosFeladatok.Funkcio_Id_Inditando,
	   EREC_HataridosFeladatok.Csoport_Id_Felelos,
	   EREC_HataridosFeladatok.Felhasznalo_Id_Felelos,
	   EREC_HataridosFeladatok.Csoport_Id_Kiado,
	   EREC_HataridosFeladatok.Felhasznalo_Id_Kiado,
	   EREC_HataridosFeladatok.HataridosFeladat_Id,
	   EREC_HataridosFeladatok.ReszFeladatSorszam,
	   EREC_HataridosFeladatok.FeladatDefinicio_Id,
	   EREC_HataridosFeladatok.FeladatDefinicio_Id_Lezaro,
	   EREC_HataridosFeladatok.Forras,
	   EREC_HataridosFeladatok.Tipus,
	   EREC_HataridosFeladatok.Altipus,
	   EREC_HataridosFeladatok.Memo,
	   EREC_HataridosFeladatok.Leiras,
	   EREC_HataridosFeladatok.Prioritas,
	   EREC_HataridosFeladatok.LezarasPrioritas,
	   EREC_HataridosFeladatok.KezdesiIdo,
	   EREC_HataridosFeladatok.IntezkHatarido,
	   EREC_HataridosFeladatok.LezarasDatuma,
	   EREC_HataridosFeladatok.Azonosito,
	   EREC_HataridosFeladatok.Obj_Id,
	   EREC_HataridosFeladatok.ObjTip_Id,
	   EREC_HataridosFeladatok.Obj_type,
	   EREC_HataridosFeladatok.Allapot,
	   EREC_HataridosFeladatok.Megoldas,
	   EREC_HataridosFeladatok.Note,
	   EREC_HataridosFeladatok.Ver,
	   EREC_HataridosFeladatok.Stat_id,
	   EREC_HataridosFeladatok.ErvKezd,
	   EREC_HataridosFeladatok.ErvVege,
	   EREC_HataridosFeladatok.Letrehozo_id,
	   EREC_HataridosFeladatok.LetrehozasIdo,
	   EREC_HataridosFeladatok.Modosito_id,
	   EREC_HataridosFeladatok.ModositasIdo,
	   EREC_HataridosFeladatok.Zarolo_id,
	   EREC_HataridosFeladatok.ZarolasIdo,
	   EREC_HataridosFeladatok.Tranz_id,
	   EREC_HataridosFeladatok.UIAccessLog_id  
   from 
     EREC_HataridosFeladatok as EREC_HataridosFeladatok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end