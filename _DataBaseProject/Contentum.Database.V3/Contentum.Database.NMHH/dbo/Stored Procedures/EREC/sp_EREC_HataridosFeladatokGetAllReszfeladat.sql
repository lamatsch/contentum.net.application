﻿create procedure [dbo].[sp_EREC_HataridosFeladatokGetAllReszfeladat]
  @FeladatId UNIQUEIDENTIFIER,
  @ExecutorUserId uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier

as

begin

BEGIN TRY

   set nocount ON;
   
   WITH FeladatHierarchy  AS
	(
	   -- Base case
	   SELECT Id,HataridosFeladat_Id,
			  0 as HierarchyLevel
	   FROM [EREC_HataridosFeladatok]
	   WHERE Id = @FeladatId

	   UNION ALL

	   -- Recursive step
	   SELECT f.Id,f.HataridosFeladat_Id,
		  fh.HierarchyLevel + 1 AS HierarchyLevel
	   FROM [EREC_HataridosFeladatok] as f
		  INNER JOIN FeladatHierarchy as fh ON
			 f.HataridosFeladat_Id = fh.Id
	)
	
	SELECT *
	FROM FeladatHierarchy
   
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end