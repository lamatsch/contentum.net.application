﻿--set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--if exists (select 1
--            from  sysobjects
--           where  id = object_id('sp_EREC_HataridosFeladatokGetAllWithExtension')
--            and   type = 'P')
--   drop procedure sp_EREC_HataridosFeladatokGetAllWithExtension
--go
create procedure [dbo].[sp_EREC_HataridosFeladatokGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_HataridosFeladatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @FelhasznaloSzervezet_Id	uniqueidentifier,
  @pageNumber		int = 0,
  @pageSize			int = -1,
  @SelectedRowId	uniqueidentifier = NULL,
  @DisplayChildsIfParentNotVisible CHAR(1) = '0',
  @Jogosultak		char(1) = '0',
  @ParentFeladatId UNIQUEIDENTIFIER = NULL

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(MAX)
   DECLARE @LocalTopRow nvarchar(10)
   declare @firstRow int
   declare @lastRow INT

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
		if (@pageSize > @TopRow)
			SET @pageSize = @TopRow;
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
                  
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = ISNULL((SELECT TOP (1) Ertek FROM KRT_Parameterek WHERE Nev = 'LISTA_ELEMSZAM_MAX'),1000);
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	END
	

	
	SET @sqlcmd = 'select EREC_HataridosFeladatok.Id, EREC_HataridosFeladatok.HataridosFeladat_Id into #filter from EREC_HataridosFeladatok'
	
   if @Where is not null and @Where!=''
   begin 
	  SET @sqlcmd = @sqlcmd + ' Where ' + @Where
   END

	IF @Jogosultak = '1'
	BEGIN
		SET @sqlcmd = @sqlcmd + ' DELETE FROM #filter WHERE Id NOT IN
									(
										Select Id from dbo.fn_GetAllRightedEREC_HataridosFeladatok(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''')
									)'

	END	

	IF (@DisplayChildsIfParentNotVisible = '1')
	BEGIN
		IF @ParentFeladatId is null
		BEGIN
			SET @sqlcmd = @sqlcmd + N'
			SELECT Id, HataridosFeladat_Id INTO #displayChilds FROM #filter
			WHERE [#filter].[HataridosFeladat_Id] IS NOT NULL
			AND [#filter].[HataridosFeladat_Id] NOT IN (SELECT Id FROM #filter)
			IF (@@ROWCOUNT > 0)
			BEGIN
				WITH ParentFeldatok AS
			   (
				SELECT #displayChilds.Id AS ChildId, #displayChilds.Id AS Id, #displayChilds.HataridosFeladat_Id AS HataridosFeladat_Id
				FROM #displayChilds

				UNION ALL

				SELECT ParentFeldatok.ChildId as ChildId,[EREC_HataridosFeladatok].Id AS Id, [EREC_HataridosFeladatok].HataridosFeladat_Id AS HataridosFeladat_Id
				FROM [EREC_HataridosFeladatok]
				INNER JOIN ParentFeldatok ON
				[EREC_HataridosFeladatok].Id = ParentFeldatok.[HataridosFeladat_Id]
				AND [EREC_HataridosFeladatok].Id NOT IN (SELECT Id FROM #filter)
				)
				SELECT * INTO #parents FROM ParentFeldatok
				WHERE [ParentFeldatok].HataridosFeladat_Id IN (SELECT Id FROM #filter)
	            
				DELETE FROM #displayChilds
				WHERE Id IN (SELECT #parents.ChildId FROM #parents)
	            
	            
		   END
	       
		   DELETE FROM #filter
		   WHERE HataridosFeladat_Id IS NOT NULL
		   AND Id NOT IN (SELECT Id FROM #displayChilds)
			'
	  END
	  ELSE
	  BEGIN
	  	SET @sqlcmd = @sqlcmd + N'
	  	DELETE FROM #filter
		WHERE HataridosFeladat_Id is NULL;
		WITH ChildFeladatok AS
		(
		
		SELECT Id AS Id, EREC_HataridosFeladatok.HataridosFeladat_Id as HataridosFeladat_Id
		FROM EREC_HataridosFeladatok
		WHERE EREC_HataridosFeladatok.HataridosFeladat_Id = ''' + cast(@ParentFeladatId as char(36)) + '''
		AND EREC_HataridosFeladatok.Id NOT IN (SELECT Id FROM #filter)

		UNION ALL

		SELECT EREC_HataridosFeladatok.Id AS Id, EREC_HataridosFeladatok.HataridosFeladat_Id as HataridosFeladat_Id
		FROM EREC_HataridosFeladatok
		INNER JOIN ChildFeladatok ON
		EREC_HataridosFeladatok.HataridosFeladat_Id = ChildFeladatok.Id
		)

		SELECT * INTO #displayChilds FROM ChildFeladatok
		WHERE ChildFeladatok.HataridosFeladat_Id != ''' + cast(@ParentFeladatId as char(36)) + ''';

		WITH ParentFeladatok AS
		(

		SELECT #displayChilds.Id AS ChildId, #displayChilds.Id AS Id, #displayChilds.HataridosFeladat_Id as HataridosFeladat_Id
		FROM #displayChilds

		UNION ALL

		SELECT ParentFeladatok.ChildId AS ChildId, EREC_HataridosFeladatok.Id AS Id, EREC_HataridosFeladatok.HataridosFeladat_Id as HataridosFeladat_Id
		FROM EREC_HataridosFeladatok
		INNER JOIN ParentFeladatok ON
		EREC_HataridosFeladatok.Id = ParentFeladatok.[HataridosFeladat_Id]
		AND [EREC_HataridosFeladatok].[HataridosFeladat_Id] != ''' + cast(@ParentFeladatId as char(36)) + '''
		)
		
		SELECT * INTO #parents FROM ParentFeladatok
		WHERE [ParentFeladatok].HataridosFeladat_Id IN (SELECT Id FROM #filter)
        
		DELETE FROM #displayChilds
		WHERE Id IN (SELECT #parents.ChildId FROM #parents)
		
		DELETE FROM #filter
		WHERE HataridosFeladat_Id != ''' + cast(@ParentFeladatId as char(36)) + '''
		AND Id NOT IN (SELECT Id FROM #displayChilds)
		'
	  END
	END
				   
   /************************************************************
	* Szurt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/
		SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	    EREC_HataridosFeladatok.Id into #result
        from EREC_HataridosFeladatok as EREC_HataridosFeladatok
        LEFT JOIN KRT_Csoportok AS KRT_Csoportok_Kiado_Csoport ON EREC_HataridosFeladatok.Csoport_Id_Kiado = KRT_Csoportok_Kiado_Csoport.Id
        LEFT JOIN KRT_Csoportok AS KRT_Csoportok_Kiado ON EREC_HataridosFeladatok.Felhasznalo_Id_kiado = KRT_Csoportok_Kiado.Id
        LEFT JOIN KRT_Csoportok AS KRT_Csoportok_Felelos_Csoport ON EREC_HataridosFeladatok.Csoport_id_felelos = KRT_Csoportok_Felelos_Csoport.Id
        LEFT JOIN KRT_Csoportok AS KRT_Csoportok_Felelos ON EREC_HataridosFeladatok.Felhasznalo_Id_Felelos = KRT_Csoportok_Felelos.Id
        left join KRT_KodCsoportok AS KRT_KodCsoportok_Allapot on KRT_KodCsoportok_Allapot.Kod=''FELADAT_ALLAPOT''
        left join KRT_KodTarak AS KRT_KodTarak_Allapot on EREC_HataridosFeladatok.Allapot COLLATE Hungarian_CS_AS = KRT_KodTarak_Allapot.Kod COLLATE Hungarian_CS_AS and KRT_KodCsoportok_Allapot.Id = KRT_KodTarak_Allapot.KodCsoport_Id and KRT_KodTarak_Allapot.Org=''' + CAST(@Org as NVarChar(40)) + '''
		left join KRT_KodCsoportok AS KRT_KodCsoportok_Tipus on KRT_KodCsoportok_Tipus.Kod=''FELADAT_TIPUS''
        left join KRT_KodTarak AS KRT_KodTarak_Tipus on EREC_HataridosFeladatok.Tipus COLLATE Hungarian_CS_AS = KRT_KodTarak_Tipus.Kod COLLATE Hungarian_CS_AS and KRT_KodCsoportok_Tipus.Id = KRT_KodTarak_Tipus.KodCsoport_Id and KRT_KodTarak_Tipus.Org=''' + CAST(@Org as NVarChar(40)) + '''
        left join KRT_KodCsoportok AS KRT_KodCsoportok_AlTipus on KRT_KodCsoportok_AlTipus.Kod=''FELADAT_ALTIPUS''
        left join KRT_KodTarak AS KRT_KodTarak_AlTipus on EREC_HataridosFeladatok.AlTipus COLLATE Hungarian_CS_AS = KRT_KodTarak_AlTipus.Kod COLLATE Hungarian_CS_AS and KRT_KodCsoportok_AlTipus.Id = KRT_KodTarak_AlTipus.KodCsoport_Id and KRT_KodTarak_AlTipus.Org=''' + CAST(@Org as NVarChar(40)) + '''
        LEFT JOIN KRT_Funkciok AS KRT_Funkciok_Inditando ON EREC_HataridosFeladatok.Funkcio_Id_Inditando = KRT_Funkciok_Inditando.Id
		LEFT JOIN KRT_Felhasznalok AS KRT_Felhasznalok_Zarolo ON EREC_HataridosFeladatok.Zarolo_id = KRT_Felhasznalok_Zarolo.Id
		LEFT JOIN KRT_Felhasznalok AS KRT_Felhasznalok_Letrehozo ON EREC_HataridosFeladatok.Letrehozo_id= KRT_Felhasznalok_Letrehozo.Id
		where EREC_HataridosFeladatok.Id in (select Id from #filter); '
		
	if (@SelectedRowId is not null)
			set @sqlcmd = @sqlcmd + N'
			if exists (select 1 from #result where Id = ''' + cast(@SelectedRowId as nvarchar(36)) + ''')
			BEGIN
				select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = ''' + cast(@SelectedRowId as nvarchar(36)) + ''';
				set @firstRow = (@pageNumber - 1) * @pageSize + 1;
				set @lastRow = @pageNumber*@pageSize;
				select @pageNumber = @pageNumber - 1
			END
			ELSE'
		--ELSE
			set @sqlcmd = @sqlcmd + N' 
				if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
				BEGIN
					select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
					set @firstRow = (@pageNumber - 1) * @pageSize + 1;
					set @lastRow = @pageNumber*@pageSize;
					select @pageNumber = @pageNumber - 1
				END ;'	
  
  /************************************************************
	* Tényleges select											*
	************************************************************/
		set @sqlcmd = @sqlcmd + N'
		select 
		#result.RowNumber,
		#result.Id,
	   EREC_HataridosFeladatok.Esemeny_Id_Kivalto,
	   Esemeny_Kivalto = (select KRT_Funkciok.Nev from KRT_Esemenyek inner join KRT_Funkciok on KRT_Funkciok.Id = KRT_Esemenyek.Funkcio_Id where KRT_Esemenyek.Id = EREC_HataridosFeladatok.Esemeny_Id_Kivalto),
	   EREC_HataridosFeladatok.Esemeny_Id_Lezaro,
       Esemeny_Lezaro = (select KRT_Funkciok.Nev from KRT_Esemenyek inner join KRT_Funkciok on KRT_Funkciok.Id = KRT_Esemenyek.Funkcio_Id where KRT_Esemenyek.Id = EREC_HataridosFeladatok.Esemeny_Id_Lezaro),
	   EREC_HataridosFeladatok.Funkcio_Id_Inditando,
	   (Select Nev from KRT_Funkciok where KRT_Funkciok.Id = EREC_HataridosFeladatok.Funkcio_Id_Inditando) as Funkcio_Nev_Inditando,
	   EREC_HataridosFeladatok.Csoport_id_felelos,
	   dbo.fn_GetCsoportNev(EREC_HataridosFeladatok.Csoport_id_felelos) as Felelos_Csoport_Nev,
	   EREC_HataridosFeladatok.Felhasznalo_Id_Felelos,
	   dbo.fn_GetCsoportNev(EREC_HataridosFeladatok.Felhasznalo_Id_Felelos) as Felelos_Nev,
	   EREC_HataridosFeladatok.Csoport_Id_Kiado,
       dbo.fn_GetCsoportNev(EREC_HataridosFeladatok.Csoport_Id_Kiado) as Kiado_Csoport_Nev,
	   EREC_HataridosFeladatok.Felhasznalo_Id_kiado,
	   dbo.fn_GetCsoportNev(EREC_HataridosFeladatok.Felhasznalo_Id_kiado) as Kiado_Nev,
	   EREC_HataridosFeladatok.HataridosFeladat_Id,
	   EREC_HataridosFeladatok.Forras,
	   EREC_HataridosFeladatok.Tipus,
	   EREC_HataridosFeladatok.Altipus,
	   dbo.fn_KodtarErtekNeve(''FELADAT_ALTIPUS'', EREC_HataridosFeladatok.Altipus,''' + CAST(@Org as NVarChar(40)) + ''')as Altipus_Nev,
	   dbo.fn_KodtarErtekNeve(''FELADAT_ALTIPUS'', EREC_HataridosFeladatok.Altipus,''' + CAST(@Org as NVarChar(40)) + ''')as KezelesTipusNev,
	   dbo.fn_KodtarErtekNeve(''FELADAT_TIPUS'', EREC_HataridosFeladatok.Tipus,''' + CAST(@Org as NVarChar(40)) + ''')as Tipus_Nev,
	   EREC_HataridosFeladatok.Leiras,
	   EREC_HataridosFeladatok.Prioritas,
	   dbo.fn_KodtarErtekNeve(''FELADAT_PRIORITAS'', EREC_HataridosFeladatok.Prioritas,''' + CAST(@Org as NVarChar(40)) + ''')as Prioritas_Nev,
	   EREC_HataridosFeladatok.LezarasPrioritas,
	   dbo.fn_KodtarErtekNeve(''FELADAT_PRIORITAS'', EREC_HataridosFeladatok.LezarasPrioritas,''' + CAST(@Org as NVarChar(40)) + ''')as LezarasPrioritas_Nev,
	   EREC_HataridosFeladatok.KezdesiIdo,
	   EREC_HataridosFeladatok.IntezkHatarido,
	   EREC_HataridosFeladatok.LezarasDatuma,
	   EREC_HataridosFeladatok.Megoldas,
	   EREC_HataridosFeladatok.Allapot,
	   dbo.fn_KodtarErtekNeve(''FELADAT_ALLAPOT'', EREC_HataridosFeladatok.Allapot,''' + CAST(@Org as NVarChar(40)) + ''')as Allapot_Nev,
	   (select count(ReszFeladatok.Id) from EREC_HataridosFeladatok as ReszFeladatok Where ReszFeladatok.HataridosFeladat_Id = #result.Id
	   and getdate() between  ReszFeladatok.ErvKezd and ReszFeladatok.ErvVege) as ReszFeladatokCount,
	   EREC_HataridosFeladatok.Memo,
	   EREC_HataridosFeladatok.Obj_Id,
	   EREC_HataridosFeladatok.Azonosito,
	   EREC_HataridosFeladatok.Azonosito as Foszam_Merge,
	   EREC_HataridosFeladatok.Azonosito as FullErkeztetoSzam,
	   EREC_HataridosFeladatok.Azonosito as IktatoSzam_Merge,
	   EREC_HataridosFeladatok.ObjTip_Id,
	   EREC_HataridosFeladatok.Obj_type,
	   EREC_HataridosFeladatok.Ver,
	   EREC_HataridosFeladatok.Note,
	   EREC_HataridosFeladatok.Stat_id,
	   EREC_HataridosFeladatok.ErvKezd,
	   EREC_HataridosFeladatok.ErvVege,
	   EREC_HataridosFeladatok.Letrehozo_id,
	   dbo.fn_GetFelhasznaloNev(EREC_HataridosFeladatok.Letrehozo_id) as Letrehozo_Nev,
	   CONVERT(nvarchar(10), EREC_HataridosFeladatok.LetrehozasIdo, 102) as LetrehozasIdo,
	   CONVERT(nvarchar(10), EREC_HataridosFeladatok.LetrehozasIdo, 102) as LetrehozasIdo_f,
	   EREC_HataridosFeladatok.Modosito_id,
	   EREC_HataridosFeladatok.ModositasIdo,
	   EREC_HataridosFeladatok.Zarolo_id,
	   dbo.fn_GetFelhasznaloNev(EREC_HataridosFeladatok.Zarolo_id) as Zarolo_Nev,
	   EREC_HataridosFeladatok.ZarolasIdo,
	   EREC_HataridosFeladatok.UIAccessLog_id  
   from 
     EREC_HataridosFeladatok as EREC_HataridosFeladatok
	 inner join #result on #result.Id = EREC_HataridosFeladatok.Id
	 where RowNumber between @firstRow and @lastRow
	 ORDER BY #result.RowNumber;'
       
   -- találatok száma és oldalszám
   set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter;';
   
   --PRINT @sqlcmd;

   execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end