﻿
-- Cél: Államigazgatási hatósági statisztikához ügyiratforgalom kimutatása.
-- kötött (védett Excelben adott) bontásban, ágazatok szerint
-- az „A”) adatlap alapján kitöltött értékek figyelembevételével 
-- 
-- Továbbá: csak a második szintet figyeljuk, pl. U.1
-- de nem lehet csak U (mert azt az Excelben kapjuk összegként)
-- es erre a szintre vonjuk össze a mélyebb hierarchia-szinteket
-- is, pl. U.3.1, U.3.2 stb. az U.3 alá
-- Az összevonásnál kihasznaljuk az ABC-sorrendet,
-- valamint, hogy az ágazati jelek
-- listáján nincs egyjegyűnél nagyobb számjel!!!
-- 

--set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--if exists (select 1
--            from  sysobjects
--           where  id = object_id('sp_EREC_HatosagiStatisztikaAllamigazgatas')
--            and   type = 'P')
--   drop procedure sp_EREC_HatosagiStatisztikaAllamigazgatas
--go


CREATE PROCEDURE [dbo].[sp_EREC_HatosagiStatisztikaAllamigazgatas]
    @Where NVARCHAR(4000) = '',
    @OrderBy NVARCHAR(200) = '',
    @TopRow NVARCHAR(5) = '',
    @ExecutorUserId UNIQUEIDENTIFIER,
    @FelhasznaloSzervezet_Id UNIQUEIDENTIFIER = null,
    @KezdDat datetime,
    @VegeDat datetime,
	@sheet nvarchar(100) = 'székhely' -- a kitoltendo Excel-tabla neve
AS 
    BEGIN
        BEGIN TRY

            SET nocount ON


DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

-- konstansok
DECLARE @Id_kcs_UgyFajtaja uniqueidentifier
SET @Id_kcs_UgyFajtaja = (
	SELECT Id FROM KRT_Kodcsoportok
	WHERE Kod ='UGY_FAJTAJA'
)

DECLARE @Kod_Allamigazgatasi nvarchar(64)
SET @Kod_Allamigazgatasi = '1'

-- a kitoltendo Excel-tabla neve
--DECLARE @sheet nvarchar(100)
--	SET @sheet = 'ökormányzat_székhelye' -- sic! (hibas az Excelben)


-- 1. Iktatott ügyiratok és hatósági adataik

DECLARE @TempTabla table (
	--Id uniqueidentifier,
	Kod nvarchar(20),
	Darab int,
	--Datum datetime,
	UgyFajtaja nvarchar(64),
	DontestHozta nvarchar(64),
	DontesFormaja nvarchar(64),
	UgyintezesHataridore nvarchar(64),
	JogorvoslatiEljarasTipusa nvarchar(64),
	JogorvoslatiDontesTipusa nvarchar(64),
	JogorvoslatiDontestHozta nvarchar(64),
	JogorvoslatiDontesTartalma nvarchar(64),
	JogorvoslatiDontes nvarchar(64)
)

-- 2. Iktatott ügyiratok és új hatósági adatok: HatosagiEllenorzes, MunkaorakSzama, EljarasiKoltseg, KozigazgatasiBirsagMerteke
DECLARE @TempTable_UjAdatok table (
	Kod nvarchar(20),
	Id uniqueidentifier,
	Ugyirat_Id uniqueidentifier,
	HatosagiEllenorzes char(1),
	MunkaorakSzama float,
	EljarasiKoltseg int,
	KozigazgatasiBirsagMerteke int
)

-- 3. 2016-os új hatósági adatok 
DECLARE @TempTable_2016UjAdatok table (
	Kod nvarchar(20),
	Id uniqueidentifier,
	Ugyirat_Id uniqueidentifier,
	SommasEljDontes nvarchar(64),
	NyolcNapBelulNemSommas nvarchar(64),
	FuggoHatalyuHatarozat nvarchar(64),
	FuggoHatHatalybaLepes nvarchar(64),
	FuggoHatalyuVegzes nvarchar(64),
	FuggoVegzesHatalyba nvarchar(64),
	HatAltalVisszafizOsszeg int,
	HatTerheloEljKtsg int,
	FelfuggHatarozat nvarchar(64)
)

-- 4. 2016-os új eljárás számadatok 
DECLARE @TempTable_2016EljSzamaAdatok table (
	Kod nvarchar(20),
	Id uniqueidentifier,
	Ugyirat_Id uniqueidentifier,
	EljSzamaElozofelevrolLezart int, -- előző félévről áthúzódó lezárt
	EljSzamaElozofelevrolFolyamatban int, -- előző félévről áthúzódó folyamatban
	EljSzamaMegismeteltLezart int, -- megismételt lezárt
	EljSzamaMegismeteltFolyamatban int, -- megismételt folyamatban
	EljSzamaTargyfelevbenIndultLezart int, -- tárgyfélévben indult lezárt
	EljSzamaTargyfelevbenIndultFolyamatban int-- tárgyfélévben indult folyamatban
)


DECLARE @Reszletes_Tabla table (
	--Id uniqueidentifier,
	Kod nvarchar(20),
	Donteshozo4 int, -- (fő)polgármester
	Donteshozo5 int, -- (fő)jegyző
	Donteshozo6 int, -- (fő)polgármesteri hivatal ügyintézője
	ErdemiDontes1 int, -- önálló határozat
	ErdemiDontes2 int, -- egyezség jóváhagyás
	ErdemiDontes3 int, -- hatósági bizonyítvány
	ErdemiDontes4 int, -- hatósági szerződések
	Vegzes5 int, -- Ket. 30
	Vegzes6 int, -- Ket. 31
	Vegzes9 int, -- Ket. 31 (1)
	Vegzes7 int, -- elsőfokú egyéb
	Vegzes8 int, -- végrehajtás
    Hatarido1 int, -- határidőn belül
    Hatarido2 int, -- határidőn túl
	JogorvoslatiDontesTartalma1 int, -- Kijavitott, kicserelt, kiegeszitett
	FellebbezesAlapjan int, -- módosított, visszavont
	VegzesKozighivJogorv5 int, -- Végzés, közigazgatási hivatal, helybenhagy 
	VegzesKozighivJogorv6 int, -- Végzés, közigazgatási hivatal, megváltoztat 
	VegzesKozighivJogorv7 int, -- Végzés, közigazgatási hivatal, megsemmisít 
	VegzesKozighivJogorv8 int, -- Végzés, közigazgatási hivatal, megsemmisít és új
	VegzesDekoncentraltJogorv5 int, -- Végzés, dekoncentrált szerv, helybenhagy 
	VegzesDekoncentraltJogorv6 int, -- Végzés, dekoncentrált szerv, megváltoztat 
	VegzesDekoncentraltJogorv7 int, -- Végzés, dekoncentrált szerv, megsemmisít 
	VegzesDekoncentraltJogorv8 int, -- Végzés, dekoncentrált szerv, megsemmisít és új
	VegzesBirosagJogorv5 int, -- Végzés, bíróság, elutasít 
	VegzesBirosagJogorv6 int, -- Végzés, bíróság, megváltoztat 
	VegzesBirosagJogorv7 int, -- Végzés, bíróság, hatályon kívül 
	VegzesBirosagJogorv8 int, -- Végzés, bíróság, hatályon kívül és új
    ErdemiUjrafelvJogorv5 int, -- Érdemi döntés, elsőfokú újrafelvételi, elutasít
    ErdemiUjrafelvJogorv2 int, -- Érdemi döntés, elsőfokú újrafelvételi, módosít
    ErdemiUjrafelvJogorv3 int, -- Érdemi döntés, elsőfokú újrafelvételi, visszavon
    ErdemiUjrafelvJogorv4 int, -- Érdemi döntés, elsőfokú újrafelvételi, új döntés
    ErdemiMeltanyosJogorv5 int, -- Érdemi döntés, elsőfokú méltányossági, elutasít
    ErdemiMeltanyosJogorv2 int, -- Érdemi döntés, elsőfokú méltányossági, módosít
    ErdemiMeltanyosJogorv3 int, -- Érdemi döntés, elsőfokú méltányossági, visszavon
    ErdemiKozighivJogorv5 int, -- Érdemi döntés, közigazgatási hivatal, helybenhagy
    ErdemiKozighivJogorv6 int, -- Érdemi döntés, közigazgatási hivatal, megváltoztat 
    ErdemiKozighivJogorv7 int, -- Érdemi döntés, közigazgatási hivatal, megsemmisít
    ErdemiKozighivJogorv8 int, -- Érdemi döntés, közigazgatási hivatal, megsemmisít és új
    ErdemiDekoncentraltJogorv5 int, -- Érdemi döntés, dekoncentrált szerv, helybenhagy
    ErdemiDekoncentraltJogorv6 int, -- Érdemi döntés, dekoncentrált szerv, megváltoztat 
    ErdemiDekoncentraltJogorv7 int, -- Érdemi döntés, dekoncentrált szerv, megsemmisít
    ErdemiDekoncentraltJogorv8 int, -- Érdemi döntés, dekoncentrált szerv, megsemmisít és új
    ErdemiBirosagJogorv5 int, -- Érdemi döntés, bíróság, elutasít
    ErdemiBirosagJogorv6 int, -- Érdemi döntés, bíróság, megváltoztat 
    ErdemiBirosagJogorv7 int, -- Érdemi döntés, bíróság, hatályon kívül
    ErdemiBirosagJogorv8 int, -- Érdemi döntés, bíróság, hatályon kívül és új
    HivatalbolModositVisszavon int, -- Hivatalból módosított és visszavont
    HivatalbolFelugyeleti6 int, -- Hivatalból, felügyeleti szerv, megváltoztat
    HivatalbolFelugyeleti7 int, -- Hivatalból, felügyeleti szerv, megsemmisít
    HivatalbolFelugyeleti8 int, -- Hivatalból, felügyeleti szerv, megsemmisít és új
    HatosagiEllenorzesekSzama int, -- hatósági ellenőrzések száma
	MunkaorakSzamaAtlagosan numeric(7,1), -- egy ügyre fordított munkaórák száma
	AtlagosEljarasiKoltseg numeric(9,1), -- egy ügyre jutó eljárási költség
	AtlagosKozigazgatasiBirsag numeric(11,1), -- az eljárásban kiszabott közigazgatási bírság átlagos mértéke 
	EljSzamaElozofelevrolLezartSzama int, -- előző félévről áthúzódó lezárt
	EljSzamaElozofelevrolFolyamatbanSzama int, -- előző félévről áthúzódó folyamatban
	EljSzamaMegismeteltLezartSzama int, -- megismételt lezárt
	EljSzamaMegismeteltFolyamatbanSzama int, -- megismételt folyamatban
	EljSzamaTargyfelevbenIndultLezartSzama int, -- tárgyfélévben indult lezárt
	EljSzamaTargyfelevbenIndultFolyamatbanSzama int, -- tárgyfélévben indult folyamatban
	SommasEljSzama int, -- sommás eljárások száma
	NyolcnaponBelulNemSommasSzama int, -- 8 napon belül nem sommás
	FuggoHatalyuHatarozatSzama int,
	FuggoHatalyuVegzesSzama int,
	FuggoHatalyuHatarozatNemHatalyos int,
	FuggoHatalyuHatarozatHatalyos int,
	FuggoHatalyuDontesNemHatalyos int,
	FuggoHatalyuDontesHatalyos int,
	VisszafizetettOsszeg int,
	HatosagotTerheloEljarasiKoltseg int
	)


-- az Excel-tabla kitoltesehez a vezerlo informaciokat tartalmazza
DECLARE @Excel_Tabla table (
	Kod nvarchar(20),
	Cells nvarchar(MAX)
)		

-- ha WHERE feltetel van, elvesznek az "ervenytelen"
-- oszlopokhoz tartozo darabertekek, ezert ket lepesben
-- szamolunk

INSERT INTO @TempTabla
SELECT SUBSTRING(a.Kod, 1, 3) AS Kod, count(onk.Id) AS Darab, onk.UgyFajtaja, onk.DontestHozta, onk.DontesFormaja, onk.UgyintezesHataridore, onk.JogorvoslatiEljarasTipusa, onk.JogorvoslatiDontesTipusa, onk.JogorvoslatiDontestHozta, onk.JogorvoslatiDontesTartalma, onk.JogorvoslatiDontes
	FROM
	EREC_AgazatiJelek a LEFT JOIN EREC_IraIrattariTetelek it
	ON it.AgazatiJel_Id = a.Id and it.Org=@Org
	LEFT JOIN (SELECT u.Id, u.IraIrattariTetel_Id, i.UgyFajtaja, i.DontestHozta, i.DontesFormaja, i.UgyintezesHataridore, i.JogorvoslatiEljarasTipusa, i.JogorvoslatiDontesTipusa, i.JogorvoslatiDontestHozta, i.JogorvoslatiDontesTartalma, i.JogorvoslatiDontes
				FROM EREC_UgyUgyiratok u
                JOIN EREC_UgyUgyiratdarabok ud
                ON ud.UgyUgyirat_Id = u.Id
                JOIN EREC_IraIratok ii
                ON ii.UgyUgyiratDarab_Id = ud.Id
				JOIN vw_EREC_IraOnkormAdatok i
				ON i.IraIratok_Id = ii.Id and ii.IktatasDatuma between @KezdDat AND @VegeDat								
				-- WHERE u.LetrehozasIdo between @KezdDat AND @VegeDat
				AND u.SztornirozasDat is null
				--MP ASP.DWH
				AND  u.Allapot != '90'
				AND i.UgyFajtaja = @Kod_Allamigazgatasi
				) AS onk
	ON onk.IraIrattariTetel_Id = it.Id	
	GROUP BY SUBSTRING(a.Kod, 1, 3), onk.UgyFajtaja, onk.DontestHozta, onk.DontesFormaja, onk.UgyintezesHataridore, onk.JogorvoslatiEljarasTipusa, onk.JogorvoslatiDontesTipusa, onk.JogorvoslatiDontestHozta, onk.JogorvoslatiDontesTartalma, onk.JogorvoslatiDontes
	ORDER BY SUBSTRING(a.Kod, 1, 3)
	
--select * from @TempTabla

INSERT INTO @TempTable_UjAdatok
SELECT SUBSTRING(a.Kod, 1, 3) AS Kod, i.Id, u.Id, i.HatosagiEllenorzes,  cast(REPLACE(i.MunkaorakSzama, ',', '.') as float), i.EljarasiKoltseg, i.KozigazgatasiBirsagMerteke
	FROM
	EREC_AgazatiJelek a JOIN EREC_IraIrattariTetelek it
	ON it.AgazatiJel_Id = a.Id and it.Org=@Org
    JOIN EREC_UgyUgyiratok u
	on u.IraIrattariTetel_Id = it.Id	
    JOIN EREC_UgyUgyiratdarabok ud
    ON ud.UgyUgyirat_Id = u.Id
    JOIN EREC_IraIratok ii
    ON ii.UgyUgyiratDarab_Id = ud.Id and ii.IktatasDatuma between @KezdDat AND @VegeDat                      /* #1603 - 2018.01.25., BogI */	
	JOIN vw_EREC_IraOnkormAdatok i
	ON i.IraIratok_Id = ii.Id						
	/* #1603 - 2018.01.25., BogI --- and u.LetrehozasIdo between @KezdDat AND @VegeDat */	
	AND u.SztornirozasDat is null
	AND  u.Allapot != '90'
	AND i.UgyFajtaja = @Kod_Allamigazgatasi
	ORDER BY SUBSTRING(a.Kod, 1, 3)

INSERT INTO @TempTable_2016UjAdatok
SELECT SUBSTRING(a.Kod, 1, 3) AS Kod, i.Id, u.Id, i.SommasEljDontes, i.NyolcNapBelulNemSommas, i.FuggoHatalyuHatarozat, i.FuggoHatHatalybaLepes, i.FuggoHatalyuVegzes, i.FuggoVegzesHatalyba, i.HatAltalVisszafizOsszeg, i.HatTerheloEljKtsg, i.FelfuggHatarozat
	FROM
	EREC_AgazatiJelek a JOIN EREC_IraIrattariTetelek it
	ON it.AgazatiJel_Id = a.Id and it.Org=@Org
    JOIN EREC_UgyUgyiratok u
	on u.IraIrattariTetel_Id = it.Id	
    JOIN EREC_UgyUgyiratdarabok ud
    ON ud.UgyUgyirat_Id = u.Id
    JOIN EREC_IraIratok ii
    ON ii.UgyUgyiratDarab_Id = ud.Id
	JOIN vw_EREC_IraOnkormAdatok i
	ON i.IraIratok_Id = ii.Id						
	and ii.IktatasDatuma between @KezdDat AND @VegeDat 
	AND u.SztornirozasDat is null
	AND  u.Allapot != '90'
	AND i.UgyFajtaja = @Kod_Allamigazgatasi
	ORDER BY SUBSTRING(a.Kod, 1, 3)

/* --- BUG_1532 - két blokkba szétszedve 2018.01.10., BogI */
INSERT INTO @TempTable_2016EljSzamaAdatok
SELECT SUBSTRING(a.Kod, 1, 3) AS Kod, i.Id, u.Id, 
		/*EljSzamaElozofelevrolLezart*/
		case when ii.IktatasDatuma < @KezdDat AND ii.IktatasDatuma > dateadd(month, -6, @KezdDat) AND ((u.LezarasDat is not null and u.LezarasDat < @VegeDat ) or (u.ElintezesDat is not null and u.ElintezesDat < @VegeDat)) then 1 else 0
		end as EljSzamaElozofelevrolLezart,
		/*EljSzamaElozofelevrolFolyamatban*/
		case when ii.IktatasDatuma < @KezdDat AND ii.IktatasDatuma > dateadd(month, -6, @KezdDat) AND ((u.LezarasDat is not null and u.LezarasDat > @VegeDat ) or (u.ElintezesDat is not null and u.ElintezesDat > @VegeDat)) then 1 else 0 
		end as EljSzamaElozofelevrolFolyamatban,
		Null,                        /*EljSzamaMegismeteltLezart*/
		Null,                        /*EljSzamaMegismeteltFolyamatban*/
		Null,                        /*EljSzamaTargyfelevbenIndultLezart*/
		Null                         /*EljSzamaTargyfelevbenIndultFolyamatban*/		
	FROM
	EREC_AgazatiJelek a JOIN EREC_IraIrattariTetelek it
	ON it.AgazatiJel_Id = a.Id and it.Org=@Org
    JOIN EREC_UgyUgyiratok u
	on u.IraIrattariTetel_Id = it.Id	
    JOIN EREC_UgyUgyiratdarabok ud
    ON ud.UgyUgyirat_Id = u.Id
    JOIN EREC_IraIratok ii
    ON ii.UgyUgyiratDarab_Id = ud.Id
	JOIN vw_EREC_IraOnkormAdatok i
	ON i.IraIratok_Id = ii.Id						
/* --- EZ a szűrés itt KIZÁRÁST eredményezne -- and u.LetrehozasIdo between @KezdDat AND @VegeDat */
	AND u.SztornirozasDat is null
	AND u.Allapot != '90'
	AND i.UgyFajtaja = @Kod_Allamigazgatasi
	ORDER BY SUBSTRING(a.Kod, 1, 3)
;
INSERT INTO @TempTable_2016EljSzamaAdatok
SELECT SUBSTRING(a.Kod, 1, 3) AS Kod, i.Id, u.Id, 
		Null,                      /*EljSzamaElozofelevrolLezart*/
		Null,                      /*EljSzamaElozofelevrolFolyamatban*/
		/*EljSzamaMegismeteltLezart*/
		case when i.JogorvoslatiEljarasTipusa = 1
				AND i.JogorvoslatiDontesTipusa = 2
				AND i.JogorvoslatiDontestHozta = 6
				AND i.JogorvoslatiDontesTartalma = 8
				AND ((u.LezarasDat is not null and u.LezarasDat < @VegeDat ) or (u.ElintezesDat is not null and u.ElintezesDat < @VegeDat)) then 1 else 0 
		end as EljSzamaMegismeteltLezart,
		/*EljSzamaMegismeteltFolyamatban*/
		case when i.JogorvoslatiEljarasTipusa = 1
				AND i.JogorvoslatiDontesTipusa = 2
				AND i.JogorvoslatiDontestHozta = 6
				AND i.JogorvoslatiDontesTartalma = 8
				AND ((u.LezarasDat is not null and u.LezarasDat > @VegeDat ) or (u.ElintezesDat is not null and u.ElintezesDat > @VegeDat)) then 1 else 0 
		end as EljSzamaMegismeteltLezart,
		/*EljSzamaTargyfelevbenIndultLezart*/
		case when ii.IktatasDatuma  between @KezdDat and @VegeDat AND ((u.LezarasDat is not null and u.LezarasDat < @VegeDat ) or (u.ElintezesDat is not null and u.ElintezesDat < @VegeDat)) then 1 else 0
		end as EljSzamaTargyfelevbenIndultLezart,
		/*EljSzamaTargyfelevbenIndultFolyamatban*/
		case when ii.IktatasDatuma  between @KezdDat and @VegeDat AND ((u.LezarasDat is not null and u.LezarasDat > @VegeDat ) or (u.ElintezesDat is not null and u.ElintezesDat > @VegeDat)) then 1 else 0
		end as EljSzamaTargyfelevbenIndultFolyamatban
	FROM
	EREC_AgazatiJelek a JOIN EREC_IraIrattariTetelek it
	ON it.AgazatiJel_Id = a.Id and it.Org=@Org
    JOIN EREC_UgyUgyiratok u
	on u.IraIrattariTetel_Id = it.Id	
    JOIN EREC_UgyUgyiratdarabok ud
    ON ud.UgyUgyirat_Id = u.Id
    JOIN EREC_IraIratok ii
    ON ii.UgyUgyiratDarab_Id = ud.Id
	JOIN vw_EREC_IraOnkormAdatok i
	ON i.IraIratok_Id = ii.Id						
	and ii.IktatasDatuma between @KezdDat AND @VegeDat
	AND u.SztornirozasDat is null
	AND u.Allapot != '90'
	AND i.UgyFajtaja = @Kod_Allamigazgatasi
	ORDER BY SUBSTRING(a.Kod, 1, 3)
;
/* --- BUG_1532
INSERT INTO @TempTable_2016EljSzamaAdatok
SELECT SUBSTRING(a.Kod, 1, 3) AS Kod, i.Id, u.Id, 
		/*EljSzamaElozofelevrolLezart*/
		case when ii.IktatasDatuma < @KezdDat AND ii.IktatasDatuma > @KezdDat-180 AND u.Allapot in ('11', '13', '09', '10') then 1 else 0
		end as EljSzamaElozofelevrolLezart,
		/*EljSzamaElozofelevrolFolyamatban*/
		case when ii.IktatasDatuma < @KezdDat AND ii.IktatasDatuma > @KezdDat-180 AND u.Allapot not in ('11', '13', '09', '10') then 1 else 0 
		end as EljSzamaElozofelevrolFolyamatban,
		/*EljSzamaMegismeteltLezart*/
		case when i.JogorvoslatiEljarasTipusa = 1
				AND i.JogorvoslatiDontesTipusa = 2
				AND i.JogorvoslatiDontestHozta = 6
				AND i.JogorvoslatiDontesTartalma = 8
				AND u.Allapot in ('11', '13', '09', '10') then 1 else 0 
		end as EljSzamaMegismeteltLezart,
		/*EljSzamaMegismeteltFolyamatban*/
		case when i.JogorvoslatiEljarasTipusa = 1
				AND i.JogorvoslatiDontesTipusa = 2
				AND i.JogorvoslatiDontestHozta = 6
				AND i.JogorvoslatiDontesTartalma = 8
				AND u.Allapot not in ('11', '13', '09', '10') then 1 else 0 
		end as EljSzamaMegismeteltLezart,
		/*EljSzamaTargyfelevbenIndultLezart*/
		case when ii.IktatasDatuma > @KezdDat AND u.Allapot in ('11', '13', '09', '10') then 1 else 0
		end as EljSzamaTargyfelevbenIndultLezart,
		/*EljSzamaTargyfelevbenIndultFolyamatban*/
		case when ii.IktatasDatuma > @KezdDat AND u.Allapot not  in ('11', '13', '09', '10') then 1 else 0
		end as EljSzamaTargyfelevbenIndultFolyamatban
	FROM
	EREC_AgazatiJelek a JOIN EREC_IraIrattariTetelek it
	ON it.AgazatiJel_Id = a.Id and it.Org=@Org
    JOIN EREC_UgyUgyiratok u
	on u.IraIrattariTetel_Id = it.Id	
    JOIN EREC_UgyUgyiratdarabok ud
    ON ud.UgyUgyirat_Id = u.Id
    JOIN EREC_IraIratok ii
    ON ii.UgyUgyiratDarab_Id = ud.Id
	JOIN vw_EREC_IraOnkormAdatok i
	ON i.IraIratok_Id = ii.Id						
	and u.LetrehozasIdo between @KezdDat AND @VegeDat
	AND u.SztornirozasDat is null
	AND i.UgyFajtaja = @Kod_Allamigazgatasi
	ORDER BY SUBSTRING(a.Kod, 1, 3)
--- BUG_1532 */
	
--select * from @TempTable_UjAdatok

---- adatok mentese a @Reszletes_Tablaba
INSERT INTO @Reszletes_Tabla
--SELECT a.Kod, Donteshozo4, Donteshozo5, Donteshozo6, ErdemiDontes1, ErdemiDontes2, ErdemiDontes3, ErdemiDontes4, Vegzes5, Vegzes6, Vegzes7, Vegzes8, Hatarido1, Hatarido2, JogorvoslatiDontesTartalma1, FellebbezesAlapjan, VegzesKozighivJogorv5, VegzesKozighivJogorv6, VegzesKozighivJogorv7, VegzesKozighivJogorv8, VegzesDekoncentraltJogorv5, VegzesDekoncentraltJogorv6, VegzesDekoncentraltJogorv7, VegzesDekoncentraltJogorv8, VegzesBirosagJogorv5, VegzesBirosagJogorv6, VegzesBirosagJogorv7, VegzesBirosagJogorv8, ErdemiUjrafelvJogorv5, ErdemiUjrafelvJogorv2, ErdemiUjrafelvJogorv3, ErdemiUjrafelvJogorv4, ErdemiMeltanyosJogorv5, ErdemiMeltanyosJogorv2, ErdemiMeltanyosJogorv3, ErdemiKozighivJogorv5, ErdemiKozighivJogorv6, ErdemiKozighivJogorv7, ErdemiKozighivJogorv8, ErdemiDekoncentraltJogorv5, ErdemiDekoncentraltJogorv6, ErdemiDekoncentraltJogorv7, ErdemiDekoncentraltJogorv8, ErdemiBirosagJogorv5, ErdemiBirosagJogorv6, ErdemiBirosagJogorv7, ErdemiBirosagJogorv8, HivatalbolModositVisszavon, HivatalbolFelugyeleti6, HivatalbolFelugyeleti7, HivatalbolFelugyeleti8
SELECT a.Kod, ISNULL(Donteshozo4,0), ISNULL(Donteshozo5,0),
			ISNULL(Donteshozo6,0),
			ISNULL(ErdemiDontes1,0), ISNULL(ErdemiDontes2,0),
			ISNULL(ErdemiDontes3,0), ISNULL(ErdemiDontes4,0),
			ISNULL(Vegzes5,0), ISNULL(Vegzes6,0), ISNULL(Vegzes9,0), ISNULL(Vegzes7,0),
			ISNULL(Vegzes8,0), ISNULL(Hatarido1,0), ISNULL(Hatarido2,0),
			ISNULL(JogorvoslatiDontesTartalma1,0), ISNULL(FellebbezesAlapjan,0),
			ISNULL(VegzesKozighivJogorv5,0), ISNULL(VegzesKozighivJogorv6,0),
			ISNULL(VegzesKozighivJogorv7,0), ISNULL(VegzesKozighivJogorv8,0),
			ISNULL(VegzesDekoncentraltJogorv5,0), ISNULL(VegzesDekoncentraltJogorv6,0),
			ISNULL(VegzesDekoncentraltJogorv7,0), ISNULL(VegzesDekoncentraltJogorv8,0),
			ISNULL(VegzesBirosagJogorv5,0), ISNULL(VegzesBirosagJogorv6,0),
			ISNULL(VegzesBirosagJogorv7,0), ISNULL(VegzesBirosagJogorv8,0),
			ISNULL(ErdemiUjrafelvJogorv5,0), ISNULL(ErdemiUjrafelvJogorv2,0),
			ISNULL(ErdemiUjrafelvJogorv3,0), ISNULL(ErdemiUjrafelvJogorv4,0),
			ISNULL(ErdemiMeltanyosJogorv5,0), ISNULL(ErdemiMeltanyosJogorv2,0),
			ISNULL(ErdemiMeltanyosJogorv3,0), ISNULL(ErdemiKozighivJogorv5,0),
			ISNULL(ErdemiKozighivJogorv6,0), ISNULL(ErdemiKozighivJogorv7,0),
			ISNULL(ErdemiKozighivJogorv8,0), ISNULL(ErdemiDekoncentraltJogorv5,0),
			ISNULL(ErdemiDekoncentraltJogorv6,0), ISNULL(ErdemiDekoncentraltJogorv7,0),
			ISNULL(ErdemiDekoncentraltJogorv8,0), ISNULL(ErdemiBirosagJogorv5,0),
			ISNULL(ErdemiBirosagJogorv6,0), ISNULL(ErdemiBirosagJogorv7,0),
			ISNULL(ErdemiBirosagJogorv8,0), ISNULL(HivatalbolModositVisszavon,0),
			ISNULL(HivatalbolFelugyeleti6,0), ISNULL(HivatalbolFelugyeleti7,0),
			ISNULL(HivatalbolFelugyeleti8,0),
			ISNULL(HatosagiEllenorzesekSzama,0),
			ISNULL(MunkaorakSzamaAtlagosan, 0),
			ISNULL(AtlagosEljarasiKoltseg, 0),
			ISNULL(AtlagosKozigazgatasiBirsag, 0),
			ISNULL(EljSzamaElozofelevrolLezartSzama, 0),
			ISNULL(EljSzamaElozofelevrolFolyamatbanSzama, 0),
			ISNULL(EljSzamaMegismeteltLezartSzama, 0),
			ISNULL(EljSzamaMegismeteltFolyamatbanSzama, 0),
			ISNULL(EljSzamaTargyfelevbenIndultLezartSzama, 0), 
			ISNULL(EljSzamaTargyfelevbenIndultFolyamatbanSzama, 0),
			ISNULL(SommasEljSzama, 0),
			ISNULL(NyolcnaponBelulNemSommasSzama, 0),
			ISNULL(FuggoHatalyuHatarozatSzama, 0),
			ISNULL(FuggoHatalyuVegzesSzama, 0),
			ISNULL(FuggoHatalyuHatarozatNemHatalyos, 0),
			ISNULL(FuggoHatalyuHatarozatHatalyos, 0),
			ISNULL(FuggoHatalyuDontesNemHatalyos, 0),
			ISNULL(FuggoHatalyuDontesHatalyos, 0),
			ISNULL(VisszafizetettOsszeg, 0),
			ISNULL(HatosagotTerheloEljarasiKoltseg, 0) 
FROM
(
	(SELECT SUBSTRING(Kod, 1, 3) AS Kod
		FROM EREC_AgazatiJelek
		GROUP BY SUBSTRING(Kod, 1, 3)
	) AS a
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS Donteshozo4 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontestHozta = '4'
					GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS Donteshozo4
	ON a.Kod = Donteshozo4.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS Donteshozo5 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontestHozta = '5'
					GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS Donteshozo5
	ON a.Kod = Donteshozo5.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS Donteshozo6 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontestHozta = '6'
					GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS Donteshozo6
	ON a.Kod = Donteshozo6.Kod
-- erdemi dontesek
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiDontes1 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '1'
					GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiDontes1
	ON a.Kod = ErdemiDontes1.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiDontes2 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '2'
					GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiDontes2
	ON a.Kod = ErdemiDontes2.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiDontes3 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '3'
					GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiDontes3
	ON a.Kod = ErdemiDontes3.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiDontes4 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '4'
					GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiDontes4
	ON a.Kod = ErdemiDontes4.Kod
-- vegzesek
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS Vegzes5 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '5'
					GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS Vegzes5
	ON a.Kod = Vegzes5.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS Vegzes6 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '6'
					GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS Vegzes6
	ON a.Kod = Vegzes6.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS Vegzes7 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '7'
					GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS Vegzes7
	ON a.Kod = Vegzes7.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS Vegzes9 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '9'
					GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS Vegzes9
	ON a.Kod = Vegzes9.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS Vegzes8 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE DontesFormaja = '8'
					GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS Vegzes8
	ON a.Kod = Vegzes8.Kod
-- hatarido
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS Hatarido1 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE UgyintezesHataridore in ('1','IGEN')
					GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS Hatarido1
	ON a.Kod = Hatarido1.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS Hatarido2 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE UgyintezesHataridore not in ('1','IGEN')
					GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS Hatarido2
	ON a.Kod = Hatarido2.Kod
-- kicserelt, kijavitott, kiegeszitett
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS JogorvoslatiDontesTartalma1 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiDontesTartalma = 1 -- 2012 előtt - Kicserélés, kiegészítés vagy kijavítás
				or JogorvoslatiDontes = 1 -- 2012 után - Kijavításra került sor
				or JogorvoslatiDontes = 2 -- 2012 után - Kiegészítésre került sor
					GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS JogorvoslatiDontesTartalma1
	ON a.Kod = JogorvoslatiDontesTartalma1.Kod
-- fellebbezes alapjan
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS FellebbezesAlapjan FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
			--AND JogorvoslatiDontestHozta = ???
				AND (JogorvoslatiDontesTartalma = 2
				OR JogorvoslatiDontesTartalma = 3)
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS FellebbezesAlapjan
	ON a.Kod = FellebbezesAlapjan.Kod
-- jogorvoslat, kerelem, vegzes
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS VegzesKozighivJogorv5 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS VegzesKozighivJogorv5
	ON a.Kod = VegzesKozighivJogorv5.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS VegzesKozighivJogorv6 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS VegzesKozighivJogorv6
	ON a.Kod = VegzesKozighivJogorv6.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS VegzesKozighivJogorv7 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS VegzesKozighivJogorv7
	ON a.Kod = VegzesKozighivJogorv7.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS VegzesKozighivJogorv8 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS VegzesKozighivJogorv8
	ON a.Kod = VegzesKozighivJogorv8.Kod

	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS VegzesDekoncentraltJogorv5 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS VegzesDekoncentraltJogorv5
	ON a.Kod = VegzesDekoncentraltJogorv5.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS VegzesDekoncentraltJogorv6 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS VegzesDekoncentraltJogorv6
	ON a.Kod = VegzesDekoncentraltJogorv6.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS VegzesDekoncentraltJogorv7 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS VegzesDekoncentraltJogorv7
	ON a.Kod = VegzesDekoncentraltJogorv7.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS VegzesDekoncentraltJogorv8 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS VegzesDekoncentraltJogorv8
	ON a.Kod = VegzesDekoncentraltJogorv8.Kod

	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS VegzesBirosagJogorv5 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS VegzesBirosagJogorv5
	ON a.Kod = VegzesBirosagJogorv5.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS VegzesBirosagJogorv6 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS VegzesBirosagJogorv6
	ON a.Kod = VegzesBirosagJogorv6.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS VegzesBirosagJogorv7 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS VegzesBirosagJogorv7
	ON a.Kod = VegzesBirosagJogorv7.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS VegzesBirosagJogorv8 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 1
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS VegzesBirosagJogorv8
	ON a.Kod = VegzesBirosagJogorv8.Kod
-- jogorvoslat, kerelem, erdemi dontes
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiUjrafelvJogorv5 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 2
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiUjrafelvJogorv5
	ON a.Kod = ErdemiUjrafelvJogorv5.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiUjrafelvJogorv2 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 2
				AND JogorvoslatiDontesTartalma = 2
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiUjrafelvJogorv2
	ON a.Kod = ErdemiUjrafelvJogorv2.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiUjrafelvJogorv3 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 2
				AND JogorvoslatiDontesTartalma = 3
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiUjrafelvJogorv3
	ON a.Kod = ErdemiUjrafelvJogorv3.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiUjrafelvJogorv4 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 2
				AND JogorvoslatiDontesTartalma = 4
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiUjrafelvJogorv4
	ON a.Kod = ErdemiUjrafelvJogorv4.Kod

	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiMeltanyosJogorv5 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 3
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiMeltanyosJogorv5
	ON a.Kod = ErdemiMeltanyosJogorv5.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiMeltanyosJogorv2 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 3
				AND JogorvoslatiDontesTartalma = 2
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiMeltanyosJogorv2
	ON a.Kod = ErdemiMeltanyosJogorv2.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiMeltanyosJogorv3 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 3
				AND JogorvoslatiDontesTartalma = 3
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiMeltanyosJogorv3
	ON a.Kod = ErdemiMeltanyosJogorv3.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiKozighivJogorv5 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiKozighivJogorv5
	ON a.Kod = ErdemiKozighivJogorv5.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiKozighivJogorv6 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiKozighivJogorv6
	ON a.Kod = ErdemiKozighivJogorv6.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiKozighivJogorv7 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiKozighivJogorv7
	ON a.Kod = ErdemiKozighivJogorv7.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiKozighivJogorv8 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 5
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiKozighivJogorv8
	ON a.Kod = ErdemiKozighivJogorv8.Kod

	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiDekoncentraltJogorv5 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiDekoncentraltJogorv5
	ON a.Kod = ErdemiDekoncentraltJogorv5.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiDekoncentraltJogorv6 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiDekoncentraltJogorv6
	ON a.Kod = ErdemiDekoncentraltJogorv6.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiDekoncentraltJogorv7 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiDekoncentraltJogorv7
	ON a.Kod = ErdemiDekoncentraltJogorv7.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiDekoncentraltJogorv8 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 6
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiDekoncentraltJogorv8
	ON a.Kod = ErdemiDekoncentraltJogorv8.Kod

	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiBirosagJogorv5 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 5
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiBirosagJogorv5
	ON a.Kod = ErdemiBirosagJogorv5.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiBirosagJogorv6 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiBirosagJogorv6
	ON a.Kod = ErdemiBirosagJogorv6.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiBirosagJogorv7 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiBirosagJogorv7
	ON a.Kod = ErdemiBirosagJogorv7.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS ErdemiBirosagJogorv8 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 1
				AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 7
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS ErdemiBirosagJogorv8
	ON a.Kod = ErdemiBirosagJogorv8.Kod
-- hivatalbol modositott, visszavont
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS HivatalbolModositVisszavon FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 2
				--AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 8
				AND (JogorvoslatiDontesTartalma = 2
					OR JogorvoslatiDontesTartalma = 3)
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS HivatalbolModositVisszavon
	ON a.Kod = HivatalbolModositVisszavon.Kod
-- hivatalbol, felugyeleti szerv
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS HivatalbolFelugyeleti6 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 2
				--AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 8
				AND JogorvoslatiDontesTartalma = 6
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS HivatalbolFelugyeleti6
	ON a.Kod = HivatalbolFelugyeleti6.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS HivatalbolFelugyeleti7 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 2
				--AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 8
				AND JogorvoslatiDontesTartalma = 7
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS HivatalbolFelugyeleti7
	ON a.Kod = HivatalbolFelugyeleti7.Kod
	LEFT JOIN
	(SELECT t.Kod, SUM(t.Darab) AS HivatalbolFelugyeleti8 FROM
		(SELECT Kod, SUM(Darab) AS Darab FROM @TempTabla
				WHERE JogorvoslatiEljarasTipusa = 2
				--AND JogorvoslatiDontesTipusa = 2
				AND JogorvoslatiDontestHozta = 8
				AND JogorvoslatiDontesTartalma = 8
			GROUP BY Kod
		) AS t
	GROUP BY t.Kod) AS HivatalbolFelugyeleti8
	ON a.Kod = HivatalbolFelugyeleti8.Kod
	LEFT JOIN
   (SELECT Kod, count(distinct Ugyirat_Id) AS HatosagiEllenorzesekSzama FROM @TempTable_UjAdatok
		    Where ISNULL(HatosagiEllenorzes,'0') in ( '1', 'IGEN')
			GROUP BY Kod
	) AS HatosagiEllenorzesekSzama
	ON a.Kod = HatosagiEllenorzesekSzama.Kod
	LEFT JOIN
   (SELECT Kod, sum(MunkaorakSzama) / count(distinct Ugyirat_Id) AS MunkaorakSzamaAtlagosan FROM @TempTable_UjAdatok
		    Where MunkaorakSzama is not null
			GROUP BY Kod
	) AS MunkaorakSzamaAtlagosan
	ON a.Kod = MunkaorakSzamaAtlagosan.Kod
	LEFT JOIN
	(SELECT Kod, cast(sum(EljarasiKoltseg) as float) / count(distinct Ugyirat_Id) AS AtlagosEljarasiKoltseg FROM @TempTable_UjAdatok
		    Where EljarasiKoltseg is not null
			GROUP BY Kod
	) AS AtlagosEljarasiKoltseg
	ON a.Kod = AtlagosEljarasiKoltseg.Kod
	LEFT JOIN
	(SELECT Kod, cast(sum(KozigazgatasiBirsagMerteke) as float) / count(distinct Ugyirat_Id) AS AtlagosKozigazgatasiBirsag FROM @TempTable_UjAdatok
		    Where KozigazgatasiBirsagMerteke is not null and KozigazgatasiBirsagMerteke > 0
			GROUP BY Kod
	) AS AtlagosKozigazgatasiBirsag
	ON a.Kod = AtlagosKozigazgatasiBirsag.Kod
-----------------
	LEFT JOIN
	(SELECT Kod, sum(EljSzamaElozofelevrolLezart) AS EljSzamaElozofelevrolLezartSzama FROM @TempTable_2016EljSzamaAdatok
		    Where EljSzamaElozofelevrolLezart is not null
			GROUP BY Kod
	) AS EljSzamaElozofelevrolLezartSzama
	ON a.Kod = EljSzamaElozofelevrolLezartSzama.Kod
	LEFT JOIN
	(SELECT Kod, sum(EljSzamaElozofelevrolFolyamatban) AS EljSzamaElozofelevrolFolyamatbanSzama FROM @TempTable_2016EljSzamaAdatok
		    Where EljSzamaElozofelevrolFolyamatban is not null
			GROUP BY Kod
	) AS EljSzamaElozofelevrolFolyamatbanSzama
	ON a.Kod = EljSzamaElozofelevrolFolyamatbanSzama.Kod
	LEFT JOIN
	(SELECT Kod, sum(EljSzamaMegismeteltLezart) AS EljSzamaMegismeteltLezartSzama FROM @TempTable_2016EljSzamaAdatok
		    Where EljSzamaMegismeteltLezart is not null
			GROUP BY Kod
	) AS EljSzamaMegismeteltLezartSzama
	ON a.Kod = EljSzamaMegismeteltLezartSzama.Kod
	LEFT JOIN
	(SELECT Kod, sum(EljSzamaMegismeteltFolyamatban) AS EljSzamaMegismeteltFolyamatbanSzama FROM @TempTable_2016EljSzamaAdatok
		    Where EljSzamaMegismeteltFolyamatban is not null
			GROUP BY Kod
	) AS EljSzamaMegismeteltFolyamatbanSzama
	ON a.Kod = EljSzamaMegismeteltFolyamatbanSzama.Kod
	LEFT JOIN
	(SELECT Kod, sum(EljSzamaTargyfelevbenIndultLezart) AS EljSzamaTargyfelevbenIndultLezartSzama FROM @TempTable_2016EljSzamaAdatok
		    Where EljSzamaTargyfelevbenIndultLezart is not null
			GROUP BY Kod
	) AS EljSzamaTargyfelevbenIndultLezartSzama
	ON a.Kod = EljSzamaTargyfelevbenIndultLezartSzama.Kod
	LEFT JOIN
	(SELECT Kod, sum(EljSzamaTargyfelevbenIndultFolyamatban) AS EljSzamaTargyfelevbenIndultFolyamatbanSzama FROM @TempTable_2016EljSzamaAdatok
		    Where EljSzamaTargyfelevbenIndultFolyamatban is not null
			GROUP BY Kod
	) AS EljSzamaTargyfelevbenIndultFolyamatbanSzama
	ON a.Kod = EljSzamaTargyfelevbenIndultFolyamatbanSzama.Kod
-----------------
	LEFT JOIN
   (SELECT Kod, count(distinct Ugyirat_Id) AS SommasEljSzama FROM @TempTable_2016UjAdatok
		    Where ISNULL(SommasEljDontes,'0') in ( '1', 'IGEN')
			GROUP BY Kod
	) AS SommasEljSzama
	ON a.Kod = SommasEljSzama.Kod
	LEFT JOIN
   (SELECT Kod, count(distinct Ugyirat_Id) AS NyolcnaponBelulNemSommasSzama FROM @TempTable_2016UjAdatok
		    Where ISNULL(NyolcNapBelulNemSommas,'0') in ( '1', 'IGEN')
			GROUP BY Kod
	) AS NyolcnaponBelulNemSommasSzama
	ON a.Kod = NyolcnaponBelulNemSommasSzama.Kod
   /* --- #222 hiba javítása -  2017.10.10., BogI --- */
   --- függő hatályú határozat
	LEFT JOIN
   (SELECT Kod, count(distinct Ugyirat_Id) AS FuggoHatalyuHatarozatSzama FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoHatalyuHatarozat,'0') in ( '1', 'IGEN')
			GROUP BY Kod
	) AS FuggoHatalyuHatarozatSzama
	ON a.Kod = FuggoHatalyuHatarozatSzama.Kod
	LEFT JOIN
   (SELECT Kod, count(distinct Ugyirat_Id) AS FuggoHatalyuHatarozatHatalyos FROM @TempTable_2016UjAdatok
            Where ISNULL(FuggoHatalyuHatarozat,'0') in ( '1', 'IGEN')
		      and ISNULL(FuggoHatHatalybaLepes,'0') in ( '1', 'IGEN')
			GROUP BY Kod
	) AS FuggoHatalyuHatarozatHatalyos
	ON a.Kod = FuggoHatalyuHatarozatHatalyos.Kod
	LEFT JOIN
   (SELECT Kod, count(distinct Ugyirat_Id) AS FuggoHatalyuHatarozatNemHatalyos FROM @TempTable_2016UjAdatok
            Where ISNULL(FuggoHatalyuHatarozat,'0') in ( '1', 'IGEN')
		      and ISNULL(FuggoHatHatalybaLepes,'0') NOT in ( '1', 'IGEN')
			GROUP BY Kod
	) AS FuggoHatalyuHatarozatNemHatalyos
	ON a.Kod = FuggoHatalyuHatarozatNemHatalyos.Kod
   --- függő hatályú végzés
	LEFT JOIN
   (SELECT Kod, count(distinct Ugyirat_Id) AS FuggoHatalyuVegzesSzama FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoHatalyuVegzes,'0') in ( '1', 'IGEN')
			GROUP BY Kod
	) AS FuggoHatalyuVegzesSzama
	ON a.Kod = FuggoHatalyuVegzesSzama.Kod
	LEFT JOIN
   (SELECT Kod, count(distinct Ugyirat_Id) AS FuggoHatalyuDontesHatalyos FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoHatalyuVegzes,'0') in ( '1', 'IGEN')
		      and ISNULL(FuggoVegzesHatalyba,'0') in ( '1', 'IGEN')
			GROUP BY Kod
	) AS FuggoHatalyuDontesHatalyos
	ON a.Kod = FuggoHatalyuDontesHatalyos.Kod
	LEFT JOIN
   (SELECT Kod, count(distinct Ugyirat_Id) AS FuggoHatalyuDontesNemHatalyos FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoHatalyuVegzes,'0') in ( '1', 'IGEN')
		      and ISNULL(FuggoVegzesHatalyba,'0') NOT in ( '1', 'IGEN')
			GROUP BY Kod
	) AS FuggoHatalyuDontesNemHatalyos
	ON a.Kod = FuggoHatalyuDontesNemHatalyos.Kod
/* --- #222 hiba javításakor (ideiglenesen ?) kiemelve 2017.10.10., BogI
	LEFT JOIN
   (SELECT Kod, count(distinct Ugyirat_Id) AS FuggoHatalyuHatarozatSzama FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoHatalyuHatarozat,'0') = '1'
			GROUP BY Kod
	) AS FuggoHatalyuHatarozatSzama
	ON a.Kod = FuggoHatalyuHatarozatSzama.Kod
	LEFT JOIN
   (SELECT Kod, count(distinct Ugyirat_Id) AS FuggoHatalyuVegzesSzama FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoHatalyuVegzes,'0') = '1'
			GROUP BY Kod
	) AS FuggoHatalyuVegzesSzama
	ON a.Kod = FuggoHatalyuVegzesSzama.Kod
	LEFT JOIN
   (SELECT Kod, count(distinct Ugyirat_Id) AS FuggoHatalyuHatarozatNemHatalyos FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoHatHatalybaLepes,'0') = '0'
			GROUP BY Kod
	) AS FuggoHatalyuHatarozatNemHatalyos
	ON a.Kod = FuggoHatalyuHatarozatNemHatalyos.Kod
	LEFT JOIN
   (SELECT Kod, count(distinct Ugyirat_Id) AS FuggoHatalyuHatarozatHatalyos FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoHatHatalybaLepes,'0') = '1'
			GROUP BY Kod
	) AS FuggoHatalyuHatarozatHatalyos
	ON a.Kod = FuggoHatalyuHatarozatHatalyos.Kod
	LEFT JOIN
   (SELECT Kod, count(distinct Ugyirat_Id) AS FuggoHatalyuDontesNemHatalyos FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoVegzesHatalyba,'0') = '0'
			GROUP BY Kod
	) AS FuggoHatalyuDontesNemHatalyos
	ON a.Kod = FuggoHatalyuDontesNemHatalyos.Kod
	LEFT JOIN
   (SELECT Kod, count(distinct Ugyirat_Id) AS FuggoHatalyuDontesHatalyos FROM @TempTable_2016UjAdatok
		    Where ISNULL(FuggoVegzesHatalyba,'0') = '1'
			GROUP BY Kod
	) AS FuggoHatalyuDontesHatalyos
	ON a.Kod = FuggoHatalyuDontesHatalyos.Kod
-------------- */
	LEFT JOIN
   (SELECT Kod, sum(HatAltalVisszafizOsszeg) AS VisszafizetettOsszeg FROM @TempTable_2016UjAdatok
		    Where HatAltalVisszafizOsszeg is not null
			GROUP BY Kod
		) AS VisszafizetettOsszeg
	ON a.Kod = VisszafizetettOsszeg.Kod
	LEFT JOIN
   (SELECT Kod, sum(HatTerheloEljKtsg) AS HatosagotTerheloEljarasiKoltseg FROM @TempTable_2016UjAdatok
		    Where HatTerheloEljKtsg is not null
			GROUP BY Kod
	) AS HatosagotTerheloEljarasiKoltseg
	ON a.Kod = HatosagotTerheloEljarasiKoltseg.Kod
)

-- Az Excel vezerlotabla feltoltese

INSERT INTO @Excel_Tabla
	SELECT SUBSTRING(a.Kod, 1, 3) AS Kod, null AS Cells
	FROM EREC_AgazatiJelek a
	GROUP BY SUBSTRING(a.Kod, 1, 3)
	ORDER BY SUBSTRING(a.Kod, 1, 3)

-- Manualis kitoltes
-- az Excel-tabla adott kodhoz tartozo sorai
DECLARE @SorokKodok_Tabla AS table (
	sor varchar(3), -- megfelelo excel-tablasor
	kod varchar(3)  -- agazati jel max. masdik szintig
)

declare @year int
set @year = DatePart(yyyy, @kezdDat)

if (@year < 2010)
BEGIN
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('14', 'A.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('15', 'A.2')
	--INSERT INTO @SorokKodok_Tabla (sor, kod) values ('16', 'A.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('17', 'B')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('18', 'C')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('20', 'E.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('21', 'E.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('22', 'E.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('23', 'F')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('24', 'G')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('26', 'H.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('27', 'H.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('28', 'H.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('29', 'H.4')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('30', 'H.5')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('31', 'H.6')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('32', 'H.7')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('33', 'H.8')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('34', 'I')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('35', 'J')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('36', 'K')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('37', 'L')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('38', 'M')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('39', 'N')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('40', 'P')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('41', 'R')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('43', 'X.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('44', 'X.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('45', 'X.3')
END
ELSE IF @year < 2015
BEGIN
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('14', 'A.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('15', 'A.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('16', 'B')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('17', 'C')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('19', 'E.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('20', 'E.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('21', 'E.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('22', 'F')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('23', 'G')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('25', 'H.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('26', 'H.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('27', 'H.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('28', 'H.4')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('29', 'H.5')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('30', 'H.6')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('31', 'H.7')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('32', 'H.8')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('33', 'I')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('34', 'J')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('35', 'K')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('36', 'L')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('37', 'M')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('38', 'N')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('39', 'P')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('40', 'R')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('42', 'X.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('43', 'X.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('44', 'X.3')
END
ELSE IF @year < 2019
BEGIN
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('14', 'A.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('15', 'A.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('16', 'B')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('17', 'C')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('19', 'E.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('20', 'E.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('21', 'E.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('22', 'E.4')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('23', 'F')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('24', 'G')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('26', 'H.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('27', 'H.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('28', 'H.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('29', 'H.4')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('30', 'H.5')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('31', 'H.6')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('32', 'H.7')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('33', 'H.8')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('34', 'I')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('35', 'J')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('36', 'K')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('37', 'L')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('38', 'M')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('39', 'N')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('40', 'P')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('41', 'R')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('43', 'X.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('44', 'X.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('45', 'X.3')
END
ELSE
BEGIN
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('9', 'A.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('10', 'A.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('11', 'B')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('12', 'C')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('14', 'E.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('15', 'E.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('16', 'E.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('17', 'E.4')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('18', 'F')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('19', 'G')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('21', 'H.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('22', 'H.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('23', 'H.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('24', 'H.4')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('25', 'H.5')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('26', 'H.6')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('27', 'H.7')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('28', 'H.8')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('29', 'I')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('30', 'J')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('31', 'K')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('32', 'L')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('33', 'M')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('34', 'N')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('35', 'P')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('36', 'R')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('38', 'X.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('39', 'X.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('40', 'X.3')
END

DECLARE  @sor nvarchar(10),
         @kod nvarchar(3)

-- cursor deklaralasa
DECLARE excelsor CURSOR FOR
	SELECT sor, kod FROM @SorokKodok_Tabla

OPEN excelsor

-- ciklus kezdete elotti fetch
FETCH excelsor INTO @sor, @kod

WHILE @@Fetch_Status = 0
	BEGIN
		IF @Year < 2010
		BEGIN
			UPDATE @Excel_Tabla SET Cells =
				@sheet + ',E' + @sor + ',Donteshozo4;'
				+ @sheet + ',F' + @sor + ',Donteshozo5;'
				+ @sheet + ',G' + @sor + ',Donteshozo6;'
				+ @sheet + ',H' + @sor + ',ErdemiDontes1;'
				+ @sheet + ',I' + @sor + ',ErdemiDontes2;'
				+ @sheet + ',J' + @sor + ',ErdemiDontes3;'
				+ @sheet + ',K' + @sor + ',ErdemiDontes4;'
				+ @sheet + ',L' + @sor + ',Vegzes5;'
				+ @sheet + ',M' + @sor + ',Vegzes6;'
				+ @sheet + ',N' + @sor + ',Vegzes7;'
				+ @sheet + ',O' + @sor + ',Vegzes8;'
				+ @sheet + ',P' + @sor + ',Hatarido1;'
				+ @sheet + ',Q' + @sor + ',Hatarido2;'
				+ @sheet + ',R' + @sor + ',JogorvoslatiDontesTartalma1;'
				+ @sheet + ',T' + @sor + ',FellebbezesAlapjan;'
				+ @sheet + ',U' + @sor + ',VegzesKozighivJogorv5;'
				+ @sheet + ',V' + @sor + ',VegzesKozighivJogorv6;'
				+ @sheet + ',W' + @sor + ',VegzesKozighivJogorv7;'
				+ @sheet + ',X' + @sor + ',VegzesKozighivJogorv8;'
				+ @sheet + ',Y' + @sor + ',VegzesDekoncentraltJogorv5;'
				+ @sheet + ',Z' + @sor + ',VegzesDekoncentraltJogorv6;'
				+ @sheet + ',AA' + @sor + ',VegzesDekoncentraltJogorv7;'
				+ @sheet + ',AB' + @sor + ',VegzesDekoncentraltJogorv8;'
				+ @sheet + ',AC' + @sor + ',VegzesBirosagJogorv5;'
				+ @sheet + ',AD' + @sor + ',VegzesBirosagJogorv6;'
				+ @sheet + ',AE' + @sor + ',VegzesBirosagJogorv7;'
				+ @sheet + ',AF' + @sor + ',VegzesBirosagJogorv8;'
				+ @sheet + ',AG' + @sor + ',ErdemiUjrafelvJogorv5;'
				+ @sheet + ',AH' + @sor + ',ErdemiUjrafelvJogorv2;'
				+ @sheet + ',AI' + @sor + ',ErdemiUjrafelvJogorv3;'
				+ @sheet + ',AJ' + @sor + ',ErdemiUjrafelvJogorv4;'
				+ @sheet + ',AK' + @sor + ',ErdemiMeltanyosJogorv5;'
				+ @sheet + ',AL' + @sor + ',ErdemiMeltanyosJogorv2;'
				+ @sheet + ',AM' + @sor + ',ErdemiMeltanyosJogorv3;'
				+ @sheet + ',AN' + @sor + ',ErdemiKozighivJogorv5;'
				+ @sheet + ',AO' + @sor + ',ErdemiKozighivJogorv6;'
				+ @sheet + ',AP' + @sor + ',ErdemiKozighivJogorv7;'
				+ @sheet + ',AQ' + @sor + ',ErdemiKozighivJogorv8;'
				+ @sheet + ',AR' + @sor + ',ErdemiDekoncentraltJogorv5;'
				+ @sheet + ',AS' + @sor + ',ErdemiDekoncentraltJogorv6;'
				+ @sheet + ',AT' + @sor + ',ErdemiDekoncentraltJogorv7;'
				+ @sheet + ',AU' + @sor + ',ErdemiDekoncentraltJogorv8;'
				+ @sheet + ',AV' + @sor + ',ErdemiBirosagJogorv5;'
				+ @sheet + ',AW' + @sor + ',ErdemiBirosagJogorv6;'
				+ @sheet + ',AX' + @sor + ',ErdemiBirosagJogorv7;'
				+ @sheet + ',AY' + @sor + ',ErdemiBirosagJogorv8;'
				+ @sheet + ',AZ' + @sor + ',HivatalbolModositVisszavon;'
				+ @sheet + ',BA' + @sor + ',HivatalbolFelugyeleti6;'
				+ @sheet + ',BB' + @sor + ',HivatalbolFelugyeleti7;'
				+ @sheet + ',BC' + @sor + ',HivatalbolFelugyeleti8;'
			WHERE Kod = @kod
		END
		ELSE IF @Year < 2015
		BEGIN
			UPDATE @Excel_Tabla SET Cells =
				@sheet + ',E' + @sor + ',Donteshozo4;'
				+ @sheet + ',F' + @sor + ',Donteshozo5;'
				+ @sheet + ',G' + @sor + ',Donteshozo6;'
				+ @sheet + ',H' + @sor + ',ErdemiDontes1;'
				+ @sheet + ',I' + @sor + ',ErdemiDontes2;'
				+ @sheet + ',J' + @sor + ',ErdemiDontes3;'
				+ @sheet + ',K' + @sor + ',ErdemiDontes4;'
				+ @sheet + ',L' + @sor + ',Vegzes5;'
				+ @sheet + ',M' + @sor + ',Vegzes6;'
				+ @sheet + ',N' + @sor + ',Vegzes7;'
				+ @sheet + ',O' + @sor + ',Vegzes8;'
				+ @sheet + ',P' + @sor + ',Hatarido1;'
				+ @sheet + ',Q' + @sor + ',Hatarido2;'
				+ @sheet + ',R' + @sor + ',JogorvoslatiDontesTartalma1;'
				+ @sheet + ',T' + @sor + ',FellebbezesAlapjan;'
				+ @sheet + ',U' + @sor + ',VegzesKozighivJogorv5;'
				+ @sheet + ',V' + @sor + ',VegzesKozighivJogorv6;'
				+ @sheet + ',W' + @sor + ',VegzesKozighivJogorv7;'
				+ @sheet + ',X' + @sor + ',VegzesKozighivJogorv8;'
				+ @sheet + ',Y' + @sor + ',VegzesDekoncentraltJogorv5;'
				+ @sheet + ',Z' + @sor + ',VegzesDekoncentraltJogorv6;'
				+ @sheet + ',AA' + @sor + ',VegzesDekoncentraltJogorv7;'
				+ @sheet + ',AB' + @sor + ',VegzesDekoncentraltJogorv8;'
				+ @sheet + ',AC' + @sor + ',VegzesBirosagJogorv5;'
				+ @sheet + ',AD' + @sor + ',VegzesBirosagJogorv6;'
				+ @sheet + ',AE' + @sor + ',VegzesBirosagJogorv7;'
				+ @sheet + ',AF' + @sor + ',VegzesBirosagJogorv8;'
				+ @sheet + ',AG' + @sor + ',ErdemiUjrafelvJogorv5;'
				+ @sheet + ',AH' + @sor + ',ErdemiUjrafelvJogorv2;'
				+ @sheet + ',AI' + @sor + ',ErdemiUjrafelvJogorv3;'
				+ @sheet + ',AJ' + @sor + ',ErdemiUjrafelvJogorv4;'
--				+ @sheet + ',AK' + @sor + ',ErdemiMeltanyosJogorv5;'
--				+ @sheet + ',AL' + @sor + ',ErdemiMeltanyosJogorv2;'
--				+ @sheet + ',AM' + @sor + ',ErdemiMeltanyosJogorv3;'
				+ @sheet + ',AK' + @sor + ',ErdemiKozighivJogorv5;'
				+ @sheet + ',AL' + @sor + ',ErdemiKozighivJogorv6;'
				+ @sheet + ',AM' + @sor + ',ErdemiKozighivJogorv7;'
				+ @sheet + ',AN' + @sor + ',ErdemiKozighivJogorv8;'
				+ @sheet + ',AO' + @sor + ',ErdemiDekoncentraltJogorv5;'
				+ @sheet + ',AP' + @sor + ',ErdemiDekoncentraltJogorv6;'
				+ @sheet + ',AQ' + @sor + ',ErdemiDekoncentraltJogorv7;'
				+ @sheet + ',AR' + @sor + ',ErdemiDekoncentraltJogorv8;'
				+ @sheet + ',AS' + @sor + ',ErdemiBirosagJogorv5;'
				+ @sheet + ',AT' + @sor + ',ErdemiBirosagJogorv6;'
				+ @sheet + ',AU' + @sor + ',ErdemiBirosagJogorv7;'
				+ @sheet + ',AV' + @sor + ',ErdemiBirosagJogorv8;'
				+ @sheet + ',AW' + @sor + ',HivatalbolModositVisszavon;'
				+ @sheet + ',AX' + @sor + ',HivatalbolFelugyeleti6;'
				+ @sheet + ',AY' + @sor + ',HivatalbolFelugyeleti7;'
				+ @sheet + ',AZ' + @sor + ',HivatalbolFelugyeleti8;'
			WHERE Kod = @kod
		END
		ELSE IF @Year < 2016
		BEGIN
			UPDATE @Excel_Tabla SET Cells =
				@sheet + ',E' + @sor + ',Donteshozo4;'
				+ @sheet + ',F' + @sor + ',Donteshozo5;'
				+ @sheet + ',G' + @sor + ',Donteshozo6;'
				+ @sheet + ',H' + @sor + ',ErdemiDontes1;'
				+ @sheet + ',I' + @sor + ',ErdemiDontes2;'
				+ @sheet + ',J' + @sor + ',ErdemiDontes3;'
				+ @sheet + ',K' + @sor + ',ErdemiDontes4;'
				+ @sheet + ',L' + @sor + ',Vegzes5;'
				+ @sheet + ',M' + @sor + ',Vegzes6;'
				+ @sheet + ',N' + @sor + ',Vegzes7;'
				+ @sheet + ',O' + @sor + ',Vegzes8;'
				+ @sheet + ',P' + @sor + ',Hatarido1;'
				+ @sheet + ',Q' + @sor + ',Hatarido2;'
				+ @sheet + ',R' + @sor + ',JogorvoslatiDontesTartalma1;'
				+ @sheet + ',T' + @sor + ',FellebbezesAlapjan;'
				+ @sheet + ',U' + @sor + ',VegzesKozighivJogorv5;'
				+ @sheet + ',V' + @sor + ',VegzesKozighivJogorv6;'
				+ @sheet + ',W' + @sor + ',VegzesKozighivJogorv7;'
				+ @sheet + ',X' + @sor + ',VegzesKozighivJogorv8;'
				+ @sheet + ',Y' + @sor + ',VegzesDekoncentraltJogorv5;'
				+ @sheet + ',Z' + @sor + ',VegzesDekoncentraltJogorv6;'
				+ @sheet + ',AA' + @sor + ',VegzesDekoncentraltJogorv7;'
				+ @sheet + ',AB' + @sor + ',VegzesDekoncentraltJogorv8;'
				+ @sheet + ',AC' + @sor + ',VegzesBirosagJogorv5;'
				+ @sheet + ',AD' + @sor + ',VegzesBirosagJogorv6;'
				+ @sheet + ',AE' + @sor + ',VegzesBirosagJogorv7;'
				+ @sheet + ',AF' + @sor + ',VegzesBirosagJogorv8;'
				+ @sheet + ',AG' + @sor + ',ErdemiUjrafelvJogorv5;'
				+ @sheet + ',AH' + @sor + ',ErdemiUjrafelvJogorv2;'
				+ @sheet + ',AI' + @sor + ',ErdemiUjrafelvJogorv3;'
				+ @sheet + ',AJ' + @sor + ',ErdemiUjrafelvJogorv4;'
				+ @sheet + ',AK' + @sor + ',ErdemiKozighivJogorv5;'
				+ @sheet + ',AL' + @sor + ',ErdemiKozighivJogorv6;'
				+ @sheet + ',AM' + @sor + ',ErdemiKozighivJogorv7;'
				+ @sheet + ',AN' + @sor + ',ErdemiKozighivJogorv8;'
				+ @sheet + ',AO' + @sor + ',ErdemiDekoncentraltJogorv5;'
				+ @sheet + ',AP' + @sor + ',ErdemiDekoncentraltJogorv6;'
				+ @sheet + ',AQ' + @sor + ',ErdemiDekoncentraltJogorv7;'
				+ @sheet + ',AR' + @sor + ',ErdemiDekoncentraltJogorv8;'
				+ @sheet + ',AS' + @sor + ',ErdemiBirosagJogorv5;'
				+ @sheet + ',AT' + @sor + ',ErdemiBirosagJogorv6;'
				+ @sheet + ',AU' + @sor + ',ErdemiBirosagJogorv7;'
				+ @sheet + ',AV' + @sor + ',ErdemiBirosagJogorv8;'
				+ @sheet + ',AW' + @sor + ',HivatalbolModositVisszavon;'
				+ @sheet + ',AX' + @sor + ',HivatalbolFelugyeleti6;'
				+ @sheet + ',AY' + @sor + ',HivatalbolFelugyeleti7;'
				+ @sheet + ',AZ' + @sor + ',HivatalbolFelugyeleti8;'
				+ @sheet + ',BA' + @sor + ',HatosagiEllenorzesekSzama;'
				+ @sheet + ',BB' + @sor + ',MunkaorakSzamaAtlagosan;'
				+ @sheet + ',BC' + @sor + ',AtlagosEljarasiKoltseg;'
				+ @sheet + ',BD' + @sor + ',AtlagosKozigazgatasiBirsag;'
			WHERE Kod = @kod
		END
-- 2016-os új statisztika
		ELSE IF @Year < 2018 
		BEGIN
			UPDATE @Excel_Tabla SET Cells =
				@sheet + ',E' + @sor + ',Donteshozo4;'
				+ @sheet + ',F' + @sor + ',Donteshozo5;'
				+ @sheet + ',G' + @sor + ',Donteshozo6;'
				+ @sheet + ',H' + @sor + ',ErdemiDontes1;'
				+ @sheet + ',I' + @sor + ',ErdemiDontes2;'
				+ @sheet + ',J' + @sor + ',ErdemiDontes3;'
				+ @sheet + ',K' + @sor + ',ErdemiDontes4;'
				+ @sheet + ',L' + @sor + ',Vegzes5;'
				+ @sheet + ',M' + @sor + ',Vegzes6;'
				+ @sheet + ',N' + @sor +  ',Vegzes9;'
				+ @sheet + ',O' + @sor +  ',Vegzes7;'
				+ @sheet + ',P' + @sor +  ',Vegzes8;'
				+ @sheet + ',Q' + @sor +  ',Hatarido1;'
				+ @sheet + ',R' + @sor +  ',Hatarido2;'
				+ @sheet + ',S' + @sor +  ',JogorvoslatiDontesTartalma1;'
				+ @sheet + ',U' + @sor +  ',FellebbezesAlapjan;'
				+ @sheet + ',V' + @sor +  ',VegzesKozighivJogorv5;'
				+ @sheet + ',W' + @sor +  ',VegzesKozighivJogorv6;'
				+ @sheet + ',X' + @sor +  ',VegzesKozighivJogorv7;'
				+ @sheet + ',Y' + @sor +  ',VegzesKozighivJogorv8;'
				+ @sheet + ',Z' + @sor +  ',VegzesDekoncentraltJogorv5;'
				+ @sheet + ',AA' + @sor + ',VegzesDekoncentraltJogorv6;'
				+ @sheet + ',AB' + @sor + ',VegzesDekoncentraltJogorv7;'
				+ @sheet + ',AC' + @sor + ',VegzesDekoncentraltJogorv8;'
				+ @sheet + ',AD' + @sor + ',VegzesBirosagJogorv5;'
				+ @sheet + ',AE' + @sor + ',VegzesBirosagJogorv6;'
				+ @sheet + ',AF' + @sor + ',VegzesBirosagJogorv7;'
				+ @sheet + ',AG' + @sor + ',VegzesBirosagJogorv8;'
				+ @sheet + ',AH' + @sor + ',ErdemiUjrafelvJogorv5;'
				+ @sheet + ',AI' + @sor + ',ErdemiUjrafelvJogorv2;'
				+ @sheet + ',AJ' + @sor + ',ErdemiUjrafelvJogorv3;'
				+ @sheet + ',AK' + @sor + ',ErdemiUjrafelvJogorv4;'
				+ @sheet + ',AL' + @sor + ',ErdemiKozighivJogorv5;'
				+ @sheet + ',AM' + @sor + ',ErdemiKozighivJogorv6;'
				+ @sheet + ',AN' + @sor + ',ErdemiKozighivJogorv7;'
				+ @sheet + ',AO' + @sor + ',ErdemiKozighivJogorv8;'
				+ @sheet + ',AP' + @sor + ',ErdemiDekoncentraltJogorv5;'
				+ @sheet + ',AQ' + @sor + ',ErdemiDekoncentraltJogorv6;'
				+ @sheet + ',AR' + @sor + ',ErdemiDekoncentraltJogorv7;'
				+ @sheet + ',AS' + @sor + ',ErdemiDekoncentraltJogorv8;'
				+ @sheet + ',AT' + @sor + ',ErdemiBirosagJogorv5;'
				+ @sheet + ',AU' + @sor + ',ErdemiBirosagJogorv6;'
				+ @sheet + ',AV' + @sor + ',ErdemiBirosagJogorv7;'
				+ @sheet + ',AW' + @sor + ',ErdemiBirosagJogorv8;'
				+ @sheet + ',AX' + @sor + ',HivatalbolModositVisszavon;'
				+ @sheet + ',AY' + @sor + ',HivatalbolFelugyeleti6;'
				+ @sheet + ',AZ' + @sor + ',HivatalbolFelugyeleti7;'
				+ @sheet + ',BA' + @sor + ',HivatalbolFelugyeleti8;'
				+ @sheet + ',BB' + @sor + ',HatosagiEllenorzesekSzama;'
				+ @sheet + ',BC' + @sor + ',MunkaorakSzamaAtlagosan;'
				+ @sheet + ',BD' + @sor + ',AtlagosEljarasiKoltseg;'
				+ @sheet + ',BE' + @sor + ',AtlagosKozigazgatasiBirsag;'
--- INNEN JÖNNEK AZ ÚJAK			
				+ @sheet + ',BF' + @sor + ',EljSzamaElozofelevrolLezartSzama;'
				+ @sheet + ',BK' + @sor + ',EljSzamaElozofelevrolFolyamatbanSzama;'
				+ @sheet + ',BL' + @sor + ',EljSzamaMegismeteltLezartSzama;'
				+ @sheet + ',BM' + @sor + ',EljSzamaMegismeteltFolyamatbanSzama;'
				+ @sheet + ',BN' + @sor + ',EljSzamaTargyfelevbenIndultLezartSzama;'
				+ @sheet + ',BO' + @sor + ',EljSzamaTargyfelevbenIndultFolyamatbanSzama;'
				+ @sheet + ',BP' + @sor + ',SommasEljSzama;'
				+ @sheet + ',BQ' + @sor + ',NyolcnaponBelulNemSommasSzama;'
				+ @sheet + ',BR' + @sor + ',FuggoHatalyuHatarozatSzama;'
				+ @sheet + ',BS' + @sor + ',FuggoHatalyuVegzesSzama;'
				+ @sheet + ',BT' + @sor + ',FuggoHatalyuHatarozatNemHatalyos;'
				+ @sheet + ',BU' + @sor + ',FuggoHatalyuHatarozatHatalyos;'
				+ @sheet + ',BV' + @sor + ',FuggoHatalyuDontesNemHatalyos;'
				+ @sheet + ',BW' + @sor + ',FuggoHatalyuDontesHatalyos;'
				+ @sheet + ',BX' + @sor + ',VisszafizetettOsszeg;'
				+ @sheet + ',BY' + @sor + ',HatosagotTerheloEljarasiKoltseg;'
				
			WHERE Kod = @kod              
		END
-- 2018-as új statisztika
		ELSE IF @Year < 2019
		BEGIN
			UPDATE @Excel_Tabla SET Cells =
				@sheet + ',E' + @sor + ',Donteshozo4;'
				+ @sheet + ',F' + @sor + ',Donteshozo5;'
				+ @sheet + ',G' + @sor + ',Donteshozo6;'
				+ @sheet + ',H' + @sor + ',ErdemiDontes1;'
				+ @sheet + ',I' + @sor + ',ErdemiDontes2;'
				+ @sheet + ',J' + @sor + ',ErdemiDontes3;'
				+ @sheet + ',K' + @sor + ',ErdemiDontes4;'
				+ @sheet + ',L' + @sor + ',Vegzes5;'
				+ @sheet + ',M' + @sor + ',Vegzes6;'
				+ @sheet + ',N' + @sor +  ',Vegzes9;'
				+ @sheet + ',O' + @sor +  ',Vegzes7;'
				+ @sheet + ',P' + @sor +  ',Vegzes8;'
				+ @sheet + ',Q' + @sor +  ',Hatarido1;'
				+ @sheet + ',R' + @sor +  ',Hatarido2;'
				+ @sheet + ',S' + @sor +  ',JogorvoslatiDontesTartalma1;'
				+ @sheet + ',U' + @sor +  ',FellebbezesAlapjan;'
				+ @sheet + ',V' + @sor +  ',VegzesKozighivJogorv5;'
				+ @sheet + ',W' + @sor +  ',VegzesKozighivJogorv6;'
				+ @sheet + ',X' + @sor +  ',VegzesKozighivJogorv7;'
				+ @sheet + ',Y' + @sor +  ',VegzesKozighivJogorv8;'
				+ @sheet + ',Z' + @sor +  ',VegzesDekoncentraltJogorv5;'
				+ @sheet + ',AA' + @sor + ',VegzesDekoncentraltJogorv6;'
				+ @sheet + ',AB' + @sor + ',VegzesDekoncentraltJogorv7;'
				+ @sheet + ',AC' + @sor + ',VegzesDekoncentraltJogorv8;'
				+ @sheet + ',AD' + @sor + ',VegzesBirosagJogorv5;'
				+ @sheet + ',AE' + @sor + ',VegzesBirosagJogorv6;'
				+ @sheet + ',AF' + @sor + ',VegzesBirosagJogorv7;'
				+ @sheet + ',AG' + @sor + ',VegzesBirosagJogorv8;'
				--+ @sheet + ',AH' + @sor + ',ErdemiUjrafelvJogorv5;'
				--+ @sheet + ',AI' + @sor + ',ErdemiUjrafelvJogorv2;'
				--+ @sheet + ',AJ' + @sor + ',ErdemiUjrafelvJogorv3;'
				--+ @sheet + ',AK' + @sor + ',ErdemiUjrafelvJogorv4;'
				+ @sheet + ',AH' + @sor + ',ErdemiKozighivJogorv5;'
				+ @sheet + ',AI' + @sor + ',ErdemiKozighivJogorv6;'
				+ @sheet + ',AJ' + @sor + ',ErdemiKozighivJogorv7;'
				+ @sheet + ',AK' + @sor + ',ErdemiKozighivJogorv8;'
				+ @sheet + ',AL' + @sor + ',ErdemiDekoncentraltJogorv5;'
				+ @sheet + ',AM' + @sor + ',ErdemiDekoncentraltJogorv6;'
				+ @sheet + ',AN' + @sor + ',ErdemiDekoncentraltJogorv7;'
				+ @sheet + ',AO' + @sor + ',ErdemiDekoncentraltJogorv8;'
				+ @sheet + ',AP' + @sor + ',ErdemiBirosagJogorv5;'
				+ @sheet + ',AQ' + @sor + ',ErdemiBirosagJogorv6;'
				+ @sheet + ',AR' + @sor + ',ErdemiBirosagJogorv7;'
				+ @sheet + ',AS' + @sor + ',ErdemiBirosagJogorv8;'
				+ @sheet + ',AT' + @sor + ',HivatalbolModositVisszavon;'
				+ @sheet + ',AU' + @sor + ',HivatalbolFelugyeleti6;'
				+ @sheet + ',AV' + @sor + ',HivatalbolFelugyeleti7;'
				+ @sheet + ',AW' + @sor + ',HivatalbolFelugyeleti8;'
				+ @sheet + ',AX' + @sor + ',HatosagiEllenorzesekSzama;'
				+ @sheet + ',AY' + @sor + ',MunkaorakSzamaAtlagosan;'
				+ @sheet + ',AZ' + @sor + ',AtlagosEljarasiKoltseg;'
				+ @sheet + ',BA' + @sor + ',AtlagosKozigazgatasiBirsag;'    
				+ @sheet + ',BB' + @sor + ',EljSzamaElozofelevrolLezartSzama;'
				+ @sheet + ',BG' + @sor + ',EljSzamaElozofelevrolFolyamatbanSzama;'
				+ @sheet + ',BH' + @sor + ',EljSzamaMegismeteltLezartSzama;'
				+ @sheet + ',BI' + @sor + ',EljSzamaMegismeteltFolyamatbanSzama;'
				+ @sheet + ',BJ' + @sor + ',EljSzamaTargyfelevbenIndultLezartSzama;'
				+ @sheet + ',BK' + @sor + ',EljSzamaTargyfelevbenIndultFolyamatbanSzama;'
				+ @sheet + ',BL' + @sor + ',SommasEljSzama;'
				+ @sheet + ',BM' + @sor + ',NyolcnaponBelulNemSommasSzama;'
				+ @sheet + ',BN' + @sor + ',FuggoHatalyuHatarozatSzama;'
				+ @sheet + ',BO' + @sor + ',FuggoHatalyuVegzesSzama;'
				+ @sheet + ',BP' + @sor + ',FuggoHatalyuHatarozatNemHatalyos;'
				+ @sheet + ',BQ' + @sor + ',FuggoHatalyuHatarozatHatalyos;'
				+ @sheet + ',BR' + @sor + ',FuggoHatalyuDontesNemHatalyos;'
				+ @sheet + ',BS' + @sor + ',FuggoHatalyuDontesHatalyos;'
				+ @sheet + ',BT' + @sor + ',VisszafizetettOsszeg;'
				+ @sheet + ',BU' + @sor + ',HatosagotTerheloEljarasiKoltseg;'
				
			WHERE Kod = @kod              
		END
		ELSE
		BEGIN
			UPDATE @Excel_Tabla SET Cells =
				@sheet + ',C' + @sor + ',Donteshozo4;'
				+ @sheet + ',D' + @sor + ',Donteshozo5;'
				+ @sheet + ',E' + @sor + ',Donteshozo6;'
				+ @sheet + ',F' + @sor + ',ErdemiDontes1;'
				+ @sheet + ',G' + @sor + ',ErdemiDontes2;'
				+ @sheet + ',H' + @sor + ',ErdemiDontes3;'
				+ @sheet + ',I' + @sor + ',ErdemiDontes4;'
				+ @sheet + ',J' + @sor + ',Vegzes5;'
				+ @sheet + ',K' + @sor + ',Vegzes6;'
				+ @sheet + ',L' + @sor +  ',Vegzes9;'
				+ @sheet + ',M' + @sor +  ',Vegzes7;'
				+ @sheet + ',N' + @sor +  ',Vegzes8;'
				+ @sheet + ',O' + @sor +  ',Hatarido1;'
				+ @sheet + ',P' + @sor +  ',Hatarido2;'
				+ @sheet + ',Q' + @sor +  ',JogorvoslatiDontesTartalma1;'
				+ @sheet + ',S' + @sor +  ',FellebbezesAlapjan;'
				+ @sheet + ',T' + @sor +  ',VegzesKozighivJogorv5;'
				+ @sheet + ',U' + @sor +  ',VegzesKozighivJogorv6;'
				+ @sheet + ',V' + @sor +  ',VegzesKozighivJogorv7;'
				+ @sheet + ',W' + @sor +  ',VegzesKozighivJogorv8;'
				+ @sheet + ',X' + @sor +  ',VegzesDekoncentraltJogorv5;'
				+ @sheet + ',Y' + @sor + ',VegzesDekoncentraltJogorv6;'
				+ @sheet + ',Z' + @sor + ',VegzesDekoncentraltJogorv7;'
				+ @sheet + ',AA' + @sor + ',VegzesDekoncentraltJogorv8;'
				+ @sheet + ',AB' + @sor + ',VegzesBirosagJogorv5;'
				+ @sheet + ',AC' + @sor + ',VegzesBirosagJogorv6;'
				+ @sheet + ',AD' + @sor + ',VegzesBirosagJogorv7;'
				+ @sheet + ',AE' + @sor + ',VegzesBirosagJogorv8;'
				--+ @sheet + ',AH' + @sor + ',ErdemiUjrafelvJogorv5;'
				--+ @sheet + ',AI' + @sor + ',ErdemiUjrafelvJogorv2;'
				--+ @sheet + ',AJ' + @sor + ',ErdemiUjrafelvJogorv3;'
				--+ @sheet + ',AK' + @sor + ',ErdemiUjrafelvJogorv4;'
				+ @sheet + ',AF' + @sor + ',ErdemiKozighivJogorv5;'
				+ @sheet + ',AG' + @sor + ',ErdemiKozighivJogorv6;'
				+ @sheet + ',AH' + @sor + ',ErdemiKozighivJogorv7;'
				+ @sheet + ',AI' + @sor + ',ErdemiKozighivJogorv8;'
				+ @sheet + ',AJ' + @sor + ',ErdemiDekoncentraltJogorv5;'
				+ @sheet + ',AK' + @sor + ',ErdemiDekoncentraltJogorv6;'
				+ @sheet + ',AL' + @sor + ',ErdemiDekoncentraltJogorv7;'
				+ @sheet + ',AM' + @sor + ',ErdemiDekoncentraltJogorv8;'
				+ @sheet + ',AN' + @sor + ',ErdemiBirosagJogorv5;'
				+ @sheet + ',AO' + @sor + ',ErdemiBirosagJogorv6;'
				+ @sheet + ',AP' + @sor + ',ErdemiBirosagJogorv7;'
				+ @sheet + ',AQ' + @sor + ',ErdemiBirosagJogorv8;'
				+ @sheet + ',AR' + @sor + ',HivatalbolModositVisszavon;'
				+ @sheet + ',AA' + @sor + ',HivatalbolFelugyeleti6;'
				+ @sheet + ',AT' + @sor + ',HivatalbolFelugyeleti7;'
				+ @sheet + ',AU' + @sor + ',HivatalbolFelugyeleti8;'
				+ @sheet + ',AV' + @sor + ',HatosagiEllenorzesekSzama;'
				+ @sheet + ',AW' + @sor + ',MunkaorakSzamaAtlagosan;'
				+ @sheet + ',AX' + @sor + ',AtlagosEljarasiKoltseg;'
				+ @sheet + ',AY' + @sor + ',AtlagosKozigazgatasiBirsag;'    
				+ @sheet + ',AZ' + @sor + ',EljSzamaElozofelevrolLezartSzama;'
				+ @sheet + ',BA' + @sor + ',EljSzamaElozofelevrolFolyamatbanSzama;'
				+ @sheet + ',BB' + @sor + ',EljSzamaMegismeteltLezartSzama;'
				+ @sheet + ',BC' + @sor + ',EljSzamaMegismeteltFolyamatbanSzama;'
				+ @sheet + ',BD' + @sor + ',EljSzamaTargyfelevbenIndultLezartSzama;'
				+ @sheet + ',BE' + @sor + ',EljSzamaTargyfelevbenIndultFolyamatbanSzama;'
				+ @sheet + ',BF' + @sor + ',SommasEljSzama;'
				+ @sheet + ',BG' + @sor + ',NyolcnaponBelulNemSommasSzama;'
				+ @sheet + ',BH' + @sor + ',FuggoHatalyuHatarozatSzama;'
				+ @sheet + ',BI' + @sor + ',FuggoHatalyuVegzesSzama;'
				+ @sheet + ',BJ' + @sor + ',FuggoHatalyuHatarozatNemHatalyos;'
				+ @sheet + ',BK' + @sor + ',FuggoHatalyuHatarozatHatalyos;'
				+ @sheet + ',BL' + @sor + ',FuggoHatalyuDontesNemHatalyos;'
				+ @sheet + ',BM' + @sor + ',FuggoHatalyuDontesHatalyos;'
				+ @sheet + ',BN' + @sor + ',VisszafizetettOsszeg;'
				+ @sheet + ',BO' + @sor + ',HatosagotTerheloEljarasiKoltseg;'
				
			WHERE Kod = @kod              
		END

		FETCH excelsor INTO @sor, @kod

	END

CLOSE excelsor

DEALLOCATE excelsor

	-- TempTabla tartalmanak torlese a kovetkezo lekerdezes elott
	DELETE FROM @TempTabla


-- Reszeredmenyek visszaadasa
	SELECT r.*, e.Cells FROM @Reszletes_Tabla r
	JOIN @Excel_Tabla e
	ON r.Kod = e.Kod

        END TRY
        BEGIN CATCH
            DECLARE @errorSeverity INT,
                @errorState INT
            DECLARE @errorCode NVARCHAR(1000)    
            SET @errorSeverity = ERROR_SEVERITY()
            SET @errorState = ERROR_STATE()
	
            IF ERROR_NUMBER() < 50000 
                SET @errorCode = '[' + CONVERT(NVARCHAR(10), ERROR_NUMBER())
                    + '] ' + ERROR_MESSAGE()
            ELSE 
                SET @errorCode = ERROR_MESSAGE()
      
            IF @errorState = 0 
                SET @errorState = 1

            RAISERROR ( @errorCode, @errorSeverity, @errorState )
 
        END CATCH

    END