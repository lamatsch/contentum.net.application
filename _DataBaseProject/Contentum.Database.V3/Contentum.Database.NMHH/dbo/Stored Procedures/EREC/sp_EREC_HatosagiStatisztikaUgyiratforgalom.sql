﻿-- Cél: Hatósági statisztikához ügyiratforgalom kimutatása
-- kötött (védett Excelben adott) bontásban, ágazatok szerint
-- valamint Sorszámra, Gyujtoív sorszámára (ilyen most nincs)
-- ill. Alszámra iktatott iratok kategóriák szerint
-- 
-- Továbbá: csak a második szintet figyeljuk, pl. U.1
-- de nem lehet csak U (mert azt az Excelben kapjuk összegként)
-- es erre a szintre vonjuk össze a mélyebb hierarchia-szinteket
-- is, pl. U.3.1, U.3.2 stb. az U.3 ala
-- Az összevonásnál kihasznaljuk az ABC-sorrendet,
-- valamint, hogy az agazati jelek
-- listajan nincs egyjegyunel nagyobb szamjel!!!
-- 

CREATE PROCEDURE [dbo].[sp_EREC_HatosagiStatisztikaUgyiratforgalom]
    @Where NVARCHAR(4000) = '',
    @OrderBy NVARCHAR(200) = '',
    @TopRow NVARCHAR(5) = '',
    @ExecutorUserId UNIQUEIDENTIFIER,
    @FelhasznaloSzervezet_Id UNIQUEIDENTIFIER = null,
    @KezdDat datetime,
    @VegeDat datetime,
    @sheet nvarchar(100) = 'önkormányzat_székhely' -- a kitoltendo Excel-tabla neve
AS 
    BEGIN
        BEGIN TRY

            SET nocount ON


DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

-- 1. Iktatott ugyiratok szama sorszamra (= foszamra, EREC_UgyUgyiratok-ban keletkezett
-- ugyek szama) ill. alszamra (= EREC_IraIratokban iktatott iratok szama)
-- Agazati jel szerint bontva

DECLARE @Foszam_Tabla table (
	--Id uniqueidentifier,
	Kod nvarchar(20),
	Darab int
)

DECLARE @Alszam_Tabla table (
	--Id uniqueidentifier,
	Kod nvarchar(20),
	Darab int
)

-- az Excel-tabla kitoltesehez a vezerlo informaciokat tartalmazza
DECLARE @Excel_Tabla table (
	Kod nvarchar(20),
	Cells nvarchar(MAX)
)

-- a kitoltendo Excel-tabla neve
--DECLARE @sheet nvarchar(100)
--	SET @sheet = 'önkormányzat_székhely'	

	-- Adatok mentese a @Foszam_Tablaba
	INSERT INTO @Foszam_Tabla
	SELECT SUBSTRING(a.Kod, 1, 3) AS Kod, count(u.Id) AS Darab FROM
	EREC_AgazatiJelek a LEFT JOIN EREC_IraIrattariTetelek it
	ON it.AgazatiJel_Id = a.Id and it.Org=@Org
	LEFT JOIN (SELECT u.Id, u.IraIrattariTetel_Id
				FROM EREC_UgyUgyiratok u
				WHERE u.LetrehozasIdo between @KezdDat AND @VegeDat
				AND u.SztornirozasDat is null
				) AS u
	ON u.IraIrattariTetel_Id = it.Id
	GROUP BY SUBSTRING(a.Kod, 1, 3)
	ORDER BY SUBSTRING(a.Kod, 1, 3)

	-- Adatok mentese a @Alszam_Tablaba
	INSERT INTO @Alszam_Tabla
	SELECT SUBSTRING(a.Kod, 1, 3) AS Kod, count(i.Id) AS Darab
	FROM
	EREC_AgazatiJelek a LEFT JOIN EREC_IraIrattariTetelek it
	ON it.AgazatiJel_Id = a.Id and it.Org=@Org
	LEFT JOIN (SELECT u.Id, u.IraIrattariTetel_Id
				FROM EREC_UgyUgyiratok u
				WHERE u.SztornirozasDat is null
				) AS u			
	ON u.IraIrattariTetel_Id = it.Id
	LEFT JOIN EREC_UgyUgyiratdarabok ud
	ON ud.UgyUgyirat_Id = u.Id
	LEFT JOIN (SELECT i.Id, i.UgyUgyiratDarab_Id
				FROM EREC_IraIratok i
				WHERE i.IktatasDatuma between @KezdDat AND @VegeDat
				AND i.SztornirozasDat is null
				) AS i
	ON i.UgyUgyiratDarab_Id = ud.Id
	GROUP BY SUBSTRING(a.Kod, 1, 3)
	ORDER BY SUBSTRING(a.Kod, 1, 3)

-- Az Excel vezerlotabla feltoltese

INSERT INTO @Excel_Tabla
	SELECT SUBSTRING(a.Kod, 1, 3) AS Kod, null AS Cells
	FROM EREC_AgazatiJelek a
	GROUP BY SUBSTRING(a.Kod, 1, 3)
	ORDER BY SUBSTRING(a.Kod, 1, 3)

-- Manualis kitoltes
-- az Excel-tabla adott kodhoz tartozo sorai
DECLARE @SorokKodok_Tabla AS table (
	sor varchar(3), -- megfelelo excel-tablasor
	kod varchar(3)  -- agazati jel max. masdik szintig
)

declare @year int
set @year = DatePart(yyyy, @kezdDat)

if (@year < 2010)
BEGIN
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('12', 'A.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('13', 'A.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('15', 'B')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('16', 'C')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('18', 'E.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('19', 'E.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('20', 'E.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('21', 'F')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('22', 'G')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('24', 'H.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('25', 'H.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('26', 'H.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('27', 'H.4')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('28', 'H.5')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('29', 'H.6')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('30', 'H.7')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('31', 'H.8')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('32', 'I')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('33', 'J')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('34', 'K')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('35', 'L')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('36', 'M')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('37', 'N')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('39', 'U.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('40', 'U.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('41', 'U.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('42', 'P')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('43', 'R')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('45', 'X.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('46', 'X.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('47', 'X.3')
END
ELSE IF @year < 2015
BEGIN
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('14', 'A.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('15', 'A.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('16', 'B')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('17', 'C')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('19', 'E.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('20', 'E.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('21', 'E.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('22', 'F')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('23', 'G')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('25', 'H.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('26', 'H.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('27', 'H.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('28', 'H.4')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('29', 'H.5')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('30', 'H.6')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('31', 'H.7')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('32', 'H.8')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('33', 'I')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('34', 'J')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('35', 'K')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('36', 'L')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('37', 'M')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('38', 'N')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('40', 'U.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('41', 'U.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('42', 'U.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('43', 'P')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('44', 'R')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('46', 'X.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('47', 'X.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('48', 'X.3')
END
ELSE
BEGIN
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('14', 'A.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('15', 'A.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('16', 'B')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('17', 'C')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('19', 'E.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('20', 'E.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('21', 'E.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('22', 'E.4')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('23', 'F')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('24', 'G')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('26', 'H.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('27', 'H.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('28', 'H.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('29', 'H.4')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('30', 'H.5')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('31', 'H.6')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('32', 'H.7')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('33', 'H.8')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('34', 'I')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('35', 'J')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('36', 'K')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('37', 'L')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('38', 'M')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('39', 'N')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('41', 'U.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('42', 'U.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('43', 'U.3')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('44', 'P')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('45', 'R')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('47', 'X.1')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('48', 'X.2')
	INSERT INTO @SorokKodok_Tabla (sor, kod) values ('49', 'X.3')
END

DECLARE  @sor nvarchar(10),
         @kod nvarchar(3)

-- cursor deklaralasa
DECLARE excelsor CURSOR FOR
	SELECT sor, kod FROM @SorokKodok_Tabla

OPEN excelsor

-- ciklus kezdete elotti fetch
FETCH excelsor INTO @sor, @kod

WHILE @@Fetch_Status = 0
	BEGIN
		UPDATE @Excel_Tabla SET Cells =
			@sheet + ',D' + @sor + ',Foszamra;'
			+ @sheet + ',E' + @sor + ',Gyujtoivre;'
			+ @sheet + ',F' + @sor + ',Alszamra;'
		WHERE Kod = @kod

		FETCH excelsor INTO @sor, @kod

	END

CLOSE excelsor

DEALLOCATE excelsor

-- Reszeredmenyek visszaadasa
	-- SELECT * FROM @Foszam_Tabla
	-- SELECT * FROM @Alszam_Tabla
	SELECT f.Kod AS Kod, e.Cells AS Cells, f.Darab AS Foszamra, 0 AS Gyujtoivre, a.Darab AS Alszamra
	FROM @Foszam_Tabla AS f JOIN @Alszam_Tabla AS a
    ON f.Kod = a.Kod
	JOIN @Excel_Tabla AS e
    ON e.Kod = f.Kod

        END TRY
        BEGIN CATCH
            DECLARE @errorSeverity INT,
                @errorState INT
            DECLARE @errorCode NVARCHAR(1000)    
            SET @errorSeverity = ERROR_SEVERITY()
            SET @errorState = ERROR_STATE()
	
            IF ERROR_NUMBER() < 50000 
                SET @errorCode = '[' + CONVERT(NVARCHAR(10), ERROR_NUMBER())
                    + '] ' + ERROR_MESSAGE()
            ELSE 
                SET @errorCode = ERROR_MESSAGE()
      
            IF @errorState = 0 
                SET @errorState = 1

            RAISERROR ( @errorCode, @errorSeverity, @errorState )
 
        END CATCH

    END