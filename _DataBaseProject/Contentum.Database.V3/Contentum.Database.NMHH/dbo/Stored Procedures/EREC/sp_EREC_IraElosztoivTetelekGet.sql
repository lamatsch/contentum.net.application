﻿create procedure [dbo].[sp_EREC_IraElosztoivTetelekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IraElosztoivTetelek.Id,
	   EREC_IraElosztoivTetelek.ElosztoIv_Id,
	   EREC_IraElosztoivTetelek.Sorszam,
	   EREC_IraElosztoivTetelek.Partner_Id,
	   EREC_IraElosztoivTetelek.Cim_Id,
	   EREC_IraElosztoivTetelek.CimSTR,
	   EREC_IraElosztoivTetelek.NevSTR,
	   EREC_IraElosztoivTetelek.Kuldesmod,
	   EREC_IraElosztoivTetelek.Visszavarolag,
	   EREC_IraElosztoivTetelek.VisszavarasiIdo,
	   EREC_IraElosztoivTetelek.Vissza_Nap_Mulva,
	   EREC_IraElosztoivTetelek.AlairoSzerep,
	   EREC_IraElosztoivTetelek.Ver,
	   EREC_IraElosztoivTetelek.Note,
	   EREC_IraElosztoivTetelek.Stat_id,
	   EREC_IraElosztoivTetelek.ErvKezd,
	   EREC_IraElosztoivTetelek.ErvVege,
	   EREC_IraElosztoivTetelek.Letrehozo_id,
	   EREC_IraElosztoivTetelek.LetrehozasIdo,
	   EREC_IraElosztoivTetelek.Modosito_id,
	   EREC_IraElosztoivTetelek.ModositasIdo,
	   EREC_IraElosztoivTetelek.Zarolo_id,
	   EREC_IraElosztoivTetelek.ZarolasIdo,
	   EREC_IraElosztoivTetelek.Tranz_id,
	   EREC_IraElosztoivTetelek.UIAccessLog_id
	   from 
		 EREC_IraElosztoivTetelek as EREC_IraElosztoivTetelek 
	   where
		 EREC_IraElosztoivTetelek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end