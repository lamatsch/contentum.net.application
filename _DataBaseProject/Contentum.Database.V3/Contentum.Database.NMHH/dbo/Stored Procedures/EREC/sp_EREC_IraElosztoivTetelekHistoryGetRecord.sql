﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_IraElosztoivTetelekHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_EREC_IraElosztoivTetelekHistoryGetRecord
go
*/
create procedure sp_EREC_IraElosztoivTetelekHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_IraElosztoivTetelekHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivTetelekHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_IraElosztoivTetelekHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivTetelekHistory Old
         inner join EREC_IraElosztoivTetelekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraElosztoivTetelekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ElosztoIv_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_IraElosztoivekAzonosito(Old.ElosztoIv_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_IraElosztoivekAzonosito(New.ElosztoIv_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivTetelekHistory Old
         inner join EREC_IraElosztoivTetelekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraElosztoivTetelekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ElosztoIv_Id as nvarchar(max)),'') != ISNULL(CAST(New.ElosztoIv_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_IraElosztoivek FTOld on FTOld.Id = Old.ElosztoIv_Id and FTOld.Ver = Old.Ver
         left join EREC_IraElosztoivek FTNew on FTNew.Id = New.ElosztoIv_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Sorszam' as ColumnName,               
               cast(Old.Sorszam as nvarchar(99)) as OldValue,
               cast(New.Sorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivTetelekHistory Old
         inner join EREC_IraElosztoivTetelekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraElosztoivTetelekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Sorszam as nvarchar(max)),'') != ISNULL(CAST(New.Sorszam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(Old.Partner_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(New.Partner_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivTetelekHistory Old
         inner join EREC_IraElosztoivTetelekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraElosztoivTetelekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Partner_Id as nvarchar(max)),'') != ISNULL(CAST(New.Partner_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Partnerek FTOld on FTOld.Id = Old.Partner_Id and FTOld.Ver = Old.Ver
         left join KRT_Partnerek FTNew on FTNew.Id = New.Partner_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Cim_Id' as ColumnName,               
               cast(Old.Cim_Id as nvarchar(99)) as OldValue,
               cast(New.Cim_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivTetelekHistory Old
         inner join EREC_IraElosztoivTetelekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraElosztoivTetelekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Cim_Id as nvarchar(max)),'') != ISNULL(CAST(New.Cim_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CimSTR' as ColumnName,               
               cast(Old.CimSTR as nvarchar(99)) as OldValue,
               cast(New.CimSTR as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivTetelekHistory Old
         inner join EREC_IraElosztoivTetelekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraElosztoivTetelekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.CimSTR as nvarchar(max)),'') != ISNULL(CAST(New.CimSTR as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'NevSTR' as ColumnName,               
               cast(Old.NevSTR as nvarchar(99)) as OldValue,
               cast(New.NevSTR as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivTetelekHistory Old
         inner join EREC_IraElosztoivTetelekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraElosztoivTetelekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.NevSTR as nvarchar(max)),'') != ISNULL(CAST(New.NevSTR as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kuldesmod' as ColumnName,               
               cast(Old.Kuldesmod as nvarchar(99)) as OldValue,
               cast(New.Kuldesmod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivTetelekHistory Old
         inner join EREC_IraElosztoivTetelekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraElosztoivTetelekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Kuldesmod as nvarchar(max)),'') != ISNULL(CAST(New.Kuldesmod as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Visszavarolag' as ColumnName,               
               cast(Old.Visszavarolag as nvarchar(99)) as OldValue,
               cast(New.Visszavarolag as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivTetelekHistory Old
         inner join EREC_IraElosztoivTetelekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraElosztoivTetelekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Visszavarolag as nvarchar(max)),'') != ISNULL(CAST(New.Visszavarolag as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VisszavarasiIdo' as ColumnName,               
               cast(Old.VisszavarasiIdo as nvarchar(99)) as OldValue,
               cast(New.VisszavarasiIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivTetelekHistory Old
         inner join EREC_IraElosztoivTetelekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraElosztoivTetelekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.VisszavarasiIdo as nvarchar(max)),'') != ISNULL(CAST(New.VisszavarasiIdo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Vissza_Nap_Mulva' as ColumnName,               
               cast(Old.Vissza_Nap_Mulva as nvarchar(99)) as OldValue,
               cast(New.Vissza_Nap_Mulva as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivTetelekHistory Old
         inner join EREC_IraElosztoivTetelekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraElosztoivTetelekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Vissza_Nap_Mulva as nvarchar(max)),'') != ISNULL(CAST(New.Vissza_Nap_Mulva as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairoSzerep' as ColumnName,               
               cast(Old.AlairoSzerep as nvarchar(99)) as OldValue,
               cast(New.AlairoSzerep as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraElosztoivTetelekHistory Old
         inner join EREC_IraElosztoivTetelekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraElosztoivTetelekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AlairoSzerep as nvarchar(max)),'') != ISNULL(CAST(New.AlairoSzerep as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go