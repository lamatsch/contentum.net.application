﻿create procedure [dbo].[sp_EREC_IraElosztoivTetelekUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @ElosztoIv_Id     uniqueidentifier  = null,         
             @Sorszam     int  = null,         
             @Partner_Id     uniqueidentifier  = null,         
             @Cim_Id     uniqueidentifier  = null,         
             @CimSTR     Nvarchar(400)  = null,         
             @NevSTR     Nvarchar(400)  = null,         
             @Kuldesmod     nvarchar(64)  = null,         
             @Visszavarolag     char(1)  = null,         
             @VisszavarasiIdo     datetime  = null,         
             @Vissza_Nap_Mulva     int  = null,         
             @AlairoSzerep     char(1)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_ElosztoIv_Id     uniqueidentifier         
     DECLARE @Act_Sorszam     int         
     DECLARE @Act_Partner_Id     uniqueidentifier         
     DECLARE @Act_Cim_Id     uniqueidentifier         
     DECLARE @Act_CimSTR     Nvarchar(400)         
     DECLARE @Act_NevSTR     Nvarchar(400)         
     DECLARE @Act_Kuldesmod     nvarchar(64)         
     DECLARE @Act_Visszavarolag     char(1)         
     DECLARE @Act_VisszavarasiIdo     datetime         
     DECLARE @Act_Vissza_Nap_Mulva     int         
     DECLARE @Act_AlairoSzerep     char(1)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_ElosztoIv_Id = ElosztoIv_Id,
     @Act_Sorszam = Sorszam,
     @Act_Partner_Id = Partner_Id,
     @Act_Cim_Id = Cim_Id,
     @Act_CimSTR = CimSTR,
     @Act_NevSTR = NevSTR,
     @Act_Kuldesmod = Kuldesmod,
     @Act_Visszavarolag = Visszavarolag,
     @Act_VisszavarasiIdo = VisszavarasiIdo,
     @Act_Vissza_Nap_Mulva = Vissza_Nap_Mulva,
     @Act_AlairoSzerep = AlairoSzerep,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_IraElosztoivTetelek
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/ElosztoIv_Id')=1
         SET @Act_ElosztoIv_Id = @ElosztoIv_Id
   IF @UpdatedColumns.exist('/root/Sorszam')=1
         SET @Act_Sorszam = @Sorszam
   IF @UpdatedColumns.exist('/root/Partner_Id')=1
         SET @Act_Partner_Id = @Partner_Id
   IF @UpdatedColumns.exist('/root/Cim_Id')=1
         SET @Act_Cim_Id = @Cim_Id
   IF @UpdatedColumns.exist('/root/CimSTR')=1
         SET @Act_CimSTR = @CimSTR
   IF @UpdatedColumns.exist('/root/NevSTR')=1
         SET @Act_NevSTR = @NevSTR
   IF @UpdatedColumns.exist('/root/Kuldesmod')=1
         SET @Act_Kuldesmod = @Kuldesmod
   IF @UpdatedColumns.exist('/root/Visszavarolag')=1
         SET @Act_Visszavarolag = @Visszavarolag
   IF @UpdatedColumns.exist('/root/VisszavarasiIdo')=1
         SET @Act_VisszavarasiIdo = @VisszavarasiIdo
   IF @UpdatedColumns.exist('/root/Vissza_Nap_Mulva')=1
         SET @Act_Vissza_Nap_Mulva = @Vissza_Nap_Mulva
   IF @UpdatedColumns.exist('/root/AlairoSzerep')=1
         SET @Act_AlairoSzerep = @AlairoSzerep
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_IraElosztoivTetelek
SET
     ElosztoIv_Id = @Act_ElosztoIv_Id,
     Sorszam = @Act_Sorszam,
     Partner_Id = @Act_Partner_Id,
     Cim_Id = @Act_Cim_Id,
     CimSTR = @Act_CimSTR,
     NevSTR = @Act_NevSTR,
     Kuldesmod = @Act_Kuldesmod,
     Visszavarolag = @Act_Visszavarolag,
     VisszavarasiIdo = @Act_VisszavarasiIdo,
     Vissza_Nap_Mulva = @Act_Vissza_Nap_Mulva,
     AlairoSzerep = @Act_AlairoSzerep,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_IraElosztoivTetelek',@Id
					,'EREC_IraElosztoivTetelekHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH