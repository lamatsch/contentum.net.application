﻿create procedure [dbo].[sp_EREC_IraElosztoivekGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraElosztoivek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
            
   DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_IraElosztoivek.Id,
	   EREC_IraElosztoivek.Org,
	   EREC_IraElosztoivek.Ev,
	   EREC_IraElosztoivek.Fajta,
	   EREC_IraElosztoivek.Kod,
	   EREC_IraElosztoivek.NEV,
	   EREC_IraElosztoivek.Hasznalat,
	   EREC_IraElosztoivek.Csoport_Id_Tulaj,
	   EREC_IraElosztoivek.Ver,
	   EREC_IraElosztoivek.Note,
	   EREC_IraElosztoivek.Stat_id,
	   EREC_IraElosztoivek.ErvKezd,
	   EREC_IraElosztoivek.ErvVege,
	   EREC_IraElosztoivek.Letrehozo_id,
	   EREC_IraElosztoivek.LetrehozasIdo,
	   EREC_IraElosztoivek.Modosito_id,
	   EREC_IraElosztoivek.ModositasIdo,
	   EREC_IraElosztoivek.Zarolo_id,
	   EREC_IraElosztoivek.ZarolasIdo,
	   EREC_IraElosztoivek.Tranz_id,
	   EREC_IraElosztoivek.UIAccessLog_id  
   from 
     EREC_IraElosztoivek as EREC_IraElosztoivek      
    Where EREC_IraElosztoivek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end