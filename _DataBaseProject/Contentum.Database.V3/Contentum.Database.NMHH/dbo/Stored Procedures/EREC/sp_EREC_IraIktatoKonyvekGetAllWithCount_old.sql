﻿create procedure [dbo].[sp_EREC_IraIktatoKonyvekGetAllWithCount_old]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by  EREC_IraIktatoKonyvek.Iktatohely,EREC_IraIktatoKonyvek.Ev',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier,
  @Jogosultak	char(1) = '0',
  @KezdDate nvarchar(23),
  @VegeDate nvarchar(23)

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(max)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
	   EREC_IraIktatoKonyvek.Iktatohely,
	   EREC_IraIktatoKonyvek.Nev,
	   EREC_IraIktatoKonyvek.Ev,
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as FoszamDb,
(select count(*) from EREC_IraIratok where EREC_IraIratok.UgyUgyIratDarab_Id IN (select EREC_UgyUgyiratdarabok.Id from EREC_UgyUgyiratdarabok where EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id) and EREC_IraIratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as AlszamDb,
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot = ''04'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot=''04'')) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as IktatottDb,
CAST(CAST((select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot = ''04'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot=''04'')) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1))*100/CAST((select (case count(*) when 0 then 1 else count(*) end) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1)) AS decimal(10,1)) as IktatottSzazalek,
	   '
SET @sqlcmd = @sqlcmd +
   '
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot = ''06'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot=''06'')) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as UgyintezesAlattDb,
CAST(CAST((select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot = ''06'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot=''06'')) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1))*100/CAST((select (case count(*) when 0 then 1 else count(*) end) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1)) AS decimal(10,1)) as UgyintezesAlattSzazalek,
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''07'',''57'',''71'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''07'',''57'',''71''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as SkontrobanDb,
CAST(CAST((select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot = ''07'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot=''07'')) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1))*100/CAST((select (case count(*) when 0 then 1 else count(*) end) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1)) AS decimal(10,1)) as SkontrobanSzazalek,
	   '
SET @sqlcmd = @sqlcmd +
   '
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot = ''10'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot=''10'')) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as IrattarbanDb,
CAST(CAST((select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot = ''10'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot=''10'')) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1))*100/CAST((select (case count(*) when 0 then 1 else count(*) end) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1)) AS decimal(10,1)) as IrattarbanSzazalek,
	   '
SET @sqlcmd = @sqlcmd +
   '
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''52'',''55'',''56'',''11'',''13'',''99'',''09'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''52'',''55'',''56'',''11'',''13'',''99'',''09''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as UgyintezesAlatt2Db,
	   '
SET @sqlcmd = @sqlcmd +
   '
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot = ''90'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''90'')) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as SztornozottDb,  
	   '
SET @sqlcmd = @sqlcmd +
   '
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot = ''60'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''60'')) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as SzereltDb,   
	   '
SET @sqlcmd = @sqlcmd +
   '
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot = ''32'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''32'')) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as SelejtezettDb,  
	   '
SET @sqlcmd = @sqlcmd +
   '
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot = ''33'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''33'')) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as LeveltarbaAdottDb, 
	   '
SET @sqlcmd = @sqlcmd +
   '
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as idoszaki_FoszamDb,
	   '
SET @sqlcmd = @sqlcmd +
   '
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as idoszaki_FoszamDb_sztornoval,
	   '
SET @sqlcmd = @sqlcmd +
   '
(select count(*) from EREC_IraIratok where EREC_IraIratok.UgyUgyIratDarab_Id IN (select EREC_UgyUgyiratdarabok.Id from EREC_UgyUgyiratdarabok where EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and EREC_UgyUgyiratdarabok.UgyUgyirat_Id IN (select EREC_UgyUgyiratok.Id from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99''))))) and EREC_IraIratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as idoszaki_AlszamDb,
	   '
SET @sqlcmd = @sqlcmd +
   '
(select count(*) from EREC_IraIratok where EREC_IraIratok.UgyUgyIratDarab_Id IN (select EREC_UgyUgyiratdarabok.Id from EREC_UgyUgyiratdarabok where EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and EREC_UgyUgyiratdarabok.UgyUgyirat_Id IN (select EREC_UgyUgyiratok.Id from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99''))))) and EREC_IraIratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as idoszaki_AlszamDb_sztornoval,
	   '
SET @sqlcmd = @sqlcmd +
   '
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''13'',''55'',''56'',''52'',''70'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''13'',''55'',''56'',''52'',''70'',''99''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as idoszaki_UgyintezesAlattDb,
	   '
SET @sqlcmd = @sqlcmd +
   '
CAST(CAST((select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''13'',''55'',''56'',''52'',''70'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''13'',''55'',''56'',''52'',''70'',''99''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1))*100/CAST((select (case count(*) when 0 then 1 else count(*) end) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1)) AS decimal(10,1)) as idoszaki_UgyintezesAlattSzazalek,
	   '
SET @sqlcmd = @sqlcmd +
   '
CAST(CAST((select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''13'',''55'',''56'',''52'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''13'',''55'',''56'',''52'',''70'',''99''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1))*100/CAST((select (case count(*) when 0 then 1 else count(*) end) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1)) AS decimal(10,1)) as idoszaki_UgyintezesAlattSzazalek_sztornoval,
	   '
SET @sqlcmd = @sqlcmd +
   '
CAST(CAST((select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''07'',''57'',''71'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''07'',''57'',''71''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1))*100/CAST((select (case count(*) when 0 then 1 else count(*) end) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1)) AS decimal(10,1)) as idoszaki_SkontrobanSzazalek,
	   '
SET @sqlcmd = @sqlcmd +
   '
CAST(CAST((select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''07'',''57'',''71'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''07'',''57'',''71''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1))*100/CAST((select (case count(*) when 0 then 1 else count(*) end) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1)) AS decimal(10,1)) as idoszaki_SkontrobanSzazalek_sztornoval,
	   '
SET @sqlcmd = @sqlcmd +
   '
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''09'',''10'',''11'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''09'',''10'',''11''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as idoszaki_IrattarbanDb,
	   '
SET @sqlcmd = @sqlcmd +
   '
CAST(CAST((select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''09'',''10'',''11'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''09'',''10'',''11''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1))*100/CAST((select (case count(*) when 0 then 1 else count(*) end) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1)) AS decimal(10,1)) as idoszaki_IrattarbanSzazalek,
	   '
SET @sqlcmd = @sqlcmd +
   '
CAST(CAST((select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''09'',''10'',''11'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''09'',''10'',''11''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1))*100/CAST((select (case count(*) when 0 then 1 else count(*) end) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1)) AS decimal(10,1)) as idoszaki_IrattarbanSzazalek_sztornoval,
	   '
SET @sqlcmd = @sqlcmd +
   '
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''30'',''31'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''30'',''31''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as EgyebDb,
	   '
SET @sqlcmd = @sqlcmd +
   '
CAST(CAST((select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot = ''60'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''60'')) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1))*100/CAST((select (case count(*) when 0 then 1 else count(*) end) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''99''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1)) AS decimal(10,1)) as idoszaki_SzereltSzazalek,
	   '
SET @sqlcmd = @sqlcmd +
   '
CAST(CAST((select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot = ''60'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''60'')) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1))*100/CAST((select (case count(*) when 0 then 1 else count(*) end) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1)) AS decimal(10,1)) as idoszaki_SzereltSzazalek_sztornoval,
	   '
SET @sqlcmd = @sqlcmd +
   '
(select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot = ''90'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''90'')) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') as idoszaki_SztornozottDb,
	   '
SET @sqlcmd = @sqlcmd +
   '
CAST(CAST((select count(*) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot = ''90'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''90'')) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1))*100/CAST((select (case count(*) when 0 then 1 else count(*) end) from EREC_UgyUgyiratok where EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id and (EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''07'',''09'',''10'',''11'',''13'',''52'',''55'',''56'',''57'',''60'',''70'',''71'',''90'',''99''))) and EREC_UgyUgyiratok.LetrehozasIdo between ''' + @KezdDate + ''' and ''' + @VegeDate + ''') AS decimal(10,1)) AS decimal(10,1)) as idoszaki_SztornozottSzazalek
	   '
SET @sqlcmd = @sqlcmd +
   '
from 
     EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek '
     
set @sqlcmd = @sqlcmd + N'
   Where EREC_IraIktatoKonyvek.IktatoErkezteto=''I'' and EREC_IraIktatoKonyvek.Ev between YEAR(''' + @KezdDate + ''') and YEAR(''' + @VegeDate + ''')
	and EREC_IraIktatoKonyvek.Org=''' + CAST(@Org as Nvarchar(40)) + '''
'
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
    -- if @Jogosultak = 1 AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
	IF @Jogosultak = 1 AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
	BEGIN
		DECLARE @OjbTip_Id uniqueidentifier;
		select @OjbTip_Id = Id from KRT_Objtipusok where Kod = 'EREC_IraIktatoKonyvek';
		set @sqlcmd = @sqlcmd + N' AND EREC_IraIktatoKonyvek.Id IN 
						(select EREC_IraIktatoKonyvek.Id 
							from EREC_IraIktatoKonyvek
								INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_IraIktatoKonyvek.Id
								INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
						UNION ALL
						SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''', ''EREC_IraIktatoKonyvek'')
							 )';
	END

   SET @sqlcmd = @sqlcmd + @OrderBy

--select @sqlcmd;

  exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end