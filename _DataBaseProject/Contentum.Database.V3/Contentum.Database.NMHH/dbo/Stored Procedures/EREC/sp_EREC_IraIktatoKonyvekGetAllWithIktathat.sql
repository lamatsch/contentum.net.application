﻿CREATE procedure [dbo].[sp_EREC_IraIktatoKonyvekGetAllWithIktathat]
  @Where nvarchar(MAX) = '',
  @Filter_IrattariTetelId uniqueidentifier = null,
  @OrderBy nvarchar(200) = ' order by   EREC_IraIktatoKonyvek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId          UNIQUEIDENTIFIER,
  @FelhasznaloSzervezet_Id UNIQUEIDENTIFIER
as

begin

BEGIN TRY

   set nocount on

   DECLARE @Org uniqueidentifier
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
   if (@Org is null)
   begin
      RAISERROR('[50202]',16,1)
   end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
      

  SET @sqlcmd = 
  'select distinct ' + @LocalTopRow + '
      EREC_IraIktatoKonyvek.Id,
      EREC_IraIktatoKonyvek.Org,
      EREC_IraIktatoKonyvek.Ev,
   EREC_IraIktatoKonyvek.Azonosito,
      EREC_IraIktatoKonyvek.Nev,
      EREC_IraIktatoKonyvek.MegkulJelzes,
      EREC_IraIktatoKonyvek.Iktatohely,
      EREC_IraIktatoKonyvek.UtolsoFoszam,
      EREC_IraIktatoKonyvek.UtolsoSorszam,
      EREC_IraIktatoKonyvek.DefaultIrattariTetelszam,
      EREC_IraIktatoKonyvek.Csoport_Id_Olvaso,
      EREC_IraIktatoKonyvek.Csoport_Id_Tulaj,
      EREC_IraIktatoKonyvek.IktSzamOsztas,
      KRT_KodTarak.Nev as IKT_SZAM_OSZTAS,
      EREC_IraIktatoKonyvek.Titkos,
      EREC_IraIktatoKonyvek.IktatoErkezteto,
      EREC_IraIktatoKonyvek.LezarasDatuma,
      EREC_IraIktatoKonyvek.PostakonyvVevokod,
      EREC_IraIktatoKonyvek.PostakonyvMegallapodasAzon,
	  EREC_IraIktatoKonyvek.HitelesExport_Dok_ID,
      EREC_IraIktatoKonyvek.Ver,
	  EREC_IraIktatoKonyvek.Ujranyitando,
      EREC_IraIktatoKonyvek.Note,
      EREC_IraIktatoKonyvek.Stat_id,
   CONVERT(nvarchar(10), EREC_IraIktatoKonyvek.ErvKezd, 102) as ErvKezd,
   CONVERT(nvarchar(10), EREC_IraIktatoKonyvek.ErvVege, 102) as ErvVege,
      EREC_IraIktatoKonyvek.Letrehozo_id,
      EREC_IraIktatoKonyvek.LetrehozasIdo,
      EREC_IraIktatoKonyvek.Modosito_id,
      EREC_IraIktatoKonyvek.ModositasIdo,
      EREC_IraIktatoKonyvek.Zarolo_id,
      EREC_IraIktatoKonyvek.ZarolasIdo,
      EREC_IraIktatoKonyvek.Tranz_id,
      EREC_IraIktatoKonyvek.UIAccessLog_id,
      EREC_IraIktatoKonyvek.Statusz  
   from 
     dbo.fn_GetAllIktathatoEREC_IraIktatokonyvekByCsoportId(@ExecutorUserId,@FelhasznaloSzervezet_Id) as EREC_IraIktatoKonyvek   
   left join KRT_KodCsoportok on KRT_KodCsoportok.Kod=''IKT_SZAM_OSZTAS''
   left join KRT_KodTarak on KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and EREC_IraIktatoKonyvek.IktSzamOsztas = KRT_KodTarak.Kod and KRT_KodTarak.Org=@Org
'

if @Filter_IrattariTetelId is not null
begin

   SET @sqlcmd = @sqlcmd + ' join EREC_IrattariTetel_Iktatokonyv as EREC_IrattariTetel_Iktatokonyv
      ON EREC_IrattariTetel_Iktatokonyv.Iktatokonyv_Id = EREC_IraIktatoKonyvek.Id 
         and EREC_IrattariTetel_Iktatokonyv.IrattariTetel_Id = @Filter_IrattariTetelId
 		 and getdate() between EREC_IrattariTetel_Iktatokonyv.ervkezd and EREC_IrattariTetel_Iktatokonyv.ervvege
'

end


set @sqlcmd = @sqlcmd + N'
   Where EREC_IraIktatoKonyvek.Org=@Org
'
   
   if @Where is not null and @Where!=''
   begin 
      SET @sqlcmd = @sqlcmd + ' and ' + @Where
   end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy

--print @sqlcmd;

   exec sp_executesql @sqlcmd,N'@ExecutorUserId uniqueidentifier, @FelhasznaloSzervezet_Id uniqueidentifier, @Org uniqueidentifier, @Filter_IrattariTetelId uniqueidentifier',@ExecutorUserId = @ExecutorUserId, @FelhasznaloSzervezet_Id=@FelhasznaloSzervezet_Id, @Org = @Org, @Filter_IrattariTetelId = @Filter_IrattariTetelId;


END TRY
BEGIN CATCH
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
