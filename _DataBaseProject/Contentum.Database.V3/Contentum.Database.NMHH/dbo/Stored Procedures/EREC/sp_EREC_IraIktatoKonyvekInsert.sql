/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_IraIktatoKonyvekInsert')
            and   type = 'P')
   drop procedure sp_EREC_IraIktatoKonyvekInsert
go
*/
create procedure sp_EREC_IraIktatoKonyvekInsert    
                @Id      uniqueidentifier = null,    
                           @Org     uniqueidentifier = null,    
	            @Ev     int,
                @Azonosito     Nvarchar(20)  = null,
                @DefaultIrattariTetelszam     uniqueidentifier  = null,
	            @Nev     Nvarchar(100),
                @MegkulJelzes     Nvarchar(20)  = null,
                @Iktatohely     Nvarchar(100)  = null,
                @KozpontiIktatasJelzo     char(1)  = null,
	            @UtolsoFoszam     int,
                @UtolsoSorszam     int  = null,
                @Csoport_Id_Olvaso     uniqueidentifier  = null,
                @Csoport_Id_Tulaj     uniqueidentifier  = null,
                @IktSzamOsztas     nvarchar(64)  = null,
                @FormatumKod     char(1)  = null,
                @Titkos     char(1)  = null,
                @IktatoErkezteto     char(1)  = null,
                @LezarasDatuma     datetime  = null,
                @PostakonyvVevokod     Nvarchar(100)  = null,
                @PostakonyvMegallapodasAzon     Nvarchar(100)  = null,
                @EFeladoJegyzekUgyfelAdatok     Nvarchar(4000)  = null,
                @HitelesExport_Dok_ID     uniqueidentifier  = null,
                @Ujranyitando     char(1)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,
                @Statusz     char(1)  = null,
                @Terjedelem     Nvarchar(20)  = null,
                @SelejtezesDatuma     datetime  = null,
                @KezelesTipusa     char(1)  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
 IF @ORG is NULL
 BEGIN 
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@Letrehozo_id)
   if (@Org is null)
   begin
   	RAISERROR('[50302]',16,1)
   end
END
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Org is not null
         begin
            SET @insertColumns = @insertColumns + ',Org'
            SET @insertValues = @insertValues + ',@Org'
         end 
       
         if @Ev is not null
         begin
            SET @insertColumns = @insertColumns + ',Ev'
            SET @insertValues = @insertValues + ',@Ev'
         end 
       
         if @Azonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Azonosito'
            SET @insertValues = @insertValues + ',@Azonosito'
         end 
       
         if @DefaultIrattariTetelszam is not null
         begin
            SET @insertColumns = @insertColumns + ',DefaultIrattariTetelszam'
            SET @insertValues = @insertValues + ',@DefaultIrattariTetelszam'
         end 
       
         if @Nev is not null
         begin
            SET @insertColumns = @insertColumns + ',Nev'
            SET @insertValues = @insertValues + ',@Nev'
         end 
       
         if @MegkulJelzes is not null
         begin
            SET @insertColumns = @insertColumns + ',MegkulJelzes'
            SET @insertValues = @insertValues + ',@MegkulJelzes'
         end 
       
         if @Iktatohely is not null
         begin
            SET @insertColumns = @insertColumns + ',Iktatohely'
            SET @insertValues = @insertValues + ',@Iktatohely'
         end 
       
         if @KozpontiIktatasJelzo is not null
         begin
            SET @insertColumns = @insertColumns + ',KozpontiIktatasJelzo'
            SET @insertValues = @insertValues + ',@KozpontiIktatasJelzo'
         end 
       
         if @UtolsoFoszam is not null
         begin
            SET @insertColumns = @insertColumns + ',UtolsoFoszam'
            SET @insertValues = @insertValues + ',@UtolsoFoszam'
         end 
       
         if @UtolsoSorszam is not null
         begin
            SET @insertColumns = @insertColumns + ',UtolsoSorszam'
            SET @insertValues = @insertValues + ',@UtolsoSorszam'
         end 
       
         if @Csoport_Id_Olvaso is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Olvaso'
            SET @insertValues = @insertValues + ',@Csoport_Id_Olvaso'
         end 
       
         if @Csoport_Id_Tulaj is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Tulaj'
            SET @insertValues = @insertValues + ',@Csoport_Id_Tulaj'
         end 
       
         if @IktSzamOsztas is not null
         begin
            SET @insertColumns = @insertColumns + ',IktSzamOsztas'
            SET @insertValues = @insertValues + ',@IktSzamOsztas'
         end 
       
         if @FormatumKod is not null
         begin
            SET @insertColumns = @insertColumns + ',FormatumKod'
            SET @insertValues = @insertValues + ',@FormatumKod'
         end 
       
         if @Titkos is not null
         begin
            SET @insertColumns = @insertColumns + ',Titkos'
            SET @insertValues = @insertValues + ',@Titkos'
         end 
       
         if @IktatoErkezteto is not null
         begin
            SET @insertColumns = @insertColumns + ',IktatoErkezteto'
            SET @insertValues = @insertValues + ',@IktatoErkezteto'
         end 
       
         if @LezarasDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',LezarasDatuma'
            SET @insertValues = @insertValues + ',@LezarasDatuma'
         end 
       
         if @PostakonyvVevokod is not null
         begin
            SET @insertColumns = @insertColumns + ',PostakonyvVevokod'
            SET @insertValues = @insertValues + ',@PostakonyvVevokod'
         end 
       
         if @PostakonyvMegallapodasAzon is not null
         begin
            SET @insertColumns = @insertColumns + ',PostakonyvMegallapodasAzon'
            SET @insertValues = @insertValues + ',@PostakonyvMegallapodasAzon'
         end 
       
         if @EFeladoJegyzekUgyfelAdatok is not null
         begin
            SET @insertColumns = @insertColumns + ',EFeladoJegyzekUgyfelAdatok'
            SET @insertValues = @insertValues + ',@EFeladoJegyzekUgyfelAdatok'
         end 
       
         if @HitelesExport_Dok_ID is not null
         begin
            SET @insertColumns = @insertColumns + ',HitelesExport_Dok_ID'
            SET @insertValues = @insertValues + ',@HitelesExport_Dok_ID'
         end 
       
         if @Ujranyitando is not null
         begin
            SET @insertColumns = @insertColumns + ',Ujranyitando'
            SET @insertValues = @insertValues + ',@Ujranyitando'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end 
       
         if @Statusz is not null
         begin
            SET @insertColumns = @insertColumns + ',Statusz'
            SET @insertValues = @insertValues + ',@Statusz'
         end 
       
         if @Terjedelem is not null
         begin
            SET @insertColumns = @insertColumns + ',Terjedelem'
            SET @insertValues = @insertValues + ',@Terjedelem'
         end 
       
         if @SelejtezesDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',SelejtezesDatuma'
            SET @insertValues = @insertValues + ',@SelejtezesDatuma'
         end 
       
         if @KezelesTipusa is not null
         begin
            SET @insertColumns = @insertColumns + ',KezelesTipusa'
            SET @insertValues = @insertValues + ',@KezelesTipusa'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_IraIktatoKonyvek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Org uniqueidentifier,@Ev int,@Azonosito Nvarchar(20),@DefaultIrattariTetelszam uniqueidentifier,@Nev Nvarchar(100),@MegkulJelzes Nvarchar(20),@Iktatohely Nvarchar(100),@KozpontiIktatasJelzo char(1),@UtolsoFoszam int,@UtolsoSorszam int,@Csoport_Id_Olvaso uniqueidentifier,@Csoport_Id_Tulaj uniqueidentifier,@IktSzamOsztas nvarchar(64),@FormatumKod char(1),@Titkos char(1),@IktatoErkezteto char(1),@LezarasDatuma datetime,@PostakonyvVevokod Nvarchar(100),@PostakonyvMegallapodasAzon Nvarchar(100),@EFeladoJegyzekUgyfelAdatok Nvarchar(4000),@HitelesExport_Dok_ID uniqueidentifier,@Ujranyitando char(1),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@Statusz char(1),@Terjedelem Nvarchar(20),@SelejtezesDatuma datetime,@KezelesTipusa char(1),@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Org = @Org,@Ev = @Ev,@Azonosito = @Azonosito,@DefaultIrattariTetelszam = @DefaultIrattariTetelszam,@Nev = @Nev,@MegkulJelzes = @MegkulJelzes,@Iktatohely = @Iktatohely,@KozpontiIktatasJelzo = @KozpontiIktatasJelzo,@UtolsoFoszam = @UtolsoFoszam,@UtolsoSorszam = @UtolsoSorszam,@Csoport_Id_Olvaso = @Csoport_Id_Olvaso,@Csoport_Id_Tulaj = @Csoport_Id_Tulaj,@IktSzamOsztas = @IktSzamOsztas,@FormatumKod = @FormatumKod,@Titkos = @Titkos,@IktatoErkezteto = @IktatoErkezteto,@LezarasDatuma = @LezarasDatuma,@PostakonyvVevokod = @PostakonyvVevokod,@PostakonyvMegallapodasAzon = @PostakonyvMegallapodasAzon,@EFeladoJegyzekUgyfelAdatok = @EFeladoJegyzekUgyfelAdatok,@HitelesExport_Dok_ID = @HitelesExport_Dok_ID,@Ujranyitando = @Ujranyitando,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id,@Statusz = @Statusz,@Terjedelem = @Terjedelem,@SelejtezesDatuma = @SelejtezesDatuma,@KezelesTipusa = @KezelesTipusa ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_IraIktatoKonyvek',@ResultUid
					,'EREC_IraIktatoKonyvekHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
go

/* használt errorcode-ok:
	50301:
	50302: 'Org' oszlop értéke nem lehet NULL
*/
