﻿CREATE procedure [dbo].[sp_EREC_IraIktatoKonyvek_MasolasKovetkezoEvre]
	@Id				uniqueidentifier,
	@ExecutorUserId		uniqueidentifier,
	@IktatoErkezteto	char(1) = null,
	@ResultUid	uniqueidentifier output
as

BEGIN TRY
	set nocount on
	
 	/* dinamikus history log összeállításhoz */
	declare @row_ids varchar(MAX)

	/* hibakódok IktatoErkezteto szurés szerint */
	declare @errorCode_Ervenytelen varchar(10)
	declare @errorCode_NincsLezarva varchar(10)
	declare @errorCode_MarLetezik varchar(10)
	declare @errorCode_NemMegfeleloTipus varchar(10)
	declare @errorCode_MultbanHozandoLetre varchar(10)
	if (@IktatoErkezteto = 'I')
	begin
		set @errorCode_Ervenytelen = '[52942]'
		set @errorCode_NincsLezarva = '[52943]'
		set @errorCode_MarLetezik = '[52948]'
		set @errorCode_NemMegfeleloTipus = '[52946]'
		set @errorCode_MultbanHozandoLetre = '[52950]'
	end
	else if (@IktatoErkezteto = 'E')
	begin
		set @errorCode_Ervenytelen = '[52922]'
		set @errorCode_NincsLezarva = '[52923]'
		set @errorCode_MarLetezik = '[52928]'
		set @errorCode_NemMegfeleloTipus = '[52926]'
		set @errorCode_MultbanHozandoLetre = '[52930]'
	end
	else
	begin
		set @errorCode_Ervenytelen = '[52902]'
		set @errorCode_NincsLezarva = '[52903]'
		set @errorCode_MarLetezik = '[52908]'
		set @errorCode_MultbanHozandoLetre = '[52910]'
	end

	declare @execTime datetime;
	declare @currentYear int;

	set @execTime = getdate();
	set @currentYear = datepart(yyyy, @execTime);

	declare @IktatoKonyv_Id_Original uniqueidentifier;
	declare @LezarasDatuma datetime;
	declare @ErvKezd datetime;
	declare @ErvVege datetime;
	declare @Ev int;
	declare @IETipus char(1);
	-- UK: Org, Nev
	declare @Org uniqueidentifier;
	declare @Nev nvarchar(100);

	select @IktatoKonyv_Id_Original = Id, @LezarasDatuma = LezarasDatuma
		, @ErvKezd = ErvKezd, @ErvVege = ErvVege
		, @Org = Org, @Nev = Nev, @Ev = Ev, @IETipus = IktatoErkezteto
		from EREC_IraIktatoKonyvek where Id = @Id
		--and (@IktatoErkezteto is null or (IktatoErkezteto = @IktatoErkezteto))

	-- Ellenorzés
	if (@IktatoKonyv_Id_Original is null)
	begin
		RAISERROR('[50101]',16,1) -- a rekord lekérése sikertelen
	end

	if not (@execTime between @ervKezd and @ErvVege)
	begin
		select @IktatoKonyv_Id_Original as Id -- a hibát kiváltó iktatókönyv Id visszaadása, pl. a sorok megjelöléséhez
		RAISERROR(@errorCode_Ervenytelen,16,1) -- már érvénytelenítve van a rekord
	end

	if (@LezarasDatuma is null or @Ev > @currentYear)
	begin
		select @IktatoKonyv_Id_Original as Id -- a hibát kiváltó iktatókönyv Id visszaadása, pl. a sorok megjelöléséhez
		-- Az iktatókönyv még nincs lezárva. Következo évre csak
		-- lezárt iktatókönyv alapján lehet új iktatókönyvet létrehozni.
		RAISERROR(@errorCode_NincsLezarva,16,1)
	end

	if (@Ev < @currentYear - 1)
	begin
		select @IktatoKonyv_Id_Original as Id -- a hibát kiváltó iktatókönyv Id visszaadása, pl. a sorok megjelöléséhez
		-- A létrehozandó iktatókönyv egy a jelenlegit megelozo évre vonatkozna, ezért a másolást a következo évre nem lehet végrehajtan! 
		RAISERROR(@errorCode_MultbanHozandoLetre,16,1)
	end

	if exists(select Id from EREC_IraIktatoKonyvek
		where Org = @Org
			and Nev = @Nev
			and getdate() between ErvKezd and ErvVege
			and Ev = @Ev + 1)
	begin
		select @IktatoKonyv_Id_Original as Id -- a hibát kiváltó iktatókönyv Id visszaadása, pl. a sorok megjelöléséhez
		-- Hiba az iktatókönyv létrehozása során: már létezik az iktatókönyv!
		RAISERROR(@errorCode_MarLetezik,16,1)
	end

	if (@IktatoErkezteto is not null and @IETipus != @IktatoErkezteto)
	begin
		select @IktatoKonyv_Id_Original as Id -- a hibát kiváltó iktatókönyv Id visszaadása, pl. a sorok megjelöléséhez
		-- Nem iktatókönyv/nem érkeztetokönyv
		RAISERROR(@errorCode_NemMegfeleloTipus,16,1)
	end

	/****** Következo évre létrehozás *****/
		-- tábla szerkezet másolása
		select top 0 * into #tmp_EREC_IktatoKonyvek_New from EREC_IraIktatoKonyvek

	-- EREC_IraIktatoKonyvek INSERT

		-- iktatókönyvek létrehozása a lezárt mintájára, de a következo évre
		declare @IktatoKonyv_Id_Copy uniqueidentifier
		declare @insertedRowId table (Id uniqueidentifier)

		insert into #tmp_EREC_IktatoKonyvek_New
			select * from EREC_IraIktatoKonyvek 
			where Id = @IktatoKonyv_Id_Original

		-- BUG_6785
		declare @azonositoHossz int
		SET @azonositoHossz = IsNull(dbo.fn_GetKRT_ParameterekErtek('IKTATOKONYV_AZONOSITO_HOSSZ', @Org),0)
		declare @len int 
		-- BUG_12083
		select @len = iif(@azonositoHossz  > len(max(convert(int, tbl.Azonosito)) + 1), @azonositoHossz-len(max(convert(int, tbl.Azonosito)) + 1), 0)
							from (select IktatoErkezteto, Azonosito, ErvKezd, ErvVege from EREC_IraIktatoKonyvek
							UNION
						select IktatoErkezteto, Azonosito, ErvKezd, ErvVege from #tmp_EREC_IktatoKonyvek_New) as tbl
					where --tbl.IktatoErkezteto = #tmp_EREC_IktatoKonyvek_New.IktatoErkezteto
					-- BUG_12083
					--and 
					--PATINDEX('[0-9][0-9][0-9][0-9][0-9]',tbl.Azonosito)  > 0
					--and 
					getdate() between tbl.ErvKezd and tbl.ErvVege

		update #tmp_EREC_IktatoKonyvek_New
			set Id = newid(), Ev = Ev + 1, UtolsoFoszam=0, Ver = 0, LezarasDatuma = null,
				Modosito_id = null, ModositasIdo = null,
				Letrehozo_id = @ExecutorUserId, LetrehozasIdo = @execTime,
				ErvKezd = @execTime,
				Statusz = '1', -- BLG_352
				-- BUG_6785
				--Azonosito = (select isnull((select replicate('0', 5 - len(convert(nvarchar(5), convert(int, Max(tbl.Azonosito)) + 1))) + convert(nvarchar(5), convert(int, Max(tbl.Azonosito)) + 1)
				--	from (select IktatoErkezteto, Azonosito, ErvKezd, ErvVege from EREC_IraIktatoKonyvek
				--			UNION
				--		select IktatoErkezteto, Azonosito, ErvKezd, ErvVege from #tmp_EREC_IktatoKonyvek_New) as tbl
				--	where tbl.IktatoErkezteto = #tmp_EREC_IktatoKonyvek_New.IktatoErkezteto
				--	and PATINDEX('[0-9][0-9][0-9][0-9][0-9]',tbl.Azonosito)  > 0
				--	and getdate() between tbl.ErvKezd and tbl.ErvVege), '00001'))
				-- BUG_12083
				Azonosito = (select isnull((select replicate('0', @len) + convert(nvarchar(20),max(convert(int, tbl.Azonosito)) + 1)
					from (select IktatoErkezteto, Azonosito, ErvKezd, ErvVege from EREC_IraIktatoKonyvek
							UNION
						select IktatoErkezteto, Azonosito, ErvKezd, ErvVege from #tmp_EREC_IktatoKonyvek_New) as tbl
					where tbl.IktatoErkezteto = #tmp_EREC_IktatoKonyvek_New.IktatoErkezteto
					-- BUG_12083
					--and PATINDEX('[0-9][0-9][0-9][0-9][0-9]',tbl.Azonosito)  > 0
					and getdate() between tbl.ErvKezd and tbl.ErvVege), '1')),
				HitelesExport_Dok_ID = null,
				Note = null,
				ZarolasIdo = null,
				Zarolo_id = null

		insert into EREC_IraIktatoKonyvek output inserted.Id into @insertedRowId
			select * from #tmp_EREC_IktatoKonyvek_New;

		set @IktatoKonyv_Id_Copy = (select Id from @insertedRowId)
		delete from @insertedRowId
		
		drop table #tmp_EREC_IktatoKonyvek_New
		
		/*** EREC_IraIktatoKonyvek history log ***/
		exec sp_LogRecordToHistory 'EREC_IraIktatoKonyvek',@IktatoKonyv_Id_Copy
					,'EREC_IraIktatoKonyvekHistory',0,@ExecutorUserId,@execTime;

		/*** EREC_Irat_Iktatokonyvei rekordok másolása ***/
		select * into #tmp_EREC_Irat_IktatoKonyvei_New from EREC_Irat_Iktatokonyvei
			where IraIktatokonyv_Id = @IktatoKonyv_Id_Original
			and getdate() between ErvKezd and ErvVege

		update #tmp_EREC_Irat_IktatoKonyvei_New
			set Id = newid(), IraIktatokonyv_Id = @IktatoKonyv_Id_Copy, Ver = 0,
			Modosito_id = null, ModositasIdo = null,
			Letrehozo_id = @ExecutorUserId, LetrehozasIdo = @execTime,
			ErvKezd = @execTime

		insert into EREC_Irat_Iktatokonyvei
			select * from #tmp_EREC_Irat_IktatoKonyvei_New;


		/*** EREC_Irat_IktatokonyveiHistory log ***/
		set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from #tmp_EREC_Irat_Iktatokonyvei_New FOR XML PATH('')),1, 2, '')
		if @row_ids is not null
			set @row_ids = @row_ids + '''';

		exec sp_LogRecordsToHistory_Tomeges
		 @TableName = 'EREC_Irat_Iktatokonyvei'
		,@Row_Ids = @row_ids
		,@Muvelet = 0
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @execTime

		drop table #tmp_EREC_Irat_IktatoKonyvei_New


		/*** EREC_IrattariTetel_Iktatokonyv rekordok másolása ***/
		select * into #tmp_EREC_IrattariTetel_IktatoKonyv_New from EREC_IrattariTetel_Iktatokonyv
			where Iktatokonyv_Id = @IktatoKonyv_Id_Original
			and getdate() between ErvKezd and ErvVege

		update #tmp_EREC_IrattariTetel_IktatoKonyv_New
			set Id = newid(), Iktatokonyv_Id = @IktatoKonyv_Id_Copy, Ver = 0,
			Modosito_id = null, ModositasIdo = null,
			Letrehozo_id = @ExecutorUserId, LetrehozasIdo = @execTime,
			ErvKezd = @execTime

		insert into EREC_IrattariTetel_IktatoKonyv
			select * from #tmp_EREC_IrattariTetel_IktatoKonyv_New;

		/*** EREC_IrattariTetel_IktatoKonyvHistory log ***/
		set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from #tmp_EREC_IrattariTetel_IktatoKonyv_New FOR XML PATH('')),1, 2, '')
		if @row_ids is not null
			set @row_ids = @row_ids + '''';

		exec sp_LogRecordsToHistory_Tomeges
		 @TableName = 'EREC_IrattariTetel_IktatoKonyv'
		,@Row_Ids = @row_ids
		,@Muvelet = 0
		,@Vegrehajto_Id = @ExecutorUserId
		,@VegrehajtasIdo = @execTime

		drop table #tmp_EREC_IrattariTetel_IktatoKonyv_New

		/*** KRT_Jogosultak rekordok másolása ***/
		-- nincs history, ezért nem kell cursor sem
		select * into #tmp_KRT_Jogosultak_New from KRT_Jogosultak
			where Obj_Id = @IktatoKonyv_Id_Original
			and getdate() between ErvKezd and ErvVege

		update #tmp_KRT_Jogosultak_New
			set Id = newid(), Obj_Id = @IktatoKonyv_Id_Copy, Ver = 0,
			Modosito_id = null, ModositasIdo = null,
			Letrehozo_id = @ExecutorUserId, LetrehozasIdo = @execTime,
			ErvKezd = @execTime

		insert into KRT_Jogosultak
			select * from #tmp_KRT_Jogosultak_New;

--		Nem létezik KRT_JogosultakHistory !!!
--		/*** KRT_JogosultakHistory log ***/

		drop table #tmp_KRT_Jogosultak_New

		set @ResultUid = @IktatoKonyv_Id_Copy

END TRY
BEGIN CATCH

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH