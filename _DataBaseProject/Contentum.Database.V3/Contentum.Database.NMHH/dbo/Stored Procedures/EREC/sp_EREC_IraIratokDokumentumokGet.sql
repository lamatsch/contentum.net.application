﻿create procedure [dbo].[sp_EREC_IraIratokDokumentumokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IraIratokDokumentumok.Id,
	   EREC_IraIratokDokumentumok.Dokumentum_Id,
	   EREC_IraIratokDokumentumok.IraIrat_Id,
	   EREC_IraIratokDokumentumok.Leiras,
	   EREC_IraIratokDokumentumok.Lapszam,
	   EREC_IraIratokDokumentumok.BarCode,
	   EREC_IraIratokDokumentumok.Forras,
	   EREC_IraIratokDokumentumok.Formatum,
	   EREC_IraIratokDokumentumok.Vonalkodozas,
	   EREC_IraIratokDokumentumok.DokumentumSzerep,
	   EREC_IraIratokDokumentumok.Ver,
	   EREC_IraIratokDokumentumok.Note,
	   EREC_IraIratokDokumentumok.Stat_id,
	   EREC_IraIratokDokumentumok.ErvKezd,
	   EREC_IraIratokDokumentumok.ErvVege,
	   EREC_IraIratokDokumentumok.Letrehozo_id,
	   EREC_IraIratokDokumentumok.LetrehozasIdo,
	   EREC_IraIratokDokumentumok.Modosito_id,
	   EREC_IraIratokDokumentumok.ModositasIdo,
	   EREC_IraIratokDokumentumok.Zarolo_id,
	   EREC_IraIratokDokumentumok.ZarolasIdo,
	   EREC_IraIratokDokumentumok.Tranz_id,
	   EREC_IraIratokDokumentumok.UIAccessLog_id
	   from 
		 EREC_IraIratokDokumentumok as EREC_IraIratokDokumentumok 
	   where
		 EREC_IraIratokDokumentumok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end