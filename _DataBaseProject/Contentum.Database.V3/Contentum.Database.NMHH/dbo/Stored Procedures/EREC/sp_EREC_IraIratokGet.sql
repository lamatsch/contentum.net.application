
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_IraIratokGet')
            and   type = 'P')
   drop procedure sp_EREC_IraIratokGet
go
*/
create procedure sp_EREC_IraIratokGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IraIratok.Id,
	   EREC_IraIratok.PostazasIranya,
	   EREC_IraIratok.Alszam,
	   EREC_IraIratok.Sorszam,
	   EREC_IraIratok.UtolsoSorszam,
	   EREC_IraIratok.UgyUgyIratDarab_Id,
	   EREC_IraIratok.Kategoria,
	   EREC_IraIratok.HivatkozasiSzam,
	   EREC_IraIratok.IktatasDatuma,
	   EREC_IraIratok.ExpedialasDatuma,
	   EREC_IraIratok.ExpedialasModja,
	   EREC_IraIratok.FelhasznaloCsoport_Id_Expedial,
	   EREC_IraIratok.Targy,
	   EREC_IraIratok.Jelleg,
	   EREC_IraIratok.SztornirozasDat,
	   EREC_IraIratok.FelhasznaloCsoport_Id_Iktato,
	   EREC_IraIratok.FelhasznaloCsoport_Id_Kiadmany,
	   EREC_IraIratok.UgyintezesAlapja,
	   EREC_IraIratok.AdathordozoTipusa,
	   EREC_IraIratok.Csoport_Id_Felelos,
	   EREC_IraIratok.Csoport_Id_Ugyfelelos,
	   EREC_IraIratok.FelhasznaloCsoport_Id_Orzo,
	   EREC_IraIratok.KiadmanyozniKell,
	   EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez,
	   EREC_IraIratok.Hatarido,
	   EREC_IraIratok.MegorzesiIdo,
	   EREC_IraIratok.IratFajta,
	   EREC_IraIratok.Irattipus,
	   EREC_IraIratok.KuldKuldemenyek_Id,
	   EREC_IraIratok.Allapot,
	   EREC_IraIratok.Azonosito,
	   EREC_IraIratok.GeneraltTargy,
	   EREC_IraIratok.IratMetaDef_Id,
	   EREC_IraIratok.IrattarbaKuldDatuma ,
	   EREC_IraIratok.IrattarbaVetelDat,
	   EREC_IraIratok.Ugyirat_Id,
	   EREC_IraIratok.Minosites,
	   EREC_IraIratok.Elintezett,
	   EREC_IraIratok.IratHatasaUgyintezesre,
	   EREC_IraIratok.FelfuggesztesOka,
	   EREC_IraIratok.LezarasOka,
	   EREC_IraIratok.Munkaallomas,
	   EREC_IraIratok.UgyintezesModja,
	   EREC_IraIratok.IntezesIdopontja,
	   EREC_IraIratok.IntezesiIdo,
	   EREC_IraIratok.IntezesiIdoegyseg,
	   EREC_IraIratok.UzemzavarKezdete,
	   EREC_IraIratok.UzemzavarVege,
	   EREC_IraIratok.UzemzavarIgazoloIratSzama,
	   EREC_IraIratok.FelfuggesztettNapokSzama,
	   EREC_IraIratok.VisszafizetesJogcime,
	   EREC_IraIratok.VisszafizetesOsszege,
	   EREC_IraIratok.VisszafizetesHatarozatSzama,
	   EREC_IraIratok.Partner_Id_VisszafizetesCimzet,
	   EREC_IraIratok.Partner_Nev_VisszafizetCimzett,
	   EREC_IraIratok.VisszafizetesHatarido,
	   EREC_IraIratok.Minosito,
	   EREC_IraIratok.MinositesErvenyessegiIdeje,
	   EREC_IraIratok.TerjedelemMennyiseg,
	   EREC_IraIratok.TerjedelemMennyisegiEgyseg,
	   EREC_IraIratok.TerjedelemMegjegyzes,
	   EREC_IraIratok.SelejtezesDat,
	   EREC_IraIratok.FelhCsoport_Id_Selejtezo,
	   EREC_IraIratok.LeveltariAtvevoNeve,
	   EREC_IraIratok.ModositasErvenyessegKezdete,
	   EREC_IraIratok.MegszuntetoHatarozat,
	   EREC_IraIratok.FelulvizsgalatDatuma,
	   EREC_IraIratok.Felulvizsgalo_id,
	   EREC_IraIratok.MinositesFelulvizsgalatEredm,
	   EREC_IraIratok.UgyintezesKezdoDatuma,
	   EREC_IraIratok.EljarasiSzakasz,
	   EREC_IraIratok.HatralevoNapok,
	   EREC_IraIratok.HatralevoMunkaNapok,
	   EREC_IraIratok.Ugy_Fajtaja,
	   EREC_IraIratok.MinositoSzervezet,
	   EREC_IraIratok.MinositesFelulvizsgalatBizTag,
	   EREC_IraIratok.Ver,
	   EREC_IraIratok.Note,
	   EREC_IraIratok.Stat_id,
	   EREC_IraIratok.ErvKezd,
	   EREC_IraIratok.ErvVege,
	   EREC_IraIratok.Letrehozo_id,
	   EREC_IraIratok.LetrehozasIdo,
	   EREC_IraIratok.Modosito_id,
	   EREC_IraIratok.ModositasIdo,
	   EREC_IraIratok.Zarolo_id,
	   EREC_IraIratok.ZarolasIdo,
	   EREC_IraIratok.Tranz_id,
	   EREC_IraIratok.UIAccessLog_id
	   from 
		 EREC_IraIratok as EREC_IraIratok 
	   where
		 EREC_IraIratok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
