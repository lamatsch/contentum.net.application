﻿ --set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--if exists (select 1 from  sysobjects
--           where  id = object_id('sp_EREC_IraIratokGetAllWithExtension')
--             and  type in ('P'))
--   drop procedure sp_EREC_IraIratokGetAllWithExtension
--go

create PROCEDURE [dbo].[sp_EREC_IraIratokGetAllWithExtension]
    @Where                            NVARCHAR(MAX)    = '',        -- általános szűrő feltétel, több táblát is érinthet - ez alapján a JOIN összeállítás dinamikus, USER állítja össze
    @OrderBy                          NVARCHAR(400)    = ' order by EREC_IraIratok.LetrehozasIdo',      -- rendezési kulcs, több táblát is érinthet - ez alapján a JOIN összeállítás dinamikus, USER állítja össze
    @TopRow                           NVARCHAR(5)      = '',        -- TOP paraméter pl SSRS lekérdezésekhez 
    @Where_Alairok                    NVARCHAR(MAX)    = '',        -- speciális szűrő feltétel, ez erősen beszűkíti a kezelendő Iratok-at
    @Where_Pld                        NVARCHAR(MAX)    = '',        -- speciális szűrő feltétel, ez erősen beszűkíti a kezelendő Iratok-at
    @ObjektumTargyszavai_ObjIdFilter  nvarchar(MAX)    = '',        -- objektum tárgyszó keresés ???, ez erősen beszűkíti a kezelendő Iratok-at
    @Where_EREC_IraOnkormAdatok       nvarchar(MAX)    = '',        -- speciális szűrő feltétel, ez erősen beszűkíti a kezelendő Iratok-at
    @ExecutorUserId                   UNIQUEIDENTIFIER,             -- a lekérdező felhasználó
    @FelhasznaloSzervezet_Id          UNIQUEIDENTIFIER = null,      -- a lekérdező felhasználó szervezete; nem azonos ORG-gal; scanneles miatt lehet null !!!
    @Jogosultak                       char(1)          = '0',       -- jelzi, hogy vizsgálunk-e jogosultságot
    @ColumnNames                      nvarchar(MAX)    = null,      -- tárgyszavak/metaadatok, mint oszlopok szűrése (*: mind)
    @pageNumber                       int              = 0,         -- hányadik lapon vagyok - OUT paraméter is
    @pageSize                         int              = -1,        -- hány sor jelenik meg egy lapon - OUT paraméter is
    @SelectedRowId                    uniqueidentifier = NULL,      -- direkt sorra ugrás esetén az EREC_IraIratok.ID
    @AlSzervezetId                    UNIQUEIDENTIFIER = null,      -- ???
    @IsUgyiratTerkepQuery             char(1)          = '0'        -- Ugyirat - Ugyiratdarab - Irat - Iratpéldány FA
	--- új paraméterek                          
	,@Csoporttagokkal                  bit              = 1         -- Csak a saját jogon láthatja az iratokat: 0; vagy a csoporttagsága jogán is: 1 (default) 
	,@CsakAktivIrat                    bit              = 1         -- Csak az AKTIV Iratokat láttatjuk ( a fix 'erős' szűrő ): 1; vagy minden irat: 0 (default)
	,@CsatolmanySzures                 int              = 0         -- 0 (default) – nincs szűrés, 1 – csak a csatolmány nélküliek, 2 – csak a csatolmánnyal rendelkezők jelenjenek meg

AS 
BEGIN

  BEGIN TRY

    set nocount on

	--- időszakaszok mérésére; GUI teszt alatt ki kell kapcsolni
---    create table #timeTMP (DESC1 char(100), TIME1 DateTime);                     --- időszakaszok mérésére
---    insert into #timeTMP select '00 - INIT beállítások, sqlcmd felépítése',current_timestamp;      --- időszakaszok mérésére

    DECLARE @Org           uniqueidentifier;                           -- ORG beállítása szűréshez
    DECLARE @FelhasznaloId UNIQUEIDENTIFIER,                         -- @AlSzervezetId kezelése esetén módosul a felhasználó/szervezet
            @SzervezetId   UNIQUEIDENTIFIER;
    DECLARE @LocalTopRow   nvarchar(10),                               -- ??? 
            @firstRow      int,                                        -- ???
            @lastRow       int;                                        -- ???   
    DECLARE @sqlcmd        nvarchar(MAX);                             -- dinamikus SQL 
	DECLARE @sqlOrderBy    nvarchar(MAX),                             -- dinamikus sql string: @OrderBy-hoz szükséges JOIN -ok 
	        @sqlwhere      nvarchar(MAX),                             -- dinamikus sql string: @where-hez szükséges JOIN -ok
	        @sqlCTL        nvarchar(MAX);                             -- CTL dinamikus felépítése
	DECLARE @IraIratok     nvarchar(MAX);                             -- adatforrás neve @CsakAktivIrat függvényében - egyelőre NEM használt

    --- ORG beállítása / ellenőrzése 
    SET @Org = [dbo].fn_GetOrgByFelhasznaloId(@ExecutorUserId)
    if (@Org is null)
      RAISERROR('[50202]',16,1);                                    -- bye, bye

	--- kezelt felhasználó/szervezet beállítása @AlSzervezetId függvényében
    if @AlSzervezetId IS NULL
      begin
        SET @FelhasznaloId = @ExecutorUserId
        SET @SzervezetId = @FelhasznaloSzervezet_Id
      end
    else
      begin
        SET @FelhasznaloId = [dbo].fn_GetLeader(@AlSzervezetId)
        if @FelhasznaloId is NULL                                   -- Az alszervezetnek nincsen vezetoje
          RAISERROR('[53203]',16,1);                                -- bye, bye
           
        SET @SzervezetId = @AlSzervezetId
      end;
   
    --- lapozás kezelés beállítások
    if (@TopRow = '' or @TopRow = '0') 
      SET @LocalTopRow = '';
    else
      begin
        if (@pageSize > @TopRow)
          SET @pageSize = @TopRow;
        SET @LocalTopRow = ' TOP ' + @TopRow;
      end;
                  
    if (@pageSize > 0 and @pageNumber > -1)
      begin
        SET @firstRow = (@pageNumber)*@pageSize + 1;
        SET @lastRow = @firstRow + @pageSize - 1;
      end
    else 
	  begin
        if (@TopRow = '' or @TopRow = '0')
          begin
            SET @firstRow = 1;
            SET @lastRow = ISNULL((SELECT TOP (1) Ertek FROM KRT_Parameterek WHERE Nev = 'LISTA_ELEMSZAM_MAX'),1000);
          end 
        else
          begin
            SET @firstRow = 1;
            SET @lastRow = @TopRow;
          end   
      end;

    SET @sqlcmd = ''+char(13)+char(13);   
    /************************************************************
    * Szűrési tábla ( #filter ) összeállítása                   *
    ************************************************************/
    --- Ha scanneles uzemmodban vagyunk, nincs szervezet es nincs jogosultsagvizsgalat
    --- Ha van jogosultsagvizsgalat, de nincs szervezet, akkor nem lathat semmit!
	---- Egyelőre NEM látom, hogy a fenti két mondatnak való megfelelés miként van kódolva 
	---- - talán #CsoportTagokAll összeállításával és szűrésével !? 
	---- - a scan esetében pedig @SelectedRowId -val !?
	----
    
    /* --- NEM használjuk sehol sem a kódban, így kiemeltem
    DECLARE @ObjTipId uniqueidentifier;
    select @ObjTipId = Id from KRT_Objtipusok where Kod = 'EREC_IraIratok';
    --- */

	--- akkor rakjuk össze a dinamikus SQL-t --- miért is alkalmazunk dinamikus SQL -T ???
	SET @sqlcmd = ''+char(13)+char(13);   
---    SET @sqlcmd += 'insert into #timeTMP select ''10 - Local TMP táblák létrehozása'',current_timestamp; '+char(13)+char(13);              --- időszakaszok mérésére

    --- Iratok tábla előszűrése, hogy ne az egészet kelljen #CsoportTagokAll-lal összevetni
	--- Alapból szűrjük ORG-ra -> így az később elhagyható ( bár szerintem már #CsoportTagokAll beállítása is biztosítja az ORG szűrést )
	--- !!! Csak akkor van értelme, ha valami fix erős szűrés van megadva az Iratok táblára: itt most az AKTIV (ALLAPOT) oszlopok szűrésével csökkentjük a tételszámot
	SET @sqlcmd += 'create table #IraIratokFiltered(Id uniqueidentifier,Ugyirat_Id uniqueidentifier); '+char(13);
    
    SET @sqlcmd += 'insert into #IraIratokFiltered
                    select EREC_IraIratok.Id, EREC_IraIratok.Ugyirat_Id
                      from [dbo].EREC_IraIratok '+char(13);
    
       
        
                                                                       
	  if charindex( lower('EREC_UgyUgyiratok'), lower(@where) ) > 0       -- egyébként csak akkor kapcsoljuk be, ha szűrés van rá
        SET @sqlcmd += 'LEFT JOIN [dbo].EREC_UgyUgyiratok as EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id '+char(13);
    -- @ORG korai szűrése miatt mindenképpen kellenek ezek a kapcsolatok 
    SET @sqlcmd += 'LEFT JOIN [dbo].EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id 
                   inner join [dbo].EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek ON EREC_IraIktatoKonyvek.Id = EREC_UgyUgyiratdarabok.IraIktatokonyv_Id and EREC_IraIktatoKonyvek.Org=@Org '+char(13);                   
    if charindex( lower('EREC_KuldKuldemenyek'), lower(@where) ) > 0       -- ezt csak akkor kapcsoljuk be, ha szűrés van rá
      SET @sqlcmd += 'LEFT JOIN [dbo].EREC_KuldKuldemenyek AS EREC_KuldKuldemenyek ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id '+char(13);

    --- a speciális szűrők kezelését már itt be tudjuk építeni, hiszen mindegyik EREC_IraIratok.ID-t szűr és jelentősen leszűkíti az Iratok forrást, viszont innentől egységes a kezelése
    if (@Where_Alairok is not null and @Where_Alairok != '')
      SET @sqlcmd += ' INNER JOIN (select Obj_Id from [dbo].EREC_IratAlairok where ' + @Where_Alairok + ') as EREC_IratAlairok ON EREC_IratAlairok.Obj_Id = EREC_IraIratok.id '+char(13);
    if (@Where_Pld is not null and @Where_Pld != '')
	-- BUG_1574
      --SET @sqlcmd += ' INNER JOIN (select EREC_PldIratPeldanyok.IraIrat_Id from [dbo].EREC_PldIratPeldanyok where ' + @Where_Pld + ') as EREC_PldIratPeldanyok ON EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.id '+char(13);
      SET @sqlcmd += ' INNER JOIN (select distinct EREC_PldIratPeldanyok.IraIrat_Id from [dbo].EREC_PldIratPeldanyok where ' + @Where_Pld + ') as EREC_PldIratPeldanyok ON EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.id '+char(13);
    if @ObjektumTargyszavai_ObjIdFilter is not null and @ObjektumTargyszavai_ObjIdFilter! = ''
      SET @sqlcmd += ' INNER JOIN (select distinct EREC_ObjektumTargyszavai.Obj_Id from [dbo].EREC_ObjektumTargyszavai 
	                                 where  EREC_ObjektumTargyszavai.Obj_Id in (' + @ObjektumTargyszavai_ObjIdFilter + ') 
									   AND getdate() BETWEEN EREC_ObjektumTargyszavai.ErvKezd AND EREC_ObjektumTargyszavai.ErvVege
							      ) as EREC_ObjektumTargyszavai on EREC_ObjektumTargyszavai.Obj_Id = EREC_IraIratok.id '+char(13);
    if (@Where_EREC_IraOnkormAdatok is not null and @Where_EREC_IraOnkormAdatok != '')
      SET @sqlcmd += ' INNER JOIN (select EREC_IraOnkormAdatok.IraIratok_Id from [dbo].EREC_IraOnkormAdatok where ' + @Where_EREC_IraOnkormAdatok + ') as EREC_IraOnkormAdatok on EREC_IraOnkormAdatok.IraIratok_Id  = EREC_IraIratok.id '+char(13);

    ---	
    SET @sqlcmd += ' WHERE 1 = 1 '+char(13);
    if @CsakAktivIrat = 1
		BEGIN
			SET @sqlcmd +=[dbo].[fn_GetAktivFilterValue] ( 'IR')
			SET @sqlcmd += char(13);
		END
	--- #1631 - 2018.07.11., BogI
	--- ezzel a fejlesztéssel egyidőben kivettük az eljárásból a "bizalmas iratok Őrző szerinti szűrését" (@iratok_executor)
	if IsNull(@CsatolmanySzures,0) = 1
	  SET @sqlcmd += ' and NOT exists ( select 1 from [dbo].[EREC_Csatolmanyok] 
                                         where [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id
                                           and getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege) '+char(13);
	if IsNull(@CsatolmanySzures,0) = 2
	  SET @sqlcmd += ' and exists ( select 1 from [dbo].[EREC_Csatolmanyok] 
                                         where [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id
                                           and getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege) '+char(13);

	--- akkor már az általános @where feltételt is értékeljük ki itt !
    if @Where is not null and @Where!=''
      SET @sqlcmd += ' and ' + @Where + '; '+char(13)+char(13);

    --- Ez az index drága: ha itt hozom létre, akkor is; ha a CREATE-ben Primary Key, akkor is - NE használjuk
    -- SET @sqlcmd += 'create index #ix_IraIratokFiltered on #IraIratokFiltered (Id); '+char(13)+char(13);
    
    --- jogosult felhasználók TMP
    SET @sqlcmd += 'create table #CsoportTagokAll(Id uniqueidentifier); '+char(13);
    
    if @Csoporttagokkal = 1
      SET @sqlcmd += 'insert into #CsoportTagokAll 
	                  select distinct Id FROM [dbo].fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId); '+char(13)+char(13);
    else              --- Peti módszerét alkalmazzuk, nehogy kikapcsoljuk a @SzervezetID keresésben rejtett ellenőrzést
	  begin           --- ill. @FelhasznaloId is kieshet - bár elenyésző eséllyel
        SET @sqlcmd += 'insert into #CsoportTagokAll 
	                    select distinct ID FROM [dbo].fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId)
						 where ID = @FelhasznaloId; '+char(13)+char(13);
        SET @sqlcmd += 'SET @FelhasznaloId = (select ID from #CsoportTagokAll ); '+char(13)+char(13);                      -- csak 1 eleme lehet vagy Null
      end;
  
    --- jogosult objektumok CTL -ben ( TMP lassabbnak bizonyult - bár elég nagy a tábla !? )
	--- Kezi és Tipus az Irat - Ugyirat kapcsolat alapján történő gyűjtésnél kell
	SET @sqlCTL = '';
    if @Csoporttagokkal = 1
  
	  SET @sqlCTL += '; with Jogosult_objektumok as 
	                    (select distinct KRT_Jogosultak.Obj_Id, KRT_Jogosultak.Kezi, KRT_Jogosultak.Tipus       
	                        from [dbo].KRT_Jogosultak
                           inner join #CsoportTagokAll as CsoportTagokAll ON CsoportTagokAll.Id = KRT_Jogosultak.Csoport_Id_Jogalany ) '+char(13);
/*
	  SET @sqlcmd += 'select distinct KRT_Jogosultak.Obj_Id, KRT_Jogosultak.Kezi, KRT_Jogosultak.Tipus
	                    into #JogosultObjektumok 
	                    from KRT_Jogosultak
                       inner join #CsoportTagokAll as CsoportTagokAll ON CsoportTagokAll.Id = KRT_Jogosultak.Csoport_Id_Jogalany; '+char(13);
*/
	else
	  SET @sqlCTL += '; with Jogosult_objektumok as 
	                    (select distinct KRT_Jogosultak.Obj_Id, KRT_Jogosultak.Kezi, KRT_Jogosultak.Tipus
	                        from [dbo].KRT_Jogosultak
                           where KRT_Jogosultak.Csoport_Id_Jogalany = @FelhasznaloId ) '+char(13);
/*
	  SET @sqlcmd += 'select distinct KRT_Jogosultak.Obj_Id, KRT_Jogosultak.Kezi, KRT_Jogosultak.Tipus
	                    into #JogosultObjektumok 
	                    from KRT_Jogosultak
                       where KRT_Jogosultak.Csoport_Id_Jogalany = @FelhasznaloId; '+char(13);
*/
/*
    SET @sqlcmd += 'create NONCLUSTERED index #ix_JogosultObjektumok on [dbo].#JogosultObjektumok (Obj_Id); '+char(13)+char(13);                    --- ez miért NONCLUSTERED index ???
*/

    --- mappa tartalmak TMP	--- ??? miért jobb a CTL-ből insert
	SET @sqlcmd += @sqlCTL + 'select KRT_MappaTartalmak.Obj_Id 
	                  into #MappaTartalmak 
	                  from [dbo].KRT_MappaTartalmak
                     inner join [dbo].KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
                     inner join jogosult_objektumok as jogosult_objektumok ON jogosult_objektumok.Obj_Id = KRT_Mappak.Id
                     where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege; '+char(13)+char(13);
/*                      inner join #JogosultObjektumok as jogosult_objektumok ON jogosult_objektumok.Obj_Id = KRT_Mappak.Id */

--- Nyomköveteés, éles futásban ki kell kapcsolni
---    SET @sqlcmd += 'select count(1) from #IraIratokFiltered; '+char(13);              --- local TMP táblák méretei
---	SET @sqlcmd += 'select count(1) from #CsoportTagokAll; '+char(13);              --- local TMP táblák méretei
---/*	SET @sqlcmd += 'select count(1) from #JogosultObjektumok; '+char(13);              --- local TMP táblák méretei */
---	SET @sqlcmd += 'select count(1) from #MappaTartalmak; '+char(13)+char(13);              --- local TMP táblák méretei
---    SET @sqlcmd += 'insert into #timeTMP select ''20 - INDUL (#subfilter felgyűjtés)'',current_timestamp; '+char(13)+char(13);              --- időszakaszok mérésére

	--- @Org szűrése már #IraIratokFiltered gyűjtésekor megtörtént, így itt emiatt nem kellenek a JOIN-ok       
	--- @where kiértékelése már #IraIratokFiltered gyűjtésekor megtörtént
	--- szét lett bontva több lépésre az UNION blokk   
    if @Jogosultak = '1' AND [dbo].fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0                              --- itt nem kellene @SzervezetId -re cserélni ???, a kettő nem feltétlen ugyanaz !
      begin

		--- gyűjtés közvetlen az Iratokból
        SET @sqlcmd += @sqlCTL + 'select /*distinct*/ EREC_IraIratok.Id 
			              into #subfilter
			              from #IraIratokFiltered as EREC_IraIratok
                          inner join jogosult_objektumok as jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_IraIratok.Id; '+char(13);
/*                          inner join #JogosultObjektumok as jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_IraIratok.Id; '+char(13); */
        /* --- MARAD #subfilter (index nélkül), mivel így nem kell a gyűjtés minden ágán distinct; meg lassabnak is tűnik - hiszen csak #result összeállításakor olvasunk belőle
		   SET @sqlcmd += 'create index #ix_filter on #subfilter (Id); '+char(13)+char(13); 
		--- */

		--- Iratok gyűjtése az Ugyirat-hoz tartozás alapján
		SET @sqlcmd += @sqlCTL + 'insert into #subfilter
                        select /*distinct*/ EREC_IraIratok.Id 
						  from #IraIratokFiltered as EREC_IraIratok
                         where EREC_IraIratok.Ugyirat_Id IN 
                               (
                                 SELECT EREC_UgyUgyiratok.Id FROM [dbo].EREC_UgyUgyiratok
                                  INNER JOIN jogosult_objektumok as jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_UgyUgyiratok.Id '+char(13)
/*                                  INNER JOIN  #JogosultObjektumok as jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_UgyUgyiratok.Id '+char(13) */
        SET @sqlcmd += [dbo].fn_GetJogosultakFilter(@Org, 'jogosult_objektumok')+char(13);                      --- további szűrés Kezi és Tipus alapján

        /* --- Iktatokonyv - Ugyirat kapcsolat 1 - SOK kapcsolat, ezért a JOIN feleslegesen nagy számú sort állít elő - így lassabb volt   --- 15 619 736 / 13:16 sec
           --- lehetne még DISTINCT-et alkalmazni, de az is lassabb volt
		SET @sqlcmd += '         UNION
                                   SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                                    INNER JOIN #JogosultObjektumok ON jogosult_objektumok.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id '+char(13);
        --- */
		SET @sqlcmd += '         UNION
                                 SELECT EREC_UgyUgyiratok.Id FROM [dbo].EREC_UgyUgyiratok                                                        ---    731 074 / 33 sec; 34 sec
                                  where Exists (select 1 from jogosult_objektumok j where j.Obj_Id = EREC_UgyUgyiratok.IraIktatokonyv_Id ) '+char(13);
/*                                  where Exists (select 1 from #JogosultObjektumok j where j.Obj_Id = EREC_UgyUgyiratok.IraIktatokonyv_Id ) '+char(13); */

        if @Csoporttagokkal = 1
          SET @sqlcmd += '       UNION
		                         SELECT EREC_UgyUgyiratok.Id FROM [dbo].EREC_UgyUgyiratok
                                  INNER JOIN #CsoportTagokAll as CsoportTagokAll ON CsoportTagokAll.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez
                                 UNION
								 SELECT EREC_UgyUgyiratok.Id FROM [dbo].EREC_UgyUgyiratok
								  INNER JOIN #CsoportTagokAll as CsoportTagokAll ON CsoportTagokAll.Id = EREC_UgyUgyiratok.Csoport_Id_Felelos
                                 UNION
                                 SELECT EREC_UgyUgyiratok.Id FROM [dbo].EREC_UgyUgyiratok
                                  INNER JOIN #CsoportTagokAll as CsoportTagokAll ON CsoportTagokAll.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo '+char(13);
        else
          SET @sqlcmd += '       UNION
                                 SELECT EREC_UgyUgyiratok.Id FROM [dbo].EREC_UgyUgyiratok where EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez = @FelhasznaloId
                                 UNION
								 SELECT EREC_UgyUgyiratok.Id FROM [dbo].EREC_UgyUgyiratok where EREC_UgyUgyiratok.Csoport_Id_Felelos = @FelhasznaloId
                                 UNION
                                 SELECT EREC_UgyUgyiratok.Id FROM [dbo].EREC_UgyUgyiratok where EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo = @FelhasznaloId '+char(13);
          /* --- NEM mérhető a sebességkülönbség          
		     SET @sqlcmd += '       SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok where @FelhasznaloId in (FelhasznaloCsoport_Id_Ugyintez, Csoport_Id_Felelos, FelhasznaloCsoport_Id_Orzo) '+char(13); 
		  --- */
		--- CR3428					   
        if @Csoporttagokkal = 1
          SET @sqlcmd += '         UNION
                                   SELECT Id FROM [dbo].fn_GetObjIdsFromFeladatok(@FelhasznaloId, @SzervezetId, ''EREC_UgyUgyiratok'') '+char(13);
        else
          SET @sqlcmd += '         UNION
                                   SELECT Id FROM [dbo].fn_GetObjIdsFromFeladatokByFelhasznalo(@FelhasznaloId, ''EREC_UgyUgyiratok'') '+char(13);

        SET @sqlcmd += '         UNION
                                 SELECT EREC_UgyUgyiratok.Id FROM [dbo].EREC_UgyUgyiratok
                                  INNER JOIN #MappaTartalmak as mappa_tartalmak ON mappa_tartalmak.Obj_Id = EREC_UgyUgyiratok.Id
                               ); '+char(13)+char(13);
							   
        --- CR1716: lenézünk a Példányba; CR 2728: aki láthatja a Pédányt, az láthatja az Iratot is
		SET @sqlcmd += @sqlCTL + 'insert into #subfilter
                        select /*distinct*/ EREC_PldIratPeldanyok.IraIrat_Id 
						  from [dbo].EREC_PldIratPeldanyok
                         inner join #IraIratokFiltered as EREC_IraIratok on EREC_IraIratok.ID = EREC_PldIratPeldanyok.IraIrat_Id
                         where EREC_PldIratPeldanyok.Id IN
                               (
                                 SELECT EREC_PldIratPeldanyok.Id FROM [dbo].EREC_PldIratPeldanyok
                                  INNER JOIN jogosult_objektumok as jogosult_objektumok  ON jogosult_objektumok.Obj_Id = EREC_PldIratPeldanyok.Id '+char(13);
/*                                  INNER JOIN #JogosultObjektumok as jogosult_objektumok  ON jogosult_objektumok.Obj_Id = EREC_PldIratPeldanyok.Id '+char(13); */
        if @Csoporttagokkal = 1 
          SET @sqlcmd += '       UNION
		                         SELECT EREC_PldIratPeldanyok.Id FROM [dbo].EREC_PldIratPeldanyok
                                  INNER JOIN #CsoportTagokAll as CsoportTagokAll ON CsoportTagokAll.Id = EREC_PldIratPeldanyok.Csoport_Id_Felelos
                                 UNION
                                 SELECT EREC_PldIratPeldanyok.Id FROM [dbo].EREC_PldIratPeldanyok
                                  INNER JOIN #CsoportTagokAll as CsoportTagokAll ON CsoportTagokAll.Id = EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo '+char(13);
        else
          SET @sqlcmd += '       UNION
		                         SELECT EREC_PldIratPeldanyok.Id FROM [dbo].EREC_PldIratPeldanyok where EREC_PldIratPeldanyok.Csoport_Id_Felelos = @FelhasznaloId
                                 UNION
                                 SELECT EREC_PldIratPeldanyok.Id FROM [dbo].EREC_PldIratPeldanyok where EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo = @FelhasznaloId '+char(13);
          /* --- NEM mérhető a sebességkülönbség          
		     SET @sqlcmd += '       SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok where @FelhasznaloId in (EREC_PldIratPeldanyok.Csoport_Id_Felelos, EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo) '+char(13); 
		  --- */
        --- CR3428							   
        if @Csoporttagokkal = 1
          SET @sqlcmd += '         UNION
                                   SELECT Id FROM [dbo].fn_GetObjIdsFromFeladatok(@FelhasznaloId, @SzervezetId /* @ExecutorUserId, @FelhasznaloSzervezet_Id */, ''EREC_PldIratPeldanyok'') '+char(13);
        else
          SET @sqlcmd += '         UNION
                                   SELECT Id FROM [dbo].fn_GetObjIdsFromFeladatokByFelhasznalo(@FelhasznaloId, ''EREC_PldIratPeldanyok'') '+char(13);

        SET @sqlcmd += '         UNION
                                 SELECT EREC_PldIratPeldanyok.Id FROM [dbo].EREC_PldIratPeldanyok
                                  INNER JOIN #MappaTartalmak as mappa_tartalmak ON mappa_tartalmak.Obj_Id = EREC_PldIratPeldanyok.Id        
                               ); '+char(13)+char(13);
							   
        --- gyűjtés a Feladatok alapján
        --- CR3428						 
        if @Csoporttagokkal = 1
		  SET @sqlcmd += 'insert into #subfilter 
		                  select /*distinct*/ alsel.Id 
			                from [dbo].fn_GetObjIdsFromFeladatok(@FelhasznaloId, @SzervezetId, ''EREC_IraIratok'') alsel
                           inner join #IraIratokFiltered as EREC_IraIratok on EREC_IraIratok.Id = alsel.Id ; '+char(13)+char(13);
        else
		  SET @sqlcmd += 'insert into #subfilter 
		                  select /*distinct*/ alsel.Id 
			                from [dbo].fn_GetObjIdsFromFeladatokByFelhasznalo(@FelhasznaloId, ''EREC_IraIratok'') alsel
                           inner join #IraIratokFiltered as EREC_IraIratok on EREC_IraIratok.Id = alsel.Id ; '+char(13)+char(13);
						 
        /* --- azaz nem áll össze, mivel minek - majd összerakjuk az elemeket #result-ban
	    --- végül itt áll össze a #filter tábla
		SET @sqlcmd += 'select distinct f1.Id 
	                      into #filter 
		                  from #IraIratokFiltered as f1
	                     inner join #subfilter as f2 on f2.Id = f1.Id; '+char(13)+char(13);
        --- */
	  end;
    /* --- ezt az ágat is #result megépítésekor kezeljük le
	else                                                                --- azaz NINCS jogosultság szűrés
	  --- speciális szűrők kiértékelése már előbb megtörtént
      SET @sqlcmd += 'select Id into #filter from #IraIratokFiltered; '+char(13)+char(13);
	--- */

    --- SET @sqlcmd += 'insert into #timeTMP select ''25 - #filter felgyűjtés vége; #filter count számítás'', current_timestamp; '
    --- SET @sqlcmd += ' select count(1) from #filter; '                                                 
---    SET @sqlcmd += 'insert into #timeTMP select ''30 - #subfilter felgyűjtés vége; Metaadatok oszlopainak lekérése'',current_timestamp; '+char(13)+char(13);
    /************************************************************
    * Metaadatok oszlopainak lekérése - ???                     *              
    ************************************************************/  
    if @ColumnNames = '*'
      begin
        SET @ColumnNames = null

        ;with OMD_Hierarchia as
        (
            -- Base case
            select Id, Felettes_Obj_Meta, Objtip_Id, 0 as Szint
            from [dbo].EREC_Obj_MetaDefinicio
            where Objtip_Id in (select TOP 1 Id from [dbo].KRT_ObjTipusok where Kod = 'EREC_IraIratok' and ObjTipus_Id_Tipus =
                     (select TOP 1 Id from [dbo].KRT_ObjTipusok where Kod = 'dbtable') and getdate() between ErvKezd and ErvVege)
            and Org=@Org
            and getdate() between ErvKezd and ErvVege

            union all

            -- Recursive step
            -- az ObjTip_Id-t örököltetjük a felettesekre is
            select omd.Id, omd.Felettes_Obj_Meta, omdh.Objtip_Id, omdh.Szint + 1 as Szint
            from [dbo].EREC_Obj_MetaDefinicio as omd
            join OMD_Hierarchia as omdh
            on omdh.Felettes_Obj_Meta = omd.Id
            and omd.Org=@Org
            and getdate() between omd.ErvKezd and omd.ErvVege
        )
        select @ColumnNames = COALESCE(@ColumnNames + ',[' + tsz.TargySzavak + ']',        /* ez NEM ad korrekt választ, amennyiben a query nem ad vissza sort */
                         '[' + tsz.TargySzavak + ']')
		  from (select min(oma.Sorszam) as Sorszam, EREC_TargySzavak.TargySzavak
                   FROM [dbo].EREC_TargySzavak AS EREC_TargySzavak
                   LEFT JOIN [dbo].EREC_Obj_MetaAdatai oma on oma.TargySzavak_Id = EREC_TargySzavak.Id
                  WHERE oma.Obj_MetaDefinicio_Id in (select Id from OMD_Hierarchia)
                    and getdate() between oma.ErvKezd and oma.ErvVege
                  group by EREC_TargySzavak.TargySzavak
		       ) as tsz
         order by tsz.Sorszam, tsz.TargySzavak;

/* --- kiakadt, ha egy adott TargySzavak kétszer fordult elő a listában  -- 2017.08.28., BogI
        select @ColumnNames = COALESCE(@ColumnNames + ',[' + tsz.TargySzavak + ']',   */     /* ez NEM ad korrekt választ, amennyiben a query nem ad vissza sort */
       /*                  '[' + tsz.TargySzavak + ']')
                        FROM  [dbo].EREC_TargySzavak AS tsz
                        LEFT JOIN [dbo].EREC_Obj_MetaAdatai oma on oma.TargySzavak_Id = tsz.Id
                        WHERE oma.Obj_MetaDefinicio_Id in (select Id from OMD_Hierarchia)
                           and getdate() between oma.ErvKezd and oma.ErvVege
                        ORDER BY oma.Sorszam, tsz.TargySzavak
*/
      end;
    if @ColumnNames is not null and @ColumnNames != ''
      begin
        SET @sqlcmd += N'select Obj_Id, 
		                       case IsNull(oma.ControlTypeSource, tsz.ControlTypeSource) 
							       when ''~/Component/EditablePartnerTextBox.ascx'' then 
									   (select TOP 1 Nev from [dbo].KRT_Partnerek where Id=otsz.Ertek and getdate() between ErvKezd and ErvVege)
                                   when ''~/Component/FelhasznaloCsoportTextBox.ascx'' then 
								       (select TOP 1 Nev from [dbo].KRT_Felhasznalok where Id=otsz.Ertek and getdate() between ErvKezd and ErvVege)
								   when ''~/Component/KodTarakDropDownList.ascx'' then
								       (select TOP 1 Nev from [dbo].KRT_KodTarak 
									     where KodCsoport_Id = (select TOP 1 Id from [dbo].KRT_KodCsoportok 
										                         where Kod=IsNull(oma.ControlTypeDataSource, tsz.ControlTypeDataSource) collate Hungarian_CS_AS
																   and getdate() between ErvKezd and ErvVege)
                                           and Org=@Org
                                           and Kod=otsz.Ertek collate Hungarian_CS_AS
                                           and getdate() between ErvKezd and ErvVege)
								  when ''~/Component/FuggoKodtarakDropDownList.ascx'' then
								       (select TOP 1 Nev from [dbo].KRT_KodTarak 
									     where KodCsoport_Id = (select TOP 1 Id from [dbo].KRT_KodCsoportok 
										                         where Kod=IsNull(oma.ControlTypeDataSource, tsz.ControlTypeDataSource) collate Hungarian_CS_AS
																   and getdate() between ErvKezd and ErvVege)
                                           and Org=@Org
                                           and Kod=otsz.Ertek collate Hungarian_CS_AS
                                           and getdate() between ErvKezd and ErvVege)
                                   else Ertek 
							   end as Ertek,
							   Targyszo 
						   into #tableToPivot
                           from [dbo].EREC_ObjektumTargyszavai otsz
                           left join [dbo].EREC_Obj_MetaAdatai oma on otsz.Obj_MetaAdatai_Id=oma.Id 
                           left join [dbo].EREC_TargySzavak tsz on oma.TargySzavak_Id = tsz.Id ';
		SET @sqlcmd += N'where 1 = 1 ';
        if @Jogosultak = '1' AND [dbo].fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0                              --- itt nem kellene @SzervezetId -re cserélni ???, a kettő nem feltétlen ugyanaz !
		  SET @sqlcmd += N' and otsz.Obj_Id in (select Id from #subfilter /* --- #filter nem lett megépítve */) ';
        else
		  SET @sqlcmd += N' and otsz.Obj_Id in (select Id from #IraIratokFiltered /* --- #subfilter sem lett megépítve */) ';

        SET  @sqlcmd += N' and getdate() between oma.ErvKezd and oma.ErvVege  
						   and getdate() between tsz.ErvKezd and tsz.ErvVege
                           and getdate() between otsz.ErvKezd and otsz.ErvVege; '+char(13)+char(13);
        SET @sqlcmd += N'select Obj_Id, ' + @ColumnNames + '
                           into #pivoted_meta
                           from #tableToPivot
                           PIVOT ( Max(Ertek) FOR Targyszo in (' + @ColumnNames + ') ) as PivotedTable; '+char(13)+char(13);
      end;

    /**************************************************************
    * Szűrt adatokhoz rendezés és sorszám összeállítása - #result *
    **************************************************************/
---    SET @sqlcmd += 'insert into #timeTMP select ''40 - #result összeállítása: @OrderBy alapján Rownum() '', current_timestamp; '+char(13)+char(13);

    --- konzisztens adatlekérés érdekében
	if charindex( lower('EREC_IraIratok.ID'), lower( @OrderBy ) ) = 0 
      SET @OrderBy += ', EREC_IraIratok.ID DESC';

	--- dinamikusan építjük fel - csak a legszükségesebb - JOIN-okat
	--- Fontos: az itt megfogalmazott kapcsolatoknak egyezniük kell a '* Tényleges select' során, az egyes adatmezők lekéréséhez felépített kapcsolatokkal
    SET @sqlOrderBy = ' ';

	-- EREC_UgyUgyiratok függő kapcsolatok
    if ( charindex( lower('EREC_UgyUgyiratok'), lower( @OrderBy ) ) > 0 ) OR 
	   ( charindex( lower('Csoportok_UgyiratUgyintezoNev'), lower( @OrderBy ) ) > 0 ) OR
	   ( charindex( lower('ITSZ'), lower( @OrderBy ) ) > 0 ) OR
	   ( charindex( lower('EljarasiSzakaszFokNev'), lower( @OrderBy ) ) > 0 ) OR	   
  	   ( charindex( lower('UgyintezesModjaKodTarak'), lower( @OrderBy ) ) > 0 )             --- #66 - 2017.09.11., BogI
	    SET @sqlOrderBy += 'LEFT JOIN [dbo].EREC_UgyUgyiratok as EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id '+char(13);
    --- #66 - 2017.09.06., BogI
   	if ( charindex( lower('Csoportok_UgyiratUgyintezoNev'), lower( @OrderBy ) ) > 0 ) /* OR ( charindex( lower('EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez'), lower( @OrderBy ) ) > 0 ) */
	  SET @sqlOrderBy += 'left join [dbo].KRT_Csoportok as Csoportok_UgyiratUgyintezoNev on Csoportok_UgyiratUgyintezoNev.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez '+char(13);
	if ( charindex( lower('ITSZ'), lower( @OrderBy ) ) > 0 )               --- csak az 'ITSZ' alias névre lehet rendezni
	  SET @sqlOrderBy += 'OUTER APPLY ( select [dbo].fn_MergeIrattariTetelSzam(EREC_AgazatiJelek.Kod,EREC_IraIrattariTetelek.IrattariTetelszam,EREC_IraIrattariTetelek.MegorzesiIdo,EREC_IraIrattariTetelek.IrattariJel,@Org) as ITSZ
                                          from [dbo].EREC_IraIrattariTetelek, [dbo].EREC_Agazatijelek
                                         where EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_Agazatijelek.Id 
		                                   and EREC_UgyUgyiratok.IraIrattariTetel_Id = EREC_IraIrattariTetelek.Id) as ITSZ  '+char(13);
    --- #66 - 2017.09.11., BogI
	if ( charindex( lower('UgyintezesModjaKodTarak'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_UgyUgyiratok.UgyintezesModja'), lower( @OrderBy ) ) > 0 )
      SET @sqlOrderBy += 'left join [dbo].KRT_Kodcsoportok as UgyintezesModjaKodCsoport on UgyintezesModjaKodCsoport.Kod = ''UGYINTEZES_ALAPJA'' 
                             left join [dbo].KRT_Kodtarak as UgyintezesModjaKodTarak on UgyintezesModjaKodTarak.Kodcsoport_Id = UgyintezesModjaKodCsoport.Id 
							       and UgyintezesModjaKodTarak.Kod = EREC_UgyUgyiratok.UgyintezesModja and UgyintezesModjaKodtarak.Org =@Org '+char(13);
    ---

	if charindex( lower('EREC_IraIktatoKonyvek'), lower( @OrderBy ) ) > 0 
      SET @sqlOrderBy += 'LEFT JOIN [dbo].EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
                           LEFT JOIN [dbo].EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek ON EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id '+char(13);
	else
	  if charindex( lower('EREC_UgyUgyiratdarabok'), lower( @OrderBy ) ) > 0 
	    SET @sqlOrderBy += 'LEFT JOIN [dbo].EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id '+char(13);
    if ( charindex( lower('EREC_KuldKuldemenyek'), lower( @OrderBy ) ) > 0 ) OR 
	   ( charindex( lower('KuldesModjaKodTarak'), lower( @OrderBy ) ) > 0 ) OR
	   ( charindex( lower('KezbesitesModjaKodtarak'), lower( @OrderBy ) ) > 0 )
	    SET @sqlOrderBy += 'LEFT JOIN [dbo].EREC_KuldKuldemenyek AS EREC_KuldKuldemenyek ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id '+char(13);
	--- !!! LEFT join-ban ON-ban az AND nem egészen úgy működik, ahogyan számítanánk rá !!!

	-- Az alias neveken történik meg a hivatkozott rendező mező mező megadása
	--- EREC_IraIratok függő
	if ( charindex( lower('AllapotKodTarak'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_IraIratok.Allapot'), lower( @OrderBy ) ) > 0 )
      SET @sqlOrderBy += 'left join [dbo].KRT_Kodcsoportok as AllapotKodCsoport on AllapotKodCsoport.Kod = ''IRAT_ALLAPOT''
                             left join [dbo].KRT_Kodtarak as AllapotKodTarak on AllapotKodTarak.Kodcsoport_Id = AllapotKodCsoport.Id and AllapotKodTarak.Kod = EREC_IraIratok.Allapot and AllapotKodTarak.Org=@Org '+char(13);
	if ( charindex( lower('AdathordozoTipusaKodTarak'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_IraIratok.AdathordozoTipusa'), lower( @OrderBy ) ) > 0 )
      SET @sqlOrderBy += 'left join [dbo].KRT_Kodcsoportok as AdathordozoTipusaKodCsoport on AdathordozoTipusaKodCsoport.Kod = ''UGYINTEZES_ALAPJA''
                             left join [dbo].KRT_Kodtarak as AdathordozoTipusaKodTarak on AdathordozoTipusaKodTarak.Kodcsoport_Id = AdathordozoTipusaKodCsoport.Id and AdathordozoTipusaKodTarak.Kod = EREC_IraIratok.AdathordozoTipusa and AdathordozoTipusaKodTarak.Org=@Org '+char(13);    
	--- EREC_KuldKuldemenyek függő
	if ( charindex( lower('KuldesModjaKodTarak'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_KuldKuldemenyek.KuldesMod'), lower( @OrderBy ) ) > 0 )
      SET @sqlOrderBy += 'left join [dbo].KRT_Kodcsoportok as KuldesModjaKodCsoport on KuldesModjaKodCsoport.Kod = ''KULDEMENY_KULDES_MODJA'' /* ''KULDES_MODJA'' - ilyen kulcs nincs */
                             left join [dbo].KRT_Kodtarak as KuldesModjaKodTarak on KuldesModjaKodTarak.Kodcsoport_Id = KuldesModjaKodCsoport.Id and KuldesModjaKodTarak.Kod = EREC_KuldKuldemenyek.KuldesMod and KuldesModjaKodtarak.Org =@Org '+char(13);
	if ( charindex( lower('KezbesitesModjaKodtarak'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_KuldKuldemenyek.KezbesitesModja'), lower( @OrderBy ) ) > 0 )
      -- SET @sqlOrderBy += 'left join [dbo].KRT_Kodcsoportok as KezbesitesModjaKodCsoport on KezbesitesModjaKodCsoport.Kod = ''KEZBESITES_MODJA'' --- #66 - 2017.09.11., BogI
	  SET @sqlOrderBy += 'left join [dbo].KRT_Kodcsoportok as KezbesitesModjaKodCsoport on KezbesitesModjaKodCsoport.Kod = ''KULD_KEZB_MODJA''
                             left join [dbo].KRT_Kodtarak as KezbesitesModjaKodTarak on KezbesitesModjaKodTarak.Kodcsoport_Id = KezbesitesModjaKodCsoport.Id and KezbesitesModjaKodTarak.Kod collate database_default = EREC_KuldKuldemenyek.KezbesitesModja collate database_default and KezbesitesModjaKodtarak.Org =@Org '+char(13);
    --- EREC_IraIratok függő
   	if ( charindex( lower('Csoportok_IktatoNev'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_IraIratok.FelhasznaloCsoport_Id_Iktato'), lower( @OrderBy ) ) > 0 )
	  SET @sqlOrderBy += 'left join [dbo].KRT_Csoportok as Csoportok_IktatoNev on Csoportok_IktatoNev.Id = EREC_IraIratok.FelhasznaloCsoport_Id_Iktato '+char(13);
   	if ( charindex( lower('Csoportok_UgyintezoNev'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez'), lower( @OrderBy ) ) > 0 )
	  SET @sqlOrderBy += 'left join [dbo].KRT_Csoportok as Csoportok_UgyintezoNev on Csoportok_UgyintezoNev.Id = EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez '+char(13);
	--- EREC_IraIratok függő
   	if ( charindex( lower('PostazasIranyaKodTarak'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_IraIratok.PostazasIranya'), lower( @OrderBy ) ) > 0 )
	  SET @sqlOrderBy += 'left join [dbo].KRT_Kodcsoportok as PostazasIranyaKodCsoport on PostazasIranyaKodCsoport.Kod = ''POSTAZAS_IRANYA''	/* -- nekrisz CR 2997 Postazas iránya szöveggel */
		                     left join [dbo].KRT_Kodtarak as PostazasIranyaKodTarak on PostazasIranyaKodTarak.Kodcsoport_Id = PostazasIranyaKodCsoport.Id and PostazasIranyaKodTarak.Kod  collate database_default = EREC_IraIratok.PostazasIranya  collate database_default and PostazasIranyaKodTarak.Org=@Org '+char(13);
  	 -- BLG_619
	if ( charindex( lower('Csoportok_IratFelelosNev'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_IraIratok.Csoport_Id_UgyFelelos'), lower( @OrderBy ) ) > 0 )
	  SET @sqlOrderBy += 'left join [dbo].KRT_Csoportok as Csoportok_IratFelelosNev on Csoportok_IratFelelosNev.Id = EREC_IraIratok.Csoport_Id_UgyFelelos '+char(13);


    --- #66 - 2017.09.06., BogI
	if ( charindex( lower('IratKezelesiFeljegyzesek'), lower( @OrderBy ) ) > 0 )               --- csak a 'KezelesiFeljegyzes' alias névre lehet rendezni
	  SET @sqlOrderBy += 'OUTER APPLY (select top 1 Leiras as KezelesiFeljegyzes from [dbo].EREC_IraKezFeljegyzesek kf where kf.IraIrat_Id = EREC_IraIratok.Id order by LetrehozasIdo desc) as IratKezelesiFeljegyzesek '+char(13);
   
   if ( charindex( lower('EljarasiSzakaszFokNev'), lower( @OrderBy ) ) > 0 )               --- csak a 'EljarasiSzakaszFokNev' alias névre lehet rendezni
	  SET @sqlOrderBy += 'OUTER APPLY  (select DBO.FN_EREC_UgyUgyirat_EljarasiSzakaszFok(EREC_UgyUgyiratok.Id,@ORG) as Nev) as EljarasiSzakaszFokNev '+char(13);
	
	-- BLG_1677
	if ( charindex( lower('UgyintezesModjaKodTarak'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_IraIratok.UgyintezesModja'), lower( @OrderBy ) ) > 0 )
      SET @sqlOrderBy += 'left join [dbo].KRT_Kodcsoportok as IratUgyintezesModjaKodCsoport on IratUgyintezesModjaKodCsoport.Kod = ''IRAT_UGYINT_MODJA'' 
                             left join [dbo].KRT_Kodtarak as IratUgyintezesModjaKodTarak on IratUgyintezesModjaKodTarak.Kodcsoport_Id = IratUgyintezesModjaKodCsoport.Id 
							       and IratUgyintezesModjaKodTarak.Kod = EREC_IraIratok.UgyintezesModja and IratUgyintezesModjaKodtarak.Org =@Org '+char(13);

    --- EREC_PldIratPeldanyok függő
	--- #66 - 2017.09.15., BogI
    if ( charindex( lower('EREC_PldIratPeldanyok'), lower( @OrderBy ) ) > 0 ) OR 
	   ( charindex( lower('KodTarak_Peldany_AllapotNev'), lower( @OrderBy ) ) > 0 ) OR
	   ( charindex( lower('Csoportok_Peldany_OrzoNev'), lower( @OrderBy ) ) > 0 ) OR
	   ( charindex( lower('Csoportok_Peldany_FelelosNev'), lower( @OrderBy ) ) > 0 ) OR
  	   ( charindex( lower('PeldanyKuldesModjaKodTarak'), lower( @OrderBy ) ) > 0 )             --- #66 - 2017.09.11., BogI
	  SET @sqlOrderBy += 'LEFT JOIN [dbo].EREC_PldIratPeldanyok as EREC_PldIratPeldanyok ON EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id and EREC_PldIratPeldanyok.Sorszam = 1'+char(13);

   	if ( charindex( lower('KodTarak_Peldany_AllapotNev'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_PldIratPeldanyok.Allapot'), lower( @OrderBy ) ) > 0 )
	  SET @sqlOrderBy += 'left join [dbo].KRT_Kodcsoportok as KodCsoport_Peldany_AllapotNev on KodCsoport_Peldany_AllapotNev.Kod = ''IRATPELDANY_ALLAPOT''
		                     left join [dbo].KRT_Kodtarak as KodTarak_Peldany_AllapotNev on KodTarak_Peldany_AllapotNev.Kodcsoport_Id = KodCsoport_Peldany_AllapotNev.Id and 
							                                 KodTarak_Peldany_AllapotNev.Kod  collate database_default = EREC_PldIratPeldanyok.Allapot collate database_default and 
															 KodTarak_Peldany_AllapotNev.Org = @Org '+char(13);
    if ( charindex( lower('Csoportok_Peldany_OrzoNev'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo'), lower( @OrderBy ) ) > 0 )
	  SET @sqlOrderBy += 'left join [dbo].KRT_Csoportok as Csoportok_Peldany_OrzoNev on Csoportok_Peldany_OrzoNev.Id = EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo '+char(13);             --- KRT_Csoportok.ErvKezd, ErvVege szűrés értelmetléennek látszik
    if ( charindex( lower('Csoportok_Peldany_FelelosNev'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_PldIratPeldanyok.Csoport_Id_Felelos'), lower( @OrderBy ) ) > 0 )
	  SET @sqlOrderBy += 'left join [dbo].KRT_Csoportok as Csoportok_Peldany_FelelosNev on Csoportok_Peldany_FelelosNev.Id = EREC_PldIratPeldanyok.Csoport_Id_Felelos '+char(13);             --- KRT_Csoportok.ErvKezd, ErvVege szűrés értelmetléennek látszik
     --- #66 - 2017.09.11., BogI
	if ( charindex( lower('PeldanyKuldesModjaKodTarak'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_PldIratPeldanyok.KuldesMod'), lower( @OrderBy ) ) > 0 )
      SET @sqlOrderBy += 'left join [dbo].KRT_Kodcsoportok as PeldanyKuldesModjaKodCsoport on PeldanyKuldesModjaKodCsoport.Kod = ''KULDEMENY_KULDES_MODJA'' 
                             left join [dbo].KRT_Kodtarak as PeldanyKuldesModjaKodTarak on PeldanyKuldesModjaKodTarak.Kodcsoport_Id = PeldanyKuldesModjaKodCsoport.Id 
							       and PeldanyKuldesModjaKodTarak.Kod = EREC_PldIratPeldanyok.KuldesMod and PeldanyKuldesModjaKodtarak.Org =@Org '+char(13);

    /*
	--- szétszedtem a kapcsolatot, hogy a direkt mezőkre rendezéseket NE lassítsa a képzett mezőkre rendezés
	--- ! Figyelem ! EREC_PldIratPeldanyok -ból NEM vesszük fel a teljes mezőlistát, csak az itt felsorolt mezők adhatók meg @OrderBy-ban
	if charindex( lower('EREC_PldIratPeldanyok'), lower( @OrderBy ) ) > 0          
      SET @sqlOrderBy += 'OUTER APPLY (select top 1 Allapot, TovabbitasAlattAllapot, FelhasznaloCsoport_Id_Orzo,
                                               (select top 1 Nev 
											      from [dbo].KRT_Kodtarak kt 
												 where kt.Kod = p.Allapot and kt.Org = @Org
                                                   and kt.Kodcsoport_Id = (select top 1 Id from [dbo].KRT_Kodcsoportok kcs where kcs.Kod = ''IRATPELDANY_ALLAPOT'')
											   ) as Allapot_Nev,
                                               (select top 1 Nev 
											      from [dbo].KRT_Csoportok cs 
												 where p.FelhasznaloCsoport_Id_Orzo=cs.Id and getdate() between cs.ErvKezd and cs.ErvVege
											   ) as Orzo_Nev,
                                               (select top 1 Nev 
											      from [dbo].KRT_Csoportok cs 
												 where p.Csoport_Id_Felelos=cs.Id 
												   and getdate() between cs.ErvKezd and cs.ErvVege
											   ) as Felelos_Nev,
                                               p.BarCode, CimSTR_Cimzett, NevSTR_Cimzett, KuldesMod, PostazasDatuma  
								     from [dbo].EREC_PldIratPeldanyok p
                                    where IraIrat_Id=EREC_IraIratok.Id and Sorszam=1
                                  ) as EREC_PldIratPeldanyok '+char(13);

	/* --- átírtam LEFT OUTER JOIN -ra, de NEM kimérhető a sebességkülönbség - az OUTER APPLY mintha egy hajszállal gyorsabb lenne
      SET @sqlcmd += 'left join 
	                    (select TOP 1 Allapot,
						        (select TOP 1 Nev from KRT_Kodtarak kt 
								  where kt.Kod = p.Allapot and kt.Org = @Org 
								    and kt.Kodcsoport_Id = (select TOP 1 Id from KRT_Kodcsoportok kcs where kcs.Kod = ''IRATPELDANY_ALLAPOT'')
								) as Allapot_Nev,
                                (select TOP 1 Nev from KRT_Csoportok cs 
								  where p.FelhasznaloCsoport_Id_Orzo=cs.Id 
								    and getdate() between cs.ErvKezd and cs.ErvVege
								) as Orzo_Nev,
                                (select TOP 1 Nev from KRT_Csoportok cs 
								  where p.Csoport_Id_Felelos=cs.Id 
								    and getdate() between cs.ErvKezd and cs.ErvVege
								) as Felelos_Nev,
                                BarCode, IraIrat_Id
						   from [dbo].EREC_PldIratPeldanyok p
                          where p.Sorszam = 1 
						) as EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id '+char(13);
	--- */
    ---
	*/

	/* --- #filter -ből így készült el #result
    SET @sqlcmd += N'select row_number() over('+@OrderBy+') as RowNumber, EREC_IraIratok.Id 
	                   into #result
                       from #filter as f
	                  inner join EREC_IraIratok as EREC_IraIratok on f.Id = EREC_IraIratok.Id '+char(13);
    --- */
	--- #filter NEM készült el, így #IraIratokFiltered és #subfilter-ből rakjuk össze #result-ot
    if @Jogosultak = '1' AND [dbo].fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0                              --- itt nem kellene @SzervezetId -re cserélni ???, a kettő nem feltétlen ugyanaz !
	  /* --- csak akkor kell így, ha #subfilter gyűjtésekor nem minden ágon JOIN -oltunk össze #IraIratokFiltered -del
      SET @sqlcmd += N'select row_number() over('+@OrderBy+') as RowNumber, EREC_IraIratok.Id 
	                     into #result
                         from (select distinct f1.Id 
                                  from #IraIratokFiltered as f1
                                 inner join #subfilter as f2 on f2.Id = f1.Id
						      ) as f
                         from (select distinct Id from #subfilter ) as f 
	                    inner join EREC_IraIratok as EREC_IraIratok on EREC_IraIratok.Id = f.Id '+char(13);
	  --- */
      SET @sqlcmd += N'select row_number() over('+@OrderBy+') as RowNumber, EREC_IraIratok.Id 
	                     into #result
                         from (select distinct Id from #subfilter ) as f 
	                    inner join [dbo].EREC_IraIratok as EREC_IraIratok on EREC_IraIratok.Id = f.Id '+char(13);               -- ez a további JOIN-ok kapcsolója
    else
      SET @sqlcmd += N'select row_number() over('+@OrderBy+') as RowNumber, EREC_IraIratok.Id 
	                     into #result
                         from #IraIratokFiltered as f
	                    inner join [dbo].EREC_IraIratok as EREC_IraIratok on EREC_IraIratok.Id = f.Id '+char(13);               -- ez a további JOIN-ok kapcsolója

    SET @sqlcmd += @sqlOrderBy;
 	
    if @ColumnNames is not null and @ColumnNames != ''
      SET @sqlcmd += ' LEFT JOIN #pivoted_meta as EREC_ObjektumTargyszavai on EREC_IraIratok.Id=EREC_ObjektumTargyszavai.Obj_Id '+char(13);

---    SET @sqlcmd += 'insert into #timeTMP select ''50 - #result felépítés vége; fix sorra ugrás beállítása'', current_timestamp; '+char(13)+char(13);
      
    if (@SelectedRowId is not null)                       --- fix sorra ugrás beállítása
	    --- #2571 - 2018.11.18., BogI
        SET @sqlcmd += N'if exists (select 1 from #result where Id = @SelectedRowId)
                           begin
                             SET @pageNumber = (select ( ((RowNumber - 1 )/ @pageSize) + 1 ) from #result where Id = @SelectedRowId );          --- NEM lehet NULL, most vizsgáltuk le
                             set @firstRow = (@pageNumber - 1) * @pageSize + 1;
                             set @lastRow = @pageNumber*@pageSize;
                             SET @pageNumber -= 1;
                           end;
                         else '
	  --- ez az ág mindíg fut, ez a normál beállítás
      SET @sqlcmd += N'if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
                           begin
                             SET @pageNumber = ( select ( ((RowNumber - 1 )/ @pageSize) + 1 ) from #result where RowNumber = ( select MAX(RowNumber) from #result ) );
                             set @firstRow = (@pageNumber - 1) * @pageSize + 1;
                             set @lastRow = @pageNumber*@pageSize;
                             SET @pageNumber -= 1;
                           end;'+char(13)+char(13);
/*    --- #366 - 2017.09.25., BogI
      SET @sqlcmd += N'if exists (select 1 from #result where Id = @SelectedRowId)
                         begin
                           *//* ez NEM ad korrekt választ, amennyiben a query nem ad vissza sort
						   -- select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
						   *//*
						   SET @pageNumber = (select ( ((RowNumber - 1 )/ @pageSize) + 1 ) from #result where Id = @SelectedRowId );
                           set @firstRow = (@pageNumber - 1) * @pageSize + 1;
                           set @lastRow = @pageNumber*@pageSize;
                           select @pageNumber = @pageNumber - 1;
                         end;
                       else
                         if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
                           begin
						     *//* ez NEM ad korrekt választ, amennyiben a query nem ad vissza sort 
                             -- select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
							 *//*
							 SET @pageNumber = (select ( ((RowNumber - 1 )/ @pageSize) + 1 ) from #result group by RowNumber having MAX(RowNumber) = RowNumber );
                             set @firstRow = (@pageNumber - 1) * @pageSize + 1;
                             set @lastRow = @pageNumber*@pageSize;
                             select @pageNumber = @pageNumber - 1;
                           end; '+char(13)+char(13);
*/
---    SET @sqlcmd += 'insert into #timeTMP select ''60 - bizalmas iratok szűrésének előkészítése'', current_timestamp; '+char(13)+char(13);
      
    /*****************************************************************
    * Bizalmas iratok Őrző szerinti szűréséhez csoporttagok lekérése *
    *****************************************************************/
	--- Előáll @iratok_executor tábla, amit majd a tényleges select-ben értékelünk ki
    if [dbo].fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0               -- and @Jogosultak = '1'      --- nem lehetne lecserélni @SzervezetId -ra ??? a kettő nem feltétlen ugyanaz !
      begin
        SET @sqlcmd += N' declare @iratok_executor table (Id uniqueidentifier primary key); '+char(13)+char(13);
        SET @sqlcmd += N' declare @confidential varchar(4000); '+char(13)+char(13);
		SET @sqlcmd += N' set @confidential = (select TOP 1 Ertek 
		                                         from [dbo].KRT_Parameterek 
		                                        where Nev=''IRAT_MINOSITES_BIZALMAS''
                                                  and Org=@Org 
												  and getdate() between ErvKezd and ErvVege); '+char(13)+char(13);
        SET @sqlcmd += N' declare @Bizalmas table (val nvarchar(64) collate Hungarian_CS_AS ); '+char(13)+char(13);
		SET @sqlcmd += N' insert into @Bizalmas (val) (select Value from [dbo].fn_Split(@confidential, '','')); '+char(13)+char(13);
		SET @sqlcmd += N' declare @csoporttagsag_tipus nvarchar(64); '+char(13)+char(13);
		SET @sqlcmd += N' SET @csoporttagsag_tipus = (select TOP 1 KRT_CsoportTagok.Tipus 
		                                                from [dbo].KRT_CsoportTagok
                                                       where KRT_CsoportTagok.Csoport_Id_Jogalany=@FelhasznaloId
                                                         and KRT_CsoportTagok.Csoport_Id=@SzervezetId
                                                         and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
														 order by KRT_CsoportTagok.Tipus desc
													 ); '+char(13)+char(13);
        SET @sqlcmd += N' DECLARE @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN nvarchar(400)
						  set @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN = (select top 1 Ertek from KRT_Parameterek where Nev=''JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN''
						  and Org=@Org and getdate() between ErvKezd and ErvVege)

						  IF @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN IS NULL
							SET @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN = ''0''
		'
        SET @sqlcmd += N' CREATE TABLE #jogosultak (Id uniqueidentifier); '+char(13)+char(13);
		SET @sqlcmd += N'if @csoporttagsag_tipus in (''3'',''4'')
                            begin
                              insert into #jogosultak 
							  select KRT_Csoportok.Id 
							    from [dbo].KRT_Csoportok
                               inner join [dbo].KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id = @SzervezetId 
                                      and KRT_Csoportok.Tipus = ''1'' 
									  and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
									  and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
                               where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege 
						    end;
                          else
                            insert into #jogosultak select @FelhasznaloId; '+char(13)+char(13);
        SET @sqlcmd += N' insert into @iratok_executor 
		                  select #result.Id
                            from [dbo].EREC_IraIratok
                           inner join #result on #result.Id = EREC_IraIratok.Id 
						          and #result.RowNumber between @firstRow and @lastRow
                           where 1 = case when (@confidential = ''MIND'' or EREC_IraIratok.Minosites in (select val from @Bizalmas))
                                               and NOT exists (select 1 
                                                                 from [dbo].KRT_Jogosultak
																where KRT_Jogosultak.Obj_Id in (#result.Id, EREC_IraIratok.Ugyirat_Id)
                                                                  and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
                                                                  and @FelhasznaloId = KRT_Jogosultak.Csoport_Id_Jogalany
															  )
											   and NOT exists (select 1
                                                                 from [dbo].EREC_PldIratPeldanyok
																 join EREC_IraIratok ii on EREC_PldIratPeldanyok.IraIrat_Id = ii.Id
																 join EREC_UgyUgyiratok ui on ii.Ugyirat_Id = ui.Id
                                                                where EREC_PldIratPeldanyok.IraIrat_Id=#result.Id
                                                                  and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo in (select Id from #jogosultak)
																  and (@JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN !=''1'' or EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo=@FelhasznaloId or (ii.Csoport_Id_Ugyfelelos is null and ui.Csoport_Id_Ugyfelelos is null) or ISNULL(ii.Csoport_Id_Ugyfelelos, ui.Csoport_Id_Ugyfelelos) = @SzervezetId)
                                                              )
												and NOT exists (select 1
                                                                 from [dbo].EREC_PldIratPeldanyok
																 join EREC_IraIratok ii on EREC_PldIratPeldanyok.IraIrat_Id = ii.Id
																 join EREC_UgyUgyiratok ui on ii.Ugyirat_Id = ui.Id
                                                                where EREC_PldIratPeldanyok.IraIrat_Id=#result.Id
                                                                  and EREC_PldIratPeldanyok.Csoport_Id_Felelos in (select Id from #jogosultak)
																  and (@JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN !=''1'' or EREC_PldIratPeldanyok.Csoport_Id_Felelos=@FelhasznaloId or (ii.Csoport_Id_Ugyfelelos is null and ui.Csoport_Id_Ugyfelelos is null) or ISNULL(ii.Csoport_Id_Ugyfelelos, ui.Csoport_Id_Ugyfelelos) = @SzervezetId)
                                                              )
							              then 0
                                          else 1 
									 end; '+char(13)+char(13);
	    SET @sqlcmd += N' drop table #jogosultak; '+char(13)+char(13);
      end;

---    SET @sqlcmd += 'insert into #timeTMP select ''90 - és végre elindul a Query '', current_timestamp; '+char(13)+char(13);
      
    /************************************************************
    * Tényleges select                                          *
    ************************************************************/
    SET @sqlcmd += N'
      select 
        #result.RowNumber,
        #result.Id,
        -- EREC_IraIratok.Id,
	    EREC_IraIratok.PostazasIranya,
        [dbo].fn_KodtarErtekNeve(''POSTAZAS_IRANYA'',  EREC_IraIratok.PostazasIranya,@Org) as PostazasIranyaNev,
        EREC_IraIratok.Alszam,
        EREC_IraIratok.Sorszam,
        EREC_IraIratok.UtolsoSorszam,
        EREC_IraIktatokonyvek.Nev as Nev,
        EREC_IraIktatokonyvek.Ev as Ev,
        EREC_IraIratok.Azonosito as IktatoSzam_Merge,
        EREC_UgyUgyiratok.Azonosito as Foszam_Merge,
        EREC_IraIktatokonyvek.MegkulJelzes,
        EREC_IraIktatokonyvek.Iktatohely,   
        EREC_UgyUgyiratok.Foszam,
        EREC_UgyUgyiratdarabok.EljarasiSzakasz,
        EREC_KuldKuldemenyek.Azonosito as ErkeztetoSzam,
        EREC_KuldKuldemenyek.Partner_Id_Bekuldo,
        EREC_KuldKuldemenyek.NevSTR_Bekuldo,
        EREC_KuldKuldemenyek.CimSTR_Bekuldo,
        EREC_KuldKuldemenyek.BeerkezesIdeje,
		-- BLG#1402:
		(select count(*) from EREC_Mellekletek m where m.IraIrat_Id = EREC_IraIratok.Id and getdate() between m.ErvKezd and m.ErvVege) as MellekletekSzama,
        /* NULL as MellekletekSzama,                --- #66 - 2017.09.05., BogI */
        /* NULL as MellekletekAdathordozoTipusa,    --- #66 - 2017.09.05., BogI */
        /* NULL as MellekletekAdathordozoFajtaja,   --- #66 - 2017.09.05., BogI */
        EREC_KuldKuldemenyek.PostazasIranya,
        /* [dbo].fn_KodtarErtekNeve(''KULDES_MODJA'',  EREC_KuldKuldemenyek.KuldesMod,@Org) as KuldesMod,  --- #66 - 2017.09.11., BogI */
		[dbo].fn_KodtarErtekNeve(''KULDEMENY_KULDES_MODJA'',  EREC_KuldKuldemenyek.KuldesMod,@Org) as KuldesMod,
        EREC_KuldKuldemenyek.LetrehozasIdo as ErkeztetesIdeje,
        /* [dbo].fn_KodtarErtekNeve(''KEZBESITES_MODJA'',  EREC_KuldKuldemenyek.KezbesitesModja,@Org) as KezbesitesModja_Nev,  --- #66 - 2017.09.11., BogI */
		[dbo].fn_KodtarErtekNeve(''KULD_KEZB_MODJA'',  EREC_KuldKuldemenyek.KezbesitesModja,@Org) as KezbesitesModja_Nev,
        EREC_KuldKuldemenyek.Targy,
        EREC_KuldKuldemenyek.Csoport_Id_Cimzett,
        [dbo].fn_GetCsoportNev(EREC_KuldKuldemenyek.Csoport_Id_Cimzett) as Csoport_Id_CimzettNev,
        EREC_UgyUgyiratok.LezarasDat as LezarasDat,
        EREC_UgyUgyiratok.Id as UgyiratId,
        EREC_UgyUgyiratok.Targy as Ugyirat_targy,
        CONVERT(nvarchar(10), EREC_UgyUgyiratok.ElintezesDat, 102) as ElintezesDat, 
        [dbo].fn_GetCsoportNev(EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez) as Ugyintezo_Nev,
        [dbo].fn_GetCsoportNev(EREC_UgyUgyiratok.Csoport_Id_Felelos) as Szervezeti_Egyseg,
        CONVERT(nvarchar(19), EREC_UgyUgyiratok.Hatarido, 120) as Ugyirat_Hatarido, 
        /* EREC_UgyUgyiratok.UgyintezesModja,  --- #66 - 2017.09.11., BogI */
		[dbo].fn_KodtarErtekNeve(''UGYINTEZES_ALAPJA'',  EREC_UgyUgyiratok.UgyintezesModja, @Org) as UgyintezesModja,
        /* NULL as Eloirat,   --- #66 - 2017.09.05., BogI */
        /* NULL as Utoirat,   --- #66 - 2017.09.05., BogI */
        (select TOP 1 EREC_HataridosFeladatok.Leiras AS MunkanaploKezelesiFeljegyzes 
		   from [dbo].EREC_HataridosFeladatok
          where EREC_UgyUgyiratok.Id = EREC_HataridosFeladatok.Obj_Id 
		    and EREC_HataridosFeladatok.AlTipus = ''13'') as KezelesiFelj,
	    CONVERT(nvarchar(10), EREC_UgyUgyiratok.LezarasDat, 102) as LezarasDat, 
        CONVERT(nvarchar(10), EREC_UgyUgyiratok.IrattarbaVetelDat, 102) as IrattarbaHelyezesDat,  
	    (select [dbo].fn_MergeIrattariTetelSzam(EREC_AgazatiJelek.Kod,EREC_IraIrattariTetelek.IrattariTetelszam,EREC_IraIrattariTetelek.MegorzesiIdo,EREC_IraIrattariTetelek.IrattariJel,@Org)
           from [dbo].EREC_IraIrattariTetelek, [dbo].EREC_Agazatijelek
          where EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_Agazatijelek.Id 
		    and EREC_UgyUgyiratok.IraIrattariTetel_Id = EREC_IraIrattariTetelek.Id) as ITSZ,
        (select TOP 1 Leiras 
           from [dbo].EREC_IraKezFeljegyzesek 
          where EREC_IraKezFeljegyzesek.IraIrat_Id = EREC_IraIratok.Id
          order by LetrehozasIdo desc) as KezelesiFeljegyzes,
        EREC_IraIratok.UgyUgyIratDarab_Id,
        EREC_IraIratok.Kategoria,
        EREC_IraIratok.HivatkozasiSzam,
        CONVERT(nvarchar(16), EREC_IraIratok.IktatasDatuma, 102) + '' '' + CONVERT(nvarchar(16), EREC_IraIratok.IktatasDatuma, 108) as IktatasDatuma,
        EREC_IraIratok.ExpedialasDatuma,
        EREC_IraIratok.ExpedialasModja,
        EREC_IraIratok.FelhasznaloCsoport_Id_Expedial,
        EREC_IraIratok.Targy as Targy1,
        EREC_IraIratok.Jelleg,
		-- BUG_2742
		dbo.fn_KodtarErtekNeve(''IRAT_JELLEG'', EREC_IraIratok.Jelleg,@Org) as JellegNev,
        CONVERT(nvarchar(10), EREC_IraIratok.SztornirozasDat, 102) as SztornirozasDat,
        EREC_IraIratok.FelhasznaloCsoport_Id_Iktato,
        [dbo].fn_GetCsoportNev(EREC_IraIratok.FelhasznaloCsoport_Id_Iktato) as FelhasznaloCsoport_Id_Iktato_Nev,   
        EREC_IraIratok.FelhasznaloCsoport_Id_Kiadmany,
        EREC_IraIratok.UgyintezesAlapja,
        [dbo].fn_KodtarErtekNeve(''UGYINTEZES_ALAPJA'', EREC_IraIratok.AdathordozoTipusa,@Org) as AdathordozoTipusa_Nev,
        EREC_IraIratok.Csoport_Id_Felelos,
        [dbo].fn_GetCsoportNev(EREC_IraIratok.Csoport_Id_Felelos) as Csoport_Id_FelelosNev,         
        EREC_IraIratok.FelhasznaloCsoport_Id_Orzo,
		-- BLG_1677
		[dbo].fn_GetCsoportNev(EREC_IraIratok.FelhasznaloCsoport_Id_Orzo) as Csoport_Id_OrzoNev,   
        EREC_IraIratok.KiadmanyozniKell, 
        EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez,
        EREC_IraIratok.Hatarido,
        [dbo].fn_GetCsoportNev(EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez) as FelhasznaloCsoport_Id_Ugyintezo_Nev,
        ISNULL([dbo].fn_GetCsoportNev(EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez),[dbo].fn_GetCsoportNev(EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez)) as FelhasznaloCsoport_Id_Ugyintezo_Nev_Print,
		-- BLG_619
		dbo.fn_GetCsoportNev(EREC_IraIratok.Csoport_Id_Ugyfelelos) as IratFelelos_Nev,
		dbo.fn_GetCsoportKod(EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos) as IratFelelos_SzervezetKod,
        EREC_IraIratok.MegorzesiIdo,
        EREC_IraIratok.IratFajta,
        EREC_IraIratok.Irattipus,
		-- BUG_2742
		dbo.fn_KodtarErtekNeve(''IRATTIPUS'', EREC_IraIratok.Irattipus,@Org) as IrattipusNev,
        EREC_IraIratok.Minosites,		
	    dbo.fn_KodtarErtekNeve(''IRAT_MINOSITES'', EREC_IraIratok.Minosites,@Org) as MinositesNev,
        EREC_IraIratok.IratMetaDef_Id,
        EREC_IraIratok.KuldKuldemenyek_Id,
        EREC_IraIratok.Ugyirat_Id,
        EREC_IraIratok.Munkaallomas,
        EREC_IraIratok.Allapot,
	    EREC_IraIratok.Elintezett,
        [dbo].fn_KodtarErtekNeve(''IRAT_ALLAPOT'', EREC_IraIratok.Allapot,@Org) as Allapot_Nev,
		-- BLG_1020
		EREC_IraIratok.IntezesiIdo,
		EREC_IraIratok.IntezesiIdoegyseg,
		EREC_IraIratok.Minosito,
		KRT_PartnerMinosito.Nev as MinositoNev,
		EREC_IraIratok.MinositoSzervezet,
		KRT_PartnerMinositoSzerv.Nev as MinositoSzervezetNev,
		EREC_IraIratok.MinositesFelulvizsgalatBizTag,
	    EREC_IraIratok.MinositesErvenyessegiIdeje,
	    EREC_IraIratok.TerjedelemMennyiseg,
	    EREC_IraIratok.TerjedelemMennyisegiEgyseg,
	    EREC_IraIratok.TerjedelemMegjegyzes,';

		/*-----BLG 5889 Minősítési jelölés alapja mező*/
		SET @sqlcmd += '(select top 1 Ertek 
		                   from [dbo].[EREC_ObjektumTargyszavai] 
                          where [EREC_ObjektumTargyszavai].Obj_Id = EREC_IraIratok.Id 
							and [EREC_ObjektumTargyszavai].Targyszo_id = ''BCB57F8B-869E-460B-A2DE-752178F2B1DC''
                            and getdate() BETWEEN [EREC_ObjektumTargyszavai].ErvKezd AND [EREC_ObjektumTargyszavai].ErvVege) as MinositesJelolesAlapja,
                       ';

    --- A bizalmas iratok kezelésének beillesztése
    if [dbo].fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0           --  and @Jogosultak = '1'                  --- nem lehetne lecserélni @SzervezetId -ra ??? a kettő nem feltétlen ugyanaz !
      begin
        SET @sqlcmd += '(select COUNT(1) 
		                   from [dbo].[EREC_Csatolmanyok] 
		                  inner join @iratok_executor ie ON ie.Id= EREC_Csatolmanyok.IraIrat_Id
                          where [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id
                            and getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege) as CsatolmanyCount,
                       ';
        SET @sqlcmd += 'Dokumentum_Id = 
					CASE 
						WHEN (
							SELECT COUNT(*)
							from [dbo].[EREC_Csatolmanyok] 
							inner join @iratok_executor ie ON ie.Id= EREC_Csatolmanyok.IraIrat_Id
							where [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id
								  and getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
							 ) >0
						THEN
							(select TOP 1 EREC_Csatolmanyok.Dokumentum_Id
							from [dbo].EREC_Csatolmanyok
							where [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id
							  and getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
						ELSE
							CAST(NULL AS UNIQUEIDENTIFIER) 
					END,
                 ';
      end;
    else
      begin
        SET @sqlcmd += '(select COUNT(1) 
		                   from [dbo].[EREC_Csatolmanyok] 
						  where [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id 
						    and getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege) as CsatolmanyCount,
                       ';
        SET @sqlcmd += 'Dokumentum_Id = 
					CASE 
					WHEN (
						SELECT COUNT(*)
						  from [dbo].[EREC_Csatolmanyok] 
											   where [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id 
											     and getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
						 ) >0
					THEN
						(select TOP 1 EREC_Csatolmanyok.Dokumentum_Id
						from [dbo].EREC_Csatolmanyok
						where [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id
						  and getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
					ELSE
						CAST(NULL AS UNIQUEIDENTIFIER) 
					END,
              ';
      end;

    -- CR#1796: (CsatolmanyCount, lásd fent):
    -- 1. csak a csatolmányokat számoljuk, a mellékleteket nem
    -- 2. az Iratba bekerĂĽlt az UGYIRAT_ID, ezért szükségtelenné váltak a JOIN műveletek

    SET @sqlcmd += ' 
        (select COUNT(1) 
		   from [dbo].EREC_UgyiratObjKapcsolatok 
          where (EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt  = EREC_IraIratok.Id
                 OR EREC_UgyiratObjKapcsolatok.Obj_Id_Elozmeny = EREC_IraIratok.Id )
            and getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege
		) as CsatolasCount,
        (select ii.[Azonosito] 
		   from [dbo].EREC_IraIratok ii      
           left join [dbo].EREC_UgyiratObjKapcsolatok uok ON uok.Obj_Id_Kapcsolt = ii.Id
                 and uok.Obj_Type_Kapcsolt = ''EREC_IraIratok''
                 and uok.KapcsolatTipus = ''06''
				 and getdate() between uok.ErvKezd and uok.ErvVege
          where getdate() between ii.ErvKezd and ii.ErvVege
            and ii.Id =(select TOP 1 Obj_Id_Kapcsolt 
			              from [dbo].EREC_UgyiratObjKapcsolatok 
                         where EREC_UgyiratObjKapcsolatok.Obj_Id_Elozmeny  = EREC_IraIratok.Id 
						   and EREC_UgyiratObjKapcsolatok.KapcsolatTipus = ''06''
                           and EREC_IraIratok.Allapot = ''06'' 
						   and EREC_UgyiratObjKapcsolatok.Obj_Type_Kapcsolt = ''EREC_IraIratok''
                           and getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege
					   )
	    ) as UjIktatoSzam,
        (select ii.Azonosito 
		   from [dbo].EREC_IraIratok ii     
           left join [dbo].EREC_UgyiratObjKapcsolatok uok ON uok.Obj_Id_Elozmeny = ii.Id
                 and uok.Obj_Type_Kapcsolt = ''EREC_IraIratok''
                 and uok.KapcsolatTipus = ''06''
				 and getdate() between uok.ErvKezd and uok.ErvVege
          where getdate() between ii.ErvKezd and ii.ErvVege
            and ii.Id =(select TOP 1 Obj_Id_Elozmeny 
			              from [dbo].EREC_UgyiratObjKapcsolatok 
                         where EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt  = EREC_IraIratok.Id 
						   and EREC_UgyiratObjKapcsolatok.KapcsolatTipus = ''06''
                           and EREC_UgyiratObjKapcsolatok.Obj_Type_Elozmeny = ''EREC_IraIratok''
                           and getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege
						)
		) as RegiIktatoSzam,';

    SET @sqlcmd += '
        EREC_PldIratPeldanyok.Id as ElsoIratPeldany_Id,
        EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo as ElsoIratPeldanyOrzo_Id,
        EREC_PldIratPeldanyok.Orzo_Nev as ElsoIratPeldanyOrzo_Nev,
        EREC_PldIratPeldanyok.Allapot as ElsoIratPeldanyAllapot,
        ElsoIratPeldanyAllapot_Nev = 
        CASE EREC_PldIratPeldanyok.Allapot
          when ''50'' then [dbo].fn_KodtarErtekNeve(''IRATPELDANY_ALLAPOT'', EREC_PldIratPeldanyok.Allapot,@Org)  
                           + '' (''+[dbo].fn_KodtarErtekNeve(''IRATPELDANY_ALLAPOT'', EREC_PldIratPeldanyok.TovabbitasAlattAllapot,@Org)+'')''       
                      else [dbo].fn_KodtarErtekNeve(''IRATPELDANY_ALLAPOT'', EREC_PldIratPeldanyok.Allapot,@Org)
        END,
        EREC_PldIratPeldanyok.Felelos_Nev as ElsoIratPeldanyFelelos_Nev,
        EREC_PldIratPeldanyok.BarCode as ElsoIratPeldanyBarCode,
        EREC_PldIratPeldanyok.CimSTR_Cimzett, 
        EREC_PldIratPeldanyok.NevSTR_Cimzett,
        /* EREC_PldIratPeldanyok.KuldesMod as PeldanyKuldesMod,   --- #66 - 2017.09.11., BogI */
		[dbo].fn_KodtarErtekNeve(''KULDEMENY_KULDES_MODJA'', EREC_PldIratPeldanyok.KuldesMod, @Org) as PeldanyKuldesMod,
        EREC_PldIratPeldanyok.PostazasDatuma,
    ';

    -- Függvényhívás helyett közvetlen lekérdezés (subselect ) - másképpen nagyon lassú ...
    -- dbo.fn_GetEREC_HataridosFeladatokCount(EREC_IraIratok.Id, ''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(ISNULL(@FelhasznaloSzervezet_Id,NEWID()) AS CHAR(36)) + ''') as FeladatCount,
    SET @sqlcmd += ' (select * from [dbo].fn_GetEREC_HataridosFeladatokCountTable(EREC_IraIratok.Id, @FelhasznaloId, ISNULL(@SzervezetId,NEWID()))) as FeladatCount,';
	                                                                          --- nem lehetne lecserélni @SzervezetId -ra ??? a kettő nem feltétlen ugyanaz !
    SET @sqlcmd += '
	    EREC_IraIratok.SelejtezesDat,
	    EREC_IraIratok.FelhCsoport_Id_Selejtezo,
	    EREC_IraIratok.LeveltariAtvevoNeve,
	    EREC_IraIratok.Ver,
        EREC_IraIratok.Note,
        EREC_IraIratok.Stat_id,
        EREC_IraIratok.ErvKezd,
        EREC_IraIratok.ErvVege,
        EREC_IraIratok.Letrehozo_id,
        EREC_IraIratok.LetrehozasIdo,
        EREC_IraIratok.Modosito_id,
        EREC_IraIratok.ModositasIdo,
        EREC_IraIratok.Zarolo_id,
        EREC_IraIratok.ZarolasIdo,
        EREC_IraIratok.Tranz_id,
        EREC_IraIratok.UIAccessLog_id,
        EREC_IraIratok.Munkaallomas,
        EREC_IraIratok.UgyintezesModja,
		-- BLG_1677
		[dbo].fn_KodtarErtekNeve(''IRAT_UGYINT_MODJA'', EREC_IraIratok.UgyintezesModja, @Org) as UgyintezesMod_nev,
        EREC_IraIratok.IntezesIdopontja --,
		-- BUG_11670
  --      (select EREC_IraOnkormAdatok.UgyFajtaja 
		--   from [dbo].EREC_IraOnkormAdatok 
  --        where EREC_IraOnkormAdatok.IraIratok_Id = EREC_IraIratok.Id
		--) as UgyFajtaja 
		, EREC_IraIratok.IratHatasaUgyintezesre
		,[dbo].fn_KodtarErtekNeve(''IRAT_HATASA_UGYINTEZESRE'', EREC_IraIratok.IratHatasaUgyintezesre, @Org) as IratHatasaUgyintezesreNev
		,[dbo].[fn_IratHatasaTipusa] (EREC_IraIratok.IratHatasaUgyintezesre) as IratHatasaUgyintezesreTipus
		, EREC_IraIratok.FelfuggesztesOka
	    , EREC_IraIratok.LezarasOka

		,EREC_IraIratok.UgyintezesKezdoDatuma
		,[dbo].fn_KodtarErtekNeve(''ELJARASI_SZAKASZ_FOK'', EREC_IraIratok.EljarasiSzakasz, @Org) as EljarasiSzakaszFokNev
		,EREC_IraIratok.HatralevoNapok
        ,EREC_IraIratok.HatralevoMunkaNapok
		,EREC_IraIratok.Ugy_Fajtaja
		,[dbo].fn_KodtarErtekNeve(''UGY_FAJTAJA'', EREC_IraIratok.Ugy_Fajtaja, @Org) as Ugy_FajtajaNev
	';
	  
    if @IsUgyiratTerkepQuery = '1'
      SET @sqlcmd += ',
        [dbo].fn_ExistsHatosagiAdat(EREC_IraIratok.Id) as ExistsHatosagiAdat ';

    if @ColumnNames is not null and @ColumnNames != ''
      SET @sqlcmd += ',' + replace(@ColumnNames, '[', 'EREC_ObjektumTargyszavai.[');

	  --statisztika

	--SET @sqlcmd = @sqlcmd + ',
	--	(select KRT_Kodtarak.Nev 
	--	from EREC_ObjektumTargyszavai
	--	left join KRT_Kodcsoportok on KRT_Kodcsoportok.Kod = ''UGYSTATISZTIKA_T1''
 --       left join KRT_Kodtarak on KRT_Kodtarak.Kodcsoport_Id = KRT_Kodcsoportok.Id and KRT_Kodtarak.Kod = EREC_ObjektumTargyszavai.Ertek COLLATE HUNGARIAN_CS_AS and KRT_Kodtarak.Org=@Org
	--	where EREC_ObjektumTargyszavai.Targyszo_Id = ''A92E4382-6D67-4A58-98B2-FAC82A138037'' and EREC_ObjektumTargyszavai.Obj_Id = EREC_IraIratok.Id ) AS IT1,
	--	(select KRT_Kodtarak.Nev 
	--	from EREC_ObjektumTargyszavai
	--	left join KRT_Kodcsoportok on KRT_Kodcsoportok.Kod = ''UGYSTATISZTIKA_T2''
 --       left join KRT_Kodtarak on KRT_Kodtarak.Kodcsoport_Id = KRT_Kodcsoportok.Id and KRT_Kodtarak.Kod = EREC_ObjektumTargyszavai.Ertek COLLATE HUNGARIAN_CS_AS and KRT_Kodtarak.Org=@Org
	--	where EREC_ObjektumTargyszavai.Targyszo_Id = ''C148F027-8BDD-4B0C-971B-C94CD4322002'' and EREC_ObjektumTargyszavai.Obj_Id = EREC_IraIratok.Id ) AS IT2,
	--	(select KRT_Kodtarak.Nev 
	--	from EREC_ObjektumTargyszavai
	--	left join KRT_Kodcsoportok on KRT_Kodcsoportok.Kod = ''UGYSTATISZTIKA_T3''
 --       left join KRT_Kodtarak on KRT_Kodtarak.Kodcsoport_Id = KRT_Kodcsoportok.Id and KRT_Kodtarak.Kod = EREC_ObjektumTargyszavai.Ertek COLLATE HUNGARIAN_CS_AS and KRT_Kodtarak.Org=@Org
	--	where EREC_ObjektumTargyszavai.Targyszo_Id = ''4CAE4517-782F-47E1-92F8-8BB45A5406D7'' and EREC_ObjektumTargyszavai.Obj_Id = EREC_IraIratok.Id ) AS IT3
	--	'
    SET @sqlcmd += ' 
        from [dbo].EREC_IraIratok as EREC_IraIratok
       inner join #result on #result.Id = EREC_IraIratok.Id           
        left join [dbo].EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
        left join [dbo].EREC_UgyUgyiratok as EREC_UgyUgyiratok ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id  
        left join [dbo].EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek ON EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id  
        left join [dbo].[EREC_KuldKuldemenyek] AS EREC_KuldKuldemenyek ON EREC_IraIratok.[KuldKuldemenyek_Id] = EREC_KuldKuldemenyek.[Id] 
		left join [dbo].KRT_Partnerek as KRT_PartnerMinosito on EREC_IraIratok.Minosito = KRT_PartnerMinosito.Id
		left join [dbo].KRT_Partnerek as KRT_PartnerMinositoSzerv on EREC_IraIratok.MinositoSzervezet = KRT_PartnerMinositoSzerv.Id';

    SET @sqlcmd += ' OUTER APPLY (select TOP 1 Id, Allapot, TovabbitasAlattAllapot, FelhasznaloCsoport_Id_Orzo,
                                               (select TOP 1 Nev from [dbo].KRT_Csoportok cs where p.FelhasznaloCsoport_Id_Orzo=cs.Id and getdate() between cs.ErvKezd and cs.ErvVege) as Orzo_Nev,
                                               (select TOP 1 Nev from [dbo].KRT_Csoportok cs where p.Csoport_Id_Felelos=cs.Id and getdate() between cs.ErvKezd and cs.ErvVege) as Felelos_Nev,
                                               p.BarCode, CimSTR_Cimzett, NevSTR_Cimzett, KuldesMod, PostazasDatuma 
								     from [dbo].EREC_PldIratPeldanyok p
                                    where p.IraIrat_Id=EREC_IraIratok.Id 
									  and p.Sorszam=1
                                 ) as EREC_PldIratPeldanyok
    ';
	/* --- átírtam LEFT OUTER JOIN -ra, de NEM kimérhető a sebességkülönbség - az OUTER APPLY mintha egy hajszállal gyorsabb lenne
    SET @sqlcmd += ' 
        left join (select TOP 1 Id, Allapot, TovabbitasAlattAllapot, FelhasznaloCsoport_Id_Orzo,
                                (select TOP 1 Nev from KRT_Csoportok cs 
								  where p.FelhasznaloCsoport_Id_Orzo=cs.Id 
								    and getdate() between cs.ErvKezd and cs.ErvVege
								) as Orzo_Nev,
                                (select TOP 1 Nev from KRT_Csoportok cs 
								  where p.Csoport_Id_Felelos=cs.Id 
								    and getdate() between cs.ErvKezd and cs.ErvVege
								) as Felelos_Nev,
                                BarCode, CimSTR_Cimzett, NevSTR_Cimzett, KuldesMod, PostazasDatuma, IraIrat_Id
					 from [dbo].EREC_PldIratPeldanyok p
                    where p.Sorszam = 1 
                  ) as EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id 
    ';
    --- */

   if @ColumnNames is not null and @ColumnNames != ''
     SET @sqlcmd += 'left join #pivoted_meta as EREC_ObjektumTargyszavai on EREC_IraIratok.Id=EREC_ObjektumTargyszavai.Obj_Id ';
   
   SET @sqlcmd +='  where RowNumber between @firstRow and @lastRow
                    order by #result.RowNumber; '+char(13)+char(13);

---    SET @sqlcmd += 'insert into #timeTMP select ''95 - Kell a Találatok száma és lapszám is'', current_timestamp; '+char(13)+char(13);

    --- Találatok száma és lapszám
    /* SET @sqlcmd += N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter; '+char(13)+char(13);                  --- Miért a #filter-ből kérdezzük le ??? */
	SET @sqlcmd += N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result; '+char(13)+char(13);                  --- #filter nem lett megépítve

---    SET @sqlcmd += 'insert into #timeTMP select ''99 - ITT a VÉGE'', current_timestamp; '+char(13)+char(13);

--- ez itt nyomkövetés - KI KELL kapcsolni éles futáson
/*
print LEFT(@sqlcmd,4000)
print substring(@sqlcmd, 4001,4000)
print substring(@sqlcmd, 8001,4000)
print substring(@sqlcmd, 12001,4000)
print substring(@sqlcmd, 16001,4000)
print substring(@sqlcmd, 20001,4000)
print substring(@sqlcmd, 24001,4000)
print substring(@sqlcmd, 28001,4000)
print substring(@sqlcmd, 32001,4000)
print substring(@sqlcmd, 36001,4000)
print substring(@sqlcmd, 40001,4000)
print substring(@sqlcmd, 44001,4000)
*/
---

    execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @ExecutorUserId uniqueidentifier, @FelhasznaloSzervezet_Id uniqueidentifier, 
	                                @SelectedRowId uniqueidentifier, @Org uniqueidentifier, @FelhasznaloId uniqueidentifier, @SzervezetId uniqueidentifier'
            ,@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @ExecutorUserId = @ExecutorUserId, @FelhasznaloSzervezet_Id = @FelhasznaloSzervezet_Id, @SelectedRowId = @SelectedRowId, @Org = @Org
            ,@FelhasznaloId = @FelhasznaloId, @SzervezetId = @SzervezetId;

--- ez itt nyomkövetés - KI KELL kapcsolni éles futáson
---select * from #timeTMP order by 1;

  END TRY
  BEGIN CATCH
    declare @errorSeverity INT,
            @errorState INT;
    declare @errorCode NVARCHAR(1000);
	
    SET @errorSeverity = ERROR_SEVERITY()
    SET @errorState = ERROR_STATE()

    if ERROR_NUMBER() < 50000 
        SET @errorCode = '[' + CONVERT(NVARCHAR(10), ERROR_NUMBER())
            + '] ' + ERROR_MESSAGE();
    else 
        SET @errorCode = ERROR_MESSAGE();

    if @errorState = 0 
      SET @errorState = 1;

    RAISERROR ( @errorCode, @errorSeverity, @errorState );
  END CATCH;
END;
