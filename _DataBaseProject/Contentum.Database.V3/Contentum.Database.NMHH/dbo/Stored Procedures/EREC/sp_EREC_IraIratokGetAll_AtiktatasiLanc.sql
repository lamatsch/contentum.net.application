﻿CREATE procedure [dbo].[sp_EREC_IraIratokGetAll_AtiktatasiLanc]
		@IratId	uniqueidentifier
as

begin

BEGIN TRY

    set nocount on;
    

    --legfobb szülo megkeresése az átiktatási láncban (régebbi iratok) -> [Obj_Id_Elozmeny] felé lépegetünk
	WITH ParentHiearchy  AS
	(
	   -- Base case
	   SELECT Id, 
			  1 as ParentHierarchyLevel
	   FROM [EREC_IraIratok]
	   WHERE Id = @IratId

	   UNION ALL

	   -- Recursive step
	   SELECT iratKapcs.[Obj_Id_Elozmeny] AS Id, 
			  ph.ParentHierarchyLevel + 1 AS ParentHierarchyLevel
	   FROM [EREC_UgyiratObjKapcsolatok] as iratKapcs
		  INNER JOIN ParentHiearchy as ph ON
			 iratKapcs.[Obj_Id_Kapcsolt] = ph.Id
	   WHERE GETDATE() BETWEEN iratKapcs.[ErvKezd] AND iratKapcs.[ErvVege]
		AND iratKapcs.[KapcsolatTipus] = '06'		
	),	
    
    -- a szülotol lefelé végigmegyünk a láncon -> [Obj_Id_Kapcsolt] felé lépegetünk
	AtiktatasHierarchy  AS
	(
	   -- Base case (A legfelso irat a láncban)
	   SELECT TOP 1 ParentHiearchy.Id AS Id,
			  1 as HierarchyLevel
	   FROM ParentHiearchy
	   WHERE ParentHiearchy.ParentHierarchyLevel = (SELECT MAX(ParentHierarchyLevel) FROM ParentHiearchy)	   

	   UNION ALL

	   -- Recursive step
	   SELECT iratKapcs.[Obj_Id_Kapcsolt] AS Id,
		  ih.HierarchyLevel + 1 AS HierarchyLevel
	   FROM [EREC_UgyiratObjKapcsolatok] as iratKapcs
		  INNER JOIN AtiktatasHierarchy as ih ON
			  iratKapcs.[Obj_Id_Elozmeny] = ih.Id
	   WHERE GETDATE() BETWEEN iratKapcs.[ErvKezd] AND iratKapcs.[ErvVege]
			AND iratKapcs.[KapcsolatTipus] = '06'
	)


--	SELECT *
--	FROM ParentHiearchy
		
	SELECT 
		HierarchyLevel, 
		[EREC_IraIratok].[Id],
		[EREC_IraIratok].[Azonosito] as IktatoSzam_Merge,
		[EREC_IraIratok].[IktatasDatuma] AS IktatasDatuma,
		dbo.[fn_GetCsoportNev]([EREC_IraIratok].[FelhasznaloCsoport_Id_Iktato]) AS Iktato_Nev
	FROM AtiktatasHierarchy
		JOIN [EREC_IraIratok]
			ON [EREC_IraIratok].[Id] = AtiktatasHierarchy.Id		
		
		
END TRY
BEGIN CATCH
	
	-- hibakezelés:
	EXEC usp_HandleException
 
END CATCH

end