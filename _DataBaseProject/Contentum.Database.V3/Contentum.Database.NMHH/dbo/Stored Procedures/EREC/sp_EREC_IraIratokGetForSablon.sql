﻿
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		MP
-- Create date: 2018.10.12.
-- Description:	IratAdatok kigyüjtése sablon készítéshez
-- =============================================

IF OBJECT_ID('dbo.sp_EREC_IraIratokGetForSablon') IS NOT NULL 
	DROP PROCEDURE [dbo].[sp_EREC_IraIratokGetForSablon]
GO

CREATE PROCEDURE [dbo].[sp_EREC_IraIratokGetForSablon] 	
		@Id uniqueidentifier,
		@ExecutorUserId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Irat_Id uniqueidentifier
	DECLARE @Org uniqueidentifier
	DECLARE @Ugyirat_Id uniqueidentifier
	DECLARE @UgyiratDarab_Id uniqueidentifier
	DECLARE @IratPeldany_Id uniqueidentifier
	DECLARE @BejovoKuldemeny_Id uniqueidentifier

	SET @Irat_Id = @Id
	SET @Org = (select Org from EREC_IraIktatoKonyvek ik inner join EREC_UgyUgyiratok u on u.IraIktatokonyv_Id = ik.Id inner join EREC_IraIratok i on i.Ugyirat_Id = u.Id where i.Id = @Irat_Id)
	SET @Ugyirat_Id = (select ugyirat_id from EREC_IraIratok where id = @Irat_Id)
	SET @UgyiratDarab_Id = (select UgyUgyIratDarab_Id from EREC_IraIratok where id = @Irat_Id)
	SET @IratPeldany_Id = (select id from EREC_PldIratPeldanyok where IraIrat_Id = @Irat_Id and sorszam = '1')
	SET @BejovoKuldemeny_Id = (select id from EREC_KuldKuldemenyek where IraIratok_Id = @Irat_Id)

	SELECT KOD, ERTEK FROM
	(
		SELECT KOD, ERTEK FROM
		(
			SELECT
			--irat
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('POSTAZAS_IRANYA',i.PostazasIranya,@Org),'') AS varchar(255)) AS IRAT_PostazasIranya,
			CAST(COALESCE(i.Alszam,'') AS varchar(255)) AS IRAT_Alszam,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('IRATKATEGORIA',i.Kategoria,@Org),'') AS varchar(255)) AS IRAT_Kategoria,
			CAST(COALESCE(i.HivatkozasiSzam,'') AS varchar(255)) AS IRAT_HivatkozasiSzam,
			CAST(COALESCE(CONVERT(nvarchar(10),i.IktatasDatuma, 102),'') AS varchar(255)) AS IRAT_IktatasDatuma,
			CAST(COALESCE(i.Targy,'') AS varchar(255)) AS IRAT_Targy,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('IRAT_JELLEG',i.Jelleg,@Org),'') AS varchar(255)) AS IRAT_Jelleg,
			CAST(COALESCE(i.SztornirozasDat,'') AS varchar(255)) AS IRAT_SztornirozasDatuma,
			CAST(COALESCE(dbo.fn_GetCsoportNev(i.FelhasznaloCsoport_Id_Iktato),'') AS varchar(255)) AS IRAT_FelhasznaloCsoport_Id_Iktato,
			CAST(COALESCE(dbo.fn_GetCsoportNev(i.FelhasznaloCsoport_Id_Kiadmany),'') AS varchar(255)) AS IRAT_FelhasznaloCsoport_Id_Kiadmany,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('UGYINTEZES_ALAPJA',i.UgyintezesAlapja,@Org),'') AS varchar(255)) AS IRAT_UgyintezesAlapja,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('ADATHORDOZO_TIPUSA',i.AdathordozoTipusa,@Org),'') AS varchar(255)) AS IRAT_AdathordozoTipusa,
			CAST(COALESCE(dbo.fn_GetCsoportNev(i.Csoport_Id_Felelos),'') AS varchar(255)) AS IRAT_Felelos,
			CAST(COALESCE(dbo.fn_GetCsoportNev(i.FelhasznaloCsoport_Id_Orzo),'') AS varchar(255)) AS IRAT_FelhasznaloCsoport_Id_Orzo,
			CAST(COALESCE( CASE WHEN i.KiadmanyozniKell = '1' THEN 'Igen' ELSE 'Nem' END,'') AS varchar(255)) AS IRAT_KiadmanyozniKell,
			CAST(COALESCE(dbo.fn_GetCsoportNev(i.FelhasznaloCsoport_Id_Ugyintez),'') AS varchar(255)) AS IRAT_Ugyintezo,
			CAST(COALESCE(CONVERT(nvarchar(10),i.Hatarido, 102),'') AS varchar(255)) AS IRAT_Hatarido,
			CAST(COALESCE(i.MegorzesiIdo,'') AS varchar(255)) AS IRAT_MegorzesiIdo,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('IRAT_FAJTA',i.IratFajta,@Org),'') AS varchar(255)) AS IRAT_IratFajta,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('IRATTIPUS',i.Irattipus,@Org),'') AS varchar(255)) AS IRAT_Irattipus,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('IRAT_ALLAPOT',i.Allapot,@Org),'') AS varchar(255)) AS IRAT_Allapot,
			CAST(COALESCE(i.Azonosito,'') AS varchar(255)) AS IRAT_Iktatoszam,
			CAST(COALESCE(CONVERT(nvarchar(10),i.IrattarbaKuldDatuma, 102),'') AS varchar(255)) AS IRAT_IrattarbaKuldDatuma,
			CAST(COALESCE(CONVERT(nvarchar(10),i.IrattarbaVetelDat, 102),'') AS varchar(255)) AS IRAT_IrattarbaVetelDat,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('IRAT_MINOSITES',i.Minosites,@Org),'') AS varchar(255)) AS IRAT_Minosites,
			CAST(COALESCE(i.Munkaallomas,'') AS varchar(255)) AS IRAT_IktatoMunkaallomas,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('IRAT_UGYINT_MODJA',i.UgyintezesModja,@Org),'') AS varchar(255)) AS IRAT_UgyintezesModja,
			CAST(COALESCE(i.IntezesIdopontja,'') AS varchar(255)) AS IRAT_IntezesIdopontja,
			CAST(COALESCE( CASE WHEN i.Elintezett = '1' THEN 'Igen' ELSE 'Nem' END,'') AS varchar(255)) AS IRAT_Elintezett,
			--kuldemeny
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('KULDEMENY_ALLAPOT',bk.Allapot,@Org),'') AS varchar(255)) AS BEJOVO_KULD_Allapot,
			CAST(COALESCE(CONVERT(nvarchar(10),bk.BeerkezesIdeje, 102),'') AS varchar(255)) AS BEJOVO_KULD_BeerkezesIdeje,
			CAST(COALESCE(CONVERT(nvarchar(10),bk.FelbontasDatuma, 102),'') AS varchar(255)) AS BEJOVO_KULD_FelbontasDatuma,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('KULDEMENY_KULDES_MODJA',bk.KuldesMod,@Org),'') AS varchar(255)) AS BEJOVO_KULD_KuldesMod,
			CAST(COALESCE(bk.Erkezteto_Szam,'') AS varchar(255)) AS BEJOVO_KULD_Erkezteto_Szam,
			CAST(COALESCE(bk.HivatkozasiSzam,'') AS varchar(255)) AS BEJOVO_KULD_HivatkozasiSzam,
			CAST(COALESCE(bk.Targy,'') AS varchar(255)) AS BEJOVO_KULD_Targy,
			CAST(COALESCE(bk.Tartalom,'') AS varchar(255)) AS BEJOVO_KULD_Tartalom,
			CAST(COALESCE(bk.RagSzam,'') AS varchar(255)) AS BEJOVO_KULD_RagSzam,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('SURGOSSEG',bk.Surgosseg,@Org),'') AS varchar(255)) AS BEJOVO_KULD_Surgosseg,
			CAST(COALESCE(CONVERT(nvarchar(10),bk.BelyegzoDatuma, 102),'') AS varchar(255)) AS BEJOVO_KULD_BelyegzoDatuma,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('IRAT_UGYINT_MODJA',bk.UgyintezesModja,@Org),'') AS varchar(255)) AS BEJOVO_KULD_UgyintezesModja,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('POSTAZAS_IRANYA',bk.PostazasIranya,@Org),'') AS varchar(255)) AS BEJOVO_KULD_PostazasIranya,
			CAST(COALESCE(dbo.fn_GetCsoportNev(bk.Tovabbito),'') AS varchar(255)) AS BEJOVO_KULD_Tovabbito,
			CAST(COALESCE(bk.PeldanySzam,'') AS varchar(255)) AS BEJOVO_KULD_PeldanySzam,
			CAST(COALESCE( CASE WHEN bk.IktatniKell = '1' THEN 'Igen' ELSE 'Nem' END,'') AS varchar(255)) AS BEJOVO_KULD_IktatniKell,
			CAST(COALESCE(bk.Iktathato,'') AS varchar(255)) AS BEJOVO_KULD_Iktathato,
			CAST(COALESCE(CONVERT(nvarchar(10),bk.SztornirozasDat, 102),'') AS varchar(255)) AS BEJOVO_KULD_SztornirozasDat,
			CAST(COALESCE(bk.Erkeztetes_Ev,'') AS varchar(255)) AS BEJOVO_KULD_Erkeztetes_Ev,
			CAST(COALESCE(dbo.fn_GetCsoportNev(bk.Csoport_Id_Cimzett),'') AS varchar(255)) AS BEJOVO_KULD_Csoport_Id_Cimzett,
			CAST(COALESCE(dbo.fn_GetCsoportNev(bk.Csoport_Id_Felelos),'') AS varchar(255)) AS BEJOVO_KULD_Csoport_Id_Felelos,
			CAST(COALESCE(dbo.fn_GetCsoportNev(bk.FelhasznaloCsoport_Id_Expedial),'') AS varchar(255)) AS BEJOVO_KULD_FelhasznaloCsoport_Id_Expedial,
			CAST(COALESCE(dbo.fn_GetCsoportNev(bk.FelhasznaloCsoport_Id_Orzo),'') AS varchar(255)) AS BEJOVO_KULD_Orzo,
			CAST(COALESCE(dbo.fn_GetCsoportNev(bk.FelhasznaloCsoport_Id_Atvevo),'') AS varchar(255)) AS BEJOVO_KULD_Atvevo,
			CAST(COALESCE(bk.CimSTR_Bekuldo,'') AS varchar(255)) AS BEJOVO_KULD_BekuldoCime,
			CAST(COALESCE(bk.NevSTR_Bekuldo,'') AS varchar(255)) AS BEJOVO_KULD_BekuldoNeve,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('ADATHORDOZO_TIPUSA',bk.AdathordozoTipusa,@Org),'') AS varchar(255)) AS BEJOVO_KULD_AdathordozoTipusa,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('ELSODLEGES_ADATHORDOZO',bk.ElsodlegesAdathordozoTipusa,@Org),'') AS varchar(255)) AS BEJOVO_KULD_ElsodlegesAdathordozoTipusa,
			CAST(COALESCE(bk.BarCode,'') AS varchar(255)) AS BEJOVO_KULD_Vonalkod,
			CAST(COALESCE(dbo.fn_GetCsoportNev(bk.FelhasznaloCsoport_Id_Bonto),'') AS varchar(255)) AS BEJOVO_KULD_Bonto,
			CAST(COALESCE(dbo.fn_GetCsoportNev(bk.CsoportFelelosEloszto_Id),'') AS varchar(255)) AS BEJOVO_KULD_Eloszto,
			CAST(COALESCE(bk.BontasiMegjegyzes,'') AS varchar(255)) AS BEJOVO_KULD_BontasiMegjegyzes,
			CAST(COALESCE(bk.Tipus,'') AS varchar(255)) AS BEJOVO_KULD_Tipus,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('IRAT_MINOSITES',bk.Minosites,@Org),'') AS varchar(255)) AS BEJOVO_KULD_Minosites,
			CAST(COALESCE(bk.MegtagadasIndoka,'') AS varchar(255)) AS BEJOVO_KULD_MegtagadasIndoka,
			CAST(COALESCE(dbo.fn_GetCsoportNev(bk.Megtagado_Id),'') AS varchar(255)) AS BEJOVO_KULD_IktatastMegtagado,
			CAST(COALESCE(CONVERT(nvarchar(10),bk.MegtagadasDat, 102),'') AS varchar(255)) AS BEJOVO_KULD_MegtagadasDat,
			CAST(COALESCE(bk.Azonosito,'') AS varchar(255)) AS BEJOVO_KULD_Azonosito,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('KULDEMENY_BORITO_TIPUS',bk.BoritoTipus,@Org),'') AS varchar(255)) AS BEJOVO_KULD_BoritoTipus,
			CAST(COALESCE( CASE WHEN bk.MegorzesJelzo = '1' THEN 'Igen' ELSE 'Nem' END,'') AS varchar(255)) AS BEJOVO_KULD_MegorzesJelzo,
			CAST(COALESCE( CASE WHEN bk.IktatastNemIgenyel = '1' THEN 'Igen' ELSE 'Nem' END,'') AS varchar(255)) AS BEJOVO_KULD_IktatastNemIgenyel,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('KULD_KEZB_MODJA',bk.KezbesitesModja,@Org),'') AS varchar(255)) AS BEJOVO_KULD_KezbesitesModja,
			CAST(COALESCE(bk.Munkaallomas,'') AS varchar(255)) AS BEJOVO_KULD_Munkaallomas,
			CAST(COALESCE( CASE WHEN bk.SerultKuldemeny = '1' THEN 'Igen' ELSE 'Nem' END,'') AS varchar(255)) AS BEJOVO_KULD_SerultKuldemeny,
			CAST(COALESCE( CASE WHEN bk.TevesCimzes = '1' THEN 'Igen' ELSE 'Nem' END,'') AS varchar(255)) AS BEJOVO_KULD_TevesCimzes,
			CAST(COALESCE( CASE WHEN bk.TevesErkeztetes = '1' THEN 'Igen' ELSE 'Nem' END,'') AS varchar(255)) AS BEJOVO_KULD_TevesErkeztetes,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('KULD_CIMZES_TIPUS',bk.CimzesTipusa,@Org),'') AS varchar(255)) AS BEJOVO_KULD_CimzesTipusa,
			--ugyirat
			CAST(COALESCE(u.Foszam,'') AS varchar(255)) AS UGYIRAT_Foszam,
			CAST(COALESCE(u.Ugyazonosito,'') AS varchar(255)) AS UGYIRAT_Ugyazonosito,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('UGYINTEZES_ALAPJA',u.UgyintezesModja,@Org),'') AS varchar(255)) AS UGYIRAT_UgyintezesModja,
			CAST(COALESCE(u.Hatarido,'') AS varchar(255)) AS UGYIRAT_Hatarido,
			CAST(COALESCE(CONVERT(nvarchar(10),u.SkontrobaDat, 102),'') AS varchar(255)) AS UGYIRAT_SkontrobaDat,
			CAST(COALESCE(CONVERT(nvarchar(10),u.LezarasDat, 102),'') AS varchar(255)) AS UGYIRAT_LezarasDat,
			CAST(COALESCE(CONVERT(nvarchar(10),u.IrattarbaKuldDatuma, 102),'') AS varchar(255)) AS UGYIRAT_IrattarbaKuldDatuma,
			CAST(COALESCE(CONVERT(nvarchar(10),u.IrattarbaVetelDat, 102),'') AS varchar(255)) AS UGYIRAT_IrattarbaVetelDat,
			CAST(COALESCE(dbo.fn_GetCsoportNev(u.FelhCsoport_Id_IrattariAtvevo),'') AS varchar(255)) AS UGYIRAT_IrattariAtvevo,
			CAST(COALESCE(CONVERT(nvarchar(10),u.SelejtezesDat, 102),'') AS varchar(255)) AS UGYIRAT_SelejtezesDat,
			CAST(COALESCE(dbo.fn_GetCsoportNev(u.FelhCsoport_Id_Selejtezo),'') AS varchar(255)) AS UGYIRAT_Selejtezo,
			CAST(COALESCE(u.LeveltariAtvevoNeve,'') AS varchar(255)) AS UGYIRAT_LeveltariAtvevoNeve,
			CAST(COALESCE(dbo.fn_GetCsoportNev(u.FelhCsoport_Id_Felulvizsgalo),'') AS varchar(255)) AS UGYIRAT_Felulvizsgalo,
			CAST(COALESCE(CONVERT(nvarchar(10),u.FelulvizsgalatDat, 102),'') AS varchar(255)) AS UGYIRAT_FelulvizsgalatDat,
			CAST(COALESCE(u.IktatoszamKieg,'') AS varchar(255)) AS UGYIRAT_IktatoszamKieg,
			CAST(COALESCE(u.Targy,'') AS varchar(255)) AS UGYIRAT_Targy,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('UGYTIPUS',u.UgyTipus,@Org),'') AS varchar(255)) AS UGYIRAT_UgyTipus,
			CAST(COALESCE(u.IrattariHely,'') AS varchar(255)) AS UGYIRAT_IrattariHely,
			CAST(COALESCE(CONVERT(nvarchar(10),u.SztornirozasDat, 102),'') AS varchar(255)) AS UGYIRAT_SztornirozasDat,
			CAST(COALESCE(dbo.fn_GetCsoportNev(u.Csoport_Id_Felelos),'') AS varchar(255)) AS UGYIRAT_Felelos,
			CAST(COALESCE(dbo.fn_GetCsoportNev(u.FelhasznaloCsoport_Id_Orzo),'') AS varchar(255)) AS UGYIRAT_Orzo,
			CAST(COALESCE(dbo.fn_GetCsoportNev(u.Csoport_Id_Cimzett),'') AS varchar(255)) AS UGYIRAT_Cimzett,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('UGYIRAT_JELLEG',u.Jelleg,@Org),'') AS varchar(255)) AS UGYIRAT_Jelleg,
			CAST(COALESCE(u.SkontroOka,'') AS varchar(255)) AS UGYIRAT_SkontroOka,
			CAST(COALESCE(CONVERT(nvarchar(10),u.SkontroVege, 102),'') AS varchar(255)) AS UGYIRAT_SkontroVege,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('SURGOSSEG',u.Surgosseg,@Org),'') AS varchar(255)) AS UGYIRAT_Surgosseg,
			CAST(COALESCE(u.SkontrobanOsszesen,'') AS varchar(255)) AS UGYIRAT_SkontrobanOsszesen,
			CAST(COALESCE(CONVERT(nvarchar(10),u.MegorzesiIdoVege, 102),'') AS varchar(255)) AS UGYIRAT_MegorzesiIdoVege,
			CAST(COALESCE(u.NevSTR_Ugyindito,'') AS varchar(255)) AS UGYIRAT_UgyinditoNev,
			CAST(COALESCE(CONVERT(nvarchar(10),u.ElintezesDat, 102),'') AS varchar(255)) AS UGYIRAT_ElintezesDat,
			CAST(COALESCE(dbo.fn_GetCsoportNev(u.FelhasznaloCsoport_Id_Ugyintez),'') AS varchar(255)) AS UGYIRAT_Ugyintezo,
			CAST(COALESCE(CONVERT(nvarchar(10),u.KolcsonKikerDat, 102),'') AS varchar(255)) AS UGYIRAT_KolcsonKikerDat,
			CAST(COALESCE(CONVERT(nvarchar(10),u.KolcsonKiadDat, 102),'') AS varchar(255)) AS UGYIRAT_KolcsonKiadDat,
			CAST(COALESCE(CONVERT(nvarchar(10),u.Kolcsonhatarido, 102),'') AS varchar(255)) AS UGYIRAT_Kolcsonhatarido,
			CAST(COALESCE(u.BARCODE,'') AS varchar(255)) AS UGYIRAT_Vonalkod,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('UGYIRAT_ALLAPOT',u.Allapot,@Org),'') AS varchar(255)) AS UGYIRAT_Allapot,
			CAST(COALESCE(u.Megjegyzes,'') AS varchar(255)) AS UGYIRAT_Megjegyzes,
			CAST(COALESCE(u.Azonosito,'') AS varchar(255)) AS UGYIRAT_Iktatoszam,
			CAST(COALESCE(dbo.fn_GetCsoportNev(u.Csoport_Id_Ugyfelelos),'') AS varchar(255)) AS UGYIRAT_Ugyfelelos,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('ELINTEZESMOD',u.ElintezesMod,@Org),'') AS varchar(255)) AS UGYIRAT_ElintezesMod,
			CAST(COALESCE(u.CimSTR_Ugyindito,'') AS varchar(255)) AS UGYIRAT_UgyinditoCim,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('LEZARAS_OKA',u.LezarasOka,@Org),'') AS varchar(255)) AS UGYIRAT_LezarasOka,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('SKONTRO_OKA',u.SkontroOka_Kod,@Org),'') AS varchar(255)) AS UGYIRAT_SkontroOka_Kod,
			--ugyiratdarab
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('ELJARASI_SZAKASZ',ud.EljarasiSzakasz,@Org),'') AS varchar(255)) AS UGYIRATDARAB_EljarasiSzakasz,
			CAST(COALESCE(ud.Azonosito,'') AS varchar(255)) AS UGYIRATDARAB_Azonosito,
			CAST(COALESCE(CONVERT(nvarchar(10),ud.Hatarido, 102),'') AS varchar(255)) AS UGYIRATDARAB_Hatarido,
			CAST(COALESCE(dbo.fn_GetCsoportNev(ud.FelhasznaloCsoport_Id_Ugyintez),'') AS varchar(255)) AS UGYIRATDARAB_Ugyintezo,
			CAST(COALESCE(CONVERT(nvarchar(10),ud.ElintezesDat, 102),'') AS varchar(255)) AS UGYIRATDARAB_ElintezesDat,
			CAST(COALESCE(CONVERT(nvarchar(10),ud.LezarasDat, 102),'') AS varchar(255)) AS UGYIRATDARAB_LezarasDat,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('ELINTEZESMOD',ud.ElintezesMod,@Org),'') AS varchar(255)) AS UGYIRATDARAB_ElintezesMod,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('LEZARAS_OKA',ud.LezarasOka,@Org),'') AS varchar(255)) AS UGYIRATDARAB_LezarasOka,
			CAST(COALESCE(ud.Leiras,'') AS varchar(255)) AS UGYIRATDARAB_Leiras,
			CAST(COALESCE(ud.Foszam,'') AS varchar(255)) AS UGYIRATDARAB_Foszam,
			CAST(COALESCE(dbo.fn_GetCsoportNev(ud.Csoport_Id_Felelos),'') AS varchar(255)) AS UGYIRATDARAB_Felelos,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('UGYIRATDARAB_ALLAPOT',ud.Allapot,@Org),'') AS varchar(255)) AS UGYIRATDARAB_Allapot,
			--iratpeldany
			CAST(COALESCE(CONVERT(nvarchar(10),p.SztornirozasDat, 102),'') AS varchar(255)) AS PELDANY_SztornirozasDat,
			CAST(COALESCE(CONVERT(nvarchar(10),p.AtvetelDatuma, 102),'') AS varchar(255)) AS PELDANY_AtvetelDatuma,
			CAST(COALESCE(p.Eredet,'') AS varchar(255)) AS PELDANY_Eredet,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('KULDEMENY_KULDES_MODJA',p.KuldesMod,@Org),'') AS varchar(255)) AS PELDANY_KuldesMod,
			CAST(COALESCE(p.Ragszam,'') AS varchar(255)) AS PELDANY_Ragszam,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('IRAT_UGYINT_MODJA',p.UgyintezesModja,@Org),'') AS varchar(255)) AS PELDANY_UgyintezesModja,
			CAST(COALESCE(CONVERT(nvarchar(10),p.VisszaerkezesiHatarido, 102),'') AS varchar(255)) AS PELDANY_VisszaerkezesiHatarido,
			CAST(COALESCE( CASE WHEN p.Visszavarolag = '1' THEN 'Igen' ELSE 'Nem' END,'') AS varchar(255)) AS PELDANY_Visszavarolag,
			CAST(COALESCE(CONVERT(nvarchar(10),p.VisszaerkezesDatuma, 102),'') AS varchar(255)) AS PELDANY_VisszaerkezesDatuma,
			CAST(COALESCE(dbo.fn_GetCsoportNev(p.Csoport_Id_Felelos),'') AS varchar(255)) AS PELDANY_Felelos,
			CAST(COALESCE(dbo.fn_GetCsoportNev(p.FelhasznaloCsoport_Id_Orzo),'') AS varchar(255)) AS PELDANY_Orzo,
			CAST(COALESCE(p.CimSTR_Cimzett,'') AS varchar(255)) AS PELDANY_CimzettCim,
			CAST(COALESCE(p.NevSTR_Cimzett,'') AS varchar(255)) AS PELDANY_CimzettNev,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('TOVABBITO_SZERVEZET',p.Tovabbito,@Org),'') AS varchar(255)) AS PELDANY_Tovabbito,
			CAST(COALESCE(p.IrattariHely,'') AS varchar(255)) AS PELDANY_IrattariHely,
			CAST(COALESCE(CONVERT(nvarchar(10),p.PostazasDatuma, 102),'') AS varchar(255)) AS PELDANY_PostazasDatuma,
			CAST(COALESCE(p.BarCode,'') AS varchar(255)) AS PELDANY_Vonalkod,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('IRATPELDANY_ALLAPOT',p.Allapot,@Org),'') AS varchar(255)) AS PELDANY_Allapot,
			CAST(COALESCE(p.Azonosito,'') AS varchar(255)) AS PELDANY_Iktatoszam,
			CAST(COALESCE( CASE WHEN p.ValaszElektronikus = '1' THEN 'Igen' ELSE 'Nem' END,'') AS varchar(255)) AS PELDANY_ValaszElektronikus,
			--iktatokonyv
			CAST(COALESCE(ik.Ev,'') AS varchar(255)) AS IKTATOKONYV_Ev,
			CAST(COALESCE(ik.Azonosito,'') AS varchar(255)) AS IKTATOKONYV_Azonosito,
			CAST(COALESCE(ik.Nev,'') AS varchar(255)) AS IKTATOKONYV_Nev,
			CAST(COALESCE(ik.MegkulJelzes,'') AS varchar(255)) AS IKTATOKONYV_MegkulJelzes,
			CAST(COALESCE(ik.Iktatohely,'') AS varchar(255)) AS IKTATOKONYV_Iktatohely,
			CAST(COALESCE( CASE WHEN ik.KozpontiIktatasJelzo = '1' THEN 'Igen' ELSE 'Nem' END,'') AS varchar(255)) AS IKTATOKONYV_KozpontiIktatasJelzo,
			CAST(COALESCE(dbo.fn_GetCsoportNev(ik.Csoport_Id_Tulaj),'') AS varchar(255)) AS IKTATOKONYV_Tulajdonos,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('IKT_SZAM_OSZTAS',ik.IktSzamOsztas,@Org),'') AS varchar(255)) AS IKTATOKONYV_IktSzamOsztas,
			CAST(COALESCE(ik.FormatumKod,'') AS varchar(255)) AS IKTATOKONYV_FormatumKod,
			CAST(COALESCE( CASE WHEN ik.Titkos = '1' THEN 'Igen' ELSE 'Nem' END,'') AS varchar(255)) AS IKTATOKONYV_Titkos,
			CAST(COALESCE( CASE ik.IktatoErkezteto WHEN 'P' THEN 'Postakönyv' WHEN 'I' THEN 'Iktatókönyv' WHEN 'E' THEN 'Érkeztetőkönyv' ELSE 'Egyéb' END,'') AS varchar(255)) AS IKTATOKONYV_IktatoErkezteto,
			CAST(COALESCE(CONVERT(nvarchar(10),ik.LezarasDatuma, 102),'') AS varchar(255)) AS IKTATOKONYV_LezarasDatuma,
			CAST(COALESCE(dbo.fn_KodtarErtekNeve('ERKKONYV_STATUSZ',ik.Statusz,@Org),'') AS varchar(255)) AS IKTATOKONYV_Statusz,
			CAST(COALESCE(ik.Terjedelem,'') AS varchar(255)) AS IKTATOKONYV_Terjedelem,
			CAST(COALESCE(CONVERT(nvarchar(10),ik.SelejtezesDatuma, 102),'') AS varchar(255)) AS IKTATOKONYV_SelejtezesDatuma,
			CAST(COALESCE( CASE ik.KezelesTipusa WHEN 'E' THEN 'Elektronikus' WHEN 'P' THEN 'Papír' ELSE 'Egyéb' END,'') AS varchar(255)) AS IKTATOKONYV_KezelesTipusa,
			CAST(COALESCE(dbo.fn_MergeIrattariTetelSzam(aj.Kod,it.IrattariTetelszam,it.MegorzesiIdo,it.IrattariJel,@Org),'') AS varchar(255)) AS IKTATOKONYV_IrattariTetelszam
			FROM 
				EREC_IraIratok i 
				left join EREC_UgyUgyiratok u on i.Ugyirat_Id = u.Id 
				left join EREC_UgyUgyiratdarabok ud on i.UgyUgyIratDarab_Id = ud.Id 
				left join EREC_KuldKuldemenyek bk on i.KuldKuldemenyek_Id = bk.Id
				left join EREC_PldIratPeldanyok p on p.IraIrat_Id = i.Id
				left join EREC_IraIktatoKonyvek ik on u.IraIktatokonyv_Id = ik.Id	
				left join EREC_IraIrattariTetelek it on u.IraIrattariTetel_Id = it.Id
				left join EREC_AgazatiJelek aj on it.AgazatiJel_Id = aj.Id
				left join EREC_IratMetaDefinicio imd on imd.Id = u.IratMetadefinicio_Id  
			 WHERE i.Id = @Irat_Id
			  and p.sorszam = '1'
			  and isnull(bk.PostazasIranya,i.PostazasIranya) COLLATE DATABASE_DEFAULT = i.PostazasIranya COLLATE DATABASE_DEFAULT
			) irat
			UNPIVOT
			  (ERTEK FOR KOD IN
				(
				--irat
				IRAT_PostazasIranya,
				IRAT_Alszam,
				IRAT_Kategoria,
				IRAT_HivatkozasiSzam,
				IRAT_IktatasDatuma,
				IRAT_Targy,
				IRAT_Jelleg,
				IRAT_SztornirozasDatuma,
				IRAT_FelhasznaloCsoport_Id_Iktato,
				IRAT_FelhasznaloCsoport_Id_Kiadmany,
				IRAT_UgyintezesAlapja,
				IRAT_AdathordozoTipusa,
				IRAT_Felelos,
				IRAT_FelhasznaloCsoport_Id_Orzo,
				IRAT_KiadmanyozniKell,
				IRAT_Ugyintezo,
				IRAT_Hatarido,
				IRAT_MegorzesiIdo,
				IRAT_IratFajta,
				IRAT_Irattipus,
				IRAT_Allapot,
				IRAT_Iktatoszam,
				IRAT_IrattarbaKuldDatuma,
				IRAT_IrattarbaVetelDat,
				IRAT_Minosites,
				--küldemény
				BEJOVO_KULD_Allapot,
				BEJOVO_KULD_BeerkezesIdeje,
				BEJOVO_KULD_FelbontasDatuma,
				BEJOVO_KULD_KuldesMod,
				BEJOVO_KULD_Erkezteto_Szam,
				BEJOVO_KULD_HivatkozasiSzam,
				BEJOVO_KULD_Targy,
				BEJOVO_KULD_Tartalom,
				BEJOVO_KULD_RagSzam,
				BEJOVO_KULD_Surgosseg,
				BEJOVO_KULD_BelyegzoDatuma,
				BEJOVO_KULD_UgyintezesModja,
				BEJOVO_KULD_PostazasIranya,
				BEJOVO_KULD_Tovabbito,
				BEJOVO_KULD_PeldanySzam,
				BEJOVO_KULD_IktatniKell,
				BEJOVO_KULD_Iktathato,
				BEJOVO_KULD_SztornirozasDat,
				BEJOVO_KULD_Erkeztetes_Ev,
				BEJOVO_KULD_Csoport_Id_Cimzett,
				BEJOVO_KULD_Csoport_Id_Felelos,
				BEJOVO_KULD_FelhasznaloCsoport_Id_Expedial,
				BEJOVO_KULD_Orzo,
				BEJOVO_KULD_Atvevo,
				BEJOVO_KULD_BekuldoCime,
				BEJOVO_KULD_BekuldoNeve,
				BEJOVO_KULD_AdathordozoTipusa,
				BEJOVO_KULD_ElsodlegesAdathordozoTipusa,
				BEJOVO_KULD_Vonalkod,
				BEJOVO_KULD_Bonto,
				BEJOVO_KULD_Eloszto,
				--ügyirat
				UGYIRAT_Foszam,
				UGYIRAT_Ugyazonosito,
				UGYIRAT_UgyintezesModja,
				UGYIRAT_Hatarido,
				UGYIRAT_SkontrobaDat,
				UGYIRAT_LezarasDat,
				UGYIRAT_IrattarbaKuldDatuma,
				UGYIRAT_IrattarbaVetelDat,
				UGYIRAT_IrattariAtvevo,
				UGYIRAT_SelejtezesDat,
				UGYIRAT_Selejtezo,
				UGYIRAT_LeveltariAtvevoNeve,
				UGYIRAT_Felulvizsgalo,
				UGYIRAT_FelulvizsgalatDat,
				UGYIRAT_IktatoszamKieg,
				UGYIRAT_Targy,
				UGYIRAT_UgyTipus,
				UGYIRAT_IrattariHely,
				UGYIRAT_SztornirozasDat,
				UGYIRAT_Felelos,
				UGYIRAT_Orzo,
				UGYIRAT_Cimzett,
				UGYIRAT_Jelleg,
				UGYIRAT_SkontroOka,
				UGYIRAT_SkontroVege,
				UGYIRAT_Surgosseg,
				UGYIRAT_SkontrobanOsszesen,
				UGYIRAT_MegorzesiIdoVege,
				UGYIRAT_UgyinditoNev,
				UGYIRAT_ElintezesDat,
				UGYIRAT_Ugyintezo,
				UGYIRAT_KolcsonKikerDat,
				UGYIRAT_KolcsonKiadDat,
				UGYIRAT_Kolcsonhatarido,
				UGYIRAT_Vonalkod,
				UGYIRAT_Allapot,
				UGYIRAT_Megjegyzes,
				UGYIRAT_Iktatoszam,
				UGYIRAT_Ugyfelelos,
				UGYIRAT_ElintezesMod,
				UGYIRAT_UgyinditoCim,
				UGYIRAT_LezarasOka,
				UGYIRAT_SkontroOka_Kod,
				--ügyiratdarab
				UGYIRATDARAB_EljarasiSzakasz,
				UGYIRATDARAB_Azonosito,
				UGYIRATDARAB_Hatarido,
				UGYIRATDARAB_Ugyintezo,
				UGYIRATDARAB_ElintezesDat,
				UGYIRATDARAB_LezarasDat,
				UGYIRATDARAB_ElintezesMod,
				UGYIRATDARAB_LezarasOka,
				UGYIRATDARAB_Leiras,
				UGYIRATDARAB_Foszam,
				UGYIRATDARAB_Felelos,
				UGYIRATDARAB_Allapot,
				--iratpéldány
				PELDANY_SztornirozasDat,
				PELDANY_AtvetelDatuma,
				--PELDANY_Eredet,
				PELDANY_KuldesMod,
				PELDANY_Ragszam,
				PELDANY_UgyintezesModja,
				PELDANY_VisszaerkezesiHatarido,
				PELDANY_Visszavarolag,
				PELDANY_VisszaerkezesDatuma,
				PELDANY_Felelos,
				PELDANY_Orzo,
				PELDANY_CimzettCim,
				PELDANY_CimzettNev,
				PELDANY_Tovabbito,
				PELDANY_IrattariHely,
				PELDANY_PostazasDatuma,
				PELDANY_Vonalkod,
				PELDANY_Allapot,
				PELDANY_Iktatoszam,
				PELDANY_ValaszElektronikus,
				--iktatókönyv
				IKTATOKONYV_Ev,
				IKTATOKONYV_Azonosito,
				IKTATOKONYV_Nev,
				IKTATOKONYV_MegkulJelzes,
				IKTATOKONYV_Iktatohely,
				IKTATOKONYV_KozpontiIktatasJelzo,
				IKTATOKONYV_Tulajdonos,
				IKTATOKONYV_IktSzamOsztas,
				IKTATOKONYV_FormatumKod,
				IKTATOKONYV_Titkos,
				IKTATOKONYV_IktatoErkezteto,
				IKTATOKONYV_LezarasDatuma,
				IKTATOKONYV_Statusz,
				IKTATOKONYV_Terjedelem,
				IKTATOKONYV_SelejtezesDatuma,
				IKTATOKONYV_KezelesTipusa,
				IKTATOKONYV_IrattariTetelszam
			)
		) as ForSablon
		
		UNION ALL
		select CONCAT('IRAT_META_',t.TargySzavak) as KOD, ot.Ertek as ERTEK from 
		EREC_Obj_MetaAdatai oma 
		left join EREC_Obj_MetaDefinicio omd on omd.id = oma.Obj_MetaDefinicio_Id
		left join EREC_TargySzavak t on oma.Targyszavak_Id = t.id
		left join KRT_ObjTipusok kot on kot.Id = omd.Objtip_Id
		left join EREC_ObjektumTargyszavai ot on ot.Obj_Metaadatai_Id = oma.Id
		where COALESCE(ot.Obj_Id,@Irat_Id) = @Irat_Id 
		and kot.Kod = 'EREC_IraIratok'

		UNION ALL
		select CONCAT('UGYIRAT_META_',t.TargySzavak) as KOD, ot.Ertek as ERTEK from 
		EREC_Obj_MetaAdatai oma 
		left join EREC_Obj_MetaDefinicio omd on omd.id = oma.Obj_MetaDefinicio_Id
		left join EREC_TargySzavak t on oma.Targyszavak_Id = t.id
		left join KRT_ObjTipusok kot on kot.Id = omd.Objtip_Id
		left join EREC_ObjektumTargyszavai ot on ot.Obj_Metaadatai_Id = oma.Id
		where COALESCE(ot.Obj_Id,@Ugyirat_Id) = @Ugyirat_Id 
		and kot.Kod = 'EREC_UgyUgyiratok'

		UNION ALL
		select CONCAT('BEJOVOKULDEMENY_META_',t.TargySzavak) as KOD, ot.Ertek as ERTEK from 
		EREC_Obj_MetaAdatai oma 
		left join EREC_Obj_MetaDefinicio omd on omd.id = oma.Obj_MetaDefinicio_Id
		left join EREC_TargySzavak t on oma.Targyszavak_Id = t.id
		left join KRT_ObjTipusok kot on kot.Id = omd.Objtip_Id
		left join EREC_ObjektumTargyszavai ot on ot.Obj_Metaadatai_Id = oma.Id
		where COALESCE(ot.Obj_Id,@BejovoKuldemeny_Id) = @BejovoKuldemeny_Id 
		and kot.Kod = 'EREC_KuldKuldemenyek'

		UNION ALL
		select CONCAT('UGYIRATDARAB_META_',t.TargySzavak) as KOD, ot.Ertek as ERTEK from 
		EREC_Obj_MetaAdatai oma 
		left join EREC_Obj_MetaDefinicio omd on omd.id = oma.Obj_MetaDefinicio_Id
		left join EREC_TargySzavak t on oma.Targyszavak_Id = t.id
		left join KRT_ObjTipusok kot on kot.Id = omd.Objtip_Id
		left join EREC_ObjektumTargyszavai ot on ot.Obj_Metaadatai_Id = oma.Id
		where COALESCE(ot.Obj_Id,@UgyiratDarab_Id) = @UgyiratDarab_Id 
		and kot.Kod = 'EREC_UgyUgyiratdarabok'

		UNION ALL
		select CONCAT('IRATPELDANY_META_',t.TargySzavak) as KOD, ot.Ertek as ERTEK from 
		EREC_Obj_MetaAdatai oma 
		left join EREC_Obj_MetaDefinicio omd on omd.id = oma.Obj_MetaDefinicio_Id
		left join EREC_TargySzavak t on oma.Targyszavak_Id = t.id
		left join KRT_ObjTipusok kot on kot.Id = omd.Objtip_Id
		left join EREC_ObjektumTargyszavai ot on ot.Obj_Metaadatai_Id = oma.Id
		where COALESCE(ot.Obj_Id,@IratPeldany_Id) = @IratPeldany_Id 
		and kot.Kod = 'EREC_PldIratPeldanyok'
	) result
	ORDER BY KOD
    
END
GO
