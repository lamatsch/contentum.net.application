﻿CREATE procedure [dbo].[sp_EREC_IraIratokGet_ASPADO_IratIds]
		 @iratIds NVARCHAR(MAX) 
AS
BEGIN TRY
	SELECT * INTO #idsTable FROM fn_Split(@iratIds, ',')

	SELECT ugyiratok.Allapot AS UgyiratAllapot,
		   iratok.Allapot AS IratAllapot,
		   iratpeldanyok.Allapot AS IratPeldanyAllapot, 
		   kuldemenyekKuldemenyIratpeldany.Allapot AS KuldemenyAllapot,
		   kuldemenyek.Allapot AS IratKuldemenyAllapot,
		   kuldemenyekIratpeldanyai.Id,
		   kuldemenyek.id,
		   iratok.KuldKuldemenyek_Id,
		   iratok.Munkaallomas,
		   iratok.Azonosito AS IratAzonosito,
		   iratok.PostazasIranya,
		   ugyiratok.Azonosito,
		   ugyiratok.UgyTipus,
		   ugyiratok.Jelleg,
		   ugyiratok.ErvVege,
		   iratpeldanyok.Id
	FROM EREC_UgyUgyiratok ugyiratok
	LEFT JOIN EREC_IraIratok iratok ON iratok.Ugyirat_Id=ugyiratok.Id
	LEFT JOIN EREC_PldIratPeldanyok iratpeldanyok ON iratpeldanyok.IraIrat_Id=iratok.Id
	LEFT JOIN EREC_Kuldemeny_IratPeldanyai kuldemenyekIratpeldanyai ON kuldemenyekIratpeldanyai.Peldany_Id=iratpeldanyok.Id
	LEFT JOIN EREC_KuldKuldemenyek kuldemenyek ON kuldemenyek.Id=iratok.KuldKuldemenyek_Id
	LEFT JOIN EREC_KuldKuldemenyek kuldemenyekKuldemenyIratpeldany ON kuldemenyekKuldemenyIratpeldany.Id=kuldemenyekIratpeldanyai.KuldKuldemeny_Id
	WHERE
	iratok.Azonosito IN
	(
		 SELECT Value FROM #idsTable
	)	
	and iratok.Munkaallomas = 'ASP.ADO'
	and iratok.Allapot != '90'
	ORDER BY ugyiratok.Allapot

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 END CATCH
