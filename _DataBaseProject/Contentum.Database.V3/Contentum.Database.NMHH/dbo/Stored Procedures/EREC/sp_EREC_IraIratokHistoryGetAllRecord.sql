﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_IraIratokHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_EREC_IraIratokHistoryGetAllRecord
go
*/
create procedure sp_EREC_IraIratokHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_IraIratokHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PostazasIranya' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PostazasIranya as nvarchar(max)),'') != ISNULL(CAST(New.PostazasIranya as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
		 left join KRT_KodCsoportok KCs on KCs.Kod = 'POSTAZAS_IRANYA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.PostazasIranya and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.PostazasIranya and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege 
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Alszam' as ColumnName,               cast(Old.Alszam as nvarchar(99)) as OldValue,
               cast(New.Alszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Alszam as nvarchar(max)),'') != ISNULL(CAST(New.Alszam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Sorszam' as ColumnName,               cast(Old.Sorszam as nvarchar(99)) as OldValue,
               cast(New.Sorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Sorszam as nvarchar(max)),'') != ISNULL(CAST(New.Sorszam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UtolsoSorszam' as ColumnName,               cast(Old.UtolsoSorszam as nvarchar(99)) as OldValue,
               cast(New.UtolsoSorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UtolsoSorszam as nvarchar(max)),'') != ISNULL(CAST(New.UtolsoSorszam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyUgyIratDarab_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratdarabokAzonosito(Old.UgyUgyIratDarab_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratdarabokAzonosito(New.UgyUgyIratDarab_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyUgyIratDarab_Id as nvarchar(max)),'') != ISNULL(CAST(New.UgyUgyIratDarab_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join EREC_UgyUgyiratdarabok FTOld on FTOld.Id = Old.UgyUgyIratDarab_Id --and FTOld.Ver = Old.Ver
         left join EREC_UgyUgyiratdarabok FTNew on FTNew.Id = New.UgyUgyIratDarab_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kategoria' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Kategoria as nvarchar(max)),'') != ISNULL(CAST(New.Kategoria as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRATKATEGORIA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Kategoria and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Kategoria and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HivatkozasiSzam' as ColumnName,               cast(Old.HivatkozasiSzam as nvarchar(99)) as OldValue,
               cast(New.HivatkozasiSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HivatkozasiSzam as nvarchar(max)),'') != ISNULL(CAST(New.HivatkozasiSzam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IktatasDatuma' as ColumnName,               cast(Old.IktatasDatuma as nvarchar(99)) as OldValue,
               cast(New.IktatasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IktatasDatuma as nvarchar(max)),'') != ISNULL(CAST(New.IktatasDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ExpedialasDatuma' as ColumnName,               cast(Old.ExpedialasDatuma as nvarchar(99)) as OldValue,
               cast(New.ExpedialasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ExpedialasDatuma as nvarchar(max)),'') != ISNULL(CAST(New.ExpedialasDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ExpedialasModja' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ExpedialasModja as nvarchar(max)),'') != ISNULL(CAST(New.ExpedialasModja as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'EXPEDIALAS_MODJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.ExpedialasModja and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.ExpedialasModja and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Expedial' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Expedial) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Expedial) as NewValue, 
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Expedial as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Expedial as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Targy' as ColumnName,               cast(Old.Targy as nvarchar(99)) as OldValue,
               cast(New.Targy as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Targy as nvarchar(max)),'') != ISNULL(CAST(New.Targy as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Jelleg' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Jelleg as nvarchar(max)),'') != ISNULL(CAST(New.Jelleg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
		 left join KRT_KodCsoportok KCs on KCs.Kod = 'IRAT_JELLEG'
		 left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Jelleg and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
		 left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Jelleg and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SztornirozasDat' as ColumnName,               cast(Old.SztornirozasDat as nvarchar(99)) as OldValue,
               cast(New.SztornirozasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SztornirozasDat as nvarchar(max)),'') != ISNULL(CAST(New.SztornirozasDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Iktato' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Iktato) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Iktato) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Iktato as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Iktato as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Kiadmany' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Kiadmany) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Kiadmany) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Kiadmany as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Kiadmany as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesAlapja' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyintezesAlapja as nvarchar(max)),'') != ISNULL(CAST(New.UgyintezesAlapja as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYINTEZES_ALAPJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.UgyintezesAlapja and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.UgyintezesAlapja and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AdathordozoTipusa' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AdathordozoTipusa as nvarchar(max)),'') != ISNULL(CAST(New.AdathordozoTipusa as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'ELSODLEGES_ADATHORDOZO'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.AdathordozoTipusa and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.AdathordozoTipusa and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Felelos' as ColumnName,               
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Felelos) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Felelos) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id_Felelos as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id_Felelos as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Ugyfelelos' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Ugyfelelos) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Ugyfelelos) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id_Ugyfelelos as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id_Ugyfelelos as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Orzo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Orzo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Orzo) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Orzo as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Orzo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KiadmanyozniKell' as ColumnName,
               case Old.KiadmanyozniKell when 0 then 'Nem' when 1 then 'Igen' end as OldValue,
               case New.KiadmanyozniKell when 0 then 'Nem' when 1 then 'Igen' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KiadmanyozniKell as nvarchar(max)),'') != ISNULL(CAST(New.KiadmanyozniKell as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Ugyintez' as ColumnName,               
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Ugyintez) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Ugyintez) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Ugyintez as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Ugyintez as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Hatarido' as ColumnName,               cast(Old.Hatarido as nvarchar(99)) as OldValue,
               cast(New.Hatarido as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Hatarido as nvarchar(max)),'') != ISNULL(CAST(New.Hatarido as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MegorzesiIdo' as ColumnName,               cast(Old.MegorzesiIdo as nvarchar(99)) as OldValue,
               cast(New.MegorzesiIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MegorzesiIdo as nvarchar(max)),'') != ISNULL(CAST(New.MegorzesiIdo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IratFajta' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IratFajta as nvarchar(max)),'') != ISNULL(CAST(New.IratFajta as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRAT_FAJTA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.IratFajta and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.IratFajta and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Irattipus' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,  
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Irattipus as nvarchar(max)),'') != ISNULL(CAST(New.Irattipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRATTIPUS'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Irattipus and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Irattipus and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldKuldemenyek_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_KuldKuldemenyekAzonosito(Old.KuldKuldemenyek_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_KuldKuldemenyekAzonosito(New.KuldKuldemenyek_Id) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KuldKuldemenyek_Id as nvarchar(max)),'') != ISNULL(CAST(New.KuldKuldemenyek_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Allapot as nvarchar(max)),'') != ISNULL(CAST(New.Allapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRAT_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Allapot and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Allapot and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Azonosito' as ColumnName,               cast(Old.Azonosito as nvarchar(99)) as OldValue,
               cast(New.Azonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Azonosito as nvarchar(max)),'') != ISNULL(CAST(New.Azonosito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'GeneraltTargy' as ColumnName,               cast(Old.GeneraltTargy as nvarchar(99)) as OldValue,
               cast(New.GeneraltTargy as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.GeneraltTargy as nvarchar(max)),'') != ISNULL(CAST(New.GeneraltTargy as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IratMetaDef_Id' as ColumnName,               cast(Old.IratMetaDef_Id as nvarchar(99)) as OldValue,
               cast(New.IratMetaDef_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IratMetaDef_Id as nvarchar(max)),'') != ISNULL(CAST(New.IratMetaDef_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarbaKuldDatuma' as ColumnName,               cast(Old.IrattarbaKuldDatuma  as nvarchar(99)) as OldValue,
               cast(New.IrattarbaKuldDatuma  as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IrattarbaKuldDatuma  as nvarchar(max)),'') != ISNULL(CAST(New.IrattarbaKuldDatuma  as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarbaVetelDat' as ColumnName,               cast(Old.IrattarbaVetelDat as nvarchar(99)) as OldValue,
               cast(New.IrattarbaVetelDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IrattarbaVetelDat as nvarchar(max)),'') != ISNULL(CAST(New.IrattarbaVetelDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ugyirat_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratokAzonosito(Old.Ugyirat_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratokAzonosito(New.Ugyirat_Id) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Ugyirat_Id as nvarchar(max)),'') != ISNULL(CAST(New.Ugyirat_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Minosites' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Minosites as nvarchar(max)),'') != ISNULL(CAST(New.Minosites as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRAT_MINOSITES'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Minosites and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Minosites and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Elintezett' as ColumnName,
               case Old.Elintezett when 0 then 'Nem' when 1 then 'Igen' end as OldValue,
               case New.Elintezett when 0 then 'Nem' when 1 then 'Igen' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Elintezett as nvarchar(max)),'') != ISNULL(CAST(New.Elintezett as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IratHatasaUgyintezesre' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IratHatasaUgyintezesre as nvarchar(max)),'') != ISNULL(CAST(New.IratHatasaUgyintezesre as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRAT_HATASA_UGYINTEZESRE'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.IratHatasaUgyintezesre and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.IratHatasaUgyintezesre and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelfuggesztesOka' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelfuggesztesOka as nvarchar(max)),'') != ISNULL(CAST(New.FelfuggesztesOka as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
		 left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYIRAT_FELFUGGESZTES_OKA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.FelfuggesztesOka and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.FelfuggesztesOka and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'LezarasOka' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.LezarasOka as nvarchar(max)),'') != ISNULL(CAST(New.LezarasOka as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
		 left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYIRAT_LEZARAS_OKA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.LezarasOka and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.LezarasOka and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Munkaallomas' as ColumnName,               cast(Old.Munkaallomas as nvarchar(99)) as OldValue,
               cast(New.Munkaallomas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Munkaallomas as nvarchar(max)),'') != ISNULL(CAST(New.Munkaallomas as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesModja' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyintezesModja as nvarchar(max)),'') != ISNULL(CAST(New.UgyintezesModja as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRAT_UGYINT_MODJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.UgyintezesModja and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.UgyintezesModja and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IntezesIdopontja' as ColumnName,               cast(Old.IntezesIdopontja as nvarchar(99)) as OldValue,
               cast(New.IntezesIdopontja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IntezesIdopontja as nvarchar(max)),'') != ISNULL(CAST(New.IntezesIdopontja as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IntezesiIdo' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IntezesiIdo as nvarchar(max)),'') != ISNULL(CAST(New.IntezesiIdo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYIRAT_INTEZESI_IDO'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.IntezesiIdo and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.IntezesiIdo and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IntezesiIdoegyseg' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IntezesiIdoegyseg as nvarchar(max)),'') != ISNULL(CAST(New.IntezesiIdoegyseg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IDOEGYSEG'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.IntezesiIdoegyseg and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.IntezesiIdoegyseg and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UzemzavarKezdete' as ColumnName,               cast(Old.UzemzavarKezdete as nvarchar(99)) as OldValue,
               cast(New.UzemzavarKezdete as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UzemzavarKezdete as nvarchar(max)),'') != ISNULL(CAST(New.UzemzavarKezdete as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UzemzavarVege' as ColumnName,               cast(Old.UzemzavarVege as nvarchar(99)) as OldValue,
               cast(New.UzemzavarVege as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UzemzavarVege as nvarchar(max)),'') != ISNULL(CAST(New.UzemzavarVege as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UzemzavarIgazoloIratSzama' as ColumnName,               cast(Old.UzemzavarIgazoloIratSzama as nvarchar(99)) as OldValue,
               cast(New.UzemzavarIgazoloIratSzama as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UzemzavarIgazoloIratSzama as nvarchar(max)),'') != ISNULL(CAST(New.UzemzavarIgazoloIratSzama as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelfuggesztettNapokSzama' as ColumnName,               cast(Old.FelfuggesztettNapokSzama as nvarchar(99)) as OldValue,
               cast(New.FelfuggesztettNapokSzama as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelfuggesztettNapokSzama as nvarchar(max)),'') != ISNULL(CAST(New.FelfuggesztettNapokSzama as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VisszafizetesJogcime' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.VisszafizetesJogcime as nvarchar(max)),'') != ISNULL(CAST(New.VisszafizetesJogcime as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'VISSZAFIZETES_JOGCIME'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.VisszafizetesJogcime and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.VisszafizetesJogcime and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VisszafizetesOsszege' as ColumnName,               cast(Old.VisszafizetesOsszege as nvarchar(99)) as OldValue,
               cast(New.VisszafizetesOsszege as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.VisszafizetesOsszege as nvarchar(max)),'') != ISNULL(CAST(New.VisszafizetesOsszege as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VisszafizetesHatarozatSzama' as ColumnName,               cast(Old.VisszafizetesHatarozatSzama as nvarchar(99)) as OldValue,
               cast(New.VisszafizetesHatarozatSzama as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.VisszafizetesHatarozatSzama as nvarchar(max)),'') != ISNULL(CAST(New.VisszafizetesHatarozatSzama as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id_VisszafizetesCimzet' as ColumnName,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(Old.Partner_Id_VisszafizetesCimzet) as OldValue,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(New.Partner_Id_VisszafizetesCimzet) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Partner_Id_VisszafizetesCimzet as nvarchar(max)),'') != ISNULL(CAST(New.Partner_Id_VisszafizetesCimzet as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Nev_VisszafizetCimzett' as ColumnName,               cast(Old.Partner_Nev_VisszafizetCimzett as nvarchar(99)) as OldValue,
               cast(New.Partner_Nev_VisszafizetCimzett as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Partner_Nev_VisszafizetCimzett as nvarchar(max)),'') != ISNULL(CAST(New.Partner_Nev_VisszafizetCimzett as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VisszafizetesHatarido' as ColumnName,               cast(Old.VisszafizetesHatarido as nvarchar(99)) as OldValue,
               cast(New.VisszafizetesHatarido as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.VisszafizetesHatarido as nvarchar(max)),'') != ISNULL(CAST(New.VisszafizetesHatarido as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Minosito' as ColumnName,               cast(Old.Minosito as nvarchar(99)) as OldValue,
               cast(New.Minosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Minosito as nvarchar(max)),'') != ISNULL(CAST(New.Minosito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MinositesErvenyessegiIdeje' as ColumnName,               cast(Old.MinositesErvenyessegiIdeje as nvarchar(99)) as OldValue,
               cast(New.MinositesErvenyessegiIdeje as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MinositesErvenyessegiIdeje as nvarchar(max)),'') != ISNULL(CAST(New.MinositesErvenyessegiIdeje as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TerjedelemMennyiseg' as ColumnName,               cast(Old.TerjedelemMennyiseg as nvarchar(99)) as OldValue,
               cast(New.TerjedelemMennyiseg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.TerjedelemMennyiseg as nvarchar(max)),'') != ISNULL(CAST(New.TerjedelemMennyiseg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TerjedelemMennyisegiEgyseg' as ColumnName,               cast(Old.TerjedelemMennyisegiEgyseg as nvarchar(99)) as OldValue,
               cast(New.TerjedelemMennyisegiEgyseg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.TerjedelemMennyisegiEgyseg as nvarchar(max)),'') != ISNULL(CAST(New.TerjedelemMennyisegiEgyseg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TerjedelemMegjegyzes' as ColumnName,               cast(Old.TerjedelemMegjegyzes as nvarchar(99)) as OldValue,
               cast(New.TerjedelemMegjegyzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.TerjedelemMegjegyzes as nvarchar(max)),'') != ISNULL(CAST(New.TerjedelemMegjegyzes as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SelejtezesDat' as ColumnName,               cast(Old.SelejtezesDat as nvarchar(99)) as OldValue,
               cast(New.SelejtezesDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SelejtezesDat as nvarchar(max)),'') != ISNULL(CAST(New.SelejtezesDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhCsoport_Id_Selejtezo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhCsoport_Id_Selejtezo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhCsoport_Id_Selejtezo) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhCsoport_Id_Selejtezo as nvarchar(max)),'') != ISNULL(CAST(New.FelhCsoport_Id_Selejtezo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'LeveltariAtvevoNeve' as ColumnName,               cast(Old.LeveltariAtvevoNeve as nvarchar(99)) as OldValue,
               cast(New.LeveltariAtvevoNeve as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.LeveltariAtvevoNeve as nvarchar(max)),'') != ISNULL(CAST(New.LeveltariAtvevoNeve as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ModositasErvenyessegKezdete' as ColumnName,               cast(Old.ModositasErvenyessegKezdete as nvarchar(99)) as OldValue,
               cast(New.ModositasErvenyessegKezdete as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ModositasErvenyessegKezdete as nvarchar(max)),'') != ISNULL(CAST(New.ModositasErvenyessegKezdete as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MegszuntetoHatarozat' as ColumnName,               cast(Old.MegszuntetoHatarozat as nvarchar(99)) as OldValue,
               cast(New.MegszuntetoHatarozat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MegszuntetoHatarozat as nvarchar(max)),'') != ISNULL(CAST(New.MegszuntetoHatarozat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelulvizsgalatDatuma' as ColumnName,               cast(Old.FelulvizsgalatDatuma as nvarchar(99)) as OldValue,
               cast(New.FelulvizsgalatDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelulvizsgalatDatuma as nvarchar(max)),'') != ISNULL(CAST(New.FelulvizsgalatDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felulvizsgalo_id' as ColumnName,               cast(Old.Felulvizsgalo_id as nvarchar(99)) as OldValue,
               cast(New.Felulvizsgalo_id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Felulvizsgalo_id as nvarchar(max)),'') != ISNULL(CAST(New.Felulvizsgalo_id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MinositesFelulvizsgalatEredm' as ColumnName,               cast(Old.MinositesFelulvizsgalatEredm as nvarchar(99)) as OldValue,
               cast(New.MinositesFelulvizsgalatEredm as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MinositesFelulvizsgalatEredm as nvarchar(max)),'') != ISNULL(CAST(New.MinositesFelulvizsgalatEredm as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesKezdoDatuma' as ColumnName,               cast(Old.UgyintezesKezdoDatuma as nvarchar(99)) as OldValue,
               cast(New.UgyintezesKezdoDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyintezesKezdoDatuma as nvarchar(max)),'') != ISNULL(CAST(New.UgyintezesKezdoDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EljarasiSzakasz' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EljarasiSzakasz as nvarchar(max)),'') != ISNULL(CAST(New.EljarasiSzakasz as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_KodCsoportok KCs on KCs.Kod = 'ELJARASI_SZAKASZ_FOK'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.EljarasiSzakasz and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.EljarasiSzakasz and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatralevoNapok' as ColumnName,               cast(Old.HatralevoNapok as nvarchar(99)) as OldValue,
               cast(New.HatralevoNapok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HatralevoNapok as nvarchar(max)),'') != ISNULL(CAST(New.HatralevoNapok as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatralevoMunkaNapok' as ColumnName,               cast(Old.HatralevoMunkaNapok as nvarchar(99)) as OldValue,
               cast(New.HatralevoMunkaNapok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HatralevoMunkaNapok as nvarchar(max)),'') != ISNULL(CAST(New.HatralevoMunkaNapok as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ugy_Fajtaja' as ColumnName,               
				  OldKT.Nev as OldValue,
				  NewKT.Nev as NewValue,               
                  U.Nev as Executor, 
				  New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Ugy_Fajtaja as nvarchar(max)),'') != ISNULL(CAST(New.Ugy_Fajtaja as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGY_FAJTAJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Ugy_Fajtaja and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Ugy_Fajtaja and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MinositoSzervezet' as ColumnName,               cast(Old.MinositoSzervezet as nvarchar(99)) as OldValue,
               cast(New.MinositoSzervezet as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MinositoSzervezet as nvarchar(max)),'') != ISNULL(CAST(New.MinositoSzervezet as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MinositesFelulvizsgalatBizTag' as ColumnName,               cast(Old.MinositesFelulvizsgalatBizTag as nvarchar(99)) as OldValue,
               cast(New.MinositesFelulvizsgalatBizTag as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraIratokHistory Old
         inner join EREC_IraIratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraIratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MinositesFelulvizsgalatBizTag as nvarchar(max)),'') != ISNULL(CAST(New.MinositesFelulvizsgalatBizTag as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go