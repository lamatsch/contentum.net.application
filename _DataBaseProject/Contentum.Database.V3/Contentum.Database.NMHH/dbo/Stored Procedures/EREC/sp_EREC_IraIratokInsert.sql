/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_IraIratokInsert')
            and   type = 'P')
   drop procedure sp_EREC_IraIratokInsert
go
*/
create procedure sp_EREC_IraIratokInsert    
                @Id      uniqueidentifier = null,    
                               @PostazasIranya     char(1)  = null,
                @Alszam     int  = null,
                @Sorszam     int  = null,
                @UtolsoSorszam     int  = null,
                @UgyUgyIratDarab_Id     uniqueidentifier  = null,
                @Kategoria     nvarchar(64)  = null,
                @HivatkozasiSzam     Nvarchar(100)  = null,
                @IktatasDatuma     datetime  = null,
                @ExpedialasDatuma     datetime  = null,
                @ExpedialasModja     nvarchar(64)  = null,
                @FelhasznaloCsoport_Id_Expedial     uniqueidentifier  = null,
	            @Targy     Nvarchar(4000),
                @Jelleg     nvarchar(64)  = null,
                @SztornirozasDat     datetime  = null,
	            @FelhasznaloCsoport_Id_Iktato     uniqueidentifier,
                @FelhasznaloCsoport_Id_Kiadmany     uniqueidentifier  = null,
                @UgyintezesAlapja     nvarchar(64)  = null,
                @AdathordozoTipusa     nvarchar(64)  = null,
                @Csoport_Id_Felelos     uniqueidentifier  = null,
                @Csoport_Id_Ugyfelelos     uniqueidentifier  = null,
                @FelhasznaloCsoport_Id_Orzo     uniqueidentifier  = null,
                @KiadmanyozniKell     char(1)  = null,
                @FelhasznaloCsoport_Id_Ugyintez     uniqueidentifier  = null,
                @Hatarido     datetime  = null,
                @MegorzesiIdo     datetime  = null,
                @IratFajta     nvarchar(64)  = null,
                @Irattipus     nvarchar(64)  = null,
                @KuldKuldemenyek_Id     uniqueidentifier  = null,
                @Allapot     nvarchar(64)  = null,
                @Azonosito     Nvarchar(100)  = null,
                @GeneraltTargy     Nvarchar(400)  = null,
                @IratMetaDef_Id     uniqueidentifier  = null,
                @IrattarbaKuldDatuma      datetime  = null,
                @IrattarbaVetelDat     datetime  = null,
                @Ugyirat_Id     uniqueidentifier  = null,
                @Minosites     nvarchar(64)  = null,
                @Elintezett     char(1)  = null,
                @IratHatasaUgyintezesre     nvarchar(64)  = null,
                @FelfuggesztesOka     Nvarchar(400)  = null,
                @LezarasOka     Nvarchar(400)  = null,
                @Munkaallomas     Nvarchar(100)  = null,
                @UgyintezesModja     Nvarchar(100)  = null,
                @IntezesIdopontja     datetime  = null,
                @IntezesiIdo     nvarchar(64)  = null,
                @IntezesiIdoegyseg     nvarchar(64)  = null,
                @UzemzavarKezdete     datetime  = null,
                @UzemzavarVege     datetime  = null,
                @UzemzavarIgazoloIratSzama     Nvarchar(400)  = null,
                @FelfuggesztettNapokSzama     int  = null,
                @VisszafizetesJogcime     nvarchar(64)  = null,
                @VisszafizetesOsszege     int  = null,
                @VisszafizetesHatarozatSzama     Nvarchar(400)  = null,
                @Partner_Id_VisszafizetesCimzet     uniqueidentifier  = null,
                @Partner_Nev_VisszafizetCimzett     Nvarchar(400)  = null,
                @VisszafizetesHatarido     datetime  = null,
                @Minosito     uniqueidentifier  = null,
                @MinositesErvenyessegiIdeje     datetime  = null,
                @TerjedelemMennyiseg     int  = null,
                @TerjedelemMennyisegiEgyseg     nvarchar(64)  = null,
                @TerjedelemMegjegyzes     Nvarchar(400)  = null,
                @SelejtezesDat     datetime  = null,
                @FelhCsoport_Id_Selejtezo     uniqueidentifier  = null,
                @LeveltariAtvevoNeve     Nvarchar(100)  = null,
                @ModositasErvenyessegKezdete     datetime  = null,
                @MegszuntetoHatarozat     Nvarchar(400)  = null,
                @FelulvizsgalatDatuma     datetime  = null,
                @Felulvizsgalo_id     uniqueidentifier  = null,
                @MinositesFelulvizsgalatEredm     nvarchar(10)  = null,
                @UgyintezesKezdoDatuma     datetime  = null,
                @EljarasiSzakasz     nvarchar(64)  = null,
                @HatralevoNapok     int  = null,
                @HatralevoMunkaNapok     int  = null,
                @Ugy_Fajtaja     nvarchar(64)  = null,
                @MinositoSzervezet     uniqueidentifier  = null,
                @MinositesFelulvizsgalatBizTag     Nvarchar(4000)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @PostazasIranya is not null
         begin
            SET @insertColumns = @insertColumns + ',PostazasIranya'
            SET @insertValues = @insertValues + ',@PostazasIranya'
         end 
       
         if @Alszam is not null
         begin
            SET @insertColumns = @insertColumns + ',Alszam'
            SET @insertValues = @insertValues + ',@Alszam'
         end 
       
         if @Sorszam is not null
         begin
            SET @insertColumns = @insertColumns + ',Sorszam'
            SET @insertValues = @insertValues + ',@Sorszam'
         end 
       
         if @UtolsoSorszam is not null
         begin
            SET @insertColumns = @insertColumns + ',UtolsoSorszam'
            SET @insertValues = @insertValues + ',@UtolsoSorszam'
         end 
       
         if @UgyUgyIratDarab_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyUgyIratDarab_Id'
            SET @insertValues = @insertValues + ',@UgyUgyIratDarab_Id'
         end 
       
         if @Kategoria is not null
         begin
            SET @insertColumns = @insertColumns + ',Kategoria'
            SET @insertValues = @insertValues + ',@Kategoria'
         end 
       
         if @HivatkozasiSzam is not null
         begin
            SET @insertColumns = @insertColumns + ',HivatkozasiSzam'
            SET @insertValues = @insertValues + ',@HivatkozasiSzam'
         end 
       
         if @IktatasDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',IktatasDatuma'
            SET @insertValues = @insertValues + ',@IktatasDatuma'
         end 
       
         if @ExpedialasDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',ExpedialasDatuma'
            SET @insertValues = @insertValues + ',@ExpedialasDatuma'
         end 
       
         if @ExpedialasModja is not null
         begin
            SET @insertColumns = @insertColumns + ',ExpedialasModja'
            SET @insertValues = @insertValues + ',@ExpedialasModja'
         end 
       
         if @FelhasznaloCsoport_Id_Expedial is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Expedial'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Expedial'
         end 
       
         if @Targy is not null
         begin
            SET @insertColumns = @insertColumns + ',Targy'
            SET @insertValues = @insertValues + ',@Targy'
         end 
       
         if @Jelleg is not null
         begin
            SET @insertColumns = @insertColumns + ',Jelleg'
            SET @insertValues = @insertValues + ',@Jelleg'
         end 
       
         if @SztornirozasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SztornirozasDat'
            SET @insertValues = @insertValues + ',@SztornirozasDat'
         end 
       
         if @FelhasznaloCsoport_Id_Iktato is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Iktato'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Iktato'
         end 
       
         if @FelhasznaloCsoport_Id_Kiadmany is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Kiadmany'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Kiadmany'
         end 
       
         if @UgyintezesAlapja is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyintezesAlapja'
            SET @insertValues = @insertValues + ',@UgyintezesAlapja'
         end 
       
         if @AdathordozoTipusa is not null
         begin
            SET @insertColumns = @insertColumns + ',AdathordozoTipusa'
            SET @insertValues = @insertValues + ',@AdathordozoTipusa'
         end 
       
         if @Csoport_Id_Felelos is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Felelos'
            SET @insertValues = @insertValues + ',@Csoport_Id_Felelos'
         end 
       
         if @Csoport_Id_Ugyfelelos is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Ugyfelelos'
            SET @insertValues = @insertValues + ',@Csoport_Id_Ugyfelelos'
         end 
       
         if @FelhasznaloCsoport_Id_Orzo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Orzo'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Orzo'
         end 
       
         if @KiadmanyozniKell is not null
         begin
            SET @insertColumns = @insertColumns + ',KiadmanyozniKell'
            SET @insertValues = @insertValues + ',@KiadmanyozniKell'
         end 
       
         if @FelhasznaloCsoport_Id_Ugyintez is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Ugyintez'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Ugyintez'
         end 
       
         if @Hatarido is not null
         begin
            SET @insertColumns = @insertColumns + ',Hatarido'
            SET @insertValues = @insertValues + ',@Hatarido'
         end 
       
         if @MegorzesiIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',MegorzesiIdo'
            SET @insertValues = @insertValues + ',@MegorzesiIdo'
         end 
       
         if @IratFajta is not null
         begin
            SET @insertColumns = @insertColumns + ',IratFajta'
            SET @insertValues = @insertValues + ',@IratFajta'
         end 
       
         if @Irattipus is not null
         begin
            SET @insertColumns = @insertColumns + ',Irattipus'
            SET @insertValues = @insertValues + ',@Irattipus'
         end 
       
         if @KuldKuldemenyek_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',KuldKuldemenyek_Id'
            SET @insertValues = @insertValues + ',@KuldKuldemenyek_Id'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @Azonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Azonosito'
            SET @insertValues = @insertValues + ',@Azonosito'
         end 
       
         if @GeneraltTargy is not null
         begin
            SET @insertColumns = @insertColumns + ',GeneraltTargy'
            SET @insertValues = @insertValues + ',@GeneraltTargy'
         end 
       
         if @IratMetaDef_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IratMetaDef_Id'
            SET @insertValues = @insertValues + ',@IratMetaDef_Id'
         end 
       
         if @IrattarbaKuldDatuma  is not null
         begin
            SET @insertColumns = @insertColumns + ',IrattarbaKuldDatuma '
            SET @insertValues = @insertValues + ',@IrattarbaKuldDatuma '
         end 
       
         if @IrattarbaVetelDat is not null
         begin
            SET @insertColumns = @insertColumns + ',IrattarbaVetelDat'
            SET @insertValues = @insertValues + ',@IrattarbaVetelDat'
         end 
       
         if @Ugyirat_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Ugyirat_Id'
            SET @insertValues = @insertValues + ',@Ugyirat_Id'
         end 
       
         if @Minosites is not null
         begin
            SET @insertColumns = @insertColumns + ',Minosites'
            SET @insertValues = @insertValues + ',@Minosites'
         end 
       
         if @Elintezett is not null
         begin
            SET @insertColumns = @insertColumns + ',Elintezett'
            SET @insertValues = @insertValues + ',@Elintezett'
         end 
       
         if @IratHatasaUgyintezesre is not null
         begin
            SET @insertColumns = @insertColumns + ',IratHatasaUgyintezesre'
            SET @insertValues = @insertValues + ',@IratHatasaUgyintezesre'
         end 
       
         if @FelfuggesztesOka is not null
         begin
            SET @insertColumns = @insertColumns + ',FelfuggesztesOka'
            SET @insertValues = @insertValues + ',@FelfuggesztesOka'
         end 
       
         if @LezarasOka is not null
         begin
            SET @insertColumns = @insertColumns + ',LezarasOka'
            SET @insertValues = @insertValues + ',@LezarasOka'
         end 
       
         if @Munkaallomas is not null
         begin
            SET @insertColumns = @insertColumns + ',Munkaallomas'
            SET @insertValues = @insertValues + ',@Munkaallomas'
         end 
       
         if @UgyintezesModja is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyintezesModja'
            SET @insertValues = @insertValues + ',@UgyintezesModja'
         end 
       
         if @IntezesIdopontja is not null
         begin
            SET @insertColumns = @insertColumns + ',IntezesIdopontja'
            SET @insertValues = @insertValues + ',@IntezesIdopontja'
         end 
       
         if @IntezesiIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',IntezesiIdo'
            SET @insertValues = @insertValues + ',@IntezesiIdo'
         end 
       
         if @IntezesiIdoegyseg is not null
         begin
            SET @insertColumns = @insertColumns + ',IntezesiIdoegyseg'
            SET @insertValues = @insertValues + ',@IntezesiIdoegyseg'
         end 
       
         if @UzemzavarKezdete is not null
         begin
            SET @insertColumns = @insertColumns + ',UzemzavarKezdete'
            SET @insertValues = @insertValues + ',@UzemzavarKezdete'
         end 
       
         if @UzemzavarVege is not null
         begin
            SET @insertColumns = @insertColumns + ',UzemzavarVege'
            SET @insertValues = @insertValues + ',@UzemzavarVege'
         end 
       
         if @UzemzavarIgazoloIratSzama is not null
         begin
            SET @insertColumns = @insertColumns + ',UzemzavarIgazoloIratSzama'
            SET @insertValues = @insertValues + ',@UzemzavarIgazoloIratSzama'
         end 
       
         if @FelfuggesztettNapokSzama is not null
         begin
            SET @insertColumns = @insertColumns + ',FelfuggesztettNapokSzama'
            SET @insertValues = @insertValues + ',@FelfuggesztettNapokSzama'
         end 
       
         if @VisszafizetesJogcime is not null
         begin
            SET @insertColumns = @insertColumns + ',VisszafizetesJogcime'
            SET @insertValues = @insertValues + ',@VisszafizetesJogcime'
         end 
       
         if @VisszafizetesOsszege is not null
         begin
            SET @insertColumns = @insertColumns + ',VisszafizetesOsszege'
            SET @insertValues = @insertValues + ',@VisszafizetesOsszege'
         end 
       
         if @VisszafizetesHatarozatSzama is not null
         begin
            SET @insertColumns = @insertColumns + ',VisszafizetesHatarozatSzama'
            SET @insertValues = @insertValues + ',@VisszafizetesHatarozatSzama'
         end 
       
         if @Partner_Id_VisszafizetesCimzet is not null
         begin
            SET @insertColumns = @insertColumns + ',Partner_Id_VisszafizetesCimzet'
            SET @insertValues = @insertValues + ',@Partner_Id_VisszafizetesCimzet'
         end 
       
         if @Partner_Nev_VisszafizetCimzett is not null
         begin
            SET @insertColumns = @insertColumns + ',Partner_Nev_VisszafizetCimzett'
            SET @insertValues = @insertValues + ',@Partner_Nev_VisszafizetCimzett'
         end 
       
         if @VisszafizetesHatarido is not null
         begin
            SET @insertColumns = @insertColumns + ',VisszafizetesHatarido'
            SET @insertValues = @insertValues + ',@VisszafizetesHatarido'
         end 
       
         if @Minosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Minosito'
            SET @insertValues = @insertValues + ',@Minosito'
         end 
       
         if @MinositesErvenyessegiIdeje is not null
         begin
            SET @insertColumns = @insertColumns + ',MinositesErvenyessegiIdeje'
            SET @insertValues = @insertValues + ',@MinositesErvenyessegiIdeje'
         end 
       
         if @TerjedelemMennyiseg is not null
         begin
            SET @insertColumns = @insertColumns + ',TerjedelemMennyiseg'
            SET @insertValues = @insertValues + ',@TerjedelemMennyiseg'
         end 
       
         if @TerjedelemMennyisegiEgyseg is not null
         begin
            SET @insertColumns = @insertColumns + ',TerjedelemMennyisegiEgyseg'
            SET @insertValues = @insertValues + ',@TerjedelemMennyisegiEgyseg'
         end 
       
         if @TerjedelemMegjegyzes is not null
         begin
            SET @insertColumns = @insertColumns + ',TerjedelemMegjegyzes'
            SET @insertValues = @insertValues + ',@TerjedelemMegjegyzes'
         end 
       
         if @SelejtezesDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SelejtezesDat'
            SET @insertValues = @insertValues + ',@SelejtezesDat'
         end 
       
         if @FelhCsoport_Id_Selejtezo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhCsoport_Id_Selejtezo'
            SET @insertValues = @insertValues + ',@FelhCsoport_Id_Selejtezo'
         end 
       
         if @LeveltariAtvevoNeve is not null
         begin
            SET @insertColumns = @insertColumns + ',LeveltariAtvevoNeve'
            SET @insertValues = @insertValues + ',@LeveltariAtvevoNeve'
         end 
       
         if @ModositasErvenyessegKezdete is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasErvenyessegKezdete'
            SET @insertValues = @insertValues + ',@ModositasErvenyessegKezdete'
         end 
       
         if @MegszuntetoHatarozat is not null
         begin
            SET @insertColumns = @insertColumns + ',MegszuntetoHatarozat'
            SET @insertValues = @insertValues + ',@MegszuntetoHatarozat'
         end 
       
         if @FelulvizsgalatDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',FelulvizsgalatDatuma'
            SET @insertValues = @insertValues + ',@FelulvizsgalatDatuma'
         end 
       
         if @Felulvizsgalo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Felulvizsgalo_id'
            SET @insertValues = @insertValues + ',@Felulvizsgalo_id'
         end 
       
         if @MinositesFelulvizsgalatEredm is not null
         begin
            SET @insertColumns = @insertColumns + ',MinositesFelulvizsgalatEredm'
            SET @insertValues = @insertValues + ',@MinositesFelulvizsgalatEredm'
         end 
       
         if @UgyintezesKezdoDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyintezesKezdoDatuma'
            SET @insertValues = @insertValues + ',@UgyintezesKezdoDatuma'
         end 
       
         if @EljarasiSzakasz is not null
         begin
            SET @insertColumns = @insertColumns + ',EljarasiSzakasz'
            SET @insertValues = @insertValues + ',@EljarasiSzakasz'
         end
       
         if @Ugy_Fajtaja is not null
         begin
            SET @insertColumns = @insertColumns + ',Ugy_Fajtaja'
            SET @insertValues = @insertValues + ',@Ugy_Fajtaja'
         end 
       
         if @MinositoSzervezet is not null
         begin
            SET @insertColumns = @insertColumns + ',MinositoSzervezet'
            SET @insertValues = @insertValues + ',@MinositoSzervezet'
         end 
       
         if @MinositesFelulvizsgalatBizTag is not null
         begin
            SET @insertColumns = @insertColumns + ',MinositesFelulvizsgalatBizTag'
            SET @insertValues = @insertValues + ',@MinositesFelulvizsgalatBizTag'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_IraIratok ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@PostazasIranya char(1),@Alszam int,@Sorszam int,@UtolsoSorszam int,@UgyUgyIratDarab_Id uniqueidentifier,@Kategoria nvarchar(64),@HivatkozasiSzam Nvarchar(100),@IktatasDatuma datetime,@ExpedialasDatuma datetime,@ExpedialasModja nvarchar(64),@FelhasznaloCsoport_Id_Expedial uniqueidentifier,@Targy Nvarchar(4000),@Jelleg nvarchar(64),@SztornirozasDat datetime,@FelhasznaloCsoport_Id_Iktato uniqueidentifier,@FelhasznaloCsoport_Id_Kiadmany uniqueidentifier,@UgyintezesAlapja nvarchar(64),@AdathordozoTipusa nvarchar(64),@Csoport_Id_Felelos uniqueidentifier,@Csoport_Id_Ugyfelelos uniqueidentifier,@FelhasznaloCsoport_Id_Orzo uniqueidentifier,@KiadmanyozniKell char(1),@FelhasznaloCsoport_Id_Ugyintez uniqueidentifier,@Hatarido datetime,@MegorzesiIdo datetime,@IratFajta nvarchar(64),@Irattipus nvarchar(64),@KuldKuldemenyek_Id uniqueidentifier,@Allapot nvarchar(64),@Azonosito Nvarchar(100),@GeneraltTargy Nvarchar(400),@IratMetaDef_Id uniqueidentifier,@IrattarbaKuldDatuma  datetime,@IrattarbaVetelDat datetime,@Ugyirat_Id uniqueidentifier,@Minosites nvarchar(64),@Elintezett char(1),@IratHatasaUgyintezesre nvarchar(64),@FelfuggesztesOka Nvarchar(400),@LezarasOka Nvarchar(400),@Munkaallomas Nvarchar(100),@UgyintezesModja Nvarchar(100),@IntezesIdopontja datetime,@IntezesiIdo nvarchar(64),@IntezesiIdoegyseg nvarchar(64),@UzemzavarKezdete datetime,@UzemzavarVege datetime,@UzemzavarIgazoloIratSzama Nvarchar(400),@FelfuggesztettNapokSzama int,@VisszafizetesJogcime nvarchar(64),@VisszafizetesOsszege int,@VisszafizetesHatarozatSzama Nvarchar(400),@Partner_Id_VisszafizetesCimzet uniqueidentifier,@Partner_Nev_VisszafizetCimzett Nvarchar(400),@VisszafizetesHatarido datetime,@Minosito uniqueidentifier,@MinositesErvenyessegiIdeje datetime,@TerjedelemMennyiseg int,@TerjedelemMennyisegiEgyseg nvarchar(64),@TerjedelemMegjegyzes Nvarchar(400),@SelejtezesDat datetime,@FelhCsoport_Id_Selejtezo uniqueidentifier,@LeveltariAtvevoNeve Nvarchar(100),@ModositasErvenyessegKezdete datetime,@MegszuntetoHatarozat Nvarchar(400),@FelulvizsgalatDatuma datetime,@Felulvizsgalo_id uniqueidentifier,@MinositesFelulvizsgalatEredm nvarchar(10),@UgyintezesKezdoDatuma datetime,@EljarasiSzakasz nvarchar(64),@Ugy_Fajtaja nvarchar(64),@MinositoSzervezet uniqueidentifier,@MinositesFelulvizsgalatBizTag Nvarchar(4000),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@PostazasIranya = @PostazasIranya,@Alszam = @Alszam,@Sorszam = @Sorszam,@UtolsoSorszam = @UtolsoSorszam,@UgyUgyIratDarab_Id = @UgyUgyIratDarab_Id,@Kategoria = @Kategoria,@HivatkozasiSzam = @HivatkozasiSzam,@IktatasDatuma = @IktatasDatuma,@ExpedialasDatuma = @ExpedialasDatuma,@ExpedialasModja = @ExpedialasModja,@FelhasznaloCsoport_Id_Expedial = @FelhasznaloCsoport_Id_Expedial,@Targy = @Targy,@Jelleg = @Jelleg,@SztornirozasDat = @SztornirozasDat,@FelhasznaloCsoport_Id_Iktato = @FelhasznaloCsoport_Id_Iktato,@FelhasznaloCsoport_Id_Kiadmany = @FelhasznaloCsoport_Id_Kiadmany,@UgyintezesAlapja = @UgyintezesAlapja,@AdathordozoTipusa = @AdathordozoTipusa,@Csoport_Id_Felelos = @Csoport_Id_Felelos,@Csoport_Id_Ugyfelelos = @Csoport_Id_Ugyfelelos,@FelhasznaloCsoport_Id_Orzo = @FelhasznaloCsoport_Id_Orzo,@KiadmanyozniKell = @KiadmanyozniKell,@FelhasznaloCsoport_Id_Ugyintez = @FelhasznaloCsoport_Id_Ugyintez,@Hatarido = @Hatarido,@MegorzesiIdo = @MegorzesiIdo,@IratFajta = @IratFajta,@Irattipus = @Irattipus,@KuldKuldemenyek_Id = @KuldKuldemenyek_Id,@Allapot = @Allapot,@Azonosito = @Azonosito,@GeneraltTargy = @GeneraltTargy,@IratMetaDef_Id = @IratMetaDef_Id,@IrattarbaKuldDatuma  = @IrattarbaKuldDatuma ,@IrattarbaVetelDat = @IrattarbaVetelDat,@Ugyirat_Id = @Ugyirat_Id,@Minosites = @Minosites,@Elintezett = @Elintezett,@IratHatasaUgyintezesre = @IratHatasaUgyintezesre,@FelfuggesztesOka = @FelfuggesztesOka,@LezarasOka = @LezarasOka,@Munkaallomas = @Munkaallomas,@UgyintezesModja = @UgyintezesModja,@IntezesIdopontja = @IntezesIdopontja,@IntezesiIdo = @IntezesiIdo,@IntezesiIdoegyseg = @IntezesiIdoegyseg,@UzemzavarKezdete = @UzemzavarKezdete,@UzemzavarVege = @UzemzavarVege,@UzemzavarIgazoloIratSzama = @UzemzavarIgazoloIratSzama,@FelfuggesztettNapokSzama = @FelfuggesztettNapokSzama,@VisszafizetesJogcime = @VisszafizetesJogcime,@VisszafizetesOsszege = @VisszafizetesOsszege,@VisszafizetesHatarozatSzama = @VisszafizetesHatarozatSzama,@Partner_Id_VisszafizetesCimzet = @Partner_Id_VisszafizetesCimzet,@Partner_Nev_VisszafizetCimzett = @Partner_Nev_VisszafizetCimzett,@VisszafizetesHatarido = @VisszafizetesHatarido,@Minosito = @Minosito,@MinositesErvenyessegiIdeje = @MinositesErvenyessegiIdeje,@TerjedelemMennyiseg = @TerjedelemMennyiseg,@TerjedelemMennyisegiEgyseg = @TerjedelemMennyisegiEgyseg,@TerjedelemMegjegyzes = @TerjedelemMegjegyzes,@SelejtezesDat = @SelejtezesDat,@FelhCsoport_Id_Selejtezo = @FelhCsoport_Id_Selejtezo,@LeveltariAtvevoNeve = @LeveltariAtvevoNeve,@ModositasErvenyessegKezdete = @ModositasErvenyessegKezdete,@MegszuntetoHatarozat = @MegszuntetoHatarozat,@FelulvizsgalatDatuma = @FelulvizsgalatDatuma,@Felulvizsgalo_id = @Felulvizsgalo_id,@MinositesFelulvizsgalatEredm = @MinositesFelulvizsgalatEredm,@UgyintezesKezdoDatuma = @UgyintezesKezdoDatuma,@EljarasiSzakasz = @EljarasiSzakasz,@Ugy_Fajtaja = @Ugy_Fajtaja,@MinositoSzervezet = @MinositoSzervezet,@MinositesFelulvizsgalatBizTag = @MinositesFelulvizsgalatBizTag,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_IraIratok',@ResultUid
					,'EREC_IraIratokHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
go

/* használt errorcode-ok:
	50301:
	50302: 'Org' oszlop értéke nem lehet NULL
*/
