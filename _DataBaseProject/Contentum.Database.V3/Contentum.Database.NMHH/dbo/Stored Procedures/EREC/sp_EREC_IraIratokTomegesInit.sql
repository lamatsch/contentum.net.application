--set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--if exists (select 1
--            from  sysobjects
--           where  id = object_id('sp_EREC_IraIratokTomegesInit')
--            and   type = 'P')
--   drop procedure sp_EREC_IraIratokTomegesInit
--go


CREATE procedure [dbo].[sp_EREC_IraIratokTomegesInit]
				@PostazasIranya char(1)  = null,      
                @AlszamList     nvarchar(max)   = null,
				@UtolsoSorszam int = null,
				@IktatasDatuma datetime = null,
				@Targy nvarchar(4000) = null,
				@FelhasznaloCsoport_Id_Iktato uniqueidentifier = null,
				@KuldKuldemenyek_IdList nvarchar(max)   = null,
				@Allapot  nvarchar(64)  = null,
				@AzonositoList nvarchar(max) = null,
				@Ugyirat_IdList nvarchar(max) = null,
                @Letrehozo_id   uniqueidentifier  = null
				
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;

declare @VegrehajtasIdo datetime
set @VegrehajtasIdo = getdate()

declare @temp table(Alszam int, Azonosito nvarchar(100), KuldKuldemenyek_Id uniqueidentifier, Ugyirat_Id uniqueidentifier, UgyUgyIratDarab_Id uniqueidentifier)

insert into @temp
(Alszam, Azonosito,  KuldKuldemenyek_Id, Ugyirat_Id)
select t1.[Value], t2.[Value], case when t3.[Value] = '' then null else t3.[Value] end, t4.[Value] from
(select [Value], Pos  from fn_SplitWithPos(@AlszamList, ',')) t1,
(select [Value], Pos  from fn_SplitWithPos(@AzonositoList, ',')) t2,
(select [Value], Pos  from fn_SplitWithPos(@KuldKuldemenyek_IdList, ',')) t3,
(select [Value], Pos  from fn_SplitWithPos(@Ugyirat_IdList, ',')) t4
where t1.Pos = t2.Pos
and t1.Pos = t3.Pos
and t1.Pos = t4.Pos

update @temp
set UgyUgyIratDarab_Id = ud.Id
from @temp t join EREC_UgyUgyiratok u on t.Ugyirat_Id = u.Id
join EREC_UgyUgyiratdarabok ud on ud.UgyUgyirat_Id = u.Id

DECLARE @InsertedRow TABLE (id uniqueidentifier)

insert into EREC_IraIratok
	(PostazasIranya,      
     Alszam,
	 UtolsoSorszam,
	 UgyUgyIratDarab_Id,
	 IktatasDatuma,
	 Targy,
	 FelhasznaloCsoport_Id_Iktato,
	 KuldKuldemenyek_Id,
	 Allapot,
	 Azonosito,
	 Ugyirat_Id,
     Letrehozo_id)
output inserted.id into @InsertedRow
select
	 @PostazasIranya,      
     t.Alszam,
	 @UtolsoSorszam,
	 t.UgyUgyIratDarab_Id,
	 @IktatasDatuma,
	 @Targy,
	 @FelhasznaloCsoport_Id_Iktato,
	 t.KuldKuldemenyek_Id,
	 @Allapot,
	 t.Azonosito,
	 t.Ugyirat_Id,
     @Letrehozo_id
from
@temp t
    

if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
      /* History Log */
      declare @row_ids varchar(MAX)
      /*** EREC_IraIratok history log ***/

      set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @InsertedRow FOR XML PATH('')),1, 2, '')
      if @row_ids is not null
         set @row_ids = @row_ids + '''';

      exec sp_LogRecordsToHistory_Tomeges
       @TableName = 'EREC_IraIratok'
      ,@Row_Ids = @row_ids
      ,@Muvelet = 0
      ,@Vegrehajto_Id = @Letrehozo_id
      ,@VegrehajtasIdo = @VegrehajtasIdo
END            


select Id from @InsertedRow
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH