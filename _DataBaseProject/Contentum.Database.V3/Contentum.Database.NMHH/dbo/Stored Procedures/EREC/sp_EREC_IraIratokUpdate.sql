
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_IraIratokUpdate')
            and   type = 'P')
   drop procedure sp_EREC_IraIratokUpdate
go
*/
create procedure sp_EREC_IraIratokUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @PostazasIranya     char(1)  = null,         
             @Alszam     int  = null,         
             @Sorszam     int  = null,         
             @UtolsoSorszam     int  = null,         
             @UgyUgyIratDarab_Id     uniqueidentifier  = null,         
             @Kategoria     nvarchar(64)  = null,         
             @HivatkozasiSzam     Nvarchar(100)  = null,         
             @IktatasDatuma     datetime  = null,         
             @ExpedialasDatuma     datetime  = null,         
             @ExpedialasModja     nvarchar(64)  = null,         
             @FelhasznaloCsoport_Id_Expedial     uniqueidentifier  = null,         
             @Targy     Nvarchar(4000)  = null,         
             @Jelleg     nvarchar(64)  = null,         
             @SztornirozasDat     datetime  = null,         
             @FelhasznaloCsoport_Id_Iktato     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Kiadmany     uniqueidentifier  = null,         
             @UgyintezesAlapja     nvarchar(64)  = null,         
             @AdathordozoTipusa     nvarchar(64)  = null,         
             @Csoport_Id_Felelos     uniqueidentifier  = null,         
             @Csoport_Id_Ugyfelelos     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Orzo     uniqueidentifier  = null,         
             @KiadmanyozniKell     char(1)  = null,         
             @FelhasznaloCsoport_Id_Ugyintez     uniqueidentifier  = null,         
             @Hatarido     datetime  = null,         
             @MegorzesiIdo     datetime  = null,         
             @IratFajta     nvarchar(64)  = null,         
             @Irattipus     nvarchar(64)  = null,         
             @KuldKuldemenyek_Id     uniqueidentifier  = null,         
             @Allapot     nvarchar(64)  = null,         
             @Azonosito     Nvarchar(100)  = null,         
             @GeneraltTargy     Nvarchar(400)  = null,         
             @IratMetaDef_Id     uniqueidentifier  = null,         
             @IrattarbaKuldDatuma      datetime  = null,         
             @IrattarbaVetelDat     datetime  = null,         
             @Ugyirat_Id     uniqueidentifier  = null,         
             @Minosites     nvarchar(64)  = null,         
             @Elintezett     char(1)  = null,         
             @IratHatasaUgyintezesre     nvarchar(64)  = null,         
             @FelfuggesztesOka     Nvarchar(400)  = null,         
             @LezarasOka     Nvarchar(400)  = null,         
             @Munkaallomas     Nvarchar(100)  = null,         
             @UgyintezesModja     Nvarchar(100)  = null,         
             @IntezesIdopontja     datetime  = null,         
             @IntezesiIdo     nvarchar(64)  = null,         
             @IntezesiIdoegyseg     nvarchar(64)  = null,         
             @UzemzavarKezdete     datetime  = null,         
             @UzemzavarVege     datetime  = null,         
             @UzemzavarIgazoloIratSzama     Nvarchar(400)  = null,         
             @FelfuggesztettNapokSzama     int  = null,         
             @VisszafizetesJogcime     nvarchar(64)  = null,         
             @VisszafizetesOsszege     int  = null,         
             @VisszafizetesHatarozatSzama     Nvarchar(400)  = null,         
             @Partner_Id_VisszafizetesCimzet     uniqueidentifier  = null,         
             @Partner_Nev_VisszafizetCimzett     Nvarchar(400)  = null,         
             @VisszafizetesHatarido     datetime  = null,         
             @Minosito     uniqueidentifier  = null,         
             @MinositesErvenyessegiIdeje     datetime  = null,         
             @TerjedelemMennyiseg     int  = null,         
             @TerjedelemMennyisegiEgyseg     nvarchar(64)  = null,         
             @TerjedelemMegjegyzes     Nvarchar(400)  = null,         
             @SelejtezesDat     datetime  = null,         
             @FelhCsoport_Id_Selejtezo     uniqueidentifier  = null,         
             @LeveltariAtvevoNeve     Nvarchar(100)  = null,         
             @ModositasErvenyessegKezdete     datetime  = null,         
             @MegszuntetoHatarozat     Nvarchar(400)  = null,         
             @FelulvizsgalatDatuma     datetime  = null,         
             @Felulvizsgalo_id     uniqueidentifier  = null,         
             @MinositesFelulvizsgalatEredm     nvarchar(10)  = null,         
             @UgyintezesKezdoDatuma     datetime  = null,         
             @EljarasiSzakasz     nvarchar(64)  = null,         
             @HatralevoNapok     int  = null,         
             @HatralevoMunkaNapok     int  = null,         
             @Ugy_Fajtaja     nvarchar(64)  = null,         
             @MinositoSzervezet     uniqueidentifier  = null,         
             @MinositesFelulvizsgalatBizTag     Nvarchar(4000)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_PostazasIranya     char(1)         
     DECLARE @Act_Alszam     int         
     DECLARE @Act_Sorszam     int         
     DECLARE @Act_UtolsoSorszam     int         
     DECLARE @Act_UgyUgyIratDarab_Id     uniqueidentifier         
     DECLARE @Act_Kategoria     nvarchar(64)         
     DECLARE @Act_HivatkozasiSzam     Nvarchar(100)         
     DECLARE @Act_IktatasDatuma     datetime         
     DECLARE @Act_ExpedialasDatuma     datetime         
     DECLARE @Act_ExpedialasModja     nvarchar(64)         
     DECLARE @Act_FelhasznaloCsoport_Id_Expedial     uniqueidentifier         
     DECLARE @Act_Targy     Nvarchar(4000)         
     DECLARE @Act_Jelleg     nvarchar(64)         
     DECLARE @Act_SztornirozasDat     datetime         
     DECLARE @Act_FelhasznaloCsoport_Id_Iktato     uniqueidentifier         
     DECLARE @Act_FelhasznaloCsoport_Id_Kiadmany     uniqueidentifier         
     DECLARE @Act_UgyintezesAlapja     nvarchar(64)         
     DECLARE @Act_AdathordozoTipusa     nvarchar(64)         
     DECLARE @Act_Csoport_Id_Felelos     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Ugyfelelos     uniqueidentifier         
     DECLARE @Act_FelhasznaloCsoport_Id_Orzo     uniqueidentifier         
     DECLARE @Act_KiadmanyozniKell     char(1)         
     DECLARE @Act_FelhasznaloCsoport_Id_Ugyintez     uniqueidentifier         
     DECLARE @Act_Hatarido     datetime         
     DECLARE @Act_MegorzesiIdo     datetime         
     DECLARE @Act_IratFajta     nvarchar(64)         
     DECLARE @Act_Irattipus     nvarchar(64)         
     DECLARE @Act_KuldKuldemenyek_Id     uniqueidentifier         
     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_Azonosito     Nvarchar(100)         
     DECLARE @Act_GeneraltTargy     Nvarchar(400)         
     DECLARE @Act_IratMetaDef_Id     uniqueidentifier         
     DECLARE @Act_IrattarbaKuldDatuma      datetime         
     DECLARE @Act_IrattarbaVetelDat     datetime         
     DECLARE @Act_Ugyirat_Id     uniqueidentifier         
     DECLARE @Act_Minosites     nvarchar(64)         
     DECLARE @Act_Elintezett     char(1)         
     DECLARE @Act_IratHatasaUgyintezesre     nvarchar(64)         
     DECLARE @Act_FelfuggesztesOka     Nvarchar(400)         
     DECLARE @Act_LezarasOka     Nvarchar(400)         
     DECLARE @Act_Munkaallomas     Nvarchar(100)         
     DECLARE @Act_UgyintezesModja     Nvarchar(100)         
     DECLARE @Act_IntezesIdopontja     datetime         
     DECLARE @Act_IntezesiIdo     nvarchar(64)         
     DECLARE @Act_IntezesiIdoegyseg     nvarchar(64)         
     DECLARE @Act_UzemzavarKezdete     datetime         
     DECLARE @Act_UzemzavarVege     datetime         
     DECLARE @Act_UzemzavarIgazoloIratSzama     Nvarchar(400)         
     DECLARE @Act_FelfuggesztettNapokSzama     int         
     DECLARE @Act_VisszafizetesJogcime     nvarchar(64)         
     DECLARE @Act_VisszafizetesOsszege     int         
     DECLARE @Act_VisszafizetesHatarozatSzama     Nvarchar(400)         
     DECLARE @Act_Partner_Id_VisszafizetesCimzet     uniqueidentifier         
     DECLARE @Act_Partner_Nev_VisszafizetCimzett     Nvarchar(400)         
     DECLARE @Act_VisszafizetesHatarido     datetime         
     DECLARE @Act_Minosito     uniqueidentifier         
     DECLARE @Act_MinositesErvenyessegiIdeje     datetime         
     DECLARE @Act_TerjedelemMennyiseg     int         
     DECLARE @Act_TerjedelemMennyisegiEgyseg     nvarchar(64)         
     DECLARE @Act_TerjedelemMegjegyzes     Nvarchar(400)         
     DECLARE @Act_SelejtezesDat     datetime         
     DECLARE @Act_FelhCsoport_Id_Selejtezo     uniqueidentifier         
     DECLARE @Act_LeveltariAtvevoNeve     Nvarchar(100)         
     DECLARE @Act_ModositasErvenyessegKezdete     datetime         
     DECLARE @Act_MegszuntetoHatarozat     Nvarchar(400)         
     DECLARE @Act_FelulvizsgalatDatuma     datetime         
     DECLARE @Act_Felulvizsgalo_id     uniqueidentifier         
     DECLARE @Act_MinositesFelulvizsgalatEredm     nvarchar(10)         
     DECLARE @Act_UgyintezesKezdoDatuma     datetime         
     DECLARE @Act_EljarasiSzakasz     nvarchar(64)         
     DECLARE @Act_HatralevoNapok     int         
     DECLARE @Act_HatralevoMunkaNapok     int         
     DECLARE @Act_Ugy_Fajtaja     nvarchar(64)         
     DECLARE @Act_MinositoSzervezet     uniqueidentifier         
     DECLARE @Act_MinositesFelulvizsgalatBizTag     Nvarchar(4000)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_PostazasIranya = PostazasIranya,
     @Act_Alszam = Alszam,
     @Act_Sorszam = Sorszam,
     @Act_UtolsoSorszam = UtolsoSorszam,
     @Act_UgyUgyIratDarab_Id = UgyUgyIratDarab_Id,
     @Act_Kategoria = Kategoria,
     @Act_HivatkozasiSzam = HivatkozasiSzam,
     @Act_IktatasDatuma = IktatasDatuma,
     @Act_ExpedialasDatuma = ExpedialasDatuma,
     @Act_ExpedialasModja = ExpedialasModja,
     @Act_FelhasznaloCsoport_Id_Expedial = FelhasznaloCsoport_Id_Expedial,
     @Act_Targy = Targy,
     @Act_Jelleg = Jelleg,
     @Act_SztornirozasDat = SztornirozasDat,
     @Act_FelhasznaloCsoport_Id_Iktato = FelhasznaloCsoport_Id_Iktato,
     @Act_FelhasznaloCsoport_Id_Kiadmany = FelhasznaloCsoport_Id_Kiadmany,
     @Act_UgyintezesAlapja = UgyintezesAlapja,
     @Act_AdathordozoTipusa = AdathordozoTipusa,
     @Act_Csoport_Id_Felelos = Csoport_Id_Felelos,
     @Act_Csoport_Id_Ugyfelelos = Csoport_Id_Ugyfelelos,
     @Act_FelhasznaloCsoport_Id_Orzo = FelhasznaloCsoport_Id_Orzo,
     @Act_KiadmanyozniKell = KiadmanyozniKell,
     @Act_FelhasznaloCsoport_Id_Ugyintez = FelhasznaloCsoport_Id_Ugyintez,
     @Act_Hatarido = Hatarido,
     @Act_MegorzesiIdo = MegorzesiIdo,
     @Act_IratFajta = IratFajta,
     @Act_Irattipus = Irattipus,
     @Act_KuldKuldemenyek_Id = KuldKuldemenyek_Id,
     @Act_Allapot = Allapot,
     @Act_Azonosito = Azonosito,
     @Act_GeneraltTargy = GeneraltTargy,
     @Act_IratMetaDef_Id = IratMetaDef_Id,
     @Act_IrattarbaKuldDatuma  = IrattarbaKuldDatuma ,
     @Act_IrattarbaVetelDat = IrattarbaVetelDat,
     @Act_Ugyirat_Id = Ugyirat_Id,
     @Act_Minosites = Minosites,
     @Act_Elintezett = Elintezett,
     @Act_IratHatasaUgyintezesre = IratHatasaUgyintezesre,
     @Act_FelfuggesztesOka = FelfuggesztesOka,
     @Act_LezarasOka = LezarasOka,
     @Act_Munkaallomas = Munkaallomas,
     @Act_UgyintezesModja = UgyintezesModja,
     @Act_IntezesIdopontja = IntezesIdopontja,
     @Act_IntezesiIdo = IntezesiIdo,
     @Act_IntezesiIdoegyseg = IntezesiIdoegyseg,
     @Act_UzemzavarKezdete = UzemzavarKezdete,
     @Act_UzemzavarVege = UzemzavarVege,
     @Act_UzemzavarIgazoloIratSzama = UzemzavarIgazoloIratSzama,
     @Act_FelfuggesztettNapokSzama = FelfuggesztettNapokSzama,
     @Act_VisszafizetesJogcime = VisszafizetesJogcime,
     @Act_VisszafizetesOsszege = VisszafizetesOsszege,
     @Act_VisszafizetesHatarozatSzama = VisszafizetesHatarozatSzama,
     @Act_Partner_Id_VisszafizetesCimzet = Partner_Id_VisszafizetesCimzet,
     @Act_Partner_Nev_VisszafizetCimzett = Partner_Nev_VisszafizetCimzett,
     @Act_VisszafizetesHatarido = VisszafizetesHatarido,
     @Act_Minosito = Minosito,
     @Act_MinositesErvenyessegiIdeje = MinositesErvenyessegiIdeje,
     @Act_TerjedelemMennyiseg = TerjedelemMennyiseg,
     @Act_TerjedelemMennyisegiEgyseg = TerjedelemMennyisegiEgyseg,
     @Act_TerjedelemMegjegyzes = TerjedelemMegjegyzes,
     @Act_SelejtezesDat = SelejtezesDat,
     @Act_FelhCsoport_Id_Selejtezo = FelhCsoport_Id_Selejtezo,
     @Act_LeveltariAtvevoNeve = LeveltariAtvevoNeve,
     @Act_ModositasErvenyessegKezdete = ModositasErvenyessegKezdete,
     @Act_MegszuntetoHatarozat = MegszuntetoHatarozat,
     @Act_FelulvizsgalatDatuma = FelulvizsgalatDatuma,
     @Act_Felulvizsgalo_id = Felulvizsgalo_id,
     @Act_MinositesFelulvizsgalatEredm = MinositesFelulvizsgalatEredm,
     @Act_UgyintezesKezdoDatuma = UgyintezesKezdoDatuma,
     @Act_EljarasiSzakasz = EljarasiSzakasz,
     @Act_Ugy_Fajtaja = Ugy_Fajtaja,
     @Act_MinositoSzervezet = MinositoSzervezet,
     @Act_MinositesFelulvizsgalatBizTag = MinositesFelulvizsgalatBizTag,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_IraIratok
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zárolás ellenőrzés:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/PostazasIranya')=1
         SET @Act_PostazasIranya = @PostazasIranya
   IF @UpdatedColumns.exist('/root/Alszam')=1
         SET @Act_Alszam = @Alszam
   IF @UpdatedColumns.exist('/root/Sorszam')=1
         SET @Act_Sorszam = @Sorszam
   IF @UpdatedColumns.exist('/root/UtolsoSorszam')=1
         SET @Act_UtolsoSorszam = @UtolsoSorszam
   IF @UpdatedColumns.exist('/root/UgyUgyIratDarab_Id')=1
         SET @Act_UgyUgyIratDarab_Id = @UgyUgyIratDarab_Id
   IF @UpdatedColumns.exist('/root/Kategoria')=1
         SET @Act_Kategoria = @Kategoria
   IF @UpdatedColumns.exist('/root/HivatkozasiSzam')=1
         SET @Act_HivatkozasiSzam = @HivatkozasiSzam
   IF @UpdatedColumns.exist('/root/IktatasDatuma')=1
         SET @Act_IktatasDatuma = @IktatasDatuma
   IF @UpdatedColumns.exist('/root/ExpedialasDatuma')=1
         SET @Act_ExpedialasDatuma = @ExpedialasDatuma
   IF @UpdatedColumns.exist('/root/ExpedialasModja')=1
         SET @Act_ExpedialasModja = @ExpedialasModja
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Expedial')=1
         SET @Act_FelhasznaloCsoport_Id_Expedial = @FelhasznaloCsoport_Id_Expedial
   IF @UpdatedColumns.exist('/root/Targy')=1
         SET @Act_Targy = @Targy
   IF @UpdatedColumns.exist('/root/Jelleg')=1
         SET @Act_Jelleg = @Jelleg
   IF @UpdatedColumns.exist('/root/SztornirozasDat')=1
         SET @Act_SztornirozasDat = @SztornirozasDat
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Iktato')=1
         SET @Act_FelhasznaloCsoport_Id_Iktato = @FelhasznaloCsoport_Id_Iktato
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Kiadmany')=1
         SET @Act_FelhasznaloCsoport_Id_Kiadmany = @FelhasznaloCsoport_Id_Kiadmany
   IF @UpdatedColumns.exist('/root/UgyintezesAlapja')=1
         SET @Act_UgyintezesAlapja = @UgyintezesAlapja
   IF @UpdatedColumns.exist('/root/AdathordozoTipusa')=1
         SET @Act_AdathordozoTipusa = @AdathordozoTipusa
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1
         SET @Act_Csoport_Id_Felelos = @Csoport_Id_Felelos
   IF @UpdatedColumns.exist('/root/Csoport_Id_Ugyfelelos')=1
         SET @Act_Csoport_Id_Ugyfelelos = @Csoport_Id_Ugyfelelos
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Orzo')=1
         SET @Act_FelhasznaloCsoport_Id_Orzo = @FelhasznaloCsoport_Id_Orzo
   IF @UpdatedColumns.exist('/root/KiadmanyozniKell')=1
         SET @Act_KiadmanyozniKell = @KiadmanyozniKell
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Ugyintez')=1
         SET @Act_FelhasznaloCsoport_Id_Ugyintez = @FelhasznaloCsoport_Id_Ugyintez
   IF @UpdatedColumns.exist('/root/Hatarido')=1
         SET @Act_Hatarido = @Hatarido
   IF @UpdatedColumns.exist('/root/MegorzesiIdo')=1
         SET @Act_MegorzesiIdo = @MegorzesiIdo
   IF @UpdatedColumns.exist('/root/IratFajta')=1
         SET @Act_IratFajta = @IratFajta
   IF @UpdatedColumns.exist('/root/Irattipus')=1
         SET @Act_Irattipus = @Irattipus
   IF @UpdatedColumns.exist('/root/KuldKuldemenyek_Id')=1
         SET @Act_KuldKuldemenyek_Id = @KuldKuldemenyek_Id
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/Azonosito')=1
         SET @Act_Azonosito = @Azonosito
   IF @UpdatedColumns.exist('/root/GeneraltTargy')=1
         SET @Act_GeneraltTargy = @GeneraltTargy
   IF @UpdatedColumns.exist('/root/IratMetaDef_Id')=1
         SET @Act_IratMetaDef_Id = @IratMetaDef_Id
   IF @UpdatedColumns.exist('/root/IrattarbaKuldDatuma ')=1
         SET @Act_IrattarbaKuldDatuma  = @IrattarbaKuldDatuma 
   IF @UpdatedColumns.exist('/root/IrattarbaVetelDat')=1
         SET @Act_IrattarbaVetelDat = @IrattarbaVetelDat
   IF @UpdatedColumns.exist('/root/Ugyirat_Id')=1
         SET @Act_Ugyirat_Id = @Ugyirat_Id
   IF @UpdatedColumns.exist('/root/Minosites')=1
         SET @Act_Minosites = @Minosites
   IF @UpdatedColumns.exist('/root/Elintezett')=1
         SET @Act_Elintezett = @Elintezett
   IF @UpdatedColumns.exist('/root/IratHatasaUgyintezesre')=1
         SET @Act_IratHatasaUgyintezesre = @IratHatasaUgyintezesre
   IF @UpdatedColumns.exist('/root/FelfuggesztesOka')=1
         SET @Act_FelfuggesztesOka = @FelfuggesztesOka
   IF @UpdatedColumns.exist('/root/LezarasOka')=1
         SET @Act_LezarasOka = @LezarasOka
   IF @UpdatedColumns.exist('/root/Munkaallomas')=1
         SET @Act_Munkaallomas = @Munkaallomas
   IF @UpdatedColumns.exist('/root/UgyintezesModja')=1
         SET @Act_UgyintezesModja = @UgyintezesModja
   IF @UpdatedColumns.exist('/root/IntezesIdopontja')=1
         SET @Act_IntezesIdopontja = @IntezesIdopontja
   IF @UpdatedColumns.exist('/root/IntezesiIdo')=1
         SET @Act_IntezesiIdo = @IntezesiIdo
   IF @UpdatedColumns.exist('/root/IntezesiIdoegyseg')=1
         SET @Act_IntezesiIdoegyseg = @IntezesiIdoegyseg
   IF @UpdatedColumns.exist('/root/UzemzavarKezdete')=1
         SET @Act_UzemzavarKezdete = @UzemzavarKezdete
   IF @UpdatedColumns.exist('/root/UzemzavarVege')=1
         SET @Act_UzemzavarVege = @UzemzavarVege
   IF @UpdatedColumns.exist('/root/UzemzavarIgazoloIratSzama')=1
         SET @Act_UzemzavarIgazoloIratSzama = @UzemzavarIgazoloIratSzama
   IF @UpdatedColumns.exist('/root/FelfuggesztettNapokSzama')=1
         SET @Act_FelfuggesztettNapokSzama = @FelfuggesztettNapokSzama
   IF @UpdatedColumns.exist('/root/VisszafizetesJogcime')=1
         SET @Act_VisszafizetesJogcime = @VisszafizetesJogcime
   IF @UpdatedColumns.exist('/root/VisszafizetesOsszege')=1
         SET @Act_VisszafizetesOsszege = @VisszafizetesOsszege
   IF @UpdatedColumns.exist('/root/VisszafizetesHatarozatSzama')=1
         SET @Act_VisszafizetesHatarozatSzama = @VisszafizetesHatarozatSzama
   IF @UpdatedColumns.exist('/root/Partner_Id_VisszafizetesCimzet')=1
         SET @Act_Partner_Id_VisszafizetesCimzet = @Partner_Id_VisszafizetesCimzet
   IF @UpdatedColumns.exist('/root/Partner_Nev_VisszafizetCimzett')=1
         SET @Act_Partner_Nev_VisszafizetCimzett = @Partner_Nev_VisszafizetCimzett
   IF @UpdatedColumns.exist('/root/VisszafizetesHatarido')=1
         SET @Act_VisszafizetesHatarido = @VisszafizetesHatarido
   IF @UpdatedColumns.exist('/root/Minosito')=1
         SET @Act_Minosito = @Minosito
   IF @UpdatedColumns.exist('/root/MinositesErvenyessegiIdeje')=1
         SET @Act_MinositesErvenyessegiIdeje = @MinositesErvenyessegiIdeje
   IF @UpdatedColumns.exist('/root/TerjedelemMennyiseg')=1
         SET @Act_TerjedelemMennyiseg = @TerjedelemMennyiseg
   IF @UpdatedColumns.exist('/root/TerjedelemMennyisegiEgyseg')=1
         SET @Act_TerjedelemMennyisegiEgyseg = @TerjedelemMennyisegiEgyseg
   IF @UpdatedColumns.exist('/root/TerjedelemMegjegyzes')=1
         SET @Act_TerjedelemMegjegyzes = @TerjedelemMegjegyzes
   IF @UpdatedColumns.exist('/root/SelejtezesDat')=1
         SET @Act_SelejtezesDat = @SelejtezesDat
   IF @UpdatedColumns.exist('/root/FelhCsoport_Id_Selejtezo')=1
         SET @Act_FelhCsoport_Id_Selejtezo = @FelhCsoport_Id_Selejtezo
   IF @UpdatedColumns.exist('/root/LeveltariAtvevoNeve')=1
         SET @Act_LeveltariAtvevoNeve = @LeveltariAtvevoNeve
   IF @UpdatedColumns.exist('/root/ModositasErvenyessegKezdete')=1
         SET @Act_ModositasErvenyessegKezdete = @ModositasErvenyessegKezdete
   IF @UpdatedColumns.exist('/root/MegszuntetoHatarozat')=1
         SET @Act_MegszuntetoHatarozat = @MegszuntetoHatarozat
   IF @UpdatedColumns.exist('/root/FelulvizsgalatDatuma')=1
         SET @Act_FelulvizsgalatDatuma = @FelulvizsgalatDatuma
   IF @UpdatedColumns.exist('/root/Felulvizsgalo_id')=1
         SET @Act_Felulvizsgalo_id = @Felulvizsgalo_id
   IF @UpdatedColumns.exist('/root/MinositesFelulvizsgalatEredm')=1
         SET @Act_MinositesFelulvizsgalatEredm = @MinositesFelulvizsgalatEredm
   IF @UpdatedColumns.exist('/root/UgyintezesKezdoDatuma')=1
         SET @Act_UgyintezesKezdoDatuma = @UgyintezesKezdoDatuma
   IF @UpdatedColumns.exist('/root/EljarasiSzakasz')=1
         SET @Act_EljarasiSzakasz = @EljarasiSzakasz
   IF @UpdatedColumns.exist('/root/Ugy_Fajtaja')=1
         SET @Act_Ugy_Fajtaja = @Ugy_Fajtaja
   IF @UpdatedColumns.exist('/root/MinositoSzervezet')=1
         SET @Act_MinositoSzervezet = @MinositoSzervezet
   IF @UpdatedColumns.exist('/root/MinositesFelulvizsgalatBizTag')=1
         SET @Act_MinositesFelulvizsgalatBizTag = @MinositesFelulvizsgalatBizTag
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_IraIratok
SET
     PostazasIranya = @Act_PostazasIranya,
     Alszam = @Act_Alszam,
     Sorszam = @Act_Sorszam,
     UtolsoSorszam = @Act_UtolsoSorszam,
     UgyUgyIratDarab_Id = @Act_UgyUgyIratDarab_Id,
     Kategoria = @Act_Kategoria,
     HivatkozasiSzam = @Act_HivatkozasiSzam,
     IktatasDatuma = @Act_IktatasDatuma,
     ExpedialasDatuma = @Act_ExpedialasDatuma,
     ExpedialasModja = @Act_ExpedialasModja,
     FelhasznaloCsoport_Id_Expedial = @Act_FelhasznaloCsoport_Id_Expedial,
     Targy = @Act_Targy,
     Jelleg = @Act_Jelleg,
     SztornirozasDat = @Act_SztornirozasDat,
     FelhasznaloCsoport_Id_Iktato = @Act_FelhasznaloCsoport_Id_Iktato,
     FelhasznaloCsoport_Id_Kiadmany = @Act_FelhasznaloCsoport_Id_Kiadmany,
     UgyintezesAlapja = @Act_UgyintezesAlapja,
     AdathordozoTipusa = @Act_AdathordozoTipusa,
     Csoport_Id_Felelos = @Act_Csoport_Id_Felelos,
     Csoport_Id_Ugyfelelos = @Act_Csoport_Id_Ugyfelelos,
     FelhasznaloCsoport_Id_Orzo = @Act_FelhasznaloCsoport_Id_Orzo,
     KiadmanyozniKell = @Act_KiadmanyozniKell,
     FelhasznaloCsoport_Id_Ugyintez = @Act_FelhasznaloCsoport_Id_Ugyintez,
     Hatarido = @Act_Hatarido,
     MegorzesiIdo = @Act_MegorzesiIdo,
     IratFajta = @Act_IratFajta,
     Irattipus = @Act_Irattipus,
     KuldKuldemenyek_Id = @Act_KuldKuldemenyek_Id,
     Allapot = @Act_Allapot,
     Azonosito = @Act_Azonosito,
     GeneraltTargy = @Act_GeneraltTargy,
     IratMetaDef_Id = @Act_IratMetaDef_Id,
     IrattarbaKuldDatuma  = @Act_IrattarbaKuldDatuma ,
     IrattarbaVetelDat = @Act_IrattarbaVetelDat,
     Ugyirat_Id = @Act_Ugyirat_Id,
     Minosites = @Act_Minosites,
     Elintezett = @Act_Elintezett,
     IratHatasaUgyintezesre = @Act_IratHatasaUgyintezesre,
     FelfuggesztesOka = @Act_FelfuggesztesOka,
     LezarasOka = @Act_LezarasOka,
     Munkaallomas = @Act_Munkaallomas,
     UgyintezesModja = @Act_UgyintezesModja,
     IntezesIdopontja = @Act_IntezesIdopontja,
     IntezesiIdo = @Act_IntezesiIdo,
     IntezesiIdoegyseg = @Act_IntezesiIdoegyseg,
     UzemzavarKezdete = @Act_UzemzavarKezdete,
     UzemzavarVege = @Act_UzemzavarVege,
     UzemzavarIgazoloIratSzama = @Act_UzemzavarIgazoloIratSzama,
     FelfuggesztettNapokSzama = @Act_FelfuggesztettNapokSzama,
     VisszafizetesJogcime = @Act_VisszafizetesJogcime,
     VisszafizetesOsszege = @Act_VisszafizetesOsszege,
     VisszafizetesHatarozatSzama = @Act_VisszafizetesHatarozatSzama,
     Partner_Id_VisszafizetesCimzet = @Act_Partner_Id_VisszafizetesCimzet,
     Partner_Nev_VisszafizetCimzett = @Act_Partner_Nev_VisszafizetCimzett,
     VisszafizetesHatarido = @Act_VisszafizetesHatarido,
     Minosito = @Act_Minosito,
     MinositesErvenyessegiIdeje = @Act_MinositesErvenyessegiIdeje,
     TerjedelemMennyiseg = @Act_TerjedelemMennyiseg,
     TerjedelemMennyisegiEgyseg = @Act_TerjedelemMennyisegiEgyseg,
     TerjedelemMegjegyzes = @Act_TerjedelemMegjegyzes,
     SelejtezesDat = @Act_SelejtezesDat,
     FelhCsoport_Id_Selejtezo = @Act_FelhCsoport_Id_Selejtezo,
     LeveltariAtvevoNeve = @Act_LeveltariAtvevoNeve,
     ModositasErvenyessegKezdete = @Act_ModositasErvenyessegKezdete,
     MegszuntetoHatarozat = @Act_MegszuntetoHatarozat,
     FelulvizsgalatDatuma = @Act_FelulvizsgalatDatuma,
     Felulvizsgalo_id = @Act_Felulvizsgalo_id,
     MinositesFelulvizsgalatEredm = @Act_MinositesFelulvizsgalatEredm,
     UgyintezesKezdoDatuma = @Act_UgyintezesKezdoDatuma,
     EljarasiSzakasz = @Act_EljarasiSzakasz,
     Ugy_Fajtaja = @Act_Ugy_Fajtaja,
     MinositoSzervezet = @Act_MinositoSzervezet,
     MinositesFelulvizsgalatBizTag = @Act_MinositesFelulvizsgalatBizTag,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_IraIratok',@Id
					,'EREC_IraIratokHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

go

/* használt errorcode-ok:
	50401:
	50402: 'Ver' oszlop értéke nem egyezik a meglévő rekordéval
   50403: érvényesség már lejárt a rekordra
	50499: rekord zárolva
*/
