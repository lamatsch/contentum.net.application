/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_IraIratok_VoltElsoInduloSakkora')
            and   type = 'P')
   drop procedure sp_EREC_IraIratok_VoltElsoInduloSakkora
go
*/
CREATE PROCEDURE [dbo].[sp_EREC_IraIratok_VoltElsoInduloSakkora]
@IratId				UNIQUEIDENTIFIER,
@Result				BIT					OUTPUT
         
AS
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @UGYIRAT_ID UNIQUEIDENTIFIER

	SELECT @UGYIRAT_ID = Ugyirat_Id FROM EREC_IraIratok WHERE ID = @IratId

	IF EXISTS (
				SELECT 1 
				FROM [dbo].[EREC_IraIratok] IH
				WHERE IH.Ugyirat_Id = @UGYIRAT_ID
				 AND IH.ErvVege > GETDATE()
				 AND IH.IratHatasaUgyintezesre IS NOT NULL 
				 AND IH.IratHatasaUgyintezesre  NOT LIKE  '0%' --KIVEVE A NINCS HATASSAL
				 AND IH.Id <> @IratId						   --MINDEN MAS IRATRA
		    )
			BEGIN
				SET @Result = CAST(1 AS BIT) 
				SELECT CAST(1 AS BIT) AS IratVoltElsoInduloSakkora
			END
	ELSE
		BEGIN
			SET @Result = CAST(0 AS BIT) 
			SELECT CAST(0 AS BIT) AS IratVoltElsoInduloSakkora
		END

END TRY
BEGIN CATCH
   
   DECLARE @ERRORSEVERITY INT, @ERRORSTATE INT
   DECLARE @ERRORCODE NVARCHAR(1000)    
   SET @ERRORSEVERITY = ERROR_SEVERITY()
   SET @ERRORSTATE = ERROR_STATE()

   IF ERROR_NUMBER()<50000	
      SET @ERRORCODE = '[' + CONVERT(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   ELSE
      SET @ERRORCODE = ERROR_MESSAGE()
      
   IF @ERRORSTATE = 0 
      SET @ERRORSTATE = 1

   RAISERROR(@ERRORCODE,@ERRORSEVERITY,@ERRORSTATE)

END CATCH
