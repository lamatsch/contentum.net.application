
--if exists (select 1
--            from  sysobjects
--           where  id = object_id('sp_EREC_IraIratokGetCsatolmanyJogosultak')
--            and   type = 'P')
--   drop procedure sp_EREC_IraIratokGetCsatolmanyJogosultak
--go



create procedure sp_EREC_IraIratokGetCsatolmanyJogosultak
			@IratId uniqueidentifier,
			@ExecutorUserId uniqueidentifier,
			@Kezi int = 1
			         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @confidential varchar(4000)
	set @confidential = (select top 1 Ertek from KRT_Parameterek where Nev='IRAT_MINOSITES_BIZALMAS'
	and getdate() between ErvKezd and ErvVege)

	DECLARE @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN nvarchar(400)
	set @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN = (select top 1 Ertek from KRT_Parameterek where Nev='JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN'
	and getdate() between ErvKezd and ErvVege)

	IF @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN IS NULL
		SET @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN = '0'

	declare @bizalmas_irat int

	IF (@confidential = 'MIND')
	BEGIN
		set @bizalmas_irat = 1
	END
	ELSE
	BEGIN

		declare @Bizalmas table (val nvarchar(64) collate Hungarian_CS_AS )
		insert into @Bizalmas (val) (select Value from fn_Split(@confidential, ','));
	
		set @bizalmas_irat = (select 1 from EREC_IraIratok
		where Id = @iratId and Minosites in (select val from @Bizalmas))

	END

	if @bizalmas_irat = 1	
	BEGIN
		
		-- BUG#4828: Ügyirat jogosultakhoz felvettek is láthassák az irat csatolmányait
		declare @ugyiratId uniqueidentifier
		set @ugyiratId = (select Ugyirat_Id from erec_IraIratok where Id = @IratId)

		;with jogosultak as
		(
			select KRT_Jogosultak.Csoport_Id_Jogalany as jogosult_id
			from KRT_Jogosultak where KRT_Jogosultak.Obj_Id  = @iratId
			and (@Kezi = 1 or KRT_Jogosultak.Kezi != 'I')
			union all
			-- Ügyirat jogosultak:
			select KRT_Jogosultak.Csoport_Id_Jogalany as jogosult_id
			from KRT_Jogosultak 
			where KRT_Jogosultak.Obj_Id = @ugyiratId and KRT_Jogosultak.Kezi = 'I'
			
			union all
			select EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo as jogosult_id
			from EREC_PldIratPeldanyok where EREC_PldIratPeldanyok.IraIrat_Id= @iratId
			union all
			select EREC_PldIratPeldanyok.Csoport_Id_Felelos as jogosult_id
			from EREC_PldIratPeldanyok where EREC_PldIratPeldanyok.IraIrat_Id= @iratId

			--vezető
			union all
			select vezetok.Csoport_Id_Jogalany as jogosult_id
			from EREC_PldIratPeldanyok
			join EREC_IraIratok on EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id
			join EREC_UgyUgyiratok on EREC_IraIratok.Ugyirat_Id = EREC_UgyUgyiratok.Id
			join KRT_CsoportTagok orzoCsoport on orzoCsoport.Csoport_Id_Jogalany = EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo
			and GETDATE() between orzoCsoport.ErvKezd and orzoCsoport.ErvVege
			join KRT_CsoportTagok vezetok on orzoCsoport.Csoport_Id = vezetok.Csoport_Id
			and vezetok.Tipus = '3'
			and GETDATE() between vezetok.ErvKezd and vezetok.ErvVege
			AND dbo.fn_IsTechnikaiFelhasznalo (EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo) = 0
			AND dbo.fn_IsTechnikaiFelhasznalo (vezetok.Csoport_Id_Jogalany) = 0
			where EREC_PldIratPeldanyok.IraIrat_Id= @iratId
			and (@JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN !='1' or (EREC_IraIratok.Csoport_Id_Ugyfelelos is null and EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos is null))
			union all
			select vezetok.Csoport_Id_Jogalany as jogosult_id
			from EREC_IraIratok
			join EREC_UgyUgyiratok on EREC_IraIratok.Ugyirat_Id = EREC_UgyUgyiratok.Id
			join KRT_CsoportTagok vezetok on ISNULL(EREC_IraIratok.Csoport_Id_Ugyfelelos, EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos)  = vezetok.Csoport_Id
			and vezetok.Tipus = '3'
			and GETDATE() between vezetok.ErvKezd and vezetok.ErvVege
			where EREC_IraIratok.Id= @iratId
			and (@JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN ='1' and (EREC_IraIratok.Csoport_Id_Ugyfelelos is not null or EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos is not null))
			--szervezet asszisztense
			union all
			select asszisztens.Csoport_Id_Jogalany as jogosult_id
			from EREC_PldIratPeldanyok
			join EREC_IraIratok on EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id
			join EREC_UgyUgyiratok on EREC_IraIratok.Ugyirat_Id = EREC_UgyUgyiratok.Id
			join KRT_CsoportTagok orzoCsoport on orzoCsoport.Csoport_Id_Jogalany = EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo
			and GETDATE() between orzoCsoport.ErvKezd and orzoCsoport.ErvVege
			join KRT_CsoportTagok asszisztens on orzoCsoport.Csoport_Id = asszisztens.Csoport_Id
			and asszisztens.Tipus = '4'
			and GETDATE() between asszisztens.ErvKezd and asszisztens.ErvVege
			AND dbo.fn_IsTechnikaiFelhasznalo (EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo) = 0
			AND dbo.fn_IsTechnikaiFelhasznalo (asszisztens.Csoport_Id_Jogalany) = 0
			where EREC_PldIratPeldanyok.IraIrat_Id= @iratId
			and (@JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN !='1' or (EREC_IraIratok.Csoport_Id_Ugyfelelos is null and EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos is null))
			union all
			select asszisztens.Csoport_Id_Jogalany as jogosult_id
			from EREC_IraIratok
			join EREC_UgyUgyiratok on EREC_IraIratok.Ugyirat_Id = EREC_UgyUgyiratok.Id
			join KRT_CsoportTagok asszisztens on ISNULL(EREC_IraIratok.Csoport_Id_Ugyfelelos, EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos)  = asszisztens.Csoport_Id
			and asszisztens.Tipus = '4'
			and GETDATE() between asszisztens.ErvKezd and asszisztens.ErvVege
			where EREC_IraIratok.Id= @iratId
			and (@JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN ='1' and (EREC_IraIratok.Csoport_Id_Ugyfelelos is not null or EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos is not null))
		)

		select * from KRT_Felhasznalok
		where Id in (select jogosult_id from jogosultak)
		and getdate() between ErvKezd and ErvVege
	END
	ELSE
	BEGIN
		declare @jogosultak table(id uniqueidentifier)

		;with iratok as
		(
			select * from EREC_IraIratok
			where Id = @iratId
		),
		ugyiratok as
		(
			select erec_ugyugyiratok.* from erec_ugyugyiratok
			join iratok on erec_ugyugyiratok.id = iratok.ugyirat_id
		),
		peldanyok as
		(
			select EREC_PldIratPeldanyok.* from EREC_PldIratPeldanyok
			join iratok on EREC_PldIratPeldanyok.IraIrat_id = iratok.id
		),
		jogosult_objektumok as
		(
			select Csoport_id_jogalany as jogosult_id from KRT_Jogosultak
			join iratok on KRT_Jogosultak.Obj_Id = iratok.Id
			and (@Kezi = 1 or KRT_Jogosultak.Kezi != 'I')
			UNION ALL
			select Csoport_id_jogalany as jogosult_id from KRT_Jogosultak
			join ugyiratok on KRT_Jogosultak.Obj_Id = ugyiratok.Id
			UNION ALL
			select Csoport_id_jogalany as jogosult_id from KRT_Jogosultak
			join ugyiratok on KRT_Jogosultak.Obj_Id = ugyiratok.IraIktatokonyv_Id
			UNION ALL
			select Csoport_id_jogalany as jogosult_id from KRT_Jogosultak
			join peldanyok on KRT_Jogosultak.Obj_Id = peldanyok.Id
		),
		jogosult_ugyiratok as
		(
			SELECT ugyiratok.FelhasznaloCsoport_Id_Ugyintez as jogosult_id FROM ugyiratok
			UNION ALL
			SELECT ugyiratok.Csoport_Id_Felelos as jogosult_id FROM ugyiratok
			UNION ALL
			SELECT ugyiratok.FelhasznaloCsoport_Id_Orzo as jogosult_id FROM ugyiratok
		),
		jogosult_peldanyok as
		(
			SELECT peldanyok.Csoport_Id_Felelos as jogosult_id FROM peldanyok
			UNION ALL
			SELECT peldanyok.FelhasznaloCsoport_Id_Orzo as jogosult_id FROM peldanyok
		),
		jogosult_iratok as
		(
			SELECT iratok.Csoport_Id_Felelos as jogosult_id FROM iratok
			UNION ALL
			SELECT iratok.FelhasznaloCsoport_Id_Orzo as jogosult_id FROM iratok
		)

		insert into @jogosultak
		select jogosult_id from jogosult_objektumok
		union select jogosult_id from jogosult_ugyiratok
		union select jogosult_id from jogosult_peldanyok
		union select jogosult_id from jogosult_iratok
	
		declare @felhasznalok table(id uniqueidentifier)

		insert into @felhasznalok
		select Id from @jogosultak

		-- szervezet felhasználói
		insert into @felhasznalok
		SELECT KRT_CsoportTagok.Csoport_Id_Jogalany AS Id
		FROM KRT_CsoportTagok
		join @jogosultak jogosultak on KRT_CsoportTagok.Csoport_id = jogosultak.id
		AND GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
		AND KRT_CsoportTagok.Tipus IS NOT NULL

		-- felhasználó szervezetének dolgozói
		insert into @felhasznalok
		SELECT Dolgozok.Csoport_Id_Jogalany AS Id
		FROM KRT_CsoportTagok
		INNER JOIN KRT_Csoportok ON KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id
		INNER JOIN KRT_CsoportTagok AS Dolgozok ON KRT_Csoportok.Id = Dolgozok.Csoport_Id
		join @jogosultak jogosultak on KRT_CsoportTagok.Csoport_Id_Jogalany = jogosultak.id
		join KRT_Csoportok csop on Dolgozok.Csoport_Id_Jogalany = csop.Id
		WHERE GETDATE() BETWEEN KRT_CsoportTagok.ErvKezd AND KRT_CsoportTagok.ErvVege
		AND GETDATE() BETWEEN Dolgozok.ErvKezd AND Dolgozok.ErvVege
		AND ISNULL(KRT_Csoportok.JogosultsagOroklesMod,'0') = '1'
		AND KRT_CsoportTagok.Tipus IS NOT NULL
		AND Dolgozok.Tipus != '1'
		AND dbo.fn_IsTechnikaiFelhasznalo (jogosultak.id) = 0

		select * from KRT_Felhasznalok
		where Id in (select id from @felhasznalok)
		and getdate() between ErvKezd and ErvVege

	END


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
