﻿create procedure [dbo].[sp_EREC_IraIrattariTetelekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IraIrattariTetelek.Id,
	   EREC_IraIrattariTetelek.Org,
	   EREC_IraIrattariTetelek.IrattariTetelszam,
	   EREC_IraIrattariTetelek.Nev,
	   EREC_IraIrattariTetelek.IrattariJel,
	   EREC_IraIrattariTetelek.MegorzesiIdo,
	   EREC_IraIrattariTetelek.Idoegyseg,
	   EREC_IraIrattariTetelek.Folyo_CM,
	   EREC_IraIrattariTetelek.AgazatiJel_Id,
	   EREC_IraIrattariTetelek.Ver,
	   EREC_IraIrattariTetelek.Note,
	   EREC_IraIrattariTetelek.Stat_id,
	   EREC_IraIrattariTetelek.ErvKezd,
	   EREC_IraIrattariTetelek.ErvVege,
	   EREC_IraIrattariTetelek.Letrehozo_id,
	   EREC_IraIrattariTetelek.LetrehozasIdo,
	   EREC_IraIrattariTetelek.Modosito_id,
	   EREC_IraIrattariTetelek.ModositasIdo,
	   EREC_IraIrattariTetelek.Zarolo_id,
	   EREC_IraIrattariTetelek.ZarolasIdo,
	   EREC_IraIrattariTetelek.Tranz_id,
	   EREC_IraIrattariTetelek.UIAccessLog_id,
	   EREC_IraIrattariTetelek.EvenTuliIktatas
	   from 
		 EREC_IraIrattariTetelek as EREC_IraIrattariTetelek 
	   where
		 EREC_IraIrattariTetelek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end