CREATE procedure [dbo].[sp_EREC_IraIrattariTetelekGetAllByIktatoKonyv_IrattariTetel_Id]
  @Iktatokonyv_Id uniqueidentifier,
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraIrattariTetelek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as
begin
BEGIN TRY
   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(4000)
   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
	   EREC_IrattariTetel_Iktatokonyv.IrattariTetel_Id
   FROM 
     EREC_IrattariTetel_Iktatokonyv as EREC_IrattariTetel_Iktatokonyv,
     EREC_IraIrattariTetelek as EREC_IraIrattariTetelek 
	left join EREC_AgazatiJelek as EREC_AgazatiJelek
		ON EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_AgazatiJelek.Id
	left join KRT_KodCsoportok on KRT_KodCsoportok.Kod=''IRATTARI_JEL''
	left join KRT_KodTarak on KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and EREC_IraIrattariTetelek.IrattariJel = KRT_KodTarak.Kod and KRT_KodTarak.Org=''' + CAST(@Org as NVarChar(40)) + '''
		and EREC_IraIrattariTetelek.ErvKezd between KRT_KodTarak.ErvKezd and KRT_KodTarak.ErvVege
left join KRT_KodCsoportok as KRT_KodCsoportok_MegorzesiIdo on KRT_KodCsoportok_MegorzesiIdo.Kod=''SELEJTEZESI_IDO''
left join KRT_KodTarak as KRT_KodTarak_MegorzesiIdo 
			ON KRT_KodCsoportok_MegorzesiIdo.Id = KRT_KodTarak_MegorzesiIdo.KodCsoport_Id and EREC_IraIrattariTetelek.MegorzesiIdo = KRT_KodTarak_MegorzesiIdo.Kod and KRT_KodTarak_MegorzesiIdo.Org=''' + CAST(@Org as NVarChar(40)) + '''
			and EREC_IraIrattariTetelek.ErvKezd between KRT_KodTarak_MegorzesiIdo.ErvKezd and KRT_KodTarak_MegorzesiIdo.ErvVege
   WHERE EREC_IrattariTetel_Iktatokonyv.Iktatokonyv_Id = ''' + CAST(@Iktatokonyv_Id as Nvarchar(40)) + '''
		AND EREC_IrattariTetel_Iktatokonyv.IrattariTetel_Id = EREC_IraIrattariTetelek.Id
           '
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end

