﻿create procedure [dbo].[sp_EREC_IraIrattariTetelekGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraIrattariTetelek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END


  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_IraIrattariTetelek.Id,
	   EREC_IraIrattariTetelek.Org,
	   EREC_IraIrattariTetelek.IrattariTetelszam,
	dbo.fn_MergeIrattariTetelSzam(EREC_AgazatiJelek.Kod,EREC_IraIrattariTetelek.IrattariTetelszam,KRT_KodTarak_MegorzesiIdo.Kod,EREC_IraIrattariTetelek.IrattariJel,''' + cast(@Org as NVarChar(40)) + ''')
			as Merge_IrattariTetelszam,
	   EREC_IraIrattariTetelek.Nev,
	   EREC_IraIrattariTetelek.IrattariJel,
	KRT_KodTarak_IrattariJel.Nev as IRATTARI_JEL,
	   EREC_IraIrattariTetelek.MegorzesiIdo,
	KRT_KodTarak_MegorzesiIdo.Nev as MegorzesiIdo_Nev,
	   EREC_IraIrattariTetelek.Idoegyseg,
	   KRT_KodTarak_Idoegyseg.Nev as Idoegyseg_Nev,
	   EREC_IraIrattariTetelek.Folyo_CM,
	   EREC_IraIrattariTetelek.AgazatiJel_Id,
	EREC_AgazatiJelek.Kod as AgazatiJelek_Kod,
	   EREC_IraIrattariTetelek.Ver,
	   EREC_IraIrattariTetelek.Note,
	   EREC_IraIrattariTetelek.Stat_id,
	   EREC_IraIrattariTetelek.ErvKezd,
	   EREC_IraIrattariTetelek.ErvVege,
	   EREC_IraIrattariTetelek.Letrehozo_id,
	   EREC_IraIrattariTetelek.LetrehozasIdo,
	   EREC_IraIrattariTetelek.Modosito_id,
	   EREC_IraIrattariTetelek.ModositasIdo,
	   EREC_IraIrattariTetelek.Zarolo_id,
	   EREC_IraIrattariTetelek.ZarolasIdo,
	   EREC_IraIrattariTetelek.Tranz_id,
	   EREC_IraIrattariTetelek.UIAccessLog_id,
       (select imd.Id FROM EREC_IratMetaDefinicio imd
              WHERE imd.Ugykor_Id = EREC_IraIrattariTetelek.Id
              AND imd.Ugytipus is NULL and UgytipusNev is NULL AND EljarasiSzakasz is NULL AND Irattipus is NULL
              AND getdate() between imd.ErvKezd AND imd.ErvVege
       ) AS IratMetaDefinicio_Id,
       EREC_IraIrattariTetelek.EvenTuliIktatas
                              
   from 
     EREC_IraIrattariTetelek as EREC_IraIrattariTetelek      
	left join EREC_AgazatiJelek as EREC_AgazatiJelek
		ON EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_AgazatiJelek.Id
	left join KRT_KodCsoportok as KRT_KodCsoportok_IrattariJel on KRT_KodCsoportok_IrattariJel.Kod=''IRATTARI_JEL''
	left join KRT_KodTarak as KRT_KodTarak_IrattariJel 
			ON KRT_KodCsoportok_IrattariJel.Id = KRT_KodTarak_IrattariJel.KodCsoport_Id and EREC_IraIrattariTetelek.IrattariJel = KRT_KodTarak_IrattariJel.Kod and KRT_KodTarak_IrattariJel.Org=''' + CAST(@Org as NVarChar(40)) + '''
			and EREC_IraIrattariTetelek.ErvKezd between KRT_KodTarak_IrattariJel.ErvKezd and KRT_KodTarak_IrattariJel.ErvVege
	left join KRT_KodCsoportok as KRT_KodCsoportok_MegorzesiIdo on KRT_KodCsoportok_MegorzesiIdo.Kod=''SELEJTEZESI_IDO''
	left join KRT_KodTarak as KRT_KodTarak_MegorzesiIdo 
			ON KRT_KodCsoportok_MegorzesiIdo.Id = KRT_KodTarak_MegorzesiIdo.KodCsoport_Id and EREC_IraIrattariTetelek.MegorzesiIdo = KRT_KodTarak_MegorzesiIdo.Kod and KRT_KodTarak_MegorzesiIdo.Org=''' + CAST(@Org as NVarChar(40)) + '''
			-- BUG_6100 Megörzési idő érvényesség 
			and EREC_IraIrattariTetelek.ErvKezd between KRT_KodTarak_MegorzesiIdo.ErvKezd and KRT_KodTarak_MegorzesiIdo.ErvVege
	left join KRT_KodCsoportok as KRT_KodCsoportok_Idoegyseg on KRT_KodCsoportok_Idoegyseg.Kod=''IDOEGYSEG''
	left join KRT_KodTarak as KRT_KodTarak_Idoegyseg 
			ON KRT_KodCsoportok_Idoegyseg.Id = KRT_KodTarak_Idoegyseg.KodCsoport_Id and EREC_IraIrattariTetelek.Idoegyseg = KRT_KodTarak_Idoegyseg.Kod collate Hungarian_CS_AS and KRT_KodTarak_Idoegyseg.Org=''' + CAST(@Org as NVarChar(40)) + '''
--			and EREC_IraIrattariTetelek.ErvKezd between KRT_KodTarak_Idoegyseg.ErvKezd and KRT_KodTarak_Idoegyseg.ErvVege
   Where EREC_IraIrattariTetelek.Org=''' + CAST(@Org as Nvarchar(40)) + ''''
   
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end