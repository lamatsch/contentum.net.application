﻿create procedure [dbo].[sp_EREC_IraKezFeljegyzesekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IraKezFeljegyzesek.Id,
	   EREC_IraKezFeljegyzesek.IraIrat_Id,
	   EREC_IraKezFeljegyzesek.KezelesTipus,
	   EREC_IraKezFeljegyzesek.Leiras,
	   EREC_IraKezFeljegyzesek.Ver,
	   EREC_IraKezFeljegyzesek.Note,
	   EREC_IraKezFeljegyzesek.Stat_id,
	   EREC_IraKezFeljegyzesek.ErvKezd,
	   EREC_IraKezFeljegyzesek.ErvVege,
	   EREC_IraKezFeljegyzesek.Letrehozo_id,
	   EREC_IraKezFeljegyzesek.LetrehozasIdo,
	   EREC_IraKezFeljegyzesek.Modosito_id,
	   EREC_IraKezFeljegyzesek.ModositasIdo,
	   EREC_IraKezFeljegyzesek.Zarolo_id,
	   EREC_IraKezFeljegyzesek.ZarolasIdo,
	   EREC_IraKezFeljegyzesek.Tranz_id,
	   EREC_IraKezFeljegyzesek.UIAccessLog_id
	   from 
		 EREC_IraKezFeljegyzesek as EREC_IraKezFeljegyzesek 
	   where
		 EREC_IraKezFeljegyzesek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end