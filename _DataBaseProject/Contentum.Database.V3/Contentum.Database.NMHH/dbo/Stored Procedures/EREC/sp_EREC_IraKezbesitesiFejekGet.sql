﻿create procedure [dbo].[sp_EREC_IraKezbesitesiFejekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IraKezbesitesiFejek.Id,
	   EREC_IraKezbesitesiFejek.CsomagAzonosito,
	   EREC_IraKezbesitesiFejek.Tipus,
	   EREC_IraKezbesitesiFejek.UtolsoNyomtatasIdo,
	   EREC_IraKezbesitesiFejek.UtolsoNyomtatas_Id,
	   EREC_IraKezbesitesiFejek.Ver,
	   EREC_IraKezbesitesiFejek.Note,
	   EREC_IraKezbesitesiFejek.Stat_id,
	   EREC_IraKezbesitesiFejek.ErvKezd,
	   EREC_IraKezbesitesiFejek.ErvVege,
	   EREC_IraKezbesitesiFejek.Letrehozo_id,
	   EREC_IraKezbesitesiFejek.LetrehozasIdo,
	   EREC_IraKezbesitesiFejek.Modosito_id,
	   EREC_IraKezbesitesiFejek.ModositasIdo,
	   EREC_IraKezbesitesiFejek.Zarolo_id,
	   EREC_IraKezbesitesiFejek.ZarolasIdo,
	   EREC_IraKezbesitesiFejek.Tranz_id,
	   EREC_IraKezbesitesiFejek.UIAccessLog_id
	   from 
		 EREC_IraKezbesitesiFejek as EREC_IraKezbesitesiFejek 
	   where
		 EREC_IraKezbesitesiFejek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end