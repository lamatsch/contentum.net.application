﻿create procedure [dbo].[sp_EREC_IraKezbesitesiFejekGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by EREC_IraKezbesitesiFejek.LetrehozasIdo desc',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_IraKezbesitesiFejek.Id,
	   EREC_IraKezbesitesiFejek.CsomagAzonosito,
	   EREC_IraKezbesitesiFejek.Tipus,
	(CASE WHEN EREC_IraKezbesitesiFejek.Tipus = ''A'' THEN
			(select count(*) from EREC_IraKezbesitesiTetelek as kezbTetelek 		
	  	   		where kezbTetelek.KezbesitesFej_Id = EREC_IraKezbesitesiFejek.Id
					and getdate() between kezbTetelek.ErvKezd and kezbTetelek.ErvVege)
		ELSE (select count(*) from EREC_IraKezbesitesiTetelek as kezbTetelek 		
	  	   	where kezbTetelek.AtveteliFej_Id = EREC_IraKezbesitesiFejek.Id
				and getdate() between kezbTetelek.ErvKezd and kezbTetelek.ErvVege)
	END) as TetelSzam,	
	   EREC_IraKezbesitesiFejek.Ver,
	   EREC_IraKezbesitesiFejek.Note,
	   EREC_IraKezbesitesiFejek.Stat_id,
	   EREC_IraKezbesitesiFejek.ErvKezd,
	   EREC_IraKezbesitesiFejek.ErvVege,
	   EREC_IraKezbesitesiFejek.Letrehozo_id,
	KRT_Csoportok_Letrehozo.Nev as Letrehozo_Nev,
	   EREC_IraKezbesitesiFejek.LetrehozasIdo,
	   EREC_IraKezbesitesiFejek.Modosito_id,
	   EREC_IraKezbesitesiFejek.ModositasIdo,
	   EREC_IraKezbesitesiFejek.Zarolo_id,
	   EREC_IraKezbesitesiFejek.ZarolasIdo,
	   EREC_IraKezbesitesiFejek.Tranz_id,
	   EREC_IraKezbesitesiFejek.UIAccessLog_id  
   from 
     EREC_IraKezbesitesiFejek as EREC_IraKezbesitesiFejek      
	left join KRT_Csoportok as KRT_Csoportok_Letrehozo
		on EREC_IraKezbesitesiFejek.Letrehozo_id = KRT_Csoportok_Letrehozo.Id
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;

	print @sqlcmd;

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end