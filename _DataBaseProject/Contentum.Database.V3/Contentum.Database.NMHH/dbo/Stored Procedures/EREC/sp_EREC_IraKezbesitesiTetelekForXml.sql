﻿CREATE procedure [dbo].[sp_EREC_IraKezbesitesiTetelekForXml]
  @Where			nvarchar(MAX) = '',
  @OrderBy			nvarchar(400) = '',
  @ExecutorUserId	uniqueidentifier,
  @TopRow NVARCHAR(5) = '',
  @AtadasIrany NVARCHAR(10) = '',
  @fejId nvarchar(40)

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   DECLARE @AtadasIranySzoveg nvarchar(20)

   DECLARE @Sub_where nvarchar(200)

   DECLARE @First_Column nvarchar(200)

   if (@AtadasIrany = 'A')
     BEGIN
       SET @AtadasIranySzoveg = 'Cimzett_Nev'
       SET @Sub_where = ' where EREC_IraKezbesitesiTetelek.KezbesitesFej_Id = CAST(''' + @fejId + ''' AS uniqueidentifier) '
       SET @First_Column = ' Fejlec_Tabla.Cimzett_Nev, Fejlec_Tabla.Atado_Nev, '
     END
   else
     BEGIN
       SET @AtadasIranySzoveg = 'Atado_Nev'
       SET @Sub_where = ' where EREC_IraKezbesitesiTetelek.AtveteliFej_Id = CAST(''' + @fejId + ''' AS uniqueidentifier) '
       SET @First_Column = ' Fejlec_Tabla.Atado_Nev, Fejlec_Tabla.Cimzett_Nev, '
	 END

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END


  SET @sqlcmd = 
'select(
select '
+ @First_Column + '
Fejlec_Tabla.Atvevo_Nev,
CONVERT(nvarchar(20), GetDate(), 120) as Date,
(select count(*) from (select 
		EREC_IraKezbesitesiTetelek.Id as Id,
		EREC_IraKezbesitesiTetelek.Obj_type,
		Ev =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_IraIktatoKonyvek_Ugyirat.Ev
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIktatoKonyvek_IratPld.Ev
            WHEN ''EREC_KuldKuldemenyek'' THEN EREC_IraIktatoKonyvek_Kuld.Ev
			ELSE ''0''
		END,
		MegkulJelzes =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_IraIktatoKonyvek_Ugyirat.MegkulJelzes
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIktatoKonyvek_IratPld.MegkulJelzes
            WHEN ''EREC_KuldKuldemenyek'' THEN EREC_IraIktatoKonyvek_Kuld.MegkulJelzes
			ELSE ''0''
		END,
		Foszam =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.Foszam
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_UgyUgyiratdarabok_IratPld.Foszam
            WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.Erkezteto_Szam
			ELSE ''0''
		END,
';
SET @sqlcmd = @sqlcmd + N'
		Alszam =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN ''0''
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIratok_IratPld.Alszam
            WHEN ''EREC_KuldKuldemenyek'' THEN ''0''
			ELSE ''0''
		END,
		Sorszam =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN ''0''
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.Sorszam
            WHEN ''EREC_KuldKuldemenyek'' THEN ''0''
			ELSE ''0''
		END,
		KozpontiIktatasJelzo =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_IraIktatoKonyvek_Ugyirat.KozpontiIktatasJelzo
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIktatoKonyvek_IratPld.KozpontiIktatasJelzo
            WHEN ''EREC_KuldKuldemenyek'' THEN ''0''
			ELSE ''0''
		END,
		Azonosito =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.Azonosito
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.Azonosito
            WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.Azonosito
			ELSE EREC_IraKezbesitesiTetelek.Azonosito_szoveges
		END,
';
SET @sqlcmd = @sqlcmd + N'
	Kuldo =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN dbo.fn_GetCsoportNev(ISNULL(EREC_UgyUgyiratok_Ugyirat.Csoport_Id_Ugyfelelos,EREC_IraKezbesitesiTetelek.Csoport_Id_Cel))
					WHEN ''1'' THEN EREC_UgyUgyiratok_Ugyirat.NevSTR_Ugyindito
					ELSE ''''
				END
			WHEN ''EREC_PldIratPeldanyok'' THEN
				CASE EREC_IraIratok_IratPld.PostazasIranya
					WHEN ''0'' THEN dbo.fn_GetCsoportNev(ISNULL(EREC_UgyUgyiratok_IratPld.Csoport_Id_Ugyfelelos,EREC_IraKezbesitesiTetelek.Csoport_Id_Cel))
					WHEN ''1'' THEN EREC_KuldKuldemenyek_IratPld.NevSTR_Bekuldo
					ELSE ''''
				END
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.NevSTR_Bekuldo                 
			ELSE ''''
		END,
	Kuldo_cim =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN ''''
					WHEN ''1'' THEN EREC_UgyUgyiratok_Ugyirat.CimSTR_Ugyindito
					ELSE ''''
				END
			WHEN ''EREC_PldIratPeldanyok'' THEN
				CASE EREC_IraIratok_IratPld.PostazasIranya
					WHEN ''0'' THEN ''''
					WHEN ''1'' THEN EREC_KuldKuldemenyek_IratPld.CimSTR_Bekuldo
					ELSE ''''
				END
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.CimSTR_Bekuldo                 
			ELSE ''''
		END,
';
SET @sqlcmd = @sqlcmd + N'
	Targy =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.Targy
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIratok_IratPld.Targy
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.Targy            
		ELSE ''''
	END,
	Barkod =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.BARCODE
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.BarCode
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.BarCode                
			ELSE isnull(EREC_UgyUgyiratok_Ugyirat.barcode,isnull(EREC_PldIratPeldanyok_IratPld.barcode,EREC_KuldKuldemenyek_Kuld.barcode))
		END,
	CimzettNev =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN EREC_UgyUgyiratok_Ugyirat.NevSTR_Ugyindito
					WHEN ''1'' THEN dbo.fn_GetCsoportNev(ISNULL(EREC_UgyUgyiratok_Ugyirat.Csoport_Id_Ugyfelelos,EREC_IraKezbesitesiTetelek.Csoport_Id_Cel))
				ELSE ''''
			END
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.NevSTR_Cimzett
			WHEN ''EREC_KuldKuldemenyek'' THEN KRT_CsoportokCsoport_Id_Cimzett_Kuld.Nev
			ELSE ''''
		END,
	CimzettCim =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN EREC_UgyUgyiratok_Ugyirat.CimSTR_Ugyindito
					WHEN ''1'' THEN ''''
					ELSE ''''
				END
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.CimSTR_Cimzett
			WHEN ''EREC_KuldKuldemenyek'' THEN ''''
			ELSE ''''
		END,
';
SET @sqlcmd = @sqlcmd + N'
	Ugyintezo =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN dbo.fn_GetCsoportNev(EREC_UgyUgyiratok_Ugyirat.FelhasznaloCsoport_Id_Ugyintez)
			WHEN ''EREC_PldIratPeldanyok'' THEN ISNULL(dbo.fn_GetCsoportNev(EREC_IraIratok_IratPld.FelhasznaloCsoport_Id_Ugyintez),dbo.fn_GetCsoportNev(EREC_UgyUgyiratok_IratPld.FelhasznaloCsoport_Id_Ugyintez))
			WHEN ''EREC_KuldKuldemenyek'' THEN ''''
			ELSE ''''
		END,
';
SET @sqlcmd = @sqlcmd + N'
KRT_Csoportok_Cel.Nev as Cimzett_Nev, 
KRT_Csoportok_Atado.Nev as Atado_Nev,
KRT_Csoportok_Atvevo.Nev as Atvevo_Nev
from EREC_IraKezbesitesiTetelek as EREC_IraKezbesitesiTetelek 
	left join KRT_Csoportok as KRT_Csoportok_Cel
		ON EREC_IraKezbesitesiTetelek.Csoport_Id_Cel = KRT_Csoportok_Cel.Id 
	left join KRT_Csoportok as KRT_Csoportok_Atado
		ON EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER = KRT_Csoportok_Atado.Id
	left join KRT_Csoportok as KRT_Csoportok_Atvevo
		ON EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoUser = KRT_Csoportok_Atvevo.Id    
	LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok_Ugyirat
		ON EREC_IraKezbesitesiTetelek.Obj_Id = EREC_UgyUgyiratok_Ugyirat.Id
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Ugyirat
		ON EREC_UgyUgyiratok_Ugyirat.IraIktatokonyv_Id = EREC_IraIktatoKonyvek_Ugyirat.Id     
	LEFT JOIN EREC_PldIratPeldanyok as EREC_PldIratPeldanyok_IratPld
		ON EREC_IraKezbesitesiTetelek.Obj_Id = EREC_PldIratPeldanyok_IratPld.Id
	LEFT JOIN EREC_IraIratok as EREC_IraIratok_IratPld
		ON EREC_PldIratPeldanyok_IratPld.IraIrat_Id = EREC_IraIratok_IratPld.Id
	LEFT JOIN EREC_KuldKuldemenyek AS EREC_KuldKuldemenyek_IratPld
		ON EREC_IraIratok_IratPld.KuldKuldemenyek_Id = EREC_KuldKuldemenyek_IratPld.Id
	LEFT JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok_IratPld
		ON EREC_IraIratok_IratPld.UgyUgyIratDarab_Id = EREC_UgyUgyiratdarabok_IratPld.Id
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_IratPld
		ON EREC_UgyUgyiratdarabok_IratPld.IraIktatokonyv_Id = EREC_IraIktatoKonyvek_IratPld.Id
	LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok_IratPld
		ON EREC_UgyUgyiratdarabok_IratPld.UgyUgyirat_Id = EREC_UgyUgyiratok_IratPld.Id
	LEFT JOIN EREC_KuldKuldemenyek as EREC_KuldKuldemenyek_Kuld
		ON EREC_IraKezbesitesiTetelek.Obj_Id = EREC_KuldKuldemenyek_Kuld.Id
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Kuld
		ON EREC_KuldKuldemenyek_Kuld.IraIktatokonyv_Id = EREC_IraIktatoKonyvek_Kuld.Id
	left join KRT_Csoportok as KRT_CsoportokCsoport_Id_Cimzett_Kuld
		on KRT_CsoportokCsoport_Id_Cimzett_Kuld.Id = EREC_KuldKuldemenyek_Kuld.Csoport_Id_Cimzett    
	LEFT JOIN EREC_UgyUgyiratdarabok AS EREC_UgyUgyiratdarabok_PostazasIranya
		ON EREC_UgyUgyiratdarabok_PostazasIranya.UgyUgyirat_Id = EREC_UgyUgyiratok_Ugyirat.Id
	LEFT JOIN EREC_IraIratok AS EREC_IraIratok_PostazasIranya
		ON EREC_IraIratok_PostazasIranya.UgyUgyiratDarab_Id = EREC_UgyUgyiratdarabok_PostazasIranya.Id AND EREC_IraIratok_PostazasIranya.Alszam = 1'
 + @Sub_where + ') as Count_Tabla where Count_Tabla.' + @AtadasIranySzoveg + ' = Fejlec_Tabla.' + @AtadasIranySzoveg + ') as szam,
Adat_Tabla.Cimzett_Nev, 
';
SET @sqlcmd = @sqlcmd + N'
Adat_Tabla.Atado_Nev,
Adat_Tabla.Atvevo_Nev,
Adat_Tabla.Azonosito,
Adat_Tabla.Kuldo,
Adat_Tabla.Kuldo_Cim,
Adat_Tabla.Targy,
Adat_Tabla.Barkod,
Adat_Tabla.CimzettNev,
Adat_Tabla.CimzettCim,
Adat_Tabla.Ugyintezo
from (select 
		EREC_IraKezbesitesiTetelek.Id as Id,
		EREC_IraKezbesitesiTetelek.Obj_type,
';
SET @sqlcmd = @sqlcmd + N'
		Ev =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_IraIktatoKonyvek_Ugyirat.Ev
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIktatoKonyvek_IratPld.Ev
            WHEN ''EREC_KuldKuldemenyek'' THEN EREC_IraIktatoKonyvek_Kuld.Ev
			ELSE ''0''
		END,
		MegkulJelzes =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_IraIktatoKonyvek_Ugyirat.MegkulJelzes
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIktatoKonyvek_IratPld.MegkulJelzes
            WHEN ''EREC_KuldKuldemenyek'' THEN EREC_IraIktatoKonyvek_Kuld.MegkulJelzes
			ELSE ''0''
		END,
		Foszam =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.Foszam
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_UgyUgyiratdarabok_IratPld.Foszam
            WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.Erkezteto_Szam
			ELSE ''0''
		END,
		Alszam =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN ''0''
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIratok_IratPld.Alszam
            WHEN ''EREC_KuldKuldemenyek'' THEN ''0''
			ELSE ''0''
		END,
		Sorszam =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN ''0''
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.Sorszam
            WHEN ''EREC_KuldKuldemenyek'' THEN ''0''
			ELSE ''0''
		END,
		KozpontiIktatasJelzo =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_IraIktatoKonyvek_Ugyirat.KozpontiIktatasJelzo
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIktatoKonyvek_IratPld.KozpontiIktatasJelzo
            WHEN ''EREC_KuldKuldemenyek'' THEN ''0''
			ELSE ''0''
		END,
';
SET @sqlcmd = @sqlcmd + N'
		Azonosito =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.Azonosito
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.Azonosito
            WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.Azonosito
			ELSE EREC_IraKezbesitesiTetelek.Azonosito_szoveges
		END,
	Kuldo =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN dbo.fn_GetCsoportNev(ISNULL(EREC_UgyUgyiratok_Ugyirat.Csoport_Id_Ugyfelelos,EREC_IraKezbesitesiTetelek.Csoport_Id_Cel))
					WHEN ''1'' THEN EREC_UgyUgyiratok_Ugyirat.NevSTR_Ugyindito
					ELSE ''''
				END
			WHEN ''EREC_PldIratPeldanyok'' THEN
				CASE EREC_IraIratok_IratPld.PostazasIranya
					WHEN ''0'' THEN dbo.fn_GetCsoportNev(ISNULL(EREC_UgyUgyiratok_IratPld.Csoport_Id_Ugyfelelos,EREC_IraKezbesitesiTetelek.Csoport_Id_Cel))
					WHEN ''1'' THEN EREC_KuldKuldemenyek_IratPld.NevSTR_Bekuldo
					ELSE ''''
				END
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.NevSTR_Bekuldo                 
			ELSE ''''
		END,
';
SET @sqlcmd = @sqlcmd + N'
	Kuldo_cim =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN ''''
					WHEN ''1'' THEN EREC_UgyUgyiratok_Ugyirat.CimSTR_Ugyindito
					ELSE ''''
				END
			WHEN ''EREC_PldIratPeldanyok'' THEN
				CASE EREC_IraIratok_IratPld.PostazasIranya
					WHEN ''0'' THEN ''''
					WHEN ''1'' THEN EREC_KuldKuldemenyek_IratPld.CimSTR_Bekuldo
					ELSE ''''
				END
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.CimSTR_Bekuldo                 
			ELSE ''''
		END,
	Targy =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.Targy
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIratok_IratPld.Targy
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.Targy            
		ELSE ''''
	END,
	Barkod =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.BARCODE
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.BarCode
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.BarCode                
			ELSE isnull(EREC_UgyUgyiratok_Ugyirat.barcode,isnull(EREC_PldIratPeldanyok_IratPld.barcode,EREC_KuldKuldemenyek_Kuld.barcode))
		END,
';
SET @sqlcmd = @sqlcmd + N'
	CimzettNev =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN EREC_UgyUgyiratok_Ugyirat.NevSTR_Ugyindito
					WHEN ''1'' THEN dbo.fn_GetCsoportNev(ISNULL(EREC_UgyUgyiratok_Ugyirat.Csoport_Id_Ugyfelelos,EREC_IraKezbesitesiTetelek.Csoport_Id_Cel))
				ELSE ''''
			END
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.NevSTR_Cimzett
			WHEN ''EREC_KuldKuldemenyek'' THEN KRT_CsoportokCsoport_Id_Cimzett_Kuld.Nev
			ELSE ''''
		END,
	CimzettCim =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN EREC_UgyUgyiratok_Ugyirat.CimSTR_Ugyindito
					WHEN ''1'' THEN ''''
					ELSE ''''
				END
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.CimSTR_Cimzett
			WHEN ''EREC_KuldKuldemenyek'' THEN ''''
			ELSE ''''
		END,
	Ugyintezo =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN dbo.fn_GetCsoportNev(EREC_UgyUgyiratok_Ugyirat.FelhasznaloCsoport_Id_Ugyintez)
			WHEN ''EREC_PldIratPeldanyok'' THEN ISNULL(dbo.fn_GetCsoportNev(EREC_IraIratok_IratPld.FelhasznaloCsoport_Id_Ugyintez),dbo.fn_GetCsoportNev(EREC_UgyUgyiratok_IratPld.FelhasznaloCsoport_Id_Ugyintez))
			WHEN ''EREC_KuldKuldemenyek'' THEN ''''
			ELSE ''''
		END,
';
SET @sqlcmd = @sqlcmd + N'
KRT_Csoportok_Cel.Nev as Cimzett_Nev, 
KRT_Csoportok_Atado.Nev as Atado_Nev,
KRT_Csoportok_Atvevo.Nev as Atvevo_Nev
from EREC_IraKezbesitesiTetelek as EREC_IraKezbesitesiTetelek 
	left join KRT_Csoportok as KRT_Csoportok_Cel
		ON EREC_IraKezbesitesiTetelek.Csoport_Id_Cel = KRT_Csoportok_Cel.Id 
	left join KRT_Csoportok as KRT_Csoportok_Atado
		ON EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER = KRT_Csoportok_Atado.Id
	left join KRT_Csoportok as KRT_Csoportok_Atvevo
		ON EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoUser = KRT_Csoportok_Atvevo.Id    
	LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok_Ugyirat
		ON EREC_IraKezbesitesiTetelek.Obj_Id = EREC_UgyUgyiratok_Ugyirat.Id
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Ugyirat
		ON EREC_UgyUgyiratok_Ugyirat.IraIktatokonyv_Id = EREC_IraIktatoKonyvek_Ugyirat.Id     
	LEFT JOIN EREC_PldIratPeldanyok as EREC_PldIratPeldanyok_IratPld
		ON EREC_IraKezbesitesiTetelek.Obj_Id = EREC_PldIratPeldanyok_IratPld.Id
	LEFT JOIN EREC_IraIratok as EREC_IraIratok_IratPld
		ON EREC_PldIratPeldanyok_IratPld.IraIrat_Id = EREC_IraIratok_IratPld.Id
	LEFT JOIN EREC_KuldKuldemenyek AS EREC_KuldKuldemenyek_IratPld
		ON EREC_IraIratok_IratPld.KuldKuldemenyek_Id = EREC_KuldKuldemenyek_IratPld.Id
	LEFT JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok_IratPld
		ON EREC_IraIratok_IratPld.UgyUgyIratDarab_Id = EREC_UgyUgyiratdarabok_IratPld.Id
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_IratPld
		ON EREC_UgyUgyiratdarabok_IratPld.IraIktatokonyv_Id = EREC_IraIktatoKonyvek_IratPld.Id
	LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok_IratPld
		ON EREC_UgyUgyiratdarabok_IratPld.UgyUgyirat_Id = EREC_UgyUgyiratok_IratPld.Id
	LEFT JOIN EREC_KuldKuldemenyek as EREC_KuldKuldemenyek_Kuld
		ON EREC_IraKezbesitesiTetelek.Obj_Id = EREC_KuldKuldemenyek_Kuld.Id
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Kuld
		ON EREC_KuldKuldemenyek_Kuld.IraIktatokonyv_Id = EREC_IraIktatoKonyvek_Kuld.Id
	left join KRT_Csoportok as KRT_CsoportokCsoport_Id_Cimzett_Kuld
		on KRT_CsoportokCsoport_Id_Cimzett_Kuld.Id = EREC_KuldKuldemenyek_Kuld.Csoport_Id_Cimzett    
	LEFT JOIN EREC_UgyUgyiratdarabok AS EREC_UgyUgyiratdarabok_PostazasIranya
		ON EREC_UgyUgyiratdarabok_PostazasIranya.UgyUgyirat_Id = EREC_UgyUgyiratok_Ugyirat.Id
	LEFT JOIN EREC_IraIratok AS EREC_IraIratok_PostazasIranya
		ON EREC_IraIratok_PostazasIranya.UgyUgyiratDarab_Id = EREC_UgyUgyiratdarabok_PostazasIranya.Id AND EREC_IraIratok_PostazasIranya.Alszam = 1'
 + @Sub_where + ') as Fejlec_Tabla
left join (select 
	EREC_IraKezbesitesiTetelek.Id as Id,
	EREC_IraKezbesitesiTetelek.Obj_type,
';
SET @sqlcmd = @sqlcmd + N'	
	Ev =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_IraIktatoKonyvek_Ugyirat.Ev
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIktatoKonyvek_IratPld.Ev
            WHEN ''EREC_KuldKuldemenyek'' THEN EREC_IraIktatoKonyvek_Kuld.Ev
			ELSE ''0''
		END,
		MegkulJelzes =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_IraIktatoKonyvek_Ugyirat.MegkulJelzes
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIktatoKonyvek_IratPld.MegkulJelzes
            WHEN ''EREC_KuldKuldemenyek'' THEN EREC_IraIktatoKonyvek_Kuld.MegkulJelzes
			ELSE ''0''
		END,
		Foszam =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.Foszam
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_UgyUgyiratdarabok_IratPld.Foszam
            WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.Erkezteto_Szam
			ELSE ''0''
		END,
		Alszam =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN ''0''
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIratok_IratPld.Alszam
            WHEN ''EREC_KuldKuldemenyek'' THEN ''0''
			ELSE ''0''
		END,
';
SET @sqlcmd = @sqlcmd + N'
		Sorszam =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN ''0''
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.Sorszam
            WHEN ''EREC_KuldKuldemenyek'' THEN ''0''
			ELSE ''0''
		END,
		KozpontiIktatasJelzo =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_IraIktatoKonyvek_Ugyirat.KozpontiIktatasJelzo
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIktatoKonyvek_IratPld.KozpontiIktatasJelzo
            WHEN ''EREC_KuldKuldemenyek'' THEN ''0''
			ELSE ''0''
		END,
	Azonosito =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.Azonosito
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.Azonosito
            WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.Azonosito
			ELSE EREC_IraKezbesitesiTetelek.Azonosito_szoveges
		END,
	Kuldo =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN dbo.fn_GetCsoportNev(ISNULL(EREC_UgyUgyiratok_Ugyirat.Csoport_Id_Ugyfelelos,EREC_IraKezbesitesiTetelek.Csoport_Id_Cel))
					WHEN ''1'' THEN EREC_UgyUgyiratok_Ugyirat.NevSTR_Ugyindito
					ELSE ''''
				END
			WHEN ''EREC_PldIratPeldanyok'' THEN
				CASE EREC_IraIratok_IratPld.PostazasIranya
					WHEN ''0'' THEN dbo.fn_GetCsoportNev(ISNULL(EREC_UgyUgyiratok_IratPld.Csoport_Id_Ugyfelelos,EREC_IraKezbesitesiTetelek.Csoport_Id_Cel))
					WHEN ''1'' THEN EREC_KuldKuldemenyek_IratPld.NevSTR_Bekuldo
					ELSE ''''
				END
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.NevSTR_Bekuldo                 
			ELSE ''''
		END,
	Kuldo_cim =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN ''''
					WHEN ''1'' THEN EREC_UgyUgyiratok_Ugyirat.CimSTR_Ugyindito
					ELSE ''''
				END
			WHEN ''EREC_PldIratPeldanyok'' THEN
				CASE EREC_IraIratok_IratPld.PostazasIranya
					WHEN ''0'' THEN ''''
					WHEN ''1'' THEN EREC_KuldKuldemenyek_IratPld.CimSTR_Bekuldo
					ELSE ''''
				END
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.CimSTR_Bekuldo                 
			ELSE ''''
		END,
';
SET @sqlcmd = @sqlcmd + N'
	Targy =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.Targy
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIratok_IratPld.Targy
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.Targy            
		ELSE ''''
	END,
';
SET @sqlcmd = @sqlcmd + N'
	Barkod =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.BARCODE
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.BarCode
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.BarCode                
			ELSE isnull(EREC_UgyUgyiratok_Ugyirat.barcode,isnull(EREC_PldIratPeldanyok_IratPld.barcode,EREC_KuldKuldemenyek_Kuld.barcode))
		END,
	CimzettNev =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN EREC_UgyUgyiratok_Ugyirat.NevSTR_Ugyindito
					WHEN ''1'' THEN dbo.fn_GetCsoportNev(ISNULL(EREC_UgyUgyiratok_Ugyirat.Csoport_Id_Ugyfelelos,EREC_IraKezbesitesiTetelek.Csoport_Id_Cel))
				ELSE ''''
			END
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.NevSTR_Cimzett
			WHEN ''EREC_KuldKuldemenyek'' THEN KRT_CsoportokCsoport_Id_Cimzett_Kuld.Nev
			ELSE ''''
		END,
	CimzettCim =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN EREC_UgyUgyiratok_Ugyirat.CimSTR_Ugyindito
					WHEN ''1'' THEN ''''
					ELSE ''''
				END
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.CimSTR_Cimzett
			WHEN ''EREC_KuldKuldemenyek'' THEN ''''
			ELSE ''''
		END,
	Ugyintezo =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN dbo.fn_GetCsoportNev(EREC_UgyUgyiratok_Ugyirat.FelhasznaloCsoport_Id_Ugyintez)
			WHEN ''EREC_PldIratPeldanyok'' THEN ISNULL(dbo.fn_GetCsoportNev(EREC_IraIratok_IratPld.FelhasznaloCsoport_Id_Ugyintez),dbo.fn_GetCsoportNev(EREC_UgyUgyiratok_IratPld.FelhasznaloCsoport_Id_Ugyintez))
			WHEN ''EREC_KuldKuldemenyek'' THEN ''''
			ELSE ''''
		END,
';
SET @sqlcmd = @sqlcmd + N'
KRT_Csoportok_Cel.Nev as Cimzett_Nev, 
KRT_Csoportok_Atado.Nev as Atado_Nev,
KRT_Csoportok_Atvevo.Nev as Atvevo_Nev
from EREC_IraKezbesitesiTetelek as EREC_IraKezbesitesiTetelek 
	left join KRT_Csoportok as KRT_Csoportok_Cel
		ON EREC_IraKezbesitesiTetelek.Csoport_Id_Cel = KRT_Csoportok_Cel.Id 
	left join KRT_Csoportok as KRT_Csoportok_Atado
		ON EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER = KRT_Csoportok_Atado.Id
	left join KRT_Csoportok as KRT_Csoportok_Atvevo
		ON EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoUser = KRT_Csoportok_Atvevo.Id    
	LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok_Ugyirat
		ON EREC_IraKezbesitesiTetelek.Obj_Id = EREC_UgyUgyiratok_Ugyirat.Id
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Ugyirat
		ON EREC_UgyUgyiratok_Ugyirat.IraIktatokonyv_Id = EREC_IraIktatoKonyvek_Ugyirat.Id     
	LEFT JOIN EREC_PldIratPeldanyok as EREC_PldIratPeldanyok_IratPld
		ON EREC_IraKezbesitesiTetelek.Obj_Id = EREC_PldIratPeldanyok_IratPld.Id
	LEFT JOIN EREC_IraIratok as EREC_IraIratok_IratPld
		ON EREC_PldIratPeldanyok_IratPld.IraIrat_Id = EREC_IraIratok_IratPld.Id
	LEFT JOIN EREC_KuldKuldemenyek AS EREC_KuldKuldemenyek_IratPld
		ON EREC_IraIratok_IratPld.KuldKuldemenyek_Id = EREC_KuldKuldemenyek_IratPld.Id
	LEFT JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok_IratPld
		ON EREC_IraIratok_IratPld.UgyUgyIratDarab_Id = EREC_UgyUgyiratdarabok_IratPld.Id
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_IratPld
		ON EREC_UgyUgyiratdarabok_IratPld.IraIktatokonyv_Id = EREC_IraIktatoKonyvek_IratPld.Id
	LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok_IratPld
		ON EREC_UgyUgyiratdarabok_IratPld.UgyUgyirat_Id = EREC_UgyUgyiratok_IratPld.Id
	LEFT JOIN EREC_KuldKuldemenyek as EREC_KuldKuldemenyek_Kuld
		ON EREC_IraKezbesitesiTetelek.Obj_Id = EREC_KuldKuldemenyek_Kuld.Id
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Kuld
		ON EREC_KuldKuldemenyek_Kuld.IraIktatokonyv_Id = EREC_IraIktatoKonyvek_Kuld.Id
	left join KRT_Csoportok as KRT_CsoportokCsoport_Id_Cimzett_Kuld
		on KRT_CsoportokCsoport_Id_Cimzett_Kuld.Id = EREC_KuldKuldemenyek_Kuld.Csoport_Id_Cimzett    
	LEFT JOIN EREC_UgyUgyiratdarabok AS EREC_UgyUgyiratdarabok_PostazasIranya
		ON EREC_UgyUgyiratdarabok_PostazasIranya.UgyUgyirat_Id = EREC_UgyUgyiratok_Ugyirat.Id
	LEFT JOIN EREC_IraIratok AS EREC_IraIratok_PostazasIranya
		ON EREC_IraIratok_PostazasIranya.UgyUgyiratDarab_Id = EREC_UgyUgyiratdarabok_PostazasIranya.Id AND EREC_IraIratok_PostazasIranya.Alszam = 1'
 + @Sub_where + ') as Adat_Tabla 
on Fejlec_Tabla.Id = Adat_Tabla.Id
'

--if @Where is not null and @Where!=''
--	begin 
--		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
--	end
	SET @sqlcmd = @sqlcmd + 'Order by Fejlec_tabla.' + @AtadasIranySzoveg + ', Fejlec_tabla.Obj_type, Fejlec_tabla.Ev, Fejlec_tabla.MegkulJelzes, Fejlec_tabla.Foszam, Fejlec_tabla.Alszam, Fejlec_tabla.KozpontiIktatasJelzo, Fejlec_tabla.Sorszam

FOR XML AUTO, ELEMENTS, ROOT(''NewDataSet'')) as string_xml
'

exec(@sqlcmd)


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end