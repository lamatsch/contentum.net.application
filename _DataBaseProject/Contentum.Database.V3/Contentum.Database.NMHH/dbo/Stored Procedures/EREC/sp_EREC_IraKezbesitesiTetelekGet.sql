﻿create procedure [dbo].[sp_EREC_IraKezbesitesiTetelekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IraKezbesitesiTetelek.Id,
	   EREC_IraKezbesitesiTetelek.KezbesitesFej_Id,
	   EREC_IraKezbesitesiTetelek.AtveteliFej_Id,
	   EREC_IraKezbesitesiTetelek.Obj_Id,
	   EREC_IraKezbesitesiTetelek.ObjTip_Id,
	   EREC_IraKezbesitesiTetelek.Obj_type,
	   EREC_IraKezbesitesiTetelek.AtadasDat,
	   EREC_IraKezbesitesiTetelek.AtvetelDat,
	   EREC_IraKezbesitesiTetelek.Megjegyzes,
	   EREC_IraKezbesitesiTetelek.SztornirozasDat,
	   EREC_IraKezbesitesiTetelek.UtolsoNyomtatas_Id,
	   EREC_IraKezbesitesiTetelek.UtolsoNyomtatasIdo,
	   EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER,
	   EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_LOGIN,
	   EREC_IraKezbesitesiTetelek.Csoport_Id_Cel,
	   EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoUser,
	   EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoLogin,
	   EREC_IraKezbesitesiTetelek.Allapot,
	   EREC_IraKezbesitesiTetelek.Azonosito_szoveges,
	   EREC_IraKezbesitesiTetelek.UgyintezesModja,
	   EREC_IraKezbesitesiTetelek.Ver,
	   EREC_IraKezbesitesiTetelek.Note,
	   EREC_IraKezbesitesiTetelek.Stat_id,
	   EREC_IraKezbesitesiTetelek.ErvKezd,
	   EREC_IraKezbesitesiTetelek.ErvVege,
	   EREC_IraKezbesitesiTetelek.Letrehozo_id,
	   EREC_IraKezbesitesiTetelek.LetrehozasIdo,
	   EREC_IraKezbesitesiTetelek.Modosito_id,
	   EREC_IraKezbesitesiTetelek.ModositasIdo,
	   EREC_IraKezbesitesiTetelek.Zarolo_id,
	   EREC_IraKezbesitesiTetelek.ZarolasIdo,
	   EREC_IraKezbesitesiTetelek.Tranz_id,
	   EREC_IraKezbesitesiTetelek.UIAccessLog_id
	   from 
		 EREC_IraKezbesitesiTetelek as EREC_IraKezbesitesiTetelek 
	   where
		 EREC_IraKezbesitesiTetelek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end