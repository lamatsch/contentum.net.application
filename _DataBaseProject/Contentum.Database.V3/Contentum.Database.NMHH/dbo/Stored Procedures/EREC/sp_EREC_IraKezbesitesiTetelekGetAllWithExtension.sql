﻿create procedure [dbo].[sp_EREC_IraKezbesitesiTetelekGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IraKezbesitesiTetelek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				UNIQUEIDENTIFIER,
  @FelhasznaloSzervezet_Id UNIQUEIDENTIFIER,
  @Jogosultak		char(1) = '0'

as

begin

BEGIN TRY

   set nocount on
   
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(MAX)
   set @sqlcmd = '';

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
    declare @isAdminInSzervezet int
	set @isAdminInSzervezet = dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id)
     
--	/************************************************************
--	* Bizalmas iratok orzo szerinti szuréséhez csoporttagok lekérése*
--	************************************************************/
--	--IF dbo.fn_IsAdmin(@ExecutorUserId) = 0
--	IF @isAdminInSzervezet = 0 AND @Jogosultak = '1'
--	begin
----	set @sqlcmd = N'declare @iratok_executor table (Id uniqueidentifier)
----insert into @iratok_executor select Id from dbo.fn_GetAllIrat_ExecutorIsOrzoOrHisLeader(
----			''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
----			''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
----'
--	set @sqlcmd = @sqlcmd + N'; declare @iratok_executor table (Id uniqueidentifier primary key)
--insert into @iratok_executor select Id from dbo.fn_GetAllConfidentialIrat_ExecutorIsOrzoOrHisLeader(
--			''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
--			''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
--'
--	end
--               
	/************************************************************
	* Tényleges select											*
	************************************************************/
  set @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_IraKezbesitesiTetelek.Id,
    EREC_IraKezbesitesiTetelek.Obj_Id,
    EREC_IraKezbesitesiTetelek.ObjTip_Id,
    EREC_IraKezbesitesiTetelek.Obj_type,
    Azonosito =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.Azonosito
            WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.Azonosito
            WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.Azonosito
            WHEN ''KRT_Mappak'' THEN KRT_Mappak.Nev
			ELSE EREC_IraKezbesitesiTetelek.Azonosito_szoveges
		END,
	Kuldo =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN dbo.fn_GetCsoportNev(EREC_UgyUgyiratok_Ugyirat.Csoport_Id_Felelos)
					WHEN ''1'' THEN EREC_UgyUgyiratok_Ugyirat.NevSTR_Ugyindito
					ELSE ''''
				END
			WHEN ''EREC_PldIratPeldanyok'' THEN
				CASE EREC_IraIratok_IratPld.PostazasIranya
					WHEN ''0'' THEN dbo.fn_GetCsoportNev(EREC_UgyUgyiratok_IratPld.Csoport_Id_Felelos)
					WHEN ''1'' THEN EREC_KuldKuldemenyek_IratPld.NevSTR_Bekuldo
					ELSE ''''
				END
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.NevSTR_Bekuldo                 
			ELSE ''''
		END,
	Kuldo_cim =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN ''''
					WHEN ''1'' THEN EREC_UgyUgyiratok_Ugyirat.CimSTR_Ugyindito
					ELSE ''''
				END
			WHEN ''EREC_PldIratPeldanyok'' THEN
				CASE EREC_IraIratok_IratPld.PostazasIranya
					WHEN ''0'' THEN ''''
					WHEN ''1'' THEN EREC_KuldKuldemenyek_IratPld.CimSTR_Bekuldo
					ELSE ''''
				END
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.CimSTR_Bekuldo                 
			ELSE ''''
		END,
	Targy =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.Targy
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_IraIratok_IratPld.Targy
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.Targy            
		ELSE ''''
	END,
	Barkod =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.BARCODE
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.BarCode
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.BarCode                
			WHEN ''KRT_Mappak'' THEN KRT_Mappak.Barcode
			ELSE isnull(EREC_UgyUgyiratok_Ugyirat.barcode,isnull(EREC_PldIratPeldanyok_IratPld.barcode,EREC_KuldKuldemenyek_Kuld.barcode))
		END,'

-- a max 4000 karakter miatt ketszer kell osszefuzni
SET @sqlcmd = @sqlcmd + '
	CimzettNev =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN EREC_UgyUgyiratok_Ugyirat.NevSTR_Ugyindito
					WHEN ''1'' THEN dbo.fn_GetCsoportNev(EREC_UgyUgyiratok_Ugyirat.Csoport_Id_Felelos)
				ELSE ''''
			END
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.NevSTR_Cimzett
			WHEN ''EREC_KuldKuldemenyek'' THEN KRT_CsoportokCsoport_Id_Cimzett_Kuld.Nev
			ELSE ''''
		END,
	CimzettCim =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN
				CASE EREC_IraIratok_PostazasIranya.PostazasIranya
					WHEN ''0'' THEN EREC_UgyUgyiratok_Ugyirat.CimSTR_Ugyindito
					WHEN ''1'' THEN ''''
					ELSE ''''
				END
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.CimSTR_Cimzett
			WHEN ''EREC_KuldKuldemenyek'' THEN ''''
			ELSE ''''
		END,
	Ugyintezo =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN dbo.fn_GetCsoportNev(EREC_UgyUgyiratok_Ugyirat.FelhasznaloCsoport_Id_Ugyintez)
			WHEN ''EREC_PldIratPeldanyok'' THEN ISNULL(dbo.fn_GetCsoportNev(EREC_IraIratok_IratPld.FelhasznaloCsoport_Id_Ugyintez),dbo.fn_GetCsoportNev(EREC_UgyUgyiratok_IratPld.FelhasznaloCsoport_Id_Ugyintez))
			WHEN ''EREC_KuldKuldemenyek'' THEN ''''
			ELSE ''''
		END,
'
----	CsatolmanyCount =
----		CASE EREC_IraKezbesitesiTetelek.Obj_type
----			WHEN ''EREC_UgyUgyiratok'' THEN dbo.fn_GetCsatolmanyCount(EREC_UgyUgyiratok_Ugyirat.Id)
----			WHEN ''EREC_PldIratPeldanyok'' THEN (SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok_IratPld.Id AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
----			WHEN ''EREC_KuldKuldemenyek'' THEN (SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek_Kuld.Id AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
----		ELSE 0
----	END,
---- megvizsgáljuk, hogy az irat bizalmas-e, és ezesetben a felhasználó az orzo vagy az orzo vezetoje-e
----if dbo.fn_IsAdmin(@ExecutorUserId) = 0 --AND @Jogosultak = '1' 
--IF @isAdminInSzervezet = 0 AND @Jogosultak = '1'
--begin
--	set @sqlcmd = @sqlcmd + '
--	CsatolmanyCount =
--		CASE EREC_IraKezbesitesiTetelek.Obj_type
--			WHEN ''EREC_UgyUgyiratok'' THEN (SELECT COUNT(*) FROM EREC_IraIratok
--               INNER JOIN EREC_Csatolmanyok
--				ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
--				and (1=case when dbo.fn_CheckIrat_IsConfidential(EREC_IraIratok.Id) = 0 then 1
--				when EREC_IraIratok.Id in (select Id from @iratok_executor) then 1 else 0 end)
--				WHERE getdate() BETWEEN EREC_Csatolmanyok.ErvKezd AND EREC_Csatolmanyok.ErvVege
--				AND EREC_IraIratok.Ugyirat_Id =  EREC_IraKezbesitesiTetelek.Obj_Id
--	        )--dbo.fn_GetCsatolmanyCountNotConfidential(EREC_UgyUgyiratok_Ugyirat.Id,''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''')
--			WHEN ''EREC_PldIratPeldanyok'' THEN (SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok_IratPld.Id
--				and (1=case when dbo.fn_CheckIrat_IsConfidential(EREC_IraIratok_IratPld.Id) = 0 then 1
--				when EREC_IraIratok_IratPld.Id in (select Id from @iratok_executor) then 1 else 0 end)
--				AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
--			WHEN ''EREC_KuldKuldemenyek'' THEN (SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek_Kuld.Id
--				and (1=case when [EREC_Csatolmanyok].IraIrat_Id is null then 1
--				when dbo.fn_CheckIrat_IsConfidential([EREC_Csatolmanyok].IraIrat_Id) = 0 then 1
--				when [EREC_Csatolmanyok].IraIrat_Id in (select Id from @iratok_executor) then 1 else 0 end)
--			  AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
--	ELSE 0
--	END,
--'
--end
--else
--begin
--	set @sqlcmd = @sqlcmd + '
--	CsatolmanyCount =
--		CASE EREC_IraKezbesitesiTetelek.Obj_type
--			WHEN ''EREC_UgyUgyiratok'' THEN dbo.fn_GetCsatolmanyCount(EREC_UgyUgyiratok_Ugyirat.Id)
--			WHEN ''EREC_PldIratPeldanyok'' THEN (SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok_IratPld.Id AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
--			WHEN ''EREC_KuldKuldemenyek'' THEN (SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek_Kuld.Id AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
--		ELSE 0
--	END,
--'
--end

-- a max 4000 karakter miatt ketszer kell osszefuzni
SET @sqlcmd = @sqlcmd + '
	UgyintezesModja =
		CASE EREC_IraKezbesitesiTetelek.Obj_type
			WHEN ''EREC_UgyUgyiratok'' THEN EREC_UgyUgyiratok_Ugyirat.UgyintezesModja
			WHEN ''EREC_PldIratPeldanyok'' THEN EREC_PldIratPeldanyok_IratPld.UgyintezesModja
			WHEN ''EREC_KuldKuldemenyek'' THEN EREC_KuldKuldemenyek_Kuld.UgyintezesModja
			ELSE ''''
		END,'

-- a max 4000 karakter miatt ketszer kell osszefuzni
SET @sqlcmd = @sqlcmd + '
	CONVERT(nvarchar(19), EREC_IraKezbesitesiTetelek.AtadasDat, 120) as AtadasDat,
	CONVERT(nvarchar(19), EREC_IraKezbesitesiTetelek.AtvetelDat, 120) as AtvetelDat,
	   EREC_IraKezbesitesiTetelek.Megjegyzes,
	   EREC_IraKezbesitesiTetelek.SztornirozasDat,
	   EREC_IraKezbesitesiTetelek.UtolsoNyomtatas_Id,
	   EREC_IraKezbesitesiTetelek.UtolsoNyomtatasIdo,
	KRT_Csoportok_Atado.Nev as Atado_Nev,
	   EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER,
	   EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_LOGIN,
	KRT_Csoportok_Cel.Nev as Cimzett_Nev,
	   EREC_IraKezbesitesiTetelek.Csoport_Id_Cel,
	KRT_Csoportok_Atvevo.Nev as Atvevo_Nev,
	   EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoUser,
	   EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoLogin,
	   EREC_IraKezbesitesiTetelek.Allapot,
	   EREC_IraKezbesitesiTetelek.Azonosito_szoveges,
coalesce(EREC_UgyUgyiratok_Ugyirat.barcode,EREC_PldIratPeldanyok_IratPld.barcode,EREC_KuldKuldemenyek_Kuld.barcode,KRT_Mappak.Barcode) as Barcode,
	   dbo.fn_GetEREC_HataridosFeladatokCount(EREC_IraKezbesitesiTetelek.Obj_Id,@ExecutorUserId, @FelhasznaloSzervezet_Id) as FeladatCount,
	   EREC_IraKezbesitesiTetelek.Ver,
	   EREC_IraKezbesitesiTetelek.Note,
	   EREC_IraKezbesitesiTetelek.Stat_id,
	   EREC_IraKezbesitesiTetelek.ErvKezd,
	   EREC_IraKezbesitesiTetelek.ErvVege,
	   EREC_IraKezbesitesiTetelek.Letrehozo_id,
	CONVERT(nvarchar(19), EREC_IraKezbesitesiTetelek.LetrehozasIdo, 120) as LetrehozasIdo,
	   EREC_IraKezbesitesiTetelek.Modosito_id,
	   EREC_IraKezbesitesiTetelek.ModositasIdo,
	   EREC_IraKezbesitesiTetelek.Zarolo_id,
	   EREC_IraKezbesitesiTetelek.ZarolasIdo,
	   EREC_IraKezbesitesiTetelek.Tranz_id,
	   EREC_IraKezbesitesiTetelek.UIAccessLog_id  
   from '

-- a max 4000 karakter miatt ketszer kell osszefuzni
SET @sqlcmd = @sqlcmd + '
    EREC_IraKezbesitesiTetelek as EREC_IraKezbesitesiTetelek 
	left join KRT_Csoportok as KRT_Csoportok_Atado
		ON EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER = KRT_Csoportok_Atado.Id
	left join KRT_Csoportok as KRT_Csoportok_Atvevo
		ON EREC_IraKezbesitesiTetelek.Felhasznalo_Id_AtvevoUser = KRT_Csoportok_Atvevo.Id
	left join KRT_Csoportok as KRT_Csoportok_Cel
		ON EREC_IraKezbesitesiTetelek.Csoport_Id_Cel = KRT_Csoportok_Cel.Id     
	LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok_Ugyirat
		ON EREC_IraKezbesitesiTetelek.Obj_Id = EREC_UgyUgyiratok_Ugyirat.Id
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Ugyirat
		ON EREC_UgyUgyiratok_Ugyirat.IraIktatokonyv_Id = EREC_IraIktatoKonyvek_Ugyirat.Id     
	LEFT JOIN EREC_PldIratPeldanyok as EREC_PldIratPeldanyok_IratPld
		ON EREC_IraKezbesitesiTetelek.Obj_Id = EREC_PldIratPeldanyok_IratPld.Id
	LEFT JOIN EREC_IraIratok as EREC_IraIratok_IratPld
		ON EREC_PldIratPeldanyok_IratPld.IraIrat_Id = EREC_IraIratok_IratPld.Id
	LEFT JOIN EREC_KuldKuldemenyek AS EREC_KuldKuldemenyek_IratPld
		ON EREC_IraIratok_IratPld.KuldKuldemenyek_Id = EREC_KuldKuldemenyek_IratPld.Id
	LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok_IratPld
		ON EREC_IraIratok_IratPld.Ugyirat_Id = EREC_UgyUgyiratok_IratPld.Id
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_IratPld
		ON EREC_UgyUgyiratok_IratPld.IraIktatokonyv_Id = EREC_IraIktatoKonyvek_IratPld.Id
	LEFT JOIN EREC_KuldKuldemenyek as EREC_KuldKuldemenyek_Kuld
		ON EREC_IraKezbesitesiTetelek.Obj_Id = EREC_KuldKuldemenyek_Kuld.Id
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Kuld
		ON EREC_KuldKuldemenyek_Kuld.IraIktatokonyv_Id = EREC_IraIktatoKonyvek_Kuld.Id
	left join KRT_Csoportok as KRT_CsoportokCsoport_Id_Cimzett_Kuld
		on KRT_CsoportokCsoport_Id_Cimzett_Kuld.Id = EREC_KuldKuldemenyek_Kuld.Csoport_Id_Cimzett
	left join KRT_Mappak ON KRT_Mappak.Id = EREC_IraKezbesitesiTetelek.Obj_Id
 '

-- a max 4000 karakter miatt ketszer kell osszefuzni
-- Kihasználjuk, hogy az Org oszlopokban nem lehet null
SET @sqlcmd = @sqlcmd + '    
	LEFT JOIN EREC_IraIratok AS EREC_IraIratok_PostazasIranya
		ON EREC_IraIratok_PostazasIranya.Ugyirat_Id = EREC_UgyUgyiratok_Ugyirat.Id AND EREC_IraIratok_PostazasIranya.Alszam = 1
Where IsNull(EREC_IraIktatoKonyvek_Ugyirat.Org,@Org)=@Org
and IsNull(EREC_IraIktatoKonyvek_IratPld.Org,@Org)=@Org
and IsNull(EREC_IraIktatoKonyvek_Kuld.Org,@Org)=@Org
and IsNull(KRT_Csoportok_Atado.Org,@Org)=@Org
and IsNull(KRT_Csoportok_Atvevo.Org,@Org)=@Org
and IsNull(KRT_Csoportok_Cel.Org,@Org)=@Org
'

     

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where;
	END
	
	-- Jogosultságszurés
	IF @isAdminInSzervezet = 0 AND @Jogosultak = '1'
	BEGIN
--		if @Where is not null and @Where!=''
--		BEGIN
			SET @sqlcmd = @sqlcmd + ' AND '
--		END
--		ELSE
--		BEGIN
--			SET @sqlcmd = @sqlcmd + ' Where '
--		END
			
		SET @sqlcmd = @sqlcmd + ' (EREC_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER IN (@ExecutorUserId,@FelhasznaloSzervezet_Id)
OR EREC_IraKezbesitesiTetelek.Csoport_Id_Cel IN (@ExecutorUserId,@FelhasznaloSzervezet_Id))
'
		
	END
     
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  
	execute sp_executesql @sqlcmd,N'@ExecutorUserId uniqueidentifier, @FelhasznaloSzervezet_Id uniqueidentifier, @Org uniqueidentifier'
		,@ExecutorUserId = @ExecutorUserId, @FelhasznaloSzervezet_Id = @FelhasznaloSzervezet_Id, @Org = @Org;


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end