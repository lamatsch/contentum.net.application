﻿create procedure [dbo].[sp_EREC_IraOnkormAdatokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IraOnkormAdatok.Id,
	   EREC_IraOnkormAdatok.IraIratok_Id,
	   EREC_IraOnkormAdatok.UgyFajtaja,
	   EREC_IraOnkormAdatok.DontestHozta,
	   EREC_IraOnkormAdatok.DontesFormaja,
	   EREC_IraOnkormAdatok.UgyintezesHataridore,
	   EREC_IraOnkormAdatok.HataridoTullepes,
	   EREC_IraOnkormAdatok.JogorvoslatiEljarasTipusa,
	   EREC_IraOnkormAdatok.JogorvoslatiDontesTipusa,
	   EREC_IraOnkormAdatok.JogorvoslatiDontestHozta,
	   EREC_IraOnkormAdatok.JogorvoslatiDontesTartalma,
	   EREC_IraOnkormAdatok.JogorvoslatiDontes,
	   EREC_IraOnkormAdatok.HatosagiEllenorzes,
	   EREC_IraOnkormAdatok.MunkaorakSzama,
	   EREC_IraOnkormAdatok.EljarasiKoltseg,
	   EREC_IraOnkormAdatok.KozigazgatasiBirsagMerteke,
	   EREC_IraOnkormAdatok.SommasEljDontes,
	   EREC_IraOnkormAdatok.NyolcNapBelulNemSommas,
	   EREC_IraOnkormAdatok.FuggoHatalyuHatarozat,
	   EREC_IraOnkormAdatok.FuggoHatHatalybaLepes,
	   EREC_IraOnkormAdatok.FuggoHatalyuVegzes,
	   EREC_IraOnkormAdatok.FuggoVegzesHatalyba,
	   EREC_IraOnkormAdatok.HatAltalVisszafizOsszeg,
	   EREC_IraOnkormAdatok.HatTerheloEljKtsg,
	   EREC_IraOnkormAdatok.FelfuggHatarozat,
	   EREC_IraOnkormAdatok.Ver,
	   EREC_IraOnkormAdatok.Note,
	   EREC_IraOnkormAdatok.Stat_id,
	   EREC_IraOnkormAdatok.ErvKezd,
	   EREC_IraOnkormAdatok.ErvVege,
	   EREC_IraOnkormAdatok.Letrehozo_id,
	   EREC_IraOnkormAdatok.LetrehozasIdo,
	   EREC_IraOnkormAdatok.Modosito_id,
	   EREC_IraOnkormAdatok.ModositasIdo,
	   EREC_IraOnkormAdatok.Zarolo_id,
	   EREC_IraOnkormAdatok.ZarolasIdo,
	   EREC_IraOnkormAdatok.Tranz_id,
	   EREC_IraOnkormAdatok.UIAccessLog_id
	   from 
		 EREC_IraOnkormAdatok as EREC_IraOnkormAdatok 
	   where
		 EREC_IraOnkormAdatok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end