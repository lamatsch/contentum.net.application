﻿CREATE procedure [dbo].[sp_EREC_IraOnkormAdatokGetWithExtensionForAdatlap]
	@Id uniqueidentifier, -- EREC_UgyUgyiratok.Id
  @ExecutorUserId	uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)


  SET @sqlcmd = 
  'select
  	 EREC_IraIratok.Id,
	   EREC_IraIratok.Azonosito as Iktatoszam,
		EREC_IraIrattariTetelek.IrattariTetelszam as IrattariTetelszam,
	substring(EREC_IraIrattariTetelek.IrattariTetelszam,1,1) as IrattariTetelszam_1,
	substring(EREC_IraIrattariTetelek.IrattariTetelszam,2,1) as IrattariTetelszam_2,
	substring(EREC_IraIrattariTetelek.IrattariTetelszam,3,1) as IrattariTetelszam_3,
	substring(EREC_AgazatiJelek.Kod,1,1) as AgazatiJel_Betu,  
	substring(EREC_AgazatiJelek.Kod,3,1) as AgazatiJel_Szam1, 
	substring(EREC_AgazatiJelek.Kod,5,1) as AgazatiJel_Szam2,
	EREC_IraOnkormAdatok.UgyFajtaja as UgyFajtaja,
	EREC_IraOnkormAdatok.DontestHozta as DontestHozta,
	EREC_IraOnkormAdatok.DontesFormaja as DontesFormaja,
	EREC_IraOnkormAdatok.UgyintezesHataridore as UgyintezesHataridore,
	EREC_IraOnkormAdatok.HataridoTullepes as HataridoTullepes,
	substring(right(''   '' + cast(EREC_IraOnkormAdatok.HataridoTullepes as nvarchar), 3), 1, 1) as HataridoTullepes_1,
	substring(right(''   '' + cast(EREC_IraOnkormAdatok.HataridoTullepes as nvarchar), 3), 2, 1) as HataridoTullepes_2,
    substring(right(''   '' + cast(EREC_IraOnkormAdatok.HataridoTullepes as nvarchar), 3), 3, 1) as HataridoTullepes_3,
	EREC_IraOnkormAdatok.JogorvoslatiEljarasTipusa as JogorvoslatiEljarasTipusa,
	EREC_IraOnkormAdatok.JogorvoslatiDontesTipusa as JogorvoslatiDontesTipusa,
	EREC_IraOnkormAdatok.JogorvoslatiDontestHozta as JogorvoslatiDontestHozta,
	EREC_IraOnkormAdatok.JogorvoslatiDontesTartalma as JogorvoslatiDontesTartalma,
	EREC_IraOnkormAdatok.JogorvoslatiDontes as JogorvoslatiDontes,
	case when ISNULL(EREC_IraOnkormAdatok.HatosagiEllenorzes,''0'') = ''1'' then ''X'' else '''' end as HatosagiEllenorzes_Igen,
	case when ISNULL(EREC_IraOnkormAdatok.HatosagiEllenorzes,''0'') <> ''1'' then ''X'' else '''' end as HatosagiEllenorzes_Nem,
	substring(right(''   '' + cast(cast(EREC_IraOnkormAdatok.MunkaorakSzama as int) as nvarchar), 3), 1, 1) as MunkaorakSzama_1,
	substring(right(''   '' + cast(cast(EREC_IraOnkormAdatok.MunkaorakSzama as int) as nvarchar), 3), 2, 1) as MunkaorakSzama_2,
	substring(right(''   '' + cast(cast(EREC_IraOnkormAdatok.MunkaorakSzama as int) as nvarchar), 3), 3, 1) as MunkaorakSzama_3,
	case when charindex(''.'',cast(EREC_IraOnkormAdatok.MunkaorakSzama as nvarchar)) > 0 
	then SUBSTRING(cast(EREC_IraOnkormAdatok.MunkaorakSzama as nvarchar), charindex(''.'',cast(EREC_IraOnkormAdatok.MunkaorakSzama as nvarchar))+1, 1)
	else '''' end as MunkaorakSzama_4,
	substring(right(''      '' + cast(EREC_IraOnkormAdatok.EljarasiKoltseg as nvarchar), 6), 1, 1) as EljarasiKoltseg_1,
	substring(right(''      '' + cast(EREC_IraOnkormAdatok.EljarasiKoltseg as nvarchar), 6), 2, 1) as EljarasiKoltseg_2,
	substring(right(''      '' + cast(EREC_IraOnkormAdatok.EljarasiKoltseg as nvarchar), 6), 3, 1) as EljarasiKoltseg_3,
	substring(right(''      '' + cast(EREC_IraOnkormAdatok.EljarasiKoltseg as nvarchar), 6), 4, 1) as EljarasiKoltseg_4,
	substring(right(''      '' + cast(EREC_IraOnkormAdatok.EljarasiKoltseg as nvarchar), 6), 5, 1) as EljarasiKoltseg_5,
	substring(right(''      '' + cast(EREC_IraOnkormAdatok.EljarasiKoltseg as nvarchar), 6), 6, 1) as EljarasiKoltseg_6,
	substring(right(''      '' + cast(EREC_IraOnkormAdatok.KozigazgatasiBirsagMerteke as nvarchar), 8), 1, 1) as KozigazgatasiBirsagMerteke_1,
	substring(right(''      '' + cast(EREC_IraOnkormAdatok.KozigazgatasiBirsagMerteke as nvarchar), 8), 2, 1) as KozigazgatasiBirsagMerteke_2,
	substring(right(''      '' + cast(EREC_IraOnkormAdatok.KozigazgatasiBirsagMerteke as nvarchar), 8), 3, 1) as KozigazgatasiBirsagMerteke_3,
	substring(right(''      '' + cast(EREC_IraOnkormAdatok.KozigazgatasiBirsagMerteke as nvarchar), 8), 4, 1) as KozigazgatasiBirsagMerteke_4,
	substring(right(''      '' + cast(EREC_IraOnkormAdatok.KozigazgatasiBirsagMerteke as nvarchar), 8), 5, 1) as KozigazgatasiBirsagMerteke_5,
	substring(right(''      '' + cast(EREC_IraOnkormAdatok.KozigazgatasiBirsagMerteke as nvarchar), 8), 6, 1) as KozigazgatasiBirsagMerteke_6,
	substring(right(''      '' + cast(EREC_IraOnkormAdatok.KozigazgatasiBirsagMerteke as nvarchar), 8), 7, 1) as KozigazgatasiBirsagMerteke_7,
	substring(right(''      '' + cast(EREC_IraOnkormAdatok.KozigazgatasiBirsagMerteke as nvarchar), 8), 8, 1) as KozigazgatasiBirsagMerteke_8 
	   '
SET @sqlcmd = @sqlcmd +
   '   
       from EREC_IraIratok
            left join EREC_UgyUgyiratdarabok
                ON EREC_IraIratok.UgyUgyiratDarab_Id = EREC_UgyUgyiratdarabok.Id
            left join EREC_UgyUgyiratok
                ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
			left join EREC_IraIktatokonyvek
				ON EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id
			left join EREC_IraIrattariTetelek as EREC_IraIrattariTetelek
				ON EREC_UgyUgyiratok.IraIrattariTetel_Id = EREC_IraIrattariTetelek.Id
			left join EREC_AgazatiJelek
				ON EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_AgazatiJelek.Id
			left join EREC_IraOnkormAdatok
				ON EREC_IraIratok.Id = EREC_IraOnkormAdatok.IraIratok_Id
'

SET @sqlcmd = @sqlcmd +
' WHERE EREC_IraIratok.Id = ''' + cast(@Id as nvarchar(40)) + ''''


	exec(@sqlcmd)



END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end