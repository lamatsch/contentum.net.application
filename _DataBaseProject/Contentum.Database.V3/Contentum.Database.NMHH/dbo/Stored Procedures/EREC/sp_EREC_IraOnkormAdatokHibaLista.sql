﻿create procedure [dbo].[sp_EREC_IraOnkormAdatokHibaLista]
  @Where nvarchar(MAX) = '',
  @Where_EREC_IraIratok NVARCHAR(MAX) = '',
  @OrderBy nvarchar(200) = ' order by EREC_IraIktatokonyvek.Ev DESC, EREC_UgyUgyiratok.Foszam DESC, EREC_IraIratok.Alszam DESC, EREC_IraIratok.Sorszam',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
	
	DECLARE @Org uniqueidentifier
	--SET @Org = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_IraIktatokonyvek.Iktatohely as IkatatoKonyv,
       EREC_IraIratok.Azonosito as IktatatoSzam,
       EREC_IraOnkormAdatok.IraIratok_Id,
	   EREC_IraOnkormAdatok.UgyFajtaja,
	   dbo.fn_HatosagiAdatKodtarErtek(''UGY_FAJTAJA'', EREC_IraOnkormAdatok.UgyFajtaja, @Org, EREC_IraIratok.LetrehozasIdo) as UgyFajtaja_Kod,
	   EREC_IraOnkormAdatok.DontestHozta,
	   dbo.fn_HatosagiAdatKodtarErtek(''DONTEST_HOZTA'', EREC_IraOnkormAdatok.DontestHozta, @Org, EREC_IraIratok.LetrehozasIdo) as DontestHozta_Kod,
	   EREC_IraOnkormAdatok.DontesFormaja,
	   dbo.fn_HatosagiAdatKodtarErtek(''DONTES_FORMAJA'', EREC_IraOnkormAdatok.DontesFormaja, @Org, EREC_IraIratok.LetrehozasIdo) as DontesFormaja_Kod,
	   EREC_IraOnkormAdatok.UgyintezesHataridore,
	   dbo.fn_KodtarErtekNeve(''UGYINTEZES_IDOTARTAMA'', EREC_IraOnkormAdatok.UgyintezesHataridore,@Org) as UgyintezesHataridore_Kod,	   
	   EREC_IraOnkormAdatok.HataridoTullepes,
	   EREC_IraOnkormAdatok.JogorvoslatiEljarasTipusa,
	   dbo.fn_KodtarErtekNeve(''JOGORVOSLATI_ELJARAS_TIPUSA'', EREC_IraOnkormAdatok.JogorvoslatiEljarasTipusa,@Org) as JogorvoslatiEljarasTipusa_Kod,
	   EREC_IraOnkormAdatok.JogorvoslatiDontesTipusa,
	   dbo.fn_KodtarErtekNeve(''JOGORVOSLATI_DONTES_TIPUSA'', EREC_IraOnkormAdatok.JogorvoslatiDontesTipusa,@Org) as JogorvoslatiDontesTipusa_Kod,
	   EREC_IraOnkormAdatok.JogorvoslatiDontestHozta,
	   dbo.fn_KodtarErtekNeve(''JOGORVOSLATI_DONTEST_HOZTA'', EREC_IraOnkormAdatok.JogorvoslatiDontestHozta,@Org) as JogorvoslatiDontestHozta_Kod,
	   EREC_IraOnkormAdatok.JogorvoslatiDontesTartalma,
	   dbo.fn_KodtarErtekNeve(''JOGORVOSLATI_DONTES_TARTALMA'', EREC_IraOnkormAdatok.JogorvoslatiDontesTartalma,@Org) as JogorvoslatiDontesTartalma_Kod,
	   EREC_IraOnkormAdatok.JogorvoslatiDontes,
	   dbo.fn_KodtarErtekNeve(''JOGORVOSLATI_DONTES'', EREC_IraOnkormAdatok.JogorvoslatiDontes,@Org) as JogorvoslatiDontes_Kod
	FROM EREC_IraIratok as EREC_IraIratok
	LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok
	ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id
	LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek
	ON EREC_IraIktatoKonyvek.Id = EREC_UgyUgyiratok.IraIktatokonyv_Id
	join VW_EREC_IraOnkormAdatok as EREC_IraOnkormAdatok
	ON EREC_IraOnkormAdatok.IraIratok_Id = EREC_IraIratok.Id
	WHERE
	(
       EREC_IraOnkormAdatok.UgyFajtaja = ''1'' 
       or EREC_IraOnkormAdatok.UgyFajtaja = ''2''
    )
    and not'
	
	SET @sqlcmd = @sqlcmd + '
	(
		(
			(
				EREC_IraOnkormAdatok.DontestHozta is null
				and EREC_IraOnkormAdatok.DontesFormaja is null
				and EREC_IraOnkormAdatok.UgyintezesHataridore is null
				and EREC_IraOnkormAdatok.HataridoTullepes is null
			)
			or
			(
				EREC_IraOnkormAdatok.DontestHozta is not null
				and EREC_IraOnkormAdatok.DontesFormaja is not null
				and EREC_IraOnkormAdatok.UgyintezesHataridore is not null
				and EREC_IraOnkormAdatok.HataridoTullepes is not null
			)
		)
		and
		(
			(
				EREC_IraOnkormAdatok.UgyintezesHataridore = ''1'' --határidon belül
				and EREC_IraOnkormAdatok.HataridoTullepes = 0
			)
			or
			(
				EREC_IraOnkormAdatok.UgyintezesHataridore = ''2'' --határidon túl
				and EREC_IraOnkormAdatok.HataridoTullepes > 0
			)
		)
		and
		(
			(
				EREC_IraOnkormAdatok.JogorvoslatiEljarasTipusa is null
				and EREC_IraOnkormAdatok.JogorvoslatiDontesTipusa is null
				and EREC_IraOnkormAdatok.JogorvoslatiDontestHozta is null
				and EREC_IraOnkormAdatok.JogorvoslatiDontesTartalma is null
				and EREC_IraOnkormAdatok.JogorvoslatiDontes is null
			)
			or
			(
				EREC_IraOnkormAdatok.JogorvoslatiEljarasTipusa is not null
				and EREC_IraOnkormAdatok.JogorvoslatiDontesTipusa is not null
				and EREC_IraOnkormAdatok.JogorvoslatiDontestHozta is not null
				and EREC_IraOnkormAdatok.JogorvoslatiDontesTartalma is not null
			)
			or
			(
				EREC_IraOnkormAdatok.JogorvoslatiEljarasTipusa is not null
				and EREC_IraOnkormAdatok.JogorvoslatiDontesTipusa is null
				and EREC_IraOnkormAdatok.JogorvoslatiDontestHozta is null
				and EREC_IraOnkormAdatok.JogorvoslatiDontesTartalma is null
			)
		)
		and
		(
			(
				EREC_IraOnkormAdatok.UgyFajtaja = ''1'' -- álammigazgatási
				and 
				(
					EREC_IraOnkormAdatok.DontestHozta is null
					or EREC_IraOnkormAdatok.DontestHozta = ''4'' -- fopolgármester
					or EREC_IraOnkormAdatok.DontestHozta = ''5'' -- fojegyzo
				)
			)
			or
			(
				EREC_IraOnkormAdatok.UgyFajtaja = ''2'' -- önkormányzati
				--and 
				--(
				--   EREC_IraOnkormAdatok.DontestHozta is null
				--   or EREC_IraOnkormAdatok.DontestHozta != ''5'' -- fojegyzo
				--)
			)
		)
	)
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
	
	if @Where_EREC_IraIratok is not null and @Where_EREC_IraIratok!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where_EREC_IraIratok
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	
	execute sp_executesql @sqlcmd,N'@Org uniqueidentifier', @Org;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
