﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_IraOnkormAdatokHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_EREC_IraOnkormAdatokHistoryGetRecord
go
*/
create procedure sp_EREC_IraOnkormAdatokHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_IraOnkormAdatokHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_IraOnkormAdatokHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyFajtaja' as ColumnName,               
               cast(Old.UgyFajtaja as nvarchar(99)) as OldValue,
               cast(New.UgyFajtaja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyFajtaja as nvarchar(max)),'') != ISNULL(CAST(New.UgyFajtaja as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DontestHozta' as ColumnName,               
               cast(Old.DontestHozta as nvarchar(99)) as OldValue,
               cast(New.DontestHozta as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.DontestHozta as nvarchar(max)),'') != ISNULL(CAST(New.DontestHozta as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DontesFormaja' as ColumnName,               
               cast(Old.DontesFormaja as nvarchar(99)) as OldValue,
               cast(New.DontesFormaja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.DontesFormaja as nvarchar(max)),'') != ISNULL(CAST(New.DontesFormaja as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesHataridore' as ColumnName,               
               cast(Old.UgyintezesHataridore as nvarchar(99)) as OldValue,
               cast(New.UgyintezesHataridore as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyintezesHataridore as nvarchar(max)),'') != ISNULL(CAST(New.UgyintezesHataridore as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HataridoTullepes' as ColumnName,               
               cast(Old.HataridoTullepes as nvarchar(99)) as OldValue,
               cast(New.HataridoTullepes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HataridoTullepes as nvarchar(max)),'') != ISNULL(CAST(New.HataridoTullepes as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'JogorvoslatiEljarasTipusa' as ColumnName,               
               cast(Old.JogorvoslatiEljarasTipusa as nvarchar(99)) as OldValue,
               cast(New.JogorvoslatiEljarasTipusa as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.JogorvoslatiEljarasTipusa as nvarchar(max)),'') != ISNULL(CAST(New.JogorvoslatiEljarasTipusa as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'JogorvoslatiDontesTipusa' as ColumnName,               
               cast(Old.JogorvoslatiDontesTipusa as nvarchar(99)) as OldValue,
               cast(New.JogorvoslatiDontesTipusa as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.JogorvoslatiDontesTipusa as nvarchar(max)),'') != ISNULL(CAST(New.JogorvoslatiDontesTipusa as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'JogorvoslatiDontestHozta' as ColumnName,               
               cast(Old.JogorvoslatiDontestHozta as nvarchar(99)) as OldValue,
               cast(New.JogorvoslatiDontestHozta as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.JogorvoslatiDontestHozta as nvarchar(max)),'') != ISNULL(CAST(New.JogorvoslatiDontestHozta as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'JogorvoslatiDontesTartalma' as ColumnName,               
               cast(Old.JogorvoslatiDontesTartalma as nvarchar(99)) as OldValue,
               cast(New.JogorvoslatiDontesTartalma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.JogorvoslatiDontesTartalma as nvarchar(max)),'') != ISNULL(CAST(New.JogorvoslatiDontesTartalma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'JogorvoslatiDontes' as ColumnName,               
               cast(Old.JogorvoslatiDontes as nvarchar(99)) as OldValue,
               cast(New.JogorvoslatiDontes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.JogorvoslatiDontes as nvarchar(max)),'') != ISNULL(CAST(New.JogorvoslatiDontes as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatosagiEllenorzes' as ColumnName,               
               cast(Old.HatosagiEllenorzes as nvarchar(99)) as OldValue,
               cast(New.HatosagiEllenorzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HatosagiEllenorzes as nvarchar(max)),'') != ISNULL(CAST(New.HatosagiEllenorzes as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MunkaorakSzama' as ColumnName,               
               cast(Old.MunkaorakSzama as nvarchar(99)) as OldValue,
               cast(New.MunkaorakSzama as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MunkaorakSzama as nvarchar(max)),'') != ISNULL(CAST(New.MunkaorakSzama as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EljarasiKoltseg' as ColumnName,               
               cast(Old.EljarasiKoltseg as nvarchar(99)) as OldValue,
               cast(New.EljarasiKoltseg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EljarasiKoltseg as nvarchar(max)),'') != ISNULL(CAST(New.EljarasiKoltseg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KozigazgatasiBirsagMerteke' as ColumnName,               
               cast(Old.KozigazgatasiBirsagMerteke as nvarchar(99)) as OldValue,
               cast(New.KozigazgatasiBirsagMerteke as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KozigazgatasiBirsagMerteke as nvarchar(max)),'') != ISNULL(CAST(New.KozigazgatasiBirsagMerteke as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SommasEljDontes' as ColumnName,               
               cast(Old.SommasEljDontes as nvarchar(99)) as OldValue,
               cast(New.SommasEljDontes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SommasEljDontes as nvarchar(max)),'') != ISNULL(CAST(New.SommasEljDontes as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'NyolcNapBelulNemSommas' as ColumnName,               
               cast(Old.NyolcNapBelulNemSommas as nvarchar(99)) as OldValue,
               cast(New.NyolcNapBelulNemSommas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.NyolcNapBelulNemSommas as nvarchar(max)),'') != ISNULL(CAST(New.NyolcNapBelulNemSommas as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FuggoHatalyuHatarozat' as ColumnName,               
               cast(Old.FuggoHatalyuHatarozat as nvarchar(99)) as OldValue,
               cast(New.FuggoHatalyuHatarozat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FuggoHatalyuHatarozat as nvarchar(max)),'') != ISNULL(CAST(New.FuggoHatalyuHatarozat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FuggoHatHatalybaLepes' as ColumnName,               
               cast(Old.FuggoHatHatalybaLepes as nvarchar(99)) as OldValue,
               cast(New.FuggoHatHatalybaLepes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FuggoHatHatalybaLepes as nvarchar(max)),'') != ISNULL(CAST(New.FuggoHatHatalybaLepes as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FuggoHatalyuVegzes' as ColumnName,               
               cast(Old.FuggoHatalyuVegzes as nvarchar(99)) as OldValue,
               cast(New.FuggoHatalyuVegzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FuggoHatalyuVegzes as nvarchar(max)),'') != ISNULL(CAST(New.FuggoHatalyuVegzes as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FuggoVegzesHatalyba' as ColumnName,               
               cast(Old.FuggoVegzesHatalyba as nvarchar(99)) as OldValue,
               cast(New.FuggoVegzesHatalyba as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FuggoVegzesHatalyba as nvarchar(max)),'') != ISNULL(CAST(New.FuggoVegzesHatalyba as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatAltalVisszafizOsszeg' as ColumnName,               
               cast(Old.HatAltalVisszafizOsszeg as nvarchar(99)) as OldValue,
               cast(New.HatAltalVisszafizOsszeg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HatAltalVisszafizOsszeg as nvarchar(max)),'') != ISNULL(CAST(New.HatAltalVisszafizOsszeg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatTerheloEljKtsg' as ColumnName,               
               cast(Old.HatTerheloEljKtsg as nvarchar(99)) as OldValue,
               cast(New.HatTerheloEljKtsg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HatTerheloEljKtsg as nvarchar(max)),'') != ISNULL(CAST(New.HatTerheloEljKtsg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelfuggHatarozat' as ColumnName,               
               cast(Old.FelfuggHatarozat as nvarchar(99)) as OldValue,
               cast(New.FelfuggHatarozat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IraOnkormAdatokHistory Old
         inner join EREC_IraOnkormAdatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IraOnkormAdatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelfuggHatarozat as nvarchar(max)),'') != ISNULL(CAST(New.FelfuggHatarozat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go