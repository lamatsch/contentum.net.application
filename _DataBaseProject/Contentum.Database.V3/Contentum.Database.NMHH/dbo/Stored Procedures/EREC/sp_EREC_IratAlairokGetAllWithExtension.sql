﻿create procedure [dbo].[sp_EREC_IratAlairokGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IratAlairok.LetrehozasIdo DESC',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
               

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_IratAlairok.Id,
       EREC_IratAlairok.PldIratPeldany_Id,
       EREC_IratAlairok.Objtip_Id,
       EREC_IratAlairok.Obj_Id,
       EREC_IratAlairok.AlairasSzabaly_Id,
	EREC_IratAlairok.AlairasDatuma as AlairasDatuma,
	   EREC_IratAlairok.Leiras,
	KRT_KodTarak_AlairoSzerep.Nev as AlairoSzerep_Nev,
	   EREC_IratAlairok.AlairoSzerep,
	KRT_Csoportok_Alairo.Nev as Alairo_Nev,
	   EREC_IratAlairok.AlairasDatuma,
	   EREC_IratAlairok.Leiras,
	   EREC_IratAlairok.AlairasSorrend,
	   EREC_IratAlairok.FelhasznaloCsoport_Id_Alairo,
	   EREC_IratAlairok.FelhaszCsoport_Id_Helyettesito,
	   EREC_IratAlairok.Azonosito,
	   EREC_IratAlairok.AlairasMod,
	   EREC_IratAlairok.AlairasSzabaly_Id,
	(SELECT krt_alairastipusok.Nev
	FROM KRT_AlairasSzabalyok
		JOIN KRT_AlairasTipusok
			ON KRT_AlairasSzabalyok.AlairasTipus_Id = KRT_AlairasTipusok.Id
	WHERE KRT_AlairasSzabalyok.Id = EREC_IratAlairok.AlairasSzabaly_Id) AS AlairasTipus,
	   EREC_IratAlairok.Allapot,
	dbo.[fn_KodtarErtekNeve](''IRATALAIRAS_ALLAPOT'',EREC_IratAlairok.Allapot,''' + CAST(@Org as NVarChar(40)) + ''') AS Allapot_Nev,
	  EREC_IratAlairok.FelhaszCsop_Id_HelyettesAlairo,
	  KRT_Csoportok_HelyettesAlairo.Nev as HelyettesAlairo_Nev,
	   EREC_IratAlairok.Ver,
	   EREC_IratAlairok.Note,
	   EREC_IratAlairok.Stat_id,
	   EREC_IratAlairok.ErvKezd,
	   EREC_IratAlairok.ErvVege,
	   EREC_IratAlairok.Letrehozo_id,
	   EREC_IratAlairok.LetrehozasIdo,
	   EREC_IratAlairok.Modosito_id,
	   EREC_IratAlairok.ModositasIdo,
	   EREC_IratAlairok.Zarolo_id,
	   EREC_IratAlairok.ZarolasIdo,
	   EREC_IratAlairok.Tranz_id,
	   EREC_IratAlairok.UIAccessLog_id    
   from 
     EREC_IratAlairok as EREC_IratAlairok   
	left join KRT_Csoportok as KRT_Csoportok_Alairo
				ON EREC_IratAlairok.FelhasznaloCsoport_Id_Alairo = KRT_Csoportok_Alairo.Id
	left join KRT_Csoportok as KRT_Csoportok_HelyettesAlairo
				ON EREC_IratAlairok.FelhaszCsop_Id_HelyettesAlairo = KRT_Csoportok_HelyettesAlairo.Id     
	left join KRT_KodCsoportok as KRT_KodCsoportok_AlairoSzerep 
		on KRT_KodCsoportok_AlairoSzerep.Kod=''ALAIRO_SZEREP''
	left join KRT_KodTarak as KRT_KodTarak_AlairoSzerep 
		on KRT_KodCsoportok_AlairoSzerep.Id = KRT_KodTarak_AlairoSzerep.KodCsoport_Id and EREC_IratAlairok.AlairoSzerep = KRT_KodTarak_AlairoSzerep.Kod and KRT_KodTarak_AlairoSzerep.Org=''' + CAST(@Org as NVarChar(40)) + '''
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end