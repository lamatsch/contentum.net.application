﻿
create procedure sp_EREC_IratAlairokInsert    
                @Id      uniqueidentifier = null,    
                               @PldIratPeldany_Id     uniqueidentifier  = null,
                @Objtip_Id     uniqueidentifier  = null,
                @Obj_Id     uniqueidentifier  = null,
                @AlairasDatuma     datetime  = null,
                @Leiras     Nvarchar(4000)  = null,
                @AlairoSzerep     nvarchar(64)  = null,
                @AlairasSorrend     int  = null,
	            @FelhasznaloCsoport_Id_Alairo     uniqueidentifier,
                @FelhaszCsoport_Id_Helyettesito     uniqueidentifier  = null,
                @Azonosito     Nvarchar(100)  = null,
                @AlairasMod     nvarchar(64)  = null,
                @AlairasSzabaly_Id     uniqueidentifier  = null,
                @Allapot     nvarchar(64)  = null,
                @FelhaszCsop_Id_HelyettesAlairo     uniqueidentifier  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @PldIratPeldany_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',PldIratPeldany_Id'
            SET @insertValues = @insertValues + ',@PldIratPeldany_Id'
         end 
       
         if @Objtip_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Objtip_Id'
            SET @insertValues = @insertValues + ',@Objtip_Id'
         end 
       
         if @Obj_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Obj_Id'
            SET @insertValues = @insertValues + ',@Obj_Id'
         end 
       
         if @AlairasDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairasDatuma'
            SET @insertValues = @insertValues + ',@AlairasDatuma'
         end 
       
         if @Leiras is not null
         begin
            SET @insertColumns = @insertColumns + ',Leiras'
            SET @insertValues = @insertValues + ',@Leiras'
         end 
       
         if @AlairoSzerep is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairoSzerep'
            SET @insertValues = @insertValues + ',@AlairoSzerep'
         end 
       
         if @AlairasSorrend is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairasSorrend'
            SET @insertValues = @insertValues + ',@AlairasSorrend'
         end 
       
         if @FelhasznaloCsoport_Id_Alairo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Alairo'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Alairo'
         end 
       
         if @FelhaszCsoport_Id_Helyettesito is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhaszCsoport_Id_Helyettesito'
            SET @insertValues = @insertValues + ',@FelhaszCsoport_Id_Helyettesito'
         end 
       
         if @Azonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Azonosito'
            SET @insertValues = @insertValues + ',@Azonosito'
         end 
       
         if @AlairasMod is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairasMod'
            SET @insertValues = @insertValues + ',@AlairasMod'
         end 
       
         if @AlairasSzabaly_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairasSzabaly_Id'
            SET @insertValues = @insertValues + ',@AlairasSzabaly_Id'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @FelhaszCsop_Id_HelyettesAlairo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhaszCsop_Id_HelyettesAlairo'
            SET @insertValues = @insertValues + ',@FelhaszCsop_Id_HelyettesAlairo'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_IratAlairok ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@PldIratPeldany_Id uniqueidentifier,@Objtip_Id uniqueidentifier,@Obj_Id uniqueidentifier,@AlairasDatuma datetime,@Leiras Nvarchar(4000),@AlairoSzerep nvarchar(64),@AlairasSorrend int,@FelhasznaloCsoport_Id_Alairo uniqueidentifier,@FelhaszCsoport_Id_Helyettesito uniqueidentifier,@Azonosito Nvarchar(100),@AlairasMod nvarchar(64),@AlairasSzabaly_Id uniqueidentifier,@Allapot nvarchar(64),@FelhaszCsop_Id_HelyettesAlairo uniqueidentifier,@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@PldIratPeldany_Id = @PldIratPeldany_Id,@Objtip_Id = @Objtip_Id,@Obj_Id = @Obj_Id,@AlairasDatuma = @AlairasDatuma,@Leiras = @Leiras,@AlairoSzerep = @AlairoSzerep,@AlairasSorrend = @AlairasSorrend,@FelhasznaloCsoport_Id_Alairo = @FelhasznaloCsoport_Id_Alairo,@FelhaszCsoport_Id_Helyettesito = @FelhaszCsoport_Id_Helyettesito,@Azonosito = @Azonosito,@AlairasMod = @AlairasMod,@AlairasSzabaly_Id = @AlairasSzabaly_Id,@Allapot = @Allapot,@FelhaszCsop_Id_HelyettesAlairo = @FelhaszCsop_Id_HelyettesAlairo,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_IratAlairok',@ResultUid
					,'EREC_IratAlairokHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH