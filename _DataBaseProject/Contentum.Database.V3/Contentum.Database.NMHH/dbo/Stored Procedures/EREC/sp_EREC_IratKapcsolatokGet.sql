﻿create procedure [dbo].[sp_EREC_IratKapcsolatokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IratKapcsolatok.Id,
	   EREC_IratKapcsolatok.KapcsolatTipus,
	   EREC_IratKapcsolatok.Leiras,
	   EREC_IratKapcsolatok.Kezi,
	   EREC_IratKapcsolatok.Irat_Irat_Beepul,
	   EREC_IratKapcsolatok.Irat_Irat_Felepul,
	   EREC_IratKapcsolatok.Ver,
	   EREC_IratKapcsolatok.Note,
	   EREC_IratKapcsolatok.Stat_id,
	   EREC_IratKapcsolatok.ErvKezd,
	   EREC_IratKapcsolatok.ErvVege,
	   EREC_IratKapcsolatok.Letrehozo_id,
	   EREC_IratKapcsolatok.LetrehozasIdo,
	   EREC_IratKapcsolatok.Modosito_id,
	   EREC_IratKapcsolatok.ModositasIdo,
	   EREC_IratKapcsolatok.Zarolo_id,
	   EREC_IratKapcsolatok.ZarolasIdo,
	   EREC_IratKapcsolatok.Tranz_id,
	   EREC_IratKapcsolatok.UIAccessLog_id
	   from 
		 EREC_IratKapcsolatok as EREC_IratKapcsolatok 
	   where
		 EREC_IratKapcsolatok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end