﻿create procedure [dbo].[sp_EREC_IratKapcsolatokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IratKapcsolatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_IratKapcsolatok.Id,
	   EREC_IratKapcsolatok.KapcsolatTipus,
	   EREC_IratKapcsolatok.Leiras,
	   EREC_IratKapcsolatok.Kezi,
	   EREC_IratKapcsolatok.Irat_Irat_Beepul,
	   EREC_IratKapcsolatok.Irat_Irat_Felepul,
	   EREC_IratKapcsolatok.Ver,
	   EREC_IratKapcsolatok.Note,
	   EREC_IratKapcsolatok.Stat_id,
	   EREC_IratKapcsolatok.ErvKezd,
	   EREC_IratKapcsolatok.ErvVege,
	   EREC_IratKapcsolatok.Letrehozo_id,
	   EREC_IratKapcsolatok.LetrehozasIdo,
	   EREC_IratKapcsolatok.Modosito_id,
	   EREC_IratKapcsolatok.ModositasIdo,
	   EREC_IratKapcsolatok.Zarolo_id,
	   EREC_IratKapcsolatok.ZarolasIdo,
	   EREC_IratKapcsolatok.Tranz_id,
	   EREC_IratKapcsolatok.UIAccessLog_id  
   from 
     EREC_IratKapcsolatok as EREC_IratKapcsolatok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end