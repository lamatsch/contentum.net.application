﻿create procedure [dbo].[sp_EREC_IratMellekletekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IratMellekletek.Id,
	   EREC_IratMellekletek.IraIrat_Id,
	   EREC_IratMellekletek.KuldMellekletek_Id,
	   EREC_IratMellekletek.AdathordozoTipus,
	   EREC_IratMellekletek.Megjegyzes,
	   EREC_IratMellekletek.SztornirozasDat,
	   EREC_IratMellekletek.MennyisegiEgyseg,
	   EREC_IratMellekletek.Mennyiseg,
	   EREC_IratMellekletek.BarCode,
	   EREC_IratMellekletek.Ver,
	   EREC_IratMellekletek.Note,
	   EREC_IratMellekletek.Stat_id,
	   EREC_IratMellekletek.ErvKezd,
	   EREC_IratMellekletek.ErvVege,
	   EREC_IratMellekletek.Letrehozo_id,
	   EREC_IratMellekletek.LetrehozasIdo,
	   EREC_IratMellekletek.Modosito_id,
	   EREC_IratMellekletek.ModositasIdo,
	   EREC_IratMellekletek.Zarolo_id,
	   EREC_IratMellekletek.ZarolasIdo,
	   EREC_IratMellekletek.Tranz_id,
	   EREC_IratMellekletek.UIAccessLog_id
	   from 
		 EREC_IratMellekletek as EREC_IratMellekletek 
	   where
		 EREC_IratMellekletek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end