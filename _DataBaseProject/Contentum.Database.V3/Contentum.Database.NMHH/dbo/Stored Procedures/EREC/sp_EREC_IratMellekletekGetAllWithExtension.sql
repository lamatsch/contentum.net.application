﻿CREATE procedure [dbo].[sp_EREC_IratMellekletekGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IratMellekletek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_IratMellekletek.Id,
	   EREC_IratMellekletek.IraIrat_Id,
	   EREC_IratMellekletek.AdathordozoTipus,
KRT_KodTarak.Nev as AdathordozoTipusNev,
	   EREC_IratMellekletek.Megjegyzes,
	   EREC_IratMellekletek.SztornirozasDat,
	   EREC_IratMellekletek.MennyisegiEgyseg,
KRT_KodTarak2.Nev as MennyisegiEgysegNev,
	   EREC_IratMellekletek.Mennyiseg,
       EREC_IratMellekletek.BarCode,
	   EREC_IratMellekletek.Ver,
	   EREC_IratMellekletek.Note,
	   EREC_IratMellekletek.Stat_id,
	   EREC_IratMellekletek.ErvKezd,
	   EREC_IratMellekletek.ErvVege,
	   EREC_IratMellekletek.Letrehozo_id,
	   EREC_IratMellekletek.LetrehozasIdo,
	   EREC_IratMellekletek.Modosito_id,
	   EREC_IratMellekletek.ModositasIdo,
	   EREC_IratMellekletek.Zarolo_id,
	   EREC_IratMellekletek.ZarolasIdo,
	   EREC_IratMellekletek.Tranz_id,
	   EREC_IratMellekletek.UIAccessLog_id  
   from 
     EREC_IratMellekletek as EREC_IratMellekletek
	left join KRT_KodCsoportok on KRT_KodCsoportok.Kod=''ADATHORDOZO_TIPUSA''
	left join KRT_KodTarak on KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and EREC_IratMellekletek.AdathordozoTipus = KRT_KodTarak.Kod and KRT_KodTarak.Org=''' + CAST(@Org as NVarChar(40)) + '''
	left join KRT_KodCsoportok as KRT_KodCsoportok2 on KRT_KodCsoportok2.Kod=''MENNYISEGI_EGYSEG''
	left join KRT_KodTarak as KRT_KodTarak2 on KRT_KodCsoportok2.Id = KRT_KodTarak2.KodCsoport_Id and EREC_IratMellekletek.MennyisegiEgyseg = KRT_KodTarak2.Kod and KRT_KodTarak2.Org=''' + CAST(@Org as NVarChar(40)) + '''
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end