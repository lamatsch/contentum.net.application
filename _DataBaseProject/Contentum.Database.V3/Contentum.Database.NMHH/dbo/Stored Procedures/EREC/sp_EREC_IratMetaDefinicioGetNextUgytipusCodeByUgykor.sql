﻿create procedure [dbo].[sp_EREC_IratMetaDefinicioGetNextUgytipusCodeByUgykor]
	@Ugykor_Id uniqueidentifier,
    @ExecutorUserId uniqueidentifier
         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	declare @LastCode nvarchar(64)
	declare @NextCode nvarchar(64)

	declare @CounterPart nvarchar(4)

	select @LastCode = Max(Ugytipus) from EREC_IratMetaDefinicio
    where Ugykor_Id = @Ugykor_Id and Ugytipus is not null
    and PATINDEX('G[0-9][0-9][0-9][0-9]',Ugytipus)  > 0
	and getdate() between ErvKezd and ErvVege

	if @LastCode is null
		set @LastCode = 'G0000'

	set @CounterPart = convert(nvarchar(4), convert(int, substring(@LastCode, 2, 4)) + 1)

	set @NextCode = 'G' + replicate('0', 4 - len(@CounterPart)) + @CounterPart

	select @NextCode

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end