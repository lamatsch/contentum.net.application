﻿create procedure [dbo].[sp_EREC_IratelemKapcsolatokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IratelemKapcsolatok.Id,
	   EREC_IratelemKapcsolatok.Melleklet_Id,
	   EREC_IratelemKapcsolatok.Csatolmany_Id,
	   EREC_IratelemKapcsolatok.Ver,
	   EREC_IratelemKapcsolatok.Note,
	   EREC_IratelemKapcsolatok.Stat_id,
	   EREC_IratelemKapcsolatok.ErvKezd,
	   EREC_IratelemKapcsolatok.ErvVege,
	   EREC_IratelemKapcsolatok.Letrehozo_id,
	   EREC_IratelemKapcsolatok.LetrehozasIdo,
	   EREC_IratelemKapcsolatok.Modosito_id,
	   EREC_IratelemKapcsolatok.ModositasIdo,
	   EREC_IratelemKapcsolatok.Zarolo_id,
	   EREC_IratelemKapcsolatok.ZarolasIdo,
	   EREC_IratelemKapcsolatok.Tranz_id,
	   EREC_IratelemKapcsolatok.UIAccessLog_id
	   from 
		 EREC_IratelemKapcsolatok as EREC_IratelemKapcsolatok 
	   where
		 EREC_IratelemKapcsolatok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end