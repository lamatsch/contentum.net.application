﻿create procedure [dbo].[sp_EREC_IratelemKapcsolatokGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IratelemKapcsolatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     -- A PKI_INTEGRACIO rendszerparameter erteketol fuggoen
-- eltero kodcsoport szerint dolgozunk az elektronikus
-- alairas kapcsan
DECLARE @kcsDokumentumAlairas nvarchar(64)

DECLARE @paramPKI_Integracio nvarchar(400)

SET @paramPKI_Integracio = (
	SELECT Ertek FROM KRT_Parameterek
	WHERE Nev ='PKI_INTEGRACIO'
	AND Org=@Org
)

IF @paramPKI_Integracio = 'Igen'
BEGIN
	SET @kcsDokumentumAlairas = 'DOKUMENTUM_ALAIRAS_PKI'
END
ELSE
BEGIN
	SET @kcsDokumentumAlairas = 'DOKUMENTUM_ALAIRAS_MANUALIS'
END     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_IratelemKapcsolatok.Id,
	   EREC_IratelemKapcsolatok.Melleklet_Id,
	   EREC_IratelemKapcsolatok.Csatolmany_Id,
	   EREC_IratelemKapcsolatok.Ver,
	   EREC_IratelemKapcsolatok.Note,
	   EREC_IratelemKapcsolatok.Stat_id,
	   EREC_IratelemKapcsolatok.ErvKezd,
	   EREC_IratelemKapcsolatok.ErvVege,
	   EREC_IratelemKapcsolatok.Letrehozo_id,
	   EREC_IratelemKapcsolatok.LetrehozasIdo,
	   EREC_IratelemKapcsolatok.Modosito_id,
	   EREC_IratelemKapcsolatok.ModositasIdo,
	   EREC_IratelemKapcsolatok.Zarolo_id,
	   EREC_IratelemKapcsolatok.ZarolasIdo,
	   EREC_IratelemKapcsolatok.Tranz_id,
	   EREC_IratelemKapcsolatok.UIAccessLog_id,	   
	KRT_Dokumentumok.Id as krtDok_Id,
	KRT_Dokumentumok.FajlNev as FajlNev,
	KRT_Dokumentumok.External_Link,	   
	KRT_Dokumentumok.External_Id,
	KRT_Dokumentumok.External_Source,
	KRT_Dokumentumok.External_Info,
	KRT_Dokumentumok.Dokumentum_Id as krtDok_Dokumentum_Id,
	KRT_Dokumentumok.Dokumentum_Id_Kovetkezo as krtDok_Dokumentum_Id_Kovetkezo,
	KRT_Dokumentumok.VerzioJel,
	KRT_Dokumentumok.Megnyithato,
	KRT_Dokumentumok.Olvashato,
	KRT_Dokumentumok.ElektronikusAlairas,
	dbo.fn_KodtarErtekNeve(''' + @kcsDokumentumAlairas +
		''', KRT_Dokumentumok.ElektronikusAlairas,''' + CAST(@Org as NVarChar(40)) + ''') as ElektronikusAlairasNev,
	KRT_Dokumentumok.AlairasFelulvizsgalat,
	KRT_Dokumentumok.Titkositas,
	KRT_Dokumentumok.Tipus,
	KRT_Dokumentumok.Formatum,
	KRT_Dokumentumok.SablonAzonosito,
	KRT_Dokumentumok.BarCode,
	KRT_Dokumentumok.TartalomHash,
	KRT_Dokumentumok.KivonatHash,
	EREC_Csatolmanyok.Leiras,
	 EREC_Mellekletek.Mennyiseg,	   
	 dbo.[fn_KodtarErtekNeve](''ADATHORDOZO_TIPUSA'',AdathordozoTipus,''' + CAST(@Org as NVarChar(40)) + ''') as AdathordozoTipusNev,
	 dbo.[fn_KodtarErtekNeve](''MENNYISEGI_EGYSEG'',MennyisegiEgyseg,''' + CAST(@Org as NVarChar(40)) + ''') as MennyisegiEgysegNev,
	 EREC_Mellekletek.Megjegyzes,
	 EREC_Mellekletek.BarCode as Melleklet_BarCode
   from 
     EREC_IratelemKapcsolatok as EREC_IratelemKapcsolatok        
	 left join EREC_Mellekletek as EREC_Mellekletek
		on EREC_IratelemKapcsolatok.Melleklet_Id = EREC_Mellekletek.Id
	 left join EREC_Csatolmanyok as EREC_Csatolmanyok
		on EREC_IratelemKapcsolatok.Csatolmany_Id = EREC_Csatolmanyok.Id
	 left join KRT_Dokumentumok as KRT_Dokumentumok 
		on EREC_Csatolmanyok.Dokumentum_Id = KRT_Dokumentumok.Id      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
      
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end