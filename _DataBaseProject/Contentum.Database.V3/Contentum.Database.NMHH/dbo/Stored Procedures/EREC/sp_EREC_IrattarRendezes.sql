﻿--set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--if exists (select 1
--            from  sysobjects
--           where  id = object_id('sp_EREC_IrattarRendezes')
--            and   type = 'P')
--   drop procedure sp_EREC_IrattarRendezes
--go

create procedure sp_EREC_IrattarRendezes
        @UgyiratIds						NVARCHAR(MAX)    ,
		@ExecutorUserId				uniqueidentifier,
		@ExecutionTime					datetime ,
		@IrattarId				uniqueidentifier,
		@IrattariHelyErtek     Nvarchar(100)
as

BEGIN TRY


WITH irattarihelyek
AS (select t.id as id, t.szuloid as Szuloid, CONVERT(nvarchar(255),t.NEV) as nev, 1 as level from EREC_IrattariHelyek t
UNION ALL 
select t.id, t.szuloid, CONVERT(nvarchar(255),CONCAT(c.Nev, ' - ' ,t.Nev)) as nev, (c.level + 1) as level from EREC_IrattariHelyek t inner join irattarihelyek c on t.SzuloId = c.id
)

select 
 @IrattariHelyErtek = i.nev
from  
 irattarihelyek i
 inner join (select id, max(level) as level from irattarihelyek group by id) m on i.id = m.id and i.level = m.level

where

 i.id = @IrattarId;


  -- szerelt ügyiratok lekérése
  WITH UgyiratHierarchy  AS
	(
	   -- Base case	   
	   SELECT Id, UgyUgyirat_Id_Szulo, 1 as HierarchyLevel
	   FROM EREC_UgyUgyiratok
	   WHERE Id IN (SELECT convert(uniqueidentifier, Value) from dbo.fn_Split(@UgyiratIds,','))

	   UNION ALL

	   -- Recursive step
	   SELECT u.Id, u.UgyUgyirat_Id_Szulo, uh.HierarchyLevel + 1 AS HierarchyLevel
	   FROM EREC_UgyUgyiratok as u
		  INNER JOIN UgyiratHierarchy as uh ON
			 u.UgyUgyirat_Id_Szulo = uh.Id
	   WHERE (u.Allapot = '60' OR u.TovabbitasAlattAllapot = '60')
	)

	update EREC_UgyUgyiratok
	set EREC_UgyUgyiratok.IrattariHely = @IrattariHelyErtek,
		EREC_UgyUgyiratok.IrattarId = @IrattarId,
		EREC_UgyUgyiratok.ModositasIdo = @ExecutionTime,
		EREC_UgyUgyiratok.Modosito_id = @ExecutorUserId
	where EREC_UgyUgyiratok.Id IN (SELECT Id FROM UgyiratHierarchy)

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InvalidateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH