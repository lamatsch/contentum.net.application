﻿
create procedure [dbo].[sp_EREC_IrattariHelyekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IrattariHelyek.Id,
	   EREC_IrattariHelyek.Ertek,
	   EREC_IrattariHelyek.Nev,
	   EREC_IrattariHelyek.Vonalkod,
	   EREC_IrattariHelyek.SzuloId,
	   EREC_IrattariHelyek.Kapacitas,
	   EREC_IrattariHelyek.Ver,
	   EREC_IrattariHelyek.Note,
	   EREC_IrattariHelyek.Stat_id,
	   EREC_IrattariHelyek.ErvKezd,
	   EREC_IrattariHelyek.ErvVege,
	   EREC_IrattariHelyek.Letrehozo_id,
	   EREC_IrattariHelyek.LetrehozasIdo,
	   EREC_IrattariHelyek.Modosito_id,
	   EREC_IrattariHelyek.ModositasIdo,
	   EREC_IrattariHelyek.Zarolo_id,
	   EREC_IrattariHelyek.ZarolasIdo,
	   EREC_IrattariHelyek.Tranz_id,
	   EREC_IrattariHelyek.UIAccessLog_id,
	   -- CR3246
--	   EREC_IrattariHelyek.IrattarTipus
	   EREC_IrattariHelyek.Felelos_Csoport_Id
	   from 
		 EREC_IrattariHelyek as EREC_IrattariHelyek 
	   where
		 EREC_IrattariHelyek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end