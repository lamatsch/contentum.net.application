﻿
create procedure [dbo].[sp_EREC_IrattariHelyekGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IrattariHelyek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_IrattariHelyek.Id,
	   EREC_IrattariHelyek.Ertek,
	   EREC_IrattariHelyek.Nev,
	   EREC_IrattariHelyek.Vonalkod,
	   EREC_IrattariHelyek.SzuloId,
	   EREC_IrattariHelyek.Kapacitas,
	   EREC_IrattariHelyek.Ver,
	   EREC_IrattariHelyek.Note,
	   EREC_IrattariHelyek.Stat_id,
	   EREC_IrattariHelyek.ErvKezd,
	   EREC_IrattariHelyek.ErvVege,
	   EREC_IrattariHelyek.Letrehozo_id,
	   EREC_IrattariHelyek.LetrehozasIdo,
	   EREC_IrattariHelyek.Modosito_id,
	   EREC_IrattariHelyek.ModositasIdo,
	   EREC_IrattariHelyek.Zarolo_id,
	   EREC_IrattariHelyek.ZarolasIdo,
	   EREC_IrattariHelyek.Tranz_id,
	   EREC_IrattariHelyek.UIAccessLog_id,  
	   -- CR3246
--	   EREC_IrattariHelyek.IrattarTipus
	   EREC_IrattariHelyek.Felelos_Csoport_Id
   from 
     EREC_IrattariHelyek as EREC_IrattariHelyek      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end