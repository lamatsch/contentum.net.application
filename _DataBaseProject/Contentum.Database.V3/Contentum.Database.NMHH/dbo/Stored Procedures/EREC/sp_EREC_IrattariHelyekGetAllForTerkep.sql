﻿CREATE procedure [dbo].[sp_EREC_IrattariHelyekGetAllForTerkep]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IrattariHelyek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
	
	DECLARE @IRATTARIHELY_KAPACITAS nvarchar(400)

	select @IRATTARIHELY_KAPACITAS=Ertek
	from KRT_Parameterek
	where Nev='IRATTARIHELY_KAPACITAS'	
	
	IF (@IRATTARIHELY_KAPACITAS='1')
	SET @sqlcmd = @sqlcmd + 
	';
		with kapacitas as
		(
		select 
			h.id as irattarihely_id, 
			h.SzuloId as irattarihely_szulo_id,
			isnull(h.Kapacitas,0) as irattarihely_kapacitas, 
			sum(isnull(u.IrattarHelyfoglalas,0)) as irattarihely_foglalt 
		from 
			EREC_IrattariHelyek h left join 
			EREC_UgyUgyiratok u on u.IrattarId = h.id 
		where  1=1
			and h.id not in (select SzuloId from EREC_IrattariHelyek where SzuloId is not null)		
		group by 
			h.id,
			h.SzuloId,
			h.Kapacitas
		union all
		select
			h.id as irattarihely_id,
			h.SzuloId as irattarihely_szulo_id,
			isnull(k.irattarihely_kapacitas,0) as irattarihely_kapacitas, 
			isnull(k.irattarihely_foglalt,0) as irattarihely_foglalt
		from
			EREC_IrattariHelyek h inner join
			kapacitas k on k.irattarihely_szulo_id = h.Id
		)
	'

	SET @sqlcmd = @sqlcmd + 
  ' select ' + @LocalTopRow + '
  	   EREC_IrattariHelyek.Id, '
	    IF (@IRATTARIHELY_KAPACITAS='1')
	   SET @sqlcmd = @sqlcmd + 
	   'EREC_IrattariHelyek.Ertek + k.foglaltsag as Ertek,'
	   ELSE
	   SET @sqlcmd = @sqlcmd + 
	   'EREC_IrattariHelyek.Ertek,'
	   SET @sqlcmd = @sqlcmd + 
	   'EREC_IrattariHelyek.Nev,
	   EREC_IrattariHelyek.Vonalkod,
	   EREC_IrattariHelyek.SzuloId,
	   EREC_IrattariHelyek.Kapacitas,
	   EREC_IrattariHelyek.Ver,
	   EREC_IrattariHelyek.Note,
	   EREC_IrattariHelyek.Stat_id,
	   EREC_IrattariHelyek.ErvKezd,
	   EREC_IrattariHelyek.ErvVege,
	   EREC_IrattariHelyek.Letrehozo_id,
	   EREC_IrattariHelyek.LetrehozasIdo,
	   EREC_IrattariHelyek.Modosito_id,
	   EREC_IrattariHelyek.ModositasIdo,
	   EREC_IrattariHelyek.Zarolo_id,
	   EREC_IrattariHelyek.ZarolasIdo,
	   EREC_IrattariHelyek.Tranz_id,
	   EREC_IrattariHelyek.UIAccessLog_id,  
	   -- CR3246
--	   EREC_IrattariHelyek.IrattarTipus
	   EREC_IrattariHelyek.Felelos_Csoport_Id
   from 
     EREC_IrattariHelyek as EREC_IrattariHelyek '
	   
	   IF (@IRATTARIHELY_KAPACITAS='1')
	   SET @sqlcmd = @sqlcmd + 
	 ' left join (select irattarihely_id, '' ('' + convert(nvarchar,sum(irattarihely_kapacitas)-sum(irattarihely_foglalt)) + ''/'' + convert(nvarchar,sum(irattarihely_kapacitas)) + '')'' as foglaltsag from kapacitas group by irattarihely_id) k on EREC_IrattariHelyek.id = k.irattarihely_id 
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
    
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end