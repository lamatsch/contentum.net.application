create procedure [dbo].[sp_EREC_IrattariHelyekPartnerKapcsolatokGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IrattariHelyek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_IrattariHelyek.Id,
	   EREC_IrattariHelyek.Ertek,
	   EREC_IrattariHelyek.Nev,
	   EREC_IrattariHelyek.Vonalkod,
	   EREC_IrattariHelyek.SzuloId,
	   EREC_IrattariHelyek.Felelos_Csoport_Id,
	   EREC_IrattariHelyek.IrattarTipus,
	   EREC_IrattariHelyek.Kapacitas,
	   KRT_PartnerKapcsolatok.Tipus,
	   KRT_PartnerKapcsolatok.Partner_id,
	   KRT_PartnerKapcsolatok.Partner_id_kapcsolt
   from 
     EREC_IrattariHelyek as EREC_IrattariHelyek
	 inner join  KRT_PartnerKapcsolatok as KRT_PartnerKapcsolatok on KRT_PartnerKapcsolatok.Partner_id_kapcsolt = EREC_IrattariHelyek.Felelos_Csoport_Id 
	 where EREC_IrattariHelyek.ErvKezd<=getdate() and EREC_IrattariHelyek.ErvVege>=getdate() and KRT_PartnerKapcsolatok.ErvKezd<=getdate() and KRT_PartnerKapcsolatok.ErvVege>=getdate() 
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

