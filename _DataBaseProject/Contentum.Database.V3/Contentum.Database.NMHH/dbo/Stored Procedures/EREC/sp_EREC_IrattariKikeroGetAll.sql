﻿create procedure [dbo].[sp_EREC_IrattariKikeroGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_IrattariKikero.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_IrattariKikero.Id,
	   EREC_IrattariKikero.Keres_note,
	   EREC_IrattariKikero.FelhasznalasiCel,
	   EREC_IrattariKikero.DokumentumTipus,
	   EREC_IrattariKikero.Indoklas_note,
	   EREC_IrattariKikero.FelhasznaloCsoport_Id_Kikero,
	   EREC_IrattariKikero.KeresDatuma,
	   EREC_IrattariKikero.KulsoAzonositok,
	   EREC_IrattariKikero.UgyUgyirat_Id,
	   EREC_IrattariKikero.Tertiveveny_Id,
	   EREC_IrattariKikero.Obj_Id,
	   EREC_IrattariKikero.ObjTip_Id,
	   EREC_IrattariKikero.KikerKezd,
	   EREC_IrattariKikero.KikerVege,
	   EREC_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy,
	   EREC_IrattariKikero.JovagyasDatuma,
	   EREC_IrattariKikero.FelhasznaloCsoport_Id_Kiado,
	   EREC_IrattariKikero.KiadasDatuma,
	   EREC_IrattariKikero.FelhasznaloCsoport_Id_Visszaad,
	   EREC_IrattariKikero.FelhasznaloCsoport_Id_Visszave,
	   EREC_IrattariKikero.VisszaadasDatuma,
	   EREC_IrattariKikero.SztornirozasDat,
	   EREC_IrattariKikero.Allapot,
	   EREC_IrattariKikero.BarCode,
	   EREC_IrattariKikero.Irattar_Id,
	   EREC_IrattariKikero.Ver,
	   EREC_IrattariKikero.Note,
	   EREC_IrattariKikero.Stat_id,
	   EREC_IrattariKikero.ErvKezd,
	   EREC_IrattariKikero.ErvVege,
	   EREC_IrattariKikero.Letrehozo_id,
	   EREC_IrattariKikero.LetrehozasIdo,
	   EREC_IrattariKikero.Modosito_id,
	   EREC_IrattariKikero.ModositasIdo,
	   EREC_IrattariKikero.Zarolo_id,
	   EREC_IrattariKikero.ZarolasIdo,
	   EREC_IrattariKikero.Tranz_id,
	   EREC_IrattariKikero.UIAccessLog_id  
   from 
     EREC_IrattariKikero as EREC_IrattariKikero      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end