﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_IrattariKikeroHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_EREC_IrattariKikeroHistoryGetAllRecord
go
*/
create procedure sp_EREC_IrattariKikeroHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_IrattariKikeroHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Keres_note' as ColumnName,               cast(Old.Keres_note as nvarchar(99)) as OldValue,
               cast(New.Keres_note as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Keres_note as nvarchar(max)),'') != ISNULL(CAST(New.Keres_note as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznalasiCel' as ColumnName,               cast(Old.FelhasznalasiCel as nvarchar(99)) as OldValue,
               cast(New.FelhasznalasiCel as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznalasiCel as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznalasiCel as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DokumentumTipus' as ColumnName,               cast(Old.DokumentumTipus as nvarchar(99)) as OldValue,
               cast(New.DokumentumTipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.DokumentumTipus as nvarchar(max)),'') != ISNULL(CAST(New.DokumentumTipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Indoklas_note' as ColumnName,               cast(Old.Indoklas_note as nvarchar(99)) as OldValue,
               cast(New.Indoklas_note as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Indoklas_note as nvarchar(max)),'') != ISNULL(CAST(New.Indoklas_note as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Kikero' as ColumnName,               cast(Old.FelhasznaloCsoport_Id_Kikero as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Kikero as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Kikero as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Kikero as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KeresDatuma' as ColumnName,               cast(Old.KeresDatuma as nvarchar(99)) as OldValue,
               cast(New.KeresDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KeresDatuma as nvarchar(max)),'') != ISNULL(CAST(New.KeresDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KulsoAzonositok' as ColumnName,               cast(Old.KulsoAzonositok as nvarchar(99)) as OldValue,
               cast(New.KulsoAzonositok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KulsoAzonositok as nvarchar(max)),'') != ISNULL(CAST(New.KulsoAzonositok as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyUgyirat_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratokAzonosito(Old.UgyUgyirat_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratokAzonosito(New.UgyUgyirat_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyUgyirat_Id as nvarchar(max)),'') != ISNULL(CAST(New.UgyUgyirat_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join EREC_UgyUgyiratok FTOld on FTOld.Id = Old.UgyUgyirat_Id --and FTOld.Ver = Old.Ver
         left join EREC_UgyUgyiratok FTNew on FTNew.Id = New.UgyUgyirat_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tertiveveny_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_KuldTertivevenyekAzonosito(Old.Tertiveveny_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_KuldTertivevenyekAzonosito(New.Tertiveveny_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tertiveveny_Id as nvarchar(max)),'') != ISNULL(CAST(New.Tertiveveny_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join EREC_KuldTertivevenyek FTOld on FTOld.Id = Old.Tertiveveny_Id --and FTOld.Ver = Old.Ver
         left join EREC_KuldTertivevenyek FTNew on FTNew.Id = New.Tertiveveny_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Id' as ColumnName,               cast(Old.Obj_Id as nvarchar(99)) as OldValue,
               cast(New.Obj_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Obj_Id as nvarchar(max)),'') != ISNULL(CAST(New.Obj_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ObjTip_Id' as ColumnName,               cast(Old.ObjTip_Id as nvarchar(99)) as OldValue,
               cast(New.ObjTip_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ObjTip_Id as nvarchar(max)),'') != ISNULL(CAST(New.ObjTip_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KikerKezd' as ColumnName,               cast(Old.KikerKezd as nvarchar(99)) as OldValue,
               cast(New.KikerKezd as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KikerKezd as nvarchar(max)),'') != ISNULL(CAST(New.KikerKezd as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KikerVege' as ColumnName,               cast(Old.KikerVege as nvarchar(99)) as OldValue,
               cast(New.KikerVege as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KikerVege as nvarchar(max)),'') != ISNULL(CAST(New.KikerVege as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Jovahagy' as ColumnName,               cast(Old.FelhasznaloCsoport_Id_Jovahagy as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Jovahagy as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Jovahagy as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Jovahagy as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'JovagyasDatuma' as ColumnName,               cast(Old.JovagyasDatuma as nvarchar(99)) as OldValue,
               cast(New.JovagyasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.JovagyasDatuma as nvarchar(max)),'') != ISNULL(CAST(New.JovagyasDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Kiado' as ColumnName,               cast(Old.FelhasznaloCsoport_Id_Kiado as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Kiado as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Kiado as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Kiado as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KiadasDatuma' as ColumnName,               cast(Old.KiadasDatuma as nvarchar(99)) as OldValue,
               cast(New.KiadasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KiadasDatuma as nvarchar(max)),'') != ISNULL(CAST(New.KiadasDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Visszaad' as ColumnName,               cast(Old.FelhasznaloCsoport_Id_Visszaad as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Visszaad as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Visszaad as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Visszaad as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Visszave' as ColumnName,               cast(Old.FelhasznaloCsoport_Id_Visszave as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Visszave as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Visszave as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Visszave as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VisszaadasDatuma' as ColumnName,               cast(Old.VisszaadasDatuma as nvarchar(99)) as OldValue,
               cast(New.VisszaadasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.VisszaadasDatuma as nvarchar(max)),'') != ISNULL(CAST(New.VisszaadasDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SztornirozasDat' as ColumnName,               cast(Old.SztornirozasDat as nvarchar(99)) as OldValue,
               cast(New.SztornirozasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SztornirozasDat as nvarchar(max)),'') != ISNULL(CAST(New.SztornirozasDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Allapot as nvarchar(max)),'') != ISNULL(CAST(New.Allapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRATKIKERES_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Allapot and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Allapot and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BarCode' as ColumnName,               cast(Old.BarCode as nvarchar(99)) as OldValue,
               cast(New.BarCode as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.BarCode as nvarchar(max)),'') != ISNULL(CAST(New.BarCode as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Irattar_Id' as ColumnName,               cast(Old.Irattar_Id as nvarchar(99)) as OldValue,
               cast(New.Irattar_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariKikeroHistory Old
         inner join EREC_IrattariKikeroHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariKikeroHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Irattar_Id as nvarchar(max)),'') != ISNULL(CAST(New.Irattar_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go