﻿
CREATE procedure [dbo].[sp_EREC_IrattariKikeroUpdate_Tomeges]
        @Ids						NVARCHAR(MAX)    ,
        @Vers						NVARCHAR(MAX)	 ,	
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Keres_note     Nvarchar(4000)  = null,         
             @FelhasznalasiCel     char(1)  = null,         
             @DokumentumTipus     char(1)  = null,         
             @Indoklas_note     Nvarchar(400)  = null,         
             @FelhasznaloCsoport_Id_Kikero     uniqueidentifier  = null,         
             @KeresDatuma     datetime  = null,         
             @KulsoAzonositok     Nvarchar(100)  = null,         
             @UgyUgyirat_Id     uniqueidentifier  = null,         
             @Tertiveveny_Id     uniqueidentifier  = null,         
             @Obj_Id     uniqueidentifier  = null,         
             @ObjTip_Id     uniqueidentifier  = null,         
             @KikerKezd     datetime  = null,         
             @KikerVege     datetime  = null,         
             @FelhasznaloCsoport_Id_Jovahagy     uniqueidentifier  = null,         
             @JovagyasDatuma     datetime  = null,         
             @FelhasznaloCsoport_Id_Kiado     uniqueidentifier  = null,         
             @KiadasDatuma     datetime  = null,         
             @FelhasznaloCsoport_Id_Visszaad     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Visszave     uniqueidentifier  = null,         
             @VisszaadasDatuma     datetime  = null,         
             @SztornirozasDat     datetime  = null,         
             @Allapot     nvarchar(64)  = null,         
             @BarCode     Nvarchar(100)  = null,         
             @Irattar_Id     uniqueidentifier  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

    declare @tempTable table(id uniqueidentifier, ver int);
	
	declare @it	int;
	declare @verPosBegin int;
	declare @verPosEnd int;
	declare @curId	nvarchar(36);
	declare @curVer	nvarchar(6);

	set @it = 0;
	set @verPosBegin = 1;
	while (@it < ((len(@Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Ids,@it*39+2,37);
		set @verPosEnd =  CHARINDEX(',',@Vers,@verPosBegin);
		if @verPosEnd = 0 
			set @verPosEnd = len(@Vers) + 1;
		set @curVer = SUBSTRING(@vers, @verPosBegin, @verPosEnd - @verPosBegin);
		set @verPosBegin = @verPosEnd+1;
		insert into @tempTable(id,ver) values(@curId,convert(int,@curVer) );
		set @it = @it + 1;
	END
	

	declare @rowNumberTotal int;
	declare @rowNumberChecked int;

	select @rowNumberTotal = count(id) from @tempTable;
	
	
	/******************************************
	* EllenĹ‘rzĂ©sek
	******************************************/
	-- verziĂłellenĹ‘rzĂ©s
	select @rowNumberChecked = count(EREC_IrattariKikero.Id)
		from EREC_IrattariKikero
			inner JOIN @tempTable t on t.id = EREC_IrattariKikero.id and t.ver = EREC_IrattariKikero.ver
			
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT EREC_IrattariKikero.Id FROM EREC_IrattariKikero
			inner join @tempTable t on t.id = EREC_IrattariKikero.id and t.ver = EREC_IrattariKikero.ver
		RAISERROR('[50402]',16,1);
	END
	
	-- zĂˇrolĂˇsellenĹ‘rzĂ©s
	select @rowNumberChecked = count(EREC_IrattariKikero.Id)
		from EREC_IrattariKikero
			inner JOIN @tempTable t on t.id = EREC_IrattariKikero.id and ISNULL(EREC_IrattariKikero.Zarolo_id,@ExecutorUserId) = @ExecutorUserId
			
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT EREC_IrattariKikero.Id FROM EREC_IrattariKikero
			inner join @tempTable t on t.id = EREC_IrattariKikero.id and ISNULL(EREC_IrattariKikero.Zarolo_id,@ExecutorUserId) = @ExecutorUserId
		RAISERROR('[50499]',16,1);
	END
	
	-- Ă©rvĂ©nyessĂ©g vĂ©ge figyelĂ©se
	select @rowNumberChecked = count(EREC_IrattariKikero.Id)
		from EREC_IrattariKikero
			inner JOIN @tempTable t on t.id = EREC_IrattariKikero.id and EREC_IrattariKikero.ErvVege > @ExecutionTime
			
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT EREC_IrattariKikero.Id FROM EREC_IrattariKikero
			inner join @tempTable t on t.id = EREC_IrattariKikero.id and EREC_IrattariKikero.ErvVege > @ExecutionTime
		RAISERROR('[50403]',16,1);
	END

/******************************************
* UPDATE string Ă¶sszeĂˇllĂ­tĂˇs
******************************************/
	DECLARE @sqlcmd	NVARCHAR(MAX);
	SET @sqlcmd = N'UPDATE EREC_IrattariKikero SET';

   IF @UpdatedColumns.exist('/root/Keres_note')=1
	     SET @sqlcmd = @sqlcmd + N' Keres_note = @Keres_note,'
   IF @UpdatedColumns.exist('/root/FelhasznalasiCel')=1
         SET @sqlcmd = @sqlcmd + N' FelhasznalasiCel = @FelhasznalasiCel,'
   IF @UpdatedColumns.exist('/root/DokumentumTipus')=1
         SET @sqlcmd = @sqlcmd + N' DokumentumTipus = @DokumentumTipus,'
   IF @UpdatedColumns.exist('/root/Indoklas_note')=1
		 SET @sqlcmd = @sqlcmd + N' Indoklas_note = @Indoklas_note,'
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Kikero')=1
	    SET @sqlcmd = @sqlcmd + N' FelhasznaloCsoport_Id_Kikero = @FelhasznaloCsoport_Id_Kikero,'
   IF @UpdatedColumns.exist('/root/KeresDatuma')=1
	    SET @sqlcmd = @sqlcmd + N' KeresDatuma = @KeresDatuma,'
   IF @UpdatedColumns.exist('/root/KulsoAzonositok')=1
	   SET @sqlcmd = @sqlcmd + N' KulsoAzonositok = @KulsoAzonositok,'
   IF @UpdatedColumns.exist('/root/UgyUgyirat_Id')=1
	     SET @sqlcmd = @sqlcmd + N' UgyUgyirat_Id = @UgyUgyirat_Id,'
   IF @UpdatedColumns.exist('/root/Tertiveveny_Id')=1
         SET @sqlcmd = @sqlcmd + N' Tertiveveny_Id = @Tertiveveny_Id,'
   IF @UpdatedColumns.exist('/root/Obj_Id')=1
        SET @sqlcmd = @sqlcmd + N' Obj_Id = @Obj_Id,'
   IF @UpdatedColumns.exist('/root/ObjTip_Id')=1
         SET @sqlcmd = @sqlcmd + N' ObjTip_Id = @ObjTip_Id,'
   IF @UpdatedColumns.exist('/root/KikerKezd')=1
   SET @sqlcmd = @sqlcmd + N' Foszam = @Foszam,'
         SET @KikerKezd = @KikerKezd
   IF @UpdatedColumns.exist('/root/KikerVege')=1
         SET @sqlcmd = @sqlcmd + N' KikerVege = @KikerVege,'
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Jovahagy')=1
         SET @sqlcmd = @sqlcmd + N' FelhasznaloCsoport_Id_Jovahagy = @FelhasznaloCsoport_Id_Jovahagy,'
   IF @UpdatedColumns.exist('/root/JovagyasDatuma')=1
         SET @sqlcmd = @sqlcmd + N' JovagyasDatuma = @JovagyasDatuma,'
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Kiado')=1
		 SET @sqlcmd = @sqlcmd + N' FelhasznaloCsoport_Id_Kiado = @FelhasznaloCsoport_Id_Kiado,'
   IF @UpdatedColumns.exist('/root/KiadasDatuma')=1
		SET @sqlcmd = @sqlcmd + N' KiadasDatuma = @KiadasDatuma,'
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Visszaad')=1
         SET @sqlcmd = @sqlcmd + N'@FelhasznaloCsoport_Id_Visszaad = @FelhasznaloCsoport_Id_Visszaad,'
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Visszave')=1
         SET @sqlcmd = @sqlcmd + N' FelhasznaloCsoport_Id_Visszave = @FelhasznaloCsoport_Id_Visszave,'
   IF @UpdatedColumns.exist('/root/VisszaadasDatuma')=1
	    SET @sqlcmd = @sqlcmd + N' VisszaadasDatuma = @VisszaadasDatuma,'
   IF @UpdatedColumns.exist('/root/SztornirozasDat')=1
        SET @sqlcmd = @sqlcmd + N' SztornirozasDat = @SztornirozasDat,'
   IF @UpdatedColumns.exist('/root/Allapot')=1
        SET @sqlcmd = @sqlcmd + N' Allapot = @Allapot,'
   IF @UpdatedColumns.exist('/root/BarCode')=1
        SET @sqlcmd = @sqlcmd + N' BarCode = @BarCode,'
   IF @UpdatedColumns.exist('/root/Irattar_Id')=1
         SET @sqlcmd = @sqlcmd + N' Irattar_Id = @Irattar_Id,'
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @sqlcmd = @sqlcmd + N' Note = @Note,'
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @sqlcmd = @sqlcmd + N' Stat_id = @Stat_id,'
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @sqlcmd = @sqlcmd + N' ErvKezd = @ErvKezd,'
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @sqlcmd = @sqlcmd + N' ErvVege = @ErvVege,'
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @sqlcmd = @sqlcmd + N' Letrehozo_id = @Letrehozo_id,'
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @sqlcmd = @sqlcmd + N' LetrehozasIdo = @LetrehozasIdo,'
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @sqlcmd = @sqlcmd + N' Modosito_id = @Modosito_id,'
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @sqlcmd = @sqlcmd + N' ModositasIdo = @ModositasIdo,'
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @sqlcmd = @sqlcmd + N' Zarolo_id = @Zarolo_id,'
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @sqlcmd = @sqlcmd + N' ZarolasIdo = @ZarolasIdo,'
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @sqlcmd = @sqlcmd + N' Tranz_id = @Tranz_id,'
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @sqlcmd = @sqlcmd + N' UIAccessLog_id = @UIAccessLog_id,'   
   SET @sqlcmd = @sqlcmd + N' Ver = Ver + 1'
   
   SET @sqlcmd = @sqlcmd + N'
   WHERE Id IN (' + @Ids + ');'
   
   EXECUTE sp_executesql @sqlcmd, N'
   @Keres_note     Nvarchar(4000)  = null,         
             @FelhasznalasiCel     char(1)  = null,         
             @DokumentumTipus     char(1)  = null,         
             @Indoklas_note     Nvarchar(400)  = null,         
             @FelhasznaloCsoport_Id_Kikero     uniqueidentifier  = null,         
             @KeresDatuma     datetime  = null,         
             @KulsoAzonositok     Nvarchar(100)  = null,         
             @UgyUgyirat_Id     uniqueidentifier  = null,         
             @Tertiveveny_Id     uniqueidentifier  = null,         
             @Obj_Id     uniqueidentifier  = null,         
             @ObjTip_Id     uniqueidentifier  = null,         
             @KikerKezd     datetime  = null,         
             @KikerVege     datetime  = null,         
             @FelhasznaloCsoport_Id_Jovahagy     uniqueidentifier  = null,         
             @JovagyasDatuma     datetime  = null,         
             @FelhasznaloCsoport_Id_Kiado     uniqueidentifier  = null,         
             @KiadasDatuma     datetime  = null,         
             @FelhasznaloCsoport_Id_Visszaad     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Visszave     uniqueidentifier  = null,         
             @VisszaadasDatuma     datetime  = null,         
             @SztornirozasDat     datetime  = null,         
             @Allapot     nvarchar(64)  = null,         
             @BarCode     Nvarchar(100)  = null,         
             @Irattar_Id     uniqueidentifier  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null',
             @Keres_note = @Keres_note,
			 @FelhasznalasiCel = @FelhasznalasiCel,
			 @DokumentumTipus = @DokumentumTipus,
			 @Indoklas_note = @Indoklas_note,
			 @FelhasznaloCsoport_Id_Kikero = @FelhasznaloCsoport_Id_Kikero,
			 @KeresDatuma = @KeresDatuma,
			 @KulsoAzonositok = @KulsoAzonositok,
			 @UgyUgyirat_Id = @UgyUgyirat_Id,
			 @Tertiveveny_Id = @Tertiveveny_Id,
			 @Obj_Id = @Obj_Id,
			 @ObjTip_Id = @ObjTip_Id,
			 @KikerKezd = @KikerKezd,
			 @KikerVege = @KikerVege,
			 @FelhasznaloCsoport_Id_Jovahagy = @FelhasznaloCsoport_Id_Jovahagy,
			 @JovagyasDatuma = @JovagyasDatuma,
			 @FelhasznaloCsoport_Id_Kiado = @FelhasznaloCsoport_Id_Kiado,
			 @KiadasDatuma = @KiadasDatuma,
			 @FelhasznaloCsoport_Id_Visszaad = @FelhasznaloCsoport_Id_Visszaad,
			 @FelhasznaloCsoport_Id_Visszave = @FelhasznaloCsoport_Id_Visszave,
			 @VisszaadasDatuma = @VisszaadasDatuma,
			 @SztornirozasDat = @SztornirozasDat,
			 @Allapot = @Allapot,
			 @BarCode = @BarCode,
			 @Irattar_Id = @Irattar_Id,
			 @Ver = @Ver,
			 @Note = @Note,
			 @Stat_id = @Stat_id,
			 @ErvKezd = @ErvKezd,
			 @ErvVege = @ErvVege,
			 @Letrehozo_id = @Letrehozo_id,
			 @LetrehozasIdo = @LetrehozasIdo,
			 @Modosito_id = @Modosito_id,
			 @ModositasIdo = @ModositasIdo,
			 @Zarolo_id = @Zarolo_id,
			 @ZarolasIdo = @ZarolasIdo,
			 @Tranz_id = @Tranz_id,
			 @UIAccessLog_id = @UIAccessLog_id

if @@rowcount != @rowNumberTotal
begin
	RAISERROR('[50401]',16,1)
END
else
begin   /* History Log */
   CREATE TABLE #ResultUid(id UNIQUEIDENTIFIER)
   INSERT INTO [#ResultUid](id) SELECT id FROM @tempTable
   exec sp_LogEREC_IrattariKikeroHistory_Tomeges 1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH