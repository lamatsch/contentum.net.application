﻿create procedure [dbo].[sp_EREC_IrattariTetel_IktatokonyvGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_IrattariTetel_Iktatokonyv.Id,
	   EREC_IrattariTetel_Iktatokonyv.IrattariTetel_Id,
	   EREC_IrattariTetel_Iktatokonyv.Iktatokonyv_Id,
	   EREC_IrattariTetel_Iktatokonyv.Ver,
	   EREC_IrattariTetel_Iktatokonyv.Note,
	   EREC_IrattariTetel_Iktatokonyv.Stat_id,
	   EREC_IrattariTetel_Iktatokonyv.ErvKezd,
	   EREC_IrattariTetel_Iktatokonyv.ErvVege,
	   EREC_IrattariTetel_Iktatokonyv.Letrehozo_id,
	   EREC_IrattariTetel_Iktatokonyv.LetrehozasIdo,
	   EREC_IrattariTetel_Iktatokonyv.Modosito_id,
	   EREC_IrattariTetel_Iktatokonyv.ModositasIdo,
	   EREC_IrattariTetel_Iktatokonyv.Zarolo_id,
	   EREC_IrattariTetel_Iktatokonyv.ZarolasIdo,
	   EREC_IrattariTetel_Iktatokonyv.Tranz_id,
	   EREC_IrattariTetel_Iktatokonyv.UIAccessLog_id
	   from 
		 EREC_IrattariTetel_Iktatokonyv as EREC_IrattariTetel_Iktatokonyv 
	   where
		 EREC_IrattariTetel_Iktatokonyv.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end