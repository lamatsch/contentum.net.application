﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_IrattariTetel_IktatokonyvHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_EREC_IrattariTetel_IktatokonyvHistoryGetAllRecord
go
*/
create procedure sp_EREC_IrattariTetel_IktatokonyvHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_IrattariTetel_IktatokonyvHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariTetel_IktatokonyvHistory Old
         inner join EREC_IrattariTetel_IktatokonyvHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariTetel_IktatokonyvHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattariTetel_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_IraIrattariTetelekAzonosito(Old.IrattariTetel_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_IraIrattariTetelekAzonosito(New.IrattariTetel_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariTetel_IktatokonyvHistory Old
         inner join EREC_IrattariTetel_IktatokonyvHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariTetel_IktatokonyvHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IrattariTetel_Id as nvarchar(max)),'') != ISNULL(CAST(New.IrattariTetel_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join EREC_IraIrattariTetelek FTOld on FTOld.Id = Old.IrattariTetel_Id --and FTOld.Ver = Old.Ver
         left join EREC_IraIrattariTetelek FTNew on FTNew.Id = New.IrattariTetel_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Iktatokonyv_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_IraIktatoKonyvekAzonosito(Old.Iktatokonyv_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_IraIktatoKonyvekAzonosito(New.Iktatokonyv_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_IrattariTetel_IktatokonyvHistory Old
         inner join EREC_IrattariTetel_IktatokonyvHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_IrattariTetel_IktatokonyvHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Iktatokonyv_Id as nvarchar(max)),'') != ISNULL(CAST(New.Iktatokonyv_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join EREC_IraIktatoKonyvek FTOld on FTOld.Id = Old.Iktatokonyv_Id --and FTOld.Ver = Old.Ver
         left join EREC_IraIktatoKonyvek FTNew on FTNew.Id = New.Iktatokonyv_Id --and FTNew.Ver = New.Ver      
)
            
end
go