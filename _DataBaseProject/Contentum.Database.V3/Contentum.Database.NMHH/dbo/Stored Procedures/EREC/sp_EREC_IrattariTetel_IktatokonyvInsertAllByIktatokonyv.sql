﻿create procedure [dbo].[sp_EREC_IrattariTetel_IktatokonyvInsertAllByIktatokonyv]    
                @Iktatokonyv_Id     uniqueidentifier,
                @ExecutorUserId     uniqueidentifier,
	            @ExecutionTime     datetime
         
AS

BEGIN TRY
 
set nocount on;

CREATE TABLE #ResultUid(Id UNIQUEIDENTIFIER)

INSERT INTO EREC_IrattariTetel_Iktatokonyv
(
 [IrattariTetel_Id],
 [Iktatokonyv_Id],
 [Letrehozo_id],
 [LetrehozasIdo]
 ) output inserted.id into #ResultUid
 SELECT [EREC_IraIrattariTetelek].[Id], @Iktatokonyv_Id ,@ExecutorUserId, @ExecutionTime
 FROM [EREC_IraIrattariTetelek]
 WHERE GETDATE() BETWEEN [EREC_IraIrattariTetelek].[ErvKezd] AND [EREC_IraIrattariTetelek].[ErvVege]
 AND [EREC_IraIrattariTetelek].[Id] NOT IN
 (
	SELECT [EREC_IrattariTetel_Iktatokonyv].[IrattariTetel_Id] FROM EREC_IrattariTetel_Iktatokonyv WHERE [EREC_IrattariTetel_Iktatokonyv].[Iktatokonyv_Id] = @Iktatokonyv_Id
	AND GETDATE() BETWEEN [EREC_IrattariTetel_Iktatokonyv].[ErvKezd] AND [EREC_IrattariTetel_Iktatokonyv].[ErvVege]
 )
 
 if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
	DECLARE @ResultUid UNIQUEIDENTIFIER

	DECLARE cur CURSOR FAST_FORWARD READ_ONLY FOR
	SELECT Id FROM #ResultUid

	OPEN cur

	FETCH NEXT FROM cur INTO @ResultUid

	WHILE @@FETCH_STATUS = 0
	BEGIN

	/* History Log */
    exec sp_LogRecordToHistory 'EREC_IrattariTetel_Iktatokonyv',@ResultUid
					,'EREC_IrattariTetel_IktatokonyvHistory',0,@ExecutorUserId,@ExecutionTime

	FETCH NEXT FROM cur INTO @ResultUid

	END

	CLOSE cur
	DEALLOCATE cur
  
END

SELECT * FROM #ResultUid

DROP TABLE #ResultUid   

   
END TRY
BEGIN CATCH
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH