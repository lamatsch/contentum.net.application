CREATE procedure [dbo].[sp_EREC_KimenoKuldemenyekOsszesitoje_SSRS_Kulonszolgaltatasok]
  @Where nvarchar(MAX) = '',
  @TopRow NVARCHAR(5) = '',
  @KezdDat NVARCHAR(23),
  @VegeDat NVARCHAR(23),
  @Iktatokonyv_Id				uniqueidentifier,
  @ExecutorUserId				uniqueidentifier
as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)                  

   SET @sqlcmd = 
   '
select 
KRT_KodTarak.Egyeb,
(CASE (CASE KRT_Kodtarak.Egyeb
			--WHEN ''Aj�nlott''	THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where Ajanlott=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			WHEN ''T�rtivev�ny'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where Tertiveveny=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			WHEN ''Saj�t k�zbe'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where SajatKezbe=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			WHEN ''E-�rtes�t�s'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where E_ertesites=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			WHEN ''E-elorejelz�s'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where E_elorejelzes=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			WHEN ''Postai lez�r� szolg�lat'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where PostaiLezaroSzolgalat=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			ELSE ''0''
		END)
WHEN (CASE KRT_Kodtarak.Egyeb
			--WHEN ''Aj�nlott''	THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where Ajanlott=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			WHEN ''T�rtivev�ny'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where Tertiveveny=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			WHEN ''Saj�t k�zbe'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where SajatKezbe=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			WHEN ''E-�rtes�t�s'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where E_ertesites=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			WHEN ''E-elorejelz�s'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where E_elorejelzes=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			WHEN ''Postai lez�r� szolg�lat'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where PostaiLezaroSzolgalat=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			ELSE ''0''
		END)
THEN (CASE KRT_Kodtarak.Egyeb
			--WHEN ''Aj�nlott''	THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where Ajanlott=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			WHEN ''T�rtivev�ny'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where Tertiveveny=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			WHEN ''Saj�t k�zbe'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where SajatKezbe=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			WHEN ''E-�rtes�t�s'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where E_ertesites=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			WHEN ''E-elorejelz�s'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where E_elorejelzes=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			WHEN ''Postai lez�r� szolg�lat'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where PostaiLezaroSzolgalat=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
			ELSE ''0''
		END)
ELSE ''0''
END) as db,
KRT_KodTarak.Nev as ft,
(CONVERT(bigint, (CASE (CASE KRT_Kodtarak.Egyeb
							--WHEN ''Aj�nlott''	THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where Ajanlott=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							WHEN ''T�rtivev�ny'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where Tertiveveny=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							WHEN ''Saj�t k�zbe'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where SajatKezbe=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							WHEN ''E-�rtes�t�s'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where E_ertesites=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							WHEN ''E-elorejelz�s'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where E_elorejelzes=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							WHEN ''Postai lez�r� szolg�lat'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where PostaiLezaroSzolgalat=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							ELSE ''0''
						 END)
					WHEN (CASE KRT_Kodtarak.Egyeb
							--WHEN ''Aj�nlott''	THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where Ajanlott=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							WHEN ''T�rtivev�ny'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where Tertiveveny=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							WHEN ''Saj�t k�zbe'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where SajatKezbe=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							WHEN ''E-�rtes�t�s'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where E_ertesites=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							WHEN ''E-elorejelz�s'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where E_elorejelzes=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							WHEN ''Postai lez�r� szolg�lat'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where PostaiLezaroSzolgalat=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							ELSE ''0''
					 	 END)
					THEN (CASE KRT_Kodtarak.Egyeb
							--WHEN ''Aj�nlott''	THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where Ajanlott=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							WHEN ''T�rtivev�ny'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where Tertiveveny=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							WHEN ''Saj�t k�zbe'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where SajatKezbe=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							WHEN ''E-�rtes�t�s'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where E_ertesites=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							WHEN ''E-elorejelz�s'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where E_elorejelzes=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							WHEN ''Postai lez�r� szolg�lat'' THEN (select SUM(Peldanyszam) from EREC_KuldKuldemenyek where PostaiLezaroSzolgalat=''1'' and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + ''' and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + ''' and ' + @Where + ')
							ELSE ''0''
						 END)
					ELSE ''0''
				END))
 * CONVERT(bigint, KRT_KodTarak.Nev)) as ossz
from 
KRT_KodCsoportok as KRT_Kodcsoportok
left join KRT_KodTarak as KRT_KodTarak on KRT_Kodcsoportok.Id = KRT_KodTarak.KodCsoport_Id and KRT_KodTarak.Org=''' + CAST(@Org as NVarChar(40)) + '''
where KRT_Kodcsoportok.Kod=''KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS'' and KRT_KodTarak.Kod in (''02'',''03'',''04'',''05'',''06'')
order by KRT_KodTarak.Kod '

exec (@sqlcmd);   

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

