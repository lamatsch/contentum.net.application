CREATE procedure [dbo].[sp_EREC_KimenoKuldemenyekOsszesitoje_SSRS_NemElsobbsegi]
  @Where nvarchar(MAX) = '',
  @TopRow NVARCHAR(5) = '',
  @KezdDat NVARCHAR(23),
  @VegeDat NVARCHAR(23),
  @Iktatokonyv_Id				uniqueidentifier,
  @ExecutorUserId				uniqueidentifier
as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)                  

  SET @sqlcmd = 
'select 
KRT_KodTarak.Nev,
(select IsNull(SUM(Peldanyszam),0) 
	from EREC_KuldKuldemenyek 
	where EREC_KuldKuldemenyek.KimenoKuldemenyFajta = CAST(KRT_KodTarak.Kod as varchar ) COLLATE Hungarian_CI_AS
		and EREC_KuldKuldemenyek.Elsobbsegi = ''0'' and EREC_KuldKuldemenyek.Ajanlott = ''0''
		and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + '''
		and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + '''
		and ' + @Where + ') as db,
KRT_KodTarakErtek.Nev as ft,
IsNull((select SUM(CONVERT(bigint,Peldanyszam)) 
	from EREC_KuldKuldemenyek 
	where EREC_KuldKuldemenyek.KimenoKuldemenyFajta = CAST(KRT_KodTarak.Kod as varchar ) COLLATE Hungarian_CI_AS
		and EREC_KuldKuldemenyek.Elsobbsegi = ''0'' and EREC_KuldKuldemenyek.Ajanlott = ''0''
		and EREC_KuldKuldemenyek.BelyegzoDatuma between ''' + @KezdDat + ''' and ''' + @VegeDat + '''
		and EREC_KuldKuldemenyek.IraIktatokonyv_Id = ''' + convert(nvarchar(50), @Iktatokonyv_Id ) + '''
		and ' + @Where + ') * CONVERT(bigint, KRT_KodTarakErtek.Nev),0) as ossz
from 
KRT_KodCsoportok as KRT_Kodcsoportok
left join KRT_KodTarak as KRT_KodTarak on KRT_Kodcsoportok.Id = KRT_KodTarak.KodCsoport_Id and KRT_KodTarak.Org=''' + CAST(@Org as NVarChar(40)) + '''
left join KRT_Kodcsoportok as KRT_KodcsoportokErtek on KRT_KodcsoportokErtek.Kod=''KIMENO_KULDEMENY_AR_NEM_ELSOBBSEGI''
left join KRT_KodTarak as KRT_KodTarakErtek on KRT_KodcsoportokErtek.Id = KRT_KodTarakErtek.KodCsoport_Id and KRT_KodTarakErtek.kod = KRT_KodTarak.kod and KRT_KodTarakErtek.Org=''' + CAST(@Org as NVarChar(40)) + '''
where KRT_Kodcsoportok.Kod=''KIMENO_KULDEMENY_FAJTA'' and KRT_KodTarak.Kod in (''01'',''02'',''03'',''04'',''05'',''06'',''07'',''18'')
order by KRT_KodTarak.Kod
	   '

exec (@sqlcmd);   

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

