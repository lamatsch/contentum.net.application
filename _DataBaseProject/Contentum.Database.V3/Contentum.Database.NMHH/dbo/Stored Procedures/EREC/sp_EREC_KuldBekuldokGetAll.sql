﻿create procedure [dbo].[sp_EREC_KuldBekuldokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_KuldBekuldok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_KuldBekuldok.Id,
	   EREC_KuldBekuldok.KuldKuldemeny_Id,
	   EREC_KuldBekuldok.Partner_Id_Bekuldo,
	   EREC_KuldBekuldok.PartnerCim_Id_Bekuldo,
	   EREC_KuldBekuldok.NevSTR,
	   EREC_KuldBekuldok.CimSTR,
	   EREC_KuldBekuldok.Ver,
	   EREC_KuldBekuldok.Note,
	   EREC_KuldBekuldok.Stat_id,
	   EREC_KuldBekuldok.ErvKezd,
	   EREC_KuldBekuldok.ErvVege,
	   EREC_KuldBekuldok.Letrehozo_id,
	   EREC_KuldBekuldok.LetrehozasIdo,
	   EREC_KuldBekuldok.Modosito_id,
	   EREC_KuldBekuldok.ModositasIdo,
	   EREC_KuldBekuldok.Zarolo_id,
	   EREC_KuldBekuldok.ZarolasIdo,
	   EREC_KuldBekuldok.Tranz_id,
	   EREC_KuldBekuldok.UIAccessLog_id  
   from 
     EREC_KuldBekuldok as EREC_KuldBekuldok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end