﻿CREATE procedure [dbo].[sp_EREC_KuldBekuldokGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_KuldBekuldok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
                      

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
EREC_KuldBekuldok.Id,
	   EREC_KuldBekuldok.KuldKuldemeny_Id,
	   EREC_KuldBekuldok.Partner_Id_Bekuldo,
--	KRT_Partnerek.Nev as PartnerNev,	   
--
--		dbo.fn_MergeCim(KRT_Cimek.Tipus, 
--				KRT_Cimek.OrszagNev, 
--				KRT_Cimek.IRSZ,
--				KRT_Cimek.TelepulesNev,
--				KRT_Cimek.KozteruletNev,
--				KRT_Cimek.KozteruletTipusNev,
--				KRT_Cimek.Hazszam,
--				KRT_Cimek.Hazszamig,
--				KRT_Cimek.HazszamBetujel,
--				KRT_Cimek.Lepcsohaz,
--				KRT_Cimek.Szint,
--				KRT_Cimek.Ajto,
--				KRT_Cimek.AjtoBetujel,
--				KRT_Cimek.CimTobbi)
--		as CimNev,

	   EREC_KuldBekuldok.PartnerCim_Id_Bekuldo,
	   EREC_KuldBekuldok.NevSTR,
	   EREC_KuldBekuldok.CimSTR,
	   EREC_KuldBekuldok.Ver,
	   EREC_KuldBekuldok.Note,
	   EREC_KuldBekuldok.Stat_id,
	   EREC_KuldBekuldok.ErvKezd,
	   EREC_KuldBekuldok.ErvVege,
	   EREC_KuldBekuldok.Letrehozo_id,
	   EREC_KuldBekuldok.LetrehozasIdo,
	   EREC_KuldBekuldok.Modosito_id,
	   EREC_KuldBekuldok.ModositasIdo,
	   EREC_KuldBekuldok.Zarolo_id,
	   EREC_KuldBekuldok.ZarolasIdo,
	   EREC_KuldBekuldok.Tranz_id,
	   EREC_KuldBekuldok.UIAccessLog_id  
   from 
     EREC_KuldBekuldok as EREC_KuldBekuldok
--     ,KRT_Partnerek as KRT_Partnerek
--	 ,KRT_Cimek as KRT_Cimek

--where EREC_KuldBekuldok.Partner_Id_Bekuldo = KRT_Partnerek.Id
--and EREC_KuldBekuldok.PartnerCim_Id_Bekuldo = KRT_Cimek.Id
           '
      
    if @Where is not null and @Where!=''
	begin 
--		SET @sqlcmd = @sqlcmd + ' and ' + @Where
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

/*
regi cim merger 2007.08.03
CASE KRT_Cimek.Tipus
	WHEN ''01'' THEN 
		CASE WHEN KRT_Cimek.OrszagNev is NULL OR KRT_Cimek.OrszagNev = '''' THEN '''' ELSE KRT_Cimek.OrszagNev + '' '' END +
		CASE WHEN KRT_Cimek.IRSZ is NULL OR KRT_Cimek.IRSZ = '''' THEN '''' ELSE KRT_Cimek.IRSZ + '' '' END +
		CASE WHEN KRT_Cimek.TelepulesNev is NULL OR KRT_Cimek.TelepulesNev = '''' THEN '''' ELSE KRT_Cimek.TelepulesNev + '' '' END +
		CASE WHEN KRT_Cimek.KozteruletNev is NULL OR KRT_Cimek.KozteruletNev = '''' THEN '''' ELSE KRT_Cimek.KozteruletNev + '' '' END +
		CASE WHEN KRT_Cimek.KozteruletTipusNev is NULL OR KRT_Cimek.KozteruletTipusNev = '''' THEN '''' ELSE KRT_Cimek.KozteruletTipusNev + '' '' END +
		CASE WHEN KRT_Cimek.Hazszam is NULL OR KRT_Cimek.Hazszam = '''' THEN '''' ELSE KRT_Cimek.Hazszam + '' '' END +
		CASE WHEN KRT_Cimek.Hazszamig is NULL OR KRT_Cimek.Hazszamig = '''' THEN '''' ELSE ''-'' + KRT_Cimek.Hazszamig + '' '' END +		
		CASE WHEN KRT_Cimek.HazszamBetujel is NULL OR KRT_Cimek.HazszamBetujel = '''' THEN '''' ELSE KRT_Cimek.HazszamBetujel + '' '' END +
		CASE WHEN KRT_Cimek.Lepcsohaz is NULL OR KRT_Cimek.Lepcsohaz = '''' THEN '''' ELSE KRT_Cimek.Lepcsohaz + '' '' END +
		CASE WHEN KRT_Cimek.Szint is NULL OR KRT_Cimek.Szint = '''' THEN '''' ELSE KRT_Cimek.Szint + '' '' END +
		CASE WHEN KRT_Cimek.Ajto is NULL OR KRT_Cimek.Ajto = '''' THEN '''' ELSE KRT_Cimek.Ajto + '' '' END +
		CASE WHEN KRT_Cimek.AjtoBetujel is NULL OR KRT_Cimek.AjtoBetujel = '''' THEN '''' ELSE KRT_Cimek.AjtoBetujel + '' '' END
ELSE
	KRT_Cimek.CimTobbi*/