﻿create procedure [dbo].[sp_EREC_KuldDokumentumokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_KuldDokumentumok.Id,
	   EREC_KuldDokumentumok.Dokumentum_Id,
	   EREC_KuldDokumentumok.KuldKuldemeny_Id,
	   EREC_KuldDokumentumok.Leiras,
	   EREC_KuldDokumentumok.Lapszam,
	   EREC_KuldDokumentumok.BarCode,
	   EREC_KuldDokumentumok.Forras,
	   EREC_KuldDokumentumok.Formatum,
	   EREC_KuldDokumentumok.Vonalkodozas,
	   EREC_KuldDokumentumok.DokumentumSzerep,
	   EREC_KuldDokumentumok.Ver,
	   EREC_KuldDokumentumok.Note,
	   EREC_KuldDokumentumok.Stat_id,
	   EREC_KuldDokumentumok.ErvKezd,
	   EREC_KuldDokumentumok.ErvVege,
	   EREC_KuldDokumentumok.Letrehozo_id,
	   EREC_KuldDokumentumok.LetrehozasIdo,
	   EREC_KuldDokumentumok.Modosito_id,
	   EREC_KuldDokumentumok.ModositasIdo,
	   EREC_KuldDokumentumok.Zarolo_id,
	   EREC_KuldDokumentumok.ZarolasIdo,
	   EREC_KuldDokumentumok.Tranz_id,
	   EREC_KuldDokumentumok.UIAccessLog_id
	   from 
		 EREC_KuldDokumentumok as EREC_KuldDokumentumok 
	   where
		 EREC_KuldDokumentumok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end