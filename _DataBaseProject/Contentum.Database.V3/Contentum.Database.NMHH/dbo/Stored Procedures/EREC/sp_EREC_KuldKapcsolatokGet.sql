﻿create procedure [dbo].[sp_EREC_KuldKapcsolatokGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_KuldKapcsolatok.Id,
	   EREC_KuldKapcsolatok.KapcsolatTipus,
	   EREC_KuldKapcsolatok.Leiras,
	   EREC_KuldKapcsolatok.Kezi,
	   EREC_KuldKapcsolatok.Kuld_Kuld_Beepul,
	   EREC_KuldKapcsolatok.Kuld_Kuld_Felepul,
	   EREC_KuldKapcsolatok.Ver,
	   EREC_KuldKapcsolatok.Note,
	   EREC_KuldKapcsolatok.Stat_id,
	   EREC_KuldKapcsolatok.ErvKezd,
	   EREC_KuldKapcsolatok.ErvVege,
	   EREC_KuldKapcsolatok.Letrehozo_id,
	   EREC_KuldKapcsolatok.LetrehozasIdo,
	   EREC_KuldKapcsolatok.Modosito_id,
	   EREC_KuldKapcsolatok.ModositasIdo,
	   EREC_KuldKapcsolatok.Zarolo_id,
	   EREC_KuldKapcsolatok.ZarolasIdo,
	   EREC_KuldKapcsolatok.Tranz_id,
	   EREC_KuldKapcsolatok.UIAccessLog_id
	   from 
		 EREC_KuldKapcsolatok as EREC_KuldKapcsolatok 
	   where
		 EREC_KuldKapcsolatok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end