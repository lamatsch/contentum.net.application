
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_KuldKuldemenyekGet')
            and   type = 'P')
   drop procedure sp_EREC_KuldKuldemenyekGet
go
*/


create procedure sp_EREC_KuldKuldemenyekGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(max)

	SET @sqlcmd = 
	  'select 
     	   EREC_KuldKuldemenyek.Id,
	   EREC_KuldKuldemenyek.Allapot,
	   EREC_KuldKuldemenyek.BeerkezesIdeje,
	   EREC_KuldKuldemenyek.FelbontasDatuma,
	   EREC_KuldKuldemenyek.IraIktatokonyv_Id,
	   EREC_KuldKuldemenyek.KuldesMod,
	   EREC_KuldKuldemenyek.Erkezteto_Szam,
	   EREC_KuldKuldemenyek.HivatkozasiSzam,
	   EREC_KuldKuldemenyek.Targy,
	   EREC_KuldKuldemenyek.Tartalom,
	   EREC_KuldKuldemenyek.RagSzam,
	   EREC_KuldKuldemenyek.Surgosseg,
	   EREC_KuldKuldemenyek.BelyegzoDatuma,
	   EREC_KuldKuldemenyek.UgyintezesModja,
	   EREC_KuldKuldemenyek.PostazasIranya,
	   EREC_KuldKuldemenyek.Tovabbito,
	   EREC_KuldKuldemenyek.PeldanySzam,
	   EREC_KuldKuldemenyek.IktatniKell,
	   EREC_KuldKuldemenyek.Iktathato,
	   EREC_KuldKuldemenyek.SztornirozasDat,
	   EREC_KuldKuldemenyek.KuldKuldemeny_Id_Szulo,
	   EREC_KuldKuldemenyek.Erkeztetes_Ev,
	   EREC_KuldKuldemenyek.Csoport_Id_Cimzett,
	   EREC_KuldKuldemenyek.Csoport_Id_Felelos,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Expedial,
	   EREC_KuldKuldemenyek.ExpedialasIdeje,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Atvevo,
	   EREC_KuldKuldemenyek.Partner_Id_Bekuldo,
	   EREC_KuldKuldemenyek.Cim_Id,
	   EREC_KuldKuldemenyek.CimSTR_Bekuldo,
	   EREC_KuldKuldemenyek.NevSTR_Bekuldo,
	   EREC_KuldKuldemenyek.AdathordozoTipusa,
	   EREC_KuldKuldemenyek.ElsodlegesAdathordozoTipusa,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Alairo,
	   EREC_KuldKuldemenyek.BarCode,
	   EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto,
	   EREC_KuldKuldemenyek.CsoportFelelosEloszto_Id,
	   EREC_KuldKuldemenyek.Csoport_Id_Felelos_Elozo,
	   EREC_KuldKuldemenyek.Kovetkezo_Felelos_Id,
	   EREC_KuldKuldemenyek.Elektronikus_Kezbesitesi_Allap,
	   EREC_KuldKuldemenyek.Kovetkezo_Orzo_Id,
	   EREC_KuldKuldemenyek.Fizikai_Kezbesitesi_Allapot,
	   EREC_KuldKuldemenyek.IraIratok_Id,
	   EREC_KuldKuldemenyek.BontasiMegjegyzes,
	   EREC_KuldKuldemenyek.Tipus,
	   EREC_KuldKuldemenyek.Minosites,
	   EREC_KuldKuldemenyek.MegtagadasIndoka,
	   EREC_KuldKuldemenyek.Megtagado_Id,
	   EREC_KuldKuldemenyek.MegtagadasDat,
	   EREC_KuldKuldemenyek.KimenoKuldemenyFajta,
	   EREC_KuldKuldemenyek.Elsobbsegi,
	   EREC_KuldKuldemenyek.Ajanlott,
	   EREC_KuldKuldemenyek.Tertiveveny,
	   EREC_KuldKuldemenyek.SajatKezbe,
	   EREC_KuldKuldemenyek.E_ertesites,
	   EREC_KuldKuldemenyek.E_elorejelzes,
	   EREC_KuldKuldemenyek.PostaiLezaroSzolgalat,
	   EREC_KuldKuldemenyek.Ar,
	   EREC_KuldKuldemenyek.KimenoKuld_Sorszam,'

SET @sqlcmd = @sqlcmd +'
	   EREC_KuldKuldemenyek.Ver,
	   EREC_KuldKuldemenyek.TovabbitasAlattAllapot,
	   EREC_KuldKuldemenyek.Azonosito,
	   EREC_KuldKuldemenyek.BoritoTipus,
	   EREC_KuldKuldemenyek.MegorzesJelzo,
	   EREC_KuldKuldemenyek.Note,
	   EREC_KuldKuldemenyek.Stat_id,
	   EREC_KuldKuldemenyek.ErvKezd,
	   EREC_KuldKuldemenyek.ErvVege,
	   EREC_KuldKuldemenyek.Letrehozo_id,
	   EREC_KuldKuldemenyek.LetrehozasIdo,
	   EREC_KuldKuldemenyek.Modosito_id,
	   EREC_KuldKuldemenyek.ModositasIdo,
	   EREC_KuldKuldemenyek.Zarolo_id,
	   EREC_KuldKuldemenyek.ZarolasIdo,
	   EREC_KuldKuldemenyek.Tranz_id,
	   EREC_KuldKuldemenyek.UIAccessLog_id,'

    set @sqlcmd =  @sqlcmd +'
	   EREC_KuldKuldemenyek.IktatastNemIgenyel,
	   EREC_KuldKuldemenyek.KezbesitesModja,
	   EREC_KuldKuldemenyek.Munkaallomas,
	   EREC_KuldKuldemenyek.SerultKuldemeny,
	   EREC_KuldKuldemenyek.TevesCimzes,
	   EREC_KuldKuldemenyek.TevesErkeztetes,
	   EREC_KuldKuldemenyek.CimzesTipusa,
	   EREC_KuldKuldemenyek.FutarJegyzekListaSzama,
	   EREC_KuldKuldemenyek.KezbesitoAtvetelIdopontja,
	   EREC_KuldKuldemenyek.KezbesitoAtvevoNeve,
	   EREC_KuldKuldemenyek.Minosito,
	   EREC_KuldKuldemenyek.MinositesErvenyessegiIdeje,
	   EREC_KuldKuldemenyek.TerjedelemMennyiseg,
	   EREC_KuldKuldemenyek.TerjedelemMennyisegiEgyseg,
	   EREC_KuldKuldemenyek.TerjedelemMegjegyzes,
	   EREC_KuldKuldemenyek.ModositasErvenyessegKezdete,
	   EREC_KuldKuldemenyek.MegszuntetoHatarozat,
	   EREC_KuldKuldemenyek.FelulvizsgalatDatuma,
	   EREC_KuldKuldemenyek.Felulvizsgalo_id,
	   EREC_KuldKuldemenyek.MinositesFelulvizsgalatEredm,
	   EREC_KuldKuldemenyek.MinositoSzervezet,
	   EREC_KuldKuldemenyek.Partner_Id_BekuldoKapcsolt,
	   EREC_KuldKuldemenyek.MinositesFelulvizsgalatBizTag,
	   EREC_KuldKuldemenyek.Erteknyilvanitas,
	   EREC_KuldKuldemenyek.ErteknyilvanitasOsszege,
	   EREC_KuldKuldemenyek.KulsoAzonosito,
	   EREC_KuldKuldemenyek.IrattarId,
	   EREC_KuldKuldemenyek.IrattariHely
	   from 
		 EREC_KuldKuldemenyek as EREC_KuldKuldemenyek 
	   where
		 EREC_KuldKuldemenyek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
