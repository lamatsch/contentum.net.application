CREATE PROCEDURE [dbo].[sp_EREC_KuldKuldemenyekGetAllWithExtensionForSSRSById2]
  @Where nvarchar(MAX) = '',
  @Where_KuldKuldemenyek_Csatolt nvarchar(MAX) = '',
  @Where_Dosszie  NVARCHAR(MAX) = '',
  @Where_Szamlak NVARCHAR(MAX) = '',
  @OrderBy nvarchar(200) = ' order by EREC_KuldKuldemenyek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
--  @ObjektumTargyszavai_ObjIdFilter nvarchar(MAX) = '', -- SELECT ... FROM EREC_ObjektumTargySzavai ...
  @ExecutorUserId          uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier,
  @Jogosultak     char(1) = '0',
    @pageNumber     int = 0,
  @pageSize       int = -1,
  @SelectedRowId  uniqueidentifier = NULL,
  @AlSzervezetId UNIQUEIDENTIFIER = null,
  @id uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

   DECLARE @Org uniqueidentifier
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
   if (@Org is null)
   begin
      RAISERROR('[50202]',16,1)
   end
   
   DECLARE @OrgKod nvarchar(100)
   set @OrgKod = (select Kod from KRT_Orgok where Id = @Org)
   
   -- CR3442
   --declare @OrgIsBOMPH int
   
   --IF(@OrgKod = 'BOPMH')
	  --set @OrgIsBOMPH = 1
   -- ELSE
   --   set @OrgIsBOMPH = 0
   
   DECLARE @sqlcmd nvarchar(MAX)

   -- blg 7824
   DECLARE @Ugyintez NVARCHAR(MAX)
   declare @FeladoNev varchar(max)
   declare @FeladoIRSZ varchar(10)
   declare @FeladoHelyseg varchar(100)
   declare @FeladoUtca varchar(100)

   SET @Ugyintez = null
   SET @FeladoNev = ''
   SET @FeladoIRSZ = ''
   SET @FeladoHelyseg = ''
   SET @FeladoUtca = ''

   --

   DECLARE @LocalTopRow nvarchar(10)
   declare @firstRow int
   declare @lastRow int

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
      if (@pageSize > @TopRow)
         SET @pageSize = @TopRow;
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

   if (@pageSize > 0 and @pageNumber > -1)
   begin
      set @firstRow = (@pageNumber)*@pageSize + 1
      set @lastRow = @firstRow + @pageSize - 1
   end
   else begin
      if (@TopRow = '' or @TopRow = '0')
      begin
         set @firstRow = 1
         set @lastRow = ISNULL((SELECT TOP (1) Ertek FROM KRT_Parameterek WHERE Nev = 'LISTA_ELEMSZAM_MAX'),1000);
      end 
      else
      begin
         set @firstRow = 1
         set @lastRow = @TopRow
      end   
   END
   
   DECLARE @FelhasznaloId UNIQUEIDENTIFIER
   DECLARE @SzervezetId UNIQUEIDENTIFIER
   
   IF @AlSzervezetId IS NULL
   BEGIN
      SET @FelhasznaloId = @ExecutorUserId
      SET @SzervezetId = @FelhasznaloSzervezet_Id
   END
   ELSE
   BEGIN
      SET @FelhasznaloId = dbo.fn_GetLeader(@AlSzervezetId)
      
      IF @FelhasznaloId is NULL
      BEGIN
         -- Az alszervezetnek nincsen vezetoje
         RAISERROR('[53203]',16,1)
      END

      SET @SzervezetId = @AlSzervezetId
   END
   
   set @sqlcmd = '';
   

   /************************************************************
   * Sz�r�si t�bla �ssze�ll�t�sa                      *
   ************************************************************/
-- DECLARE @ObjTipId uniqueidentifier;
-- select @ObjTipId = Id from KRT_Objtipusok where Kod = 'EREC_KuldKuldemenyek';

   SET @sqlcmd = ';with mappa_tartalmak as
            (
               SELECT KRT_MappaTartalmak.Obj_Id 
               FROM KRT_MappaTartalmak
               INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
               INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id
               INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
               where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
               and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
            )
            select EREC_KuldKuldemenyek.Id into #filter from EREC_KuldKuldemenyek
            left join KRT_Cimek as KRT_Cimek on KRT_Cimek.Id = EREC_KuldKuldemenyek.Cim_Id
            left join EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek on EREC_IraIktatoKonyvek.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
            left join KRT_Partnerek as KRT_PartnerekBekuldok on KRT_PartnerekBekuldok.Id = EREC_KuldKuldemenyek.Partner_Id_Bekuldo
Where @Org=IsNull(EREC_IraIktatoKonyvek.Org, (select Org from KRT_Felhasznalok where EREC_KuldKuldemenyek.Letrehozo_id=KRT_Felhasznalok.Id)) and @id = EREC_KuldKuldemenyek.Id
'

    if @Where is not null and @Where!=''
   begin 
      SET @sqlcmd = @sqlcmd + ' and ' + @Where;
   END

   --IF @Jogosultak = '1' AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
   IF @Jogosultak = '1' AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
   BEGIN
      SET @sqlcmd = @sqlcmd + ' 
      and EREC_KuldKuldemenyek.Id IN
      (
         SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
            INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.Id
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll
               ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany'

     SET @sqlcmd = @sqlcmd + dbo.fn_GetJogosultakFilter(@Org, 'krt_jogosultak')

     SET @sqlcmd = @sqlcmd + '
         UNION ALL
         SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
            INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll
               ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
         UNION ALL
         SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
            INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIratok_Id
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
               ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
         UNION ALL
         SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
               ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.Csoport_Id_Felelos
         UNION ALL
         SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
               ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
         UNION ALL
         SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
             INNER JOIN mappa_tartalmak ON mappa_tartalmak.Obj_Id = EREC_KuldKuldemenyek.Id
         UNION ALL
         '
/* bels� iratokra, elvileg ez is k�ne                 
      SET @sqlcmd = @sqlcmd + 'SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
            INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_KuldKuldemenyek.IraIratok_Id
            WHERE EREC_KuldKuldemenyek.IraIratok_Id IS NOT NULL
            AND EREC_IraIratok.Id IN
            ( 
            SELECT EREC_IraIratok.Id FROM EREC_IraIratok
            WHERE EREC_IraIratok.Ugyirat_Id IN 
               (
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
                        ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
                        ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
                        ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
                  UNION ALL
                  SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@FelhasznaloId,  @SzervezetId, ''EREC_UgyUgyiratok'')
               )
            UNION ALL --CR2407: len�z�nk a p�ld�nyba, ha l�tja az iratot, akkor a k�ldem�nyt is l�ssa
             SELECT EREC_IraIratok.Id FROM EREC_IraIratok
            INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = EREC_IraIratok.Id
            WHERE EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo = @FelhasznaloId and EREC_PldIratpeldanyok.Csoport_Id_Felelos = @FelhasznaloId
            )     
         UNION ALL
         '
*/                            
      SET @sqlcmd = @sqlcmd + 'SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
            INNER JOIN EREC_IraIratok ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id
            WHERE EREC_IraIratok.Id IN
            (
               SELECT EREC_IraIratok.Id FROM EREC_IraIratok
               WHERE EREC_IraIratok.Ugyirat_Id IN 
               (
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany'
       
	   SET @sqlcmd = @sqlcmd + dbo.fn_GetJogosultakFilter(@Org, 'krt_jogosultak')

	   SET @sqlcmd = @sqlcmd + '
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
                        ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
                        ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId,  @SzervezetId) as CsoportTagokAll 
                        ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
                  UNION ALL
                  SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@FelhasznaloId,  @SzervezetId, ''EREC_UgyUgyiratok'')
                  UNION ALL
                  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                     INNER JOIN mappa_tartalmak ON mappa_tartalmak.Obj_Id = erec_ugyugyiratok.Id
               )
               UNION ALL --CR2407: len�z�nk a p�ld�nyba, ha l�tja az iratot, akkor a k�ldem�nyt is l�ssa
               SELECT EREC_IraIratok.Id FROM EREC_IraIratok
               INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = EREC_IraIratok.Id
               WHERE EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo = @FelhasznaloId and EREC_PldIratpeldanyok.Csoport_Id_Felelos = @FelhasznaloId
            )  
         UNION ALL
         SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@FelhasznaloId,  @SzervezetId, ''EREC_KuldKuldemenyek'')
      )'
   END

   IF @Where_Dosszie IS NOT NULL AND @Where_Dosszie != ''
      SET @sqlcmd = @sqlcmd + ' delete from #filter where Id not in (
         SELECT KRT_MappaTartalmak.Obj_Id
         FROM KRT_MappaTartalmak
            INNER JOIN KRT_Mappak on KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
         WHERE GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
            AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
            AND KRT_MappaTartalmak.Obj_type = ''EREC_KuldKuldemenyek''
            AND ' + @Where_Dosszie + '); '

   IF @Where_Szamlak IS NOT NULL AND @Where_Szamlak != ''
      SET @sqlcmd = @sqlcmd + ' delete from #filter where Id not in (
         SELECT EREC_Szamlak.KuldKuldemeny_Id
         FROM EREC_Szamlak
         WHERE GETDATE() BETWEEN EREC_Szamlak.ErvKezd AND EREC_Szamlak.ErvVege
            AND EREC_Szamlak.KuldKuldemeny_Id is not null
            AND ' + @Where_Szamlak + '); '


   IF @Where_KuldKuldemenyek_Csatolt IS NOT NULL AND @Where_KuldKuldemenyek_Csatolt != ''
      SET @sqlcmd = @sqlcmd + ' delete from #filter where Id not in (
         SELECT EREC_KuldKuldemenyek.Id
         FROM EREC_KuldKuldemenyek k
            LEFT JOIN EREC_UgyiratObjKapcsolatok uok on k.Id in (uok.Obj_Id_Elozmeny, uok.Obj_Id_Kapcsolt)
            LEFT JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id in (uok.Obj_Id_Elozmeny, uok.Obj_Id_Kapcsolt)
            and EREC_KuldKuldemenyek.Id <> k.Id
         WHERE getdate() between k.ErvKezd AND k.ErvVege
            AND getdate() between uok.ErvKezd AND uok.ErvVege
            AND getdate() between EREC_KuldKuldemenyek.ErvKezd AND EREC_KuldKuldemenyek.ErvVege
            AND ' + @Where_KuldKuldemenyek_Csatolt + '); '


-- IF @ObjektumTargyszavai_ObjIdFilter is not null and @ObjektumTargyszavai_ObjIdFilter != ''
-- BEGIN
--    SET @sqlcmd = @sqlcmd + '
--;delete from #filter where Id not in (' + @ObjektumTargyszavai_ObjIdFilter + ' AND getdate() BETWEEN EREC_ObjektumTargyszavai.ErvKezd AND EREC_ObjektumTargyszavai.ErvVege)
--'
-- END

            
   /************************************************************
   * Sz�rt adatokhoz rendez�s �s sorsz�m �ssze�ll�t�sa         *
   ************************************************************/
      SET @sqlcmd = @sqlcmd + N'
   select 
      row_number() over('+@OrderBy+') as RowNumber,
      EREC_KuldKuldemenyek.Id into #result
       from EREC_KuldKuldemenyek as EREC_KuldKuldemenyek 
         left join KRT_Cimek as KRT_Cimek on KRT_Cimek.Id = EREC_KuldKuldemenyek.Cim_Id
         left join EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek on EREC_IraIktatoKonyvek.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
         left join KRT_Partnerek as KRT_PartnerekBekuldok on KRT_PartnerekBekuldok.Id = EREC_KuldKuldemenyek.Partner_Id_Bekuldo
         left join KRT_Csoportok as Csoportok_CimzettNev on Csoportok_CimzettNev.Id = EREC_KuldKuldemenyek.Csoport_Id_Cimzett
         left join KRT_Csoportok as Csoportok_FelelosNev on Csoportok_FelelosNev.Id = EREC_KuldKuldemenyek.Csoport_Id_Felelos
         left join KRT_Csoportok as Csoportok_OrzoNev on Csoportok_OrzoNev.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
         left join KRT_Csoportok as Csoportok_ErkeztetoNev on Csoportok_ErkeztetoNev.Id = EREC_KuldKuldemenyek.Letrehozo_id
         left join KRT_Kodcsoportok as AllapotKodCsoport on AllapotKodCsoport.Kod = ''KULDEMENY_ALLAPOT''
            left join KRT_Kodtarak as AllapotKodTarak on AllapotKodTarak.Kodcsoport_Id = AllapotKodCsoport.Id and AllapotKodTarak.Kod = EREC_KuldKuldemenyek.Allapot and AllapotKodTarak.Org=@Org
         left join KRT_Kodcsoportok as IktatasiKotelezettsegKodCsoport on IktatasiKotelezettsegKodCsoport.Kod = ''KULDEMENY_KULDES_MODJA''
            left join KRT_Kodtarak as IktatasiKotelezettsegKodTarak on IktatasiKotelezettsegKodTarak.Kodcsoport_Id = IktatasiKotelezettsegKodCsoport.Id and IktatasiKotelezettsegKodTarak.Kod = EREC_KuldKuldemenyek.IktatniKell
and IktatasiKotelezettsegKodTarak.Org=@Org
      left join EREC_Szamlak on EREC_Szamlak.KuldKuldemeny_Id=EREC_KuldKuldemenyek.Id
      where EREC_KuldKuldemenyek.Id in (select Id from #filter); '

      if (@SelectedRowId is not null)
         set @sqlcmd = @sqlcmd + N'
         if exists (select 1 from #result where Id = @SelectedRowId)
         BEGIN
            select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
            set @firstRow = (@pageNumber - 1) * @pageSize + 1;
            set @lastRow = @pageNumber*@pageSize;
            select @pageNumber = @pageNumber - 1
         END
         ELSE'
      --ELSE
         set @sqlcmd = @sqlcmd + N' 
            if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
            BEGIN
               select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
               set @firstRow = (@pageNumber - 1) * @pageSize + 1;
               set @lastRow = @pageNumber*@pageSize;
               select @pageNumber = @pageNumber - 1
            END ;'

   /************************************************************
   * Bizalmas iratok �rz� szerinti sz�r�s�hez csoporttagok lek�r�se*
   ************************************************************/
   --IF dbo.fn_IsAdmin(@ExecutorUserId) = 0
   IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
   begin
-- set @sqlcmd = @sqlcmd + N'; declare @iratok_executor table (Id uniqueidentifier)
--insert into @iratok_executor select Id from dbo.fn_GetAllIrat_ExecutorIsOrzoOrHisLeader(
--       ''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
--       ''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
--'
   set @sqlcmd = @sqlcmd + N'; declare @iratok_executor table (Id uniqueidentifier primary key)
'
--insert into @iratok_executor select Id from dbo.fn_GetAllConfidentialIrat_ExecutorIsOrzoOrHisLeader(
--       ''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
--       ''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
--'

   set @sqlcmd = @sqlcmd + N'; declare @kuldemenyek_executor table (Id uniqueidentifier primary key)
'
--insert into @kuldemenyek_executor select Id from dbo.fn_GetAllConfidentialKuldemeny_ExecutorIsOrzoOrHisLeader(
--       ''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
--       ''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
--'
   set @sqlcmd = @sqlcmd + N'
   DECLARE @confidential varchar(4000)
   set @confidential = (select top 1 Ertek from KRT_Parameterek where Nev=''IRAT_MINOSITES_BIZALMAS''
                     and Org=@Org and getdate() between ErvKezd and ErvVege)

   declare @Bizalmas table (val nvarchar(64) collate Hungarian_CS_AS )
   insert into @Bizalmas (val) (select Value from fn_Split(@confidential, '',''))

   DECLARE @csoporttagsag_tipus nvarchar(64)
   SET @csoporttagsag_tipus = (select top 1 KRT_CsoportTagok.Tipus from KRT_CsoportTagok
                  where KRT_CsoportTagok.Csoport_Id_Jogalany=@FelhasznaloId
                  and KRT_CsoportTagok.Csoport_Id= @SzervezetId
                  and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
                  )

   ; CREATE TABLE #jogosultak (Id uniqueidentifier)
   IF @csoporttagsag_tipus = ''3''
   BEGIN
      INSERT INTO #jogosultak select KRT_Csoportok.Id from KRT_Csoportok
                     inner join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id =  @SzervezetId 
                     and KRT_Csoportok.Tipus = ''1'' and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
                     where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege 
                     and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
   END
   ELSE
   BEGIN
      INSERT INTO #jogosultak select @FelhasznaloId
   END
'
   set @sqlcmd = @sqlcmd + N'
   insert into @kuldemenyek_executor select #result.Id
   from EREC_KuldKuldemenyek
   INNER JOIN #result on #result.Id=EREC_KuldKuldemenyek.Id and #result.RowNumber between @firstRow and @lastRow
   where 1=case when EREC_KuldKuldemenyek.Minosites in (select val from @Bizalmas)
      AND NOT EXISTS
            (select 1
               from KRT_Jogosultak where
                  KRT_Jogosultak.Obj_Id=#result.Id
                  and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
                  and @FelhasznaloId = KRT_Jogosultak.Csoport_Id_Jogalany)
      AND EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo NOT IN (select Id from #jogosultak)
	  AND EREC_KuldKuldemenyek.Csoport_Id_Felelos NOT IN (select Id from #jogosultak)
   then 0
   else 1 end;
'
   set @sqlcmd = @sqlcmd + N'
   insert into @iratok_executor select EREC_IraIratok.Id
   from EREC_IraIratok
   INNER JOIN #result on #result.Id=EREC_IraIratok.KuldKuldemenyek_Id and #result.RowNumber between @firstRow and @lastRow
   where 1=case when EREC_IraIratok.Minosites in (select val from @Bizalmas)
      AND NOT EXISTS
            (select 1
               from KRT_Jogosultak where
                  KRT_Jogosultak.Obj_Id=EREC_IraIratok.Id
                  and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
                  and @FelhasznaloId = KRT_Jogosultak.Csoport_Id_Jogalany)
      AND NOT EXISTS
         (select 1
            from EREC_PldIratPeldanyok
            where EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id
               and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo in (select Id from #jogosultak)
         )
	AND NOT EXISTS
         (select 1
            from EREC_PldIratPeldanyok
            where EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id
               and EREC_PldIratPeldanyok.Csoport_Id_Felelos in (select Id from #jogosultak)
         )
   then 0
   else 1 end;

   drop table #jogosultak
'
   end
   /************************************************************
   * T�nyleges select                                 *
   ************************************************************/
      set @sqlcmd = @sqlcmd + N'
   select 
      #result.RowNumber,
      #result.Id,
      --EREC_KuldKuldemenyek.Id,
	    (SELECT EREC_Szamlak.SzamlaSorszam FROM EREC_Szamlak JOIN EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_Szamlak.KuldKuldemeny_Id WHERE EREC_KuldKuldemenyek.Id = #result.Id) as SzamlaSorszam ,
      EREC_KuldKuldemenyek.Allapot,
dbo.fn_KodtarErtekNeve(''KULDEMENY_ALLAPOT'', EREC_KuldKuldemenyek.Allapot,@Org) as AllapotNev,
dbo.fn_KodtarErtekNeve(''KULDEMENY_ALLAPOT'', EREC_KuldKuldemenyek.TovabbitasAlattAllapot,@Org) as TovabbitasAlattAllapotNev,
AllapotNev_print = dbo.fn_KodtarErtekNeve(''KULDEMENY_ALLAPOT'', EREC_KuldKuldemenyek.Allapot,@Org) +
CASE EREC_KuldKuldemenyek.Allapot
   WHEN ''04'' THEN  '' ('' +
            (SELECT TOP 1 [EREC_IraIratok].Azonosito
             FROM [EREC_IraIratok] 
             WHERE [EREC_IraIratok].[KuldKuldemenyek_Id] = [EREC_KuldKuldemenyek].[Id]
               and erec_IraIratok.Allapot != ''08'' -- hogy ne a felszabad�tott irat j�jj�n
             ) + '')''
   WHEN ''03'' THEN '' - '' + dbo.fn_KodtarErtekNeve(''KULDEMENY_ALLAPOT'', EREC_KuldKuldemenyek.TovabbitasAlattAllapot,@Org) +
         (CASE EREC_KuldKuldemenyek.TovabbitasAlattAllapot WHEN  ''04''
            THEN '' ('' +
               (SELECT TOP 1 [EREC_IraIratok].Azonosito
                  FROM [EREC_IraIratok] 
                  WHERE [EREC_IraIratok].[KuldKuldemenyek_Id] = [EREC_KuldKuldemenyek].[Id]
                     and erec_IraIratok.Allapot != ''08'' -- hogy ne a felszabad�tott irat j�jj�n
               ) + '')''
            ELSE ''''
         END)
      ELSE ''''
   END,
      EREC_KuldKuldemenyek.TovabbitasAlattAllapot,
      CONVERT(nvarchar(16), EREC_KuldKuldemenyek.BeerkezesIdeje, 120) as BeerkezesIdeje,
       EREC_KuldKuldemenyek.IraIktatokonyv_Id,
      EREC_KuldKuldemenyek.KuldesMod,
dbo.fn_KodtarErtekNeve(''KULDEMENY_KULDES_MODJA'', EREC_KuldKuldemenyek.KuldesMod,@Org) as KuldesModNev,
--KRT_KodTarakKuldesMod.Nev as KuldesModNev,
EREC_IraIktatoKonyvek.MegkulJelzes EREC_IraIktatoKonyvek_MegkulJelzes,
EREC_IraIktatoKonyvek.MegkulJelzes,    
EREC_KuldKuldemenyek.Erkezteto_Szam,
-- BLG_292
--EREC_IraIktatoKonyvek.MegkulJelzes + '' - '' + CAST(Erkezteto_Szam AS NVarchar(100)) + '' / '' + CAST(EREC_IraIktatoKonyvek.Ev AS NVarchar(100)) FullErkeztetoSzam,
EREC_KuldKuldemenyek.Azonosito FullErkeztetoSzam,
EREC_IraIktatoKonyvek.Ev EREC_IraIktatoKonyvek_Ev,
EREC_IraIktatoKonyvek.Ev,
EREC_IraIktatoKonyvek.Nev EREC_IraIktatoKonyvek_Nev,
      EREC_KuldKuldemenyek.HivatkozasiSzam,
      EREC_KuldKuldemenyek.Targy,
      EREC_KuldKuldemenyek.Tartalom,
      EREC_KuldKuldemenyek.RagSzam,
      EREC_KuldKuldemenyek.Surgosseg,
dbo.fn_KodtarErtekNeve(''SURGOSSEG'', EREC_KuldKuldemenyek.Surgosseg,@Org) as SurgossegNev,
      EREC_KuldKuldemenyek.BelyegzoDatuma,
      EREC_KuldKuldemenyek.UgyintezesModja,
      EREC_KuldKuldemenyek.PostazasIranya,
      EREC_KuldKuldemenyek.Tovabbito,
      EREC_KuldKuldemenyek.PeldanySzam,
      EREC_KuldKuldemenyek.IktatniKell,
dbo.fn_KodtarErtekNeve(''IKTATASI_KOTELEZETTSEG'', EREC_KuldKuldemenyek.IktatniKell,@Org) as IktatniKellNev,
      EREC_KuldKuldemenyek.Iktathato,
      EREC_KuldKuldemenyek.SztornirozasDat,
      EREC_KuldKuldemenyek.KuldKuldemeny_Id_Szulo,
      EREC_KuldKuldemenyek.Erkeztetes_Ev,
      EREC_KuldKuldemenyek.Csoport_Id_Cimzett,
dbo.fn_GetCsoportNev(EREC_KuldKuldemenyek.Csoport_Id_Cimzett) as Csoport_Id_CimzettNev,
      EREC_KuldKuldemenyek.Csoport_Id_Felelos,
dbo.fn_GetCsoportNev(EREC_KuldKuldemenyek.Csoport_Id_Felelos) as Csoport_Id_FelelosNev,
      EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo,
dbo.fn_GetCsoportNev(EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo) as FelhasznaloCsoport_Id_OrzoNev,
dbo.fn_GetCsoportKod(EREC_KuldKuldemenyek.Csoport_Id_Felelos) as Csoport_Id_Szervezet,
      EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Atvevo,
      EREC_KuldKuldemenyek.Partner_Id_Bekuldo,
       EREC_KuldKuldemenyek.NevSTR_Bekuldo,
(CASE WHEN EREC_KuldKuldemenyek.NevSTR_Bekuldo is NULL THEN EREC_KuldKuldemenyek.NevSTR_Bekuldo
ELSE EREC_KuldKuldemenyek.NevSTR_Bekuldo END) as Bekuldo_Id_Nev,
      EREC_KuldKuldemenyek.AdathordozoTipusa,
dbo.fn_KodtarErtekNeve(''ADATHORDOZO_TIPUSA'', EREC_KuldKuldemenyek.AdathordozoTipusa,@Org) as AdathordozoTipusa_Nev,
(select count(*) from EREC_Mellekletek where 
EREC_Mellekletek.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id) as MellekletekSzama,
      EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Alairo,   
      EREC_KuldKuldemenyek.BarCode,
      EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto,
      EREC_KuldKuldemenyek.Tertiveveny,
(CASE WHEN ''04'' in (EREC_KuldKuldemenyek.Allapot, EREC_KuldKuldemenyek.TovabbitasAlattAllapot)
      THEN   (SELECT TOP 1 [EREC_IraIratok].Azonosito + ''***'' + convert(NVARCHAR(40), [EREC_IraIratok].[Id])
             FROM [EREC_IraIratok] 
             WHERE [EREC_IraIratok].[KuldKuldemenyek_Id] = [EREC_KuldKuldemenyek].[Id]
               and erec_IraIratok.Allapot not in (''06'', ''08'') -- hogy ne a felszabad�tott vagy �tiktatott irat j�jj�n
             )        
     ELSE ''''
  END) AS IratIktatoszamEsId,'

--BUG_8282
SET @sqlcmd = @sqlcmd + 'LTRIM( RTRIM((SELECT TOP 1 splitdata from (SELECT TOP 1 * 
FROM dbo.fnSplitString(EREC_KuldKuldemenyek.CimSTR_Bekuldo,'','') ORDER BY Id) as Id ORDER BY Id desc))) as CimSTR_Bekuldo_OrszagNev,

LTRIM( RTRIM((SELECT TOP 1 splitdata from (SELECT TOP 2 * 
FROM dbo.fnSplitString(EREC_KuldKuldemenyek.CimSTR_Bekuldo,'','') ORDER BY Id) as Id ORDER BY Id desc))) as CimSTR_Bekuldo_IRSZ,

LTRIM( RTRIM((SELECT TOP 1 splitdata from (SELECT TOP 3 * 
FROM dbo.fnSplitString(EREC_KuldKuldemenyek.CimSTR_Bekuldo,'','') ORDER BY Id) as Id ORDER BY Id desc))) as CimSTR_Bekuldo_TelepulesNev,

LTRIM( RTRIM( (SELECT TOP 1 splitdata from (SELECT TOP 4 * 
FROM dbo.fnSplitString(EREC_KuldKuldemenyek.CimSTR_Bekuldo,'','') ORDER BY Id) as Id ORDER BY Id desc))) as CimSTR_Bekuldo_Cimzes,
'

SET @sqlcmd = @sqlcmd + '
      EREC_KuldKuldemenyek.Cim_Id,
       EREC_KuldKuldemenyek.CimSTR_Bekuldo,

(CASE WHEN EREC_KuldKuldemenyek.CimSTR_Bekuldo = NULL THEN dbo.fn_MergeCim(
   KRT_Cimek.Tipus,
   KRT_Cimek.OrszagNev,
   KRT_Cimek.IRSZ,
   KRT_Cimek.TelepulesNev,
   KRT_Cimek.KozteruletNev,
   KRT_Cimek.KozteruletTipusNev,
   KRT_Cimek.Hazszam,
   KRT_Cimek.Hazszamig,
   KRT_Cimek.HazszamBetujel,
   KRT_Cimek.Lepcsohaz,
   KRT_Cimek.Szint,
   KRT_Cimek.Ajto,
   KRT_Cimek.AjtoBetujel,
   -- BLG_1347
   KRT_Cimek.HRSZ,
   KRT_Cimek.CimTobbi) ELSE EREC_KuldKuldemenyek.CimSTR_Bekuldo END) as Cim_Id_Nev,
      EREC_KuldKuldemenyek.CsoportFelelosEloszto_Id,
      EREC_KuldKuldemenyek.Tipus,
      EREC_KUldKuldemenyek.Minosites,
      EREC_KuldKuldemenyek.MegtagadasIndoka,
      EREC_KuldKuldemenyek.Megtagado_Id,
      EREC_KuldKuldemenyek.MegtagadasDat,
   dbo.fn_GetCsoportNev(EREC_KuldKuldemenyek.Letrehozo_id) as ErkeztetoNev,
'
-- megvizsg�ljuk, hogy az irat bizalmas-e, �s ezesetben a felhaszn�l� az �rz� vagy az �rz� vezet�je-e
--if dbo.fn_IsAdmin(@ExecutorUserId) = 0 --AND @Jogosultak = '1' 
IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0 --AND @Jogosultak = '1'
begin
-- set @sqlcmd = @sqlcmd + '(SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
--and ([EREC_Csatolmanyok].IraIrat_Id is null or
--   dbo.fn_CheckIrat_IsConfidential([EREC_Csatolmanyok].IraIrat_Id) = 0 or
--   [EREC_Csatolmanyok].IraIrat_Id in (select Id from @iratok_executor))
--    AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege) as CsatolmanyCount,
--'

--set @sqlcmd = @sqlcmd + '(SELECT COUNT(*) FROM
--(select Id from [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
--and dbo.fn_CheckKuldemeny_IsConfidential([EREC_Csatolmanyok].KuldKuldemeny_Id) = 0
--and [EREC_Csatolmanyok].IraIrat_Id is null
--AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
--UNION
--select Id from [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
--and [EREC_Csatolmanyok].KuldKuldemeny_Id in (select Id from @kuldemenyek_executor)
--AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
--UNION
--select Id from [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
--and [EREC_Csatolmanyok].IraIrat_Id is not null and
--   (dbo.fn_CheckIrat_IsConfidential([EREC_Csatolmanyok].IraIrat_Id) = 0 or
--   [EREC_Csatolmanyok].IraIrat_Id in (select Id from @iratok_executor))
--AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
--) tmp) as CsatolmanyCount,
--'
--
-- set @sqlcmd = @sqlcmd + 'Dokumentum_Id = case (SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
--and ([EREC_Csatolmanyok].IraIrat_Id is null or
--   dbo.fn_CheckIrat_IsConfidential([EREC_Csatolmanyok].IraIrat_Id) = 0 or
--   [EREC_Csatolmanyok].IraIrat_Id in (select Id from @iratok_executor))
--    AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
--when 1 then
--(SELECT top 1 EREC_Csatolmanyok.Dokumentum_Id FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
--and ([EREC_Csatolmanyok].IraIrat_Id is null or
--   dbo.fn_CheckIrat_IsConfidential([EREC_Csatolmanyok].IraIrat_Id) = 0 or
--   [EREC_Csatolmanyok].IraIrat_Id in (select Id from @iratok_executor))
--    AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
--else null end,
--'

set @sqlcmd = @sqlcmd + '(
(SELECT COUNT(*) FROM [EREC_Csatolmanyok] INNER JOIN @kuldemenyek_executor ke
ON ke.Id=[EREC_Csatolmanyok].KuldKuldemeny_Id and [EREC_Csatolmanyok].IraIrat_Id is null
WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
+
(SELECT COUNT(*) FROM [EREC_Csatolmanyok] INNER JOIN @iratok_executor ie
ON ie.Id=[EREC_Csatolmanyok].IraIrat_Id
WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
) as CsatolmanyCount,
'

   set @sqlcmd = @sqlcmd + 'Dokumentum_Id = 
		CASE
		WHEN  
			(
			(SELECT COUNT(*) FROM [EREC_Csatolmanyok] INNER JOIN @kuldemenyek_executor ke
			ON ke.Id=[EREC_Csatolmanyok].KuldKuldemeny_Id and [EREC_Csatolmanyok].IraIrat_Id is null
			WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
			AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
			+
			(SELECT COUNT(*) FROM [EREC_Csatolmanyok] INNER JOIN @iratok_executor ie
			ON ie.Id=[EREC_Csatolmanyok].IraIrat_Id
			WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
			AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
			) > 0
		THEN  
			(SELECT top 1 EREC_Csatolmanyok.Dokumentum_Id 
			 FROM [EREC_Csatolmanyok] 
			 WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id
				  AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
		ELSE
			CAST(NULL AS UNIQUEIDENTIFIER)  
		END,
	'

end
else
begin
   set @sqlcmd = @sqlcmd + '(SELECT COUNT(*) FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege) as CsatolmanyCount,
'

   set @sqlcmd = @sqlcmd + 'Dokumentum_Id = 
 	CASE 
		WHEN
			(SELECT COUNT(*) 
			 FROM [EREC_Csatolmanyok] 
			 WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id 
					AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
			) > 0 
		THEN
				(SELECT  top 1 EREC_Csatolmanyok.Dokumentum_Id 
				FROM [EREC_Csatolmanyok] WHERE [EREC_Csatolmanyok].KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id AND getdate() 
				BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege)
		ELSE 
			CAST(NULL AS UNIQUEIDENTIFIER)  
		END,
	'

end
   set @sqlcmd = @sqlcmd + 'dbo.fn_GetEREC_HataridosFeladatokCount(EREC_KuldKuldemenyek.Id,@ExecutorUserId, @FelhasznaloSzervezet_Id) as FeladatCount,
       EREC_Szamlak.PIRAllapot,
   dbo.fn_KodtarErtekNeve(''PIR_ALLAPOT'', EREC_Szamlak.PIRAllapot,@Org) as PIRAllapotNev,
      EREC_KuldKuldemenyek.Ver,
      EREC_KuldKuldemenyek.Note,
      EREC_KuldKuldemenyek.Stat_id,
      EREC_KuldKuldemenyek.ErvKezd,
      EREC_KuldKuldemenyek.ErvVege,
      EREC_KuldKuldemenyek.Letrehozo_id,
      CONVERT(nvarchar(19), EREC_KuldKuldemenyek.LetrehozasIdo, 120) as LetrehozasIdo,
      EREC_KuldKuldemenyek.Modosito_id,
      EREC_KuldKuldemenyek.ModositasIdo,
      EREC_KuldKuldemenyek.Zarolo_id,
      EREC_KuldKuldemenyek.ZarolasIdo,
      EREC_KuldKuldemenyek.Tranz_id,
      EREC_KuldKuldemenyek.UIAccessLog_id,
      EREC_KuldKuldemenyek.IktatastNemIgenyel,
      EREC_KuldKuldemenyek.KezbesitesModja,
      EREC_KuldKuldemenyek.Munkaallomas,
      EREC_KuldKuldemenyek.SerultKuldemeny,
      EREC_KuldKuldemenyek.TevesCimzes,
      EREC_KuldKuldemenyek.TevesErkeztetes,
      EREC_KuldKuldemenyek.CimzesTipusa,
	  EREC_KuldKuldemenyek.FutarJegyzekListaSzama,
	     EREC_KuldKuldemenyek.KezbesitoAtvetelIdopontja,
	   EREC_KuldKuldemenyek.KezbesitoAtvevoNeve,
	  EREC_KuldKuldemenyek.Minosito,
	  KRT_PartnerMinosito.Nev as MinositoNev,
	  EREC_KuldKuldemenyek.MinositesErvenyessegiIdeje,
	  EREC_KuldKuldemenyek.TerjedelemMennyiseg,
	  EREC_KuldKuldemenyek.TerjedelemMennyisegiEgyseg,
	  EREC_KuldKuldemenyek.TerjedelemMegjegyzes,
	  EREC_KuldKuldemenyek.ModositasErvenyessegKezdete,
	  EREC_KuldKuldemenyek.MegszuntetoHatarozat,
	  EREC_KuldKuldemenyek.FelulvizsgalatDatuma,
	  EREC_KuldKuldemenyek.Felulvizsgalo_id,
	  EREC_KuldKuldemenyek.MinositesFelulvizsgalatEredm,	  
	  dbo.fn_KodtarErtekNeve(''IRAT_MINOSITES'', EREC_KuldKuldemenyek.Minosites,@Org) as MinositesNev,
      '''' as IktatoSzam,
(select COUNT(*) FROM EREC_UgyiratObjKapcsolatok WHERE
EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt=EREC_KuldKuldemenyek.Id
and EREC_UgyiratObjKapcsolatok.Obj_Type_Kapcsolt=''EREC_KuldKuldemenyek''  
and getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege) as CsatolasCount_Elozmeny,
(select COUNT(*) FROM EREC_UgyiratObjKapcsolatok WHERE
EREC_UgyiratObjKapcsolatok.Obj_Id_Elozmeny=EREC_KuldKuldemenyek.Id
and EREC_UgyiratObjKapcsolatok.Obj_Type_Elozmeny=''EREC_KuldKuldemenyek''  
and getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege) as CsatolasCount_Kapcsolt'

-- CR3442
--IF @OrgIsBOMPH = 1
	set @sqlcmd = @sqlcmd + '
		  ,(select top 1 dbo.fn_KodtarErtekNeve(''FELADAT_ALTIPUS'', EREC_HataridosFeladatok.Altipus, @Org)
      from EREC_HataridosFeladatok where EREC_HataridosFeladatok.Obj_Id = EREC_KuldKuldemenyek.Id
      and EREC_HataridosFeladatok.Tipus = ''M''
      order by EREC_HataridosFeladatok.LetrehozasIdo desc) as KezelesTipusNev'
--ELSE
--	set @sqlcmd = @sqlcmd + '
--		  ,null as KezelesTipusNev'

-- blg 2730

set @sqlcmd = @sqlcmd + ', KRT_Cimek.IRSZ as IRSZ, KRT_Cimek.TelepulesNev as TelepulesNev, KRT_Cimek.KozteruletNev as KozteruletNev, KRT_Cimek.KozteruletTipusNev as KozteruletTipusNev,
                          KRT_Cimek.Hazszam as Hazszam, KRT_Cimek.HazszamBetujel as HazszamBetujel, KRT_Cimek.Lepcsohaz as Lepcsohaz, KRT_Cimek.Szint as Szint,
						  KRT_Cimek.Ajto as Ajto, KRT_Cimek.AjtoBetujel as AjtoBetujel '

-- blg7824
select  @Ugyintez = COALESCE(@Ugyintez + ', ', '') +  KRT_Felhasznalok.Nev
FROM EREC_IraIratok
LEFT JOIN KRT_Felhasznalok ON EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez = KRT_Felhasznalok.id
WHERE EREC_IraIratok.Id in
(
	select EREC_PldIratPeldanyok.IraIrat_Id from 
	EREC_PldIratPeldanyok
	LEFT JOIN EREC_Kuldemeny_IratPeldanyai on EREC_Kuldemeny_IratPeldanyai.Peldany_Id = EREC_PldIratPeldanyok.Id
	WHERE EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id = @Id
)
and KRT_Felhasznalok.Nev is not null

IF @Ugyintez is NULL
	set @Ugyintez = ''


set @sqlcmd = @sqlcmd + ', (select '''+ @Ugyintez+ ''') as IntezkedoUgyintezo '

select 
@FeladoNev = cast(cast(ik.EFeladoJegyzekUgyfelAdatok as text) as xml).value('(/ugyfel_adatok/nev)[1]', 'VARCHAR(max)') ,
@FeladoIRSZ = cast(cast(ik.EFeladoJegyzekUgyfelAdatok as text) as xml).value('(/ugyfel_adatok/iranyitoszam)[1]', 'VARCHAR(max)') ,
@FeladoHelyseg= cast(cast(ik.EFeladoJegyzekUgyfelAdatok as text) as xml).value('(/ugyfel_adatok/helyseg)[1]', 'VARCHAR(max)') ,
@FeladoUtca = cast(cast(ik.EFeladoJegyzekUgyfelAdatok as text) as xml).value('(/ugyfel_adatok/utca)[1]', 'VARCHAR(max)') 	   
from EREC_IraIktatoKonyvek ik
LEFT JOIN EREC_KuldKuldemenyek kk ON COALESCE(kk.IraIktatokonyv_Id,(SELECT rsz.Postakonyv_Id FROM KRT_RagSzamok rsz where rsz.Kod = kk.RagSzam)) = ik.Id -- BLG_8341
where kk.id = @id 

set @sqlcmd = @sqlcmd + ', (select ''' + isnull(@FeladoNev,'') + ''') as FeladoNev, (select ''' + isnull(@FeladoIRSZ,'') + ''') as FeladoIRSZ,
             (select ''' + isnull(@FeladoHelyseg,'') + ''') as FeladoHelyseg, (select ''' + isnull(@FeladoUtca,'') + ''') as FeladoUtca '


-- blg 4787

-- amennyiben csak 1 iratp�ld�ny tartozik a k�ldem�nyhez, akkor lek�rem az iratp�ld�ny irat�nak t�rgy�t, ha t�bb, akkor NULL-t adok vissza 

set @sqlcmd = @sqlcmd + ', (SELECT ii.Targy FROM EREC_Kuldemeny_IratPeldanyai kp JOIN EREC_PldIratPeldanyok pp
						ON kp.Peldany_Id = pp.Id JOIN EREC_IraIratok ii ON pp.IraIrat_Id = ii.Id
						WHERE kp.KuldKuldemeny_Id = @id and (SELECT COUNT(*) KuldKuldemeny_Id FROM EREC_Kuldemeny_IratPeldanyai 
						WHERE KuldKuldemeny_Id = @id GROUP BY KuldKuldemeny_Id) < 2) as IratTargy'

-- amennyiben csak 1 iratp�ld�ny tartozik a k�ldem�nyhez, akkor lek�rem az iratp�ld�ny �gyirat�hoz tartoz� szervezeti k�dot, ha t�bb, akkor NULL-t adok vissza 

set @sqlcmd = @sqlcmd + ', (SELECT cs.Kod FROM EREC_Kuldemeny_IratPeldanyai kp JOIN EREC_PldIratPeldanyok pp
							ON kp.Peldany_Id = pp.Id JOIN EREC_IraIratok ii ON pp.IraIrat_Id = ii.Id
							JOIN EREC_UgyUgyiratok uu ON ii.Ugyirat_Id = uu.Id JOIN KRT_Csoportok cs ON
							uu.Csoport_Id_Ugyfelelos = cs.Id
							WHERE kp.KuldKuldemeny_Id = @id and (SELECT COUNT(*) KuldKuldemeny_Id FROM EREC_Kuldemeny_IratPeldanyai 
							WHERE KuldKuldemeny_Id = @id GROUP BY KuldKuldemeny_Id) < 2) as SZKod'

------
		  
set @sqlcmd = @sqlcmd + '
   from EREC_KuldKuldemenyek as EREC_KuldKuldemenyek
      inner join #result on #result.Id = EREC_KuldKuldemenyek.Id  
      left join KRT_Cimek as KRT_Cimek on KRT_Cimek.Id = EREC_KuldKuldemenyek.Cim_Id
      left join EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek on EREC_IraIktatoKonyvek.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
      left join KRT_Partnerek as KRT_PartnerekBekuldok on KRT_PartnerekBekuldok.Id = EREC_KuldKuldemenyek.Partner_Id_Bekuldo
      left join EREC_Szamlak on EREC_Szamlak.KuldKuldemeny_Id=EREC_KuldKuldemenyek.Id
	  	  LEFT JOIN KRT_Partnerek KRT_PartnerMinosito on EREC_KuldKuldemenyek.Minosito = KRT_PartnerMinosito.Id
   WHERE  EREC_KuldKuldemenyek.id = @id
   ORDER BY #result.RowNumber;'

      -- tal�latok sz�ma �s oldalsz�m
      set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter;';

      execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @ExecutorUserId uniqueidentifier, @FelhasznaloSzervezet_Id uniqueidentifier, @SelectedRowId uniqueidentifier, @Org uniqueidentifier, @FelhasznaloId uniqueidentifier, @SzervezetId uniqueidentifier, @id uniqueidentifier'
      ,@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @ExecutorUserId = @ExecutorUserId, @FelhasznaloSzervezet_Id = @FelhasznaloSzervezet_Id, @SelectedRowId = @SelectedRowId, @Org = @Org
      ,@FelhasznaloId = @FelhasznaloId, @SzervezetId = @SzervezetId, @id = @id

END TRY
BEGIN CATCH
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

