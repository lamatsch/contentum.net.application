﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_KuldKuldemenyekHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_EREC_KuldKuldemenyekHistoryGetAllRecord
go
*/
create procedure sp_EREC_KuldKuldemenyekHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_KuldKuldemenyekHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Allapot as nvarchar(max)),'') != ISNULL(CAST(New.Allapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'KULDEMENY_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Allapot and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Allapot and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BeerkezesIdeje' as ColumnName,               cast(Old.BeerkezesIdeje as nvarchar(99)) as OldValue,
               cast(New.BeerkezesIdeje as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.BeerkezesIdeje as nvarchar(max)),'') != ISNULL(CAST(New.BeerkezesIdeje as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelbontasDatuma' as ColumnName,               cast(Old.FelbontasDatuma as nvarchar(99)) as OldValue,
               cast(New.FelbontasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelbontasDatuma as nvarchar(max)),'') != ISNULL(CAST(New.FelbontasDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIktatokonyv_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_IraIktatoKonyvekAzonosito(Old.IraIktatokonyv_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_IraIktatoKonyvekAzonosito(New.IraIktatokonyv_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IraIktatokonyv_Id as nvarchar(max)),'') != ISNULL(CAST(New.IraIktatokonyv_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join EREC_IraIktatoKonyvek FTOld on FTOld.Id = Old.IraIktatokonyv_Id --and FTOld.Ver = Old.Ver
         left join EREC_IraIktatoKonyvek FTNew on FTNew.Id = New.IraIktatokonyv_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldesMod' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KuldesMod as nvarchar(max)),'') != ISNULL(CAST(New.KuldesMod as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'KULDEMENY_KULDES_MODJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.KuldesMod and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.KuldesMod and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Erkezteto_Szam' as ColumnName,               cast(Old.Erkezteto_Szam as nvarchar(99)) as OldValue,
               cast(New.Erkezteto_Szam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Erkezteto_Szam as nvarchar(max)),'') != ISNULL(CAST(New.Erkezteto_Szam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HivatkozasiSzam' as ColumnName,               cast(Old.HivatkozasiSzam as nvarchar(99)) as OldValue,
               cast(New.HivatkozasiSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HivatkozasiSzam as nvarchar(max)),'') != ISNULL(CAST(New.HivatkozasiSzam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Targy' as ColumnName,               cast(Old.Targy as nvarchar(99)) as OldValue,
               cast(New.Targy as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Targy as nvarchar(max)),'') != ISNULL(CAST(New.Targy as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tartalom' as ColumnName,               cast(Old.Tartalom as nvarchar(99)) as OldValue,
               cast(New.Tartalom as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tartalom as nvarchar(max)),'') != ISNULL(CAST(New.Tartalom as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'RagSzam' as ColumnName,               cast(Old.RagSzam as nvarchar(99)) as OldValue,
               cast(New.RagSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.RagSzam as nvarchar(max)),'') != ISNULL(CAST(New.RagSzam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Surgosseg' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Surgosseg as nvarchar(max)),'') != ISNULL(CAST(New.Surgosseg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'SURGOSSEG'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Surgosseg and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Surgosseg and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BelyegzoDatuma' as ColumnName,               cast(Old.BelyegzoDatuma as nvarchar(99)) as OldValue,
               cast(New.BelyegzoDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.BelyegzoDatuma as nvarchar(max)),'') != ISNULL(CAST(New.BelyegzoDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesModja' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyintezesModja as nvarchar(max)),'') != ISNULL(CAST(New.UgyintezesModja as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYINTEZES_ALAPJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.UgyintezesModja and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.UgyintezesModja and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PostazasIranya' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PostazasIranya as nvarchar(max)),'') != ISNULL(CAST(New.PostazasIranya as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'POSTAZAS_IRANYA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.PostazasIranya and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.PostazasIranya and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tovabbito' as ColumnName,               cast(Old.Tovabbito as nvarchar(99)) as OldValue,
               cast(New.Tovabbito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tovabbito as nvarchar(max)),'') != ISNULL(CAST(New.Tovabbito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PeldanySzam' as ColumnName,               cast(Old.PeldanySzam as nvarchar(99)) as OldValue,
               cast(New.PeldanySzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PeldanySzam as nvarchar(max)),'') != ISNULL(CAST(New.PeldanySzam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IktatniKell' as ColumnName,
               case Old.IktatniKell when 0 then 'Nem' when 1 then 'Igen' end as OldValue,
               case New.IktatniKell when 0 then 'Nem' when 1 then 'Igen' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IktatniKell as nvarchar(max)),'') != ISNULL(CAST(New.IktatniKell as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Iktathato' as ColumnName,
               case Old.Iktathato when 0 then 'Nem' when 1 then 'Igen' end as OldValue,
               case New.Iktathato when 0 then 'Nem' when 1 then 'Igen' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Iktathato as nvarchar(max)),'') != ISNULL(CAST(New.Iktathato as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SztornirozasDat' as ColumnName,               cast(Old.SztornirozasDat as nvarchar(99)) as OldValue,
               cast(New.SztornirozasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SztornirozasDat as nvarchar(max)),'') != ISNULL(CAST(New.SztornirozasDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldKuldemeny_Id_Szulo' as ColumnName,
/*FK*/           dbo.fn_GetEREC_KuldKuldemenyekAzonosito(Old.KuldKuldemeny_Id_Szulo) as OldValue,
/*FK*/           dbo.fn_GetEREC_KuldKuldemenyekAzonosito(New.KuldKuldemeny_Id_Szulo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KuldKuldemeny_Id_Szulo as nvarchar(max)),'') != ISNULL(CAST(New.KuldKuldemeny_Id_Szulo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join EREC_KuldKuldemenyek FTOld on FTOld.Id = Old.KuldKuldemeny_Id_Szulo --and FTOld.Ver = Old.Ver
         left join EREC_KuldKuldemenyek FTNew on FTNew.Id = New.KuldKuldemeny_Id_Szulo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Erkeztetes_Ev' as ColumnName,               cast(Old.Erkeztetes_Ev as nvarchar(99)) as OldValue,
               cast(New.Erkeztetes_Ev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Erkeztetes_Ev as nvarchar(max)),'') != ISNULL(CAST(New.Erkeztetes_Ev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Cimzett' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Cimzett) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Cimzett) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id_Cimzett as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id_Cimzett as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.Csoport_Id_Cimzett --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Csoport_Id_Cimzett --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Felelos' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Felelos) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Felelos) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id_Felelos as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id_Felelos as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.Csoport_Id_Felelos --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Csoport_Id_Felelos --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Expedial' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Expedial) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Expedial) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Expedial as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Expedial as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ExpedialasIdeje' as ColumnName,               cast(Old.ExpedialasIdeje as nvarchar(99)) as OldValue,
               cast(New.ExpedialasIdeje as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ExpedialasIdeje as nvarchar(max)),'') != ISNULL(CAST(New.ExpedialasIdeje as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Orzo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Orzo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Orzo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Orzo as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Orzo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Orzo --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Orzo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Atvevo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Atvevo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Atvevo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Atvevo as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Atvevo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Atvevo --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Atvevo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id_Bekuldo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(Old.Partner_Id_Bekuldo) as OldValue,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(New.Partner_Id_Bekuldo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Partner_Id_Bekuldo as nvarchar(max)),'') != ISNULL(CAST(New.Partner_Id_Bekuldo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Partnerek FTOld on FTOld.Id = Old.Partner_Id_Bekuldo --and FTOld.Ver = Old.Ver
         left join KRT_Partnerek FTNew on FTNew.Id = New.Partner_Id_Bekuldo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Cim_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CimekAzonosito(Old.Cim_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_CimekAzonosito(New.Cim_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Cim_Id as nvarchar(max)),'') != ISNULL(CAST(New.Cim_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Cimek FTOld on FTOld.Id = Old.Cim_Id --and FTOld.Ver = Old.Ver
         left join KRT_Cimek FTNew on FTNew.Id = New.Cim_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CimSTR_Bekuldo' as ColumnName,               cast(Old.CimSTR_Bekuldo as nvarchar(99)) as OldValue,
               cast(New.CimSTR_Bekuldo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.CimSTR_Bekuldo as nvarchar(max)),'') != ISNULL(CAST(New.CimSTR_Bekuldo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'NevSTR_Bekuldo' as ColumnName,               cast(Old.NevSTR_Bekuldo as nvarchar(99)) as OldValue,
               cast(New.NevSTR_Bekuldo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.NevSTR_Bekuldo as nvarchar(max)),'') != ISNULL(CAST(New.NevSTR_Bekuldo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AdathordozoTipusa' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AdathordozoTipusa as nvarchar(max)),'') != ISNULL(CAST(New.AdathordozoTipusa as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'ADATHORDOZO_TIPUSA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.AdathordozoTipusa and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.AdathordozoTipusa and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ElsodlegesAdathordozoTipusa' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ElsodlegesAdathordozoTipusa as nvarchar(max)),'') != ISNULL(CAST(New.ElsodlegesAdathordozoTipusa as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'ELSODLEGES_ADATHORDOZO'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.ElsodlegesAdathordozoTipusa and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.ElsodlegesAdathordozoTipusa and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Alairo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Alairo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Alairo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Alairo as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Alairo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Alairo --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Alairo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BarCode' as ColumnName,               cast(Old.BarCode as nvarchar(99)) as OldValue,
               cast(New.BarCode as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.BarCode as nvarchar(max)),'') != ISNULL(CAST(New.BarCode as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Bonto' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Bonto) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Bonto) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Bonto as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Bonto as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Bonto --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Bonto --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CsoportFelelosEloszto_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.CsoportFelelosEloszto_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.CsoportFelelosEloszto_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.CsoportFelelosEloszto_Id as nvarchar(max)),'') != ISNULL(CAST(New.CsoportFelelosEloszto_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.CsoportFelelosEloszto_Id --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.CsoportFelelosEloszto_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Felelos_Elozo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Felelos_Elozo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Felelos_Elozo) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id_Felelos_Elozo as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id_Felelos_Elozo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Csoportok FTOld on FTOld.Id = Old.Csoport_Id_Felelos_Elozo --and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Csoport_Id_Felelos_Elozo --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kovetkezo_Felelos_Id' as ColumnName,               cast(Old.Kovetkezo_Felelos_Id as nvarchar(99)) as OldValue,
               cast(New.Kovetkezo_Felelos_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Kovetkezo_Felelos_Id as nvarchar(max)),'') != ISNULL(CAST(New.Kovetkezo_Felelos_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Elektronikus_Kezbesitesi_Allap' as ColumnName,               cast(Old.Elektronikus_Kezbesitesi_Allap as nvarchar(99)) as OldValue,
               cast(New.Elektronikus_Kezbesitesi_Allap as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Elektronikus_Kezbesitesi_Allap as nvarchar(max)),'') != ISNULL(CAST(New.Elektronikus_Kezbesitesi_Allap as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kovetkezo_Orzo_Id' as ColumnName,               cast(Old.Kovetkezo_Orzo_Id as nvarchar(99)) as OldValue,
               cast(New.Kovetkezo_Orzo_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Kovetkezo_Orzo_Id as nvarchar(max)),'') != ISNULL(CAST(New.Kovetkezo_Orzo_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Fizikai_Kezbesitesi_Allapot' as ColumnName,               cast(Old.Fizikai_Kezbesitesi_Allapot as nvarchar(99)) as OldValue,
               cast(New.Fizikai_Kezbesitesi_Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Fizikai_Kezbesitesi_Allapot as nvarchar(max)),'') != ISNULL(CAST(New.Fizikai_Kezbesitesi_Allapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIratok_Id' as ColumnName,
/*FK*/			   dbo.fn_GetEREC_IraIratokAzonosito(Old.IraIratok_Id) as OldValue,
/*FK*/             dbo.fn_GetEREC_IraIratokAzonosito(New.IraIratok_Id) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IraIratok_Id as nvarchar(max)),'') != ISNULL(CAST(New.IraIratok_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BontasiMegjegyzes' as ColumnName,               cast(Old.BontasiMegjegyzes as nvarchar(99)) as OldValue,
               cast(New.BontasiMegjegyzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.BontasiMegjegyzes as nvarchar(max)),'') != ISNULL(CAST(New.BontasiMegjegyzes as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tipus' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tipus as nvarchar(max)),'') != ISNULL(CAST(New.Tipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'KULDEMENY_TIPUS'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Tipus and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Tipus and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Minosites' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,  
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Minosites as nvarchar(max)),'') != ISNULL(CAST(New.Minosites as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRAT_MINOSITES'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Minosites and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Minosites and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MegtagadasIndoka' as ColumnName,               cast(Old.MegtagadasIndoka as nvarchar(99)) as OldValue,
               cast(New.MegtagadasIndoka as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MegtagadasIndoka as nvarchar(max)),'') != ISNULL(CAST(New.MegtagadasIndoka as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Megtagado_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Megtagado_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Megtagado_Id) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Megtagado_Id as nvarchar(max)),'') != ISNULL(CAST(New.Megtagado_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MegtagadasDat' as ColumnName,               cast(Old.MegtagadasDat as nvarchar(99)) as OldValue,
               cast(New.MegtagadasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MegtagadasDat as nvarchar(max)),'') != ISNULL(CAST(New.MegtagadasDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KimenoKuldemenyFajta' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KimenoKuldemenyFajta as nvarchar(max)),'') != ISNULL(CAST(New.KimenoKuldemenyFajta as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
		 left join KRT_KodCsoportok KCs on KCs.Kod = 'KIMENO_KULDEMENY_FAJTA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.KimenoKuldemenyFajta and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.KimenoKuldemenyFajta and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Elsobbsegi' as ColumnName,
               case Old.Elsobbsegi when 0 then 'Nem' when 1 then 'Igen' end as OldValue,
               case New.Elsobbsegi when 0 then 'Nem' when 1 then 'Igen' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Elsobbsegi as nvarchar(max)),'') != ISNULL(CAST(New.Elsobbsegi as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ajanlott' as ColumnName,
               case Old.Ajanlott when 0 then 'Nem' when 1 then 'Igen' end as OldValue,
               case New.Ajanlott when 0 then 'Nem' when 1 then 'Igen' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Ajanlott as nvarchar(max)),'') != ISNULL(CAST(New.Ajanlott as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tertiveveny' as ColumnName,
               case Old.Tertiveveny when 0 then 'Nem' when 1 then 'Igen' end as OldValue,
               case New.Tertiveveny when 0 then 'Nem' when 1 then 'Igen' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tertiveveny as nvarchar(max)),'') != ISNULL(CAST(New.Tertiveveny as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SajatKezbe' as ColumnName,
               case Old.SajatKezbe when 0 then 'Nem' when 1 then 'Igen' end as OldValue,
               case New.SajatKezbe when 0 then 'Nem' when 1 then 'Igen' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SajatKezbe as nvarchar(max)),'') != ISNULL(CAST(New.SajatKezbe as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'E_ertesites' as ColumnName,
               case Old.E_ertesites when 0 then 'Nem' when 1 then 'Igen' end as OldValue,
               case New.E_ertesites when 0 then 'Nem' when 1 then 'Igen' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.E_ertesites as nvarchar(max)),'') != ISNULL(CAST(New.E_ertesites as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'E_elorejelzes' as ColumnName,
               case Old.E_elorejelzes when 0 then 'Nem' when 1 then 'Igen' end as OldValue,
               case New.E_elorejelzes when 0 then 'Nem' when 1 then 'Igen' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.E_elorejelzes as nvarchar(max)),'') != ISNULL(CAST(New.E_elorejelzes as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PostaiLezaroSzolgalat' as ColumnName,
               case Old.PostaiLezaroSzolgalat when 0 then 'Nem' when 1 then 'Igen' end as OldValue,
               case New.PostaiLezaroSzolgalat when 0 then 'Nem' when 1 then 'Igen' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PostaiLezaroSzolgalat as nvarchar(max)),'') != ISNULL(CAST(New.PostaiLezaroSzolgalat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ar' as ColumnName,               cast(Old.Ar as nvarchar(99)) as OldValue,
               cast(New.Ar as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Ar as nvarchar(max)),'') != ISNULL(CAST(New.Ar as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KimenoKuld_Sorszam' as ColumnName,               cast(Old.KimenoKuld_Sorszam  as nvarchar(99)) as OldValue,
               cast(New.KimenoKuld_Sorszam  as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KimenoKuld_Sorszam  as nvarchar(max)),'') != ISNULL(CAST(New.KimenoKuld_Sorszam  as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)

      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Ugyfelelos' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(IOld.Csoport_Id_Ugyfelelos) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(INew.Csoport_Id_Ugyfelelos) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IraIratok_Id as nvarchar(max)),'') != ISNULL(CAST(New.IraIratok_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_IraIratok IOld on IOld.Id = Old.IraIratok_Id
         left join EREC_IraIratok INew on INew.Id = New.IraIratok_Id
)

union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TevesCimzes' as ColumnName,
               case Old.TevesCimzes when 0 then 'Nem' when 1 then 'Igen' end as OldValue,
               case New.TevesCimzes when 0 then 'Nem' when 1 then 'Igen' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.TevesCimzes  as nvarchar(max)),'') != ISNULL(CAST(New.TevesCimzes  as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)

union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TevesErkeztetes' as ColumnName,
               case Old.TevesErkeztetes when 0 then 'Nem' when 1 then 'Igen' end as OldValue,
               case New.TevesErkeztetes when 0 then 'Nem' when 1 then 'Igen' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.TevesErkeztetes  as nvarchar(max)),'') != ISNULL(CAST(New.TevesErkeztetes  as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)

union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SerultKuldemeny' as ColumnName,
               case Old.SerultKuldemeny when 0 then 'Nem' when 1 then 'Igen' end as OldValue,
               case New.SerultKuldemeny when 0 then 'Nem' when 1 then 'Igen' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SerultKuldemeny  as nvarchar(max)),'') != ISNULL(CAST(New.SerultKuldemeny  as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarId' as ColumnName,               cast(Old.IrattarId as nvarchar(99)) as OldValue,
               cast(New.IrattarId as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IrattarId as nvarchar(max)),'') != ISNULL(CAST(New.IrattarId as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattariHely' as ColumnName,               cast(Old.IrattariHely as nvarchar(99)) as OldValue,
               cast(New.IrattariHely as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IrattariHely as nvarchar(max)),'') != ISNULL(CAST(New.IrattariHely as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarId' as ColumnName,               cast(Old.IrattarId as nvarchar(99)) as OldValue,
               cast(New.IrattarId as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IrattarId as nvarchar(max)),'') != ISNULL(CAST(New.IrattarId as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattariHely' as ColumnName,               cast(Old.IrattariHely as nvarchar(99)) as OldValue,
               cast(New.IrattariHely as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_KuldKuldemenyekHistory Old
         inner join EREC_KuldKuldemenyekHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_KuldKuldemenyekHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IrattariHely as nvarchar(max)),'') != ISNULL(CAST(New.IrattariHely as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go