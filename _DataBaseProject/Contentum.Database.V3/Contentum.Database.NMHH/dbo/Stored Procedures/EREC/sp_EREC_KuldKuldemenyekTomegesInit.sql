﻿--set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--if exists (select 1
--            from  sysobjects
--           where  id = object_id('sp_EREC_KuldKuldemenyekTomegesInit')
--            and   type = 'P')
--   drop procedure sp_EREC_KuldKuldemenyekTomegesInit
--go


CREATE procedure [dbo].[sp_EREC_KuldKuldemenyekTomegesInit]      
                @Allapot     nvarchar(64)  = null,
	            @BeerkezesIdeje     datetime,
                @IraIktatokonyv_Id     uniqueidentifier  = null,
	            @KuldesMod     nvarchar(64),
                @Targy     Nvarchar(4000)  = null,
	            @Surgosseg     nvarchar(64),
	            @PostazasIranya     nvarchar(64),
	            @PeldanySzam     int,
                @Letrehozo_id     uniqueidentifier  = null,
			    @Erkezteto_SzamList     nvarchar(max)  = null,
				@AzonositoList nvarchar(max) = null
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;

declare @VegrehajtasIdo datetime
set @VegrehajtasIdo = getdate()

declare @temp table(Erkezteto_Szam int, Azonosito nvarchar(100))

insert into @temp
select t1.[Value], t2.[Value] from
(select [Value], Pos  from fn_SplitWithPos(@Erkezteto_SzamList, ',')) t1,
(select [Value], Pos  from fn_SplitWithPos(@AzonositoList, ',')) t2
where t1.Pos = t2.Pos

DECLARE @InsertedRow TABLE (id uniqueidentifier)

insert into EREC_KuldKuldemenyek
	(Allapot,
	BeerkezesIdeje,
	IraIktatokonyv_Id,
	KuldesMod,
	Targy,
	Surgosseg,
	PostazasIranya,
	PeldanySzam, 
	Letrehozo_id,
	Erkezteto_Szam,
	Azonosito)
output inserted.id into @InsertedRow
select
	@Allapot,
	@BeerkezesIdeje,
	@IraIktatokonyv_Id,
	@KuldesMod,
	@Targy,
	@Surgosseg,
	@PostazasIranya,
	@PeldanySzam, 
	@Letrehozo_id,
	t.Erkezteto_Szam,
	t.Azonosito
from
@temp t
    

if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
      /* History Log */
      declare @row_ids varchar(MAX)
      /*** EREC_KuldKuldemenyek history log ***/

      set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @InsertedRow FOR XML PATH('')),1, 2, '')
      if @row_ids is not null
         set @row_ids = @row_ids + '''';

      exec sp_LogRecordsToHistory_Tomeges
       @TableName = 'EREC_KuldKuldemenyek'
      ,@Row_Ids = @row_ids
      ,@Muvelet = 0
      ,@Vegrehajto_Id = @Letrehozo_id
      ,@VegrehajtasIdo = @VegrehajtasIdo
END            


select Id from @InsertedRow
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH