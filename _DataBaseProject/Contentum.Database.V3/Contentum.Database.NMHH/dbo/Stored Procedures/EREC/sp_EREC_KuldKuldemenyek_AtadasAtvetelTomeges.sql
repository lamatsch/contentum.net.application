﻿create procedure [dbo].[sp_EREC_KuldKuldemenyek_AtadasAtvetelTomeges]
	@Ids				nvarchar(MAX),
	@Vers				nvarchar(MAX),
	@KovetkezoFelelos	uniqueidentifier = null,
	@KovetkezoOrzo		uniqueidentifier = null,
	@ExecutorUserId		uniqueidentifier,
	@KezFeljegyzesTipus nvarchar(64) = null,
	@Leiras				nvarchar(400) = NULL,
	@CheckMappa			BIT = 1
as

BEGIN TRY
	set nocount on
	declare @execTime datetime;
	declare @tempTable table(id uniqueidentifier, ver int);

	set @execTime = getdate();

	/* dinamikus history log összeállításhoz */
	declare @row_ids varchar(MAX)
	declare @insertedRowIds table (Id uniqueidentifier)

	-- temp tábla töltése az id-verzió párokkal
	declare @it	int;
	declare @verPosBegin int;
	declare @verPosEnd int;
	declare @curId	nvarchar(36);
	declare @curVer	nvarchar(6);
	declare @sqlcmd	nvarchar(500);

	set @it = 0;
	set @verPosBegin = 1;
	while (@it < ((len(@Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Ids,@it*39+2,37);
		set @verPosEnd =  CHARINDEX(',',@Vers,@verPosBegin);
		if @verPosEnd = 0 
			set @verPosEnd = len(@Vers) + 1;
		set @curVer = SUBSTRING(@vers, @verPosBegin, @verPosEnd - @verPosBegin);
		set @verPosBegin = @verPosEnd+1;
		insert into @tempTable(id,ver) values(@curId,convert(int,@curVer) );
		set @it = @it + 1;
	END

	-- verzióellenorzés
	declare @rowNumberTotal int;
	declare @rowNumberChecked int;

	select @rowNumberTotal = count(id) from @tempTable;
	select @rowNumberChecked = count(erec_kuldKuldemenyek.Id)
		from EREC_KuldKuldemenyek
			inner join @tempTable t on t.id = EREC_KuldKuldemenyek.id and t.ver = EREC_KuldKuldemenyek.ver
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
			inner join @tempTable t on t.id = EREC_KuldKuldemenyek.id and t.ver = EREC_KuldKuldemenyek.ver
		RAISERROR('[50402]',16,1);
	END

	-- kimeno küldemények - példányokat, elsodleges példányokkal iratokat mozgatni kell
	CREATE TABLE #kuldIds_kimeno(Id UNIQUEIDENTIFIER)
	
	INSERT INTO [#kuldIds_kimeno] SELECT EREC_KuldKuldemenyek.Id
			FROM EREC_KuldKuldemenyek
			where Id in (select id from @tempTable)
				and (ModositasIdo < @execTime or ModositasIdo is null)
				and Zarolo_id is NULL
				and PostazasIranya = '2' -- Kimeno

	/****** Átadásra kijelölés *****/
	-- EREC_KuldKuldemenyek UPDATE
	if @KovetkezoFelelos is not null
	BEGIN
		SELECT EREC_KuldKuldemenyek.Id INTO #atadasIds
			FROM EREC_KuldKuldemenyek
			where Id in (select id from @tempTable)
				and (ModositasIdo < @execTime or ModositasIdo is null)
				and Zarolo_id is null


		-- Utóellenorzés
		if (@rowNumberTotal != (SELECT COUNT(*) FROM #atadasIds) )
		BEGIN
			SELECT Id FROM @tempTable
			EXCEPT
			SELECT Id from #atadasIds
			DROP TABLE #atadasIds;
			RAISERROR('[50402]',16,1);
		END

		-- Fizikai mappa ellenorzés
		IF @CheckMappa = 1
		BEGIN
			SELECT #atadasIds.Id INTO #kuldemenyekMappaban
				FROM #atadasIds
					INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = #atadasIds.Id
					INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
				WHERE KRT_Mappak.Tipus = '01'
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
			IF EXISTS (SELECT Id FROM #kuldemenyekMappaban)
			BEGIN
				SELECT Id FROM #kuldemenyekMappaban
				
				DROP TABLE #atadasIds;
				DROP TABLE #kuldemenyekMappaban;
				
				RAISERROR('[64016]',16,1);
			END
			
			DROP TABLE #kuldemenyekMappaban;
		END
		
		UPDATE EREC_KuldKuldemenyek 
			set Csoport_Id_Felelos_Elozo = Csoport_Id_Felelos,
				Csoport_Id_Felelos = @KovetkezoFelelos,
				TovabbitasAlattAllapot = 
					case Allapot
						when '03' then TovabbitasAlattAllapot
						else Allapot
					end,
				Allapot = '03',
				Ver = Ver + 1,
				Modosito_Id = @ExecutorUserId,
				ModositasIdo = getdate()
			where Id in (select Id FROM #atadasIds)
		DROP TABLE #atadasIds;
		
		-- kimeno küldemény mozgásának jelzése
		if exists(select 1 from #kuldIds_kimeno)
		begin
			exec sp_EREC_KimenoKuldemenyek_Moved @ExecutorUserId = @ExecutorUserId, @ExecutionTime = @exectime
		end
		DROP TABLE #kuldIds_kimeno;


		IF @CheckMappa = 1
		BEGIN
			-- Kézbesítési tételek létrehozása
			insert into EREC_IraKezbesitesiTetelek(
					Obj_Id,
					Obj_type,
					Felhasznalo_Id_Atado_LOGIN,
					Felhasznalo_Id_Atado_USER,
					Csoport_Id_Cel,
					Allapot,
					LetrehozasIdo,
					Letrehozo_id)
				output inserted.id into @insertedRowIds
				select
					Id,
					'EREC_KuldKuldemenyek',
					@ExecutorUserId,
					@ExecutorUserId,
					@KovetkezoFelelos,
					'1',
					@execTime,
					@ExecutorUserId
				from @tempTable;
		
		/*** EREC_IraKezbesitesiTetelek history log ***/
--		INSERT INTO EREC_IraKezbesitesiTetelekHistory 
--			SELECT	NEWID(),
--					'0',
--					@ExecutorUserId,
--					@execTime,
--					*
--					FROM EREC_IraKezbesitesiTetelek
--					WHERE Obj_id IN (select Id from @tempTable) AND AtadasDat = @execTime

			-- Az oszlopnevek sorrendje nem garantált, nem szabad *-gal naplózni!
			/*** EREC_IraKezbesitesiTetelek log ***/
			set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @insertedRowIds FOR XML PATH('')),1, 2, '')
			if @row_ids is not null
				set @row_ids = @row_ids + '''';

			exec [sp_LogRecordsToHistory_Tomeges] 
					 @TableName = 'EREC_IraKezbesitesiTetelek'
					,@Row_Ids = @row_ids
					,@Muvelet = 0
					,@Vegrehajto_Id = @ExecutorUserId
					,@VegrehajtasIdo = @execTime

--		-- Kezelési feljegyzés insert
----		if @KezFeljegyzesTipus != ''
----		BEGIN
----			insert into EREC_KuldKezFeljegyzesek(KuldKuldemeny_Id,KezelesTipus,Leiras)
----				select Id,@KezFeljegyzesTipus,@Leiras from @tempTable
----		END
--		if (@KezFeljegyzesTipus is not null and @KezFeljegyzesTipus <> '') or (@Leiras is not null and @Leiras <> '')
--		BEGIN
--			delete from @insertedRowIds
--			--declare @insertedRowIds table (Id uniqueidentifier)
--			insert into EREC_KuldKezFeljegyzesek(KuldKuldemeny_Id,KezelesTipus,Leiras,Letrehozo_Id,LetrehozasIdo)
--				output inserted.id into @insertedRowIds
--				select Id,@KezFeljegyzesTipus,@Leiras,@executorUserId,@execTime from @tempTable
--
--			/*** EREC_KuldKezFeljegyzesekHistory log ***/
--			set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @insertedRowIds FOR XML PATH('')),1, 2, '')
--			if @row_ids is not null
--				set @row_ids = @row_ids + '''';
--
--			exec [sp_LogRecordsToHistory_Tomeges] 
--					 @TableName = 'EREC_KuldKezFeljegyzesek'
--					,@Row_Ids = @row_ids
--					,@Muvelet = 0
--					,@Vegrehajto_Id = @ExecutorUserId
--					,@VegrehajtasIdo = @execTime
--		END
		END
	END

	/****** Átvétel update *****/
	-- EREC_KuldKuldemenyek UPDATE
	if @KovetkezoOrzo is not null
	BEGIN
		SELECT EREC_KuldKuldemenyek.Id INTO #atvetelIds
			FROM EREC_KuldKuldemenyek
			where Id in (select id from @tempTable)
				and (ModositasIdo < @execTime or ModositasIdo is null)
				and Zarolo_id is null

		-- Utóellenorzés
		if (@rowNumberTotal != (SELECT COUNT(*) FROM #atvetelIds) )
		BEGIN
			SELECT Id FROM @tempTable
			EXCEPT
			SELECT Id from #atvetelIds
			DROP TABLE #atvetelIds;
			RAISERROR('[50402]',16,1);
		END

		-- Fizikai mappa ellenorzés
		IF @CheckMappa = 1
		BEGIN
			SELECT #atvetelIds.Id INTO #kuldemenyekMappa
				FROM #atvetelIds
					INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = #atvetelIds.Id
					INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
				WHERE KRT_Mappak.Tipus = '01'
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
			IF EXISTS (SELECT Id FROM #kuldemenyekMappa)
			BEGIN
				SELECT Id FROM #kuldemenyekMappa
				
				DROP TABLE #atvetelIds;
				DROP TABLE #kuldemenyekMappa;
				
				RAISERROR('[64016]',16,1);
			END
			
			DROP TABLE #kuldemenyekMappa;
		END
		
		DECLARE @ORG_ID uniqueidentifier
		SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
		DECLARE @ORG_KOD nvarchar(100) 
		SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 
 
		DECLARE @IS_TUK char(1) 
		SET	@IS_TUK = (select TOP 1 ertek from KRT_Parameterek	where nev='TUK')

	IF((@ORG_KOD ='NMHH') and (@IS_TUK ='1'))
	BEGIN 

		declare @irattariId uniqueidentifier;
		declare @irattariHely nvarchar(100);

		select top 1 @irattariId=ih.Id, @irattariHely=ih.Ertek from 
		EREC_IrattariHelyek ih 
		inner join  KRT_PartnerKapcsolatok pk on pk.Partner_id_kapcsolt = ih.Felelos_Csoport_Id		
		inner join KRT_PartnerKapcsolatok pkf on pkf.Partner_id_kapcsolt=pk.Partner_id
		inner join KRT_CsoportTagok cst on cst.Csoport_Id=pkf.Partner_id
		inner join KRT_Csoportok cs on cs.Id=cst.Csoport_Id_Jogalany
		where pk.Tipus = '5'		and  
		((cst.Csoport_Id_Jogalany=@KovetkezoOrzo and cs.Tipus='1') or (pkf.Partner_id=@KovetkezoOrzo and cs.Tipus!='1'))
		and ih.ErvKezd<=getdate() and ih.ErvVege>=getdate() and pk.ErvKezd<=getdate() and pk.ErvVege>=getdate()
		and  cst.ErvKezd<=getdate() and cst.ErvVege>=getdate()
		and ih.Id is not null and ih.Ertek is not null

		UPDATE EREC_KuldKuldemenyek 
			set Csoport_Id_Felelos = @KovetkezoOrzo,
				FelhasznaloCsoport_Id_Orzo = @KovetkezoOrzo,
				Allapot = 
					case TovabbitasAlattAllapot
						when '52' then TovabbitasAlattAllapot -- Expediált továbbított az átvétel után is expediált lesz
						else '00' -- Átvett
					end,
				TovabbitasAlattAllapot = NULL,
				Ver = Ver + 1,
				Modosito_Id = @ExecutorUserId,
				ModositasIdo = getdate(),
				IrattarId=@irattariId,
				IrattariHely=@irattariHely
			where Id in (select Id FROM #atvetelIds)
		DROP TABLE #atvetelIds;
END
ELSE
	BEGIN
	 UPDATE EREC_KuldKuldemenyek 
			set Csoport_Id_Felelos = @KovetkezoOrzo,
				FelhasznaloCsoport_Id_Orzo = @KovetkezoOrzo,
				Allapot = 
					case TovabbitasAlattAllapot
						when '52' then TovabbitasAlattAllapot -- Expediált továbbított az átvétel után is expediált lesz
						else '00' -- Átvett
					end,
				TovabbitasAlattAllapot = NULL,
				Ver = Ver + 1,
				Modosito_Id = @ExecutorUserId,
				ModositasIdo = getdate()
			where Id in (select Id FROM #atvetelIds)
		DROP TABLE #atvetelIds;
	END
		-- kimeno küldemény mozgásának jelzése
		if exists(select 1 from #kuldIds_kimeno)
		begin
			exec sp_EREC_KimenoKuldemenyek_Moved @ExecutorUserId = @ExecutorUserId, @ExecutionTime = @exectime
		end
		DROP TABLE #kuldIds_kimeno;

	END
--
--	begin   /* History Log */
--		/*** EREC_KuldKuldemenyek history log ***/
--		declare kuldemenyHistoryCursor cursor for 
--			select Id from @tempTable;
--		declare @kuldemenyId uniqueidentifier;
--
--		open kuldemenyHistoryCursor;
--		fetch next from kuldemenyHistoryCursor into @kuldemenyId;
--		while @@FETCH_STATUS = 0
--		BEGIN
--			exec sp_LogRecordToHistory 'EREC_KuldKuldemenyek',@kuldemenyId
--						,'EREC_KuldKuldemenyekHistory',1,@ExecutorUserId,@execTime;
--			fetch next from kuldemenyHistoryCursor into @kuldemenyId;
--		END
--
--		CLOSE kuldemenyHistoryCursor;
--		DEALLOCATE kuldemenyHistoryCursor;
--	end

	/*** EREC_KuldKuldemenyekHistory log ***/
	set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @tempTable FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
		set @row_ids = @row_ids + '''';

	exec [sp_LogRecordsToHistory_Tomeges] 
			 @TableName = 'EREC_KuldKuldemenyek'
			,@Row_Ids = @row_ids
			,@Muvelet = 1
			,@Vegrehajto_Id = @ExecutorUserId
			,@VegrehajtasIdo = @execTime


END TRY
BEGIN CATCH

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH