﻿CREATE procedure [dbo].[sp_EREC_KuldMellekletekGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_KuldMellekletek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
               

  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_KuldMellekletek.Id,
	   EREC_KuldMellekletek.KuldKuldemeny_Id,
	   EREC_KuldMellekletek.Mennyiseg,
	   EREC_KuldMellekletek.Megjegyzes,
	   EREC_KuldMellekletek.SztornirozasDat,
	   EREC_KuldMellekletek.AdathordozoTipus,
KRT_KodTarak.Nev as AdathordozoTipusNev,
	   EREC_KuldMellekletek.MennyisegiEgyseg,
KRT_KodTarak2.Nev as MennyisegiEgysegNev,
       EREC_KuldMellekletek.BarCode,
	   EREC_KuldMellekletek.Ver,
	   EREC_KuldMellekletek.Note,
	   EREC_KuldMellekletek.Stat_id,
	   EREC_KuldMellekletek.ErvKezd,
	   EREC_KuldMellekletek.ErvVege,
	   EREC_KuldMellekletek.Letrehozo_id,
	   EREC_KuldMellekletek.LetrehozasIdo,
	   EREC_KuldMellekletek.Modosito_id,
	   EREC_KuldMellekletek.ModositasIdo,
	   EREC_KuldMellekletek.Zarolo_id,
	   EREC_KuldMellekletek.ZarolasIdo,
	   EREC_KuldMellekletek.Tranz_id,
	   EREC_KuldMellekletek.UIAccessLog_id  
   from 
     EREC_KuldMellekletek as EREC_KuldMellekletek
	left join KRT_KodCsoportok on KRT_KodCsoportok.Kod=''ADATHORDOZO_TIPUSA''
	left join KRT_KodTarak on KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and EREC_KuldMellekletek.AdathordozoTipus = KRT_KodTarak.Kod and KRT_KodTarak.Org=''' + CAST(@Org as NvarChar(40)) + '''
	left join KRT_KodCsoportok as KRT_KodCsoportok2 on KRT_KodCsoportok2.Kod=''MENNYISEGI_EGYSEG''
	left join KRT_KodTarak as KRT_KodTarak2 on KRT_KodCsoportok2.Id = KRT_KodTarak2.KodCsoport_Id and EREC_KuldMellekletek.MennyisegiEgyseg = KRT_KodTarak2.Kod and KRT_KodTarak2.Org=''' + CAST(@Org as NvarChar(40)) + '''
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end