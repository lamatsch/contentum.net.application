﻿
CREATE procedure [dbo].[sp_EREC_KuldMellekletekInsert]    
                @Id      uniqueidentifier = null,    
               	            @KuldKuldemeny_Id     uniqueidentifier,
	            @Mennyiseg     int,
                @Megjegyzes     Nvarchar(400)  = null,
                @SztornirozasDat     datetime  = null,
	            @AdathordozoTipus     nvarchar(64),
                @MennyisegiEgyseg     nvarchar(64)  = null,
                @BarCode     Nvarchar(100)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @myid uniqueidentifier
Set @myid   = NEWID()

if @Id is not null
begin
   SET @myid = @Id
end

DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = 'Id'
SET @insertValues = '@myid' 
       
         if @KuldKuldemeny_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',KuldKuldemeny_Id'
            SET @insertValues = @insertValues + ',@KuldKuldemeny_Id'
         end 
       
         if @Mennyiseg is not null
         begin
            SET @insertColumns = @insertColumns + ',Mennyiseg'
            SET @insertValues = @insertValues + ',@Mennyiseg'
         end 
       
         if @Megjegyzes is not null
         begin
            SET @insertColumns = @insertColumns + ',Megjegyzes'
            SET @insertValues = @insertValues + ',@Megjegyzes'
         end 
       
         if @SztornirozasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SztornirozasDat'
            SET @insertValues = @insertValues + ',@SztornirozasDat'
         end 
       
         if @AdathordozoTipus is not null
         begin
            SET @insertColumns = @insertColumns + ',AdathordozoTipus'
            SET @insertValues = @insertValues + ',@AdathordozoTipus'
         end 
       
         if @MennyisegiEgyseg is not null
         begin
            SET @insertColumns = @insertColumns + ',MennyisegiEgyseg'
            SET @insertValues = @insertValues + ',@MennyisegiEgyseg'
         end 
       
         if @BarCode is not null
         begin
            SET @insertColumns = @insertColumns + ',BarCode'
            SET @insertValues = @insertValues + ',@BarCode'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end      

DECLARE @InsertCommand NVARCHAR(4000)
SET @InsertCommand = 'insert into EREC_KuldMellekletek ('+@insertColumns+') values ('+@insertValues+')'
exec sp_executesql @InsertCommand, 
                             N'@myid uniqueidentifier,@KuldKuldemeny_Id uniqueidentifier,@Mennyiseg int,@Megjegyzes Nvarchar(400),@SztornirozasDat datetime,@AdathordozoTipus nvarchar(64),@MennyisegiEgyseg nvarchar(64),@BarCode Nvarchar(100),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier'
     ,@myid = @myid,@KuldKuldemeny_Id = @KuldKuldemeny_Id,@Mennyiseg = @Mennyiseg,@Megjegyzes = @Megjegyzes,@SztornirozasDat = @SztornirozasDat,@AdathordozoTipus = @AdathordozoTipus,@MennyisegiEgyseg = @MennyisegiEgyseg,@BarCode = @BarCode,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id

If @@error = 0
BEGIN
   SET @ResultUid = @myid
      /* History Log */
   exec sp_LogRecordToHistory 'EREC_KuldMellekletek',@myid
					,'EREC_KuldMellekletekHistory',0,@Letrehozo_id,@LetrehozasIdo   
END
ELSE
BEGIN
   RAISERROR('[50301]',16,1)
END

--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH