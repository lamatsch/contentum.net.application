﻿
CREATE procedure [dbo].[sp_EREC_KuldTertivevenyekInsert]    
                @Id      uniqueidentifier = null,    
                               @Kuldemeny_Id     uniqueidentifier  = null,
                @Ragszam     Nvarchar(400)  = null,
                @TertivisszaKod     nvarchar(64)  = null,
                @TertivisszaDat     datetime  = null,
                @AtvevoSzemely     Nvarchar(400)  = null,
                @AtvetelDat     datetime  = null,
                @BarCode     Nvarchar(100)  = null,
                @Allapot     nvarchar(64)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,
                @KezbVelelemBeallta     char(1)  = null,
                @KezbVelelemDatuma     datetime  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Kuldemeny_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Kuldemeny_Id'
            SET @insertValues = @insertValues + ',@Kuldemeny_Id'
         end 
       
         if @Ragszam is not null
         begin
            SET @insertColumns = @insertColumns + ',Ragszam'
            SET @insertValues = @insertValues + ',@Ragszam'
         end 
       
         if @TertivisszaKod is not null
         begin
            SET @insertColumns = @insertColumns + ',TertivisszaKod'
            SET @insertValues = @insertValues + ',@TertivisszaKod'
         end 
       
         if @TertivisszaDat is not null
         begin
            SET @insertColumns = @insertColumns + ',TertivisszaDat'
            SET @insertValues = @insertValues + ',@TertivisszaDat'
         end 
       
         if @AtvevoSzemely is not null
         begin
            SET @insertColumns = @insertColumns + ',AtvevoSzemely'
            SET @insertValues = @insertValues + ',@AtvevoSzemely'
         end 
       
         if @AtvetelDat is not null
         begin
            SET @insertColumns = @insertColumns + ',AtvetelDat'
            SET @insertValues = @insertValues + ',@AtvetelDat'
         end 
       
         if @BarCode is not null
         begin
            SET @insertColumns = @insertColumns + ',BarCode'
            SET @insertValues = @insertValues + ',@BarCode'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end 
       
         if @KezbVelelemBeallta is not null
         begin
            SET @insertColumns = @insertColumns + ',KezbVelelemBeallta'
            SET @insertValues = @insertValues + ',@KezbVelelemBeallta'
         end 
       
         if @KezbVelelemDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',KezbVelelemDatuma'
            SET @insertValues = @insertValues + ',@KezbVelelemDatuma'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_KuldTertivevenyek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Kuldemeny_Id uniqueidentifier,@Ragszam Nvarchar(400),@TertivisszaKod nvarchar(64),@TertivisszaDat datetime,@AtvevoSzemely Nvarchar(400),@AtvetelDat datetime,@BarCode Nvarchar(100),@Allapot nvarchar(64),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@KezbVelelemBeallta char(1),@KezbVelelemDatuma datetime,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Kuldemeny_Id = @Kuldemeny_Id,@Ragszam = @Ragszam,@TertivisszaKod = @TertivisszaKod,@TertivisszaDat = @TertivisszaDat,@AtvevoSzemely = @AtvevoSzemely,@AtvetelDat = @AtvetelDat,@BarCode = @BarCode,@Allapot = @Allapot,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id,@KezbVelelemBeallta = @KezbVelelemBeallta,@KezbVelelemDatuma = @KezbVelelemDatuma ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_KuldTertivevenyek',@ResultUid
					,'EREC_KuldTertivevenyekHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH