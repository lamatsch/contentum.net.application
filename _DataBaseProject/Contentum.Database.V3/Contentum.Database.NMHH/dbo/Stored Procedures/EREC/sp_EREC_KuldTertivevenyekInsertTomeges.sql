﻿
CREATE procedure [dbo].[sp_EREC_KuldTertivevenyekInsertTomeges]   
--        @Ids NVARCHAR(MAX),
--        @Vers   NVARCHAR(MAX),
            @Kuldemeny_Id_List   NVARCHAR(MAX),    
                @BarCode_List NVARCHAR(MAX) = null,
                @Ragszam_List NVARCHAR(MAX) = null,
            @Kuldemeny_Id     uniqueidentifier  = null, -- csak egyedi lehet, nem vesszük figyelembe
                @Ragszam     Nvarchar(400)  = null, -- csak egyedi lehet, nem vesszük figyelembe
                @TertivisszaKod     nvarchar(64)  = null,
                @TertivisszaDat     datetime  = null,
                @AtvevoSzemely     Nvarchar(400)  = null,
                @AtvetelDat     datetime  = null,
                @BarCode     Nvarchar(100)  = null, -- csak egyedi lehet, nem vesszük figyelembe
                @Allapot     nvarchar(64)  = null,
                @Ver     int  = null, -- csak 1 lehet, nem vesszük figyelembe
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
               @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null, -- ha nincs megadva, generáljuk
                @UIAccessLog_id     uniqueidentifier  = null,
                @KezbVelelemBeallta     char(1)  = null,
                @KezbVelelemDatuma     datetime  = null,
      @UpdatedColumns              xml = null -- nem használt
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
   create table #tempTable(Kuldemeny_Id uniqueidentifier
      , BarCode nvarchar(100)
      , Ragszam nvarchar(400));

   declare @it int;
-- declare @verPosBegin int;
-- declare @verPosEnd int;
   DECLARE @BarCode_List_Begin INT;
   DECLARE @Ragszam_List_Begin INT;
   DECLARE @BarCode_List_End INT;
   DECLARE @Ragszam_List_End INT;
-- declare @curId nvarchar(36);
-- declare @curVer   nvarchar(6);
   DECLARE @curKuldemeny_Id NVARCHAR(36);
   declare @curBarCode nvarchar(100);
   declare @curRagszam nvarchar(400);

   set @it = 0;
-- set @verPosBegin = 1;
   set @BarCode_List_Begin = 1;
   set @Ragszam_List_Begin = 1;
   --while (@it < ((len(@Ids)+1) / 39))
   while (@it < ((len(@Kuldemeny_Id_List)+1) / 39))
   BEGIN
--    set @curId = SUBSTRING(@Ids,@it*39+2,37);
--    set @verPosEnd =  CHARINDEX(',',@Vers,@verPosBegin);
--    if @verPosEnd = 0 
--       set @verPosEnd = len(@Vers) + 1;
--    set @curVer = SUBSTRING(@vers, @verPosBegin, @verPosEnd - @verPosBegin);
--    set @verPosBegin = @verPosEnd+1;

      set @curKuldemeny_Id = SUBSTRING(@Kuldemeny_Id_List,@it*39+2,37);

      IF @BarCode_List IS NOT NULL
      BEGIN
         set @BarCode_List_End = CHARINDEX(',',@BarCode_List,@BarCode_List_Begin);
         if @BarCode_List_End = 0
            set @BarCode_List_End = len(@BarCode_List) + 1
         set @curBarCode = SUBSTRING(@BarCode_List, @BarCode_List_Begin, @BarCode_List_End-@BarCode_List_Begin);
         set @BarCode_List_Begin = @BarCode_List_End + 1
      END
      IF @Ragszam_List IS NOT NULL
      BEGIN
         set @Ragszam_List_End = CHARINDEX(',',@Ragszam_List,@Ragszam_List_Begin);
         if @Ragszam_List_End = 0
            set @Ragszam_List_End = len(@Ragszam_List) + 1
         set @curRagszam = SUBSTRING(@Ragszam_List, @Ragszam_List_Begin, @Ragszam_List_End-@Ragszam_List_Begin);
         set @Ragszam_List_Begin = @Ragszam_List_End + 1
      END
      insert into #tempTable(Kuldemeny_Id, BarCode, RagSzam) values(@curKuldemeny_Id, @curBarCode, @curRagszam );
      set @it = @it + 1;
   END

   /* Vonalkódok ellenőrzése START */
   if exists(select 1 from #tempTable
            where not exists(
               select 1 from KRT_BarKodok
                  where Kod = #tempTable.BarCode
                  and KodType in ('N','P','G')
            )
         )
   begin
      -- A megadott tértivevény vonalkód nincs regisztrálva a rendszerben!
      select EREC_KuldKuldemenyek.BarCode
         from EREC_KuldKuldemenyek
         where Id in (
         select Kuldemeny_Id from #tempTable
               where not exists(
                  select 1 from KRT_BarKodok
                     where Kod = #tempTable.BarCode
                     and KodType in ('N','P','G')
               )
         )
        RAISERROR('[52486]', 16, 1)
   end

   if exists(select 1 from KRT_BarKodok
            where Kod in (select BarCode from #tempTable)
            and KodType in ('N','P','G') and Allapot<>'S'
         )
   begin
      -- A tértivevénynek megadott vonalkód már használatban van!
      select EREC_KuldKuldemenyek.BarCode
         from EREC_KuldKuldemenyek
         where Id in (
         select #tempTable.Kuldemeny_Id from #tempTable
            inner join KRT_BarKodok on KRT_BarKodok.Kod = #tempTable.BarCode
            and KodType in ('N','P','G') and Allapot<>'S')
        RAISERROR('[52487]', 16, 1)
   end
   /* Vonalkódok ellenőrzése END */

   if @Tranz_id IS NULL
   BEGIN 
      set @Tranz_id = newid();
   END

   DECLARE @insertColumns NVARCHAR(4000)
   DECLARE @insertValues NVARCHAR(4000)
   SET @insertColumns = ''
   SET @insertValues = '' 
       
--         if @Id is not null
--         begin
--            SET @insertColumns = @insertColumns + ',Id'
--            SET @insertValues = @insertValues + ',@Id'
--         end 
       
--         if @Kuldemeny_Id is not null
--         begin
            SET @insertColumns = @insertColumns + ',Kuldemeny_Id'
            SET @insertValues = @insertValues + ',#tempTable.Kuldemeny_Id'
--         end 
--       
--         if @Ragszam is not null
--         begin
            SET @insertColumns = @insertColumns + ',Ragszam'
            SET @insertValues = @insertValues + ',#tempTable.Ragszam'
--         end 
       
         if @TertivisszaKod is not null
         begin
            SET @insertColumns = @insertColumns + ',TertivisszaKod'
            SET @insertValues = @insertValues + ',@TertivisszaKod'
         end 
       
         if @TertivisszaDat is not null
         begin
            SET @insertColumns = @insertColumns + ',TertivisszaDat'
            SET @insertValues = @insertValues + ',@TertivisszaDat'
         end 
       
         if @AtvevoSzemely is not null
         begin
            SET @insertColumns = @insertColumns + ',AtvevoSzemely'
            SET @insertValues = @insertValues + ',@AtvevoSzemely'
         end

         if @AtvetelDat is not null
         begin
            SET @insertColumns = @insertColumns + ',AtvetelDat'
            SET @insertValues = @insertValues + ',@AtvetelDat'
         end  
       
--         if @BarCode is not null
--         begin
            SET @insertColumns = @insertColumns + ',BarCode'
            SET @insertValues = @insertValues + ',#tempTable.BarCode'
--         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
       
         if @KezbVelelemBeallta is not null
         begin
            SET @insertColumns = @insertColumns + ',KezbVelelemBeallta'
            SET @insertValues = @insertValues + ',@KezbVelelemBeallta'
         end    
       
         if @KezbVelelemDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',KezbVelelemDatuma'
            SET @insertValues = @insertValues + ',@KezbVelelemDatuma'
         end    

IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_KuldTertivevenyek ('+@insertColumns+') output inserted.id into @InsertedRow select '+@insertValues+' from #tempTable
'
--SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'
SET @InsertCommand = @InsertCommand + 'select id from @InsertedRow'

exec sp_executesql @InsertCommand, 
N'@TertivisszaKod nvarchar(64),@TertivisszaDat datetime,@AtvevoSzemely Nvarchar(400),@AtvetelDat datetime,@Allapot nvarchar(64),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@KezbVelelemBeallta char(1), @KezbVelelemDatuma datetime'
,@TertivisszaKod = @TertivisszaKod,@TertivisszaDat = @TertivisszaDat,@AtvevoSzemely = @AtvevoSzemely,@AtvetelDat = @AtvetelDat,@Allapot = @Allapot,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id,@KezbVelelemBeallta = @KezbVelelemBeallta,@KezbVelelemDatuma = @KezbVelelemDatuma


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN

   /* History Log */
   declare @row_ids varchar(MAX)
   /*** EREC_KuldTertivevenyek history log ***/

   set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from EREC_KuldTertivevenyek where Tranz_id = @Tranz_id FOR XML PATH('')),1, 2, '')
   if @row_ids is not null
      set @row_ids = @row_ids + '''';

   exec sp_LogRecordsToHistory_Tomeges
    @TableName = 'EREC_KuldTertivevenyek'
   ,@Row_Ids = @row_ids
   ,@Muvelet = 0
   ,@Vegrehajto_Id = @Letrehozo_id
   ,@VegrehajtasIdo = @LetrehozasIdo

END  


   /* Vonalkód kötése, ha meg volt adva START */
   UPDATE KRT_BarKodok
   SET
      Obj_Id = #tempTable.Kuldemeny_Id,
      Obj_type = 'EREC_KuldTertivevenyek',
      Allapot = 'F',
      Ver = Ver + 1,
      Modosito_Id = @Letrehozo_id,
      ModositasIdo = @LetrehozasIdo,
      Tranz_Id = @Tranz_id
   FROM #tempTable
      where #tempTable.BarCode = KRT_BarKodok.Kod

   /* History Log */
-- declare @row_ids varchar(MAX)
   /*** KRT_BarKodok history log ***/

   set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from KRT_BarKodok where Tranz_id = @Tranz_id FOR XML PATH('')),1, 2, '')
   if @row_ids is not null
      set @row_ids = @row_ids + '''';

   exec sp_LogRecordsToHistory_Tomeges
    @TableName = 'KRT_BarKodok'
   ,@Row_Ids = @row_ids
   ,@Muvelet = 1
   ,@Vegrehajto_Id = @Letrehozo_id
   ,@VegrehajtasIdo = @LetrehozasIdo

   /* Vonalkód kötése, ha meg volt adva END */

drop table #tempTable          
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH