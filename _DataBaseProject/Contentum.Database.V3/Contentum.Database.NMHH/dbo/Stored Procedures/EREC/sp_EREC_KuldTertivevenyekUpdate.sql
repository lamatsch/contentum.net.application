﻿
CREATE procedure [dbo].[sp_EREC_KuldTertivevenyekUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Kuldemeny_Id     uniqueidentifier  = null,         
             @Ragszam     Nvarchar(400)  = null,         
             @TertivisszaKod     nvarchar(64)  = null,         
             @TertivisszaDat     datetime  = null,         
             @AtvevoSzemely     Nvarchar(400)  = null,         
             @AtvetelDat     datetime  = null,         
             @BarCode     Nvarchar(100)  = null,         
             @Allapot     nvarchar(64)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
             @KezbVelelemBeallta     char(1)  = null,         
             @KezbVelelemDatuma     datetime  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Kuldemeny_Id     uniqueidentifier         
     DECLARE @Act_Ragszam     Nvarchar(400)         
     DECLARE @Act_TertivisszaKod     nvarchar(64)         
     DECLARE @Act_TertivisszaDat     datetime         
     DECLARE @Act_AtvevoSzemely     Nvarchar(400)         
     DECLARE @Act_AtvetelDat     datetime         
     DECLARE @Act_BarCode     Nvarchar(100)         
     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier         
     DECLARE @Act_KezbVelelemBeallta     char(1)         
     DECLARE @Act_KezbVelelemDatuma     datetime           
  
set nocount on

select 
     @Act_Kuldemeny_Id = Kuldemeny_Id,
     @Act_Ragszam = Ragszam,
     @Act_TertivisszaKod = TertivisszaKod,
     @Act_TertivisszaDat = TertivisszaDat,
     @Act_AtvevoSzemely = AtvevoSzemely,
     @Act_AtvetelDat = AtvetelDat,
     @Act_BarCode = BarCode,
     @Act_Allapot = Allapot,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id,
     @Act_KezbVelelemBeallta = KezbVelelemBeallta,
     @Act_KezbVelelemDatuma = KezbVelelemDatuma
from EREC_KuldTertivevenyek
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zĂˇrolĂˇs ellenĹ‘rzĂ©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Kuldemeny_Id')=1
         SET @Act_Kuldemeny_Id = @Kuldemeny_Id
   IF @UpdatedColumns.exist('/root/Ragszam')=1
         SET @Act_Ragszam = @Ragszam
   IF @UpdatedColumns.exist('/root/TertivisszaKod')=1
         SET @Act_TertivisszaKod = @TertivisszaKod
   IF @UpdatedColumns.exist('/root/TertivisszaDat')=1
         SET @Act_TertivisszaDat = @TertivisszaDat
   IF @UpdatedColumns.exist('/root/AtvevoSzemely')=1
         SET @Act_AtvevoSzemely = @AtvevoSzemely
   IF @UpdatedColumns.exist('/root/AtvetelDat')=1
         SET @Act_AtvetelDat = @AtvetelDat
   IF @UpdatedColumns.exist('/root/BarCode')=1
         SET @Act_BarCode = @BarCode
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id
   IF @UpdatedColumns.exist('/root/KezbVelelemBeallta')=1
         SET @Act_KezbVelelemBeallta = @KezbVelelemBeallta
   IF @UpdatedColumns.exist('/root/KezbVelelemDatuma')=1
         SET @Act_KezbVelelemDatuma = @KezbVelelemDatuma   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_KuldTertivevenyek
SET
     Kuldemeny_Id = @Act_Kuldemeny_Id,
     Ragszam = @Act_Ragszam,
     TertivisszaKod = @Act_TertivisszaKod,
     TertivisszaDat = @Act_TertivisszaDat,
     AtvevoSzemely = @Act_AtvevoSzemely,
     AtvetelDat = @Act_AtvetelDat,
     BarCode = @Act_BarCode,
     Allapot = @Act_Allapot,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id,
     KezbVelelemBeallta = @Act_KezbVelelemBeallta,
     KezbVelelemDatuma = @Act_KezbVelelemDatuma
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_KuldTertivevenyek',@Id
					,'EREC_KuldTertivevenyekHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH