﻿create procedure [dbo].[sp_EREC_Kuldemeny_IratPeldanyaiGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_Kuldemeny_IratPeldanyai.Id,
	   EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id,
	   EREC_Kuldemeny_IratPeldanyai.Peldany_Id,
	   EREC_Kuldemeny_IratPeldanyai.Ver,
	   EREC_Kuldemeny_IratPeldanyai.Note,
	   EREC_Kuldemeny_IratPeldanyai.Stat_id,
	   EREC_Kuldemeny_IratPeldanyai.ErvKezd,
	   EREC_Kuldemeny_IratPeldanyai.ErvVege,
	   EREC_Kuldemeny_IratPeldanyai.Letrehozo_id,
	   EREC_Kuldemeny_IratPeldanyai.LetrehozasIdo,
	   EREC_Kuldemeny_IratPeldanyai.Modosito_id,
	   EREC_Kuldemeny_IratPeldanyai.ModositasIdo,
	   EREC_Kuldemeny_IratPeldanyai.Zarolo_id,
	   EREC_Kuldemeny_IratPeldanyai.ZarolasIdo,
	   EREC_Kuldemeny_IratPeldanyai.Tranz_id,
	   EREC_Kuldemeny_IratPeldanyai.UIAccessLog_id
	   from 
		 EREC_Kuldemeny_IratPeldanyai as EREC_Kuldemeny_IratPeldanyai 
	   where
		 EREC_Kuldemeny_IratPeldanyai.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end