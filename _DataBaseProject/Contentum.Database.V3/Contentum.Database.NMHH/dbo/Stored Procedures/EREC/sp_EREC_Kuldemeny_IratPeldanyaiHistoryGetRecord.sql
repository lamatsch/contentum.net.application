﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_Kuldemeny_IratPeldanyaiHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_EREC_Kuldemeny_IratPeldanyaiHistoryGetRecord
go
*/
create procedure sp_EREC_Kuldemeny_IratPeldanyaiHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_Kuldemeny_IratPeldanyaiHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Kuldemeny_IratPeldanyaiHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_Kuldemeny_IratPeldanyaiHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Kuldemeny_IratPeldanyaiHistory Old
         inner join EREC_Kuldemeny_IratPeldanyaiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Kuldemeny_IratPeldanyaiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldKuldemeny_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_KuldKuldemenyekAzonosito(Old.KuldKuldemeny_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_KuldKuldemenyekAzonosito(New.KuldKuldemeny_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Kuldemeny_IratPeldanyaiHistory Old
         inner join EREC_Kuldemeny_IratPeldanyaiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Kuldemeny_IratPeldanyaiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KuldKuldemeny_Id as nvarchar(max)),'') != ISNULL(CAST(New.KuldKuldemeny_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_KuldKuldemenyek FTOld on FTOld.Id = Old.KuldKuldemeny_Id and FTOld.Ver = Old.Ver
         left join EREC_KuldKuldemenyek FTNew on FTNew.Id = New.KuldKuldemeny_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Peldany_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_PldIratPeldanyokAzonosito(Old.Peldany_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_PldIratPeldanyokAzonosito(New.Peldany_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Kuldemeny_IratPeldanyaiHistory Old
         inner join EREC_Kuldemeny_IratPeldanyaiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Kuldemeny_IratPeldanyaiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Peldany_Id as nvarchar(max)),'') != ISNULL(CAST(New.Peldany_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_PldIratPeldanyok FTOld on FTOld.Id = Old.Peldany_Id and FTOld.Ver = Old.Ver
         left join EREC_PldIratPeldanyok FTNew on FTNew.Id = New.Peldany_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go