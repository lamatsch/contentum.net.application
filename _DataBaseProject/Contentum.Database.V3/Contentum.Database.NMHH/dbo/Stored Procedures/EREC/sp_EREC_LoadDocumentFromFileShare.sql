﻿CREATE PROCEDURE [dbo].[sp_EREC_LoadDocumentFromFileShare]
	@scanServiceUrl varchar(255),
	@userName varchar(255) = '',
	@domain varchar(255) = ''
AS
BEGIN
	DECLARE @url varchar(255)
	DECLARE @obj int
	DECLARE @result int
	DECLARE @source varchar(255)
	DECLARE @desc varchar(255)

	SET @url = @scanServiceUrl + '/LoadDocumentFromFileShare?userName='  + @userName + '&domain=' + @domain

	EXEC sp_OACreate 'MSXML2.ServerXMLHTTP', @obj OUT

	EXEC sp_OASetProperty @obj, 'setTimeouts','30000','30000','30000','1200000'

	EXEC @result = sp_OAMethod @obj, 'open', NULL, 'GET', @url, false

	IF (@result <> 0)
	BEGIN
		EXEC sp_OAGetErrorInfo @obj, @source OUT, @desc OUT
		EXEC sp_OADestroy @obj
		RAISERROR ('Open failed: %s - %s', 16, 1, @desc, @scanServiceUrl)
		RETURN
	END

	EXEC @result = sp_OAMethod @obj, 'send'

	IF (@result <> 0)
	BEGIN
		EXEC sp_OAGetErrorInfo @obj, @source OUT, @desc OUT
		EXEC sp_OADestroy @obj
		RAISERROR ('Send failed: %s - %s', 16, 1, @desc, @scanServiceUrl)
		RETURN
	END

	DECLARE @statusText varchar(1000)
	DECLARE @status varchar(1000)
	DECLARE @response varchar(8000)

	EXEC sp_OAGetProperty @obj, 'StatusText', @statusText OUT
	EXEC sp_OAGetProperty @obj, 'Status', @status OUT
	EXEC sp_OAGetProperty @obj, 'responseText', @response OUT

	EXEC sp_OADestroy @obj

	IF (@status <> '200')
		RAISERROR ('Response failed: %s - %s - %s', 16, 1, @status, @statusText, @scanServiceUrl)
END
GO
