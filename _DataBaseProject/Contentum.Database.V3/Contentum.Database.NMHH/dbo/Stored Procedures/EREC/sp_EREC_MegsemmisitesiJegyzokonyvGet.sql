

CREATE procedure [dbo].[sp_EREC_MegsemmisitesiJegyzokonyvGet]
		 @Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier
       
       
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  '
	  select  ii.azonosito
       ,ij.azonosito as [nyilvszam]
       ,ii.terjedelemmennyiseg
       ,kt.nev as [TerjedelemMennyisegiEgyseg]
       ,pld.sorszam as [pldsorsz]
	   ,ii.Targy as [Targy]
	   ,ij.lezarasdatuma as [lezarasdat]
	   ,kt2.nev as [Minosites]

	  from EREC_IraJegyzekek ij 

		join EREC_IrajegyzekTetelek ijt on ij.id = ijt.jegyzek_id
		join EREC_PldIratPeldanyok pld on ijt.obj_id = pld.id
		join EREC_IraIratok ii on ii.id = pld.irairat_id
		left join KRT_KodCsoportok kcs on kcs.kod =''MENNYISEGI_EGYSEG''
		left join KRT_Kodtarak kt on 
		   ii.TerjedelemMennyisegiEgyseg COLLATE DATABASE_DEFAULT  = kt.kod COLLATE DATABASE_DEFAULT 
		      and kt.kodcsoport_id = kcs.id
		left join KRT_KodCsoportok kcs2 on kcs2.kod =''IRAT_MINOSITES''
		left join KRT_Kodtarak kt2 on 
		   ii.Minosites COLLATE DATABASE_DEFAULT  = kt2.kod COLLATE DATABASE_DEFAULT 
		      and kt2.kodcsoport_id = kcs2.id


       where ij.id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
GO


