﻿create procedure [dbo].[sp_EREC_MellekletekGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_Mellekletek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_Mellekletek.Id,
	   EREC_Mellekletek.KuldKuldemeny_Id,
	   EREC_Mellekletek.IraIrat_Id,
	   EREC_Mellekletek.AdathordozoTipus,
	   EREC_Mellekletek.Megjegyzes,
	   EREC_Mellekletek.SztornirozasDat,
	   EREC_Mellekletek.MennyisegiEgyseg,
	   EREC_Mellekletek.Mennyiseg,
	   EREC_Mellekletek.BarCode,
	   EREC_Mellekletek.Ver,
	   EREC_Mellekletek.Note,
	   EREC_Mellekletek.Stat_id,
	   EREC_Mellekletek.ErvKezd,
	   EREC_Mellekletek.ErvVege,
	   EREC_Mellekletek.Letrehozo_id,
	   EREC_Mellekletek.LetrehozasIdo,
	   EREC_Mellekletek.Modosito_id,
	   EREC_Mellekletek.ModositasIdo,
	   EREC_Mellekletek.Zarolo_id,
	   EREC_Mellekletek.ZarolasIdo,
	   EREC_Mellekletek.Tranz_id,
	   EREC_Mellekletek.UIAccessLog_id,
	   EREC_Mellekletek.MellekletAdathordozoTipus  
   from 
     EREC_Mellekletek as EREC_Mellekletek      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end