﻿CREATE procedure [dbo].[sp_EREC_MellekletekGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by EREC_Mellekletek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
 
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END


  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_Mellekletek.Id,
	   EREC_Mellekletek.KuldKuldemeny_Id,
	   EREC_Mellekletek.IraIrat_Id,
	   EREC_Mellekletek.AdathordozoTipus,
       dbo.[fn_KodtarErtekNeve](''ELSODLEGES_ADATHORDOZO'',MellekletAdathordozoTipus,'''+ CAST(@Org as NVarChar(40)) + ''') as MellekletAdathordozoTipus,
	   dbo.[fn_KodtarErtekNeve](''ADATHORDOZO_TIPUSA'',AdathordozoTipus,''' + CAST(@Org as NVarChar(40)) + ''') as AdathordozoTipusNev,
	   EREC_Mellekletek.Megjegyzes,
	   EREC_Mellekletek.SztornirozasDat,	   
	   EREC_Mellekletek.MennyisegiEgyseg,
	   dbo.[fn_KodtarErtekNeve](''MENNYISEGI_EGYSEG'',MennyisegiEgyseg,''' + CAST(@Org as NVarChar(40)) + ''') as MennyisegiEgysegNev,
	   EREC_Mellekletek.Mennyiseg,	   
       EREC_Mellekletek.BarCode,
    (select count(*) from EREC_IratelemKapcsolatok
     where EREC_IratelemKapcsolatok.Melleklet_Id = EREC_Mellekletek.Id
		   and getdate() between EREC_IratelemKapcsolatok.ErvKezd and EREC_IratelemKapcsolatok.ErvVege) as CsatolmanyCount,
Dokumentum_Id = (case (select count(*) from EREC_IratelemKapcsolatok
     where EREC_IratelemKapcsolatok.Melleklet_Id = EREC_Mellekletek.Id
		   and getdate() between EREC_IratelemKapcsolatok.ErvKezd and EREC_IratelemKapcsolatok.ErvVege)
when 1 then (select Dokumentum_Id from EREC_Csatolmanyok
left join EREC_IratelemKapcsolatok on EREC_IratelemKapcsolatok.Csatolmany_Id = EREC_Csatolmanyok.Id
		where EREC_IratelemKapcsolatok.Melleklet_Id=EREC_Mellekletek.Id
		and getdate() between EREC_IratelemKapcsolatok.ErvKezd and EREC_IratelemKapcsolatok.ErvVege
		  and getdate() between EREC_Csatolmanyok.ErvKezd and EREC_Csatolmanyok.ErvVege)
else null end),
	   EREC_Mellekletek.Ver,
	   EREC_Mellekletek.Note,
	   EREC_Mellekletek.Stat_id,
	   EREC_Mellekletek.ErvKezd,
	   EREC_Mellekletek.ErvVege,
	   EREC_Mellekletek.Letrehozo_id,
	   EREC_Mellekletek.LetrehozasIdo,
	   EREC_Mellekletek.Modosito_id,
	   EREC_Mellekletek.ModositasIdo,
	   EREC_Mellekletek.Zarolo_id,
	   EREC_Mellekletek.ZarolasIdo,
	   EREC_Mellekletek.Tranz_id,
	   EREC_Mellekletek.UIAccessLog_id,
	   dbo.[fn_KodtarErtekNeve](''ELSODLEGES_ADATHORDOZO'',MellekletAdathordozoTipus,'''+ CAST(@Org as NVarChar(40)) + ''') as MellekletAdathordozoTipus
   from 
     EREC_Mellekletek as EREC_Mellekletek
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end