﻿create procedure [dbo].[sp_EREC_Obj_MetaAdataiGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_Obj_MetaAdatai.Id,
	   EREC_Obj_MetaAdatai.Obj_MetaDefinicio_Id,
	   EREC_Obj_MetaAdatai.Targyszavak_Id,
	   EREC_Obj_MetaAdatai.AlapertelmezettErtek,
	   EREC_Obj_MetaAdatai.Sorszam,
	   EREC_Obj_MetaAdatai.Opcionalis,
	   EREC_Obj_MetaAdatai.Ismetlodo,
	   EREC_Obj_MetaAdatai.Funkcio,
	   EREC_Obj_MetaAdatai.ControlTypeSource,
	   EREC_Obj_MetaAdatai.ControlTypeDataSource,
	   EREC_Obj_MetaAdatai.SPSSzinkronizalt,
	   EREC_Obj_MetaAdatai.SPS_Field_Id,
	   EREC_Obj_MetaAdatai.Ver,
	   EREC_Obj_MetaAdatai.Note,
	   EREC_Obj_MetaAdatai.Stat_id,
	   EREC_Obj_MetaAdatai.ErvKezd,
	   EREC_Obj_MetaAdatai.ErvVege,
	   EREC_Obj_MetaAdatai.Letrehozo_Id,
	   EREC_Obj_MetaAdatai.LetrehozasIdo,
	   EREC_Obj_MetaAdatai.Modosito_Id,
	   EREC_Obj_MetaAdatai.ModositasIdo,
	   EREC_Obj_MetaAdatai.Zarolo_Id,
	   EREC_Obj_MetaAdatai.ZarolasIdo,
	   EREC_Obj_MetaAdatai.Tranz_Id,
	   EREC_Obj_MetaAdatai.UIAccessLog_Id
	   from 
		 EREC_Obj_MetaAdatai as EREC_Obj_MetaAdatai 
	   where
		 EREC_Obj_MetaAdatai.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end