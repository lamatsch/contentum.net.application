﻿create procedure [dbo].[sp_EREC_Obj_MetaAdataiHistoryGetAllRecord]
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin

   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_Obj_MetaAdataiHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaAdataiHistory Old
         inner join EREC_Obj_MetaAdataiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaAdataiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Id != New.Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_MetaDefinicio_Id' as ColumnName,               cast(Old.Obj_MetaDefinicio_Id as nvarchar(99)) as OldValue,
               cast(New.Obj_MetaDefinicio_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaAdataiHistory Old
         inner join EREC_Obj_MetaAdataiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaAdataiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Obj_MetaDefinicio_Id != New.Obj_MetaDefinicio_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Targyszavak_Id' as ColumnName,               cast(Old.Targyszavak_Id as nvarchar(99)) as OldValue,
               cast(New.Targyszavak_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaAdataiHistory Old
         inner join EREC_Obj_MetaAdataiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaAdataiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Targyszavak_Id != New.Targyszavak_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlapertelmezettErtek' as ColumnName,               cast(Old.AlapertelmezettErtek as nvarchar(99)) as OldValue,
               cast(New.AlapertelmezettErtek as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaAdataiHistory Old
         inner join EREC_Obj_MetaAdataiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaAdataiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.AlapertelmezettErtek != New.AlapertelmezettErtek 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Sorszam' as ColumnName,               cast(Old.Sorszam as nvarchar(99)) as OldValue,
               cast(New.Sorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaAdataiHistory Old
         inner join EREC_Obj_MetaAdataiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaAdataiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Sorszam != New.Sorszam 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Opcionalis' as ColumnName,               cast(Old.Opcionalis as nvarchar(99)) as OldValue,
               cast(New.Opcionalis as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaAdataiHistory Old
         inner join EREC_Obj_MetaAdataiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaAdataiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Opcionalis != New.Opcionalis 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ismetlodo' as ColumnName,               cast(Old.Ismetlodo as nvarchar(99)) as OldValue,
               cast(New.Ismetlodo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaAdataiHistory Old
         inner join EREC_Obj_MetaAdataiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaAdataiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Ismetlodo != New.Ismetlodo 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Funkcio' as ColumnName,               cast(Old.Funkcio as nvarchar(99)) as OldValue,
               cast(New.Funkcio as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaAdataiHistory Old
         inner join EREC_Obj_MetaAdataiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaAdataiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Funkcio != New.Funkcio 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ControlTypeSource' as ColumnName,               cast(Old.ControlTypeSource as nvarchar(99)) as OldValue,
               cast(New.ControlTypeSource as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaAdataiHistory Old
         inner join EREC_Obj_MetaAdataiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaAdataiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.ControlTypeSource != New.ControlTypeSource 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ControlTypeDataSource' as ColumnName,               cast(Old.ControlTypeDataSource as nvarchar(99)) as OldValue,
               cast(New.ControlTypeDataSource as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaAdataiHistory Old
         inner join EREC_Obj_MetaAdataiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaAdataiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.ControlTypeDataSource != New.ControlTypeDataSource 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SPSSzinkronizalt' as ColumnName,               cast(Old.SPSSzinkronizalt as nvarchar(99)) as OldValue,
               cast(New.SPSSzinkronizalt as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaAdataiHistory Old
         inner join EREC_Obj_MetaAdataiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaAdataiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.SPSSzinkronizalt != New.SPSSzinkronizalt 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SPS_Field_Id' as ColumnName,               cast(Old.SPS_Field_Id as nvarchar(99)) as OldValue,
               cast(New.SPS_Field_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaAdataiHistory Old
         inner join EREC_Obj_MetaAdataiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaAdataiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.SPS_Field_Id != New.SPS_Field_Id 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end