﻿create procedure [dbo].[sp_EREC_Obj_MetaAdataiUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Obj_MetaDefinicio_Id     uniqueidentifier  = null,         
             @Targyszavak_Id     uniqueidentifier  = null,         
             @AlapertelmezettErtek     Nvarchar(100)  = null,         
             @Sorszam     int  = null,         
             @Opcionalis     char(1)  = null,         
             @Ismetlodo     char(1)  = null,         
             @Funkcio     Nvarchar(100)  = null,         
             @ControlTypeSource     nvarchar(64)  = null,         
             @ControlTypeDataSource     Nvarchar(4000)  = null,         
             @SPSSzinkronizalt     char(1)  = null,         
             @SPS_Field_Id     uniqueidentifier  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_Id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_Id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_Id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_Id     uniqueidentifier  = null,         
             @UIAccessLog_Id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Obj_MetaDefinicio_Id     uniqueidentifier         
     DECLARE @Act_Targyszavak_Id     uniqueidentifier         
     DECLARE @Act_AlapertelmezettErtek     Nvarchar(100)         
     DECLARE @Act_Sorszam     int         
     DECLARE @Act_Opcionalis     char(1)         
     DECLARE @Act_Ismetlodo     char(1)         
     DECLARE @Act_Funkcio     Nvarchar(100)         
     DECLARE @Act_ControlTypeSource     nvarchar(64)         
     DECLARE @Act_ControlTypeDataSource     Nvarchar(4000)         
     DECLARE @Act_SPSSzinkronizalt     char(1)         
     DECLARE @Act_SPS_Field_Id     uniqueidentifier         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_Id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_Id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_Id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_Id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_Id     uniqueidentifier           
  
set nocount on

select 
     @Act_Obj_MetaDefinicio_Id = Obj_MetaDefinicio_Id,
     @Act_Targyszavak_Id = Targyszavak_Id,
     @Act_AlapertelmezettErtek = AlapertelmezettErtek,
     @Act_Sorszam = Sorszam,
     @Act_Opcionalis = Opcionalis,
     @Act_Ismetlodo = Ismetlodo,
     @Act_Funkcio = Funkcio,
     @Act_ControlTypeSource = ControlTypeSource,
     @Act_ControlTypeDataSource = ControlTypeDataSource,
     @Act_SPSSzinkronizalt = SPSSzinkronizalt,
     @Act_SPS_Field_Id = SPS_Field_Id,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_Id = Letrehozo_Id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_Id = Modosito_Id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_Id = Zarolo_Id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_Id = Tranz_Id,
     @Act_UIAccessLog_Id = UIAccessLog_Id
from EREC_Obj_MetaAdatai
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Obj_MetaDefinicio_Id')=1
         SET @Act_Obj_MetaDefinicio_Id = @Obj_MetaDefinicio_Id
   IF @UpdatedColumns.exist('/root/Targyszavak_Id')=1
         SET @Act_Targyszavak_Id = @Targyszavak_Id
   IF @UpdatedColumns.exist('/root/AlapertelmezettErtek')=1
         SET @Act_AlapertelmezettErtek = @AlapertelmezettErtek
   IF @UpdatedColumns.exist('/root/Sorszam')=1
         SET @Act_Sorszam = @Sorszam
   IF @UpdatedColumns.exist('/root/Opcionalis')=1
         SET @Act_Opcionalis = @Opcionalis
   IF @UpdatedColumns.exist('/root/Ismetlodo')=1
         SET @Act_Ismetlodo = @Ismetlodo
   IF @UpdatedColumns.exist('/root/Funkcio')=1
         SET @Act_Funkcio = @Funkcio
   IF @UpdatedColumns.exist('/root/ControlTypeSource')=1
         SET @Act_ControlTypeSource = @ControlTypeSource
   IF @UpdatedColumns.exist('/root/ControlTypeDataSource')=1
         SET @Act_ControlTypeDataSource = @ControlTypeDataSource
   IF @UpdatedColumns.exist('/root/SPSSzinkronizalt')=1
         SET @Act_SPSSzinkronizalt = @SPSSzinkronizalt
   IF @UpdatedColumns.exist('/root/SPS_Field_Id')=1
         SET @Act_SPS_Field_Id = @SPS_Field_Id
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_Id')=1
         SET @Act_Letrehozo_Id = @Letrehozo_Id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_Id')=1
         SET @Act_Modosito_Id = @Modosito_Id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_Id')=1
         SET @Act_Zarolo_Id = @Zarolo_Id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_Id')=1
         SET @Act_Tranz_Id = @Tranz_Id
   IF @UpdatedColumns.exist('/root/UIAccessLog_Id')=1
         SET @Act_UIAccessLog_Id = @UIAccessLog_Id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_Obj_MetaAdatai
SET
     Obj_MetaDefinicio_Id = @Act_Obj_MetaDefinicio_Id,
     Targyszavak_Id = @Act_Targyszavak_Id,
     AlapertelmezettErtek = @Act_AlapertelmezettErtek,
     Sorszam = @Act_Sorszam,
     Opcionalis = @Act_Opcionalis,
     Ismetlodo = @Act_Ismetlodo,
     Funkcio = @Act_Funkcio,
     ControlTypeSource = @Act_ControlTypeSource,
     ControlTypeDataSource = @Act_ControlTypeDataSource,
     SPSSzinkronizalt = @Act_SPSSzinkronizalt,
     SPS_Field_Id = @Act_SPS_Field_Id,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_Id = @Act_Letrehozo_Id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_Id = @Act_Modosito_Id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_Id = @Act_Zarolo_Id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_Id = @Act_Tranz_Id,
     UIAccessLog_Id = @Act_UIAccessLog_Id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_Obj_MetaAdatai',@Id
					,'EREC_Obj_MetaAdataiHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH