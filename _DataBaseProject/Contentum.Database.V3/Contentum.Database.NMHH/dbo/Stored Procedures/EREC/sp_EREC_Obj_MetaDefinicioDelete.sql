﻿create procedure [dbo].[sp_EREC_Obj_MetaDefinicioDelete] 
  @Id uniqueidentifier,
  @ExecutorUserId uniqueidentifier,
  @ExecutionTime datetime

as
begin

BEGIN TRY
--BEGIN TRANSACTION DeleteTransaction
  
	set nocount on

-- Zarolas ellenorzese:
	DECLARE @IsLockedByOtherUser BIT
	exec sp_GetLockingInfo 'EREC_Obj_MetaDefinicio',@Id,@ExecutorUserId,@IsLockedByOtherUser OUTPUT,null

	if (@IsLockedByOtherUser = 0)
	BEGIN	
         /* History Log */
      exec sp_LogDeleteToHistory 'EREC_Obj_MetaDefinicio',@Id
                 ,'EREC_Obj_MetaDefinicioHistory',@ExecutorUserId,@ExecutionTime   
		delete from EREC_Obj_MetaDefinicio 
		where Id = @Id

		if @@rowcount != 1
		begin
			RAISERROR('[50501]',16,1)
			return @@error
		end
	END
	ELSE BEGIN
		RAISERROR('[50599]',16,1)			
	END

--COMMIT TRANSACTION DeleteTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION DeleteTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()	
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end