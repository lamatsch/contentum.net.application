﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_Obj_MetaDefinicioHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_EREC_Obj_MetaDefinicioHistoryGetRecord
go
*/
create procedure sp_EREC_Obj_MetaDefinicioHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_Obj_MetaDefinicioHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_Obj_MetaDefinicioHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaDefinicioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Org' as ColumnName,               
               cast(Old.Org as nvarchar(99)) as OldValue,
               cast(New.Org as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaDefinicioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Org as nvarchar(max)),'') != ISNULL(CAST(New.Org as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Objtip_Id' as ColumnName,               
               cast(Old.Objtip_Id as nvarchar(99)) as OldValue,
               cast(New.Objtip_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaDefinicioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Objtip_Id as nvarchar(max)),'') != ISNULL(CAST(New.Objtip_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Objtip_Id_Column' as ColumnName,               
               cast(Old.Objtip_Id_Column as nvarchar(99)) as OldValue,
               cast(New.Objtip_Id_Column as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaDefinicioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Objtip_Id_Column as nvarchar(max)),'') != ISNULL(CAST(New.Objtip_Id_Column as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ColumnValue' as ColumnName,               
               cast(Old.ColumnValue as nvarchar(99)) as OldValue,
               cast(New.ColumnValue as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaDefinicioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ColumnValue as nvarchar(max)),'') != ISNULL(CAST(New.ColumnValue as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DefinicioTipus' as ColumnName,               
               cast(Old.DefinicioTipus as nvarchar(99)) as OldValue,
               cast(New.DefinicioTipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaDefinicioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.DefinicioTipus as nvarchar(max)),'') != ISNULL(CAST(New.DefinicioTipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felettes_Obj_Meta' as ColumnName,               
               cast(Old.Felettes_Obj_Meta as nvarchar(99)) as OldValue,
               cast(New.Felettes_Obj_Meta as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaDefinicioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Felettes_Obj_Meta as nvarchar(max)),'') != ISNULL(CAST(New.Felettes_Obj_Meta as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MetaXSD' as ColumnName,               
               cast(Old.MetaXSD as nvarchar(99)) as OldValue,
               cast(New.MetaXSD as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaDefinicioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MetaXSD as nvarchar(max)),'') != ISNULL(CAST(New.MetaXSD as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ImportXML' as ColumnName,               
               cast(Old.ImportXML as nvarchar(99)) as OldValue,
               cast(New.ImportXML as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaDefinicioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ImportXML as nvarchar(max)),'') != ISNULL(CAST(New.ImportXML as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EgyebXML' as ColumnName,               
               cast(Old.EgyebXML as nvarchar(99)) as OldValue,
               cast(New.EgyebXML as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaDefinicioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EgyebXML as nvarchar(max)),'') != ISNULL(CAST(New.EgyebXML as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ContentType' as ColumnName,               
               cast(Old.ContentType as nvarchar(99)) as OldValue,
               cast(New.ContentType as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaDefinicioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ContentType as nvarchar(max)),'') != ISNULL(CAST(New.ContentType as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SPSSzinkronizalt' as ColumnName,               
               cast(Old.SPSSzinkronizalt as nvarchar(99)) as OldValue,
               cast(New.SPSSzinkronizalt as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaDefinicioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SPSSzinkronizalt as nvarchar(max)),'') != ISNULL(CAST(New.SPSSzinkronizalt as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SPS_CTT_Id' as ColumnName,               
               cast(Old.SPS_CTT_Id as nvarchar(99)) as OldValue,
               cast(New.SPS_CTT_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaDefinicioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SPS_CTT_Id as nvarchar(max)),'') != ISNULL(CAST(New.SPS_CTT_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'WorkFlowVezerles' as ColumnName,               
               cast(Old.WorkFlowVezerles as nvarchar(99)) as OldValue,
               cast(New.WorkFlowVezerles as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_Obj_MetaDefinicioHistory Old
         inner join EREC_Obj_MetaDefinicioHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_Obj_MetaDefinicioHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.WorkFlowVezerles as nvarchar(max)),'') != ISNULL(CAST(New.WorkFlowVezerles as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go