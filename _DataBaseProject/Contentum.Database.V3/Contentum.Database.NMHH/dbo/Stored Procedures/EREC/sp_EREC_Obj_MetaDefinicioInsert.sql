﻿create procedure [dbo].[sp_EREC_Obj_MetaDefinicioInsert]    
                @Id      uniqueidentifier = null,    
                           @Org     uniqueidentifier = null,    
                @Objtip_Id     uniqueidentifier  = null,
                @Objtip_Id_Column     uniqueidentifier  = null,
                @ColumnValue     Nvarchar(100)  = null,
                @DefinicioTipus     nvarchar(64)  = null,
                @Felettes_Obj_Meta     uniqueidentifier  = null,
                @MetaXSD     xml  = null,
                @ImportXML     xml  = null,
                @EgyebXML     xml  = null,
                @ContentType     Nvarchar(100)  = null,
                @SPSSzinkronizalt     char(1)  = null,
                @SPS_CTT_Id     Nvarchar(400)  = null,
                @WorkFlowVezerles     char(1)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
 IF @ORG is NULL
 BEGIN 
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@Letrehozo_id)
   if (@Org is null)
   begin
   	RAISERROR('[50302]',16,1)
   end
END
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Org is not null
         begin
            SET @insertColumns = @insertColumns + ',Org'
            SET @insertValues = @insertValues + ',@Org'
         end 
       
         if @Objtip_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Objtip_Id'
            SET @insertValues = @insertValues + ',@Objtip_Id'
         end 
       
         if @Objtip_Id_Column is not null
         begin
            SET @insertColumns = @insertColumns + ',Objtip_Id_Column'
            SET @insertValues = @insertValues + ',@Objtip_Id_Column'
         end 
       
         if @ColumnValue is not null
         begin
            SET @insertColumns = @insertColumns + ',ColumnValue'
            SET @insertValues = @insertValues + ',@ColumnValue'
         end 
       
         if @DefinicioTipus is not null
         begin
            SET @insertColumns = @insertColumns + ',DefinicioTipus'
            SET @insertValues = @insertValues + ',@DefinicioTipus'
         end 
       
         if @Felettes_Obj_Meta is not null
         begin
            SET @insertColumns = @insertColumns + ',Felettes_Obj_Meta'
            SET @insertValues = @insertValues + ',@Felettes_Obj_Meta'
         end 
       
         if @MetaXSD is not null
         begin
            SET @insertColumns = @insertColumns + ',MetaXSD'
            SET @insertValues = @insertValues + ',@MetaXSD'
         end 
       
         if @ImportXML is not null
         begin
            SET @insertColumns = @insertColumns + ',ImportXML'
            SET @insertValues = @insertValues + ',@ImportXML'
         end 
       
         if @EgyebXML is not null
         begin
            SET @insertColumns = @insertColumns + ',EgyebXML'
            SET @insertValues = @insertValues + ',@EgyebXML'
         end 
       
         if @ContentType is not null
         begin
            SET @insertColumns = @insertColumns + ',ContentType'
            SET @insertValues = @insertValues + ',@ContentType'
         end 
       
         if @SPSSzinkronizalt is not null
         begin
            SET @insertColumns = @insertColumns + ',SPSSzinkronizalt'
            SET @insertValues = @insertValues + ',@SPSSzinkronizalt'
         end 
       
         if @SPS_CTT_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',SPS_CTT_Id'
            SET @insertValues = @insertValues + ',@SPS_CTT_Id'
         end 
       
         if @WorkFlowVezerles is not null
         begin
            SET @insertColumns = @insertColumns + ',WorkFlowVezerles'
            SET @insertValues = @insertValues + ',@WorkFlowVezerles'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_Obj_MetaDefinicio ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Org uniqueidentifier,@Objtip_Id uniqueidentifier,@Objtip_Id_Column uniqueidentifier,@ColumnValue Nvarchar(100),@DefinicioTipus nvarchar(64),@Felettes_Obj_Meta uniqueidentifier,@MetaXSD xml,@ImportXML xml,@EgyebXML xml,@ContentType Nvarchar(100),@SPSSzinkronizalt char(1),@SPS_CTT_Id Nvarchar(400),@WorkFlowVezerles char(1),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Org = @Org,@Objtip_Id = @Objtip_Id,@Objtip_Id_Column = @Objtip_Id_Column,@ColumnValue = @ColumnValue,@DefinicioTipus = @DefinicioTipus,@Felettes_Obj_Meta = @Felettes_Obj_Meta,@MetaXSD = @MetaXSD,@ImportXML = @ImportXML,@EgyebXML = @EgyebXML,@ContentType = @ContentType,@SPSSzinkronizalt = @SPSSzinkronizalt,@SPS_CTT_Id = @SPS_CTT_Id,@WorkFlowVezerles = @WorkFlowVezerles,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_Obj_MetaDefinicio',@ResultUid
					,'EREC_Obj_MetaDefinicioHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH