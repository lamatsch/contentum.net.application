﻿create procedure [dbo].[sp_EREC_ObjektumTargyszavaiGetAllMetaByObjectType]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_Obj_MetaAdatai.Sorszam',
  @TopRow nvarchar(5) = '',
  @ObjTip_Kod nvarchar(100) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
       NULL AS Id,
       NULL AS Ertek,
  	   EREC_Obj_MetaAdatai.Id AS Obj_Metaadatai_Id,
	   EREC_Obj_MetaAdatai.ObjTip_Id,
	   EREC_Obj_MetaAdatai.Targyszavak_Id AS Targyszo_Id,
	   EREC_Obj_MetaAdatai.Sorszam,
       ''04'' AS Forras,
	   EREC_Obj_MetaAdatai.Ver,
	   EREC_Obj_MetaAdatai.Note,
	   EREC_Obj_MetaAdatai.Stat_id,
	   EREC_Obj_MetaAdatai.ErvKezd,
	   EREC_Obj_MetaAdatai.ErvVege,
	   EREC_Obj_MetaAdatai.Letrehozo_Id,
	   EREC_Obj_MetaAdatai.LetrehozasIdo,
	   EREC_Obj_MetaAdatai.Modosito_Id,
	   EREC_Obj_MetaAdatai.ModositasIdo,
	   EREC_Obj_MetaAdatai.Zarolo_Id,
	   EREC_Obj_MetaAdatai.ZarolasIdo,
	   EREC_Obj_MetaAdatai.Tranz_Id,
	   EREC_Obj_MetaAdatai.UIAccessLog_Id,
	   EREC_TargySzavak.TargySzavak AS Targyszo,
	   EREC_TargySzavak.Csoport_Id_Tulaj,
	   EREC_TargySzavak.Tipus
   from 
     EREC_Obj_MetaAdatai as EREC_Obj_MetaAdatai
	 LEFT JOIN EREC_TargySzavak as EREC_TargySzavak
	 ON EREC_Obj_MetaAdatai.Targyszavak_Id = EREC_TargySzavak.Id
     AND EREC_Obj_MetaAdatai.ErvKezd < getdate() AND EREC_Obj_MetaAdatai.ErvVege > getdate()
     AND EREC_TargySzavak.ErvKezd < getdate() AND EREC_TargySzavak.ErvVege > getdate()  
           '

    DECLARE @LocalWhere nvarchar(4000)
    SET @LocalWhere = 'EREC_Obj_MetaAdatai.Tipus = ''O'''

	IF @ObjTip_Kod is not null and @ObjTip_Kod != ''
	BEGIN
        DECLARE @ObjTip_Id uniqueidentifier
        SET @ObjTip_Id = (SELECT TOP 1 Id FROM KRT_ObjTipusok WHERE Kod = @ObjTip_Kod)
        IF @ObjTip_Id is null
            RAISERROR('Az objektum típus nem azonosítható!', 16, 1)
        ELSE 
        SET @LocalWhere = @LocalWhere + ' AND ObjTip_Id = ''' + CONVERT(nvarchar(36), @ObjTip_Id) + ''''
    END

    if @Where is null or @Where = ''
    begin
       SET @Where = @LocalWhere
    end
    else
    begin
       SET @Where = @Where + ' and ' + @LocalWhere
    end

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end