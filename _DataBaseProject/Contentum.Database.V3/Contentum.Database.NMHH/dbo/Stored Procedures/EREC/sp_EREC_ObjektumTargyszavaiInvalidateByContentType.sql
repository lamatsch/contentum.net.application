﻿create procedure [dbo].[sp_EREC_ObjektumTargyszavaiInvalidateByContentType]
  @Obj_Id uniqueidentifier, -- az adott objektum egy rekordjának azonosítója (Id)
  @ObjTip_Id uniqueidentifier = null,
  @ObjTip_Kod nvarchar(100) = '',
  @ContentType nvarchar(2) = '', -- ContentType egyértelmuen azonosít egy metadefiníciót
  @CsakSajatSzint varchar(1) = '0',        -- '0' minden szinten érvénytelenítés, '1' csak a saját szinten
  @ExecutorUserId uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByCsoportId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	END

   declare @kt_Automatikus nvarchar(64)
   set @kt_Automatikus = '01' -- Automatikus (kcs: TARGYSZO_FORRAS)

   -- Dátum
   declare @Now datetime
   set @Now = getdate()

-- struktúra kinyerése a temporary eredménytáblába
   if OBJECT_ID('tempdb..#temp_ResultTable') is not null
   begin
       drop table #temp_ResultTable
   end
       select top 0 * into #temp_ResultTable 
       from EREC_ObjektumTargyszavai

   if @ContentType is null or @ContentType = ''
      RAISERROR('[56104]',16,1) -- Nincs megadva a ContentType!

   if @Obj_Id is null
      RAISERROR('[56100]',16,1) -- Nincs megadva az objektum azonosítója!
   if @ObjTip_Id is null AND @ObjTip_Kod is null
      RAISERROR('[56101]',16,1) -- Nincs megadva az objektum típusa (azonosító vagy kód)!

   if (@ObjTip_Id IS null)
   begin
      SET @ObjTip_Id = (select top 1 Id from KRT_ObjTipusok where Kod = @ObjTip_Kod)
      if @ObjTip_Id is null
         RAISERROR('[56102]',16,1) -- Nem azonosítható az objektum típusa (nem létezo kód)!

   end
   else
   begin
     SET @ObjTip_Kod = (select Kod from KRT_ObjTipusok where Id = @ObjTip_Id)
      if @ObjTip_Kod is null
         RAISERROR('[56103]',16,1) -- Nem azonosítható az objektum típusa (nem létezo id)!
   end

-- érvénytelenítendo hozzárendelések kinyerése temporary eredménytáblába
   if OBJECT_ID('tempdb..#temp_ObjektumTargyszavai') is not null
   begin
       drop table #temp_ObjektumTargyszavai
   end
   -- insert-exec: le kell képezni a tárolt eljárás result setjének pontos szerkezetét!!!
   create table #temp_ObjektumTargyszavai
   (
      Id uniqueidentifier not null,
      ObjTip_Id uniqueidentifier null,
      Ertek nvarchar(max) null,
      Obj_Metaadatai_Id uniqueidentifier null,
      Targyszavak_Id uniqueidentifier null,
      Sorszam int,
      Forras nvarchar(100) null,
      ForrasNev nvarchar(100) null,
      Targyszo nvarchar(100) null,
      Csoport_Id_Tulaj uniqueidentifier null,
      Tipus char(1) null,
      BelsoAzonosito nvarchar(100) null,
      RegExp nvarchar(100) null,
      Funkcio nvarchar(100) null,
      ErvKezd datetime,
      ErvVege datetime,
      Ver int,
      Note nvarchar(4000) null
    )

   insert into #temp_ObjektumTargyszavai
       exec sp_EREC_ObjektumTargyszavaiGetAllMetaByContentType
           @Where=N' Id is not null',
           @ExecutorUserId = @ExecutorUserId,
           @Obj_Id = @Obj_Id,
           @ObjTip_Id = @ObjTip_Id,
           @ContentType = @ContentType,
           @CsakSajatSzint = @CsakSajatSzint,
           @CsakAutomatikus='1'
                  
-- Érvényes hozzárendelések meghatározása
    -- valtozok deklaralasa
    declare @ObjektumTargyszavai_RecordId uniqueidentifier
    
    declare  @Obj_Metaadatai_Id uniqueidentifier

        -- cursor deklaralasa
        declare objektumtargyszavai_sor cursor local for
                select Id from EREC_ObjektumTargyszavai ot
                    where ot.Obj_Id = @Obj_Id
                    and ot.ObjTip_Id = @ObjTip_Id
                    and ot.Forras = @kt_Automatikus
                    and getdate() between ot.ErvKezd and ot.ErvVege
                    and ot.Obj_MetaAdatai_Id in (select Id from #Obj_MetaAdatai)

        open objektumtargyszavai_sor

        -- ciklus kezdete elotti fetch
        fetch objektumtargyszavai_sor into @ObjektumTargyszavai_RecordId

        while @@Fetch_Status = 0
        begin
           if (select Id from EREC_ObjektumTargyszavai
                   where Id = @ObjektumTargyszavai_RecordId
               ) is not null
           begin
               exec sp_EREC_ObjektumTargyszavaiInvalidate
                   @Id = @ObjektumTargyszavai_RecordId,
                   @ExecutorUserId = @ExecutorUserId,
                   @ExecutionTime = @Now

                   if (@@error <> 0)
                       RAISERROR('[56111]',16,1) -- Hiba történt az automatikus metaadat érvénytelenítés során!
                   else
                   begin
                       insert into #temp_ResultTable
                           select * from EREC_ObjektumTargyszavai ot
                               where ot.Id = @ObjektumTargyszavai_RecordId
                   end
           end
           fetch objektumtargyszavai_sor into @ObjektumTargyszavai_RecordId
       end

       close objektumtargyszavai_sor
       deallocate objektumtargyszavai_sor

-- érvénytelenített rekordok visszaadása
      select * from #temp_ResultTable
-- takarítás
       if OBJECT_ID('tempdb..#temp_ObjektumTargyszavai') is not null
       begin
           drop table #temp_ObjektumTargyszavai
       end
       
       if OBJECT_ID('tempdb..#temp_ResultTable') is not null 
       begin
          drop table #temp_ResultTable
       end 


END TRY
BEGIN CATCH
        DECLARE @errorSeverity INT, @errorState INT
        DECLARE @errorCode NVARCHAR(1000)    
        SET @errorSeverity = ERROR_SEVERITY()
        SET @errorState = ERROR_STATE()
        
        if ERROR_NUMBER()<50000        
                SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
        else
                SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1
   
    

        RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end