﻿create procedure [dbo].[sp_EREC_ObjektumTargyszavaiUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Targyszo_Id     uniqueidentifier  = null,         
             @Obj_Metaadatai_Id     uniqueidentifier  = null,         
             @Obj_Id     uniqueidentifier  = null,         
             @ObjTip_Id     uniqueidentifier  = null,         
             @Targyszo     Nvarchar(100)  = null,         
             @Forras     nvarchar(64)  = null,         
             @SPSSzinkronizalt     char(1)  = null,         
             @Ertek     Nvarchar(max)  = null,         
             @Sorszam     int  = null,         
             @Targyszo_XML     xml  = null,         
             @Targyszo_XML_Ervenyes     char(1)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Targyszo_Id     uniqueidentifier         
     DECLARE @Act_Obj_Metaadatai_Id     uniqueidentifier         
     DECLARE @Act_Obj_Id     uniqueidentifier         
     DECLARE @Act_ObjTip_Id     uniqueidentifier         
     DECLARE @Act_Targyszo     Nvarchar(100)         
     DECLARE @Act_Forras     nvarchar(64)         
     DECLARE @Act_SPSSzinkronizalt     char(1)         
     DECLARE @Act_Ertek     Nvarchar(max)         
     DECLARE @Act_Sorszam     int         
     DECLARE @Act_Targyszo_XML     xml         
     DECLARE @Act_Targyszo_XML_Ervenyes     char(1)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Targyszo_Id = Targyszo_Id,
     @Act_Obj_Metaadatai_Id = Obj_Metaadatai_Id,
     @Act_Obj_Id = Obj_Id,
     @Act_ObjTip_Id = ObjTip_Id,
     @Act_Targyszo = Targyszo,
     @Act_Forras = Forras,
     @Act_SPSSzinkronizalt = SPSSzinkronizalt,
     @Act_Ertek = Ertek,
     @Act_Sorszam = Sorszam,
     @Act_Targyszo_XML = Targyszo_XML,
     @Act_Targyszo_XML_Ervenyes = Targyszo_XML_Ervenyes,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_ObjektumTargyszavai
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zA?rolA?s ellenL‘rzA©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Targyszo_Id')=1
         SET @Act_Targyszo_Id = @Targyszo_Id
   IF @UpdatedColumns.exist('/root/Obj_Metaadatai_Id')=1
         SET @Act_Obj_Metaadatai_Id = @Obj_Metaadatai_Id
   IF @UpdatedColumns.exist('/root/Obj_Id')=1
         SET @Act_Obj_Id = @Obj_Id
   IF @UpdatedColumns.exist('/root/ObjTip_Id')=1
         SET @Act_ObjTip_Id = @ObjTip_Id
   IF @UpdatedColumns.exist('/root/Targyszo')=1
         SET @Act_Targyszo = @Targyszo
   IF @UpdatedColumns.exist('/root/Forras')=1
         SET @Act_Forras = @Forras
   IF @UpdatedColumns.exist('/root/SPSSzinkronizalt')=1
         SET @Act_SPSSzinkronizalt = @SPSSzinkronizalt
   IF @UpdatedColumns.exist('/root/Ertek')=1
         SET @Act_Ertek = @Ertek
   IF @UpdatedColumns.exist('/root/Sorszam')=1
         SET @Act_Sorszam = @Sorszam
   IF @UpdatedColumns.exist('/root/Targyszo_XML')=1
         SET @Act_Targyszo_XML = @Targyszo_XML
   IF @UpdatedColumns.exist('/root/Targyszo_XML_Ervenyes')=1
         SET @Act_Targyszo_XML_Ervenyes = @Targyszo_XML_Ervenyes
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_ObjektumTargyszavai
SET
     Targyszo_Id = @Act_Targyszo_Id,
     Obj_Metaadatai_Id = @Act_Obj_Metaadatai_Id,
     Obj_Id = @Act_Obj_Id,
     ObjTip_Id = @Act_ObjTip_Id,
     Targyszo = @Act_Targyszo,
     Forras = @Act_Forras,
     SPSSzinkronizalt = @Act_SPSSzinkronizalt,
     Ertek = @Act_Ertek,
     Sorszam = @Act_Sorszam,
     Targyszo_XML = @Act_Targyszo_XML,
     Targyszo_XML_Ervenyes = @Act_Targyszo_XML_Ervenyes,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_ObjektumTargyszavai',@Id
					,'EREC_ObjektumTargyszavaiHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH