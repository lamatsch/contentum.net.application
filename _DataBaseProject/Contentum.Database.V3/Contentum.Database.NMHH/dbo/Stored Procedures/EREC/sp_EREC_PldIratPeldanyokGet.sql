﻿
create procedure sp_EREC_PldIratPeldanyokGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_PldIratPeldanyok.Id,
	   EREC_PldIratPeldanyok.IraIrat_Id,
	   EREC_PldIratPeldanyok.UgyUgyirat_Id_Kulso,
	   EREC_PldIratPeldanyok.Sorszam,
	   EREC_PldIratPeldanyok.SztornirozasDat,
	   EREC_PldIratPeldanyok.AtvetelDatuma,
	   EREC_PldIratPeldanyok.Eredet,
	   EREC_PldIratPeldanyok.KuldesMod,
	   EREC_PldIratPeldanyok.Ragszam,
	   EREC_PldIratPeldanyok.UgyintezesModja,
	   EREC_PldIratPeldanyok.VisszaerkezesiHatarido,
	   EREC_PldIratPeldanyok.Visszavarolag,
	   EREC_PldIratPeldanyok.VisszaerkezesDatuma,
	   EREC_PldIratPeldanyok.Cim_id_Cimzett,
	   EREC_PldIratPeldanyok.Partner_Id_Cimzett,
	   EREC_PldIratPeldanyok.Partner_Id_CimzettKapcsolt,
	   EREC_PldIratPeldanyok.Csoport_Id_Felelos,
	   EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo,
	   EREC_PldIratPeldanyok.Csoport_Id_Felelos_Elozo,
	   EREC_PldIratPeldanyok.CimSTR_Cimzett,
	   EREC_PldIratPeldanyok.NevSTR_Cimzett,
	   EREC_PldIratPeldanyok.Tovabbito,
	   EREC_PldIratPeldanyok.IraIrat_Id_Kapcsolt,
	   EREC_PldIratPeldanyok.IrattariHely,
	   EREC_PldIratPeldanyok.Gener_Id,
	   EREC_PldIratPeldanyok.PostazasDatuma,
	   EREC_PldIratPeldanyok.BarCode,
	   EREC_PldIratPeldanyok.Allapot,
	   EREC_PldIratPeldanyok.Azonosito,
	   EREC_PldIratPeldanyok.Kovetkezo_Felelos_Id,
	   EREC_PldIratPeldanyok.Elektronikus_Kezbesitesi_Allap,
	   EREC_PldIratPeldanyok.Kovetkezo_Orzo_Id,
	   EREC_PldIratPeldanyok.Fizikai_Kezbesitesi_Allapot,
	   EREC_PldIratPeldanyok.TovabbitasAlattAllapot,
	   EREC_PldIratPeldanyok.PostazasAllapot,
	   EREC_PldIratPeldanyok.ValaszElektronikus,
	   EREC_PldIratPeldanyok.SelejtezesDat,
	   EREC_PldIratPeldanyok.FelhCsoport_Id_Selejtezo,
	   EREC_PldIratPeldanyok.LeveltariAtvevoNeve,	   
	   EREC_PldIratPeldanyok.IrattarId,
	   EREC_PldIratPeldanyok.Ver,
	   EREC_PldIratPeldanyok.Note,
	   EREC_PldIratPeldanyok.Stat_id,
	   EREC_PldIratPeldanyok.ErvKezd,
	   EREC_PldIratPeldanyok.ErvVege,
	   EREC_PldIratPeldanyok.Letrehozo_id,
	   EREC_PldIratPeldanyok.LetrehozasIdo,
	   EREC_PldIratPeldanyok.Modosito_id,
	   EREC_PldIratPeldanyok.ModositasIdo,
	   EREC_PldIratPeldanyok.Zarolo_id,
	   EREC_PldIratPeldanyok.ZarolasIdo,
	   EREC_PldIratPeldanyok.Tranz_id,
	   EREC_PldIratPeldanyok.IratPeldanyMegsemmisitve, 
	   EREC_PldIratPeldanyok.IratPeldanyMegsemmisitesDatuma, 
	   EREC_PldIratPeldanyok.UIAccessLog_id
	   from 
		 EREC_PldIratPeldanyok as EREC_PldIratPeldanyok 
	   where
		 EREC_PldIratPeldanyok.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
