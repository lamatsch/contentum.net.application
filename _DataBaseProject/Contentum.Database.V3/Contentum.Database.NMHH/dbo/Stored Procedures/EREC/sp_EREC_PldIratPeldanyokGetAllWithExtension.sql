﻿--set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--if exists (select 1 from  sysobjects
--           where  id = object_id('sp_EREC_PldIratPeldanyokGetAllWithExtension')
--             and  type in ('P'))
--   drop procedure sp_EREC_PldIratPeldanyokGetAllWithExtension
--go 

create procedure [dbo].[sp_EREC_PldIratPeldanyokGetAllWithExtension]
  @Where                              nvarchar(MAX)     = '',
  @Where_EREC_HataridosFeladatok      nvarchar(MAX)     = '',
  @Where_Dosszie	                  NVARCHAR(MAX)     = '',
  @OrderBy                            nvarchar(MAX)     = ' order by EREC_PldIratPeldanyok.LetrehozasIdo',
  @TopRow                             nvarchar(5)       = '',
  @ExecutorUserId				      uniqueidentifier,
  @FelhasznaloSzervezet_Id	          uniqueidentifier,  
  @Jogosultak		                  char(1)           = '0',
  --@Filter_KimenoKuldemenyId uniqueidentifier = null,
  @Filter_KimenoKuldemenyIds          varchar(max)      = null,
  @pageNumber		                  int               = 0,
  @pageSize			                  int               = -1,
  @SelectedRowId	                  uniqueidentifier  = null
  --- új paraméterek                          
  ,@Csoporttagokkal                   bit               = 1         -- Csak a saját jogon láthatja az iratokat: 0; vagy a csoporttagsága jogán is: 1 (default) 
  ,@CsakAktivIrat                     bit               = 1         -- Csak az AKTIV Iratokat láttatjuk ( a fix 'erős' szűrő ): 1; vagy minden irat: 0 (default)


AS 
BEGIN

  BEGIN TRY

    set nocount ON

	--- időszakaszok mérésére; GUI teszt alatt ki kell kapcsolni
---    create table #timeTMP (DESC1 char(100), TIME1 DateTime);                     --- időszakaszok mérésére
---    insert into #timeTMP select '00 - INIT beállítások, sqlcmd felépítése',current_timestamp;      --- időszakaszok mérésére

	DECLARE @Org         uniqueidentifier,
	        @OrgKod      nvarchar(100),
			@OrgIsBOMPH  int;
    DECLARE @LocalTopRow nvarchar(10),
	        @firstRow    int,
            @lastRow     int;
    DECLARE @sqlcmd      nvarchar(MAX);
	DECLARE @sqlOrderBy    nvarchar(MAX),                             -- dinamikus sql string: @OrderBy-hoz szükséges JOIN -ok 
	        @sqlwhere      nvarchar(MAX),                             -- dinamikus sql string: @where-hez szükséges JOIN -ok
	        @sqlCTL_cta    nvarchar(MAX),                             -- CTL dinamikus felépítése
	        @sqlCTL_jo     nvarchar(MAX),                             -- CTL dinamikus felépítése
	        @sqlCTL_mt     nvarchar(MAX);                             -- CTL dinamikus felépítése

    --- ORG beállítása / ellenőrzése 
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	  RAISERROR('[50202]',16,1);

    SET @OrgKod = (select Kod from KRT_Orgok where Id = @Org);
   -- CR3442
   -- SET @OrgIsBOMPH = (case when @OrgKod = 'BOPMH' then 1 else 0 end);

    --- lapozás kezelés beállítások
    if (@TopRow = '' or @TopRow = '0') 
      SET @LocalTopRow = '';
    else
      begin
        if (@pageSize > @TopRow)
          SET @pageSize = @TopRow;
        SET @LocalTopRow = ' TOP ' + @TopRow;
      end;
                  
    if (@pageSize > 0 and @pageNumber > -1)
      begin
        SET @firstRow = (@pageNumber)*@pageSize + 1;
        SET @lastRow = @firstRow + @pageSize - 1;
      end
    else 
	  begin
        if (@TopRow = '' or @TopRow = '0')
          begin
            SET @firstRow = 1;
            SET @lastRow = ISNULL((SELECT TOP (1) Ertek FROM KRT_Parameterek WHERE Nev = 'LISTA_ELEMSZAM_MAX'),1000);;
          end 
        else
          begin
            SET @firstRow = 1;
            SET @lastRow = @TopRow;
          end   
      end;

    SET @sqlcmd = '';
	/************************************************************
	* Szűrési feltételek sorrendjének összeállítása				*
	************************************************************/
    /* --- NEM használjuk sehol sem a kódban, így kiemeltem
	DECLARE @ObjTipId uniqueidentifier;
	select @ObjTipId = Id from KRT_Objtipusok where Kod = 'EREC_PldIratpeldanyok';
	--- */

    /* --- ezt a blokkot hátra tettem @where @sqlcmd-be illesztéséhez	
	if IsNull(@Filter_KimenoKuldemenyIds, '') != ''
	begin

		if @Where is null or @Where =''
			SET @Where = '' --@Where + ' where '
		else
			SET @Where = @Where + ' and '

		SET @Where = @Where + ' (EREC_PldIratPeldanyok.Id in 
					(SELECT kuldIratPeldanyai.Peldany_Id
					FROM EREC_Kuldemeny_IratPeldanyai as kuldIratPeldanyai
					WHERE kuldIratPeldanyai.KuldKuldemeny_Id in (' + @Filter_KimenoKuldemenyIds + ')
						and getdate() between kuldIratPeldanyai.ErvKezd and kuldIratPeldanyai.ErvVege )
					 )'
	END
	--- */

    /* --- ezt a blokkot hátra tettem #IratPeldanyFiltered gyűjtésébe
	create table #filterOrder (Filter nvarchar(50), RowNumber bigint);
	if @Where_EREC_HataridosFeladatok IS NOT NULL AND @Where_EREC_HataridosFeladatok != ''
      SET @sqlcmd += ' insert into #filterOrder 
	                        SELECT ''Where_EREC_HataridosFeladatok'', count(EREC_HataridosFeladatok.Obj_Id)
							  FROM EREC_HataridosFeladatok
							 WHERE '+ @Where_EREC_HataridosFeladatok;

	if @Where_Dosszie is not null and @Where_Dosszie != ''
      SET @sqlcmd += ' insert into #filterOrder 
	                        select ''Where_Dosszie'', count (KRT_MappaTartalmak.Obj_Id)
			                  FROM KRT_MappaTartalmak
				             INNER JOIN KRT_Mappak on KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
			                 WHERE GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
				               AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
				               AND KRT_MappaTartalmak.Obj_type = ''EREC_PldIratPeldanyok''
				               AND ' + @Where_Dosszie;
	
	exec sp_executesql @sqlcmd;
	--- */

	/************************************************************
	* Szűrési tábla összeállítása								*
	************************************************************/

	--- akkor rakjuk össze a dinamikus SQL-t --- miért is alkalmazunk dinamikus SQL -T ???
	SET @sqlcmd = ''+char(13)+char(13);   
---    SET @sqlcmd += 'insert into #timeTMP select ''10 - Local TMP táblák létrehozása'',current_timestamp; '+char(13)+char(13);              --- időszakaszok mérésére

    --- IratPéldány tábla előszűrése, hogy ne az egészet kelljen #CsoportTagokAll-lal összevetni
	--- Alapból szűrjük ORG-ra -> így az később elhagyható ( bár szerintem már #CsoportTagokAll beállítása is biztosítja az ORG szűrést )
	--- !!! Csak akkor van értelme, ha valami fix erős szűrés van megadva az Iratok táblára: itt most az AKTIV (ALLAPOT) oszlopok szűrésével csökkentjük a tételszámot
	SET @sqlcmd += 'create table #IratPeldanyFiltered(Id uniqueidentifier); '+char(13);
    
    SET @sqlcmd += 'insert into #IratPeldanyFiltered
                    select EREC_PldIratpeldanyok.Id
                      from EREC_PldIratpeldanyok '+char(13);
	--- IraIratok táblán keresztül kapcsolódik a rendszerbe
    
    
	 
	
	
    
	  
        SET @sqlcmd += ' LEFT JOIN EREC_IraIratok as EREC_IraIratok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id '+char(13);
	    if charindex( lower('EREC_UgyUgyiratok'), lower(@where) ) > 0       -- egyébként csak akkor kapcsoljuk be, ha szűrés van rá
          SET @sqlcmd += 'LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id '+char(13);
	  
    -- @ORG korai szűrése miatt mindenképpen kellenek ezek a kapcsolatok 
	SET @sqlcmd += '   LEFT JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
					   inner join EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek ON EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id
					          and EREC_IraIktatoKonyvek.Org=@Org '+char(13);
 
	if charindex( lower('EREC_KuldKuldemenyek'), lower(@where) ) > 0       -- ezt csak akkor kapcsoljuk be, ha szűrés van rá	
      SET @sqlcmd += ' LEFT JOIN EREC_KuldKuldemenyek ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id '+char(13);

    if charindex( lower('EREC_KuldKuldemenyek_Kimeno'), lower(@where) ) > 0       -- ezt csak akkor kapcsoljuk be, ha szűrés van rá
      SET @sqlcmd += ' LEFT Join EREC_Kuldemeny_IratPeldanyai AS ip ON ip.Peldany_Id = EREC_PldIratPeldanyok.Id
						LEFT JOIN EREC_KuldKuldemenyek as EREC_KuldKuldemenyek_Kimeno ON EREC_KuldKuldemenyek_Kimeno.id = ip.KuldKuldemeny_Id '+char(13);
 
    --- a speciális szűrők kezelését már itt be tudjuk építeni, hiszen mindegyik EREC_IraIratok.ID-t szűr és jelentősen leszűkíti az Iratok forrást, viszont innentől egységes a kezelése
	if @Where_EREC_HataridosFeladatok IS NOT NULL AND @Where_EREC_HataridosFeladatok != ''
      SET @sqlcmd += ' INNER JOIN (select Obj_Id from EREC_HataridosFeladatok where ' + @Where_EREC_HataridosFeladatok + ') as EREC_HataridosFeladatok ON EREC_HataridosFeladatok.Obj_Id = EREC_PldIratPeldanyok.Id '+char(13);
	if @Where_Dosszie is not null and @Where_Dosszie != ''
      SET @sqlcmd += ' INNER JOIN (SELECT KRT_MappaTartalmak.Obj_Id FROM KRT_MappaTartalmak
					                 INNER JOIN KRT_Mappak on KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
				                     WHERE GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
					                   AND KRT_MappaTartalmak.Obj_type = ''EREC_PldIratPeldanyok''
					                   AND ' + @Where_Dosszie + ') as KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = EREC_PldIratPeldanyok.Id '+char(13);
    ---
	SET @sqlcmd += ' where 1 = 1 '+char(13);

	IF @CsakAktivIrat = 1
	BEGIN
		SET @sqlcmd +=[dbo].[fn_GetAktivFilterValue] ( 'IP')
		SET @sqlcmd += char(13);
		SET @sqlcmd +=[dbo].[fn_GetAktivFilterValue] ( 'IR')
		SET @sqlcmd += char(13);
	END

	if IsNull(@Filter_KimenoKuldemenyIds, '') != ''
      begin
	    if @Where is null or @Where =''
			SET @Where = '';
		else
			SET @Where = @Where + ' and ';

		SET @Where +=' (EREC_PldIratPeldanyok.Id in  (SELECT kuldIratPeldanyai.Peldany_Id
					                                     FROM EREC_Kuldemeny_IratPeldanyai as kuldIratPeldanyai
					                                    WHERE kuldIratPeldanyai.KuldKuldemeny_Id in (' + @Filter_KimenoKuldemenyIds + ')
						                                  and getdate() between kuldIratPeldanyai.ErvKezd and kuldIratPeldanyai.ErvVege 
													 )
					   )'
	  end;
    if @Where is not null and @Where!=''
	  SET @sqlcmd += ' and ' + @Where + '; '+char(13)+char(13);

    --- Ez az index drága: ha itt hozom létre, akkor is; ha a CREATE-ben Primary Key, akkor is - NE használjuk
    -- SET @sqlcmd += 'create index #ix_IratPeldanyFiltered on #IratPeldanyFiltered (Id); '+char(13)+char(13);

	--- ha NEM @Csoporttagokkal kéri, akkor @ExecutorUserId direktben használom fel, de elötte essen át az 'ellenőrzésen'
	SET @sqlcmd += 'SET @ExecutorUserId = (select alsel.Id 
					                          from (select distinct Id 
	                                                   from dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) 
                                                      where Id = @ExecutorUserId 
										           ) alsel
										  ); '+char(13)+char(13);

	--- CTL hivatkozások megépítése - itt most semmit sem teszünk ki local TMP táblába
	--- jogosult 'csoport'ok
    if @Csoporttagokkal = 1
      SET @sqlCTL_cta = 'with CsoportTagokAll as (select distinct Id from dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) ) '+char(13);
    else
      SET @sqlCTL_cta = 'with CsoportTagokAll as (select distinct Id
	                                              from dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) 
												 where Id = @ExecutorUserId 
											  ) '+char(13);
	--- jogosult objektumok
    SET @sqlCTL_jo = @sqlCTL_cta + ', jogosult_objektumok as
                                      (select krt_jogosultak.Obj_Id, Kezi, Tipus 
						                  from krt_jogosultak
		                                 INNER JOIN CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
                                      ) '+char(13);
    --- mappa tartalmak 
    SET @sqlCTL_mt = @sqlCTL_jo + ', mappa_tartalmak as
                                     (SELECT KRT_MappaTartalmak.Obj_Id 
	                                     FROM KRT_MappaTartalmak
	                                    INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
	                                    INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = KRT_Mappak.Id
	                                    where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
	                                 ) '+char(13)+char(13);

--- Nyomkövetés, éles futásban ki kell kapcsolni    
---    SET @sqlcmd += 'select count(1) from #IratPeldanyFiltered; '+char(13);              --- local TMP táblák méretei
---    SET @sqlcmd += 'insert into #timeTMP select ''20 - INDUL (#subfilter felgyűjtés)'',current_timestamp; '+char(13)+char(13);              --- időszakaszok mérésére

	--- @Org szűrése már #IratPeldanyFiltered gyűjtésekor megtörtént, így itt emiatt nem kellenek a JOIN-ok       
	--- @where kiértékelése már #IratPeldanyFiltered gyűjtésekor megtörtént
	--- szét lett bontva több lépésre az UNION blokk   
	--IF @Jogosultak = '1'  AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
	if @Jogosultak = '1' AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
	  begin

		--- gyűjtés közvetlen az IratPeldany ( ID ) alapján
        SET @sqlcmd += @sqlCTL_jo + 'select /*distinct*/ EREC_PldIratpeldanyok.Id 
			                        into #subfilter
			                        from #IratPeldanyFiltered as EREC_PldIratpeldanyok
                                   inner join jogosult_objektumok as jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_PldIratpeldanyok.Id; '+char(13)+char(13);

        --- gyűjtés az IratPeldany-ban lévő 'csoport' azonosítók alapján
        if @Csoporttagokkal = 1
		  begin
		    SET @sqlcmd += @sqlCTL_cta + 'insert into #subfilter
                                          select /*distinct*/ IratPeldanyFiltered.Id 
                                            from #IratPeldanyFiltered as IratPeldanyFiltered
                                           inner join EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.Id = IratPeldanyFiltered.Id
								           INNER JOIN CsoportTagokAll ON CsoportTagokAll.Id = EREC_PldIratPeldanyok.Csoport_Id_Felelos; '+char(13)+char(13);
		    SET @sqlcmd += @sqlCTL_cta + 'insert into #subfilter
                                          select /*distinct*/ IratPeldanyFiltered.Id 
						                    from #IratPeldanyFiltered as IratPeldanyFiltered
                                           inner join EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.Id = IratPeldanyFiltered.Id
								           INNER JOIN CsoportTagokAll ON CsoportTagokAll.Id = EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo; '+char(13)+char(13);
          end;
        else
		  begin
		    SET @sqlcmd += 'insert into #subfilter
                            select /*distinct*/ IratPeldanyFiltered.Id 
                              from #IratPeldanyFiltered as IratPeldanyFiltered
                             inner join EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.Id = IratPeldanyFiltered.Id and EREC_PldIratPeldanyok.Csoport_Id_Felelos = @ExecutorUserId; '+char(13)+char(13);
		    SET @sqlcmd += 'insert into #subfilter
                            select /*distinct*/ IratPeldanyFiltered.Id 
						      from #IratPeldanyFiltered as IratPeldanyFiltered
                             inner join EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.Id = IratPeldanyFiltered.Id and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo = @ExecutorUserId; '+char(13)+char(13);
		  end;
        --- gyűjtés az IratPeldany-hoz kapcsolódó Feladatok alapján
        if @Csoporttagokkal = 1                                                    --- BogI CR3428
          SET @sqlcmd += 'insert into #subfilter 
		                  select /*distinct*/ alsel.Id
			                from dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_PldIratPeldanyok'') alsel
                           inner join #IratPeldanyFiltered as EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.Id = alsel.Id; '+char(13)+char(13);
        else
          SET @sqlcmd += 'insert into #subfilter 
		                  select /*distinct*/ alsel.Id
			                from dbo.fn_GetObjIdsFromFeladatokByFelhasznalo(@ExecutorUserId, ''EREC_PldIratPeldanyok'') alsel
                           inner join #IratPeldanyFiltered as EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.Id = alsel.Id; '+char(13)+char(13);
					 
        --- gyűjtés az IratPeldany Mappa -ba soroltsága alapján
		SET @sqlcmd += @sqlCTL_mt + 'insert into #subfilter 
		                             select /*distinct*/ EREC_PldIratPeldanyok.Id 
						               from #IratPeldanyFiltered as EREC_PldIratPeldanyok 
						              INNER JOIN mappa_tartalmak ON mappa_tartalmak.Obj_Id = EREC_PldIratPeldanyok.Id; '+char(13)+char(13);

        --- gyűjtés Irat kapcsolat ( IraIrat_ID ) alapján
		SET @sqlcmd += @sqlCTL_jo + 'insert into #subfilter
                                     select /*distinct*/ IratPeldanyFiltered.Id
                                       from #IratPeldanyFiltered as IratPeldanyFiltered
                                      inner join EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.Id = IratPeldanyFiltered.Id
									  inner join jogosult_objektumok as jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_PldIratpeldanyok.IraIrat_Id; '+char(13)+char(13);
        --- gyűjtés az Irat-hoz kapcsolódó Feladatok alapján
        if @Csoporttagokkal = 1                                                    --- BogI CR3428
          SET @sqlcmd += 'insert into #subfilter 
                          select /*distinct*/ IratPeldanyFiltered.Id 
                            from #IratPeldanyFiltered as IratPeldanyFiltered
                           inner join EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.Id = IratPeldanyFiltered.Id
                           INNER JOIN dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_IraIratok'') alsel ON alsel.Id = EREC_PldIratPeldanyok.IraIrat_Id; '+char(13)+char(13);
        else
          SET @sqlcmd += 'insert into #subfilter 
                          select /*distinct*/ IratPeldanyFiltered.Id 
                            from #IratPeldanyFiltered as IratPeldanyFiltered
                           inner join EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.Id = IratPeldanyFiltered.Id
                           INNER JOIN dbo.fn_GetObjIdsFromFeladatokByFelhasznalo(@ExecutorUserId, ''EREC_IraIratok'') alsel ON alsel.Id = EREC_PldIratPeldanyok.IraIrat_Id; '+char(13)+char(13);
						 
		--- gyűjtés Ügyirat kapcsolat alapján
        SET @sqlcmd += @sqlCTL_mt + 'insert into #subfilter
                                     select /*distinct*/ IratPeldanyFiltered.Id 
                                       from #IratPeldanyFiltered as IratPeldanyFiltered
                                      inner join EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.Id = IratPeldanyFiltered.Id
		                              INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
		                              WHERE EREC_IraIratok.Ugyirat_Id IN 
		                                      (SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				                                 INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = erec_ugyugyiratok.Id';
	    SET @sqlcmd += dbo.fn_GetJogosultakFilter(@Org, 'jogosult_objektumok');

        SET @sqlcmd += '                        UNION
			                                    SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				                                 where Exists (select 1 from jogosult_objektumok j where j.Obj_Id = EREC_UgyUgyiratok.IraIktatokonyv_Id ) '+char(13);
        if @Csoporttagokkal = 1
          SET @sqlcmd += '                      UNION
		                                        SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
                                                 INNER JOIN CsoportTagokAll as CsoportTagokAll ON CsoportTagokAll.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez
                                                UNION
								                SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
								                 INNER JOIN CsoportTagokAll as CsoportTagokAll ON CsoportTagokAll.Id = EREC_UgyUgyiratok.Csoport_Id_Felelos
                                                UNION
                                                SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
                                                 INNER JOIN CsoportTagokAll as CsoportTagokAll ON CsoportTagokAll.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo '+char(13);
        else
          SET @sqlcmd += '                      UNION
                                                SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok where EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez = @ExecutorUserId
                                                UNION
								                SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok where EREC_UgyUgyiratok.Csoport_Id_Felelos = @ExecutorUserId
                                                UNION
                                                SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok where EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo = @ExecutorUserId '+char(13);
        if @Csoporttagokkal = 1                                                    --- BogI CR3428
          SET @sqlcmd += '                      UNION
			                                    SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'') '+char(13);
        else
          SET @sqlcmd += '                      UNION
			                                    SELECT Id FROM dbo.fn_GetObjIdsFromFeladatokByFelhasznalo(@ExecutorUserId, ''EREC_UgyUgyiratok'') '+char(13);

        SET @sqlcmd += '                        UNION
			                                    SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				                                 INNER JOIN mappa_tartalmak ON mappa_tartalmak.Obj_Id = erec_ugyugyiratok.Id
		                                      ) '+char(13)+char(13);
	  end;

    --- #filter nem épül - #subfilter-t használjuk majd #result felépítéséhez

	--- itt nem játszanak a Metaadatok ???

---    SET @sqlcmd += 'insert into #timeTMP select ''30 - #subfilter felgyűjtés vége; #result építése indul'',current_timestamp; '+char(13)+char(13);

	/************************************************************
	* Szűrt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/

    --- konzisztens adatlekérés érdekében
	if charindex( lower('EREC_PldIratPeldanyok.ID'), lower( @OrderBy ) ) = 0 
      SET @OrderBy += ', EREC_PldIratPeldanyok.ID DESC';

	--- dinamikusan építjük fel - csak a legszükségesebb - JOIN-okat
	--- Fontos: az itt megfogalmazott kapcsolatoknak egyezniük kell a '* Tényleges select' során, az egyes adatmezők lekéréséhez felépített kapcsolatokkal
    SET @sqlOrderBy = ' ';
	--- Ugyirat és Irat kapcsolása
    if charindex( lower('EREC_UgyUgyiratok'), lower( @OrderBy ) ) > 0 
	  SET @sqlOrderBy += 'LEFT JOIN EREC_IraIratok as EREC_IraIratok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
		                    LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id '+char(13);
    else
      if ( charindex( lower('EREC_IraIratok'), lower( @OrderBy ) ) > 0 ) OR
	     ( charindex( lower('EREC_UgyUgyiratdarabok'), lower( @OrderBy ) ) > 0 ) OR
		 ( charindex( lower('EREC_KuldKuldemenyek'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('KuldesModKodTarak'), lower( @OrderBy ) ) > 0 ) OR
		 ( charindex( lower('EREC_IraIktatoKonyvek'), lower( @OrderBy ) ) > 0  )
		 -- BLG_619
		or charindex( lower('Csoportok_IratFelelosNev'), lower( @OrderBy ) ) > 0

        SET @sqlOrderBy += 'LEFT JOIN EREC_IraIratok as EREC_IraIratok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id '+char(13);
    --- Iktatokonyv és UgyiratDarab kapcsolása
	if charindex( lower('EREC_IraIktatoKonyvek'), lower( @OrderBy ) ) > 0 
      SET @sqlOrderBy += 'LEFT JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
                           LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek ON EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id '+char(13);
	else
	  if charindex( lower('EREC_UgyUgyiratdarabok'), lower( @OrderBy ) ) > 0 
	    SET @sqlOrderBy += 'LEFT JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id '+char(13);
	--- Kuldemenyek kapcsolása
	if ( charindex( lower('EREC_KuldKuldemenyek'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('KuldesModKodTarak'), lower( @OrderBy ) ) > 0 ) 
	    SET @sqlOrderBy += ' LEFT JOIN EREC_KuldKuldemenyek ON EREC_KuldKuldemenyek.id = EREC_IraIratok.KuldKuldemenyek_Id '+char(13);
 --- Kimenő Kuldemenyek kapcsolása
    if ( charindex( lower('EREC_KuldKuldemenyek_Kimeno'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('KuldesModKodTarak'), lower( @OrderBy ) ) > 0 ) 
	    SET @sqlOrderBy += ' LEFT Join EREC_Kuldemeny_IratPeldanyai AS ip ON ip.Peldany_Id = EREC_PldIratPeldanyok.Id
 LEFT JOIN EREC_KuldKuldemenyek as EREC_KuldKuldemenyek_Kimeno ON EREC_KuldKuldemenyek_Kimeno.id = ip.KuldKuldemeny_Id '+char(13);
	--- !!! LEFT join-ban ON-ban az AND nem egészen úgy működik, ahogyan számítanánk rá !!!
	--- EREC_PldIratPeldanyok függő
	if ( charindex( lower('AllapotKodTarak'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_PldIratPeldanyok.Allapot'), lower( @OrderBy ) ) > 0 )
      SET @sqlOrderBy += 'left join KRT_Kodcsoportok as AllapotKodCsoport on AllapotKodCsoport.Kod = ''IRATPELDANY_ALLAPOT''
				left join KRT_Kodtarak as AllapotKodTarak on AllapotKodTarak.Kodcsoport_Id = AllapotKodCsoport.Id and AllapotKodTarak.Kod = EREC_PldIratPeldanyok.Allapot and AllapotKodTarak.Org=@Org '+char(13);
	--- régebben bevezetett adatmezőkre történő rendezés hibájának javítása
	if ( charindex( lower('Irat_FajtaKodTarak'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_PldIratPeldanyok.Eredet'), lower( @OrderBy ) ) > 0 )
      SET @sqlOrderBy += 'left join KRT_Kodcsoportok as Irat_FajtaKodCsoport on Irat_FajtaKodCsoport.Kod = ''IRAT_FAJTA''
				left join KRT_Kodtarak as Irat_FajtaKodTarak on Irat_FajtaKodTarak.Kodcsoport_Id = Irat_FajtaKodCsoport.Id and Irat_FajtaKodTarak.Kod = EREC_PldIratPeldanyok.Eredet and Irat_FajtaKodTarak.Org=@Org '+char(13);
   	if ( charindex( lower('Csoportok_Letrehozo'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_PldIratPeldanyok.Letrehozo_Id'), lower( @OrderBy ) ) > 0 )
	  SET @sqlOrderBy += 'LEFT JOIN KRT_Csoportok AS Csoportok_Letrehozo ON Csoportok_Letrehozo.Id = EREC_PldIratPeldanyok.Letrehozo_Id '+char(13);				
				
    --- EREC_KuldKuldemenyek függő
	if ( charindex( lower('KuldesModKodTarak'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_KuldKuldemenyek.KuldesMod'), lower( @OrderBy ) ) > 0 )
      SET @sqlOrderBy += 'left join KRT_Kodcsoportok as KuldesModKodCsoport on KuldesModKodCsoport.Kod = ''KULDEMENY_KULDES_MODJA'' 
                             left join KRT_Kodtarak as KuldesModKodTarak on KuldesModKodTarak.Kodcsoport_Id = KuldesModKodCsoport.Id 
							       and KuldesModKodTarak.Kod = EREC_KuldKuldemenyek.KuldesMod /* EREC_PldIratPeldanyok.Allapot - ez értelmetlennek látszik */
								   and KuldesModKodtarak.Org = @Org '+char(13);
    --- EREC_IraIratok függő
	-- BLG_619
	if ( charindex( lower('Csoportok_IratFelelosNev'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_IraIratok.Csoport_Id_UgyFelelos'), lower( @OrderBy ) ) > 0 )
	  SET @sqlOrderBy += 'left join [dbo].KRT_Csoportok as Csoportok_IratFelelosNev on Csoportok_IratFelelosNev.Id = EREC_IraIratok.Csoport_Id_UgyFelelos '+char(13);

   	if ( charindex( lower('Csoportok_OrzoNev'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo'), lower( @OrderBy ) ) > 0 )
	  SET @sqlOrderBy += 'left join KRT_Csoportok as Csoportok_OrzoNev on Csoportok_OrzoNev.Id = EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo '+char(13);
   	if ( charindex( lower('Csoportok_FelelosNev'), lower( @OrderBy ) ) > 0 ) OR ( charindex( lower('EREC_PldIratPeldanyok.Csoport_Id_Felelos'), lower( @OrderBy ) ) > 0 )
	  SET @sqlOrderBy += 'LEFT JOIN KRT_Csoportok AS Csoportok_FelelosNev ON Csoportok_FelelosNev.Id = EREC_PldIratPeldanyok.Csoport_Id_Felelos '+char(13);
  -- Terti
	  if charindex( lower('EREC_KuldTertivevenyek'), lower( @OrderBy ) ) > 0
	    SET @sqlOrderBy += 'LEFT JOIN (select kip.Peldany_Id as Peldany_Id, t.*
		              from EREC_Kuldemeny_IratPeldanyai kip
		                   inner join EREC_KuldTertivevenyek as t on kip.KuldKuldemeny_Id = t.Kuldemeny_Id
		              Where getdate() between kip.ErvKezd and kip.ErvVege) as EREC_KuldTertivevenyek On EREC_KuldTertivevenyek.Peldany_Id = EREC_PldIratPeldanyok.Id'+char(13);

    if @Jogosultak = '1' AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0 
      SET @sqlcmd += N'select row_number() over('+@OrderBy+') as RowNumber, EREC_PldIratPeldanyok.Id
	                     into #result
                         from (select distinct Id from #subfilter ) as f 
	                    inner join EREC_PldIratPeldanyok as EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.Id = f.Id '+char(13);                -- ez a további JOIN-ok kapcsolója
    else
      SET @sqlcmd += N'select row_number() over('+@OrderBy+') as RowNumber, EREC_PldIratPeldanyok.Id
	                     into #result
                         from #IratPeldanyFiltered as f
	                    inner join EREC_PldIratPeldanyok as EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.Id = f.Id '+char(13);                -- ez a további JOIN-ok kapcsolója
    SET @sqlcmd += @sqlOrderBy+char(13);
 	
---    SET @sqlcmd += 'insert into #timeTMP select ''50 - #result felépítés vége; fix sorra ugrás beállítása'', current_timestamp; '+char(13)+char(13);

   if (@SelectedRowId is not null)                       --- fix sorra ugrás beállítása
	    --- #2571 - 2018.11.18., BogI
        SET @sqlcmd += N'if exists (select 1 from #result where Id = @SelectedRowId)
                           begin
                             SET @pageNumber = (select ( ((RowNumber - 1 )/ @pageSize) + 1 ) from #result where Id = @SelectedRowId );          --- NEM lehet NULL, most vizsgáltuk le
                             set @firstRow = (@pageNumber - 1) * @pageSize + 1;
                             set @lastRow = @pageNumber*@pageSize;
                             SET @pageNumber -= 1;
                           end;
                         else '
	  --- ez az ág mindíg fut, ez a normál beállítás
      SET @sqlcmd += N'if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
                           begin
                             SET @pageNumber = ( select ( ((RowNumber - 1 )/ @pageSize) + 1 ) from #result where RowNumber = ( select MAX(RowNumber) from #result ) );
                             set @firstRow = (@pageNumber - 1) * @pageSize + 1;
                             set @lastRow = @pageNumber*@pageSize;
                             SET @pageNumber -= 1;
                           end;'+char(13)+char(13);

---    SET @sqlcmd += 'insert into #timeTMP select ''60 - bizalmas iratok szűrésének előkészítése'', current_timestamp; '+char(13)+char(13);

	/************************************************************
	* Bizalmas iratok őrző szerinti szűréséhez csoporttagok lekérése*
	************************************************************/
	if dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
	  begin
        SET @sqlcmd += N' declare @iratok_executor table (Id uniqueidentifier primary key); '+char(13)+char(13);
        SET @sqlcmd += N' declare @confidential varchar(4000);
	                      set @confidential = (select top 1 Ertek 
						                        from KRT_Parameterek 
											   where Nev=''IRAT_MINOSITES_BIZALMAS''
							                     and Org=@Org 
												 and getdate() between ErvKezd and ErvVege);
                          
						  declare @Bizalmas table (val nvarchar(64) collate Hungarian_CS_AS );
	                      insert into @Bizalmas (val) (select Value from fn_Split(@confidential, '',''));

	                      declare @csoporttagsag_tipus nvarchar(64)
	                      SET @csoporttagsag_tipus = (select top 1 KRT_CsoportTagok.Tipus 
						                                from KRT_CsoportTagok
						                               where KRT_CsoportTagok.Csoport_Id_Jogalany=@ExecutorUserId
						                                 and KRT_CsoportTagok.Csoport_Id=@FelhasznaloSzervezet_Id
						                                 and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
														 order by KRT_CsoportTagok.Tipus desc);

						  DECLARE @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN nvarchar(400)
						  set @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN = (select top 1 Ertek from KRT_Parameterek where Nev=''JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN''
						  and Org=@Org and getdate() between ErvKezd and ErvVege)

						  IF @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN IS NULL
							SET @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN = ''0''

	                      CREATE TABLE #jogosultak (Id uniqueidentifier)
	                      IF @csoporttagsag_tipus in (''3'',''4'')
                            INSERT INTO #jogosultak 
							select KRT_Csoportok.Id 
							  from KRT_Csoportok
							 inner join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id = @FelhasznaloSzervezet_Id 
							            and KRT_Csoportok.Tipus = ''1'' 
										and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
										and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
							 where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege; 
	                      ELSE
                            INSERT INTO #jogosultak select @ExecutorUserId; ';
	    set @sqlcmd = @sqlcmd + N' insert into @iratok_executor 
	                               select EREC_IraIratok.Id
	                                 from EREC_IraIratok
	                                INNER JOIN EREC_PldIratPeldanyok on EREC_IraIratok.Id=EREC_PldIratPeldanyok.IraIrat_Id
	                                INNER JOIN #result on #result.Id=EREC_PldIratPeldanyok.Id and #result.RowNumber between @firstRow and @lastRow
	                                where 1=case when (@confidential = ''MIND'' or EREC_IraIratok.Minosites in (select val from @Bizalmas))
		                                              AND NOT EXISTS (select 1
					                                                     from KRT_Jogosultak 
						                                                where KRT_Jogosultak.Obj_Id in (EREC_IraIratok.Id, EREC_IraIratok.Ugyirat_Id)
						                                                  and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
						                                                  and @ExecutorUserId = KRT_Jogosultak.Csoport_Id_Jogalany)
		                                              AND NOT EXISTS (select 1
				                                                         from EREC_PldIratPeldanyok
																		 join EREC_IraIratok ii on EREC_PldIratPeldanyok.IraIrat_Id = ii.Id
																		 join EREC_UgyUgyiratok ui on EREC_IraIratok.Ugyirat_Id = ui.Id
				                                                        where EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id
					                                                      and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo in (select Id from #jogosultak) 
																		  and (@JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN !=''1'' or EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo=@ExecutorUserId or (ii.Csoport_Id_Ugyfelelos is null and ui.Csoport_Id_Ugyfelelos is null) or ISNULL(ii.Csoport_Id_Ugyfelelos, ui.Csoport_Id_Ugyfelelos) = @FelhasznaloSzervezet_Id)
																		  )
													AND NOT EXISTS (select 1
				                                                         from EREC_PldIratPeldanyok
																		 join EREC_IraIratok ii on EREC_PldIratPeldanyok.IraIrat_Id = ii.Id
																		 join EREC_UgyUgyiratok ui on EREC_IraIratok.Ugyirat_Id = ui.Id
				                                                        where EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id
					                                                      and EREC_PldIratPeldanyok.Csoport_Id_Felelos in (select Id from #jogosultak) 
																		  and (@JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN !=''1'' or EREC_PldIratPeldanyok.Csoport_Id_Felelos=@ExecutorUserId or (ii.Csoport_Id_Ugyfelelos is null and ui.Csoport_Id_Ugyfelelos is null) or ISNULL(ii.Csoport_Id_Ugyfelelos, ui.Csoport_Id_Ugyfelelos) = @FelhasznaloSzervezet_Id)
																		  )
	                                             then 0 
	                                             else 1 
										    end
                                    group by EREC_IraIratok.Id;
							       drop table #jogosultak; ';
	  end; 

--- 	SET @sqlcmd += 'insert into #timeTMP select ''90 - és végre elindul a Query '', current_timestamp; '+char(13)+char(13);

	/************************************************************
	* Tényleges select											*
	************************************************************/
	SET @sqlcmd += N'
	  select 
		#result.RowNumber,
		#result.Id,
	    EREC_PldIratPeldanyok.IraIrat_Id,
	    EREC_PldIratPeldanyok.UgyUgyirat_Id_Kulso,
	    EREC_UgyUgyiratok.Id as UgyiratId,
	    EREC_IraIratok.Jelleg,
	    CONVERT(nvarchar(10), EREC_IraIratok.IktatasDatuma, 102) as IktatasDatuma,
	    EREC_IraIktatoKonyvek.MegkulJelzes + '' - '' + CAST(Erkezteto_Szam AS NVarchar(100)) + '' / '' + CAST(EREC_IraIktatoKonyvek.Ev AS NVarchar(100)) FullErkeztetoSzam,
	    EREC_KuldKuldemenyek.HivatkozasiSzam,		
		(select top 1 k.Ragszam
		 from EREC_Kuldemeny_IratPeldanyai kip
		 join EREC_KuldKuldemenyek k on kip.KuldKuldemeny_Id = k.Id
		 where kip.Peldany_Id = EREC_PldIratPeldanyok.Id and getdate() between kip.ErvKezd and kip.ErvVege) as Ragszam,
	    EREC_KuldKuldemenyek.NevSTR_Bekuldo,
        dbo.fn_GetCsoportNev(EREC_UgyUgyiratok.Csoport_Id_Felelos) as Felelos_Nev,
	    /* KRT_Csoportok_UgyiratFelelos.Nev as Felelos_Nev, */
	    EREC_PldIratPeldanyok.Azonosito as IktatoSzam_Merge,
	    EREC_IraIratok.Azonosito as AlSzam_Merge,
	    EREC_IraIratok.Kategoria,
        dbo.fn_KodtarErtekNeve(''IRATKATEGORIA'', EREC_IraIratok.Kategoria,@Org) as Kategoria_Nev,
	    EREC_IraIratok.PostazasIranya,
        dbo.fn_KodtarErtekNeve(''POSTAZAS_IRANYA'', EREC_IraIratok.PostazasIranya,@Org) as PostazasIranya_Nev,
	    EREC_IraIktatokonyvek.MegkulJelzes,
	    EREC_IraIktatokonyvek.Iktatohely,
	    EREC_IraIktatokonyvek.Ev,
	    EREC_UgyUgyiratok.Allapot as EREC_UgyUgyiratok_Allapot,
	    EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo as EREC_UgyUgyiratok_FelhCsopIdOrzo,
	    EREC_UgyUgyiratok.TovabbitasAlattAllapot as EREC_UgyUgyiratok_TovabbitasAlattAllapot,
	    EREC_UgyUgyiratok.Csoport_Id_Felelos as EREC_UgyUgyiratok_CsopIdFelelos,
	    EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez as EREC_UgyUgyiratok_FelhCsopIdUgyintezo,
	    EREC_UgyUgyiratok.LezarasDat as EREC_UgyUgyiratok_LezarasDat,
	    EREC_UgyUgyiratok.IraIktatokonyv_Id as EREC_UgyUgyiratok_IraIktatokonyv_Id,
	    EREC_UgyUgyiratok.Kovetkezo_Felelos_Id as EREC_UgyUgyiratok_Kovetkezo_Felelos_Id,
	    EREC_UgyUgyiratok.RegirendszerIktatoszam as EREC_UgyUgyiratok_RegirendszerIktatoszam,
	    EREC_UgyUgyiratdarabok.UgyUgyirat_Id as EREC_UgyUgyiratdarabok_UgyUgyirat_Id,
	    EREC_UgyUgyiratdarabok.Allapot as EREC_UgyUgyiratdarabok_Allapot,
	    EREC_UgyUgyiratok.Foszam,
	    EREC_UgyUgyiratok.Jelleg as EREC_UgyUgyiratok_Jelleg,
		EREC_UgyUgyiratok.IraIrattariTetel_Id as EREC_UgyUgyiratok_IraIrattariTetel_Id,  -- CR3348 kapcsán státusz bővítés
        EREC_PldIratPeldanyok.Sorszam,
	    EREC_IraIratok.Alszam,
	    EREC_IraIratok.Targy as EREC_IraIratok_Targy,
	    EREC_IraIratok.Allapot as EREC_IraIratok_Allapot,
        dbo.fn_KodtarErtekNeve(''IRAT_ALLAPOT'', EREC_IraIratok.Allapot,@Org) as EREC_IraIratok_Allapot_Nev,
		-- BLG_619
		dbo.fn_GetCsoportNev(EREC_IraIratok.Csoport_Id_Ugyfelelos) as IratFelelos_Nev,
		dbo.fn_GetCsoportKod(EREC_IraIratok.Csoport_Id_Ugyfelelos) as IratFelelos_SzervezetKod,
		(select nev  from KRT_KodTarak where KodCsoport_Id = (select id from KRT_KodCsoportok where kod  = ''TERTIVEVENY_VISSZA_KOD'') and Kod = EREC_KuldTertivevenyek.TertivisszaKod) as TertiKezbesitesEredmenye,
		EREC_KuldTertivevenyek.AtvevoSzemely as TertiAtvevoSzemely,
		EREC_KuldTertivevenyek.AtvetelDat TertiAtvetelIdopontja,
		EREC_KuldTertivevenyek.Note as TertiAtvetelJogcime,		
		(select nev  from KRT_KodTarak where KodCsoport_Id = (select id from KRT_KodCsoportok where kod  = ''TERTIVEVENY_ALLAPOT'') and Kod = EREC_KuldTertivevenyek.Allapot) as TertiAllapot,			
		EREC_KuldTertivevenyek.TertiVisszaDat as TertiVisszaerkezesIdeje,
		EREC_KuldTertivevenyek.BarCode  as TertivevenyVonalkod,
		
		
      ';

    -- megvizsgáljuk, hogy az irat bizalmas-e, és ez esetben a felhasználó az őrző vagy az őrző vezetője-e
    if dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0                              --AND @Jogosultak = '1'
      begin
        SET @sqlcmd += '(SELECT COUNT(*) 
		                   FROM [EREC_Csatolmanyok] 
						  INNER JOIN @iratok_executor ie ON ie.Id= EREC_Csatolmanyok.IraIrat_Id
	                      WHERE [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id
	                        AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege) as CsatolmanyCount, 
					   ';
       SET @sqlcmd += 'Dokumentum_Id = 
						CASE
							WHEN (
								SELECT COUNT(*) 
		                        FROM [EREC_Csatolmanyok] 
								INNER JOIN @iratok_executor ie ON ie.Id= EREC_Csatolmanyok.IraIrat_Id
	                            WHERE [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id
	                                AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
								) >0
							THEN
								 (SELECT TOP 1 EREC_Csatolmanyok.Dokumentum_Id
                                                     FROM EREC_Csatolmanyok
                                                    WHERE [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id
                                                     AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
                                 )
							 ELSE 
								CAST(NULL AS UNIQUEIDENTIFIER) 
						END,
                       ';
      end
    else
      begin
	    set @sqlcmd += '(SELECT COUNT(*) 
		                   FROM [EREC_Csatolmanyok] 
						  WHERE [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id 
						    AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege) as CsatolmanyCount,
                       ';
	     set @sqlcmd += 'Dokumentum_Id = 
				CASE 
					WHEN 
						(SELECT COUNT(*) 
		                 FROM [EREC_Csatolmanyok] 
						 WHERE [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id 
							AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
		   			    ) > 0
					THEN
						(SELECT TOP 1 EREC_Csatolmanyok.Dokumentum_Id
                        FROM EREC_Csatolmanyok
				        WHERE EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
				            and getdate() BETWEEN EREC_Csatolmanyok.ErvKezd AND EREC_Csatolmanyok.ErvVege
                        )
					ELSE
						CAST(NULL AS UNIQUEIDENTIFIER) 
				END,
              ';
      end;

	SET @sqlcmd += '
	    EREC_IraIratok.FelhasznaloCsoport_Id_Iktato as FelhasznaloCsoport_Id_Iktato,
	    EREC_IraIratok.UgyUgyIratDarab_Id as EREC_IraIratok_UgyUgyIratDarab_Id, 
		';

    SET @sqlcmd += '  
	    EREC_PldIratPeldanyok.SztornirozasDat,
	    EREC_PldIratPeldanyok.AtvetelDatuma,
	    EREC_PldIratPeldanyok.Eredet,
	    dbo.fn_KodtarErtekNeve(''IRAT_FAJTA'', EREC_PldIratPeldanyok.Eredet,@Org) as Eredet_Nev,
	    EREC_PldIratPeldanyok.KuldesMod,
        dbo.fn_KodtarErtekNeve(''KULDEMENY_KULDES_MODJA'', EREC_PldIratPeldanyok.KuldesMod,@Org) as KuldesMod_Nev,
	    EREC_PldIratPeldanyok.UgyintezesModja,
	    CONVERT(nvarchar(10), EREC_PldIratPeldanyok.VisszaerkezesiHatarido, 102) as VisszaerkezesiHatarido,	   
	    EREC_PldIratPeldanyok.Visszavarolag,
	    EREC_PldIratPeldanyok.VisszaerkezesDatuma,
	    EREC_PldIratPeldanyok.Cim_id_Cimzett,
	    EREC_PldIratPeldanyok.CimSTR_Cimzett,
	    EREC_PldIratPeldanyok.Partner_Id_Cimzett,
		EREC_PldIratPeldanyok.Partner_Id_CimzettKapcsolt,
	    EREC_PldIratPeldanyok.NevSTR_Cimzett,
	    EREC_PldIratPeldanyok.Csoport_Id_Felelos,
        dbo.fn_GetCsoportNev(EREC_PldIratPeldanyok.Csoport_Id_Felelos) as Csoport_Id_Felelos_Nev,
        /* KRT_Csoportok_Felelos.Nev as Csoport_Id_Felelos_Nev, */
	    EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo,
        dbo.fn_GetCsoportNev(EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo) as FelhasznaloCsoport_Id_Orzo_Nev,
        dbo.fn_GetCsoportNev(EREC_UgyUgyiratdarabok.FelhasznaloCsoport_Id_Ugyintez) as FelhasznaloCsoport_Id_Ugyintez_nev,
	    EREC_PldIratPeldanyok.Csoport_Id_Felelos_Elozo,
	    EREC_PldIratPeldanyok.Tovabbito,
	    EREC_PldIratPeldanyok.IraIrat_Id_Kapcsolt,
	    EREC_PldIratPeldanyok.IrattariHely,
	    EREC_PldIratPeldanyok.Gener_Id,
	    EREC_PldIratPeldanyok.PostazasDatuma,
	    EREC_PldIratPeldanyok.BarCode,
	    EREC_PldIratPeldanyok.Allapot,
        ';
    SET @sqlcmd += '  
	    (select top 1 cs.Dokumentum_Id 
		   from EREC_Mellekletek m
           join EREC_KuldKuldemenyek k on k.Id=m.KuldKuldemeny_Id
		   join EREC_IratelemKapcsolatok kapcs on m.Id = kapcs.Melleklet_Id
		   join EREC_Csatolmanyok cs on kapcs.Csatolmany_Id = cs.Id
           join EREC_Kuldemeny_IratPeldanyai p on p.KuldKuldemeny_Id=cs.KuldKuldemeny_Id
          where k.PostazasIranya=''2'' -- Kimeno
            and m.AdathordozoTipus=''14'' -- Tertiveveny
            and getdate() between m.ErvKezd and m.ErvVege
            and getdate() between cs.ErvKezd and cs.ErvVege
            and k.Allapot != ''90'' --Sztornozott
            and p.Peldany_Id =EREC_PldIratPeldanyok.Id 
			order by
				m.LetrehozasIdo desc
        ) Tertiveveny_Dokumentum_Id,
        ';

    set @sqlcmd += '
        Allapot_Nev = CASE EREC_PldIratPeldanyok.Allapot
			               WHEN ''50'' THEN dbo.fn_KodtarErtekNeve(''IRATPELDANY_ALLAPOT'', EREC_PldIratPeldanyok.Allapot,@Org)  
						                    + '' (''+dbo.fn_KodtarErtekNeve(''IRATPELDANY_ALLAPOT'', EREC_PldIratPeldanyok.TovabbitasAlattAllapot,@Org)+'')''			
			                           ELSE dbo.fn_KodtarErtekNeve(''IRATPELDANY_ALLAPOT'', EREC_PldIratPeldanyok.Allapot,@Org)
		              END,
	    EREC_PldIratPeldanyok.TovabbitasAlattAllapot,
	    EREC_PldIratPeldanyok.PostazasAllapot,
	    dbo.fn_GetEREC_HataridosFeladatokCount(EREC_PldIratPeldanyok.Id,@ExecutorUserId, @FelhasznaloSzervezet_Id) as FeladatCount,
        Hat_kod = CASE	
		            WHEN EREC_IraOnkormAdatok.UgyFajtaja IN (''1'',''2'')
		            THEN ISNULL(EREC_IraOnkormAdatok.UgyFajtaja,'' '') + ISNULL(EREC_IraOnkormAdatok.DontestHozta,'' '') + 
				         ISNULL(EREC_IraOnkormAdatok.DontesFormaja,'' '') + ISNULL(EREC_IraOnkormAdatok.UgyintezesHataridore COLLATE hungarian_CS_AS,'' '') + 
				         ISNULL(EREC_IraOnkormAdatok.JogorvoslatiEljarasTipusa,'' '') + ISNULL(EREC_IraOnkormAdatok.JogorvoslatiDontesTipusa,'' '') + 
				         ISNULL(EREC_IraOnkormAdatok.JogorvoslatiDontestHozta,'' '') + ISNULL(EREC_IraOnkormAdatok.JogorvoslatiDontesTartalma,'' '') 
		            ELSE ''''
	              END,
		-- BLG#1402:		
	    dbo.fn_KodtarErtekNeve(''IRAT_MINOSITES'', EREC_IraIratok.Minosites,@Org) as MinositesNev,
		KRT_PartnerMinosito.Nev as MinositoNev,
		KRT_PartnerMinositoSzerv.Nev as MinositoSzervezetNev,
	    EREC_IraIratok.MinositesErvenyessegiIdeje,
	    EREC_IraIratok.TerjedelemMennyiseg,
	    EREC_IraIratok.TerjedelemMennyisegiEgyseg,
	    EREC_IraIratok.TerjedelemMegjegyzes,
		(select count(*) from EREC_Mellekletek m where m.IraIrat_Id = EREC_IraIratok.Id and getdate() between m.ErvKezd and m.ErvVege) as MellekletekSzama,
	    EREC_PldIratPeldanyok.SelejtezesDat,
	    EREC_PldIratPeldanyok.FelhCsoport_Id_Selejtezo,
	    EREC_PldIratPeldanyok.LeveltariAtvevoNeve,
		EREC_PldIratPeldanyok.IrattarId,
		EREC_PldIratPeldanyok.Ver,
	    EREC_PldIratPeldanyok.Note,
	    EREC_PldIratPeldanyok.Stat_id,
	    EREC_PldIratPeldanyok.ErvKezd,
	    EREC_PldIratPeldanyok.ErvVege,
	    EREC_PldIratPeldanyok.Letrehozo_id,
	    dbo.fn_GetCsoportNev(EREC_PldIratPeldanyok.Letrehozo_id) as Letrehozo_Nev,
	    EREC_PldIratPeldanyok.LetrehozasIdo,
        CONVERT(nvarchar(10), EREC_PldIratPeldanyok.LetrehozasIdo, 102) as LetrehozasIdo_Rovid,
	    EREC_PldIratPeldanyok.Modosito_id,
	    EREC_PldIratPeldanyok.ModositasIdo,
	    EREC_PldIratPeldanyok.Zarolo_id,
	    EREC_PldIratPeldanyok.ZarolasIdo,
	    EREC_PldIratPeldanyok.Tranz_id,
		EREC_PldIratPeldanyok.IratPeldanyMegsemmisitve, 
	    EREC_PldIratPeldanyok.IratPeldanyMegsemmisitesDatuma, 
	    EREC_PldIratPeldanyok.UIAccessLog_id
		';

    -- CR3442
    --if @OrgIsBOMPH = 1
	  SET @sqlcmd += '
		,(select top 1 dbo.fn_KodtarErtekNeve(''FELADAT_ALTIPUS'', EREC_HataridosFeladatok.Altipus,@Org)
            from EREC_HataridosFeladatok 
		   where EREC_HataridosFeladatok.Obj_Id = EREC_PldIratPeldanyok.Id
             and EREC_HataridosFeladatok.Tipus = ''M''
           order by EREC_HataridosFeladatok.LetrehozasIdo desc) as KezelesTipusNev';
  --  else
	 -- SET @sqlcmd += '
		--,null as KezelesTipusNev';	   
	   
    set @sqlcmd += '	   
        from EREC_PldIratPeldanyok as EREC_PldIratPeldanyok
	   inner join #result on #result.Id = EREC_PldIratPeldanyok.Id
	    LEFT JOIN EREC_IraIratok as EREC_IraIratok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
	    LEFT JOIN EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
	    LEFT JOIN EREC_UgyUgyiratok as EREC_UgyUgyiratok ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id  
	    LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek ON EREC_UgyUgyiratdarabok.IraIktatokonyv_Id = EREC_IraIktatoKonyvek.Id			
		LEFT JOIN EREC_KuldKuldemenyek AS EREC_KuldKuldemenyek ON EREC_KuldKuldemenyek.id = EREC_IraIratok.KuldKuldemenyek_Id
		LEFT JOIN [EREC_IraOnkormAdatok] AS EREC_IraOnkormAdatok ON EREC_IraIratok.[Id] = EREC_IraOnkormAdatok.[Id]
		LEFT JOIN KRT_Partnerek as KRT_PartnerMinosito on EREC_IraIratok.Minosito = KRT_PartnerMinosito.Id
		LEFT JOIN KRT_Partnerek as KRT_PartnerMinositoSzerv on EREC_IraIratok.MinositoSzervezet = KRT_PartnerMinositoSzerv.Id
		LEFT JOIN (select kip.Peldany_Id as Peldany_Id, t.*
		              from EREC_Kuldemeny_IratPeldanyai kip
		                   inner join EREC_KuldTertivevenyek as t on kip.KuldKuldemeny_Id = t.Kuldemeny_Id
		              Where getdate() between kip.ErvKezd and kip.ErvVege) as EREC_KuldTertivevenyek On EREC_KuldTertivevenyek.Peldany_Id = EREC_PldIratPeldanyok.Id 
	   WHERE RowNumber between @firstRow and @lastRow
	   ORDER BY #result.RowNumber; ';

	-- találatok száma és oldalszám
	SET @sqlcmd += N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result /* #filter - nem került legyűjtésre */; ';

--- ez itt nyomkövetés - KI KELL kapcsolni éles futáson
---    SET @sqlcmd += 'insert into #timeTMP select ''99 - ITT a VÉGE'', current_timestamp; '+char(13)+char(13);

---print LEFT(@sqlcmd,4000)
---print substring(@sqlcmd, 4001,4000)
---print substring(@sqlcmd, 8001,4000)
---print substring(@sqlcmd, 12001,4000)
---print substring(@sqlcmd, 16001,4000)
---print substring(@sqlcmd, 20001,4000)
---print substring(@sqlcmd, 24001,4000)
---print substring(@sqlcmd, 28001,4000)
---print substring(@sqlcmd, 32001,4000)
---print substring(@sqlcmd, 36001,4000)
---print substring(@sqlcmd, 40001,4000)
---print substring(@sqlcmd, 44001,4000)
---
print @sqlcmd

    execute sp_executesql @sqlcmd,
	                      N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @ExecutorUserId uniqueidentifier, @FelhasznaloSzervezet_Id uniqueidentifier, @SelectedRowId uniqueidentifier, 
						    @Org uniqueidentifier'
			              ,@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @ExecutorUserId = @ExecutorUserId, @FelhasznaloSzervezet_Id = @FelhasznaloSzervezet_Id, 
						   @SelectedRowId = @SelectedRowId, @Org = @Org;

--- ez itt nyomkövetés - KI KELL kapcsolni éles futáson
---select * from #timeTMP order by 1;

  END TRY
  BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT;
	DECLARE @errorCode NVARCHAR(1000);
	SET @errorSeverity = ERROR_SEVERITY();
	SET @errorState = ERROR_STATE();
	
	if ERROR_NUMBER()<50000	
	  SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE();
	else
	  SET @errorCode = ERROR_MESSAGE();
      
    if @errorState = 0 
	  SET @errorState = 1;
	
	RAISERROR(@errorCode,@errorSeverity,@errorState);
 
  END CATCH;

END;
