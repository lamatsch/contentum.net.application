﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_PldIratPeldanyokHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_EREC_PldIratPeldanyokHistoryGetRecord
go
*/
create procedure sp_EREC_PldIratPeldanyokHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_PldIratPeldanyokHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_PldIratPeldanyokHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIrat_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_IraIratokAzonosito(Old.IraIrat_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_IraIratokAzonosito(New.IraIrat_Id) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IraIrat_Id as nvarchar(max)),'') != ISNULL(CAST(New.IraIrat_Id as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyUgyirat_Id_Kulso' as ColumnName,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratokAzonosito(Old.UgyUgyirat_Id_Kulso) as OldValue,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratokAzonosito(New.UgyUgyirat_Id_Kulso) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyUgyirat_Id_Kulso as nvarchar(max)),'') != ISNULL(CAST(New.UgyUgyirat_Id_Kulso as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Sorszam' as ColumnName,               
               cast(Old.Sorszam as nvarchar(99)) as OldValue,
               cast(New.Sorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Sorszam as nvarchar(max)),'') != ISNULL(CAST(New.Sorszam as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SztornirozasDat' as ColumnName,               
               cast(Old.SztornirozasDat as nvarchar(99)) as OldValue,
               cast(New.SztornirozasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SztornirozasDat as nvarchar(max)),'') != ISNULL(CAST(New.SztornirozasDat as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AtvetelDatuma' as ColumnName,               
               cast(Old.AtvetelDatuma as nvarchar(99)) as OldValue,
               cast(New.AtvetelDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AtvetelDatuma as nvarchar(max)),'') != ISNULL(CAST(New.AtvetelDatuma as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Eredet' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Eredet as nvarchar(max)),'') != ISNULL(CAST(New.Eredet as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRAT_FAJTA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Eredet and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Eredet and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldesMod' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KuldesMod as nvarchar(max)),'') != ISNULL(CAST(New.KuldesMod as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'KULDEMENY_KULDES_MODJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.KuldesMod and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.KuldesMod and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ragszam' as ColumnName,               
               cast(Old.Ragszam as nvarchar(99)) as OldValue,
               cast(New.Ragszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Ragszam as nvarchar(max)),'') != ISNULL(CAST(New.Ragszam as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesModja' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyintezesModja as nvarchar(max)),'') != ISNULL(CAST(New.UgyintezesModja as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
		 left join KRT_KodCsoportok KCs on KCs.Kod = 'IRAT_UGYINT_MODJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.UgyintezesModja and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.UgyintezesModja and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VisszaerkezesiHatarido' as ColumnName,               
               cast(Old.VisszaerkezesiHatarido as nvarchar(99)) as OldValue,
               cast(New.VisszaerkezesiHatarido as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.VisszaerkezesiHatarido as nvarchar(max)),'') != ISNULL(CAST(New.VisszaerkezesiHatarido as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Visszavarolag' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Visszavarolag as nvarchar(max)),'') != ISNULL(CAST(New.Visszavarolag as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
		 left join KRT_KodCsoportok KCs on KCs.Kod = 'VISSZAVAROLAG'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Visszavarolag and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Visszavarolag and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VisszaerkezesDatuma' as ColumnName,               
               cast(Old.VisszaerkezesDatuma as nvarchar(99)) as OldValue,
               cast(New.VisszaerkezesDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.VisszaerkezesDatuma as nvarchar(max)),'') != ISNULL(CAST(New.VisszaerkezesDatuma as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Cim_id_Cimzett' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CimekAzonosito(Old.Cim_id_Cimzett) as OldValue,
/*FK*/           dbo.fn_GetKRT_CimekAzonosito(New.Cim_id_Cimzett) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Cim_id_Cimzett as nvarchar(max)),'') != ISNULL(CAST(New.Cim_id_Cimzett as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id_Cimzett' as ColumnName,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(Old.Partner_Id_Cimzett) as OldValue,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(New.Partner_Id_Cimzett) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Partner_Id_Cimzett as nvarchar(max)),'') != ISNULL(CAST(New.Partner_Id_Cimzett as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
	   (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id_CimzettKapcsolt' as ColumnName,               
 /*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(Old.Partner_Id_CimzettKapcsolt) as OldValue,
 /*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(New.Partner_Id_CimzettKapcsolt) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Partner_Id_CimzettKapcsolt as nvarchar(max)),'') != ISNULL(CAST(New.Partner_Id_CimzettKapcsolt as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Felelos' as ColumnName,               
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Felelos) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Felelos) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id_Felelos as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id_Felelos as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Orzo' as ColumnName,               
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Orzo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Orzo) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Orzo as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Orzo as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Felelos_Elozo' as ColumnName,               
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Felelos_Elozo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Felelos_Elozo) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id_Felelos_Elozo as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id_Felelos_Elozo as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CimSTR_Cimzett' as ColumnName,               
               cast(Old.CimSTR_Cimzett as nvarchar(99)) as OldValue,
               cast(New.CimSTR_Cimzett as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.CimSTR_Cimzett as nvarchar(max)),'') != ISNULL(CAST(New.CimSTR_Cimzett as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'NevSTR_Cimzett' as ColumnName,               
               cast(Old.NevSTR_Cimzett as nvarchar(99)) as OldValue,
               cast(New.NevSTR_Cimzett as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.NevSTR_Cimzett as nvarchar(max)),'') != ISNULL(CAST(New.NevSTR_Cimzett as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tovabbito' as ColumnName,               
               cast(Old.Tovabbito as nvarchar(99)) as OldValue,
               cast(New.Tovabbito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tovabbito as nvarchar(max)),'') != ISNULL(CAST(New.Tovabbito as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIrat_Id_Kapcsolt' as ColumnName,
/*FK*/           dbo.fn_GetEREC_IraIratokAzonosito(Old.IraIrat_Id_Kapcsolt) as OldValue,
/*FK*/           dbo.fn_GetEREC_IraIratokAzonosito(New.IraIrat_Id_Kapcsolt) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IraIrat_Id_Kapcsolt as nvarchar(max)),'') != ISNULL(CAST(New.IraIrat_Id_Kapcsolt as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattariHely' as ColumnName,               
               cast(Old.IrattariHely as nvarchar(99)) as OldValue,
               cast(New.IrattariHely as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IrattariHely as nvarchar(max)),'') != ISNULL(CAST(New.IrattariHely as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Gener_Id' as ColumnName,               
               cast(Old.Gener_Id as nvarchar(99)) as OldValue,
               cast(New.Gener_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Gener_Id as nvarchar(max)),'') != ISNULL(CAST(New.Gener_Id as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PostazasDatuma' as ColumnName,               
               cast(Old.PostazasDatuma as nvarchar(99)) as OldValue,
               cast(New.PostazasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PostazasDatuma as nvarchar(max)),'') != ISNULL(CAST(New.PostazasDatuma as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BarCode' as ColumnName,               
               cast(Old.BarCode as nvarchar(99)) as OldValue,
               cast(New.BarCode as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.BarCode as nvarchar(max)),'') != ISNULL(CAST(New.BarCode as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Allapot as nvarchar(max)),'') != ISNULL(CAST(New.Allapot as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRATPELDANY_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Allapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Allapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Azonosito' as ColumnName,               
               cast(Old.Azonosito as nvarchar(99)) as OldValue,
               cast(New.Azonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Azonosito as nvarchar(max)),'') != ISNULL(CAST(New.Azonosito as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kovetkezo_Felelos_Id' as ColumnName,               
               cast(Old.Kovetkezo_Felelos_Id as nvarchar(99)) as OldValue,
               cast(New.Kovetkezo_Felelos_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Kovetkezo_Felelos_Id as nvarchar(max)),'') != ISNULL(CAST(New.Kovetkezo_Felelos_Id as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Elektronikus_Kezbesitesi_Allap' as ColumnName,               
               cast(Old.Elektronikus_Kezbesitesi_Allap as nvarchar(99)) as OldValue,
               cast(New.Elektronikus_Kezbesitesi_Allap as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Elektronikus_Kezbesitesi_Allap as nvarchar(max)),'') != ISNULL(CAST(New.Elektronikus_Kezbesitesi_Allap as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kovetkezo_Orzo_Id' as ColumnName,               
               cast(Old.Kovetkezo_Orzo_Id as nvarchar(99)) as OldValue,
               cast(New.Kovetkezo_Orzo_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Kovetkezo_Orzo_Id as nvarchar(max)),'') != ISNULL(CAST(New.Kovetkezo_Orzo_Id as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Fizikai_Kezbesitesi_Allapot' as ColumnName,               
               cast(Old.Fizikai_Kezbesitesi_Allapot as nvarchar(99)) as OldValue,
               cast(New.Fizikai_Kezbesitesi_Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Fizikai_Kezbesitesi_Allapot as nvarchar(max)),'') != ISNULL(CAST(New.Fizikai_Kezbesitesi_Allapot as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TovabbitasAlattAllapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.TovabbitasAlattAllapot as nvarchar(max)),'') != ISNULL(CAST(New.TovabbitasAlattAllapot as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRATPELDANY_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.TovabbitasAlattAllapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.TovabbitasAlattAllapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PostazasAllapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PostazasAllapot as nvarchar(max)),'') != ISNULL(CAST(New.PostazasAllapot as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRATPELDANY_POSTAZAS_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.PostazasAllapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.PostazasAllapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ValaszElektronikus' as ColumnName,
               case Old.ValaszElektronikus when 0 then 'Elektronikus' when 1 then 'Papír alapú' when 2 then 'Egyéb' end as OldValue,
               case New.ValaszElektronikus when 0 then 'Elektronikus' when 1 then 'Papír alapú' when 2 then 'Egyéb' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ValaszElektronikus as nvarchar(max)),'') != ISNULL(CAST(New.ValaszElektronikus as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SelejtezesDat' as ColumnName,               
               cast(Old.SelejtezesDat as nvarchar(99)) as OldValue,
               cast(New.SelejtezesDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SelejtezesDat as nvarchar(max)),'') != ISNULL(CAST(New.SelejtezesDat as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhCsoport_Id_Selejtezo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhCsoport_Id_Selejtezo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhCsoport_Id_Selejtezo) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhCsoport_Id_Selejtezo as nvarchar(max)),'') != ISNULL(CAST(New.FelhCsoport_Id_Selejtezo as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'LeveltariAtvevoNeve' as ColumnName,               
               cast(Old.LeveltariAtvevoNeve as nvarchar(99)) as OldValue,
               cast(New.LeveltariAtvevoNeve as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
         inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.LeveltariAtvevoNeve as nvarchar(max)),'') != ISNULL(CAST(New.LeveltariAtvevoNeve as nvarchar(max)),'')  
			and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)  
	  union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarId' as ColumnName,               
               cast(Old.IrattarId as nvarchar(99)) as OldValue,
               cast(New.IrattarId as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_PldIratPeldanyokHistory Old
        inner join EREC_PldIratPeldanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_PldIratPeldanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IrattarId as nvarchar(max)),'') != ISNULL(CAST(New.IrattarId as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end