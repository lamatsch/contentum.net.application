﻿
CREATE procedure [dbo].[sp_EREC_PldIratPeldanyokUpdate_Tomeges]
        @Ids						NVARCHAR(MAX)    ,
        @Vers						NVARCHAR(MAX)	 ,
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @IraIrat_Id     uniqueidentifier  = null,         
             @UgyUgyirat_Id_Kulso     uniqueidentifier  = null,         
             @Sorszam     int  = null,         
             @SztornirozasDat     datetime  = null,         
             @AtvetelDatuma     datetime  = null,         
             @Eredet     nvarchar(64)  = null,         
             @KuldesMod     nvarchar(64)  = null,         
             @Ragszam     Nvarchar(100)  = null,         
             @UgyintezesModja     nvarchar(64)  = null,         
             @VisszaerkezesiHatarido     datetime  = null,         
             @Visszavarolag     nvarchar(64)  = null,         
             @VisszaerkezesDatuma     datetime  = null,         
             @Cim_id_Cimzett     uniqueidentifier  = null,         
             @Partner_Id_Cimzett     uniqueidentifier  = null,         
             @Csoport_Id_Felelos     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Orzo     uniqueidentifier  = null,         
             @Csoport_Id_Felelos_Elozo     uniqueidentifier  = null,         
             @CimSTR_Cimzett     Nvarchar(400)  = null,         
             @NevSTR_Cimzett     Nvarchar(400)  = null,         
             @Tovabbito     nvarchar(64)  = null,         
             @IraIrat_Id_Kapcsolt     uniqueidentifier  = null,         
             @IrattariHely     Nvarchar(100)  = null,         
             @Gener_Id     uniqueidentifier  = null,         
             @PostazasDatuma     datetime  = null,         
             @BarCode     Nvarchar(100)  = null,         
             @Allapot     nvarchar(64)  = null,         
             @Azonosito     Nvarchar(100)  = null,         
             @Kovetkezo_Felelos_Id     uniqueidentifier  = null,         
             @Elektronikus_Kezbesitesi_Allap     nvarchar(64)  = null,         
             @Kovetkezo_Orzo_Id     uniqueidentifier  = null,         
             @Fizikai_Kezbesitesi_Allapot     nvarchar(64)  = null,         
             @TovabbitasAlattAllapot     nvarchar(64)  = null,         
             @PostazasAllapot     nvarchar(64)  = null,         
             @ValaszElektronikus     char(1)  = null, 
			 @SelejtezesDat     datetime  = null,         
             @FelhCsoport_Id_Selejtezo     uniqueidentifier  = null,         
             @LeveltariAtvevoNeve     Nvarchar(100)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,      
			 @IratPeldanyMegsemmisitve char(1) = null,
			 @IratPeldanyMegsemmisitesDatuma datetime = null,
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
	declare @tempTable table(id uniqueidentifier, ver int, orzo_elozo uniqueidentifier, partner_id_cimzett_elozo uniqueidentifier);
	
	declare @it	int;
	declare @verPosBegin int;
	declare @verPosEnd int;
	declare @curId	nvarchar(36);
	declare @curVer	nvarchar(6);

	set @it = 0;
	set @verPosBegin = 1;
	while (@it < ((len(@Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Ids,@it*39+2,37);
		set @verPosEnd =  CHARINDEX(',',@Vers,@verPosBegin);
		if @verPosEnd = 0 
			set @verPosEnd = len(@Vers) + 1;
		set @curVer = SUBSTRING(@vers, @verPosBegin, @verPosEnd - @verPosBegin);
		set @verPosBegin = @verPosEnd+1;
		insert into @tempTable(id,ver) values(@curId,convert(int,@curVer) );
		set @it = @it + 1;
	END
	

	-- tömeges history loghoz
	declare @row_ids varchar(MAX)

	declare @kt_Megkereses nvarchar(64)
	set @kt_Megkereses = '7'

	declare @rowNumberTotal int;
	declare @rowNumberChecked int;

	select @rowNumberTotal = count(id) from @tempTable;
	
	create table #pldIds (Id uniqueidentifier)
	/******************************************
	* Ellenőrzések
	******************************************/
	-- verzióellenőrzés
	select @rowNumberChecked = count(EREC_PldIratPeldanyok.Id)
		from EREC_PldIratPeldanyok
			inner JOIN @tempTable t on t.id = EREC_PldIratPeldanyok.id and t.ver = EREC_PldIratPeldanyok.ver
			
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
			inner join @tempTable t on t.id = EREC_PldIratPeldanyok.id and t.ver = EREC_PldIratPeldanyok.ver
		RAISERROR('[50402]',16,1);
	END
	
	-- zárolásellenőrzés
	select @rowNumberChecked = count(EREC_PldIratPeldanyok.Id)
		from EREC_PldIratPeldanyok
			inner JOIN @tempTable t on t.id = EREC_PldIratPeldanyok.id and ISNULL(EREC_PldIratPeldanyok.Zarolo_id,@ExecutorUserId) = @ExecutorUserId
			
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
			inner join @tempTable t on t.id = EREC_PldIratPeldanyok.id and ISNULL(EREC_PldIratPeldanyok.Zarolo_id,@ExecutorUserId) = @ExecutorUserId
		RAISERROR('[50499]',16,1);
	END
	
	-- érvényesség vége figyelése
	select @rowNumberChecked = count(EREC_PldIratPeldanyok.Id)
		from EREC_PldIratPeldanyok
			inner JOIN @tempTable t on t.id = EREC_PldIratPeldanyok.id and EREC_PldIratPeldanyok.ErvVege > @ExecutionTime
			
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
			inner join @tempTable t on t.id = EREC_PldIratPeldanyok.id and EREC_PldIratPeldanyok.ErvVege > @ExecutionTime
		RAISERROR('[50403]',16,1);
	END
	
	-- mozgatás esetén fizikai mappa ellenőrés
	IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1 AND EXISTS
		(
			SELECT 1
				FROM EREC_PldIratPeldanyok
					INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = EREC_PldIratPeldanyok.Id
					INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
				WHERE EREC_PldIratPeldanyok.Id IN (SELECT Id FROM @tempTable)
					AND EREC_PldIratPeldanyok.Csoport_Id_Felelos != @Csoport_Id_Felelos
					AND KRT_Mappak.Tipus = '01'
					AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
		)
	BEGIN
		SELECT EREC_PldIratPeldanyok.Id
			FROM EREC_PldIratPeldanyok
				INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = EREC_PldIratPeldanyok.Id
				INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
			WHERE EREC_PldIratPeldanyok.Id IN (SELECT Id FROM @tempTable)
				AND EREC_PldIratPeldanyok.Csoport_Id_Felelos != @Csoport_Id_Felelos
				AND KRT_Mappak.Tipus = '01'
				AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
				AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
		RAISERROR('[64026]',16,1); -- Az iratpéldány csak dossziéval együtt mozgatható!
	END
	IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Orzo')=1 AND EXISTS
		(
			SELECT 1
				FROM EREC_PldIratPeldanyok
					INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = EREC_PldIratPeldanyok.Id
					INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
				WHERE EREC_PldIratPeldanyok.Id IN (SELECT Id FROM @tempTable)
					AND EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo != FelhasznaloCsoport_Id_Orzo
					AND KRT_Mappak.Tipus = '01'
					AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
		)
	BEGIN
		SELECT EREC_PldIratPeldanyok.Id
			FROM EREC_PldIratPeldanyok
				INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = EREC_PldIratPeldanyok.Id
				INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
			WHERE EREC_PldIratPeldanyok.Id IN (SELECT Id FROM @tempTable)
				AND EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo != FelhasznaloCsoport_Id_Orzo
				AND KRT_Mappak.Tipus = '01'
				AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
				AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
		RAISERROR('[64026]',16,1); -- Az iratpéldány csak dossziéval együtt mozgatható!
	END
	
	/* Kézbesítési tétel létrehozása ha szükséges */
	IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1
	BEGIN
		UPDATE EREC_IraKezbesitesiTetelek SET ErvVege = GETDATE() WHERE Obj_Id IN
			(SELECT Id FROM EREC_PldIratPeldanyok WHERE Id IN (SELECT Id FROM @tempTable) AND Csoport_Id_Felelos != @Csoport_Id_Felelos)
		INSERT INTO EREC_IraKezbesitesiTetelek(Obj_Id, Obj_type, Felhasznalo_Id_Atado_LOGIN, Felhasznalo_Id_Atado_USER, Csoport_Id_Cel, Allapot)
			SELECT Id, 'EREC_PldIratPeldanyok', @ExecutorUserId, @ExecutorUserId, @Csoport_Id_Felelos, '1' FROM EREC_PldIratPeldanyok WHERE Id IN (SELECT Id FROM @tempTable) AND Csoport_Id_Felelos != @Csoport_Id_Felelos
	END


	-- előző őrző lekérése az ACL-kezeléshez: csak akkor,
	-- ha a Visszavarolag értéke Megkereses volt vagy arra változott
	UPDATE t
		SET orzo_elozo = EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_orzo,
			partner_id_cimzett_elozo = EREC_PldIratPeldanyok.Partner_Id_Cimzett
		output inserted.id into #pldIds
		FROM  @tempTable t
		inner join EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.Id=t.Id
		WHERE 
			(
			(@UpdatedColumns.exist('/root/Visszavarolag')=1 and @Visszavarolag = @kt_Megkereses)
			OR
			(@UpdatedColumns.exist('/root/Visszavarolag')<>1 and EREC_PldIratPeldanyok.Visszavarolag = @kt_Megkereses)
			)
	-- Jelenleg nem szabad vonalkódot tömegesen update-elni, mert példányonként egyedinek kell lennie!
/*
	declare @vanUjVonalkod char(1)
	IF @UpdatedColumns.exist('/root/BarCode')=1
	BEGIN
		-- CheckBarCode -- eltérés a C# kódtól: nem nézzük, hogy az előző vonalkód üres volt-e
		declare @barCode_Id uniqueidentifier
		declare @barCode_Allapot = nvarchar(64)
		select top 1 @barCode_Id=Id, @barCode_Allpot=Allapot
			from KRT_BarKodok
			where Kod=@BarCode
			and KodType in ('N','P','G')
			and getdate() between ErvKezd and ErvVege

		if @barCode_Id is null
			RAISERROR('[52481]',16,1) -- A vonalkód nincs regisztrálva

		if @barCode_Allapot != 'S'
			RAISERROR('[52482]',16,1) -- A vonalkód nem szabad

		SET @vanUjVonalkod = 1
	END
*/



	/******************************************
	* UPDATE string összeállítás
	******************************************/
	DECLARE @sqlcmd	NVARCHAR(MAX);
	SET @sqlcmd = N'UPDATE EREC_PldIratPeldanyok SET';

	IF @UpdatedColumns.exist('/root/IraIrat_Id')=1
		SET @sqlcmd = @sqlcmd + N' IraIrat_Id = @IraIrat_Id,'
   IF @UpdatedColumns.exist('/root/UgyUgyirat_Id_Kulso')=1
        SET @sqlcmd = @sqlcmd + N' UgyUgyirat_Id_Kulso = @UgyUgyirat_Id_Kulso,'
   IF @UpdatedColumns.exist('/root/Sorszam')=1
         SET @sqlcmd = @sqlcmd + N' Sorszam = @Sorszam,'
   IF @UpdatedColumns.exist('/root/SztornirozasDat')=1
         SET @sqlcmd = @sqlcmd + N' SztornirozasDat = @SztornirozasDat,'
   IF @UpdatedColumns.exist('/root/AtvetelDatuma')=1
         SET @sqlcmd = @sqlcmd + N' AtvetelDatuma = @AtvetelDatuma,'
   IF @UpdatedColumns.exist('/root/Eredet')=1
         SET @sqlcmd = @sqlcmd + N' Eredet = @Eredet,'
   IF @UpdatedColumns.exist('/root/KuldesMod')=1
         SET @sqlcmd = @sqlcmd + N' KuldesMod = @KuldesMod,'
   IF @UpdatedColumns.exist('/root/Ragszam')=1
         SET @sqlcmd = @sqlcmd + N' Ragszam = @Ragszam,'
   IF @UpdatedColumns.exist('/root/UgyintezesModja')=1
         SET @sqlcmd = @sqlcmd + N' UgyintezesModja = @UgyintezesModja,'
   IF @UpdatedColumns.exist('/root/VisszaerkezesiHatarido')=1
         SET @sqlcmd = @sqlcmd + N' VisszaerkezesiHatarido = @VisszaerkezesiHatarido,'
   IF @UpdatedColumns.exist('/root/Visszavarolag')=1
         SET @sqlcmd = @sqlcmd + N' Visszavarolag = @Visszavarolag,'
   IF @UpdatedColumns.exist('/root/VisszaerkezesDatuma')=1
         SET @sqlcmd = @sqlcmd + N' VisszaerkezesDatuma = @VisszaerkezesDatuma,'
   IF @UpdatedColumns.exist('/root/Cim_id_Cimzett')=1
         SET @sqlcmd = @sqlcmd + N' Cim_id_Cimzett = @Cim_id_Cimzett,'
   IF @UpdatedColumns.exist('/root/Partner_Id_Cimzett')=1
         SET @sqlcmd = @sqlcmd + N' Partner_Id_Cimzett = @Partner_Id_Cimzett,'
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1
         SET @sqlcmd = @sqlcmd + N' Csoport_Id_Felelos = @Csoport_Id_Felelos,'
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Orzo')=1
         SET @sqlcmd = @sqlcmd + N' FelhasznaloCsoport_Id_Orzo = @FelhasznaloCsoport_Id_Orzo,'
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos_Elozo')=1
         SET @sqlcmd = @sqlcmd + N' Csoport_Id_Felelos_Elozo = @Csoport_Id_Felelos_Elozo,'
   IF @UpdatedColumns.exist('/root/CimSTR_Cimzett')=1
         SET @sqlcmd = @sqlcmd + N' CimSTR_Cimzett = @CimSTR_Cimzett,'
   IF @UpdatedColumns.exist('/root/NevSTR_Cimzett')=1
         SET @sqlcmd = @sqlcmd + N' NevSTR_Cimzett = @NevSTR_Cimzett,'
   IF @UpdatedColumns.exist('/root/Tovabbito')=1
         SET @sqlcmd = @sqlcmd + N' Tovabbito = @Tovabbito,'
   IF @UpdatedColumns.exist('/root/IraIrat_Id_Kapcsolt')=1
         SET @sqlcmd = @sqlcmd + N' IraIrat_Id_Kapcsolt = @IraIrat_Id_Kapcsolt,'
   IF @UpdatedColumns.exist('/root/IrattariHely')=1
         SET @sqlcmd = @sqlcmd + N' IrattariHely = @IrattariHely,'
   IF @UpdatedColumns.exist('/root/Gener_Id')=1
         SET @sqlcmd = @sqlcmd + N' Gener_Id = @Gener_Id,'
   IF @UpdatedColumns.exist('/root/PostazasDatuma')=1
         SET @sqlcmd = @sqlcmd + N' PostazasDatuma = @PostazasDatuma,'
   IF @UpdatedColumns.exist('/root/BarCode')=1
         SET @sqlcmd = @sqlcmd + N' BarCode = @BarCode,'
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @sqlcmd = @sqlcmd + N' Allapot = @Allapot,'
   IF @UpdatedColumns.exist('/root/Azonosito')=1
         SET @sqlcmd = @sqlcmd + N' Azonosito = @Azonosito,'
   IF @UpdatedColumns.exist('/root/Kovetkezo_Felelos_Id')=1
         SET @sqlcmd = @sqlcmd + N' Kovetkezo_Felelos_Id = @Kovetkezo_Felelos_Id,'
   IF @UpdatedColumns.exist('/root/Elektronikus_Kezbesitesi_Allap')=1
         SET @sqlcmd = @sqlcmd + N' Elektronikus_Kezbesitesi_Allap = @Elektronikus_Kezbesitesi_Allap,'
   IF @UpdatedColumns.exist('/root/Kovetkezo_Orzo_Id')=1
         SET @sqlcmd = @sqlcmd + N' Kovetkezo_Orzo_Id = @Kovetkezo_Orzo_Id,'
   IF @UpdatedColumns.exist('/root/Fizikai_Kezbesitesi_Allapot')=1
         SET @sqlcmd = @sqlcmd + N' Fizikai_Kezbesitesi_Allapot = @Fizikai_Kezbesitesi_Allapot,'
   IF @UpdatedColumns.exist('/root/TovabbitasAlattAllapot')=1
         SET @sqlcmd = @sqlcmd + N' TovabbitasAlattAllapot = @TovabbitasAlattAllapot,'
   IF @UpdatedColumns.exist('/root/PostazasAllapot')=1
         SET @sqlcmd = @sqlcmd + N' PostazasAllapot = @PostazasAllapot,'
   IF @UpdatedColumns.exist('/root/ValaszElektronikus')=1
		 SET @sqlcmd = @sqlcmd + N' ValaszElektronikus = @ValaszElektronikus,'
   IF @UpdatedColumns.exist('/root/SelejtezesDat')=1
         SET @sqlcmd = @sqlcmd + N' SelejtezesDat = @SelejtezesDat,'
   IF @UpdatedColumns.exist('/root/FelhCsoport_Id_Selejtezo')=1
         SET @sqlcmd = @sqlcmd + N' FelhCsoport_Id_Selejtezo = @FelhCsoport_Id_Selejtezo,'
   IF @UpdatedColumns.exist('/root/LeveltariAtvevoNeve')=1
         SET @sqlcmd = @sqlcmd + N' LeveltariAtvevoNeve = @LeveltariAtvevoNeve,'
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @sqlcmd = @sqlcmd + N' Note = @Note,'
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @sqlcmd = @sqlcmd + N' Stat_id = @Stat_id,'
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @sqlcmd = @sqlcmd + N' ErvKezd = @ErvKezd,'
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @sqlcmd = @sqlcmd + N' ErvVege = @ErvVege,'
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @sqlcmd = @sqlcmd + N' Letrehozo_id = @Letrehozo_id,'
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @sqlcmd = @sqlcmd + N' LetrehozasIdo = @LetrehozasIdo,'
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @sqlcmd = @sqlcmd + N' Modosito_id = @Modosito_id,'
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @sqlcmd = @sqlcmd + N' ModositasIdo = @ModositasIdo,'
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @sqlcmd = @sqlcmd + N' Zarolo_id = @Zarolo_id,'
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @sqlcmd = @sqlcmd + N' ZarolasIdo = @ZarolasIdo,'
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @sqlcmd = @sqlcmd + N' Tranz_id = @Tranz_id,'
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @sqlcmd = @sqlcmd + N' UIAccessLog_id = @UIAccessLog_id,'
   IF @UpdatedColumns.exist('/root/IratPeldanyMegsemmisitve')=1
         SET @sqlcmd = @sqlcmd + N' IratPeldanyMegsemmisitve = @IratPeldanyMegsemmisitve,'
   IF @UpdatedColumns.exist('/root/IratPeldanyMegsemmisitesDatuma')=1
         SET @sqlcmd = @sqlcmd + N' IratPeldanyMegsemmisitesDatuma = @IratPeldanyMegsemmisitesDatuma,'

   SET @sqlcmd = @sqlcmd + N' Ver = Ver + 1' 

   SET @sqlcmd = @sqlcmd + N'
	WHERE Id IN (' + @Ids + ');'

	EXECUTE sp_executesql @sqlcmd, N'
	 @IraIrat_Id     uniqueidentifier,         
	 @UgyUgyirat_Id_Kulso     uniqueidentifier,         
	 @Sorszam     int,         
	 @SztornirozasDat     datetime,         
	 @AtvetelDatuma     datetime,         
	 @Eredet     nvarchar(64),         
	 @KuldesMod     nvarchar(64),         
	 @Ragszam     Nvarchar(100),         
	 @UgyintezesModja     nvarchar(64),         
	 @VisszaerkezesiHatarido     datetime,         
	 @Visszavarolag     nvarchar(64),         
	 @VisszaerkezesDatuma     datetime,         
	 @Cim_id_Cimzett     uniqueidentifier,         
	 @Partner_Id_Cimzett     uniqueidentifier,         
	 @Csoport_Id_Felelos     uniqueidentifier,         
	 @FelhasznaloCsoport_Id_Orzo     uniqueidentifier,         
	 @Csoport_Id_Felelos_Elozo     uniqueidentifier,         
	 @CimSTR_Cimzett     Nvarchar(400),         
	 @NevSTR_Cimzett     Nvarchar(400),         
	 @Tovabbito     nvarchar(64),         
	 @IraIrat_Id_Kapcsolt     uniqueidentifier,         
	 @IrattariHely     Nvarchar(100),         
	 @Gener_Id     uniqueidentifier,         
	 @PostazasDatuma     datetime,         
	 @BarCode     Nvarchar(100),         
	 @Allapot     nvarchar(64),         
	 @Azonosito     Nvarchar(100),         
	 @Kovetkezo_Felelos_Id     uniqueidentifier,         
	 @Elektronikus_Kezbesitesi_Allap     nvarchar(64),         
	 @Kovetkezo_Orzo_Id     uniqueidentifier,         
	 @Fizikai_Kezbesitesi_Allapot     nvarchar(64),         
	 @TovabbitasAlattAllapot     nvarchar(64),         
	 @PostazasAllapot     nvarchar(64),         
	 @ValaszElektronikus     char(1),
	 @SelejtezesDat     datetime,         
     @FelhCsoport_Id_Selejtezo     uniqueidentifier,         
     @LeveltariAtvevoNeve     Nvarchar(100),             
	 @Ver     int,         
	 @Note     Nvarchar(4000),         
	 @Stat_id     uniqueidentifier,         
	 @ErvKezd     datetime,         
	 @ErvVege     datetime,         
	 @Letrehozo_id     uniqueidentifier,         
	 @LetrehozasIdo     datetime,         
	 @Modosito_id     uniqueidentifier,         
	 @ModositasIdo     datetime,         
	 @Zarolo_id     uniqueidentifier,         
	 @ZarolasIdo     datetime,         
	 @Tranz_id     uniqueidentifier,
	 @IratPeldanyMegsemmisitve bit,
	 @IratPeldanyMegsemmisitesDatuma datetime,
	 @UIAccessLog_id     uniqueidentifier',
	 @IraIrat_Id = @IraIrat_Id,
	 @UgyUgyirat_Id_Kulso = @UgyUgyirat_Id_Kulso,
	 @Sorszam = @Sorszam,
	 @SztornirozasDat = @SztornirozasDat,
	 @AtvetelDatuma = @AtvetelDatuma,
	 @Eredet = @Eredet,
	 @KuldesMod = @KuldesMod,
	 @Ragszam = @Ragszam,
	 @UgyintezesModja = @UgyintezesModja,
	 @VisszaerkezesiHatarido = @VisszaerkezesiHatarido,
	 @Visszavarolag = @Visszavarolag,
	 @VisszaerkezesDatuma = @VisszaerkezesDatuma,
	 @Cim_id_Cimzett = @Cim_id_Cimzett,
	 @Partner_Id_Cimzett = @Partner_Id_Cimzett,
	 @Csoport_Id_Felelos = @Csoport_Id_Felelos,
	 @FelhasznaloCsoport_Id_Orzo = @FelhasznaloCsoport_Id_Orzo,
	 @Csoport_Id_Felelos_Elozo = @Csoport_Id_Felelos_Elozo,
	 @CimSTR_Cimzett = @CimSTR_Cimzett,
	 @NevSTR_Cimzett = @NevSTR_Cimzett,
	 @Tovabbito = @Tovabbito,
	 @IraIrat_Id_Kapcsolt = @IraIrat_Id_Kapcsolt,
	 @IrattariHely = @IrattariHely,
	 @Gener_Id = @Gener_Id,
	 @PostazasDatuma = @PostazasDatuma,
	 @BarCode = @BarCode,
	 @Allapot = @Allapot,
	 @Azonosito = @Azonosito,
	 @Kovetkezo_Felelos_Id = @Kovetkezo_Felelos_Id,
	 @Elektronikus_Kezbesitesi_Allap = @Elektronikus_Kezbesitesi_Allap,
	 @Kovetkezo_Orzo_Id = @Kovetkezo_Orzo_Id,
	 @Fizikai_Kezbesitesi_Allapot = @Fizikai_Kezbesitesi_Allapot,
	 @TovabbitasAlattAllapot = @TovabbitasAlattAllapot,
	 @PostazasAllapot = @PostazasAllapot,
	 @ValaszElektronikus = @ValaszElektronikus,
	 @SelejtezesDat = @SelejtezesDat,         
     @FelhCsoport_Id_Selejtezo = @FelhCsoport_Id_Selejtezo,         
     @LeveltariAtvevoNeve = @LeveltariAtvevoNeve,
	 @Ver = @Ver,
	 @Note = @Note,
	 @Stat_id = @Stat_id,
	 @ErvKezd = @ErvKezd,
	 @ErvVege = @ErvVege,
	 @Letrehozo_id = @Letrehozo_id,
	 @LetrehozasIdo = @LetrehozasIdo,
	 @Modosito_id = @Modosito_id,
	 @ModositasIdo = @ModositasIdo,
	 @Zarolo_id = @Zarolo_id,
	 @ZarolasIdo = @ZarolasIdo,
	 @Tranz_id = @Tranz_id,
	 @IratPeldanyMegsemmisitve = @IratPeldanyMegsemmisitve,
	 @IratPeldanyMegsemmisitesDatuma = @IratPeldanyMegsemmisitesDatuma,
	 @UIAccessLog_id = @UIAccessLog_id;

	if @@rowcount != @rowNumberTotal
	begin
		RAISERROR('[50401]',16,1)
	END

    -- UgyintezesModja mező változásának figyelése
	IF @UpdatedColumns.exist('/root/UgyintezesModja')=1
	BEGIN
		 -- Ha megváltozott az UgyintezesModja mező az 1-es iratpéldánynál, akkor meg kell update-elni az iratot
		declare @iratIds table (Id uniqueidentifier)
		UPDATE EREC_IraIratok
			SET UgyintezesAlapja = @UgyintezesModja,
			Ver = EREC_IraIratok.Ver + 1,
			Modosito_Id = @ExecutorUserId,
			ModositasIdo = @executionTime
			output inserted.id into @iratIds
			FROM EREC_PldIratPeldanyok
			inner join @tempTable t on t.id = EREC_PldIratPeldanyok.id and EREC_PldIratPeldanyok.ErvVege > @ExecutionTime
			where EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id
				and EREC_PldIratPeldanyok.Sorszam = 1
				and EREC_PldIratPeldanyok.UgyintezesModja <> EREC_IraIratok.UgyintezesAlapja

			IF exists(select 1 from @iratIds)
			BEGIN
	--			declare @row_ids varchar(MAX)
				/*** EREC_IraIratok history log ***/

				set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @iratIds FOR XML PATH('')),1, 2, '')
				if @row_ids is not null
					set @row_ids = @row_ids + '''';

				exec sp_LogRecordsToHistory_Tomeges
				 @TableName = 'EREC_IraIratok'
				,@Row_Ids = @row_ids
				,@Muvelet = 1
				,@Vegrehajto_Id = @ExecutorUserId
				,@VegrehajtasIdo = @ExecutionTime
			END
		END


		-- ACL kezelés
		IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Orzo')=1
		BEGIN
			-- Megkeresés esetén a felelősnek adni kell kézi írási jogot az ügyirathoz,
			-- továbbá bon jogot kell adni az iratpéldány címzettjének az irathoz, feltéve, hogy még nincs
			-- (csak átvétel esetén kell jogot adni, nem véletlenül van ebben a blokkban)
				insert into KRT_Jogosultak(Csoport_Id_Jogalany, Jogszint, Obj_Id, Kezi, Letrehozo_id, Tipus)
					SELECT @Csoport_Id_Felelos, 'I', tmp.UgyUgyirat_Id, 'I', @ExecutorUserId, 'M'
					FROM (select Id as UgyUgyirat_Id FROM EREC_UgyUgyiratok
						WHERE Id in (select EREC_IraIratok.Ugyirat_Id
										from @tempTable t
										inner join EREC_PldIratPeldanyok on t.Id = EREC_PldIratPeldanyok.Id
										inner join EREC_IraIratok on EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
									WHERE EREC_PldIratPeldanyok.Visszavarolag = @kt_Megkereses -- Megkereses
									and t.orzo_elozo is not null
									)
							and not exists(select KRT_Jogosultak.Id from KRT_Jogosultak KRT_Jogosultak
								where KRT_Jogosultak.Csoport_Id_Jogalany = @Csoport_Id_felelos
								AND KRT_Jogosultak.Obj_Id = EREC_UgyUgyiratok.Id
								AND KRT_Jogosultak.Kezi = 'I'
								AND KRT_Jogosultak.Jogszint = 'I'
								AND KRT_Jogosultak.Tipus = 'M')
						) tmp


				declare @pldCimzettId uniqueidentifier
				IF @UpdatedColumns.exist('/root/Partner_Id_Cimzett')=1
				BEGIN
					set @pldCimzettId = (select Id from KRT_Felhasznalok
						where Partner_id = @Partner_Id_Cimzett
						and getdate() between Ervkezd and ErvVege)
				END

				insert into KRT_Jogosultak(Csoport_Id_Jogalany, Jogszint, Obj_Id, Kezi, Letrehozo_id, Tipus)
					SELECT distinct pldCimzettId, 'I', tmp.IraIrat_Id, 'I', @ExecutorUserId, 'B'
					FROM (select EREC_PldIratPeldanyok.IraIrat_Id, IsNull(@pldCimzettId, KRT_Felhasznalok.Id) pldCimzettId
										from @tempTable t
										inner join EREC_PldIratPeldanyok on t.Id = EREC_PldIratPeldanyok.Id
										left join KRT_Felhasznalok on EREC_PldIratPeldanyok.Partner_Id_Cimzett = KRT_Felhasznalok.Partner_id and getdate() between KRT_Felhasznalok.ErvKezd and KRT_Felhasznalok.ErvVege
									WHERE EREC_PldIratPeldanyok.Visszavarolag = @kt_Megkereses -- Megkereses
									and IsNull(@pldCimzettId, KRT_Felhasznalok.Id) is not null
									and not exists(select KRT_Jogosultak.Id from KRT_Jogosultak KRT_Jogosultak
										where KRT_Jogosultak.Csoport_Id_Jogalany = IsNull(@pldCimzettId, KRT_Felhasznalok.Id)
												AND KRT_Jogosultak.Obj_Id = EREC_PldIratPeldanyok.IraIrat_Id
												AND KRT_Jogosultak.Kezi = 'I'
												AND KRT_Jogosultak.Jogszint = 'I'
												AND KRT_Jogosultak.Tipus = 'B')
						) tmp
		END
 
		
		-- iratpéldány mozgásának jelzése
		exec sp_EREC_PldIratPeldanyok_Moved @ExecutorUserId = @ExecutorUserId, @ExecutionTime = @ExecutionTime
		DROP TABLE #pldIds;


	/* History Log */

--	declare @row_ids varchar(MAX)
	/*** EREC_PldIratPeldanyok history log ***/

	set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @tempTable FOR XML PATH('')),1, 2, '')
	if @row_ids is not null
		set @row_ids = @row_ids + '''';

	exec sp_LogRecordsToHistory_Tomeges
	 @TableName = 'EREC_PldIratPeldanyok'
	,@Row_Ids = @row_ids
	,@Muvelet = 1
	,@Vegrehajto_Id = @ExecutorUserId
	,@VegrehajtasIdo = @ExecutionTime


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
END CATCH