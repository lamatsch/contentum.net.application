﻿/* 
IF EXISTS (SELECT 1
            FROM  SYSOBJECTS
           WHERE  ID = OBJECT_ID('SP_EREC_PLDIRATPELDANYOK_MOVED')
            AND   TYPE = 'P')
   DROP PROCEDURE SP_EREC_PLDIRATPELDANYOK_MOVED
GO
*/
-- =============================================
-- Author:		4IG
-- Create date: 2019.10.31
-- Description:	sp_EREC_PldIratPeldanyok_Moved
-- =============================================
create procedure [dbo].[sp_EREC_PldIratPeldanyok_Moved]
        @Id							UNIQUEIDENTIFIER = NULL,
		@ExecutorUserId				uniqueidentifier,
		@ExecutionTime			    DATETIME

as

BEGIN TRY

  
	set nocount on
	
	IF @Id IS NOT NULL
	BEGIN
		CREATE TABLE #pldIds(Id UNIQUEIDENTIFIER)
		
		INSERT INTO [#pldIds] (
			[Id]
		) VALUES (@Id) 
	END

	--Iratok Update
	UPDATE [EREC_IraIratok]
		SET Csoport_Id_Felelos = [EREC_PldIratPeldanyok].Csoport_Id_Felelos,
			FelhasznaloCsoport_Id_Orzo = [EREC_PldIratPeldanyok].FelhasznaloCsoport_Id_Orzo,
			Modosito_id = @ExecutorUserId,
			ModositasIdo = @ExecutionTime
			--,Ver = EREC_IraIratok.Ver + 1
		FROM [EREC_PldIratPeldanyok]
			 INNER JOIN [#pldIds]
			 ON [EREC_PldIratPeldanyok].[Id]=[#pldIds].Id
		WHERE [EREC_PldIratPeldanyok].[Sorszam]  = 1 
		AND [EREC_PldIratPeldanyok].IraIrat_Id = [EREC_IraIratok].Id
			 
	--Kuldemenyek Update
	UPDATE [EREC_KuldKuldemenyek]
		SET Csoport_Id_Felelos = [EREC_PldIratPeldanyok].Csoport_Id_Felelos,
			FelhasznaloCsoport_Id_Orzo = [EREC_PldIratPeldanyok].FelhasznaloCsoport_Id_Orzo,
			TovabbitasAlattAllapot = case EREC_PldIratPeldanyok.Allapot
				when '50' then
					case EREC_KuldKuldemenyek.Allapot
						when '03' then EREC_KuldKuldemenyek.TovabbitasAlattAllapot
						else EREC_KuldKuldemenyek.Allapot
					end
				else
					case EREC_KuldKuldemenyek.Allapot
						when '03' then null
						else EREC_KuldKuldemenyek.TovabbitasAlattAllapot
					end
				end,
			Allapot = case EREC_PldIratPeldanyok.Allapot
				when '50' then '03' -- mindenképp továbbítás alatti lesz
				else
					case EREC_KuldKuldemenyek.Allapot
						when '03' then EREC_KuldKuldemenyek.TovabbitasAlattAllapot
						else EREC_KuldKuldemenyek.Allapot
					end
				end,
			Modosito_id = @ExecutorUserId,
			ModositasIdo = @ExecutionTime
			--,Ver = EREC_KuldKuldemenyek.Ver + 1
		FROM [EREC_PldIratPeldanyok]
			 INNER JOIN [#pldIds]
			 ON [EREC_PldIratPeldanyok].[Id]=[#pldIds].Id
			 INNER JOIN [EREC_IraIratok]
		     ON [EREC_PldIratPeldanyok].IraIrat_Id = [EREC_IraIratok].Id
		WHERE [EREC_PldIratPeldanyok].[Sorszam]  = 1 
	    AND [EREC_IraIratok].KuldKuldemenyek_Id = [EREC_KuldKuldemenyek].Id
	    AND EREC_IraIratok.Allapot NOT IN ('08','06') --felszabadított, átiktatott

    /**TOMEGES HISTORY**/	
		DECLARE @row_ids varchar(MAX)
		SET @ROW_IDS = STUFF((SELECT ''',''' + convert(varchar(36), Id) from [#pldIds] FOR XML PATH('')),1, 2, '')
		IF @ROW_IDS IS NOT NULL
		  SET @ROW_IDS = @ROW_IDS + '''';

		EXEC sp_LogRecordsToHistory_Tomeges
			@TableName = 'EREC_PldIratPeldanyok'
		   ,@Row_Ids = @ROW_IDS
		   ,@Muvelet = 1
		   ,@Vegrehajto_Id = @ExecutorUserId
		   ,@VegrehajtasIdo = @ExecutionTime
    /**TOMEGES HISTORY**/	
			 
	IF @Id IS NOT NULL
	BEGIN
		DROP TABLE [#pldIds]
	END		 


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH