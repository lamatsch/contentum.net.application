﻿create procedure [dbo].[sp_EREC_PldIratpeldanyok_AtadasAtvetelTomeges]
	@Ids				nvarchar(MAX),
	@Vers				nvarchar(MAX),
	@KovetkezoFelelos	uniqueidentifier = null,
	@KovetkezoOrzo		uniqueidentifier = null,
	@ExecutorUserId		uniqueidentifier,
	@KezFeljegyzesTipus nvarchar(64) = null,
	@Leiras				nvarchar(400) = NULL,
	@CheckMappa			BIT = 1
as

BEGIN TRY
	set nocount ON
	
	if @KovetkezoFelelos is not null or @KovetkezoOrzo is not null
	BEGIN
	
	declare @execTime datetime;
	declare @tempTable table(id uniqueidentifier, ver int);

	set @execTime = getdate();

	/* dinamikus history log összeállításhoz */
	declare @row_ids varchar(MAX)
	declare @insertedRowIds table (Id uniqueidentifier)

	-- temp tába töltése az id-verzió párokkal
	declare @it	int;
	declare @verPosBegin int;
	declare @verPosEnd int;
	declare @curId	nvarchar(36);
	declare @curVer	nvarchar(6);
	declare @sqlcmd	nvarchar(500);

	set @it = 0;
	set @verPosBegin = 1;
	while (@it < ((len(@Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Ids,@it*39+2,37);
		set @verPosEnd =  CHARINDEX(',',@Vers,@verPosBegin);
		if @verPosEnd = 0 
			set @verPosEnd = len(@Vers) + 1;
		set @curVer = SUBSTRING(@vers, @verPosBegin, @verPosEnd - @verPosBegin);
		set @verPosBegin = @verPosEnd+1;
		insert into @tempTable(id,ver) values(@curId,convert(int,@curVer) );
		set @it = @it + 1;
	END

	-- verzióellenorzés
	declare @rowNumberTotal int;
	declare @rowNumberChecked int;

	select @rowNumberTotal = count(id) from @tempTable;
	select @rowNumberChecked = count(EREC_PldIratpeldanyok.Id)
		from EREC_PldIratpeldanyok
			inner join @tempTable t on t.id = EREC_PldIratpeldanyok.id and t.ver = EREC_PldIratpeldanyok.ver
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT EREC_PldIratpeldanyok.Id FROM EREC_PldIratpeldanyok
			inner join @tempTable t on t.id = EREC_PldIratpeldanyok.id and t.ver = EREC_PldIratpeldanyok.ver
		RAISERROR('[50402]',16,1);
	END
	
	CREATE TABLE #pldIds(Id UNIQUEIDENTIFIER)
	
	INSERT INTO [#pldIds] SELECT EREC_PldIratpeldanyok.Id
			FROM EREC_PldIratPeldanyok
			where Id in (select id from @tempTable)
				and (ModositasIdo < @execTime or ModositasIdo is null)
				and Zarolo_id is NULL
				
	-- Utóellenorzés
	if (@rowNumberTotal != (SELECT COUNT(*) FROM #pldIds) )
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		select Id from #pldIds
		DROP TABLE #pldIds;
		RAISERROR('[50402]',16,1);
	END			


	/****** Átadásra kijelölés *****/
	-- EREC_PldIratpeldanyok UPDATE
	if @KovetkezoFelelos is not null
	BEGIN
		-- Fizikai mappa ellenorzés
		IF @CheckMappa = 1
		BEGIN
			SELECT Id INTO #iratPeldanyokMappaban
				FROM
				(
					SELECT #pldIds.Id
						FROM #pldIds
							INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = #pldIds.Id
							INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
						WHERE KRT_Mappak.Tipus = '01'
							AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
					UNION
					SELECT #pldIds.Id
						FROM #pldIds
							INNER JOIN EREC_PldIratPeldanyok ON EREC_PldIratPeldanyok.Id = #pldIds.Id
							INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
							INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id
							INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = EREC_UgyUgyiratok.Id
							INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
						WHERE KRT_Mappak.Tipus = '01'
							AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
							--AND EREC_PldIratPeldanyok.Sorszam = '1'
							AND EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo
							AND EREC_PldIratPeldanyok.Csoport_Id_Felelos = EREC_UgyUgyiratok.Csoport_Id_Felelos
				) AS PldInDosszie
						
			IF EXISTS (SELECT Id FROM #iratPeldanyokMappaban)
			BEGIN
				SELECT Id FROM #iratPeldanyokMappaban
				
				DROP TABLE #pldIds;
				DROP TABLE #iratPeldanyokMappaban;
				
				RAISERROR('[64026]',16,1);
			END
			
			DROP TABLE #iratPeldanyokMappaban;
		END
		
		UPDATE EREC_PldIratpeldanyok 
			set Csoport_Id_Felelos_Elozo = Csoport_Id_Felelos,
				Csoport_Id_Felelos = @KovetkezoFelelos,
				TovabbitasAlattAllapot = 
					case Allapot
						when '50' then TovabbitasAlattAllapot
						else Allapot
					end,
				Allapot = '50',
				Ver = Ver + 1,
				Modosito_Id = @ExecutorUserId,
				ModositasIdo = @execTime
			where Id in (select Id FROM #pldIds)
			
		-- iratpéldány mozgásának jelzése
		exec sp_EREC_PldIratPeldanyok_Moved @ExecutorUserId = @ExecutorUserId, @ExecutionTime = @exectime
		
		DROP TABLE #pldIds;

		IF @CheckMappa = 1
		BEGIN
			-- Kézbesítési tételek létrehozása
			insert into EREC_IraKezbesitesiTetelek(
					Obj_Id,
					Obj_type,
					AtadasDat,
					Felhasznalo_Id_Atado_LOGIN,
					Felhasznalo_Id_Atado_USER,
					Csoport_Id_Cel,
					Allapot,
					Letrehozo_Id)
				output inserted.id into @insertedRowIds
				select
					Id,
					'EREC_PldIratPeldanyok',
					@execTime,
					@ExecutorUserId,
					@ExecutorUserId,
					@KovetkezoFelelos,
					'1',
					@executorUserId
				from @tempTable;

		/*** EREC_IraKezbesitesiTetelek history log ***/
--		INSERT INTO EREC_IraKezbesitesiTetelekHistory 
--			SELECT	NEWID(),
--					'0',
--					@ExecutorUserId,
--					@execTime,
--					*
--					FROM EREC_IraKezbesitesiTetelek
--					WHERE Obj_id IN (select Id from @tempTable) AND AtadasDat = @execTime

			-- Az oszlopnevek sorrendje nem garantált, nem szabad *-gal naplózni!
			/*** EREC_IraKezbesitesiTetelek log ***/
			set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @insertedRowIds FOR XML PATH('')),1, 2, '')
			if @row_ids is not null
				set @row_ids = @row_ids + '''';

			exec [sp_LogRecordsToHistory_Tomeges] 
					 @TableName = 'EREC_IraKezbesitesiTetelek'
					,@Row_Ids = @row_ids
					,@Muvelet = 0
					,@Vegrehajto_Id = @ExecutorUserId
					,@VegrehajtasIdo = @execTime
		
--		-- Kezelési feljegyzés insert - irat helyett a példányhoz!
--		if (@KezFeljegyzesTipus is not null and @KezFeljegyzesTipus <> '') or (@Leiras is not null and @Leiras <> '')
--		BEGIN
----			insert into EREC_IraKezFeljegyzesek(IraIrat_Id,KezelesTipus,Leiras,Letrehozo_Id,LetrehozasIdo)
----				select t.IraIrat_Id,@KezFeljegyzesTipus,@Leiras,@executorUserId,@execTime FROM
----				(
----					SELECT DISTINCT IraIrat_Id FROM EREC_PldIratPeldanyok WHERE Id IN (SELECT Id FROM @tempTable)
----				) AS t
--
--			delete from @insertedRowids
--			--declare @insertedRowIds table (Id uniqueidentifier)
--			insert into EREC_IraKezFeljegyzesek(IraIrat_Id,KezelesTipus,Leiras,Letrehozo_Id,LetrehozasIdo)
--				output inserted.id into @insertedRowIds
--				select Id,@KezFeljegyzesTipus,@Leiras,@executorUserId,@execTime FROM @tempTable
--
--			/*** EREC_IraKezFeljegyzesekHistory log ***/
--			set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @insertedRowIds FOR XML PATH('')),1, 2, '')
--			if @row_ids is not null
--				set @row_ids = @row_ids + '''';
--
--			exec [sp_LogRecordsToHistory_Tomeges] 
--					 @TableName = 'EREC_IraKezFeljegyzesek'
--					,@Row_Ids = @row_ids
--					,@Muvelet = 0
--					,@Vegrehajto_Id = @ExecutorUserId
--					,@VegrehajtasIdo = @execTime
--		END
		END -- IF @CheckMappa = 1

	END





	/****** Átvétel *****/
	-- EREC_PldIratpeldanyok UPDATE
	if @KovetkezoOrzo is not null
	BEGIN
		-- Fizikai mappa ellenorzés
		IF @CheckMappa = 1
		BEGIN
			SELECT #pldIds.Id INTO #iratPeldanyokMappa
				FROM #pldIds
					INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = #pldIds.Id
					INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
				WHERE KRT_Mappak.Tipus = '01'
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
			IF EXISTS (SELECT Id FROM #iratPeldanyokMappa)
			BEGIN
				SELECT Id FROM #iratPeldanyokMappa
				
				DROP TABLE #pldIds;
				DROP TABLE #iratPeldanyokMappa;
				
				RAISERROR('[64026]',16,1);
			END
			
			DROP TABLE #iratPeldanyokMappa;
		END

				DECLARE @ORG_ID uniqueidentifier
		SET @ORG_ID = '450B510A-7CAA-46B0-83E3-18445C0C53A9'
		DECLARE @ORG_KOD nvarchar(100) 
		SET @ORG_KOD = (select kod from KRT_Orgok where id=@ORG_ID) 
 
		DECLARE @IS_TUK char(1) 
		SET	@IS_TUK = (select TOP 1 ertek from KRT_Parameterek	where nev='TUK')

IF((@ORG_KOD ='NMHH') and (@IS_TUK ='1'))
	BEGIN 

		declare @irattariId uniqueidentifier;
		declare @irattariHely nvarchar(100);

		select top 1 @irattariId=ih.Id, @irattariHely=ih.Ertek from 
		EREC_IrattariHelyek ih 
		inner join  KRT_PartnerKapcsolatok pk on pk.Partner_id_kapcsolt = ih.Felelos_Csoport_Id		
		inner join KRT_PartnerKapcsolatok pkf on pkf.Partner_id_kapcsolt=pk.Partner_id
		inner join KRT_CsoportTagok cst on cst.Csoport_Id=pkf.Partner_id
		inner join KRT_Csoportok cs on cs.Id=cst.Csoport_Id_Jogalany
		where pk.Tipus = '5'		and  
		((cst.Csoport_Id_Jogalany=@KovetkezoOrzo and cs.Tipus='1') or (pkf.Partner_id=@KovetkezoOrzo and cs.Tipus!='1'))
		and ih.ErvKezd<=getdate() and ih.ErvVege>=getdate() and pk.ErvKezd<=getdate() and pk.ErvVege>=getdate()
		and  cst.ErvKezd<=getdate() and cst.ErvVege>=getdate()
		and ih.Id is not null and ih.Ertek is not null

		UPDATE EREC_PldIratpeldanyok 
			set Csoport_Id_Felelos = @KovetkezoOrzo,
				FelhasznaloCsoport_Id_Orzo = @KovetkezoOrzo,
				Allapot =
					CASE 
						WHEN TovabbitasAlattAllapot = '' THEN Allapot
						WHEN TovabbitasAlattAllapot IS NULL THEN Allapot
						ELSE TovabbitasAlattAllapot
					END,
				TovabbitasAlattAllapot = NULL,
				Ver = Ver + 1,
				Modosito_Id = @ExecutorUserId,
				ModositasIdo = @execTime,
				IrattarId=@irattariId,
				IrattariHely=@irattariHely
			where Id in (select Id FROM #pldIds)
	END
ELSE
	BEGIN
	  UPDATE EREC_PldIratpeldanyok 
			set Csoport_Id_Felelos = @KovetkezoOrzo,
				FelhasznaloCsoport_Id_Orzo = @KovetkezoOrzo,
				Allapot =
					CASE 
						WHEN TovabbitasAlattAllapot = '' THEN Allapot
						WHEN TovabbitasAlattAllapot IS NULL THEN Allapot
						ELSE TovabbitasAlattAllapot
					END,
				TovabbitasAlattAllapot = NULL,
				Ver = Ver + 1,
				Modosito_Id = @ExecutorUserId,
				ModositasIdo = @execTime				
			where Id in (select Id FROM #pldIds)
	END
		
		-- iratpéldány mozgásának jelzése
		exec sp_EREC_PldIratPeldanyok_Moved @ExecutorUserId = @ExecutorUserId, @ExecutionTime = @exectime
					
			
		DROP TABLE #pldIds;

		-- Megkereséses iratpéldányoknál jogot kell adni ügyirathoz
		INSERT INTO KRT_Jogosultak(Csoport_Id_Jogalany,Jogszint,Obj_Id,Orokolheto,Kezi,Tipus)
			SELECT @KovetkezoOrzo,'I',EREC_UgyUgyiratok.Id,'0','I','M'
				FROM EREC_PldIratPeldanyok
					INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
					INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
					INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				WHERE EREC_PldIratPeldanyok.Id IN (select id from @tempTable)
					AND EREC_PldIratPeldanyok.Visszavarolag = '7'
					
		-- Megkereséses iratpéldányoknál bon jogot kell adni az iratpéldány címzettjének
		-- az irathoz, feltéve, hogy még nincs
		INSERT INTO KRT_Jogosultak(Csoport_Id_Jogalany,Jogszint,Obj_Id,Orokolheto,Kezi,Tipus)
			SELECT KRT_Felhasznalok.Id,'I',EREC_IraIratok.Id,'0','I','B'
				FROM EREC_PldIratPeldanyok
					INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
					INNER JOIN KRT_Felhasznalok ON KRT_Felhasznalok.Partner_id = ISNULL(EREC_PldIratPeldanyok.Partner_Id_Cimzett,'00000000-0000-0000-0000-000000000000')
				WHERE EREC_PldIratPeldanyok.Id IN (select id from @tempTable)
					AND EREC_PldIratPeldanyok.Visszavarolag = '7'
					AND NOT EXISTS
					(
						SELECT 1 FROM KRT_Jogosultak
							WHERE KRT_Jogosultak.Obj_Id = EREC_IraIratok.Id
								AND KRT_Jogosultak.Csoport_Id_Jogalany = KRT_Felhasznalok.Id
								AND KRT_Jogosultak.Tipus = 'B'
					)


	END


--	begin   /* History Log */
--		/*** EREC_PldIratpeldanyok history log ***/
--		declare kuldemenyHistoryCursor cursor for 
--			select Id from @tempTable;
--		declare @kuldemenyId uniqueidentifier;
--
--		open kuldemenyHistoryCursor;
--		fetch next from kuldemenyHistoryCursor into @kuldemenyId;
--		while @@FETCH_STATUS = 0
--		BEGIN
--			exec sp_LogRecordToHistory 'EREC_PldIratpeldanyok',@kuldemenyId
--						,'EREC_PldIratpeldanyokHistory',1,@ExecutorUserId,@execTime;
--			fetch next from kuldemenyHistoryCursor into @kuldemenyId;
--		END
--
--		CLOSE kuldemenyHistoryCursor;
--		DEALLOCATE kuldemenyHistoryCursor;
--	end

		/*** EREC_PldIratpeldanyokHistory log ***/
		-- BUG_11751
		-- sp_EREC_PldIratPeldanyok_Moved-ban már logol
		--set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @tempTable FOR XML PATH('')),1, 2, '')
		--if @row_ids is not null
		--	set @row_ids = @row_ids + '''';

		--exec [sp_LogRecordsToHistory_Tomeges] 
		--		 @TableName = 'EREC_PldIratpeldanyok'
		--		,@Row_Ids = @row_ids
		--		,@Muvelet = 1
		--		,@Vegrehajto_Id = @ExecutorUserId
		--		,@VegrehajtasIdo = @execTime
	
	END



END TRY
BEGIN CATCH

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH