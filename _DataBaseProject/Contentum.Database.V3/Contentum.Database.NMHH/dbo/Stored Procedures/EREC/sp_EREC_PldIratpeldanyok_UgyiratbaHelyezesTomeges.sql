﻿create procedure [dbo].[sp_EREC_PldIratpeldanyok_UgyiratbaHelyezesTomeges]
        @Ids						NVARCHAR(MAX)    ,
        @Vers						NVARCHAR(MAX)	 ,
		@FelhasznaloId				UNIQUEIDENTIFIER,		
		@FelhasznaloSzervezete UNIQUEIDENTIFIER = null,
		@LoginUserId UNIQUEIDENTIFIER,        
        @Tranz_id     uniqueidentifier  = null

as

BEGIN TRY

	DECLARE @ExecutionTime DATETIME	
	set	@ExecutionTime = GETDATE()
	
	declare @tempTable table(id uniqueidentifier, ver int);
	
	declare @it	int;
	declare @verPosBegin int;
	declare @verPosEnd int;
	declare @curId	nvarchar(36);
	declare @curVer	nvarchar(6);

	set @it = 0;
	set @verPosBegin = 1;
	while (@it < ((len(@Ids)+1) / 39))
	BEGIN
		set @curId = SUBSTRING(@Ids,@it*39+2,37);
		set @verPosEnd =  CHARINDEX(',',@Vers,@verPosBegin);
		if @verPosEnd = 0 
			set @verPosEnd = len(@Vers) + 1;
		set @curVer = SUBSTRING(@vers, @verPosBegin, @verPosEnd - @verPosBegin);
		set @verPosBegin = @verPosEnd+1;
		insert into @tempTable(id,ver) values(@curId,convert(int,@curVer) );
		set @it = @it + 1;
	END
	

	declare @rowNumberTotal int;
	declare @rowNumberChecked int;

	select @rowNumberTotal = count(id) from @tempTable;
	
	
	/******************************************
	* Ellenorzések
	******************************************/
	-- verzióellenorzés
	select @rowNumberChecked = count([EREC_PldIratPeldanyok].Id)
		from [EREC_PldIratPeldanyok]
			inner JOIN @tempTable t on t.id = [EREC_PldIratPeldanyok].id and t.ver = [EREC_PldIratPeldanyok].ver
			
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT [EREC_PldIratPeldanyok].Id FROM [EREC_PldIratPeldanyok]
			inner join @tempTable t on t.id = [EREC_PldIratPeldanyok].id and t.ver = [EREC_PldIratPeldanyok].ver
		RAISERROR('[50402]',16,1);
	END
	
	-- zárolásellenorzés
	select @rowNumberChecked = count([EREC_PldIratPeldanyok].Id)
		from [EREC_PldIratPeldanyok]
			inner JOIN @tempTable t on t.id = [EREC_PldIratPeldanyok].id and ISNULL([EREC_PldIratPeldanyok].Zarolo_id,@FelhasznaloId) = @FelhasznaloId
			
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT [EREC_PldIratPeldanyok].Id FROM [EREC_PldIratPeldanyok]
			inner join @tempTable t on t.id = [EREC_PldIratPeldanyok].id and ISNULL([EREC_PldIratPeldanyok].Zarolo_id,@FelhasznaloId) = @FelhasznaloId
		RAISERROR('[50499]',16,1);
	END
	
	-- érvényesség vége figyelése
	select @rowNumberChecked = count([EREC_PldIratPeldanyok].Id)
		from [EREC_PldIratPeldanyok]
			inner JOIN @tempTable t on t.id = [EREC_PldIratPeldanyok].id and [EREC_PldIratPeldanyok].ErvVege > @ExecutionTime
			
	if (@rowNumberTotal != @rowNumberChecked)
	BEGIN
		SELECT Id FROM @tempTable
		EXCEPT
		SELECT [EREC_PldIratPeldanyok].Id FROM [EREC_PldIratPeldanyok]
			inner join @tempTable t on t.id = [EREC_PldIratPeldanyok].id and [EREC_PldIratPeldanyok].ErvVege > @ExecutionTime
		RAISERROR('[50403]',16,1);
	END
	
	CREATE TABLE #pldIds(Id UNIQUEIDENTIFIER)
	
	INSERT INTO [#pldIds] SELECT Id FROM @tempTable
	

    UPDATE [EREC_PldIratPeldanyok]
	SET [EREC_PldIratPeldanyok].[FelhasznaloCsoport_Id_Orzo] = [EREC_UgyUgyiratok].[FelhasznaloCsoport_Id_Orzo]
		,[EREC_PldIratPeldanyok].[Csoport_Id_Felelos] = [EREC_UgyUgyiratok].[Csoport_Id_Felelos]
		,Modosito_id = @FelhasznaloId
		,ModositasIdo = @ExecutionTime
		,Ver = [EREC_PldIratPeldanyok].Ver + 1
	FROM [EREC_UgyUgyiratok] INNER JOIN [EREC_IraIratok]
	ON [EREC_UgyUgyiratok].[Id] = [EREC_IraIratok].[Ugyirat_Id]
	WHERE [EREC_PldIratPeldanyok].[Id] IN (SELECT Id FROM #pldIds)
	AND [EREC_IraIratok].[Id] = [EREC_PldIratPeldanyok].[IraIrat_Id]
		 		 	   

if @@rowcount != @rowNumberTotal
begin
	RAISERROR('[50401]',16,1)
END

-- iratpéldány mozgásának jelzése
exec sp_EREC_PldIratPeldanyok_Moved @ExecutorUserId = @FelhasznaloId, @ExecutionTime = @ExecutionTime
    
DROP TABLE [#pldIds]

DECLARE @objTipId_Ugyirat UNIQUEIDENTIFIER
select @objTipId_Ugyirat = KRT_ObjTipusok.Id from KRT_ObjTipusok where KRT_ObjTipusok.Kod = 'EREC_PldIratPeldanyok'

DECLARE @funkcioId UNIQUEIDENTIFIER
SELECT @funkcioId = KRT_Funkciok.Id from KRT_Funkciok where KRT_Funkciok.Kod = 'IratpeldanyUgyiratbaHelyezes'

DECLARE @LetrehozasIdo DATETIME
	SET @LetrehozasIdo = GETDATE()
	
DECLARE @ResultUid UNIQUEIDENTIFIER

/* History Log + Eseménynaplóba rekordok beszúrása */

/* dinamikus history log összeállításhoz */
declare @row_ids varchar(MAX)

/*** EREC_PldIratPeldanyokHistory log ***/
set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @tempTable FOR XML PATH('')),1, 2, '')
if @row_ids is not null
	set @row_ids = @row_ids + '''';

exec [sp_LogRecordsToHistory_Tomeges] 
		 @TableName = 'EREC_PldIratPeldanyok'
		,@Row_Ids = @row_ids
		,@Muvelet = 1
		,@Vegrehajto_Id = @FelhasznaloId
		,@VegrehajtasIdo = @ExecutionTime

DECLARE cur CURSOR FOR
	SELECT Id FROM @tempTable

OPEN cur;
FETCH NEXT FROM cur INTO @curId;
WHILE @@FETCH_STATUS = 0
BEGIN
PRINT @curId
--	exec sp_LogRecordToHistory 'EREC_PldIratPeldanyok',@curId
--					,'EREC_PldIratPeldanyokHistory',1,@FelhasznaloId,@ExecutionTime
	
	DECLARE @iratPldAzonosito NVARCHAR(400)
	SET @iratPldAzonosito = dbo.[fn_GetEREC_PldIratPeldanyokAzonosito](@curId)
	
	EXEC [sp_KRT_EsemenyekInsert]
		@Obj_Id = @curId,
		@ObjTip_Id = @objTipId_Ugyirat,
		@Azonositoja = @iratPldAzonosito,
		@Felhasznalo_Id_User = @FelhasznaloId,
		@Csoport_Id_FelelosUserSzerveze = @FelhasznaloSzervezete, 
		@Felhasznalo_Id_Login = @LoginUserId,	
		@Tranzakcio_Id = @Tranz_id, 
		@Funkcio_Id = @funkcioId,
		@Letrehozo_id =  @FelhasznaloId,
		@LetrehozasIdo = @LetrehozasIdo,
		@ResultUid = @ResultUid OUTPUT
	
	FETCH NEXT FROM cur INTO @curId;
END
CLOSE cur;
DEALLOCATE cur;





END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
END CATCH