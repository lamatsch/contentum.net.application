﻿create procedure [dbo].[sp_EREC_PldKapjakMegGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_PldKapjakMeg.Id,
	   EREC_PldKapjakMeg.PldIratPeldany_Id,
	   EREC_PldKapjakMeg.Csoport_Id,
	   EREC_PldKapjakMeg.Ver,
	   EREC_PldKapjakMeg.Note,
	   EREC_PldKapjakMeg.Stat_id,
	   EREC_PldKapjakMeg.ErvKezd,
	   EREC_PldKapjakMeg.ErvVege,
	   EREC_PldKapjakMeg.Letrehozo_id,
	   EREC_PldKapjakMeg.LetrehozasIdo,
	   EREC_PldKapjakMeg.Modosito_id,
	   EREC_PldKapjakMeg.ModositasIdo,
	   EREC_PldKapjakMeg.Zarolo_id,
	   EREC_PldKapjakMeg.ZarolasIdo,
	   EREC_PldKapjakMeg.Tranz_id,
	   EREC_PldKapjakMeg.UIAccessLog_id
	   from 
		 EREC_PldKapjakMeg as EREC_PldKapjakMeg 
	   where
		 EREC_PldKapjakMeg.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end