﻿create procedure [dbo].[sp_EREC_PldKapjakMegGetAllWithExtension]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_PldKapjakMeg.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_PldKapjakMeg.Id,
	   EREC_PldKapjakMeg.PldIratPeldany_Id,
	   EREC_PldKapjakMeg.Csoport_Id,
	   KRT_Csoportok.Nev as CsoportNev,
	   EREC_PldKapjakMeg.Ver,
	   EREC_PldKapjakMeg.Note,
	   EREC_PldKapjakMeg.Stat_id,
	   EREC_PldKapjakMeg.ErvKezd,
	   EREC_PldKapjakMeg.ErvVege,
	   EREC_PldKapjakMeg.Letrehozo_id,
	   EREC_PldKapjakMeg.LetrehozasIdo,
	   EREC_PldKapjakMeg.Modosito_id,
	   EREC_PldKapjakMeg.ModositasIdo,
	   EREC_PldKapjakMeg.Zarolo_id,
	   EREC_PldKapjakMeg.ZarolasIdo,
	   EREC_PldKapjakMeg.Tranz_id,
	   EREC_PldKapjakMeg.UIAccessLog_id  
   from 
     EREC_PldKapjakMeg as EREC_PldKapjakMeg
	left join KRT_Csoportok as KRT_Csoportok on EREC_PldKapjakMeg.Csoport_Id = KRT_Csoportok.Id
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end