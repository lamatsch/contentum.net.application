-- =============================================
-- Author:		Arató Csaba
-- Create date: 2019.10.09
-- Description:	Tárolt eljárás sakkóra teszteléséhez
-- =============================================
CREATE PROCEDURE [dbo].[sp_SakkoraTaroltEljaras]

	-- Add the parameters for the stored procedure here
	@Vonalkod VARCHAR(100),
	@NapKulonbseg int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here

	IF EXISTS (select * from [dbo].[EREC_UgyUgyiratok] where BARCODE = @Vonalkod )
	BEGIN

		DECLARE @UgyiratId UNIQUEIDENTIFIER;

		SET @UgyiratId = (
		Select Id From EREC_UgyUgyiratok
		WHERE BARCODE = @Vonalkod )

		UPDATE dbo.[EREC_UgyUgyiratok]
		SET UgyintezesKezdete = DATEADD(day, @NapKulonbseg, UgyintezesKezdete),
			SkontrobaDat = DATEADD(day, @NapKulonbseg, SkontrobaDat),
			IrattarbaVetelDat = DATEADD(day, @NapKulonbseg, IrattarbaVetelDat),
			SztornirozasDat = DATEADD(day, @NapKulonbseg, SztornirozasDat),
			ErvKezd = DATEADD(day, @NapKulonbseg, ErvKezd),
			LetrehozasIdo = DATEADD(day, @NapKulonbseg, LetrehozasIdo)
		WHERE Id = @UgyIratId

		UPDATE dbo.[EREC_UgyUgyiratdarabok]
			SET ErvKezd = DATEADD(day, @NapKulonbseg, ErvKezd ),
			LetrehozasIdo = DATEADD(day, @NapKulonbseg, ErvKezd )
			WHERE UgyUgyirat_Id = @UgyIratId


		UPDATE dbo.[EREC_IraIratok]
			SET [IktatasDatuma] = DATEADD(day, @NapKulonbseg, [IktatasDatuma] ),
				ExpedialasDatuma = DATEADD(day, @NapKulonbseg, ExpedialasDatuma ),
				IrattarbaVetelDat = DATEADD(day, @NapKulonbseg, IrattarbaVetelDat ),
				UgyintezesKezdoDatuma = DATEADD(day, @NapKulonbseg, UgyintezesKezdoDatuma ),
				ErvKezd = DATEADD(day, @NapKulonbseg, ErvKezd ),
				LetrehozasIdo = DATEADD(day, @NapKulonbseg, LetrehozasIdo ),
				IntezesIdopontja = DATEADD(day, @NapKulonbseg, IntezesIdopontja ),
				Hatarido = DATEADD(day, @NapKulonbseg, Hatarido )
			WHERE Ugyirat_Id = @UgyIratId

		UPDATE dbo.[EREC_PldIratPeldanyok]
			SET [LetrehozasIdo] = DATEADD(day, @NapKulonbseg, [LetrehozasIdo] ),
				ErvKezd = DATEADD(day, @NapKulonbseg, ErvKezd ),
				PostazasDatuma = DATEADD(day, @NapKulonbseg, PostazasDatuma )
			WHERE IraIrat_Id IN (SELECT ID FROM [EREC_IraIratok] WHERE Ugyirat_Id = @UgyIratId)

		-- Bejövő küldemények
		UPDATE dbo.[EREC_KuldKuldemenyek]
		SET
			[LetrehozasIdo] = DATEADD(day, @NapKulonbseg, [LetrehozasIdo] ),
			[FelbontasDatuma] = DATEADD(day, @NapKulonbseg, [FelbontasDatuma] ),
			[ErvKezd] = DATEADD(day, @NapKulonbseg, [ErvKezd] ),
			[BeerkezesIdeje] = DATEADD(day, @NapKulonbseg, [BeerkezesIdeje]),
			[BelyegzoDatuma] = DATEADD(day, @NapKulonbseg, [BelyegzoDatuma]),
			[ExpedialasIdeje] = DATEADD(day, @NapKulonbseg, [ExpedialasIdeje])
		WHERE Id IN (SELECT KuldKuldemenyek_Id FROM [EREC_IraIratok] WHERE Ugyirat_Id = @UgyIratId)

		-- Kimenő küldemények
		UPDATE dbo.[EREC_KuldKuldemenyek]
		SET
			[LetrehozasIdo] = DATEADD(day, @NapKulonbseg, [LetrehozasIdo] ),
			[FelbontasDatuma] = DATEADD(day, @NapKulonbseg, [FelbontasDatuma] ),
			[ErvKezd] = DATEADD(day, @NapKulonbseg, [ErvKezd] ),
			[BeerkezesIdeje] = DATEADD(day, @NapKulonbseg, [BeerkezesIdeje]),
			[BelyegzoDatuma] = DATEADD(day, @NapKulonbseg, [BelyegzoDatuma]),
			[ExpedialasIdeje] = DATEADD(day, @NapKulonbseg, [ExpedialasIdeje])
		WHERE Id IN (
			SELECT kip.KuldKuldemeny_Id
			FROM [EREC_IraIratok] i
			JOIN [EREC_PldIratPeldanyok] ip on ip.IraIrat_Id = i.Id
			JOIN EREC_Kuldemeny_IratPeldanyai kip ON kip.Peldany_Id = ip.Id
			WHERE Ugyirat_Id = @UgyIratId)

		UPDATE dbo.EREC_Kuldemeny_IratPeldanyai
		SET
			[LetrehozasIdo] = DATEADD(day, @NapKulonbseg, [LetrehozasIdo] ),
			[ErvKezd] = DATEADD(day, @NapKulonbseg, [ErvKezd] )
		WHERE Peldany_Id IN (
			SELECT ip.Id
			FROM [EREC_IraIratok] i
			JOIN [EREC_PldIratPeldanyok] ip on ip.IraIrat_Id = i.Id
			WHERE Ugyirat_Id = @UgyIratId)

	END
	ELSE
	BEGIN
		PRINT 'HIBA: Nincs ilyen vonalkoddal ugyirat!'
	END
END
