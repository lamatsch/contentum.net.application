﻿
create procedure [sp_EREC_SzamlakGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_Szamlak.Id,
	   EREC_Szamlak.KuldKuldemeny_Id,
	   EREC_Szamlak.Dokumentum_Id,
	   EREC_Szamlak.Partner_Id_Szallito,
	   EREC_Szamlak.Cim_Id_Szallito,
	   EREC_Szamlak.NevSTR_Szallito,
	   EREC_Szamlak.CimSTR_Szallito,
	   EREC_Szamlak.Bankszamlaszam_Id,
	   EREC_Szamlak.SzamlaSorszam,
	   EREC_Szamlak.FizetesiMod,
	   EREC_Szamlak.TeljesitesDatuma,
	   EREC_Szamlak.BizonylatDatuma,
	   EREC_Szamlak.FizetesiHatarido,
	   EREC_Szamlak.DevizaKod,
	   EREC_Szamlak.VisszakuldesDatuma,
	   EREC_Szamlak.EllenorzoOsszeg,
	   EREC_Szamlak.PIRAllapot,
	   EREC_Szamlak.VevoBesorolas,   --CR3359
	   EREC_Szamlak.Ver,
	   EREC_Szamlak.Note,
	   EREC_Szamlak.Stat_id,
	   EREC_Szamlak.ErvKezd,
	   EREC_Szamlak.ErvVege,
	   EREC_Szamlak.Letrehozo_id,
	   EREC_Szamlak.LetrehozasIdo,
	   EREC_Szamlak.Modosito_id,
	   EREC_Szamlak.ModositasIdo,
	   EREC_Szamlak.Zarolo_id,
	   EREC_Szamlak.ZarolasIdo,
	   EREC_Szamlak.Tranz_id,
	   EREC_Szamlak.UIAccessLog_id
	   from 
		 EREC_Szamlak as EREC_Szamlak 
	   where
		 EREC_Szamlak.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end