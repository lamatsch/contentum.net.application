﻿
create procedure  [sp_EREC_SzamlakGetFeltoltendoSzamlaKepek]

         
as

begin
BEGIN TRY

	set nocount on

	select
            s.modosito_id, -- ez csak az execparam-hoz kell, hogy kivel hívjuk
            s.szamlasorszam, -- ez a számla azonosítója
			s.vevobesorolas, -- vevő csoportosító mező, interface oldalon felhasználható; CR3359
            -- BLG_3876
			adoszam = case KRT_Partnerek.Tipus
				when '10' then (select KRT_Vallalkozasok.Adoszam)
				when '20' then (select KRT_Szemelyek.Adoszam)
				else null end,
            c.dokumentum_id, -- ezzel meg kell update-elni a számlák táblát, hogy tudjuk mit küldtünk már át
            d.external_link, -- a számlaképo URL-je
            d.fajlnev, -- a számlakép fájl neve
            s.ver, -- számla update-hez kell
            s.id, -- számla update-hez kell
            d.org -- exec paramhoz kell
     from 
            erec_szamlak s inner join 
            erec_csatolmanyok c on c.kuldkuldemeny_id = s.kuldkuldemeny_id inner join
            krt_dokumentumok d on c.dokumentum_id = d.id
            -- BLG_3876
			left join KRT_Partnerek  on KRT_Partnerek.Id= s.Partner_Id_Szallito
			left join KRT_Vallalkozasok on KRT_Vallalkozasok.Partner_Id=KRT_Partnerek.Id
					and getdate() between KRT_Vallalkozasok.ErvKezd and KRT_Vallalkozasok.ErvVege and KRT_Partnerek.Tipus='10'
			left join KRT_Szemelyek on KRT_Szemelyek.Partner_Id=KRT_Partnerek.Id
					and getdate() between KRT_Szemelyek.ErvKezd and KRT_Szemelyek.ErvVege and KRT_Partnerek.Tipus='20'
     where 
            s.PIRAllapot = 'V'
	    and	s.dokumentum_id is null 
            and c.dokumentumszerep = '01' 
          

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end