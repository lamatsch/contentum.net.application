﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_SzamlakHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_EREC_SzamlakHistoryGetRecord
go
*/
create procedure sp_EREC_SzamlakHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_SzamlakHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_SzamlakHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldKuldemeny_Id' as ColumnName,               
               cast(Old.KuldKuldemeny_Id as nvarchar(99)) as OldValue,
               cast(New.KuldKuldemeny_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KuldKuldemeny_Id as nvarchar(max)),'') != ISNULL(CAST(New.KuldKuldemeny_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Dokumentum_Id' as ColumnName,               
               cast(Old.Dokumentum_Id as nvarchar(99)) as OldValue,
               cast(New.Dokumentum_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Dokumentum_Id as nvarchar(max)),'') != ISNULL(CAST(New.Dokumentum_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id_Szallito' as ColumnName,               
               cast(Old.Partner_Id_Szallito as nvarchar(99)) as OldValue,
               cast(New.Partner_Id_Szallito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Partner_Id_Szallito as nvarchar(max)),'') != ISNULL(CAST(New.Partner_Id_Szallito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Cim_Id_Szallito' as ColumnName,               
               cast(Old.Cim_Id_Szallito as nvarchar(99)) as OldValue,
               cast(New.Cim_Id_Szallito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Cim_Id_Szallito as nvarchar(max)),'') != ISNULL(CAST(New.Cim_Id_Szallito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'NevSTR_Szallito' as ColumnName,               
               cast(Old.NevSTR_Szallito as nvarchar(99)) as OldValue,
               cast(New.NevSTR_Szallito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.NevSTR_Szallito as nvarchar(max)),'') != ISNULL(CAST(New.NevSTR_Szallito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CimSTR_Szallito' as ColumnName,               
               cast(Old.CimSTR_Szallito as nvarchar(99)) as OldValue,
               cast(New.CimSTR_Szallito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.CimSTR_Szallito as nvarchar(max)),'') != ISNULL(CAST(New.CimSTR_Szallito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Bankszamlaszam_Id' as ColumnName,               
               cast(Old.Bankszamlaszam_Id as nvarchar(99)) as OldValue,
               cast(New.Bankszamlaszam_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Bankszamlaszam_Id as nvarchar(max)),'') != ISNULL(CAST(New.Bankszamlaszam_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzamlaSorszam' as ColumnName,               
               cast(Old.SzamlaSorszam as nvarchar(99)) as OldValue,
               cast(New.SzamlaSorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SzamlaSorszam as nvarchar(max)),'') != ISNULL(CAST(New.SzamlaSorszam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FizetesiMod' as ColumnName,               
               cast(Old.FizetesiMod as nvarchar(99)) as OldValue,
               cast(New.FizetesiMod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FizetesiMod as nvarchar(max)),'') != ISNULL(CAST(New.FizetesiMod as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TeljesitesDatuma' as ColumnName,               
               cast(Old.TeljesitesDatuma as nvarchar(99)) as OldValue,
               cast(New.TeljesitesDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.TeljesitesDatuma as nvarchar(max)),'') != ISNULL(CAST(New.TeljesitesDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BizonylatDatuma' as ColumnName,               
               cast(Old.BizonylatDatuma as nvarchar(99)) as OldValue,
               cast(New.BizonylatDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.BizonylatDatuma as nvarchar(max)),'') != ISNULL(CAST(New.BizonylatDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FizetesiHatarido' as ColumnName,               
               cast(Old.FizetesiHatarido as nvarchar(99)) as OldValue,
               cast(New.FizetesiHatarido as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FizetesiHatarido as nvarchar(max)),'') != ISNULL(CAST(New.FizetesiHatarido as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DevizaKod' as ColumnName,               
               cast(Old.DevizaKod as nvarchar(99)) as OldValue,
               cast(New.DevizaKod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.DevizaKod as nvarchar(max)),'') != ISNULL(CAST(New.DevizaKod as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'VisszakuldesDatuma' as ColumnName,               
               cast(Old.VisszakuldesDatuma as nvarchar(99)) as OldValue,
               cast(New.VisszakuldesDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.VisszakuldesDatuma as nvarchar(max)),'') != ISNULL(CAST(New.VisszakuldesDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EllenorzoOsszeg' as ColumnName,               
               cast(Old.EllenorzoOsszeg as nvarchar(99)) as OldValue,
               cast(New.EllenorzoOsszeg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EllenorzoOsszeg as nvarchar(max)),'') != ISNULL(CAST(New.EllenorzoOsszeg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PIRAllapot' as ColumnName,               
               cast(Old.PIRAllapot as nvarchar(99)) as OldValue,
               cast(New.PIRAllapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_SzamlakHistory Old
         inner join EREC_SzamlakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_SzamlakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PIRAllapot as nvarchar(max)),'') != ISNULL(CAST(New.PIRAllapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go