﻿
create procedure [sp_EREC_SzamlakInsert]    
                @Id      uniqueidentifier = null,    
                               @KuldKuldemeny_Id     uniqueidentifier  = null,
                @Dokumentum_Id     uniqueidentifier  = null,
	            @Partner_Id_Szallito     uniqueidentifier,
                @Cim_Id_Szallito     uniqueidentifier  = null,
                @NevSTR_Szallito     Nvarchar(400)  = null,
                @CimSTR_Szallito     Nvarchar(400)  = null,
                @Bankszamlaszam_Id     uniqueidentifier  = null,
	            @SzamlaSorszam     varchar(16),
	            @FizetesiMod     varchar(4),
	            @TeljesitesDatuma     smalldatetime,
	            @BizonylatDatuma     smalldatetime,
	            @FizetesiHatarido     smalldatetime,
                @DevizaKod     varchar(3)  = null,
                @VisszakuldesDatuma     smalldatetime  = null,
	            @EllenorzoOsszeg     float,
                @PIRAllapot     char(1)  = null,
				-- CR3359
				@VevoBesorolas nvarchar(20) = null,

                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,


		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @KuldKuldemeny_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',KuldKuldemeny_Id'
            SET @insertValues = @insertValues + ',@KuldKuldemeny_Id'
         end 
       
         if @Dokumentum_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Dokumentum_Id'
            SET @insertValues = @insertValues + ',@Dokumentum_Id'
         end 
       
         if @Partner_Id_Szallito is not null
         begin
            SET @insertColumns = @insertColumns + ',Partner_Id_Szallito'
            SET @insertValues = @insertValues + ',@Partner_Id_Szallito'
         end 
       
         if @Cim_Id_Szallito is not null
         begin
            SET @insertColumns = @insertColumns + ',Cim_Id_Szallito'
            SET @insertValues = @insertValues + ',@Cim_Id_Szallito'
         end 
       
         if @NevSTR_Szallito is not null
         begin
            SET @insertColumns = @insertColumns + ',NevSTR_Szallito'
            SET @insertValues = @insertValues + ',@NevSTR_Szallito'
         end 
       
         if @CimSTR_Szallito is not null
         begin
            SET @insertColumns = @insertColumns + ',CimSTR_Szallito'
            SET @insertValues = @insertValues + ',@CimSTR_Szallito'
         end 
       
         if @Bankszamlaszam_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Bankszamlaszam_Id'
            SET @insertValues = @insertValues + ',@Bankszamlaszam_Id'
         end 
       
         if @SzamlaSorszam is not null
         begin
            SET @insertColumns = @insertColumns + ',SzamlaSorszam'
            SET @insertValues = @insertValues + ',@SzamlaSorszam'
         end 
       
         if @FizetesiMod is not null
         begin
            SET @insertColumns = @insertColumns + ',FizetesiMod'
            SET @insertValues = @insertValues + ',@FizetesiMod'
         end 
       
         if @TeljesitesDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',TeljesitesDatuma'
            SET @insertValues = @insertValues + ',@TeljesitesDatuma'
         end 
       
         if @BizonylatDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',BizonylatDatuma'
            SET @insertValues = @insertValues + ',@BizonylatDatuma'
         end 
       
         if @FizetesiHatarido is not null
         begin
            SET @insertColumns = @insertColumns + ',FizetesiHatarido'
            SET @insertValues = @insertValues + ',@FizetesiHatarido'
         end 
       
         if @DevizaKod is not null
         begin
            SET @insertColumns = @insertColumns + ',DevizaKod'
            SET @insertValues = @insertValues + ',@DevizaKod'
         end 
       
         if @VisszakuldesDatuma is not null
         begin
            SET @insertColumns = @insertColumns + ',VisszakuldesDatuma'
            SET @insertValues = @insertValues + ',@VisszakuldesDatuma'
         end 
       
         if @EllenorzoOsszeg is not null
         begin
            SET @insertColumns = @insertColumns + ',EllenorzoOsszeg'
            SET @insertValues = @insertValues + ',@EllenorzoOsszeg'
         end 
       
         if @PIRAllapot is not null
         begin
            SET @insertColumns = @insertColumns + ',PIRAllapot'
            SET @insertValues = @insertValues + ',@PIRAllapot'
         end 
       -- CR3359
	    if @VevoBesorolas is not null
         begin
            SET @insertColumns = @insertColumns + ',VevoBesorolas'
            SET @insertValues = @insertValues + ',@VevoBesorolas'
         end 

         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_Szamlak ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

-- CR3359
--exec sp_executesql @InsertCommand, 
--                             N'@Id uniqueidentifier,@KuldKuldemeny_Id uniqueidentifier,@Dokumentum_Id uniqueidentifier,@Partner_Id_Szallito uniqueidentifier,@Cim_Id_Szallito uniqueidentifier,@Bankszamlaszam_Id uniqueidentifier,@SzamlaSorszam varchar(16),@FizetesiMod varchar(4),@TeljesitesDatuma smalldatetime,@BizonylatDatuma smalldatetime,@FizetesiHatarido smalldatetime,@DevizaKod varchar(3),@VisszakuldesDatuma smalldatetime,@EllenorzoOsszeg float,@PIRAllapot char(1),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
--,@Id = @Id,@KuldKuldemeny_Id = @KuldKuldemeny_Id,@Dokumentum_Id = @Dokumentum_Id,@Partner_Id_Szallito = @Partner_Id_Szallito,@Cim_Id_Szallito = @Cim_Id_Szallito,@Bankszamlaszam_Id = @Bankszamlaszam_Id,@SzamlaSorszam = @SzamlaSorszam,@FizetesiMod = @FizetesiMod,@TeljesitesDatuma = @TeljesitesDatuma,@BizonylatDatuma = @BizonylatDatuma,@FizetesiHatarido = @FizetesiHatarido,@DevizaKod = @DevizaKod,@VisszakuldesDatuma = @VisszakuldesDatuma,@EllenorzoOsszeg = @EllenorzoOsszeg,@PIRAllapot = @PIRAllapot,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT
exec sp_executesql @InsertCommand, 
 --                            N'@Id uniqueidentifier,@KuldKuldemeny_Id uniqueidentifier,@Dokumentum_Id uniqueidentifier,@Partner_Id_Szallito uniqueidentifier,@Cim_Id_Szallito uniqueidentifier,@Bankszamlaszam_Id uniqueidentifier,@SzamlaSorszam varchar(16),@FizetesiMod varchar(4),@TeljesitesDatuma smalldatetime,@BizonylatDatuma smalldatetime,@FizetesiHatarido smalldatetime,@DevizaKod varchar(3),@VisszakuldesDatuma smalldatetime,@EllenorzoOsszeg float,@PIRAllapot char(1),@VevoBesorolas nvarchar(20),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
--,@Id = @Id,@KuldKuldemeny_Id = @KuldKuldemeny_Id,@Dokumentum_Id = @Dokumentum_Id,@Partner_Id_Szallito = @Partner_Id_Szallito,@Cim_Id_Szallito = @Cim_Id_Szallito,@Bankszamlaszam_Id = @Bankszamlaszam_Id,@SzamlaSorszam = @SzamlaSorszam,@FizetesiMod = @FizetesiMod,@TeljesitesDatuma = @TeljesitesDatuma,@BizonylatDatuma = @BizonylatDatuma,@FizetesiHatarido = @FizetesiHatarido,@DevizaKod = @DevizaKod,@VisszakuldesDatuma = @VisszakuldesDatuma,@EllenorzoOsszeg = @EllenorzoOsszeg,@PIRAllapot = @PIRAllapot, @VevoBesorolas = @VevoBesorolas, @Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT
                             N'@Id uniqueidentifier,@KuldKuldemeny_Id uniqueidentifier,@Dokumentum_Id uniqueidentifier,@Partner_Id_Szallito uniqueidentifier,@Cim_Id_Szallito uniqueidentifier,@NevSTR_Szallito Nvarchar(400),@CimSTR_Szallito Nvarchar(400),@Bankszamlaszam_Id uniqueidentifier,@SzamlaSorszam varchar(16),@FizetesiMod varchar(4),@TeljesitesDatuma smalldatetime,@BizonylatDatuma smalldatetime,@FizetesiHatarido smalldatetime,@DevizaKod varchar(3),@VisszakuldesDatuma smalldatetime,@EllenorzoOsszeg float,@PIRAllapot char(1),@VevoBesorolas nvarchar(20),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@KuldKuldemeny_Id = @KuldKuldemeny_Id,@Dokumentum_Id = @Dokumentum_Id,@Partner_Id_Szallito = @Partner_Id_Szallito,@Cim_Id_Szallito = @Cim_Id_Szallito,@NevSTR_Szallito = @NevSTR_Szallito,@CimSTR_Szallito = @CimSTR_Szallito,@Bankszamlaszam_Id = @Bankszamlaszam_Id,@SzamlaSorszam = @SzamlaSorszam,@FizetesiMod = @FizetesiMod,@TeljesitesDatuma = @TeljesitesDatuma,@BizonylatDatuma = @BizonylatDatuma,@FizetesiHatarido = @FizetesiHatarido,@DevizaKod = @DevizaKod,@VisszakuldesDatuma = @VisszakuldesDatuma,@EllenorzoOsszeg = @EllenorzoOsszeg,@PIRAllapot = @PIRAllapot,@VevoBesorolas = @VevoBesorolas,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT




if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_Szamlak',@ResultUid
					,'EREC_SzamlakHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH