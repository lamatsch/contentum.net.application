﻿
create procedure [sp_EREC_SzamlakUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @KuldKuldemeny_Id     uniqueidentifier  = null,         
             @Dokumentum_Id     uniqueidentifier  = null,         
             @Partner_Id_Szallito     uniqueidentifier  = null,         
             @Cim_Id_Szallito     uniqueidentifier  = null,         
             @NevSTR_Szallito     Nvarchar(400)  = null,         
             @CimSTR_Szallito     Nvarchar(400)  = null,         
             @Bankszamlaszam_Id     uniqueidentifier  = null,         
             @SzamlaSorszam     varchar(16)  = null,         
             @FizetesiMod     varchar(4)  = null,         
             @TeljesitesDatuma     smalldatetime  = null,         
             @BizonylatDatuma     smalldatetime  = null,         
             @FizetesiHatarido     smalldatetime  = null,         
             @DevizaKod     varchar(3)  = null,         
             @VisszakuldesDatuma     smalldatetime  = null,         
             @EllenorzoOsszeg     float  = null,         
             @PIRAllapot     char(1)  = null,      
			 -- CR3359
				@VevoBesorolas nvarchar(20) = null,   
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_KuldKuldemeny_Id     uniqueidentifier         
     DECLARE @Act_Dokumentum_Id     uniqueidentifier         
     DECLARE @Act_Partner_Id_Szallito     uniqueidentifier         
     DECLARE @Act_Cim_Id_Szallito     uniqueidentifier         
     DECLARE @Act_NevSTR_Szallito     Nvarchar(400)         
     DECLARE @Act_CimSTR_Szallito     Nvarchar(400)         
     DECLARE @Act_Bankszamlaszam_Id     uniqueidentifier         
     DECLARE @Act_SzamlaSorszam     varchar(16)         
     DECLARE @Act_FizetesiMod     varchar(4)         
     DECLARE @Act_TeljesitesDatuma     smalldatetime         
     DECLARE @Act_BizonylatDatuma     smalldatetime         
     DECLARE @Act_FizetesiHatarido     smalldatetime         
     DECLARE @Act_DevizaKod     varchar(3)         
     DECLARE @Act_VisszakuldesDatuma     smalldatetime         
     DECLARE @Act_EllenorzoOsszeg     float         
     DECLARE @Act_PIRAllapot     char(1)        
	 -- CR3359
	 DECLARE @Act_VevoBesorolas nvarchar(20)  
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_KuldKuldemeny_Id = KuldKuldemeny_Id,
     @Act_Dokumentum_Id = Dokumentum_Id,
     @Act_Partner_Id_Szallito = Partner_Id_Szallito,
     @Act_Cim_Id_Szallito = Cim_Id_Szallito,
     @Act_NevSTR_Szallito = NevSTR_Szallito,
     @Act_CimSTR_Szallito = CimSTR_Szallito,
     @Act_Bankszamlaszam_Id = Bankszamlaszam_Id,
     @Act_SzamlaSorszam = SzamlaSorszam,
     @Act_FizetesiMod = FizetesiMod,
     @Act_TeljesitesDatuma = TeljesitesDatuma,
     @Act_BizonylatDatuma = BizonylatDatuma,
     @Act_FizetesiHatarido = FizetesiHatarido,
     @Act_DevizaKod = DevizaKod,
     @Act_VisszakuldesDatuma = VisszakuldesDatuma,
     @Act_EllenorzoOsszeg = EllenorzoOsszeg,
     @Act_PIRAllapot = PIRAllapot,
	 -- CR3359
	 @Act_VevoBesorolas = VevoBesorolas,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_Szamlak
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zárolás ellenőrzés:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/KuldKuldemeny_Id')=1
         SET @Act_KuldKuldemeny_Id = @KuldKuldemeny_Id
   IF @UpdatedColumns.exist('/root/Dokumentum_Id')=1
         SET @Act_Dokumentum_Id = @Dokumentum_Id
   IF @UpdatedColumns.exist('/root/Partner_Id_Szallito')=1
         SET @Act_Partner_Id_Szallito = @Partner_Id_Szallito
   IF @UpdatedColumns.exist('/root/Cim_Id_Szallito')=1
         SET @Act_Cim_Id_Szallito = @Cim_Id_Szallito
   IF @UpdatedColumns.exist('/root/NevSTR_Szallito')=1
         SET @Act_NevSTR_Szallito = @NevSTR_Szallito
   IF @UpdatedColumns.exist('/root/CimSTR_Szallito')=1
         SET @Act_CimSTR_Szallito = @CimSTR_Szallito
   IF @UpdatedColumns.exist('/root/Bankszamlaszam_Id')=1
         SET @Act_Bankszamlaszam_Id = @Bankszamlaszam_Id
   IF @UpdatedColumns.exist('/root/SzamlaSorszam')=1
         SET @Act_SzamlaSorszam = @SzamlaSorszam
   IF @UpdatedColumns.exist('/root/FizetesiMod')=1
         SET @Act_FizetesiMod = @FizetesiMod
   IF @UpdatedColumns.exist('/root/TeljesitesDatuma')=1
         SET @Act_TeljesitesDatuma = @TeljesitesDatuma
   IF @UpdatedColumns.exist('/root/BizonylatDatuma')=1
         SET @Act_BizonylatDatuma = @BizonylatDatuma
   IF @UpdatedColumns.exist('/root/FizetesiHatarido')=1
         SET @Act_FizetesiHatarido = @FizetesiHatarido
   IF @UpdatedColumns.exist('/root/DevizaKod')=1
         SET @Act_DevizaKod = @DevizaKod
   IF @UpdatedColumns.exist('/root/VisszakuldesDatuma')=1
         SET @Act_VisszakuldesDatuma = @VisszakuldesDatuma
   IF @UpdatedColumns.exist('/root/EllenorzoOsszeg')=1
         SET @Act_EllenorzoOsszeg = @EllenorzoOsszeg
   IF @UpdatedColumns.exist('/root/PIRAllapot')=1
         SET @Act_PIRAllapot = @PIRAllapot
   -- CR3359
    IF @UpdatedColumns.exist('/root/VevoBesorolas')=1
         SET @Act_VevoBesorolas = @VevoBesorolas
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_Szamlak
SET
     KuldKuldemeny_Id = @Act_KuldKuldemeny_Id,
     Dokumentum_Id = @Act_Dokumentum_Id,
     Partner_Id_Szallito = @Act_Partner_Id_Szallito,
     Cim_Id_Szallito = @Act_Cim_Id_Szallito,
     NevSTR_Szallito = @Act_NevSTR_Szallito,
     CimSTR_Szallito = @Act_CimSTR_Szallito,
     Bankszamlaszam_Id = @Act_Bankszamlaszam_Id,
     SzamlaSorszam = @Act_SzamlaSorszam,
     FizetesiMod = @Act_FizetesiMod,
     TeljesitesDatuma = @Act_TeljesitesDatuma,
     BizonylatDatuma = @Act_BizonylatDatuma,
     FizetesiHatarido = @Act_FizetesiHatarido,
     DevizaKod = @Act_DevizaKod,
     VisszakuldesDatuma = @Act_VisszakuldesDatuma,
     EllenorzoOsszeg = @Act_EllenorzoOsszeg,
     PIRAllapot = @Act_PIRAllapot,
	 -- CR3359
	 VevoBesorolas = @Act_VevoBesorolas,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_Szamlak',@Id
					,'EREC_SzamlakHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH