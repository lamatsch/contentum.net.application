﻿create procedure [dbo].[sp_EREC_SzignalasiJegyzekekGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_SzignalasiJegyzekek.Id,
	   EREC_SzignalasiJegyzekek.UgykorTargykor_Id,
	   EREC_SzignalasiJegyzekek.Csoport_Id_Felelos,
	   EREC_SzignalasiJegyzekek.SzignalasTipusa,
	   EREC_SzignalasiJegyzekek.SzignalasiKotelezettseg,
	   EREC_SzignalasiJegyzekek.HataridosFeladatId,
	   EREC_SzignalasiJegyzekek.Ver,
	   EREC_SzignalasiJegyzekek.Note,
	   EREC_SzignalasiJegyzekek.Stat_id,
	   EREC_SzignalasiJegyzekek.ErvKezd,
	   EREC_SzignalasiJegyzekek.ErvVege,
	   EREC_SzignalasiJegyzekek.Letrehozo_id,
	   EREC_SzignalasiJegyzekek.LetrehozasIdo,
	   EREC_SzignalasiJegyzekek.Modosito_id,
	   EREC_SzignalasiJegyzekek.ModositasIdo,
	   EREC_SzignalasiJegyzekek.Zarolo_id,
	   EREC_SzignalasiJegyzekek.ZarolasIdo,
	   EREC_SzignalasiJegyzekek.Tranz_id,
	   EREC_SzignalasiJegyzekek.UIAccessLog_id
	   from 
		 EREC_SzignalasiJegyzekek as EREC_SzignalasiJegyzekek 
	   where
		 EREC_SzignalasiJegyzekek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end


