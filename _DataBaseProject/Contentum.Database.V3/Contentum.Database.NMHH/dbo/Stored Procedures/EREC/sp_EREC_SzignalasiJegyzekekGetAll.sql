﻿create procedure [dbo].[sp_EREC_SzignalasiJegyzekekGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_SzignalasiJegyzekek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_SzignalasiJegyzekek.Id,
	   EREC_SzignalasiJegyzekek.UgykorTargykor_Id,
	   EREC_SzignalasiJegyzekek.Csoport_Id_Felelos,
	   EREC_SzignalasiJegyzekek.SzignalasTipusa,
	   EREC_SzignalasiJegyzekek.SzignalasiKotelezettseg,
	   EREC_SzignalasiJegyzekek.HataridosFeladatId,
	   EREC_SzignalasiJegyzekek.Ver,
	   EREC_SzignalasiJegyzekek.Note,
	   EREC_SzignalasiJegyzekek.Stat_id,
	   EREC_SzignalasiJegyzekek.ErvKezd,
	   EREC_SzignalasiJegyzekek.ErvVege,
	   EREC_SzignalasiJegyzekek.Letrehozo_id,
	   EREC_SzignalasiJegyzekek.LetrehozasIdo,
	   EREC_SzignalasiJegyzekek.Modosito_id,
	   EREC_SzignalasiJegyzekek.ModositasIdo,
	   EREC_SzignalasiJegyzekek.Zarolo_id,
	   EREC_SzignalasiJegyzekek.ZarolasIdo,
	   EREC_SzignalasiJegyzekek.Tranz_id,
	   EREC_SzignalasiJegyzekek.UIAccessLog_id  
   from 
     EREC_SzignalasiJegyzekek as EREC_SzignalasiJegyzekek      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end