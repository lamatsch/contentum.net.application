﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_TargySzavakHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_EREC_TargySzavakHistoryGetRecord
go
*/
create procedure sp_EREC_TargySzavakHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_TargySzavakHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_TargySzavakHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TargySzavakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Org' as ColumnName,               
               cast(Old.Org as nvarchar(99)) as OldValue,
               cast(New.Org as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TargySzavakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Org as nvarchar(max)),'') != ISNULL(CAST(New.Org as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tipus' as ColumnName,               
               cast(Old.Tipus as nvarchar(99)) as OldValue,
               cast(New.Tipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TargySzavakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tipus as nvarchar(max)),'') != ISNULL(CAST(New.Tipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TargySzavak' as ColumnName,               
               cast(Old.TargySzavak as nvarchar(99)) as OldValue,
               cast(New.TargySzavak as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TargySzavakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.TargySzavak as nvarchar(max)),'') != ISNULL(CAST(New.TargySzavak as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlapertelmezettErtek' as ColumnName,               
               cast(Old.AlapertelmezettErtek as nvarchar(99)) as OldValue,
               cast(New.AlapertelmezettErtek as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TargySzavakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AlapertelmezettErtek as nvarchar(max)),'') != ISNULL(CAST(New.AlapertelmezettErtek as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BelsoAzonosito' as ColumnName,               
               cast(Old.BelsoAzonosito as nvarchar(99)) as OldValue,
               cast(New.BelsoAzonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TargySzavakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.BelsoAzonosito as nvarchar(max)),'') != ISNULL(CAST(New.BelsoAzonosito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SPSSzinkronizalt' as ColumnName,               
               cast(Old.SPSSzinkronizalt as nvarchar(99)) as OldValue,
               cast(New.SPSSzinkronizalt as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TargySzavakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SPSSzinkronizalt as nvarchar(max)),'') != ISNULL(CAST(New.SPSSzinkronizalt as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SPS_Field_Id' as ColumnName,               
               cast(Old.SPS_Field_Id as nvarchar(99)) as OldValue,
               cast(New.SPS_Field_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TargySzavakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SPS_Field_Id as nvarchar(max)),'') != ISNULL(CAST(New.SPS_Field_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Tulaj' as ColumnName,               
               cast(Old.Csoport_Id_Tulaj as nvarchar(99)) as OldValue,
               cast(New.Csoport_Id_Tulaj as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TargySzavakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id_Tulaj as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id_Tulaj as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'RegExp' as ColumnName,               
               cast(Old.RegExp as nvarchar(99)) as OldValue,
               cast(New.RegExp as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TargySzavakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.RegExp as nvarchar(max)),'') != ISNULL(CAST(New.RegExp as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ToolTip' as ColumnName,               
               cast(Old.ToolTip as nvarchar(99)) as OldValue,
               cast(New.ToolTip as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TargySzavakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ToolTip as nvarchar(max)),'') != ISNULL(CAST(New.ToolTip as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ControlTypeSource' as ColumnName,               
               cast(Old.ControlTypeSource as nvarchar(99)) as OldValue,
               cast(New.ControlTypeSource as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TargySzavakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ControlTypeSource as nvarchar(max)),'') != ISNULL(CAST(New.ControlTypeSource as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ControlTypeDataSource' as ColumnName,               
               cast(Old.ControlTypeDataSource as nvarchar(99)) as OldValue,
               cast(New.ControlTypeDataSource as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TargySzavakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ControlTypeDataSource as nvarchar(max)),'') != ISNULL(CAST(New.ControlTypeDataSource as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Targyszo_Id_Parent' as ColumnName,               
               cast(Old.Targyszo_Id_Parent as nvarchar(99)) as OldValue,
               cast(New.Targyszo_Id_Parent as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TargySzavakHistory Old
         inner join EREC_TargySzavakHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TargySzavakHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and Old.Targyszo_Id_Parent != New.Targyszo_Id_Parent 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end