
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_TomegesIktatasFolyamatGet')
            and   type = 'P')
   drop procedure sp_EREC_TomegesIktatasFolyamatGet
go
*/
create procedure sp_EREC_TomegesIktatasFolyamatGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_TomegesIktatasFolyamat.Id,
	   EREC_TomegesIktatasFolyamat.Forras,
	   EREC_TomegesIktatasFolyamat.Forras_Azonosito,
	   EREC_TomegesIktatasFolyamat.Prioritas,
	   EREC_TomegesIktatasFolyamat.RQ_Dokumentum_Id,
	   EREC_TomegesIktatasFolyamat.RS_Dokumentum_Id,
	   EREC_TomegesIktatasFolyamat.Allapot,
	   EREC_TomegesIktatasFolyamat.Ver,
	   EREC_TomegesIktatasFolyamat.Note,
	   EREC_TomegesIktatasFolyamat.Stat_id,
	   EREC_TomegesIktatasFolyamat.ErvKezd,
	   EREC_TomegesIktatasFolyamat.ErvVege,
	   EREC_TomegesIktatasFolyamat.Letrehozo_id,
	   EREC_TomegesIktatasFolyamat.LetrehozasIdo,
	   EREC_TomegesIktatasFolyamat.Modosito_id,
	   EREC_TomegesIktatasFolyamat.ModositasIdo,
	   EREC_TomegesIktatasFolyamat.Zarolo_id,
	   EREC_TomegesIktatasFolyamat.ZarolasIdo,
	   EREC_TomegesIktatasFolyamat.Tranz_id,
	   EREC_TomegesIktatasFolyamat.UIAccessLog_id
	   from 
		 EREC_TomegesIktatasFolyamat as EREC_TomegesIktatasFolyamat 
	   where
		 EREC_TomegesIktatasFolyamat.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
