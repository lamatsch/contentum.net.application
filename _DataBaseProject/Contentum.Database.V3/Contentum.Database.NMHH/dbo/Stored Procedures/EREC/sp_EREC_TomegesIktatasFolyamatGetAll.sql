/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_TomegesIktatasFolyamatGetAll')
            and   type = 'P')
   drop procedure sp_EREC_TomegesIktatasFolyamatGetAll
go
*/
create procedure sp_EREC_TomegesIktatasFolyamatGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_TomegesIktatasFolyamat.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_TomegesIktatasFolyamat.Id,
	   EREC_TomegesIktatasFolyamat.Forras,
	   EREC_TomegesIktatasFolyamat.Forras_Azonosito,
	   EREC_TomegesIktatasFolyamat.Prioritas,
	   EREC_TomegesIktatasFolyamat.RQ_Dokumentum_Id,
	   EREC_TomegesIktatasFolyamat.RS_Dokumentum_Id,
	   EREC_TomegesIktatasFolyamat.Allapot,
	   EREC_TomegesIktatasFolyamat.Ver,
	   EREC_TomegesIktatasFolyamat.Note,
	   EREC_TomegesIktatasFolyamat.Stat_id,
	   EREC_TomegesIktatasFolyamat.ErvKezd,
	   EREC_TomegesIktatasFolyamat.ErvVege,
	   EREC_TomegesIktatasFolyamat.Letrehozo_id,
	   EREC_TomegesIktatasFolyamat.LetrehozasIdo,
	   EREC_TomegesIktatasFolyamat.Modosito_id,
	   EREC_TomegesIktatasFolyamat.ModositasIdo,
	   EREC_TomegesIktatasFolyamat.Zarolo_id,
	   EREC_TomegesIktatasFolyamat.ZarolasIdo,
	   EREC_TomegesIktatasFolyamat.Tranz_id,
	   EREC_TomegesIktatasFolyamat.UIAccessLog_id  
   from 
     EREC_TomegesIktatasFolyamat as EREC_TomegesIktatasFolyamat      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go