﻿/*
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_TomegesIktatasFolyamat_Hiba_Storno')
            and   type = 'P')
   drop procedure sp_EREC_TomegesIktatasFolyamat_Hiba_Storno
go
*/
CREATE PROCEDURE [dbo].[sp_EREC_TomegesIktatasFolyamat_Hiba_Storno]
         @FolyamatId	 UNIQUEIDENTIFIER
AS

BEGIN
BEGIN TRY

	SET NOCOUNT ON

	DECLARE @UGYIRAT_ALLAPOT_STORNO NVARCHAR(100)
	DECLARE @IRAT_ALLAPOT_STORNO NVARCHAR(100)
	DECLARE @KULDEMENY_ALLAPOT_STORNO NVARCHAR(100)
	DECLARE @IRATPLD_ALLAPOT_STORNO NVARCHAR(100)

	SET @UGYIRAT_ALLAPOT_STORNO = '90'
	SET @IRAT_ALLAPOT_STORNO = '90'
	SET @KULDEMENY_ALLAPOT_STORNO = '90'
	SET @IRATPLD_ALLAPOT_STORNO = '90'

	DECLARE @UPDATE_DATETIME DATETIME
	SET @UPDATE_DATETIME = GETDATE()

	DECLARE @UPDATE_NOTE NVARCHAR(100)
	SET @UPDATE_NOTE = 'Felbemaradt tomeges iktatasok - Storno'

	IF NOT EXISTS (SELECT 1 FROM EREC_TomegesIktatasTetelek WHERE Folyamat_Id = @FolyamatId)
		RETURN

	PRINT 'EREC_UgyUgyiratok - Storno - Start'
	UPDATE EREC_UgyUgyiratok
	SET Allapot = @UGYIRAT_ALLAPOT_STORNO,
		ModositasIdo = @UPDATE_DATETIME,
		Note = @UPDATE_NOTE
	WHERE Id IN
		(		
			SELECT DISTINCT UGYIRAT_ID
			FROM EREC_IraIratok
			WHERE ID IN (
					SELECT DISTINCT Irat_Id 
					FROM EREC_TomegesIktatasTetelek 
					WHERE Folyamat_Id = @FolyamatId 
						AND Irat_Id IS NOT NULL
				)
		)

	PRINT 'EREC_IraIratok - Storno - Start'
	UPDATE EREC_IraIratok
	SET Allapot = @IRAT_ALLAPOT_STORNO,
		ModositasIdo = @UPDATE_DATETIME,
		Note = @UPDATE_NOTE
	WHERE Id IN
		(
			SELECT DISTINCT Irat_Id 
			FROM EREC_TomegesIktatasTetelek 
			WHERE Folyamat_Id = @FolyamatId 
				AND Irat_Id IS NOT NULL
		)
	
	PRINT 'EREC_PldIratPeldanyok - Storno - Start'
	UPDATE EREC_PldIratPeldanyok
	SET Allapot = @IRATPLD_ALLAPOT_STORNO,
		ModositasIdo = @UPDATE_DATETIME,
		Note = @UPDATE_NOTE
	WHERE IraIrat_Id IN
		(
			SELECT DISTINCT Id 
			FROM EREC_IraIratok 
			WHERE Id IN
				(
					SELECT DISTINCT Irat_Id 
					FROM EREC_TomegesIktatasTetelek 
					WHERE Folyamat_Id = @FolyamatId 
						AND Irat_Id IS NOT NULL
				)
		)

	PRINT 'EREC_KuldKuldemenyek - Storno - Start'
	UPDATE EREC_KuldKuldemenyek
	SET Allapot = @KULDEMENY_ALLAPOT_STORNO,
		ModositasIdo = @UPDATE_DATETIME,
		Note = @UPDATE_NOTE
	WHERE Id IN
		(
			SELECT DISTINCT KuldKuldemenyek_Id 
			FROM EREC_IraIratok 
			WHERE Id IN
				(
					SELECT DISTINCT Irat_Id 
					FROM EREC_TomegesIktatasTetelek 
					WHERE Folyamat_Id = @FolyamatId  
						AND Irat_Id IS NOT NULL
				)
		)

	RETURN 0
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
END
