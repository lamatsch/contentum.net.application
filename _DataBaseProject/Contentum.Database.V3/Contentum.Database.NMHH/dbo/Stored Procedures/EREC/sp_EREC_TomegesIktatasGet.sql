﻿
create procedure [dbo].[sp_EREC_TomegesIktatasGet]
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_TomegesIktatas.Id,
	   EREC_TomegesIktatas.ForrasTipusNev,
	   EREC_TomegesIktatas.FelelosCsoport_Id,
	   EREC_TomegesIktatas.AgazatiJelek_Id,
	   EREC_TomegesIktatas.IraIrattariTetelek_Id,
	   EREC_TomegesIktatas.Ugytipus_Id,
	   EREC_TomegesIktatas.Irattipus_Id,
	   EREC_TomegesIktatas.TargyPrefix,
	   EREC_TomegesIktatas.AlairasKell,
	   EREC_TomegesIktatas.AlairasMod,
	   EREC_TomegesIktatas.FelhasznaloCsoport_Id_Alairo,
	   EREC_TomegesIktatas.FelhaszCsoport_Id_Helyettesito,
	   EREC_TomegesIktatas.AlairasSzabaly_Id,
	   EREC_TomegesIktatas.HatosagiAdatlapKell,
	   EREC_TomegesIktatas.UgyFajtaja,
	   EREC_TomegesIktatas.DontestHozta,
	   EREC_TomegesIktatas.DontesFormaja,
	   EREC_TomegesIktatas.UgyintezesHataridore,
	   EREC_TomegesIktatas.HataridoTullepes,
	   EREC_TomegesIktatas.HatosagiEllenorzes,
	   EREC_TomegesIktatas.MunkaorakSzama,
	   EREC_TomegesIktatas.EljarasiKoltseg,
	   EREC_TomegesIktatas.KozigazgatasiBirsagMerteke,
	   EREC_TomegesIktatas.SommasEljDontes,
	   EREC_TomegesIktatas.NyolcNapBelulNemSommas,
	   EREC_TomegesIktatas.FuggoHatalyuHatarozat,
	   EREC_TomegesIktatas.FuggoHatalyuVegzes,
	   EREC_TomegesIktatas.HatAltalVisszafizOsszeg,
	   EREC_TomegesIktatas.HatTerheloEljKtsg,
	   EREC_TomegesIktatas.FelfuggHatarozat,
	   EREC_TomegesIktatas.Ver,
	   EREC_TomegesIktatas.Note,
	   EREC_TomegesIktatas.Stat_id,
	   EREC_TomegesIktatas.ErvKezd,
	   EREC_TomegesIktatas.ErvVege,
	   EREC_TomegesIktatas.Letrehozo_id,
	   EREC_TomegesIktatas.LetrehozasIdo,
	   EREC_TomegesIktatas.Modosito_id,
	   EREC_TomegesIktatas.ModositasIdo,
	   EREC_TomegesIktatas.Zarolo_id,
	   EREC_TomegesIktatas.ZarolasIdo,
	   EREC_TomegesIktatas.Tranz_id,
	   EREC_TomegesIktatas.AutoExpedialasKell,
	   EREC_TomegesIktatas.AdathordozoTipusa,
	   EREC_TomegesIktatas.UgyintezesAlapja,
	   EREC_TomegesIktatas.ExpedialasModja,
	   EREC_TomegesIktatas.UIAccessLog_id
	   from 
		 EREC_TomegesIktatas as EREC_TomegesIktatas 
	   where
		 EREC_TomegesIktatas.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end