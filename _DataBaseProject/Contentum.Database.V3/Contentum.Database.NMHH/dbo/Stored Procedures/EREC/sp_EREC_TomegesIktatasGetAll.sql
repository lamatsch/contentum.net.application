﻿
create procedure [dbo].[sp_EREC_TomegesIktatasGetAll]
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_TomegesIktatas.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @pageNumber		int = 0,
  @pageSize		int = -1,
  @SelectedRowId	uniqueidentifier = null,
  @ExecutorUserId				uniqueidentifier
as

begin

BEGIN TRY


set nocount on

   DECLARE @sqlcmd nvarchar(MAX)
   SET @sqlcmd = ''
   DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 	DECLARE @firstRow INT
	DECLARE @lastRow INT
   
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = ISNULL((SELECT TOP (1) Ertek FROM KRT_Parameterek WHERE Nev = 'LISTA_ELEMSZAM_MAX'),1000);
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end

/************************************************************
	* Szűrt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   EREC_TomegesIktatas.Id into #result
		from EREC_TomegesIktatas as EREC_TomegesIktatas
 '
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end

	if (@SelectedRowId is not null)
	begin
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id = @SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	end

	set @sqlcmd = @sqlcmd + N' 
	if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
	BEGIN
		select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
		set @firstRow = (@pageNumber - 1) * @pageSize + 1;
		set @lastRow = @pageNumber*@pageSize;
		select @pageNumber = @pageNumber - 1
	END ;'

	/************************************************************
	* Tényleges select											*
	************************************************************/
     
          
 SET @sqlcmd = @sqlcmd + 
  'select 
		#result.RowNumber,
  	   EREC_TomegesIktatas.Id,
	   EREC_TomegesIktatas.ForrasTipusNev,
	   EREC_TomegesIktatas.FelelosCsoport_Id,
	   EREC_TomegesIktatas.AgazatiJelek_Id,
	   EREC_TomegesIktatas.IraIrattariTetelek_Id,
	   EREC_TomegesIktatas.Ugytipus_Id,
	   EREC_TomegesIktatas.Irattipus_Id,
	   EREC_TomegesIktatas.TargyPrefix,
	   EREC_TomegesIktatas.AlairasKell,
	   EREC_TomegesIktatas.AlairasMod,
	   EREC_TomegesIktatas.FelhasznaloCsoport_Id_Alairo,
	   EREC_TomegesIktatas.FelhaszCsoport_Id_Helyettesito,
	   EREC_TomegesIktatas.AlairasSzabaly_Id,
	   EREC_TomegesIktatas.HatosagiAdatlapKell,
	   EREC_TomegesIktatas.UgyFajtaja,
	   EREC_TomegesIktatas.DontestHozta,
	   EREC_TomegesIktatas.DontesFormaja,
	   EREC_TomegesIktatas.UgyintezesHataridore,
	   EREC_TomegesIktatas.HataridoTullepes,
	   EREC_TomegesIktatas.HatosagiEllenorzes,
	   EREC_TomegesIktatas.MunkaorakSzama,
	   EREC_TomegesIktatas.EljarasiKoltseg,
	   EREC_TomegesIktatas.KozigazgatasiBirsagMerteke,
	   EREC_TomegesIktatas.SommasEljDontes,
	   EREC_TomegesIktatas.NyolcNapBelulNemSommas,
	   EREC_TomegesIktatas.FuggoHatalyuHatarozat,
	   EREC_TomegesIktatas.FuggoHatalyuVegzes,
	   EREC_TomegesIktatas.HatAltalVisszafizOsszeg,
	   EREC_TomegesIktatas.HatTerheloEljKtsg,
	   EREC_TomegesIktatas.FelfuggHatarozat,
	   EREC_TomegesIktatas.Ver,
	   EREC_TomegesIktatas.Note,
	   EREC_TomegesIktatas.Stat_id,
	   EREC_TomegesIktatas.ErvKezd,
	   EREC_TomegesIktatas.ErvVege,
	   EREC_TomegesIktatas.Letrehozo_id,
	   EREC_TomegesIktatas.LetrehozasIdo,
	   EREC_TomegesIktatas.Modosito_id,
	   EREC_TomegesIktatas.ModositasIdo,
	   EREC_TomegesIktatas.Zarolo_id,
	   EREC_TomegesIktatas.ZarolasIdo,
	   EREC_TomegesIktatas.Tranz_id,
	   EREC_TomegesIktatas.AutoExpedialasKell,
	   EREC_TomegesIktatas.AdathordozoTipusa,
	   EREC_TomegesIktatas.UgyintezesAlapja,
	   EREC_TomegesIktatas.ExpedialasModja,
	   EREC_TomegesIktatas.UIAccessLog_id
   from 
     EREC_TomegesIktatas as EREC_TomegesIktatas
	 inner join #result on #result.Id = EREC_TomegesIktatas.Id
    where RowNumber between @firstRow and @lastRow
	ORDER BY #result.RowNumber;'
	print @sqlcmd
	-- találatok száma és oldalszám
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';
 	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @SelectedRowId uniqueidentifier',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @SelectedRowId = @SelectedRowId;
  

	
 

	--SET @sqlcmd = @sqlcmd + 
 -- 'select ' + @LocalTopRow + '
 -- 	   EREC_TomegesIktatas.Id,
	--   EREC_TomegesIktatas.ForrasTipusNev,
	--   EREC_TomegesIktatas.FelelosCsoport_Id,
	--   EREC_TomegesIktatas.AgazatiJelek_Id,
	--   EREC_TomegesIktatas.IraIrattariTetelek_Id,
	--   EREC_TomegesIktatas.Ugytipus_Id,
	--   EREC_TomegesIktatas.Irattipus_Id,
	--   EREC_TomegesIktatas.TargyPrefix,
	--   EREC_TomegesIktatas.AlairasKell,
	--   EREC_TomegesIktatas.AlairasMod,
	--   EREC_TomegesIktatas.FelhasznaloCsoport_Id_Alairo,
	--   EREC_TomegesIktatas.FelhaszCsoport_Id_Helyettesito,
	--   EREC_TomegesIktatas.AlairasSzabaly_Id,
	--   EREC_TomegesIktatas.HatosagiAdatlapKell,
	--   EREC_TomegesIktatas.UgyFajtaja,
	--   EREC_TomegesIktatas.DontestHozta,
	--   EREC_TomegesIktatas.DontesFormaja,
	--   EREC_TomegesIktatas.UgyintezesHataridore,
	--   EREC_TomegesIktatas.HataridoTullepes,
	--   EREC_TomegesIktatas.HatosagiEllenorzes,
	--   EREC_TomegesIktatas.MunkaorakSzama,
	--   EREC_TomegesIktatas.EljarasiKoltseg,
	--   EREC_TomegesIktatas.KozigazgatasiBirsagMerteke,
	--   EREC_TomegesIktatas.SommasEljDontes,
	--   EREC_TomegesIktatas.NyolcNapBelulNemSommas,
	--   EREC_TomegesIktatas.FuggoHatalyuHatarozat,
	--   EREC_TomegesIktatas.FuggoHatalyuVegzes,
	--   EREC_TomegesIktatas.HatAltalVisszafizOsszeg,
	--   EREC_TomegesIktatas.HatTerheloEljKtsg,
	--   EREC_TomegesIktatas.FelfuggHatarozat,
	--   EREC_TomegesIktatas.Ver,
	--   EREC_TomegesIktatas.Note,
	--   EREC_TomegesIktatas.Stat_id,
	--   EREC_TomegesIktatas.ErvKezd,
	--   EREC_TomegesIktatas.ErvVege,
	--   EREC_TomegesIktatas.Letrehozo_id,
	--   EREC_TomegesIktatas.LetrehozasIdo,
	--   EREC_TomegesIktatas.Modosito_id,
	--   EREC_TomegesIktatas.ModositasIdo,
	--   EREC_TomegesIktatas.Zarolo_id,
	--   EREC_TomegesIktatas.ZarolasIdo,
	--   EREC_TomegesIktatas.Tranz_id,
	--   EREC_TomegesIktatas.UIAccessLog_id  
 --  from 
 --    EREC_TomegesIktatas as EREC_TomegesIktatas      
 --  '

	--if @Where is not null and @Where!=''
	--begin 
	--	SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	--end
     
	--SET @sqlcmd = @sqlcmd + @OrderBy;
	--exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end