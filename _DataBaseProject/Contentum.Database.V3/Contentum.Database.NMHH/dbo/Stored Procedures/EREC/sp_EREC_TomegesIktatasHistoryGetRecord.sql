﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_TomegesIktatasHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_EREC_TomegesIktatasHistoryGetRecord
go
*/
create procedure sp_EREC_TomegesIktatasHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_TomegesIktatasHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_TomegesIktatasHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ForrasTipusNev' as ColumnName,               
               cast(Old.ForrasTipusNev as nvarchar(99)) as OldValue,
               cast(New.ForrasTipusNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ForrasTipusNev as nvarchar(max)),'') != ISNULL(CAST(New.ForrasTipusNev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelelosCsoport_Id' as ColumnName,               
               cast(Old.FelelosCsoport_Id as nvarchar(99)) as OldValue,
               cast(New.FelelosCsoport_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelelosCsoport_Id as nvarchar(max)),'') != ISNULL(CAST(New.FelelosCsoport_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AgazatiJelek_Id' as ColumnName,               
               cast(Old.AgazatiJelek_Id as nvarchar(99)) as OldValue,
               cast(New.AgazatiJelek_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AgazatiJelek_Id as nvarchar(max)),'') != ISNULL(CAST(New.AgazatiJelek_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIrattariTetelek_Id' as ColumnName,               
               cast(Old.IraIrattariTetelek_Id as nvarchar(99)) as OldValue,
               cast(New.IraIrattariTetelek_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IraIrattariTetelek_Id as nvarchar(max)),'') != ISNULL(CAST(New.IraIrattariTetelek_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ugytipus_Id' as ColumnName,               
               cast(Old.Ugytipus_Id as nvarchar(99)) as OldValue,
               cast(New.Ugytipus_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Ugytipus_Id as nvarchar(max)),'') != ISNULL(CAST(New.Ugytipus_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Irattipus_Id' as ColumnName,               
               cast(Old.Irattipus_Id as nvarchar(99)) as OldValue,
               cast(New.Irattipus_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Irattipus_Id as nvarchar(max)),'') != ISNULL(CAST(New.Irattipus_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TargyPrefix' as ColumnName,               
               cast(Old.TargyPrefix as nvarchar(99)) as OldValue,
               cast(New.TargyPrefix as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.TargyPrefix as nvarchar(max)),'') != ISNULL(CAST(New.TargyPrefix as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasKell' as ColumnName,               
               cast(Old.AlairasKell as nvarchar(99)) as OldValue,
               cast(New.AlairasKell as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AlairasKell as nvarchar(max)),'') != ISNULL(CAST(New.AlairasKell as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasMod' as ColumnName,               
               cast(Old.AlairasMod as nvarchar(99)) as OldValue,
               cast(New.AlairasMod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AlairasMod as nvarchar(max)),'') != ISNULL(CAST(New.AlairasMod as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Alairo' as ColumnName,               
               cast(Old.FelhasznaloCsoport_Id_Alairo as nvarchar(99)) as OldValue,
               cast(New.FelhasznaloCsoport_Id_Alairo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Alairo as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Alairo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhaszCsoport_Id_Helyettesito' as ColumnName,               
               cast(Old.FelhaszCsoport_Id_Helyettesito as nvarchar(99)) as OldValue,
               cast(New.FelhaszCsoport_Id_Helyettesito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhaszCsoport_Id_Helyettesito as nvarchar(max)),'') != ISNULL(CAST(New.FelhaszCsoport_Id_Helyettesito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'AlairasSzabaly_Id' as ColumnName,               
               cast(Old.AlairasSzabaly_Id as nvarchar(99)) as OldValue,
               cast(New.AlairasSzabaly_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.AlairasSzabaly_Id as nvarchar(max)),'') != ISNULL(CAST(New.AlairasSzabaly_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatosagiAdatlapKell' as ColumnName,               
               cast(Old.HatosagiAdatlapKell as nvarchar(99)) as OldValue,
               cast(New.HatosagiAdatlapKell as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HatosagiAdatlapKell as nvarchar(max)),'') != ISNULL(CAST(New.HatosagiAdatlapKell as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyFajtaja' as ColumnName,               
               cast(Old.UgyFajtaja as nvarchar(99)) as OldValue,
               cast(New.UgyFajtaja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyFajtaja as nvarchar(max)),'') != ISNULL(CAST(New.UgyFajtaja as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DontestHozta' as ColumnName,               
               cast(Old.DontestHozta as nvarchar(99)) as OldValue,
               cast(New.DontestHozta as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.DontestHozta as nvarchar(max)),'') != ISNULL(CAST(New.DontestHozta as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DontesFormaja' as ColumnName,               
               cast(Old.DontesFormaja as nvarchar(99)) as OldValue,
               cast(New.DontesFormaja as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.DontesFormaja as nvarchar(max)),'') != ISNULL(CAST(New.DontesFormaja as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesHataridore' as ColumnName,               
               cast(Old.UgyintezesHataridore as nvarchar(99)) as OldValue,
               cast(New.UgyintezesHataridore as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyintezesHataridore as nvarchar(max)),'') != ISNULL(CAST(New.UgyintezesHataridore as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HataridoTullepes' as ColumnName,               
               cast(Old.HataridoTullepes as nvarchar(99)) as OldValue,
               cast(New.HataridoTullepes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HataridoTullepes as nvarchar(max)),'') != ISNULL(CAST(New.HataridoTullepes as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatosagiEllenorzes' as ColumnName,               
               cast(Old.HatosagiEllenorzes as nvarchar(99)) as OldValue,
               cast(New.HatosagiEllenorzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HatosagiEllenorzes as nvarchar(max)),'') != ISNULL(CAST(New.HatosagiEllenorzes as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MunkaorakSzama' as ColumnName,               
               cast(Old.MunkaorakSzama as nvarchar(99)) as OldValue,
               cast(New.MunkaorakSzama as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MunkaorakSzama as nvarchar(max)),'') != ISNULL(CAST(New.MunkaorakSzama as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EljarasiKoltseg' as ColumnName,               
               cast(Old.EljarasiKoltseg as nvarchar(99)) as OldValue,
               cast(New.EljarasiKoltseg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EljarasiKoltseg as nvarchar(max)),'') != ISNULL(CAST(New.EljarasiKoltseg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KozigazgatasiBirsagMerteke' as ColumnName,               
               cast(Old.KozigazgatasiBirsagMerteke as nvarchar(99)) as OldValue,
               cast(New.KozigazgatasiBirsagMerteke as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KozigazgatasiBirsagMerteke as nvarchar(max)),'') != ISNULL(CAST(New.KozigazgatasiBirsagMerteke as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SommasEljDontes' as ColumnName,               
               cast(Old.SommasEljDontes as nvarchar(99)) as OldValue,
               cast(New.SommasEljDontes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SommasEljDontes as nvarchar(max)),'') != ISNULL(CAST(New.SommasEljDontes as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'NyolcNapBelulNemSommas' as ColumnName,               
               cast(Old.NyolcNapBelulNemSommas as nvarchar(99)) as OldValue,
               cast(New.NyolcNapBelulNemSommas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.NyolcNapBelulNemSommas as nvarchar(max)),'') != ISNULL(CAST(New.NyolcNapBelulNemSommas as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FuggoHatalyuHatarozat' as ColumnName,               
               cast(Old.FuggoHatalyuHatarozat as nvarchar(99)) as OldValue,
               cast(New.FuggoHatalyuHatarozat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FuggoHatalyuHatarozat as nvarchar(max)),'') != ISNULL(CAST(New.FuggoHatalyuHatarozat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FuggoHatalyuVegzes' as ColumnName,               
               cast(Old.FuggoHatalyuVegzes as nvarchar(99)) as OldValue,
               cast(New.FuggoHatalyuVegzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FuggoHatalyuVegzes as nvarchar(max)),'') != ISNULL(CAST(New.FuggoHatalyuVegzes as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatAltalVisszafizOsszeg' as ColumnName,               
               cast(Old.HatAltalVisszafizOsszeg as nvarchar(99)) as OldValue,
               cast(New.HatAltalVisszafizOsszeg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HatAltalVisszafizOsszeg as nvarchar(max)),'') != ISNULL(CAST(New.HatAltalVisszafizOsszeg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatTerheloEljKtsg' as ColumnName,               
               cast(Old.HatTerheloEljKtsg as nvarchar(99)) as OldValue,
               cast(New.HatTerheloEljKtsg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HatTerheloEljKtsg as nvarchar(max)),'') != ISNULL(CAST(New.HatTerheloEljKtsg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelfuggHatarozat' as ColumnName,               
               cast(Old.FelfuggHatarozat as nvarchar(99)) as OldValue,
               cast(New.FelfuggHatarozat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasHistory Old
         inner join EREC_TomegesIktatasHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_TomegesIktatasHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelfuggHatarozat as nvarchar(max)),'') != ISNULL(CAST(New.FelfuggHatarozat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go