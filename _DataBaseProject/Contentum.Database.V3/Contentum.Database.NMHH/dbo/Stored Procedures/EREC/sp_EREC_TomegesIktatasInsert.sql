﻿
create procedure [dbo].[sp_EREC_TomegesIktatasInsert]    
                @Id      uniqueidentifier = null,    
               	            @ForrasTipusNev     Nvarchar(4000),
	            @FelelosCsoport_Id     uniqueidentifier,
	            @AgazatiJelek_Id     uniqueidentifier,
	            @IraIrattariTetelek_Id     uniqueidentifier,
	            @Ugytipus_Id     uniqueidentifier,
	            @Irattipus_Id     uniqueidentifier,
	            @TargyPrefix     Nvarchar(4000),
                @AlairasKell     char(1)  = null,
                @AlairasMod     uniqueidentifier  = null,
                @FelhasznaloCsoport_Id_Alairo     uniqueidentifier  = null,
                @FelhaszCsoport_Id_Helyettesito     uniqueidentifier  = null,
                @AlairasSzabaly_Id     uniqueidentifier  = null,
                @HatosagiAdatlapKell     char(1)  = null,
                @UgyFajtaja     nvarchar(64)  = null,
                @DontestHozta     nvarchar(64)  = null,
                @DontesFormaja     nvarchar(64)  = null,
                @UgyintezesHataridore     nvarchar(64)  = null,
                @HataridoTullepes     int  = null,
                @HatosagiEllenorzes     char(1)  = null,
                @MunkaorakSzama     float  = null,
                @EljarasiKoltseg     int  = null,
                @KozigazgatasiBirsagMerteke     int  = null,
                @SommasEljDontes     nvarchar(64)  = null,
                @NyolcNapBelulNemSommas     nvarchar(64)  = null,
                @FuggoHatalyuHatarozat     nvarchar(64)  = null,
                @FuggoHatalyuVegzes     nvarchar(64)  = null,
                @HatAltalVisszafizOsszeg     int  = null,
                @HatTerheloEljKtsg     int  = null,
                @FelfuggHatarozat     nvarchar(64)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,
				@AutoExpedialasKell   char(1) = null,
				@AdathordozoTipusa nvarchar(64) = null,
				@UgyintezesAlapja nvarchar(100) = null,
				@ExpedialasModja nvarchar(64) = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @ForrasTipusNev is not null
         begin
            SET @insertColumns = @insertColumns + ',ForrasTipusNev'
            SET @insertValues = @insertValues + ',@ForrasTipusNev'
         end 
       
         if @FelelosCsoport_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',FelelosCsoport_Id'
            SET @insertValues = @insertValues + ',@FelelosCsoport_Id'
         end 
       
         if @AgazatiJelek_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',AgazatiJelek_Id'
            SET @insertValues = @insertValues + ',@AgazatiJelek_Id'
         end 
       
         if @IraIrattariTetelek_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IraIrattariTetelek_Id'
            SET @insertValues = @insertValues + ',@IraIrattariTetelek_Id'
         end 
       
         if @Ugytipus_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Ugytipus_Id'
            SET @insertValues = @insertValues + ',@Ugytipus_Id'
         end 
       
         if @Irattipus_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Irattipus_Id'
            SET @insertValues = @insertValues + ',@Irattipus_Id'
         end 
       
         if @TargyPrefix is not null
         begin
            SET @insertColumns = @insertColumns + ',TargyPrefix'
            SET @insertValues = @insertValues + ',@TargyPrefix'
         end 
       
         if @AlairasKell is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairasKell'
            SET @insertValues = @insertValues + ',@AlairasKell'
         end 
       
         if @AlairasMod is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairasMod'
            SET @insertValues = @insertValues + ',@AlairasMod'
         end 
       
         if @FelhasznaloCsoport_Id_Alairo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Alairo'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Alairo'
         end 
       
         if @FelhaszCsoport_Id_Helyettesito is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhaszCsoport_Id_Helyettesito'
            SET @insertValues = @insertValues + ',@FelhaszCsoport_Id_Helyettesito'
         end 
       
         if @AlairasSzabaly_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',AlairasSzabaly_Id'
            SET @insertValues = @insertValues + ',@AlairasSzabaly_Id'
         end 
       
         if @HatosagiAdatlapKell is not null
         begin
            SET @insertColumns = @insertColumns + ',HatosagiAdatlapKell'
            SET @insertValues = @insertValues + ',@HatosagiAdatlapKell'
         end 
       
         if @UgyFajtaja is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyFajtaja'
            SET @insertValues = @insertValues + ',@UgyFajtaja'
         end 
       
         if @DontestHozta is not null
         begin
            SET @insertColumns = @insertColumns + ',DontestHozta'
            SET @insertValues = @insertValues + ',@DontestHozta'
         end 
       
         if @DontesFormaja is not null
         begin
            SET @insertColumns = @insertColumns + ',DontesFormaja'
            SET @insertValues = @insertValues + ',@DontesFormaja'
         end 
       
         if @UgyintezesHataridore is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyintezesHataridore'
            SET @insertValues = @insertValues + ',@UgyintezesHataridore'
         end 
       
         if @HataridoTullepes is not null
         begin
            SET @insertColumns = @insertColumns + ',HataridoTullepes'
            SET @insertValues = @insertValues + ',@HataridoTullepes'
         end 
       
         if @HatosagiEllenorzes is not null
         begin
            SET @insertColumns = @insertColumns + ',HatosagiEllenorzes'
            SET @insertValues = @insertValues + ',@HatosagiEllenorzes'
         end 
       
         if @MunkaorakSzama is not null
         begin
            SET @insertColumns = @insertColumns + ',MunkaorakSzama'
            SET @insertValues = @insertValues + ',@MunkaorakSzama'
         end 
       
         if @EljarasiKoltseg is not null
         begin
            SET @insertColumns = @insertColumns + ',EljarasiKoltseg'
            SET @insertValues = @insertValues + ',@EljarasiKoltseg'
         end 
       
         if @KozigazgatasiBirsagMerteke is not null
         begin
            SET @insertColumns = @insertColumns + ',KozigazgatasiBirsagMerteke'
            SET @insertValues = @insertValues + ',@KozigazgatasiBirsagMerteke'
         end 
       
         if @SommasEljDontes is not null
         begin
            SET @insertColumns = @insertColumns + ',SommasEljDontes'
            SET @insertValues = @insertValues + ',@SommasEljDontes'
         end 
       
         if @NyolcNapBelulNemSommas is not null
         begin
            SET @insertColumns = @insertColumns + ',NyolcNapBelulNemSommas'
            SET @insertValues = @insertValues + ',@NyolcNapBelulNemSommas'
         end 
       
         if @FuggoHatalyuHatarozat is not null
         begin
            SET @insertColumns = @insertColumns + ',FuggoHatalyuHatarozat'
            SET @insertValues = @insertValues + ',@FuggoHatalyuHatarozat'
         end 
       
         if @FuggoHatalyuVegzes is not null
         begin
            SET @insertColumns = @insertColumns + ',FuggoHatalyuVegzes'
            SET @insertValues = @insertValues + ',@FuggoHatalyuVegzes'
         end 
       
         if @HatAltalVisszafizOsszeg is not null
         begin
            SET @insertColumns = @insertColumns + ',HatAltalVisszafizOsszeg'
            SET @insertValues = @insertValues + ',@HatAltalVisszafizOsszeg'
         end 
       
         if @HatTerheloEljKtsg is not null
         begin
            SET @insertColumns = @insertColumns + ',HatTerheloEljKtsg'
            SET @insertValues = @insertValues + ',@HatTerheloEljKtsg'
         end 
       
         if @FelfuggHatarozat is not null
         begin
            SET @insertColumns = @insertColumns + ',FelfuggHatarozat'
            SET @insertValues = @insertValues + ',@FelfuggHatarozat'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   

		 if @AutoExpedialasKell is not null
         begin
            SET @insertColumns = @insertColumns + ',AutoExpedialasKell'
            SET @insertValues = @insertValues + ',@AutoExpedialasKell'
         end  

		 if @AdathordozoTipusa is not null
         begin
            SET @insertColumns = @insertColumns + ',AdathordozoTipusa'
            SET @insertValues = @insertValues + ',@AdathordozoTipusa'
         end 

		  if @UgyintezesAlapja is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyintezesAlapja'
            SET @insertValues = @insertValues + ',@UgyintezesAlapja'
         end 

		   if @ExpedialasModja is not null
         begin
            SET @insertColumns = @insertColumns + ',ExpedialasModja'
            SET @insertValues = @insertValues + ',@ExpedialasModja'
         end 

   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_TomegesIktatas ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@ForrasTipusNev Nvarchar(4000),@FelelosCsoport_Id uniqueidentifier,@AgazatiJelek_Id uniqueidentifier,@IraIrattariTetelek_Id uniqueidentifier,@Ugytipus_Id uniqueidentifier,@Irattipus_Id uniqueidentifier,@TargyPrefix Nvarchar(4000),@AlairasKell char(1),@AlairasMod uniqueidentifier,@FelhasznaloCsoport_Id_Alairo uniqueidentifier,@FelhaszCsoport_Id_Helyettesito uniqueidentifier,@AlairasSzabaly_Id uniqueidentifier,@HatosagiAdatlapKell char(1),@UgyFajtaja nvarchar(64),@DontestHozta nvarchar(64),@DontesFormaja nvarchar(64),@UgyintezesHataridore nvarchar(64),@HataridoTullepes int,@HatosagiEllenorzes char(1),@MunkaorakSzama float,@EljarasiKoltseg int,@KozigazgatasiBirsagMerteke int,@SommasEljDontes nvarchar(64),@NyolcNapBelulNemSommas nvarchar(64),@FuggoHatalyuHatarozat nvarchar(64),@FuggoHatalyuVegzes nvarchar(64),@HatAltalVisszafizOsszeg int,@HatTerheloEljKtsg int,@FelfuggHatarozat nvarchar(64),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@AutoExpedialasKell char(1),@AdathordozoTipusa nvarchar(64), @UgyintezesAlapja nvarchar(100), @ExpedialasModja nvarchar(64), @ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@ForrasTipusNev = @ForrasTipusNev,@FelelosCsoport_Id = @FelelosCsoport_Id,@AgazatiJelek_Id = @AgazatiJelek_Id,@IraIrattariTetelek_Id = @IraIrattariTetelek_Id,@Ugytipus_Id = @Ugytipus_Id,@Irattipus_Id = @Irattipus_Id,@TargyPrefix = @TargyPrefix,@AlairasKell = @AlairasKell,@AlairasMod = @AlairasMod,@FelhasznaloCsoport_Id_Alairo = @FelhasznaloCsoport_Id_Alairo,@FelhaszCsoport_Id_Helyettesito = @FelhaszCsoport_Id_Helyettesito,@AlairasSzabaly_Id = @AlairasSzabaly_Id,@HatosagiAdatlapKell = @HatosagiAdatlapKell,@UgyFajtaja = @UgyFajtaja,@DontestHozta = @DontestHozta,@DontesFormaja = @DontesFormaja,@UgyintezesHataridore = @UgyintezesHataridore,@HataridoTullepes = @HataridoTullepes,@HatosagiEllenorzes = @HatosagiEllenorzes,@MunkaorakSzama = @MunkaorakSzama,@EljarasiKoltseg = @EljarasiKoltseg,@KozigazgatasiBirsagMerteke = @KozigazgatasiBirsagMerteke,@SommasEljDontes = @SommasEljDontes,@NyolcNapBelulNemSommas = @NyolcNapBelulNemSommas,@FuggoHatalyuHatarozat = @FuggoHatalyuHatarozat,@FuggoHatalyuVegzes = @FuggoHatalyuVegzes,@HatAltalVisszafizOsszeg = @HatAltalVisszafizOsszeg,@HatTerheloEljKtsg = @HatTerheloEljKtsg,@FelfuggHatarozat = @FelfuggHatarozat,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id , @AutoExpedialasKell = @AutoExpedialasKell, @AdathordozoTipusa = @AdathordozoTipusa,  @UgyintezesAlapja=  @UgyintezesAlapja, @ExpedialasModja = @ExpedialasModja, @ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_TomegesIktatas',@ResultUid
					,'EREC_TomegesIktatasHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH