
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_TomegesIktatasTetelekGet')
            and   type = 'P')
   drop procedure sp_EREC_TomegesIktatasTetelekGet
go
*/
create procedure sp_EREC_TomegesIktatasTetelekGet
			@Id uniqueidentifier,
         @ExecutorUserId uniqueidentifier,
         @FromHistory   char(1) = ''

         
as

begin
BEGIN TRY

	set nocount on

	DECLARE @sqlcmd nvarchar(4000)

	SET @sqlcmd = 
	  'select 
     	   EREC_TomegesIktatasTetelek.Id,
	   EREC_TomegesIktatasTetelek.Folyamat_Id,
	   EREC_TomegesIktatasTetelek.Forras_Azonosito,
	   EREC_TomegesIktatasTetelek.IktatasTipus,
	   EREC_TomegesIktatasTetelek.Alszamra,
	   EREC_TomegesIktatasTetelek.Iktatokonyv_Id,
	   EREC_TomegesIktatasTetelek.Ugyirat_Id,
	   EREC_TomegesIktatasTetelek.Kuldemeny_Id,
	   EREC_TomegesIktatasTetelek.ExecParam,
	   EREC_TomegesIktatasTetelek.EREC_UgyUgyiratok,
	   EREC_TomegesIktatasTetelek.EREC_UgyUgyiratdarabok,
	   EREC_TomegesIktatasTetelek.EREC_IraIratok,
	   EREC_TomegesIktatasTetelek.EREC_PldIratPeldanyok,
	   EREC_TomegesIktatasTetelek.EREC_HataridosFeladatok,
	   EREC_TomegesIktatasTetelek.EREC_KuldKuldemenyek,
	   EREC_TomegesIktatasTetelek.IktatasiParameterek,
	   EREC_TomegesIktatasTetelek.ErkeztetesParameterek,
	   EREC_TomegesIktatasTetelek.Dokumentumok,
	   EREC_TomegesIktatasTetelek.Azonosito,
	   EREC_TomegesIktatasTetelek.Allapot,
	   EREC_TomegesIktatasTetelek.Irat_Id,
	   EREC_TomegesIktatasTetelek.Hiba,
	   EREC_TomegesIktatasTetelek.Expedialas,
	   EREC_TomegesIktatasTetelek.TobbIratEgyKuldemenybe,
	   EREC_TomegesIktatasTetelek.KuldemenyAtadas,
	   EREC_TomegesIktatasTetelek.HatosagiStatAdat,
	   EREC_TomegesIktatasTetelek.EREC_IratAlairok,
	   EREC_TomegesIktatasTetelek.Lezarhato,
	   EREC_TomegesIktatasTetelek.Ver,
	   EREC_TomegesIktatasTetelek.Note,
	   EREC_TomegesIktatasTetelek.Stat_id,
	   EREC_TomegesIktatasTetelek.ErvKezd,
	   EREC_TomegesIktatasTetelek.ErvVege,
	   EREC_TomegesIktatasTetelek.Letrehozo_id,
	   EREC_TomegesIktatasTetelek.LetrehozasIdo,
	   EREC_TomegesIktatasTetelek.Modosito_id,
	   EREC_TomegesIktatasTetelek.ModositasIdo,
	   EREC_TomegesIktatasTetelek.Zarolo_id,
	   EREC_TomegesIktatasTetelek.ZarolasIdo,
	   EREC_TomegesIktatasTetelek.Tranz_id,
	   EREC_TomegesIktatasTetelek.UIAccessLog_id
	   from 
		 EREC_TomegesIktatasTetelek as EREC_TomegesIktatasTetelek 
	   where
		 EREC_TomegesIktatasTetelek.Id = ''' + cast(@Id as nvarchar(40)) + '''';

	exec sp_executesql @sqlcmd;
	 
	if @@rowcount = 0
	  begin
		RAISERROR('[50101]',16,1)
	  end

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
