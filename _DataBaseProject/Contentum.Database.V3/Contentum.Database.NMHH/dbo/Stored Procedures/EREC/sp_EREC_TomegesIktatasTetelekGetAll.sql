/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_TomegesIktatasTetelekGetAll')
            and   type = 'P')
   drop procedure sp_EREC_TomegesIktatasTetelekGetAll
go
*/
create procedure sp_EREC_TomegesIktatasTetelekGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_TomegesIktatasTetelek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_TomegesIktatasTetelek.Id,
	   EREC_TomegesIktatasTetelek.Folyamat_Id,
	   EREC_TomegesIktatasTetelek.Forras_Azonosito,
	   EREC_TomegesIktatasTetelek.IktatasTipus,
	   EREC_TomegesIktatasTetelek.Alszamra,
	   EREC_TomegesIktatasTetelek.Iktatokonyv_Id,
	   EREC_TomegesIktatasTetelek.Ugyirat_Id,
	   EREC_TomegesIktatasTetelek.Kuldemeny_Id,
	   EREC_TomegesIktatasTetelek.ExecParam,
	   EREC_TomegesIktatasTetelek.EREC_UgyUgyiratok,
	   EREC_TomegesIktatasTetelek.EREC_UgyUgyiratdarabok,
	   EREC_TomegesIktatasTetelek.EREC_IraIratok,
	   EREC_TomegesIktatasTetelek.EREC_PldIratPeldanyok,
	   EREC_TomegesIktatasTetelek.EREC_HataridosFeladatok,
	   EREC_TomegesIktatasTetelek.EREC_KuldKuldemenyek,
	   EREC_TomegesIktatasTetelek.IktatasiParameterek,
	   EREC_TomegesIktatasTetelek.ErkeztetesParameterek,
	   EREC_TomegesIktatasTetelek.Dokumentumok,
	   EREC_TomegesIktatasTetelek.Azonosito,
	   EREC_TomegesIktatasTetelek.Allapot,
	   EREC_TomegesIktatasTetelek.Irat_Id,
	   EREC_TomegesIktatasTetelek.Hiba,
	   EREC_TomegesIktatasTetelek.Expedialas,
	   EREC_TomegesIktatasTetelek.TobbIratEgyKuldemenybe,
	   EREC_TomegesIktatasTetelek.KuldemenyAtadas,
	   EREC_TomegesIktatasTetelek.HatosagiStatAdat,
	   EREC_TomegesIktatasTetelek.EREC_IratAlairok,
	   EREC_TomegesIktatasTetelek.Lezarhato,
	   EREC_TomegesIktatasTetelek.Ver,
	   EREC_TomegesIktatasTetelek.Note,
	   EREC_TomegesIktatasTetelek.Stat_id,
	   EREC_TomegesIktatasTetelek.ErvKezd,
	   EREC_TomegesIktatasTetelek.ErvVege,
	   EREC_TomegesIktatasTetelek.Letrehozo_id,
	   EREC_TomegesIktatasTetelek.LetrehozasIdo,
	   EREC_TomegesIktatasTetelek.Modosito_id,
	   EREC_TomegesIktatasTetelek.ModositasIdo,
	   EREC_TomegesIktatasTetelek.Zarolo_id,
	   EREC_TomegesIktatasTetelek.ZarolasIdo,
	   EREC_TomegesIktatasTetelek.Tranz_id,
	   EREC_TomegesIktatasTetelek.UIAccessLog_id  
   from 
     EREC_TomegesIktatasTetelek as EREC_TomegesIktatasTetelek      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go