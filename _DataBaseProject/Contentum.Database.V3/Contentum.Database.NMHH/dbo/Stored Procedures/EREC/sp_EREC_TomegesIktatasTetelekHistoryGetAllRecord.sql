﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_TomegesIktatasTetelekHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_EREC_TomegesIktatasTetelekHistoryGetAllRecord
go
*/
create procedure sp_EREC_TomegesIktatasTetelekHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_TomegesIktatasTetelekHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Folyamat_Id' as ColumnName,               cast(Old.Folyamat_Id as nvarchar(99)) as OldValue,
               cast(New.Folyamat_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Folyamat_Id as nvarchar(max)),'') != ISNULL(CAST(New.Folyamat_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Forras_Azonosito' as ColumnName,               cast(Old.Forras_Azonosito as nvarchar(99)) as OldValue,
               cast(New.Forras_Azonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Forras_Azonosito as nvarchar(max)),'') != ISNULL(CAST(New.Forras_Azonosito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IktatasTipus' as ColumnName,               cast(Old.IktatasTipus as nvarchar(99)) as OldValue,
               cast(New.IktatasTipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IktatasTipus as nvarchar(max)),'') != ISNULL(CAST(New.IktatasTipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Alszamra' as ColumnName,               cast(Old.Alszamra as nvarchar(99)) as OldValue,
               cast(New.Alszamra as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Alszamra as nvarchar(max)),'') != ISNULL(CAST(New.Alszamra as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ugyirat_Id' as ColumnName,               cast(Old.Ugyirat_Id as nvarchar(99)) as OldValue,
               cast(New.Ugyirat_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Ugyirat_Id as nvarchar(max)),'') != ISNULL(CAST(New.Ugyirat_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kuldemeny_Id' as ColumnName,               cast(Old.Kuldemeny_Id as nvarchar(99)) as OldValue,
               cast(New.Kuldemeny_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Kuldemeny_Id as nvarchar(max)),'') != ISNULL(CAST(New.Kuldemeny_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Iktatokonyv_Id' as ColumnName,               cast(Old.Iktatokonyv_Id as nvarchar(99)) as OldValue,
               cast(New.Iktatokonyv_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Iktatokonyv_Id as nvarchar(max)),'') != ISNULL(CAST(New.Iktatokonyv_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ExecParam' as ColumnName,               cast(Old.ExecParam as nvarchar(99)) as OldValue,
               cast(New.ExecParam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ExecParam as nvarchar(max)),'') != ISNULL(CAST(New.ExecParam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EREC_UgyUgyiratok' as ColumnName,               cast(Old.EREC_UgyUgyiratok as nvarchar(99)) as OldValue,
               cast(New.EREC_UgyUgyiratok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EREC_UgyUgyiratok as nvarchar(max)),'') != ISNULL(CAST(New.EREC_UgyUgyiratok as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EREC_UgyUgyiratdarabok' as ColumnName,               cast(Old.EREC_UgyUgyiratdarabok as nvarchar(99)) as OldValue,
               cast(New.EREC_UgyUgyiratdarabok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EREC_UgyUgyiratdarabok as nvarchar(max)),'') != ISNULL(CAST(New.EREC_UgyUgyiratdarabok as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EREC_IraIratok' as ColumnName,               cast(Old.EREC_IraIratok as nvarchar(99)) as OldValue,
               cast(New.EREC_IraIratok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EREC_IraIratok as nvarchar(max)),'') != ISNULL(CAST(New.EREC_IraIratok as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EREC_PldIratPeldanyok' as ColumnName,               cast(Old.EREC_PldIratPeldanyok as nvarchar(99)) as OldValue,
               cast(New.EREC_PldIratPeldanyok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EREC_PldIratPeldanyok as nvarchar(max)),'') != ISNULL(CAST(New.EREC_PldIratPeldanyok as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EREC_HataridosFeladatok' as ColumnName,               cast(Old.EREC_HataridosFeladatok as nvarchar(99)) as OldValue,
               cast(New.EREC_HataridosFeladatok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EREC_HataridosFeladatok as nvarchar(max)),'') != ISNULL(CAST(New.EREC_HataridosFeladatok as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EREC_KuldKuldemenyek' as ColumnName,               cast(Old.EREC_KuldKuldemenyek as nvarchar(99)) as OldValue,
               cast(New.EREC_KuldKuldemenyek as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EREC_KuldKuldemenyek as nvarchar(max)),'') != ISNULL(CAST(New.EREC_KuldKuldemenyek as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IktatasiParameterek' as ColumnName,               cast(Old.IktatasiParameterek as nvarchar(99)) as OldValue,
               cast(New.IktatasiParameterek as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IktatasiParameterek as nvarchar(max)),'') != ISNULL(CAST(New.IktatasiParameterek as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ErkeztetesParameterek' as ColumnName,               cast(Old.ErkeztetesParameterek as nvarchar(99)) as OldValue,
               cast(New.ErkeztetesParameterek as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ErkeztetesParameterek as nvarchar(max)),'') != ISNULL(CAST(New.ErkeztetesParameterek as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Dokumentumok' as ColumnName,               cast(Old.Dokumentumok as nvarchar(99)) as OldValue,
               cast(New.Dokumentumok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Dokumentumok as nvarchar(max)),'') != ISNULL(CAST(New.Dokumentumok as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Azonosito' as ColumnName,               cast(Old.Azonosito as nvarchar(99)) as OldValue,
               cast(New.Azonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Azonosito as nvarchar(max)),'') != ISNULL(CAST(New.Azonosito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Allapot as nvarchar(max)),'') != ISNULL(CAST(New.Allapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'TOMEGES_IKTATAS_TETEL_ALLAPOT '
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Allapot and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Allapot and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Irat_Id' as ColumnName,               cast(Old.Irat_Id as nvarchar(99)) as OldValue,
               cast(New.Irat_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Irat_Id as nvarchar(max)),'') != ISNULL(CAST(New.Irat_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Hiba' as ColumnName,               cast(Old.Hiba as nvarchar(99)) as OldValue,
               cast(New.Hiba as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Hiba as nvarchar(max)),'') != ISNULL(CAST(New.Hiba as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Expedialas' as ColumnName,               cast(Old.Expedialas as nvarchar(99)) as OldValue,
               cast(New.Expedialas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Expedialas as nvarchar(max)),'') != ISNULL(CAST(New.Expedialas as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TobbIratEgyKuldemenybe' as ColumnName,               cast(Old.TobbIratEgyKuldemenybe as nvarchar(99)) as OldValue,
               cast(New.TobbIratEgyKuldemenybe as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.TobbIratEgyKuldemenybe as nvarchar(max)),'') != ISNULL(CAST(New.TobbIratEgyKuldemenybe as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldemenyAtadas' as ColumnName,               cast(Old.KuldemenyAtadas as nvarchar(99)) as OldValue,
               cast(New.KuldemenyAtadas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KuldemenyAtadas as nvarchar(max)),'') != ISNULL(CAST(New.KuldemenyAtadas as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatosagiStatAdat' as ColumnName,               cast(Old.HatosagiStatAdat as nvarchar(99)) as OldValue,
               cast(New.HatosagiStatAdat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HatosagiStatAdat as nvarchar(max)),'') != ISNULL(CAST(New.HatosagiStatAdat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EREC_IratAlairok' as ColumnName,               cast(Old.EREC_IratAlairok as nvarchar(99)) as OldValue,
               cast(New.EREC_IratAlairok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EREC_IratAlairok as nvarchar(max)),'') != ISNULL(CAST(New.EREC_IratAlairok as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Lezarhato' as ColumnName,               cast(Old.Lezarhato as nvarchar(99)) as OldValue,
               cast(New.Lezarhato as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_TomegesIktatasTetelekHistory Old
         inner join EREC_TomegesIktatasTetelekHistory New on Old.Ver = New.Ver-1
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Lezarhato as nvarchar(max)),'') != ISNULL(CAST(New.Lezarhato as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go