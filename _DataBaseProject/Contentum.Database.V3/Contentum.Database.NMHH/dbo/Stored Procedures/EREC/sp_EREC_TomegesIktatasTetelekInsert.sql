/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_TomegesIktatasTetelekInsert')
            and   type = 'P')
   drop procedure sp_EREC_TomegesIktatasTetelekInsert
go
*/
create procedure sp_EREC_TomegesIktatasTetelekInsert    
                @Id      uniqueidentifier = null,    
                               @Folyamat_Id     uniqueidentifier  = null,
                @Forras_Azonosito     Nvarchar(100)  = null,
                @IktatasTipus     int  = null,
                @Alszamra     int  = null,
                @Ugyirat_Id     uniqueidentifier  = null,
                @Kuldemeny_Id     uniqueidentifier  = null,
                @Iktatokonyv_Id     uniqueidentifier  = null,
                @ExecParam     Nvarchar(Max)  = null,
                @EREC_UgyUgyiratok     Nvarchar(Max)  = null,
                @EREC_UgyUgyiratdarabok     Nvarchar(Max)  = null,
                @EREC_IraIratok     Nvarchar(Max)  = null,
                @EREC_PldIratPeldanyok     Nvarchar(Max)  = null,
                @EREC_HataridosFeladatok     Nvarchar(Max)  = null,
                @EREC_KuldKuldemenyek     Nvarchar(Max)  = null,
                @IktatasiParameterek     Nvarchar(Max)  = null,
                @ErkeztetesParameterek     Nvarchar(Max)  = null,
                @Dokumentumok     Nvarchar(Max)  = null,
                @Azonosito     Nvarchar(100)  = null,
                @Allapot     nvarchar(64)  = null,
                @Irat_Id     uniqueidentifier  = null,
                @Hiba     Nvarchar(4000)  = null,
                @Expedialas     int  = null,
                @TobbIratEgyKuldemenybe     int  = null,
                @KuldemenyAtadas     uniqueidentifier  = null,
                @HatosagiStatAdat     Nvarchar(Max)  = null,
                @EREC_IratAlairok     Nvarchar(Max)  = null,
                @Lezarhato     int  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Folyamat_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Folyamat_Id'
            SET @insertValues = @insertValues + ',@Folyamat_Id'
         end 
       
         if @Forras_Azonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Forras_Azonosito'
            SET @insertValues = @insertValues + ',@Forras_Azonosito'
         end 
       
         if @IktatasTipus is not null
         begin
            SET @insertColumns = @insertColumns + ',IktatasTipus'
            SET @insertValues = @insertValues + ',@IktatasTipus'
         end 
       
         if @Alszamra is not null
         begin
            SET @insertColumns = @insertColumns + ',Alszamra'
            SET @insertValues = @insertValues + ',@Alszamra'
         end 
       
         if @Ugyirat_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Ugyirat_Id'
            SET @insertValues = @insertValues + ',@Ugyirat_Id'
         end 
       
         if @Kuldemeny_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Kuldemeny_Id'
            SET @insertValues = @insertValues + ',@Kuldemeny_Id'
         end 
       
         if @Iktatokonyv_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Iktatokonyv_Id'
            SET @insertValues = @insertValues + ',@Iktatokonyv_Id'
         end 
       
         if @ExecParam is not null
         begin
            SET @insertColumns = @insertColumns + ',ExecParam'
            SET @insertValues = @insertValues + ',@ExecParam'
         end 
       
         if @EREC_UgyUgyiratok is not null
         begin
            SET @insertColumns = @insertColumns + ',EREC_UgyUgyiratok'
            SET @insertValues = @insertValues + ',@EREC_UgyUgyiratok'
         end 
       
         if @EREC_UgyUgyiratdarabok is not null
         begin
            SET @insertColumns = @insertColumns + ',EREC_UgyUgyiratdarabok'
            SET @insertValues = @insertValues + ',@EREC_UgyUgyiratdarabok'
         end 
       
         if @EREC_IraIratok is not null
         begin
            SET @insertColumns = @insertColumns + ',EREC_IraIratok'
            SET @insertValues = @insertValues + ',@EREC_IraIratok'
         end 
       
         if @EREC_PldIratPeldanyok is not null
         begin
            SET @insertColumns = @insertColumns + ',EREC_PldIratPeldanyok'
            SET @insertValues = @insertValues + ',@EREC_PldIratPeldanyok'
         end 
       
         if @EREC_HataridosFeladatok is not null
         begin
            SET @insertColumns = @insertColumns + ',EREC_HataridosFeladatok'
            SET @insertValues = @insertValues + ',@EREC_HataridosFeladatok'
         end 
       
         if @EREC_KuldKuldemenyek is not null
         begin
            SET @insertColumns = @insertColumns + ',EREC_KuldKuldemenyek'
            SET @insertValues = @insertValues + ',@EREC_KuldKuldemenyek'
         end 
       
         if @IktatasiParameterek is not null
         begin
            SET @insertColumns = @insertColumns + ',IktatasiParameterek'
            SET @insertValues = @insertValues + ',@IktatasiParameterek'
         end 
       
         if @ErkeztetesParameterek is not null
         begin
            SET @insertColumns = @insertColumns + ',ErkeztetesParameterek'
            SET @insertValues = @insertValues + ',@ErkeztetesParameterek'
         end 
       
         if @Dokumentumok is not null
         begin
            SET @insertColumns = @insertColumns + ',Dokumentumok'
            SET @insertValues = @insertValues + ',@Dokumentumok'
         end 
       
         if @Azonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Azonosito'
            SET @insertValues = @insertValues + ',@Azonosito'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @Irat_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Irat_Id'
            SET @insertValues = @insertValues + ',@Irat_Id'
         end 
       
         if @Hiba is not null
         begin
            SET @insertColumns = @insertColumns + ',Hiba'
            SET @insertValues = @insertValues + ',@Hiba'
         end 
       
         if @Expedialas is not null
         begin
            SET @insertColumns = @insertColumns + ',Expedialas'
            SET @insertValues = @insertValues + ',@Expedialas'
         end 
       
         if @TobbIratEgyKuldemenybe is not null
         begin
            SET @insertColumns = @insertColumns + ',TobbIratEgyKuldemenybe'
            SET @insertValues = @insertValues + ',@TobbIratEgyKuldemenybe'
         end 
       
         if @KuldemenyAtadas is not null
         begin
            SET @insertColumns = @insertColumns + ',KuldemenyAtadas'
            SET @insertValues = @insertValues + ',@KuldemenyAtadas'
         end 
       
         if @HatosagiStatAdat is not null
         begin
            SET @insertColumns = @insertColumns + ',HatosagiStatAdat'
            SET @insertValues = @insertValues + ',@HatosagiStatAdat'
         end 
       
         if @EREC_IratAlairok is not null
         begin
            SET @insertColumns = @insertColumns + ',EREC_IratAlairok'
            SET @insertValues = @insertValues + ',@EREC_IratAlairok'
         end 
       
         if @Lezarhato is not null
         begin
            SET @insertColumns = @insertColumns + ',Lezarhato'
            SET @insertValues = @insertValues + ',@Lezarhato'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_TomegesIktatasTetelek ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Folyamat_Id uniqueidentifier,@Forras_Azonosito Nvarchar(100),@IktatasTipus int,@Alszamra int,@Ugyirat_Id uniqueidentifier,@Kuldemeny_Id uniqueidentifier,@Iktatokonyv_Id uniqueidentifier,@ExecParam Nvarchar(Max),@EREC_UgyUgyiratok Nvarchar(Max),@EREC_UgyUgyiratdarabok Nvarchar(Max),@EREC_IraIratok Nvarchar(Max),@EREC_PldIratPeldanyok Nvarchar(Max),@EREC_HataridosFeladatok Nvarchar(Max),@EREC_KuldKuldemenyek Nvarchar(Max),@IktatasiParameterek Nvarchar(Max),@ErkeztetesParameterek Nvarchar(Max),@Dokumentumok Nvarchar(Max),@Azonosito Nvarchar(100),@Allapot nvarchar(64),@Irat_Id uniqueidentifier,@Hiba Nvarchar(4000),@Expedialas int,@TobbIratEgyKuldemenybe int,@KuldemenyAtadas uniqueidentifier,@HatosagiStatAdat Nvarchar(Max),@EREC_IratAlairok Nvarchar(Max),@Lezarhato int,@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Folyamat_Id = @Folyamat_Id,@Forras_Azonosito = @Forras_Azonosito,@IktatasTipus = @IktatasTipus,@Alszamra = @Alszamra,@Ugyirat_Id = @Ugyirat_Id,@Kuldemeny_Id = @Kuldemeny_Id,@Iktatokonyv_Id = @Iktatokonyv_Id,@ExecParam = @ExecParam,@EREC_UgyUgyiratok = @EREC_UgyUgyiratok,@EREC_UgyUgyiratdarabok = @EREC_UgyUgyiratdarabok,@EREC_IraIratok = @EREC_IraIratok,@EREC_PldIratPeldanyok = @EREC_PldIratPeldanyok,@EREC_HataridosFeladatok = @EREC_HataridosFeladatok,@EREC_KuldKuldemenyek = @EREC_KuldKuldemenyek,@IktatasiParameterek = @IktatasiParameterek,@ErkeztetesParameterek = @ErkeztetesParameterek,@Dokumentumok = @Dokumentumok,@Azonosito = @Azonosito,@Allapot = @Allapot,@Irat_Id = @Irat_Id,@Hiba = @Hiba,@Expedialas = @Expedialas,@TobbIratEgyKuldemenybe = @TobbIratEgyKuldemenybe,@KuldemenyAtadas = @KuldemenyAtadas,@HatosagiStatAdat = @HatosagiStatAdat,@EREC_IratAlairok = @EREC_IratAlairok,@Lezarhato = @Lezarhato,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_TomegesIktatasTetelek',@ResultUid
					,'EREC_TomegesIktatasTetelekHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
go

/* használt errorcode-ok:
	50301:
	50302: 'Org' oszlop értéke nem lehet NULL
*/
