CREATE procedure [dbo].[sp_EREC_TomegesIktatasTetelekInsert_Bulk]    
        @ExecutorUserId				uniqueidentifier	,
		@EREC_TomegesIktatasTetelek EREC_TomegesIktatasTetelek_Type READONLY
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;

INSERT INTO EREC_TomegesIktatasTetelek
 (
           [Folyamat_Id]
           ,[Forras_Azonosito]
           ,[IktatasTipus]
           ,[Alszamra]
           ,[Iktatokonyv_Id]
           ,[Ugyirat_Id]
           ,[Kuldemeny_Id]
           ,[ExecParam]
           ,[EREC_UgyUgyiratok]
           ,[EREC_UgyUgyiratdarabok]
           ,[EREC_IraIratok]
           ,[EREC_PldIratPeldanyok]
           ,[EREC_HataridosFeladatok]
           ,[EREC_KuldKuldemenyek]
           ,[IktatasiParameterek]
           ,[ErkeztetesParameterek]
           ,[Dokumentumok]
           ,[Azonosito]
           ,[Allapot]
           ,[Irat_Id]
           ,[Hiba]
           ,[Ver]
           ,[Note]
           ,[Stat_id]
           ,[ErvKezd]
           ,[ErvVege]
           ,[Letrehozo_id]
           ,[LetrehozasIdo]
           ,[Modosito_id]
           ,[ModositasIdo]
           ,[Zarolo_id]
           ,[ZarolasIdo]
           ,[Tranz_id]
           ,[UIAccessLog_id]
           ,[Expedialas]
           ,[TobbIratEgyKuldemenybe]
           ,[KuldemenyAtadas]
           ,[HatosagiStatAdat]
           ,[EREC_IratAlairok]
           ,[Lezarhato])
    SELECT * FROM @EREC_TomegesIktatasTetelek

if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
--ELSE
--BEGIN
--   /* History Log */
--   exec sp_LogRecordToHistory 'EREC_TomegesIktatasTetelek',@ResultUid
--					,'EREC_TomegesIktatasTetelekHistory',0,@Letrehozo_id,@LetrehozasIdo
--END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
GO