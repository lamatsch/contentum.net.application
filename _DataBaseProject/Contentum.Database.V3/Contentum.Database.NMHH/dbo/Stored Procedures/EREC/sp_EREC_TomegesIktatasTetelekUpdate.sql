
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_TomegesIktatasTetelekUpdate')
            and   type = 'P')
   drop procedure sp_EREC_TomegesIktatasTetelekUpdate
go
*/
create procedure sp_EREC_TomegesIktatasTetelekUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Folyamat_Id     uniqueidentifier  = null,         
             @Forras_Azonosito     Nvarchar(100)  = null,         
             @IktatasTipus     int  = null,         
             @Alszamra     int  = null,         
             @Ugyirat_Id     uniqueidentifier  = null,         
             @Kuldemeny_Id     uniqueidentifier  = null,         
             @Iktatokonyv_Id     uniqueidentifier  = null,         
             @ExecParam     Nvarchar(Max)  = null,         
             @EREC_UgyUgyiratok     Nvarchar(Max)  = null,         
             @EREC_UgyUgyiratdarabok     Nvarchar(Max)  = null,         
             @EREC_IraIratok     Nvarchar(Max)  = null,         
             @EREC_PldIratPeldanyok     Nvarchar(Max)  = null,         
             @EREC_HataridosFeladatok     Nvarchar(Max)  = null,         
             @EREC_KuldKuldemenyek     Nvarchar(Max)  = null,         
             @IktatasiParameterek     Nvarchar(Max)  = null,         
             @ErkeztetesParameterek     Nvarchar(Max)  = null,         
             @Dokumentumok     Nvarchar(Max)  = null,         
             @Azonosito     Nvarchar(100)  = null,         
             @Allapot     nvarchar(64)  = null,         
             @Irat_Id     uniqueidentifier  = null,         
             @Hiba     Nvarchar(4000)  = null,         
             @Expedialas     int  = null,         
             @TobbIratEgyKuldemenybe     int  = null,         
             @KuldemenyAtadas     uniqueidentifier  = null,         
             @HatosagiStatAdat     Nvarchar(Max)  = null,         
             @EREC_IratAlairok     Nvarchar(Max)  = null,         
             @Lezarhato     int  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Folyamat_Id     uniqueidentifier         
     DECLARE @Act_Forras_Azonosito     Nvarchar(100)         
     DECLARE @Act_IktatasTipus     int         
     DECLARE @Act_Alszamra     int         
     DECLARE @Act_Ugyirat_Id     uniqueidentifier         
     DECLARE @Act_Kuldemeny_Id     uniqueidentifier         
     DECLARE @Act_Iktatokonyv_Id     uniqueidentifier         
     DECLARE @Act_ExecParam     Nvarchar(Max)         
     DECLARE @Act_EREC_UgyUgyiratok     Nvarchar(Max)         
     DECLARE @Act_EREC_UgyUgyiratdarabok     Nvarchar(Max)         
     DECLARE @Act_EREC_IraIratok     Nvarchar(Max)         
     DECLARE @Act_EREC_PldIratPeldanyok     Nvarchar(Max)         
     DECLARE @Act_EREC_HataridosFeladatok     Nvarchar(Max)         
     DECLARE @Act_EREC_KuldKuldemenyek     Nvarchar(Max)         
     DECLARE @Act_IktatasiParameterek     Nvarchar(Max)         
     DECLARE @Act_ErkeztetesParameterek     Nvarchar(Max)         
     DECLARE @Act_Dokumentumok     Nvarchar(Max)         
     DECLARE @Act_Azonosito     Nvarchar(100)         
     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_Irat_Id     uniqueidentifier         
     DECLARE @Act_Hiba     Nvarchar(4000)         
     DECLARE @Act_Expedialas     int         
     DECLARE @Act_TobbIratEgyKuldemenybe     int         
     DECLARE @Act_KuldemenyAtadas     uniqueidentifier         
     DECLARE @Act_HatosagiStatAdat     Nvarchar(Max)         
     DECLARE @Act_EREC_IratAlairok     Nvarchar(Max)         
     DECLARE @Act_Lezarhato     int         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on

select 
     @Act_Folyamat_Id = Folyamat_Id,
     @Act_Forras_Azonosito = Forras_Azonosito,
     @Act_IktatasTipus = IktatasTipus,
     @Act_Alszamra = Alszamra,
     @Act_Ugyirat_Id = Ugyirat_Id,
     @Act_Kuldemeny_Id = Kuldemeny_Id,
     @Act_Iktatokonyv_Id = Iktatokonyv_Id,
     @Act_ExecParam = ExecParam,
     @Act_EREC_UgyUgyiratok = EREC_UgyUgyiratok,
     @Act_EREC_UgyUgyiratdarabok = EREC_UgyUgyiratdarabok,
     @Act_EREC_IraIratok = EREC_IraIratok,
     @Act_EREC_PldIratPeldanyok = EREC_PldIratPeldanyok,
     @Act_EREC_HataridosFeladatok = EREC_HataridosFeladatok,
     @Act_EREC_KuldKuldemenyek = EREC_KuldKuldemenyek,
     @Act_IktatasiParameterek = IktatasiParameterek,
     @Act_ErkeztetesParameterek = ErkeztetesParameterek,
     @Act_Dokumentumok = Dokumentumok,
     @Act_Azonosito = Azonosito,
     @Act_Allapot = Allapot,
     @Act_Irat_Id = Irat_Id,
     @Act_Hiba = Hiba,
     @Act_Expedialas = Expedialas,
     @Act_TobbIratEgyKuldemenybe = TobbIratEgyKuldemenybe,
     @Act_KuldemenyAtadas = KuldemenyAtadas,
     @Act_HatosagiStatAdat = HatosagiStatAdat,
     @Act_EREC_IratAlairok = EREC_IratAlairok,
     @Act_Lezarhato = Lezarhato,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_TomegesIktatasTetelek
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zárolás ellenőrzés:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/Folyamat_Id')=1
         SET @Act_Folyamat_Id = @Folyamat_Id
   IF @UpdatedColumns.exist('/root/Forras_Azonosito')=1
         SET @Act_Forras_Azonosito = @Forras_Azonosito
   IF @UpdatedColumns.exist('/root/IktatasTipus')=1
         SET @Act_IktatasTipus = @IktatasTipus
   IF @UpdatedColumns.exist('/root/Alszamra')=1
         SET @Act_Alszamra = @Alszamra
   IF @UpdatedColumns.exist('/root/Ugyirat_Id')=1
         SET @Act_Ugyirat_Id = @Ugyirat_Id
   IF @UpdatedColumns.exist('/root/Kuldemeny_Id')=1
         SET @Act_Kuldemeny_Id = @Kuldemeny_Id
   IF @UpdatedColumns.exist('/root/Iktatokonyv_Id')=1
         SET @Act_Iktatokonyv_Id = @Iktatokonyv_Id
   IF @UpdatedColumns.exist('/root/ExecParam')=1
         SET @Act_ExecParam = @ExecParam
   IF @UpdatedColumns.exist('/root/EREC_UgyUgyiratok')=1
         SET @Act_EREC_UgyUgyiratok = @EREC_UgyUgyiratok
   IF @UpdatedColumns.exist('/root/EREC_UgyUgyiratdarabok')=1
         SET @Act_EREC_UgyUgyiratdarabok = @EREC_UgyUgyiratdarabok
   IF @UpdatedColumns.exist('/root/EREC_IraIratok')=1
         SET @Act_EREC_IraIratok = @EREC_IraIratok
   IF @UpdatedColumns.exist('/root/EREC_PldIratPeldanyok')=1
         SET @Act_EREC_PldIratPeldanyok = @EREC_PldIratPeldanyok
   IF @UpdatedColumns.exist('/root/EREC_HataridosFeladatok')=1
         SET @Act_EREC_HataridosFeladatok = @EREC_HataridosFeladatok
   IF @UpdatedColumns.exist('/root/EREC_KuldKuldemenyek')=1
         SET @Act_EREC_KuldKuldemenyek = @EREC_KuldKuldemenyek
   IF @UpdatedColumns.exist('/root/IktatasiParameterek')=1
         SET @Act_IktatasiParameterek = @IktatasiParameterek
   IF @UpdatedColumns.exist('/root/ErkeztetesParameterek')=1
         SET @Act_ErkeztetesParameterek = @ErkeztetesParameterek
   IF @UpdatedColumns.exist('/root/Dokumentumok')=1
         SET @Act_Dokumentumok = @Dokumentumok
   IF @UpdatedColumns.exist('/root/Azonosito')=1
         SET @Act_Azonosito = @Azonosito
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/Irat_Id')=1
         SET @Act_Irat_Id = @Irat_Id
   IF @UpdatedColumns.exist('/root/Hiba')=1
         SET @Act_Hiba = @Hiba
   IF @UpdatedColumns.exist('/root/Expedialas')=1
         SET @Act_Expedialas = @Expedialas
   IF @UpdatedColumns.exist('/root/TobbIratEgyKuldemenybe')=1
         SET @Act_TobbIratEgyKuldemenybe = @TobbIratEgyKuldemenybe
   IF @UpdatedColumns.exist('/root/KuldemenyAtadas')=1
         SET @Act_KuldemenyAtadas = @KuldemenyAtadas
   IF @UpdatedColumns.exist('/root/HatosagiStatAdat')=1
         SET @Act_HatosagiStatAdat = @HatosagiStatAdat
   IF @UpdatedColumns.exist('/root/EREC_IratAlairok')=1
         SET @Act_EREC_IratAlairok = @EREC_IratAlairok
   IF @UpdatedColumns.exist('/root/Lezarhato')=1
         SET @Act_Lezarhato = @Lezarhato
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_TomegesIktatasTetelek
SET
     Folyamat_Id = @Act_Folyamat_Id,
     Forras_Azonosito = @Act_Forras_Azonosito,
     IktatasTipus = @Act_IktatasTipus,
     Alszamra = @Act_Alszamra,
     Ugyirat_Id = @Act_Ugyirat_Id,
     Kuldemeny_Id = @Act_Kuldemeny_Id,
     Iktatokonyv_Id = @Act_Iktatokonyv_Id,
     ExecParam = @Act_ExecParam,
     EREC_UgyUgyiratok = @Act_EREC_UgyUgyiratok,
     EREC_UgyUgyiratdarabok = @Act_EREC_UgyUgyiratdarabok,
     EREC_IraIratok = @Act_EREC_IraIratok,
     EREC_PldIratPeldanyok = @Act_EREC_PldIratPeldanyok,
     EREC_HataridosFeladatok = @Act_EREC_HataridosFeladatok,
     EREC_KuldKuldemenyek = @Act_EREC_KuldKuldemenyek,
     IktatasiParameterek = @Act_IktatasiParameterek,
     ErkeztetesParameterek = @Act_ErkeztetesParameterek,
     Dokumentumok = @Act_Dokumentumok,
     Azonosito = @Act_Azonosito,
     Allapot = @Act_Allapot,
     Irat_Id = @Act_Irat_Id,
     Hiba = @Act_Hiba,
     Expedialas = @Act_Expedialas,
     TobbIratEgyKuldemenybe = @Act_TobbIratEgyKuldemenybe,
     KuldemenyAtadas = @Act_KuldemenyAtadas,
     HatosagiStatAdat = @Act_HatosagiStatAdat,
     EREC_IratAlairok = @Act_EREC_IratAlairok,
     Lezarhato = @Act_Lezarhato,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_TomegesIktatasTetelek',@Id
					,'EREC_TomegesIktatasTetelekHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

go

/* használt errorcode-ok:
	50401:
	50402: 'Ver' oszlop értéke nem egyezik a meglévő rekordéval
   50403: érvényesség már lejárt a rekordra
	50499: rekord zárolva
*/
