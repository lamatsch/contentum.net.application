﻿
create procedure [dbo].[sp_EREC_TomegesIktatasUpdate]
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @ForrasTipusNev     Nvarchar(4000)  = null,         
             @FelelosCsoport_Id     uniqueidentifier  = null,         
             @AgazatiJelek_Id     uniqueidentifier  = null,         
             @IraIrattariTetelek_Id     uniqueidentifier  = null,         
             @Ugytipus_Id     uniqueidentifier  = null,         
             @Irattipus_Id     uniqueidentifier  = null,         
             @TargyPrefix     Nvarchar(4000)  = null,         
             @AlairasKell     char(1)  = null,         
             @AlairasMod     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Alairo     uniqueidentifier  = null,         
             @FelhaszCsoport_Id_Helyettesito     uniqueidentifier  = null,         
             @AlairasSzabaly_Id     uniqueidentifier  = null,         
             @HatosagiAdatlapKell     char(1)  = null,         
             @UgyFajtaja     nvarchar(64)  = null,         
             @DontestHozta     nvarchar(64)  = null,         
             @DontesFormaja     nvarchar(64)  = null,         
             @UgyintezesHataridore     nvarchar(64)  = null,         
             @HataridoTullepes     int  = null,         
             @HatosagiEllenorzes     char(1)  = null,         
             @MunkaorakSzama     float  = null,         
             @EljarasiKoltseg     int  = null,         
             @KozigazgatasiBirsagMerteke     int  = null,         
             @SommasEljDontes     nvarchar(64)  = null,         
             @NyolcNapBelulNemSommas     nvarchar(64)  = null,         
             @FuggoHatalyuHatarozat     nvarchar(64)  = null,         
             @FuggoHatalyuVegzes     nvarchar(64)  = null,         
             @HatAltalVisszafizOsszeg     int  = null,         
             @HatTerheloEljKtsg     int  = null,         
             @FelfuggHatarozat     nvarchar(64)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
			 @AutoExpedialasKell   char(1) = null,
			 @AdathordozoTipusa nvarchar(64) = null,
			 @UgyintezesAlapja nvarchar(100) = null,
			 @ExpedialasModja nvarchar(64) = null,
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_ForrasTipusNev     Nvarchar(4000)         
     DECLARE @Act_FelelosCsoport_Id     uniqueidentifier         
     DECLARE @Act_AgazatiJelek_Id     uniqueidentifier         
     DECLARE @Act_IraIrattariTetelek_Id     uniqueidentifier         
     DECLARE @Act_Ugytipus_Id     uniqueidentifier         
     DECLARE @Act_Irattipus_Id     uniqueidentifier         
     DECLARE @Act_TargyPrefix     Nvarchar(4000)         
     DECLARE @Act_AlairasKell     char(1)         
     DECLARE @Act_AlairasMod     uniqueidentifier         
     DECLARE @Act_FelhasznaloCsoport_Id_Alairo     uniqueidentifier         
     DECLARE @Act_FelhaszCsoport_Id_Helyettesito     uniqueidentifier         
     DECLARE @Act_AlairasSzabaly_Id     uniqueidentifier         
     DECLARE @Act_HatosagiAdatlapKell     char(1)         
     DECLARE @Act_UgyFajtaja     nvarchar(64)         
     DECLARE @Act_DontestHozta     nvarchar(64)         
     DECLARE @Act_DontesFormaja     nvarchar(64)         
     DECLARE @Act_UgyintezesHataridore     nvarchar(64)         
     DECLARE @Act_HataridoTullepes     int         
     DECLARE @Act_HatosagiEllenorzes     char(1)         
     DECLARE @Act_MunkaorakSzama     float         
     DECLARE @Act_EljarasiKoltseg     int         
     DECLARE @Act_KozigazgatasiBirsagMerteke     int         
     DECLARE @Act_SommasEljDontes     nvarchar(64)         
     DECLARE @Act_NyolcNapBelulNemSommas     nvarchar(64)         
     DECLARE @Act_FuggoHatalyuHatarozat     nvarchar(64)         
     DECLARE @Act_FuggoHatalyuVegzes     nvarchar(64)         
     DECLARE @Act_HatAltalVisszafizOsszeg     int         
     DECLARE @Act_HatTerheloEljKtsg     int         
     DECLARE @Act_FelfuggHatarozat     nvarchar(64)         
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
	 DECLARE @Act_AutoExpedialasKell   char(1) 
	 DECLARE @Act_AdathordozoTipusa nvarchar(64) 
	 DECLARE @Act_UgyintezesAlapja      nvarchar(100) 
	 DECLARE @Act_ExpedialasModja nvarchar(64)  
  
set nocount on

select 
     @Act_ForrasTipusNev = ForrasTipusNev,
     @Act_FelelosCsoport_Id = FelelosCsoport_Id,
     @Act_AgazatiJelek_Id = AgazatiJelek_Id,
     @Act_IraIrattariTetelek_Id = IraIrattariTetelek_Id,
     @Act_Ugytipus_Id = Ugytipus_Id,
     @Act_Irattipus_Id = Irattipus_Id,
     @Act_TargyPrefix = TargyPrefix,
     @Act_AlairasKell = AlairasKell,
     @Act_AlairasMod = AlairasMod,
     @Act_FelhasznaloCsoport_Id_Alairo = FelhasznaloCsoport_Id_Alairo,
     @Act_FelhaszCsoport_Id_Helyettesito = FelhaszCsoport_Id_Helyettesito,
     @Act_AlairasSzabaly_Id = AlairasSzabaly_Id,
     @Act_HatosagiAdatlapKell = HatosagiAdatlapKell,
     @Act_UgyFajtaja = UgyFajtaja,
     @Act_DontestHozta = DontestHozta,
     @Act_DontesFormaja = DontesFormaja,
     @Act_UgyintezesHataridore = UgyintezesHataridore,
     @Act_HataridoTullepes = HataridoTullepes,
     @Act_HatosagiEllenorzes = HatosagiEllenorzes,
     @Act_MunkaorakSzama = MunkaorakSzama,
     @Act_EljarasiKoltseg = EljarasiKoltseg,
     @Act_KozigazgatasiBirsagMerteke = KozigazgatasiBirsagMerteke,
     @Act_SommasEljDontes = SommasEljDontes,
     @Act_NyolcNapBelulNemSommas = NyolcNapBelulNemSommas,
     @Act_FuggoHatalyuHatarozat = FuggoHatalyuHatarozat,
     @Act_FuggoHatalyuVegzes = FuggoHatalyuVegzes,
     @Act_HatAltalVisszafizOsszeg = HatAltalVisszafizOsszeg,
     @Act_HatTerheloEljKtsg = HatTerheloEljKtsg,
     @Act_FelfuggHatarozat = FelfuggHatarozat,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id,
	 @Act_AutoExpedialasKell = AutoExpedialasKell,
	 @Act_AdathordozoTipusa = AdathordozoTipusa,
	 @Act_UgyintezesAlapja  = UgyintezesAlapja,
	 @Act_ExpedialasModja = ExpedialasModja
from EREC_TomegesIktatas
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
	  or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- zĂˇrolĂˇs ellenĹ‘rzĂ©s:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
	RAISERROR('[50499]',16,1)
	return @@error
END

	   IF @UpdatedColumns.exist('/root/ForrasTipusNev')=1
         SET @Act_ForrasTipusNev = @ForrasTipusNev
   IF @UpdatedColumns.exist('/root/FelelosCsoport_Id')=1
         SET @Act_FelelosCsoport_Id = @FelelosCsoport_Id
   IF @UpdatedColumns.exist('/root/AgazatiJelek_Id')=1
         SET @Act_AgazatiJelek_Id = @AgazatiJelek_Id
   IF @UpdatedColumns.exist('/root/IraIrattariTetelek_Id')=1
         SET @Act_IraIrattariTetelek_Id = @IraIrattariTetelek_Id
   IF @UpdatedColumns.exist('/root/Ugytipus_Id')=1
         SET @Act_Ugytipus_Id = @Ugytipus_Id
   IF @UpdatedColumns.exist('/root/Irattipus_Id')=1
         SET @Act_Irattipus_Id = @Irattipus_Id
   IF @UpdatedColumns.exist('/root/TargyPrefix')=1
         SET @Act_TargyPrefix = @TargyPrefix
   IF @UpdatedColumns.exist('/root/AlairasKell')=1
         SET @Act_AlairasKell = @AlairasKell
   IF @UpdatedColumns.exist('/root/AlairasMod')=1
         SET @Act_AlairasMod = @AlairasMod
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Alairo')=1
         SET @Act_FelhasznaloCsoport_Id_Alairo = @FelhasznaloCsoport_Id_Alairo
   IF @UpdatedColumns.exist('/root/FelhaszCsoport_Id_Helyettesito')=1
         SET @Act_FelhaszCsoport_Id_Helyettesito = @FelhaszCsoport_Id_Helyettesito
   IF @UpdatedColumns.exist('/root/AlairasSzabaly_Id')=1
         SET @Act_AlairasSzabaly_Id = @AlairasSzabaly_Id
   IF @UpdatedColumns.exist('/root/HatosagiAdatlapKell')=1
         SET @Act_HatosagiAdatlapKell = @HatosagiAdatlapKell
   IF @UpdatedColumns.exist('/root/UgyFajtaja')=1
         SET @Act_UgyFajtaja = @UgyFajtaja
   IF @UpdatedColumns.exist('/root/DontestHozta')=1
         SET @Act_DontestHozta = @DontestHozta
   IF @UpdatedColumns.exist('/root/DontesFormaja')=1
         SET @Act_DontesFormaja = @DontesFormaja
   IF @UpdatedColumns.exist('/root/UgyintezesHataridore')=1
         SET @Act_UgyintezesHataridore = @UgyintezesHataridore
   IF @UpdatedColumns.exist('/root/HataridoTullepes')=1
         SET @Act_HataridoTullepes = @HataridoTullepes
   IF @UpdatedColumns.exist('/root/HatosagiEllenorzes')=1
         SET @Act_HatosagiEllenorzes = @HatosagiEllenorzes
   IF @UpdatedColumns.exist('/root/MunkaorakSzama')=1
         SET @Act_MunkaorakSzama = @MunkaorakSzama
   IF @UpdatedColumns.exist('/root/EljarasiKoltseg')=1
         SET @Act_EljarasiKoltseg = @EljarasiKoltseg
   IF @UpdatedColumns.exist('/root/KozigazgatasiBirsagMerteke')=1
         SET @Act_KozigazgatasiBirsagMerteke = @KozigazgatasiBirsagMerteke
   IF @UpdatedColumns.exist('/root/SommasEljDontes')=1
         SET @Act_SommasEljDontes = @SommasEljDontes
   IF @UpdatedColumns.exist('/root/NyolcNapBelulNemSommas')=1
         SET @Act_NyolcNapBelulNemSommas = @NyolcNapBelulNemSommas
   IF @UpdatedColumns.exist('/root/FuggoHatalyuHatarozat')=1
         SET @Act_FuggoHatalyuHatarozat = @FuggoHatalyuHatarozat
   IF @UpdatedColumns.exist('/root/FuggoHatalyuVegzes')=1
         SET @Act_FuggoHatalyuVegzes = @FuggoHatalyuVegzes
   IF @UpdatedColumns.exist('/root/HatAltalVisszafizOsszeg')=1
         SET @Act_HatAltalVisszafizOsszeg = @HatAltalVisszafizOsszeg
   IF @UpdatedColumns.exist('/root/HatTerheloEljKtsg')=1
         SET @Act_HatTerheloEljKtsg = @HatTerheloEljKtsg
   IF @UpdatedColumns.exist('/root/FelfuggHatarozat')=1
         SET @Act_FelfuggHatarozat = @FelfuggHatarozat
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @UpdatedColumns.exist('/root/AutoExpedialasKell')=1
         SET @Act_AutoExpedialasKell = @AutoExpedialasKell  	  
   IF @UpdatedColumns.exist('/root/AdathordozoTipusa')=1
         SET @Act_AdathordozoTipusa = @AdathordozoTipusa   
   IF @UpdatedColumns.exist('/root/UgyintezesAlapja')=1
         SET @Act_UgyintezesAlapja = @UgyintezesAlapja   
   IF @UpdatedColumns.exist('/root/ExpedialasModja')=1
         SET @Act_ExpedialasModja = @ExpedialasModja   
   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_TomegesIktatas
SET
     ForrasTipusNev = @Act_ForrasTipusNev,
     FelelosCsoport_Id = @Act_FelelosCsoport_Id,
     AgazatiJelek_Id = @Act_AgazatiJelek_Id,
     IraIrattariTetelek_Id = @Act_IraIrattariTetelek_Id,
     Ugytipus_Id = @Act_Ugytipus_Id,
     Irattipus_Id = @Act_Irattipus_Id,
     TargyPrefix = @Act_TargyPrefix,
     AlairasKell = @Act_AlairasKell,
     AlairasMod = @Act_AlairasMod,
     FelhasznaloCsoport_Id_Alairo = @Act_FelhasznaloCsoport_Id_Alairo,
     FelhaszCsoport_Id_Helyettesito = @Act_FelhaszCsoport_Id_Helyettesito,
     AlairasSzabaly_Id = @Act_AlairasSzabaly_Id,
     HatosagiAdatlapKell = @Act_HatosagiAdatlapKell,
     UgyFajtaja = @Act_UgyFajtaja,
     DontestHozta = @Act_DontestHozta,
     DontesFormaja = @Act_DontesFormaja,
     UgyintezesHataridore = @Act_UgyintezesHataridore,
     HataridoTullepes = @Act_HataridoTullepes,
     HatosagiEllenorzes = @Act_HatosagiEllenorzes,
     MunkaorakSzama = @Act_MunkaorakSzama,
     EljarasiKoltseg = @Act_EljarasiKoltseg,
     KozigazgatasiBirsagMerteke = @Act_KozigazgatasiBirsagMerteke,
     SommasEljDontes = @Act_SommasEljDontes,
     NyolcNapBelulNemSommas = @Act_NyolcNapBelulNemSommas,
     FuggoHatalyuHatarozat = @Act_FuggoHatalyuHatarozat,
     FuggoHatalyuVegzes = @Act_FuggoHatalyuVegzes,
     HatAltalVisszafizOsszeg = @Act_HatAltalVisszafizOsszeg,
     HatTerheloEljKtsg = @Act_HatTerheloEljKtsg,
     FelfuggHatarozat = @Act_FelfuggHatarozat,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id,
	 AutoExpedialasKell = @Act_AutoExpedialasKell,
	 AdathordozoTipusa = @Act_AdathordozoTipusa,
	 UgyintezesAlapja  = @Act_UgyintezesAlapja,
	 ExpedialasModja = @Act_ExpedialasModja
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_TomegesIktatas',@Id
					,'EREC_TomegesIktatasHistory',1,@ExecutorUserId,@ExecutionTime   
end


--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH