﻿create procedure [dbo].[sp_EREC_UgyKezFeljegyzesekGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_UgyKezFeljegyzesek.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_UgyKezFeljegyzesek.Id,
	   EREC_UgyKezFeljegyzesek.UgyUgyirat_Id,
	   EREC_UgyKezFeljegyzesek.KezelesTipus,
	   EREC_UgyKezFeljegyzesek.Leiras,
	   EREC_UgyKezFeljegyzesek.Ver,
	   EREC_UgyKezFeljegyzesek.Note,
	   EREC_UgyKezFeljegyzesek.Stat_id,
	   EREC_UgyKezFeljegyzesek.ErvKezd,
	   EREC_UgyKezFeljegyzesek.ErvVege,
	   EREC_UgyKezFeljegyzesek.Letrehozo_id,
	   EREC_UgyKezFeljegyzesek.LetrehozasIdo,
	   EREC_UgyKezFeljegyzesek.Modosito_id,
	   EREC_UgyKezFeljegyzesek.ModositasIdo,
	   EREC_UgyKezFeljegyzesek.Zarolo_id,
	   EREC_UgyKezFeljegyzesek.ZarolasIdo,
	   EREC_UgyKezFeljegyzesek.Tranz_id,
	   EREC_UgyKezFeljegyzesek.UIAccessLog_id  
   from 
     EREC_UgyKezFeljegyzesek as EREC_UgyKezFeljegyzesek      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end