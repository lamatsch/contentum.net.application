--set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--if exists (select 1
--            from  sysobjects
--           where  id = object_id('sp_EREC_UgyUgyiratdarabokTomegesInit')
--            and   type = 'P')
--   drop procedure sp_EREC_UgyUgyiratdarabokTomegesInit
--go


CREATE procedure [dbo].[sp_EREC_UgyUgyiratdarabokTomegesInit]
				@UgyUgyirat_IdList nvarchar(max) = null,
				@EljarasiSzakasz nvarchar(64) = null,
				@Allapot     nvarchar(64)  = null,
                @Letrehozo_id     uniqueidentifier  = null
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;

declare @VegrehajtasIdo datetime
set @VegrehajtasIdo = getdate()

declare @temp table(UgyUgyirat_Id uniqueidentifier, IraIktatokonyv_Id uniqueidentifier, Foszam int)

insert into @temp
(UgyUgyirat_Id)
select [Value] from fn_SplitWithPos(@UgyUgyirat_IdList, ',')

update @temp
set IraIktatokonyv_Id = u.IraIktatokonyv_Id,
	Foszam = u.Foszam
from
@temp t join EREC_UgyUgyiratok u on t.UgyUgyirat_Id = u.Id

DECLARE @InsertedRow TABLE (id uniqueidentifier)

insert into EREC_UgyUgyiratdarabok
	(UgyUgyirat_Id,
	EljarasiSzakasz,
	Allapot,
	IraIktatokonyv_Id,
	Foszam,
	Letrehozo_id)
output inserted.id into @InsertedRow
select
	t.UgyUgyirat_Id,
	@EljarasiSzakasz,
	@Allapot,
	t.IraIktatokonyv_Id,
	t.Foszam,
	@Letrehozo_id
from
@temp t
    

if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
      /* History Log */
      declare @row_ids varchar(MAX)
      /*** EREC_UgyUgyiratdarabok history log ***/

      set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @InsertedRow FOR XML PATH('')),1, 2, '')
      if @row_ids is not null
         set @row_ids = @row_ids + '''';

      exec sp_LogRecordsToHistory_Tomeges
       @TableName = 'EREC_UgyUgyiratdarabok'
      ,@Row_Ids = @row_ids
      ,@Muvelet = 0
      ,@Vegrehajto_Id = @Letrehozo_id
      ,@VegrehajtasIdo = @VegrehajtasIdo
END            


select Id from @InsertedRow
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH