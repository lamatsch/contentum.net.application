﻿--set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--if exists (select 1
--            from  sysobjects
--           where  id = object_id('sp_EREC_UgyUgyiratokCalendarView')
--            and   type = 'P')
--   drop procedure sp_EREC_UgyUgyiratokCalendarView
--go

/*
History:
--- létrehozva #321(#2221) - Naptár nézet kialakítása  ( minta: EREC_UgyUgyiratokGetAllWithExtension )
*/

CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratokCalendarView]
  @DatumTol                datetime
 ,@DatumIg                 datetime
 ,@Where                   nvarchar(MAX)     = '' 
 --- UGY_FAJTAJA szűrés megadása; 
 --- ? további @where_... szűrések 
 ,@CsakAktivIrat           bit               = 1         -- Csak az AKTIV Iratokat láttatjuk: 1 (default); vagy minden irat: 0  
 ,@OrderBy                 nvarchar(200)     = ' order by EREC_UgyUgyiratok.LetrehozasIdo'           --- ? "ügyintézőnként": EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez
 ,@ExecutorUserId          uniqueidentifier
 ,@UgyintezoId          uniqueidentifier
 --- Nem az általános láthatóság ellenőrzés alapján határozzuk meg a lekért ügyiratokat 
 ---    ha @ExecutorUserId egy szervezet, akkor a szervezet minden tagjának ügyiratát adjuk át
 ---    egyébként mindenki saját jogán ( plusz helyettesített ) lát csak ügyiratokat
 ---    Admin szervezet tagjai esetén sem szűrünk másként
as

begin

BEGIN TRY
   set nocount ON
   
   DECLARE @Org uniqueidentifier
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
   if (@Org is null)
      RAISERROR('[50202]',16,1);
   
   DECLARE @OrgKod nvarchar(100)
   set @OrgKod = (select Kod from KRT_Orgok where Id = @Org)

   --- Minden sort átadunk, nem kell lapozással foglalkozni   
   declare @firstRow int = 1
   declare @lastRow  int -- később beállítjuk rowcount() alapján
   
   DECLARE @sqlcmd nvarchar(MAX);
   DECLARE @sqlCTL nvarchar(MAX);

   /* --- */
   SET @sqlcmd = '; ';
   SET @sqlCTL = '; ';
   if ( select Tipus from KRT_Csoportok cs where cs.ID = @UgyintezoId and cs.Org = @Org ) = '1'                      -- azaz dolgozó
     SET @sqlCTL += 'with CsoportTagokAll as ( select cs.ID as ID from KRT_Csoportok cs
	                                            where cs.ID = @UgyintezoId 
												  and getdate() between cs.ErvKezd AND cs.ErvVege
												  and cs.Tipus = ''1''
												  --- and cs.Org = @Org                                               -- felesleges, hiszen @UgyintezoId alapján határoztuk meg
	             --                              UNION all
											   --select h.Felhasznalo_Id_Helyettesitett as ID from Krt_Helyettesitesek h
											   -- where h.Felhasznalo_Id_Helyettesito = @UgyintezoId                 -- ez egyben szűri @Org -ot is 
												  --and getdate() between h.HelyettesitesKezd AND h.HelyettesitesVege
												  ----- and getdate() between cs.ErvKezd AND cs.ErvVege                 -- csak szinkronban van HelyettesitesKezd-Vege értékekkel
												  --and h.HelyettesitesMod = ''1''                                          -- azaz helyettesites
                                             ) ';
   else 
     SET @sqlCTL += 'with CsoportTagokAll as ( select cs.ID as ID from KRT_Csoportok cs
	                                            where cs.ID = @UgyintezoId 
												  and getdate() between cs.ErvKezd AND cs.ErvVege
	                                           UNION all 
											   select t.Csoport_ID_Jogalany as ID from Krt_CsoportTagok t
											    where t.Csoport_Id = @UgyintezoId                                  -- ez egyben szűri @Org -ot is 
												  and getdate() between t.ErvKezd AND t.ErvVege   
												  --- and t.Tipus <> ''1''                                                -- azaz NEM alszervezet, DE egyelőre mindent hozunk
											  ) ';
											     
   SET @sqlcmd += @sqlCTL + 'select distinct EREC_UgyUgyiratok.Id into #filter from EREC_UgyUgyiratok
                              inner join CsoportTagokAll on CsoportTagokAll.ID = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez
                               left join EREC_IraIktatokonyvek on EREC_IraIktatokonyvek.Id = EREC_UgyUgyiratok.IraIktatokonyv_Id and EREC_IraIktatokonyvek.Org=@Org            -- ORG szűrés és tárgyév meghatározása
							   left join EREC_IraIratok on EREC_IraIratok.Ugyirat_ID = EREC_UgyUgyiratok.Id AND EREC_IraIratok.Alszam = 1	                                   -- IktatásDátuma szűrés
                               left join EREC_IratMetaDefinicio on EREC_IratMetaDefinicio.Id = EREC_UgyUgyiratok.IratMetadefinicio_Id                                          -- UGY_FAJTAJA szűrés    
                                 left join KRT_Kodtarak as UgyFajtajaKodTarak on UgyFajtajaKodTarak.Kod collate DATABASE_DEFAULT = EREC_IratMetaDefinicio.UgyFajta collate DATABASE_DEFAULT 
	                                 and UgyFajtajaKodTarak.Org=@Org   			
                                   and exists( select 1 from Krt_KodCsoportok as UgyFajtajaKodCsoport where UgyFajtajaKodCsoport.Id = UgyFajtajaKodTarak.Kodcsoport_Id and UgyFajtajaKodCsoport.Kod = ''UGY_FAJTAJA'' )
								   /* !!! itt left join nem szűri a csoportot; inner join pedig eldobja azokat a sorokat, ahol EREC_UgyUgyiratok.IratMetadefinicio_Id szerint EREC_IratMetaDefinicio.UgyFajta is Null */
                              where 1 = 1
							  and EREC_UgyUgyiratok.Hatarido between @DatumTol AND @DatumIg
                            '
   if @Where is not null and @Where!=''
     SET @sqlcmd = @sqlcmd + ' and ' + @Where;
    if @CsakAktivIrat = 1
		BEGIN
			SET @sqlcmd +=[dbo].[fn_GetAktivFilterValue] ( 'UG')
			SET @sqlcmd += char(13);
		END   

   --- Nem az általános láthatóság alapján gyűjtjük az Ugyiratokat, elegendő a CsoportTagokAll szerinti szűrés  

   /************************************************************
   * Szûrt adatokhoz rendezés és sorszám összeállítása         *
   ************************************************************/
   	--- Fontos: az itt megfogalmazott kapcsolatoknak egyezniük kell a '* Tényleges select' során, az egyes adatmezők lekéréséhez felépített kapcsolatokkal  
      SET @sqlcmd = @sqlcmd + N'
      CREATE TABLE #result(RowNumber bigint, Id uniqueidentifier)
      insert into #result
      select row_number() over('+@OrderBy+') as RowNumber, EREC_UgyUgyiratok.Id
       from EREC_UgyUgyiratok as EREC_UgyUgyiratok
       left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek ON EREC_IraIktatokonyvek.Id = EREC_UgyUgyiratok.IraIktatokonyv_Id and EREC_IraIktatokonyvek.Org=@Org
       left join EREC_IraIrattariTetelek as EREC_IraIrattariTetelek ON EREC_IraIrattariTetelek.Id = EREC_UgyUgyiratok.IraIrattariTetel_Id
       left join EREC_AgazatiJelek as EREC_Agazatijelek ON EREC_Agazatijelek.Id = EREC_IraIrattariTetelek.AgazatiJel_Id
       left join KRT_Csoportok AS Csoportok_UgyintezoNev ON Csoportok_UgyintezoNev.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez
	   -- BUG_4697
	   left join KRT_Csoportok AS Csoportok_UgyiratHelye ON Csoportok_UgyiratHelye.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo	
	   left join EREC_IraIratok as EREC_IraIratok on EREC_IraIratok.Ugyirat_id = EREC_UgyUgyiratok.Id and EREC_IraIratok.Alszam = 1
	   left join KRT_Kodtarak as AllapotKodTarak on AllapotKodTarak.Kod collate DATABASE_DEFAULT = EREC_UgyUgyiratok.Allapot collate DATABASE_DEFAULT 
	         and AllapotKodTarak.Org=@Org   			
            inner join KRT_Kodcsoportok as AllapotKodCsoport on AllapotKodCsoport.Id = AllapotKodTarak.Kodcsoport_Id and AllapotKodCsoport.Kod = ''UGYIRAT_ALLAPOT''          
       left join EREC_IratMetaDefinicio on EREC_IratMetaDefinicio.Id = EREC_UgyUgyiratok.IratMetadefinicio_Id
            left join KRT_Kodtarak as UgyFajtajaKodTarak on UgyFajtajaKodTarak.Kod collate DATABASE_DEFAULT = EREC_IratMetaDefinicio.UgyFajta collate DATABASE_DEFAULT 
	              and UgyFajtajaKodTarak.Org=@Org   			
			      and exists( select 1 from Krt_KodCsoportok as UgyFajtajaKodCsoport where UgyFajtajaKodCsoport.Id = UgyFajtajaKodTarak.Kodcsoport_Id and UgyFajtajaKodCsoport.Kod = ''UGY_FAJTAJA'' )
      where EREC_UgyUgyiratok.Id in (select Id from #filter); 
	  '
	  
	--- lekérési sorszámhatárok beállítása  
      SET @sqlcmd = @sqlcmd + N'
          Set @firstRow = 1	  
          set @lastRow = ( select max(RowNumber) from #result ); 
      '

	  DECLARE @Iktatas_ugytipus_iratmetadefbol varchar(400)
	  SET @Iktatas_ugytipus_iratmetadefbol=(select Top 1 Ertek from KRT_Parameterek where Nev='IKTATAS_UGYTIPUS_IRATMETADEFBOL'
         and Org=@Org and getdate() between ErvKezd and ErvVege)
      
   /************************************************************
   * Tényleges select                                 *
   ************************************************************/
     set @sqlcmd = @sqlcmd + N'
     select 
      #result.RowNumber,
      #result.Id,
	  EREC_IraIktatokonyvek.Ev,
	  case when isnull(EREC_IraIktatokonyvek.KozpontiIktatasJelzo,0) = 1 then EREC_IraIktatokonyvek.MegkulJelzes
           else EREC_IraIktatokonyvek.Iktatohely
       end as Eloszam,                                                                             --- "felelős szervezeti egység kódja"         --- fn_NergeFoszam alapján
      EREC_UgyUgyiratok.Foszam,
      --- EREC_UgyUgyiratok.Sorszam,
      --- EREC_UgyUgyiratok.Azonosito,
	  dbo.fn_GetCsoportNev(EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo) as Ugyirat_helye,
	  dbo.fn_GetCsoportNev(EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez) as Ugyirat_ugyintezo_neve,
	  EREC_UgyUgyiratok.Targy,
	  EREC_UgyUgyiratok.Hatarido as Hatarido, 
	  EREC_UgyUgyiratok.ElteltIdo, 
	  Ugy_fajtaja = 
		(case when @Iktatas_ugytipus_iratmetadefbol = 1 then  [dbo].fn_KodtarErtekNeve(''UGY_FAJTAJA'',  EREC_IratMetaDefinicio.UgyFajta, @Org)
		 else [dbo].fn_KodtarErtekNeve(''UGY_FAJTAJA'',  EREC_UgyUgyiratok.Ugy_Fajtaja , @Org)
	     END),
	  case when (EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos is not null and 
	          (select KRT_Csoportok.Tipus from KRT_Csoportok where KRT_Csoportok.ID = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez ) = ''1'' -- azaz Dolgozó
	         and (exists(select 1 from KRT_CsoportTagok cst where cst.Csoport_id = EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos and cst.Csoport_Id_Jogalany = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez and getdate() between cst.ErvKezd and cst.ErvVege  )) )       
	       then 1
           else 0
       end as Szignalt_ugyirat,
	   [dbo].fn_Get_UgyiratTargySzava(EREC_UgyUgyiratok.ID, ''FT1'', @Org ) as Ugyirat_statisztikaT1,
	   [dbo].fn_Get_UgyiratTargySzava(EREC_UgyUgyiratok.ID, ''FT2'', @Org ) as Ugyirat_statisztikaT2,
	  [dbo].fn_Get_UgyiratTargySzava(EREC_UgyUgyiratok.ID, ''FT3'', @Org ) as Ugyirat_statisztikaT3
	  --- EREC_IraIrattariTetelek.IrattariTetelszam
	  '

     set @sqlcmd = @sqlcmd + N'   
      from EREC_UgyUgyiratok as EREC_UgyUgyiratok
     inner join #result on #result.Id = EREC_UgyUgyiratok.Id
       left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek ON EREC_IraIktatokonyvek.Id = EREC_UgyUgyiratok.IraIktatokonyv_Id and EREC_IraIktatokonyvek.Org=@Org
       left join EREC_IraIrattariTetelek as EREC_IraIrattariTetelek ON EREC_IraIrattariTetelek.Id = EREC_UgyUgyiratok.IraIrattariTetel_Id
       left join EREC_AgazatiJelek as EREC_Agazatijelek ON EREC_Agazatijelek.Id = EREC_IraIrattariTetelek.AgazatiJel_Id
       left join KRT_Csoportok AS Csoportok_UgyintezoNev ON Csoportok_UgyintezoNev.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez
	   left join EREC_IraIratok as EREC_IraIratok on EREC_IraIratok.Ugyirat_id = EREC_UgyUgyiratok.Id and EREC_IraIratok.Alszam = 1
       left join EREC_IratMetaDefinicio on EREC_IratMetaDefinicio.Id = EREC_UgyUgyiratok.IratMetadefinicio_Id
     '
      
   SET @sqlcmd = @sqlcmd + N'
    where RowNumber between @firstRow and @lastRow
    order by #result.RowNumber;'


	/* --- teszteléshez 
     set @sqlcmd = @sqlcmd + N'
     select EREC_UgyUgyiratok.azonosito, ( select count(1) from EREC_IraIratok where EREC_IraIratok.Ugyirat_ID = EREC_UgyUgyiratok.ID ) as IktatoCount
      from EREC_UgyUgyiratok as EREC_UgyUgyiratok
     inner join #result on #result.Id = EREC_UgyUgyiratok.Id
       left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek ON EREC_IraIktatokonyvek.Id = EREC_UgyUgyiratok.IraIktatokonyv_Id and EREC_IraIktatokonyvek.Org=@Org
       left join EREC_IraIrattariTetelek as EREC_IraIrattariTetelek ON EREC_IraIrattariTetelek.Id = EREC_UgyUgyiratok.IraIrattariTetel_Id
       left join EREC_AgazatiJelek as EREC_Agazatijelek ON EREC_Agazatijelek.Id = EREC_IraIrattariTetelek.AgazatiJel_Id
       left join KRT_Csoportok AS Csoportok_UgyintezoNev ON Csoportok_UgyintezoNev.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez
	   left join EREC_IraIratok as EREC_IraIratok on EREC_IraIratok.Ugyirat_id = EREC_UgyUgyiratok.Id and EREC_IraIratok.Alszam = 1
       left join EREC_IratMetaDefinicio on EREC_IratMetaDefinicio.Id = EREC_UgyUgyiratok.IratMetadefinicio_Id
    where RowNumber between @firstRow and @lastRow	
    order by 2 desc;'
	 --- */

   /***********************************************************
    * Kapcsolódó IraIratok select                             *
    ***********************************************************/
      set @sqlcmd = @sqlcmd + N'
      select EREC_UgyUgyiratok.ID as Ugyirat_ID,
	         EREC_IraIratok.ID,
	         EREC_IraIratok.Alszam,
			 EREC_IraIratok.PostazasIranya as Irat_iranya,
			 [dbo].fn_KodtarErtekNeve(''POSTAZAS_IRANYA'',  EREC_IraIratok.PostazasIranya , @Org) Irat_iranya_nev,
			 [dbo].fn_KodtarErtekNeve(''IRATTIPUS'',  EREC_IraIratok.IratTipus, @Org) as Irat_tipusa,	
             [dbo].fn_Get_IratTargySzava(EREC_IraIratok.ID, ''IT1'', @Org ) as Irat_statisztikaT1,
			 [dbo].fn_Get_IratTargySzava(EREC_IraIratok.ID, ''IT2'', @Org ) as Irat_statisztikaT2,
			 [dbo].fn_Get_IratTargySzava(EREC_IraIratok.ID, ''IT3'', @Org ) as Irat_statisztikaT3,
			 EREC_KuldKuldemenyek.BeerkezesIdeje as Kuldemeny_Beerkezes_datuma, 
			 EREC_IraIratok.IktatasDatuma as Irat_Iktatas_datuma, 
			 (select min(EREC_PldIratPeldanyok.PostazasDatuma) from EREC_PldIratPeldanyok where EREC_PldIratPeldanyok.IraIrat_ID = Erec_IraIratok.Id ) as Postazas_datuma, 
			 --[dbo].fn_KodtarErtekNeve(''IRAT_HATASA_UGYINTEZESRE'',  EREC_IraIratok.IratHatasaUgyintezesre, @Org) as Irat_hatasa_sakkorara,
			 EREC_IraIratok.IratHatasaUgyintezesre as Irat_hatasa_sakkorara,
			 [dbo].fn_Get_IratTargySzava(EREC_IraIratok.ID, ''Jogerősítendő'', @Org ) as Irat_jogerositendo,
			 [dbo].fn_Get_IratTargySzava(EREC_IraIratok.ID, ''Jogerőre emelkedés dátuma'', @Org ) as Jogerore_emelkedes_datuma,
             ( select count(1) from EREC_KuldTertivevenyek 
			    inner join EREC_Kuldemeny_IratPeldanyai on EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_ID = EREC_KuldTertivevenyek.Kuldemeny_ID
				inner join EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.ID = EREC_Kuldemeny_IratPeldanyai.Peldany_ID and EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id
   			    where EREC_KuldTertivevenyek.TertivisszaDat is Null ) as Tertiveveny_NemJottVisszaDB,
             ( select count(1) from EREC_KuldTertivevenyek 
			    inner join EREC_Kuldemeny_IratPeldanyai on EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_ID = EREC_KuldTertivevenyek.Kuldemeny_ID
				inner join EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.ID = EREC_Kuldemeny_IratPeldanyai.Peldany_ID and EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id
				where EREC_KuldTertivevenyek.TertivisszaDat is NOT Null ) as Tertiveveny_VisszaJottDB,
	         ( select max(EREC_KuldTertivevenyek.TertivisszaDat) from EREC_KuldTertivevenyek
			    inner join EREC_Kuldemeny_IratPeldanyai on EREC_Kuldemeny_IratPeldanyai.KuldKuldemeny_ID = EREC_KuldTertivevenyek.Kuldemeny_ID
				inner join EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.ID = EREC_Kuldemeny_IratPeldanyai.Peldany_ID and EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id
			    where EREC_KuldTertivevenyek.TertivisszaDat is NOT Null ) as Tertiveveny_UtolsoVisszaDatum			 
        from EREC_IraIratok as EREC_IraIratok
	   inner join EREC_UgyUgyiratok on EREC_UgyUgyiratok.ID = EREC_IraIratok.Ugyirat_Id
       inner join #result on #result.Id = EREC_UgyUgyiratok.ID
       left join EREC_IraIktatokonyvek on EREC_IraIktatokonyvek.Id = EREC_UgyUgyiratok.IraIktatokonyv_Id and EREC_IraIktatokonyvek.Org=@Org	   
	   left join EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.ID = EREC_IraIratok.KuldKuldemenyek_Id
	   order by EREC_IraIktatokonyvek.Ev, EREC_UgyUgyiratok.Foszam, EREC_IraIratok.Alszam
	  ' 

/* --- teszteléshez
print LEFT(@sqlcmd,4000)
print substring(@sqlcmd, 4001,4000)
print substring(@sqlcmd, 8001,4000)
print substring(@sqlcmd, 12001,4000)
print substring(@sqlcmd, 16001,4000)
print substring(@sqlcmd, 20001,4000)
print substring(@sqlcmd, 24001,4000)
print substring(@sqlcmd, 28001,4000)
print substring(@sqlcmd, 32001,4000)
print substring(@sqlcmd, 36001,4000)
print substring(@sqlcmd, 40001,4000)
print substring(@sqlcmd, 44001,4000)
--- */
  
/* -- találatok száma és oldalszám
set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter;';
*/

	  execute sp_executesql @sqlcmd,N'@DatumTol datetime, @DatumIg datetime, @ExecutorUserId uniqueidentifier, @Org uniqueidentifier, @firstRow int, @lastRow int, @Iktatas_ugytipus_iratmetadefbol varchar(400), @UgyintezoId uniqueidentifier'
                                     ,@Datumtol = @DatumTol, @DatumIg = @DatumIg, @ExecutorUserId = @ExecutorUserId, @Org = @Org, @firstRow = @firstRow, @lastRow = @lastRow, @Iktatas_ugytipus_iratmetadefbol=@Iktatas_ugytipus_iratmetadefbol, @UgyintezoId = @UgyintezoId
                            ;

END TRY

BEGIN CATCH
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()

   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

