﻿CREATE procedure [dbo].[sp_EREC_UgyUgyiratokForElszamoltatasiJkSSRS]

  @Where			nvarchar(MAX) = '',
  @OrderBy			nvarchar(400) = ' order by Foszam_Merge',
  @ExecutorUserId	nvarchar(400),
  @TopRow NVARCHAR(5) = '',
  @Alszam bit = 0

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  IF @Alszam = 0
  BEGIN

  SET @sqlcmd = 
  '
		SELECT ROW_NUMBER() OVER(ORDER BY Id) AS RowNumber, Id, COUNT(Id) AS Alszamok_Szama INTO #temp FROM 
		(
			SELECT ' + @LocalTopRow + ' EREC_UgyUgyiratok.Id  FROM EREC_UgyUgyiratok
				LEFT JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
				left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek ON EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id
			Where EREC_IraIktatokonyvek.Org=''' + cast(@Org as NVarChar(40)) + '''
 '
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
	SET @sqlcmd = @sqlcmd + @OrderBy + '
		) AS temp
		GROUP BY Id
		ORDER BY Alszamok_Szama DESC

		--DELETE FROM #temp WHERE RowNumber = (SELECT MAX(RowNumber) FROM #temp) AND RowNumber != 1
		
  --select (
		select EREC_UgyUgyiratok.Azonosito as Foszam_Merge,
			   CONVERT(nvarchar(10), EREC_UgyUgyiratok.LetrehozasIdo, 102) as LetrehozasIdo_Rovid,
			   EREC_UgyUgyiratok.Targy,
			   (select UgytipusNev from EREC_IratMetaDefinicio where EREC_UgyUgyiratok.IratMetadefinicio_Id = EREC_IratMetaDefinicio.Id) as UgyTipus_Nev,
			   Ugyirat_helye = 
			   CASE 
					WHEN EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''06'',''11'',''13'',''52'',''55'',''56'',''57'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''13'',''52'',''54'',''55'',''56'',''99''))
						THEN ''Ügyintézőnél''
					ELSE 
						CASE WHEN EREC_Ugyugyiratok.Allapot = ''07'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''07'')
							THEN ''Skontróban''
						END
			   END,
			   '''' as Alszam,
			   '''' as IktatoSzam_Merge,
			   '''' as Erkezett,
			   '''' as Targy1,
			   '''' as Irat_helye,
			   dbo.fn_KodtarErtekNeve(''UGYIRAT_ALLAPOT'', EREC_UgyUgyiratok.Allapot,''' + cast(@Org as NVarChar(40)) + ''') as Allapot_Nev
		from EREC_UgyUgyiratok
			inner join #temp ON #temp.Id = EREC_UgyUgyiratok.Id
			LEFT JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
			left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek ON EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id
			 '
 END
 
 IF @Alszam = 1
  BEGIN

  SET @sqlcmd = 
  '
		SELECT ROW_NUMBER() OVER(ORDER BY Id) AS RowNumber, Id, COUNT(Id) AS Alszamok_Szama INTO #temp FROM 
		(
			SELECT ' + @LocalTopRow + ' EREC_UgyUgyiratok.Id  FROM EREC_UgyUgyiratok
				LEFT JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
				LEFT JOIN EREC_IraIratok ON EREC_IraIratok.UgyUgyIratDarab_Id = EREC_UgyUgyiratdarabok.Id
				left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek ON EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id
			Where EREC_IraIktatokonyvek.Org=''' + cast(@Org as NVarChar(40)) + '''
 '
   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
	SET @sqlcmd = @sqlcmd + @OrderBy + '
		) AS temp
		GROUP BY Id
		ORDER BY Alszamok_Szama DESC

		--DELETE FROM #temp WHERE RowNumber = (SELECT MAX(RowNumber) FROM #temp) AND RowNumber != 1
		
  --select (
		select EREC_UgyUgyiratok.Azonosito as Foszam_Merge,
			   CONVERT(nvarchar(10), EREC_UgyUgyiratok.LetrehozasIdo, 102) as LetrehozasIdo_Rovid,
			   EREC_UgyUgyiratok.Targy,
			   (select UgytipusNev from EREC_IratMetaDefinicio where EREC_UgyUgyiratok.IratMetadefinicio_Id = EREC_IratMetaDefinicio.Id) as UgyTipus_Nev,
			   Ugyirat_helye = 
			   CASE 
					WHEN EREC_Ugyugyiratok.Allapot in (''0'',''03'',''04'',''05'',''06'',''11'',''13'',''52'',''55'',''56'',''57'',''99'') or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in (''0'',''03'',''04'',''06'',''13'',''52'',''54'',''55'',''56'',''99''))
						THEN ''Ügyintézőnél''
					ELSE 
						CASE WHEN EREC_Ugyugyiratok.Allapot = ''07'' or (EREC_Ugyugyiratok.Allapot=''50'' and EREC_Ugyugyiratok.TovabbitasAlattAllapot = ''07'')
							THEN ''Skontróban''
						END
			   END,
			   EREC_IraIratok.Alszam,
			   dbo.fn_MergeIktatoszam(EREC_IraIktatokonyvek.MegkulJelzes,EREC_IraIktatokonyvek.Iktatohely,EREC_IraIktatokonyvek.KozpontiIktatasJelzo,EREC_IraIktatokonyvek.Ev
				   ,EREC_UgyUgyiratdarabok.Foszam,EREC_IraIratok.Alszam,null) as IktatoSzam_Merge,
			   CONVERT(nvarchar(10), EREC_IraIratok.IktatasDatuma, 102) as Erkezett,
			   EREC_IraIratok.Targy as Targy1,
			   Irat_helye = 
			   CASE 
					WHEN EREC_IraIratok.Allapot in (''0'',''03'',''04'',''05'',''06'',''13'',''30'',''52'',''55'',''56'',''90'',''99'') or (EREC_IraIratok.Allapot=''50'')
					THEN ''Ügyintézőnél''
					ELSE 
						CASE WHEN EREC_IraIratok.Allapot = ''07'' or (EREC_IraIratok.Allapot=''50'' )
							THEN ''Skontróban''
						END
			   END,
			   dbo.fn_KodtarErtekNeve(''IRAT_ALLAPOT'', EREC_IraIratok.Allapot,''' + cast(@Org as NVarChar(40)) + ''') as Allapot_Nev
		from EREC_UgyUgyiratok
			inner join #temp ON #temp.Id = EREC_UgyUgyiratok.Id
			LEFT JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
			LEFT JOIN EREC_IraIratok ON EREC_IraIratok.UgyUgyIratDarab_Id = EREC_UgyUgyiratdarabok.Id
			left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek ON EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id
			LEFT JOIN EREC_KuldKuldemenyek AS EREC_KuldKuldemenyek ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id
			LEFT JOIN EREC_PldIratPeldanyok AS EREC_PldIratPeldanyok ON EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id and EREC_PldIratPeldanyok.Sorszam = ''1''
			 '
 END
    
 SET @sqlcmd = @sqlcmd + @OrderBy + '
 --FOR XML AUTO, ELEMENTS, ROOT(''NewDataSet'')) as string_xml 
 drop table #temp;'

 

 exec(@sqlcmd)


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

