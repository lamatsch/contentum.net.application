﻿	set ANSI_NULLS ON
	set QUOTED_IDENTIFIER ON
		go
			if exists (select 1 from  sysobjects
				   where  id = object_id('sp_EREC_UgyUgyiratokForElszamoltatasiJkSSRS_Joined')
					 and  type in ('P'))
		   drop procedure sp_EREC_UgyUgyiratokForElszamoltatasiJkSSRS_Joined
		go

CREATE procedure [dbo].[sp_EREC_UgyUgyiratokForElszamoltatasiJkSSRS_Joined]

  @Where_EREC		nvarchar(MAX) = '',
  @Where_MIG			nvarchar(MAX) = '',
  @OrderBy			nvarchar(400) = ' order by Foszam_Merge',
  @ExecutorUserId	nvarchar(400),
  @TopRow NVARCHAR(5) = '',
  @Alszam bit = 0

as

begin
declare @joined table ( Foszam_Merge nvarchar(max), LetrehozasIdo_rovid nvarchar(max), Targy nvarchar(max), UgyTipus_Nev nvarchar(max), Ugyirat_helye nvarchar(max), Alszam nvarchar(max), IktatoSzam_Merge nvarchar(max), Erkezett nvarchar(max), Targy1 nvarchar(max), Irat_helye nvarchar(max), Allapot_Nev nvarchar(max) )

--eRecord
IF NULLIF(@Where_EREC, '') IS NOT NULL
BEGIN
insert into @joined exec sp_EREC_UgyUgyiratokForElszamoltatasiJkSSRS
@Where=@Where_EREC,
@OrderBy=@OrderBy,
@TopRow=@TopRow,
@ExecutorUserId=@ExecutorUserId,
@Alszam = @Alszam
END

--eMigration
IF NULLIF(@Where_MIG, '') IS NOT NULL
BEGIN
insert into @joined exec [$(MIGRDB)].[dbo].[sp_MIG_UgyUgyiratokForElszamoltatasiJkSSRS]
@Where=@Where_MIG,
@OrderBy=@OrderBy,
@TopRow=@TopRow,
@ExecutorUserId=@ExecutorUserId,
@Alszam = @Alszam
END

select * from @joined

end

