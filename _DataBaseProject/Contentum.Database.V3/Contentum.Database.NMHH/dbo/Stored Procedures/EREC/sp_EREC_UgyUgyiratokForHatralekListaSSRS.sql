﻿set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
if exists (select 1 from  sysobjects
		where  id = object_id('sp_EREC_UgyUgyiratokForHatralekListaSSRS')
			and  type in ('P'))
drop procedure sp_EREC_UgyUgyiratokForHatralekListaSSRS
GO

CREATE procedure [dbo].[sp_EREC_UgyUgyiratokForHatralekListaSSRS]
  @Where			nvarchar(MAX) = '',
  @OrderBy			nvarchar(400) = ' order by Foszam_Merge, EREC_IraIratok.Alszam',
  @ExecutorUserId	uniqueidentifier,
  @FelhasznaloSzervezet_Id	uniqueidentifier,
  @TopRow NVARCHAR(5) = ''

as

begin

BEGIN TRY

   set nocount on
   
	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  'declare @TovabbitasAlattNev nvarchar(64)
set @TovabbitasAlattNev=(select Nev from KRT_KodTarak kt
	where KodCsoport_Id=(select top 1 kcs.Id from KRT_KodCsoportok kcs
			where kcs.Kod=''UGYIRAT_ALLAPOT'' and getdate() between kcs.Ervkezd and kcs.ErvVege)
			and Kod = ''50''
			and Org=@Org
			and getdate() between kt.Ervkezd and kt.ErvVege)
'

  SET @sqlcmd = @sqlcmd + 
  '
		--select (
		select ' + @LocalTopRow + '
		Foszam_Merge = 
			   CASE dbo.fn_MergeFoszam(EREC_IraIktatokonyvek.MegkulJelzes,EREC_IraIktatokonyvek.Iktatohely,EREC_IraIktatokonyvek.KozpontiIktatasJelzo,EREC_IraIktatokonyvek.Ev,EREC_UgyUgyiratok.Foszam)
					WHEN '' /?? / '' 
					THEN NULL
					ELSE dbo.fn_MergeFoszam(EREC_IraIktatokonyvek.MegkulJelzes,EREC_IraIktatokonyvek.Iktatohely,EREC_IraIktatokonyvek.KozpontiIktatasJelzo,EREC_IraIktatokonyvek.Ev,EREC_UgyUgyiratok.Foszam)
			   END,
			   CONVERT(nvarchar(10), EREC_UgyUgyiratok.LetrehozasIdo, 102) as LetrehozasIdo_Rovid,
			   EREC_UgyUgyiratok.NevSTR_Ugyindito,
			   EREC_UgyUgyiratok.CimSTR_Ugyindito,
			   EREC_UgyUgyiratok.Targy,
			   dbo.fn_GetCsoportNev(EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez) as KRT_Csoportok_Orzo_Nev,
			   PotlistaOrzoNev = case 
					when PotlistaUgyiratHelye.Id is not null then dbo.fn_GetCsoportNev(EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo)
					else null
				end,
				--EREC_UgyUgyiratok.IratMetadefinicio_Id,
				--(select UgytipusNev from EREC_IratMetaDefinicio where EREC_IratMetaDefinicio.Id = EREC_UgyUgyiratok.IratMetadefinicio_Id) as UgyTipus_Nev,
				(select Nev from KRT_KodTarak WHERE KRT_Kodtarak.KodCsoport_Id = (select Id from KRT_KodCsoportok 
									where Kod = ''UGY_FAJTAJA'') 
									AND KRT_KodTarak.Kod = EREC_UgyUgyiratok.Ugy_Fajtaja collate Hungarian_CI_AS
									AND getdate() BETWEEN KRT_KodTarak.ErvKezd AND KRT_KodTarak.ErvVege) as UgyTipus_Nev,
			   Ugyirat_helye = case EREC_UgyUgyiratok.Allapot
					when ''50'' then @TovabbitasAlattNev + '' ('' + ktAllapot.Nev + '')''
					else ktAllapot.Nev
				end,   
			   KRT_Felhasznalok.Nev		   
		from KRT_Felhasznalok
			inner join EREC_UgyUgyiratok on EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez = KRT_Felhasznalok.Id
			inner join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek ON EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id and EREC_IraIktatokonyvek.Org=@Org
left join (select Kod, Nev from KRT_KodTarak kt
	where KodCsoport_Id=(select top 1 kcs.Id from KRT_KodCsoportok kcs
			where kcs.Kod=''UGYIRAT_ALLAPOT'' and getdate() between kcs.Ervkezd and kcs.ErvVege)
			and Kod <> ''50''
			and Org=@Org
			and getdate() between kt.Ervkezd and kt.ErvVege) ktAllapot
	on (ktAllapot.Kod=case when EREC_UgyUgyiratok.Allapot = ''50''
			then EREC_UgyUgyiratok.TovabbitasAlattAllapot
			else EREC_UgyUgyiratok.Allapot
		end
		)
outer apply (SELECT cs.Id as Id
                  , cs.Nev as Nev
         FROM KRT_csoportok cs
            where cs.Id=EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez and cs.Tipus = ''1''
				and not exists(select 1 from KRT_CsoportTagok cst where cst.Csoport_Id_Jogalany=cs.Id
				and getdate() between cst.ErvKezd and cst.ErvVege)) PotlistaUgyiratHelye
'
  SET @sqlcmd = @sqlcmd + '
where EREC_UgyUgyiratok.Id IN 
(
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
			ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
			ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
	UNION ALL
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll 
			ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
	UNION ALL
	SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'')
	-- BUG_13539 Csak olyan ügyiratok jelenjenek meg, amelyekben az utolsó irat (is), több, mint 30 napja van az ügyintézőnél
	UNION ALL
	SELECT DISTINCT(EREC_IraIratok.Ugyirat_Id) from EREC_UgyUgyiratok
		LEFT JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
		LEFT JOIN EREC_IraIratok ON EREC_IraIratok.UgyUgyIratDarab_Id = EREC_UgyUgyiratdarabok.Id
		
		WHERE GETDATE() BETWEEN EREC_IraIratok.ErvKezd AND EREC_IraIratok.ErvVege and EREC_IraIratok.LetrehozasIdo < DATEADD(DAY, -30, getdate())
		and ' + @Where + '
	-- BUG_13539 End
) 
'
	
if dbo.fn_IsLeaderInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
		begin
			if dbo.fn_HasFunctionRight(@ExecutorUserId,@FelhasznaloSzervezet_Id, 'IrattarKolcsonzesKiadasa') = 0
				and dbo.fn_HasFunctionRight(@ExecutorUserId,@FelhasznaloSzervezet_Id, 'AtmenetiIrattarAtadasKozpontiIrattarba') = 0
			begin
				-- Irattár szurés, In és nem NOT IN!
				-- Irattározott állapotok: Irattárba küldött, Irattárban orzött, Irattárból elkért, Átmeneti irattárból kikölcsönzött, Engedélyezett kikéron lévo
				SET @sqlcmd = @sqlcmd + '
	and EREC_UgyUgyiratok.Id NOT IN 
	(
		select Id from EREC_UgyUgyiratok where
		(Allapot in (''11'', ''10'', ''55'', ''54'', ''56'') or TovabbitasAlattAllapot in (''11'', ''10'', ''55'', ''54'', ''56''))
		and Csoport_Id_Felelos in (select Id from KRT_Csoportok where IsNull(JogosultsagOroklesMod, ''0'') = ''0'')
		and EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez != @ExecutorUserId
		and EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId
		and EREC_UgyUgyiratok.Id not in
		(
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			UNION ALL
			SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
				INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
				INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			UNION ALL
			SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'')
		)
	)
 '
			end
		end

   if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
	SET @sqlcmd = @sqlcmd + @OrderBy + '
 --FOR XML AUTO, ELEMENTS, ROOT(''NewDataSet'')) as string_xml '

--   exec(@sqlcmd)
		execute sp_executesql @sqlcmd,N'@ExecutorUserId uniqueidentifier, @FelhasznaloSzervezet_Id uniqueidentifier, @Org uniqueidentifier'
		, @ExecutorUserId = @ExecutorUserId, @FelhasznaloSzervezet_Id = @FelhasznaloSzervezet_Id, @Org = @Org;


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

