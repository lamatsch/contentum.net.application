﻿CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratokForLeaders]
    @Where NVARCHAR(4000) = '',
    @OrderBy NVARCHAR(200) = '',
    @TopRow NVARCHAR(5) = '',
    @ExecutorUserId UNIQUEIDENTIFIER,
    @FelhasznaloSzervezet_Id UNIQUEIDENTIFIER,
	@Jogosultak		char(1) = '1',	-- for compatibility
	@KezdDat datetime,
	@VegeDat datetime,
	@Option_VezetoiPanel bit = 0,	-- no relevance at the moment, just for compatibility
	@Precision NVARCHAR(2) = '1',	-- round to digits (not used at the moment, fixed in code below!)
	@AlSzervezetId UNIQUEIDENTIFIER = null
AS 
    BEGIN
        BEGIN TRY

            SET nocount ON

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

			if not exists(select Id from KRT_CsoportTagok
				where Csoport_Id_jogalany = @ExecutorUserId
				and Csoport_Id = @FelhasznaloSzervezet_Id
				and getdate() between ErvKezd and ErvVege
			)
			begin
				RAISERROR('[52001]',16,1) -- A felhasználó nem tagja a csoportnak!
			end
	
	Declare @LeaderId uniqueidentifier
	Declare @SzervezetId uniqueidentifier
	
	if @AlSzervezetId is null
	begin
		Set @LeaderId = @ExecutorUserId
		Set @SzervezetId = @FelhasznaloSzervezet_Id
	end
	else
	begin
		Set @LeaderId = dbo.fn_GetLeader(@AlSzervezetId)
		
		IF @LeaderId is NULL
		BEGIN
			-- Az alszervezetnek nincsen vezetoje
			RAISERROR('[53203]',16,1)
		END
		
		Set @SzervezetId = @AlSzervezetId
	end
	
	declare @sqlcmd nvarchar(MAX)
	set @sqlcmd = ''


	set @sqlcmd = @sqlcmd + '	declare @KezdDat datetime
	declare @VegeDat datetime

	set @KezdDat = ''' + convert(nvarchar, @KezdDat, 120) + ''' 
	set @VegeDat = ''' + convert(nvarchar, @VegeDat, 120) + '''
'
-- variables to hold the date part of the dates
set @sqlcmd = @sqlcmd + '
		declare @VegeDatDatePart datetime;
		SET @VegeDatDatePart = ''' + CONVERT(NVARCHAR(10), @VegeDat, 102) + '''
'

	set @sqlcmd = @sqlcmd + 'SELECT distinct Id INTO #temp_SzervezetTagok		
				FROM dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(''' + convert(nvarchar(36), @LeaderId) + ''', ''' + convert(nvarchar(36), @SzervezetId) + ''')
'
	declare @RightCheck char(1)
	IF @Jogosultak = '1' AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId, @FelhasznaloSzervezet_Id) = 0
	--IF @Jogosultak = '1' AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
	BEGIN
		SET @RightCheck = '1'
	END
	ELSE
	BEGIN
		SET @RightCheck = '0'
	END

	declare @OrgCheck char(1)
	-- ha nincs jogosultságszűrés, az alaptáblák szűrése
	declare @OrgFilter_Ugyirat nvarchar(1000)
	set @OrgFilter_Ugyirat = ''
	declare @OrgFilter_Kuldemeny nvarchar(1000)
	set @OrgFilter_Kuldemeny = ''
	declare @OrgFilter_Irat nvarchar(1000)
	set @OrgFilter_Irat = ''
	IF (select count(Id) from KRT_Orgok) > 1
	BEGIN
		SET @OrgCheck = '1'

		SET @OrgFilter_Ugyirat = ' and ''' + cast(@Org as NVarChar(40)) + '''=
(select EREC_IraIktatoKonyvek.Org from EREC_IraIktatoKonyvek
	where EREC_IraIktatoKonyvek.Id=EREC_UgyUgyiratok.IraIktatoKonyv_Id
)
'

		SET @OrgFilter_Kuldemeny = ' and (
	''' + cast(@Org as NVarChar(40)) + '''=
	(select EREC_IraIktatoKonyvek.Org from EREC_IraIktatoKonyvek
		where EREC_IraIktatoKonyvek.Id=EREC_KuldKuldemenyek.IraIktatoKonyv_Id)
	or
	(EREC_KuldKuldemenyek.IraIktatoKonyv_Id is null and ''' + cast(@Org as NVarChar(40)) + '''=
	(select KRT_Felhasznalok.Org from KRT_Felhasznalok
	where KRT_Felhasznalok.Id=EREC_KuldKuldemenyek.Letrehozo_id)
	)
)
'

		SET @OrgFilter_Irat = ' and ''' + cast(@Org as NVarChar(40)) + '''=
(select EREC_IraIktatoKonyvek.Org from EREC_IraIktatoKonyvek
	where EREC_IraIktatoKonyvek.Id=
	(select EREC_UgyUgyiratok.IraIktatoKonyv_Id from EREC_UgyUgyiratok
	where EREC_UgyUgyiratok.Id=EREC_IraIratok.Ugyirat_Id
	)
)
'
	END
	ELSE
	BEGIN
		SET @OrgCheck = '0'
	END

	IF @RightCheck = '1'
	BEGIN
-- ügyiratok szűrése láthatóság alapján
-- Figyelem: a többi szűrésnél is használjuk, ha pl. az érvényességen kívül egyéb szűkítés történne,
-- az iratnál, küldeménynél is felül kell vizsgálni
		set @sqlcmd = @sqlcmd + 'SELECT Id as Id INTO #temp_UgyiratFilter FROM
(
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
		INNER JOIN #temp_SzervezetTagok as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
		and getdate() between erec_ugyugyiratok.ErvKezd and erec_ugyugyiratok.ErvVege
	UNION
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
		INNER JOIN #temp_SzervezetTagok as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
		and getdate() between erec_ugyugyiratok.ErvKezd and erec_ugyugyiratok.ErvVege
	UNION
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN #temp_SzervezetTagok as CsoportTagokAll
			ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
		and getdate() between erec_ugyugyiratok.ErvKezd and erec_ugyugyiratok.ErvVege
	UNION
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN #temp_SzervezetTagok as CsoportTagokAll
			ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
		and getdate() between erec_ugyugyiratok.ErvKezd and erec_ugyugyiratok.ErvVege
	UNION
	SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
		INNER JOIN #temp_SzervezetTagok as CsoportTagokAll
			ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
		and getdate() between erec_ugyugyiratok.ErvKezd and erec_ugyugyiratok.ErvVege
	UNION
	SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@LeaderId AS CHAR(36)) + ''', ''' + CAST(@SzervezetId AS CHAR(36)) + ''', ''EREC_UgyUgyiratok'')
) as tmp
'

	IF @OrgCheck = 1
	BEGIN
		set @sqlcmd = @sqlcmd + 'DELETE from #temp_UgyiratFilter where Id not in (
	select EREC_UgyUgyiratok.Id from EREC_UgyUgyiratok
	inner join #temp_UgyiratFilter
	on EREC_UgyUgyiratok.Id=#temp_UgyiratFilter.Id
' + @OrgFilter_Ugyirat + ');
'
	END

-- küldemények szűrése a láthatóság alapján
-- Figyelem: mivel jelenleg csak az adott beérkezési idejűeket nézzük, itt már erre is előszűrünk
-- Figyelem: alapértelmezett szűrés: kimenőket nem nézzük (PostazasIranya not in ('2'))
		set @sqlcmd = @sqlcmd + 'SELECT Id as Id INTO #temp_KuldemenyFilter FROM
(
	SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
		INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.Id
		INNER JOIN #temp_SzervezetTagok as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
		where EREC_KuldKuldemenyek.BeerkezesIdeje between @KezdDat and @VegeDat
		and EREC_KuldKuldemenyek.PostazasIranya not in (''2'')
		and getdate() between EREC_KuldKuldemenyek.ErvKezd and EREC_KuldKuldemenyek.ErvVege
	UNION
	SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
		INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
		INNER JOIN #temp_SzervezetTagok as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
		where EREC_KuldKuldemenyek.BeerkezesIdeje between @KezdDat and @VegeDat
		and EREC_KuldKuldemenyek.PostazasIranya not in (''2'')
		and getdate() between EREC_KuldKuldemenyek.ErvKezd and EREC_KuldKuldemenyek.ErvVege
	UNION
	SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
		INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_KuldKuldemenyek.IraIratok_Id
		INNER JOIN #temp_SzervezetTagok as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
		where EREC_KuldKuldemenyek.BeerkezesIdeje between @KezdDat and @VegeDat
		and EREC_KuldKuldemenyek.PostazasIranya not in (''2'')
		and getdate() between EREC_KuldKuldemenyek.ErvKezd and EREC_KuldKuldemenyek.ErvVege
	UNION
	SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
		INNER JOIN #temp_SzervezetTagok as CsoportTagokAll
			ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.Csoport_Id_Felelos
		where EREC_KuldKuldemenyek.BeerkezesIdeje between @KezdDat and @VegeDat
		and EREC_KuldKuldemenyek.PostazasIranya not in (''2'')
		and getdate() between EREC_KuldKuldemenyek.ErvKezd and EREC_KuldKuldemenyek.ErvVege
	UNION
	SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
		INNER JOIN #temp_SzervezetTagok as CsoportTagokAll
			ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
		where EREC_KuldKuldemenyek.BeerkezesIdeje between @KezdDat and @VegeDat
		and EREC_KuldKuldemenyek.PostazasIranya not in (''2'')
		and getdate() between EREC_KuldKuldemenyek.ErvKezd and EREC_KuldKuldemenyek.ErvVege
	UNION
	SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
		INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_KuldKuldemenyek.IraIratok_Id
		WHERE EREC_KuldKuldemenyek.IraIratok_Id IS NOT NULL
			AND EREC_IraIratok.Ugyirat_Id IN 
		(select Id from #temp_UgyiratFilter)
		and EREC_KuldKuldemenyek.PostazasIranya not in (''2'')
		and getdate() between EREC_KuldKuldemenyek.ErvKezd and EREC_KuldKuldemenyek.ErvVege
	UNION
	SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@LeaderId AS CHAR(36)) + ''', ''' + CAST(@SzervezetId AS CHAR(36)) + ''', ''EREC_KuldKuldemenyek'')
)  as tmp
'

	IF @OrgCheck = 1
	BEGIN
		set @sqlcmd = @sqlcmd + 'DELETE from #temp_KuldemenyFilter where Id not in (
select EREC_KuldKuldemenyek.Id from EREC_KuldKuldemenyek
inner join #temp_KuldemenyFilter
on #temp_KuldemenyFilter.Id=EREC_KuldKuldemenyek.Id
' + @OrgFilter_Kuldemeny + ');
'
	END

-- iratok szűrése a láthatóság alapján
-- CR3135 -- nem csak a mai napiak kellenek ezért ez az előszűrés kiszedve
-- Figyelem: mivel jelenleg csak a maiakat kérjük le, itt már erre is előszűrünk
		set @sqlcmd = @sqlcmd + 'SELECT Id as Id INTO #temp_IratFilter FROM
(
	SELECT EREC_IraIratok.Id FROM EREC_IraIratok
		INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = EREC_IraIratok.Id
		INNER JOIN #temp_SzervezetTagok as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
		where EREC_IraIratok.LetrehozasIdo between @VegeDatDatePart and @VegeDat
		and getdate() between EREC_IraIratok.ErvKezd and EREC_IraIratok.ErvVege
	UNION
	SELECT EREC_IraIratok.Id FROM EREC_IraIratok
		WHERE EREC_IraIratok.Ugyirat_Id IN 
		(select Id from #temp_UgyiratFilter)
--		and EREC_IraIratok.LetrehozasIdo between @VegeDatDatePart and @VegeDat
		and getdate() between EREC_IraIratok.ErvKezd and EREC_IraIratok.ErvVege
	UNION --CR1716: lenézünk a példányba, CR 2728: aki láthatja a példányt az láthatja az iratot is
	SELECT EREC_IraIratok.Id FROM EREC_IraIratok
		INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = EREC_IraIratok.Id
		WHERE EREC_IraIratok.LetrehozasIdo between @VegeDatDatePart and @VegeDat
		and getdate() between EREC_IraIratok.ErvKezd and EREC_IraIratok.ErvVege
		AND EREC_PldIratpeldanyok.Id IN
		(
			SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
				INNER JOIN KRT_Jogosultak ON KRT_Jogosultak.Obj_Id = EREC_PldIratPeldanyok.Id
				INNER JOIN #temp_SzervezetTagok as CsoportTagokAll 
				ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
			UNION ALL
			SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
				INNER JOIN #temp_SzervezetTagok as CsoportTagokAll 
				ON CsoportTagokAll.Id = EREC_PldIratpeldanyok.Csoport_Id_Felelos
			UNION ALL
			SELECT EREC_PldIratPeldanyok.Id FROM EREC_PldIratPeldanyok
				INNER JOIN #temp_SzervezetTagok as CsoportTagokAll 
				ON CsoportTagokAll.Id = EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo
			UNION ALL
				SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@LeaderId AS CHAR(36)) + ''', ''' + CAST(@SzervezetId AS CHAR(36)) + ''', ''EREC_PldIratPeldanyok'')
		)
	UNION
	SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(''' + CAST(@LeaderId AS CHAR(36)) + ''', ''' + CAST(@SzervezetId AS CHAR(36)) + ''', ''EREC_IraIratok'')
) as tmp
'

	IF @OrgCheck = 1
	BEGIN
		set @sqlcmd = @sqlcmd + 'DELETE from #temp_IratFilter where Id not in (
select EREC_IraIratok.Id from EREC_IraIratok
inner join #temp_IratFilter
on #temp_IratFilter.Id=EREC_IraIratok.Id
' + @OrgFilter_Irat + ');
'
	END

END


-- a Naponta atlagosan erkezett kuldemenyek szamanak szamolasahoz
-- csak a munkanapok figyelembevetelevel
set @sqlcmd = @sqlcmd + '
declare @munkanapok_szama int
'

-- EB 2008.12.16: erre a WorkAroundra már nincs szükség,
-- régen nem volt feltölve a KRT_ExtraNapok tábla
---- ha a @KezdDat és @VegeDat különböző évekre esik, csak az aktuális (@VegeDat szerinti)
---- év elejéig számoljuk a munkanapokat
--declare @MunkanapokKezdDat datetime
--
--if datepart(yyyy, @KezdDat) < datepart(yyyy, @VegeDat)
--begin
--	set @MunkanapokKezdDat = convert(datetime, convert(varchar(4), datepart(yyyy, @VegeDat)))
--end
--else
--begin
--	set @MunkanapokKezdDat = @KezdDat
--end
--
--set @sqlcmd = @sqlcmd + '
--set @munkanapok_szama = dbo.fn_munkanapok_szama(''' + convert(nvarchar, @MunkanapokKezdDat, 102) + ''', ''' + convert(nvarchar, @VegeDat, 102) + ''')
--'

set @sqlcmd = @sqlcmd + '
set @munkanapok_szama = dbo.fn_munkanapok_szama(''' + convert(nvarchar, @KezdDat, 102) + ''', ''' + convert(nvarchar, @VegeDat, 102) + ''')
'

-- variables to hold the states
set @sqlcmd = @sqlcmd + '
	DECLARE @allapot_iktatott nvarchar(64)
	DECLARE @allapot_szignalt nvarchar(64)
	DECLARE @allapot_ugyintezes_alatt nvarchar(64)
	DECLARE @allapot_skontroban nvarchar(64)
	DECLARE @allapot_elintezett nvarchar(64)
	DECLARE @allapot_adacta nvarchar(64)
	DECLARE @allapot_irattarba_kuldott nvarchar(64)
	DECLARE @allapot_kolcsonzott nvarchar(64)
	DECLARE @allapot_atmeneti_irattarbol_kolcsonzott nvarchar(64)
	DECLARE @allapot_tovabbitas_alatt nvarchar(64)

	SET @allapot_iktatott = ''04''
	SET @allapot_szignalt = ''03''
	SET @allapot_ugyintezes_alatt = ''06''
	SET @allapot_skontroban = ''07''
	SET @allapot_elintezett = ''99''
	SET @allapot_adacta = ''09''
	SET @allapot_irattarba_kuldott = ''11''
	SET @allapot_kolcsonzott = ''13''
	SET @allapot_atmeneti_irattarbol_kolcsonzott = ''54''
	SET @allapot_tovabbitas_alatt = ''50''
'
	-- iktatásra előkészítettek szűréséhez
set @sqlcmd = @sqlcmd + '
	DECLARE @allapot_ugyirat_iktatasra_elokeszitett nvarchar(64)
	DECLARE @allapot_irat_iktatasra_elokeszitett nvarchar(64)
    SET @allapot_ugyirat_iktatasra_elokeszitett = ''0''
	SET @allapot_irat_iktatasra_elokeszitett = ''0''
'
	-- sztornózottak szűréséhez
set @sqlcmd = @sqlcmd + '
	DECLARE @allapot_ugyirat_sztornozott nvarchar(64)
	DECLARE @allapot_irat_sztornozott nvarchar(64)
    SET @allapot_ugyirat_sztornozott = ''90''
	SET @allapot_irat_sztornozott = ''90''
'
--CR3135
	-- iktatásra előkészítettek szűréséhez
set @sqlcmd = @sqlcmd + '
	DECLARE @allapot_irat_atiktatott nvarchar(64)
	SET @allapot_irat_atiktatott = ''06''
'

set @sqlcmd = @sqlcmd + '
	declare @ugyintezok_db real;
	declare @ugyiratok_db real;
'
		-- ügyintézők száma a csoport dolgozóiból
set @sqlcmd = @sqlcmd + '
	set @ugyintezok_db = (SELECT COUNT(*) FROM (SELECT DISTINCT cst.Csoport_Id_Jogalany
			FROM #temp_SzervezetTagok szt
			JOIN KRT_CsoportTagok cst
			ON szt.Id = cst.Csoport_Id AND cst.Tipus = 2) as tmp)
'      

			--- DECLARE ÜGYIRAT COUNTER VÁLTOZÓK ---
set @sqlcmd = @sqlcmd + '
	declare @ugyiratok_osszesen nvarchar(10);
	declare @ugyintezes_alatt nvarchar(10);
	declare @ugyiratok nvarchar(10);
	declare @ugyiratok_ma nvarchar(10);
	declare @ugyiratok_1_3nap nvarchar(10);
	declare @ugyiratok_4_7nap nvarchar(10);	
	declare @ugyiratok_7napfelett nvarchar(10);
	declare @szignalasra_varo nvarchar(10);
	declare @elintezett nvarchar(10);
	declare @lejart_hatarideju nvarchar(10);
	declare @lejart_1nap nvarchar(10);
	declare @lejart_2_5nap nvarchar(10);
	declare @lejart_6_10nap nvarchar(10);
	declare @lejart_11_15nap nvarchar(10);
	declare @lejart_15napfelett nvarchar(10);
	declare @kritikus_hatarideju nvarchar(10);
	declare @kritikus_0nap nvarchar(10);
	declare @kritikus_1nap nvarchar(10);
	declare @kritikus_2_5nap nvarchar(10);
	declare @kritikus_6_10nap nvarchar(10);
	declare @szignalt nvarchar(10);
	declare @lezart nvarchar(10);
	declare @skontroban_levo nvarchar(10);
	declare @irattarba_kuldott nvarchar(10);
	declare @kolcsonzott nvarchar(10);
'

			--- ÜGYIRAT LEKÉRDEZÉSEK ---
set @sqlcmd = @sqlcmd + '
	select 
'
--- ÜGYIRATOK ÖSSZESEN ---
set @sqlcmd = @sqlcmd + '@ugyiratok_osszesen = SUM(1)
'
--- ÜGYINTÉZÉS ALATTI ÜGYIRATOK ---
set @sqlcmd = @sqlcmd + ',@ugyintezes_alatt = SUM(case
when EREC_UgyUgyiratok.LetrehozasIdo BETWEEN @KezdDat AND @VegeDat
	and (EREC_UgyUgyiratok.Allapot=@allapot_ugyintezes_alatt
	or (EREC_UgyUgyiratok.Allapot = @allapot_tovabbitas_alatt and EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_ugyintezes_alatt))
then 1
else 0 end)
'
--- ÜGYIRATOK ---
set @sqlcmd = @sqlcmd + ',@ugyiratok = SUM(case
when EREC_UgyUgyiratok.LetrehozasIdo BETWEEN @KezdDat AND @VegeDat
	and EREC_UgyUgyiratok.Allapot <> @allapot_ugyirat_iktatasra_elokeszitett
then 1
else 0 end) 
'
--- ÜGYIRATOK MAI NAPON ---
set @sqlcmd = @sqlcmd + ',@ugyiratok_ma = SUM(case
when EREC_UgyUgyiratok.LetrehozasIdo BETWEEN @VegeDatDatePart AND @VegeDat
	and EREC_UgyUgyiratok.LetrehozasIdo >= @KezdDat
	and EREC_UgyUgyiratok.Allapot <> @allapot_ugyirat_iktatasra_elokeszitett
then 1
else 0 end)
'
--- ÜGYIRATOK 1-3 NAPJA ---
set @sqlcmd = @sqlcmd + ',@ugyiratok_1_3nap = SUM(case
when EREC_UgyUgyiratok.LetrehozasIdo BETWEEN DATEADD(day,-3,@VegeDatDatePart) AND @VegeDatDatePart
	and EREC_UgyUgyiratok.LetrehozasIdo >= @KezdDat
	and EREC_UgyUgyiratok.Allapot <> @allapot_ugyirat_iktatasra_elokeszitett
then 1
else 0 end)
'
--- ÜGYIRATOK 4-7 NAPJA ---
set @sqlcmd = @sqlcmd + ',@ugyiratok_4_7nap = SUM(case
when EREC_UgyUgyiratok.LetrehozasIdo BETWEEN DATEADD(day,-7,@VegeDatDatePart) AND DATEADD(day,-3,@VegeDatDatePart)
	and EREC_UgyUgyiratok.LetrehozasIdo >= @KezdDat
	and EREC_UgyUgyiratok.Allapot <> @allapot_ugyirat_iktatasra_elokeszitett
then 1
else 0 end)
'
--- ÜGYIRATOK TÖBB MINT 7 NAPJA ---
set @sqlcmd = @sqlcmd + ',@ugyiratok_7napfelett = SUM(case
when EREC_UgyUgyiratok.LetrehozasIdo BETWEEN @KezdDat AND DATEADD(day,-7,@VegeDatDatePart)
	and EREC_UgyUgyiratok.LetrehozasIdo >= @KezdDat
	and EREC_UgyUgyiratok.Allapot <> @allapot_ugyirat_iktatasra_elokeszitett
then 1
else 0 end)
'
--- SZIGNÁLÁSRA VÁRÓ ÜGYIRATOK ---
set @sqlcmd = @sqlcmd + ',@szignalasra_varo = SUM(case
when EREC_UgyUgyiratok.LetrehozasIdo BETWEEN @KezdDat AND @VegeDat
	and (EREC_UgyUgyiratok.Allapot=@allapot_iktatott
	or (EREC_UgyUgyiratok.Allapot = @allapot_tovabbitas_alatt and EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_iktatott))
then 1
else 0 end)
'
--- ELINTÉZETT ---
set @sqlcmd = @sqlcmd + ',@elintezett = SUM(case
when EREC_UgyUgyiratok.LetrehozasIdo BETWEEN @KezdDat AND @VegeDat
	and (EREC_UgyUgyiratok.Allapot=@allapot_elintezett or
	(EREC_UgyUgyiratok.Allapot = @allapot_tovabbitas_alatt and EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_elintezett))
then 1
else 0 end)
'
--- LEJÁRT HATÁRIDEJŰ ÜGYIRATOK ---
set @sqlcmd = @sqlcmd + ',@lejart_hatarideju = SUM(case
when (EREC_UgyUgyiratok.Allapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.Allapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.Allapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.Allapot = @allapot_skontroban
	OR
	(EREC_UgyUgyiratok.Allapot = @allapot_tovabbitas_alatt
	and (EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_skontroban)
	))
	AND Hatarido < @VegeDatDatePart
then 1
else 0 end)
'
--- LEJÁRT 1 NAPJA
set @sqlcmd = @sqlcmd + ',@lejart_1nap = SUM(case
when (EREC_UgyUgyiratok.Allapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.Allapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.Allapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.Allapot = @allapot_skontroban
	OR
	(EREC_UgyUgyiratok.Allapot = @allapot_tovabbitas_alatt
	and (EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_skontroban)
	))
	AND DATEDIFF(day,Hatarido,@VegeDat) = 1
then 1
else 0 end)
'
--- LEJÁRT 2-5 NAPJA
set @sqlcmd = @sqlcmd + ',@lejart_2_5nap = SUM(case
when (EREC_UgyUgyiratok.Allapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.Allapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.Allapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.Allapot = @allapot_skontroban
	OR
	(EREC_UgyUgyiratok.Allapot = @allapot_tovabbitas_alatt
	and (EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_skontroban)
	))
	AND DATEDIFF(day,Hatarido,@VegeDat) BETWEEN 2 and 5
then 1
else 0 end)
'
--- LEJÁRT 6-10 NAPJA
set @sqlcmd = @sqlcmd + ',@lejart_6_10nap = SUM(case
when (EREC_UgyUgyiratok.Allapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.Allapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.Allapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.Allapot = @allapot_skontroban
	OR
	(EREC_UgyUgyiratok.Allapot = @allapot_tovabbitas_alatt
	and (EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_skontroban)
	))
	AND DATEDIFF(day,Hatarido,@VegeDat) BETWEEN 6 and 10
then 1
else 0 end)
'
--- LEJÁRT 11-15 NAPJA
set @sqlcmd = @sqlcmd + ',@lejart_11_15nap = SUM(case
when (EREC_UgyUgyiratok.Allapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.Allapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.Allapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.Allapot = @allapot_skontroban
	OR
	(EREC_UgyUgyiratok.Allapot = @allapot_tovabbitas_alatt
	and (EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_skontroban)
	))
	AND DATEDIFF(day,Hatarido,@VegeDat) BETWEEN 11 and 15
then 1
else 0 end)
'
--- LEJÁRT TÖBB MINT 15 NAPJA
set @sqlcmd = @sqlcmd + ',@lejart_15napfelett = SUM(case
when (EREC_UgyUgyiratok.Allapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.Allapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.Allapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.Allapot = @allapot_skontroban
	OR
	(EREC_UgyUgyiratok.Allapot = @allapot_tovabbitas_alatt
	and (EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_skontroban)
	))
	AND DATEDIFF(day,Hatarido,@VegeDat) > 15
then 1
else 0 end)
'
--- KRITIKUS HATÁRIDEJŰ ÜGYIRATOK ---
set @sqlcmd = @sqlcmd + ',@kritikus_hatarideju = SUM(case
when (EREC_UgyUgyiratok.Allapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.Allapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.Allapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.Allapot = @allapot_skontroban
	OR
	(EREC_UgyUgyiratok.Allapot = @allapot_tovabbitas_alatt
	and (EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_skontroban)
	))
	AND DATEDIFF(day,@VegeDat,Hatarido) BETWEEN 0 and 10
then 1
else 0 end)
'
--- KRITIKUS 0 NAP ---
set @sqlcmd = @sqlcmd + ',@kritikus_0nap = SUM(case
when (EREC_UgyUgyiratok.Allapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.Allapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.Allapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.Allapot = @allapot_skontroban
	OR
	(EREC_UgyUgyiratok.Allapot = @allapot_tovabbitas_alatt
	and (EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_skontroban)
	))
	AND DATEDIFF(day,@VegeDat,Hatarido) = 0
then 1
else 0 end)
'
--- KRITIKUS 1 NAP ---
set @sqlcmd = @sqlcmd + ',@kritikus_1nap = SUM(case
when (EREC_UgyUgyiratok.Allapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.Allapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.Allapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.Allapot = @allapot_skontroban
	OR
	(EREC_UgyUgyiratok.Allapot = @allapot_tovabbitas_alatt
	and (EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_skontroban)
	))
	AND DATEDIFF(day,@VegeDat,Hatarido) = 1
then 1
else 0 end)
'
--- KRITIKUS 2-5 NAP ---
set @sqlcmd = @sqlcmd + ',@kritikus_2_5nap = SUM(case
when (EREC_UgyUgyiratok.Allapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.Allapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.Allapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.Allapot = @allapot_skontroban
	OR
	(EREC_UgyUgyiratok.Allapot = @allapot_tovabbitas_alatt
	and (EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_skontroban)
	))
	AND DATEDIFF(day,@VegeDat,Hatarido) BETWEEN 2 AND 5
then 1
else 0 end)
'
--- KRITIKUS 6-10 NAP ---
set @sqlcmd = @sqlcmd + ',@kritikus_6_10nap = SUM(case
when (EREC_UgyUgyiratok.Allapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.Allapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.Allapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.Allapot = @allapot_skontroban
	OR
	(EREC_UgyUgyiratok.Allapot = @allapot_tovabbitas_alatt
	and (EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_ugyintezes_alatt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_iktatott
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_szignalt
	OR EREC_UgyUgyiratok.TovabbitasAlattAllapot = @allapot_skontroban)
	))
	AND DATEDIFF(day,@VegeDat,Hatarido) BETWEEN 6 AND 10
then 1
else 0 end)
'

--- SZIGNÁLT ---
set @sqlcmd = @sqlcmd + ',@szignalt = SUM(case
when EREC_UgyUgyiratok.Allapot=@allapot_szignalt
	or (EREC_UgyUgyiratok.Allapot=@allapot_tovabbitas_alatt and EREC_UgyUgyiratok.TovabbitasAlattAllapot=@allapot_szignalt)
then 1
else 0 end)
'
--- LEZÁRT ---
set @sqlcmd = @sqlcmd + ',@lezart = SUM(case
when EREC_UgyUgyiratok.Allapot=@allapot_adacta
	or (EREC_UgyUgyiratok.Allapot=@allapot_tovabbitas_alatt and EREC_UgyUgyiratok.TovabbitasAlattAllapot=@allapot_adacta)
then 1
else 0 end)
'
--- SKONTRÓBAN LÉVŐ ---
set @sqlcmd = @sqlcmd + ',@skontroban_levo = SUM(case
when EREC_UgyUgyiratok.Allapot=@allapot_skontroban
	or (EREC_UgyUgyiratok.Allapot=@allapot_tovabbitas_alatt and EREC_UgyUgyiratok.TovabbitasAlattAllapot=@allapot_skontroban)
then 1
else 0 end)
'
--- IRATTÁRBA KÜLDÖTT ---
set @sqlcmd = @sqlcmd + ',@irattarba_kuldott = SUM(case
when EREC_UgyUgyiratok.Allapot=@allapot_irattarba_kuldott
	or (EREC_UgyUgyiratok.Allapot=@allapot_tovabbitas_alatt and EREC_UgyUgyiratok.TovabbitasAlattAllapot=@allapot_irattarba_kuldott)
then 1
else 0 end)
'
--- KÖLCSÖNZÖTT, ÁTMENETI IRATTÁRBÓL KÖLCSÖNZÖTT ---
set @sqlcmd = @sqlcmd + ',@kolcsonzott = SUM(case
when EREC_UgyUgyiratok.Allapot in (@allapot_kolcsonzott, @allapot_atmeneti_irattarbol_kolcsonzott)
	or (EREC_UgyUgyiratok.Allapot=@allapot_tovabbitas_alatt and EREC_UgyUgyiratok.TovabbitasAlattAllapot in (@allapot_kolcsonzott, @allapot_atmeneti_irattarbol_kolcsonzott))
then 1
else 0 end)
'
set @sqlcmd = @sqlcmd + 'from EREC_UgyUgyiratok
	'

IF @RightCheck = '1'
BEGIN
	set @sqlcmd = @sqlcmd + 'inner join #temp_UgyiratFilter on EREC_UgyUgyiratok.Id = #temp_UgyiratFilter.Id
'
END
set @sqlcmd = @sqlcmd + 'where getdate() between EREC_UgyUgyiratok.ErvKezd and EREC_UgyUgyiratok.ErvVege
' + @OrgFilter_Ugyirat

			--- ÜGYIRAT LEKÉRDEZÉSEK VÉGE ---

			--- DECLARE KÜLDEMÉNY COUNTER VÁLTOZÓK ---
set @sqlcmd = @sqlcmd + '
	declare @kuldemenyek nvarchar(10);
	declare @kuldemenyek_ma nvarchar(10);
	declare @kuldemenyek_1_3nap nvarchar(10);
	declare @kuldemenyek_4_7nap nvarchar(10);
	declare @kuldemenyek_7napfelett nvarchar(10);
'

			--- KÜLDEMÉNY LEKÉRDEZÉSEK ---
set @sqlcmd = @sqlcmd + '
	select 
'
--- KÜLDEMÉNYEK ---
set @sqlcmd = @sqlcmd + '@kuldemenyek = SUM(case
when EREC_KuldKuldemenyek.BeerkezesIdeje between @KezdDat AND @VegeDat
then 1
else 0 end)
'
--- KÜLDEMÉNYEK MA ---
set @sqlcmd = @sqlcmd + ',@kuldemenyek_ma = SUM(case
when EREC_KuldKuldemenyek.BeerkezesIdeje BETWEEN @VegeDatDatePart AND @VegeDat
then 1
else 0 end)
'
--- KÜLDEMÉNYEK 1-3 NAPJA ---
set @sqlcmd = @sqlcmd + ',@kuldemenyek_1_3nap = SUM(case
when EREC_KuldKuldemenyek.BeerkezesIdeje BETWEEN DATEADD(day,-3,@VegeDatDatePart) AND @VegeDatDatePart
	and EREC_KuldKuldemenyek.BeerkezesIdeje >= @KezdDat
then 1
else 0 end)
'
--- KÜLDEMÉNYEK 4-7 NAPJA ---
set @sqlcmd = @sqlcmd + ',@kuldemenyek_4_7nap = SUM(case
when EREC_KuldKuldemenyek.BeerkezesIdeje BETWEEN DATEADD(day,-7,@VegeDatDatePart) AND DATEADD(day,-3,@VegeDatDatePart)
	and EREC_KuldKuldemenyek.BeerkezesIdeje >= @KezdDat
then 1
else 0 end)
'
--- KÜLDEMÉNYEK TÖBB MINT 7 NAPJA ---
set @sqlcmd = @sqlcmd + ',@kuldemenyek_7napfelett = SUM(case
when EREC_KuldKuldemenyek.BeerkezesIdeje BETWEEN @KezdDat AND DATEADD(day,-7,@VegeDatDatePart)
then 1
else 0 end)
'
set @sqlcmd = @sqlcmd + 'from EREC_KuldKuldemenyek
	'

IF @RightCheck = '1'
BEGIN
	set @sqlcmd = @sqlcmd + 'inner join #temp_KuldemenyFilter on EREC_KuldKuldemenyek.Id = #temp_KuldemenyFilter.Id
'
END
set @sqlcmd = @sqlcmd + 'where PostazasIranya not in (''2'') and getdate() between EREC_KuldKuldemenyek.ErvKezd and EREC_KuldKuldemenyek.ErvVege
' + @OrgFilter_Kuldemeny

			--- KÜLDEMÉNY LEKÉRDEZÉSEK VÉGE ---

			--- DECLARE IRAT COUNTER VÁLTOZÓK ---
set @sqlcmd = @sqlcmd + '
	declare @iratok_alszamra_ma nvarchar(10);
	declare @iratok_alszamra_belso_ma nvarchar(10);
	declare @iratok_alszamra_bejovo_ma nvarchar(10);
	-- CR3135
	declare @lejart_hatarideju_irat nvarchar(10);
	declare @lejart_1nap_irat nvarchar(10);
	declare @lejart_2_5nap_irat nvarchar(10);
	declare @lejart_6_10nap_irat nvarchar(10);
	declare @lejart_11_15nap_irat nvarchar(10);
	declare @lejart_15napfelett_irat nvarchar(10);
	declare @kritikus_hatarideju_irat nvarchar(10);
	declare @kritikus_0nap_irat nvarchar(10);
	declare @kritikus_1nap_irat nvarchar(10);
	declare @kritikus_2_5nap_irat nvarchar(10);
	declare @kritikus_6_10nap_irat nvarchar(10);
'

			--- IRAT LEKÉRDEZÉSEK ---
set @sqlcmd = @sqlcmd + '
	select 
'
--- IRATOK MAI NAPON ---
set @sqlcmd = @sqlcmd + '@iratok_alszamra_ma = SUM(case
when EREC_IraIratok.LetrehozasIdo BETWEEN @VegeDatDatePart AND @VegeDat
    and EREC_IraIratok.PostazasIranya in (''0'',''1'')
    and Alszam is not null and Allapot <> @allapot_irat_iktatasra_elokeszitett
then 1
else 0 end)
'
--- IRATOK MAI NAPON (BELSO) ---
set @sqlcmd = @sqlcmd + ',@iratok_alszamra_belso_ma = SUM(case
when EREC_IraIratok.LetrehozasIdo BETWEEN @VegeDatDatePart AND @VegeDat
    and EREC_IraIratok.PostazasIranya = ''0''
    and Alszam is not null and Allapot <> @allapot_irat_iktatasra_elokeszitett
then 1
else 0 end)
'
--- IRATOK MAI NAPON (BEJOVO) ---
set @sqlcmd = @sqlcmd + ',@iratok_alszamra_bejovo_ma = SUM(case
when EREC_IraIratok.LetrehozasIdo BETWEEN @VegeDatDatePart AND @VegeDat
    and EREC_IraIratok.PostazasIranya = ''1''
    and Alszam is not null and Allapot <> @allapot_irat_iktatasra_elokeszitett
then 1
else 0 end)
'

--CR3135
--- LEJÁRT HATÁRIDEJŰ IRATOK ---
set @sqlcmd = @sqlcmd + ',@lejart_hatarideju_irat = SUM(case
when Alszam is not null and Allapot <> @allapot_irat_iktatasra_elokeszitett
	and Allapot <> @allapot_irat_sztornozott
	and Allapot <> @allapot_irat_atiktatott
	AND Hatarido < @VegeDatDatePart
then 1
else 0 end)
'
--- LEJÁRT 1 NAPJA
set @sqlcmd = @sqlcmd + ',@lejart_1nap_irat = SUM(case
when Alszam is not null and Allapot <> @allapot_irat_iktatasra_elokeszitett
	and Allapot <> @allapot_irat_sztornozott
	and Allapot <> @allapot_irat_atiktatott
	AND DATEDIFF(day,Hatarido,@VegeDat) = 1
then 1
else 0 end)
'
--- LEJÁRT 2-5 NAPJA
set @sqlcmd = @sqlcmd + ',@lejart_2_5nap_irat = SUM(case
when Alszam is not null and Allapot <> @allapot_irat_iktatasra_elokeszitett
	and Allapot <> @allapot_irat_sztornozott
	and Allapot <> @allapot_irat_atiktatott
	AND DATEDIFF(day,Hatarido,@VegeDat) BETWEEN 2 and 5
then 1
else 0 end)
'
--- LEJÁRT 6-10 NAPJA
set @sqlcmd = @sqlcmd + ',@lejart_6_10nap_irat = SUM(case
when Alszam is not null and Allapot <> @allapot_irat_iktatasra_elokeszitett
	and Allapot <> @allapot_irat_sztornozott
	and Allapot <> @allapot_irat_atiktatott
	AND DATEDIFF(day,Hatarido,@VegeDat) BETWEEN 6 and 10
then 1
else 0 end)
'
--- LEJÁRT 11-15 NAPJA
set @sqlcmd = @sqlcmd + ',@lejart_11_15nap_irat = SUM(case
when Alszam is not null and Allapot <> @allapot_irat_iktatasra_elokeszitett
	and Allapot <> @allapot_irat_sztornozott
	and Allapot <> @allapot_irat_atiktatott
	AND DATEDIFF(day,Hatarido,@VegeDat) BETWEEN 11 and 15
then 1
else 0 end)
'
--- LEJÁRT TÖBB MINT 15 NAPJA
set @sqlcmd = @sqlcmd + ',@lejart_15napfelett_irat = SUM(case
when Alszam is not null and Allapot <> @allapot_irat_iktatasra_elokeszitett
	and Allapot <> @allapot_irat_sztornozott
	and Allapot <> @allapot_irat_atiktatott
	AND DATEDIFF(day,Hatarido,@VegeDat) > 15
then 1
else 0 end)
'

--- KRITIKUS HATÁRIDEJŰ IRATOK ---
set @sqlcmd = @sqlcmd + ',@kritikus_hatarideju_irat = SUM(case
when Alszam is not null and Allapot <> @allapot_irat_iktatasra_elokeszitett
	and Allapot <> @allapot_irat_sztornozott
	and Allapot <> @allapot_irat_atiktatott
	AND DATEDIFF(day,@VegeDat,Hatarido) BETWEEN 0 and 10
then 1
else 0 end)
'
--- KRITIKUS 0 NAP ---
set @sqlcmd = @sqlcmd + ',@kritikus_0nap_irat = SUM(case
when Alszam is not null and Allapot <> @allapot_irat_iktatasra_elokeszitett
	and Allapot <> @allapot_irat_sztornozott
	and Allapot <> @allapot_irat_atiktatott
	AND DATEDIFF(day,@VegeDat,Hatarido) = 0
then 1
else 0 end)
'
--- KRITIKUS 1 NAP ---
set @sqlcmd = @sqlcmd + ',@kritikus_1nap_irat = SUM(case
when Alszam is not null and Allapot <> @allapot_irat_iktatasra_elokeszitett
	and Allapot <> @allapot_irat_sztornozott
	and Allapot <> @allapot_irat_atiktatott
	AND DATEDIFF(day,@VegeDat,Hatarido) = 1
then 1
else 0 end)
'
--- KRITIKUS 2-5 NAP ---
set @sqlcmd = @sqlcmd + ',@kritikus_2_5nap_irat = SUM(case
when Alszam is not null and Allapot <> @allapot_irat_iktatasra_elokeszitett
	and Allapot <> @allapot_irat_sztornozott
	and Allapot <> @allapot_irat_atiktatott
	AND DATEDIFF(day,@VegeDat,Hatarido) BETWEEN 2 AND 5
then 1
else 0 end)
'
--- KRITIKUS 6-10 NAP ---
set @sqlcmd = @sqlcmd + ',@kritikus_6_10nap_irat = SUM(case
when Alszam is not null and Allapot <> @allapot_irat_iktatasra_elokeszitett
	and Allapot <> @allapot_irat_sztornozott
	and Allapot <> @allapot_irat_atiktatott
	AND DATEDIFF(day,@VegeDat,Hatarido) BETWEEN 6 AND 10
then 1
else 0 end)
'


set @sqlcmd = @sqlcmd + 'from EREC_IraIratok
	'

IF @RightCheck = '1'
BEGIN
	set @sqlcmd = @sqlcmd + 'inner join #temp_IratFilter on EREC_IraIratok.Id = #temp_IratFilter.Id
'
END
set @sqlcmd = @sqlcmd + 'where getdate() between EREC_IraIratok.ErvKezd and EREC_IraIratok.ErvVege
' + @OrgFilter_Irat

			--- IRAT LEKÉRDEZÉSEK VÉGE ---

		-- Kuldemenyek napi atlaga munkanapokra vetitve
set @sqlcmd = @sqlcmd + '
	declare @kuldemenyek_napi_atlag nvarchar(10);
	if @munkanapok_szama is null
		set @kuldemenyek_napi_atlag = ''-''
	else if @munkanapok_szama = 0
		set @kuldemenyek_napi_atlag = ''0''
	else
	begin
		declare @kuldemenyek_tegnapig real
		set @kuldemenyek_tegnapig = cast(@kuldemenyek as real) - cast(@kuldemenyek_ma as real)
		set @kuldemenyek_napi_atlag =
			cast(cast(@kuldemenyek_tegnapig / cast(@munkanapok_szama as real) as decimal(38, 1)) as NVARCHAR(10))
	end
'

-- Egy ugyintezore juto ugyiratok atlagos szama
set @sqlcmd = @sqlcmd + 'set @ugyiratok_db = @ugyiratok_osszesen
'
set @sqlcmd = @sqlcmd + '
	declare @ugyiratok_ugyintezonkenti_atlag nvarchar(10);
	if @ugyintezok_db is null
		set @ugyiratok_ugyintezonkenti_atlag = ''-''
	else if @ugyintezok_db = 0
		set @ugyiratok_ugyintezonkenti_atlag = ''0''
	else
		set @ugyiratok_ugyintezonkenti_atlag = cast(cast(@ugyiratok_db / @ugyintezok_db as decimal(38,1)) as NVARCHAR(10))
'

set @sqlcmd = @sqlcmd + '
 select IsNull(@kuldemenyek, 0) as kuldemenyek,
		IsNull(@kuldemenyek_ma, 0) as kuldemenyek_ma,
		IsNull(@kuldemenyek_1_3nap, 0) as kuldemenyek_1_3nap,
		IsNull(@kuldemenyek_4_7nap, 0) as kuldemenyek_4_7nap,
		IsNull(@kuldemenyek_7napfelett, 0) as kuldemenyek_7napfelett,
		IsNull(@kuldemenyek_napi_atlag, 0) as kuldemenyek_napi_atlag,
		IsNull(@ugyiratok_osszesen, 0) as ugyiratok_osszesen,
		IsNull(@ugyiratok, 0) as ugyiratok, -- az ''új''-nak minősülők száma
		IsNull(@ugyiratok_ma, 0) as ugyiratok_ma,
		IsNull(@ugyiratok_1_3nap, 0) as ugyiratok_1_3nap,
		IsNull(@ugyiratok_4_7nap, 0) as ugyiratok_4_7nap,
		IsNull(@ugyiratok_7napfelett, 0) as ugyiratok_7napfelett,
		IsNull(@ugyintezes_alatt, 0) as ugyintezes_alatt,
		IsNull(@szignalasra_varo, 0) as szignalasra_varo,
		IsNull(@elintezett, 0) as elintezett,
		IsNull(@lejart_hatarideju, 0) as lejart_hatarideju,
		IsNull(@lejart_1nap, 0) as lejart_1nap,
		IsNull(@lejart_2_5nap, 0) as lejart_2_5nap,
		IsNull(@lejart_6_10nap, 0) as lejart_6_10nap,
		IsNull(@lejart_11_15nap, 0) as lejart_11_15nap,
		IsNull(@lejart_15napfelett, 0) as lejart_15napfelett,
		IsNull(@kritikus_hatarideju, 0) as kritikus_hatarideju,
		IsNull(@kritikus_0nap, 0) as kritikus_0nap,
		IsNull(@kritikus_1nap, 0) as kritikus_1nap,
		IsNull(@kritikus_2_5nap, 0) as kritikus_2_5nap,
		IsNull(@kritikus_6_10nap, 0) as kritikus_6_10nap,
	    IsNull(@ugyiratok_ugyintezonkenti_atlag, 0) as ugyiratok_ugyintezonkenti_atlag,
		IsNull(@szignalt, 0) as szignalt,
		IsNull(@lezart, 0) as lezart,
		IsNull(@skontroban_levo, 0) as skontroban_levo,
		IsNull(@irattarba_kuldott, 0) as irattarba_kuldott,
		IsNull(@kolcsonzott, 0) as kolcsonzott,
        IsNull(@iratok_alszamra_ma, 0) as iratok_alszamra_ma,
        IsNull(@iratok_alszamra_bejovo_ma, 0) as iratok_alszamra_bejovo_ma,
        IsNull(@iratok_alszamra_belso_ma, 0) as iratok_alszamra_belso_ma,
-- CR3135
		IsNull(@lejart_hatarideju_irat, 0) as lejart_hatarideju_irat,
		IsNull(@lejart_1nap_irat, 0) as lejart_1nap_irat,
		IsNull(@lejart_2_5nap_irat, 0) as lejart_2_5nap_irat,
		IsNull(@lejart_6_10nap_irat, 0) as lejart_6_10nap_irat,
		IsNull(@lejart_11_15nap_irat, 0) as lejart_11_15nap_irat,
		IsNull(@lejart_15napfelett_irat, 0) as lejart_15napfelett_irat,
		IsNull(@kritikus_hatarideju_irat, 0) as kritikus_hatarideju_irat,
		IsNull(@kritikus_0nap_irat, 0) as kritikus_0nap_irat,
		IsNull(@kritikus_1nap_irat, 0) as kritikus_1nap_irat,
		IsNull(@kritikus_2_5nap_irat, 0) as kritikus_2_5nap_irat,
		IsNull(@kritikus_6_10nap_irat, 0) as kritikus_6_10nap_irat
'
---- nyomtatási blokk kezdete
--   if exists (select 1 from  sysobjects where id = object_id('sp_PrintText') and type = 'P')
--   begin
--       exec dbo.sp_PrintText @sqlcmd;
--   end
---- nyomtatási blokk vége
exec (@sqlcmd)

        END TRY
        BEGIN CATCH
            DECLARE @errorSeverity INT,
                @errorState INT
            DECLARE @errorCode NVARCHAR(1000)    
            SET @errorSeverity = ERROR_SEVERITY()
            SET @errorState = ERROR_STATE()
	
            IF ERROR_NUMBER() < 50000 
                SET @errorCode = '[' + CONVERT(NVARCHAR(10), ERROR_NUMBER())
                    + '] ' + ERROR_MESSAGE()
            ELSE 
                SET @errorCode = ERROR_MESSAGE()
      
            IF @errorState = 0 
                SET @errorState = 1

            RAISERROR ( @errorCode, @errorSeverity, @errorState )
 
        END CATCH

    END
