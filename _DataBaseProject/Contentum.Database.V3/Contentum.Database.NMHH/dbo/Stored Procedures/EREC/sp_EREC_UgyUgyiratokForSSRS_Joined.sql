﻿	set ANSI_NULLS ON
	set QUOTED_IDENTIFIER ON
		go
			if exists (select 1 from  sysobjects
				   where  id = object_id('sp_EREC_UgyUgyiratokForSSRS_Joined')
					 and  type in ('P'))
		   drop procedure sp_EREC_UgyUgyiratokForSSRS_Joined
		go

CREATE procedure [dbo].[sp_EREC_UgyUgyiratokForSSRS_Joined]

  @Where_EREC		nvarchar(MAX) = '',
  @Where_MIG			nvarchar(MAX) = '',
  @OrderBy			nvarchar(400) = ' order by Foszam_Merge',
  @ExecutorUserId	nvarchar(400),
  @FelhasznaloSzervezet_Id nvarchar(400),
  @TopRow NVARCHAR(5) = '',
  @UgyintezoSet bit = 0

as

begin

declare @joined table ( Foszam_Merge nvarchar(max), LetrehozasIdo_Rovid nvarchar(max), NevSTR_Ugyindito nvarchar(max), CimSTR_Ugyindito nvarchar(max), Targy nvarchar(max), KRT_Csoportok_Orzo_Nev nvarchar(max), PotlistaOrzoNev nvarchar(max), UgyTipus_Nev nvarchar(max), Ugyirat_helye nvarchar(max),Nev nvarchar(max)  )

--eRecord - Ugyintézővel
IF NULLIF(@Where_EREC, '') IS NOT NULL AND @UgyintezoSet = 1
BEGIN
insert into @joined exec sp_EREC_UgyUgyiratokForSSRS
@Where=@Where_EREC,
@OrderBy=@OrderBy,
@TopRow=@TopRow,
@ExecutorUserId=@ExecutorUserId,
@FelhasznaloSzervezet_Id = @FelhasznaloSzervezet_Id
END

--eRecord - Ugyintéző nélkül
IF NULLIF(@Where_EREC, '') IS NOT NULL AND @UgyintezoSet = 0
BEGIN
insert into @joined exec sp_EREC_UgyUgyiratokForSSRSNincsUgyintezo
@Where=@Where_EREC,
@OrderBy=@OrderBy,
@TopRow=@TopRow,
@ExecutorUserId=@ExecutorUserId,
@FelhasznaloSzervezet_Id = @FelhasznaloSzervezet_Id
END


--eMigration - Ugyintézővel
IF NULLIF(@Where_MIG, '') IS NOT NULL AND @UgyintezoSet = 1
BEGIN
insert into @joined exec [$(MIGRDB)].[dbo].[sp_MIG_UgyUgyiratokForElszamoltatasiJkSSRS]
@Where=@Where_MIG,
@OrderBy=@OrderBy,
@TopRow=@TopRow,
@ExecutorUserId=@ExecutorUserId
--@FelhasznaloSzervezet_Id = @FelhasznaloSzervezet_Id
END

--eMigration - Ugyintéző nélkül 
IF NULLIF(@Where_MIG, '') IS NOT NULL AND @UgyintezoSet = 0
BEGIN
insert into @joined exec [$(MIGRDB)].[dbo].[sp_MIG_UgyUgyiratokForElszamoltatasiJkSSRS]
@Where=@Where_MIG,
@OrderBy=@OrderBy,
@TopRow=@TopRow,
@ExecutorUserId=@ExecutorUserId
--@FelhasznaloSzervezet_Id = @FelhasznaloSzervezet_Id
END

select * from @joined

end

