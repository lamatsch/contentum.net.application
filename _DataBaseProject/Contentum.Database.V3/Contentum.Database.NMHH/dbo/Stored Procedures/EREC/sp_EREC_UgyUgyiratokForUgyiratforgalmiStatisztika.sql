﻿
/*
History:
--- létrehozva #1312 - Ügyiratforgalmi statisztika
*/

CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratokForUgyiratforgalmiStatisztika]
  @DatumTol                datetime
 ,@DatumIg                 datetime
 ,@Where                   nvarchar(MAX)     = ''        --- Iktatóhely vagy Szervezet szűrés és ? további @where_... szűrések ?
 ,@ExecutorUserId          uniqueidentifier
 ,@CsakAktivIrat           bit               = 0         -- Csak az AKTIV Iratokat láttatjuk: 1 (default); vagy minden irat: 0  

as

begin

BEGIN TRY
  set nocount ON
   
  DECLARE @Org_ID uniqueidentifier
  SET @Org_ID = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
  if (@Org_ID is null)
      RAISERROR('[50202]',16,1);
  set @Org_ID = ( select ID from KRT_Orgok where Id = @Org_ID and exists ( select 1 from KRT_Parameterek p where p.Org = @Org_ID and p.Nev = 'TUK' and p.Ertek = 1 ) )
  if (@Org_ID is null)
      RAISERROR('[50202]',16,1);                                        --- Ide talán elkelne egy másik hibakód
   
  DECLARE @OrgKod nvarchar(100)
  set @OrgKod = (select Kod from KRT_Orgok where Id = @Org_ID)
  
  DECLARE @sqlcmd nvarchar(MAX);  

  select im.KOD, im.Minosites, im.PostazasIranya, im.MMJ, im.IratPld_DB
     into #tmp_IratMinosites
     from ( values ('ERK_SZT','005', '1', 'False', 0)
                  ,('ERK_T'  ,'004', '1', 'False', 0)
	 	  	      ,('ERK_B'  ,'003', '1', 'False', 0)
	 		      ,('ERK_KT' ,'002', '1', 'False', 0)
				  ,('SAJ_SZT','005', '0', 'False', 0)
                  ,('SAJ_T'  ,'004', '0', 'False', 0)
	 		      ,('SAJ_B'  ,'003', '0', 'False', 0)
	 		      ,('SAJ_KT' ,'002', '0', 'False', 0)
				  ,('ISM_SZT','005', '0', 'True', 0)
                  ,('ISM_T'  ,'004', '0', 'True', 0)
	 		      ,('ISM_B'  ,'003', '0', 'True', 0)
	 		      ,('ISM_KT' ,'002', '0', 'True', 0)
          ) im(Kod, Minosites, PostazasIranya, MMJ, IratPld_DB)

  declare @KOD            varchar(10)
  declare @Minosites      varchar(10)
  declare @PostazasIranya varchar(1)
  
  declare @TMP_cur CURSOR;
  Set @TMP_cur = cursor for
		select im.KOD
		      ,im.Minosites
              ,im.PostazasIranya 			  
		  from #tmp_IratMinosites im
		 where 1 = 1

  open @TMP_cur
  fetch next from @TMP_cur into @KOD, @Minosites, @PostazasIranya
  while @@FETCH_STATUS = 0
  begin			
    --- jelenleg NEM figyelem az egyes táblákon az ErvKezd, ErvVege adatmezőket 	
    --- '90' / 'Sztornózott' Ügyiratokat, Iratokat és IratPéldányokat NEM gyűjtjük
	--- sajnos a 'Megismételt minősítési jelölés' bejövő iratra is bejelölhető - de itt és most a bejövő iratpéldány darabszámot NEM bontjuk ebből a szempontból
    SET @sqlcmd = '; 
                   update t
                      SET t.IratPld_DB = ( select count(1) from EREC_PldIratPeldanyok 
                                            inner join EREC_IraIratok on EREC_IraIratok.ID = EREC_PldIratPeldanyok.IraIrat_Id 
											                         and EREC_IraIratok.Minosites collate Database_default = t.Minosites collate Database_default 
						                                             and EREC_IraIratok.PostazasIranya = t.PostazasIranya 
													                 and EREC_IraIratok.LetrehozasIdo between @DatumTol and @DatumIg
																	 and EREC_IraIratok.Allapot != ''90''
																	 and ( ( case when t.KOD like ''ERK%'' then ''False''        
																	              else isnull( ( select ot.ertek from EREC_ObjektumTargyszavai ot 
                                                                                                  inner join EREC_Obj_MetaAdatai oma on oma.Id = ot.Obj_Metaadatai_Id
                                                                                                  inner join EREC_Obj_MetaDefinicio omd on omd.id = oma.Obj_MetaDefinicio_Id and omd.ContentType = ''TUKIratok''
                                                                                                  where ot.Obj_Id = EREC_IraIratok.ID and ot.Targyszo = ''Megismételt minősítési jelölés''), ''False'' )
																			 end
																		   ) = t.MMJ
																		 )
                                            inner join EREC_UgyUgyiratok on EREC_UgyUgyiratok.ID = EREC_IraIratok.Ugyirat_ID and EREC_UgyUgyiratok.Allapot != ''90''
                                            inner join EREC_IraIktatoKonyvek EREC_IraIktatoKonyvek on EREC_IraIktatoKonyvek.ID = EREC_UgyUgyiratok.IraIktatokonyv_Id
                                            where 1 = 1
											  and EREC_PldIratPeldanyok.Allapot != ''90''
                  '							
    if IsNull(@where, '' ) != ''
      SET @sqlcmd += '                        and (' + @where + ') '
    SET @sqlcmd += '                     )
                   from #tmp_IratMinosites t
                  where t.Kod = @KOD
				'
    /* --- teszteléshez
       print LEFT(@sqlcmd,4000)
    --- */
    execute sp_executesql @sqlcmd,N'@DatumTol datetime, @DatumIg datetime, @KOD varchar(10)',@Datumtol = @DatumTol, @DatumIg = @DatumIg, @KOD = @KOD;

	fetch next from @TMP_cur into @KOD, @Minosites, @PostazasIranya
  end
  close @TMP_cur  
  deallocate @TMP_cur
  
  --- ez a visszaadott érték 
  select KOD, IratPld_DB from #tmp_IratMinosites
   
  drop table #tmp_IratMinosites   

END TRY

BEGIN CATCH
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()

   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end