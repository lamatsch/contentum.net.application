/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_UgyUgyiratokGetAll')
            and   type = 'P')
   drop procedure sp_EREC_UgyUgyiratokGetAll
go
*/
create procedure sp_EREC_UgyUgyiratokGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_UgyUgyiratok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_UgyUgyiratok.Id,
	   EREC_UgyUgyiratok.Foszam,
	   EREC_UgyUgyiratok.Sorszam,
	   EREC_UgyUgyiratok.Ugyazonosito ,
	   EREC_UgyUgyiratok.Alkalmazas_Id,
	   EREC_UgyUgyiratok.UgyintezesModja,
	   EREC_UgyUgyiratok.Hatarido,
	   EREC_UgyUgyiratok.SkontrobaDat,
	   EREC_UgyUgyiratok.LezarasDat,
	   EREC_UgyUgyiratok.IrattarbaKuldDatuma ,
	   EREC_UgyUgyiratok.IrattarbaVetelDat,
	   EREC_UgyUgyiratok.FelhCsoport_Id_IrattariAtvevo,
	   EREC_UgyUgyiratok.SelejtezesDat,
	   EREC_UgyUgyiratok.FelhCsoport_Id_Selejtezo,
	   EREC_UgyUgyiratok.LeveltariAtvevoNeve,
	   EREC_UgyUgyiratok.FelhCsoport_Id_Felulvizsgalo,
	   EREC_UgyUgyiratok.FelulvizsgalatDat,
	   EREC_UgyUgyiratok.IktatoszamKieg,
	   EREC_UgyUgyiratok.Targy,
	   EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo,
	   EREC_UgyUgyiratok.UgyUgyirat_Id_Kulso,
	   EREC_UgyUgyiratok.UgyTipus,
	   EREC_UgyUgyiratok.IrattariHely,
	   EREC_UgyUgyiratok.SztornirozasDat,
	   EREC_UgyUgyiratok.Csoport_Id_Felelos,
	   EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo,
	   EREC_UgyUgyiratok.Csoport_Id_Cimzett,
	   EREC_UgyUgyiratok.Jelleg,
	   EREC_UgyUgyiratok.IraIrattariTetel_Id,
	   EREC_UgyUgyiratok.IraIktatokonyv_Id,
	   EREC_UgyUgyiratok.SkontroOka,
	   EREC_UgyUgyiratok.SkontroVege,
	   EREC_UgyUgyiratok.Surgosseg,
	   EREC_UgyUgyiratok.SkontrobanOsszesen,
	   EREC_UgyUgyiratok.MegorzesiIdoVege,
	   EREC_UgyUgyiratok.Partner_Id_Ugyindito,
	   EREC_UgyUgyiratok.NevSTR_Ugyindito,
	   EREC_UgyUgyiratok.ElintezesDat,
	   EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez,
	   EREC_UgyUgyiratok.Csoport_Id_Felelos_Elozo,
	   EREC_UgyUgyiratok.KolcsonKikerDat,
	   EREC_UgyUgyiratok.KolcsonKiadDat,
	   EREC_UgyUgyiratok.Kolcsonhatarido,
	   EREC_UgyUgyiratok.BARCODE,
	   EREC_UgyUgyiratok.IratMetadefinicio_Id,
	   EREC_UgyUgyiratok.Allapot,
	   EREC_UgyUgyiratok.TovabbitasAlattAllapot,
	   EREC_UgyUgyiratok.Megjegyzes,
	   EREC_UgyUgyiratok.Azonosito,
	   EREC_UgyUgyiratok.Fizikai_Kezbesitesi_Allapot,
	   EREC_UgyUgyiratok.Kovetkezo_Orzo_Id,
	   EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos,
	   EREC_UgyUgyiratok.Elektronikus_Kezbesitesi_Allap,
	   EREC_UgyUgyiratok.Kovetkezo_Felelos_Id,
	   EREC_UgyUgyiratok.UtolsoAlszam,
	   EREC_UgyUgyiratok.UtolsoSorszam,
	   EREC_UgyUgyiratok.IratSzam,
	   EREC_UgyUgyiratok.ElintezesMod,
	   EREC_UgyUgyiratok.RegirendszerIktatoszam,
	   EREC_UgyUgyiratok.GeneraltTargy,
	   EREC_UgyUgyiratok.Cim_Id_Ugyindito ,
	   EREC_UgyUgyiratok.CimSTR_Ugyindito ,
	   EREC_UgyUgyiratok.LezarasOka,
	   EREC_UgyUgyiratok.FelfuggesztesOka,
	   EREC_UgyUgyiratok.IrattarId,
	   EREC_UgyUgyiratok.SkontroOka_Kod,
	   EREC_UgyUgyiratok.UjOrzesiIdo,
	   EREC_UgyUgyiratok.UjOrzesiIdoIdoegyseg,
	   EREC_UgyUgyiratok.UgyintezesKezdete,
	   EREC_UgyUgyiratok.FelfuggesztettNapokSzama,
	   EREC_UgyUgyiratok.IntezesiIdo,
	   EREC_UgyUgyiratok.IntezesiIdoegyseg,
	   EREC_UgyUgyiratok.Ugy_Fajtaja,
	   EREC_UgyUgyiratok.SzignaloId,
	   EREC_UgyUgyiratok.SzignalasIdeje,
	   EREC_UgyUgyiratok.ElteltIdo,
	   EREC_UgyUgyiratok.ElteltIdoIdoEgyseg,
	   EREC_UgyUgyiratok.ElteltidoAllapot,
	   EREC_UgyUgyiratok.ElteltIdoUtolsoModositas,
	   EREC_UgyUgyiratok.SakkoraAllapot,
	   EREC_UgyUgyiratok.HatralevoNapok,
	   EREC_UgyUgyiratok.HatralevoMunkaNapok,
	   EREC_UgyUgyiratok.ElozoAllapot,
	   EREC_UgyUgyiratok.Aktiv,
	   EREC_UgyUgyiratok.IrattarHelyfoglalas,
	   EREC_UgyUgyiratok.Ver,
	   EREC_UgyUgyiratok.Note,
	   EREC_UgyUgyiratok.Stat_id,
	   EREC_UgyUgyiratok.ErvKezd,
	   EREC_UgyUgyiratok.ErvVege,
	   EREC_UgyUgyiratok.Letrehozo_id,
	   EREC_UgyUgyiratok.LetrehozasIdo,
	   EREC_UgyUgyiratok.Modosito_id,
	   EREC_UgyUgyiratok.ModositasIdo,
	   EREC_UgyUgyiratok.Zarolo_id,
	   EREC_UgyUgyiratok.ZarolasIdo,
	   EREC_UgyUgyiratok.Tranz_id,
	   EREC_UgyUgyiratok.UIAccessLog_id  
   from 
     EREC_UgyUgyiratok as EREC_UgyUgyiratok      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go