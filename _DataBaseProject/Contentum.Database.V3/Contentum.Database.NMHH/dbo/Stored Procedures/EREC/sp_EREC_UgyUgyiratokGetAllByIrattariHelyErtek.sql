﻿
create procedure sp_EREC_UgyUgyiratokGetAllByIrattariHelyErtek
  @IrattarId uniqueidentifier,
  @OrderBy nvarchar(200) = ' order by   EREC_UgyUgyiratok.LetrehozasIdo',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''



	SET @sqlcmd = @sqlcmd + 
  'select 
  	   EREC_UgyUgyiratok.Id,
	   EREC_UgyUgyiratok.Azonosito,
	   EREC_UgyUgyiratok.Ver
   from 
     EREC_UgyUgyiratok as EREC_UgyUgyiratok      
   where
	EREC_UgyUgyiratok.IrattarId='''+CAST(@IrattarId as Nvarchar(40))+'''
	and EREC_UgyUgyiratok.ErvKezd<=getdate() and EREC_UgyUgyiratok.ErvVege >=GETDATE()
   '

	
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end