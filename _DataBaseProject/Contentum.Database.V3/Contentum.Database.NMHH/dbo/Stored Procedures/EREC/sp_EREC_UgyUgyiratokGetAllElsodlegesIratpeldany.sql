﻿create procedure [dbo].[sp_EREC_UgyUgyiratokGetAllElsodlegesIratpeldany]
	@UgyiratId			UNIQUEIDENTIFIER
as
BEGIN TRY
	SELECT *
		FROM EREC_PldIratPeldanyok
			INNER JOIN EREC_IraIratok ON EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id
			INNER JOIN EREC_UgyUgyiratdarabok ON EREC_IraIratok.UgyUgyIratDarab_Id = EREC_UgyUgyiratdarabok.Id
			INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
		WHERE EREC_UgyUgyiratok.Id = @UgyiratId
			AND EREC_PldIratPeldanyok.Sorszam = 1;
END TRY
BEGIN CATCH

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH