﻿CREATE procedure [dbo].[sp_EREC_UgyUgyiratokGetAllEsemenyWithExtension]
  @UgyiratId			UNIQUEIDENTIFIER,
  @ForMunkanaplo char(1) = '0',
  @Where nvarchar(MAX) = '', -- KRT_Esemenyekre vonatkozó feltétel
  @OrderBy			nvarchar(200) = ' order by KRT_Esemenyek.LetrehozasIdo',
  @TopRow			nvarchar(5) = '',
  @ExecutorUserId	uniqueidentifier,
  @UgyintezoId	uniqueidentifier = null,
  @pageNumber		int = 0,
  @pageSize			int = -1,
  @SelectedRowId	uniqueidentifier = null

as

begin

BEGIN TRY
	set nocount on
	DECLARE @sqlcmd nvarchar(MAX)
	DECLARE @LocalTopRow nvarchar(10)
	declare @firstRow int
	declare @lastRow INT

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

	IF @Where is null
	BEGIN
		set @Where = ''
	END

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
		if (@pageSize > @TopRow)
			SET @pageSize = @TopRow;
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
                  
	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = ISNULL((SELECT TOP (1) Ertek FROM KRT_Parameterek WHERE Nev = 'LISTA_ELEMSZAM_MAX'),1000);
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	end
	
	IF @Where <> ''
	BEGIN
		set @Where = N' and ' + @Where
	END

	declare @isIratUgyintezo char(1)

	IF @ForMunkanaplo = '1'
	BEGIN
		IF @UgyintezoId is null
		BEGIN
			set @UgyintezoId = @ExecutorUserId		
		END

		set @isIratUgyintezo = '1'
		if exists(select 1 from EREC_UgyUgyiratok
			where Id=@ugyiratId
			and FelhasznaloCsoport_Id_Ugyintez=@UgyintezoId)
		begin
			-- a felhasználó az ügyiratnak ügyintézoje
			set @isIratUgyintezo = '0'
		end
	END
	
	-- Ha munkanaplóhoz kérünk le, és nem a felhasználó az ügyirat ügyintézoje,
	-- akkor csak azon iratokhoz és azok példányaihoz
	-- kapcsolódó eseményeket adjuk vissza, ahol o az irat ügyintézoje
	-- (Az olvashatóság kedvéért két külön ág a sok kis elágazás helyett)
	IF IsNull(@isIratUgyintezo, '0') <> '1'
	BEGIN -- ha nem kell szurni irat ügyintézo szerint
		set @sqlcmd = N'
	SELECT Id into #filter FROM
	( 
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_UgyUgyiratok.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL ' + @Where + '
		UNION ALL
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_UgyUgyiratdarabok.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL ' + @Where + '
		UNION ALL
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				INNER JOIN EREC_IraIratok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_IraIratok.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL ' + @Where + '
		UNION ALL
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				INNER JOIN EREC_IraIratok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
				INNER JOIN EREC_PldIratPeldanyok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_PldIratPeldanyok.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL ' + @Where + '
		UNION ALL
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				INNER JOIN EREC_IraIratok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
				INNER JOIN EREC_PldIratPeldanyok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
				INNER JOIN EREC_KuldKuldemenyek ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_KuldKuldemenyek.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL ' + @Where + '
		UNION ALL
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN EREC_IraIratok ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id
				INNER JOIN EREC_KuldKuldemenyek ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id
				INNER JOIN HKP_DokumentumAdatok ON EREC_KuldKuldemenyek.Id = HKP_DokumentumAdatok.KuldKuldemeny_Id 
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = HKP_DokumentumAdatok.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL ' + @Where + '
	) as KRT_Esemenyek ';
	
	set @sqlcmd = @sqlcmd + N'
	INSERT INTO #filter SELECT Id FROM
	( 
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
			    INNER JOIN EREC_HataridosFeladatok on EREC_HataridosFeladatok.Obj_Id = EREC_UgyUgyiratok.Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_HataridosFeladatok.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL ' + @Where + '
		UNION ALL
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				INNER JOIN EREC_HataridosFeladatok on EREC_HataridosFeladatok.Obj_Id = EREC_UgyUgyiratdarabok.Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_HataridosFeladatok.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL ' + @Where + '
		UNION ALL
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				INNER JOIN EREC_IraIratok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
				INNER JOIN EREC_HataridosFeladatok on EREC_HataridosFeladatok.Obj_Id = EREC_IraIratok.Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_HataridosFeladatok.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL ' + @Where + '
		UNION ALL
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				INNER JOIN EREC_IraIratok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
				INNER JOIN EREC_PldIratPeldanyok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
				INNER JOIN EREC_HataridosFeladatok on EREC_HataridosFeladatok.Obj_Id = EREC_PldIratPeldanyok.Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_HataridosFeladatok.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL ' + @Where + '
		UNION ALL
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				INNER JOIN EREC_IraIratok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
				INNER JOIN EREC_PldIratPeldanyok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
				INNER JOIN EREC_KuldKuldemenyek ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id
				INNER JOIN EREC_HataridosFeladatok on EREC_HataridosFeladatok.Obj_Id = EREC_KuldKuldemenyek.Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_HataridosFeladatok.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL ' + @Where + '
	) as KRT_Esemenyek ';
	END -- ha nem kell szurni irat ügyintézo szerint
	ELSE
	BEGIN -- ha szurni kell irat ügyintézo szerint
          -- csak azokat a tételeket hozzuk, amik az általa ügyintézett irathoz vagy annak példányához kapcsolódnak 
		set @sqlcmd = N'
	SELECT Id into #filter FROM
	( 
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				INNER JOIN EREC_IraIratok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_IraIratok.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL
				AND EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez=''' + CAST(@UgyintezoId AS CHAR(36)) + ''' ' + @Where + '
		UNION ALL
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				INNER JOIN EREC_IraIratok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
				INNER JOIN EREC_PldIratPeldanyok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_PldIratPeldanyok.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL
				AND EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez=''' + CAST(@UgyintezoId AS CHAR(36)) + ''' ' + @Where + '
		UNION ALL
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				INNER JOIN EREC_IraIratok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
				INNER JOIN EREC_PldIratPeldanyok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
				INNER JOIN EREC_KuldKuldemenyek ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_KuldKuldemenyek.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL
				AND EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez=''' + CAST(@UgyintezoId AS CHAR(36)) + ''' ' + @Where + '
	) as KRT_Esemenyek ';
	
	set @sqlcmd = @sqlcmd + N'
	INSERT INTO #filter SELECT Id FROM
	( 
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				INNER JOIN EREC_IraIratok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
				INNER JOIN EREC_HataridosFeladatok on EREC_HataridosFeladatok.Obj_Id = EREC_IraIratok.Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_HataridosFeladatok.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL
				AND EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez=''' + CAST(@UgyintezoId AS CHAR(36)) + ''' ' + @Where + '
		UNION ALL
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				INNER JOIN EREC_IraIratok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
				INNER JOIN EREC_PldIratPeldanyok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
				INNER JOIN EREC_HataridosFeladatok on EREC_HataridosFeladatok.Obj_Id = EREC_PldIratPeldanyok.Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_HataridosFeladatok.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL
				AND EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez=''' + CAST(@UgyintezoId AS CHAR(36)) + ''' ' + @Where + '
		UNION ALL
		SELECT	KRT_Esemenyek.Id
			FROM EREC_UgyUgyiratok
				INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id
				INNER JOIN EREC_IraIratok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
				INNER JOIN EREC_PldIratPeldanyok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
				INNER JOIN EREC_KuldKuldemenyek ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id
				INNER JOIN EREC_HataridosFeladatok on EREC_HataridosFeladatok.Obj_Id = EREC_KuldKuldemenyek.Id
				INNER JOIN KRT_Esemenyek ON KRT_Esemenyek.Obj_Id = EREC_HataridosFeladatok.Id
			WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N'''
				AND KRT_Esemenyek.Funkcio_Id IS NOT NULL
				AND EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez=''' + CAST(@UgyintezoId AS CHAR(36)) + ''' ' + @Where + '
	) as KRT_Esemenyek ';
	END -- ha szurni kell irat ügyintézo szerint

	
	IF @ForMunkanaplo = '1'
    BEGIN
		SET @sqlcmd = @sqlcmd + '
		DELETE FROM #filter WHERE Id NOT IN (SELECT KRT_Esemenyek.Id FROM KRT_Esemenyek JOIN KRT_Funkciok
		ON KRT_Esemenyek.Funkcio_Id = KRT_Funkciok.Id AND KRT_Funkciok.MunkanaploJelzo = ''1'')
		'
    END
	
	
	SET @sqlcmd = @sqlcmd + N'
	select 
		row_number() over('+@OrderBy+') as RowNumber,
  	   KRT_Esemenyek.Id into #result
		from KRT_Esemenyek as KRT_Esemenyek
			inner join KRT_Felhasznalok as KRT_Felhasznalok on KRT_Esemenyek.Felhasznalo_Id_User = KRT_Felhasznalok.Id
			inner join KRT_Felhasznalok as KRT_Felhasznalok1 on KRT_Esemenyek.Felhasznalo_Id_Login = KRT_Felhasznalok1.Id 
			left join KRT_Csoportok on KRT_Csoportok.Id = KRT_Esemenyek.Csoport_Id_FelelosUserSzerveze
			inner join KRT_ObjTipusok on KRT_ObjTipusok.Id = KRT_Esemenyek.ObjTip_Id
			inner join KRT_Funkciok as KRT_Funkciok on KRT_Esemenyek.Funkcio_Id = KRT_Funkciok.Id    
        where KRT_Esemenyek.Id in (select Id from #filter);'

		if (@SelectedRowId is not null)
			set @sqlcmd = @sqlcmd + N'
			if exists (select 1 from #result where Id = ''' + cast(@SelectedRowId as nvarchar(36)) + ''')
			BEGIN
				select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = ''' + cast(@SelectedRowId as nvarchar(36)) + ''';
				set @firstRow = (@pageNumber - 1) * @pageSize + 1;
				set @lastRow = @pageNumber*@pageSize;
				select @pageNumber = @pageNumber - 1
			END
			ELSE'
		--ELSE
			set @sqlcmd = @sqlcmd + N' 
				if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
				BEGIN
					select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
					set @firstRow = (@pageNumber - 1) * @pageSize + 1;
					set @lastRow = @pageNumber*@pageSize;
					select @pageNumber = @pageNumber - 1
				END ;'
			
		set @sqlcmd = @sqlcmd + N'
		select 
		#result.RowNumber,
		#result.Id,
		
	   KRT_Esemenyek.Obj_Id,
	   KRT_Esemenyek.ObjTip_Id,
	   KRT_ObjTipusok.Kod AS Obj_Type,
	   CASE KRT_ObjTipusok.Kod
			WHEN ''EREC_HataridosFeladatok'' THEN (SELECT EREC_HataridosFeladatok.Obj_Type FROM EREC_HataridosFeladatok WHERE Id = KRT_Esemenyek.Obj_Id)
			ELSE ''''
	   END as ObjType_Feladat,
       --KRT_ObjTipusok.Nev as ObjTip_Id_Nev,
       CASE KRT_ObjTipusok.Kod
			WHEN ''EREC_UgyUgyiratok'' THEN ''Ügyirat''
			WHEN ''EREC_UgyUgyiratdarabok'' THEN ''Ügyiratdarab''
			WHEN ''EREC_IraIratok'' THEN ''Irat''
			WHEN ''EREC_PldIratpeldanyok'' THEN ''Iratpéldány''
			WHEN ''EREC_KuldKuldemenyek'' THEN ''Küldemény''
			WHEN ''EREC_HataridosFeladatok'' THEN ''Feljegyzés''
			WHEN ''HKP_DokumentumAdatok'' THEN ''Hivatali kapu üzenet''
			ELSE KRT_ObjTipusok.Nev
	   END as ObjTip_Id_Nev,
       KRT_ObjTipusok.Kod as ObjTip_Id_Kod,
	   CASE KRT_Funkciok.Kod
			WHEN ''EgyebEsemeny'' THEN KRT_Esemenyek.Note
			ELSE KRT_Esemenyek.Azonositoja
	   END as Azonositoja,
	   KRT_Esemenyek.Felhasznalo_Id_User,
KRT_Felhasznalok.Nev as Felhasznalo_Id_User_Nev,
	   KRT_Esemenyek.Csoport_Id_FelelosUserSzerveze,
KRT_Csoportok.Nev as Csoport_Id_FelelosUserSzerveze_Nev,
	   KRT_Esemenyek.Felhasznalo_Id_Login,
KRT_Felhasznalok1.Nev as Felhasznalo_Id_Login_Nev,
	   KRT_Esemenyek.Csoport_Id_Cel,
	   KRT_Esemenyek.Helyettesites_Id,
KRT_Helyettesitesek.HelyettesitesMod as HelyettesitesMod,
dbo.fn_KodTarErtekNeve(''HELYETTESITES_MOD'',KRT_Helyettesitesek.HelyettesitesMod,''' + cast(@Org as NVarChar(40)) + ''') as HelyettesitesMod_Nev,
--(select Nev from KRT_KodTarak where KRT_KodTarak.Kod=KRT_Helyettesitesek.HelyettesitesMod and KRT_KodTarak.KodCsoport_Id in (select Id from KRT_KodCsoportok where Kod=''HELYETTESITES_MOD'')) as HelyettesitesMod_Nev,
	   KRT_Esemenyek.Tranzakcio_Id,
	   KRT_Esemenyek.Funkcio_Id,
KRT_Funkciok.Nev as Funkciok_Nev,
CASE
	WHEN KRT_Funkciok.Kod = ''BelsoIratIktatas'' THEN EREC_PldIratPeldanyok.NevSTR_Cimzett
	WHEN KRT_Funkciok.Kod = ''BejovoIratIktatas'' THEN EREC_KuldKuldemenyek.NevSTR_Bekuldo
	else ''''
END AS PartnerNev,
CASE
	WHEN KRT_Funkciok.Kod = ''BelsoIratIktatas'' THEN ISNULL(EREC_PldIratPeldanyok.CimSTR_Cimzett,'''')
	WHEN KRT_Funkciok.Kod = ''BejovoIratIktatas'' THEN ISNULL(EREC_KuldKuldemenyek.CimSTR_Bekuldo,'''')
	else ''''
END AS PartnerCim,
CASE	
	WHEN KRT_ObjTipusok.Kod = ''EREC_IraIratok'' THEN 
		CASE WHEN EREC_IraIratok.Alszam is not null 
			THEN CAST(EREC_IraIratok.Alszam AS NVARCHAR)
			ELSE ''MI'' + cast(EREC_IraIratok.Sorszam AS nvarchar) END
END AS Alszam,
CASE KRT_ObjTipusok.Kod
	WHEN ''EREC_IraIratok'' THEN EREC_IraIratok.Targy	
	WHEN ''EREC_UgyUgyiratok'' THEN (SELECT EREC_UgyUgyiratok.Targy 
									FROM EREC_UgyUgyiratok
									WHERE EREC_UgyUgyiratok.Id = ''' + CAST(@ugyiratId AS CHAR(36)) + N''')
	ELSE Azonositoja
END AS Targy,
CASE
	WHEN KRT_Funkciok.Kod = ''BejovoIratIktatas'' THEN ISNULL(EREC_KuldKuldemenyek.HivatkozasiSzam,'''')
	else ''''
END AS HivatkozasiSzam,
	   KRT_Esemenyek.Ver,
	   KRT_Esemenyek.Note,
	   KRT_Esemenyek.Stat_id,
	   KRT_Esemenyek.ErvKezd,
	   KRT_Esemenyek.ErvVege,
	   KRT_Esemenyek.Letrehozo_id,
KRT_Esemenyek.LetrehozasIdo,
	   KRT_Esemenyek.Modosito_id,
	   KRT_Esemenyek.ModositasIdo,
	   KRT_Esemenyek.Zarolo_id,
	   KRT_Esemenyek.ZarolasIdo,
	   KRT_Esemenyek.Tranz_id,
	   KRT_Esemenyek.UIAccessLog_id,
	   KRT_Esemenyek.Munkaallomas,
	   KRT_Esemenyek.MuveletKimenete   
   from 
     KRT_Esemenyek as KRT_Esemenyek
     		inner join #result on #result.Id = KRT_Esemenyek.Id				
inner join KRT_Felhasznalok as KRT_Felhasznalok on KRT_Esemenyek.Felhasznalo_Id_User = KRT_Felhasznalok.Id
inner join KRT_Felhasznalok as KRT_Felhasznalok1 on KRT_Esemenyek.Felhasznalo_Id_Login = KRT_Felhasznalok1.Id 
left join KRT_Csoportok on KRT_Csoportok.Id = KRT_Esemenyek.Csoport_Id_FelelosUserSzerveze
inner join KRT_ObjTipusok on KRT_ObjTipusok.Id = KRT_Esemenyek.ObjTip_Id
inner join KRT_Funkciok as KRT_Funkciok on KRT_Esemenyek.Funkcio_Id = KRT_Funkciok.Id    
left join EREC_IraIratok on EREC_IraIratok.Id = KRT_Esemenyek.Obj_Id
left join EREC_PldIratPeldanyok on EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id and EREC_PldIratPeldanyok.Sorszam=1
left join EREC_KuldKuldemenyek on EREC_KuldKuldemenyek.Id = EREC_IraIratok.KuldKuldemenyek_Id
left join KRT_Helyettesitesek as KRT_Helyettesitesek on KRT_Esemenyek.Helyettesites_Id = KRT_Helyettesitesek.Id
           		where RowNumber between @firstRow and @lastRow
	ORDER BY #result.RowNumber;'
	
	set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter;';
	
	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int',@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end