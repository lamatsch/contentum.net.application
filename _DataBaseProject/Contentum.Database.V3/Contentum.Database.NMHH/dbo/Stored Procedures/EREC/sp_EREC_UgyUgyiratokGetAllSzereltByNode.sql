﻿CREATE procedure [dbo].[sp_EREC_UgyUgyiratokGetAllSzereltByNode]
		@NodeId	uniqueidentifier

as

begin

BEGIN TRY

    set nocount on;
    
    -- Elokészített szerelésre figyelni kell, azokat nem hozzuk
    -- (Azokat kell hozni, ahol UgyUgyirat_Id_Szulo ki van töltve, és vagy az Allapot, vagy a TovabbitasAlattAllapot szerelt(60) )
      
    --legfobb szülo megkeresése
	WITH ParentHiearchy  AS
	(
	   -- Base case
	   SELECT *
	   FROM EREC_UgyUgyiratok
	   WHERE Id = @NodeId

	   UNION ALL

	   -- Recursive step
	   SELECT u.*
	   FROM EREC_UgyUgyiratok as u
		  INNER JOIN ParentHiearchy as ph ON
			 u.Id = ph.UgyUgyirat_Id_Szulo
	   WHERE (ph.Allapot = '60' OR ph.TovabbitasAlattAllapot = '60')
	),
    
    -- szuló összes gyerekének megkeresése
	UgyiratHierarchy  AS
	(
	   -- Base case
	   SELECT *,
			  1 as HierarchyLevel
	   FROM ParentHiearchy
	   WHERE UgyUgyirat_Id_Szulo is NULL

	   UNION ALL

	   -- Recursive step
	   SELECT u.*,
		  uh.HierarchyLevel + 1 AS HierarchyLevel
	   FROM EREC_UgyUgyiratok as u
		  INNER JOIN UgyiratHierarchy as uh ON
			 u.UgyUgyirat_Id_Szulo = uh.Id
	   WHERE (u.Allapot = '60' OR u.TovabbitasAlattAllapot = '60')			 
	)

	SELECT *
	FROM UgyiratHierarchy
  

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end