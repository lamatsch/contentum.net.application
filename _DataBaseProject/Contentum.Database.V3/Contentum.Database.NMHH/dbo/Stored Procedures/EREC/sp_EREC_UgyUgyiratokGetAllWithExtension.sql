﻿CREATE procedure [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtension]
  @Where       nvarchar(MAX) = '',
  @Where_KuldKuldemenyek   nvarchar(MAX) = '',
  @Where_UgyUgyiratdarabok nvarchar(MAX) = '',
  @Where_IraIratok         nvarchar(MAX) = '',
  @Where_IraIktatokonyvek  nvarchar(MAX) = '',
  @Where_Dosszie        NVARCHAR(MAX) = '',
  @Where_EREC_IratMetaDefinicio  NVARCHAR(MAX) = '',
  @Where_Note NVARCHAR(MAX) = '',
  @ObjektumTargyszavai_ObjIdFilter  nvarchar(MAX) = '',
  --@FullTextSearch_Targy     nvarchar(MAX) = '',
  @Altalanos_FTS  nvarchar(MAX) = '',
  @OrderBy        nvarchar(400) = ' order by EREC_UgyUgyiratok.LetrehozasIdo',
  @TopRow         nvarchar(5) = '',
  @ExecutorUserId uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier,
  @Jogosultak     char(1) = '0',
  @ForMunkanaplo char(1) = '0',
  @Ugyintezo_Id_ForMunkanaplo uniqueidentifier = null,
  @pageNumber     int = 0,
  @pageSize       int = -1,
  @SelectedRowId  uniqueidentifier = NULL,
  @AlSzervezetId UNIQUEIDENTIFIER = NULL
  --- új paraméterek
  ,@Csoporttagokkal                  bit              = 1         -- Csak a saját jogon láthatja az iratokat: 0; vagy a csoporttagsága jogán is: 1 (default)
  ,@CsakAktivIrat                    bit              = 1         -- Csak az AKTIV Iratokat láttatjuk ( a fix 'erős' szűrő ): 1; vagy minden irat: 0 (default)
  ,@NaponBelulNincsUjAlszam int = null   -- a megadot napon belül nem született új alszám
as

begin

BEGIN TRY
   set nocount ON

   --DECLARE @Log INT
   --SET @Log = 1

   --IF @Log = 1
   --BEGIN
   -- INSERT INTO Log_EREC_UgyUgyiratokGetAllWithExtension
   -- (
   --    [WHERE],
   --    [Where_KuldKuldemenyek],
   --    [Where_UgyUgyiratdarabok],
   --    [Where_IraIratok],
   --    [Where_IraIktatokonyvek],
   --    [Where_Dosszie],
   --    [ObjektumTargyszavai_ObjIdFilter],
   --    [Altalanos_FTS],
   --    [OrderBy],
   --    [TopRow],
   --    [ExecutorUserId],
   --    [FelhasznaloSzervezet_Id],
   --    [Jogosultak],
   --    [ForMunkanaplo],
   --    [pageNumber],
   --    [pageSize],
   --    [SelectedRowId],
   --    [AlSzervezetId]
   -- )
   -- VALUES
   -- (
   --     @Where,
   --     @Where_KuldKuldemenyek,
   --     @Where_UgyUgyiratdarabok,
   --     @Where_IraIratok,
   --     @Where_IraIktatokonyvek,
   --     @Where_Dosszie,
   --     @ObjektumTargyszavai_ObjIdFilter,
   --     @Altalanos_FTS,
   --     @OrderBy,
   --     @TopRow,
   --     @ExecutorUserId,
   --     @FelhasznaloSzervezet_Id,
   --     @Jogosultak,
   --     @ForMunkanaplo,
   --     @pageNumber,
   --     @pageSize,
   --     @SelectedRowId,
   --     @AlSzervezetId
   -- )
   --END

   DECLARE @Org uniqueidentifier
   SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
   if (@Org is null)
   begin
      RAISERROR('[50202]',16,1)
   end

   DECLARE @OrgKod nvarchar(100)
   set @OrgKod = (select Kod from KRT_Orgok where Id = @Org)

   -- CR3442
   --declare @OrgIsBOMPH int

   --IF(@OrgKod = 'BOPMH')
	  --set @OrgIsBOMPH = 1
   -- ELSE
   --   set @OrgIsBOMPH = 0

   if @FelhasznaloSzervezet_Id is null
      RAISERROR('[52593]', 16, 1) -- Nincs megadva a felhasználó szervezete!

   DECLARE @sqlcmd nvarchar(MAX)
   DECLARE @sqlCTL nvarchar(MAX);                                          --- BogI CR3428
   DECLARE @LocalTopRow nvarchar(10)
   declare @firstRow int
   declare @lastRow INT

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
      if (@pageSize > @TopRow)
         SET @pageSize = @TopRow;
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

   if (@pageSize > 0 and @pageNumber > -1)
   begin
      set @firstRow = (@pageNumber)*@pageSize + 1
      set @lastRow = @firstRow + @pageSize - 1
   end
   else begin
      if (@TopRow = '' or @TopRow = '0')
      begin
         set @firstRow = 1
         set @lastRow = ISNULL((SELECT TOP (1) Ertek FROM KRT_Parameterek WHERE Nev = 'LISTA_ELEMSZAM_MAX'),1000);
      end
      else
      begin
         set @firstRow = 1
         set @lastRow = @TopRow
      end
   END

   DECLARE @FelhasznaloId UNIQUEIDENTIFIER
   DECLARE @SzervezetId UNIQUEIDENTIFIER

   IF @AlSzervezetId IS NULL
   BEGIN
      SET @FelhasznaloId = @ExecutorUserId
      SET @SzervezetId = @FelhasznaloSzervezet_Id
   END
   ELSE
   BEGIN
      SET @FelhasznaloId = dbo.fn_GetLeader(@AlSzervezetId)

      IF @FelhasznaloId is NULL
      BEGIN
         -- Az alszervezetnek nincsen vezetoje
         RAISERROR('[53203]',16,1)
      END

      SET @SzervezetId = @AlSzervezetId
   END

   set @sqlcmd = '';
   declare @isAdminInSzervezet int
   set @isAdminInSzervezet = dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id)

   /************************************************************
   * @Altalanos_FTS tartalmából szûrõfeltételek összeállítása  *
   ************************************************************/
--    declare @Where_Altalanos nvarchar(MAX)
--    set @Where_Altalanos = ''
--
--    if @Altalanos_FTS is not null and @Altalanos_FTS != ''
--    begin
--        set @Where_Altalanos = '(contains(EREC_KuldKuldemenyek.NevSTR_Bekuldo, ''' + @Altalanos_FTS + ''' )'
--                             + ' or contains(EREC_IraIratok.Targy, ''' + @Altalanos_FTS + ''' )'
--                             + ' or contains(EREC_UgyUgyiratok.Targy, ''' + @Altalanos_FTS + ''' ))'
--    end
   /************************************************************
   * Szûrési feltételek sorrendjének összeállítása          *
   ************************************************************/
   DECLARE @ObjTipId uniqueidentifier;
   select @ObjTipId = Id from KRT_Objtipusok where Kod = 'EREC_UgyUgyiratok';

   create table #filterOrder (Filter nvarchar(50), RowNumber bigint);

-- IF (@FullTextSearch_Targy != '')
-- BEGIN
--    SET @sqlcmd = @sqlcmd + ' insert into #filterOrder SELECT ''FullTextSearch_Targy'',count(Id) FROM EREC_UgyUgyiratok WHERE contains(EREC_UgyUgyiratok.Targy , ''' + @FullTextSearch_Targy + '''  );'
-- END

   IF (@Where_IraIktatokonyvek != '')
   BEGIN
      SET @sqlcmd = @sqlcmd + N' insert into #filterOrder select ''Where_IraIktatokonyvek'',
            count(erec_ugyugyiratok.id)
               from erec_ugyugyiratok
                  inner join erec_iraiktatokonyvek on erec_iraiktatokonyvek.Id = erec_ugyugyiratok.IraIktatokonyv_Id and (' + @Where_IraIktatokonyvek + ') ';

   END



   IF @Where_KuldKuldemenyek is not null and @Where_KuldKuldemenyek != ''
   BEGIN
      SET @sqlcmd = @sqlcmd + N' insert into #filterOrder select ''Where_KuldKuldemenyek'',
         count ([EREC_UgyUgyiratdarabok].[UgyUgyirat_Id])
         FROM [EREC_UgyUgyiratdarabok]
            JOIN [EREC_IraIratok] ON [EREC_UgyUgyiratdarabok].[Id] = [EREC_IraIratok].[UgyUgyIratDarab_Id]
            JOIN [EREC_KuldKuldemenyek] ON [EREC_IraIratok].[KuldKuldemenyek_Id] = [EREC_KuldKuldemenyek].[Id]
            LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Kuld on EREC_KuldKuldemenyek.IraIktatokonyv_Id = EREC_IraIktatoKonyvek_Kuld.Id and EREC_IraIktatoKonyvek_Kuld.Org=@Org
         WHERE GETDATE() BETWEEN [EREC_UgyUgyiratdarabok].[ErvKezd] AND [EREC_UgyUgyiratdarabok].[ErvVege]
            AND GETDATE() BETWEEN [EREC_IraIratok].[ErvKezd] AND [EREC_IraIratok].[ErvVege]
            AND GETDATE() BETWEEN [EREC_KuldKuldemenyek].[ErvKezd] AND [EREC_KuldKuldemenyek].[ErvVege]
            AND '+ @Where_KuldKuldemenyek
   END

   IF @Where_UgyUgyiratdarabok is not null and @Where_UgyUgyiratdarabok != ''
   BEGIN
      SET @sqlcmd = @sqlcmd + ' insert into #filterOrder select ''Where_UgyUgyiratdarabok'',
         count ([EREC_UgyUgyiratdarabok].[UgyUgyirat_Id])
         FROM [EREC_UgyUgyiratdarabok]
         WHERE GETDATE() BETWEEN [EREC_UgyUgyiratdarabok].[ErvKezd] AND [EREC_UgyUgyiratdarabok].[ErvVege]
            AND ' + @Where_UgyUgyiratdarabok
   END

   if @Where_IraIratok is not null and @Where_IraIratok != ''
   BEGIN
      SET @sqlcmd = @sqlcmd + ' insert into #filterOrder select ''Where_IraIratok'',
         count ([EREC_UgyUgyiratdarabok].[UgyUgyirat_Id])
         FROM EREC_IraIratok
            INNER JOIN [EREC_UgyUgyiratdarabok] ON [EREC_UgyUgyiratdarabok].Id = EREC_IraIratok.UgyUgyiratdarab_Id
         WHERE GETDATE() BETWEEN [EREC_UgyUgyiratdarabok].[ErvKezd] AND [EREC_UgyUgyiratdarabok].[ErvVege]
            AND GETDATE() BETWEEN EREC_IraIratok.ErvKezd AND EREC_IraIratok.ErvVege
            AND ' + @Where_IraIratok
   END

   if @Altalanos_FTS is not null and @Altalanos_FTS != ''
   BEGIN
--    SET @sqlcmd = @sqlcmd + ' insert into #filterOrder select ''Where_Altalanos'',
--       count ([EREC_UgyUgyiratok].[Id])
--       FROM EREC_IraIratok
--          INNER JOIN [EREC_UgyUgyiratdarabok] ON [EREC_UgyUgyiratdarabok].Id = EREC_IraIratok.UgyUgyiratdarab_Id
--                INNER JOIN [EREC_UgyUgyiratok] ON [EREC_UgyUgyiratok].Id = [EREC_UgyUgyiratdarabok].UgyUgyirat_Id
--                LEFT JOIN [EREC_KuldKuldemenyek] ON [EREC_KuldKuldemenyek].Id = [EREC_IraIratok].KuldKuldemenyek_Id
--                     AND GETDATE() BETWEEN [EREC_KuldKuldemenyek].[ErvKezd] AND [EREC_KuldKuldemenyek].[ErvVege]
--       WHERE GETDATE() BETWEEN [EREC_UgyUgyiratdarabok].[ErvKezd] AND [EREC_UgyUgyiratdarabok].[ErvVege]
--          AND GETDATE() BETWEEN EREC_IraIratok.ErvKezd AND EREC_IraIratok.ErvVege
--                AND GETDATE() BETWEEN [EREC_UgyUgyiratok].[ErvKezd] AND [EREC_UgyUgyiratok].[ErvVege]
--          AND ' + @Where_Altalanos

-- hatékonyságnövelés: join helyett union
   SET @sqlcmd = @sqlcmd + ' insert into #filterOrder select ''Where_Altalanos'',
      count([tmp].[Id]) FROM (
      select [EREC_UgyUgyiratok].[Id]
      FROM [EREC_UgyUgyiratok] where contains(EREC_UgyUgyiratok.Targy, ''' + @Altalanos_FTS + ''' ) and GETDATE() BETWEEN [EREC_UgyUgyiratok].[ErvKezd] AND [EREC_UgyUgyiratok].[ErvVege]
      union
      select [EREC_UgyUgyiratok].[Id]
      FROM [EREC_UgyUgyiratok] where contains(EREC_UgyUgyiratok.NevSTR_Ugyindito, ''' + @Altalanos_FTS + ''' ) and GETDATE() BETWEEN [EREC_UgyUgyiratok].[ErvKezd] AND [EREC_UgyUgyiratok].[ErvVege]
      union
      select [EREC_UgyUgyiratok].[Id]
      FROM [EREC_UgyUgyiratok] where
      [EREC_UgyUgyiratok].Id in (select EREC_IraIratok.Ugyirat_Id from [EREC_IraIratok]
      where contains(EREC_IraIratok.Targy, ''' + @Altalanos_FTS + ''' ) and GETDATE() BETWEEN [EREC_IraIratok].[ErvKezd] AND [EREC_IraIratok].[ErvVege])
      union
      select [EREC_UgyUgyiratok].[Id]
      FROM [EREC_UgyUgyiratok] where
      [EREC_UgyUgyiratok].Id in (select EREC_IraIratok.Ugyirat_Id from [EREC_IraIratok]
      where GETDATE() BETWEEN [EREC_IraIratok].[ErvKezd] AND [EREC_IraIratok].[ErvVege] and [EREC_IraIratok].KuldKuldemenyek_Id in
      (select EREC_KuldKuldemenyek.Id from EREC_KuldKuldemenyek where contains(EREC_KuldKuldemenyek.Targy, ''' + @Altalanos_FTS + ''' ) and  GETDATE() BETWEEN [EREC_KuldKuldemenyek].[ErvKezd] AND [EREC_KuldKuldemenyek].[ErvVege]))
      union
      select [EREC_UgyUgyiratok].[Id]
      FROM [EREC_UgyUgyiratok] where
      [EREC_UgyUgyiratok].Id in (select EREC_IraIratok.Ugyirat_Id from [EREC_IraIratok]
      where GETDATE() BETWEEN [EREC_IraIratok].[ErvKezd] AND [EREC_IraIratok].[ErvVege] and [EREC_IraIratok].KuldKuldemenyek_Id in
      (select EREC_KuldKuldemenyek.Id from EREC_KuldKuldemenyek where contains(EREC_KuldKuldemenyek.NevSTR_Bekuldo, ''' + @Altalanos_FTS + ''' ) and  GETDATE() BETWEEN [EREC_KuldKuldemenyek].[ErvKezd] AND [EREC_KuldKuldemenyek].[ErvVege]))) tmp'

   END

   if @Where_Note is not null and @Where_Note != ''
   BEGIN

-- hatékonyságnövelés: join helyett union
   SET @sqlcmd = @sqlcmd + ' insert into #filterOrder select ''Where_Note'',
      count([tmp].[Id]) FROM (
      select [EREC_UgyUgyiratok].[Id]
      FROM [EREC_UgyUgyiratok] where contains(EREC_UgyUgyiratok.Note, ''' + @Where_Note + ''' ) and GETDATE() BETWEEN [EREC_UgyUgyiratok].[ErvKezd] AND [EREC_UgyUgyiratok].[ErvVege]
      ) tmp'

   END

   if @Where_Dosszie is not null and @Where_Dosszie != ''
   BEGIN
      SET @sqlcmd = @sqlcmd + ' insert into #filterOrder select ''Where_Dosszie'',
         count (KRT_MappaTartalmak.Obj_Id)
         FROM KRT_MappaTartalmak
            INNER JOIN KRT_Mappak on KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
         WHERE GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
            AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
            AND KRT_MappaTartalmak.Obj_type = ''EREC_UgyUgyiratok''
            AND ' + @Where_Dosszie
   END




   if @Where_EREC_IratMetaDefinicio is not null and @Where_EREC_IratMetaDefinicio != ''
   BEGIN
      SET @sqlcmd = @sqlcmd + ' insert into #filterOrder select ''Where_EREC_IratMetaDefinicio'',
         count (EREC_UgyUgyiratok.Id)
         FROM EREC_UgyUgyiratok
            LEFT JOIN EREC_IratMetaDefinicio on EREC_IratMetaDefinicio.Id = EREC_UgyUgyiratok.IratMetadefinicio_Id
         WHERE ' + @Where_EREC_IratMetaDefinicio
   END




   --exec sp_executesql @sqlcmd;
   execute sp_executesql @sqlcmd,N'@Org uniqueidentifier',@Org = @Org;

   /************************************************************
   * Szûrési tábla összeállítása                      *
   ************************************************************/
   declare @whereFilter nvarchar(50);
   declare cur cursor for
      select Filter from #filterOrder order by RowNumber DESC;

   /* --- */
   SET @sqlcmd = '; ';
   SET @sqlCTL = '; ';
   if @Csoporttagokkal = 1
     SET @sqlCTL += 'with CsoportTagokAll as ( select distinct Id from dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId) ) ';       --- EL KÉNE VÉGRE dönteni !?  ---@ExecutorUserId, @FelhasznaloSzervezet_Id) ) ';
   else
     SET @sqlCTL += 'with CsoportTagokAll as ( select distinct Id
	                                             from dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId)
												where Id = @FelhasznaloId
											  ) ';

   SET @sqlCTL += ', jogosult_objektumok as
                     ( select krt_jogosultak.Obj_Id, Kezi, Tipus
	                     from krt_jogosultak
		                INNER JOIN CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
                     )
				   , mappa_tartalmak as
                     ( SELECT KRT_MappaTartalmak.Obj_Id
	                     FROM KRT_MappaTartalmak
	                    INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
	                    INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = KRT_Mappak.Id
	                    where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
	                 ) ';
   /* ---
   SET @sqlcmd = ';with mappa_tartalmak as
               (
                  SELECT KRT_MappaTartalmak.Obj_Id
                  FROM KRT_MappaTartalmak
                  INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
                  INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = KRT_Mappak.Id

                   INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany

                  where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
                  and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
               )
   --- */
   SET @sqlcmd += @sqlCTL + 'SELECT distinct erec_ugyugyiratok.Id into #filter from erec_ugyugyiratok
         left join (SELECT UgyUgyirat_Id_Szulo, COUNT(*) as ElokeszitettSzerelesCount FROM EREC_UgyUgyiratok as EREC_UgyUgyiratok_Szereltek
         WHERE EREC_UgyUgyiratok_Szereltek.Allapot != 60
            AND ISNULL(EREC_UgyUgyiratok_Szereltek.TovabbitasAlattAllapot,-1) != 60
            AND UgyUgyirat_Id_Szulo is not null
         GROUP BY UgyUgyirat_Id_Szulo
         ) AS EREC_UgyUgyiratok_ElokeszitettSzerelesCount
         on EREC_UgyUgyiratok.Id = EREC_UgyUgyiratok_ElokeszitettSzerelesCount.UgyUgyirat_Id_Szulo
               left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek
                  ON EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id
               left join EREC_IraIrattariTetelek as EREC_IraIrattariTetelek
                  ON EREC_UgyUgyiratok.IraIrattariTetel_Id = EREC_IraIrattariTetelek.Id
               left join EREC_AgazatiJelek as EREC_Agazatijelek
                  ON EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_Agazatijelek.Id
               left join EREC_UgyUgyiratok as EREC_UgyUgyiratok_Szulo
                  ON EREC_UgyUgyiratok_Szulo.Id = EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo
               left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek_Szulo
                  ON EREC_UgyUgyiratok_Szulo.IraIktatokonyv_Id = EREC_IraIktatokonyvek_Szulo.Id
                left join EREC_IrattariKikero as EREC_IrattariKikero
                  ON EREC_UgyUgyiratok.Id = EREC_IrattariKikero.UgyUgyirat_Id
            where EREC_IraIktatokonyvek.Org=@Org
'
   if @Where is not null and @Where!=''
     SET @sqlcmd = @sqlcmd + ' and ' + @Where;
   /* --- */
   if @CsakAktivIrat = 1
		BEGIN
			SET @sqlcmd +=[dbo].[fn_GetAktivFilterValue] ( 'UG')
			SET @sqlcmd += char(13);
		END
   /* --- */

   if @ForMunkanaplo = '1'
   begin

      declare @Munkanaplo_Irat_Ugyintezo_Enabled varchar(400)
      set @Munkanaplo_Irat_Ugyintezo_Enabled=(select Ertek from KRT_Parameterek
         where Nev='MUNKANAPLO_IRAT_UGYINTEZO_ENABLED'
         and Org=@Org
         and getdate() between ErvKezd and ErvVege)

      if @Ugyintezo_Id_ForMunkanaplo is null
      begin
         set @Ugyintezo_Id_ForMunkanaplo = @ExecutorUserId
      end

      if @Munkanaplo_Irat_Ugyintezo_Enabled in ('1','i','true','igen')
      begin
            SET @sqlcmd = @sqlcmd + '
AND erec_ugyugyiratok.Id IN
(  select uu.Id from EREC_UgyUgyiratok uu
   where uu.FelhasznaloCsoport_Id_Ugyintez=@Ugyintezo_Id_ForMunkanaplo
   UNION ALL
   select uu.Id from EREC_UgyUgyiratok uu
   where uu.FelhasznaloCsoport_Id_Ugyintez<>@Ugyintezo_Id_ForMunkanaplo
   and exists(select 1 from EREC_IraIratok i where i.Ugyirat_Id=uu.Id and i.FelhasznaloCsoport_Id_Ugyintez = @Ugyintezo_Id_ForMunkanaplo)
)
'
      end
      else
      begin
            SET @sqlcmd = @sqlcmd + '
AND erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez=@Ugyintezo_Id_ForMunkanaplo
'
      end
   end

   --IF @Jogosultak = '1' AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
   IF @Jogosultak = '1' AND @isAdminInSzervezet = 0
   BEGIN
      SET @sqlcmd = @sqlcmd + ' AND erec_ugyugyiratok.Id IN
                           (
                              SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
							     /* --- */
								 inner join jogosult_objektumok as jogosult_objektumok on jogosult_objektumok.Obj_id = erec_ugyugyiratok.Id
								 /* ---
                                 INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
                                 INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
								 --- */ '

	  /* --- */
	  SET @sqlcmd += dbo.fn_GetJogosultakFilter(@Org, 'jogosult_objektumok');
	  /* ---- SET @sqlcmd = @sqlcmd + dbo.fn_GetJogosultakFilter(@Org, 'krt_jogosultak') --- */

	  SET @sqlcmd = @sqlcmd + '
                              UNION ALL
                              SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
							     /* --- */
								 inner join jogosult_objektumok as jogosult_objektumok on jogosult_objektumok.Obj_id = erec_ugyugyiratok.IraIktatokonyv_Id
								 /* ---
                                 INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
                                 INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
								 --- */
                              UNION ALL
                              SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
							     /* --- */
								 inner join CsoportTagokAll as CsoportTagokAll on CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
								 /* ---
                                 INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
								 --- */
                              UNION ALL
                              SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
							     /* --- */
								 inner join CsoportTagokAll as CsoportTagokAll on CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
								 /* ---
                                 INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
								 --- */
                              UNION ALL
                              SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
							     /* --- */
								 inner join CsoportTagokAll as CsoportTagokAll on CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo '+char(13);
								 /* ---
                                 INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId) as CsoportTagokAll ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
								 --- */
        if @Csoporttagokkal = 1                               --- BogI CR3428
          SET @sqlcmd += '    UNION
                              SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@FelhasznaloId, @SzervezetId, ''EREC_UgyUgyiratok'') '+char(13);
        else
          SET @sqlcmd += '    UNION
                              SELECT Id FROM dbo.fn_GetObjIdsFromFeladatokByFelhasznalo(@FelhasznaloId, ''EREC_UgyUgyiratok'') '+char(13);

        SET @sqlcmd += '      UNION
							  SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                                 INNER JOIN mappa_tartalmak ON mappa_tartalmak.Obj_Id = erec_ugyugyiratok.Id
                           )'

      -- Ha (átmeneti v. központi) irattárban van az ügyirat és a felhasználó nem vezetõ és nem irattáros, valamint a
      -- szervezet dolgozói egymás tételeit nem láthatják, akkor csak a saját ügyintézésû ügyiratokat láthatja
      if dbo.fn_IsLeaderInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
      begin
         if dbo.fn_HasFunctionRight(@ExecutorUserId,@FelhasznaloSzervezet_Id, 'IrattarKolcsonzesKiadasa') = 0
            and dbo.fn_HasFunctionRight(@ExecutorUserId,@FelhasznaloSzervezet_Id, 'AtmenetiIrattarAtadasKozpontiIrattarba') = 0
         begin
            -- Irattár szûrés, In és nem NOT IN!
            -- Irattározott állapotok: Irattárba küldött, Irattárban õrzött, Irattárból elkért, Átmeneti irattárból kikölcsönzött, Engedélyezett kikérõn lévõ
             SET @sqlcmd = @sqlcmd+'; ';
/* BogI CR3428
   if @Csoporttagokkal = 1
   BEGIN
     SET @sqlcmd += 'with CsoportTagokAll as ( select distinct Id from dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId) ) '       --- EL KÉNE VÉGRE dönteni !?  ---@ExecutorUserId, @FelhasznaloSzervezet_Id) ) ';
   END
   else
   BEGIN
     SET @sqlcmd += 'with CsoportTagokAll as ( select distinct Id
	                                             from dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@FelhasznaloId, @SzervezetId)
												where Id = @FelhasznaloId
											  ) '
   END
   SET @sqlcmd += ', jogosult_objektumok as
                     ( select krt_jogosultak.Obj_Id, Kezi, Tipus
	                     from krt_jogosultak
		                INNER JOIN CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
                     )
				   , mappa_tartalmak as
                     ( SELECT KRT_MappaTartalmak.Obj_Id
	                     FROM KRT_MappaTartalmak
	                    INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
	                    INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = KRT_Mappak.Id
	                    where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
	                 ) '
            SET @sqlcmd = @sqlcmd + '
*/
            SET @sqlcmd += @sqlCTL + '
                     DELETE FROM #filter WHERE Id IN
                     (
                        select Id from EREC_UgyUgyiratok where
                        (Allapot in (''11'', ''10'', ''55'', ''54'', ''56'') or TovabbitasAlattAllapot in (''11'', ''10'', ''55'', ''54'', ''56''))
                        and Csoport_Id_Felelos in (select Id from KRT_Csoportok where IsNull(JogosultsagOroklesMod, ''0'') = ''0'')
                        and EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez != @ExecutorUserId
                        and EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != @ExecutorUserId
                        and EREC_UgyUgyiratok.Id not in
                        (
                           SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                            /* --- */
							inner join jogosult_objektumok as jogosult_objektumok on jogosult_objektumok.Obj_id = erec_ugyugyiratok.Id
							/* ---
                            INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
                            INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
							--- */
                           UNION ALL
                           SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
                            /* --- */
							inner join jogosult_objektumok as jogosult_objektumok on jogosult_objektumok.Obj_id = erec_ugyugyiratok.IraIktatokonyv_Id '+char(13);
							/* ---
                              INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
                              INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
							--- */
            if @Csoporttagokkal = 1
              SET @sqlcmd += '         UNION
                                       SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'') '+char(13);
            else
              SET @sqlcmd += '         UNION
                                       SELECT Id FROM dbo.fn_GetObjIdsFromFeladatokByFelhasznalo(@ExecutorUserId, ''EREC_UgyUgyiratok'') '+char(13);
            SET @sqlcmd +=
			           ')
                     )'
         end
      end
   END

   open cur;
   fetch next from cur into @whereFilter;
   while @@FETCH_STATUS = 0
   BEGIN
--    IF @whereFilter = 'FullTextSearch_Targy'
--       set @sqlcmd = @sqlcmd + ' delete from #filter where Id in (SELECT Id FROM EREC_UgyUgyiratok WHERE not contains(EREC_UgyUgyiratok.Targy , ''' + @FullTextSearch_Targy + '''  ));'

      IF @whereFilter = 'Where_IraIktatokonyvek'
         SET @sqlcmd = @sqlcmd + N' delete from #filter where Id in
            (
               select erec_ugyugyiratok.id
                  from erec_ugyugyiratok
                     inner join erec_iraiktatokonyvek on erec_iraiktatokonyvek.Id = erec_ugyugyiratok.IraIktatokonyv_Id and not (' + @Where_IraIktatokonyvek + '));'


      IF @whereFilter = 'Where_KuldKuldemenyek'
         SET @sqlcmd = @sqlcmd + N'delete from #filter where Id not in (
            SELECT distinct [EREC_UgyUgyiratdarabok].[UgyUgyirat_Id]
            FROM [EREC_UgyUgyiratdarabok]
               JOIN [EREC_IraIratok] ON [EREC_UgyUgyiratdarabok].[Id] = [EREC_IraIratok].[UgyUgyIratDarab_Id]
               JOIN [EREC_KuldKuldemenyek] ON [EREC_IraIratok].[KuldKuldemenyek_Id] = [EREC_KuldKuldemenyek].[Id]
               LEFT JOIN EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Kuld on EREC_KuldKuldemenyek.IraIktatokonyv_Id = EREC_IraIktatoKonyvek_Kuld.Id
            WHERE GETDATE() BETWEEN [EREC_UgyUgyiratdarabok].[ErvKezd] AND [EREC_UgyUgyiratdarabok].[ErvVege]
               AND GETDATE() BETWEEN [EREC_IraIratok].[ErvKezd] AND [EREC_IraIratok].[ErvVege]
               AND GETDATE() BETWEEN [EREC_KuldKuldemenyek].[ErvKezd] AND [EREC_KuldKuldemenyek].[ErvVege]
               AND '+ @Where_KuldKuldemenyek +')';

      IF @whereFilter = 'Where_UgyUgyiratdarabok'
         SET @sqlcmd = @sqlcmd + 'delete from #filter where Id in (
            SELECT [EREC_UgyUgyiratdarabok].[UgyUgyirat_Id]
            FROM [EREC_UgyUgyiratdarabok]
            WHERE GETDATE() BETWEEN [EREC_UgyUgyiratdarabok].[ErvKezd] AND [EREC_UgyUgyiratdarabok].[ErvVege]
               AND not ' + @Where_UgyUgyiratdarabok +')'

      IF @whereFilter = 'Where_IraIratok'
         SET @sqlcmd = @sqlcmd + 'delete from #filter where Id not in (
            SELECT [EREC_UgyUgyiratdarabok].[UgyUgyirat_Id]
            FROM EREC_IraIratok
               INNER JOIN [EREC_UgyUgyiratdarabok] ON EREC_IraIratok.UgyUgyiratdarab_Id = [EREC_UgyUgyiratdarabok].Id
            WHERE GETDATE() BETWEEN [EREC_UgyUgyiratdarabok].[ErvKezd] AND [EREC_UgyUgyiratdarabok].[ErvVege]
               AND GETDATE() BETWEEN EREC_IraIratok.ErvKezd AND EREC_IraIratok.ErvVege
               AND ' + @Where_IraIratok +'); '

--    IF @whereFilter = 'Where_Altalanos'
--       SET @sqlcmd = @sqlcmd + 'delete from #filter where Id not in (
--          SELECT [EREC_UgyUgyiratok].[Id]
--          FROM EREC_IraIratok
--          INNER JOIN [EREC_UgyUgyiratdarabok] ON [EREC_UgyUgyiratdarabok].Id = [EREC_IraIratok].UgyUgyiratdarab_Id
--                INNER JOIN [EREC_UgyUgyiratok] ON [EREC_UgyUgyiratok].Id = [EREC_UgyUgyiratdarabok].UgyUgyirat_Id
--                LEFT JOIN [EREC_KuldKuldemenyek] ON [EREC_KuldKuldemenyek].Id = [EREC_IraIratok].KuldKuldemenyek_Id
--                     AND GETDATE() BETWEEN [EREC_KuldKuldemenyek].[ErvKezd] AND [EREC_KuldKuldemenyek].[ErvVege]
--       WHERE GETDATE() BETWEEN [EREC_UgyUgyiratdarabok].[ErvKezd] AND [EREC_UgyUgyiratdarabok].[ErvVege]
--          AND GETDATE() BETWEEN EREC_IraIratok.ErvKezd AND EREC_IraIratok.ErvVege
--                AND GETDATE() BETWEEN [EREC_UgyUgyiratok].[ErvKezd] AND [EREC_UgyUgyiratok].[ErvVege]
--          AND ' + @Where_Altalanos +'); '

-- hatékonyságnövelés: join helyett union
      IF @whereFilter = 'Where_Altalanos'
         SET @sqlcmd = @sqlcmd + 'delete from #filter where Id not in (
      select [EREC_UgyUgyiratok].[Id]
      FROM [EREC_UgyUgyiratok] where contains(EREC_UgyUgyiratok.Targy, ''' + @Altalanos_FTS + ''' ) and GETDATE() BETWEEN [EREC_UgyUgyiratok].[ErvKezd] AND [EREC_UgyUgyiratok].[ErvVege]
      union
      select [EREC_UgyUgyiratok].[Id]
      FROM [EREC_UgyUgyiratok] where contains(EREC_UgyUgyiratok.NevSTR_Ugyindito, ''' + @Altalanos_FTS + ''' ) and GETDATE() BETWEEN [EREC_UgyUgyiratok].[ErvKezd] AND [EREC_UgyUgyiratok].[ErvVege]
      union
      select [EREC_UgyUgyiratok].[Id]
      FROM [EREC_UgyUgyiratok] where
      [EREC_UgyUgyiratok].Id in (select EREC_IraIratok.Ugyirat_Id from [EREC_IraIratok]
      where contains(EREC_IraIratok.Targy, ''' + @Altalanos_FTS + ''' ) and GETDATE() BETWEEN [EREC_IraIratok].[ErvKezd] AND [EREC_IraIratok].[ErvVege])
      union
      select [EREC_UgyUgyiratok].[Id]
      FROM [EREC_UgyUgyiratok] where
      [EREC_UgyUgyiratok].Id in (select EREC_IraIratok.Ugyirat_Id from [EREC_IraIratok]
      where GETDATE() BETWEEN [EREC_IraIratok].[ErvKezd] AND [EREC_IraIratok].[ErvVege] and [EREC_IraIratok].KuldKuldemenyek_Id in
      (select EREC_KuldKuldemenyek.Id from EREC_KuldKuldemenyek where contains(EREC_KuldKuldemenyek.Targy, ''' + @Altalanos_FTS + ''' ) and  GETDATE() BETWEEN [EREC_KuldKuldemenyek].[ErvKezd] AND [EREC_KuldKuldemenyek].[ErvVege]))
      union
      select [EREC_UgyUgyiratok].[Id]
      FROM [EREC_UgyUgyiratok] where
      [EREC_UgyUgyiratok].Id in (select EREC_IraIratok.Ugyirat_Id from [EREC_IraIratok]
      where GETDATE() BETWEEN [EREC_IraIratok].[ErvKezd] AND [EREC_IraIratok].[ErvVege] and [EREC_IraIratok].KuldKuldemenyek_Id in
      (select EREC_KuldKuldemenyek.Id from EREC_KuldKuldemenyek where contains(EREC_KuldKuldemenyek.NevSTR_Bekuldo, ''' + @Altalanos_FTS + ''' ) and  GETDATE() BETWEEN [EREC_KuldKuldemenyek].[ErvKezd] AND [EREC_KuldKuldemenyek].[ErvVege])))'

	   IF @whereFilter = 'Where_Note'
         SET @sqlcmd = @sqlcmd + 'delete from #filter where Id not in (
      select [EREC_UgyUgyiratok].[Id]
      FROM [EREC_UgyUgyiratok] where contains(EREC_UgyUgyiratok.Note, ''' + @Where_Note + ''' ) and GETDATE() BETWEEN [EREC_UgyUgyiratok].[ErvKezd] AND [EREC_UgyUgyiratok].[ErvVege]
   )'


      IF @whereFilter = 'Where_Dosszie'
         SET @sqlcmd = @sqlcmd + ' delete from #filter where Id not in (
            SELECT KRT_MappaTartalmak.Obj_Id
            FROM KRT_MappaTartalmak
               INNER JOIN KRT_Mappak on KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
            WHERE GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
               AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
               AND KRT_MappaTartalmak.Obj_type = ''EREC_UgyUgyiratok''
               AND ' + @Where_Dosszie + '); '

      IF @whereFilter = 'Where_EREC_IratMetaDefinicio'
      SET @sqlcmd = @sqlcmd + ' delete from #filter where Id not in (
         SELECT EREC_UgyUgyiratok.Id
         FROM EREC_UgyUgyiratok
            LEFT JOIN EREC_IratMetaDefinicio on EREC_IratMetaDefinicio.Id = EREC_UgyUgyiratok.IratMetadefinicio_Id
         WHERE ' + @Where_EREC_IratMetaDefinicio + '); '



      fetch next from cur into @whereFilter;
   END
   close cur;
   deallocate cur;

   DROP TABLE #filterOrder

   IF @ObjektumTargyszavai_ObjIdFilter is not null and @ObjektumTargyszavai_ObjIdFilter! = ''
    BEGIN
      SET @sqlcmd = @sqlcmd + N' delete from #filter where Id not in (' + @ObjektumTargyszavai_ObjIdFilter + ' AND getdate() BETWEEN EREC_ObjektumTargyszavai.ErvKezd AND EREC_ObjektumTargyszavai.ErvVege) '
    END

	IF @NaponBelulNincsUjAlszam is NOT NULL
	BEGIN
		declare @IratLetrehozasido datetime
		set @IratLetrehozasido = DATEADD(DAY,-1 * @NaponBelulNincsUjAlszam, GETDATE())
		set @IratLetrehozasido = cast(@IratLetrehozasido as date)

		SET @sqlcmd = @sqlcmd + ' delete from #filter where Id in
	     (SELECT EREC_UgyUgyiratok.Id
		 FROM EREC_UgyUgyiratok
		 join EREC_IraIratok on EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id
		 where EREC_IraIratok.LetrehozasIdo > @IratLetrehozasido) '
	END


   /************************************************************
   * Szûrt adatokhoz rendezés és sorszám összeállítása         *
   ************************************************************/
   	--- Fontos: az itt megfogalmazott kapcsolatoknak egyezniük kell a '* Tényleges select' során, az egyes adatmezők lekéréséhez felépített kapcsolatokkal
      SET @sqlcmd = @sqlcmd + N'
      CREATE TABLE #result(RowNumber bigint, Id uniqueidentifier)
      insert into #result
      select
      row_number() over('+@OrderBy+') as RowNumber,
      EREC_UgyUgyiratok.Id
       from EREC_UgyUgyiratok as EREC_UgyUgyiratok
         left join (SELECT UgyUgyirat_Id_Szulo, COUNT(*) as ElokeszitettSzerelesCount FROM EREC_UgyUgyiratok as EREC_UgyUgyiratok_Szereltek
         WHERE EREC_UgyUgyiratok_Szereltek.Allapot != 60
            AND ISNULL(EREC_UgyUgyiratok_Szereltek.TovabbitasAlattAllapot,-1) != 60
            AND UgyUgyirat_Id_Szulo is not null
            AND UgyUgyirat_Id_Szulo in (select Id from #filter)
         GROUP BY UgyUgyirat_Id_Szulo
         ) AS EREC_UgyUgyiratok_ElokeszitettSzerelesCount
            on EREC_UgyUgyiratok.Id = EREC_UgyUgyiratok_ElokeszitettSzerelesCount.UgyUgyirat_Id_Szulo
         left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek
            ON EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id
         left join EREC_IraIrattariTetelek as EREC_IraIrattariTetelek
            ON EREC_UgyUgyiratok.IraIrattariTetel_Id = EREC_IraIrattariTetelek.Id
         left join EREC_AgazatiJelek as EREC_Agazatijelek
            ON EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_Agazatijelek.Id
         left join EREC_UgyUgyiratok as EREC_UgyUgyiratok_Szulo
            ON EREC_UgyUgyiratok_Szulo.Id = EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo
         left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek_Szulo
            ON EREC_UgyUgyiratok_Szulo.IraIktatokonyv_Id = EREC_IraIktatokonyvek_Szulo.Id
          /* left join EREC_IrattariKikero as EREC_IrattariKikero                                    /* TFS #25 során javítva - 2017.09.04., BogI */
              ON EREC_UgyUgyiratok.Id = EREC_IrattariKikero.UgyUgyirat_Id */
         /* TFS #?? során javítva - 2017.09.08., BogI */
	     OUTER APPLY ( select TOP 1 CONVERT(nvarchar(10), EREC_IrattariKikero.KikerKezd, 102) as KikerKezd, CONVERT(nvarchar(10), EREC_IrattariKikero.KikerVege, 102) as KikerVege
		                 FROM EREC_IrattariKikero
						WHERE EREC_IrattariKikero.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
                          AND EREC_IrattariKikero.Allapot = ''04'') as EREC_IrattariKikero
         left join KRT_Csoportok as Csoportok_FelelosNev on Csoportok_FelelosNev.Id = EREC_UgyUgyiratok.Csoport_Id_Felelos
		  -- BLG_619
         left join KRT_Csoportok as Csoportok_UgyFelelosNev on Csoportok_UgyFelelosNev.Id = EREC_UgyUgyiratok.Csoport_Id_UgyFelelos
         left join KRT_Csoportok as Csoportok_OrzoNev on Csoportok_OrzoNev.Id = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo
         LEFT JOIN [KRT_Csoportok] AS Csoportok_UgyintezoNev ON Csoportok_UgyintezoNev.Id = [EREC_UgyUgyiratok].[FelhasznaloCsoport_Id_Ugyintez]
		 left join EREC_IraIratok as EREC_IraIratok on EREC_IraIratok.Ugyirat_id = EREC_UgyUgyiratok.Id and EREC_IraIratok.Alszam = 1         /* TFS #66 - 2017.09.05., BogI */
		 left join KRT_Csoportok as Csoportok_Iktato ON Csoportok_Iktato.Id = EREC_IraIratok.FelhasznaloCsoport_Id_Iktato
         left join KRT_Kodcsoportok as AllapotKodCsoport on AllapotKodCsoport.Kod = ''UGYIRAT_ALLAPOT''
         left join KRT_Kodtarak as AllapotKodTarak on AllapotKodTarak.Kodcsoport_Id = AllapotKodCsoport.Id and AllapotKodTarak.Kod = EREC_UgyUgyiratok.Allapot and AllapotKodTarak.Org=@Org
		-- BUG_9374 és BUG_13909
		left join EREC_IraJegyzekTetelek as EREC_IraJegyzekTetelek ON
			EREC_IraJegyzekTetelek.Id = (SELECT TOP 1 Id FROM EREC_IraJegyzekTetelek WHERE EREC_IraJegyzekTetelek.Obj_Id = EREC_UgyUgyiratok.Id )
		left join EREC_IraJegyzekek as EREC_IraJegyzekek ON EREC_IraJegyzekek.Id = EREC_IraJegyzekTetelek.Jegyzek_Id
		
		--LZS 12992
		left join EREC_IratMetaDefinicio on EREC_IratMetaDefinicio.Id = EREC_UgyUgyiratok.IratMetadefinicio_Id

		OUTER APPLY  (select DBO.FN_EREC_UgyUgyirat_EljarasiSzakaszFok(EREC_UgyUgyiratok.Id,@ORG) as Nev) as EljarasiSzakaszFokKodTarak

      where EREC_UgyUgyiratok.Id in (select Id from #filter); '

      if (@SelectedRowId is not null)                       --- fix sorra ugrás beállítása
	    --- #2571 - 2018.11.18., BogI
        SET @sqlcmd += N'if exists (select 1 from #result where Id = @SelectedRowId)
                           begin
                             SET @pageNumber = (select ( ((RowNumber - 1 )/ @pageSize) + 1 ) from #result where Id = @SelectedRowId );          --- NEM lehet NULL, most vizsgáltuk le
                             set @firstRow = (@pageNumber - 1) * @pageSize + 1;
                             set @lastRow = @pageNumber*@pageSize;
                             SET @pageNumber -= 1;
                           end;
                         else '
	  --- ez az ág mindíg fut, ez a normál beállítás
      SET @sqlcmd += N'if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
                           begin
                             SET @pageNumber = ( select ( ((RowNumber - 1 )/ @pageSize) + 1 ) from #result where RowNumber = ( select MAX(RowNumber) from #result ) );
                             set @firstRow = (@pageNumber - 1) * @pageSize + 1;
                             set @lastRow = @pageNumber*@pageSize;
                             SET @pageNumber -= 1;
                           end;'+char(13)+char(13);
/*       --- #366 - 2017.09.25., BogI
         set @sqlcmd = @sqlcmd + N'
         if exists (select 1 from #result where Id = @SelectedRowId)
         BEGIN
            select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
            set @firstRow = (@pageNumber - 1) * @pageSize + 1;
            set @lastRow = @pageNumber*@pageSize;
            select @pageNumber = @pageNumber - 1
         END
         ELSE'
      --ELSE
         set @sqlcmd = @sqlcmd + N'
            if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
            BEGIN
               select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
               set @firstRow = (@pageNumber - 1) * @pageSize + 1;
               set @lastRow = @pageNumber*@pageSize;
               select @pageNumber = @pageNumber - 1
            END ;'
*/

   /************************************************************
   * Bizalmas iratok õrzõ szerinti szûréséhez csoporttagok lekérése*
   ************************************************************/
-- IF dbo.fn_IsAdmin(@ExecutorUserId,) = 0 -- AND @Jogosultak = '1'
   IF @isAdminInSzervezet = 0 -- AND @Jogosultak = '1'
   begin
-- set @sqlcmd = @sqlcmd + N';declare @iratok_executor table (Id uniqueidentifier)
--insert into @iratok_executor select Id from dbo.fn_GetAllIrat_ExecutorIsOrzoOrHisLeader(
--       ''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
--       ''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
--'

   set @sqlcmd = @sqlcmd + N';declare @iratok_executor table (Id uniqueidentifier primary key);
'
--insert into #iratok_executor select Id from dbo.fn_GetAllConfidentialIrat_ExecutorIsOrzoOrHisLeader(
--       ''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
--       ''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
--'
   set @sqlcmd = @sqlcmd + N'
   DECLARE @confidential varchar(4000)
   set @confidential = (select top 1 Ertek from KRT_Parameterek where Nev=''IRAT_MINOSITES_BIZALMAS''
                     and Org=@Org and getdate() between ErvKezd and ErvVege)

   declare @Bizalmas table (val nvarchar(64) collate Hungarian_CS_AS )
   insert into @Bizalmas (val) (select Value from fn_Split(@confidential, '',''))

   DECLARE @csoporttagsag_tipus nvarchar(64)
   SET @csoporttagsag_tipus = (select top 1 KRT_CsoportTagok.Tipus from KRT_CsoportTagok
                  where KRT_CsoportTagok.Csoport_Id_Jogalany=@FelhasznaloId
                  and KRT_CsoportTagok.Csoport_Id=@SzervezetId
                  and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
				  order by KRT_CsoportTagok.Tipus desc
                  )
   DECLARE @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN nvarchar(400)
   set @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN = (select top 1 Ertek from KRT_Parameterek where Nev=''JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN''
   and Org=@Org and getdate() between ErvKezd and ErvVege)

   IF @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN IS NULL
	SET @JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN = ''0''

   ; CREATE TABLE #jogosultak (Id uniqueidentifier)
   IF @csoporttagsag_tipus in (''3'',''4'') -- vezető, asszisztens
   BEGIN
      INSERT INTO #jogosultak select KRT_Csoportok.Id from KRT_Csoportok
                     inner join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id = @SzervezetId
                     and KRT_Csoportok.Tipus = ''1'' and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
                     where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege
                     and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
   END
   ELSE
   BEGIN
      INSERT INTO #jogosultak select @FelhasznaloId
   END
'
   set @sqlcmd = @sqlcmd + N'
   insert into @iratok_executor select EREC_IraIratok.Id
   from EREC_IraIratok
   INNER JOIN #result on #result.Id=EREC_IraIratok.Ugyirat_Id and #result.RowNumber between @firstRow and @lastRow
   where 1=case when  (@confidential = ''MIND'' or EREC_IraIratok.Minosites in (select val from @Bizalmas))
      AND NOT EXISTS
            (select 1
               from KRT_Jogosultak where
                  KRT_Jogosultak.Obj_Id in (EREC_IraIratok.Ugyirat_Id, EREC_IraIratok.Id)
                  and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
                  and @FelhasznaloId = KRT_Jogosultak.Csoport_Id_Jogalany)
      AND NOT EXISTS
         (select 1
            from EREC_PldIratPeldanyok
			join EREC_IraIratok ii on EREC_PldIratPeldanyok.IraIrat_Id = ii.Id
			join EREC_UgyUgyiratok ui on EREC_IraIratok.Ugyirat_Id = ui.Id
            where EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id
               and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo in (select Id from #jogosultak)
			   and (@JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN !=''1'' or EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo=@FelhasznaloId or (ii.Csoport_Id_Ugyfelelos is null and ui.Csoport_Id_Ugyfelelos is null) or ISNULL(ii.Csoport_Id_Ugyfelelos, ui.Csoport_Id_Ugyfelelos) = @SzervezetId)
         )
		 AND NOT EXISTS
         (select 1
            from EREC_PldIratPeldanyok
			join EREC_IraIratok ii on EREC_PldIratPeldanyok.IraIrat_Id = ii.Id
			join EREC_UgyUgyiratok ui on EREC_IraIratok.Ugyirat_Id = ui.Id
            where EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id
               and EREC_PldIratPeldanyok.Csoport_Id_Felelos in (select Id from #jogosultak)
			   and (@JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN !=''1'' or EREC_PldIratPeldanyok.Csoport_Id_Felelos=@FelhasznaloId or (ii.Csoport_Id_Ugyfelelos is null and ui.Csoport_Id_Ugyfelelos is null) or ISNULL(ii.Csoport_Id_Ugyfelelos, ui.Csoport_Id_Ugyfelelos) = @SzervezetId)
         )
   then 0
   else 1 end
group by EREC_IraIratok.Id;

   drop table #jogosultak
'
   end

   /************************************************************
   * Tényleges select                                 *
   ************************************************************/
      set @sqlcmd = @sqlcmd + N'
      select
      #result.RowNumber,
      #result.Id,
      EREC_UgyUgyiratok.Foszam,
      EREC_UgyUgyiratok.Sorszam,
      EREC_UgyUgyiratok.Azonosito as Foszam_Merge,
	  EREC_IraIktatokonyvek.Azonosito as IktatokonyvekAzonosito,
	  EREC_IraIktatokonyvek.Statusz as IktatokonyvekStatusz, 
      EREC_IraIktatokonyvek.Ev,
      EREC_IraIktatokonyvek.MegkulJelzes,
      EREC_IraIktatokonyvek.Nev as Iktatokonyv_Nev,
      EREC_IraIktatokonyvek.Iktatohely,
      EREC_UgyUgyiratok.Ugyazonosito ,
      EREC_UgyUgyiratok.Alkalmazas_Id,
      EREC_UgyUgyiratok.UgyintezesModja,
      dbo.fn_KodtarErtekNeve(''UGYINTEZES_ALAPJA'', EREC_UgyUgyiratok.UgyintezesModja,@Org) as UgyintezesModja_Nev,
      CONVERT(nvarchar(19), EREC_UgyUgyiratok.Hatarido, 120) as Hatarido,
      CONVERT(nvarchar(10), EREC_UgyUgyiratok.SkontrobaDat, 102) as SkontrobaDat,
      CONVERT(nvarchar(10), EREC_UgyUgyiratok.LezarasDat, 102) as LezarasDat,
      CONVERT(nvarchar(10), EREC_UgyUgyiratok.IrattarbaVetelDat, 102) as IrattarbaVetelDat,
      CONVERT(nvarchar(10), EREC_UgyUgyiratok.IrattarbaKuldDatuma, 102) as IrattarbaKuldDatuma,
      EREC_UgyUgyiratok.SelejtezesDat,
      EREC_UgyUgyiratok.Targy,
      EREC_UgyUgyiratok.UgyTipus,
	  -- CR3423
	  --  dbo.fn_KodtarErtekNeve(''UGYTIPUS'', EREC_UgyUgyiratok.UgyTipus,@Org) as UgyTipus_Nev,
	  (select EREC_IratMetaDefinicio.UgytipusNev from EREC_IratMetaDefinicio where EREC_IratMetaDefinicio.Id = EREC_UgyUgyiratok.IratMetadefinicio_Id) as UgyTipus_Nev,
      --
	  EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo,
      CASE WHEN EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo is NULL THEN NULL
         ELSE EREC_UgyUgyiratok_Szulo.Azonosito
      END as Foszam_Merge_Szulo,
      EREC_UgyUgyiratok.UgyUgyirat_Id_Kulso,
       dbo.fn_GetSzereltCount(EREC_UgyUgyiratok.Id,  EREC_UgyUgyiratok.RegiRendszerIktatoszam) as SzereltCount,
--    (SELECT COUNT(*) FROM EREC_UgyUgyiratok as EREC_UgyUgyiratok_Szereltek
--    WHERE EREC_UgyUgyiratok_Szereltek.UgyUgyirat_Id_Szulo = EREC_UgyUgyiratok.Id
--       AND EREC_UgyUgyiratok_Szereltek.Allapot != 60
--       AND ISNULL(EREC_UgyUgyiratok_Szereltek.TovabbitasAlattAllapot,-1) != 60) AS ElokeszitettSzerelesCount,
      IsNull(EREC_UgyUgyiratok_ElokeszitettSzerelesCount.ElokeszitettSzerelesCount, 0) as ElokeszitettSzerelesCount,
      dbo.fn_GetCsatolasCount(EREC_UgyUgyiratok.Id) as CsatolasCount,
'
-- megvizsgáljuk, hogy az irat bizalmas-e, és ezesetben a felhasználó az õrzõ vagy az õrzõ vezetõje-e
--if dbo.fn_IsAdmin(@ExecutorUserId) = 0 --AND @Jogosultak = '1'
if @isAdminInSzervezet = 0 -- AND @Jogosultak = '1'
begin
-- set @sqlcmd = @sqlcmd + 'dbo.fn_GetCsatolmanyCountNotConfidential(EREC_UgyUgyiratok.Id,''' + CAST(@ExecutorUserId AS CHAR(36)) + ''', ''' + CAST(@FelhasznaloSzervezet_Id AS CHAR(36)) + ''') as CsatolmanyCount,
--'
-- set @sqlcmd = @sqlcmd + '(SELECT COUNT(*) FROM EREC_IraIratok
--               INNER JOIN EREC_Csatolmanyok
--          ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
--          and (1=case when dbo.fn_CheckIrat_IsConfidential(EREC_IraIratok.Id) = 0 then 1
--          when EREC_IraIratok.Id in (select Id from #iratok_executor) then 1 else 0 end)
--          WHERE getdate() BETWEEN EREC_Csatolmanyok.ErvKezd AND EREC_Csatolmanyok.ErvVege
--          AND EREC_IraIratok.Ugyirat_Id =  EREC_UgyUgyiratok.Id) as CsatolmanyCount,
--'
--set @sqlcmd = @sqlcmd + '
--Dokumentum_Id = case (SELECT COUNT(*) FROM EREC_IraIratok
--               INNER JOIN EREC_Csatolmanyok
--          ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
--          and (1=case when dbo.fn_CheckIrat_IsConfidential(EREC_IraIratok.Id) = 0 then 1
--          when EREC_IraIratok.Id in (select Id from #iratok_executor) then 1 else 0 end)
--          WHERE getdate() BETWEEN EREC_Csatolmanyok.ErvKezd AND EREC_Csatolmanyok.ErvVege
--          AND EREC_IraIratok.Ugyirat_Id =  EREC_UgyUgyiratok.Id)
--when 1 then
--(SELECT  top 1 EREC_Csatolmanyok.Dokumentum_Id
--FROM EREC_IraIratok
--               INNER JOIN EREC_Csatolmanyok
--          ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
--          WHERE getdate() BETWEEN EREC_Csatolmanyok.ErvKezd AND EREC_Csatolmanyok.ErvVege
--          AND EREC_IraIratok.Ugyirat_Id =  EREC_UgyUgyiratok.Id
--)
--else null end,
--'

   set @sqlcmd = @sqlcmd + '(SELECT COUNT(*) FROM [EREC_Csatolmanyok] INNER JOIN @iratok_executor ie
      ON ie.Id= EREC_Csatolmanyok.IraIrat_Id
         INNER JOIN EREC_IraIratok ON [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id
   AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
   where EREC_IraIratok.Ugyirat_Id =  EREC_UgyUgyiratok.Id) as CsatolmanyCount,
'

    set @sqlcmd = @sqlcmd + 'Dokumentum_Id =
	CASE
	WHEN (( SELECT COUNT(*) FROM [EREC_Csatolmanyok]
			  INNER JOIN @iratok_executor ie ON ie.Id= EREC_Csatolmanyok.IraIrat_Id
			  INNER JOIN EREC_IraIratok ON [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id
			   AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
			   WHERE EREC_IraIratok.Ugyirat_Id =  EREC_UgyUgyiratok.Id)) >=1
	THEN
		( SELECT top 1 EREC_Csatolmanyok.Dokumentum_Id
		  FROM EREC_Csatolmanyok
		  INNER JOIN EREC_IraIratok
		  ON [EREC_Csatolmanyok].IraIrat_Id = EREC_IraIratok.Id
		  WHERE EREC_IraIratok.Ugyirat_Id =  EREC_UgyUgyiratok.Id
		  AND getdate() BETWEEN [EREC_Csatolmanyok].ErvKezd AND [EREC_Csatolmanyok].ErvVege
		)
	ELSE
		CAST(NULL AS UNIQUEIDENTIFIER)
	END,
'
end
else
begin
   set @sqlcmd = @sqlcmd + ' dbo.fn_GetCsatolmanyCount(EREC_UgyUgyiratok.Id) as CsatolmanyCount,
'

    set @sqlcmd = @sqlcmd + ' Dokumentum_Id =
    CASE
	WHEN (dbo.fn_GetCsatolmanyCount(EREC_UgyUgyiratok.Id)) > 0
	THEN
			( SELECT TOP 1 EREC_Csatolmanyok.Dokumentum_Id
			  FROM EREC_IraIratok
						   INNER JOIN EREC_Csatolmanyok
						ON EREC_Csatolmanyok.IraIrat_Id = EREC_IraIratok.Id
						WHERE getdate() BETWEEN EREC_Csatolmanyok.ErvKezd AND EREC_Csatolmanyok.ErvVege
						AND EREC_IraIratok.Ugyirat_Id =  EREC_UgyUgyiratok.Id
			)
	ELSE
		CAST(NULL AS UNIQUEIDENTIFIER)
	END,
	'
end


set @sqlcmd = @sqlcmd + N'
      (select top 1 FeladatCount from fn_GetEREC_HataridosFeladatokCountTable(EREC_UgyUgyiratok.Id,@ExecutorUserId, @FelhasznaloSzervezet_Id)) as FeladatCount,
      EREC_UgyUgyiratok.IrattariHely,
';
SET @sqlcmd = @sqlcmd + N'
      CONVERT(nvarchar(10), EREC_UgyUgyiratok.FelulvizsgalatDat, 102) as FelulvizsgalatDat,
      CONVERT(nvarchar(10), EREC_UgyUgyiratok.SztornirozasDat, 102) as SztornirozasDat,
      EREC_UgyUgyiratok.Csoport_Id_Felelos,
      dbo.fn_GetCsoportNev(EREC_UgyUgyiratok.Csoport_Id_Felelos) as Felelos_Nev,
      EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo,
      dbo.fn_GetCsoportNev(EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo) as KRT_Csoportok_Orzo_Nev,
      EREC_UgyUgyiratok.Jelleg,
      EREC_UgyUgyiratok.IraIrattariTetel_Id,
      EREC_IraIrattariTetelek.IrattariTetelszam as IrattariTetelszam,
      dbo.fn_MergeIrattariTetelSzam(EREC_AgazatiJelek.Kod,EREC_IraIrattariTetelek.IrattariTetelszam,EREC_IraIrattariTetelek.MegorzesiIdo,EREC_IraIrattariTetelek.IrattariJel,@Org)
      as Merge_IrattariTetelszam,
      EREC_IraIrattariTetelek.IrattariJel as IrattariJel,
      MegorzesiIdo = CASE EREC_IraIrattariTetelek.IrattariJel WHEN ''L'' THEN ''0'' ELSE EREC_IraIrattariTetelek.MegorzesiIdo END,
      EREC_IraIrattariTetelek.MegorzesiIdo as MegorzesiIdo1,
      EREC_Agazatijelek.Kod as AgazatiJel,
      EREC_UgyUgyiratok.IraIktatokonyv_Id,
      EREC_UgyUgyiratok.SkontroOka,
	  -- Berakva (v21 tartalmazza, itt hiányzik)
	  -- CR3407
	   EREC_UgyUgyiratok.SkontroOka_Kod,
      EREC_UgyUgyiratok.SkontroVege,
      CONVERT(nvarchar(10), EREC_UgyUgyiratok.SkontroVege, 102) as SkontroVege_Rovid,
      EREC_UgyUgyiratok.Surgosseg,
      EREC_UgyUgyiratok.SkontrobanOsszesen,
      case when EREC_UgyUgyiratok.MegorzesiIdoVege < ''4700-12-31'' then CONVERT(nvarchar(10), EREC_UgyUgyiratok.MegorzesiIdoVege, 102) else '''' end as MegorzesiIdoVege,
      EREC_UgyUgyiratok.Partner_Id_Ugyindito,
      EREC_UgyUgyiratok.NevSTR_Ugyindito,
';

SET @sqlcmd = @sqlcmd + N'
      CASE WHEN EREC_UgyUgyiratok.NevSTR_Ugyindito = NULL THEN dbo.fn_GetFelhasznaloNev(EREC_UgyUgyiratok.Letrehozo_Id)
           ELSE EREC_UgyUgyiratok.NevSTR_Ugyindito END as Ugyindito_Nev,
      EREC_UgyUgyiratok.CimStr_Ugyindito as Ugyindito_Cim,
      CONVERT(nvarchar(10), EREC_UgyUgyiratok.ElintezesDat, 102) as ElintezesDat,
	  -- BLG_1677
	  EREC_UgyUgyiratok.ElintezesMod,
      EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez,
      dbo.fn_GetCsoportNev(EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez) as Ugyintezo_Nev,
      EREC_UgyUgyiratok.Csoport_Id_Felelos_Elozo,
--    EREC_UgyUgyiratok.KolcsonKikerDat,
--    EREC_UgyUgyiratok.KolcsonKiadDat,
--    EREC_UgyUgyiratok.Kolcsonhatarido,
      EREC_UgyUgyiratok.BARCODE,
      EREC_UgyUgyiratok.IratMetadefinicio_Id,
      EREC_UgyUgyiratok.IktatoszamKieg,
      EREC_UgyUgyiratok.Allapot,
      EREC_UgyUgyiratok.Kovetkezo_Felelos_Id,
      EREC_UgyUgyiratok.UtolsoAlszam,
      EREC_UgyUgyiratok.UtolsoSorszam,
      EREC_UgyUgyiratok.IratSzam,
	  -- BLG_4960, BUG_5516
      -- ISNULL(EREC_UgyUgyiratok.RegirendszerIktatoszam,EREC_UgyUgyiratok_Szulo.Azonosito) as RegirendszerIktatoszam,
	  EREC_UgyUgyiratok.RegirendszerIktatoszam as RegirendszerIktatoszam,
      Allapot_Nev =
      CASE EREC_UgyUgyiratok.Allapot
         WHEN ''50'' THEN dbo.fn_KodtarErtekNeve(''UGYIRAT_ALLAPOT'', EREC_UgyUgyiratok.Allapot,@Org)
                  + '' (''+dbo.fn_KodtarErtekNeve(''UGYIRAT_ALLAPOT'', EREC_UgyUgyiratok.TovabbitasAlattAllapot,@Org)+'')''
         ELSE
            CASE WHEN EREC_UgyUgyiratok.Foszam IS NULL
                  THEN dbo.fn_KodtarErtekNeve(''UGYIRAT_ALLAPOT'', EREC_UgyUgyiratok.Allapot,@Org) + '' (Munkaanyag)''
               ELSE dbo.fn_KodtarErtekNeve(''UGYIRAT_ALLAPOT'', EREC_UgyUgyiratok.Allapot,@Org)
            END
      END,
';

SET @sqlcmd = @sqlcmd + N'
      Allapot_Nev_Print = dbo.fn_KodtarErtekNeve(''UGYIRAT_ALLAPOT'', EREC_UgyUgyiratok.Allapot,@Org) +
      CASE EREC_UgyUgyiratok.Allapot
         WHEN ''50'' THEN '' (''+dbo.fn_KodtarErtekNeve(''UGYIRAT_ALLAPOT'', EREC_UgyUgyiratok.TovabbitasAlattAllapot,@Org)+'')''
         WHEN ''07'' THEN '' (''+CONVERT(nvarchar(10), EREC_UgyUgyiratok.SkontroVege, 102)+'')''
         WHEN ''57'' THEN '' (''+CONVERT(nvarchar(10), EREC_UgyUgyiratok.SkontroVege, 102)+'')''
         WHEN ''13'' THEN '' (''+(SELECT TOP 1  CONVERT(nvarchar(10), EREC_IrattariKikero.KikerKezd, 102) + '' - '' + CONVERT(nvarchar(10), EREC_IrattariKikero.KikerVege, 102)
                     FROM EREC_IrattariKikero
                     WHERE EREC_IrattariKikero.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
                     AND EREC_IrattariKikero.Allapot = ''04'')+'')''
         ELSE
            CASE
               WHEN EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo is not NULL THEN
               '' (''+ EREC_UgyUgyiratok_Szulo.Azonosito+'')''
               WHEN EREC_UgyUgyiratok.Foszam IS NULL THEN
               '' (Munkaanyag)''
               ELSE ''''
            END
      END,
      EREC_UgyUgyiratok.TovabbitasAlattAllapot,
      EREC_UgyUgyiratok.Megjegyzes,
	  MegjegyzesRiport = REPLACE(
			REPLACE(
				(SELECT TOP 1 * FROM (
							SELECT TOP 2 * 
							FROM [dbo].fn_Split(EREC_UgyUgyiratok.Note, '','')
				) tmp
				ORDER BY Value ASC), ''"Megjegyzes":'',''''
				),
				''"'',''''),
      EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos,
	  -- BLG_619
	  dbo.fn_GetCsoportNev(EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos) as UgyFelelos_Nev,
	  dbo.fn_GetCsoportKod(EREC_UgyUgyiratok.Csoport_Id_Ugyfelelos) as UgyFelelos_SzervezetKod,
      EREC_UgyUgyiratok.Cim_Id_Ugyindito,
      EREC_UgyUgyiratok.CimSTR_Ugyindito,
	  EREC_UgyUgyiratok.LezarasOka,
	  dbo.fn_KodtarErtekNeve(''LEZARAS_OKA'', EREC_UgyUgyiratok.LezarasOka,@Org) as LezarasOka_Nev,
      EREC_UgyUgyiratok.RegirendszerIktatoszam as RegirendszerIktatoszamEredeti,
      KolcsonzesDatuma =
         CASE EREC_UgyUgyiratok.Allapot
             WHEN ''13'' THEN (SELECT TOP 1  CONVERT(nvarchar(10), EREC_IrattariKikero.KikerKezd, 102) FROM EREC_IrattariKikero WHERE EREC_IrattariKikero.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
             AND EREC_IrattariKikero.Allapot = ''04'')
         ELSE ''''
         END,
      KolcsonzesiHatarido =
         CASE EREC_UgyUgyiratok.Allapot
             WHEN ''13'' THEN (SELECT TOP 1 CONVERT(nvarchar(10), EREC_IrattariKikero.KikerVege, 102) FROM EREC_IrattariKikero WHERE EREC_IrattariKikero.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
             AND EREC_IrattariKikero.Allapot = ''04'')
         ELSE ''''
         END,
      '

if(@ForMunkanaplo = '1')
BEGIN
   SET @sqlcmd = @sqlcmd + '
      EREC_HataridosFeladatok.Id AS MunkanaploKezelesiFeljegyzes_Id,
      EREC_HataridosFeladatok.Leiras AS MunkanaploKezelesiFeljegyzes,
      EREC_HataridosFeladatok.Ver AS MunkanaploKezelesiFeljegyzes_Ver,'
END

SET @sqlcmd = @sqlcmd + '
      EREC_UgyUgyiratok.IrattarId,
	  EREC_UgyUgyiratok.UjOrzesiIdo,
	  -- BLG_2156
	  EREC_UgyUgyiratok.UjOrzesiIdoIdoegyseg,
	  EREC_UgyUgyiratok.UgyintezesKezdete,
	  EREC_UgyUgyiratok.FelfuggesztettNapokSzama,
	  EREC_UgyUgyiratok.IntezesiIdo,
	  EREC_UgyUgyiratok.IntezesiIdoegyseg,
	  -- BLG_1677
	  -- Szignalas adatai
	  EREC_UgyUgyiratok.SzignaloId,
	  dbo.fn_GetCsoportNev(EREC_UgyUgyiratok.SzignaloId) as Szignalo,
	  EREC_UgyUgyiratok.SzignalasIdeje,
	  EREC_UgyUgyiratok.ElozoAllapot,
      EREC_UgyUgyiratok.Ver,
      EREC_UgyUgyiratok.Note,
      EREC_UgyUgyiratok.Stat_id,
      EREC_UgyUgyiratok.ErvKezd,
      EREC_UgyUgyiratok.ErvVege,
      EREC_UgyUgyiratok.Letrehozo_id,
      CONVERT(nvarchar(10), EREC_UgyUgyiratok.LetrehozasIdo, 102) as LetrehozasIdo_Rovid,
      EREC_UgyUgyiratok.LetrehozasIdo,
      EREC_UgyUgyiratok.Modosito_id,
      EREC_UgyUgyiratok.ModositasIdo,
      EREC_UgyUgyiratok.Zarolo_id,
      EREC_UgyUgyiratok.ZarolasIdo,
      EREC_UgyUgyiratok.Tranz_id,
      EREC_UgyUgyiratok.UIAccessLog_id,
     -- /* EREC_UgyUgyiratok.UjOrzesiIdo,  */
	  dbo.Fn_UgyUgyiratElintezesiHatarido(EREC_UgyUgyiratok.Id, EREC_UgyUgyiratok.IntezesiIdoegyseg, EREC_UgyUgyiratok.IratMetadefinicio_Id,EREC_UgyUgyiratok.LetrehozasIdo,EREC_UgyUgyiratok.Hatarido ) as UgyiratElintezesiHatarido,
	  dbo.Fn_Get_KRT_Kodtar_Nev(''IRAT_HATASA_UGYINTEZESRE'',EREC_UgyUgyiratok.SakkoraAllapot) as SakkoraAllapotNev ,
	  [dbo].[fn_IratHatasaTipusa] (EREC_UgyUgyiratok.SakkoraAllapot) as SakkoraAllapotTipus,

      DBO.FN_EREC_UgyUgyirat_EljarasiSzakaszFok(EREC_UgyUgyiratok.Id,@ORG) as EljarasiSzakaszFokNev,
      EREC_UgyUgyiratok.HatralevoNapok,
      EREC_UgyUgyiratok.HatralevoMunkaNapok,

  '
	   -- BLG_44
	DECLARE @Iktatas_ugytipus_iratmetadefbol varchar(400)
	SET @Iktatas_ugytipus_iratmetadefbol=(select Top 1 Ertek from KRT_Parameterek
         where Nev='IKTATAS_UGYTIPUS_IRATMETADEFBOL'
         and Org=@Org
         and getdate() between ErvKezd and ErvVege)
	IF @Iktatas_ugytipus_iratmetadefbol = 1
	BEGIN
		SET @sqlcmd = @sqlcmd + '
		(select EREC_IratMetaDefinicio.UgyFajta from EREC_IratMetaDefinicio where EREC_IratMetaDefinicio.Id = EREC_UgyUgyiratok.IratMetadefinicio_Id) AS Ugy_Fajtaja,
		'
	END
	ELSE
	BEGIN
		SET @sqlcmd = @sqlcmd + '
		EREC_UgyUgyiratok.Ugy_Fajtaja  AS Ugy_Fajtaja,
		'
	END

	SET @sqlcmd = @sqlcmd + '
      ----
      reverse(substring(reverse( (select EREC_UgyUgyiratok_Utod.Azonosito+char(59) from EREC_UgyUgyiratok as EREC_UgyUgyiratok_Utod where EREC_UgyUgyiratok_Utod.UgyUgyirat_Id_Szulo = EREC_UgyUgyiratok.Id order by EREC_UgyUgyiratok_Utod.Azonosito for xml path('''')) ),2,10000)) as Leszarmazott_Iktatoszam,
      (select cs.Nev from EREC_IraIratok ii, KRT_Csoportok cs where ii.Ugyirat_Id=EREC_UgyUgyiratok.Id and ii.Alszam=1 and cs.Id=ii.FelhasznaloCsoport_Id_Iktato) as Iktato,
      ----
	  EREC_UgyUgyiratok.ElteltIdo,
	  CASE
		WHEN EREC_UgyUgyiratok.ElteltIdo IS NULL
		THEN NULL
		ELSE EREC_UgyUgyiratok.ElteltIdo + '' ''+ ISNULL( dbo.Fn_Get_KRT_Kodtar_Nev(''IDOEGYSEG'',EREC_UgyUgyiratok.ElteltIdoIdoEgyseg),'' nap'' )
	  END as ElteltUgyintezesiIdo

	  ,EREC_UgyUgyiratok.ElteltIdoUtolsoModositas
	  , CASE
			WHEN (EREC_UgyUgyiratok.IntezesiIdo IS NULL OR EREC_UgyUgyiratok.IntezesiIdoegyseg IS NULL)
			THEN NULL
			ELSE EREC_UgyUgyiratok.IntezesiIdo + '' '' + dbo.Fn_Get_KRT_Kodtar_Nev(''IDOEGYSEG'',EREC_UgyUgyiratok.IntezesiIdoegyseg)
		END
		AS UgyintezesIdeje_Nev
	  '
-- CR3442
--IF @OrgIsBOMPH = 1
	set @sqlcmd = @sqlcmd + '
		  ,(select top 1 dbo.fn_KodtarErtekNeve(''FELADAT_ALTIPUS'', EREC_HataridosFeladatok.Altipus, @Org)
		  from EREC_HataridosFeladatok where EREC_HataridosFeladatok.Obj_Id = EREC_UgyUgyiratok.Id
		  and EREC_HataridosFeladatok.Tipus = ''M''
		  order by EREC_HataridosFeladatok.LetrehozasIdo desc) as KezelesTipusNev'
--ELSE
--	set @sqlcmd = @sqlcmd + '
--		  ,null as KezelesTipusNev'

--statisztika

	SET @sqlcmd = @sqlcmd + ',
		(select KRT_Kodtarak.Nev
		from EREC_ObjektumTargyszavai
		left join KRT_Kodcsoportok on KRT_Kodcsoportok.Kod = ''UGYSTATISZTIKA_T1''
        left join KRT_Kodtarak on KRT_Kodtarak.Kodcsoport_Id = KRT_Kodcsoportok.Id and KRT_Kodtarak.Kod = EREC_ObjektumTargyszavai.Ertek COLLATE HUNGARIAN_CS_AS and KRT_Kodtarak.Org=@Org
		where EREC_ObjektumTargyszavai.Targyszo_Id = ''A92E4382-6D67-4A58-98B2-FAC82A138037'' and EREC_ObjektumTargyszavai.Obj_Id = EREC_UgyUgyiratok.Id ) AS FT1,
		(select KRT_Kodtarak.Nev
		from EREC_ObjektumTargyszavai
		left join KRT_Kodcsoportok on KRT_Kodcsoportok.Kod = ''UGYSTATISZTIKA_T2''
        left join KRT_Kodtarak on KRT_Kodtarak.Kodcsoport_Id = KRT_Kodcsoportok.Id and KRT_Kodtarak.Kod = EREC_ObjektumTargyszavai.Ertek COLLATE HUNGARIAN_CS_AS and KRT_Kodtarak.Org=@Org
		where EREC_ObjektumTargyszavai.Targyszo_Id = ''C148F027-8BDD-4B0C-971B-C94CD4322002'' and EREC_ObjektumTargyszavai.Obj_Id = EREC_UgyUgyiratok.Id ) AS FT2,
		(select KRT_Kodtarak.Nev
		from EREC_ObjektumTargyszavai
		left join KRT_Kodcsoportok on KRT_Kodcsoportok.Kod = ''UGYSTATISZTIKA_T3''
        left join KRT_Kodtarak on KRT_Kodtarak.Kodcsoport_Id = KRT_Kodcsoportok.Id and KRT_Kodtarak.Kod = EREC_ObjektumTargyszavai.Ertek COLLATE HUNGARIAN_CS_AS and KRT_Kodtarak.Org=@Org
		where EREC_ObjektumTargyszavai.Targyszo_Id = ''4CAE4517-782F-47E1-92F8-8BB45A5406D7'' and EREC_ObjektumTargyszavai.Obj_Id = EREC_UgyUgyiratok.Id ) AS FT3
		'

-- BUG_9374
set @sqlcmd = @sqlcmd + '
	, EREC_IraJegyzekek.Nev as JegyzekAzon
	, EREC_IraJegyzekek.Allapot as JegyzekAllapot'

set @sqlcmd = @sqlcmd + '
      FROM EREC_UgyUgyiratok as EREC_UgyUgyiratok
         inner join #result on #result.Id = EREC_UgyUgyiratok.Id
         left join
         (SELECT UgyUgyirat_Id_Szulo, COUNT(*) as ElokeszitettSzerelesCount FROM EREC_UgyUgyiratok as EREC_UgyUgyiratok_Szereltek
           WHERE EREC_UgyUgyiratok_Szereltek.Allapot != 60
             AND ISNULL(EREC_UgyUgyiratok_Szereltek.TovabbitasAlattAllapot,-1) != 60
             AND UgyUgyirat_Id_Szulo is not null
             AND UgyUgyirat_Id_Szulo in (select Id from #result)
           GROUP BY UgyUgyirat_Id_Szulo
         ) AS EREC_UgyUgyiratok_ElokeszitettSzerelesCount
            on EREC_UgyUgyiratok.Id = EREC_UgyUgyiratok_ElokeszitettSzerelesCount.UgyUgyirat_Id_Szulo
         left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek
            ON EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id
         left join EREC_IraIrattariTetelek as EREC_IraIrattariTetelek
            ON EREC_UgyUgyiratok.IraIrattariTetel_Id = EREC_IraIrattariTetelek.Id
         left join EREC_AgazatiJelek as EREC_Agazatijelek
            ON EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_Agazatijelek.Id
         left join EREC_UgyUgyiratok as EREC_UgyUgyiratok_Szulo
            ON EREC_UgyUgyiratok_Szulo.Id = EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo
         left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek_Szulo
            ON EREC_UgyUgyiratok_Szulo.IraIktatokonyv_Id = EREC_IraIktatokonyvek_Szulo.Id
		-- BUG_9374 és BUG_13909
		left join EREC_IraJegyzekTetelek as EREC_IraJegyzekTetelek ON
			EREC_IraJegyzekTetelek.Id = (SELECT TOP 1 Id FROM EREC_IraJegyzekTetelek WHERE EREC_IraJegyzekTetelek.Obj_Id = EREC_UgyUgyiratok.Id )
		left join EREC_IraJegyzekek as EREC_IraJegyzekek  ON EREC_IraJegyzekek.Id = EREC_IraJegyzekTetelek.Jegyzek_Id '

if(@ForMunkanaplo = '1')
BEGIN
   SET @sqlcmd = @sqlcmd + '
   LEFT JOIN EREC_HataridosFeladatok ON [EREC_UgyUgyiratok].[Id] = EREC_HataridosFeladatok.Obj_Id
    AND EREC_HataridosFeladatok.[AlTipus] = ''13'''
END

SET @sqlcmd = @sqlcmd + '
    where RowNumber between @firstRow and @lastRow
   ORDER BY #result.RowNumber;'

      -- találatok száma és oldalszám
      set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #filter;';

	execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @ExecutorUserId uniqueidentifier, @FelhasznaloSzervezet_Id uniqueidentifier, @SelectedRowId uniqueidentifier, @Org uniqueidentifier, @Ugyintezo_Id_ForMunkanaplo uniqueidentifier, @FelhasznaloId uniqueidentifier, @SzervezetId uniqueidentifier, @IratLetrehozasido datetime'
      ,@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @ExecutorUserId = @ExecutorUserId, @FelhasznaloSzervezet_Id = @FelhasznaloSzervezet_Id, @SelectedRowId = @SelectedRowId, @Org = @Org
      ,@Ugyintezo_Id_ForMunkanaplo = @Ugyintezo_Id_ForMunkanaplo
      ,@FelhasznaloId = @FelhasznaloId, @SzervezetId = @SzervezetId, @IratLetrehozasido = @IratLetrehozasido;
END TRY
BEGIN CATCH
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()

   if ERROR_NUMBER()<50000
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()

   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)

END CATCH

end
