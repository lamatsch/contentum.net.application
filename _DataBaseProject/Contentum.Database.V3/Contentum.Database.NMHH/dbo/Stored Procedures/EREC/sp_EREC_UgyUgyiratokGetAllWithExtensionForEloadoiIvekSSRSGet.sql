/****** Object:  StoredProcedure [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForEloadoiIvekSSRSGet]    Script Date: 2/14/2019 9:39:17 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForEloadoiIvekSSRSGet]'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForEloadoiIvekSSRSGet]
GO

/****** Object:  StoredProcedure [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForEloadoiIvekSSRSGet]    Script Date: 2/14/2019 9:39:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForEloadoiIvekSSRSGet]
  @Where			nvarchar(MAX) = '',
  @OrderBy			nvarchar(200) = '',
  @TopRow			nvarchar(5) = '',
  @ExecutorUserId	uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier,
  @id				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow is null or @TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = '
SELECT ' + @LocalTopRow + '
EREC_UgyUgyiratok.Id,
EREC_UgyUgyiratok.Azonosito as Foszam_Merge,
dbo.fn_GetFelhasznaloNev(EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez) as Ugyintezo_Nev,
EREC_UgyUgyiratok.LetrehozasIdo,
(SELECT TOP 1 EREC_UgyiratObjKapcsolatok.Leiras FROM EREC_UgyiratObjKapcsolatok WHERE EREC_UgyiratObjKapcsolatok.KapcsolatTipus = ''07'' AND EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt  = EREC_UgyUgyiratok.Id AND getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege) as MIG_EloIrat,
Coalesce ((SELECT TOP 1 EREC_UgyUgyiratok_Szereltek.Azonosito FROM EREC_UgyUgyiratok as EREC_UgyUgyiratok_Szereltek WHERE EREC_UgyUgyiratok_Szereltek.UgyUgyirat_Id_Szulo = EREC_UgyUgyiratok.Id),
(SELECT TOP 1 EREC_UgyiratObjKapcsolatok.Leiras FROM EREC_UgyiratObjKapcsolatok WHERE EREC_UgyiratObjKapcsolatok.KapcsolatTipus = ''07'' AND EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt  = EREC_UgyUgyiratok.Id AND getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege)) as EREC_EloIrat,
(SELECT EREC_UgyUgyiratok_Utoirat.Azonosito FROM EREC_UgyUgyiratok as EREC_UgyUgyiratok_Utoirat WHERE EREC_UgyUgyiratok_Utoirat.Id = EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo) as Utoirat,
'''' as kap_Foszam_Merge,
dbo.fn_GetCsoportNev(EREC_UgyUgyiratok.Csoport_Id_UgyFelelos) as szervezeti_egyseg,
EREC_IraIktatokonyvek.Nev as Iktatokonyv_Nev,
EREC_UgyUgyiratok.NevSTR_Ugyindito,
EREC_UgyUgyiratok.CimSTR_Ugyindito,
EREC_UgyUgyiratok.Targy,
CONVERT(nvarchar(10), EREC_IraIratok.IktatasDatuma, 102) as IktatasDatuma,
EREC_IraIrattariTetelek.MegorzesiIdo,
EREC_UgyUgyiratok.BARCODE,
dbo.fn_barcode128ssrs(EREC_UgyUgyiratok.BARCODE) as ''Barcodefont'',
EREC_UgyUgyiratok.Hatarido, 
EREC_UgyUgyiratok.LezarasDat, 
EREC_UgyUgyiratok.IrattarbaVetelDat,
EREC_UgyUgyiratok.SelejtezesDat,
EREC_UgyUgyiratok.SkontrobaDat,
EREC_UgyUgyiratok.ModositasIdo,
EREC_UgyUgyiratok.SzignalasIdeje,
KRT_Felhasznalok.Nev as SzignaloNev,
EREC_IraIrattariTetelek.IrattariTetelszam,
case when len(EREC_IraIrattariTetelek.IrattariTetelszam) > 3 then left(EREC_IraIrattariTetelek.IrattariTetelszam,1) else '''' end as Itsz1,
case when len(EREC_IraIrattariTetelek.IrattariTetelszam) > 3 then substring(EREC_IraIrattariTetelek.IrattariTetelszam,2,1) else substring(EREC_IraIrattariTetelek.IrattariTetelszam,1,1) end  as Itsz2,
case when len(EREC_IraIrattariTetelek.IrattariTetelszam) > 3 then substring(EREC_IraIrattariTetelek.IrattariTetelszam,3,1) else substring(EREC_IraIrattariTetelek.IrattariTetelszam,3,1) end  as Itsz3,
case when len(EREC_IraIrattariTetelek.IrattariTetelszam) > 3 then substring(EREC_IraIrattariTetelek.IrattariTetelszam,4,1) else substring(EREC_IraIrattariTetelek.IrattariTetelszam,3,1) end  as Itsz4,
case when EREC_IraIrattariTetelek.IrattariJel = ''S'' then EREC_IraIrattariTetelek.MegorzesiIdo else ''NS'' end  as SelejtezesiKod,
case when EREC_IraIrattariTetelek.IrattariJel = ''S'' then '''' else EREC_IraIrattariTetelek.MegorzesiIdo end as LeveltariKod,'

SET @sqlcmd = @sqlcmd + '
EREC_Agazatijelek.Kod as AgazatiJel,
EREC_IraIrattariTetelek.IrattariJel,
coalesce(EREC_IratMetaDefinicio.Ugyfajta,EREC_UgyUgyiratok.Ugy_Fajtaja) as UgyFajtaja, 
coalesce(dbo.fn_KodtarErtekNeve(''UGY_FAJTAJA'', EREC_IratMetaDefinicio.Ugyfajta,@Org),dbo.fn_KodtarErtekNeve(''UGY_FAJTAJA'', EREC_UgyUgyiratok.Ugy_Fajtaja,@Org),''Nem hatósági ügy'') as UgyFajtaja_Nev,
EREC_IratMetaDefinicio.UgytipusNev as UgyTipus_Nev,
dbo.fn_MergeIrattariTetelSzam(EREC_AgazatiJelek.Kod,EREC_IraIrattariTetelek.IrattariTetelszam,EREC_IraIrattariTetelek.MegorzesiIdo,EREC_IraIrattariTetelek.IrattariJel,@Org) as Merge_IrattariTetelszam,
EREC_IraIratok.HivatkozasiSzam as HivatkozasiSzam,
EREC_KuldKuldemenyek.LetrehozasIdo as ErkeztetesDatuma,
dbo.fn_KodtarErtekNeve(''KULDEMENY_KULDES_MODJA'', EREC_KuldKuldemenyek.KuldesMod ,@Org) as ErkezesiMod
FROM
	EREC_UgyUgyiratok as EREC_UgyUgyiratok
	left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek
		ON EREC_UgyUgyiratok.IraIktatokonyv_Id = EREC_IraIktatokonyvek.Id			
	left join EREC_IraIrattariTetelek as EREC_IraIrattariTetelek
		ON EREC_UgyUgyiratok.IraIrattariTetel_Id = EREC_IraIrattariTetelek.Id
	left join EREC_AgazatiJelek as EREC_Agazatijelek
		ON EREC_IraIrattariTetelek.AgazatiJel_Id = EREC_Agazatijelek.Id	  
	left join EREC_UgyUgyiratok as EREC_UgyUgyiratok_Szulo 
		ON EREC_UgyUgyiratok_Szulo.Id = EREC_UgyUgyiratok.UgyUgyirat_Id_Szulo
	left join EREC_IraIktatokonyvek as EREC_IraIktatokonyvek_Szulo 
		ON EREC_UgyUgyiratok_Szulo.IraIktatokonyv_Id = EREC_IraIktatokonyvek_Szulo.Id
	left join EREC_UgyUgyiratdarabok as EREC_UgyUgyiratdarabok
		ON EREC_UgyUgyiratdarabok.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
	left join EREC_IraIratok as EREC_IraIratok
		ON EREC_IraIratok.UgyUgyiratDarab_Id = EREC_UgyUgyiratdarabok.Id
	left join EREC_IratMetaDefinicio as EREC_IratMetaDefinicio
		ON EREC_IratMetaDefinicio.Id = EREC_UgyUgyiratok.IratMetadefinicio_Id
	left join EREC_KuldKuldemenyek as EREC_KuldKuldemenyek
		ON EREC_KuldKuldemenyek.Id = EREC_IraIratok.KuldKuldemenyek_Id and EREC_KuldKuldemenyek.PostazasIranya = ''1''
	left join KRT_Felhasznalok as KRT_Felhasznalok ON EREC_UgyUgyiratok.Szignaloid = KRT_Felhasznalok.Id
WHERE (EREC_IraIratok.Alszam = ''1'' or EREC_IraIratok.Alszam is NULL )
and EREC_IraIktatokonyvek.Org = ''' + cast(@Org as NVarChar(40)) + '''
'

if @id is not null
	begin 
		SET @sqlcmd = @sqlcmd + ' AND EREC_UgyUgyiratok.Id = @id'
	end

if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' AND ' + @Where
	end

if @OrderBy is not null and @OrderBy!=''
	begin 
		SET @sqlcmd = @sqlcmd + @OrderBy
	end


  --print @sqlcmd

  execute sp_executesql @sqlcmd,N'@Org uniqueidentifier, @id uniqueidentifier', @Org = @Org, @id = @id


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

