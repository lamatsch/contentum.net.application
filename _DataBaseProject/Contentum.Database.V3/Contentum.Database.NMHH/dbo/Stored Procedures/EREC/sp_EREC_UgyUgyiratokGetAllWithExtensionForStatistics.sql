﻿CREATE procedure [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForStatistics]
  @Where			nvarchar(4000) = '',
  @OrderBy			nvarchar(200) = ' order by EREC_UgyUgyiratok.UgyTipus',
  @TopRow			nvarchar(5) = '',
  @ExecutorUserId	uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = '
SELECT DISTINCT ' + @LocalTopRow 


IF( (SELECT Kod FROM KRT_Orgok) = 'NMHH')
		SET @sqlcmd = @sqlcmd + '
		EREC_UgyUgyiratok.Ugy_Fajtaja collate Hungarian_CI_AS as Kod,
		KRT_KodTarak_Ugy_Fajtaja.Nev collate Hungarian_CI_AS as Nev,
		EREC_IratMetaDefinicio.UgyFajta collate Hungarian_CI_AS,
		Avg(DATEDIFF(day, EREC_UgyUgyiratok.LetrehozasIdo, EREC_UgyUgyiratok.ElintezesDat)) as elint1,
		AVG((DATEDIFF(day, EREC_UgyUgyiratok.LetrehozasIdo, EREC_UgyUgyiratok.ElintezesDat)-ISNULL (EREC_UgyUgyiratok.SkontrobanOsszesen,0))) as elint2
		FROM
		EREC_UgyUgyiratok as EREC_UgyUgyiratok
		inner join EREC_IraIktatoKonyvek on EREC_UgyUgyiratok.IraIktatoKonyv_Id=EREC_IraIktatoKonyvek.Id and EREC_IraIktatoKonyvek.Org=''' + cast(@Org as NVarChar(40)) + '''
		left join EREC_IratMetaDefinicio on EREC_IratMetaDefinicio.Id=EREC_UgyUgyiratok.IratMetaDefinicio_Id and EREC_IratMEtaDefinicio.Org=''' + cast(@Org as NVarChar(40)) + '''
		left join KRT_KodCsoportok as KRT_KodCsoportok_Ugy_Fajtaja on KRT_KodCsoportok_Ugy_Fajtaja.Kod=''UGY_FAJTAJA''
		left join KRT_KodTarak as KRT_KodTarak_Ugy_Fajtaja on KRT_KodCsoportok_Ugy_Fajtaja.Id = KRT_KodTarak_Ugy_Fajtaja.KodCsoport_Id and EREC_UgyUgyiratok.Ugy_Fajtaja collate Hungarian_CI_AS=KRT_KodTarak_Ugy_Fajtaja.Kod
		and KRT_KodTarak_Ugy_Fajtaja.Org= ''' + cast(@Org as NVarChar(40)) + '''
		'
ELSE
	SET @sqlcmd = @sqlcmd + '
		EREC_UgyUgyiratok.UgyTipus,
		KRT_KodTarak_UgyTipus.Nev,
		EREC_IratMetaDefinicio.UgytipusNev as Nev,
		Avg(DATEDIFF(day, EREC_UgyUgyiratok.LetrehozasIdo, EREC_UgyUgyiratok.ElintezesDat)) as elint1,
		AVG((DATEDIFF(day, EREC_UgyUgyiratok.LetrehozasIdo, EREC_UgyUgyiratok.ElintezesDat)-ISNULL (EREC_UgyUgyiratok.SkontrobanOsszesen,0))) as elint2
		FROM
		EREC_UgyUgyiratok as EREC_UgyUgyiratok
		inner join EREC_IraIktatoKonyvek on EREC_UgyUgyiratok.IraIktatoKonyv_Id=EREC_IraIktatoKonyvek.Id and EREC_IraIktatoKonyvek.Org=''' + cast(@Org as NVarChar(40)) + '''
		left join EREC_IratMetaDefinicio on EREC_IratMetaDefinicio.Id=EREC_UgyUgyiratok.IratMetaDefinicio_Id and EREC_IratMEtaDefinicio.Org=''' + cast(@Org as NVarChar(40)) + '''
		left join KRT_KodCsoportok as KRT_KodCsoportok_UgyTipus on KRT_KodCsoportok_UgyTipus.Kod=''UGYTIPUS''
		left join KRT_KodTarak as KRT_KodTarak_UgyTipus on KRT_KodCsoportok_UgyTipus.Id = KRT_KodTarak_UgyTipus.KodCsoport_Id and EREC_UgyUgyiratok.UgyTipus=KRT_KodTarak_UgyTipus.Kod
		and KRT_KodTarak_UgyTipus.Org= ''' + cast(@Org as NVarChar(40)) + '''
		'

if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
   
IF( (SELECT Kod FROM KRT_Orgok) = 'NMHH')
BEGIN
   SET @sqlcmd = @sqlcmd + ' GROUP BY
	EREC_UgyUgyiratok.Ugy_Fajtaja collate Hungarian_CI_AS,
	KRT_KodTarak_Ugy_Fajtaja.Nev,
	EREC_IratMetaDefinicio.UgyFajta '

	SET @OrderBy = ' order by EREC_UgyUgyiratok.Ugy_Fajtaja'
END
ELSE
   SET @sqlcmd = @sqlcmd + ' GROUP BY
	EREC_UgyUgyiratok.UgyTipus,
	KRT_KodTarak_UgyTipus.Nev,
	EREC_IratMetaDefinicio.UgytipusNev '

   SET @sqlcmd = @sqlcmd + @OrderBy

  --print @sqlcmd

   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end

