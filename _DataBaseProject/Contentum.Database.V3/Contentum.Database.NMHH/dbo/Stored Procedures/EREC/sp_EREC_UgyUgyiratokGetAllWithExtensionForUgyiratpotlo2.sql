﻿
/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_UgyUgyiratokGetAllWithExtensionForUgyiratpotlo2')
            and   type = 'P')
   drop procedure sp_EREC_UgyUgyiratokGetAllWithExtensionForUgyiratpotlo2
go
*/


CREATE procedure [dbo].[sp_EREC_UgyUgyiratokGetAllWithExtensionForUgyiratpotlo2]
  @Where			nvarchar(MAX) = '',
  @OrderBy			nvarchar(200) = ' order by EREC_IrattariKikero.LetrehozasIdo',
  @TopRow			nvarchar(5) = '',
  @ExecutorUserId	uniqueidentifier,
  @id				uniqueidentifier

as

begin

BEGIN TRY

   set nocount on

	--DECLARE @Org uniqueidentifier
	--SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	--if (@Org is null)
	--begin
	--	RAISERROR('[50202]',16,1)
	--end
   
   DECLARE @sqlcmd nvarchar(MAX)
   DECLARE @ExecutorUserName nvarchar(MAX)
   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SELECT @ExecutorUserName = Nev FROM KRT_Felhasznalok
  WHERE Id = @ExecutorUserId

	 
  SET @sqlcmd = '
SELECT ' + @LocalTopRow + '
	   
	   ISNULL(EREC_UgyUgyiratok.IrattariHely,'''') as Irattar_neve,
	   EREC_UgyUgyiratok.Azonosito as Azonosito,
	    ISNULL(EREC_KuldKuldemenyek.NevStr_Bekuldo + '' - ''  ,'''') +
		CASE WHEN EREC_UgyUgyiratok.Targy IS NOT NULL
			THEN + EREC_UgyUgyiratok.Targy
			ELSE ''''
		END
	    as Targy,
	   CONVERT(nvarchar(10), EREC_IrattariKikero.KiadasDatuma, 102) as KiadasDatuma,
	   EREC_IrattariKikero.FelhasznalasiCel,
		Case EREC_IrattariKikero.FelhasznalasiCel 
		When ''B'' Then ''Betekintésre''
		When ''U'' Then ''Ügyintézésre''
		End
		as FelhasznalasiCelNev,
	   EREC_IrattariKikero.Keres_note as KiemelesMegjegyzes,
	   KRT_FelhasznalokFelhasznaloCsoport_Id_Kikero.Nev as KiemelestKeroNeve,
	   KRT_FelhasznalokFelhasznaloCsoport_Id_Kiado.Nev as KiemelestVegzoNeve,
	   --(SELECT Nev FROM KRT_Felhasznalok
	   --WHERE Id = @ExecutorUserId) as KiemelestVegzoNeve,
	   CONVERT(nvarchar(10), EREC_IrattariKikero.KikerVege, 102) as KikerVege,
	   EREC_IraIrattariTetelek.IrattariTetelszam as IrattariTetelszam,
	   EREC_UgyUgyiratok.BARCODE as BARCODE
from EREC_UgyUgyiratok as EREC_UgyUgyiratok
			left join EREC_IraIratok as EREC_IraIratok
				ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id
			left join EREC_KuldKuldemenyek as EREC_KuldKuldemenyek
				ON EREC_KuldKuldemenyek.Id = EREC_IraIratok.KuldKuldemenyek_Id
			left join EREC_IrattariKikero as EREC_IrattariKikero
				ON EREC_IrattariKikero.UgyUgyirat_Id = EREC_UgyUgyiratok.Id 
				and EREC_IrattariKikero.Allapot in (''01'',''02'',''03'',''04'')
			left join KRT_Csoportok as KRT_Csoportok 
				on KRT_Csoportok.Id = EREC_IrattariKikero.Irattar_Id
			left join KRT_Felhasznalok as KRT_FelhasznalokKapta_Id 
				on KRT_FelhasznalokKapta_Id.Id = EREC_IrattariKikero.FelhasznaloCsoport_Id_Kiado	
			left join KRT_Felhasznalok as KRT_FelhasznalokFelhasznaloCsoport_Id_Kikero 
				on KRT_FelhasznalokFelhasznaloCsoport_Id_Kikero.Id = EREC_IrattariKikero.FelhasznaloCsoport_Id_Kikero
			left join KRT_Felhasznalok as KRT_FelhasznalokFelhasznaloCsoport_Id_Kiado 
				on KRT_FelhasznalokFelhasznaloCsoport_Id_Kiado.Id = EREC_IrattariKikero.FelhasznaloCsoport_Id_Kiado
			left join EREC_IraIrattariTetelek as EREC_IraIrattariTetelek
				on EREC_UgyUgyiratok.IraIrattariTetel_Id = EREC_IraIrattariTetelek.Id
		WHERE EREC_UgyUgyiratok.Id = ''' + CAST( @id as NVarChar(40)) + ''''

if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end

   SET @sqlcmd = @sqlcmd + @OrderBy

  print @sqlcmd

  execute sp_executesql @sqlcmd, N'@ExecutorUserId uniqueidentifier, @id uniqueidentifier, @Where nvarchar(MAX), @OrderBy nvarchar(200), @TopRow nvarchar(5)'
      ,@ExecutorUserId = @ExecutorUserId, @id = @id, @Where = @Where, @OrderBy = @OrderBy, @TopRow = @TopRow

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
