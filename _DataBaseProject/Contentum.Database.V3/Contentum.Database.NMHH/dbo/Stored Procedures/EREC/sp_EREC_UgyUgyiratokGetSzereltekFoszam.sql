﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[sp_EREC_UgyUgyiratokGetSzereltekFoszam]
  @Where			nvarchar(MAX) = '',
  @OrderBy			nvarchar(400) = '',
  @ExecutorUserId	uniqueidentifier,
  @TopRow NVARCHAR(5) = ''

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(MAX)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

  SET @sqlcmd = 
  '
SELECT 
	EREC_UgyUgyiratok.Id,
	EREC_UgyiratObjKapcsolatok.Leiras as Foszam
	FROM EREC_UgyUgyiratok as EREC_UgyUgyiratok
		inner join EREC_UgyiratObjKapcsolatok as EREC_UgyiratObjKapcsolatok
			on EREC_UgyiratObjKapcsolatok.KapcsolatTipus = ''07'' 
			AND EREC_UgyiratObjKapcsolatok.Obj_Id_Kapcsolt  = EREC_UgyUgyiratok.Id 
			AND getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege'
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end

SET @sqlcmd = @sqlcmd +
'UNION ALL
SELECT 
	EREC_UgyUgyiratok.Id,
	EREC_UgyUgyiratok_Szereltek.Azonosito as Foszam
	FROM EREC_UgyUgyiratok as EREC_UgyUgyiratok
		inner join EREC_UgyUgyiratok as EREC_UgyUgyiratok_Szereltek 
			on EREC_UgyUgyiratok_Szereltek.UgyUgyirat_Id_Szulo = EREC_UgyUgyiratok.Id '
			
	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
    
	if @OrderBy is not null and @OrderBy!=''
	begin 
		  SET @sqlcmd = @sqlcmd + @OrderBy
	end 
 
   exec(@sqlcmd)


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end


