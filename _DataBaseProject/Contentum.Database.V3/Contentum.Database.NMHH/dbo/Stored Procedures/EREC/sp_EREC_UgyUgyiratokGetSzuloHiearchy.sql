﻿CREATE procedure [dbo].[sp_EREC_UgyUgyiratokGetSzuloHiearchy]
		@Id	uniqueidentifier,
		@ExecutorUserId	uniqueidentifier

as

begin

BEGIN TRY

    set nocount on;
   
	WITH UgyiratHierarchy  AS
	(
	   -- Base case
	   SELECT *,
			  0 as HierarchyLevel
	   FROM EREC_UgyUgyiratok
	   WHERE Id = @Id

	   UNION ALL

	   -- Recursive step
	   SELECT u.*,
		  uh.HierarchyLevel + 1 AS HierarchyLevel
	   FROM EREC_UgyUgyiratok as u
		  INNER JOIN UgyiratHierarchy as uh ON
			 u.Id = uh.UgyUgyirat_Id_Szulo 
	)
	
	SELECT * INTO #temp
	FROM [UgyiratHierarchy]
	
	SELECT #temp.*, #temp.Azonosito as Foszam_Merge
	FROM #temp	
	ORDER BY [#temp].HierarchyLevel ASC	
  

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end