
--if exists (select 1
--            from  sysobjects
--           where  id = object_id('sp_EREC_UgyUgyiratokGetTomegesOlvasasiJog')
--            and   type = 'P')
--   drop procedure sp_EREC_UgyUgyiratokGetTomegesOlvasasiJog
--go


create procedure sp_EREC_UgyUgyiratokGetTomegesOlvasasiJog
			@UgyiratIds nvarchar(max),
			@Felhasznalo_Id uniqueidentifier,
			@FelhasznaloSzervezet_Id uniqueidentifier
			         
as

begin
BEGIN TRY

	set nocount on

	declare @ugyiratok table(Id uniqueidentifier, Csoport_Id_Felelos uniqueidentifier, FelhasznaloCsoport_Id_Orzo uniqueidentifier, Csoport_Id_Ugyfelelos uniqueidentifier)
	insert into @ugyiratok
	select Id, Csoport_Id_Felelos, FelhasznaloCsoport_Id_Orzo, Csoport_Id_Ugyfelelos from EREC_UgyUgyiratok
	where Id in (select Value from dbo.fn_Split(@UgyiratIds,','))

	declare @jogosultUgyiratok table(Id uniqueidentifier)

	--Kezel�
	insert into @jogosultUgyiratok
	select ugyiratok.Id from @ugyiratok ugyiratok
	where Csoport_Id_Felelos = @Felhasznalo_Id

	--Irat helye
	insert into @jogosultUgyiratok
	select ugyiratok.Id from @ugyiratok ugyiratok
	where FelhasznaloCsoport_Id_Orzo = @Felhasznalo_Id

	--Szervezeti egys�g vezet�je
	insert into @jogosultUgyiratok
	select ugyiratok.Id from @ugyiratok ugyiratok
	join KRT_CsoportTagok cst on ugyiratok.Csoport_Id_Ugyfelelos = cst.Csoport_Id 
	and cst.Tipus='3' and getdate() between cst.ErvKezd and cst.ErvVege
	where cst.Csoport_Id_Jogalany = @Felhasznalo_Id

	--Szervezeti egys�g asszistense
	insert into @jogosultUgyiratok
	select ugyiratok.Id from @ugyiratok ugyiratok
	join KRT_PartnerKapcsolatok pk on ugyiratok.Csoport_Id_Ugyfelelos = pk.Partner_id_kapcsolt 
	and pk.Tipus='SZA' and getdate() between pk.ErvKezd and pk.ErvVege
	join KRT_Partnerek p on pk.Partner_id = p.Id
	join KRT_Felhasznalok f on p.Id = f.Partner_id
	where f.Id = @Felhasznalo_Id

	select distinct Id from @jogosultUgyiratok 


END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end
go
