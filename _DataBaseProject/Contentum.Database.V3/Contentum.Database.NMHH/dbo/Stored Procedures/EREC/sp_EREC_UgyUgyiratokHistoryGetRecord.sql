﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_UgyUgyiratokHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_EREC_UgyUgyiratokHistoryGetRecord
go
*/
create procedure sp_EREC_UgyUgyiratokHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_UgyUgyiratokHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_UgyUgyiratokHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Foszam' as ColumnName,               
               cast(Old.Foszam as nvarchar(99)) as OldValue,
               cast(New.Foszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Foszam as nvarchar(max)),'') != ISNULL(CAST(New.Foszam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Sorszam' as ColumnName,               
               cast(Old.Sorszam as nvarchar(99)) as OldValue,
               cast(New.Sorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Sorszam as nvarchar(max)),'') != ISNULL(CAST(New.Sorszam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ugyazonosito' as ColumnName,               
               cast(Old.Ugyazonosito  as nvarchar(99)) as OldValue,
               cast(New.Ugyazonosito  as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Ugyazonosito  as nvarchar(max)),'') != ISNULL(CAST(New.Ugyazonosito  as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Alkalmazas_Id' as ColumnName,               
               cast(Old.Alkalmazas_Id as nvarchar(99)) as OldValue,
               cast(New.Alkalmazas_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Alkalmazas_Id as nvarchar(max)),'') != ISNULL(CAST(New.Alkalmazas_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesModja' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyintezesModja as nvarchar(max)),'') != ISNULL(CAST(New.UgyintezesModja as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
		 left join KRT_KodCsoportok KCs on KCs.Kod = 'IRAT_UGYINT_MODJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.UgyintezesModja and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.UgyintezesModja and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Hatarido' as ColumnName,               
               cast(Old.Hatarido as nvarchar(99)) as OldValue,
               cast(New.Hatarido as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Hatarido as nvarchar(max)),'') != ISNULL(CAST(New.Hatarido as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SkontrobaDat' as ColumnName,               
               cast(Old.SkontrobaDat as nvarchar(99)) as OldValue,
               cast(New.SkontrobaDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SkontrobaDat as nvarchar(max)),'') != ISNULL(CAST(New.SkontrobaDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'LezarasDat' as ColumnName,               
               cast(Old.LezarasDat as nvarchar(99)) as OldValue,
               cast(New.LezarasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.LezarasDat as nvarchar(max)),'') != ISNULL(CAST(New.LezarasDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarbaKuldDatuma' as ColumnName,               
               cast(Old.IrattarbaKuldDatuma  as nvarchar(99)) as OldValue,
               cast(New.IrattarbaKuldDatuma  as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IrattarbaKuldDatuma  as nvarchar(max)),'') != ISNULL(CAST(New.IrattarbaKuldDatuma  as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarbaVetelDat' as ColumnName,               
               cast(Old.IrattarbaVetelDat as nvarchar(99)) as OldValue,
               cast(New.IrattarbaVetelDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IrattarbaVetelDat as nvarchar(max)),'') != ISNULL(CAST(New.IrattarbaVetelDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhCsoport_Id_IrattariAtvevo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhCsoport_Id_IrattariAtvevo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhCsoport_Id_IrattariAtvevo) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhCsoport_Id_IrattariAtvevo as nvarchar(max)),'') != ISNULL(CAST(New.FelhCsoport_Id_IrattariAtvevo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SelejtezesDat' as ColumnName,               
               cast(Old.SelejtezesDat as nvarchar(99)) as OldValue,
               cast(New.SelejtezesDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SelejtezesDat as nvarchar(max)),'') != ISNULL(CAST(New.SelejtezesDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhCsoport_Id_Selejtezo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhCsoport_Id_Selejtezo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhCsoport_Id_Selejtezo) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhCsoport_Id_Selejtezo as nvarchar(max)),'') != ISNULL(CAST(New.FelhCsoport_Id_Selejtezo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'LeveltariAtvevoNeve' as ColumnName,               
               cast(Old.LeveltariAtvevoNeve as nvarchar(99)) as OldValue,
               cast(New.LeveltariAtvevoNeve as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.LeveltariAtvevoNeve as nvarchar(max)),'') != ISNULL(CAST(New.LeveltariAtvevoNeve as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhCsoport_Id_Felulvizsgalo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhCsoport_Id_Felulvizsgalo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhCsoport_Id_Felulvizsgalo) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhCsoport_Id_Felulvizsgalo as nvarchar(max)),'') != ISNULL(CAST(New.FelhCsoport_Id_Felulvizsgalo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelulvizsgalatDat' as ColumnName,               
               cast(Old.FelulvizsgalatDat as nvarchar(99)) as OldValue,
               cast(New.FelulvizsgalatDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelulvizsgalatDat as nvarchar(max)),'') != ISNULL(CAST(New.FelulvizsgalatDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IktatoszamKieg' as ColumnName,               
               cast(Old.IktatoszamKieg as nvarchar(99)) as OldValue,
               cast(New.IktatoszamKieg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IktatoszamKieg as nvarchar(max)),'') != ISNULL(CAST(New.IktatoszamKieg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Targy' as ColumnName,               
               cast(Old.Targy as nvarchar(99)) as OldValue,
               cast(New.Targy as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Targy as nvarchar(max)),'') != ISNULL(CAST(New.Targy as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyUgyirat_Id_Szulo' as ColumnName,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratokAzonosito(Old.UgyUgyirat_Id_Szulo) as OldValue,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratokAzonosito(New.UgyUgyirat_Id_Szulo) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyUgyirat_Id_Szulo as nvarchar(max)),'') != ISNULL(CAST(New.UgyUgyirat_Id_Szulo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_UgyUgyiratok FTOld on FTOld.Id = Old.UgyUgyirat_Id_Szulo and FTOld.Ver = Old.Ver
         left join EREC_UgyUgyiratok FTNew on FTNew.Id = New.UgyUgyirat_Id_Szulo and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyUgyirat_Id_Kulso' as ColumnName,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratokAzonosito(Old.UgyUgyirat_Id_Kulso) as OldValue,
/*FK*/           dbo.fn_GetEREC_UgyUgyiratokAzonosito(New.UgyUgyirat_Id_Kulso) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyUgyirat_Id_Kulso as nvarchar(max)),'') != ISNULL(CAST(New.UgyUgyirat_Id_Kulso as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyTipus' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyTipus as nvarchar(max)),'') != ISNULL(CAST(New.UgyTipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
		 left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYTIPUS'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.UgyTipus and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.UgyTipus and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattariHely' as ColumnName,               
               cast(Old.IrattariHely as nvarchar(99)) as OldValue,
               cast(New.IrattariHely as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IrattariHely as nvarchar(max)),'') != ISNULL(CAST(New.IrattariHely as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SztornirozasDat' as ColumnName,               
               cast(Old.SztornirozasDat as nvarchar(99)) as OldValue,
               cast(New.SztornirozasDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SztornirozasDat as nvarchar(max)),'') != ISNULL(CAST(New.SztornirozasDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Felelos' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Felelos) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Felelos) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id_Felelos as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id_Felelos as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Csoportok FTOld on FTOld.Id = Old.Csoport_Id_Felelos and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Csoport_Id_Felelos and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Orzo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Orzo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Orzo) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Orzo as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Orzo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Orzo and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Orzo and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Cimzett' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Cimzett) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Cimzett) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id_Cimzett as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id_Cimzett as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Jelleg' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Jelleg as nvarchar(max)),'') != ISNULL(CAST(New.Jelleg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGY_JELLEG'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Jelleg and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Jelleg and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIrattariTetel_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_IraIrattariTetelekAzonosito(Old.IraIrattariTetel_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_IraIrattariTetelekAzonosito(New.IraIrattariTetel_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IraIrattariTetel_Id as nvarchar(max)),'') != ISNULL(CAST(New.IraIrattariTetel_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_IraIrattariTetelek FTOld on FTOld.Id = Old.IraIrattariTetel_Id and FTOld.Ver = Old.Ver
         left join EREC_IraIrattariTetelek FTNew on FTNew.Id = New.IraIrattariTetel_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIktatokonyv_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_IraIktatoKonyvekAzonosito(Old.IraIktatokonyv_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_IraIktatoKonyvekAzonosito(New.IraIktatokonyv_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IraIktatokonyv_Id as nvarchar(max)),'') != ISNULL(CAST(New.IraIktatokonyv_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_IraIktatoKonyvek FTOld on FTOld.Id = Old.IraIktatokonyv_Id and FTOld.Ver = Old.Ver
         left join EREC_IraIktatoKonyvek FTNew on FTNew.Id = New.IraIktatokonyv_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SkontroOka' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SkontroOka as nvarchar(max)),'') != ISNULL(CAST(New.SkontroOka as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
		 left join KRT_KodCsoportok KCs on KCs.Kod = 'SKONTRO_OKA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.SkontroOka and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.SkontroOka and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SkontroVege' as ColumnName,               
               cast(Old.SkontroVege as nvarchar(99)) as OldValue,
               cast(New.SkontroVege as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SkontroVege as nvarchar(max)),'') != ISNULL(CAST(New.SkontroVege as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Surgosseg' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Surgosseg as nvarchar(max)),'') != ISNULL(CAST(New.Surgosseg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'SURGOSSEG'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Surgosseg and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Surgosseg and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SkontrobanOsszesen' as ColumnName,               
               cast(Old.SkontrobanOsszesen as nvarchar(99)) as OldValue,
               cast(New.SkontrobanOsszesen as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SkontrobanOsszesen as nvarchar(max)),'') != ISNULL(CAST(New.SkontrobanOsszesen as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MegorzesiIdoVege' as ColumnName,               
               cast(Old.MegorzesiIdoVege as nvarchar(99)) as OldValue,
               cast(New.MegorzesiIdoVege as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MegorzesiIdoVege as nvarchar(max)),'') != ISNULL(CAST(New.MegorzesiIdoVege as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id_Ugyindito' as ColumnName,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(Old.Partner_Id_Ugyindito) as OldValue,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(New.Partner_Id_Ugyindito) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Partner_Id_Ugyindito as nvarchar(max)),'') != ISNULL(CAST(New.Partner_Id_Ugyindito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Partnerek FTOld on FTOld.Id = Old.Partner_Id_Ugyindito and FTOld.Ver = Old.Ver
         left join KRT_Partnerek FTNew on FTNew.Id = New.Partner_Id_Ugyindito and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'NevSTR_Ugyindito' as ColumnName,               
               cast(Old.NevSTR_Ugyindito as nvarchar(99)) as OldValue,
               cast(New.NevSTR_Ugyindito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.NevSTR_Ugyindito as nvarchar(max)),'') != ISNULL(CAST(New.NevSTR_Ugyindito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ElintezesDat' as ColumnName,               
               cast(Old.ElintezesDat as nvarchar(99)) as OldValue,
               cast(New.ElintezesDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ElintezesDat as nvarchar(max)),'') != ISNULL(CAST(New.ElintezesDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelhasznaloCsoport_Id_Ugyintez' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.FelhasznaloCsoport_Id_Ugyintez) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.FelhasznaloCsoport_Id_Ugyintez) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelhasznaloCsoport_Id_Ugyintez as nvarchar(max)),'') != ISNULL(CAST(New.FelhasznaloCsoport_Id_Ugyintez as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Csoportok FTOld on FTOld.Id = Old.FelhasznaloCsoport_Id_Ugyintez and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.FelhasznaloCsoport_Id_Ugyintez and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Felelos_Elozo' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Felelos_Elozo) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Felelos_Elozo) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id_Felelos_Elozo as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id_Felelos_Elozo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Csoportok FTOld on FTOld.Id = Old.Csoport_Id_Felelos_Elozo and FTOld.Ver = Old.Ver
         left join KRT_Csoportok FTNew on FTNew.Id = New.Csoport_Id_Felelos_Elozo and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KolcsonKikerDat' as ColumnName,               
               cast(Old.KolcsonKikerDat as nvarchar(99)) as OldValue,
               cast(New.KolcsonKikerDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KolcsonKikerDat as nvarchar(max)),'') != ISNULL(CAST(New.KolcsonKikerDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KolcsonKiadDat' as ColumnName,               
               cast(Old.KolcsonKiadDat as nvarchar(99)) as OldValue,
               cast(New.KolcsonKiadDat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KolcsonKiadDat as nvarchar(max)),'') != ISNULL(CAST(New.KolcsonKiadDat as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kolcsonhatarido' as ColumnName,               
               cast(Old.Kolcsonhatarido as nvarchar(99)) as OldValue,
               cast(New.Kolcsonhatarido as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Kolcsonhatarido as nvarchar(max)),'') != ISNULL(CAST(New.Kolcsonhatarido as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'BARCODE' as ColumnName,               
               cast(Old.BARCODE as nvarchar(99)) as OldValue,
               cast(New.BARCODE as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.BARCODE as nvarchar(max)),'') != ISNULL(CAST(New.BARCODE as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IratMetadefinicio_Id' as ColumnName,               
               cast(Old.IratMetadefinicio_Id as nvarchar(99)) as OldValue,
               cast(New.IratMetadefinicio_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IratMetadefinicio_Id as nvarchar(max)),'') != ISNULL(CAST(New.IratMetadefinicio_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Allapot as nvarchar(max)),'') != ISNULL(CAST(New.Allapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYIRAT_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Allapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Allapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'TovabbitasAlattAllapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.TovabbitasAlattAllapot as nvarchar(max)),'') != ISNULL(CAST(New.TovabbitasAlattAllapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYIRAT_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.TovabbitasAlattAllapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.TovabbitasAlattAllapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Megjegyzes' as ColumnName,               
               cast(Old.Megjegyzes as nvarchar(99)) as OldValue,
               cast(New.Megjegyzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Megjegyzes as nvarchar(max)),'') != ISNULL(CAST(New.Megjegyzes as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Megjegyzes' as ColumnName,               
               cast(Old.Note as nvarchar(99)) as OldValue,
               cast(New.Note as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Note as nvarchar(max)),'') != ISNULL(CAST(New.Note as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Azonosito' as ColumnName,               
               cast(Old.Azonosito as nvarchar(99)) as OldValue,
               cast(New.Azonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Azonosito as nvarchar(max)),'') != ISNULL(CAST(New.Azonosito as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Fizikai_Kezbesitesi_Allapot' as ColumnName,               
               cast(Old.Fizikai_Kezbesitesi_Allapot as nvarchar(99)) as OldValue,
               cast(New.Fizikai_Kezbesitesi_Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Fizikai_Kezbesitesi_Allapot as nvarchar(max)),'') != ISNULL(CAST(New.Fizikai_Kezbesitesi_Allapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kovetkezo_Orzo_Id' as ColumnName,               
               cast(Old.Kovetkezo_Orzo_Id as nvarchar(99)) as OldValue,
               cast(New.Kovetkezo_Orzo_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Kovetkezo_Orzo_Id as nvarchar(max)),'') != ISNULL(CAST(New.Kovetkezo_Orzo_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Csoport_Id_Ugyfelelos' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(Old.Csoport_Id_Ugyfelelos) as OldValue,
/*FK*/           dbo.fn_GetKRT_CsoportokAzonosito(New.Csoport_Id_Ugyfelelos) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Csoport_Id_Ugyfelelos as nvarchar(max)),'') != ISNULL(CAST(New.Csoport_Id_Ugyfelelos as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Elektronikus_Kezbesitesi_Allap' as ColumnName,               
               cast(Old.Elektronikus_Kezbesitesi_Allap as nvarchar(99)) as OldValue,
               cast(New.Elektronikus_Kezbesitesi_Allap as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Elektronikus_Kezbesitesi_Allap as nvarchar(max)),'') != ISNULL(CAST(New.Elektronikus_Kezbesitesi_Allap as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kovetkezo_Felelos_Id' as ColumnName,               
               cast(Old.Kovetkezo_Felelos_Id as nvarchar(99)) as OldValue,
               cast(New.Kovetkezo_Felelos_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Kovetkezo_Felelos_Id as nvarchar(max)),'') != ISNULL(CAST(New.Kovetkezo_Felelos_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UtolsoAlszam' as ColumnName,               
               cast(Old.UtolsoAlszam as nvarchar(99)) as OldValue,
               cast(New.UtolsoAlszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UtolsoAlszam as nvarchar(max)),'') != ISNULL(CAST(New.UtolsoAlszam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UtolsoSorszam' as ColumnName,               
               cast(Old.UtolsoSorszam as nvarchar(99)) as OldValue,
               cast(New.UtolsoSorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UtolsoSorszam as nvarchar(max)),'') != ISNULL(CAST(New.UtolsoSorszam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IratSzam' as ColumnName,               
               cast(Old.IratSzam as nvarchar(99)) as OldValue,
               cast(New.IratSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IratSzam as nvarchar(max)),'') != ISNULL(CAST(New.IratSzam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ElintezesMod' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ElintezesMod as nvarchar(max)),'') != ISNULL(CAST(New.ElintezesMod as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'ELINTEZES_MOD'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.ElintezesMod and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.ElintezesMod and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'RegirendszerIktatoszam' as ColumnName,               
               cast(Old.RegirendszerIktatoszam as nvarchar(99)) as OldValue,
               cast(New.RegirendszerIktatoszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.RegirendszerIktatoszam as nvarchar(max)),'') != ISNULL(CAST(New.RegirendszerIktatoszam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'GeneraltTargy' as ColumnName,               
               cast(Old.GeneraltTargy as nvarchar(99)) as OldValue,
               cast(New.GeneraltTargy as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.GeneraltTargy as nvarchar(max)),'') != ISNULL(CAST(New.GeneraltTargy as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Cim_Id_Ugyindito' as ColumnName,
/*FK*/           dbo.fn_GetKRT_CimekAzonosito(Old.Cim_Id_Ugyindito) as OldValue,
/*FK*/           dbo.fn_GetKRT_CimekAzonosito(New.Cim_Id_Ugyindito) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Cim_Id_Ugyindito  as nvarchar(max)),'') != ISNULL(CAST(New.Cim_Id_Ugyindito  as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CimSTR_Ugyindito' as ColumnName,               
               cast(Old.CimSTR_Ugyindito  as nvarchar(99)) as OldValue,
               cast(New.CimSTR_Ugyindito  as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.CimSTR_Ugyindito  as nvarchar(max)),'') != ISNULL(CAST(New.CimSTR_Ugyindito  as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'LezarasOka' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.LezarasOka as nvarchar(max)),'') != ISNULL(CAST(New.LezarasOka as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'LEZARAS_OKA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.LezarasOka and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.LezarasOka and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelfuggesztesOka' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelfuggesztesOka as nvarchar(max)),'') != ISNULL(CAST(New.FelfuggesztesOka as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYIRAT_FELFUGGESZTES_OKA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and charindex(OldKt.Kod COLLATE Hungarian_CI_AS , Old.FelfuggesztesOka) > 0 and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and charindex(NewKT.Kod COLLATE Hungarian_CI_AS , New.FelfuggesztesOka) > 0 and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarId' as ColumnName,               
               cast(Old.IrattarId as nvarchar(99)) as OldValue,
               cast(New.IrattarId as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IrattarId as nvarchar(max)),'') != ISNULL(CAST(New.IrattarId as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SkontroOka_Kod' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SkontroOka_Kod as nvarchar(max)),'') != ISNULL(CAST(New.SkontroOka_Kod as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'SKONTRO_OKA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.SkontroOka_Kod and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.SkontroOka_Kod and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UjOrzesiIdo' as ColumnName,               
               cast(Old.UjOrzesiIdo as nvarchar(99)) as OldValue,
               cast(New.UjOrzesiIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UjOrzesiIdo as nvarchar(max)),'') != ISNULL(CAST(New.UjOrzesiIdo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UjOrzesiIdoIdoegyseg' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UjOrzesiIdoIdoegyseg as nvarchar(max)),'') != ISNULL(CAST(New.UjOrzesiIdoIdoegyseg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IDOEGYSEG'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.UjOrzesiIdoIdoegyseg and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.UjOrzesiIdoIdoegyseg and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UgyintezesKezdete' as ColumnName,               
               cast(Old.UgyintezesKezdete as nvarchar(99)) as OldValue,
               cast(New.UgyintezesKezdete as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UgyintezesKezdete as nvarchar(max)),'') != ISNULL(CAST(New.UgyintezesKezdete as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FelfuggesztettNapokSzama' as ColumnName,               
               cast(Old.FelfuggesztettNapokSzama as nvarchar(99)) as OldValue,
               cast(New.FelfuggesztettNapokSzama as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FelfuggesztettNapokSzama as nvarchar(max)),'') != ISNULL(CAST(New.FelfuggesztettNapokSzama as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IntezesiIdo' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IntezesiIdo as nvarchar(max)),'') != ISNULL(CAST(New.IntezesiIdo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYIRAT_INTEZESI_IDO'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.IntezesiIdo and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.IntezesiIdo and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IntezesiIdoegyseg' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IntezesiIdoegyseg as nvarchar(max)),'') != ISNULL(CAST(New.IntezesiIdoegyseg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IDOEGYSEG'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.IntezesiIdoegyseg and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.IntezesiIdoegyseg and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Ugy_Fajtaja' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Ugy_Fajtaja as nvarchar(max)),'') != ISNULL(CAST(New.Ugy_Fajtaja as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGY_FAJTAJA'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Ugy_Fajtaja and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Ugy_Fajtaja and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzignaloId' as ColumnName,               
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(Old.SzignaloId) as OldValue,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(New.SzignaloId) as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SzignaloId as nvarchar(max)),'') != ISNULL(CAST(New.SzignaloId as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SzignalasIdeje' as ColumnName,               
               cast(Old.SzignalasIdeje as nvarchar(99)) as OldValue,
               cast(New.SzignalasIdeje as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SzignalasIdeje as nvarchar(max)),'') != ISNULL(CAST(New.SzignalasIdeje as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ElteltIdo' as ColumnName,               
               cast(Old.ElteltIdo as nvarchar(99)) as OldValue,
               cast(New.ElteltIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ElteltIdo as nvarchar(max)),'') != ISNULL(CAST(New.ElteltIdo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ElteltIdoIdoEgyseg' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ElteltIdoIdoEgyseg as nvarchar(max)),'') != ISNULL(CAST(New.ElteltIdoIdoEgyseg as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
		 left join KRT_KodCsoportok KCs on KCs.Kod = 'IDOEGYSEG'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.ElteltIdoIdoEgyseg and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.ElteltIdoIdoEgyseg and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ElteltidoAllapot' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ElteltidoAllapot as nvarchar(max)),'') != ISNULL(CAST(New.ElteltidoAllapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYIRAT_ELTELT_IDO_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.ElteltidoAllapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.ElteltidoAllapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'SakkoraAllapot' as ColumnName,
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.SakkoraAllapot as nvarchar(max)),'') != ISNULL(CAST(New.SakkoraAllapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'IRAT_HATASA_UGYINTEZESRE'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.SakkoraAllapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.SakkoraAllapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege
      where Old.Id = @RecordId)    
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ElteltIdoUtolsoModositas' as ColumnName,               
               cast(Old.ElteltIdoUtolsoModositas as nvarchar(99)) as OldValue,
               cast(New.ElteltIdoUtolsoModositas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ElteltIdoUtolsoModositas as nvarchar(max)),'') != ISNULL(CAST(New.ElteltIdoUtolsoModositas as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId) 
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatralevoNapok' as ColumnName,               
               cast(Old.HatralevoNapok as nvarchar(99)) as OldValue,
               cast(New.HatralevoNapok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HatralevoNapok as nvarchar(max)),'') != ISNULL(CAST(New.HatralevoNapok as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'HatralevoMunkaNapok' as ColumnName,               
               cast(Old.HatralevoMunkaNapok as nvarchar(99)) as OldValue,
               cast(New.HatralevoMunkaNapok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.HatralevoMunkaNapok as nvarchar(max)),'') != ISNULL(CAST(New.HatralevoMunkaNapok as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ElozoAllapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ElozoAllapot as nvarchar(max)),'') != ISNULL(CAST(New.ElozoAllapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UGYIRAT_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.ElozoAllapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.ElozoAllapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Aktiv' as ColumnName,
			   case Old.Aktiv when 0 then 'Nem' when 1 then 'Igen' end as OldValue,
               case New.Aktiv when 0 then 'Nem' when 1 then 'Igen' end as NewValue,
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Aktiv as nvarchar(max)),'') != ISNULL(CAST(New.Aktiv as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IrattarHelyfoglalas' as ColumnName,               
               cast(Old.IrattarHelyfoglalas as nvarchar(99)) as OldValue,
               cast(New.IrattarHelyfoglalas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyUgyiratokHistory Old
         inner join EREC_UgyUgyiratokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyUgyiratokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IrattarHelyfoglalas as nvarchar(max)),'') != ISNULL(CAST(New.IrattarHelyfoglalas as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go