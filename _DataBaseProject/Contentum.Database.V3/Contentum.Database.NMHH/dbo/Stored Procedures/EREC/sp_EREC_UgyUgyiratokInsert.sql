﻿/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_UgyUgyiratokInsert')
            and   type = 'P')
   drop procedure sp_EREC_UgyUgyiratokInsert
go
*/
create procedure sp_EREC_UgyUgyiratokInsert    
                @Id      uniqueidentifier = null,    
                               @Foszam     int  = null,
                @Sorszam     int  = null,
                @Ugyazonosito      Nvarchar(100)  = null,
                @Alkalmazas_Id     uniqueidentifier  = null,
                @UgyintezesModja     nvarchar(64)  = null,
                @Hatarido     datetime  = null,
                @SkontrobaDat     datetime  = null,
                @LezarasDat     datetime  = null,
                @IrattarbaKuldDatuma      datetime  = null,
                @IrattarbaVetelDat     datetime  = null,
                @FelhCsoport_Id_IrattariAtvevo     uniqueidentifier  = null,
                @SelejtezesDat     datetime  = null,
                @FelhCsoport_Id_Selejtezo     uniqueidentifier  = null,
                @LeveltariAtvevoNeve     Nvarchar(100)  = null,
                @FelhCsoport_Id_Felulvizsgalo     uniqueidentifier  = null,
                @FelulvizsgalatDat     datetime  = null,
                @IktatoszamKieg     Nvarchar(2)  = null,
                @Targy     Nvarchar(4000)  = null,
                @UgyUgyirat_Id_Szulo     uniqueidentifier  = null,
                @UgyUgyirat_Id_Kulso     uniqueidentifier  = null,
                @UgyTipus     nvarchar(64)  = null,
                @IrattariHely     Nvarchar(100)  = null,
                @SztornirozasDat     datetime  = null,
                @Csoport_Id_Felelos     uniqueidentifier  = null,
                @FelhasznaloCsoport_Id_Orzo     uniqueidentifier  = null,
                @Csoport_Id_Cimzett     uniqueidentifier  = null,
                @Jelleg     nvarchar(64)  = null,
                @IraIrattariTetel_Id     uniqueidentifier  = null,
                @IraIktatokonyv_Id     uniqueidentifier  = null,
                @SkontroOka     Nvarchar(400)  = null,
                @SkontroVege     datetime  = null,
                @Surgosseg     nvarchar(64)  = null,
                @SkontrobanOsszesen     int  = null,
                @MegorzesiIdoVege     datetime  = null,
                @Partner_Id_Ugyindito     uniqueidentifier  = null,
                @NevSTR_Ugyindito     Nvarchar(400)  = null,
                @ElintezesDat     datetime  = null,
                @FelhasznaloCsoport_Id_Ugyintez     uniqueidentifier  = null,
                @Csoport_Id_Felelos_Elozo     uniqueidentifier  = null,
                @KolcsonKikerDat     datetime  = null,
                @KolcsonKiadDat     datetime  = null,
                @Kolcsonhatarido     datetime  = null,
                @BARCODE     Nvarchar(20)  = null,
                @IratMetadefinicio_Id     uniqueidentifier  = null,
                @Allapot     nvarchar(64)  = null,
                @TovabbitasAlattAllapot     nvarchar(64)  = null,
                @Megjegyzes     Nvarchar(400)  = null,
                @Azonosito     Nvarchar(100)  = null,
                @Fizikai_Kezbesitesi_Allapot     nvarchar(64)  = null,
                @Kovetkezo_Orzo_Id     uniqueidentifier  = null,
                @Csoport_Id_Ugyfelelos     uniqueidentifier  = null,
                @Elektronikus_Kezbesitesi_Allap     nvarchar(64)  = null,
                @Kovetkezo_Felelos_Id     uniqueidentifier  = null,
                @UtolsoAlszam     int  = null,
                @UtolsoSorszam     int  = null,
                @IratSzam     int  = null,
                @ElintezesMod     nvarchar(64)  = null,
                @RegirendszerIktatoszam     Nvarchar(100)  = null,
                @GeneraltTargy     Nvarchar(400)  = null,
                @Cim_Id_Ugyindito      uniqueidentifier  = null,
                @CimSTR_Ugyindito      Nvarchar(400)  = null,
                @LezarasOka     nvarchar(64)  = null,
                @FelfuggesztesOka     Nvarchar(400)  = null,
                @IrattarId     uniqueidentifier  = null,
                @SkontroOka_Kod     nvarchar(64)  = null,
                @UjOrzesiIdo     int  = null,
                @UjOrzesiIdoIdoegyseg     nvarchar(64)  = null,
                @UgyintezesKezdete     datetime  = null,
                @FelfuggesztettNapokSzama     int  = null,
                @IntezesiIdo     nvarchar(64)  = null,
                @IntezesiIdoegyseg     nvarchar(64)  = null,
                @Ugy_Fajtaja     nvarchar(64)  = null,
                @SzignaloId     uniqueidentifier  = null,
                @SzignalasIdeje     datetime  = null,
                @ElteltIdo     nvarchar(64)  = null,
                @ElteltIdoIdoEgyseg     nvarchar(64)  = null,
                @ElteltidoAllapot     int  = null,
				@ElteltIdoUtolsoModositas     datetime  = null,
                @SakkoraAllapot     nvarchar(64)  = null,
                @HatralevoNapok     int  = null,
                @HatralevoMunkaNapok     int  = null,
                @ElozoAllapot     nvarchar(64)  = null,
                @Aktiv     int  = null,
                @IrattarHelyfoglalas     float  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Foszam is not null
         begin
            SET @insertColumns = @insertColumns + ',Foszam'
            SET @insertValues = @insertValues + ',@Foszam'
         end 
       
         if @Sorszam is not null
         begin
            SET @insertColumns = @insertColumns + ',Sorszam'
            SET @insertValues = @insertValues + ',@Sorszam'
         end 
       
         if @Ugyazonosito  is not null
         begin
            SET @insertColumns = @insertColumns + ',Ugyazonosito '
            SET @insertValues = @insertValues + ',@Ugyazonosito '
         end 
       
         if @Alkalmazas_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Alkalmazas_Id'
            SET @insertValues = @insertValues + ',@Alkalmazas_Id'
         end 
       
         if @UgyintezesModja is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyintezesModja'
            SET @insertValues = @insertValues + ',@UgyintezesModja'
         end 
       
         if @Hatarido is not null
         begin
            SET @insertColumns = @insertColumns + ',Hatarido'
            SET @insertValues = @insertValues + ',@Hatarido'
         end 
       
         if @SkontrobaDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SkontrobaDat'
            SET @insertValues = @insertValues + ',@SkontrobaDat'
         end 
       
         if @LezarasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',LezarasDat'
            SET @insertValues = @insertValues + ',@LezarasDat'
         end 
       
         if @IrattarbaKuldDatuma  is not null
         begin
            SET @insertColumns = @insertColumns + ',IrattarbaKuldDatuma '
            SET @insertValues = @insertValues + ',@IrattarbaKuldDatuma '
         end 
       
         if @IrattarbaVetelDat is not null
         begin
            SET @insertColumns = @insertColumns + ',IrattarbaVetelDat'
            SET @insertValues = @insertValues + ',@IrattarbaVetelDat'
         end 
       
         if @FelhCsoport_Id_IrattariAtvevo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhCsoport_Id_IrattariAtvevo'
            SET @insertValues = @insertValues + ',@FelhCsoport_Id_IrattariAtvevo'
         end 
       
         if @SelejtezesDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SelejtezesDat'
            SET @insertValues = @insertValues + ',@SelejtezesDat'
         end 
       
         if @FelhCsoport_Id_Selejtezo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhCsoport_Id_Selejtezo'
            SET @insertValues = @insertValues + ',@FelhCsoport_Id_Selejtezo'
         end 
       
         if @LeveltariAtvevoNeve is not null
         begin
            SET @insertColumns = @insertColumns + ',LeveltariAtvevoNeve'
            SET @insertValues = @insertValues + ',@LeveltariAtvevoNeve'
         end 
       
         if @FelhCsoport_Id_Felulvizsgalo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhCsoport_Id_Felulvizsgalo'
            SET @insertValues = @insertValues + ',@FelhCsoport_Id_Felulvizsgalo'
         end 
       
         if @FelulvizsgalatDat is not null
         begin
            SET @insertColumns = @insertColumns + ',FelulvizsgalatDat'
            SET @insertValues = @insertValues + ',@FelulvizsgalatDat'
         end 
       
         if @IktatoszamKieg is not null
         begin
            SET @insertColumns = @insertColumns + ',IktatoszamKieg'
            SET @insertValues = @insertValues + ',@IktatoszamKieg'
         end 
       
         if @Targy is not null
         begin
            SET @insertColumns = @insertColumns + ',Targy'
            SET @insertValues = @insertValues + ',@Targy'
         end 
       
         if @UgyUgyirat_Id_Szulo is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyUgyirat_Id_Szulo'
            SET @insertValues = @insertValues + ',@UgyUgyirat_Id_Szulo'
         end 
       
         if @UgyUgyirat_Id_Kulso is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyUgyirat_Id_Kulso'
            SET @insertValues = @insertValues + ',@UgyUgyirat_Id_Kulso'
         end 
       
         if @UgyTipus is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyTipus'
            SET @insertValues = @insertValues + ',@UgyTipus'
         end 
       
         if @IrattariHely is not null
         begin
            SET @insertColumns = @insertColumns + ',IrattariHely'
            SET @insertValues = @insertValues + ',@IrattariHely'
         end 
       
         if @SztornirozasDat is not null
         begin
            SET @insertColumns = @insertColumns + ',SztornirozasDat'
            SET @insertValues = @insertValues + ',@SztornirozasDat'
         end 
       
         if @Csoport_Id_Felelos is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Felelos'
            SET @insertValues = @insertValues + ',@Csoport_Id_Felelos'
         end 
       
         if @FelhasznaloCsoport_Id_Orzo is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Orzo'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Orzo'
         end 
       
         if @Csoport_Id_Cimzett is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Cimzett'
            SET @insertValues = @insertValues + ',@Csoport_Id_Cimzett'
         end 
       
         if @Jelleg is not null
         begin
            SET @insertColumns = @insertColumns + ',Jelleg'
            SET @insertValues = @insertValues + ',@Jelleg'
         end 
       
         if @IraIrattariTetel_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IraIrattariTetel_Id'
            SET @insertValues = @insertValues + ',@IraIrattariTetel_Id'
         end 
       
         if @IraIktatokonyv_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IraIktatokonyv_Id'
            SET @insertValues = @insertValues + ',@IraIktatokonyv_Id'
         end 
       
         if @SkontroOka is not null
         begin
            SET @insertColumns = @insertColumns + ',SkontroOka'
            SET @insertValues = @insertValues + ',@SkontroOka'
         end 
       
         if @SkontroVege is not null
         begin
            SET @insertColumns = @insertColumns + ',SkontroVege'
            SET @insertValues = @insertValues + ',@SkontroVege'
         end 
       
         if @Surgosseg is not null
         begin
            SET @insertColumns = @insertColumns + ',Surgosseg'
            SET @insertValues = @insertValues + ',@Surgosseg'
         end 
       
         if @SkontrobanOsszesen is not null
         begin
            SET @insertColumns = @insertColumns + ',SkontrobanOsszesen'
            SET @insertValues = @insertValues + ',@SkontrobanOsszesen'
         end 
       
         if @MegorzesiIdoVege is not null
         begin
            SET @insertColumns = @insertColumns + ',MegorzesiIdoVege'
            SET @insertValues = @insertValues + ',@MegorzesiIdoVege'
         end 
       
         if @Partner_Id_Ugyindito is not null
         begin
            SET @insertColumns = @insertColumns + ',Partner_Id_Ugyindito'
            SET @insertValues = @insertValues + ',@Partner_Id_Ugyindito'
         end 
       
         if @NevSTR_Ugyindito is not null
         begin
            SET @insertColumns = @insertColumns + ',NevSTR_Ugyindito'
            SET @insertValues = @insertValues + ',@NevSTR_Ugyindito'
         end 
       
         if @ElintezesDat is not null
         begin
            SET @insertColumns = @insertColumns + ',ElintezesDat'
            SET @insertValues = @insertValues + ',@ElintezesDat'
         end 
       
         if @FelhasznaloCsoport_Id_Ugyintez is not null
         begin
            SET @insertColumns = @insertColumns + ',FelhasznaloCsoport_Id_Ugyintez'
            SET @insertValues = @insertValues + ',@FelhasznaloCsoport_Id_Ugyintez'
         end 
       
         if @Csoport_Id_Felelos_Elozo is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Felelos_Elozo'
            SET @insertValues = @insertValues + ',@Csoport_Id_Felelos_Elozo'
         end 
       
         if @KolcsonKikerDat is not null
         begin
            SET @insertColumns = @insertColumns + ',KolcsonKikerDat'
            SET @insertValues = @insertValues + ',@KolcsonKikerDat'
         end 
       
         if @KolcsonKiadDat is not null
         begin
            SET @insertColumns = @insertColumns + ',KolcsonKiadDat'
            SET @insertValues = @insertValues + ',@KolcsonKiadDat'
         end 
       
         if @Kolcsonhatarido is not null
         begin
            SET @insertColumns = @insertColumns + ',Kolcsonhatarido'
            SET @insertValues = @insertValues + ',@Kolcsonhatarido'
         end 
       
         if @BARCODE is not null
         begin
            SET @insertColumns = @insertColumns + ',BARCODE'
            SET @insertValues = @insertValues + ',@BARCODE'
         end 
       
         if @IratMetadefinicio_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IratMetadefinicio_Id'
            SET @insertValues = @insertValues + ',@IratMetadefinicio_Id'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @TovabbitasAlattAllapot is not null
         begin
            SET @insertColumns = @insertColumns + ',TovabbitasAlattAllapot'
            SET @insertValues = @insertValues + ',@TovabbitasAlattAllapot'
         end 
       
         if @Megjegyzes is not null
         begin
            SET @insertColumns = @insertColumns + ',Megjegyzes'
            SET @insertValues = @insertValues + ',@Megjegyzes'
         end 
       
         if @Azonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',Azonosito'
            SET @insertValues = @insertValues + ',@Azonosito'
         end 
       
         if @Fizikai_Kezbesitesi_Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Fizikai_Kezbesitesi_Allapot'
            SET @insertValues = @insertValues + ',@Fizikai_Kezbesitesi_Allapot'
         end 
       
         if @Kovetkezo_Orzo_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Kovetkezo_Orzo_Id'
            SET @insertValues = @insertValues + ',@Kovetkezo_Orzo_Id'
         end 
       
         if @Csoport_Id_Ugyfelelos is not null
         begin
            SET @insertColumns = @insertColumns + ',Csoport_Id_Ugyfelelos'
            SET @insertValues = @insertValues + ',@Csoport_Id_Ugyfelelos'
         end 
       
         if @Elektronikus_Kezbesitesi_Allap is not null
         begin
            SET @insertColumns = @insertColumns + ',Elektronikus_Kezbesitesi_Allap'
            SET @insertValues = @insertValues + ',@Elektronikus_Kezbesitesi_Allap'
         end 
       
         if @Kovetkezo_Felelos_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Kovetkezo_Felelos_Id'
            SET @insertValues = @insertValues + ',@Kovetkezo_Felelos_Id'
         end 
       
         if @UtolsoAlszam is not null
         begin
            SET @insertColumns = @insertColumns + ',UtolsoAlszam'
            SET @insertValues = @insertValues + ',@UtolsoAlszam'
         end 
       
         if @UtolsoSorszam is not null
         begin
            SET @insertColumns = @insertColumns + ',UtolsoSorszam'
            SET @insertValues = @insertValues + ',@UtolsoSorszam'
         end 
       
         if @IratSzam is not null
         begin
            SET @insertColumns = @insertColumns + ',IratSzam'
            SET @insertValues = @insertValues + ',@IratSzam'
         end 
       
         if @ElintezesMod is not null
         begin
            SET @insertColumns = @insertColumns + ',ElintezesMod'
            SET @insertValues = @insertValues + ',@ElintezesMod'
         end 
       
         if @RegirendszerIktatoszam is not null
         begin
            SET @insertColumns = @insertColumns + ',RegirendszerIktatoszam'
            SET @insertValues = @insertValues + ',@RegirendszerIktatoszam'
         end 
       
         if @GeneraltTargy is not null
         begin
            SET @insertColumns = @insertColumns + ',GeneraltTargy'
            SET @insertValues = @insertValues + ',@GeneraltTargy'
         end 
       
         if @Cim_Id_Ugyindito  is not null
         begin
            SET @insertColumns = @insertColumns + ',Cim_Id_Ugyindito '
            SET @insertValues = @insertValues + ',@Cim_Id_Ugyindito '
         end 
       
         if @CimSTR_Ugyindito  is not null
         begin
            SET @insertColumns = @insertColumns + ',CimSTR_Ugyindito '
            SET @insertValues = @insertValues + ',@CimSTR_Ugyindito '
         end 
       
         if @LezarasOka is not null
         begin
            SET @insertColumns = @insertColumns + ',LezarasOka'
            SET @insertValues = @insertValues + ',@LezarasOka'
         end 
       
         if @FelfuggesztesOka is not null
         begin
            SET @insertColumns = @insertColumns + ',FelfuggesztesOka'
            SET @insertValues = @insertValues + ',@FelfuggesztesOka'
         end 
       
         if @IrattarId is not null
         begin
            SET @insertColumns = @insertColumns + ',IrattarId'
            SET @insertValues = @insertValues + ',@IrattarId'
         end 
       
         if @SkontroOka_Kod is not null
         begin
            SET @insertColumns = @insertColumns + ',SkontroOka_Kod'
            SET @insertValues = @insertValues + ',@SkontroOka_Kod'
         end 
       
         if @UjOrzesiIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',UjOrzesiIdo'
            SET @insertValues = @insertValues + ',@UjOrzesiIdo'
         end 
       
         if @UjOrzesiIdoIdoegyseg is not null
         begin
            SET @insertColumns = @insertColumns + ',UjOrzesiIdoIdoegyseg'
            SET @insertValues = @insertValues + ',@UjOrzesiIdoIdoegyseg'
         end 
       
         if @UgyintezesKezdete is not null
         begin
            SET @insertColumns = @insertColumns + ',UgyintezesKezdete'
            SET @insertValues = @insertValues + ',@UgyintezesKezdete'
         end 
       
         if @FelfuggesztettNapokSzama is not null
         begin
            SET @insertColumns = @insertColumns + ',FelfuggesztettNapokSzama'
            SET @insertValues = @insertValues + ',@FelfuggesztettNapokSzama'
         end 
       
         if @IntezesiIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',IntezesiIdo'
            SET @insertValues = @insertValues + ',@IntezesiIdo'
         end 
       
         if @IntezesiIdoegyseg is not null
         begin
            SET @insertColumns = @insertColumns + ',IntezesiIdoegyseg'
            SET @insertValues = @insertValues + ',@IntezesiIdoegyseg'
         end 
       
         if @Ugy_Fajtaja is not null
         begin
            SET @insertColumns = @insertColumns + ',Ugy_Fajtaja'
            SET @insertValues = @insertValues + ',@Ugy_Fajtaja'
         end 
       
         if @SzignaloId is not null
         begin
            SET @insertColumns = @insertColumns + ',SzignaloId'
            SET @insertValues = @insertValues + ',@SzignaloId'
         end 
       
         if @SzignalasIdeje is not null
         begin
            SET @insertColumns = @insertColumns + ',SzignalasIdeje'
            SET @insertValues = @insertValues + ',@SzignalasIdeje'
         end 
       
         if @ElteltIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ElteltIdo'
            SET @insertValues = @insertValues + ',@ElteltIdo'
         end 
       
         if @ElteltIdoIdoEgyseg is not null
         begin
            SET @insertColumns = @insertColumns + ',ElteltIdoIdoEgyseg'
            SET @insertValues = @insertValues + ',@ElteltIdoIdoEgyseg'
         end 
       
         if @ElteltidoAllapot is not null
         begin
            SET @insertColumns = @insertColumns + ',ElteltidoAllapot'
            SET @insertValues = @insertValues + ',@ElteltidoAllapot'
         end 
		 
		 if @ElteltIdoUtolsoModositas is not null
         begin
            SET @insertColumns = @insertColumns + ',ElteltIdoUtolsoModositas'
            SET @insertValues = @insertValues + ',@ElteltIdoUtolsoModositas'
         end 
		 
         if @SakkoraAllapot is not null
         begin
            SET @insertColumns = @insertColumns + ',SakkoraAllapot'
            SET @insertValues = @insertValues + ',@SakkoraAllapot'
         end 
       
         --if @HatralevoNapok is not null
         --begin
         --   SET @insertColumns = @insertColumns + ',HatralevoNapok'
         --   SET @insertValues = @insertValues + ',@HatralevoNapok'
         --end 
       
         --if @HatralevoMunkaNapok is not null
         --begin
         --   SET @insertColumns = @insertColumns + ',HatralevoMunkaNapok'
         --   SET @insertValues = @insertValues + ',@HatralevoMunkaNapok'
         --end 
       
         if @ElozoAllapot is not null
         begin
            SET @insertColumns = @insertColumns + ',ElozoAllapot'
            SET @insertValues = @insertValues + ',@ElozoAllapot'
         end 
       
         if @Aktiv is not null
         begin
            SET @insertColumns = @insertColumns + ',Aktiv'
            SET @insertValues = @insertValues + ',@Aktiv'
         end 
       
         if @IrattarHelyfoglalas is not null
         begin
            SET @insertColumns = @insertColumns + ',IrattarHelyfoglalas'
            SET @insertValues = @insertValues + ',@IrattarHelyfoglalas'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_UgyUgyiratok ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
      N'@Id uniqueidentifier,@Foszam int,@Sorszam int,@Ugyazonosito  Nvarchar(100),@Alkalmazas_Id uniqueidentifier,@UgyintezesModja nvarchar(64),@Hatarido datetime,@SkontrobaDat datetime,@LezarasDat datetime,@IrattarbaKuldDatuma  datetime,@IrattarbaVetelDat datetime,@FelhCsoport_Id_IrattariAtvevo uniqueidentifier,@SelejtezesDat datetime,@FelhCsoport_Id_Selejtezo uniqueidentifier,@LeveltariAtvevoNeve Nvarchar(100),@FelhCsoport_Id_Felulvizsgalo uniqueidentifier,@FelulvizsgalatDat datetime,@IktatoszamKieg Nvarchar(2),@Targy Nvarchar(4000),@UgyUgyirat_Id_Szulo uniqueidentifier,@UgyUgyirat_Id_Kulso uniqueidentifier,@UgyTipus nvarchar(64),@IrattariHely Nvarchar(100),@SztornirozasDat datetime,@Csoport_Id_Felelos uniqueidentifier,@FelhasznaloCsoport_Id_Orzo uniqueidentifier,@Csoport_Id_Cimzett uniqueidentifier,@Jelleg nvarchar(64),@IraIrattariTetel_Id uniqueidentifier,@IraIktatokonyv_Id uniqueidentifier,@SkontroOka Nvarchar(400),@SkontroVege datetime,@Surgosseg nvarchar(64),@SkontrobanOsszesen int,@MegorzesiIdoVege datetime,@Partner_Id_Ugyindito uniqueidentifier,@NevSTR_Ugyindito Nvarchar(400),@ElintezesDat datetime,@FelhasznaloCsoport_Id_Ugyintez uniqueidentifier,@Csoport_Id_Felelos_Elozo uniqueidentifier,@KolcsonKikerDat datetime,@KolcsonKiadDat datetime,@Kolcsonhatarido datetime,@BARCODE Nvarchar(20),@IratMetadefinicio_Id uniqueidentifier,@Allapot nvarchar(64),@TovabbitasAlattAllapot nvarchar(64),@Megjegyzes Nvarchar(400),@Azonosito Nvarchar(100),@Fizikai_Kezbesitesi_Allapot nvarchar(64),@Kovetkezo_Orzo_Id uniqueidentifier,@Csoport_Id_Ugyfelelos uniqueidentifier,@Elektronikus_Kezbesitesi_Allap nvarchar(64),@Kovetkezo_Felelos_Id uniqueidentifier,@UtolsoAlszam int,@UtolsoSorszam int,@IratSzam int,@ElintezesMod nvarchar(64),@RegirendszerIktatoszam Nvarchar(100),@GeneraltTargy Nvarchar(400),@Cim_Id_Ugyindito  uniqueidentifier,@CimSTR_Ugyindito  Nvarchar(400),@LezarasOka nvarchar(64),@FelfuggesztesOka Nvarchar(400),@IrattarId uniqueidentifier,@SkontroOka_Kod nvarchar(64),@UjOrzesiIdo int,@UjOrzesiIdoIdoegyseg nvarchar(64),@UgyintezesKezdete datetime,@FelfuggesztettNapokSzama int,@IntezesiIdo nvarchar(64),@IntezesiIdoegyseg nvarchar(64),@Ugy_Fajtaja nvarchar(64),@SzignaloId uniqueidentifier,@SzignalasIdeje datetime,@ElteltIdo nvarchar(64),@ElteltIdoIdoEgyseg nvarchar(64),@ElteltidoAllapot int,@ElteltIdoUtolsoModositas datetime,@SakkoraAllapot nvarchar(64),@ElozoAllapot nvarchar(64),@Aktiv int,@IrattarHelyfoglalas float,@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Foszam = @Foszam,@Sorszam = @Sorszam,@Ugyazonosito  = @Ugyazonosito ,@Alkalmazas_Id = @Alkalmazas_Id,@UgyintezesModja = @UgyintezesModja,@Hatarido = @Hatarido,@SkontrobaDat = @SkontrobaDat,@LezarasDat = @LezarasDat,@IrattarbaKuldDatuma  = @IrattarbaKuldDatuma ,@IrattarbaVetelDat = @IrattarbaVetelDat,@FelhCsoport_Id_IrattariAtvevo = @FelhCsoport_Id_IrattariAtvevo,@SelejtezesDat = @SelejtezesDat,@FelhCsoport_Id_Selejtezo = @FelhCsoport_Id_Selejtezo,@LeveltariAtvevoNeve = @LeveltariAtvevoNeve,@FelhCsoport_Id_Felulvizsgalo = @FelhCsoport_Id_Felulvizsgalo,@FelulvizsgalatDat = @FelulvizsgalatDat,@IktatoszamKieg = @IktatoszamKieg,@Targy = @Targy,@UgyUgyirat_Id_Szulo = @UgyUgyirat_Id_Szulo,@UgyUgyirat_Id_Kulso = @UgyUgyirat_Id_Kulso,@UgyTipus = @UgyTipus,@IrattariHely = @IrattariHely,@SztornirozasDat = @SztornirozasDat,@Csoport_Id_Felelos = @Csoport_Id_Felelos,@FelhasznaloCsoport_Id_Orzo = @FelhasznaloCsoport_Id_Orzo,@Csoport_Id_Cimzett = @Csoport_Id_Cimzett,@Jelleg = @Jelleg,@IraIrattariTetel_Id = @IraIrattariTetel_Id,@IraIktatokonyv_Id = @IraIktatokonyv_Id,@SkontroOka = @SkontroOka,@SkontroVege = @SkontroVege,@Surgosseg = @Surgosseg,@SkontrobanOsszesen = @SkontrobanOsszesen,@MegorzesiIdoVege = @MegorzesiIdoVege,@Partner_Id_Ugyindito = @Partner_Id_Ugyindito,@NevSTR_Ugyindito = @NevSTR_Ugyindito,@ElintezesDat = @ElintezesDat,@FelhasznaloCsoport_Id_Ugyintez = @FelhasznaloCsoport_Id_Ugyintez,@Csoport_Id_Felelos_Elozo = @Csoport_Id_Felelos_Elozo,@KolcsonKikerDat = @KolcsonKikerDat,@KolcsonKiadDat = @KolcsonKiadDat,@Kolcsonhatarido = @Kolcsonhatarido,@BARCODE = @BARCODE,@IratMetadefinicio_Id = @IratMetadefinicio_Id,@Allapot = @Allapot,@TovabbitasAlattAllapot = @TovabbitasAlattAllapot,@Megjegyzes = @Megjegyzes,@Azonosito = @Azonosito,@Fizikai_Kezbesitesi_Allapot = @Fizikai_Kezbesitesi_Allapot,@Kovetkezo_Orzo_Id = @Kovetkezo_Orzo_Id,@Csoport_Id_Ugyfelelos = @Csoport_Id_Ugyfelelos,@Elektronikus_Kezbesitesi_Allap = @Elektronikus_Kezbesitesi_Allap,@Kovetkezo_Felelos_Id = @Kovetkezo_Felelos_Id,@UtolsoAlszam = @UtolsoAlszam,@UtolsoSorszam = @UtolsoSorszam,@IratSzam = @IratSzam,@ElintezesMod = @ElintezesMod,@RegirendszerIktatoszam = @RegirendszerIktatoszam,@GeneraltTargy = @GeneraltTargy,@Cim_Id_Ugyindito  = @Cim_Id_Ugyindito ,@CimSTR_Ugyindito  = @CimSTR_Ugyindito ,@LezarasOka = @LezarasOka,@FelfuggesztesOka = @FelfuggesztesOka,@IrattarId = @IrattarId,@SkontroOka_Kod = @SkontroOka_Kod,@UjOrzesiIdo = @UjOrzesiIdo,@UjOrzesiIdoIdoegyseg = @UjOrzesiIdoIdoegyseg,@UgyintezesKezdete = @UgyintezesKezdete,@FelfuggesztettNapokSzama = @FelfuggesztettNapokSzama,@IntezesiIdo = @IntezesiIdo,@IntezesiIdoegyseg = @IntezesiIdoegyseg,@Ugy_Fajtaja = @Ugy_Fajtaja,@SzignaloId = @SzignaloId,@SzignalasIdeje = @SzignalasIdeje,@ElteltIdo = @ElteltIdo,@ElteltIdoIdoEgyseg = @ElteltIdoIdoEgyseg,@ElteltidoAllapot = @ElteltidoAllapot,@ElteltIdoUtolsoModositas = @ElteltIdoUtolsoModositas,@SakkoraAllapot = @SakkoraAllapot,@ElozoAllapot = @ElozoAllapot,@Aktiv = @Aktiv,@IrattarHelyfoglalas = @IrattarHelyfoglalas,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_UgyUgyiratok',@ResultUid
					,'EREC_UgyUgyiratokHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
go

/* használt errorcode-ok:
	50301:
	50302: 'Org' oszlop értéke nem lehet NULL
*/
