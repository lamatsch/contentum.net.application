--set ANSI_NULLS ON
--set QUOTED_IDENTIFIER ON
--go

--if exists (select 1
--            from  sysobjects
--           where  id = object_id('sp_EREC_UgyUgyiratokTomegesInit')
--            and   type = 'P')
--   drop procedure sp_EREC_UgyUgyiratokTomegesInit
--go


CREATE procedure [dbo].[sp_EREC_UgyUgyiratokTomegesInit]
				@IraIktatokonyv_Id     uniqueidentifier  = null,      
                @Targy     Nvarchar(4000)  = null,
				@Allapot     nvarchar(64)  = null,
				@UtolsoAlszam     int = null, 
	            @IratSzam     int = null, 
                @Letrehozo_id     uniqueidentifier  = null,
			    @FoszamList     nvarchar(max)  = null,
				@AzonositoList nvarchar(max) = null
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;

declare @VegrehajtasIdo datetime
set @VegrehajtasIdo = getdate()

declare @temp table(Foszam int, Azonosito nvarchar(100), Pos int)

insert into @temp
select t1.[Value], t2.[Value], t1.Pos from
(select [Value], Pos  from fn_SplitWithPos(@FoszamList, ',')) t1,
(select [Value], Pos  from fn_SplitWithPos(@AzonositoList, ',')) t2
where t1.Pos = t2.Pos


DECLARE @InsertedRow TABLE (id uniqueidentifier, Foszam int )

insert into EREC_UgyUgyiratok
	(IraIktatokonyv_Id,
	Targy,
	Allapot,
	UtolsoAlszam,
	IratSzam,
	Letrehozo_id,
	Foszam,
	Azonosito,
	Csoport_Id_Felelos)
output inserted.id, inserted.Foszam into @InsertedRow
select
	@IraIktatokonyv_Id,
	@Targy,
	@Allapot,
	@UtolsoAlszam,
	@IratSzam,
	@Letrehozo_id,
	t.Foszam,
	t.Azonosito,
	@Letrehozo_id
from
@temp t
order by t.Pos

if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
      /* History Log */
      declare @row_ids varchar(MAX)
      /*** EREC_UgyUgyiratok history log ***/

      set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @InsertedRow FOR XML PATH('')),1, 2, '')
      if @row_ids is not null
         set @row_ids = @row_ids + '''';

      exec sp_LogRecordsToHistory_Tomeges
       @TableName = 'EREC_UgyUgyiratok'
      ,@Row_Ids = @row_ids
      ,@Muvelet = 0
      ,@Vegrehajto_Id = @Letrehozo_id
      ,@VegrehajtasIdo = @VegrehajtasIdo
END            


select i.id from @InsertedRow i
inner join @temp t on i.Foszam=t.Foszam
order by t.Pos
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH