﻿/*

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_UgyUgyiratokUpdate')
            and   type = 'P')
   drop procedure sp_EREC_UgyUgyiratokUpdate
go
*/
create procedure sp_EREC_UgyUgyiratokUpdate
        @Id							uniqueidentifier    ,		
		@ExecutorUserId				uniqueidentifier	,
      @ExecutionTime             datetime,
             @Foszam     int  = null,         
             @Sorszam     int  = null,         
             @Ugyazonosito      Nvarchar(100)  = null,         
             @Alkalmazas_Id     uniqueidentifier  = null,         
             @UgyintezesModja     nvarchar(64)  = null,         
             @Hatarido     datetime  = null,         
             @SkontrobaDat     datetime  = null,         
             @LezarasDat     datetime  = null,         
             @IrattarbaKuldDatuma      datetime  = null,         
             @IrattarbaVetelDat     datetime  = null,         
             @FelhCsoport_Id_IrattariAtvevo     uniqueidentifier  = null,         
             @SelejtezesDat     datetime  = null,         
             @FelhCsoport_Id_Selejtezo     uniqueidentifier  = null,         
             @LeveltariAtvevoNeve     Nvarchar(100)  = null,         
             @FelhCsoport_Id_Felulvizsgalo     uniqueidentifier  = null,         
             @FelulvizsgalatDat     datetime  = null,         
             @IktatoszamKieg     Nvarchar(2)  = null,         
             @Targy     Nvarchar(4000)  = null,         
             @UgyUgyirat_Id_Szulo     uniqueidentifier  = null,         
             @UgyUgyirat_Id_Kulso     uniqueidentifier  = null,         
             @UgyTipus     nvarchar(64)  = null,         
             @IrattariHely     Nvarchar(100)  = null,         
             @SztornirozasDat     datetime  = null,         
             @Csoport_Id_Felelos     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Orzo     uniqueidentifier  = null,         
             @Csoport_Id_Cimzett     uniqueidentifier  = null,         
             @Jelleg     nvarchar(64)  = null,         
             @IraIrattariTetel_Id     uniqueidentifier  = null,         
             @IraIktatokonyv_Id     uniqueidentifier  = null,         
             @SkontroOka     Nvarchar(400)  = null,         
             @SkontroVege     datetime  = null,         
             @Surgosseg     nvarchar(64)  = null,         
             @SkontrobanOsszesen     int  = null,         
             @MegorzesiIdoVege     datetime  = null,         
             @Partner_Id_Ugyindito     uniqueidentifier  = null,         
             @NevSTR_Ugyindito     Nvarchar(400)  = null,         
             @ElintezesDat     datetime  = null,         
             @FelhasznaloCsoport_Id_Ugyintez     uniqueidentifier  = null,         
             @Csoport_Id_Felelos_Elozo     uniqueidentifier  = null,         
             @KolcsonKikerDat     datetime  = null,         
             @KolcsonKiadDat     datetime  = null,         
             @Kolcsonhatarido     datetime  = null,         
             @BARCODE     Nvarchar(20)  = null,         
             @IratMetadefinicio_Id     uniqueidentifier  = null,         
             @Allapot     nvarchar(64)  = null,         
             @TovabbitasAlattAllapot     nvarchar(64)  = null,         
             @Megjegyzes     Nvarchar(400)  = null,         
             @Azonosito     Nvarchar(100)  = null,         
             @Fizikai_Kezbesitesi_Allapot     nvarchar(64)  = null,         
             @Kovetkezo_Orzo_Id     uniqueidentifier  = null,         
             @Csoport_Id_Ugyfelelos     uniqueidentifier  = null,         
             @Elektronikus_Kezbesitesi_Allap     nvarchar(64)  = null,         
             @Kovetkezo_Felelos_Id     uniqueidentifier  = null,         
             @UtolsoAlszam     int  = null,         
             @UtolsoSorszam     int  = null,         
             @IratSzam     int  = null,         
             @ElintezesMod     nvarchar(64)  = null,         
             @RegirendszerIktatoszam     Nvarchar(100)  = null,         
             @GeneraltTargy     Nvarchar(400)  = null,         
             @Cim_Id_Ugyindito      uniqueidentifier  = null,         
             @CimSTR_Ugyindito      Nvarchar(400)  = null,         
             @LezarasOka     nvarchar(64)  = null,         
             @FelfuggesztesOka     Nvarchar(400)  = null,         
             @IrattarId     uniqueidentifier  = null,         
             @SkontroOka_Kod     nvarchar(64)  = null,         
             @UjOrzesiIdo     int  = null,         
             @UgyintezesKezdete     datetime  = null,         
             @FelfuggesztettNapokSzama     int  = null,         
             @IntezesiIdo     nvarchar(64)  = null,         
             @IntezesiIdoegyseg     nvarchar(64)  = null,         
             @Ugy_Fajtaja     nvarchar(64)  = null,         
             @SzignaloId     uniqueidentifier  = null,         
             @SzignalasIdeje     datetime  = null,         
             @ElteltIdo     nvarchar(64)  = null,         
             @ElteltIdoIdoEgyseg     nvarchar(64)  = null,         
             @ElteltidoAllapot     int  = null,         
             @SakkoraAllapot     nvarchar(64)  = null,               
             @HatralevoNapok     int  = null,         
             @HatralevoMunkaNapok     int  = null,
			 @ElozoAllapot     nvarchar(64)  = null,         
			 @IrattarHelyfoglalas float = null,
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,         
       @UpdatedColumns              xml

as

BEGIN TRY
--BEGIN TRANSACTION UpdateTransaction

     DECLARE @Act_Foszam     int         
     DECLARE @Act_Sorszam     int         
     DECLARE @Act_Ugyazonosito      Nvarchar(100)         
     DECLARE @Act_Alkalmazas_Id     uniqueidentifier         
     DECLARE @Act_UgyintezesModja     nvarchar(64)         
     DECLARE @Act_Hatarido     datetime         
     DECLARE @Act_SkontrobaDat     datetime         
     DECLARE @Act_LezarasDat     datetime         
     DECLARE @Act_IrattarbaKuldDatuma      datetime         
     DECLARE @Act_IrattarbaVetelDat     datetime         
     DECLARE @Act_FelhCsoport_Id_IrattariAtvevo     uniqueidentifier         
     DECLARE @Act_SelejtezesDat     datetime         
     DECLARE @Act_FelhCsoport_Id_Selejtezo     uniqueidentifier         
     DECLARE @Act_LeveltariAtvevoNeve     Nvarchar(100)         
     DECLARE @Act_FelhCsoport_Id_Felulvizsgalo     uniqueidentifier         
     DECLARE @Act_FelulvizsgalatDat     datetime         
     DECLARE @Act_IktatoszamKieg     Nvarchar(2)         
     DECLARE @Act_Targy     Nvarchar(4000)         
     DECLARE @Act_UgyUgyirat_Id_Szulo     uniqueidentifier         
     DECLARE @Act_UgyUgyirat_Id_Kulso     uniqueidentifier         
     DECLARE @Act_UgyTipus     nvarchar(64)         
     DECLARE @Act_IrattariHely     Nvarchar(100)         
     DECLARE @Act_SztornirozasDat     datetime         
     DECLARE @Act_Csoport_Id_Felelos     uniqueidentifier         
     DECLARE @Act_FelhasznaloCsoport_Id_Orzo     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Cimzett     uniqueidentifier         
     DECLARE @Act_Jelleg     nvarchar(64)         
     DECLARE @Act_IraIrattariTetel_Id     uniqueidentifier         
     DECLARE @Act_IraIktatokonyv_Id     uniqueidentifier         
     DECLARE @Act_SkontroOka     Nvarchar(400)         
     DECLARE @Act_SkontroVege     datetime         
     DECLARE @Act_Surgosseg     nvarchar(64)         
     DECLARE @Act_SkontrobanOsszesen     int         
     DECLARE @Act_MegorzesiIdoVege     datetime         
     DECLARE @Act_Partner_Id_Ugyindito     uniqueidentifier         
     DECLARE @Act_NevSTR_Ugyindito     Nvarchar(400)         
     DECLARE @Act_ElintezesDat     datetime         
     DECLARE @Act_FelhasznaloCsoport_Id_Ugyintez     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Felelos_Elozo     uniqueidentifier         
     DECLARE @Act_KolcsonKikerDat     datetime         
     DECLARE @Act_KolcsonKiadDat     datetime         
     DECLARE @Act_Kolcsonhatarido     datetime         
     DECLARE @Act_BARCODE     Nvarchar(20)         
     DECLARE @Act_IratMetadefinicio_Id     uniqueidentifier         
     DECLARE @Act_Allapot     nvarchar(64)         
     DECLARE @Act_TovabbitasAlattAllapot     nvarchar(64)         
     DECLARE @Act_Megjegyzes     Nvarchar(400)         
     DECLARE @Act_Azonosito     Nvarchar(100)         
     DECLARE @Act_Fizikai_Kezbesitesi_Allapot     nvarchar(64)         
     DECLARE @Act_Kovetkezo_Orzo_Id     uniqueidentifier         
     DECLARE @Act_Csoport_Id_Ugyfelelos     uniqueidentifier         
     DECLARE @Act_Elektronikus_Kezbesitesi_Allap     nvarchar(64)         
     DECLARE @Act_Kovetkezo_Felelos_Id     uniqueidentifier         
     DECLARE @Act_UtolsoAlszam     int         
     DECLARE @Act_UtolsoSorszam     int         
     DECLARE @Act_IratSzam     int         
     DECLARE @Act_ElintezesMod     nvarchar(64)         
     DECLARE @Act_RegirendszerIktatoszam     Nvarchar(100)         
     DECLARE @Act_GeneraltTargy     Nvarchar(400)         
     DECLARE @Act_Cim_Id_Ugyindito      uniqueidentifier         
     DECLARE @Act_CimSTR_Ugyindito      Nvarchar(400)         
     DECLARE @Act_LezarasOka     nvarchar(64)         
     DECLARE @Act_FelfuggesztesOka     Nvarchar(400)         
     DECLARE @Act_IrattarId     uniqueidentifier         
     DECLARE @Act_SkontroOka_Kod     nvarchar(64)         
     DECLARE @Act_UjOrzesiIdo     int         
     DECLARE @Act_UgyintezesKezdete     datetime         
     DECLARE @Act_FelfuggesztettNapokSzama     int         
     DECLARE @Act_IntezesiIdo     nvarchar(64)         
     DECLARE @Act_IntezesiIdoegyseg     nvarchar(64)         
     DECLARE @Act_Ugy_Fajtaja     nvarchar(64)         
     DECLARE @Act_SzignaloId     uniqueidentifier         
     DECLARE @Act_SzignalasIdeje     datetime         
     DECLARE @Act_ElteltIdo     nvarchar(64)         
     DECLARE @Act_ElteltIdoIdoEgyseg     nvarchar(64)         
     DECLARE @Act_ElteltidoAllapot     int         
     DECLARE @Act_SakkoraAllapot     nvarchar(64)                
     DECLARE @Act_HatralevoNapok     int         
     DECLARE @Act_HatralevoMunkaNapok     int  
	 DECLARE @Act_ElozoAllapot     nvarchar(64) 
	 DECLARE @Act_IrattarHelyfoglalas     float 	        
     DECLARE @Act_Ver     int         
     DECLARE @Act_Note     Nvarchar(4000)         
     DECLARE @Act_Stat_id     uniqueidentifier         
     DECLARE @Act_ErvKezd     datetime         
     DECLARE @Act_ErvVege     datetime         
     DECLARE @Act_Letrehozo_id     uniqueidentifier         
     DECLARE @Act_LetrehozasIdo     datetime         
     DECLARE @Act_Modosito_id     uniqueidentifier         
     DECLARE @Act_ModositasIdo     datetime         
     DECLARE @Act_Zarolo_id     uniqueidentifier         
     DECLARE @Act_ZarolasIdo     datetime         
     DECLARE @Act_Tranz_id     uniqueidentifier         
     DECLARE @Act_UIAccessLog_id     uniqueidentifier           
  
set nocount on
-- BUG_11085
DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

select 
     @Act_Foszam = Foszam,
     @Act_Sorszam = Sorszam,
     @Act_Ugyazonosito  = Ugyazonosito ,
     @Act_Alkalmazas_Id = Alkalmazas_Id,
     @Act_UgyintezesModja = UgyintezesModja,
     @Act_Hatarido = Hatarido,
     @Act_SkontrobaDat = SkontrobaDat,
     @Act_LezarasDat = LezarasDat,
     @Act_IrattarbaKuldDatuma  = IrattarbaKuldDatuma ,
     @Act_IrattarbaVetelDat = IrattarbaVetelDat,
     @Act_FelhCsoport_Id_IrattariAtvevo = FelhCsoport_Id_IrattariAtvevo,
     @Act_SelejtezesDat = SelejtezesDat,
     @Act_FelhCsoport_Id_Selejtezo = FelhCsoport_Id_Selejtezo,
     @Act_LeveltariAtvevoNeve = LeveltariAtvevoNeve,
     @Act_FelhCsoport_Id_Felulvizsgalo = FelhCsoport_Id_Felulvizsgalo,
     @Act_FelulvizsgalatDat = FelulvizsgalatDat,
     @Act_IktatoszamKieg = IktatoszamKieg,
     @Act_Targy = Targy,
     @Act_UgyUgyirat_Id_Szulo = UgyUgyirat_Id_Szulo,
     @Act_UgyUgyirat_Id_Kulso = UgyUgyirat_Id_Kulso,
     @Act_UgyTipus = UgyTipus,
     @Act_IrattariHely = IrattariHely,
     @Act_SztornirozasDat = SztornirozasDat,
     @Act_Csoport_Id_Felelos = Csoport_Id_Felelos,
     @Act_FelhasznaloCsoport_Id_Orzo = FelhasznaloCsoport_Id_Orzo,
     @Act_Csoport_Id_Cimzett = Csoport_Id_Cimzett,
     @Act_Jelleg = Jelleg,
     @Act_IraIrattariTetel_Id = IraIrattariTetel_Id,
     @Act_IraIktatokonyv_Id = IraIktatokonyv_Id,
     @Act_SkontroOka = SkontroOka,
     @Act_SkontroVege = SkontroVege,
     @Act_Surgosseg = Surgosseg,
     @Act_SkontrobanOsszesen = SkontrobanOsszesen,
     @Act_MegorzesiIdoVege = MegorzesiIdoVege,
     @Act_Partner_Id_Ugyindito = Partner_Id_Ugyindito,
     @Act_NevSTR_Ugyindito = NevSTR_Ugyindito,
     @Act_ElintezesDat = ElintezesDat,
     @Act_FelhasznaloCsoport_Id_Ugyintez = FelhasznaloCsoport_Id_Ugyintez,
     @Act_Csoport_Id_Felelos_Elozo = Csoport_Id_Felelos_Elozo,
     @Act_KolcsonKikerDat = KolcsonKikerDat,
     @Act_KolcsonKiadDat = KolcsonKiadDat,
     @Act_Kolcsonhatarido = Kolcsonhatarido,
     @Act_BARCODE = BARCODE,
     @Act_IratMetadefinicio_Id = IratMetadefinicio_Id,
     @Act_Allapot = Allapot,
     @Act_TovabbitasAlattAllapot = TovabbitasAlattAllapot,
     @Act_Megjegyzes = Megjegyzes,
     @Act_Azonosito = Azonosito,
     @Act_Fizikai_Kezbesitesi_Allapot = Fizikai_Kezbesitesi_Allapot,
     @Act_Kovetkezo_Orzo_Id = Kovetkezo_Orzo_Id,
     @Act_Csoport_Id_Ugyfelelos = Csoport_Id_Ugyfelelos,
     @Act_Elektronikus_Kezbesitesi_Allap = Elektronikus_Kezbesitesi_Allap,
     @Act_Kovetkezo_Felelos_Id = Kovetkezo_Felelos_Id,
     @Act_UtolsoAlszam = UtolsoAlszam,
     @Act_UtolsoSorszam = UtolsoSorszam,
     @Act_IratSzam = IratSzam,
     @Act_ElintezesMod = ElintezesMod,
     @Act_RegirendszerIktatoszam = RegirendszerIktatoszam,
     @Act_GeneraltTargy = GeneraltTargy,
     @Act_Cim_Id_Ugyindito  = Cim_Id_Ugyindito ,
     @Act_CimSTR_Ugyindito  = CimSTR_Ugyindito ,
     @Act_LezarasOka = LezarasOka,
     @Act_FelfuggesztesOka = FelfuggesztesOka,
     @Act_IrattarId = IrattarId,
     @Act_SkontroOka_Kod = SkontroOka_Kod,
     @Act_UjOrzesiIdo = UjOrzesiIdo,
     @Act_UgyintezesKezdete = UgyintezesKezdete,
     @Act_FelfuggesztettNapokSzama = FelfuggesztettNapokSzama,
     @Act_IntezesiIdo = IntezesiIdo,
     @Act_IntezesiIdoegyseg = IntezesiIdoegyseg,
     @Act_Ugy_Fajtaja = Ugy_Fajtaja,
     @Act_SzignaloId = SzignaloId,
     @Act_SzignalasIdeje = SzignalasIdeje,
     @Act_ElteltIdo = ElteltIdo,
     @Act_ElteltIdoIdoEgyseg = ElteltIdoIdoEgyseg,
     @Act_ElteltidoAllapot = ElteltidoAllapot,
     @Act_SakkoraAllapot = SakkoraAllapot,
	 @Act_ElozoAllapot = ElozoAllapot,
	 @Act_IrattarHelyfoglalas = IrattarHelyfoglalas,
     @Act_Ver = Ver,
     @Act_Note = Note,
     @Act_Stat_id = Stat_id,
     @Act_ErvKezd = ErvKezd,
     @Act_ErvVege = ErvVege,
     @Act_Letrehozo_id = Letrehozo_id,
     @Act_LetrehozasIdo = LetrehozasIdo,
     @Act_Modosito_id = Modosito_id,
     @Act_ModositasIdo = ModositasIdo,
     @Act_Zarolo_id = Zarolo_id,
     @Act_ZarolasIdo = ZarolasIdo,
     @Act_Tranz_id = Tranz_id,
     @Act_UIAccessLog_id = UIAccessLog_id
from EREC_UgyUgyiratok
where Id = @Id


  if (@Ver is null and @Act_Ver is not null) or (@Act_Ver is null and @Ver is not null) 
     or (@Ver is not null and @Act_Ver is not null and @Act_Ver != @Ver)
  begin
       RAISERROR('[50402]',16,1)
       return @@error
  end   
  
  
if @ExecutionTime > @Act_ErvVege
   begin
       RAISERROR('[50403]',16,1)
       return @@error
   end

-- Zárolás ellenőrzés:
if (@Act_Zarolo_id is not null and (@ExecutorUserId is null or @Act_Zarolo_id != @ExecutorUserId))
BEGIN
   RAISERROR('[50499]',16,1)
   return @@error
END


   /* Elsődleges ügyiratban lévő iratpéldányok kigyűjtésee */
   SELECT EREC_PldIratPeldanyok.Id INTO #pldIds
      FROM EREC_PldIratPeldanyok
         INNER JOIN EREC_IraIratok ON EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id
         --INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
         INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id --EREC_UgyUgyiratdarabok.UgyUgyirat_Id
            AND EREC_UgyUgyiratok.Id = @Id
      WHERE EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo
         AND EREC_PldIratPeldanyok.Csoport_Id_Felelos = EREC_UgyUgyiratok.Csoport_Id_Felelos
         


       IF @UpdatedColumns.exist('/root/Foszam')=1
         SET @Act_Foszam = @Foszam
   IF @UpdatedColumns.exist('/root/Sorszam')=1
         SET @Act_Sorszam = @Sorszam
   IF @UpdatedColumns.exist('/root/Ugyazonosito ')=1
         SET @Act_Ugyazonosito  = @Ugyazonosito 
   IF @UpdatedColumns.exist('/root/Alkalmazas_Id')=1
         SET @Act_Alkalmazas_Id = @Alkalmazas_Id
   IF @UpdatedColumns.exist('/root/UgyintezesModja')=1
         SET @Act_UgyintezesModja = @UgyintezesModja
   IF @UpdatedColumns.exist('/root/Hatarido')=1
         SET @Act_Hatarido = @Hatarido
   IF @UpdatedColumns.exist('/root/SkontrobaDat')=1
         SET @Act_SkontrobaDat = @SkontrobaDat
   IF @UpdatedColumns.exist('/root/LezarasDat')=1
         SET @Act_LezarasDat = @LezarasDat
   IF @UpdatedColumns.exist('/root/IrattarbaKuldDatuma ')=1
         SET @Act_IrattarbaKuldDatuma  = @IrattarbaKuldDatuma 
   IF @UpdatedColumns.exist('/root/IrattarbaVetelDat')=1
         SET @Act_IrattarbaVetelDat = @IrattarbaVetelDat
   IF @UpdatedColumns.exist('/root/FelhCsoport_Id_IrattariAtvevo')=1
         SET @Act_FelhCsoport_Id_IrattariAtvevo = @FelhCsoport_Id_IrattariAtvevo
   IF @UpdatedColumns.exist('/root/SelejtezesDat')=1
         SET @Act_SelejtezesDat = @SelejtezesDat
   IF @UpdatedColumns.exist('/root/FelhCsoport_Id_Selejtezo')=1
         SET @Act_FelhCsoport_Id_Selejtezo = @FelhCsoport_Id_Selejtezo
   IF @UpdatedColumns.exist('/root/LeveltariAtvevoNeve')=1
         SET @Act_LeveltariAtvevoNeve = @LeveltariAtvevoNeve
   IF @UpdatedColumns.exist('/root/FelhCsoport_Id_Felulvizsgalo')=1
         SET @Act_FelhCsoport_Id_Felulvizsgalo = @FelhCsoport_Id_Felulvizsgalo
   IF @UpdatedColumns.exist('/root/FelulvizsgalatDat')=1
         SET @Act_FelulvizsgalatDat = @FelulvizsgalatDat
   IF @UpdatedColumns.exist('/root/IktatoszamKieg')=1
         SET @Act_IktatoszamKieg = @IktatoszamKieg
   IF @UpdatedColumns.exist('/root/Targy')=1
         SET @Act_Targy = @Targy
   IF @UpdatedColumns.exist('/root/UgyUgyirat_Id_Szulo')=1
         SET @Act_UgyUgyirat_Id_Szulo = @UgyUgyirat_Id_Szulo
   IF @UpdatedColumns.exist('/root/UgyUgyirat_Id_Kulso')=1
         SET @Act_UgyUgyirat_Id_Kulso = @UgyUgyirat_Id_Kulso
   IF @UpdatedColumns.exist('/root/UgyTipus')=1
         SET @Act_UgyTipus = @UgyTipus
   IF @UpdatedColumns.exist('/root/IrattariHely')=1
         SET @Act_IrattariHely = @IrattariHely
   IF @UpdatedColumns.exist('/root/SztornirozasDat')=1
         SET @Act_SztornirozasDat = @SztornirozasDat
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1
         SET @Act_Csoport_Id_Felelos = @Csoport_Id_Felelos
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Orzo')=1
         SET @Act_FelhasznaloCsoport_Id_Orzo = @FelhasznaloCsoport_Id_Orzo
   IF @UpdatedColumns.exist('/root/Csoport_Id_Cimzett')=1
         SET @Act_Csoport_Id_Cimzett = @Csoport_Id_Cimzett
   IF @UpdatedColumns.exist('/root/Jelleg')=1
         SET @Act_Jelleg = @Jelleg
   IF @UpdatedColumns.exist('/root/IraIrattariTetel_Id')=1
         SET @Act_IraIrattariTetel_Id = @IraIrattariTetel_Id
   IF @UpdatedColumns.exist('/root/IraIktatokonyv_Id')=1
         SET @Act_IraIktatokonyv_Id = @IraIktatokonyv_Id
   IF @UpdatedColumns.exist('/root/SkontroOka')=1
         SET @Act_SkontroOka = @SkontroOka
   IF @UpdatedColumns.exist('/root/SkontroVege')=1
         SET @Act_SkontroVege = @SkontroVege
   IF @UpdatedColumns.exist('/root/Surgosseg')=1
         SET @Act_Surgosseg = @Surgosseg
   IF @UpdatedColumns.exist('/root/SkontrobanOsszesen')=1
         SET @Act_SkontrobanOsszesen = @SkontrobanOsszesen
   IF @UpdatedColumns.exist('/root/MegorzesiIdoVege')=1
         SET @Act_MegorzesiIdoVege = @MegorzesiIdoVege
   IF @UpdatedColumns.exist('/root/Partner_Id_Ugyindito')=1
         SET @Act_Partner_Id_Ugyindito = @Partner_Id_Ugyindito
   IF @UpdatedColumns.exist('/root/NevSTR_Ugyindito')=1
         SET @Act_NevSTR_Ugyindito = @NevSTR_Ugyindito
   IF @UpdatedColumns.exist('/root/ElintezesDat')=1
         SET @Act_ElintezesDat = @ElintezesDat
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Ugyintez')=1
         SET @Act_FelhasznaloCsoport_Id_Ugyintez = @FelhasznaloCsoport_Id_Ugyintez
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos_Elozo')=1
         SET @Act_Csoport_Id_Felelos_Elozo = @Csoport_Id_Felelos_Elozo
   IF @UpdatedColumns.exist('/root/KolcsonKikerDat')=1
         SET @Act_KolcsonKikerDat = @KolcsonKikerDat
   IF @UpdatedColumns.exist('/root/KolcsonKiadDat')=1
         SET @Act_KolcsonKiadDat = @KolcsonKiadDat
   IF @UpdatedColumns.exist('/root/Kolcsonhatarido')=1
         SET @Act_Kolcsonhatarido = @Kolcsonhatarido
   IF @UpdatedColumns.exist('/root/BARCODE')=1
         SET @Act_BARCODE = @BARCODE
   IF @UpdatedColumns.exist('/root/IratMetadefinicio_Id')=1
         SET @Act_IratMetadefinicio_Id = @IratMetadefinicio_Id
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @Act_Allapot = @Allapot
   IF @UpdatedColumns.exist('/root/TovabbitasAlattAllapot')=1
         SET @Act_TovabbitasAlattAllapot = @TovabbitasAlattAllapot
   IF @UpdatedColumns.exist('/root/Megjegyzes')=1
         SET @Act_Megjegyzes = @Megjegyzes
   IF @UpdatedColumns.exist('/root/Azonosito')=1
         SET @Act_Azonosito = @Azonosito
   IF @UpdatedColumns.exist('/root/Fizikai_Kezbesitesi_Allapot')=1
         SET @Act_Fizikai_Kezbesitesi_Allapot = @Fizikai_Kezbesitesi_Allapot
   IF @UpdatedColumns.exist('/root/Kovetkezo_Orzo_Id')=1
         SET @Act_Kovetkezo_Orzo_Id = @Kovetkezo_Orzo_Id
   IF @UpdatedColumns.exist('/root/Csoport_Id_Ugyfelelos')=1
         SET @Act_Csoport_Id_Ugyfelelos = @Csoport_Id_Ugyfelelos
   IF @UpdatedColumns.exist('/root/Elektronikus_Kezbesitesi_Allap')=1
         SET @Act_Elektronikus_Kezbesitesi_Allap = @Elektronikus_Kezbesitesi_Allap
   IF @UpdatedColumns.exist('/root/Kovetkezo_Felelos_Id')=1
         SET @Act_Kovetkezo_Felelos_Id = @Kovetkezo_Felelos_Id
   IF @UpdatedColumns.exist('/root/UtolsoAlszam')=1
         SET @Act_UtolsoAlszam = @UtolsoAlszam
   IF @UpdatedColumns.exist('/root/UtolsoSorszam')=1
         SET @Act_UtolsoSorszam = @UtolsoSorszam
   IF @UpdatedColumns.exist('/root/IratSzam')=1
         SET @Act_IratSzam = @IratSzam
   IF @UpdatedColumns.exist('/root/ElintezesMod')=1
         SET @Act_ElintezesMod = @ElintezesMod
   IF @UpdatedColumns.exist('/root/RegirendszerIktatoszam')=1
         SET @Act_RegirendszerIktatoszam = @RegirendszerIktatoszam
   IF @UpdatedColumns.exist('/root/GeneraltTargy')=1
         SET @Act_GeneraltTargy = @GeneraltTargy
   IF @UpdatedColumns.exist('/root/Cim_Id_Ugyindito ')=1
         SET @Act_Cim_Id_Ugyindito  = @Cim_Id_Ugyindito 
   IF @UpdatedColumns.exist('/root/CimSTR_Ugyindito ')=1
         SET @Act_CimSTR_Ugyindito  = @CimSTR_Ugyindito 
   IF @UpdatedColumns.exist('/root/LezarasOka')=1
         SET @Act_LezarasOka = @LezarasOka
   IF @UpdatedColumns.exist('/root/FelfuggesztesOka')=1
         SET @Act_FelfuggesztesOka = @FelfuggesztesOka
   IF @UpdatedColumns.exist('/root/IrattarId')=1
         SET @Act_IrattarId = @IrattarId
   IF @UpdatedColumns.exist('/root/SkontroOka_Kod')=1
         SET @Act_SkontroOka_Kod = @SkontroOka_Kod
   IF @UpdatedColumns.exist('/root/UjOrzesiIdo')=1
         SET @Act_UjOrzesiIdo = @UjOrzesiIdo
   IF @UpdatedColumns.exist('/root/UgyintezesKezdete')=1
         SET @Act_UgyintezesKezdete = @UgyintezesKezdete
   IF @UpdatedColumns.exist('/root/FelfuggesztettNapokSzama')=1
         SET @Act_FelfuggesztettNapokSzama = @FelfuggesztettNapokSzama
   IF @UpdatedColumns.exist('/root/IntezesiIdo')=1
         SET @Act_IntezesiIdo = @IntezesiIdo
   IF @UpdatedColumns.exist('/root/IntezesiIdoegyseg')=1
         SET @Act_IntezesiIdoegyseg = @IntezesiIdoegyseg
   IF @UpdatedColumns.exist('/root/Ugy_Fajtaja')=1
         SET @Act_Ugy_Fajtaja = @Ugy_Fajtaja
   IF @UpdatedColumns.exist('/root/SzignaloId')=1
         SET @Act_SzignaloId = @SzignaloId
   IF @UpdatedColumns.exist('/root/SzignalasIdeje')=1
         SET @Act_SzignalasIdeje = @SzignalasIdeje
   IF @UpdatedColumns.exist('/root/ElteltIdo')=1
         SET @Act_ElteltIdo = @ElteltIdo
   IF @UpdatedColumns.exist('/root/ElteltIdoIdoEgyseg')=1
         SET @Act_ElteltIdoIdoEgyseg = @ElteltIdoIdoEgyseg
   IF @UpdatedColumns.exist('/root/ElteltidoAllapot')=1
         SET @Act_ElteltidoAllapot = @ElteltidoAllapot
   IF @UpdatedColumns.exist('/root/SakkoraAllapot')=1
         SET @Act_SakkoraAllapot = @SakkoraAllapot
   IF @UpdatedColumns.exist('/root/HatralevoNapok')=1
         SET @Act_HatralevoNapok = @HatralevoNapok
   IF @UpdatedColumns.exist('/root/HatralevoMunkaNapok')=1
         SET @Act_HatralevoMunkaNapok = @HatralevoMunkaNapok
   IF @UpdatedColumns.exist('/root/ElozoAllapot')=1
         SET @Act_ElozoAllapot = @ElozoAllapot
   IF @UpdatedColumns.exist('/root/IrattarHelyfoglalas')=1
         SET @Act_IrattarHelyfoglalas = @IrattarHelyfoglalas		 
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @Act_Note = @Note
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @Act_Stat_id = @Stat_id
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @Act_ErvKezd = @ErvKezd
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @Act_ErvVege = @ErvVege
   IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
         SET @Act_Letrehozo_id = @Letrehozo_id
   IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
         SET @Act_LetrehozasIdo = @LetrehozasIdo
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @Act_Modosito_id = @Modosito_id
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @Act_ModositasIdo = @ModositasIdo
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @Act_Zarolo_id = @Zarolo_id
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @Act_ZarolasIdo = @ZarolasIdo
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @Act_Tranz_id = @Tranz_id
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @Act_UIAccessLog_id = @UIAccessLog_id   
   IF @Act_Ver is null
		SET @Act_Ver = 2	
   ELSE
		SET @Act_Ver = @Act_Ver+1
   

UPDATE EREC_UgyUgyiratok
SET
     Foszam = @Act_Foszam,
     Sorszam = @Act_Sorszam,
     Ugyazonosito  = @Act_Ugyazonosito ,
     Alkalmazas_Id = @Act_Alkalmazas_Id,
     UgyintezesModja = @Act_UgyintezesModja,
     Hatarido = @Act_Hatarido,
     SkontrobaDat = @Act_SkontrobaDat,
     LezarasDat = @Act_LezarasDat,
     IrattarbaKuldDatuma  = @Act_IrattarbaKuldDatuma ,
     IrattarbaVetelDat = @Act_IrattarbaVetelDat,
     FelhCsoport_Id_IrattariAtvevo = @Act_FelhCsoport_Id_IrattariAtvevo,
     SelejtezesDat = @Act_SelejtezesDat,
     FelhCsoport_Id_Selejtezo = @Act_FelhCsoport_Id_Selejtezo,
     LeveltariAtvevoNeve = @Act_LeveltariAtvevoNeve,
     FelhCsoport_Id_Felulvizsgalo = @Act_FelhCsoport_Id_Felulvizsgalo,
     FelulvizsgalatDat = @Act_FelulvizsgalatDat,
     IktatoszamKieg = @Act_IktatoszamKieg,
     Targy = @Act_Targy,
     UgyUgyirat_Id_Szulo = @Act_UgyUgyirat_Id_Szulo,
     UgyUgyirat_Id_Kulso = @Act_UgyUgyirat_Id_Kulso,
     UgyTipus = @Act_UgyTipus,
     IrattariHely = @Act_IrattariHely,
     SztornirozasDat = @Act_SztornirozasDat,
     Csoport_Id_Felelos = @Act_Csoport_Id_Felelos,
     FelhasznaloCsoport_Id_Orzo = @Act_FelhasznaloCsoport_Id_Orzo,
     Csoport_Id_Cimzett = @Act_Csoport_Id_Cimzett,
     Jelleg = @Act_Jelleg,
     IraIrattariTetel_Id = @Act_IraIrattariTetel_Id,
     IraIktatokonyv_Id = @Act_IraIktatokonyv_Id,
     SkontroOka = @Act_SkontroOka,
     SkontroVege = @Act_SkontroVege,
     Surgosseg = @Act_Surgosseg,
     SkontrobanOsszesen = @Act_SkontrobanOsszesen,
     MegorzesiIdoVege = @Act_MegorzesiIdoVege,
     Partner_Id_Ugyindito = @Act_Partner_Id_Ugyindito,
     NevSTR_Ugyindito = @Act_NevSTR_Ugyindito,
     ElintezesDat = @Act_ElintezesDat,
     FelhasznaloCsoport_Id_Ugyintez = @Act_FelhasznaloCsoport_Id_Ugyintez,
     Csoport_Id_Felelos_Elozo = @Act_Csoport_Id_Felelos_Elozo,
     KolcsonKikerDat = @Act_KolcsonKikerDat,
     KolcsonKiadDat = @Act_KolcsonKiadDat,
     Kolcsonhatarido = @Act_Kolcsonhatarido,
     BARCODE = @Act_BARCODE,
     IratMetadefinicio_Id = @Act_IratMetadefinicio_Id,
     Allapot = @Act_Allapot,
     TovabbitasAlattAllapot = @Act_TovabbitasAlattAllapot,
     Megjegyzes = @Act_Megjegyzes,
     Azonosito = @Act_Azonosito,
     Fizikai_Kezbesitesi_Allapot = @Act_Fizikai_Kezbesitesi_Allapot,
     Kovetkezo_Orzo_Id = @Act_Kovetkezo_Orzo_Id,
     Csoport_Id_Ugyfelelos = @Act_Csoport_Id_Ugyfelelos,
     Elektronikus_Kezbesitesi_Allap = @Act_Elektronikus_Kezbesitesi_Allap,
     Kovetkezo_Felelos_Id = @Act_Kovetkezo_Felelos_Id,
     UtolsoAlszam = @Act_UtolsoAlszam,
     UtolsoSorszam = @Act_UtolsoSorszam,
     IratSzam = @Act_IratSzam,
     ElintezesMod = @Act_ElintezesMod,
     RegirendszerIktatoszam = @Act_RegirendszerIktatoszam,
     GeneraltTargy = @Act_GeneraltTargy,
     Cim_Id_Ugyindito  = @Act_Cim_Id_Ugyindito ,
     CimSTR_Ugyindito  = @Act_CimSTR_Ugyindito ,
     LezarasOka = @Act_LezarasOka,
     FelfuggesztesOka = @Act_FelfuggesztesOka,
     IrattarId = @Act_IrattarId,
     SkontroOka_Kod = @Act_SkontroOka_Kod,
     UjOrzesiIdo = @Act_UjOrzesiIdo,
     UgyintezesKezdete = @Act_UgyintezesKezdete,
     FelfuggesztettNapokSzama = @Act_FelfuggesztettNapokSzama,
     IntezesiIdo = @Act_IntezesiIdo,
     IntezesiIdoegyseg = @Act_IntezesiIdoegyseg,
     Ugy_Fajtaja = @Act_Ugy_Fajtaja,
     SzignaloId = @Act_SzignaloId,
     SzignalasIdeje = @Act_SzignalasIdeje,
     ElteltIdo = @Act_ElteltIdo,
     ElteltIdoIdoEgyseg = @Act_ElteltIdoIdoEgyseg,
     ElteltidoAllapot = @Act_ElteltidoAllapot,
     SakkoraAllapot = @Act_SakkoraAllapot,
	 ElozoAllapot = @Act_ElozoAllapot,
	 IrattarHelyfoglalas=@Act_IrattarHelyfoglalas,
     Ver = @Act_Ver,
     Note = @Act_Note,
     Stat_id = @Act_Stat_id,
     ErvKezd = @Act_ErvKezd,
     ErvVege = @Act_ErvVege,
     Letrehozo_id = @Act_Letrehozo_id,
     LetrehozasIdo = @Act_LetrehozasIdo,
     Modosito_id = @Act_Modosito_id,
     ModositasIdo = @Act_ModositasIdo,
     Zarolo_id = @Act_Zarolo_id,
     ZarolasIdo = @Act_ZarolasIdo,
     Tranz_id = @Act_Tranz_id,
     UIAccessLog_id = @Act_UIAccessLog_id
where
  Id = @Id
  and (Ver = @Ver or Ver is null)

if @@rowcount != 1
begin
	RAISERROR('[50401]',16,1)
end
else
begin   /* History Log */
   exec sp_LogRecordToHistory 'EREC_UgyUgyiratok',@Id
					,'EREC_UgyUgyiratokHistory',1,@ExecutorUserId,@ExecutionTime   
end
-- BUG_11085
DECLARE @paramTUK_SZIGNALAS_ENABLED nvarchar(400)
SET @paramTUK_SZIGNALAS_ENABLED = (
	SELECT Ertek FROM KRT_Parameterek
	WHERE Nev ='TUK_SZIGNALAS_ENABLED'
	AND Org=@Org
)

   -- Elsődleges ügyiratban lévő iratpéldányok update UPDATE
   UPDATE EREC_PldIratPeldanyok
      SET EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo,
         EREC_PldIratPeldanyok.Csoport_Id_Felelos = EREC_UgyUgyiratok.Csoport_Id_Felelos,
         EREC_PldIratPeldanyok.Allapot =
            CASE
               WHEN EREC_UgyUgyiratok.Allapot ='50'   THEN '50'
			    WHEN (EREC_UgyUgyiratok.Allapot ='03') AND (@paramTUK_SZIGNALAS_ENABLED <> '1') THEN '50'
               WHEN (EREC_PldIratPeldanyok.Allapot = '50'
                   AND EREC_PldIratPeldanyok.TovabbitasAlattAllapot IS NOT NULL
                   AND EREC_PldIratPeldanyok.TovabbitasAlattAllapot != '')
                   THEN EREC_PldIratPeldanyok.TovabbitasAlattAllapot
               ELSE EREC_PldIratPeldanyok.Allapot
            END,
         EREC_PldIratPeldanyok.TovabbitasAlattAllapot = 
            CASE
               WHEN EREC_UgyUgyiratok.Allapot ='50' AND EREC_PldIratPeldanyok.Allapot != '50' THEN EREC_PldIratPeldanyok.Allapot
			   WHEN EREC_UgyUgyiratok.Allapot ='03' AND EREC_PldIratPeldanyok.Allapot != '50' AND (@paramTUK_SZIGNALAS_ENABLED <> '1') THEN EREC_PldIratPeldanyok.Allapot
               -- pl. VisszakĂĽldĂ©s:
               WHEN EREC_UgyUgyiratok.Allapot = '50' AND EREC_PldIratPeldanyok.Allapot = '50' THEN EREC_PldIratPeldanyok.TovabbitasAlattAllapot
			   WHEN EREC_UgyUgyiratok.Allapot = '03' AND EREC_PldIratPeldanyok.Allapot = '50' AND (@paramTUK_SZIGNALAS_ENABLED <> '1')  THEN EREC_PldIratPeldanyok.TovabbitasAlattAllapot
               WHEN EREC_PldIratPeldanyok.Allapot = '50' THEN NULL
               ELSE EREC_PldIratPeldanyok.TovabbitasAlattAllapot
            END,
         EREC_PldIratPeldanyok.Ver = EREC_PldIratPeldanyok.Ver + 1,
         EREC_PldIratPeldanyok.Modosito_Id = @ExecutorUserId,
         EREC_PldIratPeldanyok.ModositasIdo = @ExecutionTime
      FROM EREC_IraIratok
         --INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
         INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id --EREC_UgyUgyiratdarabok.UgyUgyirat_Id
      WHERE EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
         AND EREC_PldIratPeldanyok.Id IN (SELECT Id FROM #pldIds)
      
      -- iratpéldány mozgásának jelzése
      exec sp_EREC_PldIratPeldanyok_Moved @ExecutorUserId = @ExecutorUserId, @ExecutionTime = @ExecutionTime
      
      DROP TABLE #pldIds;

--COMMIT TRANSACTION UpdateTransaction

END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION UpdateTransaction

   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH