﻿create procedure [dbo].[sp_EREC_UgyUgyiratokUpdateAllIraIratIrattarFields]
        @UgyiratIds			NVARCHAR(MAX),
		@IrattarbaKuldes	datetime = null,
		@IrattarbaVetel		datetime = null		
as
BEGIN
  
	if @IrattarbaKuldes is not NULL OR @IrattarbaVetel is not null
	begin
		update erec_irairatok 
		set erec_irairatok.IrattarbaKuldDatuma = ISNULL(@IrattarbaKuldes, erec_irairatok.IrattarbaKuldDatuma),
			erec_irairatok.IrattarbaVetelDat = ISNULL (@IrattarbaVetel, erec_irairatok.IrattarbaVetelDat)
		where [EREC_IraIratok].[Ugyirat_Id] IN (SELECT Value from dbo.fn_Split(@UgyiratIds,','))
	end

END