﻿CREATE procedure [dbo].[sp_EREC_UgyUgyiratokUpdate_Tomeges]
        @Ids                  IdTable READONLY,
        @Vers                 NVARCHAR(MAX) = NULL  ,
      @ExecutorUserId            uniqueidentifier  ,
      @ExecutionTime             datetime,
      @KellKezbesitesiTetel      char(1) = '1',
             @Foszam     int  = null,         
             @Sorszam     int  = null,
             @Ugyazonosito      Nvarchar(100)  = null,         
             @Alkalmazas_Id     uniqueidentifier  = null,         
             @UgyintezesModja     nvarchar(64)  = null,         
             @Hatarido     datetime  = null,         
             @SkontrobaDat     datetime  = null,         
             @LezarasDat     datetime  = null,         
             @IrattarbaKuldDatuma      datetime  = null,         
             @IrattarbaVetelDat     datetime  = null,         
             @FelhCsoport_Id_IrattariAtvevo     uniqueidentifier  = null,         
             @SelejtezesDat     datetime  = null,         
             @FelhCsoport_Id_Selejtezo     uniqueidentifier  = null,         
             @LeveltariAtvevoNeve     Nvarchar(100)  = null,         
             @FelhCsoport_Id_Felulvizsgalo     uniqueidentifier  = null,         
             @FelulvizsgalatDat     datetime  = null,         
             @IktatoszamKieg     Nvarchar(2)  = null,         
             @Targy     Nvarchar(4000)  = null,         
             @UgyUgyirat_Id_Szulo     uniqueidentifier  = null,
             @UgyUgyirat_Id_Kulso UNIQUEIDENTIFIER = NULL,       
             @UgyTipus     nvarchar(64)  = null,         
             @IrattariHely     Nvarchar(100)  = null,         
             @SztornirozasDat     datetime  = null,         
             @Csoport_Id_Felelos     uniqueidentifier  = null,         
             @FelhasznaloCsoport_Id_Orzo     uniqueidentifier  = null,         
             @Csoport_Id_Cimzett     uniqueidentifier  = null,         
             @Jelleg     nvarchar(64)  = null,         
             @IraIrattariTetel_Id     uniqueidentifier  = null,         
             @IraIktatokonyv_Id     uniqueidentifier  = null,         
             @SkontroOka     Nvarchar(400)  = null,  
			 -- CR3407
			 @SkontroOka_Kod     Nvarchar(64)  = null,         
             @SkontroVege     datetime  = null,         
             @Surgosseg     nvarchar(64)  = null,         
             @SkontrobanOsszesen     int  = null,         
             @MegorzesiIdoVege     datetime  = null,         
             @Partner_Id_Ugyindito     uniqueidentifier  = null,         
             @NevSTR_Ugyindito     Nvarchar(400)  = null,         
             @ElintezesDat     datetime  = null,         
             @FelhasznaloCsoport_Id_Ugyintez     uniqueidentifier  = null,         
             @Csoport_Id_Felelos_Elozo     uniqueidentifier  = null,         
             @KolcsonKikerDat     datetime  = null,         
             @KolcsonKiadDat     datetime  = null,         
             @Kolcsonhatarido     datetime  = null,         
             @BARCODE     Nvarchar(100)  = null,         
             @IratMetadefinicio_Id     uniqueidentifier  = null,         
             @Allapot     nvarchar(64)  = null,         
             @TovabbitasAlattAllapot     nvarchar(64)  = null,         
             @Megjegyzes     Nvarchar(400)  = null,         
             @Azonosito     Nvarchar(100)  = null,         
             @Fizikai_Kezbesitesi_Allapot     nvarchar(64)  = null,         
             @Kovetkezo_Orzo_Id     uniqueidentifier  = null,         
             @Csoport_Id_Ugyfelelos     uniqueidentifier  = null,         
             @Elektronikus_Kezbesitesi_Allap     nvarchar(64)  = null,         
             @Kovetkezo_Felelos_Id     uniqueidentifier  = null,         
             @UtolsoAlszam     int  = null,
             @UtolsoSorszam     int  = null,
             @IratSzam INT = NULL,         
             @ElintezesMod     nvarchar(64)  = null,         
             @RegirendszerIktatoszam     Nvarchar(100)  = null,         
             @GeneraltTargy     Nvarchar(400)  = null,   
			 @Cim_Id_Ugyindito      uniqueidentifier  = null,         
             @CimSTR_Ugyindito      Nvarchar(400)  = null,         
             @LezarasOka     nvarchar(64)  = null,         
             @FelfuggesztesOka     Nvarchar(400)  = null,              
             @UgyintezesKezdete     datetime  = null,         
             @FelfuggesztettNapokSzama     int  = null,         
             @IntezesiIdo     nvarchar(64)  = null,         
             @IntezesiIdoegyseg     nvarchar(64)  = null,         
             @Ugy_Fajtaja     nvarchar(64)  = null,         
             @SzignaloId     uniqueidentifier  = null,         
             @SzignalasIdeje     datetime  = null,         
             @ElteltIdo     nvarchar(64)  = null,         
             @ElteltIdoIdoEgyseg     nvarchar(64)  = null,         
             @ElteltidoAllapot     int  = null,         
             @SakkoraAllapot     nvarchar(64)  = null,               
             @HatralevoNapok     int  = null,         
             @HatralevoMunkaNapok     int  = null,
			 @ElozoAllapot     nvarchar(64)  = null,         
             @Ver     int  = null,         
             @Note     Nvarchar(4000)  = null,         
             @Stat_id     uniqueidentifier  = null,         
             @ErvKezd     datetime  = null,         
             @ErvVege     datetime  = null,         
             @Letrehozo_id     uniqueidentifier  = null,         
             @LetrehozasIdo     datetime  = null,         
             @Modosito_id     uniqueidentifier  = null,         
             @ModositasIdo     datetime  = null,         
             @Zarolo_id     uniqueidentifier  = null,         
             @ZarolasIdo     datetime  = null,         
             @Tranz_id     uniqueidentifier  = null,         
             @UIAccessLog_id     uniqueidentifier  = null,
             @UjOrzesiIdo     int  = null,
			 @IrattarId     uniqueidentifier  = null,
       @UpdatedColumns              xml

as

BEGIN TRY
-- BUG_11085
DECLARE @Org uniqueidentifier
SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
if (@Org is null)
begin
	RAISERROR('[50202]',16,1)
end

   declare @tempTable table(id uniqueidentifier, ver int);
   
   --Uniqueidentifier convert miatt hozzuk létre, ne kelljen minden helyen convert(uniqueidentifier,id)-t használni:
   INSERT INTO @tempTable 
   SELECT  convert(uniqueidentifier,id), ver FROM @Ids

   --Bepakoljuk egy # táblába, mert a sqlexecute-nál könnyebb ezzel belevinni a where-be:
   CREATE TABLE #Idstemp  ( id nvarchar(50), ver int);
   INSERT INTO #Idstemp
   SELECT  id, ver FROM @Ids
   
   declare @rowNumberTotal int;
   declare @rowNumberChecked int;

   select @rowNumberTotal = count(id) from @tempTable;
   
   
   /******************************************
   * Ellenőrzések
   ******************************************/
   -- verzió ellenőrzés
   select @rowNumberChecked = count(EREC_UgyUgyiratok.Id)
      from EREC_UgyUgyiratok
         inner JOIN @tempTable t on t.id  = EREC_UgyUgyiratok.id and t.ver = EREC_UgyUgyiratok.ver
         
   if (@rowNumberTotal != @rowNumberChecked)
   BEGIN
      SELECT Id FROM @tempTable
      EXCEPT
      SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
         inner join @tempTable t on t.id = EREC_UgyUgyiratok.id and t.ver = EREC_UgyUgyiratok.ver
      RAISERROR('[50402]',16,1);
   END
   
   -- zárolás ellenőrzés
   select @rowNumberChecked = count(EREC_UgyUgyiratok.Id)
      from EREC_UgyUgyiratok
         inner JOIN @tempTable t on t.id = EREC_UgyUgyiratok.id and ISNULL(EREC_UgyUgyiratok.Zarolo_id,@ExecutorUserId) = @ExecutorUserId
         
   if (@rowNumberTotal != @rowNumberChecked)
   BEGIN
      SELECT Id FROM @tempTable
      EXCEPT
      SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
         inner join @tempTable t on t.id = EREC_UgyUgyiratok.id and ISNULL(EREC_UgyUgyiratok.Zarolo_id,@ExecutorUserId) = @ExecutorUserId
      RAISERROR('[50499]',16,1);
   END
   
   -- érvényesség vége figyelése
   select @rowNumberChecked = count(EREC_UgyUgyiratok.Id)
      from EREC_UgyUgyiratok
         inner JOIN @tempTable t on t.id = EREC_UgyUgyiratok.id and EREC_UgyUgyiratok.ErvVege > @ExecutionTime
         
   if (@rowNumberTotal != @rowNumberChecked)
   BEGIN
      SELECT Id FROM @tempTable
      EXCEPT
      SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
         inner join @tempTable t on t.id = EREC_UgyUgyiratok.id and EREC_UgyUgyiratok.ErvVege > @ExecutionTime
      RAISERROR('[50403]',16,1);
   END
   
   -- mozgatás esetén fizikai mappa ellenőrés
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1 AND EXISTS
      (
         SELECT 1
            FROM EREC_UgyUgyiratok
               INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = EREC_UgyUgyiratok.Id
               INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
            WHERE EREC_UgyUgyiratok.Id IN (SELECT id FROM @tempTable)
               AND EREC_UgyUgyiratok.Csoport_Id_Felelos != @Csoport_Id_Felelos
               AND KRT_Mappak.Tipus = '01'
               AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
               AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
      )
   BEGIN
      SELECT EREC_UgyUgyiratok.Id
         FROM EREC_UgyUgyiratok
            INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = EREC_UgyUgyiratok.Id
            INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
         WHERE EREC_UgyUgyiratok.Id IN (SELECT id FROM @tempTable)
            AND EREC_UgyUgyiratok.Csoport_Id_Felelos != @Csoport_Id_Felelos
            AND KRT_Mappak.Tipus = '01'
            AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
            AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
      RAISERROR('[64006]',16,1);
   END
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Orzo')=1 AND EXISTS
      (
         SELECT 1
            FROM EREC_UgyUgyiratok
               INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = EREC_UgyUgyiratok.Id
               INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
            WHERE EREC_UgyUgyiratok.Id IN (SELECT id FROM @tempTable)
               AND EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != FelhasznaloCsoport_Id_Orzo
               AND KRT_Mappak.Tipus = '01'
               AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
               AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
      )
   BEGIN
      SELECT EREC_UgyUgyiratok.Id
         FROM EREC_UgyUgyiratok
            INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = EREC_UgyUgyiratok.Id
            INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
         WHERE EREC_UgyUgyiratok.Id IN (SELECT id FROM @tempTable)
            AND EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo != FelhasznaloCsoport_Id_Orzo
            AND KRT_Mappak.Tipus = '01'
            AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
            AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
      RAISERROR('[64006]',16,1);
   END
   
   
   /* Kézbesítési tétel létrehozása ha szükséges */
   IF  @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1 and @KellKezbesitesiTetel = '1'
   BEGIN
      UPDATE EREC_IraKezbesitesiTetelek SET ErvVege = GETDATE() WHERE Obj_Id IN
         (SELECT Id FROM EREC_UgyUgyiratok WHERE Id IN (SELECT id FROM @tempTable) AND Csoport_Id_Felelos != @Csoport_Id_Felelos)
      INSERT INTO EREC_IraKezbesitesiTetelek(Obj_Id, Obj_type, Felhasznalo_Id_Atado_LOGIN, Felhasznalo_Id_Atado_USER, Csoport_Id_Cel, Allapot)
         SELECT Id, 'EREC_UgyUgyiratok', @ExecutorUserId, @ExecutorUserId, @Csoport_Id_Felelos, '1' FROM EREC_UgyUgyiratok WHERE Id IN (select id FROM @tempTable) AND Csoport_Id_Felelos != @Csoport_Id_Felelos
   END

   /* Elsődleges ügyiratban lévő iratpéldányok kigyűjtése */
   SELECT EREC_PldIratPeldanyok.Id INTO #pldIds
      FROM EREC_PldIratPeldanyok
         INNER JOIN EREC_IraIratok ON EREC_PldIratPeldanyok.IraIrat_Id = EREC_IraIratok.Id
         --INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
         INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id --EREC_UgyUgyiratdarabok.UgyUgyirat_Id
            AND EREC_UgyUgyiratok.Id IN (SELECT id FROM @tempTable)
      WHERE EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo
         AND EREC_PldIratPeldanyok.Csoport_Id_Felelos = EREC_UgyUgyiratok.Csoport_Id_Felelos



   /******************************************
   * UPDATE string összeállítás
   ******************************************/
   DECLARE @sqlcmd   NVARCHAR(MAX);
   SET @sqlcmd = N'UPDATE EREC_UgyUgyiratok SET';
   
      IF @UpdatedColumns.exist('/root/Foszam')=1
         SET @sqlcmd = @sqlcmd + N' Foszam = @Foszam,'
   IF @UpdatedColumns.exist('/root/Sorszam')=1
         SET @sqlcmd = @sqlcmd + N' Sorszam = @Sorszam,'
   IF @UpdatedColumns.exist('/root/Ugyazonosito ')=1
         SET @sqlcmd = @sqlcmd + N' Ugyazonosito  = @Ugyazonosito ,'
   IF @UpdatedColumns.exist('/root/Alkalmazas_Id')=1
         SET @sqlcmd = @sqlcmd + N' Alkalmazas_Id = @Alkalmazas_Id,'
   IF @UpdatedColumns.exist('/root/UgyintezesModja')=1
         SET @sqlcmd = @sqlcmd + N' UgyintezesModja = @UgyintezesModja,'
   IF @UpdatedColumns.exist('/root/Hatarido')=1
         SET @sqlcmd = @sqlcmd + N' Hatarido = @Hatarido,'
   IF @UpdatedColumns.exist('/root/SkontrobaDat')=1
         SET @sqlcmd = @sqlcmd + N' SkontrobaDat = @SkontrobaDat,'
   IF @UpdatedColumns.exist('/root/LezarasDat')=1
         SET @sqlcmd = @sqlcmd + N' LezarasDat = @LezarasDat,'
   IF @UpdatedColumns.exist('/root/IrattarbaKuldDatuma ')=1
         SET @sqlcmd = @sqlcmd + N' IrattarbaKuldDatuma  = @IrattarbaKuldDatuma ,'
   IF @UpdatedColumns.exist('/root/IrattarbaVetelDat')=1
         SET @sqlcmd = @sqlcmd + N' IrattarbaVetelDat = @IrattarbaVetelDat,'
   IF @UpdatedColumns.exist('/root/FelhCsoport_Id_IrattariAtvevo')=1
         SET @sqlcmd = @sqlcmd + N' FelhCsoport_Id_IrattariAtvevo = @FelhCsoport_Id_IrattariAtvevo,'
   IF @UpdatedColumns.exist('/root/SelejtezesDat')=1
         SET @sqlcmd = @sqlcmd + N' SelejtezesDat = @SelejtezesDat,'
   IF @UpdatedColumns.exist('/root/FelhCsoport_Id_Selejtezo')=1
         SET @sqlcmd = @sqlcmd + N' FelhCsoport_Id_Selejtezo = @FelhCsoport_Id_Selejtezo,'
   IF @UpdatedColumns.exist('/root/LeveltariAtvevoNeve')=1
         SET @sqlcmd = @sqlcmd + N' LeveltariAtvevoNeve = @LeveltariAtvevoNeve,'
   IF @UpdatedColumns.exist('/root/FelhCsoport_Id_Felulvizsgalo')=1
         SET @sqlcmd = @sqlcmd + N' FelhCsoport_Id_Felulvizsgalo = @FelhCsoport_Id_Felulvizsgalo,'
   IF @UpdatedColumns.exist('/root/FelulvizsgalatDat')=1
         SET @sqlcmd = @sqlcmd + N' FelulvizsgalatDat = @FelulvizsgalatDat,'
   IF @UpdatedColumns.exist('/root/IktatoszamKieg')=1
         SET @sqlcmd = @sqlcmd + N' IktatoszamKieg = @IktatoszamKieg,'
   IF @UpdatedColumns.exist('/root/Targy')=1
         SET @sqlcmd = @sqlcmd + N' Targy = @Targy,'
   IF @UpdatedColumns.exist('/root/UgyUgyirat_Id_Szulo')=1
         SET @sqlcmd = @sqlcmd + N' UgyUgyirat_Id_Szulo = @UgyUgyirat_Id_Szulo,'
   IF @UpdatedColumns.exist('/root/UgyUgyirat_Id_Kulso')=1
         SET @sqlcmd = @sqlcmd + N' UgyUgyirat_Id_Kulso = @UgyUgyirat_Id_Kulso,'      
   IF @UpdatedColumns.exist('/root/UgyTipus')=1
         SET @sqlcmd = @sqlcmd + N' UgyTipus = @UgyTipus,'
   IF @UpdatedColumns.exist('/root/IrattariHely')=1
         SET @sqlcmd = @sqlcmd + N' IrattariHely = @IrattariHely,'
   IF @UpdatedColumns.exist('/root/SztornirozasDat')=1
         SET @sqlcmd = @sqlcmd + N' SztornirozasDat = @SztornirozasDat,'
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos')=1
         SET @sqlcmd = @sqlcmd + N' Csoport_Id_Felelos = @Csoport_Id_Felelos,'
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Orzo')=1
         SET @sqlcmd = @sqlcmd + N' FelhasznaloCsoport_Id_Orzo = @FelhasznaloCsoport_Id_Orzo,'
   IF @UpdatedColumns.exist('/root/Csoport_Id_Cimzett')=1
         SET @sqlcmd = @sqlcmd + N' Csoport_Id_Cimzett = @Csoport_Id_Cimzett,'
   IF @UpdatedColumns.exist('/root/Jelleg')=1
         SET @sqlcmd = @sqlcmd + N' Jelleg = @Jelleg,'
   IF @UpdatedColumns.exist('/root/IraIrattariTetel_Id')=1
         SET @sqlcmd = @sqlcmd + N' IraIrattariTetel_Id = @IraIrattariTetel_Id,'
   IF @UpdatedColumns.exist('/root/IraIktatokonyv_Id')=1
         SET @sqlcmd = @sqlcmd + N' IraIktatokonyv_Id = @IraIktatokonyv_Id,'
   IF @UpdatedColumns.exist('/root/SkontroOka')=1
         SET @sqlcmd = @sqlcmd + N' SkontroOka = @SkontroOka,'
   -- CR3407
     IF @UpdatedColumns.exist('/root/SkontroOka_Kod')=1
         SET @sqlcmd = @sqlcmd + N' SkontroOka_Kod = @SkontroOka_Kod,'
   IF @UpdatedColumns.exist('/root/SkontroVege')=1
         SET @sqlcmd = @sqlcmd + N' SkontroVege = @SkontroVege,'
   IF @UpdatedColumns.exist('/root/Surgosseg')=1
         SET @sqlcmd = @sqlcmd + N' Surgosseg = @Surgosseg,'
   IF @UpdatedColumns.exist('/root/SkontrobanOsszesen')=1
         SET @sqlcmd = @sqlcmd + N' SkontrobanOsszesen = @SkontrobanOsszesen,'
   IF @UpdatedColumns.exist('/root/MegorzesiIdoVege')=1
         SET @sqlcmd = @sqlcmd + N' MegorzesiIdoVege = @MegorzesiIdoVege,'
   IF @UpdatedColumns.exist('/root/Partner_Id_Ugyindito')=1
         SET @sqlcmd = @sqlcmd + N' Partner_Id_Ugyindito = @Partner_Id_Ugyindito,'
   IF @UpdatedColumns.exist('/root/NevSTR_Ugyindito')=1
         SET @sqlcmd = @sqlcmd + N' NevSTR_Ugyindito = @NevSTR_Ugyindito,'
   IF @UpdatedColumns.exist('/root/ElintezesDat')=1
         SET @sqlcmd = @sqlcmd + N' ElintezesDat = @ElintezesDat,'
   IF @UpdatedColumns.exist('/root/FelhasznaloCsoport_Id_Ugyintez')=1
         SET @sqlcmd = @sqlcmd + N' FelhasznaloCsoport_Id_Ugyintez = @FelhasznaloCsoport_Id_Ugyintez,'
   IF @UpdatedColumns.exist('/root/Csoport_Id_Felelos_Elozo')=1
         SET @sqlcmd = @sqlcmd + N' Csoport_Id_Felelos_Elozo = @Csoport_Id_Felelos_Elozo,'
   IF @UpdatedColumns.exist('/root/KolcsonKikerDat')=1
         SET @sqlcmd = @sqlcmd + N' KolcsonKikerDat = @KolcsonKikerDat,'
   IF @UpdatedColumns.exist('/root/KolcsonKiadDat')=1
         SET @sqlcmd = @sqlcmd + N' KolcsonKiadDat = @KolcsonKiadDat,'
   IF @UpdatedColumns.exist('/root/Kolcsonhatarido')=1
         SET @sqlcmd = @sqlcmd + N' Kolcsonhatarido = @Kolcsonhatarido,'
   IF @UpdatedColumns.exist('/root/BARCODE')=1
         SET @sqlcmd = @sqlcmd + N' BARCODE = @BARCODE,'
   IF @UpdatedColumns.exist('/root/IratMetadefinicio_Id')=1
         SET @sqlcmd = @sqlcmd + N' IratMetadefinicio_Id = @IratMetadefinicio_Id,'
   IF @UpdatedColumns.exist('/root/Allapot')=1
         SET @sqlcmd = @sqlcmd + N' Allapot = @Allapot,'
   IF @UpdatedColumns.exist('/root/TovabbitasAlattAllapot')=1
         SET @sqlcmd = @sqlcmd + N' TovabbitasAlattAllapot = @TovabbitasAlattAllapot,'
   IF @UpdatedColumns.exist('/root/Megjegyzes')=1
         SET @sqlcmd = @sqlcmd + N' Megjegyzes = @Megjegyzes,'
   IF @UpdatedColumns.exist('/root/Azonosito')=1
         SET @sqlcmd = @sqlcmd + N' Azonosito = @Azonosito,'
   IF @UpdatedColumns.exist('/root/Fizikai_Kezbesitesi_Allapot')=1
         SET @sqlcmd = @sqlcmd + N' Fizikai_Kezbesitesi_Allapot = @Fizikai_Kezbesitesi_Allapot,'
   IF @UpdatedColumns.exist('/root/Kovetkezo_Orzo_Id')=1
         SET @sqlcmd = @sqlcmd + N' Kovetkezo_Orzo_Id = @Kovetkezo_Orzo_Id,'
   IF @UpdatedColumns.exist('/root/Csoport_Id_Ugyfelelos')=1
         SET @sqlcmd = @sqlcmd + N' Csoport_Id_Ugyfelelos = @Csoport_Id_Ugyfelelos,'
   IF @UpdatedColumns.exist('/root/Elektronikus_Kezbesitesi_Allap')=1
         SET @sqlcmd = @sqlcmd + N' Elektronikus_Kezbesitesi_Allap = @Elektronikus_Kezbesitesi_Allap,'
   IF @UpdatedColumns.exist('/root/Kovetkezo_Felelos_Id')=1
         SET @sqlcmd = @sqlcmd + N' Kovetkezo_Felelos_Id = @Kovetkezo_Felelos_Id,'
   IF @UpdatedColumns.exist('/root/UtolsoAlszam')=1
         SET @sqlcmd = @sqlcmd + N' UtolsoAlszam = @UtolsoAlszam,'
   IF @UpdatedColumns.exist('/root/UtolsoSorszam')=1
       SET @sqlcmd = @sqlcmd + N' UtolsoSorszam = @UtolsoSorszam,'         
   IF @UpdatedColumns.exist('/root/IratSzam')=1
         SET @sqlcmd = @sqlcmd + N' IratSzam = @IratSzam,'
   IF @UpdatedColumns.exist('/root/ElintezesMod')=1
         SET @sqlcmd = @sqlcmd + N' ElintezesMod = @ElintezesMod,'
   IF @UpdatedColumns.exist('/root/RegirendszerIktatoszam')=1
         SET @sqlcmd = @sqlcmd + N' RegirendszerIktatoszam = @RegirendszerIktatoszam,'
   IF @UpdatedColumns.exist('/root/GeneraltTargy')=1
         SET @sqlcmd = @sqlcmd + N' GeneraltTargy = @GeneraltTargy,'
	IF @UpdatedColumns.exist('/root/Cim_Id_Ugyindito')=1
			 SET @sqlcmd = @sqlcmd + N' Cim_Id_Ugyindito = @Cim_Id_Ugyindito,'
	IF @UpdatedColumns.exist('/root/CimSTR_Ugyindito')=1
			 SET @sqlcmd = @sqlcmd + N' CimSTR_Ugyindito = @CimSTR_Ugyindito,'
	IF @UpdatedColumns.exist('/root/LezarasOka')=1
			 SET @sqlcmd = @sqlcmd + N' LezarasOka = @LezarasOka,'
	IF @UpdatedColumns.exist('/root/FelfuggesztesOka')=1
			 SET @sqlcmd = @sqlcmd + N' FelfuggesztesOka = @FelfuggesztesOka,'
	IF @UpdatedColumns.exist('/root/UgyintezesKezdete')=1
			 SET @sqlcmd = @sqlcmd + N' UgyintezesKezdete = @UgyintezesKezdete,'
	IF @UpdatedColumns.exist('/root/FelfuggesztettNapokSzama')=1
			 SET @sqlcmd = @sqlcmd + N' FelfuggesztettNapokSzama = @FelfuggesztettNapokSzama,'
	IF @UpdatedColumns.exist('/root/IntezesiIdo')=1
			 SET @sqlcmd = @sqlcmd + N' IntezesiIdo = @IntezesiIdo,'
	IF @UpdatedColumns.exist('/root/IntezesiIdoegyseg')=1
			 SET @sqlcmd = @sqlcmd + N' IntezesiIdoegyseg = @IntezesiIdoegyseg,'
	IF @UpdatedColumns.exist('/root/Ugy_Fajtaja')=1
			 SET @sqlcmd = @sqlcmd + N' Ugy_Fajtaja = @Ugy_Fajtaja,'
	IF @UpdatedColumns.exist('/root/SzignaloId')=1
			 SET @sqlcmd = @sqlcmd + N' SzignaloId = @SzignaloId,'
	IF @UpdatedColumns.exist('/root/SzignalasIdeje')=1
			 SET @sqlcmd = @sqlcmd + N' SzignalasIdeje = @SzignalasIdeje,'
	IF @UpdatedColumns.exist('/root/ElteltIdo')=1
			 SET @sqlcmd = @sqlcmd + N' ElteltIdo = @ElteltIdo,'
	IF @UpdatedColumns.exist('/root/ElteltIdoIdoEgyseg')=1
			 SET @sqlcmd = @sqlcmd + N' ElteltIdoIdoEgyseg = @ElteltIdoIdoEgyseg,'
	IF @UpdatedColumns.exist('/root/ElteltidoAllapot')=1
			 SET @sqlcmd = @sqlcmd + N' ElteltidoAllapot = @ElteltidoAllapot,'
	IF @UpdatedColumns.exist('/root/SakkoraAllapot')=1
			 SET @sqlcmd = @sqlcmd + N' SakkoraAllapot = @SakkoraAllapot,'
	--IF @UpdatedColumns.exist('/root/HatralevoNapok')=1
	--		 SET @sqlcmd = @sqlcmd + N' HatralevoNapok = @HatralevoNapok,'
	--IF @UpdatedColumns.exist('/root/HatralevoMunkaNapok')=1
	--		 SET @sqlcmd = @sqlcmd + N' HatralevoMunkaNapok = @HatralevoMunkaNapok,'
	IF @UpdatedColumns.exist('/root/ElozoAllapot')=1
			 SET @sqlcmd = @sqlcmd + N' ElozoAllapot = @ElozoAllapot,'
   IF @UpdatedColumns.exist('/root/Note')=1
         SET @sqlcmd = @sqlcmd + N' Note = @Note,'
   IF @UpdatedColumns.exist('/root/Stat_id')=1
         SET @sqlcmd = @sqlcmd + N' Stat_id = @Stat_id,'
   IF @UpdatedColumns.exist('/root/ErvKezd')=1
         SET @sqlcmd = @sqlcmd + N' ErvKezd = @ErvKezd,'
   IF @UpdatedColumns.exist('/root/ErvVege')=1
         SET @sqlcmd = @sqlcmd + N' ErvVege = @ErvVege,'
   --IF @UpdatedColumns.exist('/root/Letrehozo_id')=1
   --      SET @sqlcmd = @sqlcmd + N' Letrehozo_id = @Letrehozo_id,'
   --IF @UpdatedColumns.exist('/root/LetrehozasIdo')=1
   --      SET @sqlcmd = @sqlcmd + N' LetrehozasIdo = @LetrehozasIdo,'
   IF @UpdatedColumns.exist('/root/Modosito_id')=1
         SET @sqlcmd = @sqlcmd + N' Modosito_id = @Modosito_id,'
   IF @UpdatedColumns.exist('/root/ModositasIdo')=1
         SET @sqlcmd = @sqlcmd + N' ModositasIdo = @ModositasIdo,'
   IF @UpdatedColumns.exist('/root/Zarolo_id')=1
         SET @sqlcmd = @sqlcmd + N' Zarolo_id = @Zarolo_id,'
   IF @UpdatedColumns.exist('/root/ZarolasIdo')=1
         SET @sqlcmd = @sqlcmd + N' ZarolasIdo = @ZarolasIdo,'
   IF @UpdatedColumns.exist('/root/Tranz_id')=1
         SET @sqlcmd = @sqlcmd + N' Tranz_id = @Tranz_id,'
   IF @UpdatedColumns.exist('/root/UIAccessLog_id')=1
         SET @sqlcmd = @sqlcmd + N' UIAccessLog_id = @UIAccessLog_id,'   
   IF @UpdatedColumns.exist('/root/UjOrzesiIdo')=1
         SET @sqlcmd = @sqlcmd + N' UjOrzesiIdo = @UjOrzesiIdo,'   
   IF @UpdatedColumns.exist('/root/IrattarId')=1
         SET @sqlcmd = @sqlcmd + N' IrattarId = @IrattarId,'

   SET @sqlcmd = @sqlcmd + N' Ver = Ver + 1'   

   SET @sqlcmd = @sqlcmd + N'
   WHERE Id IN ( select id from #Idstemp )'
   print @sqlcmd 

   EXECUTE sp_executesql @sqlcmd, N'
            @Foszam     int,         
            @Sorszam     int,
             @Ugyazonosito      Nvarchar(100),         
             @Alkalmazas_Id     uniqueidentifier,         
             @UgyintezesModja     nvarchar(64),         
             @Hatarido     datetime,         
             @SkontrobaDat     datetime,         
             @LezarasDat     datetime,         
             @IrattarbaKuldDatuma      datetime,         
             @IrattarbaVetelDat     datetime,         
             @FelhCsoport_Id_IrattariAtvevo     uniqueidentifier,         
             @SelejtezesDat     datetime,         
             @FelhCsoport_Id_Selejtezo     uniqueidentifier,         
             @LeveltariAtvevoNeve     Nvarchar(100),         
             @FelhCsoport_Id_Felulvizsgalo     uniqueidentifier,         
             @FelulvizsgalatDat     datetime,         
             @IktatoszamKieg     Nvarchar(2),         
             @Targy     Nvarchar(4000),         
             @UgyUgyirat_Id_Szulo     uniqueidentifier,
             @UgyUgyirat_Id_Kulso uniqueidentifier,         
             @UgyTipus     nvarchar(64),         
             @IrattariHely     Nvarchar(100),         
             @SztornirozasDat     datetime,         
             @Csoport_Id_Felelos     uniqueidentifier,         
             @FelhasznaloCsoport_Id_Orzo     uniqueidentifier,         
             @Csoport_Id_Cimzett     uniqueidentifier,         
             @Jelleg     nvarchar(64),         
             @IraIrattariTetel_Id     uniqueidentifier,
             @IraIktatokonyv_Id     uniqueidentifier,         
             @SkontroOka     Nvarchar(400),    
			 -- CR3407
			 @SkontroOka_Kod     Nvarchar(64),       
             @SkontroVege     datetime,         
             @Surgosseg     nvarchar(64),         
             @SkontrobanOsszesen     int,         
             @MegorzesiIdoVege     datetime,         
             @Partner_Id_Ugyindito     uniqueidentifier,         
             @NevSTR_Ugyindito     Nvarchar(400),         
             @ElintezesDat     datetime,         
             @FelhasznaloCsoport_Id_Ugyintez     uniqueidentifier,         
             @Csoport_Id_Felelos_Elozo     uniqueidentifier,         
             @KolcsonKikerDat     datetime,         
             @KolcsonKiadDat     datetime,         
             @Kolcsonhatarido     datetime,         
             @BARCODE     Nvarchar(100),         
             @IratMetadefinicio_Id     uniqueidentifier,         
             @Allapot     nvarchar(64),         
             @TovabbitasAlattAllapot     nvarchar(64),         
             @Megjegyzes     Nvarchar(400),         
             @Azonosito     Nvarchar(100),         
             @Fizikai_Kezbesitesi_Allapot     nvarchar(64),         
             @Kovetkezo_Orzo_Id     uniqueidentifier,         
             @Csoport_Id_Ugyfelelos     uniqueidentifier,         
             @Elektronikus_Kezbesitesi_Allap     nvarchar(64),         
             @Kovetkezo_Felelos_Id     uniqueidentifier,         
             @UtolsoAlszam     int,
             @UtolsoSorszam int,
             @IratSzam int,        
             @ElintezesMod     nvarchar(64),         
             @RegirendszerIktatoszam     Nvarchar(100),         
             @GeneraltTargy     Nvarchar(400),        
			 @Cim_Id_Ugyindito      uniqueidentifier,         
             @CimSTR_Ugyindito      Nvarchar(400),         
             @LezarasOka     nvarchar(64),         
             @FelfuggesztesOka     Nvarchar(400),     
             @UgyintezesKezdete     datetime,         
             @FelfuggesztettNapokSzama     int,         
             @IntezesiIdo     nvarchar(64),         
             @IntezesiIdoegyseg     nvarchar(64),         
             @Ugy_Fajtaja     nvarchar(64),         
             @SzignaloId     uniqueidentifier,         
             @SzignalasIdeje     datetime,         
             @ElteltIdo     nvarchar(64),         
             @ElteltIdoIdoEgyseg     nvarchar(64),         
             @ElteltidoAllapot     int,         
             @SakkoraAllapot     nvarchar(64),               
             --@HatralevoNapok     int,         
             --@HatralevoMunkaNapok     int,
			 @ElozoAllapot     nvarchar(64),     
             @Ver     int,         
             @Note     Nvarchar(4000),         
             @Stat_id     uniqueidentifier,         
             @ErvKezd     datetime,         
             @ErvVege     datetime,         
             --@Letrehozo_id     uniqueidentifier,         
             --@LetrehozasIdo     datetime,         
             @Modosito_id     uniqueidentifier,         
             @ModositasIdo     datetime,         
             @Zarolo_id     uniqueidentifier,         
             @ZarolasIdo     datetime,         
             @Tranz_id     uniqueidentifier,         
             @UIAccessLog_id     uniqueidentifier,
             @UjOrzesiIdo     int,
			 @IrattarId     uniqueidentifier',
             --
             @Foszam  = @Foszam,
             @Sorszam = @Sorszam,    
             @Ugyazonosito    =@Ugyazonosito,         
             @Alkalmazas_Id     =@Alkalmazas_Id,         
             @UgyintezesModja     =@UgyintezesModja,         
             @Hatarido     =@Hatarido,         
             @SkontrobaDat     =@SkontrobaDat,         
             @LezarasDat     =@LezarasDat,         
             @IrattarbaKuldDatuma      =@IrattarbaKuldDatuma,         
             @IrattarbaVetelDat     =@IrattarbaVetelDat,         
             @FelhCsoport_Id_IrattariAtvevo     =@FelhCsoport_Id_IrattariAtvevo,         
             @SelejtezesDat     =@SelejtezesDat,         
             @FelhCsoport_Id_Selejtezo     =@FelhCsoport_Id_Selejtezo,         
             @LeveltariAtvevoNeve     =@LeveltariAtvevoNeve,         
             @FelhCsoport_Id_Felulvizsgalo     =@FelhCsoport_Id_Felulvizsgalo,         
             @FelulvizsgalatDat     =@FelulvizsgalatDat,         
             @IktatoszamKieg     =@IktatoszamKieg,         
             @Targy     =@Targy,         
             @UgyUgyirat_Id_Szulo     =@UgyUgyirat_Id_Szulo, 
             @UgyUgyirat_Id_Kulso = @UgyUgyirat_Id_Kulso,        
             @UgyTipus     =@UgyTipus,         
             @IrattariHely     =@IrattariHely,         
             @SztornirozasDat     =@SztornirozasDat,         
             @Csoport_Id_Felelos     =@Csoport_Id_Felelos,         
             @FelhasznaloCsoport_Id_Orzo     =@FelhasznaloCsoport_Id_Orzo,         
             @Csoport_Id_Cimzett     =@Csoport_Id_Cimzett,         
             @Jelleg     =@Jelleg,         
             @IraIrattariTetel_Id     =@IraIrattariTetel_Id,
             @IraIktatokonyv_Id     =@IraIktatokonyv_Id,         
             @SkontroOka     =@SkontroOka,         
			 -- CR3407
			 @SkontroOka_Kod     =@SkontroOka_Kod,
             @SkontroVege     =@SkontroVege,         
             @Surgosseg     =@Surgosseg,         
             @SkontrobanOsszesen     =@SkontrobanOsszesen,         
             @MegorzesiIdoVege     =@MegorzesiIdoVege,         
             @Partner_Id_Ugyindito     =@Partner_Id_Ugyindito,         
             @NevSTR_Ugyindito     =@NevSTR_Ugyindito,         
             @ElintezesDat     =@ElintezesDat,         
             @FelhasznaloCsoport_Id_Ugyintez     =@FelhasznaloCsoport_Id_Ugyintez,         
             @Csoport_Id_Felelos_Elozo     =@Csoport_Id_Felelos_Elozo,         
             @KolcsonKikerDat     =@KolcsonKikerDat,         
             @KolcsonKiadDat     =@KolcsonKiadDat,         
             @Kolcsonhatarido     =@Kolcsonhatarido,         
             @BARCODE     =@BARCODE,         
             @IratMetadefinicio_Id     =@IratMetadefinicio_Id,         
             @Allapot     =@Allapot,         
             @TovabbitasAlattAllapot     =@TovabbitasAlattAllapot,         
             @Megjegyzes     =@Megjegyzes,         
             @Azonosito     =@Azonosito,         
             @Fizikai_Kezbesitesi_Allapot     =@Fizikai_Kezbesitesi_Allapot,         
             @Kovetkezo_Orzo_Id     =@Kovetkezo_Orzo_Id,         
             @Csoport_Id_Ugyfelelos     =@Csoport_Id_Ugyfelelos,         
             @Elektronikus_Kezbesitesi_Allap     =@Elektronikus_Kezbesitesi_Allap,         
             @Kovetkezo_Felelos_Id     =@Kovetkezo_Felelos_Id,         
             @UtolsoAlszam     =@UtolsoAlszam,
             @UtolsoSorszam = @UtolsoSorszam,
             @IratSzam = @IratSzam,         
             @ElintezesMod     =@ElintezesMod,         
             @RegirendszerIktatoszam     =@RegirendszerIktatoszam,         
             @GeneraltTargy     =@GeneraltTargy,         
			@Cim_Id_Ugyindito = @Cim_Id_Ugyindito,
			@CimSTR_Ugyindito = @CimSTR_Ugyindito,
			@LezarasOka = @LezarasOka,
			@FelfuggesztesOka = @FelfuggesztesOka,
			@UgyintezesKezdete = @UgyintezesKezdete,
			@FelfuggesztettNapokSzama = @FelfuggesztettNapokSzama,
			@IntezesiIdo = @IntezesiIdo,
			@IntezesiIdoegyseg = @IntezesiIdoegyseg,
			@Ugy_Fajtaja = @Ugy_Fajtaja,
			@SzignaloId = @SzignaloId,
			@SzignalasIdeje = @SzignalasIdeje,
			@ElteltIdo = @ElteltIdo,
			@ElteltIdoIdoEgyseg = @ElteltIdoIdoEgyseg,
			@ElteltidoAllapot = @ElteltidoAllapot,
			@SakkoraAllapot = @SakkoraAllapot,
			--@HatralevoNapok = @HatralevoNapok,
			--@HatralevoMunkaNapok = @HatralevoMunkaNapok,
			@ElozoAllapot = @ElozoAllapot,
             @Ver     =@Ver,         
             @Note     =@Note,         
             @Stat_id     =@Stat_id,         
             @ErvKezd     =@ErvKezd,         
             @ErvVege     =@ErvVege,         
             --@Letrehozo_id     =@Letrehozo_id,         
             --@LetrehozasIdo     =@LetrehozasIdo,         
             @Modosito_id     =@Modosito_id,         
             @ModositasIdo     =@ModositasIdo,         
             @Zarolo_id     =@Zarolo_id,         
             @ZarolasIdo     =@ZarolasIdo,         
             @Tranz_id     =@Tranz_id,         
             @UIAccessLog_id     =@UIAccessLog_id,
             @UjOrzesiIdo     =@UjOrzesiIdo,
			 @IrattarId     =@IrattarId;

if @@rowcount != @rowNumberTotal
begin
   RAISERROR('[50401]',16,1)
END
-- BUG_11085
DECLARE @paramTUK_SZIGNALAS_ENABLED nvarchar(400)
SET @paramTUK_SZIGNALAS_ENABLED = (
	SELECT Ertek FROM KRT_Parameterek
	WHERE Nev ='TUK_SZIGNALAS_ENABLED'
	AND Org=@Org
)
   -- Elsődleges ügyiratban lévő iratpéldányok UPDATE
   UPDATE EREC_PldIratPeldanyok
      SET EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo,
         EREC_PldIratPeldanyok.Csoport_Id_Felelos = EREC_UgyUgyiratok.Csoport_Id_Felelos,
         EREC_PldIratPeldanyok.Allapot =
            CASE
               WHEN EREC_UgyUgyiratok.Allapot ='50'   THEN '50'
			    WHEN (EREC_UgyUgyiratok.Allapot ='03') AND (@paramTUK_SZIGNALAS_ENABLED <> '1') THEN '50'
               WHEN (EREC_PldIratPeldanyok.Allapot = '50'
                   AND EREC_PldIratPeldanyok.TovabbitasAlattAllapot IS NOT NULL
                   AND EREC_PldIratPeldanyok.TovabbitasAlattAllapot != '')
                   THEN EREC_PldIratPeldanyok.TovabbitasAlattAllapot
               ELSE EREC_PldIratPeldanyok.Allapot 
            END,
         EREC_PldIratPeldanyok.TovabbitasAlattAllapot = 
            CASE
               WHEN EREC_UgyUgyiratok.Allapot ='50' AND EREC_PldIratPeldanyok.Allapot != '50' THEN EREC_PldIratPeldanyok.Allapot
			   WHEN EREC_UgyUgyiratok.Allapot ='03' AND EREC_PldIratPeldanyok.Allapot != '50' AND (@paramTUK_SZIGNALAS_ENABLED <> '1') THEN EREC_PldIratPeldanyok.Allapot
               -- pl. VisszakĂĽldĂ©s:
               WHEN EREC_UgyUgyiratok.Allapot = '50' AND EREC_PldIratPeldanyok.Allapot = '50' THEN EREC_PldIratPeldanyok.TovabbitasAlattAllapot
			   WHEN EREC_UgyUgyiratok.Allapot = '03' AND EREC_PldIratPeldanyok.Allapot = '50' AND (@paramTUK_SZIGNALAS_ENABLED <> '1')  THEN EREC_PldIratPeldanyok.TovabbitasAlattAllapot
               WHEN EREC_PldIratPeldanyok.Allapot = '50' THEN NULL
               ELSE EREC_PldIratPeldanyok.TovabbitasAlattAllapot
            END,
         EREC_PldIratPeldanyok.Ver = EREC_PldIratPeldanyok.Ver + 1,
         EREC_PldIratPeldanyok.Modosito_Id = @ExecutorUserId,
         EREC_PldIratPeldanyok.ModositasIdo = @ExecutionTime
      FROM EREC_IraIratok
         --INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
         INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id --EREC_UgyUgyiratdarabok.UgyUgyirat_Id
      WHERE EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
         AND EREC_PldIratPeldanyok.Id IN (SELECT Id FROM #pldIds)
      
      -- iratpéldány mozgásának jelzése
      exec sp_EREC_PldIratPeldanyok_Moved @ExecutorUserId = @ExecutorUserId, @ExecutionTime = @ExecutionTime
      DROP TABLE #pldIds;


/* History Log */

   declare @row_ids varchar(MAX)
   /*** EREC_UgyUgyiratok history log ***/

   set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @Ids FOR XML PATH('')),1, 2, '')
   if @row_ids is not null
      set @row_ids = @row_ids + '''';

   exec sp_LogRecordsToHistory_Tomeges
    @TableName = 'EREC_UgyUgyiratok'
   ,@Row_Ids = @row_ids
   ,@Muvelet = 1
   ,@Vegrehajto_Id = @ExecutorUserId
   ,@VegrehajtasIdo = @ExecutionTime

--DECLARE cur CURSOR FOR
-- SELECT Id FROM @tempTable
--
--OPEN cur;
--FETCH NEXT FROM cur INTO @curId;
--WHILE @@FETCH_STATUS = 0
--BEGIN
-- exec sp_LogRecordToHistory 'EREC_UgyUgyiratok',@curId
--             ,'EREC_UgyUgyiratokHistory',1,@ExecutorUserId,@ExecutionTime
-- FETCH NEXT FROM cur INTO @curId;
--END
--

END TRY
BEGIN CATCH
   DECLARE @errorSeverity INT, @errorState INT
   DECLARE @errorCode NVARCHAR(1000)    
   SET @errorSeverity = ERROR_SEVERITY()
   SET @errorState = ERROR_STATE()
   
   if ERROR_NUMBER()<50000 
      SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
   else
      SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

   RAISERROR(@errorCode,@errorSeverity,@errorState)
END CATCH