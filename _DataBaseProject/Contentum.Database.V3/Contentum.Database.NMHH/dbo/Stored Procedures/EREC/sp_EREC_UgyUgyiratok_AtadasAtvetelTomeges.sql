﻿/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_UgyUgyiratok_AtadasAtvetelTomeges')
            and   type = 'P')
   drop procedure sp_EREC_UgyUgyiratok_AtadasAtvetelTomeges
go
*/



create procedure sp_EREC_UgyUgyiratok_AtadasAtvetelTomeges
	@Ids				nvarchar(MAX),
	@Vers				nvarchar(MAX),
	@KovetkezoFelelos	uniqueidentifier = null,
	@KovetkezoOrzo		uniqueidentifier = null,
	@ExecutorUserId		uniqueidentifier,
	@KezFeljegyzesTipus nvarchar(64) = null,
	@Leiras				nvarchar(400) = NULL,
	@CheckMappa			BIT = 1,
	@FelhCsoport_Id_IrattariAtvevo UNIQUEIDENTIFIER = NULL,
	@IrattarbaVetelDat DATETIME = NULL
as

BEGIN TRY
	set nocount ON
	
	DECLARE @Tipus CHAR(1)
	SET @Tipus = '0'
	
	IF @FelhCsoport_Id_IrattariAtvevo IS NOT NULL AND @IrattarbaVetelDat IS NOT NULL
	BEGIN
		SET @Tipus = '3' -- Irattárba vétel
	END
	
	if @KovetkezoOrzo IS NOT NULL
	BEGIN
		SET @Tipus = '2' -- Átvétel	
	END
	
	if @KovetkezoFelelos IS NOT NULL
	BEGIN
		SET @Tipus = '1' --Átadás	
	END

	
	IF @Tipus != '0'
		BEGIN
			
		declare @execTime datetime;
		declare @tempTable table(id uniqueidentifier, ver int);

		set @execTime = getdate();

		/* dinamikus history log összeállításhoz */
		declare @row_ids varchar(MAX)
		declare @insertedRowIds table (Id uniqueidentifier)

		-- temp tábla töltése az id-verzió párokkal
		declare @it	int;
		declare @verPosBegin int;
		declare @verPosEnd int;
		declare @curId	nvarchar(36);
		declare @curVer	nvarchar(6);
		declare @sqlcmd	nvarchar(500);

		set @it = 0;
		set @verPosBegin = 1;
		while (@it < ((len(@Ids)+1) / 39))
		BEGIN
			set @curId = SUBSTRING(@Ids,@it*39+2,37);
			set @verPosEnd =  CHARINDEX(',',@Vers,@verPosBegin);
			if @verPosEnd = 0 
				set @verPosEnd = len(@Vers) + 1;
			set @curVer = SUBSTRING(@vers, @verPosBegin, @verPosEnd - @verPosBegin);
			set @verPosBegin = @verPosEnd+1;
			insert into @tempTable(id,ver) values(@curId,convert(int,@curVer) );
			set @it = @it + 1;
		END

		-- verzióellenőrzés
		declare @rowNumberTotal int;
		declare @rowNumberChecked int;

		select @rowNumberTotal = count(id) from @tempTable;
		select @rowNumberChecked = count(EREC_UgyUgyiratok.Id)
			from EREC_UgyUgyiratok
				inner JOIN @tempTable t on t.id = EREC_UgyUgyiratok.id and t.ver = EREC_UgyUgyiratok.ver
				
		if (@rowNumberTotal != @rowNumberChecked)
		BEGIN
			SELECT Id FROM @tempTable
			EXCEPT
			SELECT EREC_UgyUgyiratok.Id FROM EREC_UgyUgyiratok
				inner join @tempTable t on t.id = EREC_UgyUgyiratok.id and t.ver = EREC_UgyUgyiratok.ver
			RAISERROR('[50402]',16,1);
		END
		
		
		-- Szerelt ügyiratok lekérése
		
		-- Előkészített szerelésre figyelni kell, azokat nem hozzuk
		-- (Azokat kell hozni, ahol UgyUgyirat_Id_Szulo ki van töltve, és vagy az Allapot, vagy a TovabbitasAlattAllapot szerelt(60) )
	   
		;WITH UgyiratHierarchy  AS
		(
		   -- Base case
		   SELECT Id
		   FROM EREC_UgyUgyiratok
		   WHERE UgyUgyirat_Id_Szulo in (select Id from @tempTable)
				 AND (Allapot = '60' OR TovabbitasAlattAllapot = '60')

		   UNION ALL

		   -- Recursive step
		   SELECT u.Id
		   FROM EREC_UgyUgyiratok as u
			  INNER JOIN UgyiratHierarchy as uh ON
				 u.UgyUgyirat_Id_Szulo = uh.Id
		   WHERE (u.Allapot = '60' OR u.TovabbitasAlattAllapot = '60')
		)
			select id INTO #szereltUgyiratok from UgyiratHierarchy where UgyiratHierarchy.id not in (select id from @tempTable)
		select @rowNumberTotal = @rowNumberTotal + count(id) from #szereltUgyiratok;

		INSERT INTO @tempTable(Id)
			SELECT id FROM #szereltUgyiratok;
		
		CREATE TABLE #ugyiratIds (Id UNIQUEIDENTIFIER)
		 	
		INSERT INTO #ugyiratIds SELECT EREC_UgyUgyiratok.Id
			FROM EREC_UgyUgyiratok
			where Id in (select id from @tempTable)
				and (ModositasIdo < @execTime or ModositasIdo is null)
				and Zarolo_id is NULL
		
		-- Ellenőrzés
		if (@rowNumberTotal != (SELECT COUNT(*) FROM #ugyiratIds) )
		BEGIN
			SELECT Id FROM @tempTable
			EXCEPT
			select Id from #ugyiratIds
			DROP TABLE #ugyiratIds;
			RAISERROR('[50402]',16,1);
		END
		
		-- Fizikai mappa ellenőrzés
		IF @CheckMappa = 1
		BEGIN
			SELECT #ugyiratIds.Id INTO #ugyiratokMappa
				FROM #ugyiratIds
					INNER JOIN KRT_MappaTartalmak ON KRT_MappaTartalmak.Obj_Id = #ugyiratIds.Id
					INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
				WHERE KRT_Mappak.Tipus = '01'
					AND GETDATE() BETWEEN KRT_Mappak.ErvKezd AND KRT_Mappak.ErvVege
					AND GETDATE() BETWEEN KRT_MappaTartalmak.ErvKezd AND KRT_MappaTartalmak.ErvVege
			IF EXISTS (SELECT Id FROM #ugyiratokMappa)
			BEGIN
				SELECT Id FROM #ugyiratokMappa
				
				DROP TABLE #ugyiratIds;
				DROP TABLE #ugyiratokMappa;
				DROP TABLE #szereltUgyiratok;
				
				RAISERROR('[64006]',16,1);
			END
			
			DROP TABLE #ugyiratokMappa;
		END	
		
		CREATE TABLE #pldIds (Id UNIQUEIDENTIFIER)
		
		
		INSERT INTO [#pldIds] SELECT [EREC_PldIratPeldanyok].[Id]
		FROM [EREC_PldIratPeldanyok]
			INNER JOIN [EREC_IraIratok] ON [EREC_PldIratPeldanyok].[IraIrat_Id] = [EREC_IraIratok].[Id]
			INNER JOIN [EREC_UgyUgyiratok] ON [EREC_IraIratok].[Ugyirat_Id] = [EREC_UgyUgyiratok].[Id]
			AND EREC_UgyUgyiratok.Id IN (select Id from #ugyiratIds)
		WHERE EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo
			AND EREC_PldIratPeldanyok.Csoport_Id_Felelos = EREC_UgyUgyiratok.Csoport_Id_Felelos
			
		/****** Átadásra kijelölés *****/
		if @Tipus = '1'
		BEGIN		
			-- Ügyirathoz tartozó elsődleges iratpéldányok átadásra kijelölése
			UPDATE EREC_PldIratPeldanyok
					SET EREC_PldIratPeldanyok.Csoport_Id_Felelos_Elozo = EREC_PldIratPeldanyok.Csoport_Id_Felelos,
					EREC_PldIratPeldanyok.Csoport_Id_Felelos = @KovetkezoFelelos,
					EREC_PldIratPeldanyok.TovabbitasAlattAllapot = 
							case EREC_PldIratPeldanyok.Allapot
								when '50' then EREC_PldIratPeldanyok.TovabbitasAlattAllapot
								else EREC_PldIratPeldanyok.Allapot
							END,
					EREC_PldIratPeldanyok.Allapot = '50',
					EREC_PldIratPeldanyok.Ver = EREC_PldIratPeldanyok.Ver + 1,
					EREC_PldIratPeldanyok.Modosito_Id = @ExecutorUserId,
					EREC_PldIratPeldanyok.ModositasIdo = @execTime
				WHERE EREC_PldIratPeldanyok.Id IN (SELECT Id FROM #pldIds)
			
			
			-- iratpéldány mozgásának jelzése
			exec sp_EREC_PldIratPeldanyok_Moved @ExecutorUserId = @ExecutorUserId, @ExecutionTime = @execTime
				
			DROP TABLE #pldIds
			
			-- ügyiratok átadásra kijelölése
			UPDATE EREC_UgyUgyiratok 
				set Csoport_Id_Felelos_Elozo = Csoport_Id_Felelos,
					Csoport_Id_Felelos = @KovetkezoFelelos,
					TovabbitasAlattAllapot = 
						case Allapot
							when '50' then TovabbitasAlattAllapot
							WHEN '60' THEN '50'
							WHEN '10' THEN '09'
							else Allapot
						end,
					Allapot = 
						CASE Allapot
							WHEN '60' then '60'
							ELSE '50'
						END,
					Ver = Ver + 1,
					Modosito_Id = @ExecutorUserId,
					ModositasIdo = @execTime
				where Id in (select Id from #ugyiratIds);
				
			DROP TABLE #ugyiratIds;

			IF @CheckMappa = 1
			BEGIN
				-- Kézbesítési tételek létrehozása
				insert into EREC_IraKezbesitesiTetelek(
						Obj_Id,
						Obj_type,
						AtadasDat,
						Felhasznalo_Id_Atado_LOGIN,
						Felhasznalo_Id_Atado_USER,
						Csoport_Id_Cel,
						Allapot)
					output inserted.id into @insertedRowIds
					select
						Id,
						'EREC_UgyUgyiratok',
						@execTime,
						@ExecutorUserId,
						@ExecutorUserId,
						@KovetkezoFelelos,
						'1'
					from ( SELECT Id FROM @tempTable EXCEPT SELECT Id FROM #szereltUgyiratok ) AS temp
				
					
				/*** EREC_IraKezbesitesiTetelek history log ***/
--				INSERT INTO EREC_IraKezbesitesiTetelekHistory 
--					SELECT	NEWID(),
--							'0',
--							@ExecutorUserId,
--							@execTime,
--							*
--							FROM EREC_IraKezbesitesiTetelek
--							WHERE Obj_id in (select id from @tempTable EXCEPT SELECT Id FROM #szereltUgyiratok ) and AtadasDat = @execTime

				-- Az oszlopnevek sorrendje nem garantált, nem szabad *-gal naplózni!
				/*** EREC_IraKezbesitesiTetelek log ***/
				set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @insertedRowIds FOR XML PATH('')),1, 2, '')
				if @row_ids is not null
					set @row_ids = @row_ids + '''';

				exec [sp_LogRecordsToHistory_Tomeges] 
						 @TableName = 'EREC_IraKezbesitesiTetelek'
						,@Row_Ids = @row_ids
						,@Muvelet = 0
						,@Vegrehajto_Id = @ExecutorUserId
						,@VegrehajtasIdo = @execTime

--				-- Kezelési feljegyzés insert
--				if (@KezFeljegyzesTipus is not null and @KezFeljegyzesTipus <> '') or (@Leiras is not null and @Leiras <> '')
--				BEGIN
--					delete from @insertedRowIds
--					--declare @insertedRowIds table (Id uniqueidentifier)
--					insert into EREC_UgyKezFeljegyzesek(UgyUgyirat_Id,KezelesTipus,Leiras,Letrehozo_Id,LetrehozasIdo)
--						output inserted.id into @insertedRowIds
--						select Id,@KezFeljegyzesTipus,@Leiras,@executorUserId,@execTime from @tempTable
--
--
--				/*** EREC_UgyKezFeljegyzesekHistory log ***/
--				set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @insertedRowIds FOR XML PATH('')),1, 2, '')
--				if @row_ids is not null
--					set @row_ids = @row_ids + '''';
--
--				exec [sp_LogRecordsToHistory_Tomeges] 
--						 @TableName = 'EREC_UgyKezFeljegyzesek'
--						,@Row_Ids = @row_ids
--						,@Muvelet = 0
--						,@Vegrehajto_Id = @ExecutorUserId
--						,@VegrehajtasIdo = @execTime
--				END
			END
		END

		/****** Átvétel *****/
		if @Tipus = '2'
		BEGIN
		   
			-- Ügyirathoz tartozó iratpéldányok átvétele
			UPDATE EREC_IraKezbesitesiTetelek
				set Allapot = '3',
					AtvetelDat = @execTime,
					Felhasznalo_Id_AtvevoUser = @ExecutorUserId,
					Felhasznalo_Id_AtvevoLogin = @ExecutorUserId,
					Ver = Ver + 1,
					Modosito_Id = @ExecutorUserId,
					ModositasIdo = @execTime
				WHERE EREC_IraKezbesitesiTetelek.Obj_Id IN
						(
							SELECT EREC_PldIratPeldanyok.Id
								FROM EREC_PldIratPeldanyok
									INNER JOIN EREC_IraIratok ON EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
									INNER JOIN EREC_UgyUgyiratdarabok ON EREC_UgyUgyiratdarabok.Id = EREC_IraIratok.UgyUgyIratDarab_Id
									INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = EREC_UgyUgyiratdarabok.UgyUgyirat_Id AND EREC_UgyUgyiratok.Id IN (select Id from #ugyiratIds)
								WHERE EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
									AND EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo
									AND EREC_PldIratPeldanyok.Csoport_Id_Felelos = EREC_UgyUgyiratok.Csoport_Id_Felelos
						)
						
			-- Elsődleges iratpéldányok átvétele
			UPDATE EREC_PldIratPeldanyok
				SET EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo = @KovetkezoOrzo,
					EREC_PldIratPeldanyok.Csoport_Id_Felelos = @KovetkezoOrzo,
					EREC_PldIratPeldanyok.Allapot =
						CASE
							WHEN EREC_PldIratPeldanyok.TovabbitasAlattAllapot = '' THEN EREC_PldIratPeldanyok.Allapot
							WHEN EREC_PldIratPeldanyok.TovabbitasAlattAllapot IS NULL THEN EREC_PldIratPeldanyok.Allapot
							ELSE EREC_PldIratPeldanyok.TovabbitasAlattAllapot
						END,
					EREC_PldIratPeldanyok.TovabbitasAlattAllapot = NULL,
					EREC_PldIratPeldanyok.Ver = EREC_PldIratPeldanyok.Ver + 1,
					EREC_PldIratPeldanyok.Modosito_Id = @ExecutorUserId,
					EREC_PldIratPeldanyok.ModositasIdo = @execTime
				WHERE [EREC_PldIratPeldanyok].[Id] IN (SELECT Id FROM [#pldIds])
			
			-- iratpéldány mozgásának jelzése
			exec sp_EREC_PldIratPeldanyok_Moved @ExecutorUserId = @ExecutorUserId, @ExecutionTime = @execTime
			
			DROP TABLE [#pldIds]

			-- ügyiratok átvétele	
			UPDATE EREC_UgyUgyiratok 
				set Csoport_Id_Felelos = @KovetkezoOrzo,
					FelhasznaloCsoport_Id_Orzo = @KovetkezoOrzo,
					Allapot = 
						CASE 
							WHEN Allapot = '60' THEN '60'
							when TovabbitasAlattAllapot = '03' then '06' -- Szignált -> Ügyintézés alatt
							WHEN TovabbitasAlattAllapot IS NULL OR TovabbitasAlattAllapot = '' THEN
								CASE WHEN Allapot = '03'THEN '06'
								ELSE Allapot END
							else TovabbitasAlattAllapot
						END,
					TovabbitasAlattAllapot = NULL,
					Ver = Ver + 1,
					Modosito_Id = @ExecutorUserId,
					ModositasIdo = @execTime
				where Id in (select Id FROM #ugyiratIds)

			DECLARE @IS_TUK char(1) 
			SET	@IS_TUK = (select TOP 1 ertek from KRT_Parameterek	where nev='TUK')

			/*Irattári hely*/
			IF @IS_TUK = '1'
			BEGIN
				declare @irattariId uniqueidentifier;
				declare @irattariHely nvarchar(100);

				select top 1 @irattariId=ih.Id, @irattariHely=ih.Ertek from
				EREC_IrattariHelyek ih
				inner join  KRT_PartnerKapcsolatok pk on pk.Partner_id_kapcsolt = ih.Felelos_Csoport_Id		
				inner join KRT_PartnerKapcsolatok pkf on pkf.Partner_id_kapcsolt=pk.Partner_id
				inner join KRT_CsoportTagok cst on cst.Csoport_Id=pkf.Partner_id
				inner join KRT_Csoportok cs on cs.Id=cst.Csoport_Id_Jogalany
				where pk.Tipus = '5'		and
				((cst.Csoport_Id_Jogalany=@KovetkezoOrzo and cs.Tipus='1') or (pkf.Partner_id=@KovetkezoOrzo and cs.Tipus!='1'))
				and ih.ErvKezd<=getdate() and ih.ErvVege>=getdate() and pk.ErvKezd<=getdate() and pk.ErvVege>=getdate()
				and  cst.ErvKezd<=getdate() and cst.ErvVege>=getdate()

				UPDATE EREC_PldIratPeldanyok
					SET IrattariHely = @irattariHely,
						IrattarId = @irattariId
				FROM [EREC_PldIratPeldanyok]
					INNER JOIN [EREC_IraIratok] ON [EREC_PldIratPeldanyok].[IraIrat_Id] = [EREC_IraIratok].[Id]
					INNER JOIN [EREC_UgyUgyiratok] ON [EREC_IraIratok].[Ugyirat_Id] = [EREC_UgyUgyiratok].[Id]
					AND EREC_UgyUgyiratok.Id IN (select Id from #ugyiratIds)
				WHERE EREC_PldIratPeldanyok.IrattarId = EREC_UgyUgyiratok.IrattarId

				UPDATE EREC_PldIratPeldanyok
					SET IrattariHely = @irattariHely,
						IrattarId = @irattariId
				FROM [EREC_PldIratPeldanyok]
					INNER JOIN [EREC_IraIratok] ON [EREC_PldIratPeldanyok].[IraIrat_Id] = [EREC_IraIratok].[Id]
					INNER JOIN [EREC_UgyUgyiratok] ON [EREC_IraIratok].[Ugyirat_Id] = [EREC_UgyUgyiratok].[Id]
					AND EREC_UgyUgyiratok.Id IN (select Id from #ugyiratIds)
				WHERE [EREC_IraIratok].Alszam=1 and [EREC_IraIratok].PostazasIranya='1'

				UPDATE EREC_UgyUgyiratok
					SET IrattariHely = @irattariHely,
						IrattarId = @irattariId
				WHERE Id in (select Id FROM #ugyiratIds)
			END

			DROP TABLE #ugyiratIds;
		END
		
		/****** Irattárba vétel *****/
		if @Tipus = '3'
		BEGIN
			-- ügyiratok átvétele	
			UPDATE EREC_UgyUgyiratok 
				set FelhasznaloCsoport_Id_Orzo = EREC_UgyUgyiratok.Csoport_Id_Felelos,
					Allapot = 
						CASE 
							WHEN EREC_UgyUgyiratok.Allapot = '60' THEN '60' --Szerelt
							ELSE 
								 CASE 
									WHEN EXISTS(SELECT 1 FROM EREC_IrattariKikero
												WHERE EREC_IrattariKikero.UgyUgyirat_Id = EREC_UgyUgyiratok.Id
												AND EREC_IrattariKikero.Allapot = '02' --Engedélyezhető
												AND GETDATE() BETWEEN EREC_IrattariKikero.ErvKezd AND EREC_IrattariKikero.ErvVege) 
									THEN '55' --Irattárból elkért
									ELSE '10' --Irattárban őrzött
								 END
						END,
					TovabbitasAlattAllapot = NULL,
					FelhCsoport_Id_IrattariAtvevo =  @FelhCsoport_Id_IrattariAtvevo,
					IrattarbaVetelDat = @IrattarbaVetelDat,
					Ver = EREC_UgyUgyiratok.Ver + 1,
					Modosito_Id = @ExecutorUserId,
					ModositasIdo = @execTime
				where EREC_UgyUgyiratok.Id in (select Id FROM #ugyiratIds)
			DROP TABLE #ugyiratIds;
			
			 -- Elsődleges iratpéldányok átvétele
			UPDATE EREC_PldIratPeldanyok
				SET EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo = EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo,
				EREC_PldIratPeldanyok.Csoport_Id_Felelos = EREC_UgyUgyiratok.Csoport_Id_Felelos,
				EREC_PldIratPeldanyok.Allapot =
				CASE
					WHEN EREC_PldIratPeldanyok.TovabbitasAlattAllapot = '' THEN EREC_PldIratPeldanyok.Allapot
					WHEN EREC_PldIratPeldanyok.TovabbitasAlattAllapot IS NULL THEN EREC_PldIratPeldanyok.Allapot
					ELSE EREC_PldIratPeldanyok.TovabbitasAlattAllapot
				END,
				EREC_PldIratPeldanyok.TovabbitasAlattAllapot = NULL,
				EREC_PldIratPeldanyok.Ver = EREC_PldIratPeldanyok.Ver + 1,
				EREC_PldIratPeldanyok.Modosito_Id = @ExecutorUserId,
				EREC_PldIratPeldanyok.ModositasIdo = @execTime
				FROM EREC_IraIratok
				INNER JOIN EREC_UgyUgyiratok ON EREC_UgyUgyiratok.Id = EREC_IraIratok.Ugyirat_Id
				WHERE EREC_IraIratok.Id = EREC_PldIratPeldanyok.IraIrat_Id
				AND [EREC_PldIratPeldanyok].[Id] IN (SELECT Id FROM [#pldIds])
			
			-- iratpéldány mozgásának jelzése
			exec sp_EREC_PldIratPeldanyok_Moved @ExecutorUserId = @ExecutorUserId, @ExecutionTime = @execTime
			
			DROP TABLE [#pldIds]
		END

		DROP TABLE #szereltUgyiratok;
		
--		begin   /* History Log */
--			/*** EREC_KuldKuldemenyek history log ***/
--			declare ugyiratHistoryCursor cursor for 
--				select Id from @tempTable;
--			declare @ugyiratId uniqueidentifier;
--
--			open ugyiratHistoryCursor;
--			fetch next from ugyiratHistoryCursor into @ugyiratId;
--			while @@FETCH_STATUS = 0
--			BEGIN
--				exec sp_LogRecordToHistory 'EREC_UgyUgyiratok',@ugyiratId
--							,'EREC_UgyUgyiratokHistory',1,@ExecutorUserId,@execTime;
--				fetch next from ugyiratHistoryCursor into @ugyiratId;
--			END
--			
--			CLOSE ugyiratHistoryCursor;
--			DEALLOCATE ugyiratHistoryCursor;
--		END

		/*** EREC_UgyUgyiratokHistory log ***/
		set @row_ids = STUFF((SELECT ''',''' + convert(varchar(36), Id) from @tempTable FOR XML PATH('')),1, 2, '')
		if @row_ids is not null
			set @row_ids = @row_ids + '''';

		exec [sp_LogRecordsToHistory_Tomeges] 
				 @TableName = 'EREC_UgyUgyiratok'
				,@Row_Ids = @row_ids
				,@Muvelet = 1
				,@Vegrehajto_Id = @ExecutorUserId
				,@VegrehajtasIdo = @execTime
	
	END


END TRY
BEGIN CATCH

	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH