/*
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_UgyUgyiratok_ElteltIdoNoveles')
            and   type = 'P')
   drop procedure sp_EREC_UgyUgyiratok_ElteltIdoNoveles
go
*/

-- =============================================
-- Author:		AXIS REDNSZERHAZ KFT
-- Create date: 2019.10
-- Description: N�veli az �gyirat eltelt id� �rt�k�t
-- =============================================
CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratok_ElteltIdoNoveles] 
	@UgyiratId UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ELTELT_IDO_ALLAPOT_NEW NVARCHAR(64)
	SET @ELTELT_IDO_ALLAPOT_NEW = '0'
	DECLARE @ELTELT_IDO_ALLAPOT_START NVARCHAR(64)
	SET @ELTELT_IDO_ALLAPOT_START = '1'
	DECLARE @ELTELT_IDO_ALLAPOT_STOP NVARCHAR(64)
	SET @ELTELT_IDO_ALLAPOT_STOP = '2'
	DECLARE @ELTELT_IDO_ALLAPOT_PAUSE NVARCHAR(64)
	SET @ELTELT_IDO_ALLAPOT_PAUSE = '3'

	DECLARE @IDOEGYSEG_PERC INT
	SET @IDOEGYSEG_PERC = 1
	DECLARE @IDOEGYSEG_ORA NVARCHAR(64)
	SET @IDOEGYSEG_ORA = 60
	DECLARE @IDOEGYSEG_NAP INT
	SET @IDOEGYSEG_NAP = 1440
	DECLARE @IDOEGYSEG_MUNKANAP INT
	SET @IDOEGYSEG_MUNKANAP = -1440

	IF @UgyiratId IS NOT NULL ----UNIT TEST CASE
		BEGIN

			UPDATE EREC_UGYUGYIRATOK
			SET  ElteltIdo = ElteltIdo + 1
				,ElteltIdoUtolsoModositas = GETDATE()
			WHERE ID=@UgyiratId AND ElteltidoAllapot = @ELTELT_IDO_ALLAPOT_START
						  
		END
	ELSE				      ----SQL JOB CASE
		BEGIN

			IF ([dbo].[fn_munkanap] (GETDATE()) = 1)
				BEGIN
					UPDATE EREC_UGYUGYIRATOK
					SET ElteltIdo = ElteltIdo + 1
						,ElteltIdoUtolsoModositas = GETDATE()
					WHERE ElteltidoAllapot = @ELTELT_IDO_ALLAPOT_START
						AND ElteltIdoIdoEgyseg = @IDOEGYSEG_MUNKANAP
				 END 
		
			UPDATE EREC_UGYUGYIRATOK
			SET ElteltIdo = ElteltIdo + 1
				,ElteltIdoUtolsoModositas = GETDATE()
			WHERE ElteltidoAllapot = @ELTELT_IDO_ALLAPOT_START
				AND ElteltIdoIdoEgyseg = @IDOEGYSEG_NAP
				
		END
END
GO


