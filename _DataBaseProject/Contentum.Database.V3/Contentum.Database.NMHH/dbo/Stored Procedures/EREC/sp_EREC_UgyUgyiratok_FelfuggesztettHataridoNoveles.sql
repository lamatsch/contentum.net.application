﻿/*
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
*/
-- =============================================
-- Author:		AXIS REDNSZERHAZ KFT
-- Create date: 2019.06
-- Description: Növeli a felfüggesztett ügyirat 
--              határidejét a köetkező munkanapra.
-- =============================================
CREATE PROCEDURE sp_EREC_UgyUgyiratok_FelfuggesztettHataridoNoveles 
	@UgyiratId UNIQUEIDENTIFIER = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Nap NVARCHAR(MAX) 
	DECLARE @Munkanap NVARCHAR(MAX) 
	SET  @Nap = '1440';
	SET  @Munkanap = '-1440';

	DECLARE @ELTELT_IDO_ALLAPOT_STOP NVARCHAR(64)
	SET @ELTELT_IDO_ALLAPOT_STOP = '2'

	DECLARE @ID			UNIQUEIDENTIFIER
	DECLARE @ALLAPOT	NVARCHAR(MAX)
	DECLARE @HATARIDO	DATETIME
	DECLARE @IDOEGYSEG	NVARCHAR(MAX)

	DECLARE db_cursor CURSOR FOR  
	SELECT U.Id, u.Allapot, U.hatarido, U.IntezesiIdoegyseg
	FROM dbo.EREC_UgyUgyiratok U
	WHERE U.Allapot = '62'	--Felfüggesztett állapot
		OR (    U.Allapot <> '62' 
		AND U.ElteltidoAllapot = @ELTELT_IDO_ALLAPOT_STOP
		AND [dbo].[fn_IratHatasaTipusa] (U.SakkoraAllapot) = 'PAUSE' ) 
		AND (@UgyiratId IS NULL OR U.ID = @UgyiratId)

	OPEN db_cursor   
	FETCH NEXT FROM db_cursor INTO @ID, @ALLAPOT,@HATARIDO,@IDOEGYSEG

	DECLARE @MEGVAN_KOV_DATUM BIT 
	DECLARE @LIMIT BIT 
	DECLARE @KOV_DATUM DATETIME

	WHILE @@FETCH_STATUS = 0   
	BEGIN   
		 SET @KOV_DATUM = @HATARIDO
		 SET @MEGVAN_KOV_DATUM = 0
		 SET @LIMIT = 0

		 IF (@IDOEGYSEG = @Munkanap)
			 BEGIN
				 WHILE(@MEGVAN_KOV_DATUM = 0 OR @LIMIT > 365)
					 BEGIN

						SET @KOV_DATUM = DATEADD(DAY,1,@KOV_DATUM)

						IF ([dbo].[fn_munkanap] (@KOV_DATUM) = 1)
						BEGIN
							SET @MEGVAN_KOV_DATUM = 1
							BREAK
						END
						SET @LIMIT = @LIMIT + 1
		 
					 END
			 END
		 ELSE
			BEGIN
				SET @KOV_DATUM = DATEADD(DAY,1,@KOV_DATUM)
				SET @MEGVAN_KOV_DATUM = 1
			END
		
		 UPDATE dbo.EREC_UgyUgyiratok
				SET Hatarido = @KOV_DATUM,
			    FelfuggesztettNapokSzama = ISNULL(FelfuggesztettNapokSzama, 0) + 1
		 WHERE Id = @ID 
			AND 
			   Hatarido < @KOV_DATUM

		 FETCH NEXT FROM db_cursor INTO @ID, @ALLAPOT,@HATARIDO, @IDOEGYSEG   
	END   

	CLOSE db_cursor   
	DEALLOCATE db_cursor

	-- iratokra
	DECLARE db_cursor CURSOR FOR  
	SELECT i.Id, i.Allapot, i.hatarido, i.IntezesiIdoegyseg
	FROM EREC_IraIratok i
	join dbo.EREC_UgyUgyiratok U on i.Ugyirat_Id = U.Id
	WHERE U.Allapot = '62'	--Felfüggesztett állapot
		OR (    U.Allapot <> '62' 
		AND U.ElteltidoAllapot = @ELTELT_IDO_ALLAPOT_STOP
		AND [dbo].[fn_IratHatasaTipusa] (U.SakkoraAllapot) = 'PAUSE' ) 
		AND (@UgyiratId IS NULL OR U.ID = @UgyiratId)

	OPEN db_cursor   
	FETCH NEXT FROM db_cursor INTO @ID, @ALLAPOT,@HATARIDO,@IDOEGYSEG

	WHILE @@FETCH_STATUS = 0   
	BEGIN   
		 SET @KOV_DATUM = @HATARIDO
		 SET @MEGVAN_KOV_DATUM = 0
		 SET @LIMIT = 0

		 IF (@IDOEGYSEG = @Munkanap)
			 BEGIN
				 WHILE(@MEGVAN_KOV_DATUM = 0 OR @LIMIT > 365)
					 BEGIN

						SET @KOV_DATUM = DATEADD(DAY,1,@KOV_DATUM)

						IF ([dbo].[fn_munkanap] (@KOV_DATUM) = 1)
						BEGIN
							SET @MEGVAN_KOV_DATUM = 1
							BREAK
						END
						SET @LIMIT = @LIMIT + 1
		 
					 END
			 END
		 ELSE
			BEGIN
				SET @KOV_DATUM = DATEADD(DAY,1,@KOV_DATUM)
				SET @MEGVAN_KOV_DATUM = 1
			END

		 UPDATE EREC_IraIratok
				SET Hatarido = @KOV_DATUM,
			    FelfuggesztettNapokSzama = ISNULL(FelfuggesztettNapokSzama, 0) + 1
		 WHERE Id = @ID 
			AND 
			   Hatarido < @KOV_DATUM

		 FETCH NEXT FROM db_cursor INTO @ID, @ALLAPOT,@HATARIDO,@IDOEGYSEG   
	END   

	CLOSE db_cursor   
	DEALLOCATE db_cursor

END