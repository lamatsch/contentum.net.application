﻿/*IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_EREC_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes]]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_EREC_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
*/
CREATE PROCEDURE [dbo].[sp_EREC_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes]
         @ExecutorUserId UNIQUEIDENTIFIER	=	NULL
AS

begin
BEGIN TRY

	SET NOCOUNT ON

	DECLARE @Lejart TABLE
	(
		Id					UNIQUEIDENTIFIER,
		Azonosito			NVARCHAR(MAX),
		MegorzesiIdoVege	DATETIME
	)
	DECLARE @Felelosok TABLE
	(
	  Csoport_Id_Felelos			UNIQUEIDENTIFIER,
	  Csoport_Id_Felelos_Nev		NVARCHAR(MAX),
	  Csoport_Id_Felelos_Email		NVARCHAR(MAX)
	)

	INSERT INTO @Lejart (Id, Azonosito, MegorzesiIdoVege)
	SELECT U.Id, U.Azonosito, U.MegorzesiIdoVege
	FROM EREC_UgyUgyiratok U
	LEFT JOIN EREC_IraIrattariTetelek IT ON U.IraIrattariTetel_Id = IT.Id
	WHERE 
		(U.Allapot = '10' OR U.TovabbitasAlattAllapot = '10')
		AND U.Allapot NOT IN ('30','31','34')
		AND U.ErvKezd <= GETDATE() 
		AND U.ErvVege >= GETDATE()
		AND IT.IrattariJel = 'S'
		AND U.MegorzesiIdoVege <= GETDATE()
	
	INSERT INTO @Felelosok (Csoport_Id_Felelos, Csoport_Id_Felelos_Nev, Csoport_Id_Felelos_Email)
	SELECT DISTINCT F.Id, F.Nev,f.EMail
	FROM KRT_CsoportTagok  CT
	LEFT JOIN KRT_Csoportok C    ON CT.Csoport_Id_Jogalany = C.Id
	LEFT JOIN KRT_Felhasznalok F ON CT.Csoport_Id_Jogalany = F.Id
	WHERE Csoport_Id = 
		(SELECT TOP 1 OBJ_ID FROM KRT_KodTarak WHERE KOD = 'SPEC_SZERVEK.KOZPONTIIRATTAR' AND OBJ_ID IS NOT NULL AND GETDATE() BETWEEN ErvKezd AND ErvVege)
		AND GETDATE() BETWEEN CT.ErvKezd AND CT.ErvVege
		AND GETDATE() BETWEEN  F.ErvKezd AND  F.ErvVege
		AND GETDATE() BETWEEN  C.ErvKezd AND  C.ErvVege

	SELECT L.Id, L.Azonosito, L.MegorzesiIdoVege, F.Csoport_Id_Felelos, F.Csoport_Id_Felelos_Nev, F.Csoport_Id_Felelos_Email
	FROM @Lejart L
	CROSS JOIN @Felelosok F

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
end

GO


