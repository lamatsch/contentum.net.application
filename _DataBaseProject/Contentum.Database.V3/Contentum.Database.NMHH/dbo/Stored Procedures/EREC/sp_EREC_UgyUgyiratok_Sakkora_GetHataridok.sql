﻿
CREATE procedure [dbo].[sp_EREC_UgyUgyiratok_Sakkora_GetHataridok]
		@Ugyirat_Id					UNIQUEIDENTIFIER,
		@Hatarido_Mar_Novelve		BIT,

		@LetrehozasIdo				DATETIME = NULL OUTPUT,
		@Hatarido					DATETIME = NULL OUTPUT,
		@Eltelt_Nap					INT = NULL OUTPUT,
		@Eltelt_MunkaNap			INT = NULL OUTPUT,
		@Fennmarado_Nap				INT = NULL OUTPUT,
		@Hatarido_Tullepve			BIT = NULL OUTPUT
as
BEGIN TRY
	DECLARE @CURRENT_DATE		DATETIME	=		GETDATE()
	DECLARE @START_DATE			DATETIME	
	
	SELECT @Hatarido = U.Hatarido, 
		   @LetrehozasIdo = U.LetrehozasIdo,
		   @Eltelt_Nap = U.Elteltido,
		   @Eltelt_MunkaNap = U.Elteltido
	FROM dbo.EREC_UgyUgyiratok U
	WHERE U.Id = @Ugyirat_Id

	IF (@Hatarido_Mar_Novelve = 0)
		SELECT @Hatarido = [dbo].[fn_EREC_UgyUgyiratok_Sakkora_GetHataridoKovetkezo] (@Ugyirat_Id)
		
	IF (@Hatarido IS NULL)
		RAISERROR('Határidő üres',16,1)

	/*SET @Eltelt_Nap = DATEDIFF(DAY,@LetrehozasIdo,@CURRENT_DATE)
	
	SET @Eltelt_MunkaNap = dbo.fn_munkanapok_szama(@LetrehozasIdo, @CURRENT_DATE);*/

	SET @Fennmarado_Nap = DATEDIFF(DAY,@CURRENT_DATE, @Hatarido)

	IF (@Fennmarado_Nap IS NOT NULL AND @Fennmarado_Nap <0)
		BEGIN
			SET @Hatarido_Tullepve = 1
		END
	ELSE
		BEGIN
			IF  (@CURRENT_DATE > DATEADD(Day, 1, DATEDIFF(Day, 0, @Hatarido))) 
				SET @Hatarido_Tullepve = 1
			ELSE
				SET @Hatarido_Tullepve = 0
		END

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
END CATCH 