﻿create procedure [dbo].[sp_EREC_UgyiratKapcsolatokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_UgyiratKapcsolatok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_UgyiratKapcsolatok.Id,
	   EREC_UgyiratKapcsolatok.KapcsolatTipus,
	   EREC_UgyiratKapcsolatok.Leiras,
	   EREC_UgyiratKapcsolatok.Kezi,
	   EREC_UgyiratKapcsolatok.Ugyirat_Ugyirat_Beepul,
	   EREC_UgyiratKapcsolatok.Ugyirat_Ugyirat_Felepul,
	   EREC_UgyiratKapcsolatok.Ver,
	   EREC_UgyiratKapcsolatok.Note,
	   EREC_UgyiratKapcsolatok.Stat_id,
	   EREC_UgyiratKapcsolatok.ErvKezd,
	   EREC_UgyiratKapcsolatok.ErvVege,
	   EREC_UgyiratKapcsolatok.Letrehozo_id,
	   EREC_UgyiratKapcsolatok.LetrehozasIdo,
	   EREC_UgyiratKapcsolatok.Modosito_id,
	   EREC_UgyiratKapcsolatok.ModositasIdo,
	   EREC_UgyiratKapcsolatok.Zarolo_id,
	   EREC_UgyiratKapcsolatok.ZarolasIdo,
	   EREC_UgyiratKapcsolatok.Tranz_id,
	   EREC_UgyiratKapcsolatok.UIAccessLog_id  
   from 
     EREC_UgyiratKapcsolatok as EREC_UgyiratKapcsolatok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end