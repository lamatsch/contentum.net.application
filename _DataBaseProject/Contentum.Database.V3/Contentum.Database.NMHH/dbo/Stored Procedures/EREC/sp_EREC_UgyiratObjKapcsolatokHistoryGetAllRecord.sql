﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_UgyiratObjKapcsolatokHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_EREC_UgyiratObjKapcsolatokHistoryGetAllRecord
go
*/
create procedure sp_EREC_UgyiratObjKapcsolatokHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_UgyiratObjKapcsolatokHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyiratObjKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KapcsolatTipus' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyiratObjKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KapcsolatTipus as nvarchar(max)),'') != ISNULL(CAST(New.KapcsolatTipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'KAPCSOLATTIPUS '
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.KapcsolatTipus and OldKt.Org=U.Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.KapcsolatTipus and NewKt.Org=U.Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Leiras' as ColumnName,               cast(Old.Leiras as nvarchar(99)) as OldValue,
               cast(New.Leiras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyiratObjKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Leiras as nvarchar(max)),'') != ISNULL(CAST(New.Leiras as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Kezi' as ColumnName,               cast(Old.Kezi as nvarchar(99)) as OldValue,
               cast(New.Kezi as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyiratObjKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Kezi as nvarchar(max)),'') != ISNULL(CAST(New.Kezi as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Id_Elozmeny' as ColumnName,               cast(Old.Obj_Id_Elozmeny as nvarchar(99)) as OldValue,
               cast(New.Obj_Id_Elozmeny as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyiratObjKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Obj_Id_Elozmeny as nvarchar(max)),'') != ISNULL(CAST(New.Obj_Id_Elozmeny as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Tip_Id_Elozmeny' as ColumnName,               cast(Old.Obj_Tip_Id_Elozmeny as nvarchar(99)) as OldValue,
               cast(New.Obj_Tip_Id_Elozmeny as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyiratObjKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Obj_Tip_Id_Elozmeny as nvarchar(max)),'') != ISNULL(CAST(New.Obj_Tip_Id_Elozmeny as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Type_Elozmeny' as ColumnName,               cast(Old.Obj_Type_Elozmeny as nvarchar(99)) as OldValue,
               cast(New.Obj_Type_Elozmeny as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyiratObjKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Obj_Type_Elozmeny as nvarchar(max)),'') != ISNULL(CAST(New.Obj_Type_Elozmeny as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Id_Kapcsolt' as ColumnName,               cast(Old.Obj_Id_Kapcsolt as nvarchar(99)) as OldValue,
               cast(New.Obj_Id_Kapcsolt as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyiratObjKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Obj_Id_Kapcsolt as nvarchar(max)),'') != ISNULL(CAST(New.Obj_Id_Kapcsolt as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Tip_Id_Kapcsolt' as ColumnName,               cast(Old.Obj_Tip_Id_Kapcsolt as nvarchar(99)) as OldValue,
               cast(New.Obj_Tip_Id_Kapcsolt as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyiratObjKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Obj_Tip_Id_Kapcsolt as nvarchar(max)),'') != ISNULL(CAST(New.Obj_Tip_Id_Kapcsolt as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Obj_Type_Kapcsolt' as ColumnName,               cast(Old.Obj_Type_Kapcsolt as nvarchar(99)) as OldValue,
               cast(New.Obj_Type_Kapcsolt as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_UgyiratObjKapcsolatokHistory Old
         inner join EREC_UgyiratObjKapcsolatokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_UgyiratObjKapcsolatokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Obj_Type_Kapcsolt as nvarchar(max)),'') != ISNULL(CAST(New.Obj_Type_Kapcsolt as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go