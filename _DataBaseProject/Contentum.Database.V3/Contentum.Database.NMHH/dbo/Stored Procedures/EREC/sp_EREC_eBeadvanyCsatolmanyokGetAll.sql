create procedure sp_EREC_eBeadvanyCsatolmanyokGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_eBeadvanyCsatolmanyok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_eBeadvanyCsatolmanyok.Id,
	   EREC_eBeadvanyCsatolmanyok.eBeadvany_Id,
	   EREC_eBeadvanyCsatolmanyok.Dokumentum_Id,
	   EREC_eBeadvanyCsatolmanyok.Nev,
	   EREC_eBeadvanyCsatolmanyok.Ver,
	   EREC_eBeadvanyCsatolmanyok.Note,
	   EREC_eBeadvanyCsatolmanyok.Stat_id,
	   EREC_eBeadvanyCsatolmanyok.ErvKezd,
	   EREC_eBeadvanyCsatolmanyok.ErvVege,
	   EREC_eBeadvanyCsatolmanyok.Letrehozo_id,
	   EREC_eBeadvanyCsatolmanyok.LetrehozasIdo,
	   EREC_eBeadvanyCsatolmanyok.Modosito_id,
	   EREC_eBeadvanyCsatolmanyok.ModositasIdo,
	   EREC_eBeadvanyCsatolmanyok.Zarolo_id,
	   EREC_eBeadvanyCsatolmanyok.ZarolasIdo,
	   EREC_eBeadvanyCsatolmanyok.Tranz_id,
	   EREC_eBeadvanyCsatolmanyok.UIAccessLog_id  
   from 
     EREC_eBeadvanyCsatolmanyok as EREC_eBeadvanyCsatolmanyok      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go