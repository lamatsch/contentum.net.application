/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_eBeadvanyokGetAll')
            and   type = 'P')
   drop procedure sp_EREC_eBeadvanyokGetAll
go
*/
create procedure sp_EREC_eBeadvanyokGetAll
  @Where nvarchar(MAX) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_eBeadvanyok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier

as

begin

BEGIN TRY

	set nocount on
           
	DECLARE @sqlcmd nvarchar(MAX)
	SET @sqlcmd = ''

	DECLARE @LocalTopRow nvarchar(10)

	if (@TopRow = '' or @TopRow = '0')
	BEGIN
		SET @LocalTopRow = ''
	END
	ELSE
	BEGIN
		SET @LocalTopRow = ' TOP ' + @TopRow
	END
 

	SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_eBeadvanyok.Id,
	   EREC_eBeadvanyok.Irany,
	   EREC_eBeadvanyok.Allapot,
	   EREC_eBeadvanyok.KuldoRendszer,
	   EREC_eBeadvanyok.UzenetTipusa,
	   EREC_eBeadvanyok.FeladoTipusa,
	   EREC_eBeadvanyok.PartnerKapcsolatiKod,
	   EREC_eBeadvanyok.PartnerNev,
	   EREC_eBeadvanyok.PartnerEmail,
	   EREC_eBeadvanyok.PartnerRovidNev,
	   EREC_eBeadvanyok.PartnerMAKKod,
	   EREC_eBeadvanyok.PartnerKRID,
	   EREC_eBeadvanyok.Partner_Id,
	   EREC_eBeadvanyok.Cim_Id,
	   EREC_eBeadvanyok.KR_HivatkozasiSzam,
	   EREC_eBeadvanyok.KR_ErkeztetesiSzam,
	   EREC_eBeadvanyok.Contentum_HivatkozasiSzam,
	   EREC_eBeadvanyok.PR_HivatkozasiSzam,
	   EREC_eBeadvanyok.PR_ErkeztetesiSzam,
	   EREC_eBeadvanyok.KR_DokTipusHivatal,
	   EREC_eBeadvanyok.KR_DokTipusAzonosito,
	   EREC_eBeadvanyok.KR_DokTipusLeiras,
	   EREC_eBeadvanyok.KR_Megjegyzes,
	   EREC_eBeadvanyok.KR_ErvenyessegiDatum,
	   EREC_eBeadvanyok.KR_ErkeztetesiDatum,
	   EREC_eBeadvanyok.KR_FileNev,
	   EREC_eBeadvanyok.KR_Kezbesitettseg,
	   EREC_eBeadvanyok.KR_Idopecset,
	   EREC_eBeadvanyok.KR_Valasztitkositas,
	   EREC_eBeadvanyok.KR_Valaszutvonal,
	   EREC_eBeadvanyok.KR_Rendszeruzenet,
	   EREC_eBeadvanyok.KR_Tarterulet,
	   EREC_eBeadvanyok.KR_ETertiveveny,
	   EREC_eBeadvanyok.KR_Lenyomat,
	   EREC_eBeadvanyok.KuldKuldemeny_Id,
	   EREC_eBeadvanyok.IraIrat_Id,
	   EREC_eBeadvanyok.IratPeldany_Id,
	   EREC_eBeadvanyok.Cel,
	   EREC_eBeadvanyok.PR_Parameterek,
	   EREC_eBeadvanyok.KR_Fiok,
	   EREC_eBeadvanyok.FeldolgozasStatusz,
	   EREC_eBeadvanyok.FeldolgozasiHiba,
	   EREC_eBeadvanyok.Ver,
	   EREC_eBeadvanyok.Note,
	   EREC_eBeadvanyok.Stat_id,
	   EREC_eBeadvanyok.ErvKezd,
	   EREC_eBeadvanyok.ErvVege,
	   EREC_eBeadvanyok.Letrehozo_id,
	   EREC_eBeadvanyok.LetrehozasIdo,
	   EREC_eBeadvanyok.Modosito_id,
	   EREC_eBeadvanyok.ModositasIdo,
	   EREC_eBeadvanyok.Zarolo_id,
	   EREC_eBeadvanyok.ZarolasIdo,
	   EREC_eBeadvanyok.Tranz_id,
	   EREC_eBeadvanyok.UIAccessLog_id  
   from 
     EREC_eBeadvanyok as EREC_eBeadvanyok      
   '

	if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
	SET @sqlcmd = @sqlcmd + @OrderBy;
	exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end
go