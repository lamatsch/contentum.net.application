﻿
create procedure sp_EREC_eBeadvanyokHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_eBeadvanyokHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_eBeadvanyokHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Irany' as ColumnName,               
               cast(Old.Irany as nvarchar(99)) as OldValue,
               cast(New.Irany as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Irany as nvarchar(max)),'') != ISNULL(CAST(New.Irany as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Allapot as nvarchar(max)),'') != ISNULL(CAST(New.Allapot as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'eBEADVANY_ALLAPOT'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.Allapot and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.Allapot and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldoRendszer' as ColumnName,               
               cast(Old.KuldoRendszer as nvarchar(99)) as OldValue,
               cast(New.KuldoRendszer as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KuldoRendszer as nvarchar(max)),'') != ISNULL(CAST(New.KuldoRendszer as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'UzenetTipusa' as ColumnName,               
/*KCS*/          OldKT.Nev as OldValue,
/*KCS*/          NewKT.Nev as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.UzenetTipusa as nvarchar(max)),'') != ISNULL(CAST(New.UzenetTipusa as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
         left join KRT_KodCsoportok KCs on KCs.Kod = 'UZENET_TIPUS'
         left join KRT_KodTarak OldKT on OldKt.KodCsoport_Id = KCs.Id and OldKt.Kod COLLATE Hungarian_CI_AS = Old.UzenetTipusa and OldKt.Org=@Org and Old.HistoryVegrehajtasIdo between OldKt.ErvKezd and OldKt.ErvVege
         left join KRT_KodTarak NewKT on NewKT.KodCsoport_Id = KCs.Id and NewKT.Kod COLLATE Hungarian_CI_AS = New.UzenetTipusa and NewKt.Org=@Org and New.HistoryVegrehajtasIdo between NewKt.ErvKezd and NewKt.ErvVege      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FeladoTipusa' as ColumnName,               
               cast(Old.FeladoTipusa as nvarchar(99)) as OldValue,
               cast(New.FeladoTipusa as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FeladoTipusa as nvarchar(max)),'') != ISNULL(CAST(New.FeladoTipusa as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PartnerKapcsolatiKod' as ColumnName,               
               cast(Old.PartnerKapcsolatiKod as nvarchar(99)) as OldValue,
               cast(New.PartnerKapcsolatiKod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PartnerKapcsolatiKod as nvarchar(max)),'') != ISNULL(CAST(New.PartnerKapcsolatiKod as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PartnerNev' as ColumnName,               
               cast(Old.PartnerNev as nvarchar(99)) as OldValue,
               cast(New.PartnerNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PartnerNev as nvarchar(max)),'') != ISNULL(CAST(New.PartnerNev as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PartnerEmail' as ColumnName,               
               cast(Old.PartnerEmail as nvarchar(99)) as OldValue,
               cast(New.PartnerEmail as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PartnerEmail as nvarchar(max)),'') != ISNULL(CAST(New.PartnerEmail as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PartnerRovidNev' as ColumnName,               
               cast(Old.PartnerRovidNev as nvarchar(99)) as OldValue,
               cast(New.PartnerRovidNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PartnerRovidNev as nvarchar(max)),'') != ISNULL(CAST(New.PartnerRovidNev as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PartnerMAKKod' as ColumnName,               
               cast(Old.PartnerMAKKod as nvarchar(99)) as OldValue,
               cast(New.PartnerMAKKod as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PartnerMAKKod as nvarchar(max)),'') != ISNULL(CAST(New.PartnerMAKKod as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PartnerKRID' as ColumnName,               
               cast(Old.PartnerKRID as nvarchar(99)) as OldValue,
               cast(New.PartnerKRID as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PartnerKRID as nvarchar(max)),'') != ISNULL(CAST(New.PartnerKRID as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id' as ColumnName,               
               cast(Old.Partner_Id as nvarchar(99)) as OldValue,
               cast(New.Partner_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Partner_Id as nvarchar(max)),'') != ISNULL(CAST(New.Partner_Id as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Cim_Id' as ColumnName,               
               cast(Old.Cim_Id as nvarchar(99)) as OldValue,
               cast(New.Cim_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Cim_Id as nvarchar(max)),'') != ISNULL(CAST(New.Cim_Id as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_HivatkozasiSzam' as ColumnName,               
               cast(Old.KR_HivatkozasiSzam as nvarchar(99)) as OldValue,
               cast(New.KR_HivatkozasiSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_HivatkozasiSzam as nvarchar(max)),'') != ISNULL(CAST(New.KR_HivatkozasiSzam as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_ErkeztetesiSzam' as ColumnName,               
               cast(Old.KR_ErkeztetesiSzam as nvarchar(99)) as OldValue,
               cast(New.KR_ErkeztetesiSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_ErkeztetesiSzam as nvarchar(max)),'') != ISNULL(CAST(New.KR_ErkeztetesiSzam as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Contentum_HivatkozasiSzam' as ColumnName,               
               cast(Old.Contentum_HivatkozasiSzam as nvarchar(99)) as OldValue,
               cast(New.Contentum_HivatkozasiSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Contentum_HivatkozasiSzam as nvarchar(max)),'') != ISNULL(CAST(New.Contentum_HivatkozasiSzam as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PR_HivatkozasiSzam' as ColumnName,               
               cast(Old.PR_HivatkozasiSzam as nvarchar(99)) as OldValue,
               cast(New.PR_HivatkozasiSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PR_HivatkozasiSzam as nvarchar(max)),'') != ISNULL(CAST(New.PR_HivatkozasiSzam as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PR_ErkeztetesiSzam' as ColumnName,               
               cast(Old.PR_ErkeztetesiSzam as nvarchar(99)) as OldValue,
               cast(New.PR_ErkeztetesiSzam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PR_ErkeztetesiSzam as nvarchar(max)),'') != ISNULL(CAST(New.PR_ErkeztetesiSzam as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_DokTipusHivatal' as ColumnName,               
               cast(Old.KR_DokTipusHivatal as nvarchar(99)) as OldValue,
               cast(New.KR_DokTipusHivatal as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_DokTipusHivatal as nvarchar(max)),'') != ISNULL(CAST(New.KR_DokTipusHivatal as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_DokTipusAzonosito' as ColumnName,               
               cast(Old.KR_DokTipusAzonosito as nvarchar(99)) as OldValue,
               cast(New.KR_DokTipusAzonosito as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_DokTipusAzonosito as nvarchar(max)),'') != ISNULL(CAST(New.KR_DokTipusAzonosito as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_DokTipusLeiras' as ColumnName,               
               cast(Old.KR_DokTipusLeiras as nvarchar(99)) as OldValue,
               cast(New.KR_DokTipusLeiras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_DokTipusLeiras as nvarchar(max)),'') != ISNULL(CAST(New.KR_DokTipusLeiras as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Megjegyzes' as ColumnName,               
               cast(Old.KR_Megjegyzes as nvarchar(99)) as OldValue,
               cast(New.KR_Megjegyzes as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_Megjegyzes as nvarchar(max)),'') != ISNULL(CAST(New.KR_Megjegyzes as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_ErvenyessegiDatum' as ColumnName,               
               cast(Old.KR_ErvenyessegiDatum as nvarchar(99)) as OldValue,
               cast(New.KR_ErvenyessegiDatum as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_ErvenyessegiDatum as nvarchar(max)),'') != ISNULL(CAST(New.KR_ErvenyessegiDatum as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_ErkeztetesiDatum' as ColumnName,               
               cast(Old.KR_ErkeztetesiDatum as nvarchar(99)) as OldValue,
               cast(New.KR_ErkeztetesiDatum as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_ErkeztetesiDatum as nvarchar(max)),'') != ISNULL(CAST(New.KR_ErkeztetesiDatum as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_FileNev' as ColumnName,               
               cast(Old.KR_FileNev as nvarchar(99)) as OldValue,
               cast(New.KR_FileNev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_FileNev as nvarchar(max)),'') != ISNULL(CAST(New.KR_FileNev as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Kezbesitettseg' as ColumnName,               
               cast(Old.KR_Kezbesitettseg as nvarchar(99)) as OldValue,
               cast(New.KR_Kezbesitettseg as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_Kezbesitettseg as nvarchar(max)),'') != ISNULL(CAST(New.KR_Kezbesitettseg as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Idopecset' as ColumnName,               
               cast(Old.KR_Idopecset as nvarchar(99)) as OldValue,
               cast(New.KR_Idopecset as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_Idopecset as nvarchar(max)),'') != ISNULL(CAST(New.KR_Idopecset as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Valasztitkositas' as ColumnName,               
               cast(Old.KR_Valasztitkositas as nvarchar(99)) as OldValue,
               cast(New.KR_Valasztitkositas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_Valasztitkositas as nvarchar(max)),'') != ISNULL(CAST(New.KR_Valasztitkositas as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Valaszutvonal' as ColumnName,               
               cast(Old.KR_Valaszutvonal as nvarchar(99)) as OldValue,
               cast(New.KR_Valaszutvonal as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_Valaszutvonal as nvarchar(max)),'') != ISNULL(CAST(New.KR_Valaszutvonal as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Rendszeruzenet' as ColumnName,               
               cast(Old.KR_Rendszeruzenet as nvarchar(99)) as OldValue,
               cast(New.KR_Rendszeruzenet as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_Rendszeruzenet as nvarchar(max)),'') != ISNULL(CAST(New.KR_Rendszeruzenet as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Tarterulet' as ColumnName,               
               cast(Old.KR_Tarterulet as nvarchar(99)) as OldValue,
               cast(New.KR_Tarterulet as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_Tarterulet as nvarchar(max)),'') != ISNULL(CAST(New.KR_Tarterulet as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_ETertiveveny' as ColumnName,               
               cast(Old.KR_ETertiveveny as nvarchar(99)) as OldValue,
               cast(New.KR_ETertiveveny as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_ETertiveveny as nvarchar(max)),'') != ISNULL(CAST(New.KR_ETertiveveny as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Lenyomat' as ColumnName,               
               cast(Old.KR_Lenyomat as nvarchar(99)) as OldValue,
               cast(New.KR_Lenyomat as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_Lenyomat as nvarchar(max)),'') != ISNULL(CAST(New.KR_Lenyomat as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldKuldemeny_Id' as ColumnName,               
               cast(Old.KuldKuldemeny_Id as nvarchar(99)) as OldValue,
               cast(New.KuldKuldemeny_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KuldKuldemeny_Id as nvarchar(max)),'') != ISNULL(CAST(New.KuldKuldemeny_Id as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIrat_Id' as ColumnName,               
               cast(Old.IraIrat_Id as nvarchar(99)) as OldValue,
               cast(New.IraIrat_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IraIrat_Id as nvarchar(max)),'') != ISNULL(CAST(New.IraIrat_Id as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IratPeldany_Id' as ColumnName,               
               cast(Old.IratPeldany_Id as nvarchar(99)) as OldValue,
               cast(New.IratPeldany_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IratPeldany_Id as nvarchar(max)),'') != ISNULL(CAST(New.IratPeldany_Id as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Cel' as ColumnName,               
               cast(Old.Cel as nvarchar(99)) as OldValue,
               cast(New.Cel as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Cel as nvarchar(max)),'') != ISNULL(CAST(New.Cel as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'PR_Parameterek' as ColumnName,               
               cast(Old.PR_Parameterek as nvarchar(99)) as OldValue,
               cast(New.PR_Parameterek as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.PR_Parameterek as nvarchar(max)),'') != ISNULL(CAST(New.PR_Parameterek as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KR_Fiok' as ColumnName,               
               cast(Old.KR_Fiok as nvarchar(99)) as OldValue,
               cast(New.KR_Fiok as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eBeadvanyokHistory Old
         inner join EREC_eBeadvanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eBeadvanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KR_Fiok as nvarchar(max)),'') != ISNULL(CAST(New.KR_Fiok as nvarchar(max)),'')  
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end