/*
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_eBeadvanyokInsert')
            and   type = 'P')
   drop procedure sp_EREC_eBeadvanyokInsert
go
*/
create procedure sp_EREC_eBeadvanyokInsert    
                @Id      uniqueidentifier = null,    
                               @Irany     char(1)  = null,
                @Allapot     nvarchar(64)  = null,
                @KuldoRendszer     Nvarchar(400)  = null,
                @UzenetTipusa     nvarchar(64)  = null,
                @FeladoTipusa     int  = null,
                @PartnerKapcsolatiKod     Nvarchar(400)  = null,
                @PartnerNev     Nvarchar(400)  = null,
                @PartnerEmail     Nvarchar(400)  = null,
                @PartnerRovidNev     Nvarchar(400)  = null,
                @PartnerMAKKod     Nvarchar(400)  = null,
                @PartnerKRID     Nvarchar(400)  = null,
                @Partner_Id     uniqueidentifier  = null,
                @Cim_Id     uniqueidentifier  = null,
                @KR_HivatkozasiSzam     Nvarchar(400)  = null,
                @KR_ErkeztetesiSzam     Nvarchar(400)  = null,
                @Contentum_HivatkozasiSzam     uniqueidentifier  = null,
                @PR_HivatkozasiSzam     Nvarchar(400)  = null,
                @PR_ErkeztetesiSzam     Nvarchar(400)  = null,
                @KR_DokTipusHivatal     Nvarchar(400)  = null,
                @KR_DokTipusAzonosito     Nvarchar(400)  = null,
                @KR_DokTipusLeiras     Nvarchar(400)  = null,
                @KR_Megjegyzes     Nvarchar(4000)  = null,
                @KR_ErvenyessegiDatum     datetime  = null,
                @KR_ErkeztetesiDatum     datetime  = null,
                @KR_FileNev     Nvarchar(400)  = null,
                @KR_Kezbesitettseg     int  = null,
                @KR_Idopecset     Nvarchar(4000)  = null,
                @KR_Valasztitkositas     char(1)  = null,
                @KR_Valaszutvonal     int  = null,
                @KR_Rendszeruzenet     char(1)  = null,
                @KR_Tarterulet     int  = null,
                @KR_ETertiveveny     char(1)  = null,
                @KR_Lenyomat     Nvarchar(Max)  = null,
                @KuldKuldemeny_Id     uniqueidentifier  = null,
                @IraIrat_Id     uniqueidentifier  = null,
                @IratPeldany_Id     uniqueidentifier  = null,
                @Cel     Nvarchar(400)  = null,
                @PR_Parameterek     Nvarchar(Max)  = null,
                @KR_Fiok     Nvarchar(100)  = null,
                @FeldolgozasStatusz     int  = null,
                @FeldolgozasiHiba     Nvarchar(4000)  = null,
                @Ver     int  = null,
                @Note     Nvarchar(4000)  = null,
                @Stat_id     uniqueidentifier  = null,
                @ErvKezd     datetime  = null,
                @ErvVege     datetime  = null,
                @Letrehozo_id     uniqueidentifier  = null,
	            @LetrehozasIdo     datetime,
                @Modosito_id     uniqueidentifier  = null,
                @ModositasIdo     datetime  = null,
                @Zarolo_id     uniqueidentifier  = null,
                @ZarolasIdo     datetime  = null,
                @Tranz_id     uniqueidentifier  = null,
                @UIAccessLog_id     uniqueidentifier  = null,

		@UpdatedColumns              xml = null,
		@ResultUid            uniqueidentifier OUTPUT
         
 AS

BEGIN TRY
--BEGIN TRANSACTION InsertTransaction
 
set nocount on;
 
DECLARE @insertColumns NVARCHAR(4000)
DECLARE @insertValues NVARCHAR(4000)
SET @insertColumns = ''
SET @insertValues = '' 
       
         if @Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Id'
            SET @insertValues = @insertValues + ',@Id'
         end 
       
         if @Irany is not null
         begin
            SET @insertColumns = @insertColumns + ',Irany'
            SET @insertValues = @insertValues + ',@Irany'
         end 
       
         if @Allapot is not null
         begin
            SET @insertColumns = @insertColumns + ',Allapot'
            SET @insertValues = @insertValues + ',@Allapot'
         end 
       
         if @KuldoRendszer is not null
         begin
            SET @insertColumns = @insertColumns + ',KuldoRendszer'
            SET @insertValues = @insertValues + ',@KuldoRendszer'
         end 
       
         if @UzenetTipusa is not null
         begin
            SET @insertColumns = @insertColumns + ',UzenetTipusa'
            SET @insertValues = @insertValues + ',@UzenetTipusa'
         end 
       
         if @FeladoTipusa is not null
         begin
            SET @insertColumns = @insertColumns + ',FeladoTipusa'
            SET @insertValues = @insertValues + ',@FeladoTipusa'
         end 
       
         if @PartnerKapcsolatiKod is not null
         begin
            SET @insertColumns = @insertColumns + ',PartnerKapcsolatiKod'
            SET @insertValues = @insertValues + ',@PartnerKapcsolatiKod'
         end 
       
         if @PartnerNev is not null
         begin
            SET @insertColumns = @insertColumns + ',PartnerNev'
            SET @insertValues = @insertValues + ',@PartnerNev'
         end 
       
         if @PartnerEmail is not null
         begin
            SET @insertColumns = @insertColumns + ',PartnerEmail'
            SET @insertValues = @insertValues + ',@PartnerEmail'
         end 
       
         if @PartnerRovidNev is not null
         begin
            SET @insertColumns = @insertColumns + ',PartnerRovidNev'
            SET @insertValues = @insertValues + ',@PartnerRovidNev'
         end 
       
         if @PartnerMAKKod is not null
         begin
            SET @insertColumns = @insertColumns + ',PartnerMAKKod'
            SET @insertValues = @insertValues + ',@PartnerMAKKod'
         end 
       
         if @PartnerKRID is not null
         begin
            SET @insertColumns = @insertColumns + ',PartnerKRID'
            SET @insertValues = @insertValues + ',@PartnerKRID'
         end 
       
         if @Partner_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Partner_Id'
            SET @insertValues = @insertValues + ',@Partner_Id'
         end 
       
         if @Cim_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',Cim_Id'
            SET @insertValues = @insertValues + ',@Cim_Id'
         end 
       
         if @KR_HivatkozasiSzam is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_HivatkozasiSzam'
            SET @insertValues = @insertValues + ',@KR_HivatkozasiSzam'
         end 
       
         if @KR_ErkeztetesiSzam is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_ErkeztetesiSzam'
            SET @insertValues = @insertValues + ',@KR_ErkeztetesiSzam'
         end 
       
         if @Contentum_HivatkozasiSzam is not null
         begin
            SET @insertColumns = @insertColumns + ',Contentum_HivatkozasiSzam'
            SET @insertValues = @insertValues + ',@Contentum_HivatkozasiSzam'
         end 
       
         if @PR_HivatkozasiSzam is not null
         begin
            SET @insertColumns = @insertColumns + ',PR_HivatkozasiSzam'
            SET @insertValues = @insertValues + ',@PR_HivatkozasiSzam'
         end 
       
         if @PR_ErkeztetesiSzam is not null
         begin
            SET @insertColumns = @insertColumns + ',PR_ErkeztetesiSzam'
            SET @insertValues = @insertValues + ',@PR_ErkeztetesiSzam'
         end 
       
         if @KR_DokTipusHivatal is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_DokTipusHivatal'
            SET @insertValues = @insertValues + ',@KR_DokTipusHivatal'
         end 
       
         if @KR_DokTipusAzonosito is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_DokTipusAzonosito'
            SET @insertValues = @insertValues + ',@KR_DokTipusAzonosito'
         end 
       
         if @KR_DokTipusLeiras is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_DokTipusLeiras'
            SET @insertValues = @insertValues + ',@KR_DokTipusLeiras'
         end 
       
         if @KR_Megjegyzes is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_Megjegyzes'
            SET @insertValues = @insertValues + ',@KR_Megjegyzes'
         end 
       
         if @KR_ErvenyessegiDatum is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_ErvenyessegiDatum'
            SET @insertValues = @insertValues + ',@KR_ErvenyessegiDatum'
         end 
       
         if @KR_ErkeztetesiDatum is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_ErkeztetesiDatum'
            SET @insertValues = @insertValues + ',@KR_ErkeztetesiDatum'
         end 
       
         if @KR_FileNev is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_FileNev'
            SET @insertValues = @insertValues + ',@KR_FileNev'
         end 
       
         if @KR_Kezbesitettseg is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_Kezbesitettseg'
            SET @insertValues = @insertValues + ',@KR_Kezbesitettseg'
         end 
       
         if @KR_Idopecset is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_Idopecset'
            SET @insertValues = @insertValues + ',@KR_Idopecset'
         end 
       
         if @KR_Valasztitkositas is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_Valasztitkositas'
            SET @insertValues = @insertValues + ',@KR_Valasztitkositas'
         end 
       
         if @KR_Valaszutvonal is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_Valaszutvonal'
            SET @insertValues = @insertValues + ',@KR_Valaszutvonal'
         end 
       
         if @KR_Rendszeruzenet is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_Rendszeruzenet'
            SET @insertValues = @insertValues + ',@KR_Rendszeruzenet'
         end 
       
         if @KR_Tarterulet is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_Tarterulet'
            SET @insertValues = @insertValues + ',@KR_Tarterulet'
         end 
       
         if @KR_ETertiveveny is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_ETertiveveny'
            SET @insertValues = @insertValues + ',@KR_ETertiveveny'
         end 
       
         if @KR_Lenyomat is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_Lenyomat'
            SET @insertValues = @insertValues + ',@KR_Lenyomat'
         end 
       
         if @KuldKuldemeny_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',KuldKuldemeny_Id'
            SET @insertValues = @insertValues + ',@KuldKuldemeny_Id'
         end 
       
         if @IraIrat_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IraIrat_Id'
            SET @insertValues = @insertValues + ',@IraIrat_Id'
         end 
       
         if @IratPeldany_Id is not null
         begin
            SET @insertColumns = @insertColumns + ',IratPeldany_Id'
            SET @insertValues = @insertValues + ',@IratPeldany_Id'
         end 
       
         if @Cel is not null
         begin
            SET @insertColumns = @insertColumns + ',Cel'
            SET @insertValues = @insertValues + ',@Cel'
         end 
       
         if @PR_Parameterek is not null
         begin
            SET @insertColumns = @insertColumns + ',PR_Parameterek'
            SET @insertValues = @insertValues + ',@PR_Parameterek'
         end 
       
         if @KR_Fiok is not null
         begin
            SET @insertColumns = @insertColumns + ',KR_Fiok'
            SET @insertValues = @insertValues + ',@KR_Fiok'
         end 
       
         if @FeldolgozasStatusz is not null
         begin
            SET @insertColumns = @insertColumns + ',FeldolgozasStatusz'
            SET @insertValues = @insertValues + ',@FeldolgozasStatusz'
         end 
       
         if @FeldolgozasiHiba is not null
         begin
            SET @insertColumns = @insertColumns + ',FeldolgozasiHiba'
            SET @insertValues = @insertValues + ',@FeldolgozasiHiba'
         end 
       
         SET @insertColumns = @insertColumns + ',Ver'
         SET @insertValues = @insertValues + ',1'               
       
         if @Note is not null
         begin
            SET @insertColumns = @insertColumns + ',Note'
            SET @insertValues = @insertValues + ',@Note'
         end 
       
         if @Stat_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Stat_id'
            SET @insertValues = @insertValues + ',@Stat_id'
         end 
       
         if @ErvKezd is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvKezd'
            SET @insertValues = @insertValues + ',@ErvKezd'
         end 
       
         if @ErvVege is not null
         begin
            SET @insertColumns = @insertColumns + ',ErvVege'
            SET @insertValues = @insertValues + ',@ErvVege'
         end 
       
         if @Letrehozo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Letrehozo_id'
            SET @insertValues = @insertValues + ',@Letrehozo_id'
         end 
       
         if @LetrehozasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',LetrehozasIdo'
            SET @insertValues = @insertValues + ',@LetrehozasIdo'
         end 
       
         if @Modosito_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Modosito_id'
            SET @insertValues = @insertValues + ',@Modosito_id'
         end 
       
         if @ModositasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ModositasIdo'
            SET @insertValues = @insertValues + ',@ModositasIdo'
         end 
       
         if @Zarolo_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Zarolo_id'
            SET @insertValues = @insertValues + ',@Zarolo_id'
         end 
       
         if @ZarolasIdo is not null
         begin
            SET @insertColumns = @insertColumns + ',ZarolasIdo'
            SET @insertValues = @insertValues + ',@ZarolasIdo'
         end 
       
         if @Tranz_id is not null
         begin
            SET @insertColumns = @insertColumns + ',Tranz_id'
            SET @insertValues = @insertValues + ',@Tranz_id'
         end 
       
         if @UIAccessLog_id is not null
         begin
            SET @insertColumns = @insertColumns + ',UIAccessLog_id'
            SET @insertValues = @insertValues + ',@UIAccessLog_id'
         end    
   
IF CHARINDEX(',', @insertColumns) = 1
   SET @insertColumns = SUBSTRING(@insertColumns, 2, LEN(@insertColumns) -1 )
   
IF CHARINDEX(',', @insertValues) = 1
   SET @insertValues = SUBSTRING(@insertValues, 2, LEN(@insertValues) -1 )
     

DECLARE @InsertCommand NVARCHAR(4000)

SET @InsertCommand = 'DECLARE @InsertedRow TABLE (id uniqueidentifier)
'
SET @InsertCommand = @InsertCommand + 'insert into EREC_eBeadvanyok ('+@insertColumns+') output inserted.id into @InsertedRow values ('+@insertValues+')
'
SET @InsertCommand = @InsertCommand + 'select @ResultUid = id from @InsertedRow'

exec sp_executesql @InsertCommand, 
                             N'@Id uniqueidentifier,@Irany char(1),@Allapot nvarchar(64),@KuldoRendszer Nvarchar(400),@UzenetTipusa nvarchar(64),@FeladoTipusa int,@PartnerKapcsolatiKod Nvarchar(400),@PartnerNev Nvarchar(400),@PartnerEmail Nvarchar(400),@PartnerRovidNev Nvarchar(400),@PartnerMAKKod Nvarchar(400),@PartnerKRID Nvarchar(400),@Partner_Id uniqueidentifier,@Cim_Id uniqueidentifier,@KR_HivatkozasiSzam Nvarchar(400),@KR_ErkeztetesiSzam Nvarchar(400),@Contentum_HivatkozasiSzam uniqueidentifier,@PR_HivatkozasiSzam Nvarchar(400),@PR_ErkeztetesiSzam Nvarchar(400),@KR_DokTipusHivatal Nvarchar(400),@KR_DokTipusAzonosito Nvarchar(400),@KR_DokTipusLeiras Nvarchar(400),@KR_Megjegyzes Nvarchar(4000),@KR_ErvenyessegiDatum datetime,@KR_ErkeztetesiDatum datetime,@KR_FileNev Nvarchar(400),@KR_Kezbesitettseg int,@KR_Idopecset Nvarchar(4000),@KR_Valasztitkositas char(1),@KR_Valaszutvonal int,@KR_Rendszeruzenet char(1),@KR_Tarterulet int,@KR_ETertiveveny char(1),@KR_Lenyomat Nvarchar(Max),@KuldKuldemeny_Id uniqueidentifier,@IraIrat_Id uniqueidentifier,@IratPeldany_Id uniqueidentifier,@Cel Nvarchar(400),@PR_Parameterek Nvarchar(Max),@KR_Fiok Nvarchar(100),@FeldolgozasStatusz int,@FeldolgozasiHiba Nvarchar(4000),@Ver int,@Note Nvarchar(4000),@Stat_id uniqueidentifier,@ErvKezd datetime,@ErvVege datetime,@Letrehozo_id uniqueidentifier,@LetrehozasIdo datetime,@Modosito_id uniqueidentifier,@ModositasIdo datetime,@Zarolo_id uniqueidentifier,@ZarolasIdo datetime,@Tranz_id uniqueidentifier,@UIAccessLog_id uniqueidentifier,@ResultUid uniqueidentifier OUTPUT'
,@Id = @Id,@Irany = @Irany,@Allapot = @Allapot,@KuldoRendszer = @KuldoRendszer,@UzenetTipusa = @UzenetTipusa,@FeladoTipusa = @FeladoTipusa,@PartnerKapcsolatiKod = @PartnerKapcsolatiKod,@PartnerNev = @PartnerNev,@PartnerEmail = @PartnerEmail,@PartnerRovidNev = @PartnerRovidNev,@PartnerMAKKod = @PartnerMAKKod,@PartnerKRID = @PartnerKRID,@Partner_Id = @Partner_Id,@Cim_Id = @Cim_Id,@KR_HivatkozasiSzam = @KR_HivatkozasiSzam,@KR_ErkeztetesiSzam = @KR_ErkeztetesiSzam,@Contentum_HivatkozasiSzam = @Contentum_HivatkozasiSzam,@PR_HivatkozasiSzam = @PR_HivatkozasiSzam,@PR_ErkeztetesiSzam = @PR_ErkeztetesiSzam,@KR_DokTipusHivatal = @KR_DokTipusHivatal,@KR_DokTipusAzonosito = @KR_DokTipusAzonosito,@KR_DokTipusLeiras = @KR_DokTipusLeiras,@KR_Megjegyzes = @KR_Megjegyzes,@KR_ErvenyessegiDatum = @KR_ErvenyessegiDatum,@KR_ErkeztetesiDatum = @KR_ErkeztetesiDatum,@KR_FileNev = @KR_FileNev,@KR_Kezbesitettseg = @KR_Kezbesitettseg,@KR_Idopecset = @KR_Idopecset,@KR_Valasztitkositas = @KR_Valasztitkositas,@KR_Valaszutvonal = @KR_Valaszutvonal,@KR_Rendszeruzenet = @KR_Rendszeruzenet,@KR_Tarterulet = @KR_Tarterulet,@KR_ETertiveveny = @KR_ETertiveveny,@KR_Lenyomat = @KR_Lenyomat,@KuldKuldemeny_Id = @KuldKuldemeny_Id,@IraIrat_Id = @IraIrat_Id,@IratPeldany_Id = @IratPeldany_Id,@Cel = @Cel,@PR_Parameterek = @PR_Parameterek,@KR_Fiok = @KR_Fiok,@FeldolgozasStatusz = @FeldolgozasStatusz,@FeldolgozasiHiba = @FeldolgozasiHiba,@Ver = @Ver,@Note = @Note,@Stat_id = @Stat_id,@ErvKezd = @ErvKezd,@ErvVege = @ErvVege,@Letrehozo_id = @Letrehozo_id,@LetrehozasIdo = @LetrehozasIdo,@Modosito_id = @Modosito_id,@ModositasIdo = @ModositasIdo,@Zarolo_id = @Zarolo_id,@ZarolasIdo = @ZarolasIdo,@Tranz_id = @Tranz_id,@UIAccessLog_id = @UIAccessLog_id ,@ResultUid = @ResultUid OUTPUT


if @@error != 0
BEGIN
   RAISERROR('[50301]',16,1)
END
ELSE
BEGIN
   /* History Log */
   exec sp_LogRecordToHistory 'EREC_eBeadvanyok',@ResultUid
					,'EREC_eBeadvanyokHistory',0,@Letrehozo_id,@LetrehozasIdo
END            
--COMMIT TRANSACTION InsertTransaction
   
END TRY
BEGIN CATCH
   --IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION InsertTransaction
   
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH
go

/* használt errorcode-ok:
	50301:
	50302: 'Org' oszlop értéke nem lehet NULL
*/
