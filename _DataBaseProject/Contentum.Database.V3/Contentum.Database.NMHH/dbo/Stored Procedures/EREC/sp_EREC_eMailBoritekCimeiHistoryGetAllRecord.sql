﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_eMailBoritekCimeiHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_EREC_eMailBoritekCimeiHistoryGetAllRecord
go
*/
create procedure sp_EREC_eMailBoritekCimeiHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_eMailBoritekCimeiHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekCimeiHistory Old
         inner join EREC_eMailBoritekCimeiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekCimeiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'eMailBoritek_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_eMailBoritekokAzonosito(Old.eMailBoritek_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_eMailBoritekokAzonosito(New.eMailBoritek_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekCimeiHistory Old
         inner join EREC_eMailBoritekCimeiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekCimeiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.eMailBoritek_Id as nvarchar(max)),'') != ISNULL(CAST(New.eMailBoritek_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join EREC_eMailBoritekok FTOld on FTOld.Id = Old.eMailBoritek_Id --and FTOld.Ver = Old.Ver
         left join EREC_eMailBoritekok FTNew on FTNew.Id = New.eMailBoritek_Id --and FTNew.Ver = New.Ver      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Sorszam' as ColumnName,               cast(Old.Sorszam as nvarchar(99)) as OldValue,
               cast(New.Sorszam as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekCimeiHistory Old
         inner join EREC_eMailBoritekCimeiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekCimeiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Sorszam as nvarchar(max)),'') != ISNULL(CAST(New.Sorszam as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tipus' as ColumnName,               cast(Old.Tipus as nvarchar(99)) as OldValue,
               cast(New.Tipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekCimeiHistory Old
         inner join EREC_eMailBoritekCimeiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekCimeiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tipus as nvarchar(max)),'') != ISNULL(CAST(New.Tipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'MailCim' as ColumnName,               cast(Old.MailCim as nvarchar(99)) as OldValue,
               cast(New.MailCim as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekCimeiHistory Old
         inner join EREC_eMailBoritekCimeiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekCimeiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.MailCim as nvarchar(max)),'') != ISNULL(CAST(New.MailCim as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Partner_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(Old.Partner_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_PartnerekAzonosito(New.Partner_Id) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekCimeiHistory Old
         inner join EREC_eMailBoritekCimeiHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekCimeiHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Partner_Id as nvarchar(max)),'') != ISNULL(CAST(New.Partner_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id         left join KRT_Partnerek FTOld on FTOld.Id = Old.Partner_Id --and FTOld.Ver = Old.Ver
         left join KRT_Partnerek FTNew on FTNew.Id = New.Partner_Id --and FTNew.Ver = New.Ver      
)
            
end
go