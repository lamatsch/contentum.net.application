﻿create procedure [dbo].[sp_EREC_eMailBoritekCsatolmanyokGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_eMailBoritekCsatolmanyok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier
as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(4000)
   SET @sqlcmd = ''

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
-- A PKI_INTEGRACIO rendszerparameter erteketol fuggoen
-- eltero kodcsoport szerint dolgozunk az elektronikus
-- alairas kapcsan
DECLARE @kcsDokumentumAlairas nvarchar(64)

DECLARE @paramPKI_Integracio nvarchar(400)

SET @paramPKI_Integracio = (
	SELECT Ertek FROM KRT_Parameterek
	WHERE Nev ='PKI_INTEGRACIO'
	AND Org=@Org
)

IF @paramPKI_Integracio = 'Igen'
BEGIN
	SET @kcsDokumentumAlairas = 'DOKUMENTUM_ALAIRAS_PKI'
END
ELSE
BEGIN
	SET @kcsDokumentumAlairas = 'DOKUMENTUM_ALAIRAS_MANUALIS'
END


	/************************************************************
	* Bizalmas iratok orzo szerinti szuréséhez csoporttagok lekérése*
	************************************************************/
--	--IF dbo.fn_IsAdmin(@ExecutorUserId) = 0
--	IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
--	begin
----	set @sqlcmd = N'declare @iratok_executor table (Id uniqueidentifier)
----insert into @iratok_executor select Id from dbo.fn_GetAllIrat_ExecutorIsOrzoOrHisLeader(
----			''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
----			''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
----'
--	set @sqlcmd = @sqlcmd + N'; declare @iratok_executor table (Id uniqueidentifier primary key)
--insert into @iratok_executor select Id from dbo.fn_GetAllConfidentialIrat_ExecutorIsOrzoOrHisLeader(
--			''' + CAST(@ExecutorUserId AS CHAR(36)) + ''',
--			''' + CAST( @FelhasznaloSzervezet_Id as CHAR(36)) + ''')
--'
--	end

  SET @sqlcmd = @sqlcmd + 
  'select ' + @LocalTopRow + '
  	   EREC_eMailBoritekCsatolmanyok.Id as Id
	into #filter
   from 
     EREC_eMailBoritekCsatolmanyok as EREC_eMailBoritekCsatolmanyok,
KRT_Dokumentumok as KRT_Dokumentumok
where EREC_eMailBoritekCsatolmanyok.Dokumentum_Id = KRT_Dokumentumok.Id
           '
	if (select count(Id) from KRT_Orgok) > 1
	begin
		SET @sqlcmd = @sqlcmd + ' and EREC_eMailBoritekCsatolmanyok.Letrehozo_id in (select Id from KRT_Felhasznalok where Org=''' + cast(@Org as NVarChar(40)) + ''')
'
	end

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
	SET @sqlcmd = @sqlcmd + @OrderBy

	-- bizalmas irat és végrehajtó felhasználó nem az orzo vagy annak vezetoje
	--IF dbo.fn_IsAdmin(@ExecutorUserId) = 0
	IF dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
	BEGIN
--	   SET @sqlcmd = @sqlcmd + '; DELETE FROM #filter where #filter.Id in
--	(SELECT EREC_eMailBoritekCsatolmanyok.Id
--	FROM #filter
--	inner join EREC_eMailBoritekCsatolmanyok on #filter.Id = EREC_eMailBoritekCsatolmanyok.Id
--	inner join EREC_eMailBoritekok on EREC_eMailBoritekCsatolmanyok.eMailBoritek_Id = EREC_eMailBoritekok.Id
--	left join EREC_KuldKuldemenyek on EREC_eMailBoritekok.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id and EREC_eMailBoritekok.IraIrat_Id is null
--	left join EREC_IraIratok as EREC_IraIratok_Kuldemeny on EREC_KuldKuldemenyek.Id = EREC_IraIratok_Kuldemeny.KuldKuldemenyek_Id
--	left join EREC_IraIratok on EREC_eMailBoritekok.IraIrat_Id = EREC_IraIratok.Id
--	where (dbo.fn_CheckIrat_IsConfidential(EREC_IraIratok.Id) = 1
--		and EREC_IraIratok.Id not in (select Id from @iratok_executor))
--	or
--		(dbo.fn_CheckIrat_IsConfidential(EREC_IraIratok_Kuldemeny.Id) = 1
--		and EREC_IraIratok_Kuldemeny.Id not in (select Id from @iratok_executor))
--	);
--	'

		SET @sqlcmd = @sqlcmd + ';DECLARE @csoporttagsag_tipus nvarchar(64)
	SET @csoporttagsag_tipus = (select top 1 KRT_CsoportTagok.Tipus from KRT_CsoportTagok
						where KRT_CsoportTagok.Csoport_Id_Jogalany=''' + CAST(@ExecutorUserId as CHAR(36)) + '''
						and KRT_CsoportTagok.Csoport_Id=''' + CAST(@FelhasznaloSzervezet_Id as CHAR(36)) + '''
						and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
						)

	; CREATE TABLE #jogosultak (Id uniqueidentifier)
	IF @csoporttagsag_tipus = ''3''
	BEGIN
		INSERT INTO #jogosultak select KRT_Csoportok.Id from KRT_Csoportok
							inner join KRT_Csoporttagok on KRT_Csoporttagok.Csoport_Id =''' + CAST(@FelhasznaloSzervezet_Id as CHAR(36)) + ''' 
							and KRT_Csoportok.Tipus = ''1'' and KRT_Csoportok.Id = KRT_CsoportTagok.Csoport_Id_Jogalany
							where getdate() between KRT_Csoportok.ErvKezd and KRT_Csoportok.ErvVege 
							and getdate() between KRT_CsoportTagok.ErvKezd and KRT_CsoportTagok.ErvVege
	END
	ELSE
	BEGIN
		INSERT INTO #jogosultak select ''' + CAST(@ExecutorUserId as CHAR(36)) + '''
	END
'

		SET @sqlcmd = @sqlcmd + '
	DECLARE @confidential varchar(4000)
	set @confidential = (select top 1 Ertek from KRT_Parameterek where Nev=''IRAT_MINOSITES_BIZALMAS''
							and Org=''' + CAST(@Org as CHAR(36)) + ''' and getdate() between ErvKezd and ErvVege)

	declare @Bizalmas table (val nvarchar(64) collate Hungarian_CS_AS )
	insert into @Bizalmas (val) (select Value from fn_Split(@confidential, '',''));
'
--		SET @sqlcmd = @sqlcmd + 'DELETE FROM #filter WHERE #filter.Id IN
--	(SELECT EREC_eMailBoritekCsatolmanyok.Id
--	FROM #filter
--	inner join EREC_eMailBoritekCsatolmanyok on #filter.Id = EREC_eMailBoritekCsatolmanyok.Id
--	inner join EREC_eMailBoritekok on EREC_eMailBoritekCsatolmanyok.eMailBoritek_Id = EREC_eMailBoritekok.Id
--	inner join EREC_KuldKuldemenyek on EREC_eMailBoritekok.KuldKuldemeny_Id = EREC_KuldKuldemenyek.Id and EREC_eMailBoritekok.IraIrat_Id is null
--	WHERE EREC_KuldKuldemenyek.Minosites in (select val from @Bizalmas)
--	AND NOT EXISTS
--			(select 1
--				from KRT_Jogosultak where
--					KRT_Jogosultak.Obj_Id=EREC_KuldKuldemenyek.Id
--					and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
--					and @ExecutorUserId = KRT_Jogosultak.Csoport_Id_Jogalany)
--	AND EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo NOT IN (select Id from #jogosultak)
--);
--'
--
--		SET @sqlcmd = @sqlcmd + 'DELETE FROM #filter WHERE #filter.Id IN
--	(SELECT EREC_eMailBoritekCsatolmanyok.Id
--	FROM #filter
--	inner join EREC_eMailBoritekCsatolmanyok on #filter.Id = EREC_eMailBoritekCsatolmanyok.Id
--	inner join EREC_eMailBoritekok on EREC_eMailBoritekCsatolmanyok.eMailBoritek_Id = EREC_eMailBoritekok.Id
--	inner join EREC_IraIratok on EREC_eMailBoritekok.IraIrat_Id = EREC_IraIratok.Id
--	WHERE EREC_IraIratok.Minosites in (select val from @Bizalmas)
--	AND NOT EXISTS
--			(select 1
--				from KRT_Jogosultak where
--					KRT_Jogosultak.Obj_Id=EREC_IraIratok.Id
--					and getdate() between KRT_Jogosultak.ErvKezd and KRT_Jogosultak.ErvVege
--					and @ExecutorUserId = KRT_Jogosultak.Csoport_Id_Jogalany)
--	AND NOT EXISTS
--		(select 1
--			from EREC_PldIratPeldanyok
--			where EREC_PldIratPeldanyok.IraIrat_Id=EREC_IraIratok.Id
--				and EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo in (select Id from #jogosultak)
--		);
--'
	END

  SET @sqlcmd = @sqlcmd + '
select ' + @LocalTopRow + '
  	   EREC_eMailBoritekCsatolmanyok.Id,
	   EREC_eMailBoritekCsatolmanyok.eMailBoritek_Id,
	   EREC_eMailBoritekCsatolmanyok.Dokumentum_Id,
KRT_Dokumentumok.External_Link,
KRT_Dokumentumok.FajlNev,
KRT_Dokumentumok.Megnyithato,
KRT_Dokumentumok.Olvashato,
KRT_Dokumentumok.ElektronikusAlairas,
dbo.fn_KodtarErtekNeve(''' + @kcsDokumentumAlairas +
	''', KRT_Dokumentumok.ElektronikusAlairas,''' + CAST(@Org as NVarChar(40)) + ''') as ElektronikusAlairasNev,
KRT_Dokumentumok.AlairasFelulvizsgalat,
KRT_Dokumentumok.Titkositas,
KRT_Dokumentumok.Tipus,
KRT_Dokumentumok.Formatum,
	   EREC_eMailBoritekCsatolmanyok.Nev,
	   EREC_eMailBoritekCsatolmanyok.Tomoritve,
	   EREC_eMailBoritekCsatolmanyok.Ver,
	   EREC_eMailBoritekCsatolmanyok.Note,
	   EREC_eMailBoritekCsatolmanyok.Stat_id,
	   EREC_eMailBoritekCsatolmanyok.ErvKezd,
	   EREC_eMailBoritekCsatolmanyok.ErvVege,
	   EREC_eMailBoritekCsatolmanyok.Letrehozo_id,
	   EREC_eMailBoritekCsatolmanyok.LetrehozasIdo,
	   EREC_eMailBoritekCsatolmanyok.Modosito_id,
	   EREC_eMailBoritekCsatolmanyok.ModositasIdo,
	   EREC_eMailBoritekCsatolmanyok.Zarolo_id,
	   EREC_eMailBoritekCsatolmanyok.ZarolasIdo,
	   EREC_eMailBoritekCsatolmanyok.Tranz_id,
	   EREC_eMailBoritekCsatolmanyok.UIAccessLog_id  
   from 
     EREC_eMailBoritekCsatolmanyok as EREC_eMailBoritekCsatolmanyok
inner join #filter on #filter.Id = EREC_eMailBoritekCsatolmanyok.Id
inner join KRT_Dokumentumok as KRT_Dokumentumok
on EREC_eMailBoritekCsatolmanyok.Dokumentum_Id = KRT_Dokumentumok.Id
           '
-- 
--    if @Where is not null and @Where!=''
--	begin 
--		SET @sqlcmd = @sqlcmd + ' and ' + @Where
--	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy
  
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end