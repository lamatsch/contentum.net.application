﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_eMailBoritekCsatolmanyokHistoryGetRecord')
            and   type = 'P')
   drop procedure sp_EREC_eMailBoritekCsatolmanyokHistoryGetRecord
go
*/
create procedure sp_EREC_eMailBoritekCsatolmanyokHistoryGetRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime',
	@RecordId		uniqueidentifier
		

as
begin

declare @Org uniqueidentifier
set @Org = (select U.Org
      from EREC_eMailBoritekCsatolmanyokHistory h
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and h.Id = @RecordId)

select * from
   (
   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekCsatolmanyokHistory 
         inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id
      where HistoryMuvelet_Id = 0 and EREC_eMailBoritekCsatolmanyokHistory.Id = @RecordId      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               
               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekCsatolmanyokHistory Old
         inner join EREC_eMailBoritekCsatolmanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekCsatolmanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'eMailBoritek_Id' as ColumnName,
/*FK*/           dbo.fn_GetEREC_eMailBoritekokAzonosito(Old.eMailBoritek_Id) as OldValue,
/*FK*/           dbo.fn_GetEREC_eMailBoritekokAzonosito(New.eMailBoritek_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekCsatolmanyokHistory Old
         inner join EREC_eMailBoritekCsatolmanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekCsatolmanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.eMailBoritek_Id as nvarchar(max)),'') != ISNULL(CAST(New.eMailBoritek_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join EREC_eMailBoritekok FTOld on FTOld.Id = Old.eMailBoritek_Id and FTOld.Ver = Old.Ver
         left join EREC_eMailBoritekok FTNew on FTNew.Id = New.eMailBoritek_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Dokumentum_Id' as ColumnName,
/*FK*/           dbo.fn_GetKRT_DokumentumokAzonosito(Old.Dokumentum_Id) as OldValue,
/*FK*/           dbo.fn_GetKRT_DokumentumokAzonosito(New.Dokumentum_Id) as NewValue,
               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekCsatolmanyokHistory Old
         inner join EREC_eMailBoritekCsatolmanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekCsatolmanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Dokumentum_Id as nvarchar(max)),'') != ISNULL(CAST(New.Dokumentum_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id
         left join KRT_Dokumentumok FTOld on FTOld.Id = Old.Dokumentum_Id and FTOld.Ver = Old.Ver
         left join KRT_Dokumentumok FTNew on FTNew.Id = New.Dokumentum_Id and FTNew.Ver = New.Ver      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Nev' as ColumnName,               
               cast(Old.Nev as nvarchar(99)) as OldValue,
               cast(New.Nev as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekCsatolmanyokHistory Old
         inner join EREC_eMailBoritekCsatolmanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekCsatolmanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Nev as nvarchar(max)),'') != ISNULL(CAST(New.Nev as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Tomoritve' as ColumnName,               
               cast(Old.Tomoritve as nvarchar(99)) as OldValue,
               cast(New.Tomoritve as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekCsatolmanyokHistory Old
         inner join EREC_eMailBoritekCsatolmanyokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekCsatolmanyokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Tomoritve as nvarchar(max)),'') != ISNULL(CAST(New.Tomoritve as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
      where Old.Id = @RecordId)   ) t order by t.Ver ASC
end
go