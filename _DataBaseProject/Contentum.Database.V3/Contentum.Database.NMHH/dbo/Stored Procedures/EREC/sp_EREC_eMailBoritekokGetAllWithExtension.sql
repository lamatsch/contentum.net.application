﻿CREATE procedure [dbo].[sp_EREC_eMailBoritekokGetAllWithExtension]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_eMailBoritekok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @FelhasznaloSzervezet_Id uniqueidentifier,
  @Jogosultak	char(1) = '0',
  @pageNumber		int = 0,
  @pageSize			int = -1,
  @SelectedRowId	uniqueidentifier = NULL

as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end
   
   DECLARE @sqlcmd nvarchar(max)
   
   DECLARE @LocalTopRow nvarchar(10)
   declare @firstRow int
   declare @lastRow int

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
		if (@pageSize > @TopRow)
			SET @pageSize = @TopRow;
       SET @LocalTopRow = ' TOP ' + @TopRow
     END

	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = (@pageNumber)*@pageSize + 1
		set @lastRow = @firstRow + @pageSize - 1
	end
	else begin
		if (@TopRow = '' or @TopRow = '0')
		begin
			set @firstRow = 1
			set @lastRow = (SELECT TOP (1) Ertek FROM KRT_Parameterek WHERE Nev = 'LISTA_ELEMSZAM_MAX')
		end 
		else
		begin
			set @firstRow = 1
			set @lastRow = @TopRow
		end	
	END
     

  SET @sqlcmd = 
  ';with CsoportTagokAll as
	(
		select Id from dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId, @FelhasznaloSzervezet_Id)
	),
	jogosult_objektumok as
	(
		select krt_jogosultak.Obj_Id from krt_jogosultak
			INNER JOIN CsoportTagokAll 
		ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
	),
	mappa_tartalmak as
	(
		SELECT KRT_MappaTartalmak.Obj_Id 
		FROM KRT_MappaTartalmak
		INNER JOIN KRT_Mappak ON KRT_Mappak.Id = KRT_MappaTartalmak.Mappa_Id
		INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = KRT_Mappak.Id
		where getdate() between KRT_MappaTartalmak.Ervkezd and KRT_MappaTartalmak.ErvVege
		and getdate() between KRT_Mappak.Ervkezd and KRT_Mappak.ErvVege
	)
	select
	row_number() over('+@OrderBy+') as RowNumber, 
	EREC_eMailBoritekok.Id 
	into #result
	from EREC_eMailBoritekok
	LEFT JOIN KRT_KodCsoportok as FontossagKodCsoport on FontossagKodCsoport.Kod=''EMAILBORITEK_FONTOSSAG''
    LEFT JOIN KRT_KodTarak FontossagKodTarak on EREC_eMailBoritekok.Fontossag = FontossagKodTarak.Kod and FontossagKodCsoport.Id = FontossagKodTarak.KodCsoport_Id and FontossagKodTarak.Org=@Org
	LEFT JOIN EREC_IraIratok as EREC_IraIratok_Irat
	on EREC_IraIratok_Irat.Allapot != ''06'' and EREC_IraIratok_Irat.Allapot != ''08''
	and EREC_eMailBoritekok.IraIrat_Id is not null and EREC_eMailBoritekok.IraIrat_Id=EREC_IraIratok_Irat.Id
	LEFT JOIN EREC_IraIratok as EREC_IraIratok_Kuld
	on EREC_IraIratok_Kuld.Allapot != ''06'' and EREC_IraIratok_Kuld.Allapot != ''08''
	and EREC_eMailBoritekok.IraIrat_Id is null and EREC_eMailBoritekok.KuldKuldemeny_Id is not null
    and EREC_IraIratok_Kuld.KuldKuldemenyek_Id=EREC_eMailBoritekok.KuldKuldemeny_Id
'
	IF @Jogosultak = 1 AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
	BEGIN
		set @sqlcmd = @sqlcmd + N' INNER JOIN ( SELECT DISTINCT Id FROM					
						(
							SELECT EREC_eMailBoritekok.Id from EREC_eMailBoritekok
								INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_eMailBoritekok.Id
							UNION ALL
							SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId,@FelhasznaloSzervezet_Id, ''EREC_eMailBoritekok'')
							UNION ALL
							SELECT EREC_eMailBoritekok.Id from EREC_eMailBoritekok
								WHERE EREC_eMailBoritekok.KuldKuldemeny_Id in
								(
									SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
										INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_KuldKuldemenyek.Id
									UNION ALL
									SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
										INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
									UNION ALL
									SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
										INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_KuldKuldemenyek.IraIratok_Id
									UNION ALL
									SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
										INNER JOIN CsoportTagokAll ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.Csoport_Id_Felelos
									UNION ALL
									SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
										INNER JOIN CsoportTagokAll ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
									UNION ALL
									SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
										INNER JOIN mappa_tartalmak ON mappa_tartalmak.Obj_Id = EREC_KuldKuldemenyek.Id
									UNION ALL
									SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
									INNER JOIN EREC_IraIratok ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id
									WHERE EREC_IraIratok.Id IN
										(
											SELECT EREC_IraIratok.Id FROM EREC_IraIratok
											WHERE EREC_IraIratok.Ugyirat_Id IN 
											(
												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
													INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,  @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
												UNION ALL
												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
													INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,  @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
												UNION ALL
												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,  @FelhasznaloSzervezet_Id) as CsoportTagokAll 
														ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
												UNION ALL
												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,  @FelhasznaloSzervezet_Id) as CsoportTagokAll 
														ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
												UNION ALL
												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
													INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,  @FelhasznaloSzervezet_Id) as CsoportTagokAll 
														ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
												UNION ALL
												SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId,  @FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'')
												UNION ALL
												SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
													INNER JOIN mappa_tartalmak ON mappa_tartalmak.Obj_Id = erec_ugyugyiratok.Id
											)
											UNION ALL --CR2407: lenézünk a példányba, ha látja az iratot, akkor a küldeményt is lássa
											SELECT EREC_IraIratok.Id FROM EREC_IraIratok
											INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = EREC_IraIratok.Id
											WHERE EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo = @ExecutorUserId and EREC_PldIratpeldanyok.Csoport_Id_Felelos = @ExecutorUserId
										)	
									UNION ALL
									SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId,  @FelhasznaloSzervezet_Id, ''EREC_KuldKuldemenyek'')
								 )
								 ) t
							)	 AS JogosultTetelek ON JogosultTetelek.Id = EREC_eMailBoritekok.Id 
							--Kimeno email	 
							--UNION ALL
							--SELECT EREC_eMailBoritekok.Id from EREC_eMailBoritekok
							--	WHERE EREC_eMailBoritekok.IraIrat_Id in
							--	(
							--		SELECT EREC_IraIratok.Id FROM EREC_IraIratok
							--			INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_IraIratok.Id
							--		UNION ALL
							--		SELECT EREC_IraIratok.Id FROM EREC_IraIratok
							--			WHERE EREC_IraIratok.Ugyirat_Id IN 
							--			(
							--				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
							--					INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = erec_ugyugyiratok.Id
							--				UNION ALL
							--				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
							--					INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
							--				UNION ALL
							--				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
							--					INNER JOIN CsoportTagokAll 
							--						ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
							--				UNION ALL
							--				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
							--					INNER JOIN CsoportTagokAll 
							--						ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
							--				UNION ALL
							--				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
							--					INNER JOIN CsoportTagokAll 
							--						ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
							--				UNION ALL
							--				SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'')
							--				UNION ALL
							--				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
							--					INNER JOIN mappa_tartalmak ON mappa_tartalmak.Obj_Id = erec_ugyugyiratok.Id
							--			)
							--		UNION ALL --CR1716: lenézünk a példányba
							--		SELECT EREC_IraIratok.Id FROM EREC_IraIratok
							--			INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = EREC_IraIratok.Id
							--			WHERE EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo = @ExecutorUserId and EREC_PldIratpeldanyok.Csoport_Id_Felelos = @ExecutorUserId
							--		UNION ALL
							--		SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_IraIratok'')

							--	)	 
							--)	 
							 ';
	END
SET @sqlcmd = @sqlcmd +'
	where EREC_eMailBoritekok.Letrehozo_id in (Select Id from KRT_Felhasznalok where Org = @Org)
'

    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' and ' + @Where
	end
     
    --if @Jogosultak = 1 AND dbo.fn_IsAdmin(@ExecutorUserId) = 0
	--IF @Jogosultak = 1 AND dbo.fn_IsAdminInSzervezet(@ExecutorUserId,@FelhasznaloSzervezet_Id) = 0
	--BEGIN
	--	set @sqlcmd = @sqlcmd + N' AND EREC_eMailBoritekok.Id IN 
	--					(
	--						SELECT EREC_eMailBoritekok.Id from EREC_eMailBoritekok
	--							INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_eMailBoritekok.Id
	--						UNION ALL
	--						SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId,@FelhasznaloSzervezet_Id, ''EREC_eMailBoritekok'')
	--						UNION ALL
	--						SELECT EREC_eMailBoritekok.Id from EREC_eMailBoritekok
	--							WHERE EREC_eMailBoritekok.KuldKuldemeny_Id in
	--							(
	--								SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
	--									INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_KuldKuldemenyek.Id
	--								UNION ALL
	--								SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
	--									INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
	--								UNION ALL
	--								SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
	--									INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_KuldKuldemenyek.IraIratok_Id
	--								UNION ALL
	--								SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
	--									INNER JOIN CsoportTagokAll ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.Csoport_Id_Felelos
	--								UNION ALL
	--								SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
	--									INNER JOIN CsoportTagokAll ON CsoportTagokAll.Id = EREC_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo
	--								UNION ALL
	--								SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
	--									INNER JOIN mappa_tartalmak ON mappa_tartalmak.Obj_Id = EREC_KuldKuldemenyek.Id
	--								UNION ALL
	--								SELECT EREC_KuldKuldemenyek.Id FROM EREC_KuldKuldemenyek
	--								INNER JOIN EREC_IraIratok ON EREC_IraIratok.KuldKuldemenyek_Id = EREC_KuldKuldemenyek.Id
	--								WHERE EREC_IraIratok.Id IN
	--									(
	--										SELECT EREC_IraIratok.Id FROM EREC_IraIratok
	--										WHERE EREC_IraIratok.Ugyirat_Id IN 
	--										(
	--											SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
	--												INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.Id
	--												INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,  @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
	--											UNION ALL
	--											SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
	--												INNER JOIN krt_jogosultak ON krt_jogosultak.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
	--												INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,  @FelhasznaloSzervezet_Id) as CsoportTagokAll ON CsoportTagokAll.Id = krt_jogosultak.Csoport_id_jogalany
	--											UNION ALL
	--											SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
	--												INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,  @FelhasznaloSzervezet_Id) as CsoportTagokAll 
	--													ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
	--											UNION ALL
	--											SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
	--												INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,  @FelhasznaloSzervezet_Id) as CsoportTagokAll 
	--													ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
	--											UNION ALL
	--											SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
	--												INNER JOIN dbo.fn_GetCsoportTagokAllByFelhasznaloAndSzervezetId(@ExecutorUserId,  @FelhasznaloSzervezet_Id) as CsoportTagokAll 
	--													ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
	--											UNION ALL
	--											SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId,  @FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'')
	--											UNION ALL
	--											SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
	--												INNER JOIN mappa_tartalmak ON mappa_tartalmak.Obj_Id = erec_ugyugyiratok.Id
	--										)
	--										UNION ALL --CR2407: lenézünk a példányba, ha látja az iratot, akkor a küldeményt is lássa
	--										SELECT EREC_IraIratok.Id FROM EREC_IraIratok
	--										INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = EREC_IraIratok.Id
	--										WHERE EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo = @ExecutorUserId and EREC_PldIratpeldanyok.Csoport_Id_Felelos = @ExecutorUserId
	--									)	
	--								UNION ALL
	--								SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId,  @FelhasznaloSzervezet_Id, ''EREC_KuldKuldemenyek'')
	--							 )
	--						--Kimeno email	 
	--						--UNION ALL
	--						--SELECT EREC_eMailBoritekok.Id from EREC_eMailBoritekok
	--						--	WHERE EREC_eMailBoritekok.IraIrat_Id in
	--						--	(
	--						--		SELECT EREC_IraIratok.Id FROM EREC_IraIratok
	--						--			INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = EREC_IraIratok.Id
	--						--		UNION ALL
	--						--		SELECT EREC_IraIratok.Id FROM EREC_IraIratok
	--						--			WHERE EREC_IraIratok.Ugyirat_Id IN 
	--						--			(
	--						--				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
	--						--					INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = erec_ugyugyiratok.Id
	--						--				UNION ALL
	--						--				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
	--						--					INNER JOIN jogosult_objektumok ON jogosult_objektumok.Obj_Id = erec_ugyugyiratok.IraIktatokonyv_Id
	--						--				UNION ALL
	--						--				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
	--						--					INNER JOIN CsoportTagokAll 
	--						--						ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Ugyintez
	--						--				UNION ALL
	--						--				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
	--						--					INNER JOIN CsoportTagokAll 
	--						--						ON CsoportTagokAll.Id = erec_ugyugyiratok.Csoport_Id_Felelos
	--						--				UNION ALL
	--						--				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
	--						--					INNER JOIN CsoportTagokAll 
	--						--						ON CsoportTagokAll.Id = erec_ugyugyiratok.FelhasznaloCsoport_Id_Orzo
	--						--				UNION ALL
	--						--				SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_UgyUgyiratok'')
	--						--				UNION ALL
	--						--				SELECT erec_ugyugyiratok.Id FROM erec_ugyugyiratok
	--						--					INNER JOIN mappa_tartalmak ON mappa_tartalmak.Obj_Id = erec_ugyugyiratok.Id
	--						--			)
	--						--		UNION ALL --CR1716: lenézünk a példányba
	--						--		SELECT EREC_IraIratok.Id FROM EREC_IraIratok
	--						--			INNER JOIN EREC_PldIratpeldanyok ON EREC_PldIratpeldanyok.IraIrat_Id = EREC_IraIratok.Id
	--						--			WHERE EREC_PldIratpeldanyok.FelhasznaloCsoport_Id_Orzo = @ExecutorUserId and EREC_PldIratpeldanyok.Csoport_Id_Felelos = @ExecutorUserId
	--						--		UNION ALL
	--						--		SELECT Id FROM dbo.fn_GetObjIdsFromFeladatok(@ExecutorUserId, @FelhasznaloSzervezet_Id, ''EREC_IraIratok'')

	--						--	)	 
	--						)	 
	--						 ';
	--END

	/************************************************************
	* Szurt adatokhoz rendezés és sorszám összeállítása			*
	************************************************************/
	
	--SET @sqlcmd = @sqlcmd + N'
	--select 
	--	   row_number() over('+@OrderBy+') as RowNumber,
 -- 	       EREC_eMailBoritekok.Id into #result
 -- 	       from EREC_eMailBoritekok
 -- 	       LEFT JOIN KRT_KodCsoportok as FontossagKodCsoport on FontossagKodCsoport.Kod=''EMAILBORITEK_FONTOSSAG''
 --          LEFT JOIN KRT_KodTarak FontossagKodTarak on EREC_eMailBoritekok.Fontossag = FontossagKodTarak.Kod and FontossagKodCsoport.Id = FontossagKodTarak.KodCsoport_Id and FontossagKodTarak.Org=@Org
	--       where EREC_eMailBoritekok.Id in (select Id from #filter); '

	if (@SelectedRowId is not null)
	BEGIN
		set @sqlcmd = @sqlcmd + N'
		if exists (select 1 from #result where Id = @SelectedRowId)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result where Id = @SelectedRowId;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END
		ELSE'
	END
	
	set @sqlcmd = @sqlcmd + N' 
		if @pageNumber*@pageSize > (select MAX(RowNumber) from #result)
		BEGIN
			select @pageNumber = ((RowNumber - 1 )/ @pageSize) + 1 from #result group by RowNumber having MAX(RowNumber) = RowNumber;
			set @firstRow = (@pageNumber - 1) * @pageSize + 1;
			set @lastRow = @pageNumber*@pageSize;
			select @pageNumber = @pageNumber - 1
		END ;'
			
	/************************************************************
	* Tényleges select											*
	************************************************************/
	set @sqlcmd = @sqlcmd + N'
	select
	   #result.RowNumber,
	   #result.Id,
	   EREC_eMailBoritekok.Felado,
	   REPLACE(EREC_eMailBoritekok.Cimzett,'','','', '') as Cimzett,
	   REPLACE(EREC_eMailBoritekok.CC,'','','', '') as CC,
	   EREC_eMailBoritekok.Targy,
	   EREC_eMailBoritekok.FeladasDatuma,
	   EREC_eMailBoritekok.ErkezesDatuma,
	   EREC_eMailBoritekok.Fontossag,
       KRT_KodTarak.Nev as FontossagNev,
	   EREC_eMailBoritekok.KuldKuldemeny_Id,
	   (SELECT Azonosito
		 FROM EREC_KuldKuldemenyek
		 WHERE EREC_KuldKuldemenyek.Id = EREC_eMailBoritekok.KuldKuldemeny_Id) as ErkeztetoSzam,
	   EREC_eMailBoritekok.IraIrat_Id,
	   '
	   -- Iktatószám: ha nincs megadva az IraIrat_Id, akkor a KuldKuldemeny_Id alapján keressük meg az iratot
--	   CASE 
--			WHEN IraIrat_Id is not null THEN dbo.fn_GetIratIktatoszam(IraIrat_Id) + ''***'' + convert(NVARCHAR(40), IraIrat_Id)		
--			ELSE CASE WHEN EREC_eMailBoritekok.KuldKuldemeny_Id is not null THEN 
--					(SELECT TOP 1 EREC_IraIratok.Azonosito + ''***'' + convert(NVARCHAR(40), [EREC_IraIratok].[Id])
--					FROM EREC_IraIratok
--					WHERE [EREC_IraIratok].[KuldKuldemenyek_Id] = EREC_eMailBoritekok.KuldKuldemeny_Id
--							and erec_IraIratok.Allapot != ''08'' -- hogy ne a felszabadított irat jöjjön
--					)
--				 ELSE ''''
--				 END
--	   END AS IratIktatoszamEsId,
 set @sqlcmd = @sqlcmd + 'IratIktatoszamEsId = case 
		when coalesce(EREC_IraIratok_Irat.Id, EREC_IraIratok_Kuld.Id) is null then ''''
		else coalesce(EREC_IraIratok_Irat.Azonosito, EREC_IraIratok_Kuld.Azonosito) + ''***'' + convert(NVARCHAR(40), coalesce(EREC_IraIratok_Irat.Id, EREC_IraIratok_Kuld.Id)) end,
	   EREC_eMailBoritekok.DigitalisAlairas,
	   EREC_eMailBoritekok.Uzenet,
	   EREC_eMailBoritekok.EmailForras,
	   EREC_eMailBoritekok.EmailGuid,
	   EREC_eMailBoritekok.FeldolgozasIdo,
	   EREC_eMailBoritekok.ForrasTipus,
	   EREC_eMailBoritekok.Allapot,
	   EREC_eMailBoritekok.Ver,
	   EREC_eMailBoritekok.Note,
	   EREC_eMailBoritekok.Stat_id,
	   EREC_eMailBoritekok.ErvKezd,
	   EREC_eMailBoritekok.ErvVege,
	   EREC_eMailBoritekok.Letrehozo_id,
	   EREC_eMailBoritekok.LetrehozasIdo,
	   EREC_eMailBoritekok.Modosito_id,
	   EREC_eMailBoritekok.ModositasIdo,
	   EREC_eMailBoritekok.Zarolo_id,
	   EREC_eMailBoritekok.ZarolasIdo,
	   EREC_eMailBoritekok.Tranz_id,
	   EREC_eMailBoritekok.UIAccessLog_id  
   from 
     EREC_eMailBoritekok as EREC_eMailBoritekok
     inner join #result on #result.Id = EREC_eMailBoritekok.Id	
     LEFT JOIN KRT_KodCsoportok on KRT_KodCsoportok.Kod=''EMAILBORITEK_FONTOSSAG''
     LEFT JOIN KRT_KodTarak on EREC_eMailBoritekok.Fontossag = KRT_KodTarak.Kod and KRT_KodCsoportok.Id = KRT_KodTarak.KodCsoport_Id and KRT_KodTarak.Org=@Org
LEFT JOIN (select Id, KuldKuldemenyek_Id, Azonosito from EREC_IraIratok where EREC_IraIratok.Allapot not in (''06'', ''08'')) as EREC_IraIratok_Irat
	on EREC_eMailBoritekok.IraIrat_Id is not null and EREC_eMailBoritekok.IraIrat_Id=EREC_IraIratok_Irat.Id
LEFT JOIN (select Id, KuldKuldemenyek_Id, Azonosito from EREC_IraIratok where EREC_IraIratok.Allapot not in (''06'', ''08'')) as EREC_IraIratok_Kuld
	on EREC_eMailBoritekok.IraIrat_Id is null and EREC_eMailBoritekok.KuldKuldemeny_Id is not null
		and EREC_IraIratok_Kuld.KuldKuldemenyek_Id=EREC_eMailBoritekok.KuldKuldemeny_Id
	WHERE RowNumber between @firstRow and @lastRow
	ORDER BY #result.RowNumber;'
   
   -- találatok száma és oldalszám
   set @sqlcmd = @sqlcmd + N' select count(Id) as RecordNumber, @pageNumber as PageNumber from #result;';

   execute sp_executesql @sqlcmd,N'@firstRow int, @lastRow int, @pageSize int, @pageNumber int, @ExecutorUserId uniqueidentifier, @FelhasznaloSzervezet_Id uniqueidentifier, @SelectedRowId uniqueidentifier, @Org uniqueidentifier'
		,@firstRow = @firstRow, @lastRow = @lastRow, @pageSize = @pageSize, @pageNumber = @pageNumber, @ExecutorUserId = @ExecutorUserId, @FelhasznaloSzervezet_Id = @FelhasznaloSzervezet_Id, @SelectedRowId = @SelectedRowId, @Org = @Org;

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end