﻿/*set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
if exists (select 1
            from  sysobjects
           where  id = object_id('sp_EREC_eMailBoritekokHistoryGetAllRecord')
            and   type = 'P')
   drop procedure sp_EREC_eMailBoritekokHistoryGetAllRecord
go
*/
create procedure sp_EREC_eMailBoritekokHistoryGetAllRecord
	@Where			nvarchar(255) = '',
	@OrderBy		   nvarchar(255) = ' order by ExecutionTime'
		
as
begin


   select HistoryId as RowId, 1 as Ver, 'Létrehozás' as Operation, '' as ColumnName, '' as OldValue, '' as NewValue, U.Nev as Executor, HistoryVegrehajtasIdo as ExecutionTime from EREC_eMailBoritekokHistory inner join KRT_Felhasznalok U on U.Id = HistoryVegrehajto_Id where HistoryMuvelet_Id = 0      
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Id' as ColumnName,               cast(Old.Id as nvarchar(99)) as OldValue,
               cast(New.Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Id as nvarchar(max)),'') != ISNULL(CAST(New.Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Felado' as ColumnName,               cast(Old.Felado as nvarchar(99)) as OldValue,
               cast(New.Felado as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Felado as nvarchar(max)),'') != ISNULL(CAST(New.Felado as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Cimzett' as ColumnName,               cast(Old.Cimzett as nvarchar(99)) as OldValue,
               cast(New.Cimzett as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Cimzett as nvarchar(max)),'') != ISNULL(CAST(New.Cimzett as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'CC' as ColumnName,               cast(Old.CC as nvarchar(99)) as OldValue,
               cast(New.CC as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.CC as nvarchar(max)),'') != ISNULL(CAST(New.CC as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Targy' as ColumnName,               cast(Old.Targy as nvarchar(99)) as OldValue,
               cast(New.Targy as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Targy as nvarchar(max)),'') != ISNULL(CAST(New.Targy as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FeladasDatuma' as ColumnName,               cast(Old.FeladasDatuma as nvarchar(99)) as OldValue,
               cast(New.FeladasDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FeladasDatuma as nvarchar(max)),'') != ISNULL(CAST(New.FeladasDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ErkezesDatuma' as ColumnName,               cast(Old.ErkezesDatuma as nvarchar(99)) as OldValue,
               cast(New.ErkezesDatuma as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ErkezesDatuma as nvarchar(max)),'') != ISNULL(CAST(New.ErkezesDatuma as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Fontossag' as ColumnName,               cast(Old.Fontossag as nvarchar(99)) as OldValue,
               cast(New.Fontossag as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Fontossag as nvarchar(max)),'') != ISNULL(CAST(New.Fontossag as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'KuldKuldemeny_Id' as ColumnName,               cast(Old.KuldKuldemeny_Id as nvarchar(99)) as OldValue,
               cast(New.KuldKuldemeny_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.KuldKuldemeny_Id as nvarchar(max)),'') != ISNULL(CAST(New.KuldKuldemeny_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'IraIrat_Id' as ColumnName,               cast(Old.IraIrat_Id as nvarchar(99)) as OldValue,
               cast(New.IraIrat_Id as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.IraIrat_Id as nvarchar(max)),'') != ISNULL(CAST(New.IraIrat_Id as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'DigitalisAlairas' as ColumnName,               cast(Old.DigitalisAlairas as nvarchar(99)) as OldValue,
               cast(New.DigitalisAlairas as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.DigitalisAlairas as nvarchar(max)),'') != ISNULL(CAST(New.DigitalisAlairas as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Uzenet' as ColumnName,               cast(Old.Uzenet as nvarchar(99)) as OldValue,
               cast(New.Uzenet as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Uzenet as nvarchar(max)),'') != ISNULL(CAST(New.Uzenet as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EmailForras' as ColumnName,               cast(Old.EmailForras as nvarchar(99)) as OldValue,
               cast(New.EmailForras as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EmailForras as nvarchar(max)),'') != ISNULL(CAST(New.EmailForras as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'EmailGuid' as ColumnName,               cast(Old.EmailGuid as nvarchar(99)) as OldValue,
               cast(New.EmailGuid as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.EmailGuid as nvarchar(max)),'') != ISNULL(CAST(New.EmailGuid as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'FeldolgozasIdo' as ColumnName,               cast(Old.FeldolgozasIdo as nvarchar(99)) as OldValue,
               cast(New.FeldolgozasIdo as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.FeldolgozasIdo as nvarchar(max)),'') != ISNULL(CAST(New.FeldolgozasIdo as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'ForrasTipus' as ColumnName,               cast(Old.ForrasTipus as nvarchar(99)) as OldValue,
               cast(New.ForrasTipus as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.ForrasTipus as nvarchar(max)),'') != ISNULL(CAST(New.ForrasTipus as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
      union all 
      (select  New.HistoryId as RowId, 
               New.Ver as Ver, 
               case New.HistoryMuvelet_Id 
                  when 1 then 'Módosítás'
                  when 2 then 'Érvénytelenítés'
               end as Operation, 
               'Allapot' as ColumnName,               cast(Old.Allapot as nvarchar(99)) as OldValue,
               cast(New.Allapot as nvarchar(99)) as NewValue,               
               U.Nev as Executor, New.HistoryVegrehajtasIdo as ExecutionTime
      from EREC_eMailBoritekokHistory Old
         inner join EREC_eMailBoritekokHistory New on Old.Ver = (select top 1 Tmp.Ver from EREC_eMailBoritekokHistory Tmp where Tmp.Id = New.Id and Tmp.Ver < New.Ver order by Tmp.Ver desc)
            and Old.Id = New.Id
            and ISNULL(CAST(Old.Allapot as nvarchar(max)),'') != ISNULL(CAST(New.Allapot as nvarchar(max)),'') 
            and Old.Id = New.Id
         inner join KRT_Felhasznalok U on U.Id = New.HistoryVegrehajto_Id      
)
            
end
go