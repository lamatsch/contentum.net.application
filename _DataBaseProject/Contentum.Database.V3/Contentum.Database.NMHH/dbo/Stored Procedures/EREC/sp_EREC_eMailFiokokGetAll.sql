﻿create procedure [dbo].[sp_EREC_eMailFiokokGetAll]
  @Where nvarchar(4000) = '',
  @OrderBy nvarchar(200) = ' order by   EREC_eMailFiokok.LetrehozasIdo',
  @TopRow nvarchar(5) = '',
  @ExecutorUserId				uniqueidentifier,
  @Id    uniqueidentifier = null

as

begin

BEGIN TRY

   set nocount on
   
   DECLARE @sqlcmd nvarchar(4000)

   DECLARE @LocalTopRow nvarchar(10)

   if (@TopRow = '' or @TopRow = '0')
     BEGIN
       SET @LocalTopRow = ''
     END
   ELSE
     BEGIN
       SET @LocalTopRow = ' TOP ' + @TopRow
     END
     
     
          
  SET @sqlcmd = 
  'select ' + @LocalTopRow + '
  	   EREC_eMailFiokok.Id,
	   EREC_eMailFiokok.Nev,
	   EREC_eMailFiokok.UserNev,
	   EREC_eMailFiokok.Csoportok_Id,
	   EREC_eMailFiokok.Jelszo,
	   EREC_eMailFiokok.Tipus,
	   EREC_eMailFiokok.NapiMax,
	   EREC_eMailFiokok.NapiTeny,
	   EREC_eMailFiokok.EmailCim,
	   EREC_eMailFiokok.Modul_Id,
	   EREC_eMailFiokok.MappaNev,
	   EREC_eMailFiokok.Ver,
	   EREC_eMailFiokok.Note,
	   EREC_eMailFiokok.Stat_id,
	   EREC_eMailFiokok.ErvKezd,
	   EREC_eMailFiokok.ErvVege,
	   EREC_eMailFiokok.Letrehozo_id,
	   EREC_eMailFiokok.LetrehozasIdo,
	   EREC_eMailFiokok.Modosito_id,
	   EREC_eMailFiokok.ModositasIdo,
	   EREC_eMailFiokok.Zarolo_id,
	   EREC_eMailFiokok.ZarolasIdo,
	   EREC_eMailFiokok.Tranz_id,
	   EREC_eMailFiokok.UIAccessLog_id  
   from 
     EREC_eMailFiokok as EREC_eMailFiokok      
           '
      
    if @Where is not null and @Where!=''
	begin 
		SET @sqlcmd = @sqlcmd + ' Where ' + @Where
	end
     
   
   SET @sqlcmd = @sqlcmd + @OrderBy;
   exec (@sqlcmd);

END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()
      
   if @errorState = 0 SET @errorState = 1

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end