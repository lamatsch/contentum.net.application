﻿CREATE procedure [dbo].[usp_EREC_KimenoKuldemenyekGetAllWithExtensionForEFeladoJegyzek]
  @ExecutorUserId            uniqueidentifier,            -- csak @Org meghatározására
  @FelhasznaloSzervezet_Id   uniqueidentifier,            --- a kódban NEM használt !?
  @PostaKonyv_Id             uniqueidentifier,            -- nem lehet null!
  @Kikuldo_Id                uniqueidentifier = null,     -- Csoport_Id -  ha meg van adva, erre is szurunk
  @StartDate                 datetime = null,
  @EndDate                   datetime = null,
  @Jogosultak		         char(1) = '0',               --- a kódban NEM használt !? 
  @StartPage                 int = 1,                     -- kezdooldal sorszama
  -- @PageCount                 int = null,                  -- visszaadando oldalak szama, ha null, akkor mind
  @pageNumber		         int = 0,
  @pageSize		             int = -1,
  -- @SelectedRowId	            uniqueidentifier = NULL,
  @WindowSize                int = 100000,                -- egy fajlba exportalhato tetelek max. szama, ha XML-t generalunk, oldalmeret egyebkent
  @ForXml                    char(1) = '0',               -- '0' eseten tablat, '1' eseten a @WindowWith-nek megfeleloen darabolt XML_eket ad vissza
  @ForStatistics             char(1) = '0',               --- a kódban NEM használt !? -- '1' eseten tartalmazza az id-t es szamadatokat a rekordokrol (hibas, osszes, oldalszam stb.)
							                                                           -- egyebkent ill. '0' eseten normal lekeres, 
  @IgnoreKozonseges          char(1) = '0',               -- ha '1', a kozonseges kuldemenyeket nem adjuk vissza
  @IgnoreNyilvantartott      char(1) = '0'                -- ha '1', a nyilvantartott kuldemenyeket nem adjuk vissza
as

begin

BEGIN TRY

   set nocount on

	DECLARE @Org uniqueidentifier
	SET @Org = dbo.fn_GetOrgByFelhasznaloId(@ExecutorUserId)
	if (@Org is null)
	begin
		RAISERROR('[50202]',16,1)
	end

/*
declare @notnull_kozonseges_tetelek table (col varchar(15))
insert into @notnull_kozonseges_tetelek
select 'sorszam'
union all select 'alapszolg'
union all select 'suly'
union all select 'darab'
union all select 'dij'

declare @notnull_nyilvantartott_tetelek table (col varchar(15))
insert into @notnull_nyilvantartott_tetelek
select 'sorszam'
union all select 'azonosito'
union all select 'alapszolg'
union all select 'cimzett_irsz'
union all select 'suly'
union all select 'cimzett_nev'
union all select 'cimzett_hely'
union all select 'dij'
*/

/*---------------------------------
Közönséges küldemények
----------------------------------*/

--- belfoldi kozonseges, halmozottan átadandó, ( a nemzetkozi kozonseges -sel egy, de ) önálló <jegyzek_adat>-ban
IF OBJECT_ID('tempdb..#tmp_kozonsegeskuldemenyek') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_kozonsegeskuldemenyek
END

CREATE TABLE #tmp_kozonsegeskuldemenyek (
	sorszam      int not null,       -- sorszam varchar(3) not null, -- min. 1
	alapszolg    varchar(9),         -- not null, -- min. 7
	cimzett_irsz varchar(10),         -- Null vagy 4 hosszú        --- 'Opcionális'    
	suly         varchar(5),         -- not null, -- min. 1
	darab        varchar(6),         -- not null, -- min. 1
	kulonszolgok varchar(200),
	vakok_irasa  char(1),
	meret        varchar(2),         -- min. 1
	dij          varchar(9),         -- not null, -- min. 1 
	gepre_alkalmassag char(1)                                    --- 'Opcionális'
)

--- nemzetkozi kozonseges, halmozottan átadandó, ( a belfoldi kozonseges -sel egy, de ) önálló <jegyzek_adat>-ban
--- !!! elvben ilyen sosem lehet, amíg a CIM hiánya jelzi a kozonseges -t
IF OBJECT_ID('tempdb..#tmp_nemz_kozonseges') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_nemz_kozonseges
END

CREATE TABLE #tmp_nemz_kozonseges (
	sorszam      int not null,       -- sorszam varchar(3) not null, -- min. 1
	alapszolg    varchar(9),         -- not null, -- min. 7
	viszonylat   varchar(2),         -- min. 1
	orszagkod    varchar(3),         -- min. 2
	suly         varchar(5),         -- not null, -- min. 1
	darab        varchar(6),         -- not null, -- min. 1
	kulonszolgok varchar(200),
	vakok_irasa  char(1),
	meret        varchar(2),         -- min. 1
	dij          varchar(9),         -- not null, -- min. 1 
	potlapszam   varchar(2)
)

--- a temp táblát mindenképp létrehozom, mivel a Query-ket a végén - akár üresen is, DE - vissza kell adjam
	IF OBJECT_ID('tempdb..#tmp_kozonsegeskuldemenyek_with_id') IS NOT NULL 
	BEGIN
	   DROP TABLE #tmp_kozonsegeskuldemenyek_with_id
	END

	CREATE TABLE #tmp_kozonsegeskuldemenyek_with_id (
		id           uniqueidentifier,
	    nemzetkozi   char(1),        --- '0' - belföldi; '1' - nemzetközi		
		sorszam      int,            -- not null,--sorszam varchar(3) not null, -- min. 1
		alapszolg    varchar(9),     -- not null, -- min. 7
	    cimzett_irsz varchar(10),     -- Null vagy 4 hosszú        --- 'Opcionális'    		
		viszonylat   varchar(2),     -- min. 1
		orszagkod    varchar(3),     -- min. 2
		suly         int,            --- itt miért más az adattípus ??? --varchar(5), --not null, -- min. 1
		darab        int,            --- itt miért más az adattípus ??? --varchar(6), --not null, -- min. 1
		kulonszolgok varchar(200),
		vakok_irasa  char(1),
		meret        varchar(2),     -- min. 1
		dij          int,            --- itt miért más az adattípus ??? --varchar(9), --not null, -- min. 1 
		gepre_alkalmassag char(1),
	    potlapszam   varchar(2)                                    --- 'Opcionális' 'Nemzetközi esetén tölthető'
	)

if @IgnoreKozonseges != '1'
BEGIN

	;WITH KozonsegesKuldemenyek as
	(
	select
	 EREC_KuldKuldemenyek.Id
	, nemzetkozi = case when IsNull(KRT_Orszagok.Kod, 'HU') = 'HU' then '0'	
	                    when KRT_Orszagok.Viszonylatkod is NULL    then '0'
						else '1'            -- Europai - K1, Egyéb külföld - K2; vagy Null-- Europai K1, Egyéb K2
				   end		
    /*	 
	, nemzetkozi = case when IsNull(KRT_Orszagok.Kod, 'HU') = 'HU' then '0'
	                    else '1'
				   end
    */
	, alapszolg = case
		when KimenoKuldemenyFajta in ('01','02','03','04','05','06','07','K1_01','K1_02','K1_03','K1_04','K1_05','K1_06','K1_07','K1_08','K1_09','K2_01','K2_02','K2_03','K2_04','K2_05','K2_06','K2_07','K2_08','K2_09') then 'A_111_LEV'
		--when KimenoKuldemenyFajta in ('18','K1_10','K2_10') then 'A_111_LLP'
		else null
		end
	-- BLG_1938
--	  , Null cimzett_irsz		
      ,cast(KRT_Cimek.IRSZ as varchar(10)) cimzett_irsz	

	, viszonylat = case IsNull(KRT_Orszagok.Kod, 'HU') 
		when 'HU' then null                                                    --- belföldi esetén nem kell
		else KRT_Orszagok.Viszonylatkod     -- Europai - K1, Egyéb külföld - K2; vagy Null
	    end
	, orszagkod = case when IsNull(KRT_Orszagok.Kod, 'HU') = 'HU' then Null
	                   when KRT_Orszagok.Viszonylatkod is NULL    then Null     --- mert ekkor is belföldi és ott nem kell
				       else KRT_Orszagok.Kod                                    --- 'Nemzetközi esetén a viszonylat VAGY az országkód megadása kötelező'
                  end
    /*		
	, orszagkod = case IsNull(KRT_Orszagok.Kod, 'HU')   -- ha előző nem adott ( ez mit is jelent ? )
		when 'HU' then null
		else case
			when KRT_Orszagok.Viszonylatkod is null then KRT_Orszagok.Kod
			else null
		    end
	    end
	*/			
	--, suly = case
	--	when KimenoKuldemenyFajta='01' then 30
	--	when KimenoKuldemenyFajta in ('02','K1_03','K2_03') then 50
	--	when KimenoKuldemenyFajta in ('03','K1_04','K2_04') then 100
	--	when KimenoKuldemenyFajta in ('04','K1_05','K2_05') then 250
	--	when KimenoKuldemenyFajta in ('05','K1_06','K2_06') then 500
	--	when KimenoKuldemenyFajta='06' then 750
	--	when KimenoKuldemenyFajta in ('07','K1_09','K2_09') then 2000
	--	when KimenoKuldemenyFajta='18' then
	--		case when IsNull(KRT_Orszagok.Kod, 'HU') = 'HU' then 30
	--		else 20
	--		end
	--	when KimenoKuldemenyFajta in ('K1_01','K2_01','K1_02','K2_02','K1_10','K2_10') then 20
	--	when KimenoKuldemenyFajta in ('K1_07','K2_07') then 1000
	--	when KimenoKuldemenyFajta in ('K1_08','K2_08') then 1500
	--	else null -- valódi súly, itt: hiba!!!
	--	end
	, kt_kuldemenySulya.kod as suly
	, PeldanySzam as darab
	, kulonszolgok = case Elsobbsegi
		when 1 then 'K_PRI' -- K_FEL
		else null
		end
	, null vakok_irasa
	, meret = case
		when KimenoKuldemenyFajta in ('01','K1_01','K2_01') then 1
		when KimenoKuldemenyFajta in ('18','K1_10','K2_10') then null
		else 0
		end
	, cast(Ar as int) dij
	, null gepre_alkalmassag
	, Null potlapszam
	from dbo.EREC_KuldKuldemenyek as EREC_KuldKuldemenyek
	left join dbo.EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Postakonyv on EREC_IraIktatoKonyvek_Postakonyv.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
	left join dbo.KRT_Cimek as KRT_Cimek on KRT_Cimek.Id=EREC_KuldKuldemenyek.Cim_Id 
	left join dbo.KRT_Orszagok as KRT_Orszagok on KRT_Orszagok.Id=KRT_Cimek.Orszag_Id
	-- BUG_8613
	left join dbo.KRT_KodCsoportok as kcs_KuldemenyFajta on kcs_KuldemenyFajta.kod ='KIMENO_KULDEMENY_FAJTA'
	left join dbo.KRT_KodTarak as kt_KuldemenyFajta on kt_KuldemenyFajta.kodcsoport_Id= kcs_kuldemenyFajta.id and kt_kuldemenyFajta.kod COLLATE Hungarian_CI_AS = KimenoKuldemenyFajta and EREC_KuldKuldemenyek.BelyegzoDatuma between kt_kuldemenyFajta.ervkezd and kt_kuldemenyFajta.ervvege
	left join dbo.fn_GetKodtarFuggosegAktiv('4ADBD31F-ACA9-4AFF-B1BD-372453FE7A70') as fk_KuldemenyFajta on fk_KuldemenyFajta.VezerloKodTarId=kt_KuldemenyFajta.Id
	left join dbo.KRT_KodCsoportok as kcs_KuldemenySulya on kcs_KuldemenySulya.kod ='KIMENOKULDEMENY_SULY'
	left join dbo.KRT_KodTarak as kt_KuldemenySulya on kt_KuldemenySulya.kodcsoport_Id= kcs_kuldemenySulya.id and kt_kuldemenySulya.id = fk_KuldemenyFajta.FuggoKodTarId and EREC_KuldKuldemenyek.BelyegzoDatuma between kt_kuldemenySulya.ervkezd and kt_kuldemenySulya.ervvege
	--hiv ertesito fuggo kodtarbol
	left join dbo.fn_GetKodtarFuggosegAktiv('0021581B-1E23-423A-8F06-B829DA5700E0') as fk_HivErtesito on fk_HivErtesito.VezerloKodTarId=kt_KuldemenyFajta.Id
	left join dbo.KRT_KodCsoportok as kcs_HivatalosErtesitoTipus on kcs_HivatalosErtesitoTipus.kod ='POSTA_HIV_ERTESITO_TIPUS'
	left join dbo.KRT_KodTarak as kt_HivatalosKuldemenyTipusa on kt_HivatalosKuldemenyTipusa.kodcsoport_Id= kcs_HivatalosErtesitoTipus.id and kt_HivatalosKuldemenyTipusa.id= fk_HivErtesito.FuggoKodTarId 

	where 1 = 1
	--- and EREC_KuldKuldemenyek.Cim_Id is Null                          /* csak azok, amelyekhez nincs Cim adat => nemzetközi ebből a fajtából nincs */
	--and ( EREC_KuldKuldemenyek.Cim_Id is Null OR IsNull(EREC_KuldKuldemenyek.Peldanyszam,1) > 1 )        --- ( Laura, 2018.08.31.)
	and EREC_IraIktatoKonyvek_PostaKonyv.IktatoErkezteto='P'
	and EREC_IraIktatoKonyvek_PostaKonyv.Id = @PostaKonyv_Id
	and PostazasIranya='2' -- kimenő
	and Allapot='06' -- Postázott
	--and KuldesMod in ('01', '17') -- Postai sima, Postai elsőbbségi
	--and KimenoKuldemenyFajta in ('01','02','03','04','05','06','07','18','K1_01','K1_02','K1_03','K1_04','K1_05','K1_06','K1_07','K1_08','K1_09','K1_10','K2_01','K2_02','K2_03','K2_04','K2_05','K2_06','K2_07','K2_08','K2_09','K2_10')
	and KimenoKuldemenyFajta in ('01','02','03','04','05','06','07','K1_01','K1_02','K1_03','K1_04','K1_05','K1_06','K1_07','K1_08','K1_09','K2_01','K2_02','K2_03','K2_04','K2_05','K2_06','K2_07','K2_08','K2_09')
	and Ajanlott = 0
	and Tertiveveny = 0
	and SajatKezbe = 0
	and E_ertesites = 0
	and E_elorejelzes = 0
	and PostaiLezaroSzolgalat = 0
	and EREC_KuldKuldemenyek.BelyegzoDatuma between IsNull(@StartDate, '1900-01-01') and IsNull(@EndDate, getdate())
	and (@Kikuldo_Id is null or EREC_KuldKuldemenyek.Csoport_Id_Cimzett = @Kikuldo_Id)
	)
	insert into #tmp_kozonsegeskuldemenyek_with_id
	select
	  Id
	, nemzetkozi
	, ( select sorszam 
	     from ( select row_number() over ( PARTITION by nemzetkozi 
		                                       order by kulonszolgok desc, alapszolg, viszonylat, orszagkod, suly desc ) as sorszam
	                 , alapszolg
					 , cimzett_irsz
	                 , viszonylat
	                 , orszagkod
	                 , suly
	                 -- , sum(darab) darab
	                 , kulonszolgok
	                 , vakok_irasa
	                 , meret
	                 -- , sum(dij) dij
	                 , gepre_alkalmassag
					 , potlapszam
	              from KozonsegesKuldemenyek
	              group by nemzetkozi, kulonszolgok, alapszolg, viszonylat, orszagkod, suly, meret, cimzett_irsz, vakok_irasa, gepre_alkalmassag, potlapszam
	          ) tmp
	    where ( (tmp.kulonszolgok is null and KozonsegesKuldemenyek.kulonszolgok is null)
		        or (tmp.kulonszolgok = KozonsegesKuldemenyek.kulonszolgok) )
	      and ( (tmp.alapszolg is null and KozonsegesKuldemenyek.alapszolg is null)
		        or (tmp.alapszolg = KozonsegesKuldemenyek.alapszolg) )
	      and ( (tmp.viszonylat is null and KozonsegesKuldemenyek.viszonylat is null)
		        or (tmp.viszonylat = KozonsegesKuldemenyek.viszonylat) )
	      and ( (tmp.orszagkod is null and KozonsegesKuldemenyek.orszagkod is null)
		        or (tmp.orszagkod = KozonsegesKuldemenyek.orszagkod) )
	      and ( (tmp.suly is null and KozonsegesKuldemenyek.suly is null)
		        or (tmp.suly = KozonsegesKuldemenyek.suly) )
	      and ( (tmp.meret is null and KozonsegesKuldemenyek.meret is null)
		        or (tmp.meret = KozonsegesKuldemenyek.meret) )
	      and ( (tmp.vakok_irasa is null and KozonsegesKuldemenyek.vakok_irasa is null)
		        or (tmp.vakok_irasa = KozonsegesKuldemenyek.vakok_irasa) ) 
	      and ( (tmp.cimzett_irsz is null and KozonsegesKuldemenyek.cimzett_irsz is null)
		        or (tmp.cimzett_irsz = KozonsegesKuldemenyek.cimzett_irsz) ) 
	      and ( (tmp.vakok_irasa is null and KozonsegesKuldemenyek.vakok_irasa is null)
		        or (tmp.vakok_irasa = KozonsegesKuldemenyek.vakok_irasa) ) 
	      and ( (tmp.gepre_alkalmassag is null and KozonsegesKuldemenyek.gepre_alkalmassag is null)
		        or (tmp.gepre_alkalmassag = KozonsegesKuldemenyek.gepre_alkalmassag) )
	      and ( (tmp.potlapszam is null and KozonsegesKuldemenyek.potlapszam is null)
		        or (tmp.potlapszam = KozonsegesKuldemenyek.potlapszam) )
	 ) sorszam
	, alapszolg
	, cimzett_irsz
	, viszonylat
	, orszagkod
	, suly
	, darab--, sum(darab) darab
	, kulonszolgok
	, vakok_irasa
	, meret
	, dij--, sum(dij) dij
	, gepre_alkalmassag
	, potlapszam
	from KozonsegesKuldemenyek

	insert into #tmp_kozonsegeskuldemenyek
	select 
	  sorszam 
	, alapszolg
	, cimzett_irsz
	, suly
	, sum(darab) darab
	, kulonszolgok
	, vakok_irasa
	, meret
	, sum(dij) dij
	, gepre_alkalmassag
	from #tmp_kozonsegeskuldemenyek_with_id 
	where nemzetkozi = '0'
	group by nemzetkozi, sorszam, kulonszolgok, alapszolg, viszonylat, orszagkod, suly, meret, cimzett_irsz, gepre_alkalmassag, vakok_irasa, potlapszam
	order by nemzetkozi, sorszam                      --- ??? miért is kell ez ide 
	
	
	insert into #tmp_nemz_kozonseges
	select 
	  sorszam 
	, alapszolg
	, viszonylat
	, orszagkod
	, suly
	, sum(darab) darab
	, kulonszolgok
	, vakok_irasa
	, meret
	, sum(dij) dij
	, potlapszam
	from #tmp_kozonsegeskuldemenyek_with_id 
	where nemzetkozi = '1'
	group by nemzetkozi, sorszam, kulonszolgok, alapszolg, viszonylat, orszagkod, suly, meret, cimzett_irsz, gepre_alkalmassag, vakok_irasa, potlapszam
	order by nemzetkozi, sorszam                      --- ??? miért is kell ez ide 
	
END

/*-----------------------------------
Közönséges, azonosított tétel szekció
-------------------------------------- */
--- belfoldi kozonseges azonositott, soronként átadandó; a belfoldi/nemzetkozi kozonseges -hez képest egy másik <jegyzek_adat>-ban
IF OBJECT_ID('tempdb..#tmp_belf_kozons_azon') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_belf_kozons_azon
END

CREATE TABLE #tmp_belf_kozons_azon (
	sorszam      int not null,       -- sorszam varchar(3) not null, -- min. 1
	alapszolg    varchar(9),         -- not null, -- min. 7
	azonosito    varchar(16),        -- not null, -- min. 13
	cimzett_nev  nvarchar(400),        -- not null, -- min. 3
	cimzett_irsz varchar(4),         -- not null, -- 0 v. 4	
	cimzett_hely nvarchar(400),        -- not null, -- min. 3
	cimzett_kozelebbi_cim     nvarchar(400),     -- bontott cimet adunk, ez üresen marad (kivéve HRSZ)
	cimzett_kozterulet_nev    nvarchar(400),
	cimzett_kozterulet_jelleg nvarchar(400),
	cimzett_hazszam           varchar(30),
	cimzett_epulet            varchar(10),
	cimzett_lepcsohaz         varchar(10),
	cimzett_emelet            varchar(10),
	cimzett_ajto              varchar(10),
	cimzett_postafiok         varchar(10),
	cimzett_cim_id            varchar(10),
	suly         varchar(5),         -- not null, -- min. 1
	kulonszolgok varchar(200),
	vakok_irasa  char(1),
	meret        varchar(2),         -- min. 1
	dij          varchar(9),         -- not null, -- min. 1 
	gepre_alkalmassag char(1)        -- csak <belf_tetelek> -ben használható a tag
)

--- nemzetkozi kozonseges azonositott, soronként átadandó; a belfoldi kozonseges azonositottal egy <jegyzek_adat>-ban
IF OBJECT_ID('tempdb..#tmp_nemz_kozons_azon') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_nemz_kozons_azon
END

CREATE TABLE #tmp_nemz_kozons_azon (
	sorszam      int not null,       -- sorszam varchar(3) not null, -- min. 1
	alapszolg    varchar(9),         -- not null, -- min. 7
	azonosito    varchar(16),        -- not null, -- min. 13
	orszagkod    varchar(3),         -- min. 2
	cimzett_nev  nvarchar(400),        -- not null, -- min. 3	
	cimzett_irsz varchar(10),         -- not null, -- 0 v. 4	
	cimzett_hely nvarchar(400),        -- not null, -- min. 3	
	cimzett_kozelebbi_cim nvarchar(400),     -- 
	suly         varchar(5),         -- not null, -- min. 1
	kulonszolgok varchar(200),
	vakok_irasa  char(1),
	meret        varchar(2),         -- min. 1
	dij          varchar(9),         -- not null, -- min. 1 
	vam_ertek      varchar(9),       -- lehet Null 
	export_eng     varchar(50),      -- lehet Null
	valutanem      varchar(3),       -- "Lehetséges értékei: HUF, EUR; USD"
	aru_tartalom   varchar(50),      -- lehet Null
    --- <vam_adatok> element tag-ek                   
    aru_db         varchar(100),
    aru_nev        varchar(100),
    aru_ertek      varchar(100),
    vam_tarifakod  varchar(100),
    aru_orszag     varchar(100),
    aru_suly       varchar(100),
    --- <documents> element tag-ek                                        -- "Opcionális"; típusok, hosszok NEM lettek megadva !?
    engedely_szama varchar(100),
    dok_nev        varchar(100)  
)

--- a temp táblát mindenképp létrehozom, mivel a Query-ket a végén - akár üresen is, DE - vissza kell adjam
	IF OBJECT_ID('tempdb..#tmp_kozonseges_azon_with_id') IS NOT NULL 
	BEGIN
	   DROP TABLE #tmp_kozonseges_azon_with_id
	END

CREATE TABLE #tmp_kozonseges_azon_with_id (
    id           uniqueidentifier,
	nemzetkozi   char(1),            --- '0' - belföldi; '1' - nemzetközi		
	sorszam      int not null,       -- sorszam varchar(3) not null, -- min. 1
	alapszolg    varchar(9),         -- not null, -- min. 7
	azonosito    varchar(16),        -- not null, -- min. 13
	orszagkod    varchar(3),         -- min. 2	
	cimzett_nev  nvarchar(400),        -- not null, -- min. 3
	cimzett_irsz varchar(10),         -- not null, -- 0 v. 4	
	cimzett_hely nvarchar(400),        -- not null, -- min. 3
	cimzett_kozelebbi_cim     nvarchar(400),     -- bontott cimet adunk, ez üresen marad (kivéve HRSZ)
	cimzett_kozterulet_nev    nvarchar(400),
	cimzett_kozterulet_jelleg nvarchar(400),
	cimzett_hazszam           varchar(30),
	cimzett_epulet            varchar(10),
	cimzett_lepcsohaz         varchar(10),
	cimzett_emelet            varchar(10),
	cimzett_ajto              varchar(10),
	cimzett_postafiok         varchar(10),
	cimzett_cim_id            varchar(10),
	suly           varchar(5),       -- not null, -- min. 1
	kulonszolgok   varchar(200),
	vakok_irasa    char(1),
	meret          varchar(2),       -- min. 1
	dij            varchar(9),       -- not null, -- min. 1 
	gepre_alkalmassag char(1),       -- csak <belf_tetelek> -ben használható a tag
	vam_ertek      varchar(9),       -- lehet Null 
	export_eng     varchar(50),      -- lehet Null
	valutanem      varchar(3),       -- "Lehetséges értékei: HUF, EUR; USD"
	aru_tartalom   varchar(50),      -- lehet Null
    --- <vam_adatok> element tag-ek                   
    aru_db         varchar(100),
    aru_nev        varchar(100),
    aru_ertek      varchar(100),
    vam_tarifakod  varchar(100),
    aru_orszag     varchar(100),
    aru_suly       varchar(100),
    --- <documents> element tag-ek                                        -- "Opcionális"; típusok, hosszok NEM lettek megadva !?
    engedely_szama varchar(100),
    dok_nev        varchar(100)  	
)
	
if @IgnoreKozonseges != '1'
BEGIN
    --- össze lett ollózva a kozonseges és a nyilvantartott gyujtéséből - nem kizárt, hogy módosítani kell 
	insert into #tmp_kozonseges_azon_with_id
	select
	 EREC_KuldKuldemenyek.Id
	, nemzetkozi = case when IsNull(KRT_Orszagok.Kod, 'HU') = 'HU' then '0'	
	                    when KRT_Orszagok.Viszonylatkod is NULL    then '0'
						else '1'            -- Europai - K1, Egyéb külföld - K2; vagy Null-- Europai K1, Egyéb K2
				   end
	, row_number() over ( PARTITION by case when IsNull(KRT_Orszagok.Kod, 'HU') = 'HU' then '0'	
	                                        when KRT_Orszagok.Viszonylatkod is NULL    then '0'
						                    else '1'            -- Europai - K1, Egyéb külföld - K2; vagy Null-- Europai K1, Egyéb K2
				                       end                      -- nemzetkozi adatmező szerint
	                          order by case SUBSTRING(Ragszam, 1, 2)
		                                    when 'EL' then 101  -- 1. Tételesen kezelt küldemények
		                                    when 'RB' then 102  -- 1. Tételesen kezelt küldemények
		                                    when 'VV' then 103  -- 1. Tételesen kezelt küldemények
		                                    when 'RL' then 201  -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
		                                    when 'TRL' then 202 -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
		                                    when 'KR' then 203  -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
		                                    when 'RR' then 204  -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
		                                    else null 
									   end, RagSzam ) as sorszam      -- sorszam
	, alapszolg = 
	    case
		     when KimenoKuldemenyFajta in ('01','02','03','04','05','06','07','K1_01','K1_02','K1_03','K1_04','K1_05','K1_06','K1_07','K1_08','K1_09','K2_01','K2_02','K2_03','K2_04','K2_05','K2_06','K2_07','K2_08','K2_09') then 'A_111_LEV'
		     --when KimenoKuldemenyFajta in ('18','K1_10','K2_10') then 'A_111_LLP'
		     else null
		end
	, cast( RagSzam as varchar(16) ) azonosito
	, orszagkod = case IsNull(KRT_Orszagok.Kod, 'HU')
		               when 'HU' then null
		               else KRT_Orszagok.Kod
	              end
	, cast(NevSTR_Bekuldo as nvarchar(400)) cimzett_nev
	, cimzett_irsz = case IsNull(KRT_Orszagok.Kod, 'HU')
		                  when 'HU' then cast(KRT_Cimek.IRSZ as varchar(4))
						-- BLG_1938
					--		else null --'' -- kötelező
						  else cast(KRT_Cimek.IRSZ as varchar(10))
						end
	, cast(ISNULL(KRT_Cimek.TelepulesNev,'  ') as nvarchar(400)) cimzett_hely                 -- min 2 karakter
	-- BLG_1938
	, case when IsNull(KRT_Orszagok.Kod, 'HU') = 'HU'  or KRT_Orszagok.Viszonylatkod is  NULL THEN 
			Cast( CASE WHEN KRT_Cimek.HRSZ is NULL OR KRT_Cimek.HRSZ='' THEN '' 
	             ELSE CASE WHEN KRT_Cimek.TelepulesNev is NULL OR KRT_Cimek.TelepulesNev = '' THEN '' 
				           ELSE KRT_Cimek.TelepulesNev + ' HRSZ.' + KRT_Cimek.HRSZ 
					  END 
		    END as nvarchar(400) ) 
	   ELSE 	
			 Cast( CASE WHEN KRT_Cimek.HRSZ is NULL OR KRT_Cimek.HRSZ='' THEN 
					dbo.fn_MergeKozelebbi_Cim(KRT_Cimek.Tipus,
								   KRT_Cimek.KozteruletNev,
								   KRT_Cimek.KozteruletTipusNev,
								   KRT_Cimek.Hazszam,
								   KRT_Cimek.Hazszamig,
								   KRT_Cimek.HazszamBetujel,
								   KRT_Cimek.Lepcsohaz,
								   KRT_Cimek.Szint,
								   KRT_Cimek.Ajto,
								   KRT_Cimek.AjtoBetujel,
								   -- BLG_1347
								   KRT_Cimek.HRSZ,
								   KRT_Cimek.CimTobbi) 
	             ELSE CASE WHEN KRT_Cimek.TelepulesNev is NULL OR KRT_Cimek.TelepulesNev = '' THEN '' 
				           ELSE KRT_Cimek.TelepulesNev + ' HRSZ.' + KRT_Cimek.HRSZ 
					  END 
		    END as nvarchar(400) ) 
		END  cimzett_kozelebbi_cim
 
	, Cast(KRT_Cimek.KozteruletNev as nvarchar(400)) cimzett_kozterulet_nev
	, Cast(KRT_Cimek.KozteruletTipusNev as nvarchar(400)) cimzett_kozterulet_jelleg
	, Cast( CASE WHEN KRT_Cimek.Tipus = '02' THEN NULL 
	            ELSE KRT_Cimek.Hazszam + CASE WHEN  KRT_Cimek.Hazszamig is NULL OR  KRT_Cimek.Hazszamig = '' THEN '' 
				                              ELSE ' - ' +  KRT_Cimek.Hazszamig 
										 END 
	        END as varchar(30) ) cimzett_hazszam
	, Cast(KRT_Cimek.HazszamBetujel as varchar(10)) cimzett_epulet
	, Cast(KRT_Cimek.Lepcsohaz as varchar(10)) cimzett_lepcsohaz
	, Cast(KRT_Cimek.Szint as varchar(10)) cimzett_emelet
	, Cast(KRT_Cimek.Ajto + CASE WHEN KRT_Cimek.AjtoBetujel is NULL OR KRT_Cimek.AjtoBetujel = '' THEN '' 
	                             ELSE ' / ' + KRT_Cimek.AjtoBetujel 
							END as varchar(10) ) cimzett_ajto
	, CAST(CASE WHEN KRT_Cimek.Tipus = '02' THEN KRT_Cimek.Hazszam ELSE NULL END as varchar(10)) cimzett_postafiok
    , Null cimzett_cim_id
	--, suly = case
	--	          when KimenoKuldemenyFajta='01' then 30
	--	          when KimenoKuldemenyFajta in ('02','K1_03','K2_03') then 50
	--	          when KimenoKuldemenyFajta in ('03','K1_04','K2_04') then 100
	--	          when KimenoKuldemenyFajta in ('04','K1_05','K2_05') then 250
	--	          when KimenoKuldemenyFajta in ('05','K1_06','K2_06') then 500
	--	          when KimenoKuldemenyFajta='06' then 750
	--	          when KimenoKuldemenyFajta in ('07','K1_09','K2_09') then 2000
	--	          when KimenoKuldemenyFajta='18' then
	--		                                         case when IsNull(KRT_Orszagok.Kod, 'HU') = 'HU' then 30
	--		                                              else 20
	--		                                         end
	--	          when KimenoKuldemenyFajta in ('09','11','19') then 30                -- valódi súly?
	--	          when KimenoKuldemenyFajta in ('08','10','20') then 30                -- valódi súly
	--	          when KimenoKuldemenyFajta in ('K1_01','K2_01','K1_02','K2_02','K1_10','K2_10') then 20
	--	          when KimenoKuldemenyFajta in ('K1_07','K2_07') then 1000
	--	          when KimenoKuldemenyFajta in ('K1_08','K2_08') then 1500
	--	          else null                                                            -- valódi súly, itt: hiba!!!
	--	     end
	, kt_kuldemenySulya.kod as suly
	, kulonszolgok = case Elsobbsegi 
	                      when 1 then 'K_PRI' -- K_FEL
		                  else null
		             end
	, null vakok_irasa
	, meret = case
		           when KimenoKuldemenyFajta in ('01','K1_01','K2_01') then 1
		           when KimenoKuldemenyFajta in ('18','K1_10','K2_10') then null
		           else 0
		      end
	, cast(Ar as int) dij
	, cast(null as char(1)) gepre_alkalmassag                              -- case K_FEL különszolg  when 1 then case ... 'E' v. 'G' else null
    , Null vam_ertek
    , Null export_eng
    , Null valutanem
    , Null aru_tartalom
    --- <vam_adatok> element tag-ek                   
    , Null aru_db
    , Null aru_nev
    , Null aru_ertek
	, Null vam_tarifakod
	, Null aru_orszag
	, Null aru_suly
    --- <documents> element tag-ek                                        -- "Opcionális"; típusok, hosszok NEM lettek megadva !?
	, Null engedely_szama
	, Null dok_nev
	from dbo.EREC_KuldKuldemenyek as EREC_KuldKuldemenyek
	left join dbo.EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Postakonyv on EREC_IraIktatoKonyvek_Postakonyv.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
	inner join dbo.KRT_Cimek as KRT_Cimek on KRT_Cimek.Id=EREC_KuldKuldemenyek.Cim_Id                             --- elvben az INNER JOIN biztosítja, hogy csak CIM-zett küldemények kerüljenek ebbe a szekcióba
	left join dbo.KRT_Orszagok as KRT_Orszagok on KRT_Orszagok.Id=KRT_Cimek.Orszag_Id
	-- BUG_8613
	left join dbo.KRT_KodCsoportok as kcs_KuldemenyFajta on kcs_KuldemenyFajta.kod ='KIMENO_KULDEMENY_FAJTA'
	left join dbo.KRT_KodTarak as kt_KuldemenyFajta on kt_KuldemenyFajta.kodcsoport_Id= kcs_kuldemenyFajta.id and kt_kuldemenyFajta.kod COLLATE Hungarian_CI_AS = KimenoKuldemenyFajta and EREC_KuldKuldemenyek.BelyegzoDatuma between kt_kuldemenyFajta.ervkezd and kt_kuldemenyFajta.ervvege
	left join dbo.fn_GetKodtarFuggosegAktiv('4ADBD31F-ACA9-4AFF-B1BD-372453FE7A70') as fk_KuldemenyFajta on fk_KuldemenyFajta.VezerloKodTarId=kt_KuldemenyFajta.Id
	left join dbo.KRT_KodCsoportok as kcs_KuldemenySulya on kcs_KuldemenySulya.kod ='KIMENOKULDEMENY_SULY'
	left join dbo.KRT_KodTarak as kt_KuldemenySulya on kt_KuldemenySulya.kodcsoport_Id= kcs_kuldemenySulya.id and kt_kuldemenySulya.id= fk_KuldemenyFajta.FuggoKodTarId and EREC_KuldKuldemenyek.BelyegzoDatuma between kt_kuldemenySulya.ervkezd and kt_kuldemenySulya.ervvege
	--hiv ertesito fuggo kodtarbol
	left join dbo.fn_GetKodtarFuggosegAktiv('0021581B-1E23-423A-8F06-B829DA5700E0') as fk_HivErtesito on fk_HivErtesito.VezerloKodTarId=kt_KuldemenyFajta.Id
	left join dbo.KRT_KodCsoportok as kcs_HivatalosErtesitoTipus on kcs_HivatalosErtesitoTipus.kod ='POSTA_HIV_ERTESITO_TIPUS'
	left join dbo.KRT_KodTarak as kt_HivatalosKuldemenyTipusa on kt_HivatalosKuldemenyTipusa.kodcsoport_Id= kcs_HivatalosErtesitoTipus.id and kt_HivatalosKuldemenyTipusa.id= fk_HivErtesito.FuggoKodTarId 

	where 1 = 1
	and IsNull(EREC_KuldKuldemenyek.Peldanyszam,1) = 1                                    --- "ha a darabszám 1 és a címzett adatai ki vannak töltve, akkor közönséges azonosított" ( Laura, 2018.08.31.)
	and EREC_IraIktatoKonyvek_PostaKonyv.IktatoErkezteto='P'
	and EREC_IraIktatoKonyvek_PostaKonyv.Id = @PostaKonyv_Id
	and PostazasIranya='2' -- kimenő
	and Allapot='06' -- Postázott
	-- and KuldesMod not in ('01', '17') -- Postai sima, Postai elsőbbségi
	--and KimenoKuldemenyFajta in ('01','02','03','04','05','06','07','18','K1_01','K1_02','K1_03','K1_04','K1_05','K1_06','K1_07','K1_08','K1_09','K1_10','K2_01','K2_02','K2_03','K2_04','K2_05','K2_06','K2_07','K2_08','K2_09','K2_10')
	and KimenoKuldemenyFajta in ('01_A','02_A','03_A','04_A','05_A','06_A','07_A','18_A','K1_01_A','K1_02_A','K1_03_A','K1_04_A','K1_05_A','K1_06_A','K1_07_A','K1_08_A','K1_09_A','K1_10_A','K2_01_A','K2_02_A','K2_03_A','K2_04_A','K2_05_A','K2_06_A','K2_07_A','K2_08_A','K2_09_A','K2_10_A')
	and Ajanlott = 0
	and Tertiveveny = 0
	and SajatKezbe = 0
	and E_ertesites = 0
	and E_elorejelzes = 0
	and PostaiLezaroSzolgalat = 0
	and EREC_KuldKuldemenyek.BelyegzoDatuma between IsNull(@StartDate, '1900-01-01') and IsNull(@EndDate, getdate())
	and (@Kikuldo_Id is null or EREC_KuldKuldemenyek.Csoport_Id_Cimzett = @Kikuldo_Id)
	 
	insert into #tmp_belf_kozons_azon
	select 
	  sorszam 
	, alapszolg
	, azonosito
	, cimzett_nev
	, cimzett_irsz
	, cimzett_hely
	, cimzett_kozelebbi_cim
	, cimzett_kozterulet_nev
	, cimzett_kozterulet_jelleg
	, cimzett_hazszam
	, cimzett_epulet
	, cimzett_lepcsohaz
	, cimzett_emelet
	, cimzett_ajto
	, cimzett_postafiok
	, cimzett_cim_id
	, suly
	, kulonszolgok
	, vakok_irasa
	, meret
	, dij
	, gepre_alkalmassag
	from #tmp_kozonseges_azon_with_id 
	where nemzetkozi = '0'

	insert into #tmp_nemz_kozons_azon
	select 
	  sorszam 
	, alapszolg
	, azonosito
	, orszagkod
	, cimzett_nev
	, cimzett_irsz	
	, cimzett_hely	
	, cimzett_kozelebbi_cim
	, suly
	, kulonszolgok
	, vakok_irasa
	, meret
	, dij
    , vam_ertek
	, export_eng
	, valutanem
	, aru_tartalom
	--- <vam_adatok>
	, aru_db
	, aru_nev
	, aru_ertek
	, vam_tarifakod
	, aru_orszag
	, aru_suly
	--- <documents>
	, engedely_szama
	, dok_nev
	from #tmp_kozonseges_azon_with_id 
	where nemzetkozi = '1'
END	

	
/*---------------------------------
Nyilvántartott küldemények
----------------------------------*/

	IF OBJECT_ID('tempdb..#tmp_nyilvantartottkuldemenyek') IS NOT NULL 
	BEGIN
	   DROP TABLE #tmp_nyilvantartottkuldemenyek
	END

	CREATE TABLE #tmp_nyilvantartottkuldemenyek (
		sorszam      int not null,
		azonosito    varchar(16),        -- not null, -- min. 13
		alapszolg    varchar(9),         -- not null, -- min. 1
		suly         varchar(5),         -- not null, -- min. 1
		kulonszolgok varchar(200),
		vakok_irasa  char(1),
		uv_osszeg    varchar(9),         -- min. 1
		uv_lapid     char(13),
		eny_osszeg   varchar(9),         -- min. 1
		kezelesi_mod char(1),
		meret        varchar(2),         -- min. 1
		kezb_mod     varchar(2),         --- "Lehetséges értékek: 'PM', 'HA' (Null is ez), 'PP', 'CS'
		cimzett_nev  nvarchar(400),        -- not null, -- min. 3
		cimzett_irsz varchar(10),         -- not null, -- 0 v. 4	
		cimzett_hely nvarchar(400),        -- not null, -- min. 3
		-- BUG_963
		cimzett_kozelebbi_cim     nvarchar(400),     -- bontott cimet adunk, ez üresen marad (kivéve HRSZ)
		cimzett_kozterulet_nev    nvarchar(400),
		cimzett_kozterulet_jelleg nvarchar(400),
		cimzett_hazszam           nvarchar(400),
		cimzett_epulet            nvarchar(400),
		cimzett_lepcsohaz         varchar(10),
		cimzett_emelet            varchar(10),
		cimzett_ajto              varchar(10),
		cimzett_postafiok         varchar(10),
		--
		cimzett_cim_id            varchar(10),
		cimzett_email             nvarchar(400),     --- "opcionális", vagy min. 6 karakter
		cimzett_telefon           nvarchar(50),     --- "opcionális"
		dij                       varchar(9),         -- not null, -- min. 1 
		sajat_azonosito           varchar(30),
		gepre_alkalmassag         char(1),
		tv_mod					  varchar(10), --opcionális ha FTP-ről jött a térti, akkor 2 ha HKP ről jött a térti, akkor 4 az értéke
		hiv_ertesito              varchar(10) -- hivatalos iratoknál kötelezően töltendő
		/* --- műszaki leírás még nem nyilatkozik ezekről a tag-ekről 
		,
		tv_sajat_jelzes           varchar(50),
		tv_vonalkod               varchar(20),
		tv_vonalkod_tipus         varchar(10),
		hiv_iratszam              varchar(20),
		hiv_irat_fajta            varchar(50),
		hiv_sajat_jelzes          varchar(50),
		hiv_vonalkod              varchar(50),
		hiv_vonalkod_tipus        varchar(10),
		
		*/
	)

	IF OBJECT_ID('tempdb..#tmp_nemz_nyilvantartott') IS NOT NULL 
	BEGIN
	   DROP TABLE #tmp_nemz_nyilvantartott
	END

	CREATE TABLE #tmp_nemz_nyilvantartott (
		sorszam      int not null,
		azonosito    varchar(16),        -- not null, -- min. 13
		alapszolg    varchar(9),         -- not null, -- min. 1
		orszagkod    varchar(3),         -- min. 2
		suly         varchar(5),         -- not null, -- min. 1
		kulonszolgok varchar(200),
		vakok_irasa  char(1),
		uv_osszeg    varchar(9),         -- min. 1
		uv_devizanem varchar(3),         --- "a lehetséges értékei: HUF; EUR; USD"
		eny_osszeg   varchar(9),         -- min. 1
		kezelesi_mod char(1),
		meret        varchar(2),         -- min. 1
		cimzett_nev  nvarchar(400),        -- not null, -- min. 3
		cimzett_irsz varchar(10),         -- not null, -- 0 v. 4	
		cimzett_hely nvarchar(400),        -- not null, -- min. 3
		cimzett_kozelebbi_cim     nvarchar(400),     -- bontott cimet adunk, ez üresen marad (kivéve HRSZ)
		cimzett_email             nvarchar(400),     --- "opcionális", vagy min. 6 karakter
		cimzett_telefon           nvarchar(50),     --- "opcionális"
		dij                       varchar(9),         -- not null, -- min. 1 
		potlapszam                varchar(2),
		sajat_azonosito           varchar(30),
	    vam_ertek      varchar(9),       -- lehet Null 
	    export_eng     varchar(50),      -- lehet Null
	    valutanem      varchar(3),       -- "Lehetséges értékei: HUF, EUR; USD"
	    aru_tartalom   varchar(50),      -- lehet Null
        --- <vam_adatok> element tag-ek                   
        aru_db         varchar(100),
        aru_nev        varchar(100),
        aru_ertek      varchar(100),
        vam_tarifakod  varchar(100),
        aru_orszag     varchar(100),
        aru_suly       varchar(100),
        --- <documents> element tag-ek                                        -- "Opcionális"; típusok, hosszok NEM lettek megadva !?
        engedely_szama varchar(100),
        dok_nev        varchar(100)  			
	)

--- a temp táblát mindenképp létrehozom, mivel a Query-ket a végén - akár üresen is, DE - vissza kell adjam
	IF OBJECT_ID('tempdb..#tmp_nyilvantartottkuldemenyek_with_id') IS NOT NULL 
	BEGIN
	   DROP TABLE #tmp_nyilvantartottkuldemenyek_with_id
	END

	CREATE TABLE #tmp_nyilvantartottkuldemenyek_with_id (
        id           uniqueidentifier,
	    nemzetkozi   char(1),            --- '0' - belföldi; '1' - nemzetközi		
		sorszam      int not null,
		azonosito    varchar(16),        -- not null, -- min. 13
		alapszolg    varchar(9),         -- not null, -- min. 1
		orszagkod    varchar(3),         -- min. 2		
		suly         varchar(5),         -- not null, -- min. 1
		kulonszolgok varchar(200),
		vakok_irasa  char(1),
		uv_osszeg    varchar(9),         -- min. 1
		uv_lapid     char(13),
		uv_devizanem varchar(3),         --- "a lehetséges értékei: HUF; EUR; USD"		
		eny_osszeg   varchar(9),         -- min. 1
		kezelesi_mod char(1),
		meret        varchar(2),         -- min. 1
		kezb_mod     varchar(2),         --- "Lehetséges értékek: 'PM', 'HA' (Null is ez), 'PP', 'CS'
		cimzett_nev  nvarchar(400),        -- not null, -- min. 3
		cimzett_irsz varchar(10),         -- not null, -- 0 v. 4	
		cimzett_hely nvarchar(400),        -- not null, -- min. 3
		-- BUG_963
		cimzett_kozelebbi_cim     nvarchar(400),     -- bontott cimet adunk, ez üresen marad (kivéve HRSZ)
		cimzett_kozterulet_nev    nvarchar(400),
		cimzett_kozterulet_jelleg nvarchar(400),
		cimzett_hazszam           nvarchar(400),
		cimzett_epulet            nvarchar(400),
		cimzett_lepcsohaz         varchar(10),
		cimzett_emelet            varchar(10),
		cimzett_ajto              varchar(10),
		cimzett_postafiok         varchar(10),
		--
		cimzett_cim_id            varchar(10),
		cimzett_email             nvarchar(400),     --- "opcionális", vagy min. 6 karakter
		cimzett_telefon           nvarchar(50),     --- "opcionális"
		dij                       varchar(9),         -- not null, -- min. 1 
		potlapszam                varchar(2),		
		sajat_azonosito           varchar(30),
		gepre_alkalmassag         char(1),
		tv_mod                    varchar(10),--opcionális ha FTP-ről jött a térti, akkor 2 ha HKP ről jött a térti, akkor 4 az értéke
		hiv_ertesito              varchar(10),--hivatalos küldeménynél kötelező A/1 értékkel
		/* --- műszaki leírás még nem nyilatkozik ezekről a tag-ekről 		      
		tv_sajat_jelzes           varchar(50),
		tv_vonalkod               varchar(20),
		tv_vonalkod_tipus         varchar(10),
		hiv_iratszam              varchar(20),
		hiv_irat_fajta            varchar(50),
		hiv_sajat_jelzes          varchar(50),
		hiv_vonalkod              varchar(50),
		hiv_vonalkod_tipus        varchar(10),
		
		*/
	    vam_ertek      varchar(9),       -- lehet Null 
	    export_eng     varchar(50),      -- lehet Null
	    valutanem      varchar(3),       -- "Lehetséges értékei: HUF, EUR; USD"
	    aru_tartalom   varchar(50),      -- lehet Null
        --- <vam_adatok> element tag-ek                   
        aru_db         varchar(100),
        aru_nev        varchar(100),
        aru_ertek      varchar(100),
        vam_tarifakod  varchar(100),
        aru_orszag     varchar(100),
        aru_suly       varchar(100),
        --- <documents> element tag-ek                                        -- "Opcionális"; típusok, hosszok NEM lettek megadva !?
        engedely_szama varchar(100),
        dok_nev        varchar(100)  			
	)
  
declare @ETERTI_ERKEZTETES_MOD varchar(100)
set @ETERTI_ERKEZTETES_MOD = (select Ertek from INT_Parameterek where nev = 'EPOSTAI_TERTIVEVENY.ERKEZTETES_MOD')

declare @HIV_ERTESITO_DEFAULT_ERTEK  varchar(10)
set @HIV_ERTESITO_DEFAULT_ERTEK = 'A/1' --A postával történő levelés(2020.06.19.) alapján hivatalos iratok esetében kötelező és a polgármesteri hivataloknál A/1 az értéke
if @IgnoreNyilvantartott != '1'
BEGIN
    --- bár bejöttek új adatmezők, DE nem módosítottam az adatgyűjtésen
	---    csak a row_number() lett beépítve NEMZETKOZI alapján; ill. NEMZETKOZI mezőt töltöm
	insert into #tmp_nyilvantartottkuldemenyek_with_id
	select
	 EREC_KuldKuldemenyek.Id
	, nemzetkozi = case when IsNull(KRT_Orszagok.Kod, 'HU') = 'HU' then '0'	
	                    when KRT_Orszagok.Viszonylatkod is NULL    then '0'
						else '1'            -- Europai - K1, Egyéb külföld - K2; vagy Null-- Europai K1, Egyéb K2
				   end
	/* , ( IsNull( ( select Max( cast(sorszam as int) ) from #tmp_kozonsegeskuldemenyek), 0 )+ row_number() over (order by case SUBSTRING(Ragszam, 1, 2) */
	, row_number() over ( PARTITION by case when IsNull(KRT_Orszagok.Kod, 'HU') = 'HU' then '0'	
	                                        when KRT_Orszagok.Viszonylatkod is NULL    then '0'
						                    else '1'            -- Europai - K1, Egyéb külföld - K2; vagy Null-- Europai K1, Egyéb K2
				                       end                      -- nemzetkozi adatmező szerint
	                          order by case SUBSTRING(Ragszam, 1, 2)
		                                    when 'EL' then 101  -- 1. Tételesen kezelt küldemények
		                                    when 'RB' then 102  -- 1. Tételesen kezelt küldemények
		                                    when 'VV' then 103  -- 1. Tételesen kezelt küldemények
		                                    when 'RL' then 201  -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
		                                    when 'TRL' then 202 -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
		                                    when 'KR' then 203  -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
		                                    when 'RR' then 204  -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
		                                    else null 
									   end, RagSzam ) as sorszam      -- sorszam
	, cast( RagSzam as varchar(16) ) azonosito
	        -- sorrendkod = case SUBSTRING(Ragszam, 1, 2)  -- segédmező a rendezéshez, nem adjuk vissza
	                      --	when 'EL' then 101 -- 1. Tételesen kezelt küldemények
	                      --	when 'RB' then 102 -- 1. Tételesen kezelt küldemények
	                      --	when 'VV' then 103 -- 1. Tételesen kezelt küldemények
	                      --	when 'RL' then 201 -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
	                      --	when 'TRL' then 202 -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
	                      --	when 'KR' then 203 -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
	                      --	when 'RR' then 204 -- 2. Könyvelt küldemények (ajánlott, tértivevényes, e-tértivevényes, utánvételes, saját kézbe)
	                      --	else null
	                      --end
	, alapszolg = case
		when KimenoKuldemenyFajta in ('01','02','03','04','05','06','07','K1_01','K1_02','K1_03','K1_04','K1_05','K1_06','K1_07','K1_08','K1_09','K2_01','K2_02','K2_03','K2_04','K2_05','K2_06','K2_07','K2_08','K2_09') then 'A_111_LEV'
		--when KimenoKuldemenyFajta in ('18','K1_10','K2_10') then 'A_111_LLP'
		-- BUG_1353
		--when KimenoKuldemenyFajta in ('19','20') then 'A_15_HIV'
		when KimenoKuldemenyFajta in ('08','09','10','11','19','20') then 'A_15_HIV'
		else null
		end
	, orszagkod = case IsNull(KRT_Orszagok.Kod, 'HU')
		when 'HU' then null
		else KRT_Orszagok.Kod
	end
	--, suly = case
	--	when KimenoKuldemenyFajta='01' then 30
	--	when KimenoKuldemenyFajta in ('02','K1_03','K2_03') then 50
	--	when KimenoKuldemenyFajta in ('03','K1_04','K2_04') then 100
	--	when KimenoKuldemenyFajta in ('04','K1_05','K2_05') then 250
	--	when KimenoKuldemenyFajta in ('05','K1_06','K2_06') then 500
	--	when KimenoKuldemenyFajta='06' then 750
	--	when KimenoKuldemenyFajta in ('07','K1_09','K2_09') then 2000
	--	when KimenoKuldemenyFajta='18' then
	--		case when IsNull(KRT_Orszagok.Kod, 'HU') = 'HU' then 30
	--		else 20
	--		end
	--	-- BUG_1353
	--	--when KimenoKuldemenyFajta='19' then 30 -- valódi súly?
	--	--when KimenoKuldemenyFajta='20' then 30 -- valódi súly
	--	when KimenoKuldemenyFajta in ('09','11','19') then 30 -- valódi súly?
	--	when KimenoKuldemenyFajta in ('08','10','20') then 30 -- valódi súly
	--	when KimenoKuldemenyFajta in ('K1_01','K2_01','K1_02','K2_02','K1_10','K2_10') then 20
	--	when KimenoKuldemenyFajta in ('K1_07','K2_07') then 1000
	--	when KimenoKuldemenyFajta in ('K1_08','K2_08') then 1500
	--	else null -- valódi súly, itt: hiba!!!
	--	end
	, kt_kuldemenySulya.kod as suly
	, kulonszolgok = stuff((select
	 case Elsobbsegi
		when 1 then ',K_PRI' -- K_FEL
		else ''
		end
	+
	case 
		when Ajanlott=1 then ',K_AJN'
		--BUG_1353
		--when KimenoKuldemenyFajta in ('19', '20') then  ',K_AJN' -- Hivatalos irat
		when KimenoKuldemenyFajta in ('08','09','10','11','19', '20') then  ',K_AJN' -- Hivatalos irat
		else ''
		end
	+
	case 
		when Tertiveveny=1 then ',K_TEV'
		-- BUG_1353
		--when KimenoKuldemenyFajta in ('19', '20') then  ',K_TEV' -- Hivatalos irat
		when KimenoKuldemenyFajta in ('08','09','10','11','19', '20') then  ',K_TEV' -- Hivatalos irat

		else ''
		end
	+
	case PostaiLezaroSzolgalat
		when 1 then ',K_LEZ'
		else ''
		end
	+
	case 
		when SajatKezbe=1 then ',K_SKZ'
		when KimenoKuldemenyFajta = '20' then ',K_SKZ' -- Hivatalos irat saját kézbe
		else ''
		end
	+
	case E_ertesites
		when 1 then ',K_EFF'
		else ''
		end
	+
	case E_elorejelzes
		when 1 then ',K_EFC'
		else ''
		end
	for xml PATH('')),1,1,'' )
	, null vakok_irasa
	, null uv_osszeg
	, null uv_lapid
	, Null uv_devizanem
	, eny_osszeg = case Erteknyilvanitas when '1' then ErteknyilvanitasOsszege else null end
	, kezelesi_mod = case Erteknyilvanitas when '1' then '3' else null end
	, meret = case
		when KimenoKuldemenyFajta in ('01','K1_01','K2_01') then 1
		when KimenoKuldemenyFajta in ('18','K1_10','K2_10') then null
		-- BUG_1353
		--when KimenoKuldemenyFajta in ('19','20') then null -- A_15_HIV mellett nem szerepelhet
		when KimenoKuldemenyFajta in ('08','09','10','11','19','20') then null -- A_15_HIV mellett nem szerepelhet
		else 0
		end
	, Null kezb_mod
	, cast(NevSTR_Bekuldo as nvarchar(400)) cimzett_nev	
	, cimzett_irsz = case IsNull(KRT_Orszagok.Kod, 'HU')
		when 'HU' then cast(KRT_Cimek.IRSZ as varchar(4))
		-- BLG_1938
		--else null --'' -- kötelező
      else cast(KRT_Cimek.IRSZ as varchar(10))
	end
	, cast(ISNULL(KRT_Cimek.TelepulesNev,'  ') as nvarchar(400)) cimzett_hely  -- min 2 karakter
	
	--, cast((KRT_Cimek.KozteruletNev + ' ' + KRT_Cimek.KozteruletTipusNev + ' ' + KRT_Cimek.Hazszam) as varchar(45)) cimzett_kozelebbi_cim
	-- BUG_963
	--,cast(dbo.fn_MergeCim(KRT_Cimek.Tipus,
	--	   null,
	--	   null,
	--	   KRT_Cimek.TelepulesNev,
	--	   KRT_Cimek.KozteruletNev,
	--	   KRT_Cimek.KozteruletTipusNev,
	--	   KRT_Cimek.Hazszam,
	--	   KRT_Cimek.Hazszamig,
	--	   KRT_Cimek.HazszamBetujel,
	--	   KRT_Cimek.Lepcsohaz,
	--	   KRT_Cimek.Szint,
	--	   KRT_Cimek.Ajto,
	--	   KRT_Cimek.AjtoBetujel,
	--	   KRT_Cimek.CimTobbi) as varchar(45)) cimzett_kozelebbi_cim
	-- BLG_1938
	--, Cast(CASE WHEN KRT_Cimek.HRSZ is NULL OR KRT_Cimek.HRSZ='' THEN '' ELSE 
	--	   CASE WHEN KRT_Cimek.TelepulesNev is NULL OR KRT_Cimek.TelepulesNev = '' THEN '' ELSE KRT_Cimek.TelepulesNev + ' HRSZ.' + KRT_Cimek.HRSZ END 
	--	   END as varchar(45)) cimzett_kozelebbi_cim
	, CASE when IsNull(KRT_Orszagok.Kod, 'HU') = 'HU'  or  KRT_Orszagok.Viszonylatkod is  NULL  THEN 
			Cast( CASE WHEN KRT_Cimek.HRSZ is NULL OR KRT_Cimek.HRSZ='' THEN '' 
	             ELSE CASE WHEN KRT_Cimek.TelepulesNev is NULL OR KRT_Cimek.TelepulesNev = '' THEN '' 
				           ELSE KRT_Cimek.TelepulesNev + ' HRSZ.' + KRT_Cimek.HRSZ 
					  END 
		    END as nvarchar(400) ) 
	   ELSE 		
			 Cast( CASE WHEN KRT_Cimek.HRSZ is NULL OR KRT_Cimek.HRSZ='' THEN 
					dbo.fn_MergeKozelebbi_Cim(KRT_Cimek.Tipus,
								   KRT_Cimek.KozteruletNev,
								   KRT_Cimek.KozteruletTipusNev,
								   KRT_Cimek.Hazszam,
								   KRT_Cimek.Hazszamig,
								   KRT_Cimek.HazszamBetujel,
								   KRT_Cimek.Lepcsohaz,
								   KRT_Cimek.Szint,
								   KRT_Cimek.Ajto,
								   KRT_Cimek.AjtoBetujel,
								   -- BLG_1347
								   KRT_Cimek.HRSZ,
								   KRT_Cimek.CimTobbi) 
	             ELSE CASE WHEN KRT_Cimek.TelepulesNev is NULL OR KRT_Cimek.TelepulesNev = '' THEN '' 
				           ELSE KRT_Cimek.TelepulesNev + ' HRSZ.' + KRT_Cimek.HRSZ 
					  END 
		    END as nvarchar(400) ) 
		END  cimzett_kozelebbi_cim
 
	, Cast(KRT_Cimek.KozteruletNev as nvarchar(400)) cimzett_kozterulet_nev
	, Cast(KRT_Cimek.KozteruletTipusNev as nvarchar(400)) cimzett_kozterulet_jelleg
	, Cast(CASE WHEN KRT_Cimek.Tipus = '02' THEN NULL ELSE KRT_Cimek.Hazszam + CASE WHEN  KRT_Cimek.Hazszamig is NULL OR  KRT_Cimek.Hazszamig = '' THEN '' ELSE ' - ' +  KRT_Cimek.Hazszamig END END as varchar(30)) cimzett_hazszam
	, Cast(KRT_Cimek.HazszamBetujel as varchar(10)) cimzett_epulet
	, Cast(KRT_Cimek.Lepcsohaz as varchar(10)) cimzett_lepcsohaz
	, Cast(KRT_Cimek.Szint as varchar(10)) cimzett_emelet
	, Cast(KRT_Cimek.Ajto + CASE WHEN KRT_Cimek.AjtoBetujel is NULL OR KRT_Cimek.AjtoBetujel = '' THEN '' ELSE ' / ' + KRT_Cimek.AjtoBetujel END as varchar(10)) cimzett_ajto
	, CAST(CASE WHEN KRT_Cimek.Tipus = '02' THEN KRT_Cimek.Hazszam ELSE NULL END as varchar(10)) cimzett_postafiok
	--
    , Null cimzett_cim_id
	, Null cimzett_email
	, Null cimzett_telefon
	, cast(Ar as int) dij
	, cast(null as varchar(2)) potlapszam -- K_POT
	, cast(null as varchar(30)) sajat_azonosito -- case K_ETV különszolg when 1 then  ...  else null
	, cast(null as char(1)) gepre_alkalmassag -- case K_FEL különszolg  when 1 then case ... 'E' v. 'G' else null
	/* --- műszaki leírás még nem nyilatkozik ezekről a tag-ekről 	
		tv_sajat_jelzes           varchar(50),
		tv_vonalkod               varchar(20),
		tv_vonalkod_tipus         varchar(10),
		hiv_iratszam              varchar(20),
		hiv_irat_fajta            varchar(50),
		hiv_sajat_jelzes          varchar(50),
		hiv_vonalkod              varchar(50),
		hiv_vonalkod_tipus        varchar(10),		
	*/
	, CAST(CASE 		
		WHEN @ETERTI_ERKEZTETES_MOD LIKE '%SFTP%'	THEN '2'
		WHEN @ETERTI_ERKEZTETES_MOD LIKE '%FTPS%'	THEN '2'
		WHEN @ETERTI_ERKEZTETES_MOD LIKE '%HKP%'	THEN '4'
		ELSE NULL 
		END as varchar(10)) tv_mod	--opcionális ha FTP-ről jött a térti, akkor 2 ha HKP ről jött a térti, akkor 4 az értéke
	, kt_HivatalosKuldemenyTipusa.Kod as  hiv_ertesito
	--, '0' vam_ertek -- 8.3 xsd alapján kötelező
	, Null vam_ertek -- 8.7 xsd alapján LZS - 2020.10.28. BLG_15517
    , Null export_eng
    --, 'HUF' valutanem -- 8.3 xsd alapján kötelező
	, Null valutanem -- 8.7 xsd alapján LZS - 2020.10.28. BLG_15517
    , Null aru_tartalom
    --- <vam_adatok> element tag-ek                   
    , Null aru_db
    , Null aru_nev
    , Null aru_ertek
	, Null vam_tarifakod
	, Null aru_orszag
	, Null aru_suly
    --- <documents> element tag-ek                                        -- "Opcionális"; típusok, hosszok NEM lettek megadva !?
	, Null engedely_szama
	, Null dok_nev
	from dbo.EREC_KuldKuldemenyek as EREC_KuldKuldemenyek
	left join dbo.EREC_IraIktatoKonyvek as EREC_IraIktatoKonyvek_Postakonyv on EREC_IraIktatoKonyvek_Postakonyv.Id = EREC_KuldKuldemenyek.IraIktatokonyv_Id
	left join dbo.KRT_Cimek as KRT_Cimek on KRT_Cimek.Id=EREC_KuldKuldemenyek.Cim_Id
	left join dbo.KRT_Orszagok as KRT_Orszagok on KRT_Orszagok.Id=KRT_Cimek.Orszag_Id
		-- BUG_8613
	left join dbo.KRT_KodCsoportok as kcs_KuldemenyFajta on kcs_KuldemenyFajta.kod ='KIMENO_KULDEMENY_FAJTA'
	left join dbo.KRT_KodTarak as kt_KuldemenyFajta on kt_KuldemenyFajta.kodcsoport_Id= kcs_kuldemenyFajta.id and kt_kuldemenyFajta.kod COLLATE Hungarian_CI_AS = KimenoKuldemenyFajta and EREC_KuldKuldemenyek.BelyegzoDatuma between kt_kuldemenyFajta.ervkezd and kt_kuldemenyFajta.ervvege
	left join dbo.fn_GetKodtarFuggosegAktiv('4ADBD31F-ACA9-4AFF-B1BD-372453FE7A70') as fk_KuldemenyFajta on fk_KuldemenyFajta.VezerloKodTarId=kt_KuldemenyFajta.Id
	left join dbo.KRT_KodCsoportok as kcs_KuldemenySulya on kcs_KuldemenySulya.kod ='KIMENOKULDEMENY_SULY'
	left join dbo.KRT_KodTarak as kt_KuldemenySulya on kt_KuldemenySulya.kodcsoport_Id= kcs_kuldemenySulya.id and kt_kuldemenySulya.id= fk_KuldemenyFajta.FuggoKodTarId and EREC_KuldKuldemenyek.BelyegzoDatuma between kt_kuldemenySulya.ervkezd and kt_kuldemenySulya.ervvege
	--hiv ertesito fuggo kodtarbol
	left join dbo.fn_GetKodtarFuggosegAktiv('0021581B-1E23-423A-8F06-B829DA5700E0') as fk_HivErtesito on fk_HivErtesito.VezerloKodTarId=kt_KuldemenyFajta.Id
	left join dbo.KRT_KodCsoportok as kcs_HivatalosErtesitoTipus on kcs_HivatalosErtesitoTipus.kod ='POSTA_HIV_ERTESITO_TIPUS'
	left join dbo.KRT_KodTarak as kt_HivatalosKuldemenyTipusa on kt_HivatalosKuldemenyTipusa.kodcsoport_Id= kcs_HivatalosErtesitoTipus.id and kt_HivatalosKuldemenyTipusa.id= fk_HivErtesito.FuggoKodTarId 

	where
	EREC_IraIktatoKonyvek_PostaKonyv.IktatoErkezteto='P'
	and EREC_IraIktatoKonyvek_PostaKonyv.Id = @PostaKonyv_Id
	and PostazasIranya='2' -- kimenő
	and Allapot='06' -- Postázott
	--and KuldesMod in ('01', '17') -- Postai sima, Postai elsőbbségi
	-- BUG_1353
	--and KimenoKuldemenyFajta in ('01','02','03','04','05','06','07','18','19','20','K1_01','K1_02','K1_03','K1_04','K1_05','K1_06','K1_07','K1_08','K1_09','K1_10','K2_01','K2_02','K2_03','K2_04','K2_05','K2_06','K2_07','K2_08','K2_09','K2_10')
	--and KimenoKuldemenyFajta in ('01','02','03','04','05','06','07','08','09','10','11','18','19','20','K1_01','K1_02','K1_03','K1_04','K1_05','K1_06','K1_07','K1_08','K1_09','K1_10','K2_01','K2_02','K2_03','K2_04','K2_05','K2_06','K2_07','K2_08','K2_09','K2_10')
	and KimenoKuldemenyFajta in ('01','02','03','04','05','06','07','08','09','10','11','19','20','K1_01','K1_02','K1_03','K1_04','K1_05','K1_06','K1_07','K1_08','K1_09','K2_01','K2_02','K2_03','K2_04','K2_05','K2_06','K2_07','K2_08','K2_09')
	and 1 = case
		-- BUG_1353
		--when KimenoKuldemenyFajta in ('19', '20') then 1
		when KimenoKuldemenyFajta in ('08','09','10','11','19', '20') then 1
		when IsNull(Ajanlott, 0) + IsNull(Tertiveveny, 0) + IsNull(SajatKezbe, 0) + IsNull(E_ertesites, 0) + IsNull(E_elorejelzes, 0) + IsNull(PostaiLezaroSzolgalat, 0) > 0 then 1
		else 0
	end
	and EREC_KuldKuldemenyek.BelyegzoDatuma between IsNull(@StartDate, '1900-01-01') and IsNull(@EndDate, getdate())
	and (@Kikuldo_Id is null or EREC_KuldKuldemenyek.Csoport_Id_Cimzett = @Kikuldo_Id)

	insert into #tmp_nyilvantartottkuldemenyek
	select 
	  sorszam
	, azonosito              -- azonosito - kötelező
	, alapszolg
	, suly
	, kulonszolgok
	, vakok_irasa
	, uv_osszeg
	, uv_lapid
	, eny_osszeg
	, kezelesi_mod
	, meret
	, kezb_mod
	, cimzett_nev            -- kotelezo
	, cimzett_irsz           -- cimzett_irsz
	, cimzett_hely           -- cimzett_hely -- kotelezo
	-- BUG_963
	, cimzett_kozelebbi_cim
	, cimzett_kozterulet_nev
	, cimzett_kozterulet_jelleg
	, cimzett_hazszam
	, cimzett_epulet
	, cimzett_lepcsohaz
	, cimzett_emelet
	, cimzett_ajto
	, cimzett_postafiok
	--
    , cimzett_cim_id
    , cimzett_email
	, cimzett_telefon
	, dij                    -- kotelezo
	, sajat_azonosito
	, gepre_alkalmassag
	, tv_mod --opcionális ha FTP-ről jött a térti, akkor 2 ha HKP ről jött a térti, akkor 4 az értéke
	, hiv_ertesito --hivatalos iratnál kötelező
	/* --- műszaki leírás még nem nyilatkozik ezekről a tag-ekről 
		, tv_sajat_jelzes
		, tv_vonalkod
		, tv_vonalkod_tipus
		, hiv_iratszam
		, hiv_irat_fajta
		, hiv_sajat_jelzes
		, hiv_vonalkod
		, hiv_vonalkod_tipus
		
	*/
	from #tmp_nyilvantartottkuldemenyek_with_id                       -- Nyilvantartottkuldemenyek
	where nemzetkozi = '0'	
	--order by sorszam
	
	insert into #tmp_nemz_nyilvantartott
	select 
	  sorszam
	, azonosito              -- azonosito - kötelező
	, alapszolg
	, orszagkod
	, suly
	, kulonszolgok
	, vakok_irasa
	, uv_osszeg
	, uv_devizanem
	, eny_osszeg
	, kezelesi_mod
	, meret
	, cimzett_nev            -- kotelezo
	, cimzett_irsz           -- cimzett_irsz
	, cimzett_hely           -- cimzett_hely -- kotelezo
	, cimzett_kozelebbi_cim
	, cimzett_email
	, cimzett_telefon
	, dij                    -- kotelezo
	, potlapszam
	, sajat_azonosito
    , vam_ertek
	, export_eng
	, valutanem
	, aru_tartalom
	--- <vam_adatok>
	, aru_db
	, aru_nev
	, aru_ertek
	, vam_tarifakod
	, aru_orszag
	, aru_suly
	--- <documents>
	, engedely_szama
	, dok_nev
	from #tmp_nyilvantartottkuldemenyek_with_id                       -- Nyilvantartottkuldemenyek
	where nemzetkozi = '1'
	--order by sorszam	
END

/*-----------------------------------------------------------------------
EREDMENYEK VISSZAADASA
-------------------------------------------------------------------------*/
if IsNull(@WindowSize, 0) <= 0
begin
	set @WindowSize = 100000
end

if IsNull(@StartPage, 0) <= 0
begin
	set @StartPage = 1
end

declare @cnt_belf_kozonseges  int = ( select count(1) from #tmp_kozonsegeskuldemenyek )
declare @cnt_nemz_kozonseges  int = ( select count(1) from #tmp_nemz_kozonseges )
declare @cnt_kozonseges       int = @cnt_belf_kozonseges + @cnt_nemz_kozonseges

declare @cnt_belf_kozons_azon int = ( select count(1) from #tmp_belf_kozons_azon )
declare @cnt_nemz_kozons_azon int = ( select count(1) from #tmp_nemz_kozons_azon )
declare @cnt_kozonseges_azon  int = @cnt_belf_kozons_azon + @cnt_nemz_kozons_azon

declare @cnt_belf_nyilv       int = ( select count(1) from #tmp_nyilvantartottkuldemenyek )
declare @cnt_nemz_nyilv       int = ( select count(1) from #tmp_nemz_nyilvantartott )   
declare @cnt_nyilvantartott   int = @cnt_belf_nyilv + @cnt_nemz_nyilv

declare @cnt_all              int = @cnt_kozonseges + @cnt_kozonseges_azon + @cnt_nyilvantartott

/* --- folyamatos sorszámok beállítása - ezt LE KELL OLTANI, ha szekciónként történik a SORSZAM átadás
   az adatátadás sorrendje
     <jegyzek_adat>1
         1. belfoldi kozonseges 
	     2. nemzetkozi kozonseges		 
     <jegyzek_adat>2		 
	     3. belfoldi kozonseges azonositott
	     4. belfoldi nyilvántartott
	     5. nemzetkozi kozonseges azonositott
	     6. nemzetkozi nyilvántartott
*/	  
--- belföldi közönséges az 1. szekció, így ott jó a SORSZAM
--- nemzetkozi kozonseges
update #tmp_nemz_kozonseges
   SET sorszam = sorszam + @cnt_belf_kozonseges
   
--- belfoldi kozonseges azonositott
update #tmp_belf_kozons_azon
   SET sorszam = sorszam + @cnt_kozonseges
update #tmp_nyilvantartottkuldemenyek
   SET sorszam = sorszam + @cnt_kozonseges + @cnt_belf_kozons_azon
update #tmp_nemz_kozons_azon
   SET sorszam = sorszam + @cnt_kozonseges + @cnt_belf_kozons_azon + @cnt_belf_nyilv
update #tmp_nemz_nyilvantartott
   SET sorszam = sorszam + @cnt_kozonseges + @cnt_kozonseges_azon + @cnt_belf_nyilv
/* --- */
 
declare @mutato_start int
declare @mutato_end int
set @mutato_start = (@StartPage - 1) * @WindowSize + 1
if @StartPage > 1
begin
	set @cnt_all = @cnt_all - @mutato_start + 1
end

if @ForXml = '0'
begin

	-- mindent visszadunk es nem modositjuk a sorszamot ( mert az már fent megtörtént )
	set @mutato_end = @mutato_start - 1 + @WindowSize

	-- lapozas
	declare @firstRow int
	declare @lastRow int

	if (@pageSize > 0 and @pageNumber > -1)
	begin
		set @firstRow = @pageNumber * @pageSize
		set @lastRow = @firstRow + @pageSize - 1

		if (@mutato_start + @lastRow < @mutato_end)
		begin
			set @mutato_end = @mutato_start + @lastRow
		end
		set @mutato_start = @mutato_start + @firstRow
	end
--
--	select @WindowSize as WindowWidth
--	, @cnt_kozonseges as cnt_kozonseges
--	, @cnt_nyilvantartott as cnt_nyilvantartott
--	, @cnt_all as cnt_all
--	, @mutato_start as mutato_start
--	, @mutato_end as mutato_end
--	, @pageSize as pageSize
--	, @pageNumber as pageNumber
----	, @pagecountall as pagecountall
----	--, @pagecounter as pagecounter
----	, @PageCount as PageCount

	-- lapozas info
	select @cnt_all as RecordNumber, @pageNumber as PageNumber;

	--- belfoldi és nemzetkozi kozonseges a <jegyzek_adat>1 -be; SORSZAM -ok feljebb elrendezve
	--- mindenképp vissza kell adni, ha egy sora sincs, akkor is    -- if @IgnoreKozonseges != '1'
	BEGIN
		select /*'.' as 'Belfoldi_kozonseges',*/ #tmp_kozonsegeskuldemenyek.*, tmp.ids from #tmp_kozonsegeskuldemenyek
			cross apply ( select STUFF( ( SELECT ';' + convert(varchar(36), Id) from #tmp_kozonsegeskuldemenyek_with_id
				                          where #tmp_kozonsegeskuldemenyek.sorszam=#tmp_kozonsegeskuldemenyek_with_id.sorszam
										    and #tmp_kozonsegeskuldemenyek_with_id.nemzetkozi = '0'
				                            FOR XML PATH('')
									    ),1, 1, ''
									  ) as ids
						) tmp
		 where 1 = 1
		   and #tmp_kozonsegeskuldemenyek.sorszam between @mutato_start and @mutato_end
	     order by #tmp_kozonsegeskuldemenyek.sorszam
		 ---
		select /*'.' as 'Nemzetkozi_kozonseges',*/ #tmp_nemz_kozonseges.*, tmp.ids from #tmp_nemz_kozonseges
			cross apply ( select STUFF( ( SELECT ';' + convert(varchar(36), Id) from #tmp_kozonsegeskuldemenyek_with_id
				                          where #tmp_nemz_kozonseges.sorszam=#tmp_kozonsegeskuldemenyek_with_id.sorszam + @cnt_belf_kozonseges
										    and #tmp_kozonsegeskuldemenyek_with_id.nemzetkozi = '1'
				                            FOR XML PATH('')
									    ),1, 1, ''
									  ) as ids
						) tmp
		 where 1 = 1
		   and #tmp_nemz_kozonseges.sorszam between @mutato_start and @mutato_end
	     order by #tmp_nemz_kozonseges.sorszam		 
	END

	--- belfoldi kozonseges azonositott a <jegyzek_adat>2 -be; SORSZAM -ok feljebb elrendezve
	--- mindenképp vissza kell adni, ha egy sora sincs, akkor is    -- if @IgnoreKozonseges != '1'
	BEGIN
		select /*'.' as 'Belfoldi_kozonseges_azonositott',*/ #tmp_belf_kozons_azon.*, tmp.ids from #tmp_belf_kozons_azon
			cross apply ( select STUFF( ( SELECT ';' + convert(varchar(36), Id) from #tmp_kozonseges_azon_with_id
				                          where #tmp_belf_kozons_azon.sorszam = #tmp_kozonseges_azon_with_id.sorszam + @cnt_kozonseges
										    and #tmp_kozonseges_azon_with_id.nemzetkozi = '0'
				                            FOR XML PATH('')
									    ),1, 1, ''
									  ) as ids
						) tmp
		 where 1 = 1
		   and #tmp_belf_kozons_azon.sorszam between @mutato_start and @mutato_end
	     order by #tmp_belf_kozons_azon.sorszam		 
	END
	--- belfoldi nyilvántartott a <jegyzek_adat>2 -be; SORSZAM -ok feljebb elrendezve
	--- mindenképp vissza kell adni, ha egy sora sincs, akkor is    -- if @IgnoreNyilvantartott != '1'
	BEGIN
		select /*'.' as 'Belfoldi_nyilvantartott',*/ #tmp_nyilvantartottkuldemenyek.*, tmp.ids from #tmp_nyilvantartottkuldemenyek
			cross apply ( select STUFF( ( SELECT ';' + convert(varchar(36), Id) from #tmp_nyilvantartottkuldemenyek_with_id
			 						 	  where #tmp_nyilvantartottkuldemenyek.sorszam = #tmp_nyilvantartottkuldemenyek_with_id.sorszam + @cnt_kozonseges + @cnt_belf_kozons_azon
			 	                             and #tmp_nyilvantartottkuldemenyek_with_id.nemzetkozi = '0'
				                            FOR XML PATH('')
									    ),1, 1, ''
									  ) as ids
						) tmp
		 where 1 = 1
		   and #tmp_nyilvantartottkuldemenyek.sorszam between @mutato_start and @mutato_end
	     order by #tmp_nyilvantartottkuldemenyek.sorszam		 
	END
	--- nemzetkozi kozonseges azonositott a <jegyzek_adat>2 -be; SORSZAM -ok feljebb elrendezve
	--- mindenképp vissza kell adni, ha egy sora sincs, akkor is    -- if @IgnoreKozonseges != '1'
	BEGIN
		select /*'.' as 'Nemzetkozi_kozonseges_azonosított',*/ #tmp_nemz_kozons_azon.*, tmp.ids from #tmp_nemz_kozons_azon
			cross apply ( select STUFF( ( SELECT ';' + convert(varchar(36), Id) from #tmp_kozonseges_azon_with_id
									 	  -- BLG_1938
										  where #tmp_nemz_kozons_azon.sorszam = #tmp_kozonseges_azon_with_id.sorszam +  @cnt_kozonseges + @cnt_belf_kozons_azon + @cnt_belf_nyilv
				                             and #tmp_kozonseges_azon_with_id.nemzetkozi = '1'
				                            FOR XML PATH('')
									    ),1, 1, ''
									  ) as ids
						) tmp
		 where 1 = 1
		   and #tmp_nemz_kozons_azon.sorszam between @mutato_start and @mutato_end
	     order by #tmp_nemz_kozons_azon.sorszam		 
	END
	--- nemzetkozi nyilvántartott a <jegyzek_adat>2 -be; SORSZAM -ok feljebb elrendezve
	--- mindenképp vissza kell adni, ha egy sora sincs, akkor is    -- if @IgnoreNyilvantartott != '1'
	BEGIN
		select /*'.' as 'Nemzetkozi_nyilvantartott',*/ #tmp_nemz_nyilvantartott.*, tmp.ids from #tmp_nemz_nyilvantartott
			cross apply ( select STUFF( ( SELECT ';' + convert(varchar(36), Id) from #tmp_nyilvantartottkuldemenyek_with_id
				                          where #tmp_nemz_nyilvantartott.sorszam = #tmp_nyilvantartottkuldemenyek_with_id.sorszam + @cnt_kozonseges + @cnt_kozonseges_azon + @cnt_belf_nyilv
										    and #tmp_nyilvantartottkuldemenyek_with_id.nemzetkozi = '1'
				                            FOR XML PATH('')
									    ),1, 1, ''
									  ) as ids
						) tmp
		 where 1 = 1
		   and #tmp_nemz_nyilvantartott.sorszam between @mutato_start and @mutato_end
	     order by #tmp_nemz_nyilvantartott.sorszam		 
	END
	
end
else                                       --- @ForXML = '1'
--- ez az ág jelenleg nincs használatban, !!! ezért nem történt meg az átírása a kiterjesztett struktúrára (2018.03.05 -i változtatásokra)
begin

	declare @pagecounter int
	set @pagecounter = 1

	set @mutato_end = @mutato_start - 1 + case when @cnt_all < @WindowSize then @cnt_all
			                                   else @WindowSize
		                                  end

--		select @WindowSize as WindowWidth
--		, @cnt_kozonseges as cnt_kozonseges
--		, @cnt_nyilvantartott as cnt_nyilvantartott
--		, @cnt_all as cnt_all
--		, @mutato_start as mutato_start
--		, @mutato_end as mutato_end
--		, @pagecountall as pagecountall
--		, @pagecounter as pagecounter
--		, @PageCount as PageCount

	-- lapozas info
	select @cnt_all as RecordNumber, @pageNumber as PageNumber;

		select cast( ( case when ( @cnt_kozonseges >= @mutato_start ) 
					        then ( select cast ( ( select (sorszam - @mutato_start + 1) as sorszam
		                                                , alapszolg
		                                                , viszonylat
		                                                , orszagkod
		                                                , suly
		                                                , darab
		                                                , kulonszolgok
		                                                , vakok_irasa
		                                                , meret
		                                                , dij
		                                                , gepre_alkalmassag
		                                             from #tmp_kozonsegeskuldemenyek 
													where sorszam between @mutato_start and @mutato_end 
													order by sorszam 
													  FOR xml PATH('kozonseges_tetel') 
												 ) as xml
											   ) 
								      FOR xml PATH('kozonseges_tetelek')
								 )
			                else '' 
					   end
			           +
			           case when ( @cnt_kozonseges < @mutato_end ) 
					        then ( select cast ( ( select (sorszam - @mutato_start + 1) as sorszam
		                                                , azonosito
		                                                , alapszolg
		                                                , orszagkod
		                                                , suly
		                                                , kulonszolgok
		                                                , vakok_irasa
		                                                , uv_osszeg
		                                                , uv_lapid
		                                                , eny_osszeg
		                                                , kezelesi_mod
		                                                , meret
		                                                , cimzett_nev
		                                                , cimzett_irsz
		                                                , cimzett_hely
		                                             -- BUG_963
		                                                , cimzett_kozelebbi_cim
		                                                , cimzett_kozterulet_nev
		                                                , cimzett_kozterulet_jelleg
		                                                , cimzett_hazszam
		                                                , cimzett_epulet
		                                                , cimzett_lepcsohaz
		                                                , cimzett_emelet
		                                                , cimzett_ajto
		                                                , cimzett_postafiok
		                                             --
		                                                , dij
		                                                , potlapszam
		                                                , sajat_azonosito
		                                                , gepre_alkalmassag
		                                             from #tmp_nyilvantartottkuldemenyek 
													where sorszam between @mutato_start and @mutato_end 
													order by sorszam 
													  FOR xml PATH('nyilvantartott_tetel')
												 ) as xml
											   ) 
									  FOR xml PATH('nyilvantartott_tetelek')
							     )
			                else '' 
					   end
		             ) as xml 
				   ) 
		   FOR xml PATH('tomeges_adatok')

end

--- temporary táblák törlése
IF OBJECT_ID('tempdb..#tmp_kozonsegeskuldemenyek') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_kozonsegeskuldemenyek
END
IF OBJECT_ID('tempdb..#tmp_nemz_kozonseges') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_nemz_kozonseges
END
IF OBJECT_ID('tempdb..#tmp_kozonsegeskuldemenyek_with_id') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_kozonsegeskuldemenyek_with_id
END
---
IF OBJECT_ID('tempdb..#tmp_belf_kozons_azon') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_belf_kozons_azon
END
IF OBJECT_ID('tempdb..#tmp_nemz_kozons_azon') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_nemz_kozons_azon
END
IF OBJECT_ID('tempdb..#tmp_kozonseges_azon_with_id') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_kozonseges_azon_with_id
END
---
IF OBJECT_ID('tempdb..#tmp_nyilvantartottkuldemenyek') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_nyilvantartottkuldemenyek
END
IF OBJECT_ID('tempdb..#tmp_nemz_nyilvantartott') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_nemz_nyilvantartott
END
IF OBJECT_ID('tempdb..#tmp_nyilvantartottkuldemenyek_with_id') IS NOT NULL 
BEGIN
   DROP TABLE #tmp_nyilvantartottkuldemenyek_with_id
END


/*
'KULDEMENY_KULDES_MODJA'
01	Postai sima	Postai sima
02	Postai tértivevényes	Postai tértivevényes
03	Postai ajánlott	Postai ajánlott
04	Ügyfél	Ügyfél
05	Állami futárszolgálat	Állami futárszolgálat
07	Diplomáciai futárszolgálat	Diplomáciai futárszolgálat
08	Légiposta	Légiposta
09	Kézbesítő útján	Kézbesítő útján
10	Fax	Fax
11	E-mail	Elektronikus
13	Helyben	
14	Postai távirat	Postai távirat
15	Portálon keresztül	Elektronikus
17	Postai elsőbbségi	NULL
18	Futárszolgálat	NULL

'KIMENO_KULDEMENY_FAJTA'
01	Szabvány levél 30 g-ig	NULL
02	Levél 50 g-ig	NULL
03	Levél 100 g-ig	NULL
04	Levél 250 g-ig	NULL
05	Levél 500 g-ig	NULL
06	Levél 750 g-ig	NULL
07	Levél 2000 g-ig	NULL
18	Levelezőlap	NULL
19	Hivatalos irat	Nincs megkülönböztetve, hogy elsőbbségi vagy sem
20	Hivatalos irat saját kézbe	Nincs megkülönböztetve, hogy elsőbbségi vagy sem

*/


/*
select * from EREC_KuldKuldemenyek kk
left join KRT_KodTarak k1 on kk.KuldesMod collate Hungarian_CS_AS = k1.Kod and k1.KodCsoport_Id=(select Id from KRT_KodCsoportok where Kod='KULDEMENY_KULDES_MODJA')
where PostazasIranya='2' and Allapot='06'

select * from EREC_KuldKuldemenyek kk
left join KRT_KodTarak k1 on kk.KimenoKuldemenyFajta collate Hungarian_CS_AS = k1.Kod and k1.KodCsoport_Id=(select Id from KRT_KodCsoportok where Kod='KIMENO_KULDEMENY_FAJTA')
where PostazasIranya='2' and Allapot='06'
and Kod not in '19' -- Hivatalos irat

select Kod, Nev, Egyeb, Note from KRT_KodTarak where KodCsoport_Id=(select Id from KRT_KodCsoportok where Kod='KIMENO_KULDEMENY_FAJTA')
group by Org, Kod, Nev, Egyeb, Note


KIMENO_KULDEMENY_AR_ELSOBBSEGI
KIMENO_KULDEMENY_AR_NEM_ELSOBBSEGI
KIMENO_KULDEMENY_AR_TOBBLETSZOLGALTATAS
*/
END TRY
BEGIN CATCH
	DECLARE @errorSeverity INT, @errorState INT
	DECLARE @errorCode NVARCHAR(1000)    
	SET @errorSeverity = ERROR_SEVERITY()
	SET @errorState = ERROR_STATE()
	
	if ERROR_NUMBER()<50000	
		SET @errorCode = '[' + convert(NVARCHAR(10),ERROR_NUMBER()) + '] ' + ERROR_MESSAGE()
	else
		SET @errorCode = ERROR_MESSAGE()

	RAISERROR(@errorCode,@errorSeverity,@errorState)
 
END CATCH

end  
